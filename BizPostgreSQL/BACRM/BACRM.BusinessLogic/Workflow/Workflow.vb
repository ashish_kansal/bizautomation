﻿
Option Explicit On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.Workflow
    Public Class Workflow
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _WFID As Long
        Public Property WFID() As Long
            Get
                Return _WFID
            End Get
            Set(ByVal value As Long)
                _WFID = value
            End Set
        End Property

        Private _WFName As String
        Public Property WFName() As String
            Get
                Return _WFName
            End Get
            Set(ByVal value As String)
                _WFName = value
            End Set
        End Property

        Private _WFDescription As String
        Public Property WFDescription() As String
            Get
                Return _WFDescription
            End Get
            Set(ByVal value As String)
                _WFDescription = value
            End Set
        End Property
        'added By Sachin||Date:21stFeb14
        'Purpose:To show Action Text in workflowList(Repeater)
        Private _vcWFAction As String
        Public Property vcWFAction() As String
            Get
                Return _vcWFAction
            End Get
            Set(ByVal value As String)
                _vcWFAction = value
            End Set
        End Property
        'End of code 
        'added By Sachin||Date:25thMarch14
        'Purpose:To implement Time based actions
        Private _vcDateField As String
        Public Property vcDateField() As String
            Get
                Return _vcDateField
            End Get
            Set(ByVal value As String)
                _vcDateField = value
            End Set
        End Property
        Private _intDays As Integer
        Public Property intDays() As Integer
            Get
                Return _intDays
            End Get
            Set(ByVal value As Integer)
                _intDays = value
            End Set
        End Property
        '
        Private _intActionOn As Integer
        Public Property intActionOn() As Integer
            Get
                Return _intActionOn
            End Get
            Set(ByVal value As Integer)
                _intActionOn = value
            End Set
        End Property
        Private _intAlertStatus As Integer
        Public Property intAlertStatus() As Integer
            Get
                Return _intAlertStatus
            End Get
            Set(ByVal value As Integer)
                _intAlertStatus = value
            End Set
        End Property

        Private _numWFAlertID As Integer
        Public Property numWFAlertID() As Integer
            Get
                Return _numWFAlertID
            End Get
            Set(value As Integer)
                _numWFAlertID = value
            End Set
        End Property
        'End of code

        Private _boolActive As Boolean
        Public Property boolActive() As Boolean
            Get
                Return _boolActive
            End Get
            Set(ByVal value As Boolean)
                _boolActive = value
            End Set
        End Property

        Private _FormID As Long
        Public Property FormID() As Long
            Get
                Return _FormID
            End Get
            Set(ByVal value As Long)
                _FormID = value
            End Set
        End Property

        Private _FieldID As Long
        Public Property FieldID() As Long
            Get
                Return _FieldID
            End Get
            Set(ByVal value As Long)
                _FieldID = value
            End Set
        End Property


        Private _WFTriggerOn As Short
        Public Property WFTriggerOn() As Short
            Get
                Return _WFTriggerOn
            End Get
            Set(ByVal value As Short)
                _WFTriggerOn = value
            End Set
        End Property

        Private _columnName As String
        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Private _columnSortOrder As String
        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Private _SortCharacter As Char
        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Private _TotalRecords As Integer
        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property
        Private _TotalHistoryRecords As Integer
        Public Property TotalHistoryRecords() As Integer
            Get
                Return _TotalHistoryRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalHistoryRecords = Value
            End Set
        End Property
        Private _PageSize As Integer
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Private _CurrentPage As Integer
        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Private _strText As String
        Public Property strText() As String
            Get
                Return _strText
            End Get
            Set(ByVal value As String)
                _strText = value
            End Set
        End Property
        Private _vcAlertMessage As String
        Public Property vcAlertMessage() As String
            Get
                Return _vcAlertMessage
            End Get
            Set(ByVal value As String)
                _vcAlertMessage = value
            End Set
        End Property



        Private _WFQueueID As Long
        Public Property WFQueueID() As Long
            Get
                Return _WFQueueID
            End Get
            Set(ByVal value As Long)
                _WFQueueID = value
            End Set
        End Property

        Private _tintProcessStatus As Short
        Public Property tintProcessStatus() As Short
            Get
                Return _tintProcessStatus
            End Get
            Set(ByVal value As Short)
                _tintProcessStatus = value
            End Set
        End Property

        Private _textQuery As String
        Public Property textQuery() As String
            Get
                Return _textQuery
            End Get
            Set(ByVal value As String)
                _textQuery = value
            End Set
        End Property

        Private _ClientTimeZoneOffset As Int32
        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _RecordID As Long
        Public Property RecordID() As Long
            Get
                Return _RecordID
            End Get
            Set(ByVal value As Long)
                _RecordID = value
            End Set
        End Property
        Private _boolCustom As Boolean
        Public Property boolCustom() As Boolean
            Get
                Return _boolCustom
            End Get
            Set(ByVal value As Boolean)
                _boolCustom = value
            End Set
        End Property
        Private _numFieldID As Long
        Public Property numFieldID() As Long
            Get
                Return _numFieldID
            End Get
            Set(ByVal value As Long)
                _numFieldID = value
            End Set
        End Property

        Private _FormFieldId As Long
        Public Property FormFieldId() As Long
            Get
                Return _FormFieldId
            End Get
            Set(ByVal value As Long)
                _FormFieldId = value
            End Set
        End Property

        Public Property WebsitePage As String
        Public Property WebsiteDays As Integer

        Function GetWorkFlowMaster() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFormID", _FormID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@SortChar", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char))

                    .Add(SqlDAL.Add_Parameter("@columnName", _columnName, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@SearchStr", _WFName, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetWorkFlowMasterList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DeleteWorkFlowMaster() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numWFID", _WFID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteWorkFlowMaster", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetWorkFlowFormFieldMaster() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFormID", _FormID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetWorkFlowFormFieldMaster", objParam, True)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function GetWorkFlowCompareFields() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFieldID", _FieldID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormID", _FormID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_WFACompareFields", objParam, True)

                Return ds.Tables(1)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function GetWorkFlowMasterDetail() As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numWFID", _WFID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur4", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur5", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(GetConnection.GetConnectionString, "USP_GetWorkFlowMasterDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Function ManageWorkFlowMaster() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numWFID", _WFID, NpgsqlTypes.NpgsqlDbType.Numeric, 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@vcWFName", _WFName, NpgsqlTypes.NpgsqlDbType.VarChar, 200))

                    .Add(SqlDAL.Add_Parameter("@vcWFDescription", _WFDescription, NpgsqlTypes.NpgsqlDbType.VarChar, 500))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@bitActive", _boolActive, NpgsqlTypes.NpgsqlDbType.Boolean))

                    .Add(SqlDAL.Add_Parameter("@numFormID", _FormID, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@tintWFTriggerOn", _WFTriggerOn, NpgsqlTypes.NpgsqlDbType.Numeric))
                    'added By Sachin||Date:21stFeb14
                    'Purpose:To show Action Text in workflowList(Repeater)
                    .Add(SqlDAL.Add_Parameter("@vcWFAction", _vcWFAction, NpgsqlTypes.NpgsqlDbType.Text))
                    'End by sachin
                    'added By Sachin||Date:25thMarch14
                    'Purpose:To Implement Time based actions
                    .Add(SqlDAL.Add_Parameter("@vcDateField", _vcDateField, NpgsqlTypes.NpgsqlDbType.Text))
                    .Add(SqlDAL.Add_Parameter("@intDays", _intDays, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intActionOn", _intActionOn, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@bitCustom", _boolCustom, NpgsqlTypes.NpgsqlDbType.Boolean))
                    .Add(SqlDAL.Add_Parameter("@numFieldID", _numFieldID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    'End by sachin

                    .Add(SqlDAL.Add_Parameter("@strText", _strText, NpgsqlTypes.NpgsqlDbType.Xml))
                    .Add(SqlDAL.Add_Parameter("@vcWebsitePage", WebsitePage, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@intWebsiteDays", WebsiteDays, NpgsqlTypes.NpgsqlDbType.Integer))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageWorkFlowMaster", objParam, True)
                _WFID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetDatasetToWFObject(ds As DataSet) As WorkFlowObject
            Try
                Dim objWF As New WorkFlowObject

                Dim dtWFList As DataTable = ds.Tables(0)
                Dim dtWFTriggerFields As DataTable = ds.Tables(1)
                Dim dtWFConditions As DataTable = ds.Tables(2)
                Dim dtWFActions As DataTable = ds.Tables(3)
                Dim dtWFActionUpdateFields As DataTable = ds.Tables(4)

                'json = JsonConvert.SerializeObject(ds, Formatting.None)

                objWF = New WorkFlowObject
                objWF.WFName = CCommon.ToString(dtWFList.Rows(0)("vcWFName"))
                objWF.WFDescription = CCommon.ToString(dtWFList.Rows(0)("vcWFDescription"))
                objWF.Active = CCommon.ToBool(dtWFList.Rows(0)("bitActive"))
                objWF.FormID = CCommon.ToLong(dtWFList.Rows(0)("numFormID"))
                objWF.WFTriggerOn = CCommon.ToShort(dtWFList.Rows(0)("tintWFTriggerOn"))

                objWF.TriggerFieldList = New List(Of String)

                For Each dr As DataRow In dtWFTriggerFields.Rows
                    objWF.TriggerFieldList.Add(dr("numFieldID").ToString() & "_" & dr("bitCustom").ToString())
                Next

                objWF.Conditions = New List(Of ConditionObject)

                For Each dr As DataRow In dtWFConditions.Rows
                    objWF.Conditions.Add(New ConditionObject() With {.Column = dr("numFieldID").ToString() & "_" & dr("bitCustom").ToString(), .FilterValue = dr("vcFilterValue").ToString(), .FilterOperator = dr("vcFilterOperator").ToString(), .FilterANDOR = dr("vcFilterANDOR").ToString()})
                Next

                objWF.Actions = New List(Of ActionObject)

                For Each dr As DataRow In dtWFActions.Rows
                    Dim ActionUpdateFields As New List(Of ActionUpdateFieldsObject)

                    Dim foundRows() As DataRow = dtWFActionUpdateFields.Select("numWFActionID = " & dr("numWFActionID"))
                    If foundRows.Length > 0 Then
                        For Each drUF As DataRow In foundRows
                            ActionUpdateFields.Add(New ActionUpdateFieldsObject() With {.Column = drUF("numFieldID") & "_" & drUF("bitCustom"), .tintModuleType = drUF("tintModuleType"), .vcValue = drUF("vcValue")})
                        Next
                    End If

                    objWF.Actions.Add(New ActionObject() With {.ActionType = dr("tintActionType") _
                                                                , .TemplateID = dr("numTemplateID") _
                                                                , .vcEmailToType = dr("vcEmailToType").ToString() _
                                                                , .TicklerActionAssignedTo = dr("tintTicklerActionAssignedTo") _
                                                                , .vcAlertMessage = dr("vcAlertMessage").ToString() _
                                                              , .ActionUpdateFields = ActionUpdateFields})
                Next

                Return objWF
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetWorkFlowQueue() As DataTable
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_GetWorkFlowQueue", sqlParams.ToArray())
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Public Function ManageWorkFlowQueue(ByVal Mode As Short, Optional ByVal _bitSuccess As Boolean = False) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numWFQueueID", _WFQueueID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormID", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintProcessStatus", _tintProcessStatus, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintWFTriggerOn", 0, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintMode", Mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcColumnsUpdated", "", NpgsqlTypes.NpgsqlDbType.VarChar, 1000))
                    .Add(SqlDAL.Add_Parameter("@numWFID", _WFID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitSuccess", _bitSuccess, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcDescription", _WFDescription, NpgsqlTypes.NpgsqlDbType.Text))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageWorkFlowQueue", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function WFConditionQueryExecute(ByVal Mode As Short) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@textQuery", _textQuery, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@boolCondition", False, NpgsqlTypes.NpgsqlDbType.Bit, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@tintMode", Mode, NpgsqlTypes.NpgsqlDbType.Smallint))

                End With

                Dim objParam() As Object = sqlParams.ToArray()

                SqlDAL.ExecuteNonQuery(connString, "USP_WFConditionQueryExecute", objParam, True)
                Return CBool(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWFRecordData() As DataTable
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFormID", _FormID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_GetWFRecordData", sqlParams.ToArray())
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function
        Function SaveWorkFlowAlerts() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@vcAlertMessage", _vcAlertMessage, NpgsqlTypes.NpgsqlDbType.Text))
                    .Add(SqlDAL.Add_Parameter("@numWFID", _WFID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@intAlertStatus", _intAlertStatus, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_WorkFlowAlertList_Insert", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function UpdateWorkFlowAlertStatus() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numWFAlertID", _numWFAlertID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@intAlertStatus", _intAlertStatus, NpgsqlTypes.NpgsqlDbType.Integer))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_WorkflowAlertList_UpdateStatus", objParam, True)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAlertMessageData() As DataSet
            Dim dsResult As New DataSet

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dsResult = SqlDAL.ExecuteDataset(connString, "USP_WorkflowAlertList_SelectAll", sqlParams.ToArray())
            Catch ex As Exception
                dsResult = Nothing
                Throw ex
            End Try

            Return dsResult
        End Function

        Public Function ShowHistory() As DataSet
            Dim dsResult As New DataSet

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormID", FormID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalHistoryRecords, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SortChar", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@columnName", _columnName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@SearchStr", _WFName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                Dim objParams() As Object = sqlParams.ToArray()
                dsResult = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_WorkFlowMaster_History", objParams, True)
                _TotalHistoryRecords = CInt(DirectCast(objParams, Npgsql.NpgsqlParameter())(4).Value)
                '_TotalHistoryRecords = 20

                Return dsResult
            Catch ex As Exception
                dsResult = Nothing
                Throw ex
            End Try


        End Function

        'Author:Sachin Sadhu||Date:28Apr2014
        'Purpose: To enter Opportunity Data into Work Flow Queue 

        Function SaveWFOrderQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code
        'Author:Sachin Sadhu||Date:29Apr2014
        'Purpose: To enter Biz Doc  Data into Work Flow Queue 

        Function SaveWFBizDocQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityBizDocs_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code
        'Author:Sachin Sadhu||Date:14thMay2014
        'Purpose: To enter Biz Doc  Data into Work Flow Queue 

        Function SaveWFBusinessProcessQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_StagePercentageDetails_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code
        'Author:Sachin Sadhu||Date:22ndMay2014
        'Purpose: To enter Organization data into Work Flow Queue 

        Function SaveWFOrganizationQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_Organization_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code

        'Author:Sachin Sadhu||Date:23rdJuly2014
        'Purpose: To enter Contact data into Work Flow Queue 

        Function SaveWFContactQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_Contacts_CT", objParam, True)


                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code


        'Author:Sachin Sadhu||Date:29thJuly2014
        'Purpose: To enter Project data into Work Flow Queue 

        Function SaveWFProjectsQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_Projects_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code


        'Author:Sachin Sadhu||Date:30thJuly2014
        'Purpose: To enter Cases data into Work Flow Queue 
        Function SaveWFCasesQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_Cases_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code



        'Author:Sachin Sadhu||Date:4thAug2014
        'Purpose: To enter Cases data into Work Flow Queue 
        Function SaveWFActionItemsQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ActionItems_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code


        Function WFCustomFieldsInsert(ByVal Fld_ID As Long, ByVal Fld_Value As String) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams


                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numFormID", FormID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@Fld_ID", Fld_ID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@Fld_Value", Fld_Value, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))
                    .Add(SqlDAL.Add_Parameter("@boolInsert", False, NpgsqlTypes.NpgsqlDbType.Bit, ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                SqlDAL.ExecuteNonQuery(connString, "USP_WFCustomFieldsUpdate", objParam, True)
                Return CBool(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Author:Sachin Sadhu||Date:14thOct2014
        'Purpose: To enter Custom Fields for Organization data into Work Flow Queue 
        Function SaveWFCFOrganizationQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormFieldId", _FormFieldId, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_CFW_FLD_Values_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code

        'Author:Sachin Sadhu||Date:14thOct2014
        'Purpose: To enter Custom Fields for Organization data into Work Flow Queue 
        Function SaveWFCFContactsQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormFieldId", _FormFieldId, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_CFW_FLD_Values_Cont_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code

        'Author:Sachin Sadhu||Date:14thOct2014
        'Purpose: To enter Custom Fields for Organization data into Work Flow Queue 
        Function SaveWFCFOrdersQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormFieldId", _FormFieldId, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_CFW_Fld_Values_Opp_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code
        'Author:Sachin Sadhu||Date:14thOct2014
        'Purpose: To enter Custom Fields for Organization data into Work Flow Queue 
        Function SaveWFCFProjectsQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormFieldId", _FormFieldId, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_CFW_FLD_Values_Pro_CT", objParam, True)

                '
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code
        'Author:Sachin Sadhu||Date:14thOct2014
        'Purpose: To enter Custom Fields for Organization data into Work Flow Queue 
        Function SaveWFCFCasesQueue() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", _RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormFieldId", _FormFieldId, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_CFW_FLD_Values_Case_CT", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code

        ' Date Field Workflows methods

        Public Function GetDateFieldWorkFlows() As DataTable
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_GetDateFieldWorkFlows", sqlParams.ToArray())
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Public Function GetDateFieldWorkFlowQueue(ByVal numFormID As Long, ByVal numWFID As Long, ByVal textCondition As String, ByVal textConditionCustom As String) As DataTable
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormID", numFormID, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numWFID", numWFID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@textCondition", textCondition, NpgsqlTypes.NpgsqlDbType.VarChar, 2000))
                    .Add(SqlDAL.Add_Parameter("@textConditionCustom", textConditionCustom, NpgsqlTypes.NpgsqlDbType.Varchar, 2000))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_GetDateFieldWorkFlowQueue", sqlParams.ToArray())
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Function SaveWorkFlowDateFieldExecutionHistory(ByVal numRecordId As Long, ByVal numFormID As Long) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", numRecordId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormID", numFormID, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numWFID", _WFID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_WorkFlowDateFieldExecutionHistory_Insert", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ' Date Field Workflows methods

        ' A/R Aging Worflow Methods

        Public Function GetARAgingWorkFlows() As DataTable
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_GetARAgingWorkFlows", sqlParams.ToArray())
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Public Function GetARAgingWorkFlowQueue(ByVal numUserCntID As Long, ByVal numPastDueDays As Long) As DataTable
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", numUserCntID, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@NoOfDaysPastDue", numPastDueDays, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_GetARAgingWorkFlowQueue", sqlParams.ToArray())
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Function SaveWorkFlowARAgingExecutionHistory(ByVal numRecordId As Long, ByVal numFormID As Long) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", numRecordId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFormID", numFormID, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numWFID", _WFID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_WorkFlowARAgingExecutionHistory_Insert", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ' A/R Aging Worflow Methods
    End Class
End Namespace

