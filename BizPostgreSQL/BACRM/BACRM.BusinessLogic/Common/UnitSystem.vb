Imports System.Web.UI.WebControls
Imports System.Web

Namespace BACRM.BusinessLogic.Common '
    Public Class UnitSystem
        Inherits BACRM.BusinessLogic.CBusinessBase


        Sub AddUnitSystemItems(ByVal ddlUnits As DropDownList, ByVal UnitSystem As String)
            Dim objCommon As New CCommon
            objCommon.sb_FillComboFromDB(ddlUnits, 175, HttpContext.Current.Session("DomainID")) ' For Production ID is 175 and Local 164 change it as per req
            Dim lstItem As ListItem
            If UnitSystem = "E" Then

                lstItem = New ListItem
                lstItem.Text = "Units"
                lstItem.Value = "U"
                ddlUnits.Items.Add(lstItem)


                lstItem = New ListItem
                lstItem.Text = "Inch"
                lstItem.Value = "in"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Foot"
                lstItem.Value = "ft"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Yard"
                lstItem.Value = "yd"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Mile"
                lstItem.Value = "mi"
                ddlUnits.Items.Add(lstItem)


                lstItem = New ListItem
                lstItem.Text = "Grain"
                lstItem.Value = "gr"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Ounce"
                lstItem.Value = "oz"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Pound"
                lstItem.Value = "lb"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Quart"
                lstItem.Value = "qt"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Gallon"
                lstItem.Value = "gal"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Hours"
                lstItem.Value = "hr"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Minutes"
                lstItem.Value = "min"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Kits"
                lstItem.Value = "K"
                ddlUnits.Items.Add(lstItem)


            ElseIf UnitSystem = "M" Then

                lstItem = New ListItem
                lstItem.Text = "Units"
                lstItem.Value = "U"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Millimeter"
                lstItem.Value = "mm"
                ddlUnits.Items.Add(lstItem)


                lstItem = New ListItem
                lstItem.Text = "Centimeter"
                lstItem.Value = "cm"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Meter"
                lstItem.Value = "m"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Kilometer"
                lstItem.Value = "km"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Milligram"
                lstItem.Value = "mg"
                ddlUnits.Items.Add(lstItem)


                lstItem = New ListItem
                lstItem.Text = "Gram"
                lstItem.Value = "g"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Kilogram"
                lstItem.Value = "kg"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Liter"
                lstItem.Value = "l"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Hours"
                lstItem.Value = "hr"
                ddlUnits.Items.Add(lstItem)

                lstItem = New ListItem
                lstItem.Text = "Minutes"
                lstItem.Value = "min"
                ddlUnits.Items.Add(lstItem)



                lstItem = New ListItem
                lstItem.Text = "Kits"
                lstItem.Value = "K"
                ddlUnits.Items.Add(lstItem)
            End If

        End Sub

        Public Function ConversionofUnits(ByVal valueForCoversion As String, ByVal UnitBeforeConversion As String, ByVal UnitAfterConversion As String, ByVal UnitSystem As String) As String
            Dim strValue As String = "0"
            If UnitSystem = "E" Then
                If UnitBeforeConversion = "in" Then
                    If UnitAfterConversion = "ft" Then
                        strValue = valueForCoversion * 0.08333
                    ElseIf UnitAfterConversion = "yd" Then
                        strValue = valueForCoversion * 0.02778
                    ElseIf UnitAfterConversion = "mi" Then
                        strValue = valueForCoversion * 0.00002
                    End If
                ElseIf UnitBeforeConversion = "ft" Then
                    If UnitAfterConversion = "in" Then
                        strValue = valueForCoversion * 12
                    ElseIf UnitAfterConversion = "yd" Then
                        strValue = valueForCoversion * 0.33333
                    ElseIf UnitAfterConversion = "mi" Then
                        strValue = valueForCoversion * 0.00019
                    End If
                ElseIf UnitBeforeConversion = "mi" Then
                    If UnitAfterConversion = "in" Then
                        strValue = valueForCoversion * 63360.0
                    ElseIf UnitAfterConversion = "yd" Then
                        strValue = valueForCoversion * 1760
                    ElseIf UnitAfterConversion = "ft" Then
                        strValue = valueForCoversion * 5280
                    End If
                ElseIf UnitBeforeConversion = "yd" Then
                    If UnitAfterConversion = "in" Then
                        strValue = valueForCoversion * 36
                    ElseIf UnitAfterConversion = "mi" Then
                        strValue = valueForCoversion * 0.00057
                    ElseIf UnitAfterConversion = "ft" Then
                        strValue = valueForCoversion * 3
                    End If
                ElseIf UnitBeforeConversion = "gr" Then
                    If UnitAfterConversion = "oz" Then
                        strValue = valueForCoversion * 0.00228571429
                    ElseIf UnitAfterConversion = "lb" Then
                        strValue = valueForCoversion * 0.000142857143
                    End If
                ElseIf UnitBeforeConversion = "oz" Then
                    If UnitAfterConversion = "gr" Then
                        strValue = valueForCoversion * 437.5
                    ElseIf UnitAfterConversion = "lb" Then
                        strValue = valueForCoversion * 0.0625
                    End If
                ElseIf UnitBeforeConversion = "lb" Then
                    If UnitAfterConversion = "gr" Then
                        strValue = valueForCoversion * 7000
                    ElseIf UnitAfterConversion = "oz" Then
                        strValue = valueForCoversion * 16
                    End If
                ElseIf UnitBeforeConversion = "qt" Then
                    If UnitAfterConversion = "gal" Then
                        strValue = valueForCoversion * 0.25
                    End If
                ElseIf UnitBeforeConversion = "gal" Then
                    If UnitAfterConversion = "qt" Then
                        strValue = valueForCoversion * 4
                    End If
                End If
            ElseIf UnitSystem = "M" Then
                If UnitBeforeConversion = "mm" Then
                    If UnitAfterConversion = "cm" Then
                        strValue = valueForCoversion * 0.1
                    ElseIf UnitAfterConversion = "m" Then
                        strValue = valueForCoversion * 0.001
                    ElseIf UnitAfterConversion = "km" Then
                        strValue = valueForCoversion * 0.000001
                    End If
                ElseIf UnitBeforeConversion = "cm" Then
                    If UnitAfterConversion = "mm" Then
                        strValue = valueForCoversion * 10
                    ElseIf UnitAfterConversion = "m" Then
                        strValue = valueForCoversion * 0.01
                    ElseIf UnitAfterConversion = "km" Then
                        strValue = valueForCoversion * 0.00001
                    End If
                ElseIf UnitBeforeConversion = "m" Then
                    If UnitAfterConversion = "mm" Then
                        strValue = valueForCoversion * 1000
                    ElseIf UnitAfterConversion = "cm" Then
                        strValue = valueForCoversion * 100
                    ElseIf UnitAfterConversion = "km" Then
                        strValue = valueForCoversion * 0.001
                    End If
                ElseIf UnitBeforeConversion = "km" Then
                    If UnitAfterConversion = "mm" Then
                        strValue = valueForCoversion * 1000000
                    ElseIf UnitAfterConversion = "cm" Then
                        strValue = valueForCoversion * 100000
                    ElseIf UnitAfterConversion = "m" Then
                        strValue = valueForCoversion * 1000
                    End If
                End If
            End If
            Return strValue
        End Function

    End Class
End Namespace

