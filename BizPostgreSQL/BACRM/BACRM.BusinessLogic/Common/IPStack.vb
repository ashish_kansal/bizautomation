﻿Imports Newtonsoft.Json

Namespace BACRM.BusinessLogic.Common
    Public Class IPStack
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property ip As String
        Public Property country_name As String
        Public Property country_code As String
        Public Property city As String
        Public Property region_name As String
        Public Property zip As String
        Public Property latitude As String
        Public Property longitude As String

        Public Property CountryID As Long
        Public Property StateID As Long

#End Region

#Region "Public Methods"

        Public Sub GetUserLocationDetailFromIp(ByVal ipAddress As String)
            Try
                Dim APIKey As String = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("IPStackAccessKey"))
                Dim url As String = String.Format("http://api.ipstack.com/{0}?access_key={1}", ipAddress, APIKey)

                Using client As New Net.WebClient()
                    Dim json As String = client.DownloadString(url)
                    Dim location As IPStack = Newtonsoft.Json.JsonConvert.DeserializeObject(Of IPStack)(json)

                    Me.ip = ipAddress         
                    Me.country_name = location.country_name
                    Me.country_code = location.country_code
                    Me.city = location.city
                    Me.region_name = location.region_name
                    Me.zip = location.zip
                    Me.latitude = location.latitude
                    Me.longitude = location.longitude

                    Dim objCommon As New CCommon
                    objCommon.DomainID = Me.DomainID
                    Dim dt As DataTable = objCommon.GetStateNCountryIds(Me.country_name, Me.region_name)

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        Me.CountryID = CCommon.ToLong(dt.Rows(0)("CountryId"))
                        Me.StateID = CCommon.ToLong(dt.Rows(0)("StateId"))
                    End If
                End Using
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class
End Namespace

