﻿Imports System
Imports System.Collections.Generic
Imports System.Globalization
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Converters
Imports J = Newtonsoft.Json.JsonPropertyAttribute
Imports R = Newtonsoft.Json.Required
Imports N = Newtonsoft.Json.NullValueHandling
Imports System.Net
Imports System.IO


Namespace BACRM.BusinessLogic.Common

    Public Enum BuiltWithIsPremium
        Maybe
        No
        Yes
    End Enum

    Public Enum BuiltWithUrl
        InternalPage
        Root
        Mobile
    End Enum

    Public Class BuiltWith

#Region "Public Properties"

        <J("Results")>
        Public Property Results As BuiltWithResultElement()
        <J("Errors")>
        Public Property Errors As Object()

        Public Property JsonResult As String

#End Region

#Region "Public Methods"

        Public Function GetDomainDetails(ByVal website As String) As BuiltWith
            Try
                Dim webReq As HttpWebRequest = CType(WebRequest.Create("https://api.builtwith.com/v18/api.json?KEY=" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BuiltWithAPIKey")) & "&LOOKUP=" & website), HttpWebRequest)

                Using response As WebResponse = webReq.GetResponse()
                    Using responseStream As New StreamReader(response.GetResponseStream(), False)
                        Dim data As String = responseStream.ReadToEnd()
                        Dim objTemp As BuiltWith = FromJson(data)
                        objTemp.JsonResult = data
                        Return objTemp
                    End Using
                End Using
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function FromJson(ByVal json As String) As BuiltWith
            Return JsonConvert.DeserializeObject(Of BuiltWith)(json, New JsonSerializerSettings With {.MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            .DateParseHandling = DateParseHandling.None,
            .Converters = New List(Of JsonConverter) From
            {
            IsPremiumConverter.Singleton,
            UrlConverter.Singleton,
            New IsoDateTimeConverter With {.DateTimeStyles = DateTimeStyles.AssumeUniversal}
            }})
        End Function

#End Region

    End Class

    Partial Public Class BuiltWithResultElement
        <J("Result")>
        Public Property Result As BuiltWithResultResult
        <J("Meta")>
        Public Property Meta As BuiltWithMeta
        <J("Attributes")>
        Public Property Attributes As Dictionary(Of String, Long)
        <J("FirstIndexed")>
        Public Property FirstIndexed As Long
        <J("LastIndexed")>
        Public Property LastIndexed As Long
        <J("Lookup")>
        Public Property Lookup As String
    End Class

    Partial Public Class BuiltWithMeta
        <J("Majestic")>
        Public Property Majestic As Long
        <J("Umbrella")>
        Public Property Umbrella As Long
        <J("Vertical")>
        Public Property Vertical As String
        <J("Social")>
        Public Property Social As Uri()
        <J("CompanyName")>
        Public Property CompanyName As String
        <J("Telephones")>
        Public Property Telephones As String()
        <J("Emails")>
        Public Property Emails As Object()
        <J("City")>
        Public Property City As String
        <J("State")>
        Public Property State As String
        <J("Postcode")>
        Public Property Postcode As String
        <J("Country")>
        Public Property Country As String
        <J("Names")>
        Public Property Names As BuiltWithName()
        <J("ARank")>
        Public Property ARank As Long
        <J("QRank")>
        Public Property QRank As Long
    End Class

    Partial Public Class BuiltWithName
        <J("Name")>
        Public Property NameName As String
        <J("Type")>
        Public Property Type As Long
        <J("Email")>
        Public Property Email As String
    End Class

    Partial Public Class BuiltWithResultResult
        <J("IsDB")>
        Public Property IsDb As String
        <J("Spend")>
        Public Property Spend As Long
        <J("Paths")>
        Public Property Paths As BuiltWithPath()
    End Class

    Partial Public Class BuiltWithPath
        <J("FirstIndexed")>
        Public Property FirstIndexed As Long
        <J("LastIndexed")>
        Public Property LastIndexed As Long
        <J("Domain")>
        Public Property Domain As String
        <J("Url")>
        Public Property Url As BuiltWithUrl
        <J("SubDomain")>
        Public Property SubDomain As String
        <J("Technologies")>
        Public Property Technologies As BuiltWithTechnology()
    End Class

    Partial Public Class BuiltWithTechnology
        <J("Categories")>
        Public Property Categories As String()
        <J("IsPremium")>
        Public Property IsPremium As BuiltWithIsPremium
        <J("Name")>
        Public Property Name As String
        <J("Description")>
        Public Property Description As String
        <J("Link")>
        Public Property Link As Uri
        <J("Tag")>
        Public Property Tag As String
        <J("FirstDetected")>
        Public Property FirstDetected As Long
        <J("LastDetected")>
        Public Property LastDetected As Long
    End Class

    Friend Class IsPremiumConverter
        Inherits JsonConverter

        Public Overrides Function CanConvert(ByVal t As Type) As Boolean
            Return t = GetType(BuiltWithIsPremium) OrElse t = GetType(BuiltWithIsPremium?)
        End Function

        Public Overrides Function ReadJson(ByVal reader As JsonReader, ByVal t As Type, ByVal existingValue As Object, ByVal serializer As JsonSerializer) As Object
            If reader.TokenType = JsonToken.Null Then Return Nothing

            Select Case serializer.Deserialize(Of String)(reader)
                Case "maybe"
                    Return BuiltWithIsPremium.Maybe
                Case "no"
                    Return BuiltWithIsPremium.No
                Case "yes"
                    Return BuiltWithIsPremium.Yes
            End Select

            Throw New Exception("Cannot unmarshal type IsPremium")
        End Function

        Public Overrides Sub WriteJson(ByVal writer As JsonWriter, ByVal untypedValue As Object, ByVal serializer As JsonSerializer)
            If untypedValue Is Nothing Then
                serializer.Serialize(writer, Nothing)
                Return
            End If

            Select Case CType(untypedValue, BuiltWithIsPremium)
                Case BuiltWithIsPremium.Maybe
                    serializer.Serialize(writer, "maybe")
                    Return
                Case BuiltWithIsPremium.No
                    serializer.Serialize(writer, "no")
                    Return
                Case BuiltWithIsPremium.Yes
                    serializer.Serialize(writer, "yes")
                    Return
            End Select

            Throw New Exception("Cannot marshal type IsPremium")
        End Sub

        Public Shared ReadOnly Singleton As IsPremiumConverter = New IsPremiumConverter()
    End Class

    Friend Class UrlConverter
        Inherits JsonConverter

        Public Overrides Function CanConvert(ByVal t As Type) As Boolean
            Return t = GetType(BuiltWithUrl) OrElse t = GetType(BuiltWithUrl?)
        End Function

        Public Overrides Function ReadJson(ByVal reader As JsonReader, ByVal t As Type, ByVal existingValue As Object, ByVal serializer As JsonSerializer) As Object
            If reader.TokenType = JsonToken.Null Then Return Nothing

            Select Case serializer.Deserialize(Of String)(reader)
                Case ""
                    Return BuiltWithUrl.Root
                Case "dd"
                    Return BuiltWithUrl.InternalPage
                Case "m"
                    Return BuiltWithUrl.Mobile
            End Select

            Throw New Exception("Cannot unmarshal type Url")
        End Function

        Public Overrides Sub WriteJson(ByVal writer As JsonWriter, ByVal untypedValue As Object, ByVal serializer As JsonSerializer)
            If untypedValue Is Nothing Then
                serializer.Serialize(writer, Nothing)
                Return
            End If

            Select Case CType(untypedValue, BuiltWithUrl)
                Case BuiltWithUrl.Root
                    serializer.Serialize(writer, "")
                    Return
                Case BuiltWithUrl.InternalPage
                    serializer.Serialize(writer, "dd")
                    Return
                Case BuiltWithUrl.Mobile
                    serializer.Serialize(writer, "m")
                    Return
            End Select

            Throw New Exception("Cannot marshal type Url")
        End Sub

        Public Shared ReadOnly Singleton As UrlConverter = New UrlConverter()
    End Class


End Namespace