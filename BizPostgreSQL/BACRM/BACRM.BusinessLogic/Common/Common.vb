Option Explicit On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Web.UI.WebControls
Imports System
Imports System.IO
Imports System.Text
Imports System.Security.Cryptography
Imports BACRM.BusinessLogic.Admin
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports Telerik.Web.UI
Imports System.Text.RegularExpressions
Imports System.Collections.Generic
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Web.Script.Serialization

Namespace BACRM.BusinessLogic.Common

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CCommon
    ' Modified By:Anoop Jayaraj
    ' Description  : This is the business logic classe for Common Stuff
    '**********************************************************************************
    Public Class CCommon
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        'Public Shared m_PageRightsArray As New Hashtable    ' declare a hashtable
        'Public Const m_ViewRights As String = "m_ViewRights" 'For holding key name for View right
        'Public Const m_AddRights As String = "m_AddRights" 'For holding key name for add right
        'Public Const m_UpdateRights As String = "m_UpdateRights" 'For holding key name for update right
        'Public Const m_DeleteRights As String = "m_DeleteRights"  'For holding key name for delete right.
        'Public Const m_ExportRights As String = "m_ExportRights"   'For holding key name for export right
        'Public Const m_PrintRights As String = "m_PrintRights"
        'this is added to get value of parameter from presentation layer
        Private _UserID As Integer = 0
        Private _PageName As String = String.Empty
        Private _ModuleID As Integer = 0

        Private _PageID As Integer = 0
        'Private DomainId As Long
        Private _TerittoryID As Long
        Private _ContactID As Long = 0
        Private _ListID As Long
        Private _CreatedBy As Long
        Private _byteMode As Short
        Private _EmailID As String
        Private _Signature As String
        Private _CompanyName As String
        Private _DivisionID As Long
        Private _AdminID As Long
        Private _CaseID As Long
        Private _ContractID As Long
        Private _CommID As Long
        Private _OppID As Long
        Private _ProID As Long
        Private _CRMType As Short
        Private _CompID As Long
        Private _ContactName As String
        Private _Phone As String
        'Private UserCntID As Long
        Private _ListItemID As Long
        Private _ListItemName As String
        Private _tinyOrder As Int32
        Private _xmlStr As String = ""
        Private _numAuthoritativeMode As Int32
        Private _numAuthoritativePurchase As Integer
        Private _numAuthoritativeSales As Integer
        Private _ISPresent As Boolean
        Private _CountSalesBizDocs As Integer
        Private _CountPurchaseBizDocs As Integer
        Private _Password As String
        Private _charModule As Char

        Private _intOption As Integer
        Private _FieldRelID As Long
        Private _FieldRelDTLID As Long
        Private _PrimaryListID As Long
        Private _PrimaryListItemID As Long
        Private _SecondaryListID As Long
        Private _SecondaryListItemID As Long
        Private _Mode As Short
        Private _ContactType As Long
        Private _Filter As String
        Private _Comments As String
        Private _Heading As String
        Private _CommentID As Long

        Private _PaymentGWId As Integer

        Private _lngTabId As Long

        'Added By:Sachin Sadhu||Date:12thJune2014
        'Purpose:Added One Field numListType to save  bizdoc type wise BizDoc status   
        Private _lngListType As Long

        Private _FollowUpCampaign As String
        Private _LastFollowup As String
        Private _NextFollowup As String
        Private _PhoneExt As String
        Private _CustomerRelation As String

        Public Property PhoneExt() As String
            Get
                Return _PhoneExt
            End Get
            Set(ByVal value As String)
                _PhoneExt = value
            End Set
        End Property
        Public Property CustomerRelation() As String
            Get
                Return _CustomerRelation
            End Get
            Set(ByVal value As String)
                _CustomerRelation = value
            End Set
        End Property
        Public Property lngListType() As Long
            Get
                Return _lngListType
            End Get
            Set(ByVal value As Long)
                _lngListType = value
            End Set
        End Property
        'End of Code
        Private _numSiteId As Long
        Public Property numSiteId As Long
            Get
                Return _numSiteId
            End Get
            Set(ByVal value As Long)
                _numSiteId = value
            End Set
        End Property
        Public Property TabId() As Long
            Get
                Return _lngTabId
            End Get
            Set(ByVal value As Long)
                _lngTabId = value
            End Set
        End Property

        Private _lngGroupID As Long
        Public Property GroupID() As Long
            Get
                Return _lngGroupID
            End Get
            Set(ByVal value As Long)
                _lngGroupID = value
            End Set
        End Property

        Private _lngParentID As Long
        Public Property lngParentID() As Long
            Get
                Return _lngParentID
            End Get
            Set(ByVal value As Long)
                _lngParentID = value
            End Set
        End Property

        Private _iMode As Long
        Public Property iMode() As Long
            Get
                Return _iMode
            End Get
            Set(ByVal value As Long)
                _iMode = value
            End Set
        End Property

        Public Property PaymentGWId() As Integer
            Get
                Return _PaymentGWId
            End Get
            Set(ByVal value As Integer)
                _PaymentGWId = value
            End Set
        End Property

        Private _StageID As Long
        Public Property StageID() As Long
            Get
                Return _StageID
            End Get
            Set(ByVal value As Long)
                _StageID = value
            End Set
        End Property


        Public Property Filter() As String
            Get
                Return _Filter
            End Get
            Set(ByVal Value As String)
                _Filter = Value
            End Set
        End Property

        Public Property ContactType() As Long
            Get
                Return _ContactType
            End Get
            Set(ByVal Value As Long)
                _ContactType = Value
            End Set
        End Property

        Public Property CommentID() As Long
            Get
                Return _CommentID
            End Get
            Set(ByVal Value As Long)
                _CommentID = Value
            End Set
        End Property


        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal Value As String)
                _Comments = Value
            End Set
        End Property


        Public Property Heading() As String
            Get
                Return _Heading
            End Get
            Set(ByVal Value As String)
                _Heading = Value
            End Set
        End Property

        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal value As Short)
                _Mode = value
            End Set
        End Property



        Public Property SecondaryListItemID() As Long
            Get
                Return _SecondaryListItemID
            End Get
            Set(ByVal value As Long)
                _SecondaryListItemID = value
            End Set
        End Property


        Public Property SecondaryListID() As Long
            Get
                Return _SecondaryListID
            End Get
            Set(ByVal value As Long)
                _SecondaryListID = value
            End Set
        End Property


        Public Property PrimaryListItemID() As Long
            Get
                Return _PrimaryListItemID
            End Get
            Set(ByVal value As Long)
                _PrimaryListItemID = value
            End Set
        End Property



        Public Property PrimaryListID() As Long
            Get
                Return _PrimaryListID
            End Get
            Set(ByVal value As Long)
                _PrimaryListID = value
            End Set
        End Property



        Public Property FieldRelDTLID() As Long
            Get
                Return _FieldRelDTLID
            End Get
            Set(ByVal value As Long)
                _FieldRelDTLID = value
            End Set
        End Property



        Public Property FieldRelID() As Long
            Get
                Return _FieldRelID
            End Get
            Set(ByVal value As Long)
                _FieldRelID = value
            End Set
        End Property


        Public Property intOption() As Integer
            Get
                Return _intOption
            End Get
            Set(ByVal value As Integer)
                _intOption = value
            End Set
        End Property



        Public Property charModule() As Char
            Get
                Return _charModule
            End Get
            Set(ByVal Value As Char)
                _charModule = Value
            End Set
        End Property


        Public Property Password() As String
            Get
                Return _Password
            End Get
            Set(ByVal Value As String)
                _Password = Value
            End Set
        End Property


        Public Property xmlStr() As String
            Get
                Return _xmlStr
            End Get
            Set(ByVal Value As String)
                _xmlStr = Value
            End Set
        End Property

        Public Property tinyOrder() As Int32
            Get
                Return _tinyOrder
            End Get
            Set(ByVal Value As Int32)
                _tinyOrder = Value
            End Set
        End Property
        Public Property ListItemName() As String
            Get
                Return _ListItemName
            End Get
            Set(ByVal Value As String)
                _ListItemName = Value
            End Set
        End Property

        Public Property ListItemID() As Long
            Get
                Return _ListItemID
            End Get
            Set(ByVal Value As Long)
                _ListItemID = Value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property Phone() As String
            Get
                Return _Phone
            End Get
            Set(ByVal Value As String)
                _Phone = Value
            End Set
        End Property

        Public Property ContactName() As String
            Get
                Return _ContactName
            End Get
            Set(ByVal Value As String)
                _ContactName = Value
            End Set
        End Property

        Public Property CompID() As Long
            Get
                Return _CompID
            End Get
            Set(ByVal Value As Long)
                _CompID = Value
            End Set
        End Property

        Public Property CRMType() As Short
            Get
                Return _CRMType
            End Get
            Set(ByVal Value As Short)
                _CRMType = Value
            End Set
        End Property

        Public Property ProID() As Long
            Get
                Return _ProID
            End Get
            Set(ByVal Value As Long)
                _ProID = Value
            End Set
        End Property

        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal Value As Long)
                _OppID = Value
            End Set
        End Property

        Public Property CommID() As Long
            Get
                Return _CommID
            End Get
            Set(ByVal Value As Long)
                _CommID = Value
            End Set
        End Property

        Public Property CaseID() As Long
            Get
                Return _CaseID
            End Get
            Set(ByVal Value As Long)
                _CaseID = Value
            End Set
        End Property

        '' modified by shaziya
        ''Contracts
        Public Property ContractId() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property



        Public Property AdminID() As Long
            Get
                Return _AdminID
            End Get
            Set(ByVal Value As Long)
                _AdminID = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property CompanyName() As String
            Get
                Return _CompanyName
            End Get
            Set(ByVal Value As String)
                _CompanyName = Value
            End Set
        End Property

        Public Property Signature() As String
            Get
                Return _Signature
            End Get
            Set(ByVal Value As String)
                _Signature = Value
            End Set
        End Property



        Public Property EmailID() As String
            Get
                Return _EmailID
            End Get
            Set(ByVal Value As String)
                _EmailID = Value
            End Set
        End Property

        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Public Property CreatedBy() As Long
            Get
                Return _CreatedBy
            End Get
            Set(ByVal Value As Long)
                _CreatedBy = Value
            End Set
        End Property

        Public Property ListID() As Long
            Get
                Return _ListID
            End Get
            Set(ByVal Value As Long)
                _ListID = Value
            End Set
        End Property
        Public Property PageName() As String
            Get
                Return _PageName
            End Get
            Set(ByVal Value As String)
                _PageName = Value
            End Set
        End Property

        Public Property UserID() As Integer
            Get
                Return _UserID
            End Get
            Set(ByVal Value As Integer)
                _UserID = Value
            End Set
        End Property

        Public Property ModuleID() As Integer
            Get
                Return _ModuleID
            End Get
            Set(ByVal Value As Integer)
                _ModuleID = Value
            End Set
        End Property
        Public Property PageID() As Integer
            Get
                Return _PageID
            End Get
            Set(ByVal Value As Integer)
                _PageID = Value
            End Set
        End Property
        Public Property TerittoryID() As Long
            Get
                Return _TerittoryID
            End Get
            Set(ByVal Value As Long)
                _TerittoryID = Value
            End Set
        End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property



        Public Property numAuthoritativeMode() As Int32
            Get
                Return _numAuthoritativeMode
            End Get
            Set(ByVal Value As Int32)
                _numAuthoritativeMode = Value
            End Set
        End Property

        Public Property numAuthoritativePurchase() As Integer
            Get
                Return _numAuthoritativePurchase
            End Get
            Set(ByVal Value As Integer)
                _numAuthoritativePurchase = Value
            End Set
        End Property

        Public Property numAuthoritativeSales() As Integer
            Get
                Return _numAuthoritativeSales
            End Get
            Set(ByVal Value As Integer)
                _numAuthoritativeSales = Value
            End Set
        End Property


        Public Property ISPresent() As Boolean
            Get
                Return _ISPresent
            End Get
            Set(ByVal value As Boolean)
                _ISPresent = value
            End Set
        End Property

        Public Property CountSalesBizDocs() As Integer
            Get
                Return _CountSalesBizDocs
            End Get
            Set(ByVal Value As Integer)
                _CountSalesBizDocs = Value
            End Set
        End Property


        Public Property CountPurchaseBizDocs() As Integer
            Get
                Return _CountPurchaseBizDocs
            End Get
            Set(ByVal Value As Integer)
                _CountPurchaseBizDocs = Value
            End Set
        End Property

        Private _UnitId As Long

        Public Property UnitId() As Long
            Get
                Return _UnitId
            End Get
            Set(ByVal Value As Long)
                _UnitId = Value
            End Set
        End Property

        Private _UnitName As String

        Public Property UnitName() As String
            Get
                Return _UnitName
            End Get
            Set(ByVal Value As String)
                _UnitName = Value
            End Set
        End Property

        Private _UnitType As Short

        Public Property UnitType() As Short
            Get
                Return _UnitType
            End Get
            Set(ByVal Value As Short)
                _UnitType = Value
            End Set
        End Property

        Private _UnitEnable As Boolean

        Public Property UnitEnable() As Boolean
            Get
                Return _UnitEnable
            End Get
            Set(ByVal Value As Boolean)
                _UnitEnable = Value
            End Set
        End Property

        Private _UnitId1 As Long

        Public Property UnitId1() As Long
            Get
                Return _UnitId1
            End Get
            Set(ByVal Value As Long)
                _UnitId1 = Value
            End Set
        End Property

        Private _Conv1 As Decimal

        Public Property Conv1() As Decimal
            Get
                Return _Conv1
            End Get
            Set(ByVal Value As Decimal)
                _Conv1 = Value
            End Set
        End Property

        Private _Conv2 As Decimal

        Public Property Conv2() As Decimal
            Get
                Return _Conv2
            End Get
            Set(ByVal Value As Decimal)
                _Conv2 = Value
            End Set
        End Property

        Private _strUnitConversion As String

        Public Property strUnitConversion() As String
            Get
                Return _strUnitConversion
            End Get
            Set(ByVal Value As String)
                _strUnitConversion = Value
            End Set
        End Property

        Private _ItemCode As Long

        Public Property ItemCode() As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal Value As Long)
                _ItemCode = Value
            End Set
        End Property

        Private _UOMAll As Boolean = False

        Public Property UOMAll() As Boolean
            Get
                Return _UOMAll
            End Get
            Set(ByVal Value As Boolean)
                _UOMAll = Value
            End Set
        End Property
        Private _BizDocType As Short

        Public Property BizDocType() As Short
            Get
                Return _BizDocType
            End Get
            Set(ByVal Value As Short)
                _BizDocType = Value
            End Set
        End Property

        Private _UpdateRecordID As Long
        Public Property UpdateRecordID() As Long
            Get
                Return _UpdateRecordID
            End Get
            Set(ByVal value As Long)
                _UpdateRecordID = value
            End Set
        End Property

        Private _UpdateValueID As Long
        Public Property UpdateValueID() As Long
            Get
                Return _UpdateValueID
            End Get
            Set(ByVal value As Long)
                _UpdateValueID = value
            End Set
        End Property

        Private _str As String
        Public Property Str() As String
            Get
                Return _str
            End Get
            Set(ByVal value As String)
                _str = value
            End Set
        End Property

        Dim _FormFieldId As Long
        Public Property FormFieldId() As Long
            Get
                Return _FormFieldId
            End Get
            Set(ByVal Value As Long)
                _FormFieldId = Value
            End Set
        End Property

        Dim _CustomField As Boolean
        Public Property CustomField() As Boolean
            Get
                Return _CustomField
            End Get
            Set(ByVal Value As Boolean)
                _CustomField = Value
            End Set
        End Property

        Dim _InlineEditValue As String
        Public Property InlineEditValue() As String
            Get
                Return _InlineEditValue
            End Get
            Set(ByVal Value As String)
                _InlineEditValue = Value
            End Set
        End Property

        Private _numWOId As Long

        Public Property numWOId() As Long
            Get
                Return _numWOId
            End Get
            Set(ByVal Value As Long)
                _numWOId = Value
            End Set
        End Property

        Private _RecordId As Long

        Public Property RecordId() As Long
            Get
                Return _RecordId
            End Get
            Set(ByVal Value As Long)
                _RecordId = Value
            End Set
        End Property

        Private _numWODetailId As Long

        Public Property numWODetailId() As Long
            Get
                Return _numWODetailId
            End Get
            Set(ByVal Value As Long)
                _numWODetailId = Value
            End Set
        End Property

        Private _vcNumOfEmp As String
        Public Property vcNumOfEmp() As String
            Get
                Return _vcNumOfEmp
            End Get
            Set(ByVal Value As String)
                _vcNumOfEmp = Value
            End Set
        End Property

        Private _CurrencyID As String
        Public Property CurrencyID() As String
            Get
                Return _CurrencyID
            End Get
            Set(ByVal Value As String)
                _CurrencyID = Value
            End Set
        End Property

        Private _OnCreditHold As Boolean
        Public Property OnCreditHold() As Boolean
            Get
                Return _OnCreditHold
            End Get
            Set(ByVal Value As Boolean)
                _OnCreditHold = Value
            End Set
        End Property

        Private _intBillingDays As Long
        Public Property intBillingDays() As Long
            Get
                Return _intBillingDays
            End Get
            Set(ByVal value As Long)
                _intBillingDays = value
            End Set
        End Property

        Private _lngDefaultExpenseAccountID As Long
        Public Property DefaultExpenseAccountID() As Long
            Get
                Return _lngDefaultExpenseAccountID
            End Get
            Set(ByVal value As Long)
                _lngDefaultExpenseAccountID = value
            End Set
        End Property


        Private _lngAccountClassID As Long
        Public Property AccountClassID() As Long
            Get
                Return _lngAccountClassID
            End Get
            Set(ByVal value As Long)
                _lngAccountClassID = value
            End Set
        End Property

        Private _bitEnableItemLevelUOM As Boolean
        Public Property EnableItemLevelUOM() As Boolean
            Get
                Return _bitEnableItemLevelUOM
            End Get
            Set(ByVal value As Boolean)
                _bitEnableItemLevelUOM = value
            End Set
        End Property

        Private _bitEnforceMinOrderAmount As Boolean
        Public Property EnforceMinOrderAmount() As Boolean
            Get
                Return _bitEnforceMinOrderAmount
            End Get
            Set(ByVal value As Boolean)
                _bitEnforceMinOrderAmount = value
            End Set
        End Property

        Private _fltMinOrderAmount As Decimal
        Public Property MinOrderAmount() As Decimal
            Get
                Return _fltMinOrderAmount
            End Get
            Set(ByVal value As Decimal)
                _fltMinOrderAmount = value
            End Set
        End Property

        Public Property RecordOwner As Long
        Public Property tintOppOrder As Short

        Public Property FollowupCampaign() As String
            Get
                Return _FollowUpCampaign
            End Get
            Set(ByVal Value As String)
                _FollowUpCampaign = Value
            End Set
        End Property

        Public Property LastFollowup() As String
            Get
                Return _LastFollowup
            End Get
            Set(ByVal Value As String)
                _LastFollowup = Value
            End Set
        End Property

        Public Property NextFollowup() As String
            Get
                Return _NextFollowup
            End Get
            Set(ByVal Value As String)
                _NextFollowup = Value
            End Set
        End Property

        Private _bitPartialQtyOrderStatus As Boolean
        Public Property BitPartialQtyOrderStatus() As Boolean
            Get
                Return _bitPartialQtyOrderStatus
            End Get
            Set(ByVal value As Boolean)
                _bitPartialQtyOrderStatus = value
            End Set
        End Property

        'Private _PartialQtyOrderStatusID As Long
        'Public Property PartialQtyOrderStatusID() As Long
        '    Get
        '        Return _PartialQtyOrderStatusID
        '    End Get
        '    Set(ByVal Value As Long)
        '        _PartialQtyOrderStatusID = Value
        '    End Set
        'End Property

        Private _bitAllQtyOrderStatus As Boolean
        Public Property BitAllQtyOrderStatus() As Boolean
            Get
                Return _bitAllQtyOrderStatus
            End Get
            Set(ByVal value As Boolean)
                _bitAllQtyOrderStatus = value
            End Set
        End Property

        Private _FullReceiveOrderStatus As Long
        Public Property FullReceiveOrderStatus() As Long
            Get
                Return _FullReceiveOrderStatus
            End Get
            Set(ByVal Value As Long)
                _FullReceiveOrderStatus = Value
            End Set
        End Property

        Private _PartialReceiveOrderStatus As Long
        Public Property PartialReceiveOrderStatus() As Long
            Get
                Return _PartialReceiveOrderStatus
            End Get
            Set(ByVal Value As Long)
                _PartialReceiveOrderStatus = Value
            End Set
        End Property

        'Private _bitAddBizDoc As Boolean
        'Public Property BitAddBizDoc() As Boolean
        '    Get
        '        Return _bitAddBizDoc
        '    End Get
        '    Set(ByVal value As Boolean)
        '        _bitAddBizDoc = value
        '    End Set
        'End Property

        'Private _BizDocID As Long
        'Public Property BizDocID() As Long
        '    Get
        '        Return _BizDocID
        '    End Get
        '    Set(ByVal Value As Long)
        '        _BizDocID = Value
        '    End Set
        'End Property

        Private _bitClosePO As Boolean
        Public Property BitClosePO() As Boolean
            Get
                Return _bitClosePO
            End Get
            Set(ByVal value As Boolean)
                _bitClosePO = value
            End Set
        End Property

        Private _IsTaskRelation As Boolean
        Public Property IsTaskRelation() As Boolean
            Get
                Return _IsTaskRelation
            End Get
            Set(ByVal value As Boolean)
                _IsTaskRelation = value
            End Set
        End Property

        Private _numListItemGroupId As Int64
        Public Property numListItemGroupId() As Int64
            Get
                Return _numListItemGroupId
            End Get
            Set(ByVal value As Int64)
                _numListItemGroupId = value
            End Set
        End Property

        Private _vcColorScheme As String
        Public Property vcColorScheme() As String
            Get
                Return _vcColorScheme
            End Get
            Set(ByVal value As String)
                _vcColorScheme = value
            End Set
        End Property
        Public Property TotalRecords As Long
        Public Property ClientTimeZoneOffset As Integer
        Public Property OppItemID As Long

        Public Function CheckUserAtLogin(ByVal strUserName As String, ByVal strPassword As String, ByVal strLinkedinId As String) As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@v_vcUserName", strUserName, NpgsqlTypes.NpgsqlDbType.Varchar, 100))
                    .Add(SqlDAL.Add_Parameter("@v_vcPassword", strPassword, NpgsqlTypes.NpgsqlDbType.Varchar, 100))
                    .Add(SqlDAL.Add_Parameter("@v_vcLinkedinId", strLinkedinId, NpgsqlTypes.NpgsqlDbType.Varchar, 300))
                    .Add(SqlDAL.Add_Parameter("@v_numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(GetConnection.GetConnectionString, "USP_CheckUserAtLogin", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Function PaymentGateways() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(GetConnection.GetConnectionString, "USP_GetPaymentGateways", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function PageLevelUserRights() As Array
            ''This array will hold the rights for the user.
            Dim aryPageRights(4) As Integer
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = Me.UserID

                arParms(1) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = Me.ModuleID

                arParms(2) = New Npgsql.NpgsqlParameter("@numPageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = Me.PageID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "usp_GetPageLevelUserRights", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        aryPageRights(RIGHTSTYPE.VIEW) = CInt(dr("intViewAllowed"))
                        aryPageRights(RIGHTSTYPE.ADD) = CInt(dr("intAddAllowed"))
                        aryPageRights(RIGHTSTYPE.DELETE) = CInt(dr("intDeleteAllowed"))
                        aryPageRights(RIGHTSTYPE.EXPORT) = CInt(dr("intExportAllowed"))
                        aryPageRights(RIGHTSTYPE.UPDATE) = CInt(dr("intUpdateAllowed"))
                    Next
                Else
                    aryPageRights(RIGHTSTYPE.VIEW) = 3
                    aryPageRights(RIGHTSTYPE.ADD) = 3
                    aryPageRights(RIGHTSTYPE.DELETE) = 3
                    aryPageRights(RIGHTSTYPE.EXPORT) = 3
                    aryPageRights(RIGHTSTYPE.UPDATE) = 3
                End If

                Return aryPageRights
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAuthDetailsForDomain() As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(GetConnection.GetConnectionString, "USP_GetAuthDetailsForDomain", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ExtPageLevelUserRights() As Array
            ''This array will hold the rights for the user.
            Dim aryPageRights(4) As Integer
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                '@UserID   Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = Me.ContactID

                '@PageName   Parameter 
                arParms(1) = New Npgsql.NpgsqlParameter("@vcFileName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = Me.PageName

                '@ModuleID   Parameter 
                arParms(2) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = Me.ModuleID

                '@PageID   Parameter 
                arParms(3) = New Npgsql.NpgsqlParameter("@numPageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = Me.PageID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_PageLevelExtranetUserRights", arParms)


                aryPageRights(RIGHTSTYPE.VIEW) = 0
                aryPageRights(RIGHTSTYPE.ADD) = 0
                aryPageRights(RIGHTSTYPE.DELETE) = 0
                aryPageRights(RIGHTSTYPE.EXPORT) = 0
                aryPageRights(RIGHTSTYPE.UPDATE) = 0

                If ds.Tables(0).Rows.Count = 1 Then
                    aryPageRights(RIGHTSTYPE.VIEW) = CInt(ds.Tables(0).Rows(0).Item("intViewAllowed"))
                    aryPageRights(RIGHTSTYPE.ADD) = CInt(ds.Tables(0).Rows(0).Item("intAddAllowed"))
                    aryPageRights(RIGHTSTYPE.DELETE) = CInt(ds.Tables(0).Rows(0).Item("intDeleteAllowed"))
                    aryPageRights(RIGHTSTYPE.EXPORT) = CInt(ds.Tables(0).Rows(0).Item("intExportAllowed"))
                    aryPageRights(RIGHTSTYPE.UPDATE) = CInt(ds.Tables(0).Rows(0).Item("intUpdateAllowed"))
                End If
                ds = Nothing
                Return aryPageRights
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function PurchaseItems(ByVal lngDomainId As Long, ByVal lngDivisionID As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = lngDomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = lngDivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_PurchaseItems", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function NewOppItems(ByVal lngDomainId As Long, ByVal lngDivisionID As Long, ByVal OppType As Short, ByVal ItemName As String, Optional ByVal SearchOrderCustomerHistory As Short = 0) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = OppType

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = lngDomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = lngDivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(3).Value = ItemName

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintSearchOrderCustomerHistory", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = SearchOrderCustomerHistory

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetNewOppItems", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function NewOppItemsForReturn(ByVal lngDomainId As Long, ByVal lngDivisionID As Long, ByVal OppType As Short, ByVal ItemName As String, Optional ByVal SearchOrderCustomerHistory As Short = 0) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = OppType

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = lngDomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = lngDivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(3).Value = ItemName

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintSearchOrderCustomerHistory", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = SearchOrderCustomerHistory

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetNewOppItemsForReturn", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDivision(ByVal lngDomainID As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDivision", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Sub sb_FillComboPurchaseItem(ByRef objCombo As DropDownList, ByVal lngDivID As Long, ByVal lngDomainID As Long)
            Try
                objCombo.DataSource = PurchaseItems(lngDomainID, lngDivID)
                objCombo.DataTextField = "vcItemName"
                objCombo.DataValueField = "numItemCode"
                objCombo.DataBind()
                objCombo.Items.Insert(0, "--Select One--")
                objCombo.Items.FindByText("--Select One--").Value = "0"

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Sub sb_FillComboFromDBwithSel(ByRef objCombo As DropDownList, ByVal lngListID As Long, ByVal lngDomainID As Long)
            Try

                objCombo.DataSource = GetMasterListItems(lngListID, lngDomainID)
                objCombo.DataTextField = "vcData"
                objCombo.DataValueField = "numListItemID"
                objCombo.DataBind()
                objCombo.Items.Insert(0, "--Select One--")
                objCombo.Items.FindByText("--Select One--").Value = "0"

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Added by Sachin Sadhu||Date:14thJune2014
        'Purpose:Filter Status Bizdoc Type wise
        Public Sub sb_FillComboFromDBwithSel(ByRef objCombo As DropDownList, ByVal lngListID As Long, ByVal lngDomainID As Long, ByVal strListType As String)
            Try
                Dim dtTable As DataTable = GetMasterListItems(lngListID, lngDomainID)
                Dim dvSource As DataView = dtTable.DefaultView
                If lngListID = 176 And strListType = "2" Then
                    dvSource.RowFilter = "numListType IN (" + CCommon.ToString(strListType) + ")"
                Else
                    dvSource.RowFilter = "numListType IN (0," + CCommon.ToString(strListType) + ")"
                End If
                'objCombo.DataSource = GetMasterListItems(lngListID, lngDomainID)
                objCombo.DataSource = dvSource
                objCombo.DataTextField = "vcData"
                objCombo.DataValueField = "numListItemID"
                objCombo.DataBind()
                objCombo.Items.Insert(0, "--Select One--")
                objCombo.Items.FindByText("--Select One--").Value = "0"

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'End of Code

        'Added by sandeep to support RadComboBox
        Public Sub sb_FillComboFromDBwithSel(ByRef objCombo As RadComboBox, ByVal lngListID As Long, ByVal lngDomainID As Long)
            Try
                objCombo.DataSource = GetMasterListItems(lngListID, lngDomainID)
                objCombo.DataTextField = "vcData"
                objCombo.DataValueField = "numListItemID"
                objCombo.DataBind()
                objCombo.Items.Insert(0, New RadComboBoxItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        ''Public Sub sb_FillComboFromDBwithSelforDepartement(ByRef objCombo As DropDownList, ByVal lngListID As Long, ByVal lngDomainID As Long)
        ''    Try
        ''        objCombo.DataSource = GetMasterListItems(lngListID, lngDomainID)
        ''        objCombo.DataTextField = "vcData"
        ''        objCombo.DataValueField = "numListItemID"
        ''        objCombo.DataBind()
        ''        objCombo.Items.Insert(0, "--All One--")
        ''        objCombo.Items.FindByText("--All One--").Value = "0"

        ''    Catch ex As Exception
        ''        Throw ex
        ''    End Try
        ''End Sub
        Public Sub sb_FillAttibuesFromDB(ByRef objCombo As DropDownList, ByVal lngListID As Long, ByVal lngItemCode As Long, ByVal strAttr As String)
            Try
                If lngListID > 0 Then
                    objCombo.DataSource = GetAttibutesBasedonSelection(lngListID, lngItemCode, strAttr)
                    objCombo.DataTextField = "vcData"
                    objCombo.DataValueField = "numListItemID"
                    objCombo.DataBind()
                End If
                objCombo.Items.Insert(0, "--Select One--")
                objCombo.Items.FindByText("--Select One--").Value = "0"

            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Function TaxPercentage(ByVal lngDivisionID As Long, ByVal lngBillCountry As Long, ByVal lngBillState As Long, ByVal lngDomainID As Long, ByVal lngTaxItemID As Long, ByVal vcCity As String, ByVal vcZipPostal As String, ByVal shrtBaseTaxOnArea As Short, Optional ByVal shrtBaseTaxCalcOn As Short = 2) As String 'Tax calc based on Shipping address -2,Billing address-1
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@DivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBillCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngBillCountry

                arParms(2) = New Npgsql.NpgsqlParameter("@numBillState", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = lngBillState

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = lngDomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@numTaxItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = lngTaxItemID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintBaseTaxCalcOn", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = shrtBaseTaxCalcOn

                arParms(6) = New Npgsql.NpgsqlParameter("@tintBaseTaxOnArea", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = shrtBaseTaxOnArea

                arParms(7) = New Npgsql.NpgsqlParameter("@vcCity", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = vcCity

                arParms(8) = New Npgsql.NpgsqlParameter("@vcZipPostal", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(8).Value = vcZipPostal

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_GetTaxPercentage", arParms))

            Catch ex As Exception

                Throw ex
            End Try
        End Function


        Public Sub sb_FillAttibuesForEcommFromDB(ByRef objCombo As DropDownList, ByVal lngListID As Long, ByVal lngItemCode As Long, ByVal strAttr As String, ByVal WareHouseID As String)
            Try
                If lngListID > 0 Then
                    objCombo.DataSource = GetAttibutesBasedonSelectionEcomm(lngListID, lngItemCode, strAttr, WareHouseID)
                    objCombo.DataTextField = "vcData"
                    objCombo.DataValueField = "numListItemID"
                    objCombo.DataBind()
                End If
                objCombo.Items.Insert(0, "--Select One--")
                objCombo.Items.FindByText("--Select One--").Value = "0"

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub sb_FillAttibuesForEcommFromDB(ByRef objRadio As RadioButtonList, ByVal lngListID As Long, ByVal lngItemCode As Long, ByVal strAttr As String, ByVal WareHouseID As String)
            Try
                If lngListID > 0 Then
                    objRadio.DataSource = GetAttibutesBasedonSelectionEcomm(lngListID, lngItemCode, strAttr, WareHouseID)
                    objRadio.DataTextField = "vcData"
                    objRadio.DataValueField = "numListItemID"
                    objRadio.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Sub sb_FillComboFromDB(ByRef objCombo As DropDownList, ByVal lngListID As Long, ByVal lngDomainID As Long)
            Try
                objCombo.DataSource = GetMasterListItems(lngListID, lngDomainID)
                objCombo.DataTextField = "vcData"
                objCombo.DataValueField = "numListItemID"
                objCombo.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Sub sb_FillListFromDB(ByRef objListBox As ListBox, ByVal lngListID As Long, ByVal lngDomainID As Long)
            objListBox.DataSource = GetMasterListItems(lngListID, lngDomainID)
            objListBox.DataTextField = "vcData"
            objListBox.DataValueField = "numListItemID"
            objListBox.DataBind()
        End Sub

        Public Sub sb_FillCheckBoxListFromDB(ByRef objListBox As CheckBoxList, ByVal lngListID As Long, ByVal lngDomainID As Long)
            objListBox.DataSource = GetMasterListItems(lngListID, lngDomainID)
            objListBox.DataTextField = "vcData"
            objListBox.DataValueField = "numListItemID"
            objListBox.DataBind()
        End Sub

        Public Sub sb_FillCheckBoxListFromDB(ByRef objListBox As CheckBoxList, ByVal lngListID As Long, ByVal lngDomainID As Long, ByVal strListType As String)
            Dim dtTable As DataTable = GetMasterListItems(lngListID, lngDomainID)
            'objListBox.DataSource = GetMasterListItems(lngListID, lngDomainID)
            Dim dvSource As DataView = dtTable.DefaultView
            dvSource.RowFilter = "numListType IN (0," + CCommon.ToString(strListType) + ")"
            'objCombo.DataSource = GetMasterListItems(lngListID, lngDomainID)
            objListBox.DataSource = dvSource
            objListBox.DataTextField = "vcData"
            objListBox.DataValueField = "numListItemID"
            objListBox.DataBind()
        End Sub
        Public Sub sb_FillItemFromDB(ByRef objCombo As DropDownList, ByVal lngDomainID As Long)
            objCombo.DataSource = GetItems(lngDomainID)
            objCombo.DataTextField = "vcItemName"
            objCombo.DataValueField = "numItemCode"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"
        End Sub


        Public Sub sb_FillGroupsFromDBSel(ByRef objCombo As DropDownList)
            objCombo.DataSource = GetGroups()
            objCombo.DataTextField = "vcGrpName"
            objCombo.DataValueField = "numGrpID"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"
        End Sub

        Public Sub sb_FillGroups(ByRef objCombo As DropDownList)
            objCombo.DataSource = GetGroups()
            objCombo.DataTextField = "vcGrpName"
            objCombo.DataValueField = "numGrpID"
            objCombo.DataBind()
        End Sub

        Public Sub sb_FillConEmpFromDBSel(ByRef objCombo As DropDownList, ByVal lngDomainID As Long, ByVal bitPartner As Boolean, ByVal lngContactID As Long)
            objCombo.DataSource = ConEmpList(lngDomainID, bitPartner, lngContactID)
            objCombo.DataTextField = "vcUserName"
            objCombo.DataValueField = "numContactID"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"
        End Sub

        'Added by sandeep to support RadComboBox
        Public Sub sb_FillConEmpFromDBSel(ByRef objCombo As RadComboBox, ByVal lngDomainID As Long, ByVal bitPartner As Boolean, ByVal lngContactID As Long)
            objCombo.DataSource = ConEmpList(lngDomainID, bitPartner, lngContactID)
            objCombo.DataTextField = "vcUserName"
            objCombo.DataValueField = "numContactID"
            objCombo.DataBind()
            Dim item As New RadComboBoxItem
            item.Text = "--Select One--"
            item.Value = 0
            objCombo.Items.Insert(0, item)
        End Sub

        Public Sub sb_FillConEmpFromDBUTeam(ByRef objCombo As DropDownList, ByVal lngDomainID As Long, ByVal lngUserCntID As Long)
            objCombo.DataSource = ConEmpList(lngDomainID, lngUserCntID)
            objCombo.DataTextField = "vcUserName"
            objCombo.DataValueField = "numContactID"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"
        End Sub

        'Added by sandeep to support RadComboBox
        Public Sub sb_FillConEmpFromDBUTeam(ByRef objCombo As RadComboBox, ByVal lngDomainID As Long, ByVal lngUserCntID As Long)
            objCombo.DataSource = ConEmpList(lngDomainID, lngUserCntID)
            objCombo.DataTextField = "vcUserName"
            objCombo.DataValueField = "numContactID"
            objCombo.DataBind()
            Dim item As New RadComboBoxItem
            item.Text = "--Select One--"
            item.Value = 0
            objCombo.Items.Insert(0, item)
        End Sub

        Public Sub sb_FillConEmpFromTerritories(ByRef objCombo As DropDownList, ByVal lngDomainID As Long, ByVal bitPartner As Boolean, ByVal lngContactID As Long, ByVal lngTerID As Long)
            objCombo.DataSource = ConEmpListFromTerritories(lngDomainID, bitPartner, lngContactID, lngTerID)
            objCombo.DataTextField = "vcUserName"
            objCombo.DataValueField = "numContactID"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"
        End Sub

        'Added by sandeep to support RadComboBox
        Public Sub sb_FillConEmpFromTerritories(ByRef objCombo As RadComboBox, ByVal lngDomainID As Long, ByVal bitPartner As Boolean, ByVal lngContactID As Long, ByVal lngTerID As Long)
            objCombo.DataSource = ConEmpListFromTerritories(lngDomainID, bitPartner, lngContactID, lngTerID)
            objCombo.DataTextField = "vcUserName"
            objCombo.DataValueField = "numContactID"
            objCombo.DataBind()
            Dim item As New RadComboBoxItem
            item.Text = "--Select One--"
            item.Value = 0
            objCombo.Items.Insert(0, item)
        End Sub


        Public Sub sb_FillConEmp(ByRef objListBox As ListBox, ByVal lngDomainID As Long, ByVal bitPartner As Boolean, ByVal lngContactID As Long)
            objListBox.DataSource = ConEmpList(lngDomainID, bitPartner, lngContactID)
            objListBox.DataTextField = "vcUserName"
            objListBox.DataValueField = "numContactID"
            objListBox.DataBind()
        End Sub


        Public Sub sb_FillManagerFromDBSel(ByRef objCombo As DropDownList, ByVal lngDomainID As Long, ByVal lngContactID As Long, ByVal lngDivID As Long)
            objCombo.DataSource = GetManagers(lngDomainID, lngContactID, lngDivID)
            objCombo.DataTextField = "Name"
            objCombo.DataValueField = "numContactID"
            objCombo.DataBind()
            objCombo.Items.Insert(0, "--Select One--")
            objCombo.Items.FindByText("--Select One--").Value = "0"
        End Sub


        Public Function ConEmpList(ByVal lngDomainID As Long, ByVal biPartner As Boolean, ByVal lngContactID As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = biPartner

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = lngContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ConEmpList", arParms).Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function ConEmpListByTeam(ByVal lngDomainID As Long, ByVal lngTeamId As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numTeamId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngTeamId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ConEmpListByTeamId", arParms).Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function ConEmpListByGroupId(ByVal lngDomainID As Long, ByVal lngGroupId As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngGroupId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ConEmpListByGroupId", arParms).Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function ConOverrideEmpList(ByVal lngDomainID As Long, ByVal numAssignedContact As Long, ByVal numContactId As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAssignedContact", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numAssignedContact

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numContactId

                Return SqlDAL.ExecuteDataset(connString, "USP_AddOverrideAssigntoEmp", arParms).Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function ConEmpList(ByVal lngDomainID As Long, ByVal lngUserCntID As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngUserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ConEmpListBasedonTeam", arParms).Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function ConEmpListFromTerritories(ByVal lngDomainID As Long, ByVal biPartner As Boolean, ByVal lngContactID As Long, ByVal lngTerID As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = biPartner

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = lngContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = lngTerID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ConEmpListFromTerr", arParms).Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function GetManagers(ByVal lngDomainID As Long, ByVal lngConID As Long, ByVal lngDivID As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDivID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngConID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = lngDomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "GetManagers", arParms).Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function


        Public Sub sb_FillDivisionListBoxFromDB(ByRef objList As ListBox, ByVal lngDomainID As Long)
            objList.DataSource = GetDivision(lngDomainID)
            objList.DataTextField = "vcData"
            objList.DataValueField = "numListItemID"
            objList.DataBind()
        End Sub

        Public Function GetItems(ByVal lngDomainID As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItems", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try

        End Function



        Public Sub LoadListMaster(ByRef objDropDown As DropDownList, ByVal lngDomainID As Long, Optional ByVal ModuleID As Long = 0, Optional ByVal bitIsCustom As Boolean = False)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ModuleID

                arParms(2) = New Npgsql.NpgsqlParameter("@bitCustom", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = bitIsCustom

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                objDropDown.DataSource = SqlDAL.ExecuteDataset(connString, "USP_GetListMaster", arParms).Tables(0)
                objDropDown.DataTextField = "vcListName"
                objDropDown.DataValueField = "numListID"
                objDropDown.DataBind()
                'objDropDown.Items.Insert(0, "--Select One--")
                'objDropDown.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Function GetMasterListItems(ByVal lngListID As Long, ByVal lngDomainID As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngListID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngDomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetMasterListItems", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWebReferringInfo() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetWebReferringInfo", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageWebReferringInfo(ByVal SearchText As String) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@searchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = SearchText

                arParms(2) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ListItemID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageWebReferringInfo", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by :Sachin Sadhu||Date:19thJune2014
        'Purpose:Sales/Purchase Type wise OrderStatus
        Public Function GetMasterListItems(ByVal lngListID As Long, ByVal lngDomainID As Long, ByVal lngListType As Long, ByVal tintOppOrOrder As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngListID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngDomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numListType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = lngListType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintOppOrOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = tintOppOrOrder

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetMasterListItemsUsingListType", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'end of Code

        Public Function CheckMasterlistItem(ByVal lngDomainID As Long, ByVal lngListID As Long, ByVal lngListItemID As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngDomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@ListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngListID

                arParms(2) = New Npgsql.NpgsqlParameter("@ListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = lngListItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_CheckMasterlistItem", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAttibutesBasedonSelection(ByVal lngListID As Long, ByVal lngItemCode As Long, ByVal strAttr As String) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngListID

                arParms(2) = New Npgsql.NpgsqlParameter("@strAtrr", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = strAttr

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_LoadAttributes", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAttibutesBasedonSelectionEcomm(ByVal lngListID As Long, ByVal lngItemCode As Long, ByVal strAttr As String, ByVal WareHouseID As String) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngListID

                arParms(2) = New Npgsql.NpgsqlParameter("@strAtrr", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = strAttr

                arParms(3) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt, 2)
                arParms(3).Value = CCommon.ToLong(WareHouseID)

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_LoadAttributesEcommerce", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWarehouseOnAttrSel(ByVal lngItemCode As Long, ByVal strAttr As String) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@strAtrr", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = strAttr

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_LoadWarehouseAttributes", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWarehouseAttrBasedItem(ByVal lngItemCode As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_LoadWarehouseBasedOnItem", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWarehousesForSelectedItem(ByVal lngItemCode As Long, ByVal numWareHouseID As Long, ByVal qty As Double, ByVal orderSource As Long, ByVal sourceType As Short, ByVal shipToCountry As Long, ByVal shipToState As Long, ByVal selectedKitChildItems As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numItemCode", lngItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWareHouseID", numWareHouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQty", qty, NpgsqlTypes.NpgsqlDbType.Double))
                    .Add(SqlDAL.Add_Parameter("@numOrderSource", orderSource, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintSourceType", sourceType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numShipToCountry", shipToCountry, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numShipToState", shipToState, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSelectedKitChildItems", selectedKitChildItems, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetWarehousesForSelectedItem", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function LoadWarehouseBasedOnItemsWithAttr(ByVal lngItemCode As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_LoadWarehouseBasedOnItemsWithAttr", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetWarehouseOnAttrSelEcomm(ByVal lngItemCode As Long, ByVal strAttr As String, ByVal WareHouseID As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@strAtrr", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = strAttr

                arParms(2) = New Npgsql.NpgsqlParameter("@numWareHouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = WareHouseID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_LoadWarehouseAttributesEcomm", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetMasterListItemsForBizDocs(ByVal lngListID As Long, ByVal lngDomainID As Long, ByVal lngOppId As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = lngListID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngDomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = lngOppId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetMasterListItemsForBizDocs", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetListDetailAttributesUsingDomainID() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim dt As DataTable

                dt = SqlDAL.ExecuteDataset(connString, "USP_GetListDetailAttributesUsingDomainID", arParms).Tables(0)

                Return dt
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetMasterListItemsWithRights() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ListID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetMasterListItemsUsingDomainID", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMasterListItemsWithOutDefaultItemsRights() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ListID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetMasterListItemsWithoutConstFlag", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdatePOFulfillmentSettings(ByVal bizDocID As Long) As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numPartialReceiveOrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = PartialReceiveOrderStatus

                arParms(2) = New Npgsql.NpgsqlParameter("@numFullReceiveOrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = FullReceiveOrderStatus

                arParms(3) = New Npgsql.NpgsqlParameter("@bitPartialQtyOrderStatus", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = BitPartialQtyOrderStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@bitAllQtyOrderStatus", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = BitAllQtyOrderStatus

                'arParms(4) = New Npgsql.NpgsqlParameter("@bitBizDoc", NpgsqlTypes.NpgsqlDbType.Bit)
                'arParms(4).Value = BitAddBizDoc

                'arParms(5) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                'arParms(5).Value = BizDocID

                arParms(5) = New Npgsql.NpgsqlParameter("@bitClosePO", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = BitClosePO

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = bizDocID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdatePOFulfillmentSettings", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPOFulfillmentSettings() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim dt As DataTable

                dt = SqlDAL.ExecuteDataset(connString, "USP_GetPOFulfillmentSettings", arParms).Tables(0)
                Return dt
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        'Added By :Sachin Sadhu||Date:20thJune2014
        'Purpose  :Get OrderType wise Order Status
        Public Function GetMasterListItemsWithRights(ByVal numListType As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ListID

                arParms(2) = New Npgsql.NpgsqlParameter("@numListType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numListType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetMasterListitemsByListType", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'end of code


        Public Function ManageItemList() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _ListItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcData", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _ListItemName

                arParms(3) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ListID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                'Added By:Sachin Sadhu||Date:12thJune2014
                'Purpose:Added One Field numListType to save  bizdoc type wise BizDoc status   
                arParms(5) = New Npgsql.NpgsqlParameter("@numListType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _lngListType
                'End of code

                arParms(6) = New Npgsql.NpgsqlParameter("@tintOppOrOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = tintOppOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@bitEnforceMinOrderAmount", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _bitEnforceMinOrderAmount

                arParms(8) = New Npgsql.NpgsqlParameter("@fltMinOrderAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(8).Value = _fltMinOrderAmount

                arParms(9) = New Npgsql.NpgsqlParameter("@numListItemGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _numListItemGroupId

                arParms(10) = New Npgsql.NpgsqlParameter("@vcColorScheme", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(10).Value = _vcColorScheme

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageItemList", objParam, True)
                _ListItemID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SortUpdateItemList() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@xmlStr", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _xmlStr

                arParms(1) = New Npgsql.NpgsqlParameter("@ListId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ListID

                arParms(2) = New Npgsql.NpgsqlParameter("@DomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_SortItemList", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Sub DeleteItemList()
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ListItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ListID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteItemList", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function CheckOpeningBalance() As Decimal
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ListItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "USP_GetOpeningBalanceForMasterListDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPageNavigationDTLs() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ModuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _lngGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetPageNavigation", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetPageNavigationRelDTLs() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ModuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _lngGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetPageNavigationRel", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetGroups() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(0).Value = Nothing
                arparms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetGroups", arparms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function UpdateLicense(ByVal strLicNo As String, ByVal intLicNo As Integer) As Boolean
            Dim connString As String = GetConnection.GetConnectionString
            Dim cnn As New Npgsql.NpgsqlConnection(connString)
            Dim cmd As New Npgsql.NpgsqlCommand


            cmd.Connection = cnn
            cnn.Open()
            Try
                cmd.CommandText = "update [user] set vcUserData='" & strLicNo & "'"
                cmd.ExecuteNonQuery()
                Return True
            Catch ex As Exception
                Return False
            Finally
                cmd.Dispose()
                cnn.Close()

            End Try
        End Function


        Public Function GetManagerEmail() As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _UserID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CCommon.ToString(SqlDAL.ExecuteScalar(connString, "USP_GetManagerEmail", arParms))


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAccountHoldersEmail() As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _UserID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CCommon.ToString(SqlDAL.ExecuteScalar(connString, "USP_GetAccountUserEmail", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetContactsEmail() As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CCommon.ToString(SqlDAL.ExecuteScalar(connString, "USP_GetContactsEmail", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateSignature() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _UserID

                arParms(1) = New Npgsql.NpgsqlParameter("@txtSignature", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(1).Value = _Signature

                SqlDAL.ExecuteNonQuery(connString, "usp_UpdateSignature", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ListCustomer() As DataSet
            Try
                Dim Connection As New Npgsql.NpgsqlConnection(GetConnection.GetConnectionString)
                Dim arParams() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParams(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(0).Value = DomainID

                arParams(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(1).Value = UserCntID

                arParams(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParams(2).Value = Nothing
                arParams(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(Connection, "usp_GetAutoComCompany", arParams)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetCompanySpecificValues1() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(22) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCommID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CommID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppID


                arParms(2) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ProID


                arParms(3) = New Npgsql.NpgsqlParameter("@numCasesID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CaseID


                arParms(4) = New Npgsql.NpgsqlParameter("@numDivID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _DivisionID


                arParms(5) = New Npgsql.NpgsqlParameter("@numCompID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _CompID


                arParms(6) = New Npgsql.NpgsqlParameter("@numContID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _ContactID


                arParms(7) = New Npgsql.NpgsqlParameter("@tintCRMType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Direction = ParameterDirection.InputOutput
                arParms(7).Value = _CRMType


                arParms(8) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _TerittoryID

                arParms(9) = New Npgsql.NpgsqlParameter("@vcCompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(9).Direction = ParameterDirection.InputOutput
                arParms(9).Value = _CompanyName

                arParms(10) = New Npgsql.NpgsqlParameter("@vcContactName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(10).Direction = ParameterDirection.InputOutput
                arParms(10).Value = _ContactName

                arParms(11) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(11).Direction = ParameterDirection.InputOutput
                arParms(11).Value = _EmailID


                arParms(12) = New Npgsql.NpgsqlParameter("@vcPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(12).Direction = ParameterDirection.InputOutput
                arParms(12).Value = _Phone

                arParms(13) = New Npgsql.NpgsqlParameter("@charModule", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(13).Value = _charModule

                arParms(14) = New Npgsql.NpgsqlParameter("@vcNoOfEmployeesId", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(14).Direction = ParameterDirection.InputOutput
                arParms(14).Value = _vcNumOfEmp

                arParms(15) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Direction = ParameterDirection.InputOutput
                arParms(15).Value = Convert.ToInt64(_CurrencyID)

                arParms(16) = New Npgsql.NpgsqlParameter("@bitOnCreditHold", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(16).Direction = ParameterDirection.InputOutput
                arParms(16).Value = _OnCreditHold

                arParms(17) = New Npgsql.NpgsqlParameter("@numBillingDays", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Direction = ParameterDirection.InputOutput
                arParms(17).Value = _intBillingDays

                arParms(18) = New Npgsql.NpgsqlParameter("@numDefaultExpenseAccountID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Direction = ParameterDirection.InputOutput
                arParms(18).Value = _lngDefaultExpenseAccountID

                arParms(19) = New Npgsql.NpgsqlParameter("@numAccountClassID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Direction = ParameterDirection.InputOutput
                arParms(19).Value = _lngAccountClassID

                arParms(20) = New Npgsql.NpgsqlParameter("@numRecordOwner", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(20).Direction = ParameterDirection.InputOutput
                arParms(20).Value = RecordOwner

                arParms(21) = New Npgsql.NpgsqlParameter("@vcPhoneExt", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(21).Direction = ParameterDirection.InputOutput
                arParms(21).Value = _PhoneExt

                arParms(22) = New Npgsql.NpgsqlParameter("@vcCustomerRelation", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(22).Direction = ParameterDirection.InputOutput
                arParms(22).Value = _CustomerRelation

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetCompSpecificValues", objParam, True)

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(9).Value) Then
                    _CompanyName = ""
                Else
                    _CompanyName = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(9).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(10).Value) Then
                    _ContactName = ""
                Else
                    _ContactName = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(10).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(11).Value) Then
                    _EmailID = ""
                Else
                    _EmailID = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(11).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(12).Value) Then
                    _Phone = ""
                Else
                    _Phone = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(12).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value) Then
                    _TerittoryID = 0
                Else
                    _TerittoryID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value) Then
                    _CRMType = 0
                Else
                    _CRMType = CShort(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value) Then
                    _ContactID = 0
                Else
                    _ContactID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value) Then
                    _CompID = 0
                Else
                    _CompID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value) Then
                    _DivisionID = 0
                Else
                    _DivisionID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(14).Value) Then
                    _vcNumOfEmp = "0"
                Else
                    _vcNumOfEmp = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(14).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(15).Value) Then
                    _CurrencyID = "0"
                Else
                    _CurrencyID = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(15).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(16).Value) Then
                    _OnCreditHold = False
                Else
                    _OnCreditHold = CBool(DirectCast(objParam, Npgsql.NpgsqlParameter())(16).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(17).Value) Then
                    _intBillingDays = 0
                Else
                    _intBillingDays = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(17).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(18).Value) Then
                    _lngDefaultExpenseAccountID = 0
                Else
                    _lngDefaultExpenseAccountID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(18).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(19).Value) Then
                    _lngAccountClassID = 0
                Else
                    _lngAccountClassID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(19).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(20).Value) Then
                    RecordOwner = 0
                Else
                    RecordOwner = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(20).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(21).Value) Then
                    _PhoneExt = ""
                Else
                    _PhoneExt = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(21).Value)
                End If

                If Convert.IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(22).Value) Then
                    _CustomerRelation = ""
                Else
                    _CustomerRelation = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(22).Value)
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetCount(ByVal lngDomainID As Long) As Long

            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim StrQuery As String
            Try
                StrQuery = "select count(*) from UserMaster where bitActivateFlag=1 and numDomainID=" & lngDomainID
                Return CLng(SqlDAL.ExecuteScalar(connString, CommandType.Text, StrQuery))
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function Encrypt(ByVal plainText As String) As String

            Dim passPhrase As String
            Dim saltValue As String
            Dim hashAlgorithm As String
            Dim passwordIterations As Integer
            Dim initVector As String
            Dim keySize As Integer

            passPhrase = "AnoopSivaGangaNithyaCarl768@@@@#"        ' can be any string
            saltValue = "s@AnoopCarl234358SivaGanga1231943"        ' can be any string
            hashAlgorithm = "SHA1"             ' can be "MD5"
            passwordIterations = 2                  ' can be any number
            initVector = "FNitsD4Ab@Anup@@" ' must be 16 bytes
            keySize = 256                ' can be 192 or 128

            ' Convert strings into byte arrays.
            ' Let us assume that strings only contain ASCII codes.
            ' If strings include Unicode characters, use Unicode, UTF7, or UTF8 
            ' encoding.
            Dim initVectorBytes As Byte()
            initVectorBytes = Encoding.ASCII.GetBytes(initVector)

            Dim saltValueBytes As Byte()
            saltValueBytes = Encoding.ASCII.GetBytes(saltValue)

            ' Convert our plaintext into a byte array.
            ' Let us assume that plaintext contains UTF8-encoded characters.
            Dim plainTextBytes As Byte()
            plainTextBytes = Encoding.UTF8.GetBytes(plainText)

            ' First, we must create a password, from which the key will be derived.
            ' This password will be generated from the specified passphrase and 
            ' salt value. The password will be created using the specified hash 
            ' algorithm. Password creation can be done in several iterations.
            Dim password As PasswordDeriveBytes
            password = New PasswordDeriveBytes(passPhrase,
                                               saltValueBytes,
                                               hashAlgorithm,
                                               passwordIterations)

            ' Use the password to generate pseudo-random bytes for the encryption
            ' key. Specify the size of the key in bytes (instead of bits).
            Dim keyBytes As Byte()
            keyBytes = password.GetBytes(keySize / 8)

            ' Create uninitialized Rijndael encryption object.
            Dim symmetricKey As RijndaelManaged
            symmetricKey = New RijndaelManaged()

            ' It is reasonable to set encryption mode to Cipher Block Chaining
            ' (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.CBC

            ' Generate encryptor from the existing key bytes and initialization 
            ' vector. Key size will be defined based on the number of the key 
            ' bytes.
            Dim encryptor As ICryptoTransform
            encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes)

            ' Define memory stream which will be used to hold encrypted data.
            Dim memoryStream As MemoryStream
            memoryStream = New MemoryStream()

            ' Define cryptographic stream (always use Write mode for encryption).
            Dim cryptoStream As CryptoStream
            cryptoStream = New CryptoStream(memoryStream,
                                            encryptor,
                                            CryptoStreamMode.Write)
            ' Start encrypting.
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length)

            ' Finish encrypting.
            cryptoStream.FlushFinalBlock()

            ' Convert our encrypted data from a memory stream into a byte array.
            Dim cipherTextBytes As Byte()
            cipherTextBytes = memoryStream.ToArray()

            ' Close both streams.
            memoryStream.Close()
            cryptoStream.Close()

            ' Convert encrypted data into a base64-encoded string.
            Dim cipherText As String
            cipherText = Convert.ToBase64String(cipherTextBytes)

            ' Return encrypted string.
            Encrypt = cipherText
        End Function

        ' <summary>
        ' Decrypts specified ciphertext using Rijndael symmetric key algorithm.
        ' </summary>
        ' <param name="cipherText">
        ' Base64-formatted ciphertext value.
        ' </param>
        ' <param name="passPhrase">
        ' Passphrase from which a pseudo-random password will be derived. The 
        ' derived password will be used to generate the encryption key. 
        ' Passphrase can be any string. In this example we assume that this 
        ' passphrase is an ASCII string.
        ' </param>
        ' <param name="saltValue">
        ' Salt value used along with passphrase to generate password. Salt can 
        ' be any string. In this example we assume that salt is an ASCII string.
        ' </param>
        ' <param name="hashAlgorithm">
        ' Hash algorithm used to generate password. Allowed values are: "MD5" and
        ' "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
        ' </param>
        ' <param name="passwordIterations">
        ' Number of iterations used to generate password. One or two iterations
        ' should be enough.
        ' </param>
        ' <param name="initVector">
        ' Initialization vector (or IV). This value is required to encrypt the 
        ' first block of plaintext data. For RijndaelManaged class IV must be 
        ' exactly 16 ASCII characters long.
        ' </param>
        ' <param name="keySize">
        ' Size of encryption key in bits. Allowed values are: 128, 192, and 256. 
        ' Longer keys are more secure than shorter keys.
        ' </param>
        ' <returns>
        ' Decrypted string value.
        ' </returns>
        ' <remarks>
        ' Most of the logic in this function is similar to the Encrypt 
        ' logic. In order for decryption to work, all parameters of this function
        ' - except cipherText value - must match the corresponding parameters of 
        ' the Encrypt function which was called to generate the 
        ' ciphertext.
        ' </remarks>
        Public Function Decrypt(ByVal cipherText As String) As String
            cipherText = cipherText.Replace(" ", "+")
            Dim passPhrase As String
            Dim saltValue As String
            Dim hashAlgorithm As String
            Dim passwordIterations As Integer
            Dim initVector As String
            Dim keySize As Integer

            passPhrase = "AnoopSivaGangaNithyaCarl768@@@@#"        ' can be any string
            saltValue = "s@AnoopCarl234358SivaGanga1231943"        ' can be any string
            hashAlgorithm = "SHA1"             ' can be "MD5"
            passwordIterations = 2                  ' can be any number
            initVector = "FNitsD4Ab@Anup@@" ' must be 16 bytes
            keySize = 256                ' can be 192 or 128

            ' Convert strings defining encryption key characteristics into byte
            ' arrays. Let us assume that strings only contain ASCII codes.
            ' If strings include Unicode characters, use Unicode, UTF7, or UTF8
            ' encoding.
            Dim initVectorBytes As Byte()
            initVectorBytes = Encoding.ASCII.GetBytes(initVector)

            Dim saltValueBytes As Byte()
            saltValueBytes = Encoding.ASCII.GetBytes(saltValue)

            ' Convert our ciphertext into a byte array.
            Dim cipherTextBytes As Byte()
            cipherTextBytes = Convert.FromBase64String(cipherText)

            ' First, we must create a password, from which the key will be 
            ' derived. This password will be generated from the specified 
            ' passphrase and salt value. The password will be created using
            ' the specified hash algorithm. Password creation can be done in
            ' several iterations.
            Dim password As PasswordDeriveBytes
            password = New PasswordDeriveBytes(passPhrase,
                                               saltValueBytes,
                                               hashAlgorithm,
                                               passwordIterations)

            ' Use the password to generate pseudo-random bytes for the encryption
            ' key. Specify the size of the key in bytes (instead of bits).
            Dim keyBytes As Byte()
            keyBytes = password.GetBytes(keySize / 8)

            ' Create uninitialized Rijndael encryption object.
            Dim symmetricKey As RijndaelManaged
            symmetricKey = New RijndaelManaged()

            ' It is reasonable to set encryption mode to Cipher Block Chaining
            ' (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.CBC

            ' Generate decryptor from the existing key bytes and initialization 
            ' vector. Key size will be defined based on the number of the key 
            ' bytes.
            Dim decryptor As ICryptoTransform
            decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)

            ' Define memory stream which will be used to hold encrypted data.
            Dim memoryStream As MemoryStream
            memoryStream = New MemoryStream(cipherTextBytes)

            ' Define memory stream which will be used to hold encrypted data.
            Dim cryptoStream As CryptoStream
            cryptoStream = New CryptoStream(memoryStream,
                                            decryptor,
                                            CryptoStreamMode.Read)

            ' Since at this point we don't know what the size of decrypted data
            ' will be, allocate the buffer long enough to hold ciphertext;
            ' plaintext is never longer than ciphertext.
            Dim plainTextBytes As Byte()
            ReDim plainTextBytes(cipherTextBytes.Length)

            ' Start decrypting.
            Dim decryptedByteCount As Integer
            decryptedByteCount = cryptoStream.Read(plainTextBytes,
                                                   0,
                                                   plainTextBytes.Length)

            ' Close both streams.
            memoryStream.Close()
            cryptoStream.Close()

            ' Convert decrypted data into a string. 
            ' Let us assume that the original plaintext string was UTF8-encoded.
            Dim plainText As String
            plainText = Encoding.UTF8.GetString(plainTextBytes,
                                                0,
                                                decryptedByteCount)

            ' Return decrypted string.
            Decrypt = plainText
        End Function
        Public Function GetBizDocs() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ListID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_MastItemDetailsForBizDocs", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAuthoritativeBizDocs() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetAuthoritativeBizDocs", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function SaveAuthoritativeBizDocs() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAuthoritativeMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _numAuthoritativeMode

                arParms(2) = New Npgsql.NpgsqlParameter("@numAuthoritativePurchase", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _numAuthoritativePurchase

                arParms(3) = New Npgsql.NpgsqlParameter("@numAuthoritativeSales", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _numAuthoritativeSales


                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_SaveAuthoritativeBizDocs", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAuthoritativeBizDocsDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetAuthoritativeBizDocsDetails", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetAttributesForWarehouseItem(ByVal RecID As Long, ByVal bitSerialized As Boolean) As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = RecID


                arParms(1) = New Npgsql.NpgsqlParameter("@bitSerialize", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = bitSerialized

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_GetOppWareHouseItemAttributes", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAttributesIdsForWarehouseItem(ByVal RecID As Long) As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = RecID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_GetOppWareHouseItemAttributesIds", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAuthorizativeBizDocsFromOpportunity() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAuthorizativeSalesId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numAuthoritativeSales

                arParms(1) = New Npgsql.NpgsqlParameter("@numAuthorizativePurchaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numAuthoritativePurchase

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numCountSalesBizDocs", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _CountSalesBizDocs

                arParms(4) = New Npgsql.NpgsqlParameter("@numCountPurchaseBizDocs", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _CountPurchaseBizDocs

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetAuthorizativeBizDocsFromOpportunity", objParam, True)
                _CountSalesBizDocs = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)
                _CountPurchaseBizDocs = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DecryptExch(ByVal cipherText As String, ByVal passPhrase As String, ByVal saltValue As String, ByVal hashAlgorithm As String, ByVal passwordIterations As Integer, ByVal initVector As String, ByVal keySize As Integer) As String

            ' Convert strings defining encryption key characteristics into byte
            ' arrays. Let us assume that strings only contain ASCII codes.
            ' If strings include Unicode characters, use Unicode, UTF7, or UTF8
            ' encoding.
            Dim initVectorBytes As Byte()
            initVectorBytes = Encoding.ASCII.GetBytes(initVector)

            Dim saltValueBytes As Byte()
            saltValueBytes = Encoding.ASCII.GetBytes(saltValue)

            ' Convert our ciphertext into a byte array.
            Dim cipherTextBytes As Byte()
            cipherTextBytes = Convert.FromBase64String(cipherText)

            ' First, we must create a password, from which the key will be 
            ' derived. This password will be generated from the specified 
            ' passphrase and salt value. The password will be created using
            ' the specified hash algorithm. Password creation can be done in
            ' several iterations.
            Dim password As PasswordDeriveBytes
            password = New PasswordDeriveBytes(passPhrase,
                                               saltValueBytes,
                                               hashAlgorithm,
                                               passwordIterations)

            ' Use the password to generate pseudo-random bytes for the encryption
            ' key. Specify the size of the key in bytes (instead of bits).
            Dim keyBytes As Byte()
            keyBytes = password.GetBytes(keySize / 8)

            ' Create uninitialized Rijndael encryption object.
            Dim symmetricKey As RijndaelManaged
            symmetricKey = New RijndaelManaged()

            ' It is reasonable to set encryption mode to Cipher Block Chaining
            ' (CBC). Use default options for other symmetric key parameters.
            symmetricKey.Mode = CipherMode.CBC

            ' Generate decryptor from the existing key bytes and initialization 
            ' vector. Key size will be defined based on the number of the key 
            ' bytes.
            Dim decryptor As ICryptoTransform
            decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)

            ' Define memory stream which will be used to hold encrypted data.
            Dim memoryStream As MemoryStream
            memoryStream = New MemoryStream(cipherTextBytes)

            ' Define memory stream which will be used to hold encrypted data.
            Dim cryptoStream As CryptoStream
            cryptoStream = New CryptoStream(memoryStream,
                                            decryptor,
                                            CryptoStreamMode.Read)

            ' Since at this point we don't know what the size of decrypted data
            ' will be, allocate the buffer long enough to hold ciphertext;
            ' plaintext is never longer than ciphertext.
            Dim plainTextBytes As Byte()
            ReDim plainTextBytes(cipherTextBytes.Length)

            ' Start decrypting.
            Dim decryptedByteCount As Integer
            decryptedByteCount = cryptoStream.Read(plainTextBytes,
                                                   0,
                                                   plainTextBytes.Length)

            ' Close both streams.
            memoryStream.Close()
            cryptoStream.Close()

            ' Convert decrypted data into a string. 
            ' Let us assume that the original plaintext string was UTF8-encoded.
            Dim plainText As String
            plainText = Encoding.UTF8.GetString(plainTextBytes,
                                                0,
                                                decryptedByteCount)

            ' Return decrypted string.
            Return plainText
        End Function
        Public Function GetCustomData(ByVal intOptions As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = intOptions

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCustomData", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetDomainDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "getDeatilsDomain", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDefaultValues() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@intoption", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _intOption

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_getDefaultValues", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function StripTags(ByVal HTML As String, Optional ByVal bit As Boolean = True) As String
            ' Removes tags from passed HTML
            HTML = Regex.Replace(HTML, "<style>[\s\S]+?</style>", "", RegexOptions.IgnoreCase)
            ' then run another pass over the html (twice), removing unwanted attributes
            'htmlText = Regex.Replace(htmlText, "&lt;([^&gt;]*)(?:class|lang|style|size|face|[ovwxp]:\w+)=(?:'[^']*'|""[^""]*""|[^&gt;]+)([^&gt;]*)&gt;", "&lt;$1$2&gt;", RegexOptions.IgnoreCase)
            'htmlText = Regex.Replace(htmlText, "&lt;([^&gt;]*)(?:class|lang|style|size|face|[ovwxp]:\w+)=(?:'[^']*'|""[^""]*""|[^&gt;]+)([^&gt;]*)&gt;", "&lt;$1$2&gt;", RegexOptions.IgnoreCase)
            HTML = Regex.Replace(HTML, "<[^>]*>", "", RegexOptions.IgnoreCase).Replace("P {", "").Replace("MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px", "")

            If bit = True Then
                If HTML.Length > 11 Then
                    HTML = Str.Remove(0, 11)
                End If
            End If
            Return HTML.Trim
        End Function
        'object  to string conversion
        Public Shared Function ToString(ByVal obj As Object) As String
            Try
                If IsDBNull(obj) Then
                    Return ""
                ElseIf obj Is Nothing Then
                    Return ""
                Else
                    Return Convert.ToString(obj)
                End If
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function ToLong(ByVal value As Object) As Int64
            Try
                If IsDBNull(value) Then
                    Return 0
                ElseIf value Is Nothing Then
                    Return 0
                ElseIf value.ToString() = "" Then
                    Return 0
                Else
                    Return System.Convert.ToInt64(value)
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function ToInteger(ByVal value As Object) As Int32
            Try
                If IsDBNull(value) Then
                    Return 0
                ElseIf value Is Nothing Then
                    Return 0
                ElseIf value.ToString() = "" Then
                    Return 0
                Else
                    Return System.Convert.ToInt32(value)
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function ToShort(ByVal value As Object) As Short
            Try
                If IsDBNull(value) Then
                    Return 0
                ElseIf value Is Nothing Then
                    Return 0
                ElseIf value.ToString() = "" Then
                    Return 0
                Else
                    Return System.Convert.ToInt16(value)
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function ToDecimal(ByVal value As Object) As Decimal
            Try
                If IsDBNull(value) Then
                    Return 0
                ElseIf value Is Nothing Then
                    Return 0
                ElseIf value.ToString() = "" Then
                    Return 0
                Else
                    Return System.Convert.ToDecimal(value)
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function ToDouble(ByVal value As Object) As Double
            Try
                If IsDBNull(value) Then
                    Return 0
                ElseIf value Is Nothing Then
                    Return 0
                ElseIf value.ToString() = "" Then
                    Return 0
                Else
                    Return System.Convert.ToDouble(value)
                End If
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function ToBool(ByVal value As Object) As Boolean
            Try
                If IsDBNull(value) Then
                    Return False
                ElseIf value Is Nothing Then
                    Return False
                ElseIf value.ToString() = "" Then
                    Return False
                Else
                    Return System.Convert.ToBoolean(value)
                End If
            Catch ex As Exception
                Return False
            End Try
        End Function
        ' returns 2009-11-25 06:46:41.140
        Public Shared Function ToSqlDate(ByVal value As Object) As Date
            Try
                If value = Nothing Then
                    Return Now.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.000")
                ElseIf IsDBNull(value) Then
                    Return Now.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.000")
                ElseIf value.ToString() = "" Then
                    Return Now.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.000")
                Else
                    Dim parsedDatetime As Date = Date.Parse(value)
                    Return parsedDatetime.ToString("yyyy-MM-dd HH:mm:ss.000")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' RenderControl receives any control and returns its HTML string representation.
        ''' </summary>
        ''' <param name="ctrl"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function RenderControl(ByVal ctrl As System.Web.UI.Control) As String
            Try
                Dim sb As New StringBuilder
                Dim tw As New StringWriter(sb)
                Dim hw As New Web.UI.HtmlTextWriter(tw)
                ctrl.RenderControl(hw)
                Return sb.ToString()
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function GetPaymentGatewayDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@intPaymentGateway", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _PaymentGWId

                arParms(2) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _numSiteId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetUSAEpayDtls", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function ManageFieldRelationships() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _Mode

                arParms(1) = New Npgsql.NpgsqlParameter("@numFieldRelID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _FieldRelID

                arParms(2) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ModuleID


                arParms(3) = New Npgsql.NpgsqlParameter("@numPrimaryListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _PrimaryListID

                arParms(4) = New Npgsql.NpgsqlParameter("@numSecondaryListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _SecondaryListID

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@numFieldRelDTLID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _FieldRelDTLID

                arParms(7) = New Npgsql.NpgsqlParameter("@numPrimaryListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _PrimaryListItemID

                arParms(8) = New Npgsql.NpgsqlParameter("@numSecondaryListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _SecondaryListItemID

                arParms(9) = New Npgsql.NpgsqlParameter("@bitTaskRelation", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = IsTaskRelation

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageFieldRelationship", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetFieldRelationships() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@byteMode", _Mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numFieldRelID", _FieldRelID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numModuleID", ModuleID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPrimaryListID", PrimaryListID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPrimaryListItemID", PrimaryListItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSecondaryListID", SecondaryListID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitTaskRelation", IsTaskRelation, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_GetFieldRelationship", sqlParams.ToArray())
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Sub GetAuthorizedSubTabs(ByVal uTab As Telerik.Web.UI.RadTabStrip, ByVal DomainID As Long, ByVal LocationID As Long, ByVal GroupID As Long)
            Try
                Dim dtSubTabs As DataTable
                Dim objCustomFields As New CustomFields
                objCustomFields.locId = LocationID
                objCustomFields.DomainID = DomainID
                objCustomFields.Type = 2
                objCustomFields.GroupID = GroupID
                dtSubTabs = objCustomFields.GetCustomFieldSubTabs
                'add default subtabs
                If dtSubTabs.Rows.Count > 0 Then
                    Dim i As Integer = 0
                    Dim tabkey As String = GetQueryStringValOld(HttpContext.Current.Request.QueryString("enc"), "tabkey")

                    For Each tabItem As RadTab In uTab.Tabs
                        tabItem.Text = CCommon.ToString(dtSubTabs.Rows(i)("Grp_Name"))
                        If dtSubTabs.Rows(i)("bitAllowed") = False Then
                            tabItem.Visible = False
                        Else
                            tabItem.Visible = True
                        End If

                        If tabkey = tabItem.Value Then
                            uTab.SelectedIndex = tabItem.Index
                        End If

                        i = i + 1
                    Next
                End If

                'add custom field tabs 
                'Dim dr() As DataRow
                'dr = dtSubTabs.Select(" tintType=0 or tintType=1 ")
                'If dr.Length > 0 Then
                '    Dim tab As Infragistics.Web.UI.LayoutControls.ContentTabItem
                '    For Each dr1 As DataRow In dr
                '        tab = New Infragistics.Web.UI.LayoutControls.ContentTabItem
                '        tab.Key = dr1("Grp_id").ToString
                '        tab.Text = "&nbsp;&nbsp;" & dr1("Grp_Name").ToString & "&nbsp;&nbsp;"
                '        If dr1("bitAllowed") = True Then
                '            tab.Visible = True
                '        Else
                '            tab.Visible = False
                '        End If
                '        uTab.Tabs.Add(tab)
                '    Next
                'End If
                objCustomFields.Type = 0
                dtSubTabs = objCustomFields.GetCustomFieldSubTabs
                If dtSubTabs.Rows.Count > 0 Then
                    Dim tab As RadTab
                    Dim pageView As New RadPageView()

                    For Each dr As DataRow In dtSubTabs.Rows
                        tab = New RadTab
                        tab.Value = dr("Grp_id").ToString
                        tab.Text = "&nbsp;&nbsp;" & dr("Grp_Name").ToString & "&nbsp;&nbsp;"
                        If dr("bitAllowed") = True Then
                            tab.Visible = True
                        Else
                            tab.Visible = False
                        End If
                        tab.PageViewID = "radPageView_" & dr("Grp_id").ToString
                        uTab.Tabs.Add(tab)

                        pageView = New RadPageView
                        pageView.ID = "radPageView_" & dr("Grp_id").ToString
                        uTab.MultiPage.PageViews.Add(pageView)
                    Next
                End If
                objCustomFields.Type = 1
                dtSubTabs = objCustomFields.GetCustomFieldSubTabs
                If dtSubTabs.Rows.Count > 0 Then
                    Dim tab As RadTab
                    Dim pageView As New RadPageView()

                    For Each dr As DataRow In dtSubTabs.Rows
                        tab = New RadTab
                        tab.Value = dr("Grp_id").ToString
                        tab.Text = "&nbsp;&nbsp;" & dr("Grp_Name").ToString & "&nbsp;&nbsp;"
                        If dr("bitAllowed") = True Then
                            tab.Visible = True
                        Else
                            tab.Visible = False
                        End If
                        tab.PageViewID = "radPageView_" & dr("Grp_id").ToString
                        uTab.Tabs.Add(tab)

                        pageView = New RadPageView
                        pageView.ID = "radPageView_" & dr("Grp_id").ToString
                        uTab.MultiPage.PageViews.Add(pageView)
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
            'Dim dtblSubTab As DataTable
            'Me.DomainID = DomainID
            'Me.ModuleID = ModuleID
            'dtblSubTab = GetSubTabsControlInformation()
            'If Not dtblSubTab Is Nothing Then
            '    Dim intCounter As Integer = dtblSubTab.Rows.Count
            '    If uTab.Tabs.Count < intCounter Then
            '        intCounter = uTab.Tabs.Count
            '    End If
            '    Dim i As Integer
            '    For i = 0 To intCounter - 1
            '        uTab.Tabs(i).Text = dtblSubTab.Rows(i)("numTabName").ToString()
            '        If Not dtblSubTab.Rows(i)("IsVisible") Then
            '            uTab.Tabs(i).Visible = False
            '        End If
            '    Next
            'End If
        End Sub

        Public Sub GetAuthorizedSubTabsNew(ByVal uTab As Telerik.Web.UI.RadTabStrip, ByVal DomainID As Long, ByVal LocationID As Long, ByVal GroupID As Long)
            Try
                Dim dtSubTabs As DataTable
                Dim objCustomFields As New CustomFields
                objCustomFields.locId = LocationID
                objCustomFields.DomainID = DomainID
                objCustomFields.Type = 2
                objCustomFields.GroupID = GroupID
                dtSubTabs = objCustomFields.GetCustomFieldSubTabs
                'add default subtabs
                If dtSubTabs.Rows.Count > 0 Then
                    Dim tabkey As String = GetQueryStringValOld(HttpContext.Current.Request.QueryString("enc"), "tabkey")

                    For Each drTab As DataRow In dtSubTabs.Rows
                        If Not uTab.Tabs.FindTabByText(CCommon.ToString(drTab("Grp_Name"))) Is Nothing Then
                            If Not CCommon.ToBool(drTab("bitAllowed")) Then
                                uTab.Tabs.FindTabByText(CCommon.ToString(drTab("Grp_Name"))).Visible = False
                            Else
                                uTab.Tabs.FindTabByText(CCommon.ToString(drTab("Grp_Name"))).Visible = True
                            End If
                        End If
                    Next
                End If

                objCustomFields.Type = 0
                dtSubTabs = objCustomFields.GetCustomFieldSubTabs
                If dtSubTabs.Rows.Count > 0 Then
                    Dim tab As RadTab
                    Dim pageView As New RadPageView()

                    For Each dr As DataRow In dtSubTabs.Rows
                        tab = New RadTab
                        tab.Value = dr("Grp_id").ToString
                        tab.Text = "&nbsp;&nbsp;" & dr("Grp_Name").ToString & "&nbsp;&nbsp;"
                        If dr("bitAllowed") = True Then
                            tab.Visible = True
                        Else
                            tab.Visible = False
                        End If
                        tab.PageViewID = "radPageView_" & dr("Grp_id").ToString
                        uTab.Tabs.Add(tab)

                        pageView = New RadPageView
                        pageView.ID = "radPageView_" & dr("Grp_id").ToString
                        uTab.MultiPage.PageViews.Add(pageView)
                    Next
                End If
                objCustomFields.Type = 1
                dtSubTabs = objCustomFields.GetCustomFieldSubTabs
                If dtSubTabs.Rows.Count > 0 Then
                    Dim tab As RadTab
                    Dim pageView As New RadPageView()

                    For Each dr As DataRow In dtSubTabs.Rows
                        tab = New RadTab
                        tab.Value = dr("Grp_id").ToString
                        tab.Text = "&nbsp;&nbsp;" & dr("Grp_Name").ToString & "&nbsp;&nbsp;"
                        If dr("bitAllowed") = True Then
                            tab.Visible = True
                        Else
                            tab.Visible = False
                        End If
                        tab.PageViewID = "radPageView_" & dr("Grp_id").ToString
                        uTab.Tabs.Add(tab)

                        pageView = New RadPageView
                        pageView.ID = "radPageView_" & dr("Grp_id").ToString
                        uTab.MultiPage.PageViews.Add(pageView)
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub


        Public Sub GetAuthorizedSubTabs(ByVal uTab As Infragistics.Web.UI.LayoutControls.WebTab, ByVal DomainID As Long, ByVal LocationID As Long, ByVal GroupID As Long)
            Try
                Dim dtSubTabs As DataTable
                Dim objCustomFields As New CustomFields
                objCustomFields.locId = LocationID
                objCustomFields.DomainID = DomainID
                objCustomFields.Type = 2
                objCustomFields.GroupID = GroupID
                dtSubTabs = objCustomFields.GetCustomFieldSubTabs
                'add default subtabs
                If dtSubTabs.Rows.Count > 0 Then
                    Dim i As Integer = 0
                    Dim tabkey As String = GetQueryStringValOld(HttpContext.Current.Request.QueryString("enc"), "tabkey")

                    For Each tabItem As Infragistics.Web.UI.LayoutControls.ContentTabItem In uTab.Tabs
                        tabItem.Text = CCommon.ToString(dtSubTabs.Rows(i)("Grp_Name"))
                        If dtSubTabs.Rows(i)("bitAllowed") = False Then
                            tabItem.Visible = False
                        Else
                            tabItem.Visible = True
                        End If

                        If tabkey = tabItem.Key Then
                            uTab.SelectedIndex = tabItem.Index
                        End If

                        i = i + 1
                    Next
                End If

                'add custom field tabs 
                'Dim dr() As DataRow
                'dr = dtSubTabs.Select(" tintType=0 or tintType=1 ")
                'If dr.Length > 0 Then
                '    Dim tab As Infragistics.Web.UI.LayoutControls.ContentTabItem
                '    For Each dr1 As DataRow In dr
                '        tab = New Infragistics.Web.UI.LayoutControls.ContentTabItem
                '        tab.Key = dr1("Grp_id").ToString
                '        tab.Text = "&nbsp;&nbsp;" & dr1("Grp_Name").ToString & "&nbsp;&nbsp;"
                '        If dr1("bitAllowed") = True Then
                '            tab.Visible = True
                '        Else
                '            tab.Visible = False
                '        End If
                '        uTab.Tabs.Add(tab)
                '    Next
                'End If
                objCustomFields.Type = 0
                dtSubTabs = objCustomFields.GetCustomFieldSubTabs
                If dtSubTabs.Rows.Count > 0 Then
                    Dim tab As Infragistics.Web.UI.LayoutControls.ContentTabItem
                    For Each dr As DataRow In dtSubTabs.Rows
                        tab = New Infragistics.Web.UI.LayoutControls.ContentTabItem
                        tab.Key = dr("Grp_id").ToString
                        tab.Text = "&nbsp;&nbsp;" & dr("Grp_Name").ToString & "&nbsp;&nbsp;"
                        If dr("bitAllowed") = True Then
                            tab.Visible = True
                        Else
                            tab.Visible = False
                        End If
                        uTab.Tabs.Add(tab)
                    Next
                End If
                objCustomFields.Type = 1
                dtSubTabs = objCustomFields.GetCustomFieldSubTabs
                If dtSubTabs.Rows.Count > 0 Then
                    Dim tab As Infragistics.Web.UI.LayoutControls.ContentTabItem
                    For Each dr As DataRow In dtSubTabs.Rows
                        tab = New Infragistics.Web.UI.LayoutControls.ContentTabItem
                        tab.Key = dr("Grp_id").ToString
                        tab.Text = "&nbsp;&nbsp;" & dr("Grp_Name").ToString & "&nbsp;&nbsp;"
                        If dr("bitAllowed") = True Then
                            tab.Visible = True
                        Else
                            tab.Visible = False
                        End If
                        uTab.Tabs.Add(tab)
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
            'Dim dtblSubTab As DataTable
            'Me.DomainID = DomainID
            'Me.ModuleID = ModuleID
            'dtblSubTab = GetSubTabsControlInformation()
            'If Not dtblSubTab Is Nothing Then
            '    Dim intCounter As Integer = dtblSubTab.Rows.Count
            '    If uTab.Tabs.Count < intCounter Then
            '        intCounter = uTab.Tabs.Count
            '    End If
            '    Dim i As Integer
            '    For i = 0 To intCounter - 1
            '        uTab.Tabs(i).Text = dtblSubTab.Rows(i)("numTabName").ToString()
            '        If Not dtblSubTab.Rows(i)("IsVisible") Then
            '            uTab.Tabs(i).Visible = False
            '        End If
            '    Next
            'End If
        End Sub

        Public Function AssignTo() As DataTable
            Try
                Dim ds As DataSet

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactType

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_OPPAssignTo", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function PopulateOrganization() As DataSet
            Try
                Dim Connection As New Npgsql.NpgsqlConnection(GetConnection.GetConnectionString)
                Dim arParams() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParams(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(0).Value = DomainID

                arParams(1) = New Npgsql.NpgsqlParameter("@vcCmpFilter", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParams(1).Value = _Filter

                arParams(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(2).Value = UserCntID

                arParams(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParams(3).Value = Nothing
                arParams(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(Connection, "usp_GetCompany", arParams)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCompanyName() As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "usp_GetCompanyName", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetCardTypes() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@sintType", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(1).Value = _tinyOrder

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCardTypes", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub FillAssignToBasedOnPreference(ByRef objDropdown As DropDownList, Optional ByVal lngDivisionID As Long = 0)
            Try
                If HttpContext.Current.Session("PopulateUserCriteria") = 1 Then
                    _DivisionID = lngDivisionID
                    _charModule = "D"
                    GetCompanySpecificValues1()
                    sb_FillConEmpFromTerritories(objDropdown, HttpContext.Current.Session("DomainID"), 0, 0, _TerittoryID)
                ElseIf HttpContext.Current.Session("PopulateUserCriteria") = 2 Then
                    sb_FillConEmpFromDBUTeam(objDropdown, HttpContext.Current.Session("DomainID"), HttpContext.Current.Session("UserContactID"))
                Else : sb_FillConEmpFromDBSel(objDropdown, HttpContext.Current.Session("DomainID"), 0, 0)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function RemoveDuplicatesInArray(ByVal items As String()) As String()
            Dim noDupsArrList As New ArrayList()
            For i As Integer = 0 To items.Length - 1
                If Not noDupsArrList.Contains(items(i).Trim()) Then
                    noDupsArrList.Add(items(i).Trim())
                End If
            Next

            Dim uniqueItems As String() = New String(noDupsArrList.Count - 1) {}
            noDupsArrList.CopyTo(uniqueItems)
            Return uniqueItems
        End Function
        'Supported date format in bizautomation -from domain details
        'MM/DD/YYYY
        'MM-DD-YYYY
        'DD/MON/YYYY
        'DD-MON-YYYY
        'DD/MONTH/YYYY
        'DD-MONTH-YYYY
        'DD/MM/YYYY
        'DD-MM-YYYY
        'return date format supported by javascript calender
        Public Shared Function GetDateFormat() As String
            Try
                Dim strFormat As String = CCommon.ToString(HttpContext.Current.Session("DateFormat"))
                If strFormat = "" Then
                    strFormat = "%m/%d/%Y"
                Else
                    strFormat = strFormat.Replace("YYYY", "%Y")
                    strFormat = strFormat.Replace("MM", "%m")
                    strFormat = strFormat.Replace("DD", "%d")
                    strFormat = strFormat.Replace("MONTH", "%B")
                    strFormat = strFormat.Replace("MON", "%b")
                End If
                Return strFormat
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetValidationDateFormat() As String
            Try
                Dim strFormat As String = CCommon.ToString(HttpContext.Current.Session("DateFormat"))
                If strFormat = "" Then
                    strFormat = "MM/dd/yyyy"
                Else
                    strFormat = strFormat.Replace("YYYY", "yyyy")
                    strFormat = strFormat.Replace("MM", "MM")
                    strFormat = strFormat.Replace("DD", "dd")
                    strFormat = strFormat.Replace("MONTH", "MMM")
                    strFormat = strFormat.Replace("MON", "NNN")
                End If
                Return strFormat
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetDocumentPath(Optional ByVal DomainID As Long = 0) As String
            Dim strPortalLocation As String = System.Configuration.ConfigurationManager.AppSettings("PortalVirtualDirectoryName")
            If String.IsNullOrEmpty(strPortalLocation) = False AndAlso strPortalLocation.Length > 0 Then
                strPortalLocation = "/" & strPortalLocation
            End If

            Return HttpContext.Current.Session("SiteType") & "//" & HttpContext.Current.Request.ServerVariables("SERVER_NAME") & strPortalLocation & "/Documents/Docs/" & IIf(DomainID > 0, DomainID.ToString() & "/", "")
        End Function
        ''' <summary>
        ''' Will return Physical path where documents are stored , DomainID is optional 
        ''' </summary>
        ''' <param name="DomainID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetDocumentPhysicalPath(Optional ByVal DomainID As Long = 0) As String
            Return CCommon.ToString(System.Configuration.ConfigurationManager.AppSettings("PortalLocation")) & "\Documents\Docs\" & IIf(DomainID > 0, DomainID.ToString() & "\", "")
        End Function

        Public Shared Function GetLuceneIndexPath(ByVal DomainID As Long, ByVal SiteID As Long) As String
            Return CCommon.ToString(System.Configuration.ConfigurationManager.AppSettings("LuceneIndex")) + "\\" + DomainID.ToString() + "\\LuceneIndex\\Sites\\" + SiteID.ToString
        End Function


        Public Shared Function GetImageHTML(ByVal strThumbImage As Object, ByVal Mode As Short, Optional ByVal imgWidth As Double = 0, Optional ByVal imgHeight As Double = 0) As String
            Dim strImg As String
            If Not CCommon.ToString(strThumbImage).Trim = "" Then
                If imgHeight > 0 AndAlso imgWidth > 0 Then
                    strImg = "<img class='resizeme' style='width:" & imgWidth & "px;height:" & imgHeight & "px;border-width: 0px;' alt='change image' src='" & GetDocumentPath(CCommon.ToLong(HttpContext.Current.Session("DomainID"))) & CCommon.ToString(strThumbImage) & "'> "
                Else
                    strImg = "<img class='resizeme' style='border-width: 0px;' alt='change image' src='" & GetDocumentPath(CCommon.ToLong(HttpContext.Current.Session("DomainID"))) & CCommon.ToString(strThumbImage) & "'> "
                End If
                'strImg = "<img class='resizeme' style='border-width: 0px;' alt='change image' src='" & GetDocumentPath(CCommon.ToLong(HttpContext.Current.Session("DomainID"))) & CCommon.ToString(strThumbImage) & "'> "
            Else
                If Mode = 1 Then
                    strImg = "<img style='border-width: 0px;' align='middle' alt='Image not available' src='../images/tag.png' > NA"
                Else
                    strImg = "<img style='border-width: 0px;' align='middle' alt='change image' src='../images/tag.png' >Change Image"
                End If

            End If
            Return strImg
        End Function
        Public Shared Function ReplaceLastOccurrence(ByVal Source As String, ByVal Find As String, ByVal Replace As String) As String
            Dim Place As Integer = Source.LastIndexOf(Find)
            Dim result As String = Source.Remove(Place, Find.Length).Insert(Place, Replace)
            Return result
        End Function
        Public Function GetListItemValue(ByVal strFieldValue As String, ByVal lngDomainID As Long) As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcFldValue", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(0).Value = strFieldValue

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = lngDomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CCommon.ToString(SqlDAL.ExecuteScalar(connString, "USP_GetListItemData", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Function GetDecimalFormat(ByVal value As Object) As String
            Dim DecimalPoint As Short
            If Not HttpContext.Current Is Nothing Then
                If HttpContext.Current.Session("DecimalPoints") Is Nothing Then
                    DecimalPoint = 2
                Else
                    DecimalPoint = CType(HttpContext.Current.Session("DecimalPoints"), Short)
                End If
            Else
                DecimalPoint = 2
            End If

            Select Case DecimalPoint
                Case 1
                    Return String.Format("{0:#,##0.0}", value)
                Case 2
                    Return String.Format("{0:#,##0.00}", value)
                Case 3
                    Return String.Format("{0:#,##0.000}", value)
                Case 4
                    Return String.Format("{0:#,##0.0000}", value)
                Case 5
                    Return String.Format("{0:#,##0.00000}", value)
                Case Else
                    Return String.Format("{0:#,##0.00}", value)
            End Select

        End Function
        Public Shared Function SetValueInDecimalFormat(ByVal value As Object, ByRef textBox As TextBox)
            Dim DecimalPoint As Short

            If Not HttpContext.Current Is Nothing Then
                If HttpContext.Current.Session("DecimalPoints") Is Nothing Then
                    DecimalPoint = 2
                Else
                    DecimalPoint = CType(HttpContext.Current.Session("DecimalPoints"), Short)
                End If
            Else
                DecimalPoint = 2
            End If

            Select Case DecimalPoint
                Case 1
                    textBox.Text = String.Format("{0:0.0}", value)
                Case 2
                    textBox.Text = String.Format("{0:0.00}", value)
                Case 3
                    textBox.Text = String.Format("{0:0.000}", value)
                Case 4
                    textBox.Text = String.Format("{0:0.0000}", value)
                Case 5
                    textBox.Text = String.Format("{0:0.00000}", value)
                Case Else
                    textBox.Text = String.Format("{0:0.00}", value)
            End Select
        End Function
        Public Shared Function SetValueInDecimalFormat(ByVal value As Object, ByRef label As Label)
            Dim DecimalPoint As Short

            If Not HttpContext.Current Is Nothing Then
                If HttpContext.Current.Session("DecimalPoints") Is Nothing Then
                    DecimalPoint = 2
                Else
                    DecimalPoint = CType(HttpContext.Current.Session("DecimalPoints"), Short)
                End If
            Else
                DecimalPoint = 2
            End If

            Select Case DecimalPoint
                Case 1
                    label.Text = String.Format("{0:0.0}", value)
                Case 2
                    label.Text = String.Format("{0:0.00}", value)
                Case 3
                    label.Text = String.Format("{0:0.000}", value)
                Case 4
                    label.Text = String.Format("{0:0.0000}", value)
                Case 5
                    label.Text = String.Format("{0:0.00000}", value)
                Case Else
                    label.Text = String.Format("{0:0.00}", value)
            End Select

        End Function
        Public Shared Function SetCommaSeparatedInDecimalFormat(ByVal value As Object, ByRef textBox As TextBox)
            Dim DecimalPoint As Short

            If Not HttpContext.Current Is Nothing Then
                If HttpContext.Current.Session("DecimalPoints") Is Nothing Then
                    DecimalPoint = 2
                Else
                    DecimalPoint = CType(HttpContext.Current.Session("DecimalPoints"), Short)
                End If
            Else
                DecimalPoint = 2
            End If

            Select Case DecimalPoint
                Case 1
                    textBox.Text = String.Format("{0:#,##0.0}", value)
                Case 2
                    textBox.Text = String.Format("{0:#,##0.00}", value)
                Case 3
                    textBox.Text = String.Format("{0:#,##0.000}", value)
                Case 4
                    textBox.Text = String.Format("{0:#,##0.0000}", value)
                Case 5
                    textBox.Text = String.Format("{0:#,##0.00000}", value)
                Case Else
                    textBox.Text = String.Format("{0:#,##0.00}", value)
            End Select
        End Function
        Public Shared Function SetCommaSeparatedInDecimalFormat(ByVal value As Object, ByRef label As Label)
            Dim DecimalPoint As Short
            If Not HttpContext.Current Is Nothing Then
                If HttpContext.Current.Session("DecimalPoints") Is Nothing Then
                    DecimalPoint = 2
                Else
                    DecimalPoint = CType(HttpContext.Current.Session("DecimalPoints"), Short)
                End If
            Else
                DecimalPoint = 2
            End If

            Select Case DecimalPoint
                Case 1
                    label.Text = String.Format("{0:#,##0.0}", value)
                Case 2
                    label.Text = String.Format("{0:#,##0.00}", value)
                Case 3
                    label.Text = String.Format("{0:#,##0.000}", value)
                Case 4
                    label.Text = String.Format("{0:#,##0.0000}", value)
                Case 5
                    label.Text = String.Format("{0:#,##0.00000}", value)
                Case Else
                    label.Text = String.Format("{0:#,##0.00}", value)
            End Select

        End Function
        Public Shared Function GetDecimalPoints() As Short
            Dim DecimalPoint As Short

            If Not HttpContext.Current Is Nothing Then
                If HttpContext.Current.Session("DecimalPoints") Is Nothing Then
                    DecimalPoint = 2
                Else
                    DecimalPoint = CType(HttpContext.Current.Session("DecimalPoints"), Short)
                End If
            Else
                DecimalPoint = 2
            End If

            Return DecimalPoint
        End Function
        Public Shared Function GetDataFormatString() As String
            Dim DecimalPoint As Short

            If Not HttpContext.Current Is Nothing Then
                If HttpContext.Current.Session("DecimalPoints") Is Nothing Then
                    DecimalPoint = 2
                Else
                    DecimalPoint = CType(HttpContext.Current.Session("DecimalPoints"), Short)
                End If
            Else
                DecimalPoint = 2
            End If

            Select Case DecimalPoint
                Case 1
                    Return "{0:#,##0.0}"
                Case 2
                    Return "{0:#,##0.00}"
                Case 3
                    Return "{0:#,##0.000}"
                Case 4
                    Return "{0:#,##0.0000}"
                Case 5
                    Return "{0:#,##0.00000}"
                Case Else
                    Return "{0:#,##0.00}"
            End Select

        End Function

        Public Shared Function GetDataFormatStringWithCurrency() As String
            Dim DecimalPoint As Short

            If Not HttpContext.Current Is Nothing Then
                If HttpContext.Current.Session("DecimalPoints") Is Nothing Then
                    DecimalPoint = 2
                Else
                    DecimalPoint = CType(HttpContext.Current.Session("DecimalPoints"), Short)
                End If
            Else
                DecimalPoint = 2
            End If

            Select Case DecimalPoint
                Case 1
                    Return "{0:" & CCommon.ToString(HttpContext.Current.Session("Currency")) & "#,##0.0}"
                Case 2
                    Return "{0:" & CCommon.ToString(HttpContext.Current.Session("Currency")) & "#,##0.00}"
                Case 3
                    Return "{0:" & CCommon.ToString(HttpContext.Current.Session("Currency")) & "#,##0.000}"
                Case 4
                    Return "{0:" & CCommon.ToString(HttpContext.Current.Session("Currency")) & "#,##0.0000}"
                Case 5
                    Return "{0:" & CCommon.ToString(HttpContext.Current.Session("Currency")) & "#,##0.00000}"
                Case Else
                    Return "{0:" & CCommon.ToString(HttpContext.Current.Session("Currency")) & "#,##0.00}"
            End Select

        End Function

        Public Shared Function GetPriceDataFormatString() As String
            Dim DecimalPoint As Short

            If Not HttpContext.Current Is Nothing Then
                If HttpContext.Current.Session("DecimalPoints") Is Nothing Then
                    DecimalPoint = 2
                Else
                    DecimalPoint = CType(HttpContext.Current.Session("DecimalPoints"), Short)
                End If
            Else
                DecimalPoint = 2
            End If

            Select Case DecimalPoint
                Case 1
                    Return "{0:#,##0.0####}"
                Case 2
                    Return "{0:#,##0.00###}"
                Case 3
                    Return "{0:#,##0.000##}"
                Case 4
                    Return "{0:#,##0.0000#}"
                Case 5
                    Return "{0:#,##0.00000}"
                Case Else
                    Return "{0:#,##0.00###}"
            End Select

        End Function
        Public Sub AddAttchmentToSession(ByVal strFileName As String, ByVal strFileLogicalPath As String, ByVal strFilePhysicalPath As String, Optional ByVal strFileType As String = "", Optional ByVal strFilesize As String = "")
            Try
                Dim dtTable As DataTable
                If System.Web.HttpContext.Current.Session("Attachements") Is Nothing Then
                    dtTable = New DataTable
                    dtTable.Columns.Add("Filename")
                    dtTable.Columns.Add("FileLocation")
                    dtTable.Columns.Add("FilePhysicalPath")
                    dtTable.Columns.Add("FileSize", GetType(Long))
                    dtTable.Columns.Add("FileType")
                Else : dtTable = CType(System.Web.HttpContext.Current.Session("Attachements"), DataTable)
                End If

                Dim dr As DataRow
                dr = dtTable.NewRow

                dr("Filename") = strFileName
                dr("FileLocation") = strFileLogicalPath
                dr("FilePhysicalPath") = strFilePhysicalPath
                dr("FileType") = strFileType
                dr("FileSize") = CCommon.ToLong(strFilesize)
                dtTable.Rows.Add(dr)
                System.Web.HttpContext.Current.Session("Attachements") = dtTable
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function ManageComments() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _CaseID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcHeading", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _Heading

                arParms(3) = New Npgsql.NpgsqlParameter("@txtComments", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _Comments

                arParms(4) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _ContactID

                arParms(5) = New Npgsql.NpgsqlParameter("@numCommentID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _CommentID

                arParms(6) = New Npgsql.NpgsqlParameter("@numStageID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _StageID

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = DomainID

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_Comments", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Note: Javascript requires Two variable to be set 
        '1) DivisionID -> Required to get items for selected vendor(only when purchase opp)
        '2) OppType  
        Sub InitializeClientSideTemplate(ByVal radCmbItem As RadComboBox)
            Try
                UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                Dim ds As DataSet = NewOppItems(CCommon.ToLong(HttpContext.Current.Session("DomainID")), 0, 0, "")
                Dim dtTable As DataTable
                dtTable = ds.Tables(1)
                'If dtTable.Rows.Count = 0 Then
                '    dtTable = ds.Tables(2)
                'End If
                'genrate header template dynamically
                radCmbItem.HeaderTemplate = New RadComboBoxTemplate(ListItemType.Header, dtTable, radCmbItem.DropDownWidth.Value)
                'generate Clientside Template dynamically
                Dim sb As New System.Text.StringBuilder
                Dim strWidth As String = CCommon.ToInteger(((radCmbItem.DropDownWidth.Value - 25) / dtTable.Rows.Count)).ToString()

                sb.AppendLine("var cb;")
                sb.AppendLine(" function cb_onDataBound(pComboboxItem, pDropDownLine) {")
                sb.AppendLine("     var customAttributes = pComboboxItem.get_attributes();")
                sb.AppendLine("if(customAttributes.get_count() == 0)return;")
                sb.AppendLine("            pDropDownLine.innerHTML =")
                sb.AppendLine("''")

                Dim preText As String = " + '<span class=""column"" style=""width:" & strWidth & "px;"">' "
                Dim postText As String = " + '</span>'"
                For Each dr As DataRow In dtTable.Rows
                    If dr("vcDbColumnName").ToString = "txtItemDesc" Or dr("vcDbColumnName").ToString = "vcItemName" Then
                        sb.AppendLine(preText + " + customAttributes.getAttribute(""" & dr("vcDbColumnName").ToString & """)" + postText)
                    Else
                        sb.AppendLine(preText + " + customAttributes.getAttribute(""" & dr("vcDbColumnName").ToString & """)" + postText)
                    End If
                Next
                sb.AppendLine(";")
                sb.AppendLine("}")
                'Following code is commented and u have add pageload function to each page
                'where u want to use this method because of if multiple page load function exist on 
                'page then this will not work
                'sb.AppendLine(" function pageLoad(sender, args) {")
                'sb.AppendLine("initComboExtensions();")
                'sb.AppendLine("cb = $find(""" & radCmbItem.ClientID & """);")
                'sb.AppendLine("if (cb != null) {")
                'sb.AppendLine("cb.onDataBound = cb_onDataBound;")
                'sb.AppendLine("}")
                'sb.AppendLine("        };")

                Dim p As Page = CType(System.Web.HttpContext.Current.Handler, Page)
                Dim Include2 As New UI.HtmlControls.HtmlGenericControl("script")
                Include2.Attributes.Add("type", "text/javascript")
                Include2.InnerHtml = sb.ToString()
                p.Header.Controls.Add(Include2)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Shared Sub UpdateItemRadComboValues(ByVal OppType As String, ByVal DivisionID As String)
            Try
                'Set javascript Variable for radCmbItem
                Dim sb As New System.Text.StringBuilder
                If DivisionID = "" Then DivisionID = "0"
                If OppType = "" Then OppType = "1"

                sb.AppendLine("var SearchCharLength = " & IIf(CCommon.ToInteger(HttpContext.Current.Session("ChrForItemSearch")) = 0, 1, CCommon.ToInteger(HttpContext.Current.Session("ChrForItemSearch"))).ToString & ";")
                Dim p As Page = CType(System.Web.HttpContext.Current.Handler, Page)
                p.ClientScript.RegisterClientScriptBlock(p.GetType, "UpdateValues", sb.ToString, True)
                If Not CType(p.FindControl("hdnCmbDivisionID"), HiddenField) Is Nothing Then
                    CType(p.FindControl("hdnCmbDivisionID"), HiddenField).Value = DivisionID
                ElseIf Not p.Master.FindControl("Content") Is Nothing Then
                    If Not CType(p.Master.FindControl("Content").FindControl("hdnCmbDivisionID"), HiddenField) Is Nothing Then
                        CType(p.Master.FindControl("Content").FindControl("hdnCmbDivisionID"), HiddenField).Value = DivisionID
                    End If
                ElseIf Not p.Master.FindControl("TabsPlaceHolder") Is Nothing Then
                    If Not CType(p.Master.FindControl("TabsPlaceHolder").FindControl("hdnCmbDivisionID"), HiddenField) Is Nothing Then
                        CType(p.Master.FindControl("TabsPlaceHolder").FindControl("hdnCmbDivisionID"), HiddenField).Value = DivisionID
                    End If
                End If
                'If Not CType(CCommon.FindControlRecursive(p, "hdnCmbDivisionID"), HiddenField) Is Nothing Then
                '    CType(CCommon.FindControlRecursive(p, "hdnCmbDivisionID"), HiddenField).Value = DivisionID
                'End If

                'If Not CType(CCommon.FindControlRecursive(p, "hdnCmbOppType"), HiddenField) Is Nothing Then
                '    CType(CCommon.FindControlRecursive(p, "hdnCmbOppType"), HiddenField).Value = OppType
                'End If

                If Not CType(p.FindControl("hdnCmbOppType"), HiddenField) Is Nothing Then
                    CType(p.FindControl("hdnCmbOppType"), HiddenField).Value = OppType
                ElseIf Not p.Master.FindControl("Content") Is Nothing Then
                    If Not CType(p.Master.FindControl("Content").FindControl("hdnCmbOppType"), HiddenField) Is Nothing Then
                        CType(p.Master.FindControl("Content").FindControl("hdnCmbOppType"), HiddenField).Value = OppType
                    End If
                ElseIf Not p.Master.FindControl("TabsPlaceHolder") Is Nothing Then
                    If Not CType(p.Master.FindControl("TabsPlaceHolder").FindControl("hdnCmbOppType"), HiddenField) Is Nothing Then
                        CType(p.Master.FindControl("TabsPlaceHolder").FindControl("hdnCmbOppType"), HiddenField).Value = OppType
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function PreventSqlInjection(ByVal Val As String) As String
            Try
                Return Val.Trim().Replace("'", "")
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub AddStyleSheetLink(ByVal filename As String, ByVal page As System.Web.UI.Page)
            Try
                Dim link As New HtmlLink()
                link.Attributes("href") = filename
                link.Attributes("type") = "text/css"
                link.Attributes("rel") = "stylesheet"
                page.Header.Controls.Add(link)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub AddJavascriptLink(ByVal filename As String, ByVal page As System.Web.UI.Page)
            Try

                Dim javaScript As New HtmlGenericControl("script")
                javaScript.Attributes.Add("type", "text/javascript")
                javaScript.Attributes.Add("src", filename)
                page.Header.Controls.Add(javaScript)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub CheckdirtyForm(ByVal page As System.Web.UI.Page)
            Try
                If CCommon.ToBool(HttpContext.Current.Session("IsDirtyForm")) = True Then
                    If page.FindControl("form1") Is Nothing Then
                        If CType(page.Master.FindControl("hdnDirtyFlag"), TextBox) Is Nothing Then
                            Dim hdnElement As New TextBox
                            hdnElement.ID = "hdnDirtyFlag"
                            hdnElement.Attributes.Add("Style", "display: none")
                            page.Master.FindControl("form1").Controls.Add(hdnElement)
                        End If

                        Dim javaScript As New HtmlGenericControl("script")
                        javaScript.Attributes.Add("type", "text/javascript")
                        javaScript.Attributes.Add("src", "../JavaScript/DirtyForm.js")
                        page.Master.FindControl("form1").Controls.Add(javaScript)
                    Else
                        If CType(page.FindControl("hdnDirtyFlag"), TextBox) Is Nothing Then
                            Dim hdnElement As New TextBox
                            hdnElement.ID = "hdnDirtyFlag"
                            hdnElement.Attributes.Add("Style", "display: none")
                            page.FindControl("form1").Controls.Add(hdnElement)
                        End If

                        Dim javaScript As New HtmlGenericControl("script")
                        javaScript.Attributes.Add("type", "text/javascript")
                        javaScript.Attributes.Add("src", "../JavaScript/DirtyForm.js")
                        page.FindControl("form1").Controls.Add(javaScript)
                    End If

                    'page.Controls.Add(hdnElement)

                    'If Not page.IsClientScriptBlockRegistered("DirtyForm") Then
                    '    page.RegisterClientScriptBlock("DirtyForm", "<script type=""text/javascript"" src=""../JavaScript/DirtyForm.js"">")
                    'End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub CustomizePortalUI(ByVal page As System.Web.UI.Page)
            Try
                Dim MasterCSS As New HtmlLink
                If Not page.FindControl("MasterCSS") Is Nothing Then
                    MasterCSS = CType(page.FindControl("MasterCSS"), HtmlLink)
                End If
                If CCommon.ToBool(HttpContext.Current.Session("EnableCustomizePortal")) = True Then
                    MasterCSS.Visible = False
                    If Not HttpContext.Current.Session("PortalStyle") Is Nothing Then
                        Dim dt As DataTable = CType(HttpContext.Current.Session("PortalStyle"), DataTable)
                        For Each row As DataRow In dt.Rows
                            CCommon.AddStyleSheetLink(CCommon.GetDocumentPath(CCommon.ToLong(HttpContext.Current.Session("DomainID"))) & "PortalCss/" & row("StyleFileName").ToString, page)
                        Next
                    End If
                Else
                    If Not MasterCSS Is Nothing Then
                        MasterCSS.Visible = True
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' http://support.microsoft.com/kb/316063
        '''     This method is safe casts the string for XML
        ''' </summary>
        ''' <param name = "sValue">The string which is to be made XML safe</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/14/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Shared Function MakeSafeForXML(ByVal sValue As String) As String
            Try
                Return Replace(Replace(Replace(Replace(Replace(sValue, "'", "&#39;"), """", "&#39;&#39;"), "&", "&amp;"), "<", "&lt;"), ">", "&gt;")
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function Truncate(ByVal input As String, ByVal characterLimit As Integer) As String
            Dim output As String = input

            ' Check if the string is longer than the allowed amount
            ' otherwise do nothing
            If output.Length > characterLimit AndAlso characterLimit > 0 Then

                ' cut the string down to the maximum number of characters
                output = output.Substring(0, characterLimit)

                ' Check if the character right after the truncate point was a space
                ' if not, we are in the middle of a word and need to remove the rest of it
                If input.Substring(output.Length, 1) <> " " Then
                    Dim LastSpace As Integer = output.LastIndexOf(" ")

                    ' if we found a space then, cut back to that space
                    If LastSpace <> -1 Then
                        output = output.Substring(0, LastSpace)
                    End If
                End If
                ' Finally, add the "..."
                output += "..."
            End If
            Return output
        End Function

        Public Function GetUOM() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintUnitType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _UnitType

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUOM", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetUnit() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUOMId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _UnitId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUOM_Unit", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageUOM() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUOMId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _UnitId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcUnitName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = _UnitName

                arParms(3) = New Npgsql.NpgsqlParameter("@tintUnitType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _UnitType

                arParms(4) = New Npgsql.NpgsqlParameter("@bitEnabled", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _UnitEnable

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageUOM", objParam, True)
                _UnitId = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                If _UnitId <= 0 Then
                    Return False
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Public Function DeleteUnit() As Boolean
        '    Try

        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = DomainID

        '        arParms(1) = New Npgsql.NpgsqlParameter("@numUOMId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(1).Value = _UnitId
        '        SqlDAL.ExecuteNonQuery(connString, "USP_DeleteUOM", arParms)


        '        Return True
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        Public Function GetUOMConversion() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUOMConversion", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageUOMConversion() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcUnitConversion", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strUnitConversion

                arParms(2) = New Npgsql.NpgsqlParameter("@bitEnableItemLevelUOM", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _bitEnableItemLevelUOM

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageUOMConversion", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemUOM() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@bUnitAll", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _UOMAll

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetItemUOM", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemUOMConversion() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numUOMId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _UnitId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetItemUOMConversion", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetBizDocType() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@BizDocType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _BizDocType

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetBizDocType", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetListDetail() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ListID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_getlistDataType", arParms).Tables(0)


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Shared Sub AddDivisionCookieToBrowser(ByVal lnDivID As Long)
            Try
                'add cookie to browser of Division id which can be used to track future visit of same user
                Dim CookieEnabled As Boolean
                CookieEnabled = HttpContext.Current.Request.Browser.Cookies
                If CookieEnabled = True Then
                    If Not HttpContext.Current.Request.Cookies("DivID") Is Nothing Then
                        HttpContext.Current.Response.Cookies("DivID").Value = lnDivID.ToString
                    Else
                        Dim cookie As New HttpCookie("DivID", lnDivID.ToString)
                        cookie.Expires = DateAdd(DateInterval.Year, 1, Now)
                        HttpContext.Current.Response.Cookies.Set(cookie)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub AddColumnsToDataTable(ByRef dt As DataTable, ByVal strCommaSeperatedColumns As String)
            Try
                If strCommaSeperatedColumns.Length > 1 Then
                    Dim strColumns() As String = strCommaSeperatedColumns.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)
                    For i As Integer = 0 To strColumns.Length - 1
                        If dt.Columns.Contains(strColumns(i)) = False Then
                            dt.Columns.Add(strColumns(i))
                        End If

                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Function GetUOMConversionAllUnit() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUOMId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _UnitId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUOMConversionAllUnit", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetListModule() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetListModule", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' Updates database table field based on predefined Mode value
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function UpdateSingleFieldValue() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _Mode

                arParms(1) = New Npgsql.NpgsqlParameter("@numUpdateRecordID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _UpdateRecordID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUpdateValueID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _UpdateValueID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcText", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _Comments

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return CBool(SqlDAL.ExecuteScalar(connString, "USP_UpdateSingleFieldValue", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' Get database table field based on predefined Mode value
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetSingleFieldValue() As Object
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@vcInput", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _str

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "USP_GetScalerValue", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocsWithBuggyJournals() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocsWithBuggyJournals", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOrderWithoutJournals() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOrderWithoutJournals", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetInLineEditPropertyName() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormFieldId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _FormFieldId

                arParms(3) = New Npgsql.NpgsqlParameter("@bitCustom", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _CustomField

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_InLineEditPropertyName", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveInLineEditData() As Object
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormFieldId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _FormFieldId

                arParms(3) = New Npgsql.NpgsqlParameter("@bitCustom", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _CustomField

                arParms(4) = New Npgsql.NpgsqlParameter("@InlineEditValue", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = _InlineEditValue

                arParms(5) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _ContactID

                arParms(6) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _DivisionID

                arParms(7) = New Npgsql.NpgsqlParameter("@numWOId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _numWOId

                arParms(8) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _ProID

                arParms(9) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _OppID

                arParms(10) = New Npgsql.NpgsqlParameter("@numOppItemId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = OppItemID

                arParms(11) = New Npgsql.NpgsqlParameter("@numRecId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _RecordId

                arParms(12) = New Npgsql.NpgsqlParameter("@vcPageName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(12).Value = Me.PageName

                arParms(13) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _CaseID

                arParms(14) = New Npgsql.NpgsqlParameter("@numWODetailId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _numWODetailId

                arParms(15) = New Npgsql.NpgsqlParameter("@numCommID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _CommID

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "USP_InLineEditDataSave", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPrimaryListItemID() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormFieldId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _FormFieldId

                arParms(3) = New Npgsql.NpgsqlParameter("@numPrimaryListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ListID

                arParms(4) = New Npgsql.NpgsqlParameter("@numRecordID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _RecordId

                arParms(5) = New Npgsql.NpgsqlParameter("@bitCustomField", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _CustomField

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_GetPrimaryListItemID", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AddVisiteddetails() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRecordID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RecordId

                arParms(1) = New Npgsql.NpgsqlParameter("@chrRecordType", NpgsqlTypes.NpgsqlDbType.Char, 2)
                arParms(1).Value = _charModule

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_AddVisitedDetails", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SaveGridColumnWidth() As Object
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _str

                Return SqlDAL.ExecuteScalar(connString, "USP_SaveGridColumnWidth", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveAdvanceSearchGridColumnWidth(ByVal numViewID As Int16) As Object
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _str

                arParms(3) = New Npgsql.NpgsqlParameter("@numViewID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = numViewID

                Return SqlDAL.ExecuteScalar(connString, "USP_SaveAdvanceSearchGridColumnWidth", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function FindControlRecursive(ByVal Root As Control, ByVal Id As String) As Control
            If Root.ID = Id Then

                Return Root
            End If

            For Each Ctl As Control In Root.Controls


                Dim FoundCtl As Control = FindControlRecursive(Ctl, Id)

                If FoundCtl IsNot Nothing Then

                    Return FoundCtl

                End If
            Next

            Return Nothing
        End Function

        Public Shared Function SerializeToXML(Of T)(ByVal source As List(Of T)) As String
            Dim serializer As New XmlSerializer(source.[GetType]())

            Dim _StringWriter As New StringWriter()
            Dim _XmlTextWriter As New XmlTextWriter(_StringWriter)

            'Setting for xml
            Dim settings As New XmlWriterSettings()
            settings.Indent = True
            settings.OmitXmlDeclaration = True

            Dim _XmlWriter As XmlWriter = XmlWriter.Create(_StringWriter, settings)
            Dim arr() As XmlQualifiedName = {XmlQualifiedName.Empty}

            'Remove Qualifier feolds from nodes
            Dim emptyNs As New XmlSerializerNamespaces(arr)

            serializer.Serialize(_XmlWriter, source, emptyNs)

            Return _StringWriter.ToString()
        End Function

        Public Function GetOpportunitySource() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetOpportunitySource", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPartnerSource() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetPartnerSource", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDropShip() As DataTable
            Try
                Dim dtData As New DataTable
                dtData.Columns.Add("Value")
                dtData.Columns.Add("Text")

                Dim dr1 As DataRow
                dr1 = dtData.NewRow
                dr1("Value") = "0"
                dr1("Text") = "Select One"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "1"
                dr1("Text") = "Not Available"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "2"
                dr1("Text") = "Blank Available"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "3"
                dr1("Text") = "Vendor Label"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "4"
                dr1("Text") = "Private Label"
                dtData.Rows.Add(dr1)
                Return dtData

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPartnerContacts() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = RecordId

                arParms(2) = New Npgsql.NpgsqlParameter("@DivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetPartnerContacts", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetDivPartnerContacts() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = RecordId

                arParms(2) = New Npgsql.NpgsqlParameter("@DivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetDivPartnerContacts", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetPartnerAllContacts() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetPartnerAllContacts", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDivPartnerAllContacts() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetDivPartnerAllContacts", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetInventoryStatus() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetInventoryStatus", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPageNavigationAuthorizationDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numGroupID", _lngGroupID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numTabID", _lngTabId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_GetPageNavigationAuthorizationDetails", sqlParams.ToArray()).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTreeNodesForRelationship(ByVal bitVisibleOnly As Boolean) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numGroupID", _lngGroupID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numTabID", TabId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitVisibleOnly", bitVisibleOnly, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTreeNodesForRelationship", sqlParams.ToArray()).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManagePageNavigationAuthorization() As Integer
            Dim intResult As Integer = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numGroupID", _lngGroupID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numTabID", _lngTabId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@strItems", _xmlStr, NpgsqlTypes.NpgsqlDbType.VarChar))

                End With

                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_ManagePageNavigationAuthorization", sqlParams.ToArray())

            Catch ex As Exception
                intResult = -1
                Throw ex
            End Try

            Return intResult
        End Function

        Public Shared Sub ManageGridAsPerConfig(ByRef dgName As DataGrid)
            Try

                For Each dgCol As DataGridColumn In dgName.Columns
                    If dgCol.HeaderText.Contains("CLS") = True Then
                        dgCol.Visible = IIf(CCommon.ToInteger(HttpContext.Current.Session("DefaultClassType")) > 0, True, False)

                    ElseIf dgCol.HeaderText.Contains("PRJT") = True Then
                        dgCol.Visible = CCommon.ToBool(HttpContext.Current.Session("IsEnableProjectTracking"))

                    ElseIf dgCol.HeaderText.Contains("CMPGN") = True Then
                        dgCol.Visible = CCommon.ToBool(HttpContext.Current.Session("IsEnableCampaignTracking"))

                    End If
                Next

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ManageGridAsPerConfig(ByRef gvName As GridView)
            Try

                'Dim objUserAccess As New UserAccess
                'objUserAccess.DomainID = Session
                'Dim dtTable As DataTable = objUserAccess.GetDomainDetails()

                For intCol As Integer = 0 To gvName.HeaderRow.Cells.Count - 1
                    If gvName.HeaderRow.Cells(intCol).Text.Contains("CLS") Then
                        gvName.Columns(intCol).Visible = IIf(CCommon.ToInteger(HttpContext.Current.Session("DefaultClassType")) > 0, True, False)
                    End If

                    If gvName.HeaderRow.Cells(intCol).Text.Contains("PRJT") Then
                        gvName.Columns(intCol).Visible = CCommon.ToBool(HttpContext.Current.Session("IsEnableProjectTracking"))
                    End If

                    If gvName.HeaderRow.Cells(intCol).Text.Contains("CMPGN") Then
                        gvName.Columns(intCol).Visible = CCommon.ToBool(HttpContext.Current.Session("IsEnableCampaignTracking"))
                    End If
                Next

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetOpportunityMasterTaxItems(ByVal lngOppId As Long, ByVal lngOppItemID As Long, intMode As Short) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numOppId", lngOppId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", lngOppItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@tintMode", intMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_GetOpportunityMasterTaxItems", sqlParams.ToArray()).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function SendPushNotification(Message As String, Optional ByVal token As String = "2IorXCNP7ntiTVBTbitlqaTpD89kGj")
            Try
                Dim parameters = New Specialized.NameValueCollection() From {{"token", token}, {"user", "8x6XvbOVPpOajfqH5fACHi6AgcYfjP"}, {"message", Message}}

                Using client = New Net.WebClient()
                    client.UploadValues("https://api.pushover.net/1/messages.json", parameters)
                End Using
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetFileExtention(ByVal fileName As String, ByVal includeDot As Boolean) As String
            Try
                Dim fileExtension As String = ""
                Dim flength As Integer = fileName.Length
                Dim fdot As Integer = fileName.LastIndexOf("."c)
                If fdot <> flength And fdot > 0 And flength > 0 Then
                    If includeDot Then
                        fileExtension = fileName.Substring(fdot, flength - fdot)
                    Else
                        fileExtension = fileName.Substring(fdot + 1, flength - fdot - 1)
                    End If
                End If
                Return fileExtension
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetDocumentImage(ByVal DocumentName As String) As String
            Try
                Dim strExtention As String
                strExtention = CCommon.GetFileExtention(DocumentName, False)

                If strExtention = "pdf" Then
                    Return "Pdf.jpg"
                ElseIf strExtention = "txt" Then
                    Return "Txt.jpg"
                ElseIf strExtention = "docx" Then
                    Return "Word.jpg"
                ElseIf strExtention = "ppt" Then
                    Return "Ppt.jpg"
                ElseIf strExtention = "xlsx" Then
                    Return "Excel.jpg"
                ElseIf strExtention = "rar" Or strExtention = "zip" Then
                    Return "Rar.jpg"
                End If

                Return ""

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function CheckForImage(ByVal DocumentName As String) As Boolean
            Try
                Dim strExtention As String
                strExtention = CCommon.GetFileExtention(DocumentName, False)

                If strExtention <> "" Then
                    If strExtention.ToLower() = "jpg" Or strExtention.ToLower() = "jpeg" Or strExtention.ToLower() = "png" Or strExtention.ToLower() = "gif" Then
                        Return True
                    End If
                End If

                Return False

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Function GetDropDownValue(numListID, vcListItemType, vcDbColumnName) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numListID", numListID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@vcListItemType", vcListItemType, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@vcDbColumnName", vcDbColumnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetDropDownValue", objParam, True)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetItemPriceAndDiscount(ByVal ItemCode As Long, ByVal WareHouseItemID As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numWareHouseItemID", WareHouseItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetPriceLevelAndDiscount", objParam, True)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by sandeep for item search new sales order form
        'Start
        Shared Sub InitializeOrganizationClientSideTemplate(ByVal lngDomainID As Long, ByVal lngUserContactID As Long, ByVal radCmbCompany As RadComboBox)
            Try
                Dim objContact As BACRM.BusinessLogic.Contacts.CContacts
                objContact = New BACRM.BusinessLogic.Contacts.CContacts
                objContact.DomainID = lngDomainID
                objContact.FormId = 96
                objContact.UserCntID = 0

                Dim ds As DataSet = objContact.GetColumnConfiguration

                Dim dtTable As New DataTable
                dtTable = ds.Tables(1)

                If dtTable.Rows.Count = 0 Then
                    Dim drNew As DataRow
                    drNew = dtTable.NewRow()
                    drNew("numFieldID") = 97
                    drNew("vcFieldName") = "Organization Name"
                    drNew("vcDbColumnName") = "vcCompanyName"
                    drNew("Custom") = 0
                    drNew("tintOrder") = 0
                    dtTable.Rows.InsertAt(drNew, 0)
                End If

                'genrate header template dynamically
                radCmbCompany.HeaderTemplate = New RadComboBoxTemplate(ListItemType.Header, dtTable, radCmbCompany.DropDownWidth.Value)
                'generate Clientside Template dynamically
                Dim sb As New System.Text.StringBuilder
                Dim strWidth As String = CCommon.ToInteger(((radCmbCompany.DropDownWidth.Value - 50) / dtTable.Rows.Count)).ToString()

                sb.AppendLine("var cb;")
                sb.AppendLine(" function cb_onDataBoundOrganization(pComboboxItem, pDropDownLine) {")
                sb.AppendLine("     var customAttributes = pComboboxItem.get_attributes();")
                sb.AppendLine("if(customAttributes.get_count() == 0)return;")
                sb.AppendLine("            pDropDownLine.innerHTML = ")
                sb.AppendLine("''")

                Dim preText As String = " + '<span style=""width:" & strWidth & "px; display:inline-block;word-wrap:break-word;"">' "
                Dim postText As String = " + '</span>'"
                For Each dr As DataRow In dtTable.Rows
                    If dr("vcDbColumnName").ToString = "vcCompanyName" Then
                        sb.AppendLine(preText + " + customAttributes.getAttribute(""" & dr("vcDbColumnName").ToString & """)" + postText)
                    Else
                        sb.AppendLine(preText + " + customAttributes.getAttribute(""" & dr("vcDbColumnName").ToString & """)" + postText)
                    End If
                Next
                sb.AppendLine(";")
                sb.AppendLine("}")
                Dim p As Page = CType(System.Web.HttpContext.Current.Handler, Page)
                Dim Include2 As New UI.HtmlControls.HtmlGenericControl("script")
                Include2.Attributes.Add("type", "text/javascript")
                Include2.InnerHtml = sb.ToString()
                p.Header.Controls.Add(Include2)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function SearchOrganization(ByVal lngDomainId As Long, ByVal lngDivisionID As Long, ByVal isStartWithSearch As Boolean, ByVal searchText As String, Optional ByVal excludeemployer As Boolean = False) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = lngDomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = lngDivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@isStartWithSearch", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = isStartWithSearch

                arParms(3) = New Npgsql.NpgsqlParameter("@searchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = searchText

                arParms(4) = New Npgsql.NpgsqlParameter("@bitExcludeEmployer", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = excludeemployer

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_CompanyInfo_Search", arParms, True)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SearchOrganizationWithClass(ByVal lngDomainId As Long, ByVal lngDivisionID As Long, ByVal isStartWithSearch As Boolean, ByVal searchText As String, ByVal lngAccountClass As Long) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = lngDomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = lngDivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@isStartWithSearch", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(2).Value = isStartWithSearch

                arParms(3) = New Npgsql.NpgsqlParameter("@searchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = searchText

                arParms(4) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = lngAccountClass

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_CompanyInfo_SearchWithClass", arParms, True)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SearchItem(ByVal lngDomainId As Long, ByVal lngDivisionID As Long, ByVal OppType As Short, ByVal ItemName As String, ByVal pageIndex As Integer, ByVal pageSize As Integer, ByRef totalCount As Integer, ByVal warehouseId As Integer, ByVal isTransferTo As Boolean, Optional ByVal SearchOrderCustomerHistory As Short = 0, Optional IsCustomerPartSearch As Boolean = 0, Optional ByVal searchType As String = "1") As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = OppType

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = lngDomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = lngDivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@str", NpgsqlTypes.NpgsqlDbType.Varchar, 20)
                arParms(3).Value = ItemName

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintSearchOrderCustomerHistory", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = SearchOrderCustomerHistory

                arParms(6) = New Npgsql.NpgsqlParameter("@numPageIndex", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = pageIndex

                arParms(7) = New Npgsql.NpgsqlParameter("@numPageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = pageSize

                arParms(8) = New Npgsql.NpgsqlParameter("@WarehouseID", NpgsqlTypes.NpgsqlDbType.Integer)
                If warehouseId = 0 Then
                    arParms(8).Value = DBNull.Value
                Else
                    arParms(8).Value = warehouseId
                End If

                arParms(9) = New Npgsql.NpgsqlParameter("@TotalCount", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Direction = ParameterDirection.Output
                arParms(9).Value = 0

                arParms(10) = New Npgsql.NpgsqlParameter("@bitTransferTo", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = isTransferTo

                arParms(11) = New Npgsql.NpgsqlParameter("@bitCustomerPartSearch", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = IsCustomerPartSearch

                arParms(12) = New Npgsql.NpgsqlParameter("p_vcSearchType", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(12).Value = searchType

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_Item_SearchForSalesOrder", objParam, True)
                totalCount = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(9).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteUnit() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUOMId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _UnitId
                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteUOM", arParms)


                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End

        'Added by:Sachin Sadhu ||Date:22ndAug2014
        'Purpose : To be used in WFA service-generate bizdoc
        Public Function GetDomainDateFormat(ByVal DomainID As Long) As DataSet
            Try

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(GetConnection.GetConnectionString, "USP_Domain_GetDateFormat", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'end of code
        Public Function GetEcommerceNavigationDTLs() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                'cmd.Parameters.AddWithValue("@numModuleID", 13)
                'cmd.Parameters.AddWithValue("@numDomainID", 1)
                'cmd.Parameters.AddWithValue("@numGroupID", 2)
                'cmd.Parameters.AddWithValue("@numParentID", 73)
                'cmd.Parameters.AddWithValue("@Mode", 0)

                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ModuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _lngGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@numParentID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _lngParentID

                arParms(4) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _iMode

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_Ecommerce_PageNavigation", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetDecimalFormatService(ByVal value As Object, ByVal strDecimalPoint As String) As String
            Dim DecimalPoint As Short
            If strDecimalPoint Is Nothing Then
                DecimalPoint = 2
            Else
                DecimalPoint = CType(strDecimalPoint, Short)
            End If
            Select Case DecimalPoint
                Case 1
                    Return String.Format("{0:#,##0.0}", value)
                Case 2
                    Return String.Format("{0:#,##0.00}", value)
                Case 3
                    Return String.Format("{0:#,##0.000}", value)
                Case 4
                    Return String.Format("{0:#,##0.0000}", value)
                Case 5
                    Return String.Format("{0:#,##0.00000}", value)
                Case Else
                    Return String.Format("{0:#,##0.00}", value)
            End Select

        End Function
        Public Shared Function GetDataFormatStringService(ByVal strDecimalPoint As String) As String
            Dim DecimalPoint As Short
            If strDecimalPoint Is Nothing Then
                DecimalPoint = 2
            Else
                DecimalPoint = CType(strDecimalPoint, Short)
            End If
            Select Case DecimalPoint
                Case 1
                    Return "{0:#,##0.0}"
                Case 2
                    Return "{0:#,##0.00}"
                Case 3
                    Return "{0:#,##0.000}"
                Case 4
                    Return "{0:#,##0.0000}"
                Case 5
                    Return "{0:#,##0.00000}"
                Case Else
                    Return "{0:#,##0.00}"
            End Select

        End Function

        Public Function GetBizDocsByOrderType(ByVal oppType As Int16, ByVal oppStatus As Int16) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ListID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = oppType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintOppStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = oppStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetBizDocsByOrderType", arParms).Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Shared Function ConvertDataTabletoJSONString(ByVal dt As DataTable) As String
            Dim strResult As String = ""

            Try
                Dim serializer As JavaScriptSerializer = New JavaScriptSerializer()
                Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
                Dim row As Dictionary(Of String, Object)

                For Each dr As DataRow In dt.Rows
                    row = New Dictionary(Of String, Object)
                    For Each dCol As DataColumn In dt.Columns
                        row.Add(dCol.ColumnName, dr(dCol))
                    Next
                    rows.Add(row)
                Next

                strResult = serializer.Serialize(rows)
            Catch ex As Exception
                strResult = Nothing
                Throw ex
            End Try

            Return strResult
        End Function

        Public Function GetSimpleSearchResult(ByVal searchText As String, ByVal searchType As String, ByVal searchCriteria As String, ByVal orderType As String, ByVal bizDocType As Integer, ByVal skip As Integer, ByVal isStartWithSearch As Integer) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@searchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = searchText

                arParms(3) = New Npgsql.NpgsqlParameter("@searchType", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = searchType

                arParms(4) = New Npgsql.NpgsqlParameter("@searchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = searchCriteria

                arParms(5) = New Npgsql.NpgsqlParameter("@orderType", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = orderType

                arParms(6) = New Npgsql.NpgsqlParameter("@bizDocType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = bizDocType

                arParms(7) = New Npgsql.NpgsqlParameter("@numSkip", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = skip

                arParms(8) = New Npgsql.NpgsqlParameter("@isStartWithSearch", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = isStartWithSearch

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_SimpleSearch", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetDomainIDName() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Domain_GetIDName", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetAccountingClass() As Long
            Try
                Dim ingAccountClassID As Long = 0
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ingAccountClassID = CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_GetAccountClass", sqlParams.ToArray()))

                Return ingAccountClassID
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetEmployer() As DataTable
            Try
                Dim ingAccountClassID As Long = 0
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetEmployer", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetNamedPriceLevel() As DataTable
            Try
                Dim ingAccountClassID As Long = 0
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetNamedPriceLevel", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetNavigation() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = GroupID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(GetConnection.GetConnectionString, "USP_GetNavigation", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SearchOrders(ByVal searchText As String, ByVal tintOppType As Int16, ByVal tintUserRightType As Int16, ByVal tintMode As Int16, ByVal tintShipped As Int16, ByVal tintOppStatus As Int16) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@searchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = searchText

                arParms(3) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = tintOppType

                arParms(4) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = tintUserRightType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = tintMode '0:column to display, 1: search result

                arParms(6) = New Npgsql.NpgsqlParameter("@tintShipped", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = tintShipped '0:Open, 1:Closed, 2:All

                arParms(7) = New Npgsql.NpgsqlParameter("@tintOppStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = tintOppStatus

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_SearchOrders", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function CalculateItemProfit(ByVal Qty As Decimal, ByVal price As Decimal, ByVal cost As Decimal, ByVal vendor As Long, ByVal item As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OppID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUnitHour", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = Qty

                arParms(3) = New Npgsql.NpgsqlParameter("@monPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Value = price

                arParms(4) = New Npgsql.NpgsqlParameter("@numCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = cost

                arParms(5) = New Npgsql.NpgsqlParameter("@numVendorID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = vendor

                arParms(6) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = item

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_CalculateItemProfit", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetRecordByLinkingID(ByVal intImportType As Integer, ByVal tintItemLinkingID As Short, ByVal vcValue As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@intImportType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = intImportType

                arParms(2) = New Npgsql.NpgsqlParameter("@tintLinkingID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = tintItemLinkingID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcValue", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = vcValue

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetRecordByLinkingID", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SearchSalesOrder(ByVal searchText As String) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcOppName", searchText, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityMaster_SearchSalesOrder", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetDefaultFieldForParentChildRelationship(ByVal tintModule As Short) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@tintModule", tintModule, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_ParentChildCustomFieldMap_GetDynamicFields", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function FetchFollowUpCampaignDetails(ByVal lngDomainID As Long, ByVal numContactId As Long) As DataSet
            Try
                Dim Connection As New Npgsql.NpgsqlConnection(GetConnection.GetConnectionString)
                Dim arParams() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParams(0) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(0).Value = numContactId

                arParams(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(1).Value = lngDomainID

                arParams(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParams(2).Value = Nothing
                arParams(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(Connection, "USP_GetAutomatedFollowups", arParams)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCustProjectManagerEmailId(ByVal lngDomainID As Long, ByVal numProId As Long) As String
            Try
                Dim Connection As New Npgsql.NpgsqlConnection(GetConnection.GetConnectionString)
                Dim arParams() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParams(0) = New Npgsql.NpgsqlParameter("@ProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(0).Value = numProId

                arParams(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(1).Value = lngDomainID

                arParams(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParams(2).Value = Nothing
                arParams(2).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(Connection, "USP_GetCustProjectManager", arParams))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SearchListDetails(ByVal searchText As String, ByVal offset As Long, ByVal pageSize As Long) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ListID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSearchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = searchText

                arParms(3) = New Npgsql.NpgsqlParameter("@intOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = offset

                arParms(4) = New Npgsql.NpgsqlParameter("@intPageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = pageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@intTotalRecords", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.Output

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_ListDetails_Search", objParam, True)
                TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function GetCommaSeparatedOrderedShipped(OrderedShippedValue As String) As String
            Try
                Dim strText As String = OrderedShippedValue
                Dim strArr() As String
                strArr = Split(strText, "/", 2)
                Dim s1 As String = strArr(0)
                Dim s2 As String = strArr(1)
                Dim i As Integer = s1.LastIndexOf(">") + 1
                Dim j As Integer = s1.Length

                Dim strOrdered As String = String.Format("{0:#,##0}", CCommon.ToDouble(s1.Substring(i, j - i)))
                Dim j1 As Integer = s2.IndexOf("<") - 1
                Dim strShipped As String = String.Format("{0:#,##0}", CCommon.ToDouble(s2.Substring(1, j1)))

                s1 = s1.Replace(s1.Substring(i, j - i), strOrdered)
                s2 = s2.Replace(s2.Substring(1, j1), strShipped)

                Return String.Join("/", strArr)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetEDICustomFields(ByVal GrpId As Integer, ByVal RecID As Long) As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@Grp_id", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = GrpId

                arParms(2) = New Npgsql.NpgsqlParameter("@RecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = RecID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetEDICustomFields", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateEDICustomFields(ByVal GrpId As Integer, ByVal RecID As Long, ByVal Fld_id As Long, ByVal Fld_Value As String, ByVal FldDTLID As Long, ByVal numModifiedBy As Long, ByVal bintModifiedBy As String) As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@Grp_id", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = GrpId

                arParms(1) = New Npgsql.NpgsqlParameter("@RecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = RecID

                arParms(2) = New Npgsql.NpgsqlParameter("@Fld_id", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = Fld_id

                arParms(3) = New Npgsql.NpgsqlParameter("@Fld_Value", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(3).Value = Fld_Value

                arParms(4) = New Npgsql.NpgsqlParameter("@FldDTLID", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(4).Value = FldDTLID

                arParms(5) = New Npgsql.NpgsqlParameter("@numModifiedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = numModifiedBy

                arParms(6) = New Npgsql.NpgsqlParameter("@bintModifiedBy", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(6).Value = bintModifiedBy

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_UpdateEDICustomFields", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDomainSettingValue(ByVal vcDbColumnName As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcDbColumnName", vcDbColumnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Domain_GetColumnValue", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetBizDocIdsForOrderStatus(ByVal numOppId As Long) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numOppId", numOppId, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetBizDocIdsForOrderStatus", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetAdvancedSearchModuleAndFields() As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_AdvancedSearchGetModuleAndFields", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetAdvancedSearchListDetails(ByVal numListID As Long, ByVal vcListItemType As String, ByVal vcSearchText As String, ByVal numPageIndex As Integer, ByVal numPageSize As Integer) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numListID", numListID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItemType", vcListItemType, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSearchText", vcSearchText, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@intOffset", numPageIndex, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@intPageSize", numPageSize, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_AdvancedSearch_GetListDetails", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function WFDateFieldsDataSync()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                SqlDAL.ExecuteNonQuery(connString, "USP_WFDateFieldsDataSync", Nothing)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetEmployeesWithCapacityLoad(ByVal workOrderID As Long, ByVal dateRange As Short, ByVal fromDate As DateTime) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWorkOrderID", workOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintDateRange", dateRange, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", fromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetEmployeesWithCapacityLoad", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetTeamsWithCapacityLoad(ByVal workOrderID As Long, ByVal dateRange As Short, ByVal fromDate As DateTime) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWorkOrderID", workOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintDateRange", dateRange, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", fromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetTeamsWithCapacityLoad", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetEmployees() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetEmployee", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetARStatementRecipient(ByVal vcDivIDs As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcDivIDs", vcDivIDs, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetARStatementRecipient", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetCustomerARStatement(ByVal divisionID As Long, ByVal toDate As DateTime) As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", divisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtToDate", toDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCustomerARStatementHtml", sqlParams.ToArray())
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Function
        Public Function GetOtherContactsFromOrganization() As String
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintMode", Mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcContactIds", Str, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_GetOtherContactsFromOrganization", sqlParams.ToArray()))
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Function

        Public Function GetOpenOrdersAddBill() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetOpenOrdersAddBill", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetOrderExpenseItemsAddBill() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", OppItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetOrderExpenseItemsAddBill", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetStateNCountryIds(ByVal vcCountry As String, ByVal vcState As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@vcState", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = vcState

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCountryName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = vcCountry

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetStateCountryIDs", arParms)
            Catch ex As Exception
                Throw
            End Try

        End Function
    End Class
End Namespace

