﻿Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI

Public Class RadComboBoxTemplate
    Implements ITemplate

    Dim TemplateType As ListItemType
    Dim dtTable1 As DataTable
    Dim Field1 As String
    Dim i As Integer = 0
    Dim _DropDownWidth As Double

    Sub New(ByVal type As ListItemType, ByVal dtTable As DataTable, Optional ByVal DropDownWidth As Double = 500)
        Try
            TemplateType = type
            dtTable1 = dtTable
            _DropDownWidth = DropDownWidth
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
        i = 0
        Dim label1 As Label
        Dim table1 As New HtmlTable
        Dim tblCell As HtmlTableCell
        Dim tblRow As HtmlTableRow
        table1.Width = "100%"
        Select Case TemplateType
            Case ListItemType.Header
                'tblRow = New HtmlTableRow
                For Each dr As DataRow In dtTable1.Rows
                    'tblCell = New HtmlTableCell
                    'tblCell.Width = 100 / dtTable1.Rows.Count & "%"
                    label1 = New Label
                    label1.Text = dr("vcFieldName").ToString
                    label1.CssClass = "column"
                    label1.Width = Unit.Pixel(CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count))
                    'label1.Width = 100 / dtTable1.Rows.Count & "%"
                    'Field1 = dr("vcDbColumnName")
                    'AddHandler label1.DataBinding, AddressOf label1_DataBinding
                    'tblCell.Controls.Add(label1)
                    'tblRow.Cells.Add(tblCell)
                    container.Controls.Add(label1)
                Next
                'table1.Rows.Add(tblRow)
                container.Controls.Add(table1)
            Case ListItemType.Item
                tblRow = New HtmlTableRow
                For Each dr As DataRow In dtTable1.Rows
                    tblCell = New HtmlTableCell
                    tblCell.Width = 100 / dtTable1.Rows.Count & "%"
                    label1 = New Label
                    label1.CssClass = "normal1"
                    'label1.Text = dr("vcFormFieldName")
                    Field1 = dr("vcDbColumnName").ToString
                    'label1.Text = "<%#DataBinder.Eval(Container.DataItem, """ & dr("vcDbColumnName") & """)%>"
                    AddHandler label1.DataBinding, AddressOf label1_DataBinding
                    tblCell.Controls.Add(label1)
                    tblRow.Cells.Add(tblCell)
                Next
                table1.Rows.Add(tblRow)
                container.Controls.Add(table1)
        End Select

    End Sub

    Private Sub label1_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
        Dim target As Label = CType(sender, Label)
        Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
        Dim itemText As String = CType(DataBinder.Eval(item.DataItem, "Attributes[""" & dtTable1.Rows(i).Item("vcDbColumnName").ToString & """]"), String)
        target.Text = itemText
        i = i + 1
    End Sub
End Class
