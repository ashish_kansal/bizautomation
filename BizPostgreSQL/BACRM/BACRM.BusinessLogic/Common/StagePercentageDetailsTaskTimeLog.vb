﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic

Namespace BACRM.BusinessLogic.Common

    Public Class StagePercentageDetailsTaskTimeLog
        Inherits CBusinessBase

#Region "Public Properties"

        Public Property ID As Long
        Public Property TaskID As Long
        Public Property bitForProject As Boolean
        Public Property ActionType As Short
        Public Property ActionTime As DateTime
        Public Property ProcessedQty As Double
        Public Property ReasonForPause As Long
        Public Property Notes As String
        Public Property ClientTimeZoneOffset As Integer
        Public Property IsManualEntry As Boolean
        Public Property TotalTimeSpendOnTask As String

#End Region

#Region "Public Methods"

        Public Function Save() As Long
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@ID", ID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTaskID", TaskID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintAction", ActionType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@dtActionTime", ActionTime, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@numProcessedQty", ProcessedQty, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numReasonForPause", ReasonForPause, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcNotes", Notes, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitManualEntry", IsManualEntry, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_StagePercentageDetailsTaskTimeLog_Save", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetByTaskID() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTaskID", TaskID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@bitForProject", bitForProject, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcTotalTimeSpendOnTask", TotalTimeSpendOnTask, NpgsqlTypes.NpgsqlDbType.Varchar, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_StagePercentageDetailsTaskTimeLog_GetByTaskID", objParam, True)

                TotalTimeSpendOnTask = Convert.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    Return ds.Tables(0)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub Delete()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ID", ID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTaskID", TaskID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_StagePercentageDetailsTaskTimeLog_Delete", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

    End Class

End Namespace


