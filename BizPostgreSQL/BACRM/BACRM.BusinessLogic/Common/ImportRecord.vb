'**********************************************************************************
' <CImportRecord.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************

Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Common

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CCommon
    ' 
    ' Description  : This is the business logic classe for Common Stuff
    '**********************************************************************************
    Public Class CImportRecord
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Private _ConnString As String
        'Private DomainId As Integer
        Private _UserID As Integer
        Private _State As String
        Private _Country As String
        Private _AnnualRevenue As String
        Private _Employees As Long
        Private _DivisionID As Integer
        Private _ContactID As Integer

        Public Property ConnString() As String
            Get
                Return _ConnString
            End Get
            Set(ByVal Value As String)
                _ConnString = Value
            End Set
        End Property

        'Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Integer)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property UserID() As Integer
            Get
                Return _UserID
            End Get
            Set(ByVal Value As Integer)
                _UserID = Value
            End Set
        End Property

        Public Property State() As String
            Get
                Return _State
            End Get
            Set(ByVal Value As String)
                _State = Value
            End Set
        End Property

        Public Property Country() As String
            Get
                Return _Country
            End Get
            Set(ByVal Value As String)
                _Country = Value
            End Set
        End Property

        Public Property AnnualRevenue() As String
            Get
                Return _AnnualRevenue
            End Get
            Set(ByVal Value As String)
                _AnnualRevenue = Value
            End Set
        End Property

        Public Property Employees() As Long
            Get
                Return _Employees
            End Get
            Set(ByVal Value As Long)
                _Employees = Value
            End Set
        End Property

        Public Property DivisionID() As Integer
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Integer)
                _DivisionID = Value
            End Set
        End Property

        Public Property ContactID() As Integer
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Integer)
                _ContactID = Value
            End Set
        End Property


        Public Function fn_SaveData(ByVal strQuery As String) As String
            'Declare the SQL COnnection Object
            Dim cnnSaveData As New Npgsql.NpgSqlConnection(_ConnString)
            'Declare a command object for saving the data.
            Dim cmdSaveData As New Npgsql.NpgsqlCommand(strQuery, cnnSaveData)
            'OPen the Connection.
            cnnSaveData.Open()
            'Execute teh command.
            fn_SaveData = CStr(cmdSaveData.ExecuteScalar)
            'Dispose the command
            cmdSaveData.Dispose()
            'Close the connection
            cnnSaveData.Close()
            'Release the Objects
            cmdSaveData = Nothing
            cnnSaveData = Nothing
        End Function
    End Class
End Namespace

