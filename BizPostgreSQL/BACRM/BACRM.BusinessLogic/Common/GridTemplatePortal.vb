﻿Imports BACRM.BusinessLogic.Common
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Drawing
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.ShioppingCart

Public Class GridTemplatePortal
    Implements ITemplate

    Dim TemplateType As ListItemType
    Dim ColumnName, FieldName, DBColumnName, OrigDBColumnName, ControlType, LookBackTableName, tableAlias, ListType, SortColumnName, SortColumnOrder, FieldDataType As String
    Dim Custom, AllowEdit, AllowSorting, AllowFiltering As Boolean
    Dim EditPermission, ColumnWidth As Integer
    Dim ListID, ListRelID, FormID, FormFieldId As Long
    Dim htGridColumnSearch As Hashtable
    Dim objCommon As New CCommon

    Sub New(ByVal type As ListItemType, ByVal drRow As DataRow, ByVal EPermission As Integer, GridColumnSearch As Hashtable, numFormID As Long, Optional ByVal vcSortColumnName As String = "", Optional ByVal vcSortColumnOrder As String = "")
        Try
            TemplateType = type

            FieldName = drRow("vcFieldName").ToString
            DBColumnName = drRow("vcDbColumnName").ToString
            OrigDBColumnName = drRow("vcOrigDbColumnName").ToString
            AllowSorting = CCommon.ToBool(drRow("bitAllowSorting"))
            FormFieldId = CCommon.ToLong(drRow("numFieldId"))
            AllowEdit = CCommon.ToBool(drRow("bitAllowEdit"))
            Custom = CCommon.ToBool(drRow("bitCustomField"))
            ControlType = drRow("vcAssociatedControlType").ToString
            ListType = drRow("vcListItemType").ToString
            ListID = CCommon.ToLong(drRow("numListID"))
            ListRelID = CCommon.ToLong(drRow("ListRelID"))
            ColumnWidth = CCommon.ToInteger(drRow("intColumnWidth"))
            LookBackTableName = drRow("vcLookBackTableName").ToString
            AllowFiltering = CCommon.ToBool(drRow("bitAllowFiltering"))
            FieldDataType = drRow("vcFieldDataType").ToString

            If LookBackTableName = "CompanyInfo" Then
                tableAlias = "cmp"
            ElseIf LookBackTableName = "DivisionMaster" Then
                tableAlias = "DM"
            ElseIf LookBackTableName = "AdditionalContactsInformation" Then
                tableAlias = "ADC"
            ElseIf LookBackTableName = "ProjectProgress" Then
                tableAlias = "PP"
            ElseIf LookBackTableName = "ProjectsMaster" Then
                tableAlias = "Pro"
            ElseIf LookBackTableName = "Cases" Then
                tableAlias = "cs"
            ElseIf LookBackTableName = "OpportunityMaster" Then
                tableAlias = "Opp"
            ElseIf LookBackTableName = "OpportunityRecurring" Then
                tableAlias = "OPR"
            ElseIf LookBackTableName = "AddressDetails" Then
                tableAlias = "AD"
            ElseIf LookBackTableName = "OpportunityBizDocs" Then
                tableAlias = "OBD"
            ElseIf LookBackTableName = "Item" Then
                tableAlias = "I"
            ElseIf LookBackTableName = "OpportunityBizDocItems" Then
                tableAlias = "OBDI"
            ElseIf LookBackTableName = "OpportunityItems" Then
                tableAlias = "OI"
            ElseIf LookBackTableName = "WareHouseItems" Then
                tableAlias = "WHI"
            ElseIf LookBackTableName = "Warehouses" Then
                tableAlias = "WH"
            ElseIf LookBackTableName = "OpportunityLinking" Then
                tableAlias = "OL"
            ElseIf Custom = True Then
                tableAlias = "CFW"
            End If

            ColumnName = DBColumnName & "~" & FormFieldId & "~" & IIf(Custom, 1, 0).ToString

            EditPermission = EPermission
            htGridColumnSearch = GridColumnSearch
            FormID = numFormID

            SortColumnName = vcSortColumnName
            SortColumnOrder = vcSortColumnOrder
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
        Try
            Dim lbl1 As Label = New Label()
            Dim lbl2 As Label = New Label()
            Dim lbl3 As Label = New Label()
            Dim lbl4 As Label = New Label()
            Dim lnkButton As New LinkButton
            Dim lnk As New HyperLink

            Select Case TemplateType
                Case ListItemType.Header
                    Dim cell As DataControlFieldHeaderCell = CType(Container, DataControlFieldHeaderCell)
                    
                    If ControlType <> "DeleteCheckBox" Then

                        If AllowSorting = False Then
                            lbl1.ID = FormFieldId & DBColumnName
                            lbl1.Text = FieldName
                            Container.Controls.Add(lbl1)

                        ElseIf AllowSorting = True Then
                            lnkButton.ID = FormFieldId & DBColumnName
                            lnkButton.Text = FieldName
                            lnkButton.Attributes.Add("onclick", "return SortColumn('" & tableAlias & "." & OrigDBColumnName.Replace("ADC.", "") & "')")
                            Container.Controls.Add(lnkButton)

                            If tableAlias & "." & OrigDBColumnName.Replace("ADC.", "") = SortColumnName Then
                                Container.Controls.Add(New LiteralControl(String.Format("&nbsp;<font color='black'>{0}</font>", IIf(SortColumnOrder = "Desc", "&#9660;", "&#9650;"))))
                            End If
                        End If

                        'Dim txtSearch As TextBox = New TextBox()
                        'Dim ddlSearch As DropDownList = New DropDownList()
                        Container.Controls.Add(New LiteralControl("<br />"))

                    End If

                    cell.VerticalAlign = VerticalAlign.Top
                    If ColumnWidth > 0 Then
                        cell.Width = ColumnWidth
                    End If
                    cell.Attributes.Add("id", FormID & "~" & FormFieldId & "~" & Custom)

                Case ListItemType.Item

                    If DBColumnName = "intTotalProgress" Then
                        AddHandler lbl1.DataBinding, AddressOf BindProgressBar ' make progressbar html
                        Container.Controls.Add(lbl1)

                    ElseIf ControlType <> "DeleteCheckBox" Then
                        AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                        Container.Controls.Add(lbl1)

                    Else
                        AddHandler lbl1.DataBinding, AddressOf Bindvalue
                        Container.Controls.Add(lbl1)

                        If FormID = 57 Then
                            AddHandler lbl2.DataBinding, AddressOf BindvalueOppBizDocsID
                            Container.Controls.Add(lbl2)

                            AddHandler lbl3.DataBinding, AddressOf BindOppBizDocItemID
                            Container.Controls.Add(lbl3)

                            AddHandler lbl4.DataBinding, AddressOf BindnumoppitemtCode
                            Container.Controls.Add(lbl4)
                        Else
                            AddHandler lbl2.DataBinding, AddressOf BindvalueROwnr
                            AddHandler lbl3.DataBinding, AddressOf BindvalueTerr
                            Container.Controls.Add(lbl2)
                            Container.Controls.Add(lbl3)
                        End If
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub Bindvalue(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
            lbl1.ID = "lbl1"

            If FormID = 34 Or FormID = 35 Or FormID = 36 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numDivisionID").ToString
            ElseIf FormID = 10 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numContactID").ToString
            ElseIf FormID = 80 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numCaseID").ToString
            ElseIf FormID = 82 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numProId").ToString
            ElseIf FormID = 38 Or FormID = 78 Or FormID = 40 Or FormID = 79 Or FormID = 57 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numOppId").ToString
            End If
            lbl1.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindvalueOppBizDocsID(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl2 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl2.NamingContainer, GridViewRow)
            lbl2.ID = "lbl2"
            lbl2.Text = DataBinder.Eval(Container.DataItem, "numOppBizDocsID").ToString
            lbl2.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindOppBizDocItemID(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl3 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl3.NamingContainer, GridViewRow)
            lbl3.ID = "lbl3"
            lbl3.Text = DataBinder.Eval(Container.DataItem, "numOppBizDocItemID").ToString
            lbl3.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindnumoppitemtCode(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl4 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl4.NamingContainer, GridViewRow)
            lbl4.ID = "lbl4"
            lbl4.Text = DataBinder.Eval(Container.DataItem, "numoppitemtCode").ToString
            lbl4.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindvalueTerr(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl3 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl3.NamingContainer, GridViewRow)
            lbl3.ID = "lbl3"
            lbl3.Text = DataBinder.Eval(Container.DataItem, "numTerID").ToString
            lbl3.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindvalueROwnr(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl2 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl2.NamingContainer, GridViewRow)
            lbl2.ID = "lbl2"
            lbl2.Text = DataBinder.Eval(Container.DataItem, "numRecOwner").ToString
            lbl2.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindProgressBar(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
            Dim i As Integer = 0
            i = Container.RowIndex

            lbl1.Text = CType(Container.DataItem, DataRowView).Row(ColumnName) & "% <div id=""TotalProgressContainer" & i.ToString & """ class=""ProgressContainer"">" & _
                        "&nbsp;<div id=""TotalProgress" & i.ToString & """ class=""ProgressBar"" ></div></div>"

            ScriptManager.RegisterStartupScript(Container.Page, Me.GetType, "ProgressBar" & i.ToString, "progress(" & CType(Container.DataItem, DataRowView).Row(ColumnName) & ",'TotalProgressContainer" & i.ToString & "','TotalProgress" & i.ToString & "',90);", True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)

            'Change color of Row
            If DirectCast(Container.DataItem, System.Data.DataRowView).DataView.Table.Columns("vcColorScheme") IsNot Nothing Then
                If DataBinder.Eval(Container.DataItem, "vcColorScheme").ToString.Length > 0 Then
                    Container.CssClass = DataBinder.Eval(Container.DataItem, "vcColorScheme")
                End If
            End If

            If DBColumnName = "vcEmail" Or DBColumnName = "vcCompanyName" Or DBColumnName = "vcFirstName" Or DBColumnName = "vcLastName" Or DBColumnName = "vcWebSite" _
                Or DBColumnName = "numRecOwner" Or DBColumnName = "numContactId" Or DBColumnName = "vcCaseNumber" Or DBColumnName = "vcProjectName" Or DBColumnName = "numCustPrjMgr" _
                 Or DBColumnName = "vcPoppName" Or DBColumnName = "numShareWith" Or DBColumnName = "numShippingReportID" Or DBColumnName = "vcBizDocsList" Then

                lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName).ToString), "", DataBinder.Eval(Container.DataItem, ColumnName).ToString).ToString

                Dim intermediatory As Integer
                intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)

                If DBColumnName = "vcBizDocsList" Then
                    lbl1.Text = ""

                    If DataBinder.Eval(Container.DataItem, ColumnName).ToString.Length > 0 Then
                        Dim strBizDocsList() As String = DataBinder.Eval(Container.DataItem, ColumnName).Split(New String() {"$^$"}, StringSplitOptions.None)
                        Dim strIDValue() As String

                        If strBizDocsList.Length > 0 Then
                            For i As Integer = 0 To strBizDocsList.Length - 1
                                strIDValue = strBizDocsList(i).Split(New String() {"#^#"}, StringSplitOptions.None)

                                lbl1.Text += " <a  href='javascript:OpenBizInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & strIDValue(0) & ")'>" & strIDValue(1) & "</a>,"
                            Next
                        End If
                    End If

                    lbl1.Text = lbl1.Text.Trim(",")
                End If

                If AllowEdit = True Then
                    ControlClass = "click"
                End If
            Else
                If ControlType = "CheckBox" Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName).ToString), "", IIf(CCommon.ToBool(DataBinder.Eval(Container.DataItem, ColumnName)), "Yes", "No").ToString).ToString

                Else
                    If FieldDataType = "M" Then
                        lbl1.Text = ReturnMoney(DataBinder.Eval(Container.DataItem, ColumnName))
                    Else
                        lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString
                    End If

                End If

                If AllowEdit = True Then
                    Select Case ControlType
                        Case "Website", "Email", "TextBox"
                            ControlClass = "click"
                        Case "SelectBox"
                            ControlClass = "editable_select"
                        Case "TextArea"
                            ControlClass = "editable_textarea"
                        Case "CheckBox"
                            ControlClass = "editable_CheckBox"
                        Case "DateField"
                            ControlClass = ""
                    End Select

                    If DBColumnName = "monPAmount" Then
                        ControlClass = "editable_Amount"
                    End If

                    If FormID = 38 Or FormID = 40 Then
                        If DBColumnName = "numPercentageComplete" AndAlso DataBinder.Eval(Container.DataItem, "numBusinessProcessID") > 0 Then
                            ControlClass = ""
                        End If
                    End If

                ElseIf FormID = 80 AndAlso DBColumnName = "numStatus" Then
                    Select Case ControlType
                        Case "SelectBox"
                            ControlClass = "editable_select"
                    End Select

                End If
            End If

            If ControlClass.Length > 0 Then
                If AllowEdit = True And EditPermission <> 0 Then

                    Dim vcFormName As String

                    Select Case FormID
                        Case 34, 35, 36
                            vcFormName = "Organization"
                        Case 10
                            vcFormName = "Contact"
                        Case 80
                            vcFormName = "Case"
                        Case 82
                            vcFormName = "Project"
                        Case 38, 78, 40, 79, 57
                            vcFormName = "Opp"
                    End Select

                    Dim cell As DataControlFieldCell = CType(lbl1.Parent, DataControlFieldCell)
                    If FormID = 34 Or FormID = 35 Or FormID = 36 Or FormID = 10 Then
                        cell.Attributes.Add("id", vcFormName & "~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numContactID").ToString & "~" & DataBinder.Eval(Container.DataItem, "numDivisionID").ToString & "~" & DataBinder.Eval(Container.DataItem, "numTerID").ToString & "~" & ListRelID)
                    ElseIf FormID = 80 Then
                        cell.Attributes.Add("id", vcFormName & "~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numCaseID") & "~" & DataBinder.Eval(Container.DataItem, "numDivisionID") & "~" & DataBinder.Eval(Container.DataItem, "numTerID") & "~" & ListRelID)
                    ElseIf FormID = 82 Then
                        cell.Attributes.Add("id", vcFormName & "~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numProID") & "~" & DataBinder.Eval(Container.DataItem, "numDivisionID") & "~" & DataBinder.Eval(Container.DataItem, "numTerID") & "~" & ListRelID)
                    ElseIf FormID = 38 Or FormID = 78 Or FormID = 40 Or FormID = 79 Or FormID = 57 Then
                        cell.Attributes.Add("id", vcFormName & "~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numOppID") & "~" & DataBinder.Eval(Container.DataItem, "numTerID") & "~" & ListRelID)
                    End If

                    cell.Attributes.Add("class", ControlClass)

                    'cell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                    'cell.Attributes.Add("onmouseout", "bgColor=''")
                ElseIf FormID = 80 AndAlso DBColumnName = "numStatus" Then

                    Dim cell As DataControlFieldCell = CType(lbl1.Parent, DataControlFieldCell)
                    cell.Attributes.Add("id", "Case" & "~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numCaseID") & "~" & DataBinder.Eval(Container.DataItem, "numDivisionID") & "~" & DataBinder.Eval(Container.DataItem, "numTerID") & "~" & ListRelID)
                    cell.Attributes.Add("class", ControlClass)

                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function GetDropDownData() As DataTable
        Try
            Dim dtData As New DataTable

            If DBColumnName = "tintSource" AndAlso LookBackTableName = "OpportunityMaster" Then
                objCommon.DomainID = HttpContext.Current.Session("DomainID")
                dtData = objCommon.GetOpportunitySource()
            ElseIf ListID > 0 Then
                dtData = objCommon.GetMasterListItems(ListID, CCommon.ToLong(HttpContext.Current.Session("DomainID")))
            ElseIf ListType = "U" Then
                dtData = objCommon.ConEmpList(CCommon.ToLong(HttpContext.Current.Session("DomainID")), False, 0)
            ElseIf ListType = "AG" Then
                dtData = objCommon.GetGroups()
            ElseIf ListType = "C" Then
                Dim objCampaign As New BACRM.BusinessLogic.Reports.PredefinedReports
                objCampaign.byteMode = 2
                objCampaign.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                dtData = objCampaign.GetCampaign()
            ElseIf ListType = "DC" Then
                Dim objCampaign As New BACRM.BusinessLogic.Marketing.Campaign
                With objCampaign
                    .SortCharacter = "0"
                    .UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                    .PageSize = 100
                    .TotalRecords = 0
                    .DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    .columnSortOrder = "Asc"
                    .CurrentPage = 1
                    .columnName = "vcECampName"
                    dtData = objCampaign.ECampaignList
                    Dim drow As DataRow = dtData.NewRow
                    drow("numECampaignID") = -1
                    drow("vcECampName") = "-- Disengaged --"
                    dtData.Rows.Add(drow)
                End With
            ElseIf ListType = "SYS" Then
                dtData = New DataTable
                dtData.Columns.Add("Value")
                dtData.Columns.Add("Text")
                Dim dr1 As DataRow
                dr1 = dtData.NewRow
                dr1("Value") = "0"
                dr1("Text") = "Lead"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "1"
                dr1("Text") = "Prospect"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "2"
                dr1("Text") = "Account"
                dtData.Rows.Add(dr1)
            ElseIf DBColumnName = "numManagerID" Then
                objCommon.ContactID = objCommon.ContactID
                objCommon.charModule = "C"
                objCommon.GetCompanySpecificValues1()
                dtData = objCommon.GetManagers(CCommon.ToLong(HttpContext.Current.Session("DomainID")), objCommon.ContactID, objCommon.DivisionID)
            ElseIf DBColumnName = "charSex" Then
                dtData = New DataTable
                dtData.Columns.Add("Value")
                dtData.Columns.Add("Text")
                Dim dr1 As DataRow
                dr1 = dtData.NewRow
                dr1("Value") = "M"
                dr1("Text") = "Male"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "F"
                dr1("Text") = "Female"
                dtData.Rows.Add(dr1)
            ElseIf DBColumnName = "tintOppStatus" Then
                dtData = New DataTable
                dtData.Columns.Add("numListItemID")
                dtData.Columns.Add("vcData")

                Dim row As DataRow = dtData.NewRow
                row("numListItemID") = 1
                row("vcData") = "Deal Won"
                dtData.Rows.Add(row)

                row = dtData.NewRow
                row("numListItemID") = 2
                row("vcData") = "Deal Lost"
                dtData.Rows.Add(row)
                dtData.AcceptChanges()
            ElseIf ListType = "WI" Then
                dtData = New DataTable
                dtData.Columns.Add("numListItemID", System.Type.GetType("System.String"))
                dtData.Columns.Add("vcData", System.Type.GetType("System.String"))

                Dim objAPI As New WebAPI
                Dim dtWebApi As DataTable
                objAPI.Mode = 1
                dtWebApi = objAPI.GetWebApi

                Dim row As DataRow = dtData.NewRow

                For Each dr As DataRow In dtWebApi.Rows
                    row = dtData.NewRow
                    row(0) = dr("WebApiID")
                    row(1) = dr("vcProviderName")
                    dtData.Rows.Add(row)
                Next

                'If FormID = 38 Or FormID = 78 Or FormID = 40 Or FormID = 79 Then
                Dim objSite As New Sites
                Dim dtSites As DataTable
                objSite.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                dtSites = objSite.GetSites()

                For Each dr As DataRow In dtSites.Rows
                    row = dtData.NewRow
                    row(0) = "Sites~" & dr("numSiteID")
                    row(1) = dr("vcSiteName")
                    dtData.Rows.Add(row)
                Next

                'End If

                dtData.AcceptChanges()
            End If

            Return dtData
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
