﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System
Imports System.IO
Imports System.Text
Imports BACRM.BusinessLogic.Admin
Imports System.Web
'Imports System.Web.UI
Imports System.Xml

Namespace BACRM.BusinessLogic.Common

    Public Class ErrorMessage
        Inherits CBusinessBase
        Enum GetApplication As Int16
            BizAutomation = 1
            Portal = 2
            ECommerce = 3
        End Enum
        Private _ErrorID As Long
        Public Property ErrorID() As Long
            Get
                Return _ErrorID
            End Get
            Set(ByVal Value As Long)
                _ErrorID = Value
            End Set
        End Property
        Private _ErrorCode As String
        Public Property ErrorCode() As String
            Get
                Return _ErrorCode
            End Get
            Set(ByVal Value As String)
                _ErrorCode = Value
            End Set
        End Property
        Private _ErrorDesc As String
        Public Property ErrorDesc() As String
            Get
                Return _ErrorDesc
            End Get
            Set(ByVal Value As String)
                _ErrorDesc = Value
            End Set
        End Property
        Private _Application As Integer
        Public Property Application() As Integer
            Get
                Return _Application
            End Get
            Set(ByVal Value As Integer)
                _Application = Value
            End Set
        End Property
        Private _ErrorDetailId As Long
        Public Property ErrorDetailId() As Long
            Get
                Return _ErrorDetailId
            End Get
            Set(ByVal Value As Long)
                _ErrorDetailId = Value
            End Set
        End Property
        Private _SiteID As Long
        Public Property SiteID() As Long
            Get
                Return _SiteID
            End Get
            Set(ByVal Value As Long)
                _SiteID = Value
            End Set
        End Property

        Private _CultureID As Long
        Public Property CultureID() As Long
            Get
                Return _CultureID
            End Get
            Set(ByVal Value As Long)
                _CultureID = Value
            End Set
        End Property
        Private _strErrorMessages As String
        Public Property strErrorMessages() As String
            Get
                Return _strErrorMessages
            End Get
            Set(ByVal Value As String)
                _strErrorMessages = Value
            End Set
        End Property

        Public Function GetErrorMessages() As String
            Try
                Dim isErrorCodeFound As Boolean = False
                Dim errorMessage As String = ""

                If Not Directory.Exists(CCommon.GetDocumentPhysicalPath(DomainID)) Then
                    Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(DomainID))
                End If

                Dim strXmlfile As String = CCommon.GetDocumentPhysicalPath(DomainID) & "ErrorMsg" & "_" & DomainID & "_" & _SiteID & "_" & _Application & "_" & _CultureID & ".xml"
                If File.Exists(strXmlfile) Then
                    Dim xDoc As XmlDocument = New XmlDocument()
                    xDoc.Load(strXmlfile)
                    Dim xElements As XmlElement = xDoc.DocumentElement
                    Dim lstNodes As XmlNodeList = xElements.ChildNodes
                    For Each node As XmlNode In lstNodes
                        If node.Attributes("ErrorCode").Value = _ErrorCode Then
                            isErrorCodeFound = True
                            errorMessage = node.Attributes("ErrorDesc").Value
                            Exit For
                        End If
                    Next
                End If

                If Not isErrorCodeFound Then
                    Dim getconnection As New GetConnection
                    Dim connString As String = getconnection.GetConnectionString
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                    arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(0).Value = DomainID

                    arParms(1) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(1).Value = _SiteID

                    arParms(2) = New Npgsql.NpgsqlParameter("@numCultureID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(2).Value = _CultureID

                    arParms(3) = New Npgsql.NpgsqlParameter("tintApplication", NpgsqlTypes.NpgsqlDbType.SmallInt)
                    arParms(3).Value = _Application

                    arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                    arParms(4).Value = Nothing
                    arParms(4).Direction = ParameterDirection.InputOutput

                    Dim dtErrorMessage As DataTable
                    dtErrorMessage = SqlDAL.ExecuteDataset(connString, "USP_GetErrorMessages", arParms).Tables(0)

                    Dim xDoc As XmlDocument = New XmlDocument()
                    Dim root As XmlElement = xDoc.CreateElement("ErrorMessages")

                    For Each Row As DataRow In dtErrorMessage.Rows
                        Dim nm As XmlElement = xDoc.CreateElement("ErrorMessage")
                        nm.SetAttribute("ErrorCode", CCommon.ToString(Row("vcErrorCode")))
                        nm.SetAttribute("ErrorDesc", CCommon.ToString(Row("vcErrorDesc")))

                        root.AppendChild(nm)
                    Next
                    xDoc.AppendChild(root)
                    xDoc.Save(strXmlfile)
                    xDoc.Load(strXmlfile)
                    Dim xElements As XmlElement = xDoc.DocumentElement
                    Dim lstNodes As XmlNodeList = xElements.ChildNodes
                    For Each node As XmlNode In lstNodes
                        If node.Attributes("ErrorCode").Value = _ErrorCode Then
                            isErrorCodeFound = True
                            errorMessage = node.Attributes("ErrorDesc").Value
                            Exit For
                        End If
                    Next
                End If

                Return errorMessage
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAllErrorMessages() As DataTable
            Try
                Dim dtErrorMessages As DataTable = New DataTable()
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCultureID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CultureID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintApplication", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(3).Value = _Application

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                dtErrorMessages = SqlDAL.ExecuteDataset(connString, "USP_GetErrorMessages", arParms).Tables(0)
                Return dtErrorMessages
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageErrorDetails() As Boolean
            Try
                Dim dtErrorMessages As DataTable = New DataTable()
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCultureID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CultureID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintApplication", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(3).Value = _Application

                arParms(4) = New Npgsql.NpgsqlParameter("@strErrorMessages", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(4).Value = _strErrorMessages

                SqlDAL.ExecuteDataset(connString, "USP_ManageErrorDetail", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class

End Namespace
