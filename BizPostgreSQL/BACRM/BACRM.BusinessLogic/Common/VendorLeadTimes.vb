﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic

Namespace BACRM.BusinessLogic.Common
    Public Class VendorLeadTimes
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property VendorLeadTimesID As Long
        Public Property VendorID As Long
        Public Property data As String
        Public Property SelectedIDs As String

#End Region

#Region "Public Methods"

        Public Function GetByVendor() As String
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numdivisionid", VendorID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_swvrefcur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToString(SqlDAL.ExecuteScalar(connString, "usp_vendorleadtimes_getbyvendor", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetByID() As String
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numdivisionid", VendorID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendorleadtimesid", VendorLeadTimesID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("p_swvrefcur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToString(SqlDAL.ExecuteScalar(connString, "usp_vendorleadtimes_getbyid", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub Save()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendorleadtimesid", VendorLeadTimesID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("p_numdivisionid", VendorID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_vcdata", data, NpgsqlTypes.NpgsqlDbType.Jsonb))
                End With

                SqlDAL.ExecuteNonQuery(connString, "usp_vendorleadtimes_save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numdivisionid", VendorID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendorleadtimesids", SelectedIDs, NpgsqlTypes.NpgsqlDbType.Text))
                End With

                SqlDAL.ExecuteNonQuery(connString, "usp_vendorleadtimes_delete", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        'NOT COMPLETED YET
        'Public Function GetVendorLeadTime(ByVal shipVia As Long, ByVal shipService As Long, ByVal shipFromLocation As Long, ByVal itemGroup As Long, ByVal shipToCountry As Long, ByVal shipToState As Long) As Integer
        '    Try
        '        Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = GetConnection.GetConnectionString

        '        With sqlParams
        '            .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
        '            .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
        '            .Add(SqlDAL.Add_Parameter("p_numdivisionid", VendorID, NpgsqlTypes.NpgsqlDbType.Bigint))
        '            .Add(SqlDAL.Add_Parameter("p_numShipVia", shipVia, NpgsqlTypes.NpgsqlDbType.Bigint))
        '            .Add(SqlDAL.Add_Parameter("p_numdShippingService", shipService, NpgsqlTypes.NpgsqlDbType.Bigint))
        '            .Add(SqlDAL.Add_Parameter("p_numShipFromLocation", shipFromLocation, NpgsqlTypes.NpgsqlDbType.Bigint))
        '            .Add(SqlDAL.Add_Parameter("p_numItemGroup", itemGroup, NpgsqlTypes.NpgsqlDbType.Bigint))
        '            .Add(SqlDAL.Add_Parameter("p_numShipToCountry", shipToCountry, NpgsqlTypes.NpgsqlDbType.Bigint))
        '            .Add(SqlDAL.Add_Parameter("p_numShipToState", shipToState, NpgsqlTypes.NpgsqlDbType.Bigint))
        '            .Add(SqlDAL.Add_Parameter("p_swvrefcur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
        '        End With

        '        Return CCommon.ToInteger(SqlDAL.ExecuteScalar(connString, "usp_vendorleadtimes_getleadtime", sqlParams.ToArray()))
        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function
#End Region

    End Class

End Namespace

