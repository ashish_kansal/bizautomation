'**********************************************************************************
' <Enumeration.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************


Option Explicit On
Option Strict On

Imports System.String
Imports System.Enum

Namespace BACRM.BusinessLogic.Common

    'Enumeration for Page IDs which will be used within the Database in different tables.
    'Add new values as and when required keeping inmind that teh IDs are unique.
    Public Enum PAGEID
        LEAD_LIST = 1
        PROSPECT_LIST = 2
        ACCOUNT_LIST = 3
        CONTACT_LIST = 4
        OPPORTUNITY_LIST = 5
        LEAD_DETAIL = 6
        PROSPECT_DETAIL = 7
        ACCOUNT_DETAIL = 8
        CONTACT_DETAIL = 9
        OPPORTUNITY_DETAIL = 10

        IMPORT_WIZARD = 49
        TIME_AND_EXPENSE = 117
    End Enum


    'Enum for the Table List for TimeStamp
    Public Enum TABLEID
        CASES = 1
        CONTACT = 2
        OPPORT = 3
        PROSPECT = 4
    End Enum


    'Enum for the ListItem Master
    Public Enum LISTMASTER
        COMP_STAT = 1
        COMP_RAT = 2
        COMP_CRDT = 3
        INDUSTRY = 4
        PROSP_TYPE = 5
        ANNL_REVEN = 6
        NO_EMPLOYEES = 7
        CONTACT_TYPE = 8
        SOURCE = 9
        STAGE = 10
        GENERAL = 11
        DEAL_CONCL = 12
        CASE_PRIORITY = 13
        CASE_STATUS = 14
        CASE_ORIGIN = 15
        CASE_TYPE = 16
        CASE_REASON = 17
        CONT_LOC = 20
    End Enum


    'Enum For Module IDs for Security.
    Public Enum MODULEID
        Tickler = 1
        Leads = 2
        Prospects = 3
        Accounts = 4
        Forecasting = 5
        Marketing = 6
        Support = 7
        Pre_Defined_Reports = 8
        Advanced_Search = 9
        Opportunities = 10
        Contacts = 11
        Projects = 12
        Administration = 13
        Documents = 14
        Extranet = 15
        Dashboard = 24
        PRM_Relationship = 25
        PRM_Action_Items = 26
        PRM_Contacts = 28
        PRM_Opportunities = 29
        PRM_Projects = 30
        PRM_Cases = 31
        Relationships = 32
        Outlook = 33
        Contract_Management = 34
        Accounting = 35
        Subscription = 36
        Items = 37
        TimeAndExpense = 40
    End Enum
    Public Enum RecetlyViewdRecordType
        Item 'I
        AssetItem 'AI
        Document  'D
        Opportunity 'O
        Support 'S
        Project 'P
        Account_Pospect_Lead 'C Customer
        ActionItem 'A
        Contact 'U
        Campaign 'C Campaign
        ReturnMA 'R
        BizDoc 'BD
        WorkOrder 'WO
    End Enum

    Public Enum RecurringEventType
        RecurringForOrder = 1
        RecurringForBizdoc = 2
    End Enum



    'This enum is created for the rights on a specific page on Page Level.
    Public Enum RIGHTSTYPE
        VIEW = 0
        ADD = 1
        UPDATE = 2
        DELETE = 3
        EXPORT = 4
    End Enum

    'The following Enum is for rthe Rights values.
    Public Enum RIGHTSVALUE
        NO_RIGHTS = 0
        ONLY_OWNER = 1
        ONLY_TERRITORY = 2
        ALL_RECORDS = 3
    End Enum
    Public Enum SORT_TYPE
        ACTIVE = 1
        INACTIVE = 2
        MY_RECORDS = 3
        ALL_RECORDS = 4
        RATING = 5
        LAST7DAYS = 6
        LAST20ADDED = 7
        LAST20MODIFIED = 8
    End Enum
    'replace '_' with ' ' white space
    Public Enum enmSearchCriteria
        Contains_ = 1
        Starts_With_ = 2
        Ends_With_ = 3
        Contains_no_value_ = 4
        Or_ = 5
        Equal_To_ = 6
        Not_Equal_To_ = 7
        Less_than_ = 8
        Greater_Than_ = 9
        _Between_ = 10
        Between_ = 11
        _Equal_To = 12 'equal to used for dropdown list search criteria 
        _Not_Equal_To_ = 13
        _Equal_To_ = 14 ' equal to used for date field search criteria 
    End Enum
    Public Enum IndexType
        Items = 0
        Emails = 1
        Documents = 2
    End Enum
    Public Enum enmTransChargeBareBy
        Employer = 0
        Customer = 1
    End Enum
    Public Class Constants
        Public g_strConnectionString As String
        Public Const g_strDecrpKey As String = "g_intEMPLOYERITEMID" 'This is the Decryption Key used for the Connection String.
        Public Const g_intEMPLOYERITEMID As Integer = 93 'For holding the prospect type of Employer
        Public Const g_intEMPLOYEEITEMID As Integer = 92 'For holding the contact type of Employee
        Public Const g_intPRIMARYCONTACTID As Integer = 70 'For holding the primary contact Id
        Public Const g_intLMSOURCEID As Integer = 9   'This is the List Master ID for Source.
        Public Const g_intLMSTAGEID As Integer = 10   'This is the List Master ID for Stage.
        Public Const g_intCLOSEDDEAL As Integer = 90  'For holding the Closed deal ID for Opportunity & Cases.
        Public Const g_intCLOSEDLOST As Integer = 91  'For holding the Closed Lost ID for Opportunity & Cases.

        'Constants for holding the Percentage Id's from the StagePercentageMaster Table
        Public Const g_stagePercentageId5 As Integer = 1 'When the stage percentage is 5%
        Public Const g_stagePercentageId20 As Integer = 2 'When the stage percentage is 20%
        Public Const g_stagePercentageId25 As Integer = 3 'When the stage percentage is 25% 
        Public Const g_stagePercentageId35 As Integer = 4 'When the stage percentage is 35%
        Public Const g_stagePercentageId40 As Integer = 5 'When the stage percentage is 40%
        Public Const g_stagePercentageId50 As Integer = 6 'When the stage percentage is 50%
        Public Const g_stagePercentageId60 As Integer = 7 'When the stage percentage is 60%
        Public Const g_stagePercentageId65 As Integer = 8 'When the stage percentage is 65%
        Public Const g_stagePercentageId75 As Integer = 9 'When the stage percentage is 75%
        Public Const g_stagePercentageId80 As Integer = 10 'When the stage percentage is 80%
        Public Const g_stagePercentageId95 As Integer = 11 'When the stage percentage is 95%
        Public Const g_stagePercentageId100 As Integer = 12 'When the stage percentage is 100% (Deal Closed Equivalent)
        Public Const g_stagePercentageId As Integer = 13 'When the stage percentage is 0% (Deal Lost Equivalent)

        Public Const g_intMaxLengthOfTextArea As Integer = 50
        Public Const g_intMaxLengthOfTextBox As Integer = 50

        Public Const g_CASE_CLOSED As Integer = 136

        'PAGE IDs for Security
        'Page ID for Prospect List
        Public Const g_PAGEID_PROSPECT_LIST As Integer = 1
        'Page ID for Account List
        Public Const g_PAGEID_ACCOUNT_LIST As Integer = 1
        'PAGE ID For Contacts within Prospects-Display Via Accounts.
        Public Const g_PAGEID_PROSPECTDISPLAY_ACCOUNTS_CONTACTS As Integer = 17
        'PAGE ID For Contacts within Prospects-Display Via Prospects.
        Public Const g_PAGEID_PROSPECTDISPLAY_PROSPECTS_CONTACTS As Integer = 20
        'PAGE ID For Opportunities within Prospects-Display Via Accounts.
        Public Const g_PAGEID_PROSPECTDISPLAY_ACCOUNTS_OPPTS As Integer = 18
        'PAGE ID For Action Items within Via Accounts.
        Public Const g_PAGEID_PROSPECTDISPLAY_ACCOUNTS_ACTION_ITEMS As Integer = 20

        'PAGE ID For Action Items within Via Prospects.
        Public Const g_PAGEID_PROSPECTDISPLAY_PROSPECTS_ACTION_ITEMS As Integer = 22

        'PAGE ID For Opportunuty Via Opportunity List Page.
        Public Const g_PAGEID_PROSPECTDISPLAY_OPPORTUNITY_OPPORTUNITY As Integer = 3

        'PAGE ID For Opportunities within Prospects-Display Via Prospects.
        Public Const g_PAGEID_PROSPECTDISPLAY_PROSPECTS_OPPTS As Integer = 21
        'PAGE ID For Cases within Prospects-Display Via Accounts.
        Public Const g_PAGEID_PROSPECTDISPLAY_ACCOUNTS_CASES As Integer = 19
        'PAGE ID For Communication Within Tickler
        Public Const g_PAGEID_TICKLER_COMMUNICATION As Integer = 5
        'PAGE ID For Opportunity Within Tickler
        Public Const g_PAGEID_TICKLER_OPPT As Integer = 2
        'PAGE ID For Cases Within Tickler
        Public Const g_PAGEID_TICKLER_CASES As Integer = 6
        'Page ID for Simple Search.
        Public Const g_PAGEID_SEARCH_ADV As Integer = 2
        'Page ID for Advanced Search.
        Public Const g_PAGEID_SEARCH_SMP As Integer = 1
        'Page ID for Case Search.
        Public Const g_PAGEID_SEARCH_CASE As Integer = 3
        'Page ID for Survey List.
        Public Const g_PAGEID_SURVEY_LIST As Integer = 1
        'Page ID for Survey Questions.
        Public Const g_PAGEID_SURVEY_QUESTIONS As Integer = 2
        'Page ID for Survey Report.
        Public Const g_PAGEID_SURVEY_REPORT As Integer = 3
        'Page ID for Multi-view Report.
        Public Const g_PAGEID_MULTIV_REPORT As Integer = 2
        'Page ID for Custom Report.
        Public Const g_PAGEID_CUSTOMREPORT As Integer = 44
        'PageID for Subordinate Table in Performance Analysis
        Public Const g_PAGEID_PREFANA_SUBORDINATES As Integer = 2
        'PageID for Company Wide Table in Performance Analysis
        Public Const g_PAGEID_PREFANA_COMPANYWIDE As Integer = 3
        'PageID for Account Demote
        Public Const g_PAGEID_ACCOUNT_DEMOTE As Integer = 22


        'Page Names.
        Public Const g_PAGE_LEADS As String = "newleads1.aspx"
        Public Const g_PAGE_CONTACTLIST As String = "contactlist.aspx"
        Public Const g_PAGE_MASTERITEMLIST As String = "masteritemlist.aspx"
        Public Const g_PAGE_ACTIONLIST As String = "newaction.aspx"
        Public Const g_PAGE_ACTIONITEMDETAILS As String = "actionitemdetails.aspx"


        'The Following are for the List Master.
        Public Const g_LM_CAMPAIGNTYPE As String = "CampaignType"
        Public Const g_LM_CAMPAIGNLEVEL As String = "CampaignLevel"
        Public Const g_LM_CAMPAIGNFOCUS As String = "CampaignFocus"

        'MODULE IDs
        Public g_MODULEID_PROSPECT_LIST As String = CStr(MODULEID.Prospects)  'Module ID for Prospect LIst.
        Public g_MODULEID_ACCOUNT_LIST As String = CStr(MODULEID.Accounts)  'Module ID for Account LIst.

    End Class

    Public Enum enmBizDocTemplate_BizDocID
        Estimate = 288
        Pro_Forma_Invoice_1 = 290
        Pro_Forma_Invoice_2 = 290
        Invoice_1 = 287
        Invoice_2 = 287
        Invoice_3 = 287
        PO_Invoice_1 = 15151
        PO_Invoice_2 = 842
        Packing_Slip_Template = 29397

        Sales_Opportunity = 291
        Purchase_Opportunity = 292
        SalesOrder = 293 '30719
        Sales_Credit_Memo = 294 '30720
        Purchase_Credit_Memo = 295 '30721
        Fulfillment_Order = 296
        Credit_Memo = 297
        Refund_Receipt = 298 '33555
        RMA = 299 '33556
        PurchaseOrder = 300
        CustomerStatement = 40911

    End Enum

    Public Enum IMPORT_TYPE
        Item = 20
        Item_WareHouse = 48
        Item_Assembly = 31
        Item_Images = 54
        Item_Vendor = 55
        Organization = 133
        Contact = 134
        Organization_Correspondence = 43
        Address_Details = 98
        Item_Price_Level = 132
        SalesOrder = 136
        PurchaseOrder = 137
        JournalEntries = 140
    End Enum

    ''' <summary>
    ''' This field idendicates that value stored in UPC Field will act as one of following in amazon
    ''' </summary>
    ''' <remarks></remarks>
    Public Enum enmStandardProductIDType
        ASIN = 1
        EAN = 2
        GTIN = 3
        ISBN = 4
        UPC = 5
    End Enum

    '''<summary>
    ''' This field represents various status for Import File data
    ''' </summary>
    ''' <remarks></remarks>

    Public Enum enmImportDataStatus
        Import_Pending = 0
        Import_Completed = 1
        Import_Rollback_Pending = 2
        Import_Rollback_Incomplete = 3
    End Enum

    Public Enum enmAmazonShipServices

        AMZ_Standard = 1
        AMZ_Expedited = 2
        AMZ_Two_Day = 3
        AMZ_One_Day = 4
        AMZ_International = 5
        AMZ_International_Expedited = 6

    End Enum

    Public Enum enmReferenceType
        CheckHeader = 1 'for write check use this
        CheckDetail = 2
        BillHeader = 3 'Add bill 
        BillDetail = 4
        DirectJournal = 5 ' New Journal Entry
        DepositHeader = 6 'Deposits
        DepositDetail = 7
        BillPaymentHeader = 8 'Bill Payment 
        BillPaymentDetail = 9
        RMA = 10
        PayrollCommission = 11
        TimeAndExpense = 12
        TimeAndExpenseDetail = 12
    End Enum

    Public Enum enmReturnStatus
        Pending = 301
        Received = 302
        Returned = 303
    End Enum

    Public Enum enmReferenceTypeForMirrorBizDocs
        SalesOrder = 1
        PurchaseOrder = 2
        SalesOpportuinity = 3
        PurchaseOpportunity = 4
        SalesReturns = 5
        PurchaseReturns = 6
        SalesCreditMemo = 7
        PurchaseCreditMemo = 8
        RefundReceipt = 9
        CreditMemo = 10
    End Enum

    Public Enum enmOpportunityAutomationQueue
        PendingExecution = 1
        InProcess = 2
        Success = 3
        Failed = 4
        ChangeStatus = 5
    End Enum

    Public Enum PaymentMethod
        GoogleCheckout = 31488
        BillMe = 27261
        CreditCard = 1
        Paypal = 35141
        SalesInquiry = 84
    End Enum

    Public Enum enmWFQueueProcessStatus
        PendingExecution = 1
        InProcess = 2
        Success = 3
        Failed = 4
        NoWFFound = 5
        ConditionNotMatch = 6
    End Enum

    Public Enum enmWFAction
        SendAlerts = 1
        AssignActionItems = 2
        UpdateFields = 3
        DisplayAlertMessage = 4
        BizDocApproval = 5
        CreateBizDoc = 6
        PromoteOrganization = 7
        DemoteOrganization = 8
        SMSAlerts = 9

    End Enum
End Namespace



