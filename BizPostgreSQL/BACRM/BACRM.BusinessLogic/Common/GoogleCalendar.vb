﻿Option Explicit On
Option Strict Off
Imports System.Configuration
Imports System.Web.HttpContext
Imports System
Imports System.IO
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports BACRM.BusinessLogic.Admin
Imports System.Text.RegularExpressions
Imports System.Collections.Generic
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
'Imports Google.GData.Calendar
'Imports Google.GData.Client
'Imports Google.GData.Extensions


Imports Google.Apis.Calendar.v3
Imports Google.Apis.Calendar.v3.Data
Imports Google.Apis.Services
Imports Google.Apis.Util
Imports Google.Apis.Auth
Imports Google.Apis.Auth.OAuth2
Imports Google.Apis.Auth.OAuth2.Web
Imports Google.Apis.Auth.OAuth2.Flows
Imports Google.Apis.Auth.OAuth2.Responses

Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook
Imports System.Net
Imports Newtonsoft.Json.Linq
Imports GoogleAuthenticate
Imports Newtonsoft.Json

Public Class GoogleCalendar

    Public Shared ReadOnly ClientID As String = ConfigurationManager.AppSettings("GoogleClientID")
    Public Shared ReadOnly ClientSecret As String = ConfigurationManager.AppSettings("GoogleClientSecret")

    'Private Function CreateAuthenticator(ByVal RefreshToken As String) As OAuth2Authenticator(Of WebServerClient)
    '    ' Register the authenticator.
    '    Dim provider As WebServerClient = New WebServerClient(GoogleAuthenticationServer.Description)
    '    provider.ClientIdentifier = ClientID
    '    provider.ClientSecret = ClientSecret
    '    '  'Dim authenticator As OAuth2Authenticator(Of WebServerClient) = New OAuth2Authenticator(Of WebServerClient)(provider, CType(GetAuthorization(RefreshToken), Global.System.Func(Of Global.DotNetOpenAuth.OAuth2.WebServerClient, Global.DotNetOpenAuth.OAuth2.IAuthorizationState))) With { _
    '    '  '  .NoCaching = True
    '    '  '}
    '    Dim authenticator As OAuth2Authenticator(Of WebServerClient) = New OAuth2Authenticator(Of WebServerClient)(provider, GetAuthorization) With { _
    '    .NoCaching = True
    '  }
    '    Return authenticator

    '    'Dim provider = New WebServerClient(GoogleAuthenticationServer.Description)
    '    'provider.ClientIdentifier = ClientID
    '    'provider.ClientSecret = ClientSecret
    '    'WebServerClient(provider, Func < WebServerClient, IAuthorizationState > GetAuthorization())
    '    'Dim authenticator = New OAuth2Authenticator(Of WebServerClient)(provider, With { _
    '    '  .NoCaching = True _
    '    '}
    '    'Return Authenticator
    'End Function

    'Private Function GetAuthorization() As IAuthorizationState
    '    Dim post As String = String.Format("refresh_token={0}&client_id={1}&client_secret={2}&grant_type=refresh_token", "1/7vjfM6NA87R9bQjKJG3DHxplaEBESp5IAOLdt6kUHcM", ClientID, ClientSecret)

    '    Dim httpRequest As HttpWebRequest = DirectCast(WebRequest.Create("https://accounts.google.com/o/oauth2/token?" & post), HttpWebRequest)
    '    httpRequest.Method = "POST"
    '    httpRequest.ContentType = "application/x-www-form-urlencoded"

    '    httpRequest.ContentLength = post.Length

    '    Dim streamWriter As New StreamWriter(httpRequest.GetRequestStream())
    '    streamWriter.Write(post, 0, post.Length)
    '    streamWriter.Flush()
    '    streamWriter.Close()

    '    Dim ss As HttpWebResponse = DirectCast(httpRequest.GetResponse(), HttpWebResponse)
    '    Dim stream As Stream = ss.GetResponseStream()
    '    Dim sr As New StreamReader(stream)
    '    'Read the Resonse
    '    Dim response As String = sr.ReadToEnd()

    '    Dim state As IAuthorizationState = GetCachedRefreshToken(response, "1/7vjfM6NA87R9bQjKJG3DHxplaEBESp5IAOLdt6kUHcM")

    '    Return state
    'End Function

    'Public Shared Function GetCachedRefreshToken(ByVal response As String, ByVal RefreshToken As String) As IAuthorizationState
    '    Dim token As JToken = JObject.Parse(response)

    '    Dim scopes As String() = "https://www.googleapis.com/auth/calendar.readonly".Split({" "c}, StringSplitOptions.RemoveEmptyEntries)

    '    Return New AuthorizationState(scopes) With { _
    '     .RefreshToken = RefreshToken, _
    '     .AccessToken = token.SelectToken("access_token").ToString(), _
    '     .AccessTokenIssueDateUtc = DateTime.UtcNow, _
    '     .AccessTokenExpirationUtc = DateTime.UtcNow.AddSeconds(CInt(token.SelectToken("expires_in"))) _
    '    }
    'End Function

    Public Function BizToGoogleCalendar(ByVal UserContactID As Long, ByVal numDomainId As Long, ByVal TimeZone As Integer) As String
        Try
            Dim lastUID As Long
            Dim dtTable As New DataTable
            Dim dtCredientials As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.UserCntID = UserContactID
            objUserAccess.DomainID = numDomainId
            dtCredientials = objUserAccess.GetImapDtls()

            Dim username As String
            Dim Password As String
            Dim RefreshToken As String

            If dtCredientials.Rows.Count > 0 Then
                If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                    Dim objCommon As New CCommon
                    Password = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                    username = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                    lastUID = CLng(dtCredientials.Rows(0).Item("LastUID"))
                    RefreshToken = CStr(dtCredientials.Rows(0).Item("vcGoogleRefreshToken"))

                    If RefreshToken.Length = 0 Then
                        Return "Imap Is not Integrated for Google calendar"
                        Exit Function
                    End If
                Else
                    Return "Imap Is not Integrated for Google calendar"
                    Exit Function
                End If
            Else
                Return "Imap Is not Integrated for Google Calendar" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                Exit Function
            End If

            Try
                Dim token = New TokenResponse() With {.RefreshToken = RefreshToken}
                Dim secrets As New ClientSecrets() With {.ClientId = ClientID, .ClientSecret = ClientSecret}

                Dim credentials = New UserCredential(New GoogleAuthorizationCodeFlow(New GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = secrets}), "Bizautomation.com", token)

                Dim service As CalendarService = New CalendarService(New BaseClientService.Initializer() With { _
                    .HttpClientInitializer = credentials, _
                    .ApplicationName = "Bizautomation.com" _
                })


                Dim ObjOutlook As New COutlook
                Dim ResourceID As String = String.Empty
                If ObjOutlook Is Nothing Then
                    ObjOutlook = New COutlook
                End If
                ObjOutlook.UserCntID = UserContactID
                ObjOutlook.DomainID = numDomainId
                dtTable = ObjOutlook.GetResourceId
                If dtTable.Rows.Count > 0 Then
                    ResourceID = dtTable.Rows(0).Item("ResourceId").ToString()
                End If

                If ResourceID.Trim.Length > 0 Then
                    Dim dtCalandar As New DataTable

                    ObjOutlook.StartDateTimeUtc = DateTime.Now.Date.AddDays(-1).Date
                    ObjOutlook.ResourceName = ResourceID

                    dtCalandar = ObjOutlook.ActivityFutureDate()

                    For Each Row As DataRow In dtCalandar.Rows

                        If Not IsDBNull(Row.Item("GoogleEventId")) Then
                            Try
                                Dim entry As [Event] = service.Events.Get("primary", Row.Item("GoogleEventId").ToString()).Execute()

                                entry.Summary = IIf(IsDBNull(Row.Item("subject")), "", Row.Item("subject")).ToString()
                                entry.Description = IIf(IsDBNull(Row.Item("ActivityDescription")), "", Row.Item("ActivityDescription").ToString.Replace("~", "")).ToString()
                                entry.Location = IIf(IsDBNull(Row.Item("Location")), "", Row.Item("Location")).ToString()

                                Dim StartDate As EventDateTime = New EventDateTime()
                                StartDate.DateTime = Convert.ToDateTime(Row.Item("StartDateTimeUtc")).ToString("yyyy-MM-ddTHH:mm:ss.000Z")
                                entry.Start = StartDate

                                Dim EndDate As EventDateTime = New EventDateTime()
                                EndDate.DateTime = Convert.ToDateTime(Row.Item("StartDateTimeUtc")).AddSeconds(CDbl(Row.Item("Duration"))).ToString("yyyy-MM-ddTHH:mm:ss.000Z")
                                entry.End = EndDate

                                'entry.AllDay = CBool(Row.Item("AllDayEvent"))

                                Dim updatedEvent As [Event] = service.Events.Update(entry, "primary", entry.Id).Execute()
                            Catch ex2 As Exception
                                ObjOutlook.ActivityID = CCommon.ToLong(Row.Item("ActivityID"))

                                ObjOutlook.RemoveActivity()
                            End Try
                        Else
                            Dim entry As [Event] = New [Event]

                            ' Set the title and content of the entry.
                            entry.Summary = IIf(IsDBNull(Row.Item("subject")), "", Row.Item("subject")).ToString()
                            entry.Description = IIf(IsDBNull(Row.Item("ActivityDescription")), "", Row.Item("ActivityDescription").ToString.Replace("~", "")).ToString()

                            entry.Location = IIf(IsDBNull(Row.Item("Location")), "", Row.Item("Location")).ToString()

                            Dim StartDate As EventDateTime = New EventDateTime()
                            StartDate.DateTime = Convert.ToDateTime(Row.Item("StartDateTimeUtc")).ToString("yyyy-MM-ddTHH:mm:ss.000Z")
                            entry.Start = StartDate

                            Dim EndDate As EventDateTime = New EventDateTime()
                            EndDate.DateTime = Convert.ToDateTime(Row.Item("StartDateTimeUtc")).AddSeconds(CDbl(Row.Item("Duration"))).ToString("yyyy-MM-ddTHH:mm:ss.000Z")
                            entry.End = EndDate

                            Dim createdEvent As [Event] = service.Events.Insert(entry, "primary").Execute()

                            ObjOutlook.ActivityID = CCommon.ToLong(Row.Item("ActivityID"))
                            ObjOutlook.Activity_GoogleEventId(createdEvent.Id.ToString())
                        End If
                    Next

                    'GoogleCalendarToBiz(UserContactID, numDomainId)
                End If

                Return "BizToGoogleCalendar Successfully updated!!!"

            Catch ex1 As Exception
                Return "Invalid Credential for Google Calendar:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                Exit Function
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function BizToOffice365Calendar(ByVal UserContactID As Long, ByVal numDomainId As Long, ByVal TimeZone As Integer) As String
        Try
            Dim lastUID As Long
            Dim dtTable As New DataTable
            Dim dtCredientials As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.UserCntID = UserContactID
            objUserAccess.DomainID = numDomainId
            dtCredientials = objUserAccess.GetImapDtls()

            Dim username As String
            Dim Password As String
            Dim RefreshToken As String

            If dtCredientials.Rows.Count > 0 Then
                If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                    Dim objCommon As New CCommon
                    Password = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                    username = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                    lastUID = CLng(dtCredientials.Rows(0).Item("LastUID"))
                    RefreshToken = CStr(dtCredientials.Rows(0).Item("vcGoogleRefreshToken"))

                    If RefreshToken.Length = 0 Then
                        Return "Imap Is not Integrated for Google calendar"
                        Exit Function
                    End If
                Else
                    Return "Imap Is not Integrated for Google calendar"
                    Exit Function
                End If
            Else
                Return "Imap Is not Integrated for Google Calendar" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                Exit Function
            End If

            Dim office365AppClientID As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365AppClientID"))
            Dim office365AppClientSecret As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365AppClientSecret"))
            Dim office365AppRedirectURL As String = CCommon.ToString(ConfigurationManager.AppSettings("office365AppRedirectURL"))

            Dim postData As String = ""
            postData += "client_id" + "=" + HttpUtility.UrlEncode(office365AppClientID) + "&"
            postData += "refresh_token" + "=" + HttpUtility.UrlEncode(RefreshToken) + "&"
            postData += "grant_type" + "=" + HttpUtility.UrlEncode("refresh_token") + "&"
            postData += "client_secret" + "=" + HttpUtility.UrlEncode(office365AppClientSecret)

            Dim request As HttpWebRequest = HttpWebRequest.Create("https://login.microsoftonline.com/common/oauth2/v2.0/token")
            request.Method = "POST"
            request.ContentType = "application/x-www-form-urlencoded"

            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
            request.ContentLength = byteArray.Length

            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()

            Try
                Dim response As HttpWebResponse = request.GetResponse()
                Dim responseString As String = New StreamReader(response.GetResponseStream()).ReadToEnd()
                response.Close()

                Dim objOffice365TokenResponse = JValue.Parse(responseString)

                If Not objOffice365TokenResponse Is Nothing Then
                    objUserAccess.UpdateGoogleRefreshToken(CCommon.ToString(objOffice365TokenResponse("refresh_token")))

                    Dim ObjOutlook As New COutlook
                    Dim ResourceID As String = String.Empty
                    If ObjOutlook Is Nothing Then
                        ObjOutlook = New COutlook
                    End If
                    ObjOutlook.UserCntID = UserContactID
                    ObjOutlook.DomainID = numDomainId
                    dtTable = ObjOutlook.GetResourceId
                    If dtTable.Rows.Count > 0 Then
                        ResourceID = dtTable.Rows(0).Item("ResourceId").ToString()
                    End If

                    If ResourceID.Trim.Length > 0 Then
                        Dim dtCalandar As New DataTable

                        ObjOutlook.StartDateTimeUtc = DateTime.Now.Date.AddDays(-1).Date
                        ObjOutlook.ResourceName = ResourceID

                        dtCalandar = ObjOutlook.ActivityFutureDate()

                        For Each Row As DataRow In dtCalandar.Rows

                            If Not IsDBNull(Row.Item("GoogleEventId")) Then
                                Dim eventJson As String = "{" & _
                                                                """subject"":""" & CCommon.ToString(Row.Item("subject")) & """," & _
                                                                """body"":{" & _
                                                                    """contentType"":""html""," & _
                                                                    """content"":""" & CCommon.ToString(Row.Item("ActivityDescription")) & """" & _
                                                                "}," & _
                                                                """start"": {" & _
                                                                    """dateTime"": """ & Convert.ToDateTime(Row.Item("StartDateTimeUtc")).ToString("yyyy-MM-ddTHH:mm:ss.000Z") & """," & _
                                                                    """timeZone"": ""UTC""" & _
                                                                "}," & _
                                                                """end"": {" & _
                                                                    """dateTime"": """ & Convert.ToDateTime(Row.Item("StartDateTimeUtc")).AddSeconds(CDbl(Row.Item("Duration"))).ToString("yyyy-MM-ddTHH:mm:ss.000Z") & """," & _
                                                                    """timeZone"": ""UTC""" & _
                                                                "}," & _
                                                                """location"":{" & _
                                                                   """displayName"":""" & CCommon.ToString(Row.Item("Location")) & """" & _
                                                                "}," & _
                                                            "}"


                                request = HttpWebRequest.Create("https://graph.microsoft.com/v1.0/users/" & username & "/events/" & CCommon.ToString(Row.Item("GoogleEventId")))
                                request.Headers.Add("Authorization", "Bearer " + CCommon.ToString(objOffice365TokenResponse("access_token")))
                                request.Method = "PATCH"
                                request.ContentType = "application/json"

                                byteArray = Encoding.UTF8.GetBytes(eventJson)
                                request.ContentLength = byteArray.Length

                                dataStream = request.GetRequestStream()
                                dataStream.Write(byteArray, 0, byteArray.Length)
                                dataStream.Close()

                                Try
                                    response = request.GetResponse()
                                    responseString = New StreamReader(response.GetResponseStream()).ReadToEnd()
                                    response.Close()
                                Catch exWeb As WebException
                                    ObjOutlook.ActivityID = CCommon.ToLong(Row.Item("ActivityID"))
                                    ObjOutlook.RemoveActivity()
                                Catch ex As Exception
                                    ObjOutlook.ActivityID = CCommon.ToLong(Row.Item("ActivityID"))
                                    ObjOutlook.RemoveActivity()
                                End Try
                            Else
                                Dim eventJson As String = "{" & _
                                                              """subject"":""" & CCommon.ToString(Row.Item("subject")) & """," & _
                                                              """body"":{" & _
                                                                    """contentType"":""html""," & _
                                                                    """content"":""" & CCommon.ToString(Row.Item("ActivityDescription")) & """" & _
                                                                "}," & _
                                                              """start"": {" & _
                                                                  """dateTime"": """ & Convert.ToDateTime(Row.Item("StartDateTimeUtc")).ToString("yyyy-MM-ddTHH:mm:ss.000Z") & """," & _
                                                                  """timeZone"": ""UTC""" & _
                                                              "}," & _
                                                              """end"": {" & _
                                                                  """dateTime"": """ & Convert.ToDateTime(Row.Item("StartDateTimeUtc")).AddSeconds(CDbl(Row.Item("Duration"))).ToString("yyyy-MM-ddTHH:mm:ss.000Z") & """," & _
                                                                  """timeZone"": ""UTC""" & _
                                                              "}," & _
                                                              """location"":{" & _
                                                                 """displayName"":""" & CCommon.ToString(Row.Item("Location")) & """" & _
                                                              "}," & _
                                                          "}"


                                request = HttpWebRequest.Create("https://graph.microsoft.com/v1.0/users/" & username & "/events")
                                request.Headers.Add("Authorization", "Bearer " + CCommon.ToString(objOffice365TokenResponse("access_token")))
                                request.Method = "POST"
                                request.ContentType = "application/json"

                                byteArray = Encoding.UTF8.GetBytes(eventJson)
                                request.ContentLength = byteArray.Length

                                dataStream = request.GetRequestStream()
                                dataStream.Write(byteArray, 0, byteArray.Length)
                                dataStream.Close()

                                Try
                                    response = request.GetResponse()
                                    responseString = New StreamReader(response.GetResponseStream()).ReadToEnd()
                                    response.Close()

                                    Dim objOffice365CalendarEvents = JValue.Parse(responseString)

                                    ObjOutlook.ActivityID = CCommon.ToLong(Row.Item("ActivityID"))
                                    ObjOutlook.Activity_GoogleEventId(CCommon.ToString(objOffice365CalendarEvents("id")))
                                Catch exWeb As WebException

                                Catch ex As Exception

                                End Try
                            End If
                        Next
                    End If
                End If

                Return "BizToOffice365Calendar Successfully updated!!!"
            Catch exWeb As WebException
                If exWeb.Response IsNot Nothing Then
                    Dim tokenResponse As String = New StreamReader(exWeb.Response.GetResponseStream()).ReadToEnd()
                    Dim objOffice365TokenErrorResponse = JValue.Parse(tokenResponse)

                    Return "Invalid Credential for Office 365 Calendar:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString & " & ErrorMessage:" & CCommon.ToString(objOffice365TokenErrorResponse("error_description"))
                    Exit Function
                End If
            Catch ex As Exception
                Return "Invalid Credential for Office 365 Calendar:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                Exit Function
            End Try

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GoogleCalendarToBiz(ByVal UserContactID As Long, ByVal numDomainId As Long, ByVal TimeZone As Integer) As String
        Try
            Dim lastUID As Long
            Dim dtTable As New DataTable
            Dim dtCredientials As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.UserCntID = UserContactID
            objUserAccess.DomainID = numDomainId
            dtCredientials = objUserAccess.GetImapDtls()

            Dim username As String
            Dim Password As String
            Dim RefreshToken As String

            If dtCredientials.Rows.Count > 0 Then
                If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                    Dim objCommon As New CCommon
                    Password = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                    username = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                    lastUID = CLng(dtCredientials.Rows(0).Item("LastUID"))
                    RefreshToken = CStr(dtCredientials.Rows(0).Item("vcGoogleRefreshToken"))

                    If RefreshToken.Length = 0 Then
                        Return "Imap Is not Integrated for Google calendar"
                        Exit Function
                    End If
                Else
                    Return "Imap Is not Integrated for Google Calendar"
                    Exit Function
                End If
            Else
                Return "Imap Is not Integrated for Google Calendar" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                Exit Function
            End If

            Try
                Dim token = New TokenResponse() With {.RefreshToken = RefreshToken}
                Dim secrets As New ClientSecrets() With {.ClientId = ClientID, .ClientSecret = ClientSecret}

                Dim credentials = New UserCredential(New GoogleAuthorizationCodeFlow(New GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = secrets}), "Bizautomation.com", token)

                Dim service As CalendarService = New CalendarService(New BaseClientService.Initializer() With { _
                    .HttpClientInitializer = credentials, _
                    .ApplicationName = "Bizautomation.com" _
                })

                Dim ObjOutlook As New COutlook
                Dim ResourceID As String = String.Empty
                If ObjOutlook Is Nothing Then
                    ObjOutlook = New COutlook
                End If
                ObjOutlook.UserCntID = UserContactID
                ObjOutlook.DomainID = numDomainId
                dtTable = ObjOutlook.GetResourceId
                If dtTable.Rows.Count > 0 Then
                    ResourceID = dtTable.Rows(0).Item("ResourceId").ToString()
                End If

                If ResourceID.Trim.Length > 0 Then
                    Dim er As EventsResource.ListRequest = service.Events.List("primary")
                    er.TimeMin = DateTime.Now.[Date].AddDays(-1).ToString("yyyy-MM-dd") & "T00:00:00Z"

                    Dim calFeed As Events = er.Execute()

                    For Each entry As [Event] In calFeed.Items
                        If entry.Recurrence Is Nothing AndAlso entry.Summary IsNot Nothing Then
                            Dim dtCalandar As New DataTable

                            dtCalandar = ObjOutlook.GetAcivitybyGoogleEventId(entry.Id, CCommon.ToInteger(ResourceID))

                            If dtCalandar.Rows.Count > 0 Then

                                ObjOutlook.ActivityDescription = entry.Description
                                ObjOutlook.Location = entry.Location
                                ObjOutlook.Subject = entry.Summary

                                Dim dtStart As DateTime
                                Dim dtend As DateTime

                                If entry.Start.DateTime IsNot Nothing Then
                                    dtStart = DateTime.Parse(entry.Start.DateTimeRaw, Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                    ObjOutlook.AllDayEvent = False
                                Else
                                    dtStart = DateTime.Parse(entry.Start.Date, Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                    ObjOutlook.AllDayEvent = True
                                End If

                                If entry.End.DateTime IsNot Nothing Then
                                    dtend = DateTime.Parse(entry.End.DateTimeRaw, Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                Else
                                    dtend = DateTime.Parse(entry.End.Date, Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                End If

                                ObjOutlook.Duration = CInt(DateDiff(DateInterval.Second, dtStart, dtend))
                                ObjOutlook.StartDateTimeUtc = dtStart

                                ObjOutlook.EnableReminder = CBool(dtCalandar.Rows(0).Item("EnableReminder"))
                                ObjOutlook.ReminderInterval = CInt(dtCalandar.Rows(0).Item("ReminderInterval"))
                                ObjOutlook.ShowTimeAs = CInt(dtCalandar.Rows(0).Item("ShowTimeAs"))
                                ObjOutlook.Importance = CInt(dtCalandar.Rows(0).Item("Importance"))
                                ObjOutlook.Status = CInt(dtCalandar.Rows(0).Item("Status"))

                                ObjOutlook.RecurrenceKey = CInt(dtCalandar.Rows(0).Item("RecurrenceID"))
                                ObjOutlook.ActivityID = CCommon.ToLong(dtCalandar.Rows(0).Item("ActivityID"))

                                ObjOutlook.HtmlLink = entry.HtmlLink

                                ObjOutlook.UpdateActivity()

                                Dim objAttendeeList As New AttendeeCollection
                                If entry.Attendees IsNot Nothing AndAlso entry.Attendees.Count > 0 Then
                                    For Each attendee As EventAttendee In entry.Attendees
                                        If attendee.Organizer Is Nothing Then
                                            Dim objAttendee As New Attendee()
                                            objAttendee.EmailId = attendee.Email
                                            objAttendee.ResponseStatus = attendee.ResponseStatus
                                            objAttendeeList.Add(objAttendee)
                                        End If
                                    Next
                                End If
                                objAttendeeList.Save(ObjOutlook.ActivityID, objUserAccess.ContactID, objUserAccess.DomainID)

                            Else

                                ObjOutlook.ActivityDescription = entry.Description
                                ObjOutlook.Location = entry.Location
                                ObjOutlook.Subject = entry.Summary

                                Dim dtStart As DateTime
                                Dim dtend As DateTime

                                If entry.Start.DateTime IsNot Nothing Then
                                    dtStart = DateTime.Parse(entry.Start.DateTimeRaw, Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                    ObjOutlook.AllDayEvent = False
                                Else
                                    dtStart = DateTime.Parse(entry.Start.Date, Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                    ObjOutlook.AllDayEvent = True
                                End If

                                If entry.End.DateTime IsNot Nothing Then
                                    dtend = DateTime.Parse(entry.End.DateTimeRaw, Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                Else
                                    dtend = DateTime.Parse(entry.End.Date, Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                End If

                                ObjOutlook.Duration = CInt(DateDiff(DateInterval.Second, dtStart, dtend))
                                ObjOutlook.StartDateTimeUtc = dtStart

                                ObjOutlook.EnableReminder = True
                                ObjOutlook.ReminderInterval = 900
                                ObjOutlook.ShowTimeAs = 3
                                ObjOutlook.Importance = 2
                                ObjOutlook.RecurrenceKey = -999
                                ObjOutlook.ResourceName = ResourceID
                                ObjOutlook.HtmlLink = entry.HtmlLink
                                Dim ActivityID As Long = ObjOutlook.AddActivity()
                                ObjOutlook.ActivityID = ActivityID
                                Dim objAttendeeList As New AttendeeCollection

                                If entry.Attendees IsNot Nothing AndAlso entry.Attendees.Count > 0 Then
                                    For Each attendee As EventAttendee In entry.Attendees
                                        If attendee.Organizer Is Nothing Then
                                            Dim objAttendee As New Attendee()
                                            objAttendee.EmailId = attendee.Email
                                            objAttendee.ResponseStatus = attendee.ResponseStatus
                                            objAttendeeList.Add(objAttendee)
                                        End If
                                    Next
                                End If
                                objAttendeeList.Save(ObjOutlook.ActivityID, objUserAccess.ContactID, objUserAccess.DomainID)
                                'End If
                                ObjOutlook.Activity_GoogleEventId(entry.Id.ToString)
                            End If

                        End If
                    Next
                End If

                Return "GoogleCalendarToBiz Successfully updated!!!"

            Catch ex1 As Exception
                Return "Invalid Credential for Google Calendar:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                Exit Function
            End Try
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function Office365CalendarToBiz(ByVal UserContactID As Long, ByVal numDomainId As Long, ByVal TimeZone As Integer) As String
        Try
            Dim lastUID As Long
            Dim dtTable As New DataTable
            Dim dtCredientials As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.UserCntID = UserContactID
            objUserAccess.DomainID = numDomainId
            dtCredientials = objUserAccess.GetImapDtls()

            Dim username As String
            Dim Password As String
            Dim RefreshToken As String

            If dtCredientials.Rows.Count > 0 Then
                If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                    Dim objCommon As New CCommon
                    Password = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                    username = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                    lastUID = CLng(dtCredientials.Rows(0).Item("LastUID"))
                    RefreshToken = CStr(dtCredientials.Rows(0).Item("vcGoogleRefreshToken"))

                    If RefreshToken.Length = 0 Then
                        Return "Imap Is not Integrated for Office 365 calendar"
                        Exit Function
                    End If
                Else
                    Return "Imap Is not Integrated for Office 365 Calendar"
                    Exit Function
                End If
            Else
                Return "Imap Is not Integrated for Office 365 Calendar" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                Exit Function
            End If

            Dim office365AppClientID As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365AppClientID"))
            Dim office365AppClientSecret As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365AppClientSecret"))
            Dim office365AppRedirectURL As String = CCommon.ToString(ConfigurationManager.AppSettings("office365AppRedirectURL"))

            Dim postData As String = ""
            postData += "client_id" + "=" + HttpUtility.UrlEncode(office365AppClientID) + "&"
            postData += "refresh_token" + "=" + HttpUtility.UrlEncode(RefreshToken) + "&"
            postData += "grant_type" + "=" + HttpUtility.UrlEncode("refresh_token") + "&"
            postData += "client_secret" + "=" + HttpUtility.UrlEncode(office365AppClientSecret)

            Dim request As HttpWebRequest = HttpWebRequest.Create("https://login.microsoftonline.com/common/oauth2/v2.0/token")
            request.Method = "POST"
            request.ContentType = "application/x-www-form-urlencoded"

            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
            request.ContentLength = byteArray.Length

            Dim dataStream As Stream = request.GetRequestStream()
            dataStream.Write(byteArray, 0, byteArray.Length)
            dataStream.Close()

            Try
                Dim response As HttpWebResponse = request.GetResponse()
                Dim responseString As String = New StreamReader(response.GetResponseStream()).ReadToEnd()
                response.Close()

                Dim objOffice365TokenResponse = JValue.Parse(responseString)

                If Not objOffice365TokenResponse Is Nothing Then
                    objUserAccess.UpdateGoogleRefreshToken(CCommon.ToString(objOffice365TokenResponse("refresh_token")))

                    request = HttpWebRequest.Create("https://graph.microsoft.com/v1.0/users/" & username & "/events?$filter=createdDateTime ge " & DateTime.UtcNow.[Date].AddDays(-1).ToString("yyyy-MM-dd") & "T00:00:00Z")
                    request.Headers.Add("Authorization", "Bearer " + CCommon.ToString(objOffice365TokenResponse("access_token")))
                    request.ContentType = "application/json"

                    Try
                        response = request.GetResponse()
                        responseString = New StreamReader(response.GetResponseStream()).ReadToEnd()
                        response.Close()

                        Dim objOffice365CalendarEvents = JValue.Parse(responseString)

                        If Not objOffice365CalendarEvents Is Nothing AndAlso objOffice365CalendarEvents("value").Count > 0 Then
                            Dim ObjOutlook As New COutlook
                            Dim ResourceID As String = String.Empty
                            If ObjOutlook Is Nothing Then
                                ObjOutlook = New COutlook
                            End If
                            ObjOutlook.UserCntID = UserContactID
                            ObjOutlook.DomainID = numDomainId
                            dtTable = ObjOutlook.GetResourceId
                            If dtTable.Rows.Count > 0 Then
                                ResourceID = dtTable.Rows(0).Item("ResourceId").ToString()
                            End If

                            If ResourceID.Trim.Length > 0 Then
                                For Each entry As Object In objOffice365CalendarEvents("value")
                                    If String.IsNullOrEmpty(CCommon.ToString(entry("recurrence"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(entry("subject"))) Then
                                        Dim dtCalandar As New DataTable

                                        dtCalandar = ObjOutlook.GetAcivitybyGoogleEventId(CCommon.ToString(entry("id")), CCommon.ToInteger(ResourceID))

                                        If dtCalandar.Rows.Count > 0 Then

                                            ObjOutlook.ActivityDescription = CCommon.ToString(entry("bodyPreview"))
                                            ObjOutlook.Location = DirectCast(entry("location"), JObject)("displayName")
                                            ObjOutlook.Subject = entry("subject")

                                            If CCommon.ToBool(entry("isAllDay")) Then
                                                ObjOutlook.AllDayEvent = True
                                            Else
                                                ObjOutlook.AllDayEvent = False
                                            End If

                                            Dim dtStart As DateTime
                                            Dim dtend As DateTime

                                            If DirectCast(entry("start"), JObject)("dateTime") IsNot Nothing Then
                                                dtStart = DateTime.Parse(DirectCast(entry("start"), JObject)("dateTime"), Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                            Else
                                                dtStart = DateTime.Parse(DirectCast(entry("start"), JObject)("date"), Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                            End If

                                            If DirectCast(entry("end"), JObject)("dateTime") IsNot Nothing Then
                                                dtend = DateTime.Parse(DirectCast(entry("end"), JObject)("dateTime"), Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                            Else
                                                dtend = DateTime.Parse(DirectCast(entry("end"), JObject)("date"), Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                            End If

                                            ObjOutlook.Duration = CInt(DateDiff(DateInterval.Second, dtStart, dtend))
                                            ObjOutlook.StartDateTimeUtc = dtStart

                                            ObjOutlook.EnableReminder = CBool(dtCalandar.Rows(0).Item("EnableReminder"))
                                            ObjOutlook.ReminderInterval = CInt(dtCalandar.Rows(0).Item("ReminderInterval"))
                                            ObjOutlook.ShowTimeAs = CInt(dtCalandar.Rows(0).Item("ShowTimeAs"))
                                            ObjOutlook.Importance = CInt(dtCalandar.Rows(0).Item("Importance"))
                                            ObjOutlook.Status = CInt(dtCalandar.Rows(0).Item("Status"))

                                            ObjOutlook.RecurrenceKey = CInt(dtCalandar.Rows(0).Item("RecurrenceID"))
                                            ObjOutlook.ActivityID = CCommon.ToLong(dtCalandar.Rows(0).Item("ActivityID"))

                                            ObjOutlook.HtmlLink = CCommon.ToString(entry("webLink"))

                                            ObjOutlook.UpdateActivity()

                                            Dim objAttendeeList As New AttendeeCollection
                                            If entry("attendees") IsNot Nothing AndAlso entry("attendees").Count > 0 Then
                                                For Each attendee As Object In entry("attendees")
                                                    If attendee("organizer") Is Nothing Then
                                                        Dim objAttendee As New Attendee()
                                                        objAttendee.EmailId = CCommon.ToString(DirectCast(attendee("emailAddress"), JObject)("address"))
                                                        objAttendee.ResponseStatus = CCommon.ToString(DirectCast(attendee("status"), JObject)("response"))
                                                        objAttendeeList.Add(objAttendee)
                                                    End If
                                                Next
                                            End If
                                            objAttendeeList.Save(ObjOutlook.ActivityID, objUserAccess.ContactID, objUserAccess.DomainID)

                                        Else
                                            ObjOutlook.ActivityDescription = CCommon.ToString(entry("bodyPreview"))
                                            ObjOutlook.Location = DirectCast(entry("location"), JObject)("displayName")
                                            ObjOutlook.Subject = entry("subject")

                                            If CCommon.ToBool(entry("isAllDay")) Then
                                                ObjOutlook.AllDayEvent = True
                                            Else
                                                ObjOutlook.AllDayEvent = False
                                            End If

                                            Dim dtStart As DateTime
                                            Dim dtend As DateTime

                                            If DirectCast(entry("start"), JObject)("dateTime") IsNot Nothing Then
                                                dtStart = DateTime.Parse(DirectCast(entry("start"), JObject)("dateTime"), Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                            Else
                                                dtStart = DateTime.Parse(DirectCast(entry("start"), JObject)("date"), Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                            End If

                                            If DirectCast(entry("end"), JObject)("dateTime") IsNot Nothing Then
                                                dtend = DateTime.Parse(DirectCast(entry("end"), JObject)("dateTime"), Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                            Else
                                                dtend = DateTime.Parse(DirectCast(entry("end"), JObject)("date"), Nothing, System.Globalization.DateTimeStyles.AdjustToUniversal)
                                            End If

                                            ObjOutlook.Duration = CInt(DateDiff(DateInterval.Second, dtStart, dtend))
                                            ObjOutlook.StartDateTimeUtc = dtStart

                                            ObjOutlook.EnableReminder = True
                                            ObjOutlook.ReminderInterval = 900
                                            ObjOutlook.ShowTimeAs = 3
                                            ObjOutlook.Importance = 2
                                            ObjOutlook.RecurrenceKey = -999
                                            ObjOutlook.ResourceName = ResourceID
                                            ObjOutlook.HtmlLink = CCommon.ToString(entry("webLink"))
                                            Dim ActivityID As Long = ObjOutlook.AddActivity()
                                            ObjOutlook.ActivityID = ActivityID
                                            Dim objAttendeeList As New AttendeeCollection

                                            If entry("attendees") IsNot Nothing AndAlso entry("attendees").Count > 0 Then
                                                For Each attendee As Object In entry("attendees")
                                                    If attendee("organizer") Is Nothing Then
                                                        Dim objAttendee As New Attendee()
                                                        objAttendee.EmailId = CCommon.ToString(DirectCast(attendee("emailAddress"), JObject)("address"))
                                                        objAttendee.ResponseStatus = CCommon.ToString(DirectCast(attendee("status"), JObject)("response"))
                                                        objAttendeeList.Add(objAttendee)
                                                    End If
                                                Next
                                            End If
                                            objAttendeeList.Save(ObjOutlook.ActivityID, objUserAccess.ContactID, objUserAccess.DomainID)
                                            'End If
                                            ObjOutlook.Activity_GoogleEventId(entry("id"))
                                        End If

                                    End If
                                Next
                            End If
                        End If
                    Catch ex As WebException
                        If ex.Response IsNot Nothing Then
                            Dim tokenResponse As String = New StreamReader(ex.Response.GetResponseStream()).ReadToEnd()
                        End If
                    End Try

                End If

                Return "Office365ToBizCalendar Successfully updated!!!"
            Catch exWeb As WebException
                If exWeb.Response IsNot Nothing Then
                    Dim tokenResponse As String = New StreamReader(exWeb.Response.GetResponseStream()).ReadToEnd()
                    Dim objOffice365TokenErrorResponse = JValue.Parse(tokenResponse)

                    Return "Invalid Credential for Office 365 Calendar:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString & " & ErrorMessage:" & CCommon.ToString(objOffice365TokenErrorResponse("error_description"))
                    Exit Function
                End If
            Catch ex As Exception
                Return "Invalid Credential for Office 365 Calendar:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                Exit Function
            End Try


        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GoogleActivityDelete(ByVal UserContactID As Long, ByVal numDomainId As Long, ByVal ActivityID As Long) As String
        Try
            Dim lastUID As Long
            Dim dtTable As New DataTable
            Dim dtCredientials As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.UserCntID = UserContactID
            objUserAccess.DomainID = numDomainId
            dtCredientials = objUserAccess.GetImapDtls()

            Dim username As String
            Dim Password As String
            Dim RefreshToken As String

            If dtCredientials.Rows.Count > 0 Then
                If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                    Dim objCommon As New CCommon
                    Password = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                    username = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                    lastUID = CLng(dtCredientials.Rows(0).Item("LastUID"))
                    RefreshToken = CStr(dtCredientials.Rows(0).Item("vcGoogleRefreshToken"))

                    If RefreshToken.Length = 0 Then
                        Return "Imap Is not Integrated for Google calendar"
                        Exit Function
                    End If
                Else
                    Return "Imap Is not Integrated for Google Calendar"
                    Exit Function
                End If
            Else
                Return "Imap Is not Integrated for Google Calendar" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                Exit Function
            End If

            Try
                Dim token = New TokenResponse() With {.RefreshToken = RefreshToken}
                Dim secrets As New ClientSecrets() With {.ClientId = ClientID, .ClientSecret = ClientSecret}

                Dim credentials = New UserCredential(New GoogleAuthorizationCodeFlow(New GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = secrets}), "Bizautomation.com", token)

                Dim service As CalendarService = New CalendarService(New BaseClientService.Initializer() With { _
                    .HttpClientInitializer = credentials, _
                    .ApplicationName = "Bizautomation.com" _
                })

                Dim ObjOutlook As New COutlook

                ObjOutlook.ActivityID = ActivityID
                ObjOutlook.mode = True
                Dim dtCalandar As New DataTable

                dtCalandar = ObjOutlook.getActivityByActId()


                If dtCalandar.Rows.Count > 0 Then
                    If Not IsDBNull(dtCalandar.Rows(0).Item("GoogleEventId")) Then
                        'Dim query As New EventQuery()
                        'query.Uri = New Uri("http://www.google.com/calendar/feeds/default/private/full/" & dtCalandar.Rows(0).Item("GoogleEventId").ToString())

                        '' Tell the service to query:
                        'Dim calFeed As EventFeed = service.Query(query)

                        'For Each entry As Google.GData.Calendar.EventEntry In calFeed.Entries
                        '    entry.Delete()
                        'Next
                        service.Events.Delete("primary", dtCalandar.Rows(0).Item("GoogleEventId").ToString()).Execute()
                    End If
                End If

                Return ""
            Catch ex1 As Exception
                Return "Invalid Credential for Google Calendar:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                Exit Function
            End Try
        Catch ex As Exception
            Return ""
        End Try
    End Function

End Class

