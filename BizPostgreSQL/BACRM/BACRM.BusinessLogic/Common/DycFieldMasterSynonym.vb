﻿Imports BACRM.BusinessLogic.Common
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Common

    Public Class DycFieldMasterSynonym
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property ID As Long
        Public Property FieldID As Long
        Public Property IsCustomField As Boolean
        Public Property Synonym As String
        Public Property IsDefault As Boolean
        Public Property SelectedRecords As String

#End Region

        Public Function GetByFieldID() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFieldID", FieldID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitCustomField", IsCustomField, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_DycFieldMasterSynonym_GetByFieldID", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub Save()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numFieldID", FieldID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSynonym", Synonym, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitCustomField", IsCustomField, NpgsqlTypes.NpgsqlDbType.Bit))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_DycFieldMasterSynonym_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcIDs", SelectedRecords, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_DycFieldMasterSynonym_Delete", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

    End Class


End Namespace

