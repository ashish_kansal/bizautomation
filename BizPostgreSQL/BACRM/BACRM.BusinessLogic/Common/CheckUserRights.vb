'Option Explicit On
'Option Strict On
'Imports System.Data.SqlClient
'Imports BACRMAPI.DataAccessLayer
'Imports BACRMBUSSLOGIC.BussinessLogic
'Namespace BACRM.BusinessLogic.Common
'    Public Class CheckUserRights
'Inherits BACRM.BusinessLogic.CBusinessBase
'        Public Shared m_PageRightsArray As New Hashtable    ' declare a hashtable
'        'this is added to get value of parameter from presentation layer
'        Private _UserID As Integer = 0
'        Private _PageName As String = String.Empty
'        Private _ModuleID As Integer = 0
'        Private _PageID As Integer = 0
'        Public Sub New()
'            REM Function that is implemented during the creation of an instance of this object
'            MyBase.New()
'        End Sub
'        'defining property for parameters
'        Public Property PageName() As String
'            Get
'                Return _PageName
'            End Get
'            Set(ByVal Value As String)
'                _PageName = Value
'            End Set
'        End Property

'        Public Property UserID() As Integer
'            Get
'                Return _UserID
'            End Get
'            Set(ByVal Value As Integer)
'                _UserID = Value
'            End Set
'        End Property

'        Public Property ModuleID() As Integer
'            Get
'                Return _ModuleID
'            End Get
'            Set(ByVal Value As Integer)
'                _ModuleID = Value
'            End Set
'        End Property
'        Public Property PageID() As Integer
'            Get
'                Return _PageID
'            End Get
'            Set(ByVal Value As Integer)
'                _PageID = Value
'            End Set
'        End Property
'        '***************************************************************************************************************************
'        '     Function / Subroutine  Name:  GetPageListUserRights()
'        '     Purpose					 :  This function will check users rights. 
'        '     Example                    :  
'        '                                   Dim result As Integer =  ExecuteNonQuery(connString, "CustomerOrders")
'        '     Parameters                 :  UserID, PageName ,@ModuleID ,@PageID                       
'        '                                 
'        '     Outputs					 :  It return array which is use to check user rights
'        '						
'        '     Author Name				 :  Ajeet Singh
'        '     Date Written				 :  18/10/2004
'        '     Cross References 			 :  List of functions being called by this function.
'        '     Modification History
'        '     -------------------------------------------------------------------------------------------------------------------------------------------
'        '        Modified Date         Modified By                          Purpose Of Modification
'        '     -------------------------------------------------------------------------------------------------------------------------------------------        
'        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
'        '***************************************************************************************************************************

'        Public Function GetPageListUserRights() As Array
'            ''This array will hold the rights for the user.
'            Dim aryPageRights(4) As Integer
'            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
'            Try
'                Dim dr As Npgsql.NpgsqlDataReader = Nothing
'                Dim getconnection As New getconnection
'                Dim connString As String = getconnection.GetConnectionString

'                '@UserID   Parameter 
'                arParms(0) = New Npgsql.NpgsqlParameter("@UserID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
'                arParms(0).Value = _UserID

'                '@PageName   Parameter 
'                arParms(1) = New Npgsql.NpgsqlParameter("@PageName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
'                arParms(1).Value = _PageName

'                '@ModuleID   Parameter 
'                arParms(2) = New Npgsql.NpgsqlParameter("@ModuleID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
'                arParms(2).Value = _ModuleID

'                '@PageID   Parameter 
'                arParms(3) = New Npgsql.NpgsqlParameter("@PageID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
'                arParms(3).Value = _PageID

'                ' If _ModuleID > 0 And _PageID > 0 Then
'                dr = SqlDAL.ExecuteReader(connString, "usp_GetPageLevelUserRights", arParms)
'                ' End If

'                aryPageRights(RIGHTSTYPE.VIEW) = 0
'                aryPageRights(RIGHTSTYPE.ADD) = 0
'                aryPageRights(RIGHTSTYPE.DELETE) = 0
'                aryPageRights(RIGHTSTYPE.EXPORT) = 0
'                aryPageRights(RIGHTSTYPE.UPDATE) = 0

'                Do While dr.Read
'                    aryPageRights(RIGHTSTYPE.VIEW) = CInt(dr("intViewAllowed"))
'                    aryPageRights(RIGHTSTYPE.ADD) = CInt(dr("intAddAllowed"))
'                    aryPageRights(RIGHTSTYPE.DELETE) = CInt(dr("intDeleteAllowed"))
'                    aryPageRights(RIGHTSTYPE.EXPORT) = CInt(dr("intExportAllowed"))
'                    aryPageRights(RIGHTSTYPE.UPDATE) = CInt(dr("intUpdateAllowed"))
'                Loop
'                'Return the Array generated.
'                GetPageListUserRights = aryPageRights
'            Catch ex As Exception

'            End Try
'        End Function
'    End Class
'End Namespace
