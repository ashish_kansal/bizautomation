﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic

Namespace BACRM.BusinessLogic.Common

    Public Class StagePercentageDetailsTask
        Inherits CBusinessBase

#Region "Public Properties"

        Public Property StageDetailsID As Long
        Public Property TaskID As Long
        Public Property AssignedTo As Long
        Public Property TaskHours As Long
        Public Property TaskMinutes As Long
        Public Property StartTime As DateTime
        Public Property EndTime As DateTime

#End Region

#Region "Public Methods"

        Public Function GetByStage() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numStageDetailsId", StageDetailsID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_StagePercentageDetailsTask_GetByStage", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub ChangeAssignee()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTaskID", TaskID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numAssignedTo", AssignedTo, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_StagePercentageDetailsTask_ChangeAssignee", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub ChangeTime(ByVal isMasterUpdateAlso As Boolean)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTaskID", TaskID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numHours", TaskHours, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numMinutes", TaskMinutes, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitMasterUpdateAlso", isMasterUpdateAlso, NpgsqlTypes.NpgsqlDbType.Bit))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_StagePercentageDetailsTask_ChangeTime", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub ChangeTimeProductionPlanning()
            Try
                Try
                    Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                    Dim getconnection As New GetConnection
                    Dim connString As String = getconnection.GetConnectionString

                    With sqlParams
                        .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@numTaskID", TaskID, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@dtStartTime", StartTime, NpgsqlTypes.NpgsqlDbType.Timestamp))
                        .Add(SqlDAL.Add_Parameter("@dtEndTime", EndTime, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    End With

                    SqlDAL.ExecuteNonQuery(connString, "USP_StagePercentageDetailsTask_ChangeTimeProductionPlanning", sqlParams.ToArray())
                Catch ex As Exception
                    Throw
                End Try
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Close()
            Try
                Try
                    Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                    Dim getconnection As New GetConnection
                    Dim connString As String = getconnection.GetConnectionString

                    With sqlParams
                        .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@numTaskID", TaskID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    End With

                    SqlDAL.ExecuteNonQuery(connString, "USP_StagePercentageDetailsTask_Close", sqlParams.ToArray())
                Catch ex As Exception
                    Throw
                End Try
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class

End Namespace
