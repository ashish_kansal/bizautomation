﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic

Namespace BACRM.BusinessLogic.Common

    Public Class StageGradeDetails
        Inherits CBusinessBase

        Public Function GetByProcess(ByVal processID As Long) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProcessID", processID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_tblStageGradeDetails_GetByProcess", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

End Namespace


