

Public Class NumericToWord
    Inherits BACRM.BusinessLogic.CBusinessBase
    '''' Commented by chintan see bug id 905 #4
    'Public Function changeNumericToWords(ByVal numb As Double) As String
    '    Dim num As String = numb.ToString
    '    Return changeToWords(num, False)
    'End Function

    'Public Function changeCurrencyToWords(ByVal numb As String) As String
    '    Return changeToWords(numb, True)
    'End Function

    'Public Function changeNumericToWords(ByVal numb As String) As String
    '    Return changeToWords(numb, False)
    'End Function

    'Public Function changeCurrencyToWords(ByVal numb As Double) As String
    '    Return changeToWords(numb.ToString, True)
    'End Function

    'Private Function changeToWords(ByVal numb As String, ByVal isCurrency As Boolean) As String
    '    Dim val As String = ""
    '    Dim wholeNo As String = numb
    '    Dim points As String = ""
    '    Dim andStr As String = ""
    '    Dim pointStr As String = ""
    '    Dim endStr As String = Microsoft.VisualBasic.IIf((isCurrency), ("Only"), (""))
    '    Try
    '        Dim decimalPlace As Integer = numb.IndexOf(".")
    '        If decimalPlace > 0 Then
    '            wholeNo = numb.Substring(0, decimalPlace)
    '            points = numb.Substring(decimalPlace + 1)
    '            If Convert.ToInt32(points) >= 0 Then
    '                andStr = Microsoft.VisualBasic.IIf((isCurrency), ("and"), (" and "))
    '                endStr = Microsoft.VisualBasic.IIf((isCurrency), ("Cents " + endStr), (""))
    '                pointStr = translateCents(points)
    '            End If
    '        End If
    '        val = String.Format("{0} {1}{2} {3}", IIf(wholeNo > 0, translateWholeNumber(wholeNo).Trim, "Zero"), andStr, pointStr, endStr)
    '    Catch

    '    End Try
    '    Return val
    'End Function

    'Private Function translateWholeNumber(ByVal number As String) As String
    '    Dim word As String = ""
    '    Try
    '        Dim beginsZero As Boolean = False
    '        Dim isDone As Boolean = False
    '        Dim dblAmt As Double = (Convert.ToDouble(number))
    '        If dblAmt > 0 Then
    '            beginsZero = number.StartsWith("0")
    '            Dim numDigits As Integer = number.Length
    '            Dim pos As Integer = 0
    '            Dim place As String = ""
    '            Select Case numDigits
    '                Case 1
    '                    word = ones(number)
    '                    isDone = True
    '                    ' break 
    '                Case 2
    '                    word = tens(number)
    '                    isDone = True
    '                    ' break 
    '                Case 3
    '                    pos = (numDigits Mod 3) + 1
    '                    place = " Hundred "
    '                    ' break 
    '                Case 4, 5, 6
    '                    pos = (numDigits Mod 4) + 1
    '                    place = " Thousand "
    '                    ' break 
    '                Case 7, 8, 9
    '                    pos = (numDigits Mod 7) + 1
    '                    place = " Million "
    '                    ' break 
    '                Case 10
    '                    pos = (numDigits Mod 10) + 1
    '                    place = " Billion "
    '                    ' break 
    '                Case Else
    '                    isDone = True
    '                    ' break 
    '            End Select
    '            If Not isDone Then
    '                word = translateWholeNumber(number.Substring(0, pos)) + place + translateWholeNumber(number.Substring(pos))
    '                If beginsZero Then
    '                    word = " and " + word.Trim
    '                End If
    '            End If
    '            If word.Trim.Equals(place.Trim) Then
    '                word = ""
    '            End If

    '        End If
    '    Catch

    '    End Try
    '    Return word.Trim
    'End Function

    'Private Function tens(ByVal digit As String) As String
    '    Dim digt As Integer = Convert.ToInt32(digit)
    '    Dim name As String = Nothing
    '    Select Case digt
    '        Case 10
    '            name = "Ten"
    '            ' break 
    '        Case 11
    '            name = "Eleven"
    '            ' break 
    '        Case 12
    '            name = "Twelve"
    '            ' break 
    '        Case 13
    '            name = "Thirteen"
    '            ' break 
    '        Case 14
    '            name = "Fourteen"
    '            ' break 
    '        Case 15
    '            name = "Fifteen"
    '            ' break 
    '        Case 16
    '            name = "Sixteen"
    '            ' break 
    '        Case 17
    '            name = "Seventeen"
    '            ' break 
    '        Case 18
    '            name = "Eighteen"
    '            ' break 
    '        Case 19
    '            name = "Nineteen"
    '            ' break 
    '        Case 20
    '            name = "Twenty"
    '            ' break 
    '        Case 30
    '            name = "Thirty"
    '            ' break 
    '        Case 40
    '            name = "Fourty"
    '            ' break 
    '        Case 50
    '            name = "Fifty"
    '            ' break 
    '        Case 60
    '            name = "Sixty"
    '            ' break 
    '        Case 70
    '            name = "Seventy"
    '            ' break 
    '        Case 80
    '            name = "Eighty"
    '            ' break 
    '        Case 90
    '            name = "Ninety"
    '            ' break 
    '        Case Else
    '            If digt > 0 Then
    '                name = tens(digit.Substring(0, 1) + "0") + " " + ones(digit.Substring(1))
    '            End If
    '            ' break 
    '    End Select
    '    Return name
    'End Function

    'Private Function ones(ByVal digit As String) As String
    '    Dim digt As Integer = Convert.ToInt32(digit)
    '    Dim name As String = ""
    '    Select Case digt
    '        Case 1
    '            name = "One"
    '            ' break 
    '        Case 2
    '            name = "Two"
    '            ' break 
    '        Case 3
    '            name = "Three"
    '            ' break 
    '        Case 4
    '            name = "Four"
    '            ' break 
    '        Case 5
    '            name = "Five"
    '            ' break 
    '        Case 6
    '            name = "Six"
    '            ' break 
    '        Case 7
    '            name = "Seven"
    '            ' break 
    '        Case 8
    '            name = "Eight"
    '            ' break 
    '        Case 9
    '            name = "Nine"
    '            ' break 
    '    End Select
    '    Return name
    'End Function

    'Private Function translateCents(ByVal cents As String) As String
    '    Dim cts As String = ""
    '    Dim digit As String = ""
    '    Dim engOne As String = ""
    '    Dim i As Integer = 0
    '    If cents.Length > 0 Then
    '        cts = cents & "/ 100"
    '        ''digit = cents(i).ToString
    '        ''If digit.Equals("0") Then
    '        ''    engOne = "Zero"
    '        ''Else
    '        ''    engOne = ones(digit)
    '        ''End If
    '        ''cts += " " + engOne
    '        ''System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
    '    End If
    '    Return cts
    'End Function

    Public Function SpellNumber(ByVal MyNumber As String)
        Dim Dollars, Cents, Temp
        Dim DecimalPlace, Count
        Dim Place(9) As String
        Place(2) = " Thousand "
        Place(3) = " Million "
        Place(4) = " Billion "
        Place(5) = " Trillion "
        ' String representation of amount
        MyNumber = Convert.ToString(MyNumber)
        ' Position of decimal place 0 if none
        DecimalPlace = InStr(MyNumber, ".")
        'Convert cents and set MyNumber to dollar amount
        If DecimalPlace > 0 Then
            Cents = Left(Mid(MyNumber, DecimalPlace + 1) & "00", 2)
            MyNumber = Trim(Left(MyNumber, DecimalPlace - 1))
        Else
            Cents = "00"
        End If
        Count = 1
        Do While MyNumber <> ""
            Temp = GetHundreds(Right(MyNumber, 3))
            If Temp <> "" Then Dollars = Temp & Place(Count) & Dollars
            If Len(MyNumber) > 3 Then
                MyNumber = Left(MyNumber, Len(MyNumber) - 3)
            Else
                MyNumber = ""
            End If
            Count = Count + 1
        Loop
        Select Case Dollars
            Case ""
                Dollars = "zero "
            Case "One"
                Dollars = "One "
        End Select

        Cents = " and " & Cents & "/100"

        Return Dollars & Cents
    End Function
    'Converts a number from 100-999 into text
    Private Function GetHundreds(ByVal MyNumber As String)
        Dim Result As String
        If Val(MyNumber) = 0 Then Exit Function
        MyNumber = Right("000" & MyNumber, 3)
        'Convert the hundreds place
        If Mid(MyNumber, 1, 1) <> "0" Then
            Result = GetDigit(Mid(MyNumber, 1, 1)) & " Hundred "
        End If
        'Convert the tens and ones place
        If Mid(MyNumber, 2, 1) <> "0" Then
            Result = Result & GetTens(Mid(MyNumber, 2))
        Else
            Result = Result & GetDigit(Mid(MyNumber, 3))
        End If
        GetHundreds = Result
    End Function

    'Converts a number from 10 to 99 into text
    Private Function GetTens(ByVal TensText As String)
        Dim Result As String
        Result = "" 'null out the temporary function value
        If Val(Left(TensText, 1)) = 1 Then ' If value between 10-19
            Select Case Val(TensText)
                Case 10 : Result = "Ten"
                Case 11 : Result = "Eleven"
                Case 12 : Result = "Twelve"
                Case 13 : Result = "Thirteen"
                Case 14 : Result = "Fourteen"
                Case 15 : Result = "Fifteen"
                Case 16 : Result = "Sixteen"
                Case 17 : Result = "Seventeen"
                Case 18 : Result = "Eighteen"
                Case 19 : Result = "Nineteen"
                Case Else
            End Select
        Else ' If value between 20-99
            Select Case Val(Left(TensText, 1))
                Case 2 : Result = "Twenty "
                Case 3 : Result = "Thirty "
                Case 4 : Result = "Forty "
                Case 5 : Result = "Fifty "
                Case 6 : Result = "Sixty "
                Case 7 : Result = "Seventy "
                Case 8 : Result = "Eighty "
                Case 9 : Result = "Ninety "
                Case Else
            End Select
            Result = Result & GetDigit(Right(TensText, 1)) 'Retrieve ones place
        End If
        GetTens = Result
    End Function

    'Converts a number from 1 to 9 into text
    Private Function GetDigit(ByVal Digit As String)
        Select Case Val(Digit)
            Case 1 : GetDigit = "One"
            Case 2 : GetDigit = "Two"
            Case 3 : GetDigit = "Three"
            Case 4 : GetDigit = "Four"
            Case 5 : GetDigit = "Five"
            Case 6 : GetDigit = "Six"
            Case 7 : GetDigit = "Seven"
            Case 8 : GetDigit = "Eight"
            Case 9 : GetDigit = "Nine"
            Case Else : GetDigit = ""
        End Select
    End Function
End Class
