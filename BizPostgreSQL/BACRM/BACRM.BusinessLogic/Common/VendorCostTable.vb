﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic

Namespace BACRM.BusinessLogic.Common
    Public Class VendorCostTable
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property VendorCostTableID As Long
        Public Property VendorTcode As Long
        Public Property VendorID As Long
        Public Property ItemCode As Long
        Public Property CurrencyID As Long
        Public Property Row As Integer
        Public Property FromQty As Integer
        Public Property ToQty As Integer
        Public Property StaticCost As Decimal
        Public Property DynamicCost As Decimal
        Public Property ClientTimeZoneOffset As Integer

#End Region

#Region "Public Methods"
        Public Function GetByItemVendor() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendortcode", VendorTcode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendorid", VendorID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numitemcode", ItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numcurrencyid", CurrencyID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("p_swvrefcur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "usp_vendorcosttable_getbyitemvendor", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function Save(ByVal costRange As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendortcode", VendorTcode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendorid", VendorID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numitemcode", ItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numcurrencyid", CurrencyID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_vcCostRange", costRange, NpgsqlTypes.NpgsqlDbType.Jsonb))
                End With

                Return SqlDAL.ExecuteDatable(connString, "usp_vendorcosttable_save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function UpdateDefaultRange(ByVal costRange As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_vcCostRange", costRange, NpgsqlTypes.NpgsqlDbType.Jsonb))
                End With

                Return SqlDAL.ExecuteDatable(connString, "usp_vendorcosttable_updatedefaultrange", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Sub UpdateCost()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendortcode", VendorTcode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendorid", VendorID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numitemcode", ItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numcurrencyid", CurrencyID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendorcosttableid", VendorCostTableID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_monStaticCost", StaticCost, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("p_monDynamicCost", DynamicCost, NpgsqlTypes.NpgsqlDbType.Numeric))
                End With

                SqlDAL.ExecuteNonQuery(connString, "usp_vendorcosttable_updatecost", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Sub UpdateStaticCost(ByVal selectedItemGroups As String)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_vcItemGroupsIDs", VendorTcode, NpgsqlTypes.NpgsqlDbType.Text))
                End With

                SqlDAL.ExecuteNonQuery(connString, "usp_vendorcosttable_updatestaticcost", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function IsRangeExist(ByVal quantity As Decimal) As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numvendorid", VendorID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numitemcode", ItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numcurrencyid", CurrencyID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_numQty", quantity, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("p_swvrefcur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "usp_vendorcosttable_checkifrangeexist", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function
#End Region

    End Class

End Namespace