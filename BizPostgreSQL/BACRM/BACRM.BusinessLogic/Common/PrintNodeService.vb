﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports System.Globalization
Imports System.IO
Imports System.Net
Imports System.Security.Cryptography
Imports System.Text.RegularExpressions

Public Class PrintNodeService
    Private ReadOnly address As String
    Private ReadOnly username As String

    ' TODO send base64 image
    ' TODO remove this
    Private ReadOnly Endpoints As New Dictionary(Of String, String)() From { _
        {"Computers", "computers"}, _
        {"Printers", "printers"}, _
        {"PrintJobs", "printjobs"} _
    }

    Public Sub New(address As String, username As String)
        Me.address = address
        Me.username = username
    End Sub

    Public Function GetComputers() As String
        Return [Get]("Computers")
    End Function

    Public Function GetPrinters() As String
        Return [Get]("Printers")
    End Function

    Public Function GetPrintJobs() As String
        Return [Get]("PrintJobs")
    End Function

    '*
    '         * @return string  id of printjob
    '         

    Public Function SubmitPrintJob(ByVal labelUrl As String, ByVal printerId As String) As String
        Dim title As String = "Biz Shipping Label Printing"
  
        Dim sb As New StringBuilder()
        sb.Append("{")
        sb.Append((Convert.ToString("""printer"": ") & printerId) + ",")
        sb.Append((Convert.ToString("""title"": """) & title) + """,")
        sb.Append("""contentType"": ""raw_base64"",")
        sb.Append("""content"": """ + GetBase64String(labelUrl) + """,")
        sb.Append("""source"": ""PrintNodeApi/3.0""")
        sb.Append("}")


        Dim response As String = Post("PrintJobs", sb.ToString())

        ' strips off quotes around the id
        If response IsNot Nothing Then
            Return response.Substring(1, response.Length - 2)
        Else
            Return response
        End If
    End Function

    Private Function BuildUri(dir As String) As Uri
        If Not Endpoints.ContainsKey(dir) Then
            Throw New Exception((Convert.ToString("no endpoint defined for ") & dir) + ". consider adding the endpoint to the Endpoints dictionary defined in PrintNodeService class")
        End If
        Dim url As String = (address & Convert.ToString("/"c)) + Endpoints(dir)
        Return New Uri(url)
    End Function

    Private Function BuildRequest(dir As String, method As [String]) As HttpWebRequest
        Dim uri As Uri = BuildUri(dir)

        Dim encoding__1 As New ASCIIEncoding()
        Dim request As HttpWebRequest = DirectCast(WebRequest.Create(uri), HttpWebRequest)
        ' headers
        request.Method = method
        request.ContentType = "application/json"

        ' basic authentication
        Dim credentials As String = [String].Format("{0}:", username)
        Dim bytes As Byte() = Encoding.ASCII.GetBytes(credentials)
        Dim base64 As String = Convert.ToBase64String(bytes)
        Dim authorization As String = [String].Concat("Basic ", base64)
        request.Headers.Add("Authorization", authorization)

        Return request
    End Function

    Private Function getResponseBody(request As HttpWebRequest) As String
        ' gets response
        Dim response As HttpWebResponse = Nothing
        response = DirectCast(request.GetResponse(), HttpWebResponse)
        Return New StreamReader(response.GetResponseStream()).ReadToEnd()
    End Function

    Private Function [Get](dir As String) As String
        Dim request As HttpWebRequest = BuildRequest(dir, "GET")
        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12
        Return getResponseBody(request)
    End Function

    Private Function Post(dir As String, postData As String) As String
        Dim request As HttpWebRequest = BuildRequest(dir, "POST")
        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12
        ' data
        Dim data As Byte() = Encoding.UTF8.GetBytes(postData)
        request.ContentLength = data.Length

        ' writes data

        Dim requestStream As Stream = request.GetRequestStream()
        requestStream.Write(data, 0, data.Length)
        requestStream.Close()

        Return getResponseBody(request)
    End Function

    Private Function GetBase64String(url As String) As String
        Dim stream As Stream = Nothing
        Dim buf As Byte()

        Try
            Dim myProxy As New WebProxy()
            Dim req As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)

            Dim response As HttpWebResponse = DirectCast(req.GetResponse(), HttpWebResponse)
            stream = response.GetResponseStream()

            Using br As New BinaryReader(stream)
                Dim len As Integer = CInt(response.ContentLength)
                buf = br.ReadBytes(len)
                br.Close()
            End Using

            stream.Close()
            response.Close()
        Catch exp As Exception
            buf = Nothing
        End Try

        Return Convert.ToBase64String(buf)
    End Function
End Class
