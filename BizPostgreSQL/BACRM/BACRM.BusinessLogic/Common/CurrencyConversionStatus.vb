﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports System.Data.SqlClient

Namespace BACRM.BusinessLogic.Common

    Public Class CurrencyConversionStatus
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"
        Public Property Id As Int32
        Public Property LastExecutedDate As Date?
        Public Property IsSucceed As Boolean
        Public Property NoOfTimesTried As Int32
        Public Property IsFailureNotificationSent As Boolean
#End Region

#Region "Public Methods"
        ''' <summary>
        ''' Saves date and status of fetching currency conversion rates from yahoo api through bizservice
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub SaveConversionStatus()
            Try
                Dim ingAccountClassID As Long = 0
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@dtLastExecuted", LastExecutedDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@bitSucceed", IsSucceed, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@intNoOfTimesTried", NoOfTimesTried, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitFailureNotificationSent", IsFailureNotificationSent, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_CurrencyConversionStatus_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        ''' <summary>
        ''' Gets currency conversion codes group by base currency so that we do not have call yahoo api for each domain
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetDataToFetchCurrencyConversion() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_CurrencyConversionStatus_GetDomainByBaseCurrency", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        ''' <summary>
        ''' Gets a status of fetching currency conversion rates from yahoo api through bizservice
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Sub GetCurrencyConversionStatus()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_CurrencyConversionStatus_GET", arParms)

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.Id = DirectCast(IIf(dt.Rows(0)("Id") Is DBNull.Value, 0, dt.Rows(0)("Id")), Int32)
                    Me.LastExecutedDate = DirectCast(IIf(dt.Rows(0)("dtLastExecuted") Is DBNull.Value, DirectCast(Nothing, Nullable(Of Date)), dt.Rows(0)("dtLastExecuted")), Nullable(Of Date))
                    Me.IsSucceed = DirectCast(IIf(dt.Rows(0)("bitSucceed") Is DBNull.Value, False, dt.Rows(0)("bitSucceed")), Boolean)
                    Me.NoOfTimesTried = DirectCast(IIf(dt.Rows(0)("intNoOfTimesTried") Is DBNull.Value, 0, dt.Rows(0)("intNoOfTimesTried")), Int32)
                    Me.IsFailureNotificationSent = DirectCast(IIf(dt.Rows(0)("bitFailureNotificationSent") Is DBNull.Value, False, dt.Rows(0)("bitFailureNotificationSent")), Boolean)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

    End Class
End Namespace