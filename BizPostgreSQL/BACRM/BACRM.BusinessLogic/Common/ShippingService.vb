﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Common

    Public Class ShippingService
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property ShippingServiceID As Long
        Public Property ShipVia As Long
        Public Property ShippingService As String
        Public Property Abbrevations As String

#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numShippingServiceID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ShippingServiceID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numShipViaID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ShipVia

                arParms(3) = New Npgsql.NpgsqlParameter("@vcShippingService", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(3).Value = ShippingService

                arParms(4) = New Npgsql.NpgsqlParameter("@vcAbbreviation", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(4).Value = Abbrevations

                SqlDAL.ExecuteNonQuery(connString, "USP_ShippingService_Save", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete()
            Try
                Try
                    Dim getconnection As New GetConnection
                    Dim connString As String = getconnection.GetConnectionString
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                    arParms(0) = New Npgsql.NpgsqlParameter("@numShippingServiceID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(0).Value = ShippingServiceID

                    arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(1).Value = DomainID

                    SqlDAL.ExecuteNonQuery(connString, "USP_ShippingService_Delete", arParms)
                Catch ex As Exception
                    Throw ex
                End Try
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetByShipVia() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numShipViaId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ShipVia

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ShippingService_GetByShipVia", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

#End Region


    End Class
End Namespace