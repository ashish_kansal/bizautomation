﻿Option Explicit On
Option Strict On
Imports System.Configuration
Imports System.Web.HttpContext
Imports System
Imports System.IO
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports System.Text.RegularExpressions
Imports System.Collections.Generic
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports Google.GData.Contacts
Imports Google.GData.Client
Imports Google.GData.Extensions
Imports Google.Contacts
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Leads

Namespace BACRM.BusinessLogic.Common

    Public Class GoogleContact
        Inherits BACRM.BusinessLogic.CBusinessBase

        Public Function GoogleGroups(ByVal UserContactID As Long, ByVal numDomainId As Long) As DataTable
            Try
                Dim lastUID As Long
                Dim dtTable As New DataTable
                Dim dr As DataRow

                dtTable.Columns.Add("vcGroupName")
                dtTable.Columns.Add("numGroupID")

                Dim dtCredientials As DataTable
                Dim objUserAccess As New UserAccess
                Dim objContact As New CContacts
                objUserAccess.UserCntID = UserContactID
                objUserAccess.DomainID = numDomainId
                dtCredientials = objUserAccess.GetImapDtls()

                Dim username As String
                Dim Password As String

                Dim cr As ContactsRequest

                If dtCredientials.Rows.Count > 0 Then
                    If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                        Dim objCommon As New CCommon
                        Password = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                        username = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                        lastUID = CLng(dtCredientials.Rows(0).Item("LastUID"))
                    Else
                        Return dtTable
                        Exit Function
                    End If
                Else
                    Exit Function
                End If

                Try
                    Dim rs As New RequestSettings("Contact Infomation", username, Password)
                    rs.AutoPaging = True
                    cr = New ContactsRequest(rs)

                    Dim f As Feed(Of Google.Contacts.Group) = cr.GetGroups()
                    For Each entry As Google.Contacts.Group In f.Entries
                        dr = dtTable.NewRow
                        dr("vcGroupName") = entry.Title
                        dr("numGroupID") = entry.Id.ToString.Substring(entry.Id.ToString.LastIndexOf("/") + 1)

                        dtTable.Rows.Add(dr)
                    Next
                    Return dtTable
                Catch ex1 As InvalidCredentialsException
                    Return dtTable
                    Exit Function
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetContacts(ByVal numDomainId As Long) As String
            Try
                Dim UserContactID As Long
                Dim dtTable As New DataTable
                Dim dtCredientials As DataTable
                Dim objUserAccess As New UserAccess
                Dim objContact As New CContacts
                Dim dtUsers As DataTable
                Dim objCommon As New CCommon

                dtUsers = objUserAccess.GetImapEnabledUsers(5, numDomainId)

                Dim dtGoogleContact As New DataTable
                dtGoogleContact.Columns.Add("UserCntID", System.Type.GetType("System.Int32"))
                dtGoogleContact.Columns.Add("DomainID", System.Type.GetType("System.Int32"))
                dtGoogleContact.Columns.Add("Name")
                dtGoogleContact.Columns.Add("Cell")
                dtGoogleContact.Columns.Add("HomePhone")
                dtGoogleContact.Columns.Add("Email")
                dtGoogleContact.Columns.Add("Comments")
                dtGoogleContact.Columns.Add("CompanyName")

                Dim dr As DataRow

                For j As Integer = 0 To dtUsers.Rows.Count - 1
                    UserContactID = CCommon.ToLong(dtUsers.Rows(j)("numUserCntId"))

                    objUserAccess.UserCntID = UserContactID
                    objUserAccess.DomainID = numDomainId
                    dtCredientials = objUserAccess.GetImapDtls()

                    Dim username As String
                    Dim Password As String

                    Dim cr As ContactsRequest

                    If dtCredientials.Rows.Count > 0 Then
                        Password = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                        username = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                    Else
                        Return "Imap Is not Integrated for Google Contacts" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                        Exit Function
                    End If

                    Try
                        Dim rs As New RequestSettings("Contact Infomation", username, Password)
                        rs.AutoPaging = True
                        cr = New ContactsRequest(rs)

                        Dim dtUserAccessDetails As DataTable

                        objUserAccess = New UserAccess
                        objUserAccess.UserId = UserContactID
                        objUserAccess.DomainID = numDomainId
                        objUserAccess.GroupType = 2
                        dtUserAccessDetails = objUserAccess.GetGoogleContactGroups

                        If dtUserAccessDetails.Rows.Count > 0 Then
                            For i As Integer = 0 To dtUserAccessDetails.Rows.Count - 1
                                Dim query As New ContactsQuery(ContactsQuery.CreateContactsUri(username))
                                query.Group = "http://www.google.com/m8/feeds/groups/" & username & "/base/" & dtUserAccessDetails.Rows(i).Item("vcGroupId").ToString()

                                ''The example assumes the ContactRequest object (cr) is already set up.
                                Dim f As Feed(Of Contact) = cr.[Get](Of Contact)(query)

                                'Dim f As Feed(Of Contact) = cr.GetContacts()
                                For Each entry As Contact In f.Entries
                                    If entry.Emails.Count >= 1 Then
                                        dr = dtGoogleContact.NewRow

                                        'objContact.UserCntID = UserContactID
                                        'objContact.DomainID = numDomainId
                                        'objContact.Name = entry.Name.FullName

                                        'If entry.Phonenumbers.Count >= 1 Then
                                        '    objContact.ContactPhone = entry.Phonenumbers(0).Value
                                        'End If

                                        'objContact.Email = entry.Emails(0).Address
                                        'objContact.Comments = entry.Content

                                        'objContact.ManageGoogleContact()
                                        If entry.Organizations.Count > 0 Then
                                            dr("CompanyName") = entry.Organizations(0).Name
                                        End If

                                        dr("UserCntID") = UserContactID
                                        dr("DomainID") = numDomainId
                                        dr("Name") = entry.Name.FullName

                                        If entry.Phonenumbers.Count > 0 Then
                                            dr("Cell") = IIf(entry.Phonenumbers(0).Work, entry.Phonenumbers(0).Value, "")
                                            dr("HomePhone") = IIf(entry.Phonenumbers(0).Home, entry.Phonenumbers(0).Value, "")
                                        Else
                                            dr("Cell") = ""
                                            dr("HomePhone") = ""
                                        End If
                                        dr("Email") = entry.Emails(0).Address
                                        dr("Comments") = entry.Content

                                        dtGoogleContact.Rows.Add(dr)
                                    End If
                                Next
                            Next
                        End If
                    Catch ex1 As InvalidCredentialsException
                        'Return "Invalid Credential for Google Contacts:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                        'Exit Function
                    End Try
                Next

                If dtGoogleContact.Rows.Count > 0 Then
                    Dim dtDistinctGoogleContact As New DataTable
                    Dim dvGoogleContact As New DataView(dtGoogleContact)

                    dtDistinctGoogleContact = dvGoogleContact.ToTable(True, "Email")

                    dtDistinctGoogleContact.Columns.Add("UserCntID", System.Type.GetType("System.Int32"))
                    dtDistinctGoogleContact.Columns.Add("DomainID", System.Type.GetType("System.Int32"))
                    dtDistinctGoogleContact.Columns.Add("Name")
                    dtDistinctGoogleContact.Columns.Add("Cell")
                    dtDistinctGoogleContact.Columns.Add("HomePhone")
                    dtDistinctGoogleContact.Columns.Add("Comments")
                    dtDistinctGoogleContact.Columns.Add("CompanyName")

                    Dim drContacts() As DataRow

                    For Each dr In dtDistinctGoogleContact.Rows
                        dr("UserCntID") = 0
                        dr("DomainID") = numDomainId
                        dr("Name") = ""
                        dr("Cell") = ""
                        dr("HomePhone") = ""
                        dr("Comments") = ""

                        drContacts = dtGoogleContact.Select("Email='" & dr("Email").ToString() & "'")
                        For i As Integer = 0 To (drContacts.Length - 1)
                            If drContacts(i)("Cell").ToString.Length > 0 Then
                                dr("Cell") = drContacts(i)("Cell")
                            End If

                            If drContacts(i)("HomePhone").ToString.Length > 0 Then
                                dr("HomePhone") = drContacts(i)("HomePhone")
                            End If

                            If drContacts(i)("CompanyName").ToString.Length > 0 Then
                                dr("CompanyName") = drContacts(i)("CompanyName")
                            End If

                            dr("UserCntID") = drContacts(i)("UserCntID")
                            dr("DomainID") = drContacts(i)("DomainID")
                            dr("Name") = drContacts(i)("Name")

                            dr("Cell") = drContacts(i)("Cell")
                            dr("HomePhone") = drContacts(i)("HomePhone")

                            dr("Comments") = dr("Comments").ToString() + "  " + drContacts(i)("Comments").ToString()
                        Next
                    Next

                    For Each dr In dtDistinctGoogleContact.Rows
                        Dim objOutlook As New COutlook
                        objOutlook.DomainId = numDomainId
                        objOutlook.Email = dr("Email").ToString()
                        Dim dttableChk As New DataTable
                        dttableChk = objOutlook.getDetailsFromEmail()

                        If dttableChk.Rows.Count > 0 Then
                            'objCommon = New CCommon
                            'objCommon.Mode = 3
                            'objCommon.UpdateRecordID = CCommon.ToLong(dttableChk.Rows(0).Item("numContactID"))
                            'objCommon.Comments = dr("Comments").ToString()
                            'objCommon.UpdateSingleFieldValue()
                        Else
                            Dim objLead As New CLeads()
                            objLead.FirstName = dr("Name").ToString()
                            objLead.Cell = dr("Cell").ToString()
                            objLead.HomePhone = dr("HomePhone").ToString()
                            objLead.Comments = dr("Comments").ToString()
                            objLead.Email = dr("Email").ToString()

                            objLead.CompanyName = dr("CompanyName").ToString()

                            objLead.GroupID = 0
                            objLead.DomainID = numDomainId
                            objLead.UserCntID = CCommon.ToLong(dr("UserCntID"))
                            objLead.CRMType = 0
                            objLead.ContactType = 0 'commented by chintan, contact type 70 is obsolete. default contact type is 0
                            objLead.PrimaryContact = True

                            objContact.Email = dr("Email").ToString()
                            objContact.DomainID = numDomainId
                            objContact.ContactPhone = dr("HomePhone").ToString()
                            Dim dtMatch As New DataTable
                            dtMatch = objContact.ManageGoogleContact()

                            If dtMatch.Rows.Count = 0 Then
                                'Create Company
                                objLead.CompanyID = objLead.CreateRecordCompanyInfo()
                                Dim lngDivisionId As Long = 0
                                'Create Division
                                If objLead.DivisionID = 0 Then
                                    lngDivisionId = objLead.CreateRecordDivisionsInfo()
                                    objLead.DivisionID = lngDivisionId
                                End If
                            Else
                                objLead.CompanyID = CCommon.ToLong(dtMatch.Rows(0).Item("numCompanyID"))
                                objLead.DivisionID = CCommon.ToLong(dtMatch.Rows(0).Item("numDivisionID"))
                            End If
                            'Create Contact
                            objLead.CreateRecordAddContactInfo()
                        End If
                    Next
                End If

                Return "GoogleToBizContact Successfully updated!!!"
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function BizToGoogle(ByVal UserContactID As Long, ByVal numDomainId As Long, ByVal numContactId As Long) As String
            Try
                Dim lastUID As Long
                Dim dtTable As New DataTable
                Dim dtCredientials As DataTable
                Dim objUserAccess As New UserAccess
                Dim objContact As New CContacts
                objUserAccess.UserCntID = UserContactID
                objUserAccess.DomainID = numDomainId
                dtCredientials = objUserAccess.GetImapDtls()

                Dim username As String
                Dim Password As String

                Dim cr As ContactsRequest

                If dtCredientials.Rows.Count > 0 Then
                    If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                        Dim objCommon As New CCommon
                        Password = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                        username = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                        lastUID = CLng(dtCredientials.Rows(0).Item("LastUID"))
                    Else
                        Return "Imap Is not Integrated for Google Contacts"
                        Exit Function
                    End If
                Else
                    Return "Imap Is not Integrated for Google Contacts" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End If

                Try
                    Dim objContacts As New ContactIP
                    Dim dtContactInfo As DataTable
                    objContacts.ContactID = numContactId
                    objContacts.DomainID = numDomainId
                    objContacts.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                    dtContactInfo = objContacts.GetCntInfoForEdit1

                    Dim rs As New RequestSettings("Contact Infomation", username, Password)
                    rs.AutoPaging = True
                    cr = New ContactsRequest(rs)

                    If Not IsDBNull(dtContactInfo.Rows(0).Item("vcEmail")) Then
                        If dtContactInfo.Rows(0).Item("vcEmail").ToString.Trim.Length > 0 AndAlso Regex.IsMatch(dtContactInfo.Rows(0).Item("vcEmail").ToString(), "\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*") Then
                            'Dim f As Feed(Of Contact) = cr.GetContacts()
                            'Dim bFound As Boolean = False


                            'For Each entry As Contact In f.Entries
                            '    If entry.Emails.Count >= 1 Then
                            '        For i As Integer = 0 To entry.Emails.Count - 1
                            '            If entry.Emails(i).Address = dtContactInfo.Rows(0).Item("vcEmail").ToString() Then
                            '                bFound = True
                            '                Exit For
                            '            End If
                            '        Next
                            '    End If
                            'Next

                            'If bFound = False Then

                            Dim newEntry As New Contact()
                            newEntry.Name.FullName = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcFirstName")), "", dtContactInfo.Rows(0).Item("vcFirstName")).ToString() & " " & IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcLastName")), "", dtContactInfo.Rows(0).Item("vcLastName")).ToString()

                            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcCompanyName")) Then
                                If dtContactInfo.Rows(0).Item("vcCompanyName").ToString.Trim.Length > 0 Then
                                    Dim org As New Organization()
                                    org.Rel = ContactsRelationships.IsWork
                                    org.Title = dtContactInfo.Rows(0).Item("vcTitle").ToString()
                                    org.Name = dtContactInfo.Rows(0).Item("vcCompanyName").ToString()
                                    org.Primary = True
                                    newEntry.Organizations.Add(org)
                                End If
                            End If

                            Dim primaryEmail As New Google.GData.Extensions.EMail()
                            primaryEmail.Address = dtContactInfo.Rows(0).Item("vcEmail").ToString()
                            primaryEmail.Primary = True
                            primaryEmail.Rel = ContactsRelationships.IsWork
                            newEntry.Emails.Add(primaryEmail)

                            If Not IsDBNull(dtContactInfo.Rows(0).Item("numPhone")) Then
                                If dtContactInfo.Rows(0).Item("numPhone").ToString.Trim.Length > 0 Then
                                    Dim phoneNumber As New PhoneNumber(dtContactInfo.Rows(0).Item("numPhone").ToString())
                                    phoneNumber.Primary = True
                                    phoneNumber.Rel = ContactsRelationships.IsWork
                                    newEntry.Phonenumbers.Add(phoneNumber)
                                End If
                            End If

                            If Not IsDBNull(dtContactInfo.Rows(0).Item("numCell")) Then
                                If dtContactInfo.Rows(0).Item("numCell").ToString.Trim.Length > 0 Then
                                    Dim phoneNumber As New PhoneNumber(dtContactInfo.Rows(0).Item("numCell").ToString())
                                    phoneNumber.Primary = False
                                    phoneNumber.Rel = ContactsRelationships.IsMobile
                                    newEntry.Phonenumbers.Add(phoneNumber)
                                End If
                            End If

                            If Not IsDBNull(dtContactInfo.Rows(0).Item("vcFax")) Then
                                If dtContactInfo.Rows(0).Item("vcFax").ToString.Trim.Length > 0 Then
                                    Dim phoneNumber As New PhoneNumber(dtContactInfo.Rows(0).Item("vcFax").ToString())
                                    phoneNumber.Primary = False
                                    phoneNumber.Rel = ContactsRelationships.IsWorkFax
                                    newEntry.Phonenumbers.Add(phoneNumber)
                                End If
                            End If

                            Dim postalAddress As New StructuredPostalAddress()
                            postalAddress.Street = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcpStreet")), "", dtContactInfo.Rows(0).Item("vcpStreet")).ToString()
                            postalAddress.City = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPCity")), "", dtContactInfo.Rows(0).Item("vcpCity")).ToString()
                            postalAddress.Region = IIf(IsDBNull(dtContactInfo.Rows(0).Item("State")), "", dtContactInfo.Rows(0).Item("State")).ToString()
                            postalAddress.Postcode = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcPPostalCode")), "", dtContactInfo.Rows(0).Item("vcPPostalCode")).ToString()
                            postalAddress.Country = IIf(IsDBNull(dtContactInfo.Rows(0).Item("Country")), "", dtContactInfo.Rows(0).Item("Country")).ToString()
                            postalAddress.Primary = True
                            postalAddress.Rel = ContactsRelationships.IsWork
                            newEntry.PostalAddresses.Add(postalAddress)

                            Dim dtUserAccessDetails As DataTable

                            objUserAccess = New UserAccess
                            objUserAccess.UserId = UserContactID
                            objUserAccess.DomainID = numDomainId
                            objUserAccess.GroupType = 1
                            dtUserAccessDetails = objUserAccess.GetGoogleContactGroups

                            If dtUserAccessDetails.Rows.Count > 0 Then
                                Dim group As New GroupMembership
                                group.HRef = "http://www.google.com/m8/feeds/groups/" & username & "/base/" & dtUserAccessDetails.Rows(0).Item("vcGroupId").ToString()

                                newEntry.GroupMembership.Add(group)
                            End If

                            Dim feedUri As New Uri(ContactsQuery.CreateContactsUri("default"))

                            Dim createdEntry As Contact = cr.Insert(feedUri, newEntry)

                            Return "Google Contact added successfully!!!"
                            'Else
                            'Return "Google Contact already exists!!!"
                            'End If
                        End If
                    End If
                    'Dim query As New ContactsQuery(ContactsQuery.CreateContactsUri("default"))
                    'query.Query = "name='Carl Zaldivar'"


                    'Dim feed As Feed(Of Contact) = cr.[Get](Of Contact)(query)
                    'For Each contact As Contact In feed.Entries
                    '    Console.WriteLine(contact.Title)
                    '    Console.WriteLine("Updated on: " & contact.Updated.ToString())

                    '    Return "Google Contact added successfully!!!"
                    'Next

                Catch ex1 As InvalidCredentialsException
                    Return "Invalid Credential for Google Contacts:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End Try
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
