﻿Imports BACRM.BusinessLogic.Common
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Drawing

Public Class AccountProspectContactGrid
    Implements ITemplate

    Dim TemplateType As ListItemType
    Dim ColumnName, FieldName, DBColumnName, OrigDBColumnName, ControlType, LookBackTableName, tableAlias, ListType, SortColumnName, SortColumnOrder As String
    Dim Custom As Boolean
    Dim EditPermission, ColumnWidth As Integer
    Dim ListID, ListRelID, FormID, FormFieldId As Long
    Dim htGridColumnSearch As Hashtable
    Dim objCommon As New CCommon

    Sub New(ByVal type As ListItemType, ByVal drRow As DataRow)
        Try
            TemplateType = type

            FieldName = drRow("vcFieldName").ToString
            DBColumnName = drRow("vcDbColumnName").ToString
            OrigDBColumnName = drRow("vcOrigDbColumnName").ToString
            FormFieldId = CCommon.ToLong(drRow("numFieldId"))
            Custom = CCommon.ToBool(drRow("bitCustomField"))
            ControlType = drRow("vcAssociatedControlType").ToString
            ListType = drRow("vcListItemType").ToString
            ListID = CCommon.ToLong(drRow("numListID"))
            ListRelID = CCommon.ToLong(drRow("ListRelID"))
            ColumnWidth = CCommon.ToInteger(drRow("intColumnWidth"))
            LookBackTableName = drRow("vcLookBackTableName").ToString

            If LookBackTableName = "CompanyInfo" Then
                tableAlias = "cmp"
            ElseIf LookBackTableName = "DivisionMaster" Then
                tableAlias = "DM"
            ElseIf LookBackTableName = "AdditionalContactsInformation" Then
                tableAlias = "ADC"
            ElseIf LookBackTableName = "ProjectProgress" Then
                tableAlias = "PP"
            ElseIf LookBackTableName = "Projects" Then
                tableAlias = "Pro"
            ElseIf LookBackTableName = "Cases" Then
                tableAlias = "cs"
            ElseIf LookBackTableName = "OpportunityMaster" Then
                tableAlias = "Opp"
            ElseIf LookBackTableName = "OpportunityRecurring" Then
                tableAlias = "OPR"
            ElseIf LookBackTableName = "AddressDetails" Then
                tableAlias = "AD"
            ElseIf Custom = True Then
                tableAlias = "CFW"
            End If

            ColumnName = DBColumnName & "~" & FormFieldId & "~" & IIf(Custom, 1, 0).ToString

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
        Try
            Dim lbl1 As Label = New Label()
            Dim lbl2 As Label = New Label()
            Dim lbl3 As Label = New Label()
            Dim lnkButton As New LinkButton
            Dim lnk As New HyperLink
            Select Case TemplateType
                Case ListItemType.Header
                    Dim cell As DataControlFieldHeaderCell = CType(Container, DataControlFieldHeaderCell)

                    If ControlType <> "Delete" Then
                        lbl1.ID = FormFieldId & DBColumnName
                        lbl1.Text = FieldName
                        Container.Controls.Add(lbl1)
                    End If

                Case ListItemType.Item

                    If ControlType <> "Delete" Then
                        AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                        Container.Controls.Add(lbl1)
                    Else
                        Dim btnDelete As New Button
                        btnDelete.ID = "btnDelete"
                        btnDelete.CommandName = "DeleteContact"
                        btnDelete.CssClass = "button Delete"
                        btnDelete.Text = "X"
                        AddHandler btnDelete.DataBinding, AddressOf BindbtnDelete
                        Container.Controls.Add(btnDelete)

                        Dim lnkdelete As New LinkButton
                        lnkdelete.ID = "lnkdelete"
                        lnkdelete.Text = "<font color='#730000'>*</font>"
                        lnkdelete.Visible = False
                        Container.Controls.Add(lnkdelete)
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindbtnDelete(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim btnDelete As Button = CType(Sender, Button)

            Dim Container As GridViewRow = CType(btnDelete.NamingContainer, GridViewRow)

            btnDelete.CommandArgument = DataBinder.Eval(Container.DataItem, "numContactID").ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)

            Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)

            If DBColumnName = "vcEmail" Or DBColumnName = "vcCompanyName" Or DBColumnName = "vcFirstName" Or DBColumnName = "vcLastName" Or DBColumnName = "vcWebSite" _
                Or DBColumnName = "numRecOwner" Or DBColumnName = "numContactId" Or DBColumnName = "NewActionItem" Or DBColumnName = "Last10ActionItemOpened" Or DBColumnName = "Last10ActionItemClosed" Then

                If Not (DBColumnName = "NewActionItem" Or DBColumnName = "Last10ActionItemOpened" Or DBColumnName = "Last10ActionItemClosed") Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName).ToString), "", DataBinder.Eval(Container.DataItem, ColumnName).ToString).ToString
                End If

                Dim intermediatory As Integer
                intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)

                If DBColumnName = "vcCompanyName" Then
                    lbl1.Text = "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, "numDivisionID").ToString & "," & DataBinder.Eval(Container.DataItem, "tintCRMType").ToString & "," & intermediatory & ")'>" & lbl1.Text & "</a>"
                ElseIf DBColumnName = "vcFirstName" Or DBColumnName = "vcLastName" Or DBColumnName = "numContactId" Then
                    lbl1.Text = "<a  href='javascript:OpenContact(" & DataBinder.Eval(Container.DataItem, "numContactID").ToString & "," & intermediatory & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "numRecOwner" Then
                    lbl1.Text = "<a  href='javascript:OpenContact(" & DataBinder.Eval(Container.DataItem, "numRecOwner").ToString & "," & intermediatory & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "vcEmail" Then
                    lbl1.Attributes.Add("onclick", "return OpemEmail('" & lbl1.Text.ToString & "'," & DataBinder.Eval(Container.DataItem, "numContactID").ToString & ")")
                    lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "vcWebSite" Then
                    lbl1.Attributes.Add("onclick", "return fn_GoToURL('" & lbl1.Text.ToString & "')")
                    lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "NewActionItem" Then
                    lbl1.Attributes.Add("onclick", "return OpenNewAction(" & DataBinder.Eval(Container.DataItem, "numContactID").ToString & ")")
                    lbl1.Text = "<a  href=#>Action</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "Last10ActionItemOpened" Then
                    lbl1.Attributes.Add("onclick", "return OpenLst('" & "../ActionItems/frmLast10ActionItems.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&cntid=" & DataBinder.Eval(Container.DataItem, "numContactID").ToString & "&type=0" & "')")
                    lbl1.Text = "<a  href=#>Opened</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "Last10ActionItemClosed" Then
                    lbl1.Attributes.Add("onclick", "return OpenLst('" & " ../ActionItems/frmLast10ActionItems.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&cntid=" & DataBinder.Eval(Container.DataItem, "numContactID").ToString & "&type=1" & "')")
                    lbl1.Text = "<a  href=#>Closed</a> &nbsp;&nbsp;&nbsp;"
                End If
            Else
                If ControlType = "CheckBox" Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName).ToString), "", IIf(CCommon.ToBool(DataBinder.Eval(Container.DataItem, ColumnName)), "Yes", "No").ToString).ToString
                Else
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
