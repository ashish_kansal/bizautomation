﻿Imports BACRM.BusinessLogic.Common
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System
Imports System.Data
Imports System.IO
Imports Ionic.Zip
Imports DocumentFormat.OpenXml
Imports DocumentFormat.OpenXml.Packaging
Imports System.Collections.Generic

Public Class CSVExport
    Inherits BACRM.BusinessLogic.CBusinessBase
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DataSetToExcel(ByVal ds As DataSet, strFileName As String, lngDomainID As Long) As String
        Try
            Dim destination As String = CCommon.GetDocumentPhysicalPath(lngDomainID) & strFileName

            Using workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook)
                Dim workbookPart = workbook.AddWorkbookPart()
                workbook.WorkbookPart.Workbook = New DocumentFormat.OpenXml.Spreadsheet.Workbook()
                workbook.WorkbookPart.Workbook.Sheets = New DocumentFormat.OpenXml.Spreadsheet.Sheets()
                Dim sheetId As UInteger = 1

                For Each table As DataTable In ds.Tables
                    Dim sheetPart = workbook.WorkbookPart.AddNewPart(Of WorksheetPart)()
                    sheetPart.Worksheet = New DocumentFormat.OpenXml.Spreadsheet.Worksheet(New Spreadsheet.SheetData())
                    Dim sheets As DocumentFormat.OpenXml.Spreadsheet.Sheets = workbook.WorkbookPart.Workbook.GetFirstChild(Of DocumentFormat.OpenXml.Spreadsheet.Sheets)()
                    Dim sheetData As Spreadsheet.SheetData = sheetPart.Worksheet.GetFirstChild(Of Spreadsheet.SheetData)()

                    Dim relationshipId As String = workbook.WorkbookPart.GetIdOfPart(sheetPart)

                    If sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().Count() > 0 Then
                        sheetId = sheets.Elements(Of DocumentFormat.OpenXml.Spreadsheet.Sheet)().[Select](Function(s) s.SheetId.Value).Max() + 1
                    End If

                    Dim sheet As DocumentFormat.OpenXml.Spreadsheet.Sheet = New DocumentFormat.OpenXml.Spreadsheet.Sheet() With {
                        .Id = relationshipId,
                        .SheetId = sheetId,
                        .Name = table.TableName
                    }
                    sheets.Append(sheet)
                    Dim headerRow As DocumentFormat.OpenXml.Spreadsheet.Row = New DocumentFormat.OpenXml.Spreadsheet.Row()
                    Dim columns As List(Of String) = New List(Of String)()

                    For Each column As DataColumn In table.Columns
                        columns.Add(column.ColumnName)
                        Dim cell As DocumentFormat.OpenXml.Spreadsheet.Cell = New DocumentFormat.OpenXml.Spreadsheet.Cell()
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String
                        cell.CellValue = New DocumentFormat.OpenXml.Spreadsheet.CellValue(column.ColumnName)
                        headerRow.AppendChild(cell)
                    Next

                    sheetData.AppendChild(headerRow)

                    For Each dsrow As DataRow In table.Rows
                        Dim newRow As DocumentFormat.OpenXml.Spreadsheet.Row = New DocumentFormat.OpenXml.Spreadsheet.Row()

                        For Each col As String In columns
                            Dim cell As DocumentFormat.OpenXml.Spreadsheet.Cell = New DocumentFormat.OpenXml.Spreadsheet.Cell()

                            If table.Columns(col).DataType = GetType(Double) Or table.Columns(col).DataType = GetType(Decimal) Then
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number
                            Else
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String
                            End If

                            cell.CellValue = New DocumentFormat.OpenXml.Spreadsheet.CellValue(dsrow(col).ToString())
                            newRow.AppendChild(cell)
                        Next

                        sheetData.AppendChild(newRow)
                    Next
                Next
            End Using

            Return strFileName
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Shared Sub ProduceCSV(ByVal dt As DataTable, ByVal httpStream As System.IO.TextWriter, ByVal WriteHeader As Boolean)
        If WriteHeader Then
            Dim arr As String() = New [String](dt.Columns.Count - 1) {}
            For i As Integer = 0 To dt.Columns.Count - 1
                arr(i) = dt.Columns(i).ColumnName
                arr(i) = GetWriteableValue(arr(i))
            Next

            httpStream.WriteLine(String.Join(",", arr))
        End If

        For j As Integer = 0 To dt.Rows.Count - 1
            Dim dataArr As String() = New [String](dt.Columns.Count - 1) {}
            For i As Integer = 0 To dt.Columns.Count - 1
                Dim o As Object = dt.Rows(j)(i)
                dataArr(i) = GetWriteableValue(o)
            Next
            httpStream.WriteLine(String.Join(",", dataArr))
        Next
    End Sub

#Region "CSV Producer"
    Public Shared Sub ProduceCSV(ByVal dt As DataTable, ByVal file As System.IO.StreamWriter, ByVal WriteHeader As Boolean)
        If WriteHeader Then
            Dim arr As String() = New [String](dt.Columns.Count - 1) {}
            For i As Integer = 0 To dt.Columns.Count - 1
                arr(i) = dt.Columns(i).ColumnName
                arr(i) = GetWriteableValue(arr(i))
            Next

            file.WriteLine(String.Join(",", arr))
        End If

        For j As Integer = 0 To dt.Rows.Count - 1
            Dim dataArr As String() = New [String](dt.Columns.Count - 1) {}
            For i As Integer = 0 To dt.Columns.Count - 1
                Dim o As Object = dt.Rows(j)(i)

                'If IsNumeric(o) = False And IsDate(o) = False And IsDBNull(o) = False Then
                '    o = Replace(o, """", """""")
                'End If

                dataArr(i) = GetWriteableValue(o)
            Next
            file.WriteLine(String.Join(",", dataArr))
        Next
        file.Flush()
        file.Close()
    End Sub

    Public Shared Function GetWriteableValue(ByVal o As Object) As String
        'using special characte † to replace the "," 

        If IsDBNull(o) Then
            Return ""
        ElseIf o Is Nothing Then
            Return ""
            'ElseIf o.ToString().IndexOf(Environment.NewLine) = -1 Then
            '    Return """" & o.ToString() & """"
            'ElseIf o.ToString().IndexOf(",") = -1 Then
            '    Return o.ToString().Replace(Environment.NewLine, "")
        Else
            'http://en.wikipedia.org/wiki/Comma-separated_values
            Return """" & o.ToString().Replace(Environment.NewLine, "").Replace("""", """""") & """"
        End If
    End Function

    Public Function ZipFiles(ByVal files As ArrayList) As String
        Try


            Dim archiveName As String = Path.GetTempFileName() & String.Format("archive-{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"))
            Using zip As New ZipFile()
                For i As Integer = 0 To files.Count - 1
                    If CCommon.ToString(files(i)).Length > 0 Then
                        zip.AddFile(files(i).ToString, "")
                    End If
                Next
                zip.Save(archiveName)
            End Using

            Return archiveName

            'Dim objZip As New Zip
            'objZip.ArchiveFile = Path.GetTempFileName() & "files.zip"
            'objZip.RecurseSubdirectories = False


            'For i As Integer = 0 To files.Count - 1
            '    objZip.IncludeFiles(files(i).ToString)
            'Next

            'objZip.Compress()

            'Return objZip.ArchiveFile
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ExtractFiles(ByVal ExtractToPath As String, ByVal ZipFilePath As String, ByVal Overwrite As Boolean) As ArrayList
        Try
            Dim objZip As New ZipFile

            If Directory.Exists(ExtractToPath) Then

                Dim strImages As ArrayList = New ArrayList()

                Using zip1 As ZipFile = ZipFile.Read(ZipFilePath)
                    'AddHandler zip1.ExtractProgress, AddressOf MyExtractProgress
                    Dim e As ZipEntry
                    ' here, we extract every entry, but we could extract    
                    ' based on entry name, size, date, etc.   
                    For Each e In zip1
                        If Not (e.FileName.ToLower().EndsWith(".aspx") Or e.FileName.ToLower().EndsWith(".config") Or e.FileName.ToLower().EndsWith(".ascx") Or e.FileName.ToLower().EndsWith(".dll") Or e.FileName.ToLower().EndsWith(".exe") Or e.FileName.ToLower().EndsWith(".asax")) Then
                            e.Extract(ExtractToPath, ExtractExistingFileAction.OverwriteSilently)
                        End If

                        If (File.Exists(ExtractToPath & e.FileName)) Then
                            Dim strExt As String = Path.GetExtension(ExtractToPath & e.FileName).ToLower()
                            If (strExt = ".jpg" Or strExt = ".png" Or strExt = ".jpeg" Or strExt = ".gif" Or strExt = ".bmp") Then
                                strImages.Add(e.FileName)
                            End If
                        End If

                    Next

                End Using


                Return strImages
            Else
                Throw New Exception("Folder Doesn't Exist")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private _ExportTables As String
    Public Property ExportTables() As String
        Get
            Return _ExportTables
        End Get
        Set(ByVal value As String)
            _ExportTables = value
        End Set
    End Property



    Private _Mode As Short
    Public Property Mode() As Short
        Get
            Return _Mode
        End Get
        Set(ByVal value As Short)
            _Mode = value
        End Set
    End Property

    Private _Table As Short
    Public Property Table() As Short
        Get
            Return _Table
        End Get
        Set(ByVal value As Short)
            _Table = value
        End Set
    End Property

    Private _FromDate As DateTime
    Public Property FromDate() As DateTime
        Get
            Return _FromDate
        End Get
        Set(ByVal value As DateTime)
            _FromDate = value
        End Set
    End Property

    Private _ToDate As DateTime
    Public Property ToDate() As DateTime
        Get
            Return _ToDate
        End Get
        Set(ByVal value As DateTime)
            _ToDate = value
        End Set
    End Property

    Public Function GetExportDataSettings() As DataSet
        Try
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = DomainID

            arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(1).Value = Nothing
            arParms(1).Direction = ParameterDirection.InputOutput

            Return SqlDAL.ExecuteDataset(connString, "USP_GetDataToExport", arParms)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function ExportData() As DataSet
        Try
            Dim ds As New DataSet
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            connString = connString.TrimEnd(CChar(";")) & ";Timeout=1000" 'Also set  command.CommandTimeout in Method PrepareCommand in SqlDAL.vb
            Using conn As New Npgsql.NpgsqlConnection(connString) 'in seconds 
                Dim cmd As New Npgsql.NpgsqlCommand()
                cmd.Connection = conn
                cmd.CommandText = "usp_exportdatabackup"
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandTimeout = 1800 'don't if this is seconds or mili seconds

                cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("v_tinttable", _Table, NpgsqlTypes.NpgsqlDbType.Smallint))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("v_dtfromdate", _FromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("v_dttodate", _ToDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur2", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur3", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur4", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))


                Dim adp As New Npgsql.NpgsqlDataAdapter
                Dim i As Int32 = 0
                conn.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = conn.BeginTransaction()
                    cmd.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In cmd.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                adp = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), conn)
                                ds.Tables.Add(parm.Value.ToString())
                                adp.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                conn.Close()
            End Using

            Return ds
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ManageExportSettings() As Boolean
        Try
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = DomainID

            arParms(1) = New Npgsql.NpgsqlParameter("@vcExportTables", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
            arParms(1).Value = _ExportTables

            SqlDAL.ExecuteNonQuery(connString, "USP_ManageExportData", arParms)
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class
