﻿Option Explicit On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Namespace BACRM.BusinessLogic.Common


    Public Class ExceptionPublisher
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _Message As String


        Public Property Message() As String
            Get
                Return _Message
            End Get
            Set(ByVal value As String)
                _Message = value
            End Set
        End Property

        Private _URL As String


        Public Property URL() As String
            Get
                Return _URL
            End Get
            Set(ByVal value As String)
                _URL = value
            End Set
        End Property

        Private _Referrer As String


        Public Property Referrer() As String
            Get
                Return _Referrer
            End Get
            Set(ByVal value As String)
                _Referrer = value
            End Set
        End Property

        Private _Browser As String

        Public Property Browser() As String
            Get
                Return _Browser
            End Get
            Set(ByVal value As String)
                _Browser = value
            End Set
        End Property

        Private _BrowserAgent As String

        Public Property BrowserAgent() As String
            Get
                Return _BrowserAgent
            End Get
            Set(ByVal value As String)
                _BrowserAgent = value
            End Set
        End Property


        'Private DomainId As Long

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Long)
        '        DomainId = value
        '    End Set
        'End Property


        ''Private _UserContactID As Long

        ''Public Property UserContactID() As Long
        '    Get
        '        Return _UserContactID
        '    End Get
        '    Set(ByVal value As Long)
        '        _UserContactID = value
        '    End Set
        'End Property



        Public Function ManageException() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(0).Value = _Message

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(3).Value = _URL

                arParms(4) = New Npgsql.NpgsqlParameter("@vcReferrer", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(4).Value = _Referrer

                arParms(5) = New Npgsql.NpgsqlParameter("@vcBrowser", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Value = _Browser

                arParms(6) = New Npgsql.NpgsqlParameter("@vcBrowserAgent", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(6).Value = _BrowserAgent


                SqlDAL.ExecuteNonQuery(connString, "USP_ManageException", arParms)

                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function


    End Class
End Namespace
