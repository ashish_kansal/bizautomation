﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient

Namespace BACRM.BusinessLogic.Common
    Public Class RecordHistory
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"
        Public RecordID As Long
        Public ModuleID As Short
        Public PageSize As Int32
        Public PageNumber As Int32
        Public TotalRecords As Int32
        Public filterEvent As String
        Public filterUserName As String
        Public filterFieldName As String
        Public filterUser As String
        Public filterFromDate As Date?
        Public filterToDate As Date?
#End Region

#Region "Public Methods"
        Public Function GetHistory() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ModuleID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRecordID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = RecordID

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@PageNumber", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = PageNumber
                arParms(5) = New Npgsql.NpgsqlParameter("@v_dtFilterFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = filterFromDate

                arParms(6) = New Npgsql.NpgsqlParameter("@v_dtFilterToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(6).Value = filterToDate
                arParms(7) = New Npgsql.NpgsqlParameter("@TotalRecords", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Direction = ParameterDirection.InputOutput
                arParms(7).Value = TotalRecords

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput




                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(GetConnection.GetConnectionString, "USP_RecordHistory_Get", objParam, True)
                TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub FetchDataFromCDC()
            Try
                SqlDAL.ExecuteNonQuery(GetConnection.GetConnectionString, "USP_RecordHistory_FetchFromCDC", Nothing)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetRecordHistoryStatus() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(GetConnection.GetConnectionString, "USP_RecordHistoryStatus_Get", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub UpdateRecordHistoryStatus(ByVal isSuccess As Boolean)
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@bitSuccess", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(0).Value = isSuccess

                SqlDAL.ExecuteNonQuery(GetConnection.GetConnectionString, "USP_RecordHistoryStatus_Update", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region
    End Class
End Namespace