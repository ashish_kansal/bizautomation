﻿
Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports Lucene.Net
Imports Lucene.Net.Analysis
Imports Lucene.Net.Index
Imports Lucene.Net.Documents
Imports Lucene.Net.QueryParsers
Imports Lucene.Net.Search
Imports System.IO
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.ShioppingCart
Imports Microsoft.Web.Administration
Imports System.ServiceProcess
Imports System.Threading


Namespace BACRM.BusinessLogic.Common

    Public Class SearchLucene
        Inherits BACRM.BusinessLogic.CBusinessBase

        'Public strIndexPath As String
        'Public Sub New(ByVal IndexPath As String)
        '    If Not Directory.Exists(IndexPath) Then
        '        Directory.CreateDirectory(IndexPath)
        '    End IfIn
        '    strIndexPath = IndexPath
        'End Sub
        Shared analyzer As New CaseInsensitiveWhitespaceAnalyzer()
        Shared searcher As Lucene.Net.Search.Searcher = Nothing
        Shared my_lock As Object = ""



        Public Shared Function LuceneEscapeSpecialChar(ByVal strInput As String, Optional ByVal LookForExactMatch As Boolean = True) As String
            Try
                ''*' or '?' not allowed as first character in WildcardQuery 
                Dim strOutput As String
                strOutput = strInput.TrimStart(Char.Parse("*"), Char.Parse("?"))
                strOutput = strOutput.Replace("?", "") ' Do not allow ? character as combination of ? and * creates error
                strOutput = strOutput.Replace("""", "") 'replace double quote 
                strOutput = strOutput.Replace("\", "\\").Replace("+", "\+").Replace("-", "\-").Replace(":", "\:").Replace("?", "\?").Replace("&&", "\&&").Replace("||", "\||").Replace("!", "\!").Replace("(", "\(").Replace(")", "\)").Replace("{", "\{").Replace("}", "\}").Replace("[", "\[").Replace("]", "\]").Replace("^", "\^").Replace("~", "\~").ToString()
                Dim chrSplit() As Char = {CChar(" ")}
                Dim strTerms() As String = strOutput.Split(chrSplit, StringSplitOptions.RemoveEmptyEntries)
                'when search has multiple words
                If strTerms.Length > 1 Then
                    If LookForExactMatch Then
                        'Search for exact match
                        'strOutput = """" & strOutput & "*""^2 " ' Whole word matching has more importance than individual

                        strOutput = ""
                        For i As Integer = 0 To strTerms.Length - 1
                            strOutput = strOutput & "+" & strTerms(i).TrimEnd(Char.Parse("*")) & "* "
                        Next
                    Else
                        'When Exact match not found split string and search.
                        'do wildcard search for each term entered
                        strOutput = ""
                        For i As Integer = 0 To strTerms.Length - 1
                            strOutput = strOutput & strTerms(i).TrimEnd(Char.Parse("*")) & "* OR "
                        Next
                        Dim charToTrim As Char() = {CChar("R"), CChar("O")}
                        strOutput = strOutput.Trim().TrimEnd(charToTrim)
                    End If
                Else 'single word  wild card search
                    strOutput = "*" & strOutput.TrimEnd(Char.Parse("*")) & "*"
                End If

                'strOutput = strOutput.Trim()

                Return strOutput
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Sub IndexData(ByVal strIndexPath As String, ByVal dt As DataTable, ByVal enmIT As Common.IndexType, Optional ByVal IsUpdateDocument As Boolean = False)
            SyncLock my_lock ' If a thread is updating the index, no other thread should be doing anything with it.
                Try

                    If dt.Rows.Count > 0 Then

                        Dim directory As Store.Directory = Store.FSDirectory.Open(New DirectoryInfo(strIndexPath))
                        Dim writer As New IndexWriter(directory, analyzer, True, IndexWriter.MaxFieldLength.UNLIMITED)

                        Dim strText As String = ""
                        For Each dr As DataRow In dt.Rows
                            If enmIT = IndexType.Items Then
                                Dim doc As New Lucene.Net.Documents.Document
                                doc.Add(New Field("ItemID", dr("numItemCode").ToString, Field.Store.YES, Field.Index.NOT_ANALYZED))

                                'Add custom field here 
                                Dim ColumnName As String
                                For i As Integer = 1 To dr.Table.Columns.Count - 1
                                    ColumnName = dt.Columns(i).ColumnName
                                    If ColumnName.IndexOf("_CustomField") > 0 Then
                                        ColumnName = dt.Columns(i).ColumnName.Replace("_CustomField", "")
                                        doc.Add(New Field(ColumnName.Replace(" ", ""), CCommon.ToString(dr(ColumnName + "_CustomField")), Field.Store.NO, Field.Index.ANALYZED))
                                    Else
                                        If ColumnName.IndexOf("Long Description") >= 0 Then
                                            doc.Add(New Field(ColumnName.Replace(" ", ""), System.Web.HttpContext.Current.Server.HtmlDecode(CCommon.ToString(dr(ColumnName))), Field.Store.NO, Field.Index.ANALYZED))
                                        Else
                                            doc.Add(New Field(ColumnName.Replace(" ", ""), CCommon.ToString(dr(ColumnName)), Field.Store.NO, Field.Index.ANALYZED))
                                        End If
                                    End If
                                Next
                                writer.AddDocument(doc)
                            End If
                        Next
                        writer.Optimize()
                        writer.Close()
                    End If


                Catch ex As Exception
                    Throw ex
                End Try
            End SyncLock
        End Sub
        Public Shared Sub UpdateIndexData(ByVal strIndexPath As String, ByVal dt As DataTable, ByVal enmIT As Common.IndexType)
            SyncLock my_lock

                Dim writer As IndexWriter
                Try
                    'Close existing searcher if any .. so it will not create seperate index when updating index
                    If Not searcher Is Nothing Then
                        Try
                            searcher.Close()
                        Catch ex As Exception
                            Throw ex
                        End Try
                        searcher = Nothing
                    End If
                    Dim directory As Store.Directory = Store.FSDirectory.Open(New DirectoryInfo(strIndexPath))
                    writer = New IndexWriter(directory, analyzer, False, IndexWriter.MaxFieldLength.UNLIMITED)

                    Dim strText As String = ""
                    For Each dr As DataRow In dt.Rows
                        If enmIT = IndexType.Items Then
                            Dim trm As New Index.Term("ItemID", dr("numItemCode").ToString)

                            Dim doc As New Lucene.Net.Documents.Document
                            doc.Add(New Field("ItemID", dr("numItemCode").ToString, Field.Store.YES, Field.Index.NOT_ANALYZED))

                            'Add custom field here 
                            Dim ColumnName As String
                            For i As Integer = 1 To dr.Table.Columns.Count - 1
                                ColumnName = dt.Columns(i).ColumnName
                                If ColumnName.IndexOf("_CustomField") > 0 Then
                                    ColumnName = dt.Columns(i).ColumnName.Replace("_CustomField", "")
                                    doc.Add(New Field(ColumnName.Replace(" ", ""), CCommon.ToString(dr(ColumnName + "_CustomField")), Field.Store.NO, Field.Index.ANALYZED))
                                Else
                                    If ColumnName.IndexOf("Long Description") >= 0 Then
                                        doc.Add(New Field(ColumnName.Replace(" ", ""), System.Web.HttpUtility.HtmlDecode(CCommon.ToString(dr(ColumnName))), Field.Store.NO, Field.Index.ANALYZED))
                                    Else
                                        doc.Add(New Field(ColumnName.Replace(" ", ""), CCommon.ToString(dr(ColumnName)), Field.Store.NO, Field.Index.ANALYZED))
                                    End If
                                End If
                            Next
                            writer.UpdateDocument(trm, doc, analyzer)
                        End If
                    Next

                    writer.Commit()
                    writer.Optimize()

                Catch ex As Exception
                    Throw ex
                Finally
                    writer.Close()
                End Try
            End SyncLock
        End Sub

        Public Shared Sub DeleteIndex(ByVal strIndexPath As String, ByVal strDeleteValue As String, ByVal enmIT As Common.IndexType)
            SyncLock my_lock
                Try

                    'Dim writer As New IndexWriter(strIndexPath, analyzer, false, IndexWriter.MaxFieldLength.UNLIMITED)
                    Dim directory As Store.Directory = Store.FSDirectory.Open(New DirectoryInfo(strIndexPath))
                    Dim reader As IndexReader = IndexReader.Open(directory, False)

                    If enmIT = Common.IndexType.Items Then

                        Dim trm As New Index.Term("ItemID", strDeleteValue)

                        reader.DeleteDocuments(trm)
                        reader.Commit()
                        reader.Close()
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            End SyncLock
        End Sub

        Public Shared Function SearchIndex(ByVal strIndexPath As String, ByVal SearchTerm As String, ByVal enmIT As Common.IndexType, Optional ByVal IsAdvanceSearch As Boolean = False, Optional ByRef ActuallSearchString As String = "") As String
            SyncLock my_lock
                Try
                    Dim parser As QueryParser
                    Dim query As Query
                    Dim hits As Hits
                    searcher = New IndexSearcher(strIndexPath, True)

                    Dim objSearch As New Common.CSearch()
                    objSearch.DomainID = CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))
                    objSearch.SiteID = CCommon.ToLong(System.Web.HttpContext.Current.Session("SiteID"))
                    objSearch.byteMode = 0
                    objSearch.SchemaOnly = True
                    Dim dt As DataTable = objSearch.GetDataForIndexing()
                    Dim strBizCartSearchFields As String
                    For Each dr As DataRow In dt.Rows
                        strBizCartSearchFields = strBizCartSearchFields + CCommon.ToString(dr("vcFormFieldName")).Replace(" ", "") + ":(#SearchText#) "
                    Next

                    If Not IsAdvanceSearch Then
                        'build search condition
                        SearchTerm = strBizCartSearchFields.Replace("#SearchText#", SearchTerm)
                    End If
                    ActuallSearchString = SearchTerm

                    If enmIT = IndexType.Items Then
                        parser = New QueryParser(Util.Version.LUCENE_29, "LongDescription", analyzer)
                    End If
                    parser.SetAllowLeadingWildcard(True)
                    query = parser.Parse(SearchTerm)
                    hits = searcher.Search(query)

                    Dim sb As New System.Text.StringBuilder
                    For i As Integer = 0 To hits.Length - 1

                        If enmIT = IndexType.Items Then
                            Dim doc As Document = hits.Doc(i)
                            sb.Append(doc.GetField("ItemID").StringValue() & ",")
                        End If
                    Next
                    '8000 character can cover aprox 1000 item ids
                    If sb.ToString().Length > 8000 Then
                        Dim strTrimedIDs As String = TrimLength(sb.ToString(), 8000)
                        strTrimedIDs = strTrimedIDs.Substring(0, strTrimedIDs.LastIndexOf(CChar(",")))
                        Return strTrimedIDs
                    Else
                        Return sb.ToString().TrimEnd(Char.Parse(","))
                    End If


                Catch ex As Exception
                    Throw ex
                End Try
            End SyncLock
        End Function

        Public Shared Function ReIndexAllWebsiteOfDomain(ByVal DomainID As Long, ByVal siteID As Long, ByRef ErrorMessageToLog As String) As String
            Try
                Dim objSite As New Sites
                Dim objSearch As New CSearch
                Dim dtSites As DataTable

                objSite.DomainID = DomainID
              

                objSearch.DomainID = DomainID
                objSearch.byteMode = 0
                objSearch.AddUpdateMode = 1 'Add index data
                objSearch.DeleteAll = True
                objSearch.SiteID = siteID
                Dim writer As IndexWriter
                Try
                    Dim directory As Store.Directory = Store.FSDirectory.Open(New DirectoryInfo(CCommon.GetLuceneIndexPath(objSearch.DomainID, objSearch.SiteID)))
                    writer = New IndexWriter(directory, analyzer, True, IndexWriter.MaxFieldLength.UNLIMITED)
                    writer.DeleteAll()
                    writer.Close()

                    Dim ds As DataSet = objSearch.GetDataForIndexRebuild(1)

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                        UpdateIndexData(CCommon.GetLuceneIndexPath(objSearch.DomainID, objSearch.SiteID), ds.Tables(1), IndexType.Items)

                        Dim batchSize As Integer = CCommon.ToInteger(ds.Tables(0).Rows(0)("BatchSize"))
                        Dim totalRecords As Integer = CCommon.ToInteger(ds.Tables(0).Rows(0)("TotalRecords"))

                        If totalRecords > batchSize Then
                            Dim i As Integer = 1
                            While i < Math.Ceiling(Convert.ToDouble(totalRecords / batchSize))
                                ds = objSearch.GetDataForIndexRebuild(i + 1)

                                If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                                    UpdateIndexData(CCommon.GetLuceneIndexPath(objSearch.DomainID, objSearch.SiteID), ds.Tables(1), IndexType.Items)
                                End If

                                i = i + 1
                            End While
                        End If
                    End If
                Catch ex As Exception
                    ErrorMessageToLog = ex.Message

                    Return "Can not save results. Index are already used by site. Please try later"
                    Exit Function
                Finally
                    writer.Close()
                End Try


                Return ""
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function StartStopWebSite(webSiteName As String, start As Boolean, totalWait As Integer) As Boolean
            Try
                Dim specifiedWait As Integer = totalWait
                Dim serverManager As New ServerManager()

                Dim site As Site = (From s In serverManager.Sites Where s.Name = webSiteName).SingleOrDefault()
                If start Then
                    If site.State = ObjectState.Started Then
                        Return True
                    End If

                    site.Start()
                    While site.State <> ObjectState.Started AndAlso totalWait > 0
                        Thread.Sleep(1000)
                        totalWait -= 1
                    End While

                    Return site.State = ObjectState.Started
                End If

                If site.State = ObjectState.Stopped Then
                    Return True
                End If

                site.Stop()
                While site.State <> ObjectState.Stopped AndAlso totalWait > 0
                    Thread.Sleep(1000)
                    totalWait -= 1
                End While
                Return site.State = ObjectState.Stopped

            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class

    Public Class CaseInsensitiveWhitespaceAnalyzer
        Inherits Analyzer
        ''' <summary>
        ''' </summary>
        Public Overrides Function TokenStream(fieldName As String, reader As TextReader) As TokenStream
            Dim t As TokenStream = Nothing
            t = New WhitespaceTokenizer(reader)
            t = New LowerCaseFilter(t)

            Return t
        End Function
    End Class
End Namespace
