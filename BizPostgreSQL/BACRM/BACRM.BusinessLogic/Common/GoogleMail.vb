﻿Option Explicit On
Option Strict On
Imports System.Configuration
Imports System.Web.HttpContext
Imports System
Imports System.IO
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports System.Text.RegularExpressions
Imports System.Collections.Generic
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports Google.GData.Apps
Imports Google.GData.Extensions
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Leads

Namespace BACRM.BusinessLogic.Common
    Public Class GoogleMail
        Inherits BACRM.BusinessLogic.CBusinessBase

        Public Function BizToGoogleSPAM(ByVal UserContactID As Long, ByVal numDomainId As Long) As String
            Try
                Dim lastUID As Long
                Dim dtTable As New DataTable
                Dim dtCredientials As DataTable
                Dim objUserAccess As New UserAccess
                Dim objContact As New CContacts
                objUserAccess.UserCntID = UserContactID
                objUserAccess.DomainID = numDomainId
                dtCredientials = objUserAccess.GetImapDtls()

                Dim username As String
                Dim Password As String

                'Dim cr As ContactsRequest

                If dtCredientials.Rows.Count > 0 Then
                    If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                        Dim objCommon As New CCommon
                        Password = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                        username = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                        lastUID = CLng(dtCredientials.Rows(0).Item("LastUID"))
                    Else
                        Return "Imap Is not Integrated for Google Email SPAM"
                        Exit Function
                    End If
                Else
                    Return "Imap Is not Integrated for Google Email SPAM" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace
