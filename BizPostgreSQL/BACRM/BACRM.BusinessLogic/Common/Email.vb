﻿'Created by Anoop Jayaraj

Option Explicit On
Imports System.Configuration
Imports System.Web.HttpContext
Imports System.IO
Imports MailBee
Imports MailBee.Mime
Imports MailBee.Security
Imports MailBee.ImapMail
Imports MailBee.SmtpMail
Imports MailBee.SmtpMail.Smtp
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Outlook
Imports System.Text.RegularExpressions
Imports System.Collections.Generic
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRM.BusinessLogic.Marketing
Imports System.Web
Imports MailBee.Pop3Mail
Imports MailBee.DnsMX
Imports System.Text

Namespace BACRM.BusinessLogic.Common

    Public Class Email


        Private _BroadcastId As Long
        Public Property BroadcastId() As Long
            Get
                Return _BroadcastId
            End Get
            Set(ByVal value As Long)
                _BroadcastId = value
            End Set
        End Property
        Private _ShowError As Boolean = False
        Private _ErrorMessage As String = ""
        Public Property ErrorMessage() As String
            Get
                Return _ErrorMessage
            End Get
            Set(ByVal Value As String)
                _ErrorMessage = Value
            End Set
        End Property

        Public Property ShowError() As Boolean
            Get
                Return _ShowError
            End Get
            Set(ByVal Value As Boolean)
                _ShowError = Value
            End Set
        End Property

        Private _ClientTimeZoneOffset As Integer

        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public _PrivateKey As String = ""
        Public Property PrivateKey() As String
            Get
                Return _PrivateKey
            End Get
            Set(ByVal Value As String)
                _PrivateKey = Value
            End Set
        End Property
        Dim SessionObject As Dictionary(Of String, Object)
        Public Property ListAttachment As List(Of Tuple(Of String, String))
        ''' <summary>
        ''' each time we instantiate new object it will copy session values into Dictionary.
        ''' Purpose: In Asyncronus Methods/threads session values are not available
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub New()
            SessionObject = CopySession()
        End Sub

        Private Function IsValidEmailAddress(ByVal strEmailAddress As String) As Boolean
            If strEmailAddress = "" Then
                Return False
            Else
                Return Regex.IsMatch(strEmailAddress, "^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|[a-zA-Z]{2})$", RegexOptions.IgnorePatternWhitespace)

            End If
        End Function

        Private Function PrepareSmtpForGmail(ByVal lngDomainID As Long, ByVal lngUserCntID As Long, ByRef objSmtp As Smtp, ByVal dt As DataTable) As Boolean
            Try
                Dim token As New Google.Apis.Auth.OAuth2.Responses.TokenResponse() With {.AccessToken = CCommon.ToString(dt.Rows(0)("vcMailAccessToken")), .RefreshToken = CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))}
                Dim secrets As New Google.Apis.Auth.OAuth2.ClientSecrets() With {.ClientId = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientID")), .ClientSecret = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientSecret"))}
                Dim credentials As New Google.Apis.Auth.OAuth2.UserCredential(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = secrets}), "Bizautomation.com", token)

                If token.IsExpired(credentials.Flow.Clock) Then
                    Dim success As Boolean = credentials.RefreshTokenAsync(Threading.CancellationToken.None).Result

                    If success Then
                        Dim objUserAccess As New UserAccess
                        objUserAccess.DomainID = lngDomainID
                        objUserAccess.UserCntID = lngUserCntID
                        objUserAccess.UpdateMailRefreshToken(1, credentials.Token.AccessToken, credentials.Token.RefreshToken, 0)
                    End If
                End If

                Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), credentials.Token.AccessToken)
                Dim objServer As New SmtpServer()
                objServer.Name = "smtp.gmail.com"
                objServer.Port = 587
                objServer.AccountName = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                objServer.SslMode = MailBee.Security.SslStartupMode.UseStartTls
                objServer.AuthMethods = AuthenticationMethods.SaslOAuth2
                objServer.Password = xoauthKey
                objServer.Timeout = 300000
                objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                objSmtp.SmtpServers.Add(objServer)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Private Function PrepareSmtpForOffice365(ByVal lngDomainID As Long, ByVal lngUserCntID As Long, ByRef objSmtp As Smtp, ByRef strFrom As String, ByVal dt As DataTable) As Boolean
            Try
                If CCommon.ToBool(dt.Rows(0)("bitExpired")) Then
                    Dim office365MailAppClientID As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365MailAppClientID"))
                    Dim office365MailAppClientSecret As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365MailAppClientSecret"))
                    Dim office365MailRedirectURI As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365MailAppRedirectURL"))

                    Dim postData As String = ""
                    postData += "client_id" + "=" + HttpUtility.UrlEncode(office365MailAppClientID) + "&"
                    postData += "refresh_token" + "=" + HttpUtility.UrlEncode(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) + "&"
                    postData += "grant_type" + "=" + HttpUtility.UrlEncode("refresh_token") + "&"
                    postData += "client_secret" + "=" + HttpUtility.UrlEncode(office365MailAppClientSecret)

                    Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create("https://login.microsoftonline.com/common/oauth2/v2.0/token")
                    request.Method = "POST"
                    request.ContentType = "application/x-www-form-urlencoded"

                    Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
                    request.ContentLength = byteArray.Length

                    Dim dataStream As Stream = request.GetRequestStream()
                    dataStream.Write(byteArray, 0, byteArray.Length)
                    dataStream.Close()

                    Dim response As Net.HttpWebResponse = CType(request.GetResponse(), Net.HttpWebResponse)
                    Dim responseString As String = New StreamReader(response.GetResponseStream()).ReadToEnd()
                    response.Close()

                    Dim objOffice365TokenResponse As Newtonsoft.Json.Linq.JObject = CType(Newtonsoft.Json.JsonConvert.DeserializeObject(responseString), Newtonsoft.Json.Linq.JObject)

                    If Not objOffice365TokenResponse Is Nothing Then
                        Dim objUserAccess As New UserAccess
                        objUserAccess.DomainID = lngDomainID
                        objUserAccess.UserCntID = lngUserCntID
                        objUserAccess.UpdateMailRefreshToken(2, CCommon.ToString(objOffice365TokenResponse("access_token")), CCommon.ToString(objOffice365TokenResponse("refresh_token")), CCommon.ToInteger(objOffice365TokenResponse("expires_in")))

                        Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), CCommon.ToString(objOffice365TokenResponse("access_token")))
                        Dim objServer As New SmtpServer()
                        objServer.Name = "smtp.office365.com"
                        objServer.Port = 587
                        objServer.AccountName = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                        objServer.SslMode = MailBee.Security.SslStartupMode.UseStartTls
                        objServer.AuthMethods = AuthenticationMethods.SaslOAuth2
                        objServer.Password = xoauthKey
                        objServer.Timeout = 300000
                        objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                        objSmtp.SmtpServers.Add(objServer)                        
                        objSmtp.From.Email = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                        strFrom = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                    Else
                        Return False
                    End If
                Else
                    Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), CCommon.ToString(dt.Rows(0)("vcMailAccessToken")))
                    Dim objServer As New SmtpServer()
                    objServer.Name = "smtp.office365.com"
                    objServer.Port = 587
                    objServer.AccountName = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                    objServer.SslMode = MailBee.Security.SslStartupMode.UseStartTls
                    objServer.AuthMethods = AuthenticationMethods.SaslOAuth2
                    objServer.Password = xoauthKey
                    objServer.Timeout = 300000
                    objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                    objSmtp.SmtpServers.Add(objServer)
                    objSmtp.From.Email = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                    strFrom = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                End If

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Private Function PrepareImapForOffice365(ByVal lngDomainID As Long, ByVal lngUserCntID As Long, ByRef objImap As Imap, ByVal dt As DataTable) As Tuple(Of Boolean, String)
            Try
                objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto

                If CCommon.ToBool(dt.Rows(0)("bitExpired")) Then
                    Dim office365MailAppClientID As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365MailAppClientID"))
                    Dim office365MailAppClientSecret As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365MailAppClientSecret"))
                    Dim office365MailRedirectURI As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365MailAppRedirectURL"))

                    Dim postData As String = ""
                    postData += "client_id" + "=" + HttpUtility.UrlEncode(office365MailAppClientID) + "&"
                    postData += "refresh_token" + "=" + HttpUtility.UrlEncode(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) + "&"
                    postData += "grant_type" + "=" + HttpUtility.UrlEncode("refresh_token") + "&"
                    postData += "client_secret" + "=" + HttpUtility.UrlEncode(office365MailAppClientSecret)

                    Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create("https://login.microsoftonline.com/common/oauth2/v2.0/token")
                    request.Method = "POST"
                    request.ContentType = "application/x-www-form-urlencoded"

                    Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
                    request.ContentLength = byteArray.Length

                    Dim dataStream As Stream = request.GetRequestStream()
                    dataStream.Write(byteArray, 0, byteArray.Length)
                    dataStream.Close()

                    Dim response As Net.HttpWebResponse = CType(request.GetResponse(), Net.HttpWebResponse)
                    Dim responseString As String = New StreamReader(response.GetResponseStream()).ReadToEnd()
                    response.Close()

                    Dim objOffice365TokenResponse As Newtonsoft.Json.Linq.JObject = CType(Newtonsoft.Json.JsonConvert.DeserializeObject(responseString), Newtonsoft.Json.Linq.JObject)

                    If Not objOffice365TokenResponse Is Nothing Then
                        Dim objUserAccess As New UserAccess
                        objUserAccess.DomainID = lngDomainID
                        objUserAccess.UserCntID = lngUserCntID
                        objUserAccess.UpdateMailRefreshToken(2, CCommon.ToString(objOffice365TokenResponse("access_token")), CCommon.ToString(objOffice365TokenResponse("refresh_token")), CCommon.ToInteger(objOffice365TokenResponse("expires_in")))
                        Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), CCommon.ToString(objOffice365TokenResponse("access_token")))
                        Try
                            objImap.Connect("outlook.office365.com")
                            If Not objImap.Login(Nothing, xoauthKey, AuthenticationMethods.SaslOAuth2, AuthenticationOptions.None, Nothing) Then
                                Return New Tuple(Of Boolean, String)(False, "Imap login failed")
                            End If
                        Catch ex1 As Exception
                            Return New Tuple(Of Boolean, String)(False, "Imap login failed")
                        End Try
                    Else
                        Return New Tuple(Of Boolean, String)(False, "Not able to access acount.")
                    End If
                Else
                    Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), CCommon.ToString(dt.Rows(0)("vcMailAccessToken")))

                    Try
                        objImap.Connect("outlook.office365.com")
                        If Not objImap.Login(Nothing, xoauthKey, AuthenticationMethods.SaslOAuth2, AuthenticationOptions.None, Nothing) Then
                            Return New Tuple(Of Boolean, String)(False, "Imap login failed")
                        End If
                    Catch ex1 As Exception
                        Return New Tuple(Of Boolean, String)(False, "Imap login failed")
                    End Try
                End If

                Return New Tuple(Of Boolean, String)(True, "")
            Catch ex As Exception
                Return New Tuple(Of Boolean, String)(False, ex.Message)
            End Try
        End Function

        Public Function GetNewEmails(ByVal UserContactID As Long, ByVal numDomainId As Long, ByVal strFolderName As String) As String
            Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
            Dim i As Integer
            Dim lastUID As Long
            Dim objImap As New Imap
            objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
            Dim emailId As String
            Dim dtTable As New DataTable
            Try
                Dim isOauthTokenExists As Boolean = False
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = numDomainId
                objUserAccess.UserCntID = UserContactID
                Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                    emailId = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                    If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                        Dim token As New Google.Apis.Auth.OAuth2.Responses.TokenResponse() With {.AccessToken = CCommon.ToString(dt.Rows(0)("vcMailAccessToken")), .RefreshToken = CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))}
                        Dim secrets As New Google.Apis.Auth.OAuth2.ClientSecrets() With {.ClientId = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientID")), .ClientSecret = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientSecret"))}
                        Dim credentials As New Google.Apis.Auth.OAuth2.UserCredential(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = secrets}), "Bizautomation.com", token)

                        If token.IsExpired(credentials.Flow.Clock) Then
                            Dim isTokenRefereshed As Boolean = credentials.RefreshTokenAsync(Threading.CancellationToken.None).Result

                            If isTokenRefereshed Then
                                objUserAccess.UpdateMailRefreshToken(1, credentials.Token.AccessToken, credentials.Token.RefreshToken, 0)
                            End If
                        End If

                        Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), credentials.Token.AccessToken)

                        objImap.Connect("imap.gmail.com")


                        Try
                            If Not objImap.Login(Nothing, xoauthKey, AuthenticationMethods.SaslOAuth2, AuthenticationOptions.None, Nothing) Then
                                Return "Could Not Connect To the Server" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                                Exit Function
                            End If
                        Catch ex1 As Exception
                            _ShowError = True
                            _ErrorMessage = ex1.Message
                            Return ex1.Message & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                            Exit Function
                        End Try


                        isOauthTokenExists = True

                    ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                        Dim objTuple As Tuple(Of Boolean, String) = PrepareImapForOffice365(numDomainId, UserContactID, objImap, dt)

                        If Not CCommon.ToBool(objTuple.Item1) AndAlso Not String.IsNullOrEmpty(objTuple.Item2) Then
                            _ShowError = True
                            _ErrorMessage = objTuple.Item2
                            Return objTuple.Item2 & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                            Exit Function
                        End If

                        isOauthTokenExists = CCommon.ToBool(objTuple.Item1)
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim success As Boolean
                    Dim ServerUrl As String
                    Dim Password As String
                    Dim UseUserName As Boolean

                    objUserAccess.UserCntID = UserContactID
                    objUserAccess.DomainID = numDomainId
                    Dim dtCredientials As DataTable = objUserAccess.GetImapDtls()

                    If dtCredientials.Rows.Count > 0 Then
                        If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                            Dim strPassword As String
                            Dim objCommon As New CCommon
                            strPassword = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                            ServerUrl = CStr(dtCredientials.Rows(0).Item("vcImapServerUrl"))
                            UseUserName = CBool(dtCredientials.Rows(0).Item("bitUseUserName"))
                            emailId = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                            Password = strPassword
                            lastUID = CLng(dtCredientials.Rows(0).Item("LastUID"))
                            If CBool(dtCredientials.Rows(0).Item("bitSSl")) = True Then
                                objImap.SslMode = SslStartupMode.OnConnect
                            End If
                        Else
                            'DisableImap(UserContactID, numDomainId)
                            Return "Imap Is not Integrated"
                            Exit Function
                        End If
                    Else
                        'DisableImap(UserContactID, numDomainId)
                        Return "Imap Is not Integrated" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                        Exit Function
                    End If

                    Try
                        success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                    Catch ex1 As Exception
                        'DisableImap(UserContactID, numDomainId)
                        _ShowError = True
                        _ErrorMessage = ex1.Message
                        Return ex1.Message & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                        Exit Function
                    End Try

                    If (success <> True) Then
                        'DisableImap(UserContactID, numDomainId)
                        Return "Could Not Connect To the Server" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                        Exit Function
                    End If

                    Try
                        If UseUserName = True Then
                            success = objImap.Login(emailId.Split(CChar("@"))(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        Else
                            success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        End If
                    Catch ex2 As Exception
                        _ShowError = True
                        _ErrorMessage = ex2.Message
                        Return ex2.Message & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    End Try
                    If (success <> True) Then
                        Return "Invalid Credentials" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                        Exit Function
                    End If
                End If

                If File.Exists("C:\log.txt") Then
                    objImap.Log.Filename = "C:\log.txt"
                    objImap.Log.Enabled = True
                Else
                    objImap.Log.Enabled = False
                End If

                Dim lstfolders As FolderCollection
                lstfolders = objImap.DownloadFolders()

                Dim dsFolder As New DataSet
                Dim dtFolder As New DataTable
                dtFolder.Columns.Add("vcFolderName")
                dtFolder.Columns.Add("vcParentFolderName")

                Dim drFolder As DataRow
   
                Dim lstFoldersDetails As New List(Of Folder)
                For Each objFolder As Folder In lstfolders
                    Dim strParentNodeText As String
                    strParentNodeText = ""
                    If objFolder.ShortName <> "[Gmail]" Then
                        If objFolder.NestingLevel > 0 Then
                            Dim strFolderArray() As String
                            strFolderArray = objFolder.Name.Split(Convert.ToChar("/"))
                            If strFolderArray(objFolder.NestingLevel - 1) <> "[Gmail]" Then
                                strParentNodeText = strFolderArray(objFolder.NestingLevel - 1)
                            End If
                        End If
                        drFolder = dtFolder.NewRow
                        drFolder("vcFolderName") = objFolder.ShortName
                        drFolder("vcParentFolderName") = strParentNodeText
                        dtFolder.Rows.Add(drFolder)

                        lstFoldersDetails.Add(objFolder)
                    End If
                Next

                Dim dtFolders As DataTable
                Dim objOutlookFolder As New COutlook
                objOutlookFolder.UserCntID = UserContactID
                objOutlookFolder.DomainID = numDomainId
                objOutlookFolder.ModeType = 3
                If dtFolder.Rows.Count > 0 Then
                    dsFolder.Tables.Add(dtFolder)
                    objOutlookFolder.NodeName = dsFolder.GetXml()
                Else
                    objOutlookFolder.NodeName = ""
                End If
                objOutlookFolder.GetTree()

                objOutlookFolder.ModeType = 0
                dtFolders = objOutlookFolder.GetTree()
                Dim dataView As DataView = dtFolders.DefaultView
                If (strFolderName <> "") Then
                    dataView.RowFilter = "vcNodeName = '" & strFolderName & "'"
                End If
                Dim strEmailUid As String
                For Each rowView As DataRowView In dataView
                    Try
                        Dim dr As DataRow = rowView.Row
                        If CCommon.ToString(dr("vcNodeName")) <> "" Then

                            lastUID = 0
                            strEmailUid = ""
                            Dim objFolder As Folder
                            objFolder = (From p In lstFoldersDetails.AsEnumerable() Where p.ShortName.ToLower() = CCommon.ToString(dr("vcNodeName")).ToLower() Select p).FirstOrDefault()
                            lastUID = CCommon.ToLong(dr("numLastUid"))
                            objImap.ExamineFolder(objFolder.Name)

                            'Dim envelopes As EnvelopeCollection
                            Dim Mails As MailMessageCollection
                            Dim MessageIndex As String
                            'Bug fix 1744 #1
                            'If lastUID = 0 And objImap.UidNext > 2500 Then
                            '    lastUID = objImap.UidNext - 2000
                            'End If
                            If lastUID = 0 Then
                                If (objImap.MessageCount > 0) Then
                                    lastUID = objImap.UidNext - 1
                                    Dim countTotalMessage As Integer = objImap.MessageCount
                                    'If objImap.UidNext = 1 Then
                                    '    lastUID = 1
                                    'End If
                                    MessageIndex = (countTotalMessage - 1).ToString() + ":" + (countTotalMessage).ToString()
                                    lastUID = lastUID + 20
                                    Mails = objImap.DownloadEntireMessages(MessageIndex, False)
                                    Dim msg As MailMessage = Mails(0)
                                    lastUID = CCommon.ToLong(msg.UidOnServer)
                                    'If (objImap.UidNext > 0) Then
                                    '    If ((objImap.UidNext - 1) = 0) Then
                                    '        'Return "Total (0) New Emails Fetched"
                                    '        Mails = New MailMessageCollection
                                    '    Else

                                    '        'Dim msg As MailBee.Mime.MailMessage = objImap.DownloadEntireMessage(lastUID, True)
                                    '        'lastUID = CCommon.ToLong(msg.UidOnServer)
                                    '        'Mails = New MailMessageCollection
                                    '        'Mails.Add(msg)
                                    '    End If
                                    'End If


                                End If
                            Else
                                If objImap.UidNext - lastUID > 20 Then
                                    MessageIndex = (lastUID + 1).ToString() + ":" + (lastUID + 20).ToString()
                                    lastUID = lastUID + 20
                                    Mails = objImap.DownloadEntireMessages(MessageIndex, True) ', EnvelopeParts.MailBeeEnvelope Or EnvelopeParts.BodyStructure, 0)
                                Else
                                    If ((objImap.UidNext - 1) - lastUID = 0) Then
                                        'Return "Total (0) New Emails Fetched"
                                        Mails = New MailMessageCollection
                                    Else
                                        MessageIndex = (lastUID + 1).ToString() + ":*"
                                        lastUID = objImap.UidNext - 1
                                        Mails = objImap.DownloadEntireMessages(MessageIndex, True) ', EnvelopeParts.MailBeeEnvelope Or EnvelopeParts.BodyStructure, 0)
                                    End If
                                End If
                            End If




                            If Mails.Count > 0 Then
                                Dim j As Integer = 0
                                Dim ObjOutlook As New COutlook

                                For i = 0 To Mails.Count - 1
                                    Dim msg As MailMessage = Mails(i)
                                    msg.Parser.PlainToHtmlMode = PlainToHtmlAutoConvert.IfNoHtml
                                    msg.Parser.DatesAsUtc = True

                                    If CCommon.ToLong(msg.UidOnServer) > 0 Then 'From Carl's mailbox it was giving blank mail with uid -1 so added this condition
                                        'Dim filename As String = ""
                                        'Dim filetype As String = ""
                                        'Dim newFileName As String = ""
                                        'Dim strFile As String = ""
                                        'Dim strFilepath As String = ""
                                        'Dim fileSize As String = ""

                                        'Dim strDirectory As String = CCommon.GetDocumentPhysicalPath(numDomainId) 'ConfigurationManager.AppSettings("PortalPath") & .ToString
                                        'Dim strFileExtension As String = ""
                                        'Dim r As New Random
                                        'Try
                                        '    For Each file As Attachment In msg.Attachments
                                        '        If Not file.Filename Is Nothing Then
                                        '            If file.Filename.Length > 0 Then

                                        '                If Not System.IO.Directory.Exists(strDirectory) Then
                                        '                    System.IO.Directory.CreateDirectory(strDirectory)
                                        '                End If
                                        '                'When File doesn't contain any extension then give extension .biz
                                        '                'Bug fix 967 by chintan
                                        '                If file.Filename.Contains(".") = True Then
                                        '                    Dim arFilePart() As String = file.Filename.Split(Char.Parse("."))
                                        '                    strFileExtension = "." & arFilePart(arFilePart.Length - 1).ToString()
                                        '                Else : strFileExtension = ".zip"
                                        '                End If

                                        '                strFile = "File" & r.Next(0, 9999999).ToString("0000000") & Now.Millisecond.ToString & strFileExtension
                                        '                strFilepath = strDirectory & strFile
                                        '                file.Save(strFilepath, False)

                                        '                filename = filename & "|" & file.Filename
                                        '                filetype = filetype & "|" & file.ContentType
                                        '                newFileName = newFileName & "|" & strFile
                                        '                fileSize = fileSize & "|" & file.Size
                                        '                'fileExtension = fileExtension & "|" & strFileExtension


                                        '            End If
                                        '        End If
                                        '    Next
                                        'Catch ex As Exception
                                        '    ExceptionModule.ExceptionPublish(ex, CLng(SessionObject("DomainID")), CLng(SessionObject("UserContactID")), Current.Request)
                                        'End Try

                                        Dim ToName As String = ""
                                        Dim CCName As String = ""
                                        Dim BCCName As String = ""

                                        For j = 0 To msg.To.Count - 1
                                            ToName = ToName & "#^#" & IIf(IsValidEmailAddress(msg.To(j).DisplayName) = True, " ", IIf(msg.To(j).DisplayName = "", " ", msg.To(j).DisplayName)).ToString & "$^$" & msg.To(j).Email
                                        Next
                                        If ToName.Length > 0 Then
                                            ToName = ToName.Remove(0, 3)
                                        End If

                                        j = 0
                                        For j = 0 To msg.Cc.Count - 1
                                            CCName = CCName & "#^#" & IIf(IsValidEmailAddress(msg.Cc(j).DisplayName) = True, " ", IIf(msg.Cc(j).DisplayName = "", " ", msg.Cc(j).DisplayName)).ToString & "$^$" & msg.Cc(j).Email
                                        Next
                                        If CCName.Length > 0 Then
                                            CCName = CCName.Remove(0, 3)
                                        End If

                                        j = 0
                                        For j = 0 To msg.Bcc.Count - 1
                                            BCCName = BCCName & "#^#" & IIf(IsValidEmailAddress(msg.Bcc(j).DisplayName) = True, " ", IIf(msg.Bcc(j).DisplayName = "", " ", msg.Bcc(j).DisplayName)).ToString & "$^$" & msg.Bcc(j).Email
                                        Next
                                        If BCCName.Length > 0 Then
                                            BCCName = BCCName.Remove(0, 3)
                                        End If

                                        If UserContactID = -1 Then 'For Support Email Create Case
                                            Dim objCases As New [Case].CaseIP

                                            Dim CaseNo As Long = GetCaseNumberFromSubject(msg.Subject)

                                            objCases.DomainID = numDomainId
                                            objCases.CaseNo = CStr(CaseNo)
                                            Dim dtCaseInfo As DataTable = objCases.GetEmailToCaseCheck(msg.From.Email)

                                            If dtCaseInfo.Rows.Count = 1 AndAlso CLng(dtCaseInfo.Rows(0)("numDivisionId")) > 0 AndAlso CLng(dtCaseInfo.Rows(0)("numcontactid")) > 0 Then
                                                Dim _CaseID As Long = 0

                                                If CLng(dtCaseInfo.Rows(0)("numCaseId")) = 0 Then
                                                    With objCases
                                                        .CaseNo = Format(objCases.GetMaxCaseID, "00000000000")
                                                        .DivisionID = CLng(dtCaseInfo.Rows(0)("numDivisionId"))
                                                        .ContactID = CLng(dtCaseInfo.Rows(0)("numcontactid"))
                                                        .DomainID = numDomainId
                                                        .UserCntID = UserContactID
                                                        .SupportKeyType = 0
                                                        .Subject = msg.Subject
                                                        .AssignTo = UserContactID
                                                    End With

                                                    If objCases.TargetResolveDate.Year = 1753 Then
                                                        objCases.TargetResolveDate = Date.Now
                                                    End If

                                                    _CaseID = objCases.SaveCases

                                                    Try

                                                        Dim objSendEmail As New Email

                                                        objSendEmail.DomainID = numDomainId
                                                        objSendEmail.ModuleID = 5

                                                        objSendEmail.TemplateCode = "#SYS#EMAIL_ALERT:Email_To_Case"

                                                        objSendEmail.RecordIds = CStr(_CaseID)
                                                        objSendEmail.FromEmail = emailId
                                                        objSendEmail.ToEmail = msg.From.Email
                                                        objSendEmail.UserID = UserContactID
                                                        objSendEmail.SendEmailTemplate()
                                                    Catch ex As Exception

                                                    End Try
                                                Else
                                                    _CaseID = CLng(dtCaseInfo.Rows(0)("numCaseId"))
                                                End If

                                                Dim objCommon As New CCommon
                                                objCommon.CaseID = _CaseID
                                                objCommon.StageID = 0
                                                objCommon.Comments = msg.BodyHtmlText
                                                objCommon.Heading = "Email to Case"
                                                objCommon.ContactID = UserContactID
                                                objCommon.CommentID = 0
                                                objCommon.byteMode = 1
                                                objCommon.DomainID = numDomainId
                                                objCommon.ManageComments()
                                            End If
                                        Else
                                            If ObjOutlook Is Nothing Then ObjOutlook = New COutlook

                                            ObjOutlook.UserCntID = UserContactID
                                            ObjOutlook.DomainID = numDomainId

                                            ObjOutlook.Uid = CLng(msg.UidOnServer)
                                            ObjOutlook.Subject = msg.Subject
                                            ObjOutlook.Body = msg.BodyHtmlText
                                            ObjOutlook.BodyText = msg.BodyPlainText
                                            ObjOutlook.ReceivedOn = CDate(IIf(msg.DateReceived = Nothing, Date.UtcNow, msg.DateReceived))
                                            ObjOutlook.vcSize = CStr(msg.SizeOnServer)
                                            ObjOutlook.IsRead = False 'IIf(msg.flags.ToString.Contains("\Seen") = True, True, False)
                                            ObjOutlook.HasAttachments = CBool(IIf(msg.Attachments.Count > 0, True, False))
                                            ObjOutlook.FromName = msg.From.DisplayName & "$^$" & msg.From.Email
                                            ObjOutlook.ToName = ToName
                                            ObjOutlook.CCName = CCName
                                            ObjOutlook.BCCName = BCCName
                                            ObjOutlook.NodeId = CCommon.ToLong(dr("numNodeID"))
                                            ObjOutlook.AttachmentName = ""
                                            ObjOutlook.AttachmentType = ""
                                            ObjOutlook.NewAttachmentName = ""
                                            ObjOutlook.AttachmentSize = ""
                                            ObjOutlook.InsertImapNewMails()
                                        End If
                                    End If




                                Next
                            End If
                            'Update Last Email UID fetched Reason: some times  mailbox doesn't return any mails for initial mail counts e.g. 1 to 20 
                            objOutlookFolder = New COutlook
                            objOutlookFolder.UserCntID = UserContactID
                            objOutlookFolder.DomainID = numDomainId
                            objOutlookFolder.ModeType = 4
                            objOutlookFolder.Uid = lastUID
                            objOutlookFolder.NodeId = CCommon.ToLong(dr("numNodeID"))
                            objOutlookFolder.GetTree()
                        End If

                    Catch ex As Exception
                        Dim strException As String
                        strException = ex.ToString()
                    End Try
                Next
                objImap.Disconnect()
                Return "Total (" & dtTable.Rows.Count.ToString() & ") New Emails Fetched"
            Catch ex As Exception
                Throw ex
            Finally
                If objImap.IsConnected Then
                    objImap.Disconnect()
                End If
            End Try
        End Function


        Public Function GetOldEmails(ByVal UserContactID As Long, ByVal numDomainId As Long) As String
            Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
            Dim i As Integer
            Dim objImap As New Imap
            objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
            Try
                Dim lastUID As Long
                Dim dtTable As New DataTable
                Dim dtCredientials As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.UserCntID = UserContactID
                objUserAccess.DomainID = numDomainId
                dtCredientials = objUserAccess.GetImapDtls()
                Dim success As Boolean
                Dim ServerUrl As String
                Dim emailId As String
                Dim Password As String
                Dim UseUserName As Boolean

                If dtCredientials.Rows.Count > 0 Then
                    If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                        Dim strPassword As String
                        Dim objCommon As New CCommon
                        strPassword = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                        ServerUrl = CStr(dtCredientials.Rows(0).Item("vcImapServerUrl"))
                        UseUserName = CBool(dtCredientials.Rows(0).Item("bitUseUserName"))
                        emailId = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                        Password = strPassword
                        If CBool(dtCredientials.Rows(0).Item("bitSSl")) = True Then
                            objImap.SslMode = SslStartupMode.OnConnect
                        End If
                    Else
                        'DisableImap(UserContactID, numDomainId)
                        Return "Imap Is not Integrated"
                        Exit Function
                    End If
                Else
                    'DisableImap(UserContactID, numDomainId)
                    Return "Imap Is not Integrated" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End If

                If File.Exists("C:\log.txt") Then
                    objImap.Log.Filename = "C:\log.txt"
                    objImap.Log.Enabled = True
                Else
                    objImap.Log.Enabled = False
                End If
                Try
                    success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                Catch ex1 As Exception
                    'DisableImap(UserContactID, numDomainId)
                    _ShowError = True
                    _ErrorMessage = ex1.Message
                    Return ex1.Message & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End Try

                If (success <> True) Then
                    'DisableImap(UserContactID, numDomainId)
                    Return "Could Not Connect To the Server" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End If
                Try


                    If UseUserName = True Then
                        success = objImap.Login(emailId.Split(CChar("@"))(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)

                    Else
                        success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)

                    End If
                Catch ex2 As Exception
                    'DisableImap(UserContactID, numDomainId)
                    _ShowError = True
                    _ErrorMessage = ex2.Message
                    Return ex2.Message & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                End Try
                If (success <> True) Then
                    'DisableImap(UserContactID, numDomainId)
                    Return "Invalid Credentials" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End If


                Dim lstfolders As FolderCollection
                lstfolders = objImap.DownloadFolders()

                Dim dsFolder As New DataSet
                Dim dtFolder As New DataTable
                dtFolder.Columns.Add("vcFolderName")
                dtFolder.Columns.Add("vcParentFolderName")

                Dim drFolder As DataRow

                Dim lstFoldersDetails As New List(Of Folder)
                For Each objFolder As Folder In lstfolders
                    Dim strParentNodeText As String
                    strParentNodeText = ""
                    If objFolder.ShortName <> "[Gmail]" Then
                        If objFolder.NestingLevel > 0 Then
                            Dim strFolderArray() As String
                            strFolderArray = objFolder.Name.Split(Convert.ToChar("/"))
                            If strFolderArray(objFolder.NestingLevel - 1) <> "[Gmail]" Then
                                strParentNodeText = strFolderArray(objFolder.NestingLevel - 1)
                            End If
                        End If

                        drFolder = dtFolder.NewRow
                        drFolder("vcFolderName") = objFolder.ShortName
                        drFolder("vcParentFolderName") = strParentNodeText
                        dtFolder.Rows.Add(drFolder)

                        lstFoldersDetails.Add(objFolder)
                    End If
                Next

                Dim dtFolders As DataTable
                Dim objOutlookFolder As New COutlook
                objOutlookFolder.UserCntID = UserContactID
                objOutlookFolder.DomainID = numDomainId
                objOutlookFolder.ModeType = 3
                If dtFolder.Rows.Count > 0 Then
                    dsFolder.Tables.Add(dtFolder)
                    objOutlookFolder.NodeName = dsFolder.GetXml()
                Else
                    objOutlookFolder.NodeName = ""
                End If
                objOutlookFolder.GetTree()

                objOutlookFolder.ModeType = 0
                dtFolders = objOutlookFolder.GetTree()
                Dim strEmailUid As String
                For Each dr As DataRow In dtFolders.Rows
                    Try

                        If CCommon.ToString(dr("vcNodeName")) <> "" Then

                            lastUID = 0
                            strEmailUid = ""
                            Dim objFolder As Folder
                            objFolder = (From p In lstFoldersDetails.AsEnumerable() Where p.ShortName.ToLower() = CCommon.ToString(dr("vcNodeName")).ToLower() Select p).FirstOrDefault()
                            lastUID = CCommon.ToLong(dr("numOldEmailLastUid"))
                            objImap.ExamineFolder(objFolder.Name)

                            'Dim envelopes As EnvelopeCollection
                            Dim Mails As MailMessageCollection
                            Dim MessageIndex As String
                            'Bug fix 1744 #1
                            'If lastUID = 0 And objImap.UidNext > 2500 Then
                            '    lastUID = objImap.UidNext - 2000
                            'End If

                            If lastUID > 1 Then
                                If lastUID > 20 Then
                                    MessageIndex = (lastUID - 20).ToString() + ":" + (lastUID - 1).ToString()
                                    lastUID = lastUID - 20
                                    Mails = objImap.DownloadEntireMessages(MessageIndex, True) ', EnvelopeParts.MailBeeEnvelope Or EnvelopeParts.BodyStructure, 0)
                                Else
                                    MessageIndex = (lastUID - 1).ToString() + ":*"
                                    lastUID = 1
                                    Mails = objImap.DownloadEntireMessages(MessageIndex, True) ', EnvelopeParts.MailBeeEnvelope Or EnvelopeParts.BodyStructure, 0)
                                End If
                            Else
                                Mails = New MailMessageCollection
                            End If



                            If Mails.Count > 0 Then
                                Dim j As Integer = 0
                                Dim ObjOutlook As New COutlook

                                For i = 0 To Mails.Count - 1
                                    Dim msg As MailMessage = Mails(i)
                                    msg.Parser.PlainToHtmlMode = PlainToHtmlAutoConvert.IfNoHtml
                                    msg.Parser.DatesAsUtc = True

                                    If CCommon.ToLong(msg.UidOnServer) > 0 Then 'From Carl's mailbox it was giving blank mail with uid -1 so added this condition
                                        Dim filename As String = ""
                                        Dim filetype As String = ""
                                        Dim newFileName As String = ""
                                        Dim strFile As String = ""
                                        Dim strFilepath As String = ""
                                        Dim fileSize As String = ""
                                        'Dim fileExtension As String = ""
                                        Dim strDirectory As String = CCommon.GetDocumentPhysicalPath(numDomainId) 'ConfigurationManager.AppSettings("PortalPath") & .ToString
                                        Dim strFileExtension As String = ""
                                        Dim r As New Random
                                        Try
                                            For Each file As Attachment In msg.Attachments
                                                If Not file.Filename Is Nothing Then
                                                    If file.Filename.Length > 0 Then

                                                        If Not System.IO.Directory.Exists(strDirectory) Then
                                                            System.IO.Directory.CreateDirectory(strDirectory)
                                                        End If
                                                        'When File doesn't contain any extension then give extension .biz
                                                        'Bug fix 967 by chintan
                                                        If file.Filename.Contains(".") = True Then
                                                            Dim arFilePart() As String = file.Filename.Split(Char.Parse("."))
                                                            strFileExtension = "." & arFilePart(arFilePart.Length - 1).ToString()
                                                        Else : strFileExtension = ".zip"
                                                        End If

                                                        strFile = "File" & r.Next(0, 9999999).ToString("0000000") & Now.Millisecond.ToString & strFileExtension
                                                        strFilepath = strDirectory & strFile
                                                        file.Save(strFilepath, False)

                                                        filename = filename & "|" & file.Filename
                                                        filetype = filetype & "|" & file.ContentType
                                                        newFileName = newFileName & "|" & strFile
                                                        fileSize = fileSize & "|" & file.Size
                                                        'fileExtension = fileExtension & "|" & strFileExtension


                                                    End If
                                                End If
                                            Next
                                        Catch ex As Exception
                                            ExceptionModule.ExceptionPublish(ex, CLng(SessionObject("DomainID")), CLng(SessionObject("UserContactID")), Current.Request)
                                        End Try
                                        'msg.Attachments.SaveAll(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\")

                                        Dim ToName As String = ""
                                        Dim CCName As String = ""
                                        Dim BCCName As String = ""

                                        For j = 0 To msg.To.Count - 1
                                            ToName = ToName & "#^#" & IIf(IsValidEmailAddress(msg.To(j).DisplayName) = True, " ", IIf(msg.To(j).DisplayName = "", " ", msg.To(j).DisplayName)).ToString & "$^$" & msg.To(j).Email
                                        Next
                                        If ToName.Length > 0 Then
                                            ToName = ToName.Remove(0, 3)
                                        End If

                                        j = 0
                                        For j = 0 To msg.Cc.Count - 1
                                            CCName = CCName & "#^#" & IIf(IsValidEmailAddress(msg.Cc(j).DisplayName) = True, " ", IIf(msg.Cc(j).DisplayName = "", " ", msg.Cc(j).DisplayName)).ToString & "$^$" & msg.Cc(j).Email
                                        Next
                                        If CCName.Length > 0 Then
                                            CCName = CCName.Remove(0, 3)
                                        End If

                                        j = 0
                                        For j = 0 To msg.Bcc.Count - 1
                                            BCCName = BCCName & "#^#" & IIf(IsValidEmailAddress(msg.Bcc(j).DisplayName) = True, " ", IIf(msg.Bcc(j).DisplayName = "", " ", msg.Bcc(j).DisplayName)).ToString & "$^$" & msg.Bcc(j).Email
                                        Next
                                        If BCCName.Length > 0 Then
                                            BCCName = BCCName.Remove(0, 3)
                                        End If

                                        If UserContactID = -1 Then 'For Support Email Create Case
                                            Dim objCases As New [Case].CaseIP

                                            Dim CaseNo As Long = GetCaseNumberFromSubject(msg.Subject)

                                            objCases.DomainID = numDomainId
                                            objCases.CaseNo = CStr(CaseNo)
                                            Dim dtCaseInfo As DataTable = objCases.GetEmailToCaseCheck(msg.From.Email)

                                            If dtCaseInfo.Rows.Count = 1 AndAlso CLng(dtCaseInfo.Rows(0)("numDivisionId")) > 0 AndAlso CLng(dtCaseInfo.Rows(0)("numcontactid")) > 0 Then
                                                Dim _CaseID As Long = 0

                                                If CLng(dtCaseInfo.Rows(0)("numCaseId")) = 0 Then
                                                    With objCases
                                                        .CaseNo = Format(objCases.GetMaxCaseID, "00000000000")
                                                        .DivisionID = CLng(dtCaseInfo.Rows(0)("numDivisionId"))
                                                        .ContactID = CLng(dtCaseInfo.Rows(0)("numcontactid"))
                                                        .DomainID = numDomainId
                                                        .UserCntID = UserContactID
                                                        .SupportKeyType = 0
                                                        .Subject = msg.Subject
                                                        .AssignTo = UserContactID
                                                    End With

                                                    If objCases.TargetResolveDate.Year = 1753 Then
                                                        objCases.TargetResolveDate = Date.Now
                                                    End If

                                                    _CaseID = objCases.SaveCases

                                                    Try

                                                        Dim objSendEmail As New Email

                                                        objSendEmail.DomainID = numDomainId
                                                        objSendEmail.ModuleID = 5

                                                        objSendEmail.TemplateCode = "#SYS#EMAIL_ALERT:Email_To_Case"

                                                        objSendEmail.RecordIds = CStr(_CaseID)
                                                        objSendEmail.FromEmail = emailId
                                                        objSendEmail.ToEmail = msg.From.Email
                                                        objSendEmail.UserID = UserContactID
                                                        objSendEmail.SendEmailTemplate()
                                                        'Dim objAlerts As New Alerts.CAlerts
                                                        'Dim dtDetails As DataTable
                                                        'objAlerts.AlertDTLID = 11 'Alert DTL ID for sending alerts in opportunities
                                                        'objAlerts.DomainID = numDomainId
                                                        'dtDetails = objAlerts.GetIndAlertDTL
                                                        'If dtDetails.Rows.Count > 0 Then
                                                        '    Dim dtEmailTemplate As DataTable
                                                        '    Dim objDocuments As New Documents.DocumentList
                                                        '    objDocuments.GenDocID = CLng(dtDetails.Rows(0).Item("numEmailTemplate"))
                                                        '    objDocuments.DomainID = DomainID
                                                        '    dtEmailTemplate = objDocuments.GetDocByGenDocID
                                                        '    If dtEmailTemplate.Rows.Count > 0 Then
                                                        '        Dim objSendMail As New Email
                                                        '        Dim dtMergeFields As New DataTable
                                                        '        Dim drNew As DataRow
                                                        '        dtMergeFields.Columns.Add("CaseID")
                                                        '        dtMergeFields.Columns.Add("Organization")
                                                        '        dtMergeFields.Columns.Add("Employee")
                                                        '        drNew = dtMergeFields.NewRow
                                                        '        drNew("CaseID") = _CaseID
                                                        '        drNew("Organization") = ""
                                                        '        drNew("Employee") = ""
                                                        '        dtMergeFields.Rows.Add(drNew)

                                                        '        objSendMail.SendEmail(CStr(dtEmailTemplate.Rows(0).Item("vcSubject")), CStr(dtEmailTemplate.Rows(0).Item("vcDocdesc")), "", "", msg.From.Email, dtMergeFields, DomainID:=DomainID, UserId:=UserContactID)
                                                        '    End If
                                                        'End If
                                                    Catch ex As Exception

                                                    End Try
                                                Else
                                                    _CaseID = CLng(dtCaseInfo.Rows(0)("numCaseId"))
                                                End If

                                                Dim objCommon As New CCommon
                                                objCommon.CaseID = _CaseID
                                                objCommon.StageID = 0
                                                objCommon.Comments = msg.BodyHtmlText
                                                objCommon.Heading = "Email to Case"
                                                objCommon.ContactID = UserContactID
                                                objCommon.CommentID = 0
                                                objCommon.byteMode = 1
                                                objCommon.DomainID = numDomainId
                                                objCommon.ManageComments()
                                            End If
                                        Else
                                            If ObjOutlook Is Nothing Then ObjOutlook = New COutlook

                                            ObjOutlook.UserCntID = UserContactID
                                            ObjOutlook.DomainID = numDomainId

                                            ObjOutlook.Uid = CLng(msg.UidOnServer)
                                            ObjOutlook.Subject = msg.Subject
                                            ObjOutlook.Body = msg.BodyHtmlText
                                            ObjOutlook.BodyText = msg.BodyPlainText
                                            ObjOutlook.ReceivedOn = CDate(IIf(msg.DateReceived = Nothing, Date.UtcNow, msg.DateReceived))
                                            ObjOutlook.vcSize = CStr(msg.SizeOnServer)
                                            ObjOutlook.IsRead = False 'IIf(msg.flags.ToString.Contains("\Seen") = True, True, False)
                                            ObjOutlook.HasAttachments = CBool(IIf(msg.Attachments.Count > 0, True, False))
                                            ObjOutlook.FromName = msg.From.DisplayName & "$^$" & msg.From.Email
                                            ObjOutlook.ToName = ToName
                                            ObjOutlook.CCName = CCName
                                            ObjOutlook.BCCName = BCCName
                                            ObjOutlook.NodeId = CCommon.ToLong(dr("numNodeID"))
                                            ObjOutlook.AttachmentName = filename.TrimStart(Char.Parse("|"))
                                            ObjOutlook.AttachmentType = filetype.TrimStart(Char.Parse("|"))
                                            ObjOutlook.NewAttachmentName = newFileName.TrimStart(Char.Parse("|"))
                                            ObjOutlook.AttachmentSize = fileSize.TrimStart(Char.Parse("|"))
                                            ObjOutlook.InsertImapNewMails()
                                        End If
                                    End If




                                Next
                            End If
                            'Update Last Email UID fetched Reason: some times  mailbox doesn't return any mails for initial mail counts e.g. 1 to 20 
                            objOutlookFolder = New COutlook
                            objOutlookFolder.UserCntID = UserContactID
                            objOutlookFolder.DomainID = numDomainId
                            objOutlookFolder.ModeType = 5
                            objOutlookFolder.Uid = lastUID
                            objOutlookFolder.NodeId = CCommon.ToLong(dr("numNodeID"))
                            objOutlookFolder.GetTree()
                        End If

                    Catch ex As Exception
                        Dim strException As String
                        strException = ex.ToString()
                    End Try
                Next
                objImap.Disconnect()
                Return "Total (" & dtTable.Rows.Count.ToString() & ") New Emails Fetched"
            Catch ex As Exception
                Throw ex
            Finally
                If objImap.IsConnected Then
                    objImap.Disconnect()
                End If
            End Try
        End Function

        Public Function GetAnyNewMessage(ByVal UserContactID As Long, ByVal numDomainId As Long, ByVal NodeId As Long) As String
            Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
            Dim i As Integer
            Dim objImap As New Imap
            objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
            Try
                Dim dtTable As New DataTable
                Dim dtCredientials As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.UserCntID = UserContactID
                objUserAccess.DomainID = numDomainId
                dtCredientials = objUserAccess.GetImapDtls()
                Dim success As Boolean
                Dim ServerUrl As String
                Dim emailId As String
                Dim Password As String
                Dim UseUserName As Boolean

                If dtCredientials.Rows.Count > 0 Then
                    If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                        Dim strPassword As String
                        Dim objCommon As New CCommon
                        strPassword = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                        ServerUrl = CStr(dtCredientials.Rows(0).Item("vcImapServerUrl"))
                        UseUserName = CBool(dtCredientials.Rows(0).Item("bitUseUserName"))
                        emailId = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                        Password = strPassword
                        If CBool(dtCredientials.Rows(0).Item("bitSSl")) = True Then
                            objImap.SslMode = SslStartupMode.OnConnect
                        End If
                    Else
                        'DisableImap(UserContactID, numDomainId)
                        Return "Imap Is not Integrated"
                        Exit Function
                    End If
                Else
                    'DisableImap(UserContactID, numDomainId)
                    Return "Imap Is not Integrated" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End If

                If File.Exists("C:\log.txt") Then
                    objImap.Log.Filename = "C:\log.txt"
                    objImap.Log.Enabled = True
                Else
                    objImap.Log.Enabled = False
                End If
                Try
                    success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                Catch ex1 As Exception
                    'DisableImap(UserContactID, numDomainId)
                    _ShowError = True
                    _ErrorMessage = ex1.Message
                    Return ex1.Message & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End Try

                If (success <> True) Then
                    'DisableImap(UserContactID, numDomainId)
                    Return "Could Not Connect To the Server" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End If
                Try


                    If UseUserName = True Then
                        success = objImap.Login(emailId.Split(CChar("@"))(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)

                    Else
                        success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)

                    End If
                Catch ex2 As Exception
                    'DisableImap(UserContactID, numDomainId)
                    _ShowError = True
                    _ErrorMessage = ex2.Message
                    Return ex2.Message & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                End Try
                If (success <> True) Then
                    'DisableImap(UserContactID, numDomainId)
                    Return "Invalid Credentials" & ",Imap disabled for ContactID:" & UserContactID.ToString & " & DomainID: " & numDomainId.ToString
                    Exit Function
                End If
                Dim objOutlookFolder As New COutlook
                objOutlookFolder.UserCntID = UserContactID
                objOutlookFolder.DomainID = numDomainId
                objOutlookFolder.ModeType = 7
                objOutlookFolder.NodeId = NodeId
                Dim dtFolderTable As DataTable
                dtFolderTable = objOutlookFolder.GetTree()
                Dim strFolderName As String
                strFolderName = ""
                If dtFolderTable.Rows.Count > 0 Then
                    strFolderName = CCommon.ToString(dtFolderTable.Rows(0)("vcNodeName"))
                End If
                Dim lstfolders As FolderCollection
                lstfolders = objImap.DownloadFolders()
                Dim strFolderFromServer As String
                strFolderFromServer = ""
                For Each objFolder As Folder In lstfolders
                    If objFolder.ShortName.ToLower() = strFolderName.ToLower() Then
                        strFolderFromServer = objFolder.Name
                    End If
                Next
                objImap.SelectFolder(strFolderFromServer)
                Dim lastUid As Long = 0
                If dtFolderTable.Rows.Count > 0 Then
                    lastUid = CCommon.ToLong(dtFolderTable.Rows(0)("numLastUid"))
                End If
                Dim msgNums As MessageIndexCollection = Nothing
                msgNums = objImap.Search(True, "", Nothing)
                Dim maxUid As Long = 0
                maxUid = CCommon.ToLong((From p In msgNums Select p).Max())

                If msgNums.Count > 0 AndAlso lastUid < maxUid Then
                    GetNewEmails(UserContactID, numDomainId, strFolderName)
                    Return "1"
                Else
                    Return "0"
                End If
            Catch ex As Exception
                Return ex.ToString()
            End Try

        End Function

        Public Shared Function GetCaseNumberFromSubject(subject As String) As Integer
            If String.IsNullOrEmpty(subject) Then
                Return 0
            End If

            Dim NumRegex As New Regex("\d+", RegexOptions.Compiled)

            Dim match As Match = NumRegex.Match(subject)
            If match.Success Then
                Return Integer.Parse(match.Value)
            End If

            Return 0
        End Function

        Public Function SyncMailbox(ByVal LastMailsCount As Integer, ByVal UserContactID As Long, ByVal numDomainId As Long) As String
            Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
            Dim objImap As New Imap
            objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
            Try
                Dim i As Integer
                Dim lastUID As Long
                Dim dtTable As New DataTable
                Dim dtCredientials As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.UserCntID = UserContactID
                objUserAccess.DomainID = numDomainId
                dtCredientials = objUserAccess.GetImapDtls()
                Dim success As Boolean
                Dim ServerUrl As String
                Dim emailId As String
                Dim Password As String
                Dim UseUserName As Boolean

                If dtCredientials.Rows.Count > 0 Then
                    If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                        Dim strPassword As String
                        Dim objCommon As New CCommon
                        strPassword = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                        ServerUrl = CStr(dtCredientials.Rows(0).Item("vcImapServerUrl"))
                        UseUserName = CBool(dtCredientials.Rows(0).Item("bitUseUserName"))
                        emailId = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                        Password = strPassword
                        lastUID = CLng(dtCredientials.Rows(0).Item("LastUID"))
                        If CBool(dtCredientials.Rows(0).Item("bitSSl")) = True Then
                            objImap.SslMode = SslStartupMode.OnConnect
                        End If
                    Else
                        Return "Imap Is not Integrated"
                        Exit Function
                    End If
                Else
                    Return "Imap Is not Integrated"
                    Exit Function
                End If

                If File.Exists("C:\log.txt") Then
                    objImap.Log.Filename = "C:\log.txt"
                    objImap.Log.Enabled = True
                Else
                    objImap.Log.Enabled = False
                End If
                Try
                    success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                Catch ex1 As Exception
                    _ShowError = True
                    _ErrorMessage = ex1.Message
                    Return _ErrorMessage
                    Exit Function
                End Try

                If (success <> True) Then
                    Return "Could Not Connect To the Server"
                    Exit Function
                End If
                Try
                    If UseUserName = True Then
                        success = objImap.Login(emailId.Split(CChar("@"))(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)

                    Else
                        success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)

                    End If
                Catch ex2 As Exception
                    _ShowError = True
                    _ErrorMessage = ex2.Message
                End Try
                If (success <> True) Then
                    Return "Invalid Credentials"
                    Exit Function
                End If

                Dim j As Integer = 0
                Dim DeleteCount As Integer = 0
                Dim dtInbox As DataTable
                Dim objOutlook As New Outlook.COutlook

                Dim dtFolders As DataTable
                Dim objOutlookFolder As New COutlook
                objOutlookFolder.UserCntID = UserContactID
                objOutlookFolder.DomainID = numDomainId
                objOutlookFolder.ModeType = 0
                dtFolders = objOutlookFolder.GetTree()

                Dim lstfolders As FolderCollection
                lstfolders = objImap.DownloadFolders()
                Dim dataView As DataView = dtFolders.DefaultView
                For Each objFolder As Folder In lstfolders
                    objOutlook = New COutlook
                    dtInbox = New DataTable
                    dataView.RowFilter = "vcNodeName = '" & objFolder.ShortName & "'"
                    Dim drRowView As DataRowView
                    drRowView = dataView(0)
                    objOutlook.PageSize = LastMailsCount
                    objOutlook.DomainID = numDomainId
                    objOutlook.UserCntID = UserContactID
                    objOutlook.NodeId = CCommon.ToLong(drRowView("numNodeId"))
                    dtInbox = objOutlook.getInboxUidRange()
                    objImap.ExamineFolder(objFolder.Name)
                    If dtInbox.Rows.Count > 0 Then
                        ' Download envelopes for last 20 messages in the inbox. If there are
                        ' less than 20 messages, we'll get envelopes for all of them. In this
                        ' sample, we define higher boundary of the range as imp.MessageCount
                        ' but we could also use "*" character for that.
                        Dim StartCount As Integer
                        Dim EndCount As Integer


                        StartCount = CCommon.ToInteger(dtInbox.Compute("min(numUid)", ""))
                        EndCount = CCommon.ToInteger(dtInbox.Compute("max(numUid)", ""))
                        If StartCount < 1 Then
                            StartCount = 1
                        End If
                        EndCount = EndCount + 1

                        Dim uids As New UidCollection
                        Dim envelopes As EnvelopeCollection = objImap.DownloadEnvelopes(StartCount.ToString() + ":" + EndCount.ToString(), True, EnvelopeParts.Uid, 0)

                        For i = 0 To envelopes.Count - 1
                            uids.Add(envelopes(i).Uid)
                        Next

                        'uids = CType(objImap.Search(True, StartCount.ToString() + ":" + EndCount.ToString(), Nothing), UidCollection)
                        ' uids.Item(0).ToString() + "," + uids.Item(1).ToString() + ","+ uids.Item(2).ToString() + ","+ uids.Item(3).ToString() + ","+ uids.Item(4).ToString() + ","+ uids.Item(5).ToString() + ","+ uids.Item(6).ToString() + ","+ uids.Item(7).ToString() 
                        Dim flag As Boolean = False
                        For i = 0 To dtInbox.Rows.Count - 1
                            For j = 0 To uids.Count - 1
                                If uids.Item(j).ToString.ToLower = dtInbox.Rows(i)("numUid").ToString.ToLower Then
                                    flag = True
                                End If
                            Next
                            If flag = False Then
                                objOutlook.DomainID = numDomainId
                                objOutlook.UserCntID = UserContactID
                                objOutlook.Uid = CCommon.ToLong(dtInbox.Rows(i)("numUid"))
                                objOutlook.NodeId = CCommon.ToLong(drRowView("numNodeId"))
                                objOutlook.deleteInboxItemByUid()
                                DeleteCount = DeleteCount + 1
                            End If
                            flag = False
                        Next
                    End If
                Next





                Return "Total (" & DeleteCount.ToString() & ")s Emails deleted"
            Catch ex As Exception
                Throw ex
            Finally
                If objImap.IsConnected Then
                    objImap.Disconnect()
                End If
            End Try
        End Function
        Public Function ValidateSMTP(ByVal strAccountName As String, ByVal strPassword As String, ByVal inPort As Integer, ByVal strServer As String, ByVal bitIsSSL As Boolean, ByVal bitUseAuthentication As Boolean) As Boolean
            Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
            Dim objSMTP As Smtp
            Try
                ValidateSMTP = False
                Dim objServer As SmtpServer = New SmtpServer
                objSMTP = New Smtp
                If bitUseAuthentication Then
                    objServer.AuthMethods = AuthenticationMethods.Auto
                    objServer.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                Else
                    objServer.AuthMethods = AuthenticationMethods.None
                    objServer.AuthOptions = AuthenticationOptions.None
                End If

                objServer.AccountName = strAccountName
                objServer.Password = strPassword
                objServer.Port = inPort
                objServer.Name = strServer
                If bitIsSSL Then
                    objServer.SslMode = SslStartupMode.UseStartTls
                End If

                objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                objSMTP.SmtpServers.Add(objServer)

                objSMTP.From.AsString = strAccountName

                objSMTP.To.AsString = "noreply@bizautomation.com"


                objSMTP.Subject = "test SMTP"
                objSMTP.BodyHtmlText = "TEST SMTP"


                Return objSMTP.Send()


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ValidateImap(ByVal ServerUrl As String, ByVal numPort As Integer, ByVal UseUserName As Boolean, ByVal emailId As String, ByVal Password As String, ByVal bitIsSSL As Boolean) As Boolean
            Try
                Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
                Dim objImap As New Imap
                objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                Dim success As Boolean
                ValidateImap = False
                If bitIsSSL Then
                    objImap.SslMode = SslStartupMode.OnConnect
                End If
                success = objImap.Connect(ServerUrl, numPort)

                If success = True Then
                    If UseUserName = True Then
                        success = objImap.Login(emailId.Split(CChar("@"))(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                    Else
                        success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                    End If

                End If
                If objImap.IsConnected Then
                    objImap.Disconnect()
                End If
                Return success
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteEmail(ByVal numEmailHstrId As Long, ByVal numDomainID As Long) As Boolean
            Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
            Dim objImap As New Imap
            objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
            If File.Exists("C:\log.txt") Then
                objImap.Log.Filename = "C:\log.txt"
                objImap.Log.Enabled = True
            Else
                objImap.Log.Enabled = False
            End If
            Dim dtTableDtl As New DataTable
            Try
                Dim emailId As String
                Dim isOauthTokenExists As Boolean = False
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objUserAccess.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                    emailId = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                    If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                        Dim token As New Google.Apis.Auth.OAuth2.Responses.TokenResponse() With {.AccessToken = CCommon.ToString(dt.Rows(0)("vcMailAccessToken")), .RefreshToken = CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))}
                        Dim secrets As New Google.Apis.Auth.OAuth2.ClientSecrets() With {.ClientId = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientID")), .ClientSecret = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientSecret"))}
                        Dim credentials As New Google.Apis.Auth.OAuth2.UserCredential(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = secrets}), "Bizautomation.com", token)

                        If token.IsExpired(credentials.Flow.Clock) Then
                            Dim isTokenRefereshed As Boolean = credentials.RefreshTokenAsync(Threading.CancellationToken.None).Result

                            If isTokenRefereshed Then
                                objUserAccess.UpdateMailRefreshToken(1, credentials.Token.AccessToken, credentials.Token.RefreshToken, 0)
                            End If
                        End If

                        Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), credentials.Token.AccessToken)

                        objImap.Connect("imap.gmail.com")

                        Try
                            If Not objImap.Login(Nothing, xoauthKey, AuthenticationMethods.SaslOAuth2, AuthenticationOptions.None, Nothing) Then
                                Throw New Exception("Could Not Connect To the Server")
                            End If
                        Catch ex1 As Exception
                            Throw New Exception("Imap disabled")
                        End Try

                        isOauthTokenExists = True
                    ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                        Dim objTuple As Tuple(Of Boolean, String) = PrepareImapForOffice365(CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), objImap, dt)

                        If Not CCommon.ToBool(objTuple.Item1) AndAlso Not String.IsNullOrEmpty(objTuple.Item2) Then
                            Throw New Exception("Imap disabled")
                        End If

                        isOauthTokenExists = CCommon.ToBool(objTuple.Item1)
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim success As Boolean
                    Dim ServerUrl As String
                    Dim Password As String
                    Dim UseUserName As Boolean

                    objUserAccess.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                    objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    Dim dtCredientials As DataTable = objUserAccess.GetImapDtls()

                    If dtCredientials.Rows.Count > 0 Then
                        If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                            Dim strPassword As String
                            Dim objCommon As New CCommon
                            strPassword = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                            ServerUrl = CStr(dtCredientials.Rows(0).Item("vcImapServerUrl"))
                            UseUserName = CBool(dtCredientials.Rows(0).Item("bitUseUserName"))
                            emailId = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                            Password = strPassword
                            If CBool(dtCredientials.Rows(0).Item("bitSSl")) = True Then
                                objImap.SslMode = SslStartupMode.OnConnect
                            End If
                        Else
                            Throw New Exception("Imap Is not Integrated")
                        End If
                    Else
                        Throw New Exception("Imap Is not Integrated")
                    End If

                    Try
                        success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                    Catch ex1 As Exception
                        Throw New Exception("Imap disabled")
                    End Try

                    If (success <> True) Then
                        Throw New Exception("Could Not Connect To the Server. Imap disabled")
                    End If

                    Try
                        If UseUserName = True Then
                            success = objImap.Login(emailId.Split(CChar("@"))(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        Else
                            success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        End If
                    Catch ex2 As Exception
                        Throw New Exception("Imap disabled")
                    End Try
                    If (success <> True) Then
                        Throw New Exception("Invalid Credentials")
                    End If
                End If

                Dim ObjOutlook As New COutlook
                Dim dtTable As DataTable

                ObjOutlook.DomainID = numDomainID
                ObjOutlook.numEmailHstrID = numEmailHstrId
                ObjOutlook.ClientTimeZoneOffset = CCommon.ToInteger(Current.Session("ClientMachineUTCTimeOffset"))
                dtTable = ObjOutlook.getMail()
                If dtTable.Rows.Count > 0 Then
                    If dtTable.Rows(0).Item("Source").ToString = "I" Then
                        
                        'Added By Rajesh for real time Message will be Move in Gmail/Trash Folder ||Date:27th-Nov-2014
                        Return objImap.MoveMessages(dtTable.Rows(0).Item("numUId").ToString(), True, "[Gmail]/Trash")

                        Return True
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteEmail(ByVal numEmailHstrId As Long, ByVal numUID As String, ByVal folderName As String, ByVal chrSource As String) As Boolean
            Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
            Dim objImap As New Imap
            objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
            If File.Exists("C:\log.txt") Then
                objImap.Log.Filename = "C:\log.txt"
                objImap.Log.Enabled = True
            Else
                objImap.Log.Enabled = False
            End If
            Dim dtTableDtl As New DataTable
            Try
                Dim emailId As String
                Dim isOauthTokenExists As Boolean = False
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objUserAccess.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                    emailId = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                    If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                        Dim token As New Google.Apis.Auth.OAuth2.Responses.TokenResponse() With {.AccessToken = CCommon.ToString(dt.Rows(0)("vcMailAccessToken")), .RefreshToken = CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))}
                        Dim secrets As New Google.Apis.Auth.OAuth2.ClientSecrets() With {.ClientId = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientID")), .ClientSecret = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientSecret"))}
                        Dim credentials As New Google.Apis.Auth.OAuth2.UserCredential(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = secrets}), "Bizautomation.com", token)

                        If token.IsExpired(credentials.Flow.Clock) Then
                            Dim isTokenRefereshed As Boolean = credentials.RefreshTokenAsync(Threading.CancellationToken.None).Result

                            If isTokenRefereshed Then
                                objUserAccess.UpdateMailRefreshToken(1, credentials.Token.AccessToken, credentials.Token.RefreshToken, 0)
                            End If
                        End If

                        Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), credentials.Token.AccessToken)

                        objImap.Connect("imap.gmail.com")

                        Try
                            If Not objImap.Login(Nothing, xoauthKey, AuthenticationMethods.SaslOAuth2, AuthenticationOptions.None, Nothing) Then
                                Throw New Exception("Could Not Connect To the Server")
                            End If
                        Catch ex1 As Exception
                            Throw New Exception("Imap disabled")
                        End Try

                        isOauthTokenExists = True
                    ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                        Dim objTuple As Tuple(Of Boolean, String) = PrepareImapForOffice365(CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), objImap, dt)

                        If Not CCommon.ToBool(objTuple.Item1) AndAlso Not String.IsNullOrEmpty(objTuple.Item2) Then
                            Throw New Exception("Imap disabled")
                        End If

                        isOauthTokenExists = CCommon.ToBool(objTuple.Item1)
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim success As Boolean
                    Dim ServerUrl As String
                    Dim Password As String
                    Dim UseUserName As Boolean

                    objUserAccess.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                    objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    Dim dtCredientials As DataTable = objUserAccess.GetImapDtls()

                    If dtCredientials.Rows.Count > 0 Then
                        If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                            Dim strPassword As String
                            Dim objCommon As New CCommon
                            strPassword = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                            ServerUrl = CStr(dtCredientials.Rows(0).Item("vcImapServerUrl"))
                            UseUserName = CBool(dtCredientials.Rows(0).Item("bitUseUserName"))
                            emailId = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                            Password = strPassword
                            If CBool(dtCredientials.Rows(0).Item("bitSSl")) = True Then
                                objImap.SslMode = SslStartupMode.OnConnect
                            End If
                        Else
                            Throw New Exception("Imap Is not Integrated")
                        End If
                    Else
                        Throw New Exception("Imap Is not Integrated")
                    End If

                    Try
                        success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                    Catch ex1 As Exception
                        Throw New Exception("Imap disabled")
                    End Try

                    If (success <> True) Then
                        Throw New Exception("Could Not Connect To the Server. Imap disabled")
                    End If

                    Try
                        If UseUserName = True Then
                            success = objImap.Login(emailId.Split(CChar("@"))(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        Else
                            success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        End If
                    Catch ex2 As Exception
                        Throw New Exception("Imap disabled")
                    End Try
                    If (success <> True) Then
                        Throw New Exception("Invalid Credentials")
                    End If
                End If

                If chrSource = "I" Then
                    Dim lstfolders As FolderCollection
                    lstfolders = objImap.DownloadFolders()
                    Dim strFoldersFromServer As String
                    strFoldersFromServer = ""
                    For Each objFolder As Folder In lstfolders
                        If objFolder.ShortName.ToLower() = folderName.ToLower() Then
                            folderName = objFolder.Name
                        End If
                        If objFolder.ShortName = "Trash" Or objFolder.ShortName = "Deleted" Then
                            strFoldersFromServer = objFolder.Name
                        End If
                    Next
                    If String.IsNullOrEmpty(folderName) Then
                        objImap.SelectFolder("Inbox")
                    Else
                        objImap.SelectFolder(folderName)
                    End If

                    'Added By Rajesh for real time Message will be Move in Gmail/Trash Folder ||Date:27th-Nov-2014
                    'Dim res As Boolean = objImap.MoveMessages(numUID, True, "[Gmail]/Trash")
                    Dim res As Boolean = objImap.MoveMessages(numUID, True, strFoldersFromServer)
                    If res = True Then
                        Dim objOutlook As New COutlook
                        objOutlook.numEmailHstrID = CCommon.ToLong(numEmailHstrId)
                        objOutlook.DomainID = CCommon.ToLong(DomainID)
                        objOutlook.deleteInboxItem()
                    End If
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetEmail(ByVal numEmailHstrId As Long, ByVal numDomainID As Long) As DataTable

            Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
            Dim objImap As New Imap
            objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
            If File.Exists("C:\log.txt") Then
                objImap.Log.Filename = "C:\log.txt"
                objImap.Log.Enabled = True
            Else
                objImap.Log.Enabled = False
            End If
            Dim dtTableDtl As New DataTable
            Try
                Dim ObjOutlook As New COutlook
                Dim dtTable As DataTable

                ObjOutlook.DomainID = numDomainID
                ObjOutlook.ClientTimeZoneOffset = CCommon.ToInteger(Current.Session("ClientMachineUTCTimeOffset"))
                ObjOutlook.numEmailHstrID = numEmailHstrId
                dtTable = ObjOutlook.getMail()
                If dtTable.Rows.Count > 0 Then
                    Dim objUserAccess As New UserAccess
                    Dim dtCredientials As DataTable
                    objUserAccess.UserCntID = CLng(dtTable.Rows(0).Item("numUserCntId"))
                    objUserAccess.DomainID = numDomainID
                    dtCredientials = objUserAccess.GetImapDtls()
                    Dim success As Boolean
                    Dim ServerUrl As String
                    Dim emailId As String
                    Dim Password As String
                    If dtCredientials.Rows.Count > 0 Then
                        If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then

                            Dim strPassword As String
                            Dim objCommon As New CCommon
                            strPassword = objCommon.Decrypt(dtCredientials.Rows(0).Item("vcImapPassword").ToString)

                            ServerUrl = dtCredientials.Rows(0).Item("vcImapServerUrl").ToString
                            emailId = dtCredientials.Rows(0).Item("vcEmailID").ToString
                            Password = strPassword
                            If CBool(dtCredientials.Rows(0).Item("bitSSl")) = True Then
                                objImap.SslMode = SslStartupMode.OnConnect
                            End If
                        Else
                            Return dtTableDtl
                            Exit Function
                        End If
                    Else
                        Return dtTableDtl
                        Exit Function
                    End If

                    success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                    If (success <> True) Then
                        Return dtTableDtl
                        Exit Function
                    End If
                    success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                    If (success <> True) Then
                        Return dtTableDtl
                        Exit Function
                    End If

                    objImap.SelectFolder("Inbox") 'To select the folder for read-only access, the developer should use ExamineFolder method.

                    Dim msg As MailMessage

                    If objImap.MessageCount > 0 Then
                        msg = objImap.DownloadEntireMessage(CLng(dtTable.Rows(0).Item("numUId")), True)
                        msg.Parser.HeadersAsHtml = True
                        msg.Parser.PlainToHtmlMode = PlainToHtmlAutoConvert.IfNoHtml
                        msg.Parser.PlainToHtmlOptions = PlainToHtmlConvertOptions.UriToLink
                        msg.Parser.Apply()
                        'Mark message as read/seen
                        objImap.SetMessageFlags(CCommon.ToString(dtTable.Rows(0).Item("numUId")), True, SystemMessageFlags.Seen, MessageFlagAction.Add)
                    End If

                    ' Dim msg As MailMessage = imp.DownloadEntireMessage(dtTable.Rows(0).Item("numUId").ToString, True)


                    If Not msg Is Nothing Then

                        ' imp.SetMessageFlags(dtTable.Rows(0).Item("numUId").ToString, True, SystemMessageFlags.Seen, MessageFlagAction.Replace, True)
                        Dim objDR As System.Data.DataRow
                        dtTableDtl.Columns.Add("FromEmail", GetType(String))
                        dtTableDtl.Columns.Add("FromName", GetType(String))
                        dtTableDtl.Columns.Add("Subject", GetType(String))
                        dtTableDtl.Columns.Add("Sent", GetType(Date))
                        dtTableDtl.Columns.Add("Size", GetType(String))
                        dtTableDtl.Columns.Add("HasAttachments", GetType(Boolean))
                        dtTableDtl.Columns.Add("ToName", GetType(String))
                        dtTableDtl.Columns.Add("ToAddress", GetType(String))
                        dtTableDtl.Columns.Add("CCName", GetType(String))
                        dtTableDtl.Columns.Add("CCAddress", GetType(String))
                        dtTableDtl.Columns.Add("AttachmentName", GetType(String))
                        dtTableDtl.Columns.Add("Body", GetType(String))
                        dtTableDtl.Columns.Add("BodyText", GetType(String))

                        Dim j As Integer = 0

                        objDR = dtTableDtl.NewRow


                        objDR("FromEmail") = msg.From.Email
                        objDR("FromName") = msg.From.DisplayName
                        objDR("Subject") = msg.Subject

                        'msg.Parser.HeadersAsHtml = True
                        'msg.Parser.PlainToHtmlMode = PlainToHtmlAutoConvert.IfNoHtml
                        'msg.Parser.PlainToHtmlOptions = PlainToHtmlConvertOptions.UriToLink
                        'msg.Parser.PlainToHtmlMode = PlainToHtmlAutoConvert.IfPlain
                        'msg.Parser.Apply()


                        objDR("Body") = msg.BodyHtmlText
                        objDR("BodyText") = msg.BodyPlainText
                        objDR("Sent") = msg.DateReceived
                        objDR("Size") = msg.Size

                        Dim ToName As String = ""
                        Dim ToAddress As String = ""
                        Dim CCName As String = ""
                        Dim CCAddress As String = ""

                        For j = 0 To msg.To.Count - 1
                            If ToName = "" Then
                                ToName = msg.To(j).DisplayName
                            Else
                                ToName = ToName & "," & msg.To(j).DisplayName
                            End If
                            If ToAddress = "" Then
                                ToAddress = msg.To(j).Email
                            Else
                                ToAddress = ToAddress & "," & msg.To(j).Email
                            End If

                        Next
                        j = 0
                        For j = 0 To msg.Cc.Count - 1
                            If CCName = "" Then
                                CCName = msg.Cc(j).DisplayName
                            Else
                                CCName = CCName & msg.Cc(j).DisplayName
                            End If
                            If CCAddress = "" Then
                                CCAddress = msg.Cc(j).Email
                            Else
                                CCAddress = CCAddress & msg.Cc(j).Email
                            End If
                        Next
                        objDR("HasAttachments") = IIf(msg.Attachments.Count = 0, False, True)

                        Dim strAttachment As String = ""
                        Dim strAttachmentType As String = ""
                        If msg.Attachments.Count > 0 Then
                            msg.Attachments.SaveAll(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\")
                            For j = 0 To msg.Attachments.Count - 1
                                Dim attach As Attachment = msg.Attachments(j)
                                If strAttachment = "" Then
                                    strAttachment = attach.Filename
                                    strAttachmentType = attach.ContentType
                                Else
                                    strAttachment = strAttachment & "|" & attach.Filename
                                    strAttachmentType = strAttachmentType & "|" & attach.ContentType
                                End If
                            Next

                        End If
                        objDR("ToName") = ToName
                        objDR("ToAddress") = ToAddress
                        objDR("CCName") = CCName
                        objDR("CCAddress") = CCAddress
                        objDR("AttachmentName") = strAttachment

                        dtTableDtl.Rows.Add(objDR)
                    End If

                End If
                If objImap.IsConnected Then
                    objImap.Disconnect()
                End If
                Return dtTableDtl
            Catch ex As MailBee.ImapMail.MailBeeImapMessageIndexNotFoundException
                Throw New Exception("The message you’re trying to retrieve is not available because you’ve removed from your inbox on your email server.", ex.InnerException)
            Catch ex As Exception
                If objImap.IsConnected Then
                    objImap.Disconnect()
                End If
                Throw ex
            End Try
        End Function

        Public Sub UpdateReadStatus(ByVal strEmailHstrId As String, ByVal numDomainID As Long, ByVal isRead As Boolean)

            Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
            Dim objImap As New Imap
            objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
            If File.Exists("C:\log.txt") Then
                objImap.Log.Filename = "C:\log.txt"
                objImap.Log.Enabled = True
            Else
                objImap.Log.Enabled = False
            End If
            Dim dtTableDtl As New DataTable
            Try
                Dim emailId As String
                Dim isOauthTokenExists As Boolean = False
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objUserAccess.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                    emailId = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                    If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                        Dim token As New Google.Apis.Auth.OAuth2.Responses.TokenResponse() With {.AccessToken = CCommon.ToString(dt.Rows(0)("vcMailAccessToken")), .RefreshToken = CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))}
                        Dim secrets As New Google.Apis.Auth.OAuth2.ClientSecrets() With {.ClientId = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientID")), .ClientSecret = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientSecret"))}
                        Dim credentials As New Google.Apis.Auth.OAuth2.UserCredential(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = secrets}), "Bizautomation.com", token)

                        If token.IsExpired(credentials.Flow.Clock) Then
                            Dim isTokenRefereshed As Boolean = credentials.RefreshTokenAsync(Threading.CancellationToken.None).Result

                            If isTokenRefereshed Then
                                objUserAccess.UpdateMailRefreshToken(1, credentials.Token.AccessToken, credentials.Token.RefreshToken, 0)
                            End If
                        End If

                        Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), credentials.Token.AccessToken)

                        objImap.Connect("imap.gmail.com")

                        Try
                            If Not objImap.Login(Nothing, xoauthKey, AuthenticationMethods.SaslOAuth2, AuthenticationOptions.None, Nothing) Then
                                Throw New Exception("Could Not Connect To the Server")
                            End If
                        Catch ex1 As Exception
                            Throw New Exception("Imap disabled")
                        End Try

                        isOauthTokenExists = True
                    ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                        Dim objTuple As Tuple(Of Boolean, String) = PrepareImapForOffice365(CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), objImap, dt)

                        If Not CCommon.ToBool(objTuple.Item1) AndAlso Not String.IsNullOrEmpty(objTuple.Item2) Then
                            Throw New Exception("Imap disabled")
                        End If

                        isOauthTokenExists = CCommon.ToBool(objTuple.Item1)
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim success As Boolean
                    Dim ServerUrl As String
                    Dim Password As String
                    Dim UseUserName As Boolean

                    objUserAccess.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                    objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    Dim dtCredientials As DataTable = objUserAccess.GetImapDtls()

                    If dtCredientials.Rows.Count > 0 Then
                        If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                            Dim strPassword As String
                            Dim objCommon As New CCommon
                            strPassword = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                            ServerUrl = CStr(dtCredientials.Rows(0).Item("vcImapServerUrl"))
                            UseUserName = CBool(dtCredientials.Rows(0).Item("bitUseUserName"))
                            emailId = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                            Password = strPassword
                            If CBool(dtCredientials.Rows(0).Item("bitSSl")) = True Then
                                objImap.SslMode = SslStartupMode.OnConnect
                            End If
                        Else
                            Throw New Exception("Imap Is not Integrated")
                        End If
                    Else
                        Throw New Exception("Imap Is not Integrated")
                    End If

                    Try
                        success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                    Catch ex1 As Exception
                        Throw New Exception("Imap disabled")
                    End Try

                    If (success <> True) Then
                        Throw New Exception("Could Not Connect To the Server. Imap disabled")
                    End If

                    Try
                        If UseUserName = True Then
                            success = objImap.Login(emailId.Split(CChar("@"))(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        Else
                            success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        End If
                    Catch ex2 As Exception
                        Throw New Exception("Imap disabled")
                    End Try
                    If (success <> True) Then
                        Throw New Exception("Invalid Credentials")
                    End If
                End If

                Dim ObjOutlook As New COutlook
                Dim dtTable As DataTable

                Dim strArrayHistoryId As String()
                strArrayHistoryId = strEmailHstrId.Split(CChar(","))
                For Each numEmailHstrId As String In strArrayHistoryId
                    ObjOutlook = New COutlook
                    ObjOutlook.DomainID = numDomainID
                    ObjOutlook.ClientTimeZoneOffset = CCommon.ToInteger(Current.Session("ClientMachineUTCTimeOffset"))
                    ObjOutlook.numEmailHstrID = CCommon.ToLong(numEmailHstrId)
                    dtTable = ObjOutlook.getMail()
                    If dtTable.Rows.Count > 0 Then
                        Dim strFolderName As String
                        strFolderName = CCommon.ToString(dtTable.Rows(0).Item("vcNodeName"))
                        Dim lstfolders As FolderCollection
                        lstfolders = objImap.DownloadFolders()
                        For Each objFolder As Folder In lstfolders
                            If objFolder.ShortName = strFolderName Then
                                strFolderName = objFolder.Name
                            End If
                        Next
                        If String.IsNullOrEmpty(strFolderName) Then
                            objImap.SelectFolder("Inbox")
                        Else
                            objImap.SelectFolder(strFolderName) 'To select the folder for read-only access, the developer should use ExamineFolder method.
                        End If

                        Dim msg As MailMessage
                        If (isRead = True) Then
                            objImap.SetMessageFlags(CCommon.ToString(dtTable.Rows(0).Item("numUId")), True, SystemMessageFlags.Seen, MessageFlagAction.Replace)
                        Else
                            objImap.SetMessageFlags(CCommon.ToString(dtTable.Rows(0).Item("numUId")), True, SystemMessageFlags.None, MessageFlagAction.Replace)
                        End If
                    End If
                    If objImap.IsConnected Then
                        objImap.Disconnect()
                    End If

                Next
                ObjOutlook = New COutlook
                ObjOutlook.DomainID = numDomainID
                ObjOutlook.strRecordsIds = strEmailHstrId
                ObjOutlook.IsRead = isRead
                ObjOutlook.UpdateMultipleEmailFlag()
            Catch ex As MailBee.ImapMail.MailBeeImapMessageIndexNotFoundException
                Throw New Exception("The message you’re trying to retrieve is not available because you’ve removed from your inbox on your email server.", ex.InnerException)
            Catch ex As Exception
                If objImap.IsConnected Then
                    objImap.Disconnect()
                End If
                Throw ex
            End Try
        End Sub

        Public Function SetEmailFlag(ByVal numEmailHstrId As Long, ByVal numDomainID As Long) As DataTable

            Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
            Dim objImap As New Imap
            objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
            If File.Exists("C:\log.txt") Then
                objImap.Log.Filename = "C:\log.txt"
                objImap.Log.Enabled = True
            Else
                objImap.Log.Enabled = False
            End If
            Dim dtTableDtl As New DataTable
            Try
                Dim emailId As String
                Dim isOauthTokenExists As Boolean = False
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objUserAccess.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                    emailId = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                    If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                        Dim token As New Google.Apis.Auth.OAuth2.Responses.TokenResponse() With {.AccessToken = CCommon.ToString(dt.Rows(0)("vcMailAccessToken")), .RefreshToken = CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))}
                        Dim secrets As New Google.Apis.Auth.OAuth2.ClientSecrets() With {.ClientId = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientID")), .ClientSecret = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientSecret"))}
                        Dim credentials As New Google.Apis.Auth.OAuth2.UserCredential(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = secrets}), "Bizautomation.com", token)

                        If token.IsExpired(credentials.Flow.Clock) Then
                            Dim isTokenRefereshed As Boolean = credentials.RefreshTokenAsync(Threading.CancellationToken.None).Result

                            If isTokenRefereshed Then
                                objUserAccess.UpdateMailRefreshToken(1, credentials.Token.AccessToken, credentials.Token.RefreshToken, 0)
                            End If
                        End If

                        Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), credentials.Token.AccessToken)

                        objImap.Connect("imap.gmail.com")

                        Try
                            If Not objImap.Login(Nothing, xoauthKey, AuthenticationMethods.SaslOAuth2, AuthenticationOptions.None, Nothing) Then
                                Throw New Exception("Could Not Connect To the Server")
                            End If
                        Catch ex1 As Exception
                            Throw New Exception("Imap disabled")
                        End Try

                        isOauthTokenExists = True
                    ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                        Dim objTuple As Tuple(Of Boolean, String) = PrepareImapForOffice365(CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), objImap, dt)

                        If Not CCommon.ToBool(objTuple.Item1) AndAlso Not String.IsNullOrEmpty(objTuple.Item2) Then
                            Throw New Exception("Imap disabled")
                        End If

                        isOauthTokenExists = CCommon.ToBool(objTuple.Item1)
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim success As Boolean
                    Dim ServerUrl As String
                    Dim Password As String
                    Dim UseUserName As Boolean

                    objUserAccess.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                    objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    Dim dtCredientials As DataTable = objUserAccess.GetImapDtls()

                    If dtCredientials.Rows.Count > 0 Then
                        If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                            Dim strPassword As String
                            Dim objCommon As New CCommon
                            strPassword = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                            ServerUrl = CStr(dtCredientials.Rows(0).Item("vcImapServerUrl"))
                            UseUserName = CBool(dtCredientials.Rows(0).Item("bitUseUserName"))
                            emailId = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                            Password = strPassword
                            If CBool(dtCredientials.Rows(0).Item("bitSSl")) = True Then
                                objImap.SslMode = SslStartupMode.OnConnect
                            End If
                        Else
                            Throw New Exception("Imap Is not Integrated")
                        End If
                    Else
                        Throw New Exception("Imap Is not Integrated")
                    End If

                    Try
                        success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                    Catch ex1 As Exception
                        Throw New Exception("Imap disabled")
                    End Try

                    If (success <> True) Then
                        Throw New Exception("Could Not Connect To the Server. Imap disabled")
                    End If

                    Try
                        If UseUserName = True Then
                            success = objImap.Login(emailId.Split(CChar("@"))(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        Else
                            success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        End If
                    Catch ex2 As Exception
                        Throw New Exception("Imap disabled")
                    End Try
                    If (success <> True) Then
                        Throw New Exception("Invalid Credentials")
                    End If
                End If

                Dim ObjOutlook As New COutlook
                Dim dtTable As DataTable

                ObjOutlook.DomainID = numDomainID
                ObjOutlook.ClientTimeZoneOffset = CCommon.ToInteger(Current.Session("ClientMachineUTCTimeOffset"))
                ObjOutlook.numEmailHstrID = numEmailHstrId
                dtTable = ObjOutlook.getMail()
                If dtTable.Rows.Count > 0 Then
                    objImap.SelectFolder("Inbox") 'To select the folder for read-only access, the developer should use ExamineFolder method.

                    Dim msg As MailMessage

                    If objImap.MessageCount > 0 Then
                        msg = objImap.DownloadEntireMessage(CLng(dtTable.Rows(0).Item("numUId")), True)
                        msg.Parser.HeadersAsHtml = True
                        msg.Parser.PlainToHtmlMode = PlainToHtmlAutoConvert.IfNoHtml
                        msg.Parser.PlainToHtmlOptions = PlainToHtmlConvertOptions.UriToLink
                        msg.Parser.Apply()

                        objImap.SetMessageFlags(CCommon.ToString(dtTable.Rows(0).Item("numUId")), True, "[Gmail]/Spam", MessageFlagAction.Add, True)
                    End If
                End If
                If objImap.IsConnected Then
                    objImap.Disconnect()
                End If
                Return dtTableDtl
            Catch ex As MailBee.ImapMail.MailBeeImapMessageIndexNotFoundException
                Throw New Exception("The message you’re trying to retrieve is not available because you’ve removed from your inbox on your email server.", ex.InnerException)
            Catch ex As Exception
                If objImap.IsConnected Then
                    objImap.Disconnect()
                End If
                Throw ex
            End Try
        End Function

        Sub MoveCopyGmail(ByVal vcEmailHstrID As String, ByVal SourceFolder As String, ByVal DestinationFolder As String, ByVal nodeID As Int32, ByVal IsCopy As Boolean)
            Try
                Dim success As Boolean
                Dim ServerUrl As String
                Dim emailId As String
                Dim Password As String
                Dim strPassword As String
                Dim dtTable As DataTable
                Dim dtCredientials As DataTable

                Imap.LicenseKey = ConfigurationManager.AppSettings("ImapLicense")
                Dim objImap As New Imap
                objImap.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                'GET IMAP DETAILS OF USER
                Dim isOauthTokenExists As Boolean = False
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objUserAccess.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                    emailId = CCommon.ToString(dt.Rows(0)("vcPSMTPUserName"))
                    If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                        Dim token As New Google.Apis.Auth.OAuth2.Responses.TokenResponse() With {.AccessToken = CCommon.ToString(dt.Rows(0)("vcMailAccessToken")), .RefreshToken = CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))}
                        Dim secrets As New Google.Apis.Auth.OAuth2.ClientSecrets() With {.ClientId = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientID")), .ClientSecret = CCommon.ToString(ConfigurationManager.AppSettings("GmailAppClientSecret"))}
                        Dim credentials As New Google.Apis.Auth.OAuth2.UserCredential(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow(New Google.Apis.Auth.OAuth2.Flows.GoogleAuthorizationCodeFlow.Initializer() With {.ClientSecrets = secrets}), "Bizautomation.com", token)

                        If token.IsExpired(credentials.Flow.Clock) Then
                            Dim isTokenRefereshed As Boolean = credentials.RefreshTokenAsync(Threading.CancellationToken.None).Result

                            If isTokenRefereshed Then
                                objUserAccess.UpdateMailRefreshToken(1, credentials.Token.AccessToken, credentials.Token.RefreshToken, 0)
                            End If
                        End If

                        Dim xoauthKey As String = OAuth2.GetXOAuthKeyStatic(CCommon.ToString(dt.Rows(0)("vcPSMTPUserName")), credentials.Token.AccessToken)

                        objImap.Connect("imap.gmail.com")

                        Try
                            If Not objImap.Login(Nothing, xoauthKey, AuthenticationMethods.SaslOAuth2, AuthenticationOptions.None, Nothing) Then
                                Throw New Exception("Could Not Connect To the Server")
                            End If
                        Catch ex1 As Exception
                            Throw New Exception("Imap disabled")
                        End Try

                        isOauthTokenExists = True
                    ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                        Dim objTuple As Tuple(Of Boolean, String) = PrepareImapForOffice365(CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), objImap, dt)

                        If Not CCommon.ToBool(objTuple.Item1) AndAlso Not String.IsNullOrEmpty(objTuple.Item2) Then
                            Throw New Exception("Imap disabled")
                        End If

                        isOauthTokenExists = CCommon.ToBool(objTuple.Item1)
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim UseUserName As Boolean

                    dtCredientials = objUserAccess.GetImapDtls()

                    If dtCredientials.Rows.Count > 0 Then
                        If CBool(dtCredientials.Rows(0).Item("bitImap")) = True Then
                            Dim objCommon As New CCommon
                            strPassword = objCommon.Decrypt(CStr(dtCredientials.Rows(0).Item("vcImapPassword")))
                            ServerUrl = CStr(dtCredientials.Rows(0).Item("vcImapServerUrl"))
                            UseUserName = CBool(dtCredientials.Rows(0).Item("bitUseUserName"))
                            emailId = CStr(dtCredientials.Rows(0).Item("vcEmailID"))
                            Password = strPassword
                            If CBool(dtCredientials.Rows(0).Item("bitSSl")) = True Then
                                objImap.SslMode = SslStartupMode.OnConnect
                            End If
                        Else
                            Throw New Exception("Imap Is not Integrated")
                        End If
                    Else
                        Throw New Exception("Imap Is not Integrated")
                    End If

                    Try
                        success = objImap.Connect(ServerUrl, CInt(dtCredientials.Rows(0).Item("numPort")))
                    Catch ex1 As Exception
                        Throw New Exception("Imap disabled")
                    End Try

                    If (success <> True) Then
                        Throw New Exception("Could Not Connect To the Server. Imap disabled")
                    End If

                    Try
                        If UseUserName = True Then
                            success = objImap.Login(emailId.Split(CChar("@"))(0), Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        Else
                            success = objImap.Login(emailId, Password, AuthenticationMethods.Auto, AuthenticationOptions.PreferSimpleMethods, Nothing)
                        End If
                    Catch ex2 As Exception
                        Throw New Exception("Imap disabled")
                    End Try
                    If (success <> True) Then
                        Throw New Exception("Invalid Credentials")
                    End If
                End If

                Try
                    'If SourceFolder = "Sent Messages" Then
                    '    SourceFolder = "[Gmail]/Sent Mail"
                    'End If
                    Dim lstfolders As FolderCollection
                    lstfolders = objImap.DownloadFolders()
                    Dim objSourceFolder As Folder
                    Dim objDestinationFolder As Folder
                    objSourceFolder = (From p In lstfolders Where p.ShortName = CCommon.ToString(SourceFolder) Select p).FirstOrDefault()

                    Try
                        objImap.SelectFolder(objSourceFolder.Name)

                        Try
                            objDestinationFolder = (From p In lstfolders Where p.ShortName = CCommon.ToString(DestinationFolder) Select p).FirstOrDefault()
                            'CHECK IF DESTINATION FOLDER EXIST IF NOT THEN CREATE 
                            Dim folderstatus As FolderStatus = objImap.GetFolderStatus(objDestinationFolder.Name)
                            DestinationFolder = objDestinationFolder.Name
                        Catch ex As MailBeeImapNegativeResponseException
                            objImap.CreateFolder(DestinationFolder.Trim())
                        End Try

                    Catch ex As Exception

                    End Try

                    Dim objOutLook As New COutlook
                    objOutLook.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    objOutLook.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))

                    'GET UIDs OF SELECTED EMAILS
                    dtTable = objOutLook.GetEmailHistryByIDs(vcEmailHstrID)

                    'MOVE/COPY SELECTED EMAILS IN BIZ
                    MoveCopyByIDs(vcEmailHstrID, nodeID, dtTable, IsCopy, DestinationFolder, objImap)
                Catch ex As MailBeeImapNegativeResponseException
                    'SOURCE FOLDER EMAIL ARCHIVE, DELETED MAILS WILL NOT BE AVAILABLE IN GMAIL
                Catch ex As Exception
                    Throw
                End Try

                objImap.Disconnect()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub MoveCopyByIDs(ByVal vcEmailHstrID As String, ByVal nodeID As Int32, ByVal dtTable As DataTable,
                                  ByVal IsCopy As Boolean, ByVal DestinationFolder As String, ByRef objImap As Imap)
            Try
                Dim objCommon As New CCommon
                objCommon.DomainID = DomainID
                objCommon.Mode = 45

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                'MOVE/COPY SELECTED EMAILS IN GAMIL
                If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then
                    For Each newRow As DataRow In dtTable.Rows
                        Dim objUIDResult As New MailBee.ImapMail.UidPlusResult
                        Dim res As Boolean

                        Try
                            If IsCopy Then
                                res = objImap.CopyMessages(newRow("numUid").ToString(), True, DestinationFolder, objUIDResult)
                            Else
                                res = objImap.MoveMessages(newRow("numUid").ToString(), True, DestinationFolder, objUIDResult)
                            End If
                        Catch ex As Exception
                            'DO NOT TROW EXCEPTION
                        End Try


                        'MOVE/COPY SELECTED EMAILS IN BIZ

                        arParms(0) = New Npgsql.NpgsqlParameter("@numEmailHstrID", NpgsqlTypes.NpgsqlDbType.BigInt)
                        arParms(0).Value = CCommon.ToLong(newRow("numEmailHstrID"))

                        arParms(1) = New Npgsql.NpgsqlParameter("@numNodeId", NpgsqlTypes.NpgsqlDbType.BigInt)
                        arParms(1).Value = nodeID

                        arParms(2) = New Npgsql.NpgsqlParameter("@ModeType", NpgsqlTypes.NpgsqlDbType.BigInt)
                        arParms(2).Value = CCommon.ToInteger(IsCopy)

                        arParms(3) = New Npgsql.NpgsqlParameter("@numUID", NpgsqlTypes.NpgsqlDbType.BigInt)
                        arParms(3).Value = CCommon.ToLong(objUIDResult.DestUidString)

                        SqlDAL.ExecuteNonQuery(connString, "USP_EmailHistory_MoveCopyIDs", arParms)
                    Next


                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        ''' <summary>
        ''' Pass Domain ID only if it is domain emails 
        ''' ************************************
        ''' this function must have column vcFirstName and EMail field in dtMailMerge if it has more than one receipient
        ''' </summary>
        ''' <param name="strSubject"></param>
        ''' <param name="strBody"></param>
        ''' <param name="strCC"></param>
        ''' <param name="strFrom"></param>
        ''' <param name="strTo"></param>
        ''' <param name="dtTable"></param>
        ''' <param name="FromDisplayName"></param>
        ''' <param name="strBCC"></param>
        ''' <param name="DomainID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        ''' 
        ''
        '

        Public Function SendEmail(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String,
                                  ByRef dtMailMerge As DataTable, Optional ByVal FromDisplayName As String = "", Optional ByVal strBCC As String = "",
                                  Optional ByVal DomainID As Long = 0, Optional ByVal IsAttachmentDelete As Boolean = False, Optional ByVal strPageUrl As String = "",
                                  Optional ByVal UserId As Long = 0) As Boolean
            Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
            Try
                Dim isOauthTokenExists As Boolean = False
                Dim objSMTP As Smtp = New Smtp

                If DomainID = 0 Or UserId = 0 Then
                    'First check if Gmail or Office365 oauth2 token exists
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = CCommon.ToLong(SessionObject("DomainID"))
                    objUserAccess.UserCntID = CCommon.ToLong(SessionObject("UserContactID"))
                    Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        If (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                            If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                                isOauthTokenExists = PrepareSmtpForGmail(CCommon.ToLong(SessionObject("DomainID")), CCommon.ToLong(SessionObject("UserContactID")), objSMTP, dt)
                            ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                                isOauthTokenExists = PrepareSmtpForOffice365(CCommon.ToLong(SessionObject("DomainID")), CCommon.ToLong(SessionObject("UserContactID")), objSMTP, strFrom, dt)
                            End If
                        End If
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim objServer As SmtpServer = New SmtpServer
                    GetSMTPDetails(objServer, DomainID, UserId)

                    'Add SMTP server
                    objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                    objSMTP.SmtpServers.Add(objServer)

                    If SMTPServerIntegration = False Then
                        Exit Function
                    End If
                End If

                'Logging Events
                If File.Exists("C:\log.txt") Then
                    objSMTP.Log.Filename = "C:\log.txt"
                    objSMTP.Log.Enabled = True
                Else
                    objSMTP.Log.Enabled = False
                End If
                objSMTP.Log.DisableOnException = False

                ' Subscribe to events to track send bulk mail progress.
                AddHandler objSMTP.MessageNotSent, AddressOf OnMessageNotSent

                'Add MailMessage object
                Dim msg As New MailMessage
                ' Set sender (the same value for all messages).
                msg.From.AsString = strFrom
                If FromDisplayName <> "" Then
                    msg.From.DisplayName = FromDisplayName
                Else
                    msg.From.DisplayName = _EMailFromDisplayName
                End If

                'Author:Sachin Sadhu||Date:5thDec2013||Case No:512
                'Code Starts :Sachin
                ' Set recipient pattern.
                If strPageUrl = "/BACRMUI/contact/frmComposeWindow.aspx" Then 'ComposeWindow of Email
                    Dim strToMails As String() = strTo.Split(CChar(","))
                    If strToMails.Length >= 2 Then 'More than One Recipients
                        If dtMailMerge.Rows.Count > 1 Then 'Definetly more than one
                            Dim intCount As Integer = 0
                            For intCount = 0 To dtMailMerge.Rows.Count - 1
                                msg.To.Add(dtMailMerge.Rows(intCount).Item("ContactEmail").ToString(), dtMailMerge.Rows(intCount).Item("ContactFirstName").ToString())
                            Next
                        Else
                            msg.To.AsString = strTo.Trim().TrimEnd(Char.Parse(",")) 'will be used for email alerts.. where generally it needs to be sent to only one user
                        End If
                    Else 'For Single Recipient
                        If dtMailMerge.Rows.Count > 1 Then
                            msg.To.AsString = "##ContactFirstName## <##ContactEmail##>" 'Will be used from compose new message
                        Else
                            msg.To.AsString = strTo.Trim().TrimEnd(Char.Parse(",")) 'will be used for email alerts.. where generally it needs to be sent to only one user
                        End If
                    End If
                Else 'Case Of Marketing or Broadcasting Mail
                    If dtMailMerge.Rows.Count > 1 Then
                        msg.To.AsString = "##ContactFirstName## <##ContactEmail##>" 'Will be used from compose new message
                    Else
                        msg.To.AsString = strTo.Trim().TrimEnd(Char.Parse(",")) 'will be used for email alerts.. where generally it needs to be sent to only one user
                    End If
                End If
                'End of Sachin Code


                If strCC <> "" Then
                    msg.Cc.AsString = strCC.Trim().TrimEnd(Char.Parse(","))
                End If
                If strBCC <> "" Then
                    msg.Bcc.AsString = strBCC.Trim().TrimEnd(Char.Parse(","))
                End If

                ' Subject is the same for all messages.
                msg.Subject = strSubject

                ' Set body pattern.
                msg.BodyHtmlText = strBody

                Dim dtAttachments As DataTable
                Dim strSplitedNames() As String
                Dim strLocation As String
                Dim strLocation1 As String
                Dim mailStatus As Boolean
                'Add Attachments
                If SessionObject IsNot Nothing AndAlso SessionObject.ContainsKey("Attachements") Then
                    If Not SessionObject("Attachements") Is Nothing Then
                        ' Dim dtAttachments As DataTable
                        Dim k As Integer
                        dtAttachments = CType(SessionObject("Attachements"), DataTable)

                        If Not dtAttachments Is Nothing Then
                            For k = 0 To dtAttachments.Rows.Count - 1
                                strSplitedNames = Split(dtAttachments.Rows(k).Item("FileLocation").ToString, "/")
                                dtAttachments.Rows(k).Item("FileLocation") = Replace(dtAttachments.Rows(k).Item("FileLocation").ToString, "..", ConfigurationManager.AppSettings("BACRMLocation"))
                                'Checking whether the Files is present

                                strLocation = CCommon.GetDocumentPhysicalPath() & strSplitedNames(strSplitedNames.Length - 1).ToString
                                strLocation1 = CCommon.GetDocumentPhysicalPath(DomainID) & strSplitedNames(strSplitedNames.Length - 1).ToString 'Check into domain's Folder

                                If dtAttachments.Columns.Contains("FilePhysicalPath") Then
                                    strLocation = dtAttachments.Rows(k)("FilePhysicalPath").ToString
                                End If

                                If File.Exists(strLocation) = True Then
                                    msg.Attachments.Add(strLocation, CStr(dtAttachments.Rows(k).Item("Filename")))
                                ElseIf File.Exists(strLocation1) Then
                                    msg.Attachments.Add(strLocation1, CStr(dtAttachments.Rows(k).Item("Filename")))
                                ElseIf File.Exists(dtAttachments.Rows(k).Item("FileLocation").ToString) = True Then
                                    msg.Attachments.Add(CCommon.ToString(dtAttachments.Rows(k).Item("FileLocation")), CStr(dtAttachments.Rows(k).Item("Filename")))
                                End If

                            Next
                        End If

                    Else
                        msg.Merge.AddAttachmentPattern("##AttchmentFilePath##")
                    End If
                    SessionObject("Attachements") = Nothing
                End If

                If Not ListAttachment Is Nothing AndAlso ListAttachment.Count > 0 Then
                    For Each filePath As Tuple(Of String, String) In ListAttachment
                        msg.Attachments.Add(filePath.Item2, filePath.Item1)
                    Next
                End If

                objSMTP.Message = msg
                If CCommon.ToString(ConfigurationManager.AppSettings("IsDebugMode")).ToLower() = "true" Then
                    Dim pickupFolderPath As String = ConfigurationManager.AppSettings("MailQueuePath")
                    objSMTP.AddJob(Nothing, strFrom, Nothing, dtMailMerge)
                    objSMTP.SubmitJobsToPickupFolder(pickupFolderPath, True)
                Else
                    'Author:Sachin Sadhu||Date:5thDec2013||Case No:512
                    'Code Starts :Sachin
                    'Set recipient pattern.
                    If strPageUrl = "/BACRMUI/contact/frmComposeWindow.aspx" Then 'ComposeWindow of Email
                        Dim dtnew As DataTable
                        Dim strToMails As String() = strTo.Split(CChar(","))
                        If strToMails.Length >= 2 Then 'More than One Recipients
                            'dtnew = dtMailMerge.Clone
                            'Dim dr1 As DataRow = dtMailMerge.Rows(0)
                            ''Dim intCount As Integer = 0
                            ''For intCount = 0 To dtMailMerge.Rows.Count - 1
                            'dtnew.ImportRow(dr1)
                            '' Next

                            'Dim strContactFirstName As String = Nothing
                            'Dim strContactLastName As String = Nothing
                            'Dim strContactPhone As String = Nothing
                            'Dim strContactPhoneExt As String = Nothing
                            'Dim strContactCell As String = Nothing
                            'Dim strContactHomePhone As String = Nothing
                            'Dim strContactPosition As String = Nothing
                            'Dim strContactManager As String = Nothing
                            'Dim strContactStatus As String = Nothing
                            'Dim strContactTitle As String = Nothing
                            'Dim strContactPrimaryAddress As String = Nothing
                            'Dim strOrgNoOfEmployee As String = Nothing

                            'If dtMailMerge.Rows.Count > 1 Then
                            '    Dim intCount As Integer = 0
                            '    For intCount = 0 To dtMailMerge.Rows.Count - 1
                            '        strContactFirstName = strContactFirstName & "," & dtMailMerge.Rows(intCount).Item("ContactFirstName").ToString()
                            '        strContactLastName = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactLastName").ToString()
                            '        'strContactPhone = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactPhone").ToString()
                            '        'strContactPhoneExt = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactPhoneExt").ToString()
                            '        'strContactCell = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactCell").ToString()
                            '        'strContactHomePhone = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactHomePhone").ToString()
                            '        'strContactPosition = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactPosition").ToString()
                            '        'strContactManager = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactManager").ToString()
                            '        'strContactStatus = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactStatus").ToString()
                            '        'strContactTitle = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactTitle").ToString()
                            '        'strContactPrimaryAddress = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactPrimaryAddress").ToString()
                            '        'strOrgNoOfEmployee = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("OrgNoOfEmployee").ToString()
                            '    Next
                            '    'Clear Rows of MailMerge table to Send only one mail to User If there's more than two recipeints
                            '    dtMailMerge.Rows.Clear()
                            'End If
                            'Dim dr As DataRow
                            'dr = dtMailMerge.NewRow
                            'dr("ContactEmail") = strTo
                            'dr("ContactFirstName") = strContactFirstName
                            'dr("ContactLastName") = strContactLastName
                            ''dr("ContactPhone") = strContactPhone
                            ''dr("ContactPhoneExt") = strContactPhoneExt
                            ''dr("ContactCell") = strContactCell
                            ''dr("ContactHomePhone") = strContactHomePhone
                            ''dr("ContactPosition") = strContactPosition
                            ''dr("ContactManager") = strContactManager
                            ''dr("ContactStatus") = strContactStatus
                            ''dr("ContactTitle") = strContactTitle
                            ''dr("ContactPrimaryAddress") = strContactPrimaryAddress
                            ''dr("OrgNoOfEmployee") = strOrgNoOfEmployee


                            'dtMailMerge.Rows.Add(dr)
                            If dtMailMerge.Rows.Count > 1 Then
                                msg.To.AsString = "##ContactFirstName## <##ContactEmail##>"
                            Else
                                msg.To.AsString = strTo.Trim().TrimEnd(Char.Parse(","))
                            End If

                            mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                        Else 'For Only One Recipeint
                            mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                        End If
                    Else 'Case Of Marketing or Broadcasting Mail
                        mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                    End If
                    'End of Sachin Code

                    ' mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                End If

                'Delete Attachment
                If IsAttachmentDelete Then
                    Dim count As Integer
                    If Not dtAttachments Is Nothing Then
                        For count = 0 To dtAttachments.Rows.Count - 1
                            strSplitedNames = Split(dtAttachments.Rows(count).Item("FileLocation").ToString, "/")
                            dtAttachments.Rows(count).Item("FileLocation") = Replace(dtAttachments.Rows(count).Item("FileLocation").ToString, "..", ConfigurationManager.AppSettings("BACRMLocation"))
                            'Checking whether the Files is present

                            strLocation = CCommon.GetDocumentPhysicalPath() & strSplitedNames(strSplitedNames.Length - 1).ToString
                            strLocation1 = CCommon.GetDocumentPhysicalPath(DomainID) & strSplitedNames(strSplitedNames.Length - 1).ToString 'Check into domain's Folder

                            If dtAttachments.Columns.Contains("FilePhysicalPath") Then
                                strLocation = dtAttachments.Rows(count)("FilePhysicalPath").ToString
                            End If

                            If File.Exists(strLocation) = True Then
                                File.Delete(strLocation)
                            ElseIf File.Exists(strLocation1) Then
                                File.Delete(strLocation1)
                            ElseIf File.Exists(dtAttachments.Rows(count).Item("FileLocation").ToString) = True Then
                                File.Delete(CCommon.ToString(dtAttachments.Rows(count).Item("FileLocation")))
                            End If
                        Next
                    End If
                End If
                'Delete Attachment

                Return mailStatus

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Sachin Try
        Public Function SendEmailWS(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String, ByRef dtMailMerge As DataTable, ByVal dtAttachment As DataTable, Optional ByVal FromDisplayName As String = "", Optional ByVal strBCC As String = "", Optional ByVal DomainID As Long = 0, Optional ByVal IsAttachmentDelete As Boolean = False, Optional ByVal strPageUrl As String = "", Optional ByRef ErrorMessage As String = "", Optional UserCntID As Long = 0, Optional ByVal tintEmailFrom As Short = 0) As Boolean
            Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
            Try
                Dim isOauthTokenExists As Boolean = False
                Dim objSMTP As Smtp = New Smtp

                'First check if Gmail or Office365 oauth2 token exists
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = DomainID
                objUserAccess.UserCntID = 0
                Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                        If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                            isOauthTokenExists = PrepareSmtpForGmail(DomainID, 0, objSMTP, dt)
                        ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                            isOauthTokenExists = PrepareSmtpForOffice365(DomainID, 0, objSMTP, strFrom, dt)
                        End If
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim objServer As SmtpServer = New SmtpServer
                    GetSMTPDetails(objServer, DomainID, UserCntID)

                    'Add SMTP server
                    objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                    objSMTP.SmtpServers.Add(objServer)

                    If SMTPServerIntegration = False Then
                        Return False
                        Exit Function
                    End If
                End If

                'Logging Events
                If File.Exists("C:\log.txt") Then
                    objSMTP.Log.Filename = "C:\log.txt"
                    objSMTP.Log.Enabled = True
                Else
                    objSMTP.Log.Enabled = False
                End If
                objSMTP.Log.DisableOnException = False

                ' Subscribe to events to track send bulk mail progress.
                AddHandler objSMTP.MessageNotSent, AddressOf OnMessageNotSent

                'Add MailMessage object
                Dim msg As New MailMessage
                ' Set sender (the same value for all messages).
                msg.From.AsString = strFrom
                If FromDisplayName <> "" Then
                    msg.From.DisplayName = FromDisplayName
                Else
                    msg.From.DisplayName = _EMailFromDisplayName
                End If

                'Author:Sachin Sadhu||Date:5thDec2013||Case No:512
                'Code Starts :Sachin
                ' Set recipient pattern.
                Dim strToMails As String() = strTo.Split(CChar(","))

                For Each strEMail As String In strToMails
                    If Not dtMailMerge Is Nothing AndAlso dtMailMerge.Select("ContactEmail='" & strEMail & "'").Length > 0 Then
                        Dim arrDataRow() As DataRow = dtMailMerge.Select("ContactEmail='" & strEMail & "'")
                        msg.To.Add(Convert.ToString(arrDataRow(0)("ContactEmail")), Convert.ToString(arrDataRow(0)("ContactFirstName")))
                    Else
                        msg.To.Add(strEMail, "")
                    End If
                Next
                'End of Sachin Code


                If strCC <> "" Then
                    msg.Cc.AsString = strCC.Trim().TrimEnd(Char.Parse(","))
                End If
                If strBCC <> "" Then
                    msg.Bcc.AsString = strBCC.Trim().TrimEnd(Char.Parse(","))
                End If

                ' Subject is the same for all messages.
                msg.Subject = strSubject

                ' Set body pattern.
                msg.BodyHtmlText = strBody

                Dim dtAttachments As DataTable
                Dim strSplitedNames() As String
                Dim strLocation As String
                Dim strLocation1 As String
                Dim mailStatus As Boolean
                'Add Attachments


                ' Dim dtAttachments As DataTable
                Dim k As Integer
                dtAttachments = dtAttachment

                If Not dtAttachments Is Nothing Then
                    For k = 0 To dtAttachments.Rows.Count - 1
                        strSplitedNames = Split(dtAttachments.Rows(k).Item("FileLocation").ToString, "/")
                        dtAttachments.Rows(k).Item("FileLocation") = Replace(dtAttachments.Rows(k).Item("FileLocation").ToString, "..", ConfigurationManager.AppSettings("BACRMLocation"))
                        'Checking whether the Files is present

                        strLocation = CCommon.GetDocumentPhysicalPath() & strSplitedNames(strSplitedNames.Length - 1).ToString
                        strLocation1 = CCommon.GetDocumentPhysicalPath(DomainID) & strSplitedNames(strSplitedNames.Length - 1).ToString 'Check into domain's Folder

                        If dtAttachments.Columns.Contains("FilePhysicalPath") Then
                            strLocation = dtAttachments.Rows(k)("FilePhysicalPath").ToString
                        End If

                        If File.Exists(strLocation) = True Then
                            msg.Attachments.Add(strLocation, CStr(dtAttachments.Rows(k).Item("Filename")))
                        ElseIf File.Exists(strLocation1) Then
                            msg.Attachments.Add(strLocation1, CStr(dtAttachments.Rows(k).Item("Filename")))
                        ElseIf File.Exists(dtAttachments.Rows(k).Item("FileLocation").ToString) = True Then
                            msg.Attachments.Add(CCommon.ToString(dtAttachments.Rows(k).Item("FileLocation")), CStr(dtAttachments.Rows(k).Item("Filename")))
                        End If

                    Next
                End If

                If Not ListAttachment Is Nothing AndAlso ListAttachment.Count > 0 Then
                    For Each filePath As Tuple(Of String, String) In ListAttachment
                        msg.Attachments.Add(filePath.Item2, filePath.Item1)
                    Next
                End If

                objSMTP.Message = msg
                Dim result As TestSendResult = TestSendResult.OK

                If CCommon.ToString(ConfigurationManager.AppSettings("IsDebugMode")).ToLower() = "true" Then
                    Dim pickupFolderPath As String = ConfigurationManager.AppSettings("MailQueuePath")
                    objSMTP.AddJob(Nothing, strFrom, Nothing, dtMailMerge)
                    objSMTP.SubmitJobsToPickupFolder(pickupFolderPath, True)
                Else
                    'Author:Sachin Sadhu||Date:5thDec2013||Case No:512
                    'Code Starts :Sachin
                    'Set recipient pattern.
                    If strPageUrl = "/BACRMUI/contact/frmComposeWindow.aspx" Then 'ComposeWindow of Email
                        If strToMails.Length >= 2 Then 'More than One Recipients
                            Try
                                If Not dtMailMerge Is Nothing AndAlso dtMailMerge.Rows.Count > 0 Then
                                    dtMailMerge = dtMailMerge.AsEnumerable().Take(1).CopyToDataTable()
                                    mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                                Else
                                    mailStatus = objSMTP.Send()
                                End If
                            Catch e As MailBeeInvalidArgumentException
                                If e.ErrorCode = ErrorCodes.NoSender Then
                                    result = TestSendResult.NoSender
                                Else
                                    If e.ErrorCode = ErrorCodes.NoRecipients Then
                                        result = TestSendResult.NoRecipients
                                    Else
                                        If e.ErrorCode = ErrorCodes.EmptyHostNameForDnsQuery Then
                                            result = TestSendResult.NoDomainInRecipientEmail
                                        Else
                                            result = TestSendResult.UnknownError
                                        End If
                                    End If
                                End If
                            Catch e As MailBeeSmtpRefusedSenderException
                                result = TestSendResult.BadSender
                            Catch e As MailBeeSmtpRefusedRecipientException
                                result = TestSendResult.BadRecipient
                            Catch e As MailBeeSmtpNoAcceptedRecipientsException
                                result = TestSendResult.NoAcceptedRecipients
                            Catch e As MailBeeLoginNoCredentialsException
                                result = TestSendResult.NoCredentials
                            Catch e As MailBeeLoginNoSupportedMethodsException
                                result = TestSendResult.NoSupportedAuth
                            Catch e As MailBeeSmtpLoginBadMethodException
                                result = TestSendResult.BadAuthMethod
                            Catch e As MailBeeSmtpLoginBadCredentialsException
                                result = TestSendResult.BadCredentials
                            Catch e As MailBeePop3LoginBadCredentialsException
                                result = TestSendResult.BadCredentials
                            Catch e As MailBeeSmtpNegativeResponseException
                                result = TestSendResult.NegativeSmtpResponse
                            Catch e As MailBeePop3NegativeResponseException
                                result = TestSendResult.NegativePop3Response
                            Catch e As MailBeeDnsNameErrorException
                                result = TestSendResult.NoMXRecord
                            Catch e As MailBeeDnsProtocolException
                                result = TestSendResult.DnsProtocolError
                            Catch e As MailBeeConnectionException
                                Select Case e.Protocol
                                    Case TopLevelProtocolType.Smtp
                                        result = TestSendResult.SmtpConnectionError
                                    Case TopLevelProtocolType.Dns
                                        result = TestSendResult.DnsConnectionError
                                    Case TopLevelProtocolType.Pop3
                                        result = TestSendResult.Pop3ConnectionError
                                    Case Else
                                        result = TestSendResult.UnknownError
                                End Select
                            Catch e As MailBeeGetRemoteHostNameException
                                Select Case e.HostProtocol
                                    Case TopLevelProtocolType.Smtp
                                        result = TestSendResult.SmtpResolveHostError
                                    Case TopLevelProtocolType.Pop3
                                        result = TestSendResult.Pop3ResolveHostError
                                    Case Else
                                        result = TestSendResult.UnknownError
                                End Select
                            Catch e As MailBeeException
                                result = TestSendResult.UnknownError
                            End Try

                            If result <> TestSendResult.OK Then
                                Try
                                    ErrorMessage = Convert.ToString(result)
                                Catch ex As Exception
                                    'DO NOT TRHOW ERROR
                                End Try
                            End If
                        Else 'For Only One Recipeint
                            Try
                                If Not dtMailMerge Is Nothing AndAlso dtMailMerge.Rows.Count > 0 Then
                                    mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                                Else
                                    mailStatus = objSMTP.Send()
                                End If
                            Catch e As MailBeeInvalidArgumentException
                                If e.ErrorCode = ErrorCodes.NoSender Then
                                    result = TestSendResult.NoSender
                                Else
                                    If e.ErrorCode = ErrorCodes.NoRecipients Then
                                        result = TestSendResult.NoRecipients
                                    Else
                                        If e.ErrorCode = ErrorCodes.EmptyHostNameForDnsQuery Then
                                            result = TestSendResult.NoDomainInRecipientEmail
                                        Else
                                            result = TestSendResult.UnknownError
                                        End If
                                    End If
                                End If
                            Catch e As MailBeeSmtpRefusedSenderException
                                result = TestSendResult.BadSender
                            Catch e As MailBeeSmtpRefusedRecipientException
                                result = TestSendResult.BadRecipient
                            Catch e As MailBeeSmtpNoAcceptedRecipientsException
                                result = TestSendResult.NoAcceptedRecipients
                            Catch e As MailBeeLoginNoCredentialsException
                                result = TestSendResult.NoCredentials
                            Catch e As MailBeeLoginNoSupportedMethodsException
                                result = TestSendResult.NoSupportedAuth
                            Catch e As MailBeeSmtpLoginBadMethodException
                                result = TestSendResult.BadAuthMethod
                            Catch e As MailBeeSmtpLoginBadCredentialsException
                                result = TestSendResult.BadCredentials
                            Catch e As MailBeePop3LoginBadCredentialsException
                                result = TestSendResult.BadCredentials
                            Catch e As MailBeeSmtpNegativeResponseException
                                result = TestSendResult.NegativeSmtpResponse
                            Catch e As MailBeePop3NegativeResponseException
                                result = TestSendResult.NegativePop3Response
                            Catch e As MailBeeDnsNameErrorException
                                result = TestSendResult.NoMXRecord
                            Catch e As MailBeeDnsProtocolException
                                result = TestSendResult.DnsProtocolError
                            Catch e As MailBeeConnectionException
                                Select Case e.Protocol
                                    Case TopLevelProtocolType.Smtp
                                        result = TestSendResult.SmtpConnectionError
                                    Case TopLevelProtocolType.Dns
                                        result = TestSendResult.DnsConnectionError
                                    Case TopLevelProtocolType.Pop3
                                        result = TestSendResult.Pop3ConnectionError
                                    Case Else
                                        result = TestSendResult.UnknownError
                                End Select
                            Catch e As MailBeeGetRemoteHostNameException
                                Select Case e.HostProtocol
                                    Case TopLevelProtocolType.Smtp
                                        result = TestSendResult.SmtpResolveHostError
                                    Case TopLevelProtocolType.Pop3
                                        result = TestSendResult.Pop3ResolveHostError
                                    Case Else
                                        result = TestSendResult.UnknownError
                                End Select
                            Catch e As MailBeeException
                                result = TestSendResult.UnknownError
                            End Try

                            If result <> TestSendResult.OK Then
                                Try
                                    ErrorMessage = Convert.ToString(result)
                                Catch ex As Exception
                                    'DO NOT TRHOW ERROR
                                End Try
                            End If
                        End If
                    Else 'Case Of Marketing or Broadcasting Mail
                        Try
                            If Not dtMailMerge Is Nothing AndAlso dtMailMerge.Rows.Count > 0 Then
                                dtMailMerge = dtMailMerge.AsEnumerable().Take(1).CopyToDataTable()
                                mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                            Else
                                mailStatus = objSMTP.Send()
                            End If
                        Catch e As MailBeeInvalidArgumentException
                            If e.ErrorCode = ErrorCodes.NoSender Then
                                result = TestSendResult.NoSender
                            Else
                                If e.ErrorCode = ErrorCodes.NoRecipients Then
                                    result = TestSendResult.NoRecipients
                                Else
                                    If e.ErrorCode = ErrorCodes.EmptyHostNameForDnsQuery Then
                                        result = TestSendResult.NoDomainInRecipientEmail
                                    Else
                                        result = TestSendResult.UnknownError
                                    End If
                                End If
                            End If
                        Catch e As MailBeeSmtpRefusedSenderException
                            result = TestSendResult.BadSender
                        Catch e As MailBeeSmtpRefusedRecipientException
                            result = TestSendResult.BadRecipient
                        Catch e As MailBeeSmtpNoAcceptedRecipientsException
                            result = TestSendResult.NoAcceptedRecipients
                        Catch e As MailBeeLoginNoCredentialsException
                            result = TestSendResult.NoCredentials
                        Catch e As MailBeeLoginNoSupportedMethodsException
                            result = TestSendResult.NoSupportedAuth
                        Catch e As MailBeeSmtpLoginBadMethodException
                            result = TestSendResult.BadAuthMethod
                        Catch e As MailBeeSmtpLoginBadCredentialsException
                            result = TestSendResult.BadCredentials
                        Catch e As MailBeePop3LoginBadCredentialsException
                            result = TestSendResult.BadCredentials
                        Catch e As MailBeeSmtpNegativeResponseException
                            result = TestSendResult.NegativeSmtpResponse
                        Catch e As MailBeePop3NegativeResponseException
                            result = TestSendResult.NegativePop3Response
                        Catch e As MailBeeDnsNameErrorException
                            result = TestSendResult.NoMXRecord
                        Catch e As MailBeeDnsProtocolException
                            result = TestSendResult.DnsProtocolError
                        Catch e As MailBeeConnectionException
                            Select Case e.Protocol
                                Case TopLevelProtocolType.Smtp
                                    result = TestSendResult.SmtpConnectionError
                                Case TopLevelProtocolType.Dns
                                    result = TestSendResult.DnsConnectionError
                                Case TopLevelProtocolType.Pop3
                                    result = TestSendResult.Pop3ConnectionError
                                Case Else
                                    result = TestSendResult.UnknownError
                            End Select
                        Catch e As MailBeeGetRemoteHostNameException
                            Select Case e.HostProtocol
                                Case TopLevelProtocolType.Smtp
                                    result = TestSendResult.SmtpResolveHostError
                                Case TopLevelProtocolType.Pop3
                                    result = TestSendResult.Pop3ResolveHostError
                                Case Else
                                    result = TestSendResult.UnknownError
                            End Select
                        Catch e As MailBeeException
                            result = TestSendResult.UnknownError
                        End Try

                        If result <> TestSendResult.OK Then
                            Try
                                ErrorMessage = Convert.ToString(result)
                            Catch ex As Exception
                                'DO NOT TRHOW ERROR
                            End Try
                        End If
                    End If
                End If

                'Delete Attachment
                If IsAttachmentDelete Then
                    Dim count As Integer
                    If Not dtAttachments Is Nothing Then
                        For count = 0 To dtAttachments.Rows.Count - 1
                            strSplitedNames = Split(dtAttachments.Rows(count).Item("FileLocation").ToString, "/")
                            dtAttachments.Rows(count).Item("FileLocation") = Replace(dtAttachments.Rows(count).Item("FileLocation").ToString, "..", ConfigurationManager.AppSettings("BACRMLocation"))
                            'Checking whether the Files is present

                            strLocation = CCommon.GetDocumentPhysicalPath() & strSplitedNames(strSplitedNames.Length - 1).ToString
                            strLocation1 = CCommon.GetDocumentPhysicalPath(DomainID) & strSplitedNames(strSplitedNames.Length - 1).ToString 'Check into domain's Folder

                            If dtAttachments.Columns.Contains("FilePhysicalPath") Then
                                strLocation = dtAttachments.Rows(count)("FilePhysicalPath").ToString
                            End If

                            If File.Exists(strLocation) = True Then
                                File.Delete(strLocation)
                            ElseIf File.Exists(strLocation1) Then
                                File.Delete(strLocation1)
                            ElseIf File.Exists(dtAttachments.Rows(count).Item("FileLocation").ToString) = True Then
                                File.Delete(CCommon.ToString(dtAttachments.Rows(count).Item("FileLocation")))
                            End If
                        Next
                    End If
                End If
                'Delete Attachment

                Return mailStatus

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ' Reports failed attempt of sending mail merge e-mail.

        Public Function SendEmailWFA(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String,
                                  ByRef dtMailMerge As DataTable, Optional ByVal FromDisplayName As String = "", Optional ByVal strBCC As String = "",
                                  Optional ByVal DomainID As Long = 0, Optional ByVal IsAttachmentDelete As Boolean = False, Optional ByVal strPageUrl As String = "",
                                  Optional ByVal UserId As Long = 0, Optional ByRef ErrorMessage As String = "", Optional ByVal tintEmailFrom As Short = 0) As Boolean
            Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
            Try
                Dim isOauthTokenExists As Boolean = False
                Dim objSMTP As Smtp = New Smtp

                'First check if Gmail or Office365 oauth2 token exists
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = DomainID
                objUserAccess.UserCntID = UserId
                Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                        If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                            isOauthTokenExists = PrepareSmtpForGmail(DomainID, UserId, objSMTP, dt)
                        ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                            isOauthTokenExists = PrepareSmtpForOffice365(DomainID, UserId, objSMTP, strFrom, dt)
                        End If
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim objServer As SmtpServer = New SmtpServer
                    GetSMTPDetails(objServer, DomainID, UserId)

                    'Add SMTP server
                    objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                    objSMTP.SmtpServers.Add(objServer)

                    If SMTPServerIntegration = False Then
                        Return False
                        Exit Function
                    End If
                End If

                'Logging Events
                If File.Exists("C:\log.txt") Then
                    objSMTP.Log.Filename = "C:\log.txt"
                    objSMTP.Log.Enabled = True
                Else
                    objSMTP.Log.Enabled = False
                End If
                objSMTP.Log.DisableOnException = False

                ' Subscribe to events to track send bulk mail progress.
                AddHandler objSMTP.MessageNotSent, AddressOf OnMessageNotSent

                'Add MailMessage object
                Dim msg As New MailMessage
                ' Set sender (the same value for all messages).
                msg.From.AsString = strFrom
                If FromDisplayName <> "" Then
                    msg.From.DisplayName = FromDisplayName
                Else
                    msg.From.DisplayName = _EMailFromDisplayName
                End If

                'Author:Sachin Sadhu||Date:5thDec2013||Case No:512
                'Code Starts :Sachin
                ' Set recipient pattern.
                If strPageUrl = "/BACRMUI/contact/frmComposeWindow.aspx" Then 'ComposeWindow of Email
                    Dim strToMails As String() = strTo.Split(CChar(","))
                    If strToMails.Length >= 2 Then 'More than One Recipients
                        If dtMailMerge.Rows.Count > 1 Then 'Definetly more than one
                            Dim intCount As Integer = 0
                            For intCount = 0 To dtMailMerge.Rows.Count - 1
                                msg.To.Add(dtMailMerge.Rows(intCount).Item("ContactEmail").ToString(), dtMailMerge.Rows(intCount).Item("ContactFirstName").ToString())
                            Next
                        Else
                            msg.To.AsString = strTo.Trim().TrimEnd(Char.Parse(",")) 'will be used for email alerts.. where generally it needs to be sent to only one user
                        End If
                    Else 'For Single Recipient
                        If dtMailMerge.Rows.Count > 1 Then
                            '  msg.To.AsString = "##ContactFirstName## <##ContactEmail##>" 'Will be used from compose new message
                            msg.To.AsString = strTo.Trim().TrimEnd(Char.Parse(","))
                        Else
                            msg.To.AsString = strTo.Trim().TrimEnd(Char.Parse(",")) 'will be used for email alerts.. where generally it needs to be sent to only one user
                        End If
                    End If
                Else 'Case Of Marketing or Broadcasting Mail
                    If dtMailMerge.Rows.Count > 1 Then
                        msg.To.AsString = "##ContactFirstName## <##ContactEmail##>" 'Will be used from compose new message
                    Else
                        msg.To.AsString = strTo.Trim().TrimEnd(Char.Parse(",")) 'will be used for email alerts.. where generally it needs to be sent to only one user
                    End If
                End If
                'End of Sachin Code


                If strCC <> "" Then
                    msg.Cc.AsString = strCC.Trim().TrimEnd(Char.Parse(","))
                End If
                If strBCC <> "" Then
                    msg.Bcc.AsString = strBCC.Trim().TrimEnd(Char.Parse(","))
                End If

                ' Subject is the same for all messages.
                msg.Subject = strSubject

                ' Set body pattern.
                msg.BodyHtmlText = strBody

                Dim dtAttachments As DataTable
                Dim strSplitedNames() As String
                Dim strLocation As String
                Dim strLocation1 As String
                Dim mailStatus As Boolean
                'Add Attachments
                If SessionObject IsNot Nothing AndAlso SessionObject.ContainsKey("Attachements") Then
                    If Not SessionObject("Attachements") Is Nothing Then
                        ' Dim dtAttachments As DataTable
                        Dim k As Integer
                        dtAttachments = CType(SessionObject("Attachements"), DataTable)

                        If Not dtAttachments Is Nothing Then
                            For k = 0 To dtAttachments.Rows.Count - 1
                                strSplitedNames = Split(dtAttachments.Rows(k).Item("FileLocation").ToString, "/")
                                dtAttachments.Rows(k).Item("FileLocation") = Replace(dtAttachments.Rows(k).Item("FileLocation").ToString, "..", ConfigurationManager.AppSettings("BACRMLocation"))
                                'Checking whether the Files is present

                                strLocation = CCommon.GetDocumentPhysicalPath() & strSplitedNames(strSplitedNames.Length - 1).ToString
                                strLocation1 = CCommon.GetDocumentPhysicalPath(DomainID) & strSplitedNames(strSplitedNames.Length - 1).ToString 'Check into domain's Folder

                                If dtAttachments.Columns.Contains("FilePhysicalPath") Then
                                    strLocation = dtAttachments.Rows(k)("FilePhysicalPath").ToString
                                End If

                                If File.Exists(strLocation) = True Then
                                    msg.Attachments.Add(strLocation, CStr(dtAttachments.Rows(k).Item("Filename")))
                                ElseIf File.Exists(strLocation1) Then
                                    msg.Attachments.Add(strLocation1, CStr(dtAttachments.Rows(k).Item("Filename")))
                                ElseIf File.Exists(dtAttachments.Rows(k).Item("FileLocation").ToString) = True Then
                                    msg.Attachments.Add(CCommon.ToString(dtAttachments.Rows(k).Item("FileLocation")), CStr(dtAttachments.Rows(k).Item("Filename")))
                                End If

                            Next
                        End If

                    Else
                        msg.Merge.AddAttachmentPattern("##AttchmentFilePath##")
                    End If
                    SessionObject("Attachements") = Nothing
                End If

                objSMTP.Message = msg
                Dim result As TestSendResult = TestSendResult.OK

                If CCommon.ToString(ConfigurationManager.AppSettings("IsDebugMode")).ToLower() = "true" Then
                    ' ss comment If ConfigurationManager.AppSettings("IsDebugMode") = "false" Then
                    Dim pickupFolderPath As String = ConfigurationManager.AppSettings("MailQueuePath")
                    objSMTP.AddJob(Nothing, strFrom, Nothing, dtMailMerge)
                    objSMTP.SubmitJobsToPickupFolder(pickupFolderPath, True)
                Else
                    'Author:Sachin Sadhu||Date:5thDec2013||Case No:512
                    'Code Starts :Sachin
                    'Set recipient pattern.
                    If strPageUrl = "/BACRMUI/contact/frmComposeWindow.aspx" Then 'ComposeWindow of Email
                        Dim strToMails As String() = strTo.Split(CChar(","))
                        If strToMails.Length >= 2 Then 'More than One Recipients

                            'Dim strContactFirstName As String = Nothing
                            'Dim strContactLastName As String = Nothing
                            'Dim strContactPhone As String = Nothing
                            'Dim strContactPhoneExt As String = Nothing
                            'Dim strContactCell As String = Nothing
                            'Dim strContactHomePhone As String = Nothing
                            'Dim strContactPosition As String = Nothing
                            'Dim strContactManager As String = Nothing
                            'Dim strContactStatus As String = Nothing
                            'Dim strContactTitle As String = Nothing
                            'Dim strContactPrimaryAddress As String = Nothing
                            'Dim strOrgNoOfEmployee As String = Nothing

                            'If dtMailMerge.Rows.Count > 1 Then
                            '    Dim intCount As Integer = 0
                            '    For intCount = 0 To dtMailMerge.Rows.Count - 1
                            '        strContactFirstName = strContactFirstName & "," & dtMailMerge.Rows(intCount).Item("ContactFirstName").ToString()
                            '        strContactLastName = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactLastName").ToString()
                            '        strContactPhone = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactPhone").ToString()
                            '        strContactPhoneExt = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactPhoneExt").ToString()
                            '        strContactCell = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactCell").ToString()
                            '        strContactHomePhone = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactHomePhone").ToString()
                            '        strContactPosition = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactPosition").ToString()
                            '        strContactManager = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactManager").ToString()
                            '        strContactStatus = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactStatus").ToString()
                            '        strContactTitle = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactTitle").ToString()
                            '        strContactPrimaryAddress = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("ContactPrimaryAddress").ToString()
                            '        strOrgNoOfEmployee = strContactLastName & "," & dtMailMerge.Rows(intCount).Item("OrgNoOfEmployee").ToString()
                            '    Next
                            '    'Clear Rows of MailMerge table to Send only one mail to User If there's more than two recipeints
                            '    dtMailMerge.Rows.Clear()
                            'End If
                            'Dim dr As DataRow
                            'dr = dtMailMerge.NewRow
                            'dr("ContactEmail") = strTo
                            'dr("ContactFirstName") = strContactFirstName
                            'dr("ContactLastName") = strContactLastName
                            'dr("ContactPhone") = strContactPhone
                            'dr("ContactPhoneExt") = strContactPhoneExt
                            'dr("ContactCell") = strContactCell
                            'dr("ContactHomePhone") = strContactHomePhone
                            'dr("ContactPosition") = strContactPosition
                            'dr("ContactManager") = strContactManager
                            'dr("ContactStatus") = strContactStatus
                            'dr("ContactTitle") = strContactTitle
                            'dr("ContactPrimaryAddress") = strContactPrimaryAddress
                            'dr("OrgNoOfEmployee") = strOrgNoOfEmployee


                            'dtMailMerge.Rows.Add(dr)

                            Try
                                If Not dtMailMerge Is Nothing AndAlso dtMailMerge.Rows.Count > 1 Then
                                    msg.To.AsString = "##ContactFirstName## <##ContactEmail##>"
                                Else
                                    msg.To.AsString = strTo.Trim().TrimEnd(Char.Parse(","))
                                End If

                                If Not dtMailMerge Is Nothing AndAlso dtMailMerge.Rows.Count > 0 Then
                                    mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                                Else
                                    mailStatus = objSMTP.Send()
                                End If
                            Catch e As MailBeeInvalidArgumentException
                                If e.ErrorCode = ErrorCodes.NoSender Then
                                    result = TestSendResult.NoSender
                                Else
                                    If e.ErrorCode = ErrorCodes.NoRecipients Then
                                        result = TestSendResult.NoRecipients
                                    Else
                                        If e.ErrorCode = ErrorCodes.EmptyHostNameForDnsQuery Then
                                            result = TestSendResult.NoDomainInRecipientEmail
                                        Else
                                            result = TestSendResult.UnknownError
                                        End If
                                    End If
                                End If
                            Catch e As MailBeeSmtpRefusedSenderException
                                result = TestSendResult.BadSender
                            Catch e As MailBeeSmtpRefusedRecipientException
                                result = TestSendResult.BadRecipient
                            Catch e As MailBeeSmtpNoAcceptedRecipientsException
                                result = TestSendResult.NoAcceptedRecipients
                            Catch e As MailBeeLoginNoCredentialsException
                                result = TestSendResult.NoCredentials
                            Catch e As MailBeeLoginNoSupportedMethodsException
                                result = TestSendResult.NoSupportedAuth
                            Catch e As MailBeeSmtpLoginBadMethodException
                                result = TestSendResult.BadAuthMethod
                            Catch e As MailBeeSmtpLoginBadCredentialsException
                                result = TestSendResult.BadCredentials
                            Catch e As MailBeePop3LoginBadCredentialsException
                                result = TestSendResult.BadCredentials
                            Catch e As MailBeeSmtpNegativeResponseException
                                result = TestSendResult.NegativeSmtpResponse
                            Catch e As MailBeePop3NegativeResponseException
                                result = TestSendResult.NegativePop3Response
                            Catch e As MailBeeDnsNameErrorException
                                result = TestSendResult.NoMXRecord
                            Catch e As MailBeeDnsProtocolException
                                result = TestSendResult.DnsProtocolError
                            Catch e As MailBeeConnectionException
                                Select Case e.Protocol
                                    Case TopLevelProtocolType.Smtp
                                        result = TestSendResult.SmtpConnectionError
                                    Case TopLevelProtocolType.Dns
                                        result = TestSendResult.DnsConnectionError
                                    Case TopLevelProtocolType.Pop3
                                        result = TestSendResult.Pop3ConnectionError
                                    Case Else
                                        result = TestSendResult.UnknownError
                                End Select
                            Catch e As MailBeeGetRemoteHostNameException
                                Select Case e.HostProtocol
                                    Case TopLevelProtocolType.Smtp
                                        result = TestSendResult.SmtpResolveHostError
                                    Case TopLevelProtocolType.Pop3
                                        result = TestSendResult.Pop3ResolveHostError
                                    Case Else
                                        result = TestSendResult.UnknownError
                                End Select
                            Catch e As MailBeeException
                                result = TestSendResult.UnknownError
                            End Try

                            If result <> TestSendResult.OK Then
                                Try
                                    ErrorMessage = Convert.ToString(result)
                                Catch ex As Exception
                                    'DO NOT TRHOW ERROR
                                End Try
                            End If
                        Else 'For Only One Recipeint
                            Try
                                If Not dtMailMerge Is Nothing AndAlso dtMailMerge.Rows.Count > 0 Then
                                    mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                                Else
                                    mailStatus = objSMTP.Send()
                                End If
                            Catch e As MailBeeInvalidArgumentException
                                If e.ErrorCode = ErrorCodes.NoSender Then
                                    result = TestSendResult.NoSender
                                Else
                                    If e.ErrorCode = ErrorCodes.NoRecipients Then
                                        result = TestSendResult.NoRecipients
                                    Else
                                        If e.ErrorCode = ErrorCodes.EmptyHostNameForDnsQuery Then
                                            result = TestSendResult.NoDomainInRecipientEmail
                                        Else
                                            result = TestSendResult.UnknownError
                                        End If
                                    End If
                                End If
                            Catch e As MailBeeSmtpRefusedSenderException
                                result = TestSendResult.BadSender
                            Catch e As MailBeeSmtpRefusedRecipientException
                                result = TestSendResult.BadRecipient
                            Catch e As MailBeeSmtpNoAcceptedRecipientsException
                                result = TestSendResult.NoAcceptedRecipients
                            Catch e As MailBeeLoginNoCredentialsException
                                result = TestSendResult.NoCredentials
                            Catch e As MailBeeLoginNoSupportedMethodsException
                                result = TestSendResult.NoSupportedAuth
                            Catch e As MailBeeSmtpLoginBadMethodException
                                result = TestSendResult.BadAuthMethod
                            Catch e As MailBeeSmtpLoginBadCredentialsException
                                result = TestSendResult.BadCredentials
                            Catch e As MailBeePop3LoginBadCredentialsException
                                result = TestSendResult.BadCredentials
                            Catch e As MailBeeSmtpNegativeResponseException
                                result = TestSendResult.NegativeSmtpResponse
                            Catch e As MailBeePop3NegativeResponseException
                                result = TestSendResult.NegativePop3Response
                            Catch e As MailBeeDnsNameErrorException
                                result = TestSendResult.NoMXRecord
                            Catch e As MailBeeDnsProtocolException
                                result = TestSendResult.DnsProtocolError
                            Catch e As MailBeeConnectionException
                                Select Case e.Protocol
                                    Case TopLevelProtocolType.Smtp
                                        result = TestSendResult.SmtpConnectionError
                                    Case TopLevelProtocolType.Dns
                                        result = TestSendResult.DnsConnectionError
                                    Case TopLevelProtocolType.Pop3
                                        result = TestSendResult.Pop3ConnectionError
                                    Case Else
                                        result = TestSendResult.UnknownError
                                End Select
                            Catch e As MailBeeGetRemoteHostNameException
                                Select Case e.HostProtocol
                                    Case TopLevelProtocolType.Smtp
                                        result = TestSendResult.SmtpResolveHostError
                                    Case TopLevelProtocolType.Pop3
                                        result = TestSendResult.Pop3ResolveHostError
                                    Case Else
                                        result = TestSendResult.UnknownError
                                End Select
                            Catch e As MailBeeException
                                result = TestSendResult.UnknownError
                            End Try

                            If result <> TestSendResult.OK Then
                                Try
                                    ErrorMessage = Convert.ToString(result)
                                Catch ex As Exception
                                    'DO NOT TRHOW ERROR
                                End Try
                            End If
                        End If
                    Else 'Case Of Marketing or Broadcasting Mail

                        Try
                            Dim strToMails As String() = strTo.Split(CChar(","))
                            If strToMails.Length >= 2 Then 'More than One Recipients
                                If Not dtMailMerge Is Nothing AndAlso dtMailMerge.Rows.Count > 1 Then
                                    msg.To.AsString = "##ContactFirstName## <##ContactEmail##>"
                                Else
                                    msg.To.AsString = strTo.Trim().TrimEnd(Char.Parse(","))
                                End If
                            End If

                            If Not dtMailMerge Is Nothing AndAlso dtMailMerge.Rows.Count > 0 Then
                                mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                            Else
                                mailStatus = objSMTP.Send()
                            End If

                        Catch e As MailBeeInvalidArgumentException
                            If e.ErrorCode = ErrorCodes.NoSender Then
                                result = TestSendResult.NoSender
                            Else
                                If e.ErrorCode = ErrorCodes.NoRecipients Then
                                    result = TestSendResult.NoRecipients
                                Else
                                    If e.ErrorCode = ErrorCodes.EmptyHostNameForDnsQuery Then
                                        result = TestSendResult.NoDomainInRecipientEmail
                                    Else
                                        result = TestSendResult.UnknownError
                                    End If
                                End If
                            End If
                        Catch e As MailBeeSmtpRefusedSenderException
                            result = TestSendResult.BadSender
                        Catch e As MailBeeSmtpRefusedRecipientException
                            result = TestSendResult.BadRecipient
                        Catch e As MailBeeSmtpNoAcceptedRecipientsException
                            result = TestSendResult.NoAcceptedRecipients
                        Catch e As MailBeeLoginNoCredentialsException
                            result = TestSendResult.NoCredentials
                        Catch e As MailBeeLoginNoSupportedMethodsException
                            result = TestSendResult.NoSupportedAuth
                        Catch e As MailBeeSmtpLoginBadMethodException
                            result = TestSendResult.BadAuthMethod
                        Catch e As MailBeeSmtpLoginBadCredentialsException
                            result = TestSendResult.BadCredentials
                        Catch e As MailBeePop3LoginBadCredentialsException
                            result = TestSendResult.BadCredentials
                        Catch e As MailBeeSmtpNegativeResponseException
                            result = TestSendResult.NegativeSmtpResponse
                        Catch e As MailBeePop3NegativeResponseException
                            result = TestSendResult.NegativePop3Response
                        Catch e As MailBeeDnsNameErrorException
                            result = TestSendResult.NoMXRecord
                        Catch e As MailBeeDnsProtocolException
                            result = TestSendResult.DnsProtocolError
                        Catch e As MailBeeConnectionException
                            Select Case e.Protocol
                                Case TopLevelProtocolType.Smtp
                                    result = TestSendResult.SmtpConnectionError
                                Case TopLevelProtocolType.Dns
                                    result = TestSendResult.DnsConnectionError
                                Case TopLevelProtocolType.Pop3
                                    result = TestSendResult.Pop3ConnectionError
                                Case Else
                                    result = TestSendResult.UnknownError
                            End Select
                        Catch e As MailBeeGetRemoteHostNameException
                            Select Case e.HostProtocol
                                Case TopLevelProtocolType.Smtp
                                    result = TestSendResult.SmtpResolveHostError
                                Case TopLevelProtocolType.Pop3
                                    result = TestSendResult.Pop3ResolveHostError
                                Case Else
                                    result = TestSendResult.UnknownError
                            End Select
                        Catch e As MailBeeException
                            result = TestSendResult.UnknownError
                        End Try

                        If result <> TestSendResult.OK Then
                            Try
                                ErrorMessage = Convert.ToString(result)
                            Catch ex As Exception
                                'DO NOT TRHOW ERROR
                            End Try
                        End If
                    End If
                    'End of Sachin Code

                    ' mailStatus = objSMTP.SendMailMerge(strFrom, Nothing, dtMailMerge)
                End If

                'Delete Attachment
                If IsAttachmentDelete Then
                    Dim count As Integer
                    If Not dtAttachments Is Nothing Then
                        For count = 0 To dtAttachments.Rows.Count - 1
                            strSplitedNames = Split(dtAttachments.Rows(count).Item("FileLocation").ToString, "/")
                            dtAttachments.Rows(count).Item("FileLocation") = Replace(dtAttachments.Rows(count).Item("FileLocation").ToString, "..", ConfigurationManager.AppSettings("BACRMLocation"))
                            'Checking whether the Files is present

                            strLocation = CCommon.GetDocumentPhysicalPath() & strSplitedNames(strSplitedNames.Length - 1).ToString
                            strLocation1 = CCommon.GetDocumentPhysicalPath(DomainID) & strSplitedNames(strSplitedNames.Length - 1).ToString 'Check into domain's Folder

                            If dtAttachments.Columns.Contains("FilePhysicalPath") Then
                                strLocation = dtAttachments.Rows(count)("FilePhysicalPath").ToString
                            End If

                            If File.Exists(strLocation) = True Then
                                File.Delete(strLocation)
                            ElseIf File.Exists(strLocation1) Then
                                File.Delete(strLocation1)
                            ElseIf File.Exists(dtAttachments.Rows(count).Item("FileLocation").ToString) = True Then
                                File.Delete(CCommon.ToString(dtAttachments.Rows(count).Item("FileLocation")))
                            End If
                        Next
                    End If
                End If
                'Delete Attachment

                Return mailStatus

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Shared Sub OnMessageNotSent(ByVal sender As Object, ByVal e As SmtpMessageNotSentEventArgs)
            Try
                ' Display e-mail address of the failed e-mail.
                Dim strMessage As New System.Text.StringBuilder

                strMessage.Append("Dear customer, <br/> ")
                strMessage.Append("<br />")
                strMessage.Append("Delivery of following email failed.<br />")
                strMessage.Append("<br />")
                strMessage.Append("Recipient: " & e.MailMessage.To.ToString() & "<br />")
                strMessage.Append("==============Original Email=================<br />")
                strMessage.Append("<b>Subject:</b>" & e.MailMessage.Subject & "<br />")
                strMessage.Append(e.MailMessage.BodyHtmlText & "<br />")
                strMessage.Append("=============================================<br />")
                strMessage.Append("<br />")
                strMessage.Append("Reason: <span style='color: #c00000;'>" & e.Reason.ToString() & "</span><br />")
                strMessage.Append("<br />")
                strMessage.Append("<br />")
                strMessage.Append("this is automated mail from bizautomation.<br />")
                strMessage.Append("please do not respond to this email.<br />")
                strMessage.Append("<br />")
                strMessage.Append("BizAutomation.com<br />")
                strMessage.Append("One system for your <strong><em><span style='color: #00b050;'>ENTIRE</span></em></strong>")
                strMessage.Append("business<br />")
                strMessage.Append("U.S.(888)-407-4781<br />")
                strMessage.Append("Outside U.S. (408)-786-5116<br />")
                strMessage.Append("<br />")
                strMessage.Append("<br />")
                Dim objEmail As New Email
                objEmail.SendSystemEmail("Delivery Status Notification (Failure)", strMessage.ToString(), "", "noreply@bizautomation.com", e.MailMessage.From.AsString, "BizAutomation", "")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Function SendSimpleEmail(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String, Optional ByVal FromDisplayName As String = "", Optional ByVal strBCC As String = "", Optional ByVal DomainID As Long = 0) As Boolean
            Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
            Try
                Dim isOauthTokenExists As Boolean = False
                Dim objSMTP As Smtp = New Smtp

                If DomainID > 0 Then
                    'First check if Gmail or Office365 oauth2 token exists
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = DomainID
                    objUserAccess.UserCntID = 0
                    Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        If (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                            If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                                isOauthTokenExists = PrepareSmtpForGmail(DomainID, 0, objSMTP, dt)
                            ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                                isOauthTokenExists = PrepareSmtpForOffice365(DomainID, 0, objSMTP, strFrom, dt)
                            End If
                        End If
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim objServer As SmtpServer = New SmtpServer
                    GetSMTPDetails(objServer, DomainID)

                    'Add SMTP server
                    objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                    objSMTP.SmtpServers.Add(objServer)

                    If SMTPServerIntegration = False Then
                        Return False
                        Exit Function
                    End If
                End If

                If File.Exists("C:\log.txt") Then
                    objSMTP.Log.Filename = "C:\log.txt"
                    objSMTP.Log.Enabled = True
                Else
                    objSMTP.Log.Enabled = False
                End If
                objSMTP.Log.DisableOnException = False

                objSMTP.From.AsString = strFrom

                If FromDisplayName <> "" Then
                    objSMTP.From.DisplayName = FromDisplayName
                End If
                objSMTP.To.AsString = strTo
                If strCC <> "" Then
                    objSMTP.Cc.AsString = strCC
                End If
                If strBCC <> "" Then
                    objSMTP.Bcc.AsString = strBCC
                End If
                objSMTP.Subject = strSubject
                objSMTP.BodyHtmlText = strBody

                Dim strLocation As String
                Dim dtTable As DataTable
                Dim strSplitedNames() As String
                'Added Code for attachment- chintan

                If SessionObject.ContainsKey("Attachements") Then
                    If Not SessionObject("Attachements") Is Nothing Then
                        Dim dtAttachments As New DataTable
                        Dim k As Integer
                        dtTable = CType(SessionObject("Attachements"), DataTable)


                        For k = 0 To dtTable.Rows.Count - 1
                            strSplitedNames = Split(dtTable.Rows(k).Item("FileLocation").ToString, "/")
                            dtTable.Rows(k).Item("FileLocation") = Replace(dtTable.Rows(k).Item("FileLocation").ToString, "../documents/docs/", Current.Server.MapPath("../documents/docs/"))
                            'Checking whether the Files is present

                            strLocation = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & strSplitedNames(strSplitedNames.Length - 1).ToString
                            If File.Exists(strLocation) = True Then
                                '  Dim a As New Attachment(dtTable.Rows(k).Item("FileLocation"))
                                objSMTP.AddAttachment(strLocation)
                            ElseIf File.Exists(dtTable.Rows(k).Item("FileLocation").ToString) = True Then
                                objSMTP.AddAttachment(dtTable.Rows(k).Item("FileLocation").ToString)
                            End If
                        Next
                        SessionObject("Attachements") = Nothing
                    End If
                End If

                Return objSMTP.Send()
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        'Below function can be used only when we have to send mail which is required for system .e.g sending forgotten password , reporting exception details 
        'here smtp account will be used is ours so should not be used to send user generated mails.
        Public Function SendSystemEmail(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String, Optional ByVal FromDisplayName As String = "", Optional ByVal strBCC As String = "") As Boolean
            Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
            Dim objSMTP As Smtp
            Try
                Dim objServer As SmtpServer = New SmtpServer
                objSMTP = New Smtp
                If File.Exists("C:\log.txt") Then
                    objSMTP.Log.Filename = "C:\log.txt"
                    objSMTP.Log.Enabled = True
                Else
                    objSMTP.Log.Enabled = False
                End If
                objSMTP.Log.DisableOnException = False

                objServer.AuthMethods = AuthenticationMethods.Auto
                objServer.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                objServer.AccountName = "noreply@bizautomation.com"
                Dim strPassword As String
                strPassword = "Chintan1213"
                objServer.Password = strPassword

                objServer.Port = 587
                objServer.SslMode = SslStartupMode.UseStartTls
                objServer.Name = "smtp.gmail.com"
                objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                objSMTP.SmtpServers.Add(objServer)
                objSMTP.From.AsString = strFrom

                If FromDisplayName <> "" Then
                    objSMTP.From.DisplayName = FromDisplayName
                End If
                objSMTP.To.AsString = strTo
                If strCC <> "" Then
                    objSMTP.Cc.AsString = strCC
                End If
                If strBCC <> "" Then
                    objSMTP.Bcc.AsString = strBCC
                End If
                objSMTP.Subject = strSubject
                objSMTP.BodyHtmlText = strBody
                Return objSMTP.Send()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub GetFromEmailDetails(ByRef smtp As MailBee.SmtpMail.Smtp, ByVal DomainID As Long)
            Try
                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = DomainID
                dtTable = objUserAccess.GetFromEmailDetails()
                If dtTable.Rows.Count = 1 Then
                    smtp.From.DisplayName = dtTable.Rows(0)("FromName").ToString()
                    smtp.From.AsString = dtTable.Rows(0)("FromEmail").ToString()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Function GetSMTPDetails(ByRef objServer As SmtpServer, ByVal DomainID As Long, Optional ByVal UserId As Long = 0) As Boolean
            Dim objCommon As New CCommon
            Dim strPassword As String
            If DomainID > 0 AndAlso (UserId = -1 Or UserId > 0) Then
                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.UserId = UserId
                objUserAccess.DomainID = DomainID
                dtTable = objUserAccess.GetUserSMTPDetails

                _SMTPServerIntegration = False
                If dtTable.Rows.Count > 0 Then
                    _SMTPServerIntegration = CBool(dtTable.Rows(0).Item("bitSMTPServer"))

                    objServer.Name = dtTable.Rows(0).Item("vcSMTPServer").ToString
                    'Use Authentication?
                    If CBool(dtTable.Rows(0).Item("bitSMTPAuth")) = True Then
                        objServer.AuthMethods = AuthenticationMethods.Auto
                        objServer.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                    Else
                        objServer.AuthMethods = AuthenticationMethods.None
                        objServer.AuthOptions = AuthenticationOptions.None
                    End If
                    If CBool(dtTable.Rows(0).Item("bitSMTPServer")) = True Then
                        objServer.AccountName = dtTable.Rows(0).Item("vcSMTPUserName").ToString
                        strPassword = objCommon.Decrypt(dtTable.Rows(0).Item("vcSMTPPassword").ToString)
                        objServer.Password = strPassword
                    End If
                    If CInt(dtTable.Rows(0).Item("numSMTPPort")) <> 0 Then
                        objServer.Port = CInt(dtTable.Rows(0).Item("numSMTPPort"))
                    End If
                    If CBool(dtTable.Rows(0).Item("bitSMTPSSL")) = True Then
                        objServer.SslMode = SslStartupMode.UseStartTls
                    End If
                    _EMailFromDisplayName = ""
                End If
            ElseIf DomainID > 0 Then
                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = DomainID
                dtTable = objUserAccess.GetDomainDetails()

                _SMTPServerIntegration = CBool(dtTable.Rows(0).Item("bitPSMTPServer"))

                objServer.Name = dtTable.Rows(0).Item("vcPSMTPServer").ToString
                'Use Authentication?
                If CBool(dtTable.Rows(0).Item("bitPSMTPAuth")) = True Then
                    objServer.AuthMethods = AuthenticationMethods.Auto
                    objServer.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                Else
                    objServer.AuthMethods = AuthenticationMethods.None
                    objServer.AuthOptions = AuthenticationOptions.None
                End If
                If CBool(dtTable.Rows(0).Item("bitPSMTPServer")) = True Then
                    objServer.AccountName = dtTable.Rows(0).Item("vcPSMTPUserName").ToString
                    strPassword = objCommon.Decrypt(dtTable.Rows(0).Item("vcPSMTPPassword").ToString)
                    objServer.Password = strPassword
                End If
                If CInt(dtTable.Rows(0).Item("numPSMTPPort")) <> 0 Then
                    objServer.Port = CInt(dtTable.Rows(0).Item("numPSMTPPort"))
                End If
                If CBool(dtTable.Rows(0).Item("bitPSMTPSSL")) = True Then
                    objServer.SslMode = SslStartupMode.UseStartTls
                End If
                _EMailFromDisplayName = CCommon.ToString(dtTable.Rows(0).Item("vcPSMTPDisplayName"))
            Else
                _SMTPServerIntegration = CBool(SessionObject("SMTPServerIntegration"))
                objServer.Name = SessionObject("SMTPServer").ToString
                'Use Authentication?
                If CBool(SessionObject("SMTPAuth")) = True Then
                    objServer.AuthMethods = AuthenticationMethods.Auto
                    objServer.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                Else
                    objServer.AuthMethods = AuthenticationMethods.None
                    objServer.AuthOptions = AuthenticationOptions.None
                End If
                If CBool(SessionObject("SMTPServerIntegration")) = True Then
                    If Not String.IsNullOrEmpty(CCommon.ToString(SessionObject("UserEmailAlias"))) Then
                        objServer.AccountName = CCommon.ToString(SessionObject("UserEmailAlias"))
                        strPassword = objCommon.Decrypt(SessionObject("UserEmailAliasPassword").ToString)
                        objServer.Password = strPassword
                    Else
                        objServer.AccountName = CCommon.ToString(SessionObject("UserEmail"))
                        strPassword = objCommon.Decrypt(SessionObject("SMTPPassword").ToString)
                        objServer.Password = strPassword
                    End If
                End If
                If CInt(SessionObject("SMTPPort")) <> 0 Then
                    objServer.Port = CInt(SessionObject("SMTPPort"))
                End If
                If CBool(SessionObject("bitSMTPSSL")) = True Then
                    objServer.SslMode = SslStartupMode.UseStartTls
                End If
                _EMailFromDisplayName = ""
            End If

            Return True
        End Function


        Public Function SendEmailToQueue(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String, ByRef dtTable As DataTable, Optional ByVal FromDisplayName As String = "", Optional ByVal strBCC As String = "", Optional ByVal DomainID As Long = 0) As Boolean
            Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
            Dim pickupFolderPath As String = ConfigurationManager.AppSettings("MailQueuePath")
            Dim objSMTP As Smtp
            Try

                'If CBool(SessionObject("SMTPServerIntegration")) = False Then
                '    Exit Function
                'End If

                'Dim objServer As SmtpServer = New SmtpServer
                objSMTP = New Smtp

                If File.Exists("C:\log.txt") Then
                    objSMTP.Log.Filename = "C:\log.txt"
                    objSMTP.Log.Enabled = True
                Else
                    objSMTP.Log.Enabled = False
                End If

                objSMTP.Log.DisableOnException = False

                'GetSMTPDetails(objServer, DomainID)
                'objSMTP.SmtpServers.Add(objServer)

                objSMTP.From.AsString = strFrom
                objSMTP.To.AsString = strTo
                If strCC <> "" Then
                    objSMTP.Cc.AsString = strCC
                End If
                If strBCC <> "" Then
                    objSMTP.Bcc.AsString = strBCC
                End If

                objSMTP.Subject = strSubject

                objSMTP.BodyHtmlText = strBody

                If FromDisplayName <> "" Then
                    objSMTP.From.DisplayName = FromDisplayName
                End If
                If SessionObject.ContainsKey("Attachements") Then
                    If Not SessionObject("Attachements") Is Nothing Then
                        Dim dtAttachments As New DataTable
                        Dim k As Integer
                        dtTable = CType(SessionObject("Attachements"), DataTable)
                        Dim strSplitedNames() As String

                        For k = 0 To dtTable.Rows.Count - 1
                            strSplitedNames = Split(dtTable.Rows(k).Item("FileLocation").ToString, "/")
                            dtTable.Rows(k).Item("FileLocation") = Replace(dtTable.Rows(k).Item("FileLocation").ToString, "../documents/docs/", Current.Server.MapPath("../documents/docs/"))
                            'Checking whether the Files is present
                            Dim strLocation As String
                            strLocation = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & strSplitedNames(strSplitedNames.Length - 1).ToString
                            If File.Exists(strLocation) = True Then
                                '  Dim a As New Attachment(dtTable.Rows(k).Item("FileLocation"))
                                objSMTP.AddAttachment(strLocation)
                            ElseIf File.Exists(dtTable.Rows(k).Item("FileLocation").ToString) = True Then
                                objSMTP.AddAttachment(dtTable.Rows(k).Item("FileLocation").ToString)
                            End If
                        Next
                        SessionObject("Attachements") = Nothing
                    End If
                End If
                AddHandler objSMTP.SubmittingMessageToPickupFolder, AddressOf OnSubmittingMessageToPickupFolder
                objSMTP.AddJob(Nothing, strFrom, Nothing, dtTable)

                Return objSMTP.SubmitJobsToPickupFolder(pickupFolderPath, True) 'kishan change pass true --in new dll new perameter added
            Catch ex As Exception
                'Send email alerting that MailQueue doesn't work with exception details
                SendSystemEmail("Email Queue Error", "Error:" & ex.Message & "<br> StackTrace:" & ex.StackTrace & "<br>DomainID:" & CCommon.ToString(SessionObject("DomainID")), "", "noreply@bizautomation.com", "support@bizautomation.com", "BizAutomation", "")
                Return False
            End Try
        End Function
        Sub OnSubmittingMessageToPickupFolder(ByVal sender As Object, ByVal e As SmtpSubmittingMessageToPickupFolderEventArgs)
            Try
                If BroadcastId > 0 Then
                    Dim objCampaign As New BACRM.BusinessLogic.Marketing.Campaign
                    Dim BroadCastBizMessage As String
                    Dim dtBroadcastingLink As DataTable
                    objCampaign.broadcastID = BroadcastId
                    dtBroadcastingLink = objCampaign.GetBroadcastingLink()
                    BroadCastBizMessage = e.MailMessage.BodyHtmlText
                    If dtBroadcastingLink.Rows.Count > 0 Then
                        For Each dr As DataRow In dtBroadcastingLink.Rows
                            BroadCastBizMessage = BroadCastBizMessage.Replace(ConfigurationManager.AppSettings("EmailTracking") & "?BroadCastID=" & BroadcastId & "&LinkID=" & CCommon.ToString(dr("numLinkId")), ConfigurationManager.AppSettings("EmailTracking") & "?BroadCastID=" & BroadcastId & "&LinkID=" & CCommon.ToString(dr("numLinkId")) & "&ContactID=" & CCommon.ToString(e.MergeTable.Rows(e.MergeRowIndex)("numContactID")))
                        Next
                    End If
                    e.MailMessage.BodyHtmlText = BroadCastBizMessage
                End If
                e.MailMessage.Headers.Add("List-Unsubscribe", "<" + e.MergeTable.Rows(e.MergeRowIndex)("ContactOpt-OutLink").ToString + ">,<mailto:reply-2257c90864-47133df407-2226@bizautomationmail.com?subject=unsubscribe>", True)
                e.MailMessage.Headers.Add("Preference", "<bulk>", True)
                e.MailMessage.DomainKeysSign(False, Nothing, PrivateKey, False, "bizautomationmail", DomainKeysTypes.Both)
            Catch ex As Exception
                SendSystemEmail("Email Queue Error", "Error:" & ex.Message & "<br> StackTrace:" & ex.StackTrace & "<br>DomainID:" & CCommon.ToString(SessionObject("DomainID")), "", "noreply@bizautomation.com", "support@bizautomation.com", "BizAutomation", "")
            End Try
        End Sub
        Public Sub DisableImap(ByVal ContactId As Long, ByVal DomainID As Long)
            Try
                Dim objUserAccess As New UserAccess
                'Those accounts which throws error shoud be disabled.. once user enter correct info then it will active again
                objUserAccess.ContactID = ContactId
                objUserAccess.DomainID = DomainID
                objUserAccess.DisableImap()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub DisableSMTP(ByVal UserID As Long, ByVal DomainID As Long)
            Try
                Dim objUserAccess As New UserAccess
                'Those accounts which throws error shoud be disabled.. once user enter correct info then it will active again
                objUserAccess.UserId = UserID
                objUserAccess.DomainID = DomainID
                objUserAccess.DisableSMTP()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Function ExtractEmailAddressesFromString(ByVal source As String) As String()
            Dim mc As MatchCollection
            Dim i As Integer

            ' expression garnered from www.regexlib.com - thanks guys!

            mc = Regex.Matches(source,
                "([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})")
            Dim results(mc.Count - 1) As String
            For i = 0 To results.Length - 1
                results(i) = mc(i).Value
            Next

            Return results
        End Function
        'For more information on asynchronous function call refere following
        'Author:Sachin Sadhu||Date:11Dec2013||case No:512
        'Modification in this Function:Added optional parameter:StrPageUrl to check Current open Page
        'http://www.codeproject.com/KB/cs/AsyncMethodInvocation.aspx
        Public Delegate Function MethodDelegate(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String, ByRef dtMailMerge As DataTable, ByVal FromDisplayName As String, ByVal strBCC As String, ByVal DomainID As Long, ByVal IsAttachmentDelete As Boolean, ByVal strPageUrl As String, ByVal UserID As Long) As Boolean
        Public Sub AddEmailToJob(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String, ByVal dtMailMerge As DataTable, Optional ByVal FromDisplayName As String = "", Optional ByVal strBCC As String = "", Optional ByVal DomainID As Long = 0, Optional ByVal IsAttachmentDelete As Boolean = False, Optional ByVal strPageUrl As String = "", Optional ByVal UserID As Long = 0)
            Try
                Dim dlgt As New MethodDelegate(AddressOf SendEmail)

                ' Create the callback delegate.
                Dim cb As New AsyncCallback(AddressOf MyAsyncCallback)

                If SessionObject IsNot Nothing AndAlso SessionObject.Count <> 0 Then
                    'Copy Request object's properties to be used for exception handling if any occours
                    SessionObject.Add("AbsolutePath", Current.Request.Url.AbsolutePath)
                    SessionObject.Add("BrowserType", Current.Request.Browser.Type)
                    SessionObject.Add("UserAgent", Current.Request.UserAgent)
                    SessionObject.Add("UserHostAddress", Current.Request.UserHostAddress)
                    If (Not Current.Request.UrlReferrer Is Nothing) Then
                        SessionObject.Add("UrlReferrer", Current.Request.UrlReferrer)
                    End If
                    If Not Current.Request.QueryString("enc") Is Nothing Then
                        SessionObject.Add("enc", Current.Request.QueryString("enq"))
                    Else
                        SessionObject.Add("enc", "")
                    End If
                End If

                Dim ar As IAsyncResult = dlgt.BeginInvoke(strSubject, strBody, strCC, strFrom, strTo, dtMailMerge, FromDisplayName, strBCC, DomainID, IsAttachmentDelete, strPageUrl, UserID, cb, dlgt)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub MyAsyncCallback(ByVal ar As IAsyncResult)
            Dim dtMailMerge As DataTable
            ' Because you passed your original delegate in the asyncState parameter
            ' of the Begin call, you can get it back here to complete the call.
            Dim dlgt As MethodDelegate = DirectCast(ar.AsyncState, MethodDelegate)
            Try
                ' Complete the call.
                dlgt.EndInvoke(dtMailMerge, ar)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, 1, 1, SessionObject)
            End Try
        End Sub

        Public Shared Function CopySession() As Dictionary(Of String, Object)
            Try
                Dim objDict As New Dictionary(Of String, Object)()
                If Not Current Is Nothing Then
                    If Not Current.Session Is Nothing Then

                        Dim Keys As IEnumerator = Current.Session.Keys.GetEnumerator()
                        While Keys.MoveNext()
                            objDict.Add(Keys.Current.ToString(), Current.Session(Keys.Current.ToString()))
                        End While

                        'Retrive values from dictionary
                        'objDict("Key")
                        '    Or
                        'For Each entry As KeyValuePair(Of String, Object) In objDict
                        '    Response.Write((entry.Key & "=") + entry.Value.ToString() & "<br>")
                        'Next

                    End If
                End If
                Return objDict
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetEmailTemplateByCode(ByVal strTemplateCode As String, ByVal DomainID As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcTemplateCode", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = strTemplateCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetEmailTemplateByCode", arParms)
                If ds.Tables(0).Rows.Count > 0 Then
                    _TemplateSubject = CCommon.ToString(ds.Tables(0).Rows(0)("vcSubject"))
                    _TemplateBody = CCommon.ToString(ds.Tables(0).Rows(0)("vcDocDesc"))
                End If

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetEmailMergeData(ByVal ModuleID As Long, ByVal strRecordIDs As String, ByVal DomainID As Long, ByVal Mode As Short, ByVal ClientTimeZone As Int32, Optional ByVal numOppBizDocId As Long = 0) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ModuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcRecordIDs", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(1).Value = strRecordIDs

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = Mode

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = ClientTimeZone

                arParms(5) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = numOppBizDocId

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetEmailMergeData", arParms)

                ' Added by Priya(Display CreditCard Info) (12 Feb 2018)
                If (ModuleID = 1 Or ModuleID = 45) Then
                    Dim dtEmailMergeTable As DataTable
                    Dim objEncryption As New QueryStringValues
                    Dim strCard As String = ""
                    dtEmailMergeTable = ds.Tables(0)
                    For Each drrow As DataRow In dtEmailMergeTable.Rows
                        If (drrow("PrimaryCreditCardNo").ToString() IsNot "") Then
                            strCard = objEncryption.Decrypt(drrow("PrimaryCreditCardNo").ToString())
                            drrow("PrimaryCreditCardNo") = "************" & strCard.Substring(strCard.Length - 4, 4)
                        End If
                    Next
                    Return dtEmailMergeTable
                ElseIf (ModuleID = 2) Then
                    Dim dtEmailMergeTable As DataTable
                    Dim strCard As String = ""
                    dtEmailMergeTable = ds.Tables(0)
                    For Each drrow As DataRow In dtEmailMergeTable.Rows
                        If (drrow("PrimaryCreditCardNo").ToString() IsNot "") Then
                            Dim objCommon As New CCommon
                            strCard = objCommon.Decrypt(drrow("PrimaryCreditCardNo").ToString())
                            drrow("PrimaryCreditCardNo") = "************" & strCard.Substring(strCard.Length - 4, 4)
                        End If
                    Next
                    Return dtEmailMergeTable
                Else
                    Return ds.Tables(0)
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _DomainID As Long
        Public Property DomainID() As Long
            Get
                Return _DomainID
            End Get
            Set(ByVal value As Long)
                _DomainID = value
            End Set
        End Property
        Private _IsAttachmentDelete As Boolean
        Public Property IsAttachmentDelete() As Boolean
            Get
                Return _IsAttachmentDelete
            End Get
            Set(ByVal value As Boolean)
                _IsAttachmentDelete = value
            End Set
        End Property

        Private _TemplateCode As String
        Public Property TemplateCode() As String
            Get
                Return _TemplateCode
            End Get
            Set(ByVal value As String)
                _TemplateCode = value
            End Set
        End Property

        Private _ModuleID As Long
        Public Property ModuleID() As Long
            Get
                Return _ModuleID
            End Get
            Set(ByVal value As Long)
                _ModuleID = value
            End Set
        End Property

        Private _RecordIDs As String
        Public Property RecordIds() As String
            Get
                Return _RecordIDs
            End Get
            Set(ByVal value As String)
                _RecordIDs = value
            End Set
        End Property

        Private _TemplateSubject As String
        Public Property TemplateSubject() As String
            Get
                Return _TemplateSubject
            End Get
            Set(ByVal value As String)
                _TemplateSubject = value
            End Set
        End Property

        Private _TemplateBody As String
        Public Property TemplateBody() As String
            Get
                Return _TemplateBody
            End Get
            Set(ByVal value As String)
                _TemplateBody = value
            End Set
        End Property


        Private _AdditionalMergeFields As Hashtable
        Public Property AdditionalMergeFields() As Hashtable
            Get
                If _AdditionalMergeFields Is Nothing Then
                    _AdditionalMergeFields = New Hashtable
                End If
                Return _AdditionalMergeFields
            End Get
            Set(ByVal value As Hashtable)
                _AdditionalMergeFields = value
            End Set
        End Property

        Private _FromEmail As String
        Public Property FromEmail() As String
            Get
                Return _FromEmail
            End Get
            Set(ByVal value As String)
                _FromEmail = value
            End Set
        End Property

        Private _EMailFromDisplayName As String
        Public Property EMailFromDisplayName() As String
            Get
                Return _EMailFromDisplayName
            End Get
            Set(ByVal value As String)
                _EMailFromDisplayName = value
            End Set
        End Property


        Private _ToEmail As String
        Public Property ToEmail() As String
            Get
                Return _ToEmail
            End Get
            Set(ByVal value As String)
                _ToEmail = value
            End Set
        End Property


        Private _CCEmail As String
        Public Property CCEmail() As String
            Get
                Return _CCEmail
            End Get
            Set(ByVal value As String)
                _CCEmail = value
            End Set
        End Property

        Private _DocID As Long
        Public Property DocID() As Long
            Get
                Return _DocID
            End Get
            Set(ByVal value As Long)
                _DocID = value
            End Set
        End Property

        Private _SMTPServerIntegration As Boolean
        Public Property SMTPServerIntegration() As Boolean
            Get
                Return _SMTPServerIntegration
            End Get
            Set(ByVal value As Boolean)
                _SMTPServerIntegration = value
            End Set
        End Property

        Private _UserID As Long
        Public Property UserID() As Long
            Get
                Return _UserID
            End Get
            Set(ByVal value As Long)
                _UserID = value
            End Set
        End Property

        Function SendEmailTemplate(ByVal ParamArray paramValues() As Object) As Boolean
            Try
                If _DocID > 0 Then
                    Dim objDocuments As New Documents.DocumentList
                    objDocuments.GenDocID = _DocID
                    objDocuments.DomainID = _DomainID
                    Dim dtEmailTemplate As DataTable = objDocuments.GetDocByGenDocID
                    If dtEmailTemplate.Rows.Count > 0 Then
                        _TemplateSubject = CCommon.ToString(dtEmailTemplate.Rows(0)("vcSubject"))
                        _TemplateBody = CCommon.ToString(dtEmailTemplate.Rows(0)("vcDocDesc"))
                    End If
                Else
                    GetEmailTemplateByCode(_TemplateCode, _DomainID)
                End If

                'Get Merger Fields data
                Dim dtMailMerge As DataTable
                dtMailMerge = GetEmailMergeData(_ModuleID, _RecordIDs, _DomainID, 0, _ClientTimeZoneOffset)

                If _ModuleID = 1 Then AdditionalMergeFields.Add("ContactOpt-OutLink", "")

                If _ModuleID = 8 Then
                    'Add new columns for attachment merge
                    CCommon.AddColumnsToDataTable(dtMailMerge, "AttachmentFileName,AttchmentFilePath")
                    dtMailMerge.PrimaryKey = New DataColumn() {dtMailMerge.Columns("numOppBizDocsID")}

                    Dim dtValues As DataTable
                    If paramValues.Length > 0 Then
                        dtValues = CType(paramValues(1), DataTable)
                        'Update values in merge table
                        For Each dr As DataRow In dtValues.Rows
                            Dim dRowFound As DataRow = dtMailMerge.Rows.Find(CCommon.ToLong(dr("numOppBizDocsID")))
                            dRowFound.Item("AttachmentFileName") = dr("AttachmentFileName")
                            dRowFound.Item("AttchmentFilePath") = dr("AttchmentFilePath")
                        Next
                    End If



                ElseIf _ModuleID = 11 Then
                    'Add new columns for attachment merge
                    AdditionalMergeFields.Add("Signature", "")

                    CCommon.AddColumnsToDataTable(dtMailMerge, "AttachmentFileName,AttchmentFilePath")
                    dtMailMerge.PrimaryKey = New DataColumn() {dtMailMerge.Columns("numContactId")}
                    Dim dtValues As DataTable
                    If paramValues.Length > 0 Then
                        dtValues = CType(paramValues(1), DataTable)
                        'Update values in merge table
                        For Each dr As DataRow In dtValues.Rows
                            Dim dRowFound As DataRow = dtMailMerge.Rows.Find(CCommon.ToLong(dr("numContactID")))
                            dRowFound.Item("AttachmentFileName") = dr("AttachmentFileName")
                            dRowFound.Item("AttchmentFilePath") = dr("AttchmentFilePath")
                        Next
                    End If

                End If

                For Each iKey As Object In AdditionalMergeFields.Keys
                    dtMailMerge.Columns.Add(iKey.ToString)
                    'Assign values to rows in merge table
                    For Each row As DataRow In dtMailMerge.Rows
                        row(iKey.ToString) = CCommon.ToString(AdditionalMergeFields(iKey))

                        'update unsubscibe link value
                        If _ModuleID = 1 And iKey.ToString = "ContactOpt-OutLink" Then
                            Dim strUnsubscribeLink As String = CCommon.ToString(System.Web.HttpContext.Current.Session("PortalURL")).ToLower.Replace("/login.aspx", "") & "/Common/frmUnsubscribe.aspx?e=" & row("ContactEmail").ToString & "&c=" & row("numContactID").ToString & "&d=" & _DomainID
                            row(iKey.ToString) = strUnsubscribeLink
                        End If

                        If _ModuleID = 11 And iKey.ToString = "Signature" Then
                            row(iKey.ToString) = SessionObject("Signature")
                        End If
                    Next
                Next


                Select Case _TemplateCode
                    Case "#SYS#PORTAL_NEW_USER"
                        dtMailMerge.PrimaryKey = New DataColumn() {dtMailMerge.Columns("numContactID")}
                        Dim dtValues As DataTable
                        If paramValues.Length > 0 Then
                            dtValues = CType(paramValues(0), DataTable)
                            'Update values in merge table
                            For Each dr As DataRow In dtValues.Rows
                                Dim dRowFound As DataRow = dtMailMerge.Rows.Find(CCommon.ToLong(dr("numContactID")))
                                dRowFound.Item("PortalPassword") = dr("PortalPassword")
                            Next

                        End If

                    Case "#SYS#ECOMMERCE_SHOPPING_COMPLETED"
                        If paramValues.Length > 0 Then
                            _TemplateBody = _TemplateBody.Replace("#EMAIL_TEMPLATE#SYS#ECOMMERCE_BIZDOC_ITEMS#", CType(paramValues(0), String))
                        End If

                    Case "#SYS#CUSTOMER_STATEMENT"
                        If paramValues.Length > 0 Then
                            _TemplateBody = _TemplateBody.Replace("#EMAIL_TEMPLATE#SYS#ECOMMERCE_BIZDOC_ITEMS#", CType(paramValues(0), String))
                        End If
                End Select

                If dtMailMerge.Rows.Count > 0 Then
                    AddEmailToJob(_TemplateSubject, _TemplateBody, _CCEmail, _FromEmail, _ToEmail, dtMailMerge, DomainID:=_DomainID, IsAttachmentDelete:=_IsAttachmentDelete, UserID:=UserID)
                    Return True
                End If
                Return False
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ConvertToJSON(ByVal dtTable As DataTable) As String
            Dim strJSON As New System.Text.StringBuilder
            Dim intColumn As Integer = dtTable.Columns.Count - 1
            Dim intRows As Integer = dtTable.Rows.Count - 1
            Dim intCount As Integer = 0
            Dim intSubcount As Integer = 0
            Dim strColumnName As String = ""

            strJSON.Append("[")
            For intCount = 0 To intRows - 1
                strJSON.Append("{")
                For intSubcount = 0 To intColumn - 1
                    strColumnName = dtTable.Columns(intColumn).ColumnName
                    strJSON.Append("""" & strColumnName & """:""" & dtTable.Rows(intRows)(strColumnName).ToString & """,")
                Next
                strJSON.Append("},")
            Next
            Return strJSON.ToString
        End Function

        Public Function SendCustomReportEmail(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String, ByVal fileName As String, Optional ByVal FromDisplayName As String = "", Optional ByVal strBCC As String = "", Optional ByVal DomainID As Long = 0) As Boolean
            Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")

            Try
                Dim isOauthTokenExists As Boolean = False
                Dim objSMTP As Smtp = New Smtp

                If DomainID > 0 Then
                    'First check if Gmail or Office365 oauth2 token exists
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = DomainID
                    objUserAccess.UserCntID = 0
                    Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        If (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                            If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                                isOauthTokenExists = PrepareSmtpForGmail(DomainID, 0, objSMTP, dt)
                            ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                                isOauthTokenExists = PrepareSmtpForOffice365(DomainID, 0, objSMTP, strFrom, dt)
                            End If
                        End If
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim objServer As SmtpServer = New SmtpServer
                    GetSMTPDetails(objServer, DomainID)

                    'Add SMTP server
                    objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                    objSMTP.SmtpServers.Add(objServer)

                    If SMTPServerIntegration = False Then
                        Return False
                        Exit Function
                    End If
                End If


                If File.Exists("C:\log.txt") Then
                    objSMTP.Log.Filename = "C:\log.txt"
                    objSMTP.Log.Enabled = True
                Else
                    objSMTP.Log.Enabled = False
                End If
                objSMTP.Log.DisableOnException = False

                objSMTP.From.AsString = strFrom

                If FromDisplayName <> "" Then
                    objSMTP.From.DisplayName = FromDisplayName
                End If
                objSMTP.To.AsString = strTo
                If strCC <> "" Then
                    objSMTP.Cc.AsString = strCC
                End If
                If strBCC <> "" Then
                    objSMTP.Bcc.AsString = strBCC
                End If
                objSMTP.Subject = strSubject
                objSMTP.BodyHtmlText = strBody

                Dim strLocation As String
                Dim strLocation1 As String
                Dim dtTable As DataTable
                Dim strSplitedNames() As String
                'Added Code for attachment- chintan

                If Not String.IsNullOrEmpty(fileName) Then
                    strLocation = CCommon.GetDocumentPhysicalPath(DomainID) & fileName

                    If File.Exists(strLocation) = True Then
                        objSMTP.AddAttachment(strLocation)
                    End If
                End If


                Return objSMTP.Send()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SendCustomCollabarationEmail(ByVal strSubject As String, ByVal strBody As String, ByVal strCC As String, ByVal strFrom As String, ByVal strTo As String, ByVal fileName As String, Optional ByVal FromDisplayName As String = "", Optional ByVal strBCC As String = "", Optional ByVal DomainID As Long = 0) As Boolean
            Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")

            Try
                Dim isOauthTokenExists As Boolean = False
                Dim objSMTP As Smtp = New Smtp

                If DomainID > 0 Then
                    'First check if Gmail or Office365 oauth2 token exists
                    Dim objUserAccess As New UserAccess
                    objUserAccess.DomainID = DomainID
                    objUserAccess.UserCntID = 0
                    Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        If (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                            If CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Then
                                isOauthTokenExists = PrepareSmtpForGmail(DomainID, 0, objSMTP, dt)
                            ElseIf CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2 Then
                                isOauthTokenExists = PrepareSmtpForOffice365(DomainID, 0, objSMTP, strFrom, dt)
                            End If
                        End If
                    End If
                End If

                If Not isOauthTokenExists Then
                    Dim objServer As SmtpServer = New SmtpServer
                    GetSMTPDetails(objServer, DomainID)

                    'Add SMTP server
                    objServer.SslProtocol = MailBee.Security.SecurityProtocol.TlsAuto
                    objSMTP.SmtpServers.Add(objServer)

                    If SMTPServerIntegration = False Then
                        Return False
                        Exit Function
                    End If
                End If


                If File.Exists("C:\log.txt") Then
                    objSMTP.Log.Filename = "C:\log.txt"
                    objSMTP.Log.Enabled = True
                Else
                    objSMTP.Log.Enabled = False
                End If
                objSMTP.Log.DisableOnException = False

                objSMTP.From.AsString = strFrom

                If FromDisplayName <> "" Then
                    objSMTP.From.DisplayName = FromDisplayName
                End If
                objSMTP.To.AsString = strTo
                If strCC <> "" Then
                    objSMTP.Cc.AsString = strCC
                End If
                If strBCC <> "" Then
                    objSMTP.Bcc.AsString = strBCC
                End If
                objSMTP.Subject = strSubject
                objSMTP.BodyHtmlText = strBody

                Dim strLocation As String
                Dim strLocation1 As String
                Dim dtTable As DataTable
                Dim strSplitedNames() As String
                'Added Code for attachment- chintan

                If Not String.IsNullOrEmpty(fileName) Then
                    strLocation = CCommon.GetDocumentPhysicalPath(DomainID) & fileName

                    If File.Exists(strLocation) = True Then
                        objSMTP.AddAttachment(strLocation)
                    End If
                End If


                Return objSMTP.Send()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ' Created by Priya for Fetching Custom Fields value in Emails
        Public Function GetCustomMergeFieldsData(ByVal ModuleID As Long, ByVal DomainID As Long, ByVal strRecordIDs As String, ByVal OppId As Long) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ModuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcRecordIDs", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(2).Value = strRecordIDs

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = OppId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCustomMergeFieldsForEmailTemplate", arParms)

                If (Not ds Is Nothing And ds.Tables.Count > 0) Then
                    Return ds.Tables(0)
                Else
                    Return Nothing
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ' Created by Priya for Fetching Merge Fields value For Mirror BizDoc
        Public Function GetEmailMergeDataMirrorBizDoc(ByVal UserCntID As Long, ByVal DomainID As Long, ByVal RefType As Short, ByVal RefId As Long, ByVal ClientTimeZone As Int32) As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numReferenceID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = RefId

                arParms(1) = New Npgsql.NpgsqlParameter("@numReferenceType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = RefType

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = ClientTimeZone

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetEmailMergeDataForMirrorBizDoc", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace