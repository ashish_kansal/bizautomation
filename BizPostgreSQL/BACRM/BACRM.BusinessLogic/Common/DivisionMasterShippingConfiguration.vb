﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports System.Data.SqlClient

Namespace BACRM.BusinessLogic.Common
    Public Class DivisionMasterShippingConfiguration
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"
        Public Property DivisionID As Long
        Public Property IsAdditionalHandling As Boolean
        Public Property IsCOD As Boolean
        Public Property IsHomeDelivery As Boolean
        Public Property IsInsideDelevery As Boolean
        Public Property IsInsidePickup As Boolean
        Public Property IsSaturdayDelivery As Boolean
        Public Property IsSaturdayPickup As Boolean
        Public Property IsLargePackage As Boolean
        Public Property DeliveryConfirmation As String
        Public Property Description As String
        Public Property SignatureType As String
        Public Property CODType As String
#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@IsAdditionalHandling", IsAdditionalHandling, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@IsCOD", IsCOD, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@IsHomeDelivery", IsHomeDelivery, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@IsInsideDelevery", IsInsideDelevery, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@IsInsidePickup", IsInsidePickup, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@IsSaturdayDelivery", IsSaturdayDelivery, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@IsSaturdayPickup", IsSaturdayPickup, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@IsLargePackage", IsLargePackage, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcDeliveryConfirmation", DeliveryConfirmation, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcDescription", Description, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcSignatureType", SignatureType, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcCODType", CODType, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_DivisionMasterShippingConfiguration_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetByDivision() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_DivisionMasterShippingConfiguration_Get", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

    End Class
End Namespace