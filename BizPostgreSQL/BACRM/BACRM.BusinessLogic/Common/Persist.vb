﻿Imports BACRM.BusinessLogic.Common
Imports System.Collections
Imports System.IO
Imports System.Xml.Serialization
Imports System.Runtime.CompilerServices
Imports System.Web

Namespace BACRM.BusinessLogic.Common
    ''' <summary>
    ''' Reason to not implement KeyName in Enum is that enums return integer, and string based enums are not possible in .net
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Class PersistKey
        Public Shared ReadOnly CurrentPage As String = "CurrentPage"
        Public Shared ReadOnly PageSize As String = "PageSize"
        Public Shared ReadOnly SortColumnName As String = "SortColumnName"
        Public Shared ReadOnly SortOrder As String = "SortOrder"
        Public Shared ReadOnly SortCharacter As String = "SortCharacter"
        Public Shared ReadOnly FilterBy As String = "FilterBy"
        Public Shared ReadOnly FirstName As String = "FirstName"
        Public Shared ReadOnly LastName As String = "LastName"
        Public Shared ReadOnly OrgName As String = "OrgName"
        Public Shared ReadOnly SearchValue As String = "SearchValue"
        Public Shared ReadOnly GridColumnSearch As String = "GridColumnSearch"
        Public Shared ReadOnly SelectedTab As String = "SelectedTab"
        Public Shared ReadOnly EmailTabAdvanceSearchVisible As String = "EmailTabAdvanceSearchVisible"
        Public Shared ReadOnly OrderStatus As String = "OrderStatus"
    End Class

    <Serializable()>
    Public Class PersistAttribute
        Private _Key As String
        Public Property Key() As String
            Get
                Return _Key
            End Get
            Set(ByVal value As String)
                _Key = value
            End Set
        End Property
        Private _Value As Object
        Public Property Value() As Object
            Get
                Return _Value
            End Get
            Set(ByVal value As Object)
                _Value = value
            End Set
        End Property
        Sub New()

        End Sub
        Sub New(strkey As String, objValue As Object)
            Key = strkey
            Value = objValue
        End Sub

    End Class

    '<Serializable()>
    'Public Class BizPersist

    '    Private _PageName As String
    '    ''' <summary>
    '    ''' PageName will automatically convert page name to lower case
    '    ''' </summary>
    '    ''' <value></value>
    '    ''' <returns></returns>
    '    ''' <remarks></remarks>
    '    Public Property PageName() As String
    '        Get
    '            Return _PageName.ToLower()
    '        End Get
    '        Set(ByVal value As String)
    '            _PageName = value.ToLower()
    '        End Set
    '    End Property

    '    Shared Sub Save(ByVal PersistTable As Hashtable, ByVal PageName As String, ByVal UserCntID As Long)
    '        Try
    '            Dim p As New Generic.List(Of PersistAttribute)
    '            Dim Enumerator As IDictionaryEnumerator = PersistTable.GetEnumerator()

    '            While Enumerator.MoveNext()
    '                p.Add(New PersistAttribute(Enumerator.Key.ToString, Enumerator.Value))
    '            End While


    '            'Serialize object to a text file.
    '            Dim objStreamWriter As New StreamWriter(CCommon.GetDocumentPhysicalPath() & PageName & "_" & UserCntID.ToString & ".xml")
    '            Dim x As New XmlSerializer(p.GetType)
    '            x.Serialize(objStreamWriter, p)
    '            objStreamWriter.Close()
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Sub



    '    Shared Function Load(ByVal PageName As String, ByVal UserCntID As Long) As Hashtable
    '        Try
    '            Dim objStreamReader As New StreamReader(CCommon.GetDocumentPhysicalPath() & PageName & "_" & UserCntID & ".xml")
    '            Dim p As New Generic.List(Of PersistAttribute)
    '            Dim x As New XmlSerializer(p.GetType)
    '            p = CType(x.Deserialize(objStreamReader), Generic.List(Of PersistAttribute))
    '            objStreamReader.Close()
    '            Dim PersistTable As New Hashtable
    '            Dim Enumerator As Generic.IEnumerator(Of PersistAttribute) = p.GetEnumerator()
    '            While Enumerator.MoveNext()
    '                PersistTable.Add(Enumerator.Current.Key, Enumerator.Current.Value)
    '            End While
    '            Return PersistTable
    '        Catch ex As Exception
    '            Throw ex
    '        End Try
    '    End Function

    'End Class

End Namespace