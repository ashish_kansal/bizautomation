'**********************************************************************************
' <CSearch.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************

Option Explicit On 
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Common

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CCommon
    ' 
    ' Description  : This is the business logic classe for Common Stuff
    '**********************************************************************************
    Public Class CSearch
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        'Defining private variables
        ''Private DomainId As Long = 0
        Private _PublicPrivate As Integer = 0
        Private _FirstName As String = String.Empty
        Private _LastName As String = String.Empty
        Private _CompanyName As String = String.Empty
        Private _DivisionName As String = String.Empty
        Private _Country As String = String.Empty
        Private _ContactType As Integer = 0
        Private _Credit As Integer = 0
        Private _CompanyType As Integer = 0
        Private _Rating As Integer = 0
        Private _Industry As Integer = 0
        Private _CloseProb As Integer = 0
        Private _OpptAmount As Decimal = 0
        Private _Stage As Integer = 0
        Private _Group As Integer = 0
        Private _Profile As String = String.Empty
        Private _TerritoryID As Integer = 0
        Private _AOIList As String = String.Empty
        Private _SortChar As String = String.Empty
        Private _UserID As Integer = 0
        Private _UserTerID As Integer = 0
        Private _UserRights As Integer = 0
        Private _CustomerID As Integer = 0
        Private _State As String = String.Empty
        Private _City As String = String.Empty
        Private _LeadsID As Integer = 0
        Private _ProspectsID As Integer = 0
        Private _AccountsID As Integer = 0
        Private _StartDate As Long = 0
        Private _EndDate As Long = 0
        Private _CampaignID As Long = 0
        Private _SearchClause2 As String = String.Empty
        Private _SearchClause1 As String = String.Empty
        Private _Operator As Integer = 0
        Private _CFLDVAL1 As String = String.Empty
        Private _CFLDVAL2 As String = String.Empty
        Private _CFLDVAL3 As String = String.Empty
        Private _CFLDVAL4 As String = String.Empty
        Private _CFLD1 As String = String.Empty
        Private _CFLD2 As String = String.Empty
        Private _CFLD3 As String = String.Empty
        Private _CFLD4 As String = String.Empty
        Private _Gender As String = String.Empty
        Private _AgeFrom As Integer = 0
        Private _AgeTo As Integer = 0
        Private _PostalCodeFrom As String = String.Empty
        Private _PostalCodeTo As String = String.Empty
        Private _SicCode As String = String.Empty
        Private _Status As Long = 0
        'Defining property
        Public Property SicCode() As String
            Get
                Return _SicCode
            End Get
            Set(ByVal Value As String)
                _SicCode = Value
            End Set
        End Property
        Public Property Status() As Long
            Get
                Return _Status
            End Get
            Set(ByVal Value As Long)
                _Status = Value
            End Set
        End Property
        Public Property PostalCodeFrom() As String
            Get
                Return _PostalCodeFrom
            End Get
            Set(ByVal Value As String)
                _PostalCodeFrom = Value
            End Set
        End Property
        Public Property PostalCodeTo() As String
            Get
                Return _PostalCodeTo
            End Get
            Set(ByVal Value As String)
                _PostalCodeTo = Value
            End Set
        End Property
        Public Property Gender() As String
            Get
                Return _Gender
            End Get
            Set(ByVal Value As String)
                _Gender = Value
            End Set
        End Property
        Public Property AgeFrom() As Integer
            Get
                Return _AgeFrom
            End Get
            Set(ByVal Value As Integer)
                _AgeFrom = Value
            End Set
        End Property
        Public Property AgeTo() As Integer
            Get
                Return _AgeTo
            End Get
            Set(ByVal Value As Integer)
                _AgeTo = Value
            End Set
        End Property

        Public Property CFLDVAL1() As String
            Get
                Return _CFLDVAL1
            End Get
            Set(ByVal Value As String)
                _CFLDVAL1 = Value
            End Set
        End Property
        Public Property CFLDVAL2() As String
            Get
                Return _CFLDVAL2
            End Get
            Set(ByVal Value As String)
                _CFLDVAL2 = Value
            End Set
        End Property
        Public Property CFLDVAL3() As String
            Get
                Return _CFLDVAL3
            End Get
            Set(ByVal Value As String)
                _CFLDVAL3 = Value
            End Set
        End Property
        Public Property CFLDVAL4() As String
            Get
                Return _CFLDVAL4
            End Get
            Set(ByVal Value As String)
                _CFLDVAL4 = Value
            End Set
        End Property
        Public Property CFLD1() As String
            Get
                Return _CFLD1
            End Get
            Set(ByVal Value As String)
                _CFLD1 = Value
            End Set
        End Property
        Public Property CFLD2() As String
            Get
                Return _CFLD2
            End Get
            Set(ByVal Value As String)
                _CFLD2 = Value
            End Set
        End Property
        Public Property CFLD3() As String
            Get
                Return _CFLD3
            End Get
            Set(ByVal Value As String)
                _CFLD3 = Value
            End Set
        End Property
        Public Property CFLD4() As String
            Get
                Return _CFLD4
            End Get
            Set(ByVal Value As String)
                _CFLD4 = Value
            End Set
        End Property
        Public Property SearchClause1() As String
            Get
                Return _SearchClause1
            End Get
            Set(ByVal Value As String)
                _SearchClause1 = Value
            End Set
        End Property
        Public Property SearchClause2() As String
            Get
                Return _SearchClause2
            End Get
            Set(ByVal Value As String)
                _SearchClause2 = Value
            End Set
        End Property
        Public Property [Operator]() As Integer
            Get
                Return _Operator
            End Get
            Set(ByVal Value As Integer)
                _Operator = Value
            End Set
        End Property
        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property
        Public Property PublicPrivate() As Integer
            Get
                Return _PublicPrivate
            End Get
            Set(ByVal Value As Integer)
                _PublicPrivate = Value
            End Set
        End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property DivisionName() As String
            Get
                Return _DivisionName
            End Get
            Set(ByVal Value As String)
                _DivisionName = Value
            End Set
        End Property
        Public Property CompanyName() As String
            Get
                Return _CompanyName
            End Get
            Set(ByVal Value As String)
                _CompanyName = Value
            End Set
        End Property
        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property
        Public Property Credit() As Integer
            Get
                Return _Credit
            End Get
            Set(ByVal Value As Integer)
                _Credit = Value
            End Set
        End Property
        Public Property ContactType() As Integer
            Get
                Return _ContactType
            End Get
            Set(ByVal Value As Integer)
                _ContactType = Value
            End Set
        End Property
        Public Property Country() As String
            Get
                Return _Country
            End Get
            Set(ByVal Value As String)
                _Country = Value
            End Set
        End Property
        Public Property Industry() As Integer
            Get
                Return _Industry
            End Get
            Set(ByVal Value As Integer)
                _Industry = Value
            End Set
        End Property
        Public Property Rating() As Integer
            Get
                Return _Rating
            End Get
            Set(ByVal Value As Integer)
                _Rating = Value
            End Set
        End Property
        Public Property CompanyType() As Integer
            Get
                Return _CompanyType
            End Get
            Set(ByVal Value As Integer)
                _CompanyType = Value
            End Set
        End Property
        Public Property Stage() As Integer
            Get
                Return _Stage
            End Get
            Set(ByVal Value As Integer)
                _Stage = Value
            End Set
        End Property
        Public Property OpptAmount() As Decimal
            Get
                Return _OpptAmount
            End Get
            Set(ByVal Value As Decimal)
                _OpptAmount = Value
            End Set
        End Property
        Public Property CloseProb() As Integer
            Get
                Return _CloseProb
            End Get
            Set(ByVal Value As Integer)
                _CloseProb = Value
            End Set
        End Property
        Public Property TerritoryID() As Integer
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Integer)
                _TerritoryID = Value
            End Set
        End Property
        Public Property Profile() As String
            Get
                Return _Profile
            End Get
            Set(ByVal Value As String)
                _Profile = Value
            End Set
        End Property
        Public Property Group() As Integer
            Get
                Return _Group
            End Get
            Set(ByVal Value As Integer)
                _Group = Value
            End Set
        End Property
        Public Property CampaignID() As Long
            Get
                Return _CampaignID
            End Get
            Set(ByVal Value As Long)
                _CampaignID = Value
            End Set
        End Property
        Public Property EndDate() As Long
            Get
                Return _EndDate
            End Get
            Set(ByVal Value As Long)
                _EndDate = Value
            End Set
        End Property
        Public Property StartDate() As Long
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As Long)
                _StartDate = Value
            End Set
        End Property
        Public Property AccountsID() As Integer
            Get
                Return _AccountsID
            End Get
            Set(ByVal Value As Integer)
                _AccountsID = Value
            End Set
        End Property
        Public Property ProspectsID() As Integer
            Get
                Return _ProspectsID
            End Get
            Set(ByVal Value As Integer)
                _ProspectsID = Value
            End Set
        End Property
        Public Property LeadsID() As Integer
            Get
                Return _LeadsID
            End Get
            Set(ByVal Value As Integer)
                _LeadsID = Value
            End Set
        End Property
        Public Property City() As String
            Get
                Return _City
            End Get
            Set(ByVal Value As String)
                _City = Value
            End Set
        End Property
        Public Property State() As String
            Get
                Return _State
            End Get
            Set(ByVal Value As String)
                _State = Value
            End Set
        End Property
        Public Property CustomerID() As Integer
            Get
                Return _CustomerID
            End Get
            Set(ByVal Value As Integer)
                _CustomerID = Value
            End Set
        End Property
        Public Property UserRights() As Integer
            Get
                Return _UserRights
            End Get
            Set(ByVal Value As Integer)
                _UserRights = Value
            End Set
        End Property
        Public Property UserTerID() As Integer
            Get
                Return _UserTerID
            End Get
            Set(ByVal Value As Integer)
                _UserTerID = Value
            End Set
        End Property
        Public Property UserID() As Integer
            Get
                Return _UserID
            End Get
            Set(ByVal Value As Integer)
                _UserID = Value
            End Set
        End Property
        Public Property SortChar() As String
            Get
                Return _SortChar
            End Get
            Set(ByVal Value As String)
                _SortChar = Value
            End Set
        End Property
        Public Property AOIList() As String
            Get
                Return _AOIList
            End Get
            Set(ByVal Value As String)
                _AOIList = Value
            End Set
        End Property
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  AdvancedSearch()
        '     Purpose					 :  This function will return  search records.  
        '     Example                    :  
        '     Parameters                 :                     
        '                                 
        '     Outputs					 :  It return search records.  
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  23/12/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function AdvancedSearch() As DataSet
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(38) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@DomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@PublicPrivate", NpgsqlTypes.NpgsqlDbType.Smallint, 2)
                arParms(1).Value = _PublicPrivate

                arParms(2) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _FirstName

                arParms(3) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _LastName

                arParms(4) = New Npgsql.NpgsqlParameter("@CompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _CompanyName

                arParms(5) = New Npgsql.NpgsqlParameter("@DivisionName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _DivisionName

                arParms(6) = New Npgsql.NpgsqlParameter("@Country", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _Country

                arParms(7) = New Npgsql.NpgsqlParameter("@ContactType", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(7).Value = _ContactType

                arParms(8) = New Npgsql.NpgsqlParameter("@Credit", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(8).Value = _Credit

                arParms(9) = New Npgsql.NpgsqlParameter("@CompanyType", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(9).Value = _CompanyType

                arParms(10) = New Npgsql.NpgsqlParameter("@Rating", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(10).Value = _Rating

                arParms(11) = New Npgsql.NpgsqlParameter("@Industry", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(11).Value = _Industry

                arParms(12) = New Npgsql.NpgsqlParameter("@CloseProb", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(12).Value = _CloseProb

                arParms(13) = New Npgsql.NpgsqlParameter("@OpptAmount", NpgsqlTypes.NpgsqlDbType.Numeric, 9)
                arParms(13).Value = _OpptAmount

                arParms(14) = New Npgsql.NpgsqlParameter("@Stage", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(14).Value = _Stage

                arParms(15) = New Npgsql.NpgsqlParameter("@Group", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(15).Value = _Group

                arParms(16) = New Npgsql.NpgsqlParameter("@Profile", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(16).Value = _Profile

                arParms(17) = New Npgsql.NpgsqlParameter("@TerritoryID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(17).Value = _TerritoryID

                arParms(18) = New Npgsql.NpgsqlParameter("@AOIList", NpgsqlTypes.NpgsqlDbType.VarChar, 250)
                arParms(18).Value = _AOIList

                arParms(19) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(19).Value = _SortChar

                arParms(20) = New Npgsql.NpgsqlParameter("@UserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(20).Value = _UserID

                arParms(21) = New Npgsql.NpgsqlParameter("@UserTerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(21).Value = _UserTerID

                arParms(22) = New Npgsql.NpgsqlParameter("@UserRights", NpgsqlTypes.NpgsqlDbType.Smallint, 4)
                arParms(22).Value = _UserRights

                arParms(23) = New Npgsql.NpgsqlParameter("@CustomerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(23).Value = _CustomerID

                arParms(24) = New Npgsql.NpgsqlParameter("@State", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(24).Value = _State

                arParms(25) = New Npgsql.NpgsqlParameter("@City", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(25).Value = _City

                arParms(26) = New Npgsql.NpgsqlParameter("@LeadsID", NpgsqlTypes.NpgsqlDbType.Smallint, 4)
                arParms(26).Value = _LeadsID

                arParms(27) = New Npgsql.NpgsqlParameter("@ProspectsID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(27).Value = _ProspectsID

                arParms(28) = New Npgsql.NpgsqlParameter("@AccountsID", NpgsqlTypes.NpgsqlDbType.Smallint, 4)
                arParms(28).Value = _AccountsID

                arParms(29) = New Npgsql.NpgsqlParameter("@StartDate", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(29).Value = _StartDate

                arParms(30) = New Npgsql.NpgsqlParameter("@EndDate", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(30).Value = _EndDate

                arParms(31) = New Npgsql.NpgsqlParameter("@CampaignID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(31).Value = _CampaignID

                arParms(32) = New Npgsql.NpgsqlParameter("@Gender", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(32).Value = _Gender

                arParms(33) = New Npgsql.NpgsqlParameter("@AgeFrom", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(33).Value = _AgeFrom

                arParms(34) = New Npgsql.NpgsqlParameter("@AgeTo", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(34).Value = _AgeTo

                arParms(35) = New Npgsql.NpgsqlParameter("@PostalCodeFrom", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(35).Value = _PostalCodeFrom

                arParms(36) = New Npgsql.NpgsqlParameter("@PostalCodeTo", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(36).Value = _PostalCodeTo

                arParms(37) = New Npgsql.NpgsqlParameter("@SicCode", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(37).Value = _SicCode

                arParms(38) = New Npgsql.NpgsqlParameter("@Status", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(38).Value = _Status




                Dim ds As DataSet
                'Call ExecuteReader static method of SqlDal class that returns an Object. Then cast the return value to string.
                ' We pass in database connection string, command type, stored procedure name and productID Npgsql.NpgsqlParameter
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSearchResultsAdvanced", arParms)

                'total no of records 
                Return ds 'return dataset 
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  SimpleSearch()
        '     Purpose					 :  This function will return simple  search records.  
        '     Example                    :  
        '     Parameters                 :                     
        '                                 
        '     Outputs					 :  It return search records.  
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  23/12/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function SimpleSearch() As DataSet
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@DomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@SearchClause1", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _SearchClause1

                arParms(2) = New Npgsql.NpgsqlParameter("@SearchClause2", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _SearchClause2

                arParms(3) = New Npgsql.NpgsqlParameter("@Operator", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _Operator

                arParms(4) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(4).Value = _SortChar

                arParms(5) = New Npgsql.NpgsqlParameter("@UserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _UserID

                arParms(6) = New Npgsql.NpgsqlParameter("@TerritoryID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _TerritoryID

                arParms(7) = New Npgsql.NpgsqlParameter("@UserRights", NpgsqlTypes.NpgsqlDbType.Smallint, 4)
                arParms(7).Value = _UserRights

                Dim ds As DataSet
                'Call ExecuteReader static method of SqlDal class that returns an Object. Then cast the return value to string.
                ' We pass in database connection string, command type, stored procedure name and productID Npgsql.NpgsqlParameter
                ds = SqlDAL.ExecuteDataset(connString, "usp_USearchResults", arParms)

                'total no of records 
                Return ds 'return dataset 
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  CustomFieldSearch()
        '     Purpose					 :  This function will return simple  search records.  
        '     Example                    :  
        '     Parameters                 :                     
        '                                 
        '     Outputs					 :  It return search records.  
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  23/12/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function CustomFieldSearch() As DataSet
            Try



                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@CFLD1", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(0).Value = _CFLD1

                arParms(1) = New Npgsql.NpgsqlParameter("@CFLD2", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(1).Value = _CFLD2

                arParms(2) = New Npgsql.NpgsqlParameter("@CFLD3", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(2).Value = _CFLD3

                arParms(3) = New Npgsql.NpgsqlParameter("@CFLD4", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(3).Value = _CFLD4

                arParms(4) = New Npgsql.NpgsqlParameter("@CFLDVAL1", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(4).Value = _CFLDVAL1

                arParms(5) = New Npgsql.NpgsqlParameter("@CFLDVAL2", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(5).Value = _CFLDVAL2

                arParms(6) = New Npgsql.NpgsqlParameter("@CFLDVAL3", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(6).Value = _CFLDVAL3

                arParms(7) = New Npgsql.NpgsqlParameter("@CFLDVAL4", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(7).Value = _CFLDVAL4

                arParms(8) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(8).Value = _SortChar

                Dim ds As DataSet
                'Call ExecuteReader static method of SqlDal class that returns an Object. Then cast the return value to string.
                ' We pass in database connection string, command type, stored procedure name and productID Npgsql.NpgsqlParameter
                ds = SqlDAL.ExecuteDataset(connString, "cfldsearch", arParms)

                'total no of records 
                Return ds 'return dataset 
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  CustomFieldSearchCase()
        '     Purpose					 :  This function will return simple  search records.  
        '     Example                    :  
        '     Parameters                 :                     
        '                                 
        '     Outputs					 :  It return search records.  
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  27/12/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function CustomFieldSearchCase() As DataSet
            Try



                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@CFLD1", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(0).Value = _CFLD1

                arParms(1) = New Npgsql.NpgsqlParameter("@CFLD2", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(1).Value = _CFLD2

                arParms(2) = New Npgsql.NpgsqlParameter("@CFLD3", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(2).Value = _CFLD3

                arParms(3) = New Npgsql.NpgsqlParameter("@CFLD4", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(3).Value = _CFLD4

                arParms(4) = New Npgsql.NpgsqlParameter("@CFLDVAL1", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(4).Value = _CFLDVAL1

                arParms(5) = New Npgsql.NpgsqlParameter("@CFLDVAL2", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(5).Value = _CFLDVAL2

                arParms(6) = New Npgsql.NpgsqlParameter("@CFLDVAL3", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(6).Value = _CFLDVAL3

                arParms(7) = New Npgsql.NpgsqlParameter("@CFLDVAL4", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(7).Value = _CFLDVAL4

                Dim ds As DataSet
                'Call ExecuteReader static method of SqlDal class that returns an Object. Then cast the return value to string.
                ' We pass in database connection string, command type, stored procedure name and productID Npgsql.NpgsqlParameter
                ds = SqlDAL.ExecuteDataset(connString, "cfldsearchcase", arParms)

                'total no of records 
                Return ds 'return dataset 
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  CustomFieldSearchContact()
        '     Purpose					 :  This function will return simple  search records.  
        '     Example                    :  
        '     Parameters                 :                     
        '                                 
        '     Outputs					 :  It return search records.  
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  27/12/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function CustomFieldSearchContact() As DataSet
            Try



                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@CFLD1", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(0).Value = _CFLD1

                arParms(1) = New Npgsql.NpgsqlParameter("@CFLD2", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(1).Value = _CFLD2

                arParms(2) = New Npgsql.NpgsqlParameter("@CFLD3", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(2).Value = _CFLD3

                arParms(3) = New Npgsql.NpgsqlParameter("@CFLD4", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(3).Value = _CFLD4

                arParms(4) = New Npgsql.NpgsqlParameter("@CFLDVAL1", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(4).Value = _CFLDVAL1

                arParms(5) = New Npgsql.NpgsqlParameter("@CFLDVAL2", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(5).Value = _CFLDVAL2

                arParms(6) = New Npgsql.NpgsqlParameter("@CFLDVAL3", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(6).Value = _CFLDVAL3

                arParms(7) = New Npgsql.NpgsqlParameter("@CFLDVAL4", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(7).Value = _CFLDVAL4

                Dim ds As DataSet
                'Call ExecuteReader static method of SqlDal class that returns an Object. Then cast the return value to string.
                ' We pass in database connection string, command type, stored procedure name and productID Npgsql.NpgsqlParameter
                ds = SqlDAL.ExecuteDataset(connString, "cfldsearchcontact", arParms)

                'total no of records 
                Return ds 'return dataset 
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  CustomFieldSearchOpportunity()
        '     Purpose					 :  This function will return simple  search records.  
        '     Example                    :  
        '     Parameters                 :                     
        '                                 
        '     Outputs					 :  It return search records.  
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  27/12/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function CustomFieldSearchOpportunity() As DataSet
            Try



                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@CFLD1", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(0).Value = _CFLD1

                arParms(1) = New Npgsql.NpgsqlParameter("@CFLD2", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(1).Value = _CFLD2

                arParms(2) = New Npgsql.NpgsqlParameter("@CFLD3", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(2).Value = _CFLD3

                arParms(3) = New Npgsql.NpgsqlParameter("@CFLD4", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(3).Value = _CFLD4

                arParms(4) = New Npgsql.NpgsqlParameter("@CFLDVAL1", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(4).Value = _CFLDVAL1

                arParms(5) = New Npgsql.NpgsqlParameter("@CFLDVAL2", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(5).Value = _CFLDVAL2

                arParms(6) = New Npgsql.NpgsqlParameter("@CFLDVAL3", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(6).Value = _CFLDVAL3

                arParms(7) = New Npgsql.NpgsqlParameter("@CFLDVAL4", NpgsqlTypes.NpgsqlDbType.VarChar, 25)
                arParms(7).Value = _CFLDVAL4

                Dim ds As DataSet
                'Call ExecuteReader static method of SqlDal class that returns an Object. Then cast the return value to string.
                ' We pass in database connection string, command type, stored procedure name and productID Npgsql.NpgsqlParameter
                ds = SqlDAL.ExecuteDataset(connString, "cfldsearchopp", arParms)

                'total no of records 
                Return ds 'return dataset 
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        '***************************************************************************************************************************
        '     Function / Subroutine  Name:  CustomFieldSearchOpportunity()
        '     Purpose					 :  This function will return simple  search records.  
        '     Example                    :  
        '     Parameters                 :                     
        '                                 
        '     Outputs					 :  It return search records.  
        '						
        '     Author Name				 :  Ajeet Singh
        '     Date Written				 :  27/12/2004
        '     Cross References 			 :  List of functions being called by this function.
        '     Modification History
        '     -------------------------------------------------------------------------------------------------------------------------------------------
        '        Modified Date         Modified By                          Purpose Of Modification
        '     -------------------------------------------------------------------------------------------------------------------------------------------        
        '        mm/dd/yyyy         Modifierís Name           	            Purpose Of Modification 
        '***************************************************************************************************************************
        Public Function SearchAOI() As DataSet
            Try



                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@AOIList", NpgsqlTypes.NpgsqlDbType.VarChar, 250)
                arParms(0).Value = _AOIList

                Dim ds As DataSet
                'Call ExecuteReader static method of SqlDal class that returns an Object. Then cast the return value to string.
                ' We pass in database connection string, command type, stored procedure name and productID Npgsql.NpgsqlParameter
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSearchResultsAOI", arParms)

                'total no of records 
                Return ds 'return dataset 
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Private _Mode As Short
        Public Property byteMode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal value As Short)
                _Mode = value
            End Set
        End Property

        Private _SiteID As Long
        Public Property SiteID() As Long
            Get
                Return _SiteID
            End Get
            Set(ByVal value As Long)
                _SiteID = value
            End Set
        End Property

        Private _SchemaOnly As Boolean
        Public Property SchemaOnly() As Boolean
            Get
                Return _SchemaOnly
            End Get
            Set(ByVal value As Boolean)
                _SchemaOnly = value
            End Set
        End Property

        Private _AddUpdateMode As Short
        ''' <summary>
        ''' 1=Add Mode
        ''' 2=Update Mode
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property AddUpdateMode() As Short
            Get
                Return _AddUpdateMode
            End Get
            Set(ByVal value As Short)
                _AddUpdateMode = value
            End Set
        End Property

        Private _DeleteAll As Boolean
        Public Property DeleteAll() As Boolean
            Get
                Return _DeleteAll
            End Get
            Set(ByVal value As Boolean)
                _DeleteAll = value
            End Set
        End Property
        Public Function GetDataForIndexing() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _Mode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _SiteID

                arParms(3) = New Npgsql.NpgsqlParameter("@bitSchemaOnly", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(3).Value = _SchemaOnly

                arParms(4) = New Npgsql.NpgsqlParameter("@tintAddUpdateMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _AddUpdateMode

                arParms(5) = New Npgsql.NpgsqlParameter("@bitDeleteAll", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _DeleteAll

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetDataForIndexing", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetDataForIndexRebuild(ByVal currentPage As Integer) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCurrentPage", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = currentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetDataForIndexRebuild", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private _StrItems As String
        Public Property StrItems() As String
            Get
                Return _StrItems
            End Get
            Set(ByVal value As String)
                _StrItems = value
            End Set
        End Property

        Public Function GetGoogleMap(ByVal miles As Int32, ByVal zipcode As String) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@xmlStr", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _StrItems

                arParms(2) = New Npgsql.NpgsqlParameter("@miles", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = miles

                arParms(3) = New Npgsql.NpgsqlParameter("@zipcode", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = zipcode

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetGoogleMap", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace

