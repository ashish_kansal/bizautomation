Imports System.Configuration
Namespace BACRMBUSSLOGIC.BussinessLogic
    Public Class GetConnection
        Inherits BACRM.BusinessLogic.CBusinessBase
        Public Shared Function GetConnectionString() As String
            GetConnectionString = ConfigurationManager.AppSettings("ConnectionString")
        End Function
    End Class
End Namespace