﻿Imports System.Collections.Generic
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Common
    Public Class TrackingVisitorsDTL
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Methods"

        Public Function GetWebsitePagesForWorkflow() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_TrackingVisitorsDTL_GetWebsitePagesForWorkflow", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

    End Class
End Namespace

