﻿Imports System.Text
Imports System.IO
Imports System.Web.Mail
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Leads
Imports System.Data
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System
Imports System.Configuration
Imports System.Web.UI
Imports BACRM.BusinessLogic.Common

Public Module GenerateGenericFormControls

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Represents the DomainID.
    ''' </summary>
    ''' <remarks>
    '''     This holds the domain id.
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/09/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private _DomainID As Long
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the Domain id.
    ''' </summary>
    ''' <value>Returns the domain id as long.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the Domain id. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/09/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property DomainID() As Long
        Get
            Return _DomainID
        End Get
        Set(ByVal Value As Long)
            _DomainID = Value
        End Set
    End Property
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Represents the Authentication Group ID.
    ''' </summary>
    ''' <remarks>
    '''     This holds the user Group id.
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/30/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private _AuthGroupId As Long
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the User's authenticaiton group id.
    ''' </summary>
    ''' <value>Returns the user authentication group id as long.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the User authentiation group id. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/30/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property AuthGroupId() As Long
        Get
            Return _AuthGroupId
        End Get
        Set(ByVal Value As Long)
            _AuthGroupId = Value
        End Set
    End Property


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Represents the form id.
    ''' </summary>
    ''' <remarks>
    '''     This holds the form id.
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/09/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private _FormID As Long
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the Domain id.
    ''' </summary>
    ''' <value>Returns the domain id as long.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the Domain id. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/09/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property FormID() As Long
        Get
            Return _FormID
        End Get
        Set(ByVal Value As Long)
            _FormID = Value
        End Set
    End Property
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the sXMLFilePath whihc is path to the xml file.
    ''' </summary>
    ''' <value>Returns the sXMLFilePath as String.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the sXMLFilePath. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Private _sXMLFilePath As String
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the sXMLFilePath whihc is path to the xml file.
    ''' </summary>
    ''' <value>Returns the sXMLFilePath as String.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the sXMLFilePath. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property sXMLFilePath() As String
        Get
            Return _sXMLFilePath
        End Get
        Set(ByVal Value As String)
            _sXMLFilePath = Value
        End Set
    End Property
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Represents the indicator for AOI field.
    ''' </summary>
    ''' <remarks>
    '''     This holds the flag to the AOI field.
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/09/2005	Created
    ''' </history>
    Private _boolAOIField As Int16
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This Sets and Gets the boolAOIField which indicates if the field is a AOI or not.
    ''' </summary>
    ''' <value>Returns the boolAOIField as Boolean.</value>
    ''' <remarks>
    '''     This property is used to Set and Get the boolAOIField. 
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	07/20/2005	Created
    ''' </history>
    ''' -----------------------------------------------------------------------------
    Public Property boolAOIField() As Int16
        Get
            Return _boolAOIField
        End Get
        Set(ByVal Value As Int16)
            _boolAOIField = Value
        End Set
    End Property

    Private _SubFormId As Int16
    Public Property SubFormId() As Int16
        Get
            Return _SubFormId
        End Get
        Set(value As Int16)
            _SubFormId = value
        End Set
    End Property

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This subroutine finds the controls on the form by parsing the xml file which is created from BizForm Wizard
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Function getFormControlConfig() As DataTable
        Try


            Dim dtFormConfig As DataTable                                                       'declare the datatable which will contain the form config
            Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
            objConfigWizard.FormID = FormID                                                     'Set the Form ids
            objConfigWizard.AuthGroupId = AuthGroupId                                           'sets teh user's authenticaiton group id
            objConfigWizard.DomainID = DomainID                                                 'sets the user's domain id
            objConfigWizard.SubFormId = SubFormId
            dtFormConfig = objConfigWizard.getDynamicFormConfig(sXMLFilePath)                   'calls to get the form config
            Return dtFormConfig                                                                 'return the datatable
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This subroutine creates the controls on the form 
    ''' </summary>
    ''' <param name="dtFormConfig">The form config as stored in the BizForm Wizard.</param>
    ''' <value>Returns the controls in a table.</value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Function createFormControls(ByVal dtFormConfig As DataTable, Optional ByVal JavaScriptValidator As Boolean = False) As HtmlTable
        Try
            Dim tblDynamicFormControls As New HtmlTable
            'tblDynamicFormControls.BorderColor = Color.Black                                           'Border Color is Black
            'tblDynamicFormControls.BorderWidth = Unit.Pixel(1)                                         'Border Width is 1 pixel
            tblDynamicFormControls.Width = "100%"                                                       'Width of the table is 100%
            tblDynamicFormControls.ID = "tblFormLeadBoxNonAOITable"                                     'give a name to the table
            tblDynamicFormControls.Border = 0                                                           'There are no GridLines
            tblDynamicFormControls.EnableViewState = True                                               'Enable view state for the table
            tblDynamicFormControls.CellPadding = 0                                                      'Set the cell padding
            tblDynamicFormControls.CellSpacing = 0                                                      'Set the cell spacing
            'Dim boolCountryListBoxPresent As Boolean = False                                            'A flag which indicates if a country drop down is present or not
            If dtFormConfig.Rows.Count > 0 Then                                                         'determine that the XML file exists and there is atleast one field
                Dim iMaxRows As Integer                                                                 'declare a variable which will contain the max nos of rows
                dtFormConfig.Select("boolAOIField = " & boolAOIField)                                   'filter out the records as per AOI/ NOn AOI flag
                iMaxRows = CInt(dtFormConfig.Compute("Max(intRowNumVirtual)", ""))                      'Get the max value of the row number
                Dim iMaxCols As Integer                                                                 'declare a variable which will contain the max nos of rows
                iMaxCols = CInt(dtFormConfig.Compute("Max(intColumnNum)", ""))                          'Get the max value of the Columns
                Dim dvFormConfig As DataView                                                            'declare a dataview object
                Dim iRowIndex, iColumnIndex As Integer                                                  'declare a row index variable
                Dim iColNum, iRowNum As Integer                                                         'declare a column index variable
                Dim tblRow As HtmlTableRow                                                                  'declare a table row object
                Dim litLableText As Literal                                                             'declare a literal
                Dim tblCell, tblControlCell, tblLiteralCell As HtmlTableCell                                'declare a table cell object
                For iRowIndex = 0 To iMaxRows                                                           'loop through the max rows to create rows for the table
                    iRowNum = iRowIndex                                                                 'find the row number
                    tblRow = New HtmlTableRow                                                               'instantiate a new table row
                    tblRow.Height = 23                                                    'set the height attribute
                    For iColumnIndex = 0 To iMaxCols * 2                                                'loop through the cell in the table row
                        dvFormConfig = New DataView(dtFormConfig)                                       'store the dataview as the dataview of the form config
                        tblControlCell = New HtmlTableCell                                                  'create a new table literal cell
                        tblControlCell.Attributes.Add("class", "ControlCell")                                           'set the class attribute for teh tablecell
                        tblLiteralCell = New HtmlTableCell                                                  'create a new table control cell
                        tblLiteralCell.Attributes.Add("class", "normal1")                                          'set the class attribute for teh tablecell
                        'tblLiteralCell.Align = "right" 'align will be controled through css class                        'align the labels to the right
                        If (iColumnIndex Mod 2) = 0 Then                                                'Odd cells
                            iColNum = (tblRow.Cells.Count / 2) + 1                                      'the column number is dynamic
                            dvFormConfig.RowFilter = "intRowNum = " & iRowNum & " and intColumnNum = " & iColNum & " and boolAOIField = " & _boolAOIField 'set the row filter
                            If dvFormConfig.Count > 0 Then
                                tblLiteralCell.InnerHtml = CStr(dvFormConfig(0).Item("vcNewFormFieldName")) & IIf(dvFormConfig(0).Item("boolRequired") = 1, " <span class=normal4>*</span>", "") & "&nbsp;" 'get the text as lable

                                tblControlCell.Controls.Add(getDynamicControlAndData(Replace(Replace(Replace(dvFormConfig(0).Item("numFormFieldId"), "R", ""), "C", ""), "D", ""), dvFormConfig(0).Item("vcDbColumnName"), dvFormConfig(0).Item("vcListItemType"), dvFormConfig(0).Item("vcAssociatedControlType"), dvFormConfig(0).Item("numListID"), iRowIndex, iColNum)) 'add the dynamic control to the table cell

                                If JavaScriptValidator = False Then
                                    If dvFormConfig(0).Item("boolRequired") = 1 Then                        'if mandatory then add an required field validator
                                        tblControlCell.Controls.Add(getDynamicValidationsControlsRequired(dvFormConfig(0).Item("vcNewFormFieldName"), dvFormConfig(0).Item("vcDbColumnName"), dvFormConfig(0).Item("boolRequired"))) 'Call to add Validation Controls
                                    End If
                                    'If dvFormConfig(0).Item("vcDbColumnName").IndexOf("Country") > -1 Then  'If its a drop down of country
                                    '    boolCountryListBoxPresent = True                                    'Set the flag
                                    'End If
                                    tblControlCell.Controls.Add(getDynamicValidationsControlsDataType(dvFormConfig(0).Item("vcNewFormFieldName"), dvFormConfig(0).Item("vcDbColumnName"), dvFormConfig(0).Item("vcFieldDataType"))) 'Call to add Validation Controls
                                End If
                            End If
                        End If
                        tblRow.Cells.Add(tblLiteralCell)                                                'add the literal cell to the row
                        tblRow.Cells.Add(tblControlCell)                                                'add the control cell to the row
                        iColumnIndex += 1                                                               'increment the column number as two cells are added at a time
                    Next
                    tblDynamicFormControls.Rows.Add(tblRow)                                             'add the new table row to the existing table
                Next
                'Commented by chintan, its being handled in postback event occured by country dropdown
                'If boolCountryListBoxPresent Then                                                       'If a country drop down exists
                '    tblRow = New HtmlTableRow                                                               'instantiate a new table row
                '    tblRow.Height = 1                                                       'set the height attribute
                '    tblControlCell = New HtmlTableCell                                                      'create a new table literal cell
                '    tblControlCell.ColSpan = iMaxCols                                                'Set the column span
                '    tblControlCell.Controls.Add(New LiteralControl("<script language='javascript' src=""" & ConfigurationManager.AppSettings("StateAndCountryList") & "StatesInCountries_" & DomainID & ".js""></script>" & vbCrLf & "<script language='javascript' src=""../javascript/StateAndCountries.js""></script>")) 'Add the script which will filter the States
                '    tblRow.Cells.Add(tblControlCell)                                                    'add the literal cell to the row
                '    tblDynamicFormControls.Rows.Add(tblRow)                                             'add the new table row to the existing table
                'End If
            Else
                Dim tblRow As HtmlTableRow                                                                  'declare a table row onject
                tblRow = New HtmlTableRow                                                                   'instantiate a tablerow object
                Dim tblLiteralCell As HtmlTableCell                                                         'declare a tablecell object
                tblLiteralCell = New HtmlTableCell                                                          'create a new table control cell
                tblLiteralCell.Attributes.Add("class", "normal1")                                                     'set the class attribute for teh tablecell
                Dim litLableText As New LiteralControl("The screen is not created. Please use BizForm Wizard to create this screen.")                                                            'declare a literal
                tblLiteralCell.Controls.Add(litLableText)                                               'Add the literal control to the table cell
                tblRow.Cells.Add(tblLiteralCell)                                                        'Add the table cell to the table row
                tblDynamicFormControls.Rows.Add(tblRow)                                                 'Add the table row to the table
            End If
            tblDynamicFormControls.Align = "Center"
            Return tblDynamicFormControls
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Sub AssignValuesPasswordBox(ByVal objLeads As CLeads, ByVal vcValue As String, ByVal vcColumnName As String)
        Select Case vcColumnName
            Case "vcPassword"
                objLeads.Password = vcValue
            Case "vcPasswordConfirm"
                objLeads.PasswordConfirm = vcValue
        End Select
    End Sub

    Function GenerateValidationScript(ByVal dtFormConfig As DataTable) As String
        Try
            Dim sb As New System.Text.StringBuilder
            Dim ClientID, fieldName, FieldMessage As String

            sb.AppendLine("function validateFormsFields() {")

            For Each dr As DataRow In dtFormConfig.Rows
                fieldName = dr("vcNewFormFieldName").ToString.Replace("'", "\'").Trim()
                ClientID = dr("vcDbColumnName").ToString & "_" & dr("numFormFieldId")
                FieldMessage = "Please enter " & fieldName
                'If dr("boolRequired") = 1 Then ' removing this so by default all fields will be required. 
                'Required Field 

                If dr("vcAssociatedControlType") = "SelectBox" Then
                    sb.AppendLine("if(RequiredFieldDropDown('" & ClientID & "','" & fieldName & "','" & FieldMessage & "')==false) {return false;} ")
                Else
                    sb.AppendLine("if(RequiredField('" & ClientID & "','" & fieldName & "','" & FieldMessage & "')==false) {return false;} ")
                End If

                Select Case dr("vcFieldDataType").ToString()
                    Case "N"                                                                                    'Numeric Data Type
                        sb.AppendLine("if(IsNumeric('" & ClientID & "','" & fieldName & "','" & FieldMessage & "')==false) {return false;} ")
                        Exit Select
                    Case "E"                                                                                    'Email
                        sb.AppendLine("if(CheckMail('" & ClientID & "','" & fieldName & "','" & FieldMessage & "')==false) {return false;} ")
                        Exit Select
                End Select
                'End If
            Next
            sb.AppendLine("return true;}")
            Return sb.ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub AssignValuesTextBox(ByVal objLeads As CLeads, ByVal vcValue As String, ByVal vcColumnName As String)
        Select Case vcColumnName
            Case "txtComments"
                objLeads.Comments = vcValue
        End Select
    End Sub

    Sub AssignValuesEditBox(ByVal objLeads As CLeads, ByVal vcValue As String, ByVal vcColumnName As String)
        Try


            Select Case vcColumnName
                Case "vcFirstName"
                    objLeads.FirstName = vcValue
                Case "vcLastName"
                    objLeads.LastName = vcValue
                Case "vcEmail"
                    objLeads.Email = vcValue
                Case "vcComPhone"
                    objLeads.ComPhone = vcValue
                Case "vcStreet"
                    objLeads.PStreet = vcValue
                Case "vcCity"
                    objLeads.PCity = vcValue
                Case "vcPostalCode"
                    objLeads.PPostalCode = vcValue
                Case "numPhone"
                    objLeads.ContactPhone = vcValue
                Case "numPhoneExtension"
                    objLeads.PhoneExt = vcValue
                Case "vcWebSite"
                    objLeads.WebSite = vcValue
                Case "numCell"
                    objLeads.Cell = vcValue
                Case "numHomePhone"
                    objLeads.HomePhone = vcValue
                Case "vcDivisionName"
                    objLeads.DivisionName = vcValue
                Case "numCompanyIndustry"
                    objLeads.CompanyIndustry = vcValue
                Case "txtComments"
                    objLeads.Comments = vcValue
                Case "vcCompanyName"
                    objLeads.CompanyName = vcValue
                Case "vcFax"
                    objLeads.Fax = vcValue
                Case "vcShipPostCode"
                    objLeads.SPostalCode = vcValue
                Case "vcShipCity"
                    objLeads.SCity = vcValue
                Case "vcShipStreet"
                    objLeads.SStreet = vcValue
                Case "vcShipStreet"
                    objLeads.SStreet = vcValue
                Case "vcComFax"
                    objLeads.ComFax = vcValue
                Case "vcBillStreet"
                    objLeads.Street = vcValue
                Case "vcBillCity"
                    objLeads.City = vcValue
                Case "vcBillPostCode"
                    objLeads.PostalCode = vcValue
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub AssignValuesSelectBox(ByVal objLeads As CLeads, ByVal vcValue As String, ByVal vcColumnName As String)
        Try


            Select Case vcColumnName
                Case "vcCategory"
                    objLeads.Category = CLng(vcValue)
                Case "numState"
                    objLeads.PState = CLng(vcValue)
                Case "numCountry"
                    objLeads.PCountry = CLng(vcValue)
                Case "vcPosition"
                    objLeads.Position = CLng(vcValue)
                Case "numTeam"
                    objLeads.Team = CLng(vcValue)
                Case "numAnnualRevID"
                    objLeads.AnnualRevenue = CLng(vcValue)
                Case "numNoOfEmployeesId"
                    objLeads.NumOfEmp = CLng(vcValue)
                Case "vcHow"
                    objLeads.InfoSource = CLng(vcValue)
                Case "vcProfile"
                    objLeads.Profile = CLng(vcValue)
                Case "numEmpStatus"
                    objLeads.EmpStatus = CLng(vcValue)
                Case "numCompanyIndustry"
                    objLeads.CompanyIndustry = CLng(vcValue)
                Case "numCompanyType"
                    objLeads.CompanyType = CLng(vcValue)
                Case "numCompanyRating"
                    objLeads.CompanyRating = CLng(vcValue)
                Case "numStatusID"
                    objLeads.StatusID = CLng(vcValue)
                Case "vcShipCountry"
                    objLeads.SCountry = CLng(vcValue)
                Case "numTerID"
                    objLeads.TerritoryID = CLng(vcValue)
                Case "vcShipState"
                    objLeads.SState = CLng(vcValue)
                Case "numCampaignID"
                    objLeads.CampaignID = CLng(vcValue)
                Case "numCompanyCredit"
                    objLeads.CompanyCredit = CLng(vcValue)
                Case "vcDepartment"
                    objLeads.Department = CLng(vcValue)
                Case "numFollowUpStatus"
                    objLeads.FollowUpStatus = CLng(vcValue)
                Case "vcBilState"
                    objLeads.State = CLng(vcValue)
                Case "vcBillCountry"
                    objLeads.Country = CLng(vcValue)
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to create the array of form elements
    ''' </summary>
    ''' <value>Returns the string of array elements.</value>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    ''' 
    Public Function getJavascriptArray(ByVal dtFormConfig As DataTable) As String
        Dim sDynamicFieldInfo As New System.Text.StringBuilder                              'This will contain the XML fields
        Dim iRowIndex As Int16 = 0                                                          'Declare an integer to hold the row index
        sDynamicFieldInfo.Append(vbCrLf & "<script language='javascript'>")
        sDynamicFieldInfo.Append(vbCrLf & "var arrFieldConfig = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript
        Dim dtRows As DataRow                                                               'Declare a datarow
        For Each dtRows In dtFormConfig.Rows
            If dtRows.Item("boolAOIField") = 1 Then                                         'Since checkbox lsit is use for displayign AOIs
                sDynamicFieldInfo.Append("arrFieldConfig[" & iRowIndex & "] = new classFormFieldConfig('" & "vcInterested_" & CInt(dtRows.Item("intRowNum")) & "','" & Replace(dtRows.Item("vcNewFormFieldName"), "'", "\'") & "','',''," & dtRows.Item("intRowNum") & "," & dtRows.Item("intColumnNum") & ",'" & dtRows.Item("vcAssociatedControlType") & "'," & dtRows.Item("boolAOIField") & ");" & vbCrLf) 'start creating the array elements, each holding field information
            Else                                                                            'db fieldbname is same as control name for non AIOs
                sDynamicFieldInfo.Append("arrFieldConfig[" & iRowIndex & "] = new classFormFieldConfig('" & dtRows.Item("vcDbColumnName") & "','" & Replace(dtRows.Item("vcNewFormFieldName"), "'", "\'") & "','',''," & dtRows.Item("intRowNum") & "," & dtRows.Item("intColumnNum") & ",'" & dtRows.Item("vcAssociatedControlType") & "'," & dtRows.Item("boolAOIField") & ");" & vbCrLf) 'start creating the array elements, each holding field information
            End If
            iRowIndex += 1                                                                  'increment the Array Index
        Next
        sDynamicFieldInfo.Append("</script>")                                               'end the client side javascript block
        Return sDynamicFieldInfo.ToString()                                                 'Return the array as string
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to return the appropriate control to be put into the tablecell
    ''' </summary>
    ''' <param name="vcNewFormFieldName">Represents if the field name.</param>
    ''' <param name="vcDbColumnName">Represents if the database field name.</param>
    ''' <param name="boolRequired">Represents if the field is mandatory or not.</param>
    ''' <value>Returns the controls as an object.</value>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    ''' 
    Public Function getDynamicValidationsControlsRequired(ByVal vcNewFormFieldName As String, ByVal vcDbColumnName As String, ByVal boolRequired As Boolean) As RequiredFieldValidator
        Try


            Dim valReqControl As New RequiredFieldValidator                                             'Declare a required field validator
            valReqControl.ControlToValidate = vcDbColumnName                                            'Attach the control to validate
            valReqControl.EnableClientScript = True                                                     'Enable Client script
            valReqControl.InitialValue = ""                                                             'Set the Initial value so that it also validates drop down
            valReqControl.ErrorMessage = "Please enter " & vcNewFormFieldName & "."                     'Specify the error message to be displayed
            valReqControl.Display = ValidatorDisplay.None                                               'Error message never displays inline
            Return valReqControl
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to return the appropriate control to be put into the tablecell
    ''' </summary>
    ''' <param name="vcNewFormFieldName">Represents if the field name.</param>
    ''' <param name="vcDbColumnName">Represents if the database field name.</param>
    ''' <param name="vcDataFieldType">Represents if the field data type (V: Varchar, N: Numeric, B: Boolean, M: Money.</param>
    ''' <value>Returns the controls as an object.</value>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Public Function getDynamicValidationsControlsDataType(ByVal vcNewFormFieldName As String, ByVal vcDbColumnName As String, ByVal vcFieldDataType As Char) As Object
        Try


            Select Case vcFieldDataType
                Case "N"                                                                                    'Numeric Data Type
                    Dim valDataType As New RangeValidator                                                   'Create an instance of Range Validator Control
                    valDataType.MinimumValue = 0                                                            'Set the lower limit
                    valDataType.MaximumValue = 999999999999999999                                           'Set the upper limit
                    valDataType.ControlToValidate = vcDbColumnName                                          'Set the control to validate
                    valDataType.EnableClientScript = True                                                   'Enable Client script
                    valDataType.Type = ValidationDataType.Double                                            'set the datatype
                    valDataType.ErrorMessage = "Invalid value entered for " & vcNewFormFieldName            'Set tge error message
                    valDataType.Display = ValidatorDisplay.None                                             'Error message never displays inline
                    Return valDataType
                    Exit Select
                Case "B"                                                                                    'Boolean Case
                    Dim valDataType As New RangeValidator                                                   'Create an instance of Range Validator Control
                    valDataType.MinimumValue = 0                                                            'Set the lower limit
                    valDataType.MaximumValue = 1                                                            'Set the upper limit
                    valDataType.ControlToValidate = vcDbColumnName                                          'Set the control to validate
                    valDataType.EnableClientScript = True                                                   'Enable Client script
                    valDataType.Type = ValidationDataType.Integer                                           'set the datatype
                    valDataType.ErrorMessage = "Invalid value entered for " & vcNewFormFieldName            'Set tge error message
                    valDataType.Display = ValidatorDisplay.None                                             'Error message never displays inline
                    Return valDataType
                    Exit Select
                Case "M"                                                                                    'Money Data Type
                    Dim valDataType As New RangeValidator                                                   'Create an instance of Range Validator Control
                    valDataType.MinimumValue = 0                                                            'Set the lower limit
                    valDataType.MaximumValue = 999999999999999999                                           'Set the upper limit
                    valDataType.ControlToValidate = vcDbColumnName                                          'Set the control to validate
                    valDataType.EnableClientScript = True                                                   'Enable Client script
                    valDataType.Type = ValidationDataType.Currency                                          'set the datatype
                    valDataType.ErrorMessage = "Invalid value entered for " & vcNewFormFieldName            'Set tge error message
                    valDataType.Display = ValidatorDisplay.None                                             'Error message never displays inline
                    Return valDataType
                    Exit Select
                Case "E"                                                                                    'Email
                    Dim valDataType As New RegularExpressionValidator                                       'Create an instance of Regular Expression Validator Control
                    valDataType.ControlToValidate = vcDbColumnName                                          'Set the control to validate
                    valDataType.ValidationExpression = "^[\w-]+(?:\.[\w-]+)*@(?:[\w-]+\.)+[a-zA-Z]{2,7}$"   'Set the expression for email validaiton
                    valDataType.EnableClientScript = True                                                   'Enable Client script
                    valDataType.ErrorMessage = "Invalid value entered for " & vcNewFormFieldName            'Set tge error message
                    valDataType.Display = ValidatorDisplay.None                                             'Error message never displays inline
                    Return valDataType
                    Exit Select
                Case Else
                    Return New LiteralControl("")                                                           'just return a default control
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This method is used to return the appropriate control to be put into the tablecell
    ''' </summary>
    ''' <param name="vcDbColumnName">Represents the database column name where this field will dump data.</param>
    ''' <param name="vcAssociatedControlType">Represents the associated control type.</param>
    ''' <param name="boolRequired">Represents if the field is mandatory or not.</param>
    ''' <param name="numListID">If the control is a listbox, this links the options of the listbox.</param>
    ''' <value>Returns the controls as an object.</value>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	09/19/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Public Function getDynamicControlAndData(ByVal numFormFieldId As Integer, ByVal vcDbColumnName As String, ByVal vcListItemType As String, ByVal vcAssociatedControlType As String, ByVal numListID As Integer, ByVal numRowNum As Integer, ByVal numColNum As Integer) As Object
        Try
            'declare a generalized object
            Dim oControlBox As Object
            Select Case vcAssociatedControlType
                Case "TextArea"                                                                         'Text Area is selected to be provided
                    oControlBox = New TextBox                                                           'Instantiate a TextBox
                    oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                    oControlBox.Width = Unit.Pixel(150)                                                 'set the width of the textbox                                                'set the height of the textbox
                    oControlBox.TextMode = TextBoxMode.MultiLine                                        'This is a multiline textarea
                    oControlBox.CssClass = "textbox"                                                     'Set the class attribute for the textbox control
                    oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
                Case "SelectBox"                                                                        'If the control to be used is a selectbox
                    Dim objConfigWizard As New FormGenericFormContents                                  'Create an object of class which encaptulates the functionaltiy
                    objConfigWizard.DomainID = DomainID                                                 'set the domain id
                    objConfigWizard.ListItemType = vcListItemType                                       'set the listitem type
                    oControlBox = New System.Web.UI.WebControls.DropDownList                                                       'instantiate a drop down
                    oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                    If vcDbColumnName.IndexOf("Country") > -1 Then                                      'Special Treatment for drop down of Countries, since its a drop down which triggers the filteration of states
                        'oControlBox.Attributes.Add("onchange", "javascript: filterStates4Country(this);") 'Set the javascript which will filter the states whenever country is selected
                    End If
                    Dim dtTable As DataTable                                                            'declare a datatable
                    dtTable = objConfigWizard.GetMasterListByListId(numListID)                          'call function to fill the datatable
                    Dim dRow As DataRow                                                                 'declare a datarow
                    dRow = dtTable.NewRow()                                                             'get a new row object
                    dtTable.Rows.InsertAt(dRow, 0)                                                      'insert the row
                    dtTable.Rows(0).Item("numItemID") = DBNull.Value                                               'set the values for the first entry
                    dtTable.Rows(0).Item("vcItemName") = "---Select One---"                             'set the group name
                    oControlBox.DataSource = dtTable                                                    'set the datasource of the drop down
                    oControlBox.DataTextField = "vcItemName"                                            'Set the Text property of the drop down
                    oControlBox.DataValueField = "numItemID"                                            'Set the value attribute of the textbox
                    oControlBox.DataBind()                                                              'Databind the drop down
                    oControlBox.Width = Unit.Pixel(150)                                                 'set the width of the drop down
                    oControlBox.CssClass = "dropdown"                                                     'Set the class attribute for the selectbox control
                    oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
                Case "CheckBox"                                                                         'Checkbox control is requested
                    oControlBox = New CheckBox                                                          'instantiate a new checkbox
                    oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                    oControlBox.CssClass = "text"                                                       'set the class attribute
                    oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
                    'Checkbox of Billing Address Same as Shpping needs scripting/ Requested by Carl on the 1st of Mar 2006
                    If vcDbColumnName.IndexOf("bitSameAddr") > -1 Then  'If its a checkbox of Same Address for Billing/ Shiiping
                        oControlBox.Attributes.Add("onclick", "javascript: MakeAddressShippingSameAsBilling(this);") 'Attach Javascript
                    End If
                Case "RadioBox"                                                                         'Radio button is requested
                    oControlBox = New RadioButton                                                       'instantiate the radio
                    oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                    oControlBox.CssClass = "text"                                                       'set the class attribute
                    oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
                Case "PasswordBox"                                                                         'Text Area is selected to be provided
                    oControlBox = New TextBox                                                           'Instantiate a TextBox
                    oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                    oControlBox.Width = Unit.Pixel(150)                                                 'set the width of the textbox
                    oControlBox.CssClass = "textbox"                                                     'Set the class attribute for the textbox control
                    oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
                    oControlBox.TextMode = TextBoxMode.Password                                         'Set text mode to Password
                Case Else                                                                               'Edit Box is to be provided
                    oControlBox = New TextBox                                                           'instantiate a new editbox
                    oControlBox.ID = vcDbColumnName                                                     'set the name of the textbox
                    oControlBox.MaxLength = 50                                                          'Set the Maximum character that can be entered
                    oControlBox.CssClass = "textbox"                                                     'Set the class attribute for the editbox control
                    oControlBox.width = 150
                    'If vcDbColumnName.IndexOf("PostalCode") > -1 Then                                      'Special Treatment for drop down of Countries, since its a drop down which triggers the filteration of states
                    '    oControlBox.Attributes.Add("onkeypress", "return CheckNumber()")
                    'ElseIf vcDbColumnName.IndexOf("PostCode") > -1 Then
                    '    oControlBox.Attributes.Add("onkeypress", "return CheckNumber()")
                    'End If
                    oControlBox.TabIndex = System.Int16.Parse(numRowNum + (100 * numColNum))            'Set the Tab Index
            End Select
            Return CType(oControlBox, Object)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     This subroutine creates the controls on the form for AOIs
    ''' </summary>
    ''' <param name="dtFormConfig">The form config as stored in the BizForm Wizard.</param>
    ''' <value>Returns the controls in a checkboxlist.</value>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/30/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Sub createFormControlsAOI(ByVal dsFormConfig As DataSet, ByVal plhFormControls As PlaceHolder)
        Try
            Dim tblAdvSearchAOI As New HtmlTable
            Dim cbListDynamicAOIs As New CheckBoxList                                                   'instantiate a checkboxlist
            cbListDynamicAOIs.DataSource = dsFormConfig.Tables(1)                                                 'set a datashource 
            cbListDynamicAOIs.DataTextField = "vcAOIName"                                      'set the text for the checkboxlist
            cbListDynamicAOIs.DataTextFormatString = "&nbsp;{0}"                                        'set the data text format
            cbListDynamicAOIs.DataValueField = "numAOIID"                                         'set the value attribute
            cbListDynamicAOIs.CssClass = "text"                                                         'set the class attribute
            cbListDynamicAOIs.DataBind()                                                                'databind to display the data
            cbListDynamicAOIs.ID = "cbListAOI"                                                       'set the AOI checkboxlist ids
            cbListDynamicAOIs.RepeatColumns = 1                                                        'display 3 columns
            cbListDynamicAOIs.RepeatDirection = RepeatDirection.Vertical                              'display horizontally
            cbListDynamicAOIs.RepeatLayout = RepeatLayout.Table                                         'display in tabular format
            If cbListDynamicAOIs.Items.Count > 0 Then
                Dim tblRow As HtmlTableRow
                Dim tblCell As HtmlTableCell
                tblRow = New HtmlTableRow
                tblCell = New HtmlTableCell
                tblCell.InnerHtml = "<br/>What areas are you most interested in ? "
                tblCell.Align = "Left"
                tblCell.Attributes.Add("class", "text_bold")
                tblRow.Cells.Add(tblCell)
                tblAdvSearchAOI.Rows.Add(tblRow)

                tblRow = New HtmlTableRow
                tblCell = New HtmlTableCell
                tblCell.Controls.Add(cbListDynamicAOIs)
                tblRow.Cells.Add(tblCell)
                tblAdvSearchAOI.Rows.Add(tblRow)
                plhFormControls.Controls.Add(tblAdvSearchAOI)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub createFormControlsAOIHTML(ByVal dsFormConfig As DataSet, ByVal plhFormControls As PlaceHolder, ByVal EnableLinkedin As Integer)
        Try
            Dim tblAdvSearchAOI As New HtmlTable
            Dim oControlBox As CheckBox

            Dim tblRow As HtmlTableRow
            Dim tblCell As HtmlTableCell

            If dsFormConfig.Tables(1).Rows.Count > 0 Then

                tblRow = New HtmlTableRow
                tblCell = New HtmlTableCell
                tblCell.InnerHtml = "<br/>What areas are you most interested in ? "
                tblCell.Align = "Left"
                tblCell.Attributes.Add("class", "text_bold")
                tblRow.Cells.Add(tblCell)
                tblAdvSearchAOI.Rows.Add(tblRow)

                For i As Integer = 0 To dsFormConfig.Tables(1).Rows.Count - 1
                    tblRow = New HtmlTableRow
                    tblCell = New HtmlTableCell
                    oControlBox = New CheckBox                                                          'instantiate a new checkbox
                    oControlBox.ID = "cbListAOI_" & i                                                     'set the name of the textbox
                    oControlBox.CssClass = "text"                                                       'set the class attribute
                    oControlBox.Text = dsFormConfig.Tables(1).Rows(i)("vcAOIName")                                                      'set the class attribute
                    oControlBox.InputAttributes("value") = dsFormConfig.Tables(1).Rows(i)("numAOIID")
                    tblCell.Controls.Add(oControlBox)
                    tblRow.Cells.Add(tblCell)
                    tblAdvSearchAOI.Rows.Add(tblRow)
                Next
            End If

            tblRow = New HtmlTableRow
            tblCell = New HtmlTableCell                                                  'create a new table literal cell
            tblCell.Attributes.Add("class", "normal1")                                           'set the class attribute for teh tablecell

            Dim button As New Button()
            button.ID = "btnSubmit"
            button.Text = "Submit"
            button.Attributes.Add("onclick", "return validateFormsFields();")
            tblCell.Controls.Add(button)
            tblRow.Cells.Add(tblCell)
            If EnableLinkedin = 1 Then
                tblCell = New HtmlTableCell                                                  'create a new table literal cell
                tblCell.Attributes.Add("class", "normal1")                                           'set the class attribute for teh tablecell

                Dim buttonLinkedin As New Button()
                buttonLinkedin.ID = "btnLinkedinLogin"
                buttonLinkedin.Text = "Populate with Linkedin"
                buttonLinkedin.Attributes.Add("onclick", "return onLinkedInLoad();")
                tblCell.Controls.Add(buttonLinkedin)
                tblRow.Cells.Add(tblCell)
            End If
           
            tblAdvSearchAOI.Rows.Add(tblRow)

            plhFormControls.Controls.Add(tblAdvSearchAOI)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''' -----------------------------------------------------------------------------
    ''' <summary>
    '''     Send an Email notificaiton also attaching the XML data 
    ''' </summary>
    ''' <remarks>
    ''' </remarks>
    ''' <history>
    ''' 	[Debasish Tapan Nag]	08/03/2005	Created
    ''' </history>
    '''  -----------------------------------------------------------------------------
    Public Sub sendEmailNotification(ByVal strFileName As String)
        strFileName = sXMLFilePath & "\" & strFileName                                          'Set the file name
        Try
            'Dim msg As New MailMessage                                                          'Create the email message

            'msg.From = "carl@bizautomation.com"                                                 'Set basic properties
            'msg.To = "carl@bizautomation.com"
            'msg.Subject = "The following LeadBox information could not be added to the database"
            'msg.Body = "Sir," & vbCrLf & "The following LeadBox information posted by an user could not be uploaded into the database and it failed to generate a Lead." _
            '& vbCrLf & "You are hence requested to upload it manually." & vbCrLf

            'msg.Attachments.Add(New MailAttachment(strFileName))                                'Add an attachment (as many as needed)

            'SmtpMail.SmtpServer = "localhost"                                                   'Specify the outgoing SMTP mail server

            'SmtpMail.Send(msg)                                                                  'Send it off
            File.Delete(strFileName)                                                            'Delete the file after the email has been sent
        Catch ex As Exception
            'Throw ex
            'Email Sending failed
        End Try
    End Sub


End Module
