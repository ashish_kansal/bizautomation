﻿'Created by Anoop Jayaraj

Option Explicit On
Option Strict On

Imports Winnovative
Imports System.Configuration
Imports System.Drawing
Imports System.IO
Imports System.Collections.Specialized
Imports System.Text
Imports System.Data
Imports System.Web.HttpContext
Imports BACRM.BusinessLogic.Opportunities
Imports System.Web.UI.WebControls
Imports System.Web.HttpServerUtility

Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebNavigator
Imports System.Web.Caching
Imports Telerik.Web.UI
Imports System.Text.RegularExpressions
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports BACRM.BusinessLogic.Accounting

Namespace BACRM.BusinessLogic.Common

    Public Class HTMLToPDF
        Inherits BACRM.BusinessLogic.CBusinessBase

        Public Function Convert(ByVal URL As String, ByVal DomainID As Long) As String
            Try
                Dim pdfConverter As HtmlToPdfConverter = New HtmlToPdfConverter
                pdfConverter.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))
                Dim FilePath As String = CCommon.GetDocumentPhysicalPath(DomainID)
                Dim path As String = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter
                pdfConverter.PdfDocumentOptions.StretchToFit = True
                pdfConverter.PdfDocumentOptions.PdfPageSize.Width = 600
                pdfConverter.ConvertUrlToFile(URL, FilePath & path)

                Return path
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        <Obsolete("Not in use")>
        Public Function ConvertInvoice(ByVal URL As String) As String
            Try
                Dim pdfConverter As HtmlToPdfConverter = New HtmlToPdfConverter
                pdfConverter.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))
                Dim FilePath As String = ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs"
                Dim path As String = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A3
                pdfConverter.PdfDocumentOptions.StretchToFit = True
                pdfConverter.PdfDocumentOptions.PdfPageSize.Width = 900
                pdfConverter.ConvertUrlToFile(URL, FilePath & "\" & path)

                Return path

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ConvertHTML2PDF(ByVal strHTML As String, _
                                        ByVal DomainID As Long, _
                                        Optional ByVal BaseURL As String = "", _
                                        Optional ByVal IsAddRandomChar As Boolean = False,
                                        Optional ByVal numOrientation As Integer = 1,
                                        Optional ByVal keepFooterAtBottom As Boolean = False) As String
            Try
                Dim pdfConverter As HtmlToPdfConverter = New HtmlToPdfConverter
                pdfConverter.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))
                Dim FilePath As String = CCommon.GetDocumentPhysicalPath(DomainID)
                If Directory.Exists(FilePath) = False Then
                    Directory.CreateDirectory(FilePath)
                End If

                Dim path As String
                If IsAddRandomChar = True Then
                    path = "File" & Format(Now, "ddmmyyyyhhmmssfff") & PasswordGenerator.Generate(4) & ".pdf"
                Else
                    path = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                End If

                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal

                If numOrientation = 2 Then
                    pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Landscape
                Else
                    pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait
                End If

                pdfConverter.PdfDocumentOptions.StretchToFit = True
                pdfConverter.PdfDocumentOptions.BottomMargin = 5
                pdfConverter.PdfDocumentOptions.TopMargin = 5
                pdfConverter.PdfDocumentOptions.LeftMargin = 3
                pdfConverter.PdfDocumentOptions.RightMargin = 3
                pdfConverter.ConvertHtmlToFile(strHTML, BaseURL, FilePath & "\" & path)

                If keepFooterAtBottom Then
                    Dim document As Winnovative.Document = New Winnovative.Document(FilePath & "\" & path)

                    If numOrientation = 2 Then
                        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Landscape

                        If (document.Pages.Count > 1) Then
                            pdfConverter.PdfDocumentOptions.PdfPageSize.Height = 780 + (470 * (document.Pages.Count - 1)) + CCommon.ToInteger(IIf(document.Pages.Count = 2, 0, (150 * (document.Pages.Count - 2))))
                        Else
                            pdfConverter.PdfDocumentOptions.PdfPageSize.Height = 780 * document.Pages.Count
                        End If
                    Else
                        pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait
                        If (document.Pages.Count > 1) Then
                            pdfConverter.PdfDocumentOptions.PdfPageSize.Height = 1312 + (1119 * (document.Pages.Count - 1)) + CCommon.ToInteger(IIf(document.Pages.Count = 2, 0, (140 * (document.Pages.Count - 2))))
                        Else
                            pdfConverter.PdfDocumentOptions.PdfPageSize.Height = 1312 * (document.Pages.Count)
                        End If

                    End If

                    pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
                    pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter
                    pdfConverter.PdfDocumentOptions.StretchToFit = True
                    pdfConverter.PdfDocumentOptions.BottomMargin = 5
                    pdfConverter.PdfDocumentOptions.TopMargin = 5
                    pdfConverter.PdfDocumentOptions.LeftMargin = 3
                    pdfConverter.PdfDocumentOptions.RightMargin = 3

                    document.Close()
                    pdfConverter.ConvertHtmlToFile(strHTML, BaseURL, FilePath & "\" & path)
                End If

                Return path
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ' Added By Nitin, Modified by chintan
        Public Function ExportChecksToPdf(ByVal Vendors As ArrayList) As String
            Try
                Dim url As String = Current.Server.MapPath("../Accounting/CheckTemplate.htm")
                Dim oStreamReader As System.IO.StreamReader = New System.IO.StreamReader(url)
                Dim strTemplate As String

                'Create content 
                Dim obj As Object
                Dim ldVendorEntry As ListDictionary
                Dim strVendorChecks As New StringBuilder()
                Dim strTemp As String


                strTemplate = oStreamReader.ReadToEnd()
                oStreamReader.Close()


                Dim i As Integer = 0
                For Each obj In Vendors
                    If (i = Vendors.Count - 1) Then ' Bug Fix ID 1534 , comment 4747
                        strTemplate = strTemplate.Replace("page-break-after:always", "page-break-after:right")
                    End If
                    ldVendorEntry = CType(obj, ListDictionary)
                    strTemp = [String].Copy(strTemplate)
                    strTemp = strTemp.Replace("$CheckDate$", CStr(ldVendorEntry("Date")))
                    strTemp = strTemp.Replace("$PayTo$", CStr(ldVendorEntry("CompanyName")))
                    strTemp = strTemp.Replace("$PayToID$", CStr(ldVendorEntry("CompanyName")) & "," & CStr(ldVendorEntry("CompanyID")))
                    strTemp = strTemp.Replace("$Amount$", CStr(ldVendorEntry("Amount")))
                    strTemp = strTemp.Replace("$AmountInWords$", CStr(ldVendorEntry("AmountInWords")))
                    strTemp = strTemp.Replace("$BizDocIds$", CStr(ldVendorEntry("BizDocIds")))
                    strTemp = strTemp.Replace("$VendorInvoiceID$", CStr(ldVendorEntry("PurchaseOrderNo")))
                    strTemp = strTemp.Replace("$BillToAddress$", CStr(ldVendorEntry("BillToAddress")))
                    strTemp = strTemp.Replace("$EmployerName$", CStr(ldVendorEntry("EmployerName")))
                    strTemp = strTemp.Replace("$TotalAmount$", CStr(ldVendorEntry("totalAmount")))

                    strVendorChecks.Append(strTemp)
                    i = i + 1
                    strTemp = Nothing
                Next obj

                'Added by Manish Anjara - 22nd April,2013
                Dim strCheckes As String = strVendorChecks.ToString()
                If strCheckes IsNot Nothing AndAlso strCheckes.ToString.Contains("page-break-after: always") Then
                    strCheckes = CCommon.ReplaceLastOccurrence(strCheckes.ToString(), "page-break-after: always", "page-break-after:right")
                End If
                strVendorChecks.Clear()
                strVendorChecks.Append(strCheckes)

                Dim fileName As String = "CheckPrint." & Now.ToString("yyyyMMddHHMMssfff") & ".pdf"

                Dim SavePath As String = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))) & fileName
                'bug id 2024
                Dim DownloadPath As String = CCommon.GetDocumentPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))) & fileName

                If Not Directory.Exists(SavePath) Then
                    Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))))
                End If

                Dim pdfConverter As HtmlToPdfConverter = New HtmlToPdfConverter
                pdfConverter.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))

                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
                pdfConverter.PdfDocumentOptions.LeftMargin = 5
                pdfConverter.PdfDocumentOptions.RightMargin = 5
                pdfConverter.PdfDocumentOptions.TopMargin = 5
                pdfConverter.PdfDocumentOptions.BottomMargin = 5
                pdfConverter.PdfDocumentOptions.ShowHeader = False

                Dim footerText As New TextElement(0, 30, "Page &p; of &P;  ", New System.Drawing.Font(New System.Drawing.FontFamily("Times New Roman"), 10, System.Drawing.GraphicsUnit.Point))
                footerText.TextAlign = HorizontalTextAlign.Right
                footerText.ForeColor = Color.Navy
                footerText.EmbedSysFont = True
                pdfConverter.PdfFooterOptions.AddElement(footerText)
                Dim footerWidth As Single = pdfConverter.PdfDocumentOptions.PdfPageSize.Width - pdfConverter.PdfDocumentOptions.LeftMargin - pdfConverter.PdfDocumentOptions.RightMargin

                Dim footerLine As New LineElement(0, 0, footerWidth, 0)
                footerLine.ForeColor = Color.Gray
                pdfConverter.PdfFooterOptions.AddElement(footerLine)


                'Dim downloadBytes() As Byte = pdfConverter.GetPdfBytesFromHtmlString(strVendorChecks.ToString())
                'Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
                pdfConverter.ConvertHtmlToFile(strVendorChecks.ToString(), "", SavePath)

                Return DownloadPath

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Function ConvertInchesPoints(ByVal dcValue As Decimal, Optional ByVal IsPointToInches As Boolean = False) As Decimal
            Dim dcResult As Decimal = 0
            Try
                If IsPointToInches Then
                    dcResult = dcValue / 72
                Else
                    dcResult = dcValue * 72
                End If

            Catch ex As Exception
                dcResult = 0
            End Try

            Return dcResult
        End Function

        Public Function ExportChecksToPdfUsingITextSharp(ByVal Vendors As ArrayList, ByVal DomainID As Long, ByVal dtSelectedChecks As DataTable) As String
            Try
                Dim blankPDF As String = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))) & "BizCheckBlank.pdf"

                ''Create content 
                Dim files As New System.Collections.Generic.List(Of String)()
                Dim obj As Object
                Dim ldVendorEntry As ListDictionary

                Dim isUseDeluxeCheckStock As Boolean = False

                Dim objCommon As New CCommon
                objCommon.DomainID = DomainID
                Dim dt As DataTable = objCommon.GetDomainSettingValue("bitUseDeluxeCheckStock")
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    isUseDeluxeCheckStock = CCommon.ToBool(dt.Rows(0)(0))
                End If

                Dim fileName As String = "CheckPrint." & Now.ToString("yyyyMMddHHMMssfff") & ".pdf"

                Dim SavePath As String = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))) & fileName
                If Not Directory.Exists(SavePath) Then
                    Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))))
                End If

                Dim DownloadPath As String = CCommon.GetDocumentPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))) & fileName

                Dim i As Integer = 0
                For Each obj In Vendors
                    ldVendorEntry = CType(obj, ListDictionary)

                    'Create table with appropriate bizdoc/check details
                    Dim objChecks As New Checks
                    objChecks.DomainID = DomainID
                    objChecks.Mode = 4
                    objChecks.Type = 0
                    objChecks.AccountId = CCommon.ToInteger(ldVendorEntry("AccountID"))
                    objChecks.DivisionId = CCommon.ToInteger(ldVendorEntry("DivisionID"))
                    Dim ds As DataSet = objChecks.ManageCheckHeader()
                    If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        Dim reader As PdfReader = Nothing
                        Dim stamper As PdfStamper = Nothing

                        If Not isUseDeluxeCheckStock Then
                            If Not String.IsNullOrEmpty(blankPDF) AndAlso File.Exists(blankPDF) Then
                                Try
                                    SetCheckPrintPreferences(DomainID)

                                    Dim pathin As String = CCommon.ToString(blankPDF)
                                    Dim pathout As String = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))) & "CheckPrint" & CCommon.ToLong(System.Web.HttpContext.Current.Session("UserContactID")) & DateTime.Now.ToString("yyyyMMddHHmmssfff") & i & ".pdf"
                                    If File.Exists(pathout) Then
                                        Try
                                            File.Delete(pathout)
                                        Catch ex As Exception

                                        End Try
                                    End If

                                    reader = New PdfReader(pathin)
                                    'select two pages from the original document
                                    reader.SelectPages("1-1")
                                    stamper = New PdfStamper(reader, New FileStream(pathout, FileMode.Create))
                                    Dim pbover As PdfContentByte = stamper.GetOverContent(1)
                                    'add content to the page using ColumnText
                                    ColumnText.ShowTextAligned(pbover, Element.ALIGN_LEFT, New Phrase(CStr(ldVendorEntry("Date"))), CSng(_CheckDateLeft), CSng(reader.GetPageSize(1).Height - CheckDateTop), 0)
                                    ColumnText.ShowTextAligned(pbover, Element.ALIGN_LEFT, New Phrase(CStr(ldVendorEntry("CompanyName"))), CSng(_PayeeNameLeft), CSng(reader.GetPageSize(1).Height - _PayeeNameTop), 0)
                                    ColumnText.ShowTextAligned(pbover, Element.ALIGN_LEFT, New Phrase(String.Format("{0:#,##0.00}", CCommon.ToDecimal(ldVendorEntry("Amount")))), CSng(_AmountLeft), CSng(reader.GetPageSize(1).Height - _AmountTop), 0)
                                    ColumnText.ShowTextAligned(pbover, Element.ALIGN_LEFT, New Phrase(CStr(ldVendorEntry("AmountInWords"))), CSng(_AmountInWordsLeft), CSng(reader.GetPageSize(1).Height - _AmountInWordsTop), 0)


                                    If _EmployerNameIncluded = 1 Then
                                        ColumnText.ShowTextAligned(pbover, Element.ALIGN_LEFT, New Phrase(CStr(ldVendorEntry("EmployerName"))), CSng(_EmployerNameLeft), CSng(reader.GetPageSize(1).Height - _EmployerNameTop), 0)
                                    End If

                                    Dim bizdocsTable As PdfPTable = New PdfPTable(6)
                                    bizdocsTable.TotalWidth = reader.GetPageSize(1).Width
                                    bizdocsTable.HorizontalAlignment = Element.ALIGN_LEFT
                                    bizdocsTable.WidthPercentage = 100
                                    bizdocsTable.SetWidths(New Integer() {2, 4, 3, 3, 5, 2})

                                    Dim hCell1 As PdfPCell
                                    hCell1 = New PdfPCell(New Phrase("Date", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                                    hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                                    hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                                    hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    hCell1 = New PdfPCell(New Phrase("Payee Name & ID", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                                    hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                                    hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                                    hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    hCell1 = New PdfPCell(New Phrase("P.O. ID(s)", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                                    hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                                    hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                                    hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    hCell1 = New PdfPCell(New Phrase("Vendor Invoice ID", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                                    hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                                    hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                                    hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    hCell1 = New PdfPCell(New Phrase("Memo/Description", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                                    hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                                    hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                                    hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    hCell1 = New PdfPCell(New Phrase("Amount", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE)))
                                    hCell1.HorizontalAlignment = PdfPCell.ALIGN_CENTER
                                    hCell1.VerticalAlignment = PdfPCell.ALIGN_MIDDLE
                                    hCell1.BackgroundColor = iTextSharp.text.BaseColor.GRAY
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    Dim FooterTotal As Double = 0
                                    For Each dr As DataRow In ds.Tables(0).Rows
                                        If dtSelectedChecks.Select("numCheckHeaderID=" & CCommon.ToLong(dr("numCheckHeaderID"))).Length > 0 Then
                                            hCell1 = New PdfPCell(New Phrase(FormattedDateFromDate(CDate(dr("dtCheckDate")), CStr(HttpContext.Current.Session("DateFormat"))), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)))
                                            hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                            hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                            bizdocsTable.AddCell(hCell1)

                                            hCell1 = New PdfPCell(New Phrase(CCommon.ToString(dr("vcCompanyName")) & "," & CCommon.ToString(dr("numDivisionID")), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)))
                                            hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                            hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                            bizdocsTable.AddCell(hCell1)

                                            hCell1 = New PdfPCell(New Phrase(CCommon.ToString(dr("vcBizDoc")), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)))
                                            hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                            hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                            bizdocsTable.AddCell(hCell1)

                                            hCell1 = New PdfPCell(New Phrase(CCommon.ToString(dr("vcRefOrderNo")), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)))
                                            hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                            hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                            bizdocsTable.AddCell(hCell1)

                                            hCell1 = New PdfPCell(New Phrase(CCommon.ToString(dr("vcMemo")), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)))
                                            hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                            hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                            bizdocsTable.AddCell(hCell1)

                                            hCell1 = New PdfPCell(New Phrase(CCommon.ToString(String.Format("{0:#,##0.00}", dr("monAmount"))), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)))
                                            hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                            hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                            bizdocsTable.AddCell(hCell1)

                                            FooterTotal = FooterTotal + CCommon.ToDouble(dr("monAmount"))
                                        End If
                                    Next

                                    hCell1 = New PdfPCell(New Phrase("", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)))
                                    hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    hCell1 = New PdfPCell(New Phrase("", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)))
                                    hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    hCell1 = New PdfPCell(New Phrase("", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)))
                                    hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    hCell1 = New PdfPCell(New Phrase("", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)))
                                    hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    hCell1 = New PdfPCell(New Phrase("Total", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)))
                                    hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)

                                    hCell1 = New PdfPCell(New Phrase(CCommon.ToString(String.Format("{0:#,##0.00}", FooterTotal)), FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)))
                                    hCell1.HorizontalAlignment = CCommon.ToInteger(HorizontalAlign.Left)
                                    hCell1.BorderColor = iTextSharp.text.BaseColor.LIGHT_GRAY
                                    bizdocsTable.AddCell(hCell1)


                                    Dim ct As ColumnText = New ColumnText(pbover)

                                    If _AddressIncluded = 1 Then
                                        Dim address As String = ""
                                        If CCommon.ToString(ldVendorEntry("BillToAddress")).Length > 0 AndAlso CCommon.ToString(ldVendorEntry("BillToAddress")).Split(CChar("<br>")).Length > 0 Then
                                            address = CCommon.ToString(ldVendorEntry("BillToAddress"))
                                        End If

                                        If address.Length > 0 Then
                                            Dim addressTable As New PdfPTable(1)
                                            addressTable.HorizontalAlignment = Element.ALIGN_LEFT

                                            Dim cellAddress As PdfPCell = New PdfPCell(New Phrase(CStr(ldVendorEntry("BillToAddress"))))
                                            cellAddress.Border = 0

                                            Dim iNewelement As IElement
                                            If CCommon.ToString(ldVendorEntry("BillToAddress")).Length > 0 AndAlso CCommon.ToString(ldVendorEntry("BillToAddress")).Split(CChar("<br>")).Length > 0 Then
                                                For Each strItem As String In CCommon.ToString(ldVendorEntry("BillToAddress")).Split(CChar("<br>"))
                                                    strItem = Replace(strItem, "<br>", "")
                                                    strItem = Replace(strItem, "br>", "")
                                                    strItem = Replace(strItem, "<br", "")
                                                    strItem = Replace(strItem, "br", "")
                                                    strItem = Replace(strItem, "<", "")
                                                    strItem = Replace(strItem, ">", "")
                                                    If Not String.IsNullOrWhiteSpace(strItem) Then
                                                        iNewelement = New Phrase(strItem)
                                                        cellAddress.AddElement(iNewelement)
                                                    End If
                                                Next
                                            Else
                                                iNewelement = New Phrase("")
                                                cellAddress.AddElement(iNewelement)
                                            End If
                                            addressTable.AddCell(cellAddress)

                                            ct.SetSimpleColumn(CSng(_AddressLeft), 0, CSng(_AddressLeft) + 300, CSng(reader.GetPageSize(1).Height - _AddressTop))
                                            ct.AddElement(addressTable)
                                            ct.Go()
                                        End If


                                    End If

                                    ct.SetSimpleColumn(10, 0, (reader.GetPageSize(1).Width - 10), CSng(reader.GetPageSize(1).Height - Stub1Top))
                                    ct.AddElement(bizdocsTable)
                                    ct.Go()

                                    ct.SetSimpleColumn(10, 0, (reader.GetPageSize(1).Width - 10), CSng(reader.GetPageSize(1).Height - Stub2Top))
                                    ct.AddElement(bizdocsTable)
                                    ct.Go()

                                    'close the stamper
                                    stamper.Close()

                                    files.Add(pathout)
                                Catch ex As Exception
                                    Throw
                                Finally
                                    stamper.Close()
                                    reader.Close()
                                    reader.Dispose()
                                End Try
                            Else
                                Throw New Exception("Upload your check pdf.")
                            End If
                        Else
                            Dim pathout As String = CCommon.GetDocumentPhysicalPath(CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))) & "CheckPrint" & CCommon.ToLong(System.Web.HttpContext.Current.Session("UserContactID")) & DateTime.Now.ToString("yyyyMMddHHmmssfff") & i & ".pdf"
                            If File.Exists(pathout) Then
                                Try
                                    File.Delete(pathout)
                                Catch ex As Exception

                                End Try
                            End If

                            Dim pdfDocument As New Winnovative.Document()

                            'Get the selected PDF page size
                            Dim pdfPageSize As PdfPageSize = pdfPageSize.A4

                            'Get the selected PDF page orientation
                            Dim pdfPageOrientation As PdfPageOrientation = pdfPageOrientation.Portrait

                            'Get the PDF page margins
                            Dim pdfPageMargins As New Margins(0, 0, 0, 0)

                            'Create a PDF page in PDF document
                            Dim firstPdfPage As Winnovative.PdfPage = pdfDocument.AddPage(pdfPageSize, pdfPageMargins, pdfPageOrientation)

                            Try
                                If CCommon.ToString(HttpContext.Current.Session("vcLogoForBizTheme")) <> "" Then
                                    Dim FilePath As String = CCommon.GetDocumentPath(CCommon.ToLong(HttpContext.Current.Session("DomainID")))
                                    If File.Exists(FilePath & CCommon.ToString(HttpContext.Current.Session("vcLogoForBizTheme"))) Then

                                    End If
                                End If


                                Dim checkHtml As String = "<!DOCTYPE html><html><head><title></title><style type='text/css'> html, body { width:100%; margin:0;font-family:sans-serif;} table > tr > td { min-height:40px; }</style></head><body><table style='width:100%'>"
                                checkHtml += "<tr><td colspan='4'>"
                                checkHtml += "<table>"
                                checkHtml += "<tr>"
                                checkHtml += "<td>"
                                If CCommon.ToString(HttpContext.Current.Session("vcLogoForBizTheme")) <> "" Then
                                    Dim FilePath As String = CCommon.GetDocumentPath(CCommon.ToLong(HttpContext.Current.Session("DomainID")))
                                    If File.Exists(FilePath & CCommon.ToString(HttpContext.Current.Session("vcLogoForBizTheme"))) Then
                                        checkHtml += "<img id='imgThemeLogo' class='img img-responsive' src='" & (FilePath & CCommon.ToString(HttpContext.Current.Session("vcLogoForBizTheme"))) & "' style='border-width: 0px; max-height: 37px'>"
                                    End If
                                End If
                                checkHtml += "</td>"
                                checkHtml += "<td>"
                                checkHtml += ("<b>" & CCommon.ToString(ldVendorEntry("Employer")) & "</b><br />" & CCommon.ToString(ldVendorEntry("EmployerAddress")) & "</td></tr>")
                                checkHtml += "</td>"
                                checkHtml += "</tr>"
                                checkHtml += "</table>"
                                checkHtml += "</td></tr>"
                                checkHtml += "<tr><td colspan='4'>"
                                checkHtml += "<table style='float:right'>"
                                checkHtml += "<tr>"
                                checkHtml += "<td><b>Date </b></td>"
                                checkHtml += "<td style='border-bottom: 1px solid #000;'>" & CCommon.ToString(ldVendorEntry("Date")) & "</td>"
                                checkHtml += "</tr>"
                                checkHtml += "</table>"
                                checkHtml += "</td></tr>"
                                checkHtml += "<tr><td><br /></td></tr>"
                                checkHtml += ("<tr><td style='white-space:nowrap'><b>PAY TO THE <br />ORDER OF</b></td><td style='width:100%; border-bottom:1px solid #000;vertical-align:bottom'>" & CCommon.ToString(ldVendorEntry("CompanyName")) & "</td><td style='text-align:right;white-space:nowrap;min-width:30px;'><b>$ </b></td><td style='text-align:center;white-space:nowrap;border:1px solid #000;padding:0px 10px;min-width:130px;'>" & String.Format("{0:#,##0.00}", CCommon.ToDecimal(ldVendorEntry("Amount"))) & "</td></tr>")
                                checkHtml += "<tr><td><br /></td></tr>"
                                checkHtml += ("<tr><td style='white-space:nowrap;min-width:100px'></td><td style='width:100%;border-bottom:1px solid #000;'>" & CCommon.ToString(ldVendorEntry("AmountInWords")) & "</td><td colspan='2' style='text-align:left; white-space:nowrap; min-width:160px;'> <b>DOLLARS</b></td></tr>")
                                checkHtml += "</table></body></html>"
                                'Create the HTML to PDF element
                                Dim htmlToPdfElement As New HtmlToPdfElement(5, 5, checkHtml, "")
                                htmlToPdfElement.Width = 567

                                'Add the HTML to PDF element to PDF document
                                firstPdfPage.AddElement(htmlToPdfElement)

                                Dim billsHtml As String = "<table style='width:100%;border-collapse:collapse;border: 1px solid #dee2e6;'> <thead> <tr> <th style='width:100px;color:#495057;background-color:#e9ecef;border-color:#dee2e6;border-bottom: 2px solid #dee2e6;padding: .75rem;'>Date</th> <th style='color:#495057;background-color:#e9ecef;border-color:#dee2e6;border-bottom: 2px solid #dee2e6;padding: .75rem;'>Payee Name & ID</th> <th style='color:#495057;background-color:#e9ecef;border-color:#dee2e6;border-bottom: 2px solid #dee2e6;padding: .75rem;'>P.O. ID(s)</th> <th style='color:#495057;background-color:#e9ecef;border-color:#dee2e6;border-bottom: 2px solid #dee2e6;padding: .75rem;'>Vendor Invoice ID</th> <th style='color:#495057;background-color:#e9ecef;border-color:#dee2e6;border-bottom: 2px solid #dee2e6;padding: .75rem;'>Memo/Description</th> <th style='width:120px;color:#495057;background-color:#e9ecef;border-color:#dee2e6;border-bottom: 2px solid #dee2e6;padding: .75rem;'>Amount</th></tr></thead><tbody>"

                                Dim FooterTotal As Double = 0
                                For Each dr As DataRow In ds.Tables(0).Rows
                                    If dtSelectedChecks.Select("numCheckHeaderID=" & CCommon.ToLong(dr("numCheckHeaderID"))).Length > 0 Then
                                        billsHtml += "<tr>"
                                        billsHtml += ("<td style='padding:.75rem;vertical-align:top;border:1px solid #dee2e6;'>" & FormattedDateFromDate(CDate(dr("dtCheckDate")), CStr(HttpContext.Current.Session("DateFormat"))) & "</td>")
                                        billsHtml += ("<td style='padding:.75rem;vertical-align:top;border:1px solid #dee2e6;'>" & CCommon.ToString(dr("vcCompanyName")) & "," & CCommon.ToString(dr("numDivisionID")) & "</td>")
                                        billsHtml += ("<td style='padding:.75rem;vertical-align:top;border:1px solid #dee2e6;'>" & CCommon.ToString(dr("vcBizDoc")) & "</td>")
                                        billsHtml += ("<td style='padding:.75rem;vertical-align:top;border:1px solid #dee2e6;'>" & CCommon.ToString(dr("vcRefOrderNo")) & "</td>")
                                        billsHtml += ("<td style='padding:.75rem;vertical-align:top;border:1px solid #dee2e6;'>" & CCommon.ToString(dr("vcMemo")) & "</td>")
                                        billsHtml += ("<td style='padding:.75rem;vertical-align:top;border:1px solid #dee2e6;text-align:right'>" & String.Format("{0:#,##0.00}", dr("monAmount")) & "</td>")
                                        billsHtml += "</tr>"

                                        FooterTotal = FooterTotal + CCommon.ToDouble(dr("monAmount"))
                                    End If
                                Next

                                billsHtml += ("</tbody> <tfoot> <tr> <td colspan='5' style='padding:.75rem;text-align: right; font-weight: bold;border:1px solid #dee2e6;'>Total</td> <td style='padding:.75rem;text-align: right;border:1px solid #dee2e6;'>" & String.Format("{0:#,##0.00}", FooterTotal) & "</td></tr></tfoot></table>")

                                Dim htmlBills1 As New HtmlToPdfElement(5, 245, billsHtml, "")
                                firstPdfPage.AddElement(htmlBills1)

                                Dim htmlBills2 As New HtmlToPdfElement(5, 490, billsHtml, "")
                                firstPdfPage.AddElement(htmlBills2)

                                'Save the PDF document in a memory buffer
                                Dim outPdfBuffer() As Byte = pdfDocument.Save()


                                File.WriteAllBytes(pathout, outPdfBuffer)

                                files.Add(pathout)
                            Catch ex As Exception
                                Throw
                            Finally
                                pdfDocument.Close()
                            End Try
                        End If
                    End If

                    i = i + 1
                Next obj

                CombineMultiplePDFs(files.ToArray(), SavePath)

                For Each pdffile As String In files
                    If File.Exists(pdffile) Then
                        Try
                            File.Delete(pdffile)
                        Catch ex As Exception
                            'DO NOT THROW ERROR
                        End Try
                    End If
                Next

                Return DownloadPath

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub SetCheckPrintPreferences(ByVal DomainID As Long)
            Try
                Dim strPreferenceFile As String = CCommon.GetDocumentPhysicalPath(DomainID) & "\CheckPreferences.xml"
                If Not File.Exists(strPreferenceFile) Then
                    strPreferenceFile = CCommon.GetDocumentPhysicalPath(0) & "\CheckPreferences.xml"
                End If

                Dim dsPref As New DataSet
                dsPref.ReadXml(strPreferenceFile)
                If dsPref IsNot Nothing AndAlso dsPref.Tables.Count > 0 AndAlso dsPref.Tables(0).Rows.Count > 0 Then
                    Dim drPref() As DataRow

                    drPref = dsPref.Tables(0).Select("Name = 'CheckDate'")
                    If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                        _CheckDateFontFamily = CCommon.ToString(drPref(0)("FontFamily"))
                        _CheckDateFontSize = CCommon.ToInteger(drPref(0)("FontSize"))
                        _CheckDateFontStyle = CCommon.ToInteger(drPref(0)("FontStyle"))

                        _CheckDateLeft = CCommon.ToDecimal(drPref(0)("Left"))
                        _CheckDateTop = CCommon.ToDecimal(drPref(0)("Top"))
                    End If

                    drPref = dsPref.Tables(0).Select("Name = 'PayeeName'")
                    If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                        _PayeeNameFontFamily = CCommon.ToString(drPref(0)("FontFamily"))
                        _PayeeNameFontSize = CCommon.ToInteger(drPref(0)("FontSize"))
                        _PayeeNameFontStyle = CCommon.ToInteger(drPref(0)("FontStyle"))

                        _PayeeNameLeft = CCommon.ToDecimal(drPref(0)("Left"))
                        _PayeeNameTop = CCommon.ToDecimal(drPref(0)("Top"))
                    End If

                    drPref = dsPref.Tables(0).Select("Name = 'Amount'")
                    If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                        _AmountFontFamily = CCommon.ToString(drPref(0)("FontFamily"))
                        _AmountFontSize = CCommon.ToInteger(drPref(0)("FontSize"))
                        _AmountFontStyle = CCommon.ToInteger(drPref(0)("FontStyle"))

                        _AmountLeft = CCommon.ToDecimal(drPref(0)("Left"))
                        _AmountTop = CCommon.ToDecimal(drPref(0)("Top"))
                    End If

                    drPref = dsPref.Tables(0).Select("Name = 'AmountInWords'")
                    If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                        _AmountInWordsFontFamily = CCommon.ToString(drPref(0)("FontFamily"))
                        _AmountInWordsFontSize = CCommon.ToInteger(drPref(0)("FontSize"))
                        _AmountInWordsFontStyle = CCommon.ToInteger(drPref(0)("FontStyle"))

                        _AmountInWordsLeft = CCommon.ToDecimal(drPref(0)("Left"))
                        _AmountInWordsTop = CCommon.ToDecimal(drPref(0)("Top"))
                    End If

                    drPref = dsPref.Tables(0).Select("Name = 'EmployerName'")
                    If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                        _EmployerNameFontFamily = CCommon.ToString(drPref(0)("FontFamily"))
                        _EmployerNameFontSize = CCommon.ToInteger(drPref(0)("FontSize"))
                        _EmployerNameFontStyle = CCommon.ToInteger(drPref(0)("FontStyle"))

                        _EmployerNameLeft = CCommon.ToDecimal(drPref(0)("Left"))
                        _EmployerNameTop = CCommon.ToDecimal(drPref(0)("Top"))
                        _EmployerNameIncluded = CCommon.ToShort(drPref(0)("IsIncluded"))
                    End If

                    drPref = dsPref.Tables(0).Select("Name = 'Address'")
                    If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                        _AddressFontFamily = CCommon.ToString(drPref(0)("FontFamily"))
                        _AddressFontSize = CCommon.ToInteger(drPref(0)("FontSize"))
                        _AddressFontStyle = CCommon.ToInteger(drPref(0)("FontStyle"))

                        _AddressLeft = CCommon.ToDecimal(drPref(0)("Left"))
                        _AddressTop = CCommon.ToDecimal(drPref(0)("Top"))
                        _AddressIncluded = CCommon.ToShort(drPref(0)("IsIncluded"))
                    End If

                    drPref = dsPref.Tables(0).Select("Name = 'Stub1'")
                    If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                        Stub1Left = CCommon.ToDouble(drPref(0)("Left"))
                        Stub1Top = CCommon.ToDouble(drPref(0)("Top"))
                    Else
                        Stub1Left = 0
                        Stub1Top = 300
                    End If

                    drPref = dsPref.Tables(0).Select("Name = 'Stub2'")
                    If drPref IsNot Nothing AndAlso drPref.Length > 0 Then
                        Stub2Left = CCommon.ToDouble(drPref(0)("Left"))
                        Stub2Top = CCommon.ToDouble(drPref(0)("Top"))
                    Else
                        Stub2Left = 0
                        Stub2Top = 600
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetFonts(ByVal FontName As String, ByVal FontSize As Integer, ByVal FontStyle As Integer) As iTextSharp.text.Font

            Dim fontResult As iTextSharp.text.Font
            Try
                If FontName = "Arial" Then
                    fontResult = FontFactory.GetFont("Arial", FontSize, FontStyle)

                ElseIf FontName = "Courier" Then
                    fontResult = FontFactory.GetFont(BaseFont.COURIER, FontSize, FontStyle)

                ElseIf FontName = "Helvetica" Then
                    fontResult = FontFactory.GetFont(BaseFont.HELVETICA, FontSize, FontStyle)

                ElseIf FontName = "Times New Roman" Then
                    fontResult = FontFactory.GetFont(BaseFont.TIMES_ROMAN, FontSize, FontStyle)

                Else
                    fontResult = FontFactory.GetFont("Arial", FontSize, FontStyle)

                End If
            Catch ex As Exception

            End Try
            Return fontResult

        End Function


        Public Function CreatePackingSlipExportPdf(ByVal dt As DataTable) As Integer
            Dim downloadName As String = "PackingSlip.pdf"
            Dim pdfConverter As HtmlToPdfConverter = New HtmlToPdfConverter
            pdfConverter.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))
            Dim strTemp As String
            Dim dtItem As DataTable
            Dim strCss As String

            Try
                Dim strTemplate As String
                Dim strPackingSlip As New StringBuilder()
                Dim objEmail As New Email
                Dim dtEmailTemplate As New DataTable
                Dim dtOppBizAddDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs

                objOppBizDocs.DomainID = CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))
                objOppBizDocs.byteMode = 1

                Dim ht As New Collections.Hashtable
                Dim dvItem As DataView = dt.DefaultView
                Dim dtOrders As DataTable = dvItem.ToTable()

                Dim objCommon As New CCommon
                objCommon.DomainID = objOppBizDocs.DomainID
                objCommon.Str = CStr(enmBizDocTemplate_BizDocID.Packing_Slip_Template)
                objCommon.Mode = 17
                Dim intBizDocTempId As Integer = CCommon.ToInteger(objCommon.GetSingleFieldValue())

                If intBizDocTempId <= 0 Then
                    Return 101
                End If

                Dim imgLogo As New System.Web.UI.WebControls.Image
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = objCommon.DomainID
                objOppBizDoc.BizDocTemplateID = intBizDocTempId
                objOppBizDoc.OppType = 0
                objOppBizDoc.TemplateType = 0

                dtEmailTemplate = objOppBizDoc.GetBizDocTemplate()
                If dtEmailTemplate IsNot Nothing AndAlso dtEmailTemplate.Rows.Count > 0 Then
                    strTemplate = If(dtEmailTemplate.Rows(0)("txtBizDocTemplate") Is DBNull.Value, "", dtEmailTemplate.Rows(0)("txtBizDocTemplate").ToString())
                    strTemplate = System.Web.HttpContext.Current.Server.HtmlDecode(strTemplate)
                    strCss = "<style>" & HttpContext.Current.Server.HtmlDecode(dtEmailTemplate.Rows(0)("txtCSS").ToString()) & "</style>"

                    If Not IsDBNull(dtEmailTemplate.Rows(0)("vcBizDocImagePath")) Then
                        imgLogo.Visible = True
                        imgLogo.ImageUrl = CCommon.GetDocumentPath(objCommon.DomainID) & CType(dtEmailTemplate.Rows(0)("vcBizDocImagePath"), System.String)
                    Else : imgLogo.Visible = False
                    End If
                End If

                Dim objConfigWizard As New FormConfigWizard             'Create an object of form config wizard
                objConfigWizard.DomainID = objCommon.DomainID
                objConfigWizard.FormID = 7
                objConfigWizard.AuthenticationGroupID = enmBizDocTemplate_BizDocID.Packing_Slip_Template
                objConfigWizard.BizDocTemplateID = intBizDocTempId

                Dim dtConfigFields As DataTable
                dtConfigFields = objConfigWizard.getFieldList("")

                Dim intItemCnt As Integer = If(dtConfigFields.Select("intColumnNum=1") IsNot Nothing, dtConfigFields.Select("intColumnNum=1").Count, 0)
                '--------------------------------------------------------------------------------------------
                Dim dtOpp As New DataTable


                dtOpp = dtOrders.DefaultView.ToTable(True, "numOppID")
                For intCnt As Integer = 0 To dtOpp.Rows.Count - 1
                    strTemp = [String].Copy(strTemplate)

                    objOppBizDocs.OppId = CCommon.ToLong(dtOpp.Rows(intCnt)("numOppID"))
                    dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail

                    Dim intRowCnt As Integer = 0
                    Dim drOrder As DataRow() = dtOrders.Select("numOppID = " & objOppBizDocs.OppId)

                    For intRowCnt = 0 To 1 - 1
                        If dtOppBizAddDtl.Rows.Count = 1 Then
                            Dim strShipTo As String = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("ShipAdd"))
                            Dim strBillTo As String = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("BillAdd"))

                            strTemp = strTemp.Replace("#ShipTo#", RemovePRETag(strShipTo))
                            strTemp = strTemp.Replace("#BillTo#", RemovePRETag(strBillTo))
                        End If

                        strTemp = strTemp.Replace("#Logo#", CCommon.RenderControl(imgLogo))

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        'Customer/Vendor Information
                        strTemp = strTemp.Replace("#Customer/VendorOrganizationName#", CCommon.ToString(drOrder(intRowCnt)("OrganizationName")))
                        strTemp = strTemp.Replace("#Customer/VendorOrganizationPhone#", CCommon.ToString(drOrder(intRowCnt)("OrganizationPhone")))

                        Dim dsAddress As DataSet = objOppBizDocs.GetOPPGetOppAddressDetails

                        If dsAddress.Tables(0).Rows.Count = 0 Then
                            strTemp = strTemp.Replace("#OppOrderBillToCompanyName#", "")
                            strTemp = strTemp.Replace("#OppOrderBillToAddress#", "")
                            strTemp = strTemp.Replace("#OppOrderBillToStreet#", "")
                            strTemp = strTemp.Replace("#OppOrderBillToCity#", "")
                            strTemp = strTemp.Replace("#OppOrderBillToPostal#", "")
                            strTemp = strTemp.Replace("#OppOrderBillToState#", "")
                            strTemp = strTemp.Replace("#OppOrderBillToCountry#", "")
                            strTemp = strTemp.Replace("#OppOrderBillToAddressName#", "")
                            strTemp = strTemp.Replace("#BillingContact#", "")
                        Else
                            strTemp = strTemp.Replace("#OppOrderBillToCompanyName#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCompanyName")))
                            strTemp = strTemp.Replace("#OppOrderBillToAddress#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcFullAddress")))
                            strTemp = strTemp.Replace("#OppOrderBillToStreet#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcStreet")))
                            strTemp = strTemp.Replace("#OppOrderBillToCity#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCity")))
                            strTemp = strTemp.Replace("#OppOrderBillToPostal#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcPostalCode")))
                            strTemp = strTemp.Replace("#OppOrderBillToState#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcState")))
                            strTemp = strTemp.Replace("#OppOrderBillToCountry#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCountry")))
                            strTemp = strTemp.Replace("#OppOrderBillToAddressName#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcAddressName")))
                            strTemp = strTemp.Replace("#BillingContact#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcContact")))
                        End If

                        If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 4 AndAlso dsAddress.Tables(4).Rows.Count > 0 Then
                            strTemp = strTemp.Replace("#Customer/VendorBillToCompanyName#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCompanyName")))
                            strTemp = strTemp.Replace("#Customer/VendorBillToAddress#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcFullAddress")))
                            strTemp = strTemp.Replace("#Customer/VendorBillToStreet#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcStreet")))
                            strTemp = strTemp.Replace("#Customer/VendorBillToCity#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCity")))
                            strTemp = strTemp.Replace("#Customer/VendorBillToPostal#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcPostalCode")))
                            strTemp = strTemp.Replace("#Customer/VendorBillToState#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcState")))
                            strTemp = strTemp.Replace("#Customer/VendorBillToCountry#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCountry")))
                            strTemp = strTemp.Replace("#Customer/VendorBillToAddressName#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcAddressName")))
                        Else
                            strTemp = strTemp.Replace("#Customer/VendorBillToCompanyName#", "")
                            strTemp = strTemp.Replace("#Customer/VendorBillToAddress#", "")
                            strTemp = strTemp.Replace("#Customer/VendorBillToStreet#", "")
                            strTemp = strTemp.Replace("#Customer/VendorBillToCity#", "")
                            strTemp = strTemp.Replace("#Customer/VendorBillToPostal#", "")
                            strTemp = strTemp.Replace("#Customer/VendorBillToState#", "")
                            strTemp = strTemp.Replace("#Customer/VendorBillToCountry#", "")
                            strTemp = strTemp.Replace("#Customer/VendorBillToAddressName#", "")
                        End If

                        If dsAddress.Tables(1).Rows.Count = 0 Then
                            strTemp = strTemp.Replace("#OppOrderShipToCompanyName#", "")
                            strTemp = strTemp.Replace("#OppOrderShipToAddress#", "")
                            strTemp = strTemp.Replace("#OppOrderShipToStreet#", "")
                            strTemp = strTemp.Replace("#OppOrderShipToCity#", "")
                            strTemp = strTemp.Replace("#OppOrderShipToPostal#", "")
                            strTemp = strTemp.Replace("#OppOrderShipToState#", "")
                            strTemp = strTemp.Replace("#OppOrderShipToCountry#", "")
                            strTemp = strTemp.Replace("#OppOrderShipToAddressName#", "")
                            strTemp = strTemp.Replace("#ShippingContact#", "")
                        Else
                            strTemp = strTemp.Replace("#OppOrderShipToCompanyName#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCompanyName")))
                            strTemp = strTemp.Replace("#OppOrderShipToAddress#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcFullAddress")))
                            strTemp = strTemp.Replace("#OppOrderShipToStreet#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcStreet")))
                            strTemp = strTemp.Replace("#OppOrderShipToCity#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCity")))
                            strTemp = strTemp.Replace("#OppOrderShipToPostal#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcPostalCode")))
                            strTemp = strTemp.Replace("#OppOrderShipToState#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcState")))
                            strTemp = strTemp.Replace("#OppOrderShipToCountry#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCountry")))
                            strTemp = strTemp.Replace("#OppOrderShipToAddressName#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcAddressName")))
                            strTemp = strTemp.Replace("#ShippingContact#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcContact")))
                        End If

                        If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 5 AndAlso dsAddress.Tables(5).Rows.Count > 0 Then
                            strTemp = strTemp.Replace("#Customer/VendorShipToCompanyName#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCompanyName")))
                            strTemp = strTemp.Replace("#Customer/VendorShipToAddress#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcFullAddress")))
                            strTemp = strTemp.Replace("#Customer/VendorShipToStreet#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcStreet")))
                            strTemp = strTemp.Replace("#Customer/VendorShipToCity#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCity")))
                            strTemp = strTemp.Replace("#Customer/VendorShipToPostal#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcPostalCode")))
                            strTemp = strTemp.Replace("#Customer/VendorShipToState#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcState")))
                            strTemp = strTemp.Replace("#Customer/VendorShipToCountry#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCountry")))
                            strTemp = strTemp.Replace("#Customer/VendorShipToAddressName#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcAddressName")))
                        Else
                            strTemp = strTemp.Replace("#Customer/VendorShipToCompanyName#", "")
                            strTemp = strTemp.Replace("#Customer/VendorShipToAddress#", "")
                            strTemp = strTemp.Replace("#Customer/VendorShipToStreet#", "")
                            strTemp = strTemp.Replace("#Customer/VendorShipToCity#", "")
                            strTemp = strTemp.Replace("#Customer/VendorShipToPostal#", "")
                            strTemp = strTemp.Replace("#Customer/VendorShipToState#", "")
                            strTemp = strTemp.Replace("#Customer/VendorShipToCountry#", "")
                            strTemp = strTemp.Replace("#Customer/VendorShipToAddressName#", "")
                        End If



                        strTemp = strTemp.Replace("#Customer/VendorOrganizationComments#", CCommon.ToString(drOrder(intRowCnt)("vcOrganizationComments")))
                        strTemp = strTemp.Replace("#Customer/VendorOrganizationContactName#", CCommon.ToString(drOrder(intRowCnt)("OrgContactName")))
                        strTemp = strTemp.Replace("#Customer/VendorOrganizationContactPhone#", CCommon.ToString(drOrder(intRowCnt)("OrgContactPhone")))
                        strTemp = strTemp.Replace("#Customer/VendorOrganizationContactEmail#", CCommon.ToString(drOrder(intRowCnt)("OrgContactEmail")))
                        strTemp = strTemp.Replace("#OppOrderChangeShipToHeader(Ship To)#", "")
                        strTemp = strTemp.Replace("#OppOrderChangeShipToHeader#", "")
                        strTemp = strTemp.Replace("#OppOrderChangeBillToHeader(Bill To)#", "")
                        strTemp = strTemp.Replace("#OppOrderChangeBillToHeader#", "")
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


                        strTemp = strTemp.Replace("#AssigneeName#", CCommon.ToString(drOrder(intRowCnt)("AssigneeName")))
                        strTemp = strTemp.Replace("#AssigneeEmail#", CCommon.ToString(drOrder(intRowCnt)("AssigneeEmail")))
                        strTemp = strTemp.Replace("#AssigneePhoneNo#", CCommon.ToString(drOrder(intRowCnt)("AssigneePhone")))
                        strTemp = strTemp.Replace("#Currency#", CCommon.ToString(drOrder(intRowCnt)("varCurrSymbol")))
                        strTemp = strTemp.Replace("#Discount#", CCommon.ToString(drOrder(intRowCnt)("DiscAmt")))
                        strTemp = strTemp.Replace("#P.O.NO#", drOrder(intRowCnt)("P.O.No").ToString())
                        strTemp = strTemp.Replace("#CustomerPO##", drOrder(intRowCnt)("vcCustomerPO#").ToString())
                        strTemp = strTemp.Replace("#OrderID#", drOrder(intRowCnt)("vcPOppName").ToString())
                        strTemp = strTemp.Replace("#InventoryStatus#", CCommon.ToString(drOrder(intRowCnt)("vcInventoryStatus")))
                        strTemp = strTemp.Replace("#Comments#", drOrder(intRowCnt)("Comments").ToString())
                        strTemp = strTemp.Replace("#OrderRecOwner#", CCommon.ToString(drOrder(intRowCnt)("OrderRecOwner")))
                        strTemp = strTemp.Replace("#ShippingCompany#", drOrder(intRowCnt)("ShipVia").ToString())
                        strTemp = strTemp.Replace("#ShippingService#", CCommon.ToString(drOrder(intRowCnt)("vcShippingService")))
                        strTemp = strTemp.Replace("#PackingSlipID#", CCommon.ToString(drOrder(intRowCnt)("vcPackingSlip")))
                        strTemp = strTemp.Replace("#PartnerSource#", drOrder(intRowCnt)("vcPartner").ToString())
                        strTemp = strTemp.Replace("#ReleaseDate#", CCommon.ToString(drOrder(intRowCnt)("vcReleaseDate")))
                        strTemp = strTemp.Replace("#RequiredDate#", CCommon.ToString(drOrder(intRowCnt)("vcRequiredDate")))
                        strTemp = strTemp.Replace("#TrackingNo#", CCommon.ToString(drOrder(intRowCnt)("vcTrackingNo")))

                        strTemp = strTemp.Replace("#SOComments#", drOrder(intRowCnt)("vcSOComments").ToString())
                        strTemp = strTemp.Replace("#ParcelShippingAccount#", drOrder(intRowCnt)("vcShippersAccountNo").ToString())
                        strTemp = strTemp.Replace("#OrderCreatedDate#", drOrder(intRowCnt)("OrderCreatedDate").ToString())
                        strTemp = strTemp.Replace("#VendorInvoice#", drOrder(intRowCnt)("vcVendorInvoice").ToString())
                        'Unused field replaced by empty string
                        strTemp = strTemp.Replace("#BizDocType#", "")

                        strTemp = strTemp.Replace("#EmployerBillToAddress#", "")
                        strTemp = strTemp.Replace("#EmployerChangeBillToHeader(Bill To)#", "")
                        strTemp = strTemp.Replace("#EmployerChangeBillToHeader#", "")
                        strTemp = strTemp.Replace("#EmployerBillToAddressName#", "")
                        strTemp = strTemp.Replace("#EmployerBillToCompanyName#", "")
                        strTemp = strTemp.Replace("#EmployerBillToStreet#", "")
                        strTemp = strTemp.Replace("#EmployerBillToCity#", "")
                        strTemp = strTemp.Replace("#EmployerBillToPostal#", "")
                        strTemp = strTemp.Replace("#EmployerBillToState#", "")
                        strTemp = strTemp.Replace("#EmployerBillToCountry#", "")

                        strTemp = strTemp.Replace("#EmployerShipToAddress#", "")
                        strTemp = strTemp.Replace("#EmployerChangeShipToHeader(Ship To)#", "")
                        strTemp = strTemp.Replace("#EmployerChangeShipToHeader#", "")
                        strTemp = strTemp.Replace("#EmployerShipToAddressName#", "")
                        strTemp = strTemp.Replace("#EmployerShipToCompanyName#", "")
                        strTemp = strTemp.Replace("#EmployerShipToStreet#", "")
                        strTemp = strTemp.Replace("#EmployerShipToCity#", "")
                        strTemp = strTemp.Replace("#EmployerShipToPostal#", "")
                        strTemp = strTemp.Replace("#EmployerShipToState#", "")
                        strTemp = strTemp.Replace("#EmployerShipToCountry#", "")

                        strTemp = strTemp.Replace("#BizDocStatus#", "")
                        strTemp = strTemp.Replace("#AmountPaid#", "")
                        strTemp = strTemp.Replace("#AmountPaidPopUp#", "")
                        strTemp = strTemp.Replace("#BalanceDue#", "")
                        strTemp = strTemp.Replace("#BillingTerms#", "")
                        strTemp = strTemp.Replace("#BillingTermsName#", "")
                        strTemp = strTemp.Replace("#BillingTermFromDate#", "")
                        strTemp = strTemp.Replace("#DueDate#", "")
                        strTemp = strTemp.Replace("#ChangeDueDate#", "")
                        strTemp = strTemp.Replace("#BizDocID#", "")
                        strTemp = strTemp.Replace("#BizDocSummary#", "")
                        strTemp = strTemp.Replace("#BizDocCreatedDate#", FormattedDateFromDate(DateTime.Now.Date, CCommon.ToString(System.Web.HttpContext.Current.Session("DateFormat"))))
                        strTemp = strTemp.Replace("#BizDocTemplateName#", "")
                        'strTemp = strTemp.Replace("#Signature#", "")
                        strTemp = strTemp.Replace("#EmailAddress#", "")
                        strTemp = strTemp.Replace("#WatersAvail#", "")
                        strTemp = strTemp.Replace("#TestArea#", "")
                        strTemp = strTemp.Replace("#Enddate#", "")
                        strTemp = strTemp.Replace("#Probabilityofclosure#", "")
                        strTemp = strTemp.Replace("#SalesOppStatus#", "")

                        Dim objCustomFields As New CustomFields
                        If CCommon.ToInteger(drOrder(intRowCnt)("tintOppType")) = 1 Then
                            objCustomFields.locId = 2
                        Else
                            objCustomFields.locId = 6
                        End If
                        objCustomFields.RelId = 0
                        objCustomFields.DomainID = CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))
                        objCustomFields.RecordId = CCommon.ToLong(dtOpp.Rows(intCnt)("numOppID"))
                        Dim dsCus As DataSet = objCustomFields.GetCustFlds
                        For Each drow As DataRow In dsCus.Tables(0).Rows
                            If CCommon.ToLong(drow("numListID")) > 0 Then
                                strTemp = strTemp.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", objCommon.GetListItemValue(CCommon.ToString(drow("Value")), CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))))
                            Else
                                strTemp = strTemp.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", CCommon.ToString(drow("Value")))
                            End If
                        Next

                        Dim dvConfig As DataView = dtConfigFields.DefaultView
                        Dim intSort As Integer = 0

                        dtItem = New DataTable
                        Dim drTemp As DataRow

                        dtItem.Columns.Add("vcDbColumnName")
                        dtItem.Columns.Add("vcFormFieldName")
                        dtItem.Columns.Add("vcFieldDataType")
                        dtItem.AcceptChanges()

                        dvConfig.Sort = "intRowNum"
                        For Each drRow As DataRowView In dvConfig
                            drTemp = dtItem.NewRow
                            If CType(drRow("intColumnNum"), System.Int32) = 1 Then
                                drTemp = dtItem.NewRow
                                drTemp("vcDbColumnName") = CType(drRow("vcDbColumnName"), System.String)
                                drTemp("vcFormFieldName") = CType(drRow("vcFormFieldName"), System.String)
                                drTemp("vcFieldDataType") = CType(drRow("vcFieldDataType"), System.String)
                                dtItem.Rows.Add(drTemp)
                            End If
                        Next
                    Next

                    Dim drData() As DataRow = dt.Select("numOppID = " & CLng(drOrder(0)("numOppID")))

                    If drData.Count > 0 Then
                        strTemp = strTemp.Replace("#Products#", GetBizDocItems(dtItem, drData))

                        strPackingSlip.Append(strTemp)
                        strTemp = Nothing
                        dtItem = Nothing
                    Else
                        strTemp = strTemp.Replace("#Products#", "")
                        Continue For
                    End If


                    Dim objSalesFulfillmentQueue As New SalesFulfillmentQueue
                    objSalesFulfillmentQueue.DomainID = CCommon.ToLong(System.Web.HttpContext.Current.Session("DomainID"))
                    objSalesFulfillmentQueue.SFQID = CCommon.ToLong(dtOpp.Rows(intCnt)("numOppID"))
                    objSalesFulfillmentQueue.Message = "Packing Slip Generated."
                    objSalesFulfillmentQueue.UpdateStatus(6, True)
                Next

                If strPackingSlip Is Nothing OrElse CCommon.ToString(strPackingSlip.ToString).Length = 0 Then
                    Return 102
                End If

                strTemp = strPackingSlip.ToString()
                strTemp = strTemp.Replace("break-after: page", "page-break-after:always")
                '--------------------------------------------------------------------------------------------
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
                pdfConverter.PdfDocumentOptions.LeftMargin = 3
                pdfConverter.PdfDocumentOptions.RightMargin = 3
                pdfConverter.PdfDocumentOptions.TopMargin = 5
                pdfConverter.PdfDocumentOptions.BottomMargin = 5

                pdfConverter.PdfDocumentOptions.ShowHeader = False
                Dim footerText As New TextElement(0, 30, "Page &p; of &P;  ", New System.Drawing.Font(New System.Drawing.FontFamily("Times New Roman"), 10, System.Drawing.GraphicsUnit.Point))
                footerText.TextAlign = HorizontalTextAlign.Right
                footerText.ForeColor = Color.Navy
                footerText.EmbedSysFont = True
                pdfConverter.PdfFooterOptions.AddElement(footerText)
            Catch ex As Exception
                Throw ex
            End Try

            Dim downloadBytes() As Byte '= pdfConverter.GetPdfBytesFromHtmlString(strTemp)
            Dim response As System.Web.HttpResponse '= System.Web.HttpContext.Current.Response

            Dim objStringBuilder As New StringBuilder
            objStringBuilder.Append("<html><head><link rel='stylesheet' href='" & Current.Server.MapPath("~/CSS/master.css") & "' type='text/css' />" & strCss)
            objStringBuilder.Append("</head><body>")
            strTemp = objStringBuilder.ToString() & strTemp & "</body></html>"

            downloadBytes = pdfConverter.ConvertHtml(strTemp, "")
            response = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", ("attachment; filename=" _
                            + (downloadName + ("; size=" + downloadBytes.Length.ToString))))
            response.Flush()
            response.BinaryWrite(downloadBytes)
            response.Flush()
            response.End()

        End Function

        Public Function CreatePackingSlipPDF(ByVal oppID As Long, ByVal oppBizDocID As Long, ByVal lngDomainID As Long, ByVal lngUserCntID As Long, ByVal intClientTimeZoneOffset As Integer, ByVal dateFormat As String) As Byte()
            Try
                Dim objWorkFlowExecution As New Workflow.WorkFlowExecution
                Dim bizDocHtml As String = objWorkFlowExecution.GenerateBizDocPDF(oppBizDocID, oppID, lngDomainID, lngUserCntID, clientTimeZoneOffset:=intClientTimeZoneOffset, dateFormat:=dateFormat)

                Dim pdfConverter As HtmlToPdfConverter = New HtmlToPdfConverter
                pdfConverter.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
                pdfConverter.PdfDocumentOptions.LeftMargin = 3
                pdfConverter.PdfDocumentOptions.RightMargin = 3
                pdfConverter.PdfDocumentOptions.TopMargin = 5
                pdfConverter.PdfDocumentOptions.BottomMargin = 5
                pdfConverter.PdfDocumentOptions.ShowHeader = False
                Dim footerText As New TextElement(0, 30, "Page &p; of &P;  ", New System.Drawing.Font(New System.Drawing.FontFamily("Times New Roman"), 10, System.Drawing.GraphicsUnit.Point))
                footerText.TextAlign = HorizontalTextAlign.Right
                footerText.ForeColor = Color.Navy
                footerText.EmbedSysFont = True
                pdfConverter.PdfFooterOptions.AddElement(footerText)
                Return pdfConverter.ConvertHtml(bizDocHtml, "")
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function CreateBizDocPDF(ByVal oppID As Long, ByVal oppBizDocID As Long, ByVal lngDomainID As Long, ByVal lngUserCntID As Long, ByVal intClientTimeZoneOffset As Integer, ByVal dateFormat As String) As Byte()
            Try
                Dim objWorkFlowExecution As New Workflow.WorkFlowExecution
                Dim bizDocHtml As String = objWorkFlowExecution.GenerateBizDocPDF(oppBizDocID, oppID, lngDomainID, lngUserCntID, clientTimeZoneOffset:=intClientTimeZoneOffset, dateFormat:=dateFormat)

                Try
                    If lngDomainID = 72 Then
                        ExceptionModule.ExceptionPublish(bizDocHtml, lngDomainID, lngUserCntID, Nothing)
                    End If
                Catch ex As Exception

                End Try

                Dim pdfConverter As HtmlToPdfConverter = New HtmlToPdfConverter
                pdfConverter.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
                pdfConverter.PdfDocumentOptions.LeftMargin = 3
                pdfConverter.PdfDocumentOptions.RightMargin = 3
                pdfConverter.PdfDocumentOptions.TopMargin = 5
                pdfConverter.PdfDocumentOptions.BottomMargin = 5
                pdfConverter.PdfDocumentOptions.ShowHeader = False
                Dim footerText As New TextElement(0, 30, "Page &p; of &P;  ", New System.Drawing.Font(New System.Drawing.FontFamily("Times New Roman"), 10, System.Drawing.GraphicsUnit.Point))
                footerText.TextAlign = HorizontalTextAlign.Right
                footerText.ForeColor = Color.Navy
                footerText.EmbedSysFont = True
                pdfConverter.PdfFooterOptions.AddElement(footerText)
                Return pdfConverter.ConvertHtml(bizDocHtml, "")
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Function GetBizDocItems(ByVal dtItem As DataTable, ByVal drData() As DataRow) As String
            Dim stringBuilder As New StringBuilder
            stringBuilder.Append("<table style=""background-color:White;border-style:None;font-size:10px;width:100%;border-collapse:collapse;"">")
            Dim i As Integer
            Dim j As Integer
            Dim dtdgColumns As DataTable
            dtdgColumns = dtItem

            If dtdgColumns.Rows.Count > 0 Then
                'Generates Header Row
                stringBuilder.Append("<tr class=""ItemHeader"">")
                For i = 0 To dtdgColumns.Rows.Count - 1
                    If CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "txtItemDesc" And dtdgColumns.Rows.Count = 2 Then
                        stringBuilder.Append("<td class=""TwoColumns"" style=""text-align:center"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                    ElseIf CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "txtItemDesc" And dtdgColumns.Rows.Count = 1 Then
                        stringBuilder.Append("<td class=""OneColumns"" style=""text-align:center"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                    ElseIf CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "txtItemDesc" And dtdgColumns.Rows.Count = 3 Then
                        stringBuilder.Append("<td class=""ThreeColumns"" style=""text-align:center"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                    ElseIf CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "txtItemDesc" And dtdgColumns.Rows.Count > 3 Then
                        stringBuilder.Append("<td class=""WordWrapSerialNo"" style=""text-align:center"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                    ElseIf CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "SerialLotNo" Or CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "vcAttributes" Then
                        stringBuilder.Append("<td class=""WordWrapSerialNo"" style=""text-align:center"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                    Else
                        stringBuilder.Append("<td style=""text-align:center"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                    End If
                Next

                stringBuilder.Append("</tr>")

                'Generates Data Rows
                'Dim dtOppBiDocItems As DataTable = dt

                For j = 0 To drData.Count - 1
                    If j Mod 2 = 0 Then
                        stringBuilder.Append("<tr style=""page-break-inside : avoid"" class=""ItemStyle"">")
                    Else
                        stringBuilder.Append("<tr style=""page-break-inside : avoid"" class=""AltItemStyle"">")
                    End If

                    For Each drColumn As DataRow In dtdgColumns.Rows
                        If CCommon.ToString(drColumn("vcDbColumnName")) = "txtItemDesc" Then
                            stringBuilder.Append("<td class=""BizDocItemDesc"">")
                        ElseIf CCommon.ToString(drColumn("vcDbColumnName")) = "SerialLotNo" Or CCommon.ToString(drColumn("vcDbColumnName")) = "vcAttributes" Then
                            stringBuilder.Append("<td class=""WordWrapSerialNo"">")
                        Else
                            stringBuilder.Append("<td>")
                        End If

                        If CCommon.ToString(drColumn("vcFieldDataType")) = "M" Then
                            stringBuilder.Append(String.Format(CCommon.GetDataFormatString(), drData(j)(CCommon.ToString(drColumn("vcDbColumnName")))))
                        Else
                            stringBuilder.Append(drData(j)(CCommon.ToString(drColumn("vcDbColumnName"))))
                        End If

                        stringBuilder.Append("</td>")
                    Next
                    stringBuilder.Append("</tr>")
                Next
            End If

            stringBuilder.Append("</table>")

            Return stringBuilder.ToString()
        End Function

        Function ReturnMoney(ByVal Money As Object) As String
            Try
                Return String.Format("{0:#,###.##}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function RemovePRETag(ByVal strOrgString As String) As String
            Dim strResult As String = ""
            Try
                If strOrgString.Contains("<pre></pre> ,") = True Then strOrgString = strOrgString.Replace("<pre></pre> ,", " ")
                If strOrgString.Contains("<pre></pre>") = True Then strOrgString = strOrgString.Replace("<pre></pre>", " ")
                If strOrgString.Contains("<pre>") = True Then strOrgString = strOrgString.Replace("<pre>", " ")
                If strOrgString.Contains("</pre>") = True Then strOrgString = strOrgString.Replace("</pre>", ",")

                strResult = strOrgString
            Catch ex As Exception
                Throw ex
            End Try
            Return strResult
        End Function

        Public Sub CreatePickListPDF(ByVal dt As DataTable)
            Try
                Dim domainID As Long = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                Dim document As New iTextSharp.text.Document(PageSize.A4, 1, 1, 1, 1)
                document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate())

                ' '' Create a new PdfWrite object, writing the output to a MemoryStream
                Dim output As New MemoryStream()
                Dim writer As PdfWriter
                writer = PdfWriter.GetInstance(document, output)

                ' '' Open the Document for writing
                document.Open()
                Dim titleFont As iTextSharp.text.Font = FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD)
                Dim title As New Paragraph("Pick List", titleFont)
                title.Alignment = Element.ALIGN_CENTER
                document.Add(title)

                document.Add(New Paragraph(" "))

                Dim table As New PdfPTable(7)
                table.WidthPercentage = 99

                Dim widths As Integer() = New Integer() {62, 100, 100, 100, 45, 80, 80}
                table.SetWidths(widths)

                Dim cell As PdfPCell = New PdfPCell(New Phrase("Image"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                cell.MinimumHeight = 30
                table.AddCell(cell)

                cell = New PdfPCell(New Phrase("Item Name (Model ID)"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                table.AddCell(cell)

                cell = New PdfPCell(New Phrase("Description"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                table.AddCell(cell)

                cell = New PdfPCell(New Phrase("Warehouse, Location"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                table.AddCell(cell)

                cell = New PdfPCell(New Phrase("Qty to Pick"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                table.AddCell(cell)

                cell = New PdfPCell(New Phrase("SKU"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                table.AddCell(cell)

                cell = New PdfPCell(New Phrase("UPC"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                table.AddCell(cell)

                For Each dr As DataRow In dt.Rows
                    Dim cellImage As New PdfPCell()

                    Dim cellItem As New PdfPCell()
                    cellItem.VerticalAlignment = Element.ALIGN_MIDDLE
                    cellItem.PaddingTop = 10
                    cellItem.PaddingBottom = 10
                    cellItem.UseAscender = True

                    Dim cellDescription As New PdfPCell()

                    Dim cellWarehouse As New PdfPCell()
                    cellWarehouse.VerticalAlignment = Element.ALIGN_MIDDLE
                    cellWarehouse.UseAscender = True

                    Dim cellQty As New PdfPCell()
                    cellQty.VerticalAlignment = Element.ALIGN_MIDDLE
                    cellQty.UseAscender = True

                    Dim cellSKU As New PdfPCell()
                    cellSKU.VerticalAlignment = Element.ALIGN_MIDDLE
                    cellSKU.UseAscender = True

                    Dim cellUPC As New PdfPCell()
                    cellUPC.VerticalAlignment = Element.ALIGN_MIDDLE
                    cellUPC.UseAscender = True

                    If Not String.IsNullOrWhiteSpace(CCommon.ToString(dr("vcImage"))) Then
                        Try
                            Dim url As String = CCommon.GetDocumentPath(domainID) & CCommon.ToString(dr("vcImage"))
                            Dim img As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(url))
                            img.ScaleToFit(60.0F, 60.0F)
                            img.Alignment = Element.ALIGN_CENTER
                            cellImage.AddElement(img)
                        Catch ex As Exception
                            'DO NOT THROW EXCEPTION
                        End Try
                    End If


                    cellItem.AddElement(New Paragraph(CCommon.ToString(dr("vcItemName")) & If(Not String.IsNullOrWhiteSpace(CCommon.ToString(dr("vcModelID"))), " (" & CCommon.ToString(dr("vcModelID")) & ")", "")) With {.Alignment = Element.ALIGN_CENTER})
                    cellDescription.AddElement(New Phrase(CCommon.ToString(dr("txtItemDesc"))))

                    cellWarehouse.AddElement(New Paragraph(CCommon.ToString(dr("vcWareHouse")) & If(Not String.IsNullOrWhiteSpace(CCommon.ToString(dr("vcLocation"))), "," & CCommon.ToString(dr("vcLocation")), "")) With {.Alignment = Element.ALIGN_CENTER})


                    cellQty.AddElement(New Paragraph(CCommon.ToString(dr("numUnitHour"))) With {.Alignment = Element.ALIGN_CENTER})
                    cellSKU.AddElement(New Paragraph(CCommon.ToString(dr("vcSKU"))) With {.Alignment = Element.ALIGN_CENTER})
                    cellUPC.AddElement(New Paragraph(CCommon.ToString(dr("vcUPC"))) With {.Alignment = Element.ALIGN_CENTER})

                    table.AddCell(cellImage)
                    table.AddCell(cellItem)
                    table.AddCell(cellDescription)
                    table.AddCell(cellWarehouse)
                    table.AddCell(cellQty)
                    table.AddCell(cellSKU)
                    table.AddCell(cellUPC)
                Next

                document.Add(table)
                document.Close()

                HttpContext.Current.Response.ContentType = "application/pdf"
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=PickList.pdf")
                HttpContext.Current.Response.BinaryWrite(output.ToArray())
                HttpContext.Current.Response.Flush()
                HttpContext.Current.Response.End()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub CreateGroupListPDF(ByVal dt As DataTable)
            Try
                Dim domainID As Long = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                Dim document As New iTextSharp.text.Document(PageSize.A4, 1, 1, 1, 1)
                document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate())

                ' '' Create a new PdfWrite object, writing the output to a MemoryStream
                Dim output As New MemoryStream()
                Dim writer As PdfWriter
                writer = PdfWriter.GetInstance(document, output)

                ' '' Open the Document for writing
                document.Open()
                Dim titleFont As iTextSharp.text.Font = FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD)
                Dim title As New Paragraph("Group List", titleFont)
                title.Alignment = Element.ALIGN_CENTER
                document.Add(title)

                document.Add(New Paragraph(" "))

                Dim table As New PdfPTable(5)
                table.WidthPercentage = 99

                Dim widths As Integer() = New Integer() {62, 150, 45, 145, 145}
                table.SetWidths(widths)

                Dim cell As PdfPCell = New PdfPCell(New Phrase("Image"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                cell.MinimumHeight = 30
                table.AddCell(cell)

                cell = New PdfPCell(New Phrase("Item Name (Model ID)"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                table.AddCell(cell)

                cell = New PdfPCell(New Phrase("Qty Ordered"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                table.AddCell(cell)

                cell = New PdfPCell(New Phrase("Fullfillment Order Barcode"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                table.AddCell(cell)

                cell = New PdfPCell(New Phrase("SKU Barcode"))
                cell.BackgroundColor = BaseColor.LIGHT_GRAY
                cell.HorizontalAlignment = Element.ALIGN_CENTER
                cell.VerticalAlignment = Element.ALIGN_MIDDLE
                table.AddCell(cell)

                For Each dr As DataRow In dt.Rows
                    Dim cellImage As New PdfPCell()
                    cellImage.VerticalAlignment = Element.ALIGN_MIDDLE

                    Dim cellItem As New PdfPCell()
                    cellItem.VerticalAlignment = Element.ALIGN_MIDDLE
                    cellItem.PaddingTop = 10
                    cellItem.PaddingBottom = 10
                    cellItem.UseAscender = True

                    Dim cellQty As New PdfPCell()
                    cellQty.VerticalAlignment = Element.ALIGN_MIDDLE
                    cellQty.UseAscender = True

                    Dim cellFullfillmentOrder As New PdfPCell()
                    cellFullfillmentOrder.VerticalAlignment = Element.ALIGN_MIDDLE
                    cellFullfillmentOrder.UseAscender = True

                    Dim cellSKU As New PdfPCell()
                    cellSKU.VerticalAlignment = Element.ALIGN_MIDDLE
                    cellSKU.UseAscender = True

                    If Not String.IsNullOrWhiteSpace(CCommon.ToString(dr("vcImage"))) Then
                        Try
                            Dim url As String = CCommon.GetDocumentPath(domainID) & CCommon.ToString(dr("vcImage"))
                            Dim img As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(New Uri(url))
                            img.ScaleToFit(60.0F, 60.0F)
                            img.Alignment = Element.ALIGN_CENTER
                            cellImage.AddElement(img)
                        Catch ex As Exception
                            'DO NOT THROW EXCEPTION
                        End Try
                    End If

                    cellItem.AddElement(New Paragraph(CCommon.ToString(dr("vcItemName")) & If(Not String.IsNullOrWhiteSpace(CCommon.ToString(dr("vcModelID"))), " (" & CCommon.ToString(dr("vcModelID")) & ")", "")) With {.Alignment = Element.ALIGN_CENTER})
                    cellQty.AddElement(New Paragraph(CCommon.ToString(dr("numUnitHour"))) With {.Alignment = Element.ALIGN_CENTER})

                    If Not String.IsNullOrWhiteSpace(CCommon.ToString(dr("numOppBizDocsId"))) Then
                        Dim barImg As New Barcode39()
                        barImg.Code = CCommon.ToString(dr("numOppBizDocsId"))
                        barImg.GuardBars = True
                        Dim cb As PdfContentByte = writer.DirectContent
                        cellFullfillmentOrder.Padding = 10
                        cellFullfillmentOrder.AddElement(barImg.CreateImageWithBarcode(cb, Nothing, Nothing))
                    End If

                    If Not String.IsNullOrWhiteSpace(CCommon.ToString(dr("vcSKU"))) Then
                        Dim barImg As New Barcode39()
                        barImg.Code = CCommon.ToString(dr("vcSKU"))
                        barImg.GuardBars = True
                        Dim cb As PdfContentByte = writer.DirectContent
                        cellSKU.Padding = 10
                        cellSKU.AddElement(barImg.CreateImageWithBarcode(cb, Nothing, Nothing))
                    End If

                    table.AddCell(cellImage)
                    table.AddCell(cellItem)
                    table.AddCell(cellQty)
                    table.AddCell(cellFullfillmentOrder)
                    table.AddCell(cellSKU)
                Next

                document.Add(table)
                document.Close()

                HttpContext.Current.Response.ContentType = "application/pdf"
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=GroupList.pdf")
                HttpContext.Current.Response.BinaryWrite(output.ToArray())
                HttpContext.Current.Response.Flush()
                HttpContext.Current.Response.End()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub ConvertHTML2PDF(ByVal domainID As Long, ByVal header As String, ByVal strHTML As String, ByVal footer As String, ByVal name As String)
            Try
                Dim pdfConverter As New HtmlToPdfConverter
                pdfConverter.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))
                Dim FilePath As String = CCommon.GetDocumentPhysicalPath(domainID)
                If Directory.Exists(FilePath) = False Then
                    Directory.CreateDirectory(FilePath)
                End If

                FilePath = FilePath & name

                Dim headerHtml As New HtmlToPdfElement(header, "")
                Dim footerHtml As New HtmlToPdfElement(footer, "")

                pdfConverter.PdfHeaderOptions.AddElement(headerHtml)
                pdfConverter.PdfDocumentOptions.ShowHeader = True

                If Not String.IsNullOrWhiteSpace(footer) Then
                    pdfConverter.PdfFooterOptions.AddElement(footerHtml)
                    pdfConverter.PdfDocumentOptions.ShowFooter = True
                    pdfConverter.PdfDocumentOptions.BottomMargin = 60
                End If

                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait

                pdfConverter.PdfDocumentOptions.StretchToFit = True
                pdfConverter.PdfDocumentOptions.BottomMargin = 10
                pdfConverter.PdfDocumentOptions.TopMargin = 10
                pdfConverter.PdfDocumentOptions.LeftMargin = 10
                pdfConverter.PdfDocumentOptions.RightMargin = 10
                pdfConverter.ConvertHtmlToFile(strHTML, "", FilePath)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ConvertHtmlToPDFAndSaveToPath(ByVal html As String, ByVal path As String)
            Try
                Dim pdfConverter As HtmlToPdfConverter = New HtmlToPdfConverter
                pdfConverter.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait
                pdfConverter.PdfDocumentOptions.StretchToFit = True
                pdfConverter.PdfDocumentOptions.BottomMargin = 5
                pdfConverter.PdfDocumentOptions.TopMargin = 5
                pdfConverter.PdfDocumentOptions.LeftMargin = 3
                pdfConverter.PdfDocumentOptions.RightMargin = 3
                pdfConverter.ConvertHtmlToFile(html, "", path)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function ConvertHtmlToPDF(ByVal html As String) As Byte()
            Try
                Dim pdfConverter As HtmlToPdfConverter = New HtmlToPdfConverter
                pdfConverter.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))
                pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.Letter
                pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal
                pdfConverter.PdfDocumentOptions.PdfPageOrientation = PdfPageOrientation.Portrait
                pdfConverter.PdfDocumentOptions.StretchToFit = True
                pdfConverter.PdfDocumentOptions.BottomMargin = 5
                pdfConverter.PdfDocumentOptions.TopMargin = 5
                pdfConverter.PdfDocumentOptions.LeftMargin = 3
                pdfConverter.PdfDocumentOptions.RightMargin = 3
                Return pdfConverter.ConvertHtml(html, "")
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Shared Sub CombineMultiplePDFs(fileNames As String(), outFile As String)
            ' step 1: creation of a document-object
            Dim document As New iTextSharp.text.Document()

            ' step 2: we create a writer that listens to the document
            Dim writer As New PdfCopy(document, New FileStream(outFile, FileMode.Create))
            If writer Is Nothing Then
                Return
            End If

            ' step 3: we open the document
            document.Open()

            For Each fileName As String In fileNames
                ' we create a reader for a certain document
                Dim reader As New PdfReader(fileName)
                reader.ConsolidateNamedDestinations()

                ' step 4: we add content
                For i As Integer = 1 To reader.NumberOfPages
                    Dim page As PdfImportedPage = writer.GetImportedPage(reader, i)
                    writer.AddPage(page)
                Next

                reader.Close()
            Next

            ' step 5: we close the document and writer
            writer.Close()
            document.Close()
        End Sub

        Public Function CreatePickListPDFMassSalesFulfillment(ByVal isPickListByOrder As Boolean, ByVal batchName As String, ByVal timeZoneOffset As Integer, ByVal dt As DataTable) As Byte()
            Try
                Dim pdfDocument As New Winnovative.Document
                pdfDocument.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))

                If isPickListByOrder Then
                    Dim view As New DataView(dt)
                    Dim tblOpp As DataTable = view.ToTable(True, "numOppID")

                    For Each dr As DataRow In tblOpp.Rows
                        Dim html As String = GetPickListHtml(isPickListByOrder, batchName, timeZoneOffset, dt.Select("numOppID=" & CCommon.ToString(dr("numOppID"))).CopyToDataTable())
                        Dim pdfPage As Winnovative.PdfPage = pdfDocument.AddPage(PdfPageSize.Letter, New Winnovative.Margins(3, 3, 5, 5))
                        pdfPage.ShowHeader = False
                        pdfPage.ShowFooter = True

                        pdfPage.AddElement(New HtmlToPdfElement(html, ""))
                    Next
                Else
                    Dim html As String = GetPickListHtml(isPickListByOrder, batchName, timeZoneOffset, dt)
                    Dim pdfPage As Winnovative.PdfPage = pdfDocument.AddPage(PdfPageSize.Letter, New Winnovative.Margins(3, 3, 5, 5))
                    pdfPage.ShowHeader = False
                    pdfPage.ShowFooter = True

                    pdfPage.AddElement(New HtmlToPdfElement(html, ""))
                End If

                If pdfDocument.Pages.Count > 0 Then
                    pdfDocument.AddFooterTemplate(40)
                    Dim footerText As New TextElement(0, 30, "Page &p; of &P;  ", New System.Drawing.Font(New System.Drawing.FontFamily("Times New Roman"), 10, System.Drawing.GraphicsUnit.Point))
                    footerText.TextAlign = HorizontalTextAlign.Right
                    footerText.ForeColor = Color.Navy
                    footerText.EmbedSysFont = True
                    pdfDocument.Footer.AddElement(footerText)
                End If

                Return pdfDocument.Save()
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Function GetPickListHtml(ByVal isPickListByOrder As Boolean, ByVal batchName As String, ByVal timeZoneOffset As Integer, ByVal dt As DataTable) As String
            Try
                Dim html As String = "<!DOCTYPE html><html lang='en' xmlns='http://www.w3.org/1999/xhtml'><head><title></title><style type='text/css'>body{font-family: sans-serif;font-size: 14px; } #tblTitle, #tblItems { width: 100%; border-spacing: 0px; } .title-left { text-align: left; font-size: 18px; } .title-center { text-align: center; } .title-right { text-align: right; font-size: 14px; } #tblItems thead tr th { border-width: 2px 0px 2px 0px; border-color: black; border-style: solid; font-size: 14px; padding: 10px; page-break-inside: avoid; } #tblItems tbody tr td { border-width: 0px 0px 1px 0px; border-color: black; border-style: solid; padding: 10px; page-break-inside: avoid; } .thImage { width: 80px; } .thItemName { width: 60%; } .thInclusion { width: 40%; } .thWarehouse { white-space: nowrap; } .thQty { white-space: nowrap; } .tdImage { white-space: nowrap; } .tdQty { white-space: nowrap; text-align:right } </style></head>"
                html += "<table id='tblTitle'>"
                html += "<tr>"
                html += "<td class='title-left'><b>Pick List</b></td>"
                If isPickListByOrder AndAlso dt.Rows.Count > 0 Then
                    html += "<td class='title-center'><b>Order Name:</b> " & CCommon.ToString(dt.Rows(0)("vcOppName")) & "</td>"
                End If
                html += "<td class='title-center'><b>Batch Name:</b> " & batchName & "</td>"
                html += "<td class='title-right'>" & DateTime.UtcNow.AddMinutes(-1 * timeZoneOffset).ToString("dddd, MMMM dd, yyyy hh:mm tt") & "</td>"
                html += "</tr>"
                html += "</table>"
                html += "<br />"
                html += "<br />"
                html += "<table id='tblItems'>"
                html += "<thead>"
                html += "<tr>"
                html += "<th class='thImage'>Image</th>"
                html += "<th class='thItemName'>Item Name</th>"
                html += "<th class='thInclusion'>Inclusion Detail</th>"
                html += "<th class='thWarehouse'>Warehouse, Location</th>"
                html += "<th class='thQty'>Qty to Pick</th>"
                html += "</tr>"
                html += "</thead>"
                html += "<tbody>"

                If dt.Rows.Count > 0 Then
                    For Each dr As DataRow In dt.Rows
                        html += "<tr>"
                        html += "<td class='tdImage'><img src='" & CCommon.GetDocumentPath(DomainID) & CCommon.ToString(dr("vcImage")) & "' height='60' /></td>"
                        html += "<td>" & CCommon.ToString(dr("vcItemName")) & "<br />"
                        html += "<p style='font-size: 12px;'><b>SKU:</b>" & CCommon.ToString(dr("vcSKU")) & " <b>UPC:</b>" & CCommon.ToString(dr("vcUPC")) & " <b>Attributes:</b>" & CCommon.ToString(dr("vcAttributes")) & "</p>"
                        html += "</td>"
                        html += "<td>" & CCommon.ToString(dr("vcInclusionDetail")) & "</td>"
                        html += "<td>" & CCommon.ToString(dr("vcLocation")) & "</td>"
                        html += "<td class='tdQty'>" & CCommon.ToString(dr("numUnitHour")) & "</td>"
                        html += "</tr>"
                    Next
                End If

                html += "</tbody>"
                html += "</table>"
                html += "</body></html>"
                Return html
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Function GetCell(ByVal text As String, ByVal font As iTextSharp.text.Font, ByVal padding As Double, ByVal halignment As Integer, ByVal valignment As Integer) As PdfPCell
            Dim cell As New PdfPCell(New Phrase(text, font))
            cell.Padding = CSng(padding)
            cell.HorizontalAlignment = halignment
            cell.VerticalAlignment = valignment
            cell.Border = PdfPCell.NO_BORDER
            Return cell
        End Function

        Public Function PrintBizDocMassSalesFulfillment(ByVal dt As DataTable, ByVal lngDomainID As Long, ByVal lngUserCntID As Long, ByVal intClientTimeZoneOffset As Integer, ByVal dateFormat As String) As Byte()
            Try
                Dim pdfDocument As New Winnovative.Document
                pdfDocument.LicenseKey = CCommon.ToString(ConfigurationManager.AppSettings("WinnovativeLicense"))


                For Each dr As DataRow In dt.Rows
                    Dim objWorkFlowExecution As New Workflow.WorkFlowExecution
                    Dim bizDocHtml As String = objWorkFlowExecution.GenerateBizDocPDF(CCommon.ToLong(dr("OppBizDocID")), CCommon.ToLong(dr("OppID")), lngDomainID, lngUserCntID, clientTimeZoneOffset:=intClientTimeZoneOffset, dateFormat:=dateFormat)

                    Dim pdfPage As Winnovative.PdfPage = pdfDocument.AddPage(PdfPageSize.Letter, New Winnovative.Margins(3, 3, 5, 5))
                    pdfPage.ShowHeader = False
                    pdfPage.ShowFooter = True

                    pdfPage.AddElement(New HtmlToPdfElement(bizDocHtml, ""))
                Next

                If pdfDocument.Pages.Count > 0 Then
                    pdfDocument.AddFooterTemplate(40)
                    Dim footerText As New TextElement(0, 30, "Page &p; of &P;  ", New System.Drawing.Font(New System.Drawing.FontFamily("Times New Roman"), 10, System.Drawing.GraphicsUnit.Point))
                    footerText.TextAlign = HorizontalTextAlign.Right
                    footerText.ForeColor = Color.Navy
                    footerText.EmbedSysFont = True
                    pdfDocument.Footer.AddElement(footerText)
                End If

                Return pdfDocument.Save()
            Catch ex As Exception
                Throw
            End Try
        End Function

#Region "Printing Properties"

        'Check Date
        Private _CheckDateFontFamily As String
        Public Property CheckDateFontFamily() As String
            Get
                Return _CheckDateFontFamily
            End Get
            Set(ByVal value As String)
                _CheckDateFontFamily = value
            End Set
        End Property
        Private _CheckDateFontSize As Integer
        Public Property CheckDateFontSize() As Integer
            Get
                Return _CheckDateFontSize
            End Get
            Set(ByVal value As Integer)
                _CheckDateFontSize = value
            End Set
        End Property
        Private _CheckDateFontStyle As Integer
        Public Property CheckDateFontStyle() As Integer
            Get
                Return _CheckDateFontStyle
            End Get
            Set(ByVal value As Integer)
                _CheckDateFontStyle = value
            End Set
        End Property
        Private _CheckDateLeft As Double
        Public Property CheckDateLeft() As Double
            Get
                Return _CheckDateLeft
            End Get
            Set(ByVal value As Double)
                _CheckDateLeft = value
            End Set
        End Property
        Private _CheckDateTop As Double
        Public Property CheckDateTop() As Double
            Get
                Return _CheckDateTop
            End Get
            Set(ByVal value As Double)
                _CheckDateTop = value
            End Set
        End Property

        'Payee Name
        Private _PayeeNameFontFamily As String
        Public Property PayeeNameFontFamily() As String
            Get
                Return _PayeeNameFontFamily
            End Get
            Set(ByVal value As String)
                _PayeeNameFontFamily = value
            End Set
        End Property
        Private _PayeeNameFontSize As Integer
        Public Property PayeeNameFontSize() As Integer
            Get
                Return _PayeeNameFontSize
            End Get
            Set(ByVal value As Integer)
                _PayeeNameFontSize = value
            End Set
        End Property
        Private _PayeeNameFontStyle As Integer
        Public Property PayeeNameFontStyle() As Integer
            Get
                Return _PayeeNameFontStyle
            End Get
            Set(ByVal value As Integer)
                _PayeeNameFontStyle = value
            End Set
        End Property
        Private _PayeeNameLeft As Double
        Public Property PayeeNameLeft() As Double
            Get
                Return _PayeeNameLeft
            End Get
            Set(ByVal value As Double)
                _PayeeNameLeft = value
            End Set
        End Property
        Private _PayeeNameTop As Double
        Public Property PayeeNameTop() As Double
            Get
                Return _PayeeNameTop
            End Get
            Set(ByVal value As Double)
                _PayeeNameTop = value
            End Set
        End Property

        'Amount
        Private _AmountFontFamily As String
        Public Property AmountFontFamily() As String
            Get
                Return _AmountFontFamily
            End Get
            Set(ByVal value As String)
                _AmountFontFamily = value
            End Set
        End Property
        Private _AmountFontSize As Integer
        Public Property AmountFontSize() As Integer
            Get
                Return _AmountFontSize
            End Get
            Set(ByVal value As Integer)
                _AmountFontSize = value
            End Set
        End Property
        Private _AmountFontStyle As Integer
        Public Property AmountFontStyle() As Integer
            Get
                Return _AmountFontStyle
            End Get
            Set(ByVal value As Integer)
                _AmountFontStyle = value
            End Set
        End Property
        Private _AmountLeft As Double
        Public Property AmountLeft() As Double
            Get
                Return _AmountLeft
            End Get
            Set(ByVal value As Double)
                _AmountLeft = value
            End Set
        End Property
        Private _AmountTop As Double
        Public Property AmountTop() As Double
            Get
                Return _AmountTop
            End Get
            Set(ByVal value As Double)
                _AmountTop = value
            End Set
        End Property

        'Amount In Words
        Private _AmountInWordsFontFamily As String
        Public Property AmountInWordsFontFamily() As String
            Get
                Return _AmountInWordsFontFamily
            End Get
            Set(ByVal value As String)
                _AmountInWordsFontFamily = value
            End Set
        End Property
        Private _AmountInWordsFontSize As Integer
        Public Property AmountInWordsFontSize() As Integer
            Get
                Return _AmountInWordsFontSize
            End Get
            Set(ByVal value As Integer)
                _AmountInWordsFontSize = value
            End Set
        End Property
        Private _AmountInWordsFontStyle As Integer
        Public Property AmountInWordsFontStyle() As Integer
            Get
                Return _AmountInWordsFontStyle
            End Get
            Set(ByVal value As Integer)
                _AmountInWordsFontStyle = value
            End Set
        End Property
        Private _AmountInWordsLeft As Double
        Public Property AmountInWordsLeft() As Double
            Get
                Return _AmountInWordsLeft
            End Get
            Set(ByVal value As Double)
                _AmountInWordsLeft = value
            End Set
        End Property
        Private _AmountInWordsTop As Double
        Public Property AmountInWordsTop() As Double
            Get
                Return _AmountInWordsTop
            End Get
            Set(ByVal value As Double)
                _AmountInWordsTop = value
            End Set
        End Property

        'EmployerName
        Private _EmployerNameFontFamily As String
        Public Property EmployerNameFontFamily() As String
            Get
                Return _EmployerNameFontFamily
            End Get
            Set(ByVal value As String)
                _EmployerNameFontFamily = value
            End Set
        End Property
        Private _EmployerNameFontSize As Integer
        Public Property EmployerNameFontSize() As Integer
            Get
                Return _EmployerNameFontSize
            End Get
            Set(ByVal value As Integer)
                _EmployerNameFontSize = value
            End Set
        End Property
        Private _EmployerNameFontStyle As Integer
        Public Property EmployerNameFontStyle() As Integer
            Get
                Return _EmployerNameFontStyle
            End Get
            Set(ByVal value As Integer)
                _EmployerNameFontStyle = value
            End Set
        End Property
        Private _EmployerNameLeft As Double
        Public Property EmployerNameLeft() As Double
            Get
                Return _EmployerNameLeft
            End Get
            Set(ByVal value As Double)
                _EmployerNameLeft = value
            End Set
        End Property
        Private _EmployerNameTop As Double
        Public Property EmployerNameTop() As Double
            Get
                Return _EmployerNameTop
            End Get
            Set(ByVal value As Double)
                _EmployerNameTop = value
            End Set
        End Property

        'Address
        Private _AddressFontFamily As String
        Public Property AddressFontFamily() As String
            Get
                Return _AddressFontFamily
            End Get
            Set(ByVal value As String)
                _AddressFontFamily = value
            End Set
        End Property
        Private _AddressFontSize As Integer
        Public Property AddressFontSize() As Integer
            Get
                Return _AddressFontSize
            End Get
            Set(ByVal value As Integer)
                _AddressFontSize = value
            End Set
        End Property
        Private _AddressFontStyle As Integer
        Public Property AddressFontStyle() As Integer
            Get
                Return _AddressFontStyle
            End Get
            Set(ByVal value As Integer)
                _AddressFontStyle = value
            End Set
        End Property
        Private _AddressLeft As Double
        Public Property AddressLeft() As Double
            Get
                Return _AddressLeft
            End Get
            Set(ByVal value As Double)
                _AddressLeft = value
            End Set
        End Property
        Private _AddressTop As Double
        Public Property AddressTop() As Double
            Get
                Return _AddressTop
            End Get
            Set(ByVal value As Double)
                _AddressTop = value
            End Set
        End Property

        Private _EmployerNameIncluded As Short
        Public Property EmployerNameIncluded() As Short
            Get
                Return _EmployerNameIncluded
            End Get
            Set(ByVal value As Short)
                _EmployerNameIncluded = value
            End Set
        End Property
        Private _AddressIncluded As Short
        Public Property AddressIncluded() As Short
            Get
                Return _AddressIncluded
            End Get
            Set(ByVal value As Short)
                _AddressIncluded = value
            End Set
        End Property

        Public Property Stub1Top As Double
        Public Property Stub1Left As Double
        Public Property Stub2Top As Double
        Public Property Stub2Left As Double
#End Region

    End Class

End Namespace
