﻿'Modified by Anoop Jayaraj

Option Explicit On
Option Strict On

Imports System.Web.UI.WebControls
Imports System.Web.UI
Imports System.Web.UI.HtmlControls
Imports System.Reflection
Imports System.Web.UI.UserControl
Imports System.Web.UI.Control
Imports BACRM.BusinessLogic.Admin
Imports Infragistics.Web.UI.LayoutControls.WebTab
Imports BACRM.BusinessLogic.Contacts
Imports System.Web
Imports System.Text
Imports Telerik.Web.UI

Namespace BACRM.BusinessLogic.Common
    ''' <summary>
    ''' This Class Handles the creation of controls in the UI Layer
    ''' </summary>
    ''' <remarks></remarks>
    Public Class PageControls
        Inherits System.Web.UI.Page

        Private tblCell As TableCell
        Private tblRow As TableRow
        Private ddl As DropDownList
        Private radcmb As RadComboBox
        Private lst As ListBox
        Private txt As TextBox
        Private chk As CheckBox
        Private hpl As HyperLink
        Private btn As Button
        Private chkbl As CheckBoxList
        Dim objCustomFields As New CustomFields
        Dim dtCustomFields As DataTable



        Enum Location As Short
            Organization = 1 'for Lead/Prospects/Accounts Shared Detail section
            SalesOppotunity = 2
            Support = 3
            Contact = 4
            PurchaseOppotunity = 6
            Projects = 11
            Prospects = 12
            Accounts = 13
            Leads = 14
            ActionItemDetail = 128 'Added to differentiate they way custom field works.
        End Enum

        Private _DivisionID As Long
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Private _ContactID As Long = 0
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Private _TerritoryID As Long = 0
        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property

        Private _strPageType As String
        Public Property strPageType() As String
            Get
                Return _strPageType
            End Get
            Set(ByVal Value As String)
                _strPageType = Value
            End Set
        End Property

        Private _EditPermission As Integer
        Public Property EditPermission() As Integer
            Get
                Return _EditPermission
            End Get
            Set(ByVal Value As Integer)
                _EditPermission = Value
            End Set
        End Property

        Private _ProjectID As Long = 0
        Public Property ProjectID() As Long
            Get
                Return _ProjectID
            End Get
            Set(ByVal Value As Long)
                _ProjectID = Value
            End Set
        End Property

        Private _OpportunityId As Long = 0
        Public Property OpportunityId() As Long
            Get
                Return _OpportunityId
            End Get
            Set(ByVal Value As Long)
                _OpportunityId = Value
            End Set
        End Property

        Private _CaseId As Long = 0
        Public Property CaseId() As Long
            Get
                Return _CaseId
            End Get
            Set(ByVal Value As Long)
                _CaseId = Value
            End Set
        End Property

        Private _WorkOrderId As Long = 0
        Public Property WorkOrderId() As Long
            Get
                Return _WorkOrderId
            End Get
            Set(ByVal Value As Long)
                _WorkOrderId = Value
            End Set
        End Property

        Public Property CustomFieldsTable() As DataTable
            Get
                Return dtCustomFields
            End Get
            Set(ByVal Value As DataTable)
                dtCustomFields = Value
            End Set
        End Property

        Public Sub New()

        End Sub


        Public Sub CreateTemplateRow(ByRef tbl As Table)
            tblRow = New TableRow
            tblCell = New TableCell
            tblCell.Width = Unit.Percentage(20)
            tblRow.Cells.Add(tblCell)
            tblCell = New TableCell
            tblCell.Width = Unit.Percentage(30)
            tblRow.Cells.Add(tblCell)
            tblCell = New TableCell
            tblCell.Width = Unit.Percentage(20)
            tblRow.Cells.Add(tblCell)
            tblCell = New TableCell
            tblCell.Width = Unit.Percentage(30)
            tblRow.Cells.Add(tblCell)
            tbl.Rows.Add(tblRow)
        End Sub






        'Dim lbl As Label
        Public Function CreateCells(ByVal Row As TableRow, ByVal dr As DataRow, ByVal IntermediatoryPage As Boolean, Optional ByVal dtSource As DataTable = Nothing, Optional ByVal dtSel As DataTable = Nothing, Optional ByVal RecordID As Long = 0, Optional ByVal boolEnabled As Boolean = True, Optional ByVal lngCaseID As Long = 0, Optional ByVal boolAdd As Boolean = False) As Object

            tblCell = New TableCell
            'tblCell.Width = Unit.Percentage(25)
            tblCell.CssClass = "normal1Right"

            If boolAdd = False Then
                tblCell.Font.Bold = True
            End If

            If CStr(dr("fld_type")) = "Popup" Then
                hpl = New HyperLink
                hpl.Text = dr("vcFieldName").ToString & IIf(IntermediatoryPage, " : ", "").ToString
                hpl.CssClass = "normal1Right"
                hpl.Font.Bold = True
                hpl.NavigateUrl = "#"
                If CStr(dr("vcDbColumnName")) = "BillingAddress" AndAlso _strPageType = "Opp" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "('Bill'," & RecordID & ")")
                ElseIf CStr(dr("vcDbColumnName")) = "ShippingAddress" AndAlso _strPageType = "Opp" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "('Ship'," & RecordID & ")")
                ElseIf CStr(dr("vcDbColumnName")) = "ShippingAddress" AndAlso _strPageType = "Project" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "('Ship'," & RecordID & ")")
                ElseIf CStr(dr("vcDbColumnName")) = "vcCreditCards" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "(" & RecordID & ",0)")
                ElseIf Not IsDBNull(dr("PopupFunctionName")) AndAlso _strPageType = "Case" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "(" & lngCaseID & ")")
                    If CCommon.ToString(dr("PopupFunctionName")) = "openSolutionPopup" Then
                        hpl.Text = "<img src='../images/solutions.png' />" & dr("vcFieldName").ToString & "(##SolutionCount##)" & IIf(IntermediatoryPage, " : ", "").ToString
                        hpl.ID = "SolutionForCase"
                    End If
                ElseIf Not IsDBNull(dr("PopupFunctionName")) Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "(" & RecordID & ")")
                End If
                tblCell.Controls.Add(hpl)
            Else
                'tblCell.Text = dr("vcFieldName").ToString & IIf(CCommon.ToInteger(dr("bitIsRequired")) = 1 And IntermediatoryPage = False, "<font color=""red"">*</font>", "").ToString() & IIf(IntermediatoryPage, " : ", "").ToString

                Dim lblFieldName As Label
                lblFieldName = New Label
                lblFieldName.Text = dr("vcFieldName").ToString & IIf(CCommon.ToInteger(dr("bitIsRequired")) = 1 And IntermediatoryPage = False, "<font color=""red"">*</font>", "").ToString() & IIf(IntermediatoryPage, " : ", "").ToString
                tblCell.Controls.Add(lblFieldName)
            End If

            If Not IsDBNull(dr("vcToolTip")) And dr("vcToolTip").ToString.Trim.Length > 0 Then
                Dim lblToolTip As Label
                lblToolTip = New Label
                lblToolTip.Text = "[?]"
                lblToolTip.CssClass = "tip"
                If dr("vcFieldName").ToString = "Follow-up campaign" Then
                    lblToolTip.ToolTip = "Following up With New leads, prospects, And sometimes even accounts (heck even fellow employees) Is an important part Of anyone's work day. With automated follow-ups you can schedule automated email templates and action items such as calls and tasks, to be created / sent out on a schedule, so you never have to remember to follow-up with a contact again…well, action items do require you look at your action item list, but emails are sent automatically by BizAutomation on your behalf, and you can always set an email alert to ping you from workflow automation.  You can disengage follow-up from the contact record itself, or by opening the compose message window, and clicking on the follow-up campaign 'Disengage from Follow-up campaign' button." + Environment.NewLine + "To setup an Automation Template, go To Marketing | Automated Follow-up."
                Else
                    lblToolTip.ToolTip = dr("vcToolTip").ToString.Trim
            End If
            lblToolTip.Attributes.Add("data-toggle", "tooltip")
            tblCell.Controls.Add(lblToolTip)
            End If

            tblCell.HorizontalAlign = HorizontalAlign.Right
            Row.Cells.Add(tblCell)

            If IntermediatoryPage = True Then
                tblCell = New TableCell
                'tblCell.Width = Unit.Percentage(25)

                If CStr(dr("fld_type")) = "DateField" Then
                    If dr("vcValue").ToString = "01/01/0001 00:00:00" Or dr("vcValue").ToString = "01/01/1753 00:00:00" Or dr("vcValue").ToString.Trim.Length = 0 Then
                        tblCell.Text = ""
                    ElseIf IsDate(dr("vcValue")) Then
                        tblCell.Text = FormattedDateFromDate(CDate(dr("vcValue")), CStr(Session("DateFormat")))
                        'Added by Priya (As changed followup fields from label to datefield for filter)
                    ElseIf ((dr("vcFieldName").ToString = "Next Follow-up" Or dr("vcFieldName").ToString = "Last Follow-up") And Not (IsDate(dr("vcValue")))) Then
                        tblCell.Text = dr("vcValue").ToString()
                    Else
                        tblCell.Text = ""
                    End If
                ElseIf CStr(dr("fld_type")) = "ListBox" Then
                    If Not dtSel Is Nothing Then
                        Dim i As Integer = 0
                        Dim str As String = ""
                        For i = 0 To dtSel.Rows.Count - 1
                            str = str & dtSel.Rows(0).Item(1).ToString & "<br>,"
                        Next
                        If str <> "" Then str = str.Substring(0, str.Length - 5)
                        tblCell.Text = str
                    Else
                        tblCell.Text = dr("vcValue").ToString()
                    End If
                ElseIf CStr(dr("fld_type")) = "Website" Then
                    hpl = New HyperLink
                    hpl.Text = IIf(dr("vcValue").ToString.Length > 0, dr("vcValue").ToString, "--").ToString
                    hpl.CssClass = "normal1"
                    hpl.Target = "_blank"
                    hpl.NavigateUrl = dr("vcValue").ToString
                    tblCell.Controls.Add(hpl)

                ElseIf CStr(dr("fld_type")) = "Email" Then
                    hpl = New HyperLink
                    hpl.Text = IIf(dr("vcValue").ToString.Length > 0, dr("vcValue").ToString, "--").ToString
                    hpl.CssClass = "normal1"
                    hpl.NavigateUrl = "#"
                    hpl.Attributes.Add("onclick", "return OpenEmail('" & dr("vcValue").ToString & "'," & System.Web.HttpContext.Current.Session("CompWindow").ToString & "," & RecordID & IIf(lngCaseID > 0, "," & lngCaseID.ToString, "").ToString & ");") 'pass CaseID so compose window can load relevant merger fields
                    tblCell.Controls.Add(hpl)
                ElseIf CStr(dr("fld_type")) = "CheckBox" Then
                    tblCell.Text = CStr(IIf(CCommon.ToString(dr("vcValue")) = "1" Or CCommon.ToBool(dr("vcValue")), "Yes", "No"))
                ElseIf CStr(dr("fld_type")) = "TextArea" Then
                    tblCell.Text = "<pre class=""WordWrap"">" & CCommon.ToString(dr("vcValue")) & "</pre>"
                ElseIf CStr(dr("fld_type")) = "Link" Then
                    hpl = New HyperLink
                    hpl.Text = IIf(dr("vcValue").ToString.Length > 0, dr("vcValue").ToString, "--").ToString
                    hpl.CssClass = "normal1"
                    hpl.Target = "_blank"



                    If dr("vcValue").ToString.StartsWith("http") Or dr("vcValue").ToString.StartsWith("https") Then
                        hpl.NavigateUrl = dr("vcValue").ToString
                    Else
                        hpl.NavigateUrl = "http://" & dr("vcValue").ToString
                    End If



                    tblCell.Controls.Add(hpl)
                Else
                    tblCell.Text = dr("vcValue").ToString
                End If

                If CBool(dr("bitCanBeUpdated")) = True And _strPageType <> "" And CBool(System.Web.HttpContext.Current.Session("InlineEdit")) = True And EditPermission <> 0 Then
                    Dim ControlClass As String = ""
                    Select Case CStr(dr("fld_type"))
                        Case "Website", "Email", "TextBox", "Email", "Link"
                            ControlClass = "click"
                        Case "SelectBox"
                            ControlClass = "editable_select"
                        Case "TextArea"
                            ControlClass = "editable_textarea"
                        Case "CheckBox"
                            ControlClass = "editable_CheckBox"
                        Case "DateField"
                            ControlClass = "editable_DateField"
                        Case "CheckBoxList"
                            ControlClass = "editable_CheckBoxList"
                    End Select

                    If dr("vcDbColumnName").ToString() = "monPAmount" Then
                        ControlClass = "editable_Amount"
                    End If


                    If (_strPageType = "Organization" Or _strPageType = "Contact") And ControlClass.Length > 0 Then
                        tblCell.Attributes.Add("id", _strPageType & "~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _ContactID & "~" & _DivisionID & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                        tblCell.Attributes.Add("class", ControlClass)
                        tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                        tblCell.Attributes.Add("onmouseout", "bgColor=''")
                    ElseIf _strPageType = "Project" And ControlClass.Length > 0 Then
                        tblCell.Attributes.Add("id", "Project~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _ProjectID & "~" & _DivisionID & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                        tblCell.Attributes.Add("class", ControlClass)
                        tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                        tblCell.Attributes.Add("onmouseout", "bgColor=''")
                    ElseIf _strPageType = "Opp" And ControlClass.Length > 0 Then
                        tblCell.Attributes.Add("id", "Opp~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _OpportunityId & "~" & _TerritoryID & "~" & dr("ListRelID").ToString & "~" & _DivisionID)
                        tblCell.Attributes.Add("class", ControlClass)
                        tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                        tblCell.Attributes.Add("onmouseout", "bgColor=''")
                    ElseIf _strPageType = "Case" And ControlClass.Length > 0 Then
                        tblCell.Attributes.Add("id", "Case~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _CaseId & "~" & _DivisionID & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                        tblCell.Attributes.Add("class", ControlClass)
                        tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                        tblCell.Attributes.Add("onmouseout", "bgColor=''")
                    ElseIf _strPageType = "WorkOrder" And ControlClass.Length > 0 Then
                        tblCell.Attributes.Add("id", "WorkOrder~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _WorkOrderId & "~" & _DivisionID & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                        tblCell.Attributes.Add("class", ControlClass)
                        tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                        tblCell.Attributes.Add("onmouseout", "bgColor=''")
                    Else
                        tblCell.CssClass = "normal1"
                    End If
                Else
                    tblCell.CssClass = "normal1"
                End If

                Row.Cells.Add(tblCell)
            Else
                tblCell = New TableCell
                tblCell.Width = Unit.Percentage(25)

                dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")
                If CStr(dr("fld_type")) = "SelectBox" Then
                    ddl = New DropDownList
                    ddl.CssClass = "signup"
                    ddl.Width = 200
                    ddl.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString
                    ddl.DataSource = dtSource
                    ddl.DataValueField = dtSource.Columns(0).ColumnName
                    ddl.DataTextField = dtSource.Columns(1).ColumnName
                    ddl.DataBind()

                    If dr("vcFieldName").ToString = "Drip Campaign" Then
                        ddl.Attributes.Add("onchange", "javascript:alert('hi');")
                        ddl.Attributes.Add("attr", "numPartenerContact")
                    End If

                    'Added By Prasanta to view contacts based on partner source
                    If dr("vcPropertyName").ToString = "numPartenerContact" Then
                        ddl.Attributes.Add("onchange", "javascript:MappingContact();")
                        ddl.Attributes.Add("attr", "numPartenerContact")
                    End If
                    If dr("vcPropertyName").ToString = "numPartenerSource" AndAlso _strPageType = "Organization" Then
                        ddl.Attributes.Add("onchange", "javascript:BindPartnerContact(" & CLng(Session("DomainID")) & "," & _OpportunityId & ");")
                        ddl.Attributes.Add("attr", "numPartenerSource")
                    End If
                    If (dr("vcPropertyName").ToString = "numPartner" Or dr("vcPropertyName").ToString = "numPartenerSource") AndAlso _strPageType = "Opp" Then
                        ddl.Attributes.Add("onchange", "javascript:BindPartnerContact(" & CLng(Session("DomainID")) & "," & _OpportunityId & ");")
                        ddl.Attributes.Add("attr", "numPartner")
                    End If
                    'Added by sandeep to provide grouping in dropdown for New Item form
                    If dr("vcPropertyName").ToString = "AssetChartAcntId" Or dr("vcPropertyName").ToString = "IncomeChartAcntId" Or dr("vcPropertyName").ToString = "COGSChartAcntId" Then
                        If (dtSource.Columns.Count = 3 And ddl.Items.Count > 0) Then
                            For i As Integer = 0 To ddl.Items.Count - 1
                                ddl.Items(i).Attributes("OptionGroup") = CStr(dtSource.Rows(i).Item(dtSource.Columns(2).ColumnName))
                            Next
                        End If
                    End If
                    ddl.Items.Insert(0, "-- Select One --")
                    ddl.Items.FindByText("-- Select One --").Value = "0"
                    ddl.Enabled = boolEnabled
                    ddl.EnableViewState = False
                    If Not IsDBNull(dr("vcValue")) Then
                        If CBool(dr("bitCustomField")) = False Then
                            If Not ddl.Items.FindByValue(dr("vcValue").ToString) Is Nothing Then
                                ddl.Items.FindByValue(dr("vcValue").ToString).Selected = True
                            End If
                        Else
                            If Not ddl.Items.FindByText(dr("vcValue").ToString) Is Nothing Then
                                ddl.Items.FindByText(dr("vcValue").ToString).Selected = True
                            End If
                        End If

                    End If
                    tblCell.CssClass = "normal1"
                    tblCell.Controls.Add(ddl)
                    If CCommon.ToString(dr("vcDbColumnName")).Trim = "numPercentageComplete" Then
                        ddl.Attributes.Add("onclick", "return SubTabSectionMsg('" & DirectCast(ddl, Object).ToString() & "');")
                    End If
                    Row.Cells.Add(tblCell)
                    Return ddl
                ElseIf CStr(dr("fld_type")) = "TextBox" Or CStr(dr("fld_type")) = "Website" Or CStr(dr("fld_type")) = "TextArea" Or CStr(dr("fld_type")) = "Email" Then
                    If CCommon.ToString(dr("vcPropertyName")) = "vcCompactContactDetails" Then
                        tblCell.CssClass = "normal1"
                        tblCell.Text = dr("vcValue").ToString
                        Row.Cells.Add(tblCell)
                    Else
                        txt = New TextBox
                        txt.MaxLength = CCommon.ToInteger(dr("intFieldMaxLength"))
                        If CStr(dr("fld_type")) = "Website" Or CStr(dr("fld_type")) = "Email" Then
                            txt.Width = 160
                        Else
                            txt.Width = 200
                        End If
                        If CStr(dr("fld_type")) = "Email" Then
                            txt.CssClass = "signup txtAutoLookUpEmail"
                        Else
                            txt.CssClass = "signup"
                        End If

                        If CBool(dr("bitCustomField")) = False Then
                            txt.Text = dr("vcValue").ToString
                        Else
                            If dr("vcValue").ToString <> "0" Then
                                txt.Text = dr("vcValue").ToString
                            Else
                                txt.Text = ""
                            End If
                        End If

                        If CStr(dr("fld_type")) = "TextArea" Then
                            txt.TextMode = TextBoxMode.MultiLine
                            txt.Height = 40

                        End If
                        txt.EnableViewState = False
                        txt.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                        tblCell.CssClass = "normal1"
                        tblCell.Controls.Add(txt)
                        If CStr(dr("fld_type")) = "Website" Then
                            btn = New Button
                            btn.CssClass = "button"
                            btn.Text = "Go"
                            btn.Attributes.Add("onclick", "return OpenWebsite('" & txt.ID & "');")
                            btn.Attributes.Add("style", "margin-left:3px;")
                            tblCell.Controls.Add(btn)
                        ElseIf CStr(dr("fld_type")) = "Email" Then
                            btn = New Button
                            btn.CssClass = "button btnGoNewEmail"
                            btn.Text = "Go"
                            btn.Attributes.Add("onclick", "return OpenEmail('" & txt.ClientID & "'," & System.Web.HttpContext.Current.Session("CompWindow").ToString & "," & RecordID & ");")
                            btn.Attributes.Add("style", "margin-left:3px;")
                            tblCell.Controls.Add(btn)
                        End If
                    End If
                    Row.Cells.Add(tblCell)
                ElseIf CStr(dr("fld_type")) = "CheckBox" Then
                    chk = New CheckBox
                    chk.Checked = CBool(dr("vcValue").ToString)
                    chk.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                    'Added by sandeep to provide single selection between Lot and Serialized item in New Item form
                    If dr("vcPropertyName").ToString = "bitLotNo" Or dr("vcPropertyName").ToString = "bitSerialized" Then
                        chk.Attributes.Add("onclick", "SingleSelect(this);")
                    End If
                    chk.EnableViewState = False
                    tblCell.CssClass = "normal1"
                    tblCell.Controls.Add(chk)
                    Row.Cells.Add(tblCell)
                ElseIf CStr(dr("fld_type")) = "DateField" Then
                    Dim bizCalendar As UserControl
                    Dim _myUC_DueDate As PropertyInfo
                    Dim strDate As String
                    bizCalendar = CType(LoadControl("include/calandar.ascx"), UserControl)
                    bizCalendar.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                    bizCalendar.EnableViewState = False
                    bizCalendar.ClientIDMode = UI.ClientIDMode.AutoID
                    If Not IsDBNull(dr("vcValue")) Then
                        Dim _myControlType As Type = bizCalendar.GetType()
                        _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                        strDate = dr("vcValue").ToString
                        If strDate = "0" Then strDate = ""
                        If strDate = "01/01/0001 00:00:00" Or strDate = "01/01/1753 00:00:00" Then strDate = ""
                        If strDate <> "" Then
                            _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                        End If
                    End If
                    tblCell.CssClass = "normal1"
                    tblCell.Controls.Add(bizCalendar)
                    Row.Cells.Add(tblCell)
                ElseIf CStr(dr("fld_type")) = "Popup" Or CStr(dr("fld_type")) = "Label" Then
                    tblCell.CssClass = "normal1"
                    tblCell.Text = dr("vcValue").ToString
                    Row.Cells.Add(tblCell)
                ElseIf CStr(dr("fld_type")) = "ListBox" Then
                    lst = New ListBox
                    lst.CssClass = "signup"
                    lst.Width = 200
                    lst.SelectionMode = ListSelectionMode.Multiple
                    lst.Height = 40
                    lst.EnableViewState = False
                    lst.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                    lst.DataSource = dtSource
                    lst.DataValueField = dtSource.Columns(0).ColumnName
                    lst.DataTextField = dtSource.Columns(1).ColumnName
                    lst.DataBind()
                    Dim i As Integer = 0
                    For i = 0 To dtSel.Rows.Count - 1
                        If Not lst.Items.FindByValue(dtSel.Rows(i).Item(0).ToString) Is Nothing Then
                            lst.Items.FindByValue(dtSel.Rows(i).Item(0).ToString).Selected = True
                        End If
                    Next
                    tblCell.Controls.Add(lst)
                    tblCell.CssClass = "normal1"
                    Row.Cells.Add(tblCell)
                ElseIf CStr(dr("fld_type")) = "Custom" Then
                    tblCell.CssClass = "normal1"
                    Row.Cells.Add(tblCell)
                ElseIf CStr(dr("fld_type")) = "CheckBoxList" Then
                    chkbl = New CheckBoxList
                    chkbl.CssClass = "signup"
                    chkbl.EnableViewState = False
                    chkbl.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString

                    Dim tempValue As String()
                    If Not IsDBNull(dr("vcValue")) Then
                        tempValue = CCommon.ToString(dr("vcValue")).Split(CChar(","))
                    End If

                    For Each tempDr As DataRow In dtSource.Rows
                        Dim chkblItem As New ListItem
                        chkblItem.Text = CCommon.ToString(tempDr(dtSource.Columns(1).ColumnName))
                        chkblItem.Value = CCommon.ToString(tempDr(dtSource.Columns(0).ColumnName))

                        If Not tempValue Is Nothing AndAlso tempValue.Contains(CCommon.ToString(tempDr(dtSource.Columns(1).ColumnName))) Then
                            chkblItem.Selected = True
                        End If

                        chkbl.Items.Add(chkblItem)
                    Next

                    tblCell.Controls.Add(chkbl)
                    tblCell.CssClass = "normal1"
                    Row.Cells.Add(tblCell)
                Else
                    tblCell.CssClass = "normal1"
                    Row.Cells.Add(tblCell)
                End If
            End If
        End Function


        Public Function CreateCells(ByRef divForm As HtmlGenericControl, ByVal dr As DataRow, ByVal IntermediatoryPage As Boolean, Optional ByVal dtSource As DataTable = Nothing, Optional ByVal dtSel As DataTable = Nothing, Optional ByVal RecordID As Long = 0, Optional ByVal boolEnabled As Boolean = True, Optional ByVal lngCaseID As Long = 0, Optional ByVal boolAdd As Boolean = False) As Object
            Dim label As New HtmlGenericControl("label")

            If CStr(dr("fld_type")) = "Popup" Then
                hpl = New HyperLink
                hpl.Text = dr("vcFieldName").ToString
                hpl.CssClass = "normal1Right"
                hpl.Font.Bold = True
                hpl.NavigateUrl = "#"
                If CStr(dr("vcDbColumnName")) = "BillingAddress" AndAlso _strPageType = "Opp" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "('Bill'," & RecordID & ")")
                ElseIf CStr(dr("vcDbColumnName")) = "ShippingAddress" AndAlso _strPageType = "Opp" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "('Ship'," & RecordID & ")")
                ElseIf CStr(dr("vcDbColumnName")) = "ShippingAddress" AndAlso _strPageType = "Project" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "('Ship'," & RecordID & ")")

                ElseIf Not IsDBNull(dr("PopupFunctionName")) AndAlso _strPageType = "Case" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "(" & lngCaseID & ")")

                ElseIf Not IsDBNull(dr("PopupFunctionName")) Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "(" & RecordID & ")")
                End If
                label.Controls.Add(hpl)
            Else
                Dim lblFieldName As Label
                lblFieldName = New Label
                lblFieldName.Text = dr("vcFieldName").ToString & IIf((CCommon.ToInteger(dr("bitIsRequired")) = 1 Or CCommon.ToBool(dr("bitIsEmail")) = True Or CCommon.ToBool(dr("bitIsNumeric")) = True Or CCommon.ToBool(dr("bitIsAlphaNumeric")) = True Or CCommon.ToBool(dr("bitIsLengthValidation")) = True) And IntermediatoryPage = False, "<font color=""red"">*</font>", "").ToString()
                label.Controls.Add(lblFieldName)
            End If

            If Not IsDBNull(dr("vcToolTip")) And dr("vcToolTip").ToString.Trim.Length > 0 Then
                Dim lblToolTip As Label
                lblToolTip = New Label
                lblToolTip.Text = "[?]"
                lblToolTip.CssClass = "tip"
                lblToolTip.ToolTip = dr("vcToolTip").ToString.Trim
                label.Controls.Add(lblToolTip)
            End If

            divForm.Controls.Add(label)

            If IntermediatoryPage = True Then
                Dim div As New HtmlGenericControl("div")

                If CStr(dr("fld_type")) = "DateField" Then
                    If dr("vcValue").ToString = "01/01/0001 00:00:00" Or dr("vcValue").ToString = "01/01/1753 00:00:00" Or dr("vcValue").ToString.Trim.Length = 0 Then
                        div.InnerHtml = "-"
                    ElseIf IsDate(dr("vcValue")) Then
                        div.InnerHtml = FormattedDateFromDate(CDate(dr("vcValue")), CStr(Session("DateFormat")))
                    Else
                        div.InnerHtml = "-"
                    End If
                ElseIf CStr(dr("fld_type")) = "ListBox" Then
                    If Not dtSel Is Nothing Then
                        Dim i As Integer = 0
                        Dim str As String = ""
                        For i = 0 To dtSel.Rows.Count - 1
                            str = str & dtSel.Rows(0).Item(1).ToString & "<br>,"
                        Next
                        If str <> "" Then str = str.Substring(0, str.Length - 5)
                        div.InnerHtml = str
                    Else
                        div.InnerHtml = CStr(IIf(String.IsNullOrEmpty(CCommon.ToString(dr("vcValue"))), "-", CCommon.ToString(dr("vcValue"))))
                    End If
                ElseIf CStr(dr("fld_type")) = "Website" Then
                    hpl = New HyperLink
                    hpl.Text = IIf(dr("vcValue").ToString.Length > 0, dr("vcValue").ToString, "--").ToString
                    hpl.CssClass = "normal1"
                    hpl.Target = "_blank"
                    hpl.NavigateUrl = dr("vcValue").ToString
                    div.Controls.Add(hpl)

                ElseIf CStr(dr("fld_type")) = "Email" Then
                    hpl = New HyperLink
                    hpl.Text = IIf(dr("vcValue").ToString.Length > 0, dr("vcValue").ToString, "--").ToString
                    hpl.CssClass = "normal1"
                    hpl.NavigateUrl = "#"
                    hpl.Attributes.Add("onclick", "return OpenEmail('" & dr("vcValue").ToString & "'," & System.Web.HttpContext.Current.Session("CompWindow").ToString & "," & RecordID & IIf(lngCaseID > 0, "," & lngCaseID.ToString, "").ToString & ");") 'pass CaseID so compose window can load relevant merger fields
                    div.Controls.Add(hpl)
                ElseIf CStr(dr("fld_type")) = "CheckBox" Then
                    div.InnerHtml = CStr(IIf(CBool(dr("vcValue")), "Yes", "No"))
                ElseIf CStr(dr("fld_type")) = "TextArea" Then
                    div.InnerHtml = CCommon.ToString(dr("vcValue"))
                ElseIf CStr(dr("fld_type")) = "Link" Then
                    hpl = New HyperLink
                    hpl.Text = IIf(dr("vcValue").ToString.Length > 0, dr("vcValue").ToString, "--").ToString
                    hpl.CssClass = "normal1"
                    hpl.Target = "_blank"

                    If dr("vcValue").ToString.StartsWith("http") Or dr("vcValue").ToString.StartsWith("https") Then
                        hpl.NavigateUrl = dr("vcValue").ToString
                    Else
                        hpl.NavigateUrl = "http://" & dr("vcValue").ToString
                    End If

                    div.Controls.Add(hpl)
                Else
                    div.InnerHtml = CStr(IIf(String.IsNullOrEmpty(CCommon.ToString(dr("vcValue"))), "-", CCommon.ToString(dr("vcValue"))))
                End If

                If CBool(dr("bitCanBeUpdated")) = True And _strPageType <> "" And CBool(System.Web.HttpContext.Current.Session("InlineEdit")) = True And EditPermission <> 0 Then
                    Dim ControlClass As String = ""
                    Select Case CStr(dr("fld_type"))
                        Case "Website", "Email", "TextBox", "Email", "Link"
                            ControlClass = "click"
                        Case "SelectBox"
                            ControlClass = "editable_select"
                        Case "TextArea"
                            ControlClass = "editable_textarea WordWrap"
                        Case "CheckBox"
                            ControlClass = "editable_CheckBox"
                        Case "DateField"
                            ControlClass = "editable_DateField"
                        Case "CheckBoxList"
                            ControlClass = "editable_CheckBoxList"
                    End Select

                    If dr("vcDbColumnName").ToString() = "monPAmount" Then
                        ControlClass = "editable_Amount form-control"
                    End If


                    If (_strPageType = "Organization" Or _strPageType = "Contact") And ControlClass.Length > 0 Then
                        div.Attributes.Add("id", _strPageType & "~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _ContactID & "~" & _DivisionID & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                        div.Attributes.Add("class", ControlClass)
                        div.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                        div.Attributes.Add("onmouseout", "bgColor=''")
                    ElseIf _strPageType = "Project" And ControlClass.Length > 0 Then
                        div.Attributes.Add("id", "Project~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _ProjectID & "~" & _DivisionID & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                        div.Attributes.Add("class", ControlClass)
                        div.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                        div.Attributes.Add("onmouseout", "bgColor=''")
                    ElseIf _strPageType = "Opp" And ControlClass.Length > 0 Then
                        div.Attributes.Add("id", "Opp~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _OpportunityId & "~" & _TerritoryID & "~" & dr("ListRelID").ToString & "~" & _DivisionID)
                        div.Attributes.Add("class", ControlClass)
                        div.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                        div.Attributes.Add("onmouseout", "bgColor=''")
                    ElseIf _strPageType = "Case" And ControlClass.Length > 0 Then
                        div.Attributes.Add("id", "Case~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _CaseId & "~" & _DivisionID & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                        div.Attributes.Add("class", ControlClass)
                        div.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                        div.Attributes.Add("onmouseout", "bgColor=''")
                    End If
                End If

                divForm.Controls.Add(div)
            Else
                Dim div As New HtmlGenericControl("div")

                dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")
                If CStr(dr("fld_type")) = "SelectBox" Then
                    ddl = New DropDownList
                    ddl.CssClass = "form-control"
                    ddl.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString
                    ddl.DataSource = dtSource
                    ddl.DataValueField = dtSource.Columns(0).ColumnName
                    ddl.DataTextField = dtSource.Columns(1).ColumnName
                    ddl.DataBind()

                    'Added by sandeep to provide grouping in dropdown for New Item form
                    If dr("vcPropertyName").ToString = "AssetChartAcntId" Or dr("vcPropertyName").ToString = "IncomeChartAcntId" Or dr("vcPropertyName").ToString = "COGSChartAcntId" Then
                        If (dtSource.Columns.Count = 3 And ddl.Items.Count > 0) Then
                            For i As Integer = 0 To ddl.Items.Count - 1
                                ddl.Items(i).Attributes("OptionGroup") = CStr(dtSource.Rows(i).Item(dtSource.Columns(2).ColumnName))
                            Next
                        End If
                    End If

                    ddl.Items.Insert(0, "-- Select One --")
                    ddl.Items.FindByText("-- Select One --").Value = "0"
                    ddl.Enabled = boolEnabled
                    ddl.EnableViewState = False

                    If Not IsDBNull(dr("vcValue")) Then
                        If CBool(dr("bitCustomField")) = False Then
                            If Not ddl.Items.FindByValue(dr("vcValue").ToString) Is Nothing Then
                                ddl.Items.FindByValue(dr("vcValue").ToString).Selected = True
                            End If
                        Else
                            If Not ddl.Items.FindByText(dr("vcValue").ToString) Is Nothing Then
                                ddl.Items.FindByText(dr("vcValue").ToString).Selected = True
                            End If
                        End If
                    End If

                    div.Controls.Add(ddl)

                    If CCommon.ToString(dr("vcDbColumnName")).Trim = "numPercentageComplete" Then
                        ddl.Attributes.Add("onclick", "return SubTabSectionMsg('" & DirectCast(ddl, Object).ToString() & "');")
                    End If

                    divForm.Controls.Add(div)

                    Return ddl
                ElseIf CStr(dr("fld_type")) = "TextBox" Or CStr(dr("fld_type")) = "Website" Or CStr(dr("fld_type")) = "TextArea" Or CStr(dr("fld_type")) = "Email" Then
                    txt = New TextBox
                    txt.MaxLength = CCommon.ToInteger(dr("intFieldMaxLength"))
                    txt.CssClass = "form-control"
                    If CBool(dr("bitCustomField")) = False Then
                        txt.Text = dr("vcValue").ToString
                    Else
                        If dr("vcValue").ToString <> "0" Then
                            txt.Text = dr("vcValue").ToString
                        Else
                            txt.Text = ""
                        End If
                    End If

                    If CStr(dr("fld_type")) = "TextArea" Then
                        txt.TextMode = TextBoxMode.MultiLine
                    End If

                    txt.EnableViewState = False
                    txt.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                    divForm.Controls.Add(txt)

                    If CStr(dr("fld_type")) = "Website" Then
                        btn = New Button
                        btn.CssClass = "btn btn-xs btn-primary"
                        btn.Text = "Go"
                        btn.Attributes.Add("onclick", "return OpenWebsite('" & txt.ID & "');")
                        div.Controls.Add(btn)
                    ElseIf CStr(dr("fld_type")) = "Email" Then
                        btn = New Button
                        btn.CssClass = "btn btn-xs btn-primary"
                        btn.Text = "Go"
                        btn.Attributes.Add("onclick", "return OpenEmail('" & txt.ClientID & "'," & System.Web.HttpContext.Current.Session("CompWindow").ToString & "," & RecordID & ");")
                        div.Controls.Add(btn)
                    End If

                    divForm.Controls.Add(div)
                ElseIf CStr(dr("fld_type")) = "CheckBox" Then
                    chk = New CheckBox
                    chk.Checked = CBool(dr("vcValue").ToString)
                    chk.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString

                    'Added by sandeep to provide single selection between Lot and Serialized item in New Item form
                    If dr("vcPropertyName").ToString = "bitLotNo" Or dr("vcPropertyName").ToString = "bitSerialized" Then
                        chk.Attributes.Add("onclick", "SingleSelect(this);")
                    End If
                    chk.EnableViewState = False

                    div.Controls.Add(chk)
                    divForm.Controls.Add(div)
                ElseIf CStr(dr("fld_type")) = "DateField" Then
                    Dim bizCalendar As UserControl
                    Dim _myUC_DueDate As PropertyInfo
                    Dim strDate As String
                    bizCalendar = CType(LoadControl("include/calandar.ascx"), UserControl)
                    bizCalendar.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                    bizCalendar.EnableViewState = False
                    bizCalendar.ClientIDMode = UI.ClientIDMode.AutoID
                    If Not IsDBNull(dr("vcValue")) Then
                        Dim _myControlType As Type = bizCalendar.GetType()
                        _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                        strDate = dr("vcValue").ToString
                        If strDate = "0" Then strDate = ""
                        If strDate = "01/01/0001 00:00:00" Or strDate = "01/01/1753 00:00:00" Then strDate = ""
                        If strDate <> "" Then
                            _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                        End If
                    End If

                    div.Controls.Add(bizCalendar)
                    divForm.Controls.Add(div)
                ElseIf CStr(dr("fld_type")) = "Popup" Or CStr(dr("fld_type")) = "Label" Then
                    div.InnerHtml = dr("vcValue").ToString
                    divForm.Controls.Add(div)
                ElseIf CStr(dr("fld_type")) = "ListBox" Then
                    lst = New ListBox
                    lst.SelectionMode = ListSelectionMode.Multiple
                    lst.Height = 40
                    lst.EnableViewState = False
                    lst.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                    lst.DataSource = dtSource
                    lst.DataValueField = dtSource.Columns(0).ColumnName
                    lst.DataTextField = dtSource.Columns(1).ColumnName
                    lst.DataBind()
                    Dim i As Integer = 0
                    For i = 0 To dtSel.Rows.Count - 1
                        If Not lst.Items.FindByValue(dtSel.Rows(i).Item(0).ToString) Is Nothing Then
                            lst.Items.FindByValue(dtSel.Rows(i).Item(0).ToString).Selected = True
                        End If
                    Next
                    div.Controls.Add(lst)
                    divForm.Controls.Add(div)
                ElseIf CStr(dr("fld_type")) = "Custom" Then
                    divForm.Controls.Add(div)
                ElseIf CStr(dr("fld_type")) = "CheckBoxList" Then
                    chkbl = New CheckBoxList
                    chkbl.CssClass = "signup"
                    chkbl.EnableViewState = False
                    chkbl.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString

                    Dim tempValue As String()
                    If Not IsDBNull(dr("vcValue")) Then
                        tempValue = CCommon.ToString(dr("vcValue")).Split(CChar(","))
                    End If

                    For Each tempDr As DataRow In dtSource.Rows
                        Dim chkblItem As New ListItem
                        chkblItem.Text = CCommon.ToString(tempDr(dtSource.Columns(1).ColumnName))
                        chkblItem.Value = CCommon.ToString(tempDr(dtSource.Columns(0).ColumnName))

                        If Not tempValue Is Nothing AndAlso tempValue.Contains(CCommon.ToString(tempDr(dtSource.Columns(1).ColumnName))) Then
                            chkblItem.Selected = True
                        End If

                        chkbl.Items.Add(chkblItem)
                    Next

                    div.Controls.Add(chkbl)
                    divForm.Controls.Add(div)
                Else
                    divForm.Controls.Add(div)
                End If
            End If
        End Function

        Public Function CreateDivFormControls(ByRef ctl As HtmlGenericControl, ByVal dr As DataRow, ByVal isReadOnly As Boolean, ByVal IntermediatoryPage As Boolean, ByRef tabIndex As Short, Optional ByVal dtSource As DataTable = Nothing, Optional ByVal dtSel As DataTable = Nothing, Optional ByVal RecordID As Long = 0, Optional ByVal boolEnabled As Boolean = True, Optional ByVal lngCaseID As Long = 0, Optional ByVal boolAdd As Boolean = False) As Object
            Dim divFormGroup As New HtmlGenericControl("div")
            divFormGroup.Attributes.Add("class", "form-group")

            Dim divFormGroupLabel As New HtmlGenericControl("label")
            divFormGroupLabel.Attributes.Add("class", "col-3-12")
            divFormGroup.Controls.Add(divFormGroupLabel)

            Dim divFormGroupControl As New HtmlGenericControl("div")
            divFormGroupControl.Attributes.Add("class", "wordwrap col-9-12")
            divFormGroup.Controls.Add(divFormGroupControl)

            If CStr(dr("fld_type")) = "Popup" Then
                hpl = New HyperLink
                hpl.Text = dr("vcFieldName").ToString
                hpl.CssClass = "normal1Right"
                hpl.Font.Bold = True
                hpl.NavigateUrl = "#"
                If CStr(dr("vcDbColumnName")) = "BillingAddress" AndAlso _strPageType = "Opp" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "('Bill'," & RecordID & ")")
                ElseIf CStr(dr("vcDbColumnName")) = "ShippingAddress" AndAlso _strPageType = "Opp" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "('Ship'," & RecordID & ")")
                ElseIf CStr(dr("vcDbColumnName")) = "ShippingAddress" AndAlso _strPageType = "Project" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "('Ship'," & RecordID & ")")

                ElseIf Not IsDBNull(dr("PopupFunctionName")) AndAlso _strPageType = "Case" Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "(" & lngCaseID & ")")

                ElseIf Not IsDBNull(dr("PopupFunctionName")) Then
                    hpl.Attributes.Add("onclick", "return " & dr("PopupFunctionName").ToString & "(" & RecordID & ")")
                End If

                divFormGroupControl.Controls.Add(hpl)
            Else
                Dim lblFieldName As Label
                lblFieldName = New Label
                lblFieldName.Text = dr("vcFieldName").ToString & IIf((CCommon.ToInteger(dr("bitIsRequired")) = 1 Or CCommon.ToBool(dr("bitIsEmail")) = True Or CCommon.ToBool(dr("bitIsNumeric")) = True Or CCommon.ToBool(dr("bitIsAlphaNumeric")) = True Or CCommon.ToBool(dr("bitIsLengthValidation")) = True) And IntermediatoryPage = False, "<font color=""red"">*</font>", "").ToString() & IIf(IntermediatoryPage, " : ", "").ToString
                divFormGroupLabel.Controls.Add(lblFieldName)
            End If

            If Not isReadOnly Then
                If Not IsDBNull(dr("vcToolTip")) And dr("vcToolTip").ToString.Trim.Length > 0 Then
                    Dim lblToolTip As Label
                    lblToolTip = New Label
                    lblToolTip.Text = "[?]"
                    lblToolTip.CssClass = "tip"
                    lblToolTip.ToolTip = dr("vcToolTip").ToString.Trim
                    divFormGroupLabel.Controls.Add(lblToolTip)
                End If
            End If

            If IntermediatoryPage = True Then
                'TODO: Commented by sandeep because this method is developed for new sales form and not used by any other code yet
                'If CStr(dr("fld_type")) = "DateField" Then
                '    If dr("vcValue").ToString = "01/01/0001 00:00:00" Or dr("vcValue").ToString = "01/01/1753 00:00:00" Or dr("vcValue").ToString.Trim.Length = 0 Then
                '        tblCell.Text = ""
                '    ElseIf IsDate(dr("vcValue")) Then
                '        tblCell.Text = FormattedDateFromDate(CDate(dr("vcValue")), CStr(Session("DateFormat")))
                '    Else
                '        tblCell.Text = ""
                '    End If
                'ElseIf CStr(dr("fld_type")) = "ListBox" Then
                '    Dim i As Integer = 0
                '    Dim str As String = ""
                '    For i = 0 To dtSel.Rows.Count - 1
                '        str = str & dtSel.Rows(0).Item(1).ToString & "<br>,"
                '    Next
                '    If str <> "" Then str = str.Substring(0, str.Length - 5)
                '    tblCell.Text = str
                'ElseIf CStr(dr("fld_type")) = "Website" Then
                '    hpl = New HyperLink
                '    hpl.Text = IIf(dr("vcValue").ToString.Length > 0, dr("vcValue").ToString, "--").ToString
                '    hpl.CssClass = "normal1"
                '    hpl.Target = "_blank"
                '    hpl.NavigateUrl = dr("vcValue").ToString
                '    tblCell.Controls.Add(hpl)

                'ElseIf CStr(dr("fld_type")) = "Email" Then
                '    hpl = New HyperLink
                '    hpl.Text = IIf(dr("vcValue").ToString.Length > 0, dr("vcValue").ToString, "--").ToString
                '    hpl.CssClass = "normal1"
                '    hpl.NavigateUrl = "#"
                '    hpl.Attributes.Add("onclick", "return OpenEmail('" & dr("vcValue").ToString & "'," & System.Web.HttpContext.Current.Session("CompWindow").ToString & "," & RecordID & IIf(lngCaseID > 0, "," & lngCaseID.ToString, "").ToString & ");") 'pass CaseID so compose window can load relevant merger fields
                '    tblCell.Controls.Add(hpl)
                'ElseIf CStr(dr("fld_type")) = "CheckBox" Then
                '    tblCell.Text = CStr(IIf(CBool(dr("vcValue")), "Yes", "No"))
                'ElseIf CStr(dr("fld_type")) = "TextArea" Then
                '    tblCell.Text = "<pre class=""WordWrap"">" & CCommon.ToString(dr("vcValue")) & "</pre>"
                'Else
                '    tblCell.Text = dr("vcValue").ToString
                'End If

                'If CBool(dr("bitCanBeUpdated")) = True And _strPageType <> "" And CBool(System.Web.HttpContext.Current.Session("InlineEdit")) = True And EditPermission <> 0 Then
                '    Dim ControlClass As String = ""
                '    Select Case CStr(dr("fld_type"))
                '        Case "Website", "Email", "TextBox", "Email"
                '            ControlClass = "click"
                '        Case "SelectBox"
                '            ControlClass = "editable_select"
                '        Case "TextArea"
                '            ControlClass = "editable_textarea"
                '        Case "CheckBox"
                '            ControlClass = "editable_CheckBox"
                '        Case "DateField"
                '            ControlClass = ""
                '    End Select

                '    If dr("vcDbColumnName").ToString() = "monPAmount" Then
                '        ControlClass = "editable_Amount"
                '    End If


                '    If (_strPageType = "Organization" Or _strPageType = "Contact") And ControlClass.Length > 0 Then
                '        tblCell.Attributes.Add("id", _strPageType & "~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _ContactID & "~" & _DivisionID & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                '        tblCell.Attributes.Add("class", ControlClass)
                '        tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                '        tblCell.Attributes.Add("onmouseout", "bgColor=''")
                '    ElseIf _strPageType = "Project" And ControlClass.Length > 0 Then
                '        tblCell.Attributes.Add("id", "Project~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _ProjectID & "~" & _DivisionID & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                '        tblCell.Attributes.Add("class", ControlClass)
                '        tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                '        tblCell.Attributes.Add("onmouseout", "bgColor=''")
                '    ElseIf _strPageType = "Opp" And ControlClass.Length > 0 Then
                '        tblCell.Attributes.Add("id", "Opp~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _OpportunityId & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                '        tblCell.Attributes.Add("class", ControlClass)
                '        tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                '        tblCell.Attributes.Add("onmouseout", "bgColor=''")
                '    ElseIf _strPageType = "Case" And ControlClass.Length > 0 Then
                '        tblCell.Attributes.Add("id", "Case~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & _CaseId & "~" & _DivisionID & "~" & _TerritoryID & "~" & dr("ListRelID").ToString)
                '        tblCell.Attributes.Add("class", ControlClass)
                '        tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                '        tblCell.Attributes.Add("onmouseout", "bgColor=''")
                '    Else
                '        tblCell.CssClass = "normal1"
                '    End If
                'Else
                '    tblCell.CssClass = "normal1"
                'End If

                'Row.Cells.Add(tblCell)
            Else
                dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")
                If CStr(dr("fld_type")) = "SelectBox" Then
                    If isReadOnly Then
                        Dim lblvalueField As New Label
                        lblvalueField = New Label
                        lblvalueField.CssClass = "textdiv"
                        lblvalueField.Attributes.Add("style", "max-width:250px")
                        lblvalueField.TabIndex = tabIndex
                        tabIndex = CShort(tabIndex + 1)

                        If Not IsDBNull(dr("vcValue")) AndAlso Not dtSource Is Nothing Then
                            If CBool(dr("bitCustomField")) = False Then
                                Dim foundRows As DataRow()
                                foundRows = dtSource.Select(dtSource.Columns(0).ColumnName.ToString() + "=" + Convert.ToString(dr("vcValue")))
                                If (foundRows.Length > 0) Then
                                    lblvalueField.Text = CCommon.ToString(foundRows(0)(dtSource.Columns(1).ColumnName))
                                End If
                            Else
                                lblvalueField.Text = dr("vcValue").ToString
                            End If
                        End If

                        divFormGroupControl.Controls.Add(lblvalueField)
                        ctl.Controls.Add(divFormGroup)
                    Else
                        radcmb = New RadComboBox
                        radcmb.ClientIDMode = UI.ClientIDMode.Static
                        radcmb.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString
                        radcmb.Width = New Unit("100%")
                        radcmb.DataSource = dtSource
                        radcmb.DataValueField = dtSource.Columns(0).ColumnName
                        radcmb.DataTextField = dtSource.Columns(1).ColumnName
                        radcmb.DataBind()
                        'Added by sandeep to provide grouping in dropdown for New Item form
                        If dr("vcPropertyName").ToString = "AssetChartAcntId" Or dr("vcPropertyName").ToString = "IncomeChartAcntId" Or dr("vcPropertyName").ToString = "COGSChartAcntId" Then
                            If (dtSource.Columns.Count = 3 And radcmb.Items.Count > 0) Then
                                For i As Integer = 0 To radcmb.Items.Count - 1
                                    radcmb.Items(i).Attributes("OptionGroup") = CStr(dtSource.Rows(i).Item(dtSource.Columns(2).ColumnName))
                                Next
                            End If
                        End If
                        radcmb.Items.Insert(0, New RadComboBoxItem("-- Select One --", "0"))
                        radcmb.OnClientLoad = "onRadComboBoxLoad"
                        radcmb.Enabled = boolEnabled
                        radcmb.EnableViewState = False
                        If Not IsDBNull(dr("vcValue")) Then
                            If CBool(dr("bitCustomField")) = False Then
                                If Not radcmb.Items.FindItemByValue(dr("vcValue").ToString) Is Nothing Then
                                    radcmb.Items.FindItemByValue(dr("vcValue").ToString).Selected = True
                                End If
                            Else
                                If Not radcmb.Items.FindItemByText(dr("vcValue").ToString) Is Nothing Then
                                    radcmb.Items.FindItemByText(dr("vcValue").ToString).Selected = True
                                End If
                            End If

                        End If
                        radcmb.TabIndex = tabIndex
                        tabIndex = CShort(tabIndex + 1)
                        divFormGroupControl.Controls.Add(radcmb)
                        If CCommon.ToString(dr("vcDbColumnName")).Trim = "numPercentageComplete" Then
                            radcmb.Attributes.Add("onclick", "return SubTabSectionMsg('" & DirectCast(radcmb, Object).ToString() & "');")
                        End If
                        ctl.Controls.Add(divFormGroup)
                        Return radcmb
                    End If
                ElseIf CStr(dr("fld_type")) = "TextBox" Or CStr(dr("fld_type")) = "Website" Or CStr(dr("fld_type")) = "TextArea" Or CStr(dr("fld_type")) = "Email" Then
                    If isReadOnly Then
                        Dim lblvalueField As New Label
                        lblvalueField = New Label
                        lblvalueField.CssClass = "textdiv"
                        lblvalueField.Attributes.Add("style", "max-width:250px")
                        lblvalueField.TabIndex = tabIndex
                        tabIndex = CShort(tabIndex + 1)
                        If CBool(dr("bitCustomField")) = False Then
                            lblvalueField.Text = dr("vcValue").ToString
                        Else
                            If dr("vcValue").ToString <> "0" Then
                                lblvalueField.Text = dr("vcValue").ToString
                            Else
                                lblvalueField.Text = ""
                            End If
                        End If
                        divFormGroupControl.Controls.Add(lblvalueField)
                    Else
                        txt = New TextBox
                        txt.MaxLength = CCommon.ToInteger(dr("intFieldMaxLength"))
                        txt.CssClass = "signup"
                        If CBool(dr("bitCustomField")) = False Then
                            txt.Text = dr("vcValue").ToString
                        Else
                            If dr("vcValue").ToString <> "0" Then
                                txt.Text = dr("vcValue").ToString
                            Else
                                txt.Text = ""
                            End If
                        End If

                        If CStr(dr("fld_type")) = "TextArea" Then
                            txt.TextMode = TextBoxMode.MultiLine
                            txt.Height = 40

                        End If
                        txt.EnableViewState = False
                        txt.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                        txt.ReadOnly = isReadOnly
                        txt.TabIndex = tabIndex
                        tabIndex = CShort(tabIndex + 1)
                        divFormGroupControl.Controls.Add(txt)
                        If CStr(dr("fld_type")) = "Website" Then
                            btn = New Button
                            btn.CssClass = "button"
                            btn.Text = "Go"
                            btn.Attributes.Add("onclick", "return OpenWebsite('" & txt.ID & "');")
                            btn.Enabled = Not isReadOnly
                            btn.TabIndex = tabIndex
                            tabIndex = CShort(tabIndex + 1)
                            divFormGroupControl.Controls.Add(btn)
                        ElseIf CStr(dr("fld_type")) = "Email" Then
                            btn = New Button
                            btn.CssClass = "button"
                            btn.Text = "Go"
                            btn.Attributes.Add("onclick", "return OpenEmail('" & txt.ClientID & "'," & System.Web.HttpContext.Current.Session("CompWindow").ToString & "," & RecordID & ");")
                            btn.Enabled = Not isReadOnly
                            btn.TabIndex = tabIndex
                            tabIndex = CShort(tabIndex + 1)
                            divFormGroupControl.Controls.Add(btn)
                        End If
                    End If
                ElseIf CStr(dr("fld_type")) = "CheckBox" Then
                    chk = New CheckBox
                    chk.Checked = CBool(dr("vcValue").ToString)
                    chk.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                    'Added by sandeep to provide single selection between Lot and Serialized item in New Item form
                    If dr("vcPropertyName").ToString = "bitLotNo" Or dr("vcPropertyName").ToString = "bitSerialized" Then
                        chk.Attributes.Add("onclick", "SingleSelect(this);")
                    End If
                    chk.EnableViewState = False
                    chk.Enabled = Not isReadOnly
                    chk.TabIndex = tabIndex
                    tabIndex = CShort(tabIndex + 1)
                    divFormGroupControl.Attributes.Add("class", "col-9-12 textdiv")
                    divFormGroupControl.Controls.Add(chk)
                ElseIf CStr(dr("fld_type")) = "DateField" Then
                    Dim bizCalendar As UserControl
                    Dim _myUC_DueDate As PropertyInfo
                    Dim strDate As String
                    bizCalendar = CType(LoadControl("include/calandar.ascx"), UserControl)
                    bizCalendar.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                    bizCalendar.EnableViewState = False
                    bizCalendar.ClientIDMode = UI.ClientIDMode.AutoID
                    If Not IsDBNull(dr("vcValue")) Then
                        Dim _myControlType As Type = bizCalendar.GetType()
                        _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                        strDate = dr("vcValue").ToString
                        If strDate = "0" Then strDate = ""
                        If strDate = "01/01/0001 00:00:00" Or strDate = "01/01/1753 00:00:00" Then strDate = ""
                        If strDate <> "" Then
                            _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                        End If
                    End If
                    tabIndex = CShort(tabIndex + 1)
                    divFormGroupControl.Controls.Add(bizCalendar)
                ElseIf CStr(dr("fld_type")) = "Popup" Or CStr(dr("fld_type")) = "Label" Then
                    divFormGroupLabel.InnerText = dr("vcValue").ToString
                ElseIf CStr(dr("fld_type")) = "ListBox" Then
                    lst = New ListBox
                    lst.CssClass = "signup"
                    lst.SelectionMode = ListSelectionMode.Multiple
                    lst.Height = 40
                    lst.EnableViewState = False
                    lst.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString
                    lst.DataSource = dtSource
                    lst.DataValueField = dtSource.Columns(0).ColumnName
                    lst.DataTextField = dtSource.Columns(1).ColumnName
                    lst.DataBind()
                    Dim i As Integer = 0
                    For i = 0 To dtSel.Rows.Count - 1
                        If Not lst.Items.FindByValue(dtSel.Rows(i).Item(0).ToString) Is Nothing Then
                            lst.Items.FindByValue(dtSel.Rows(i).Item(0).ToString).Selected = True
                        End If
                    Next
                    lst.Enabled = Not isReadOnly
                    lst.TabIndex = tabIndex
                    tabIndex = CShort(tabIndex + 1)
                    divFormGroupControl.Controls.Add(lst)
                End If
            End If

            ctl.Controls.Add(divFormGroup)
        End Function

        Public Function SetValueForStaticFields(ByVal dr As DataRow, ByRef obj As Object, ByRef objUIControl As Control, Optional ByRef flag As Boolean = True) As Boolean
            Dim objPropertyInfo As PropertyInfo

            dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")

            If CStr(dr("fld_type")) = "SelectBox" Then
                Dim ctl As Control
                ctl = objUIControl.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString)

                If Not ctl Is Nothing AndAlso TypeOf ctl Is RadComboBox Then
                    radcmb = CType(ctl, RadComboBox)
                    If (flag) Then
                        obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CInt(radcmb.SelectedValue), Nothing)
                    Else
                        obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, radcmb.SelectedItem.Text, Nothing)
                    End If
                Else
                    ddl = CType(ctl, DropDownList)
                    If (flag) Then
                        If DirectCast(DirectCast(obj.GetType.GetProperty(CCommon.ToString(dr("vcPropertyName"))), System.Reflection.PropertyInfo).PropertyType, System.Type).Name = "Int16" Then
                            obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CCommon.ToShort(ddl.SelectedValue), Nothing)
                        Else
                            obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CInt(ddl.SelectedValue), Nothing)
                        End If

                    Else
                        obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, ddl.SelectedItem.Text, Nothing)
                    End If
                End If
            ElseIf CStr(dr("fld_type")) = "TextBox" Or CStr(dr("fld_type")) = "Email" Or CStr(dr("fld_type")) = "Website" Or CStr(dr("fld_type")) = "TextArea" Then
                If CCommon.ToString(dr("vcPropertyName")) <> "vcCompactContactDetails" Then
                    txt = CType(objUIControl.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString), TextBox)
                    objPropertyInfo = obj.GetType.GetProperty(dr("vcPropertyName").ToString)

                    If objPropertyInfo.PropertyType.FullName = "System.Decimal" Then
                        objPropertyInfo.SetValue(obj, CType(txt.Text.Trim(), Decimal), Nothing)
                    Else
                        objPropertyInfo.SetValue(obj, txt.Text.Trim(), Nothing)
                    End If
                End If
            ElseIf CStr(dr("fld_type")) = "CheckBox" Then
                chk = CType(objUIControl.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString), CheckBox)
                obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CBool(chk.Checked), Nothing)
            ElseIf CStr(dr("fld_type")) = "DateField" Then
                Dim BizCalendar As UserControl
                BizCalendar = CType(objUIControl.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString), UserControl)
                Dim strDueDate As String
                Dim _myControlType As Type = BizCalendar.GetType()
                Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                If Not _myUC_DueDate.GetValue(BizCalendar, Nothing) Is Nothing Then
                    strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing).ToString
                    If strDueDate <> "" Then
                        obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CDate(strDueDate), Nothing)
                    Else
                        obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CDate("1/1/1753"), Nothing)
                    End If
                Else
                    obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CDate("1/1/1753"), Nothing)
                End If

            End If
        End Function


        Public Function SetValueForStaticFields(ByVal dr As DataRow, ByRef obj As Object, ByRef objUIControl As Table) As Boolean
            Dim objPropertyInfo As PropertyInfo
            If CStr(dr("fld_type")) = "SelectBox" Then
                ddl = CType(objUIControl.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString), DropDownList)
                obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CInt(ddl.SelectedValue), Nothing)
            ElseIf CStr(dr("fld_type")) = "TextBox" Or CStr(dr("fld_type")) = "Email" Or CStr(dr("fld_type")) = "Website" Or CStr(dr("fld_type")) = "TextArea" Then
                txt = CType(objUIControl.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString), TextBox)
                objPropertyInfo = obj.GetType.GetProperty(dr("vcPropertyName").ToString)

                If objPropertyInfo.PropertyType.FullName = "System.Decimal" Then
                    'If condition is added to avoid error when textbox is empty
                    'If Not String.IsNullOrEmpty(txt.Text.Trim()) Then
                    objPropertyInfo.SetValue(obj, CType(txt.Text.Trim(), Decimal), Nothing)
                    'End If
                    'Added by sandeep to for double type
                ElseIf objPropertyInfo.PropertyType.FullName = "System.Double" Then
                    'If Not String.IsNullOrEmpty(txt.Text.Trim()) Then
                    objPropertyInfo.SetValue(obj, CCommon.ToDouble(txt.Text.Trim()), Nothing)
                    'End If
                Else
                    objPropertyInfo.SetValue(obj, txt.Text.Trim(), Nothing)
                End If
            ElseIf CStr(dr("fld_type")) = "CheckBox" Then
                chk = CType(objUIControl.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString), CheckBox)
                obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CBool(chk.Checked), Nothing)
            ElseIf CStr(dr("fld_type")) = "DateField" Then
                Dim BizCalendar As UserControl
                BizCalendar = CType(objUIControl.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString), UserControl)
                Dim strDueDate As String
                Dim _myControlType As Type = BizCalendar.GetType()
                Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                If Not _myUC_DueDate.GetValue(BizCalendar, Nothing) Is Nothing Then
                    strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing).ToString
                    If strDueDate <> "" Then
                        obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CDate(strDueDate), Nothing)
                    Else
                        obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CDate("1/1/1753"), Nothing)
                    End If
                Else
                    obj.GetType.GetProperty(dr("vcPropertyName").ToString).SetValue(obj, CDate("1/1/1753"), Nothing)
                End If

            End If
        End Function

        Public Function SetValueForComments(ByRef obj As Object, ByRef objUIControl As Control) As Boolean
            txt = CType(objUIControl.FindControl("txtComment"), TextBox)
            obj = txt.Text
        End Function

        'Public Function SetValueForComments(ByRef obj As Opportunities.OppotunitiesIP, ByRef objUIControl As Table) As Boolean
        '    txt = CType(objUIControl.FindControl("txtComment"), TextBox)
        '    obj.OppComments = txt.Text
        'End Function

        Public Function SetValueForInternalComments(ByRef obj As Object, ByRef objUIControl As Control) As Boolean
            txt = CType(objUIControl.FindControl("txtInternalComment"), TextBox)
            obj = txt.Text
        End Function



        Public Function CreateComments(ByVal tbl As Table, ByVal Comments As String, ByVal IntermediatoryPage As Boolean) As Boolean
            tblRow = New TableRow
            tblCell = New TableCell
            tblCell.Font.Bold = True

            tblCell.CssClass = "normal1Right"
            tblCell.Text = "Comments" & IIf(IntermediatoryPage, " : ", "").ToString
            tblCell.HorizontalAlign = HorizontalAlign.Right
            tblRow.Cells.Add(tblCell)

            If IntermediatoryPage = False Then
                tblCell = New TableCell
                tblCell.ColumnSpan = 3
                txt = New TextBox
                txt.ID = "txtComment"
                txt.Text = Replace(Comments, "''''", "'")
                txt.Text = Replace(txt.Text, "''", "'")
                txt.CssClass = "signup"
                txt.TextMode = TextBoxMode.MultiLine
                txt.Width = 400
                txt.Height = 80
                tblCell.CssClass = "normal1"
                tblCell.Controls.Add(txt)
                tblRow.Cells.Add(tblCell)
            Else
                tblCell = New TableCell
                tblCell.ColumnSpan = 3
                tblCell.Text = Replace(Comments, "''''", "'")
                tblCell.Text = "<pre  class=""WordWrap"">" & Replace(tblCell.Text, "''", "'") & "</pre>"
                tblCell.CssClass = "normal1"
                tblRow.Cells.Add(tblCell)
            End If
            tbl.Rows.Add(tblRow)
        End Function


        Public Function CreateInternalComments(ByVal tbl As Table, ByVal Comments As String, ByVal IntermediatoryPage As Boolean) As Boolean
            tblRow = New TableRow
            tblCell = New TableCell
            tblCell.Font.Bold = True
            tblCell.CssClass = "normal1Right"
            tblCell.Text = "Internal Comments" & IIf(IntermediatoryPage, " : ", "").ToString
            tblCell.HorizontalAlign = HorizontalAlign.Right
            tblRow.Cells.Add(tblCell)

            If IntermediatoryPage = False Then
                tblCell = New TableCell
                tblCell.ColumnSpan = 3
                txt = New TextBox
                txt.ID = "txtInternalComment"
                txt.Text = Comments
                txt.CssClass = "signup"
                txt.TextMode = TextBoxMode.MultiLine
                txt.Width = 400
                txt.Height = 80
                tblCell.CssClass = "normal1"
                tblCell.Controls.Add(txt)
                tblRow.Cells.Add(tblCell)
            Else
                tblCell = New TableCell
                tblCell.ColumnSpan = 3
                tblCell.Text = Comments
                tblCell.CssClass = "normal1"
                tblRow.Cells.Add(tblCell)
            End If
            tbl.Rows.Add(tblRow)
        End Function
        Function DisplayDynamicFlds(ByVal RecordID As Long, ByVal DependentID As Long, ByVal DomainID As Long, ByVal LocationID As Integer, ByRef uwTab As Telerik.Web.UI.RadTabStrip, ByVal FormID As Long) As Boolean
            Try
                Dim strDate As String
                Dim bizCalendar As UserControl
                Dim _myUC_DueDate As PropertyInfo
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim ds As DataSet
                Dim dtTabs As DataTable

                objCustomFields.locId = LocationID
                objCustomFields.RelId = DependentID
                objCustomFields.DomainID = DomainID
                objCustomFields.RecordId = RecordID
                objCustomFields.UserCntID = CLng(Session("UserContactId"))
                objCustomFields.FormId = FormID

                ds = objCustomFields.GetCustFlds
                dtCustomFields = ds.Tables(0)
                dtTabs = ds.Tables(1)

                Dim intCol1Count, intCol2Count As Integer
                Dim NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0

                Dim Table As Table
                Dim aspTable As HtmlTable

                'dtCustomFields = New DataTable
                Dim dtCustomTabField As DataTable

                For Each dr As DataRow In dtTabs.Rows
                    Table = New Table
                    Table.Width = Unit.Percentage(100)
                    Table.GridLines = GridLines.None
                    Table.Height = Unit.Pixel(300)
                    Table.CssClass = "aspTable"

                    tblCell = New TableCell
                    tblRow = New TableRow
                    tblCell.VerticalAlign = VerticalAlign.Top

                    aspTable = New HtmlTable
                    aspTable.ID = "aspTable" & dr("TabId").ToString

                    aspTable.Width = "100%"

                    objRow = New HtmlTableRow
                    objCell = New HtmlTableCell
                    objCell.ColSpan = 4
                    objCell.InnerHtml = "<div class=""btn btn-default btn-sm"" style=""float: right"" onclick=""return ShowCustomTabLayout('" & FormID & "','" & dr("TabId").ToString & "','" & DependentID & "','" & LocationID & "');""><span><i class=""fa fa-columns""></i>&nbsp;&nbsp;Layout</span></div>"
                    objRow.Cells.Add(objCell)
                    aspTable.Rows.Add(objRow)

                    tblCell.Controls.Add(aspTable)
                    tblRow.Cells.Add(tblCell)
                    Table.Rows.Add(tblRow)

                    uwTab.Tabs.FindTabByValue(dr("TabId").ToString).PageView.Controls.Add(Table)

                    objCustomFields.Mode = 1
                    objCustomFields.TabId = CLng(dr("TabId"))
                    ds = objCustomFields.GetCustomTabGetFields

                    dtCustomTabField = ds.Tables(0)
                    'dtCustomFields.Merge(dtCustomTabField)

                    If dtCustomTabField.Rows.Count > 0 Then
                        intCol1Count = CInt(dtCustomTabField.Compute("Count(intcoulmn)", "intcoulmn=1"))
                        intCol2Count = CInt(dtCustomTabField.Compute("Count(intcoulmn)", "intcoulmn=2"))
                        ViewState("PreviouTabID") = 0
                        ColCount = 0
                        intCol1 = 0
                        intCol2 = 0

                        For Each drFields As DataRow In dtCustomTabField.Rows

                            If ColCount = 0 Then
                                objRow = New HtmlTableRow
                            End If

                            If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                                objCell = New HtmlTableCell
                                objCell.Attributes.Add("class", "normal1")
                                objRow.Cells.Add(objCell)

                                objCell = New HtmlTableCell
                                objCell.Attributes.Add("class", "normal1")
                                objRow.Cells.Add(objCell)
                                ColCount = 1
                            End If

                            If CType(drFields("TabId"), Integer) <> 0 Then
                                If CInt(drFields("TabId")) <> CInt(ViewState("PreviouTabID")) Then
                                    aspTable = CType(uwTab.Tabs.FindTabByValue(drFields("TabId").ToString()).PageView.FindControl("aspTable" & drFields("TabId").ToString), HtmlTable)
                                    ViewState("PreviouTabID") = drFields("TabId")
                                    objRow = New HtmlTableRow
                                End If

                                objCell = New HtmlTableCell
                                objCell.Align = "right"
                                objCell.Attributes.Add("class", "normal1")
                                If drFields("fld_type").ToString <> "Link" Then
                                    objCell.InnerText = drFields("fld_label").ToString
                                End If

                                If dtCustomFields.Columns.Contains("vcToolTip") Then
                                    If Not IsDBNull(drFields("vcToolTip")) And drFields("vcToolTip").ToString.Trim.Length > 0 Then
                                        Dim lblToolTip As Label
                                        lblToolTip = New Label
                                        lblToolTip.Text = "[?]"
                                        lblToolTip.CssClass = "tip"
                                        lblToolTip.ToolTip = drFields("vcToolTip").ToString.Trim
                                        objCell.Controls.Add(lblToolTip)
                                    End If
                                End If

                                objRow.Cells.Add(objCell)
                                If drFields("fld_type").ToString = "TextBox" Then
                                    objCell = New HtmlTableCell
                                    CreateTexBox(objRow, objCell, drFields("fld_id").ToString & drFields("fld_label").ToString, drFields("Value").ToString)
                                ElseIf drFields("fld_type").ToString = "SelectBox" Then
                                    objCell = New HtmlTableCell
                                    CreateDropdown(objRow, objCell, drFields("fld_id").ToString & drFields("fld_label").ToString & "~" & IIf(IsDBNull(drFields("numListId")), 0, drFields("numListId").ToString).ToString, CInt(drFields("Value")), CInt(drFields("numlistid")))
                                ElseIf drFields("fld_type").ToString = "CheckBox" Then
                                    objCell = New HtmlTableCell
                                    CreateChkBox(objRow, objCell, drFields("fld_id").ToString & drFields("fld_label").ToString, CInt(drFields("Value")))
                                ElseIf drFields("fld_type").ToString = "TextArea" Then
                                    objCell = New HtmlTableCell
                                    CreateTextArea(objRow, objCell, drFields("fld_id").ToString & drFields("fld_label").ToString, drFields("Value").ToString)
                                ElseIf drFields("fld_type").ToString = "DateField" Then
                                    objCell = New HtmlTableCell
                                    bizCalendar = CType(LoadControl("include/calandar.ascx"), UserControl)
                                    bizCalendar.ID = drFields("fld_id").ToString & drFields("fld_label").ToString
                                    bizCalendar.EnableViewState = False
                                    bizCalendar.ClientIDMode = UI.ClientIDMode.AutoID
                                    If Not IsDBNull(drFields("Value")) Then
                                        Dim _myControlType As Type = bizCalendar.GetType()
                                        _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                                        strDate = drFields("Value").ToString
                                        If strDate = "0" Then strDate = ""
                                        If strDate = "01/01/0001 00:00:00" Or strDate = "01/01/1753 00:00:00" Then strDate = ""
                                        If strDate <> "" Then
                                            _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                                        End If
                                    End If
                                    objCell.Controls.Add(bizCalendar)
                                    objRow.Cells.Add(objCell)
                                ElseIf drFields("fld_type").ToString = "Link" Then
                                    objCell = New HtmlTableCell
                                    CreateLink(objRow, objCell, drFields("fld_id").ToString & drFields("fld_label").ToString, drFields("vcURL").ToString, RecordID, drFields("fld_label").ToString)
                                ElseIf drFields("fld_type").ToString = "Frame" Then
                                    objCell = New HtmlTableCell
                                    Dim strFrame As String
                                    Dim URL As String
                                    URL = drFields("vcURL").ToString
                                    URL = URL.Replace("RecordID", RecordID.ToString)
                                    strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                    objCell.Controls.Add(New LiteralControl(strFrame))
                                    objRow.Cells.Add(objCell)
                                End If
                                If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                                    objCell = New HtmlTableCell
                                    objCell.Attributes.Add("class", "normal1")
                                    objRow.Cells.Add(objCell)

                                    objCell = New HtmlTableCell
                                    objCell.Attributes.Add("class", "normal1")
                                    objRow.Cells.Add(objCell)

                                    ColCount = 1
                                End If


                                If CInt(drFields("intcoulmn").ToString) = 2 Then
                                    If intCol1 <> intCol1Count Then
                                        intCol1 = intCol1 + 1
                                    End If

                                    If intCol2 <> intCol2Count Then
                                        intCol2 = intCol2 + 1
                                    End If
                                End If

                                ColCount = ColCount + 1
                                If NoOfColumns = ColCount Then
                                    ColCount = 0
                                    aspTable.Rows.Add(objRow)
                                End If

                            End If
                        Next

                        If ColCount > 0 Then
                            aspTable.Rows.Add(objRow)
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DisplayDynamicFlds(ByVal RecordID As Long, ByVal DependentID As Long, ByVal DomainID As Long, ByVal LocationID As Integer, ByRef uwTab As Infragistics.Web.UI.LayoutControls.WebTab) As Boolean
            Try
                Dim strDate As String
                Dim bizCalendar As UserControl
                Dim _myUC_DueDate As PropertyInfo
                Dim objRow As HtmlTableRow
                Dim objCell As HtmlTableCell
                Dim i, k As Integer
                Dim ds As DataSet
                Dim dtTabs As DataTable

                objCustomFields.locId = LocationID
                objCustomFields.RelId = DependentID
                objCustomFields.DomainID = DomainID
                objCustomFields.RecordId = RecordID
                ds = objCustomFields.GetCustFlds
                dtCustomFields = ds.Tables(0)
                dtTabs = ds.Tables(1)



                Dim Table As Table
                Dim aspTable As HtmlTable
                Dim Tab As Infragistics.Web.UI.LayoutControls.ContentTabItem

                For Each dr As DataRow In dtTabs.Rows
                    'Below section is moved to objCommon.GetAuthorizedSubTabs
                    'If uwTab.Tabs.FromKeyTab(dr("TabId").ToString) Is Nothing Then
                    '    Tab = New Tab
                    '    Tab.Key = dr("TabId").ToString
                    '    Tab.Text = "&nbsp;&nbsp;" & dr("tabname").ToString & "&nbsp;&nbsp;"
                    '    uwTab.Tabs.Add(Tab)
                    'End If
                    Table = New Table
                    Table.Width = Unit.Percentage(100)
                    Table.BorderColor = System.Drawing.Color.FromName("black")
                    Table.GridLines = GridLines.None
                    Table.BorderWidth = Unit.Pixel(1)
                    Table.Height = Unit.Pixel(300)
                    Table.CssClass = "aspTable"
                    tblCell = New TableCell
                    tblRow = New TableRow
                    tblCell.VerticalAlign = VerticalAlign.Top

                    aspTable = New HtmlTable
                    aspTable.ID = "aspTable" & dr("TabId").ToString

                    aspTable.Width = "100%"
                    objRow = New HtmlTableRow
                    objCell = New HtmlTableCell
                    objCell.InnerHtml = "<br>"
                    objRow.Cells.Add(objCell)
                    aspTable.Rows.Add(objRow)


                    tblCell.Controls.Add(aspTable)
                    tblRow.Cells.Add(tblCell)
                    Table.Rows.Add(tblRow)
                    uwTab.Tabs.FindTabFromKey(dr("TabId").ToString).Controls.Add(Table)
                Next


                If dtCustomFields.Rows.Count > 0 Then
                    ViewState("PreviouTabID") = 0
                    k = 0
                    ' Tabstrip3.Items.Clear()

                    For i = 0 To dtCustomFields.Rows.Count - 1

                        If CType(dtCustomFields.Rows(i).Item("TabId"), Integer) <> 0 Then
                            If CInt(dtCustomFields.Rows(i).Item("TabId")) <> CInt(ViewState("PreviouTabID")) Then
                                If k > 0 Then
                                    aspTable.Rows.Add(objRow)
                                End If
                                aspTable = CType(uwTab.FindControl("aspTable" & dtCustomFields.Rows(i).Item("TabId").ToString), HtmlTable)
                                ViewState("PreviouTabID") = dtCustomFields.Rows(i).Item("TabId")
                                k = 0
                                objRow = New HtmlTableRow
                            End If
                            If k = 3 Then
                                k = 0
                                aspTable.Rows.Add(objRow)
                                objRow = New HtmlTableRow
                            End If
                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Attributes.Add("class", "normal1")
                            If dtCustomFields.Rows(i).Item("fld_type").ToString <> "Link" Then
                                objCell.InnerText = dtCustomFields.Rows(i).Item("fld_label").ToString
                            End If

                            If dtCustomFields.Columns.Contains("vcToolTip") Then
                                If Not IsDBNull(dtCustomFields.Rows(i).Item("vcToolTip")) And dtCustomFields.Rows(i).Item("vcToolTip").ToString.Trim.Length > 0 Then
                                    Dim lblToolTip As Label
                                    lblToolTip = New Label
                                    lblToolTip.Text = "[?]"
                                    lblToolTip.CssClass = "tip"
                                    lblToolTip.ToolTip = dtCustomFields.Rows(i).Item("vcToolTip").ToString.Trim
                                    objCell.Controls.Add(lblToolTip)
                                End If
                            End If

                            objRow.Cells.Add(objCell)
                            If dtCustomFields.Rows(i).Item("fld_type").ToString = "TextBox" Then
                                objCell = New HtmlTableCell
                                CreateTexBox(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString, dtCustomFields.Rows(i).Item("Value").ToString)
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "SelectBox" Then
                                objCell = New HtmlTableCell
                                CreateDropdown(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString & "~" & IIf(IsDBNull(dtCustomFields.Rows(i).Item("numListId")), 0, dtCustomFields.Rows(i).Item("numListId").ToString).ToString, CInt(dtCustomFields.Rows(i).Item("Value")), CInt(dtCustomFields.Rows(i).Item("numlistid")))
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "CheckBox" Then
                                objCell = New HtmlTableCell
                                CreateChkBox(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString, CInt(dtCustomFields.Rows(i).Item("Value")))
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "TextArea" Then
                                objCell = New HtmlTableCell
                                CreateTextArea(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString, dtCustomFields.Rows(i).Item("Value").ToString)
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "DateField" Then
                                objCell = New HtmlTableCell
                                bizCalendar = CType(LoadControl("include/calandar.ascx"), UserControl)
                                bizCalendar.ID = dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString
                                bizCalendar.EnableViewState = False
                                bizCalendar.ClientIDMode = UI.ClientIDMode.AutoID
                                If Not IsDBNull(dtCustomFields.Rows(i).Item("Value")) Then
                                    Dim _myControlType As Type = bizCalendar.GetType()
                                    _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                                    strDate = dtCustomFields.Rows(i).Item("Value").ToString
                                    If strDate = "0" Then strDate = ""
                                    If strDate = "01/01/0001 00:00:00" Or strDate = "01/01/1753 00:00:00" Then strDate = ""
                                    If strDate <> "" Then
                                        _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                                    End If
                                End If
                                objCell.Controls.Add(bizCalendar)
                                objRow.Cells.Add(objCell)
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "Link" Then
                                objCell = New HtmlTableCell
                                CreateLink(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString, dtCustomFields.Rows(i).Item("vcURL").ToString, RecordID, dtCustomFields.Rows(i).Item("fld_label").ToString)
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "Frame" Then
                                objCell = New HtmlTableCell
                                Dim strFrame As String
                                Dim URL As String
                                URL = dtCustomFields.Rows(i).Item("vcURL").ToString
                                URL = URL.Replace("RecordID", RecordID.ToString)
                                strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                objCell.Controls.Add(New LiteralControl(strFrame))
                                objRow.Cells.Add(objCell)
                            End If
                            k = k + 1
                        End If
                    Next
                    If k > 0 Then
                        aspTable.Rows.Add(objRow)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DisplayDynamicFlds(ByVal RecordID As Long, ByVal DependentID As Long, ByVal DomainID As Long, ByVal LocationID As Integer, ByRef uwTab As Infragistics.Web.UI.LayoutControls.WebTab, ByRef pnlTab As Control) As Boolean
            Try
                Dim objPageLayout As New CPageLayout
                Dim ds As DataSet

                objPageLayout.CoType = CChar("T")
                objPageLayout.UserCntID = CInt(Session("UserContactId"))
                objPageLayout.RecordId = RecordID
                objPageLayout.DomainID = CLng(Session("DomainID"))
                objPageLayout.PageId = 1
                objPageLayout.numRelCntType = DependentID

                objPageLayout.FormId = 42
                objPageLayout.PageType = 3

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtCustomFields = ds.Tables(0)

                If dtCustomFields.Columns("Value") IsNot Nothing Then

                    Dim strDate As String
                    Dim bizCalendar As UserControl
                    Dim _myUC_DueDate As PropertyInfo
                    Dim objRow As HtmlTableRow
                    Dim objCell As HtmlTableCell
                    Dim i, k As Integer

                    'Dim Table As Table
                    'Dim aspTable As HtmlTable
                    'Dim Tab As Tab


                    'Table = New Table
                    ''Table.Width = Unit.Percentage(100)
                    'Table.BorderColor = System.Drawing.Color.FromName("black")
                    'Table.GridLines = GridLines.None
                    'Table.Width = Unit.Percentage(100)
                    ''Table.BorderWidth = Unit.Pixel(1)
                    ''Table.Height = Unit.Pixel(300)
                    ''Table.CssClass = "aspTable"
                    'tblCell = New TableCell
                    'tblRow = New TableRow
                    'tblCell.VerticalAlign = VerticalAlign.Top

                    'aspTable = New HtmlTable
                    'aspTable.ID = "aspTable"

                    'aspTable.Width = "100%"
                    'objRow = New HtmlTableRow
                    'objCell = New HtmlTableCell
                    'aspTable.Rows.Add(objRow)


                    'tblCell.Controls.Add(aspTable)
                    'tblRow.Cells.Add(tblCell)
                    'Table.Rows.Add(tblRow)

                    'pnlTab.Controls.Add(Table)

                    If dtCustomFields.Rows.Count > 0 Then
                        k = 0
                        ' Tabstrip3.Items.Clear()
                        objRow = New HtmlTableRow

                        For i = 0 To dtCustomFields.Rows.Count - 1

                            'aspTable = CType(pnlTab.FindControl("aspTable"), HtmlTable)

                            'objRow = New HtmlTableRow


                            objCell = New HtmlTableCell
                            objCell.Align = "right"
                            objCell.Attributes.Add("class", "normal1")
                            If dtCustomFields.Rows(i).Item("fld_type").ToString <> "Link" Then
                                objCell.InnerText = dtCustomFields.Rows(i).Item("vcFieldName").ToString
                            End If
                            objCell.Width = "9%"
                            objRow.Cells.Add(objCell)

                            'objCell = New HtmlTableCell
                            'objCell.Align = "right"
                            'objCell.Attributes.Add("class", "normal1")
                            'objCell.InnerText = dtCustomFields.Rows(i).Item("Value").ToString
                            'objRow.Cells.Add(objCell)

                            'objCell = New HtmlTableCell
                            'objCell.Align = "right"
                            'objCell.Attributes.Add("class", "normal1")

                            If dtCustomFields.Rows(i).Item("fld_type").ToString = "TextBox" Then
                                objCell = New HtmlTableCell
                                CreateTexBox(objRow, objCell, dtCustomFields.Rows(i).Item("numFieldId").ToString & dtCustomFields.Rows(i).Item("vcFieldName").ToString, dtCustomFields.Rows(i).Item("Value").ToString)
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "SelectBox" Then
                                objCell = New HtmlTableCell
                                CreateDropdown(objRow, objCell, dtCustomFields.Rows(i).Item("numFieldId").ToString & dtCustomFields.Rows(i).Item("vcFieldName").ToString & "~" & IIf(IsDBNull(dtCustomFields.Rows(i).Item("numListId")), 0, dtCustomFields.Rows(i).Item("numListId").ToString).ToString, CInt(IIf(IsDBNull(dtCustomFields.Rows(i).Item("Value")), 0, dtCustomFields.Rows(i).Item("Value"))), CInt(dtCustomFields.Rows(i).Item("numlistid")))
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "CheckBox" Then
                                objCell = New HtmlTableCell
                                CreateChkBox(objRow, objCell, dtCustomFields.Rows(i).Item("numFieldId").ToString & dtCustomFields.Rows(i).Item("vcFieldName").ToString, CInt(IIf(IsDBNull(dtCustomFields.Rows(i).Item("Value")), 0, dtCustomFields.Rows(i).Item("Value"))))
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "TextArea" Then
                                objCell = New HtmlTableCell
                                CreateTextArea(objRow, objCell, dtCustomFields.Rows(i).Item("numFieldId").ToString & dtCustomFields.Rows(i).Item("vcFieldName").ToString, dtCustomFields.Rows(i).Item("Value").ToString)
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "DateField" Then
                                objCell = New HtmlTableCell
                                bizCalendar = CType(LoadControl("include/calandar.ascx"), UserControl)
                                bizCalendar.ID = dtCustomFields.Rows(i).Item("numFieldId").ToString & dtCustomFields.Rows(i).Item("vcFieldName").ToString
                                bizCalendar.EnableViewState = False
                                bizCalendar.ClientIDMode = UI.ClientIDMode.AutoID
                                If Not IsDBNull(dtCustomFields.Rows(i).Item("Value")) Then
                                    Dim _myControlType As Type = bizCalendar.GetType()
                                    _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                                    strDate = dtCustomFields.Rows(i).Item("Value").ToString
                                    If strDate = "0" Then strDate = ""
                                    If strDate = "01/01/0001 00:00:00" Or strDate = "01/01/1753 00:00:00" Then strDate = ""
                                    If strDate <> "" Then
                                        _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                                    End If
                                End If
                                objCell.Width = "41%"
                                objCell.Controls.Add(bizCalendar)
                                objRow.Cells.Add(objCell)
                            End If
                            'If dtCustomFields.Rows(i).Item("fld_type").ToString = "TextBox" Or dtCustomFields.Rows(i).Item("fld_type").ToString = "TextArea" Or dtCustomFields.Rows(i).Item("fld_type").ToString = "DateField" Then
                            '    objCell.InnerText = dtCustomFields.Rows(i).Item("vcValue").ToString
                            '    objRow.Cells.Add(objCell)
                            '    'CreateTexBox(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString, dtCustomFields.Rows(i).Item("Value").ToString)
                            'ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "SelectBox" Then
                            '    If CInt(IIf(IsDBNull(dtCustomFields.Rows(i).Item("vcValue")), 0, dtCustomFields.Rows(i).Item("vcValue"))) > 0 Then
                            '        Dim dtTable As DataTable
                            '        Dim ObjCusfld As New CustomFields
                            '        ObjCusfld.ListId = CInt(dtCustomFields.Rows(i).Item("numlistid"))
                            '        dtTable = ObjCusfld.GetMasterListByListId
                            '        Dim dRows() As DataRow

                            '        dRows = dtTable.Select("numListItemID = " & CInt(dtCustomFields.Rows(i).Item("vcValue")), "")

                            '        objCell.InnerText = dRows(0)("vcData").ToString()
                            '        objRow.Cells.Add(objCell)
                            '    End If

                            '    'CreateDropdown(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString & "~" & IIf(IsDBNull(dtCustomFields.Rows(i).Item("numListId")), 0, dtCustomFields.Rows(i).Item("numListId").ToString).ToString, CInt(dtCustomFields.Rows(i).Item("Value")), CInt(dtCustomFields.Rows(i).Item("numlistid")))
                            'ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "CheckBox" Then

                            '    objCell.InnerText = CStr(IIf(CInt(dtCustomFields.Rows(i).Item("vcValue")) = 1, True, False))
                            '    objRow.Cells.Add(objCell)
                            '    'CreateChkBox(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString, CInt(dtCustomFields.Rows(i).Item("Value")))
                            'End If

                            If k = 1 Then
                                k = 0
                                'aspTable.Rows.Add(objRow)
                                pnlTab.Controls.Add(objRow)
                                objRow = New HtmlTableRow
                            Else
                                k = k + 1
                            End If
                        Next
                        If k = 1 Then
                            'aspTable.Rows.Add(objRow)
                            pnlTab.Controls.Add(objRow)
                        End If
                    End If
                End If

                'Dim strDate As String
                'Dim bizCalendar As UserControl
                'Dim _myUC_DueDate As PropertyInfo
                'Dim objRow As HtmlTableRow
                'Dim objCell As HtmlTableCell
                'Dim i, k As Integer
                'Dim ds As DataSet
                'Dim dtTabs As DataTable

                'objCustomFields.locId = LocationID
                'objCustomFields.RelId = DependentID
                'objCustomFields.DomainID = DomainID
                'objCustomFields.RecordId = RecordID
                'ds = objCustomFields.GetCustFlds
                'dtCustomFields = ds.Tables(0)
                'dtTabs = ds.Tables(1)

                'Dim objlayout As New CcustPageLayout

                'Dim ds1 As New DataSet
                'Dim dttable2 As DataTable
                'objlayout.DomainID = CLng(Session("domainId"))
                'objlayout.UserCntId = CLng(Session("UserContactId"))
                'objlayout.ColumnID = 1
                'objlayout.PageId = LocationID
                'objlayout.numRelation = DependentID
                'objlayout.CoType = CChar("T")
                'ds1 = objlayout.getValuesWithddl()
                'dttable2 = ds1.Tables(1)


                'Dim Table As Table
                'Dim aspTable As HtmlTable
                'Dim Tab As Tab


                'Table = New Table
                'Table.Width = Unit.Percentage(100)
                'Table.BorderColor = System.Drawing.Color.FromName("black")
                'Table.GridLines = GridLines.None
                'Table.BorderWidth = Unit.Pixel(1)
                'Table.Height = Unit.Pixel(300)
                'Table.CssClass = "aspTable"
                'tblCell = New TableCell
                'tblRow = New TableRow
                'tblCell.VerticalAlign = VerticalAlign.Top

                'aspTable = New HtmlTable
                'aspTable.ID = "aspTable"

                'aspTable.Width = "100%"
                'objRow = New HtmlTableRow
                'objCell = New HtmlTableCell
                'objCell.InnerHtml = "<br>"
                'objRow.Cells.Add(objCell)
                'aspTable.Rows.Add(objRow)


                'tblCell.Controls.Add(aspTable)
                'tblRow.Cells.Add(tblCell)
                'Table.Rows.Add(tblRow)

                'pnlTab.Controls.Add(Table)


                'If dtCustomFields.Rows.Count > 0 Then
                '    ViewState("PreviouTabID") = 0
                '    k = 0
                '    ' Tabstrip3.Items.Clear()
                '    For Each drField As DataRow In dttable2.Rows

                '        For i = 0 To dtCustomFields.Rows.Count - 1
                '            If CInt(drField("numFieldId")) = CInt(dtCustomFields.Rows(i).Item("fld_id")) Then

                '                If k > 0 Then
                '                    aspTable.Rows.Add(objRow)
                '                End If

                '                aspTable = CType(pnlTab.FindControl("aspTable"), HtmlTable)
                '                k = 0
                '                objRow = New HtmlTableRow

                '                If k = 3 Then
                '                    k = 0
                '                    aspTable.Rows.Add(objRow)
                '                    objRow = New HtmlTableRow
                '                End If
                '                objCell = New HtmlTableCell
                '                objCell.Align = "right"
                '                objCell.Attributes.Add("class", "normal1")
                '                If dtCustomFields.Rows(i).Item("fld_type").ToString <> "Link" Then
                '                    objCell.InnerText = dtCustomFields.Rows(i).Item("fld_label").ToString
                '                End If
                '                objRow.Cells.Add(objCell)

                '                'objCell = New HtmlTableCell
                '                'objCell.Align = "right"
                '                'objCell.Attributes.Add("class", "normal1")
                '                'objCell.InnerText = dtCustomFields.Rows(i).Item("Value").ToString
                '                'objRow.Cells.Add(objCell)

                '                objCell = New HtmlTableCell
                '                objCell.Align = "right"
                '                objCell.Attributes.Add("class", "normal1")

                '                If dtCustomFields.Rows(i).Item("fld_type").ToString = "TextBox" Or dtCustomFields.Rows(i).Item("fld_type").ToString = "TextArea" Or dtCustomFields.Rows(i).Item("fld_type").ToString = "DateField" Then
                '                    objCell.InnerText = dtCustomFields.Rows(i).Item("value").ToString
                '                    objRow.Cells.Add(objCell)
                '                    'CreateTexBox(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString, dtCustomFields.Rows(i).Item("Value").ToString)
                '                ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "SelectBox" Then
                '                    If CInt(dtCustomFields.Rows(i).Item("Value")) > 0 Then
                '                        Dim dtTable As DataTable
                '                        Dim ObjCusfld As New CustomFields
                '                        ObjCusfld.ListId = CInt(dtCustomFields.Rows(i).Item("numlistid"))
                '                        dtTable = ObjCusfld.GetMasterListByListId
                '                        Dim dRows() As DataRow

                '                        dRows = dtTable.Select("numListItemID = " & CInt(dtCustomFields.Rows(i).Item("Value")), "")

                '                        objCell.InnerText = dRows(0)("vcData").ToString()
                '                        objRow.Cells.Add(objCell)
                '                    End If

                '                    'CreateDropdown(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString & "~" & IIf(IsDBNull(dtCustomFields.Rows(i).Item("numListId")), 0, dtCustomFields.Rows(i).Item("numListId").ToString).ToString, CInt(dtCustomFields.Rows(i).Item("Value")), CInt(dtCustomFields.Rows(i).Item("numlistid")))
                '                ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "CheckBox" Then

                '                    objCell.InnerText = CStr(IIf(CInt(dtCustomFields.Rows(i).Item("Value")) = 1, True, False))
                '                    objRow.Cells.Add(objCell)
                '                    'CreateChkBox(objRow, objCell, dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString, CInt(dtCustomFields.Rows(i).Item("Value")))
                '                End If
                '                k = k + 1
                '            End If
                '        Next
                '    Next
                '    If k > 0 Then
                '        aspTable.Rows.Add(objRow)
                '    End If
                'End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DisplayDynamicFlds(ByVal RecordID As Long, ByVal DependentID As Long, ByVal DomainID As Long, ByVal LocationID As Integer) As Boolean
            Try
                Dim ds As DataSet

                objCustomFields.locId = LocationID
                objCustomFields.RelId = DependentID
                objCustomFields.DomainID = DomainID
                objCustomFields.RecordId = RecordID
                ds = objCustomFields.GetCustFlds
                dtCustomFields = ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function SaveCusField(ByVal RecordID As Long, ByVal DependentID As Long, ByVal DomainID As Long, ByVal LocationID As Integer, ByRef objUIControl As Control) As Boolean
            Try
                If Not dtCustomFields Is Nothing Then
                    Dim i As Integer
                    For i = 0 To dtCustomFields.Rows.Count - 1
                        If IsDBNull(dtCustomFields.Rows(i).Item("FldDTLID")) Then
                            dtCustomFields.Rows(i).Item("FldDTLID") = 0
                        End If
                        dtCustomFields.Rows(i).Item("fld_label") = dtCustomFields.Rows(i).Item("fld_label").ToString.Replace("$", "")

                        If dtCustomFields.Rows(i).Item("fld_type").ToString = "TextBox" Then
                            Dim txt As TextBox
                            txt = CType(objUIControl.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), TextBox)
                            If Not txt Is Nothing Then
                                dtCustomFields.Rows(i).Item("Value") = txt.Text
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "SelectBox" Then
                            Dim ctl As Control
                            ctl = objUIControl.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString & "~" & IIf(IsDBNull(dtCustomFields.Rows(i).Item("numListId")), 0, dtCustomFields.Rows(i).Item("numListId").ToString).ToString)

                            If Not ctl Is Nothing AndAlso TypeOf ctl Is RadComboBox Then
                                Dim radcmb As RadComboBox
                                radcmb = CType(objUIControl.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString & "~" & IIf(IsDBNull(dtCustomFields.Rows(i).Item("numListId")), 0, dtCustomFields.Rows(i).Item("numListId").ToString).ToString), RadComboBox)
                                If Not radcmb Is Nothing Then
                                    dtCustomFields.Rows(i).Item("Value") = CStr(radcmb.SelectedItem.Value)
                                End If
                            Else
                                Dim ddl As DropDownList
                                ddl = CType(objUIControl.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString & "~" & IIf(IsDBNull(dtCustomFields.Rows(i).Item("numListId")), 0, dtCustomFields.Rows(i).Item("numListId").ToString).ToString), DropDownList)
                                If Not ddl Is Nothing Then
                                    dtCustomFields.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                                End If
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "CheckBox" Then
                            Dim chk As CheckBox
                            chk = CType(objUIControl.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), CheckBox)
                            If Not chk Is Nothing Then
                                If chk.Checked = True Then
                                    dtCustomFields.Rows(i).Item("Value") = "1"
                                Else : dtCustomFields.Rows(i).Item("Value") = "0"
                                End If
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "TextArea" Then
                            Dim txt As TextBox
                            txt = CType(objUIControl.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), TextBox)
                            If Not txt Is Nothing Then
                                dtCustomFields.Rows(i).Item("Value") = txt.Text
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "DateField" Then
                            Dim BizCalendar As UserControl
                            BizCalendar = CType(objUIControl.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), UserControl)
                            If Not BizCalendar Is Nothing Then
                                Dim strDueDate As String = ""
                                Dim _myControlType As Type = BizCalendar.GetType()
                                Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                                If Not _myUC_DueDate.GetValue(BizCalendar, Nothing) Is Nothing Then
                                    strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing).ToString
                                End If
                                If strDueDate <> "" Then
                                    dtCustomFields.Rows(i).Item("Value") = strDueDate
                                Else : dtCustomFields.Rows(i).Item("Value") = ""
                                End If
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "CheckBoxList" Then
                            Dim chkbl As CheckBoxList
                            chkbl = CType(objUIControl.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), CheckBoxList)
                            If Not chkbl Is Nothing Then
                                Dim selectedValue As String = ""

                                For Each listItem As ListItem In chkbl.Items
                                    If listItem.Selected Then
                                        selectedValue = selectedValue & CCommon.ToString(IIf(String.IsNullOrEmpty(selectedValue), listItem.Value, "," & listItem.Value))
                                    End If
                                Next

                                dtCustomFields.Rows(i).Item("Value") = selectedValue
                            End If
                        End If
                    Next

                    Dim ds As New DataSet
                    Dim strdetails As String
                    dtCustomFields.TableName = "Table"
                    ds.Tables.Add(dtCustomFields.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    objCustomFields.strDetails = strdetails
                    objCustomFields.locId = LocationID
                    objCustomFields.DomainID = DomainID
                    objCustomFields.RecordId = RecordID
                    objCustomFields.SaveCustomFldsByRecId()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function SaveCusField(ByVal RecordID As Long, ByVal DependentID As Long, ByVal DomainID As Long, ByVal LocationID As Integer, ByRef uwTab As Infragistics.Web.UI.LayoutControls.WebTab) As Boolean
            Try
                If Not dtCustomFields Is Nothing Then
                    Dim i As Integer
                    For i = 0 To dtCustomFields.Rows.Count - 1
                        If IsDBNull(dtCustomFields.Rows(i).Item("FldDTLID")) Then
                            dtCustomFields.Rows(i).Item("FldDTLID") = 0
                        End If
                        dtCustomFields.Rows(i).Item("fld_label") = dtCustomFields.Rows(i).Item("fld_label").ToString.Replace("$", "")

                        If dtCustomFields.Rows(i).Item("fld_type").ToString = "TextBox" Then
                            Dim txt As TextBox
                            txt = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), TextBox)
                            If Not txt Is Nothing Then
                                dtCustomFields.Rows(i).Item("Value") = txt.Text
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "SelectBox" Then
                            Dim ddl As DropDownList
                            ddl = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString & "~" & IIf(IsDBNull(dtCustomFields.Rows(i).Item("numListId")), 0, dtCustomFields.Rows(i).Item("numListId").ToString).ToString), DropDownList)
                            If Not ddl Is Nothing Then
                                dtCustomFields.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "CheckBox" Then
                            Dim chk As CheckBox
                            chk = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), CheckBox)
                            If Not chk Is Nothing Then
                                If chk.Checked = True Then
                                    dtCustomFields.Rows(i).Item("Value") = "1"
                                Else : dtCustomFields.Rows(i).Item("Value") = "0"
                                End If
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "TextArea" Then
                            Dim txt As TextBox
                            txt = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), TextBox)
                            If Not txt Is Nothing Then
                                dtCustomFields.Rows(i).Item("Value") = txt.Text
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "DateField" Then
                            Dim BizCalendar As UserControl
                            BizCalendar = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), UserControl)
                            If Not BizCalendar Is Nothing Then
                                Dim strDueDate As String = ""
                                Dim _myControlType As Type = BizCalendar.GetType()
                                Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                                If Not _myUC_DueDate.GetValue(BizCalendar, Nothing) Is Nothing Then
                                    strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing).ToString
                                End If
                                If strDueDate <> "" Then
                                    dtCustomFields.Rows(i).Item("Value") = strDueDate
                                Else : dtCustomFields.Rows(i).Item("Value") = ""
                                End If
                            End If
                        End If
                    Next

                    Dim ds As New DataSet
                    Dim strdetails As String
                    dtCustomFields.TableName = "Table"
                    ds.Tables.Add(dtCustomFields.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    objCustomFields.strDetails = strdetails
                    objCustomFields.locId = LocationID
                    objCustomFields.DomainID = DomainID
                    objCustomFields.RecordId = RecordID
                    objCustomFields.SaveCustomFldsByRecId()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function SaveCusField(ByVal RecordID As Long, ByVal DependentID As Long, ByVal DomainID As Long, ByVal LocationID As Integer, ByRef uwTab As Table) As Boolean
            Try
                If Not dtCustomFields Is Nothing Then
                    Dim i As Integer
                    For i = 0 To dtCustomFields.Rows.Count - 1
                        If IsDBNull(dtCustomFields.Rows(i).Item("FldDTLID")) Then
                            dtCustomFields.Rows(i).Item("FldDTLID") = 0
                        End If
                        If dtCustomFields.Rows(i).Item("fld_type").ToString = "TextBox" Then
                            Dim txt As TextBox
                            txt = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), TextBox)
                            If Not txt Is Nothing Then
                                dtCustomFields.Rows(i).Item("Value") = txt.Text
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "SelectBox" Then
                            Dim ddl As DropDownList
                            ddl = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString & "~" & IIf(IsDBNull(dtCustomFields.Rows(i).Item("numListId")), 0, dtCustomFields.Rows(i).Item("numListId").ToString).ToString), DropDownList)
                            If Not ddl Is Nothing Then
                                dtCustomFields.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "CheckBox" Then
                            Dim chk As CheckBox
                            chk = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), CheckBox)
                            If Not chk Is Nothing Then
                                If chk.Checked = True Then
                                    dtCustomFields.Rows(i).Item("Value") = "1"
                                Else : dtCustomFields.Rows(i).Item("Value") = "0"
                                End If
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "TextArea" Then
                            Dim txt As TextBox
                            txt = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), TextBox)
                            If Not txt Is Nothing Then
                                dtCustomFields.Rows(i).Item("Value") = txt.Text
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "DateField" Then
                            Dim BizCalendar As UserControl
                            BizCalendar = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), UserControl)
                            If Not BizCalendar Is Nothing Then
                                Dim strDueDate As String = ""
                                Dim _myControlType As Type = BizCalendar.GetType()
                                Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                                If Not _myUC_DueDate.GetValue(BizCalendar, Nothing) Is Nothing Then
                                    strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing).ToString
                                End If
                                If strDueDate <> "" Then
                                    dtCustomFields.Rows(i).Item("Value") = strDueDate
                                Else : dtCustomFields.Rows(i).Item("Value") = ""
                                End If
                            End If
                        ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "CheckBoxList" Then
                            Dim chkbl As CheckBoxList
                            chkbl = CType(uwTab.FindControl(dtCustomFields.Rows(i).Item("fld_id").ToString & dtCustomFields.Rows(i).Item("fld_label").ToString), CheckBoxList)
                            If Not chkbl Is Nothing Then
                                Dim selectedValue As String = ""

                                For Each listItem As ListItem In chkbl.Items
                                    If listItem.Selected Then
                                        selectedValue = selectedValue & CCommon.ToString(IIf(String.IsNullOrEmpty(selectedValue), listItem.Value, "," & listItem.Value))
                                    End If
                                Next

                                dtCustomFields.Rows(i).Item("Value") = selectedValue
                            End If
                        End If
                    Next

                    Dim ds As New DataSet
                    Dim strdetails As String
                    dtCustomFields.TableName = "Table"
                    ds.Tables.Add(dtCustomFields.Copy)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    objCustomFields.strDetails = strdetails
                    objCustomFields.locId = LocationID
                    objCustomFields.DomainID = DomainID
                    objCustomFields.RecordId = RecordID
                    objCustomFields.SaveCustomFldsByRecId()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function SaveCusField(ByVal RecordID As Long, ByVal DependentID As Long, ByVal DomainID As Long, ByVal LocationID As Integer, ByRef uwTab As Infragistics.Web.UI.LayoutControls.WebTab, ByRef pnlTab As Panel) As Boolean
            Try
                If Not dtCustomFields Is Nothing Then
                    If dtCustomFields.Columns("Value") IsNot Nothing Then

                        Dim i As Integer
                        For i = 0 To dtCustomFields.Rows.Count - 1
                            If IsDBNull(dtCustomFields.Rows(i).Item("FldDTLID")) Then
                                dtCustomFields.Rows(i).Item("FldDTLID") = 0
                            End If
                            If dtCustomFields.Rows(i).Item("fld_type").ToString = "TextBox" Then
                                Dim txt As TextBox
                                txt = CType(pnlTab.FindControl(dtCustomFields.Rows(i).Item("numFieldId").ToString & dtCustomFields.Rows(i).Item("vcFieldName").ToString), TextBox)
                                If Not txt Is Nothing Then
                                    dtCustomFields.Rows(i).Item("Value") = txt.Text
                                End If
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "SelectBox" Then
                                Dim ddl As DropDownList
                                ddl = CType(pnlTab.FindControl(dtCustomFields.Rows(i).Item("numFieldId").ToString & dtCustomFields.Rows(i).Item("vcFieldName").ToString & "~" & IIf(IsDBNull(dtCustomFields.Rows(i).Item("numListId")), 0, dtCustomFields.Rows(i).Item("numListId").ToString).ToString), DropDownList)
                                If Not ddl Is Nothing Then
                                    dtCustomFields.Rows(i).Item("Value") = CStr(ddl.SelectedItem.Value)
                                End If
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "CheckBox" Then
                                Dim chk As CheckBox
                                chk = CType(pnlTab.FindControl(dtCustomFields.Rows(i).Item("numFieldId").ToString & dtCustomFields.Rows(i).Item("vcFieldName").ToString), CheckBox)
                                If Not chk Is Nothing Then
                                    If chk.Checked = True Then
                                        dtCustomFields.Rows(i).Item("Value") = "1"
                                    Else : dtCustomFields.Rows(i).Item("Value") = "0"
                                    End If
                                End If
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "TextArea" Then
                                Dim txt As TextBox
                                txt = CType(pnlTab.FindControl(dtCustomFields.Rows(i).Item("numFieldId").ToString & dtCustomFields.Rows(i).Item("vcFieldName").ToString), TextBox)
                                If Not txt Is Nothing Then
                                    dtCustomFields.Rows(i).Item("Value") = txt.Text
                                End If
                            ElseIf dtCustomFields.Rows(i).Item("fld_type").ToString = "DateField" Then
                                Dim BizCalendar As UserControl
                                BizCalendar = CType(pnlTab.FindControl(dtCustomFields.Rows(i).Item("numFieldId").ToString & dtCustomFields.Rows(i).Item("vcFieldName").ToString), UserControl)
                                If Not BizCalendar Is Nothing Then
                                    Dim strDueDate As String = ""
                                    Dim _myControlType As Type = BizCalendar.GetType()
                                    Dim _myUC_DueDate As PropertyInfo = _myControlType.GetProperty("SelectedDate")
                                    If Not _myUC_DueDate.GetValue(BizCalendar, Nothing) Is Nothing Then
                                        strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing).ToString
                                    End If
                                    If strDueDate <> "" Then
                                        dtCustomFields.Rows(i).Item("Value") = strDueDate
                                    Else : dtCustomFields.Rows(i).Item("Value") = ""
                                    End If
                                End If
                            End If
                        Next

                        Dim ds As New DataSet
                        Dim strdetails As String
                        dtCustomFields.TableName = "Table"

                        Dim dtl As DataTable = dtCustomFields

                        dtl.Columns("numFieldId").ColumnName = "fld_id"

                        ds.Tables.Add(dtl.Copy)
                        strdetails = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))

                        objCustomFields.strDetails = strdetails
                        objCustomFields.locId = LocationID
                        objCustomFields.DomainID = DomainID
                        objCustomFields.RecordId = RecordID
                        objCustomFields.SaveCustomFldsByRecId()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function CreateTexBox(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal strText As String) As Boolean

            Dim txt As New TextBox
            txt.CssClass = "signup"
            txt.ID = strId
            If strText <> "0" Then
                txt.Text = strText
            Else
                txt.Text = ""
            End If
            txt.Width = Unit.Pixel(180)
            tblCell.Controls.Add(txt)
            tblRow.Cells.Add(tblCell)
            Return True
        End Function



        Public Function CreateDropdown(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal intSelValue As Integer, ByVal intListId As Integer) As Boolean

            Dim ddl As New DropDownList
            ddl.CssClass = "signup"
            Dim dtTable As DataTable
            Dim ObjCusfld As New CustomFields
            ObjCusfld.ListId = CInt(intListId)
            dtTable = ObjCusfld.GetMasterListByListId
            ddl.ID = strId
            ddl.DataSource = dtTable
            ddl.DataTextField = "vcData"
            ddl.DataValueField = "numListItemID"
            ddl.DataBind()
            ddl.Items.Insert(0, "--Select One--")
            ddl.Items.FindByText("--Select One--").Value = "0"
            ddl.Width = Unit.Pixel(180)

            tblCell.Controls.Add(ddl)
            tblRow.Cells.Add(tblCell)
            If intSelValue > 0 Then
                If Not IsNothing(ddl.Items.FindByValue(CStr(intSelValue))) Then
                    ddl.Items.FindByValue(CStr(intSelValue)).Selected = True
                End If
            End If
            Return True
        End Function

        Public Function CreateTextArea(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal strText As String) As Boolean


            Dim txt As New TextBox
            txt.CssClass = "signup"
            txt.Width = Unit.Pixel(200)
            txt.Height = Unit.Pixel(40)
            txt.TextMode = TextBoxMode.MultiLine
            txt.ID = strId
            If strText <> "0" Then
                txt.Text = strText
            Else
                txt.Text = ""
            End If
            tblCell.Controls.Add(txt)
            tblRow.Cells.Add(tblCell)
            Return True
        End Function




        Public Function CreateChkBox(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal intSel As Integer) As Boolean

            Dim chk As New CheckBox
            chk.ID = strId
            If intSel = 1 Then
                chk.Checked = True
            Else
                chk.Checked = False
            End If
            tblCell.Controls.Add(chk)
            tblRow.Cells.Add(tblCell)
            Return True
        End Function


        Public Function CreateLink(ByVal tblRow As HtmlTableRow, ByVal tblCell As HtmlTableCell, ByVal strId As String, ByVal URL As String, ByVal RecID As Long, ByVal Label As String) As Boolean
            Dim hpl As New HyperLink
            hpl.ID = "hpl" & strId
            hpl.Text = Label
            URL = URL.Replace("RecordID", CStr(RecID))
            hpl.NavigateUrl = URL
            hpl.Target = "_blank"
            hpl.CssClass = "hyperlink"
            tblCell.Controls.Add(hpl)
            tblRow.Cells.Add(tblCell)
            Return True
        End Function


        Function GetPrimaryListItemID(ByVal RelListID As Long, ByRef dtTableInfo As DataTable, ByVal obj As Object) As Long
            Dim dr As DataRow
            For i As Int32 = 0 To dtTableInfo.Rows.Count - 1
                dr = dtTableInfo.Rows(i)
                If Not IsDBNull(dr("numListID")) Then
                    If CLng(dr("numListID")) = RelListID Then
                        If CBool(dr("bitCustomField")) = True Then
                            'For Dropdown relationship 
                            Return CCommon.ToLong(dr("numListItemID")) 'added by chintan, when adding dropdown relationship it used to throw error
                        Else
                            Return CLng(obj.GetType.GetProperty(dr("vcPropertyName").ToString).GetValue(obj, Nothing))
                        End If
                    End If
                End If
            Next
            Return 0
        End Function



        'Sub PopulateDropdowns(ByVal ddl As DropDownList, ByVal dtTableInfo As DataTable, ByVal objCommon As CCommon, ByVal uwOppTab As Infragistics.Web.UI.LayoutControls.WebTab, ByVal DomainID As Long)
        '    If CLng(ddl.SelectedValue) > 0 Then
        '        Dim ddl1 As DropDownList
        '        Dim dvItems As DataView
        '        dvItems = dtTableInfo.DefaultView
        '        dvItems.RowFilter = "ListRelID=" & ddl.ID.Split(CChar("~"))(1)
        '        For iViewCount As Integer = 0 To dvItems.Count - 1
        '            If Not uwOppTab.FindControl(dvItems(iViewCount).Item("numFieldId").ToString & dvItems(iViewCount).Item("vcFieldName").ToString & "~" & IIf(IsDBNull(dvItems(iViewCount).Item("numListId").ToString), 0, dvItems(iViewCount).Item("numListId").ToString).ToString) Is Nothing Then
        '                ddl1 = CType(uwOppTab.FindControl(dvItems(iViewCount).Item("numFieldId").ToString & dvItems(iViewCount).Item("vcFieldName").ToString & "~" & IIf(IsDBNull(dvItems(iViewCount).Item("numListId").ToString), 0, dvItems(iViewCount).Item("numListId").ToString).ToString), DropDownList)
        '                Dim dtData As DataTable
        '                objCommon.Mode = 3
        '                objCommon.DomainID = DomainID
        '                objCommon.PrimaryListItemID = CLng(ddl.SelectedValue)
        '                objCommon.SecondaryListID = CLng(dvItems(iViewCount).Item("numListId"))
        '                dtData = objCommon.GetFieldRelationships.Tables(0)

        '                ddl1.DataSource = dtData
        '                ddl1.DataValueField = dtData.Columns(0).ColumnName
        '                ddl1.DataTextField = dtData.Columns(1).ColumnName
        '                ddl1.DataBind()
        '                ddl1.Items.Insert(0, "-- Select One --")
        '                ddl1.Items.FindByText("-- Select One --").Value = "0"
        '            End If
        '        Next
        '    End If
        'End Sub

        'Sub PopulateDropdowns(ByVal ddl As DropDownList, ByVal dtTableInfo As DataTable, ByVal objCommon As CCommon, ByVal uwOppTab As Table, ByVal DomainID As Long)
        '    If CLng(ddl.SelectedValue) > 0 Then
        '        Dim ddl1 As DropDownList
        '        Dim dvItems As DataView
        '        dvItems = dtTableInfo.DefaultView
        '        dvItems.RowFilter = "ListRelID=" & ddl.ID.Split(CChar("~"))(1)
        '        For iViewCount As Integer = 0 To dvItems.Count - 1
        '            If Not uwOppTab.FindControl(dvItems(iViewCount).Item("numFieldId").ToString & dvItems(iViewCount).Item("vcFieldName").ToString & "~" & IIf(IsDBNull(dvItems(iViewCount).Item("numListId").ToString), 0, dvItems(iViewCount).Item("numListId").ToString).ToString) Is Nothing Then
        '                ddl1 = CType(uwOppTab.FindControl(dvItems(iViewCount).Item("numFieldId").ToString & dvItems(iViewCount).Item("vcFieldName").ToString & "~" & IIf(IsDBNull(dvItems(iViewCount).Item("numListId").ToString), 0, dvItems(iViewCount).Item("numListId").ToString).ToString), DropDownList)
        '                Dim dtData As DataTable
        '                objCommon.Mode = 3
        '                objCommon.DomainID = DomainID
        '                objCommon.PrimaryListItemID = CLng(ddl.SelectedValue)
        '                objCommon.SecondaryListID = CLng(dvItems(iViewCount).Item("numListId"))
        '                dtData = objCommon.GetFieldRelationships.Tables(0)

        '                ddl1.DataSource = dtData
        '                ddl1.DataValueField = dtData.Columns(0).ColumnName
        '                ddl1.DataTextField = dtData.Columns(1).ColumnName
        '                ddl1.DataBind()
        '                ddl1.Items.Insert(0, "-- Select One --")
        '                ddl1.Items.FindByText("-- Select One --").Value = "0"
        '            End If
        '        Next
        '    End If
        'End Sub

        Sub PopulateDropdowns(ByVal ddl As DropDownList, ByVal dtTableInfo As DataTable, ByVal objCommon As CCommon, ByVal uwOppTab As Control, ByVal DomainID As Long)
            If CLng(ddl.SelectedValue) > 0 Then
                Dim ddl1 As DropDownList
                Dim dvItems As DataView
                dvItems = dtTableInfo.DefaultView
                dvItems.RowFilter = "ListRelID=" & ddl.ID.Split(CChar("~"))(1)
                For iViewCount As Integer = 0 To dvItems.Count - 1
                    If Not uwOppTab.FindControl(dvItems(iViewCount).Item("numFieldId").ToString & dvItems(iViewCount).Item("vcFieldName").ToString & "~" & IIf(IsDBNull(dvItems(iViewCount).Item("numListId").ToString), 0, dvItems(iViewCount).Item("numListId").ToString).ToString) Is Nothing Then
                        ddl1 = CType(uwOppTab.FindControl(dvItems(iViewCount).Item("numFieldId").ToString & dvItems(iViewCount).Item("vcFieldName").ToString & "~" & IIf(IsDBNull(dvItems(iViewCount).Item("numListId").ToString), 0, dvItems(iViewCount).Item("numListId").ToString).ToString), DropDownList)
                        Dim dtData As DataTable
                        objCommon.Mode = 3
                        objCommon.DomainID = DomainID
                        objCommon.PrimaryListItemID = CLng(ddl.SelectedValue)
                        objCommon.SecondaryListID = CLng(dvItems(iViewCount).Item("numListId"))
                        dtData = objCommon.GetFieldRelationships.Tables(0)

                        ddl1.DataSource = dtData
                        ddl1.DataValueField = dtData.Columns(0).ColumnName
                        ddl1.DataTextField = dtData.Columns(1).ColumnName
                        ddl1.DataBind()
                        ddl1.Items.Insert(0, "-- Select One --")
                        ddl1.Items.FindByText("-- Select One --").Value = "0"
                    End If
                Next
            End If
        End Sub

        'Added to support RadComboBox
        Sub PopulateDropdowns(ByVal ddl As RadComboBox, ByVal dtTableInfo As DataTable, ByVal objCommon As CCommon, ByVal uwOppTab As Control, ByVal DomainID As Long)
            If CLng(ddl.SelectedValue) > 0 Then
                Dim ddl1 As RadComboBox
                Dim dvItems As DataView
                dvItems = dtTableInfo.DefaultView
                dvItems.RowFilter = "ListRelID=" & ddl.ID.Split(CChar("~"))(1)
                For iViewCount As Integer = 0 To dvItems.Count - 1
                    If Not uwOppTab.FindControl(dvItems(iViewCount).Item("numFieldId").ToString & dvItems(iViewCount).Item("vcFieldName").ToString & "~" & IIf(IsDBNull(dvItems(iViewCount).Item("numListId").ToString), 0, dvItems(iViewCount).Item("numListId").ToString).ToString) Is Nothing Then
                        ddl1 = CType(uwOppTab.FindControl(dvItems(iViewCount).Item("numFieldId").ToString & dvItems(iViewCount).Item("vcFieldName").ToString & "~" & IIf(IsDBNull(dvItems(iViewCount).Item("numListId").ToString), 0, dvItems(iViewCount).Item("numListId").ToString).ToString), RadComboBox)
                        Dim dtData As DataTable
                        objCommon.Mode = 3
                        objCommon.DomainID = DomainID
                        objCommon.PrimaryListItemID = CLng(ddl.SelectedValue)
                        objCommon.SecondaryListID = CLng(dvItems(iViewCount).Item("numListId"))
                        dtData = objCommon.GetFieldRelationships.Tables(0)

                        ddl1.DataSource = dtData
                        ddl1.DataValueField = dtData.Columns(0).ColumnName
                        ddl1.DataTextField = dtData.Columns(1).ColumnName
                        ddl1.DataBind()
                        ddl1.Items.Insert(0, New RadComboBoxItem("-- Select One --", "0"))
                    End If
                Next
            End If
        End Sub

        Function GenerateValidationScript(ByVal dtCustomFields As DataTable) As String
            Try
                Dim sb As New System.Text.StringBuilder
                Dim ClientID, fieldName, FieldMessage As String

                sb.AppendLine("function validateCustomFields() {")

                For Each dr As DataRow In dtCustomFields.Rows
                    fieldName = dr("vcFieldName").ToString.Replace("'", "\'")

                    If CCommon.ToString(dr("fld_type")) = "SelectBox" Then
                        ClientID = dr("numFieldId").ToString & fieldName & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString
                    Else
                        ClientID = dr("numFieldId").ToString & fieldName
                    End If

                    'MStart Pinkal Patel Date:11-Nov-2011
                    'FieldMessage = IIf(CCommon.ToBool(dr("bitFieldMessage")), dr("vcFieldMessage").ToString.Replace("'", "\'"), "").ToString
                    FieldMessage = IIf(CCommon.ToBool(dr("bitFieldMessage")), HttpUtility.JavaScriptStringEncode(dr("vcFieldMessage").ToString.Replace("{$FieldName}", CCommon.ToString(fieldName))), "").ToString
                    'MEnd Pinkal Patel Date:11-Nov-2011
                    If (CCommon.ToString(dr("fld_type")) = "TextBox" Or CCommon.ToString(dr("fld_type")) = "TextArea") Then
                        'Required Field 
                        If CCommon.ToInteger(dr("bitIsRequired")) = 1 Then
                            sb.AppendLine("if(RequiredField('" & ClientID & "','" & fieldName & "','" & FieldMessage & "')==false) {return false;} ")
                        End If
                        'Email Address
                        If CCommon.ToBool(dr("bitIsEmail")) = True Then
                            sb.AppendLine("if(CheckMail('" & ClientID & "','" & fieldName & "','" & FieldMessage & "')==false) {return false;} ")
                        End If
                        'Numeric
                        If CCommon.ToBool(dr("bitIsNumeric")) = True Then
                            sb.AppendLine("if(IsNumeric('" & ClientID & "','" & fieldName & "','" & FieldMessage & "')==false) {return false;} ")
                        End If
                        'AlphaNumeric
                        If CCommon.ToBool(dr("bitIsAlphaNumeric")) = True Then
                            sb.AppendLine("if(IsAlphaNumeric('" & ClientID & "','" & fieldName & "','" & FieldMessage & "')==false) {return false;} ")
                        End If
                        'Is Length Validation
                        If CCommon.ToBool(dr("bitIsLengthValidation")) = True Then
                            sb.AppendLine("if(RangeValidator('" & ClientID & "','" & fieldName & "','" & CCommon.ToInteger(dr("intMinLength")) & "','" & CCommon.ToInteger(dr("intMaxLength")) & "','" & FieldMessage & "')==false) {return false;} ")
                        End If
                        ' Field Message
                        'If CCommon.ToBool(dr("bitFieldMessage")) = True Then
                        '    sb.AppendLine("if(IsNumeric('" & ClientID & "','" & fieldName & "')==false) {return false;} ")
                        'End If
                    ElseIf CCommon.ToString(dr("fld_type")) = "SelectBox" Then
                        'Required Field 
                        If CCommon.ToInteger(dr("bitIsRequired")) = 1 Then
                            sb.AppendLine("if(RequiredFieldDropDown('" & ClientID & "','" & fieldName & "','" & FieldMessage & "')==false) {return false;} ")
                        End If
                    ElseIf CCommon.ToString(dr("fld_type")) = "DateField" Then
                        'Required Field 
                        If CCommon.ToInteger(dr("bitIsRequired")) = 1 Then
                            sb.AppendLine("if(RequiredField('" & ClientID & "_txtDate" & "','" & fieldName & "','" & FieldMessage & "')==false) {return false;} ")
                        End If
                    End If
                Next
                sb.AppendLine("return true;}")
                Return sb.ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GenerateInlineEditValidationScript(ByVal dtCustomFields As DataTable) As String
            Try
                Dim sb As New System.Text.StringBuilder
                Dim ClientID, FieldMessage As String
                Dim iRowIndex As Integer = 0

                sb.Append(vbCrLf & "var arrFieldConfig = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript

                For Each dr As DataRow In dtCustomFields.Rows
                    ClientID = dr("numFieldId").ToString
                    FieldMessage = IIf(CCommon.ToBool(dr("bitFieldMessage")), HttpUtility.JavaScriptStringEncode(dr("vcFieldMessage").ToString.Replace("{$FieldName}", CCommon.ToString(dr("vcFieldName")))), dr("vcFieldName").ToString.Replace("'", "\'")).ToString
                    sb.Append("arrFieldConfig[" & iRowIndex & "] = new InlineEditValidation('" & dr("numFieldId").ToString & "','" & dr("bitCustomField").ToString & "','" & CCommon.ToBool(dr("bitIsRequired")) & "','" & CCommon.ToBool(dr("bitIsNumeric")) & "','" & CCommon.ToBool(dr("bitIsAlphaNumeric")) & "','" & CCommon.ToBool(dr("bitIsEmail")) & "','" & CCommon.ToBool(dr("bitIsLengthValidation")) & "','" & CCommon.ToInteger(dr("intMaxLength")) & "','" & CCommon.ToInteger(dr("intMinLength")) & "','" & FieldMessage & "');" & vbCrLf)
                    iRowIndex += 1
                Next

                Return sb.ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GenerateGoogleMapScript(ByVal dtCustomFields As DataTable) As String
            Try
                Dim sb As New System.Text.StringBuilder
                Dim iRowIndex As Integer = 0

                sb.Append(vbCrLf & "var arrGoogleMapAddress = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript

                For Each dr As DataRow In dtCustomFields.Rows
                    sb.Append("arrGoogleMapAddress[" & iRowIndex & "] = new GoogleMapAddress('" & dr("Latitude").ToString & "','" & dr("Longitude").ToString & "','" & HttpUtility.JavaScriptStringEncode(dr("vcCompanyName").ToString()) & " Contact:" & HttpUtility.JavaScriptStringEncode(dr("vcLastName").ToString()) & " " & HttpUtility.JavaScriptStringEncode(dr("vcFirstName").ToString()) & "','" & HttpUtility.JavaScriptStringEncode(dr("vcFullAddress").ToString()) & "','" & dr("Distance").ToString() & "');" & vbCrLf)
                    iRowIndex += 1
                Next

                Return sb.ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GenerateRuleModuleFieldListScript(ByVal dtCustomFields As DataTable) As String
            Try
                Dim sb As New System.Text.StringBuilder
                Dim iRowIndex As Integer = 0

                sb.Append(vbCrLf & "var arrFieldConfig = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript

                For Each dr As DataRow In dtCustomFields.Rows
                    sb.Append("arrFieldConfig[" & iRowIndex & "] = new RuleModuleFieldList('" & dr("numFormFieldId").ToString & "','" & dr("vcDbColumnName").ToString & "','" & dr("vcFormFieldName").ToString & "','" & dr("vcAssociatedControlType").ToString() & "','" & dr("vcListItemType").ToString() & "','" & dr("numListID").ToString() & "','" & dr("vcLookBackTableName").ToString() & "' ,'" & dr("vcFieldDataType").ToString() & "');" & vbCrLf)
                    iRowIndex += 1
                Next

                Return sb.ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by sandeep to support new Jquery Validation
        'Test validation is working peoperly or not after using this method
        Function GenerateJqueryValidationScript(ByVal dtCustomFields As DataTable) As String
            Try
                Dim sb As New System.Text.StringBuilder
                Dim isFirstelement As Boolean = True
                Dim ClientID, fieldName, FieldMessage As String

                sb.Append("function validateCustomFields() {")
                sb.Append("$('#form1').validate({")
                sb.Append("rules: {")

                For Each dr As DataRow In dtCustomFields.Rows
                    fieldName = dr("vcFieldName").ToString.Replace("'", "\'")
                    ClientID = dr("numFieldId").ToString & fieldName

                    If (CCommon.ToInteger(dr("bitIsRequired")) = 1 Or
                       CCommon.ToBool(dr("bitIsEmail")) = True Or
                        CCommon.ToBool(dr("bitIsNumeric")) = True Or
                        CCommon.ToBool(dr("bitIsAlphaNumeric")) = True Or
                        CCommon.ToBool(dr("bitIsLengthValidation")) = True) Then

                        If isFirstelement Then
                            isFirstelement = False
                        Else
                            sb.Append(",")
                        End If
                        If CStr(dr("fld_type")) = "SelectBox" Then
                            sb.Append("'ctl00$Content$" + dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString + "': {")
                        Else
                            sb.Append("'ctl00$Content$" + ClientID + "': {")
                        End If
                        If CCommon.ToInteger(dr("bitIsRequired")) = 1 Then
                            sb.Append("required: true,")
                        End If
                        'Email Address
                        If CCommon.ToBool(dr("bitIsEmail")) = True Then
                            sb.Append("email: true,")
                        End If
                        'Numeric
                        If CCommon.ToBool(dr("bitIsNumeric")) = True Then
                            sb.Append("number: true,")
                        End If
                        'AlphaNumeric
                        If CCommon.ToBool(dr("bitIsAlphaNumeric")) = True Then
                            sb.Append("alphanumeric: true,")
                        End If
                        'Is Length Validation
                        If CCommon.ToBool(dr("bitIsLengthValidation")) = True Then
                            sb.Append("rangelength: [" & dr("intMinLength").ToString & "," & dr("intMaxLength").ToString & "]")
                        End If

                        If (sb.ToString().Last = ",") Then
                            sb.Remove(sb.Length - 1, 1)
                        End If

                        sb.Append("}")

                    End If
                Next
                sb.Append("},")
                sb.Append("messages: {")
                'Error Message block
                isFirstelement = True
                For Each dr As DataRow In dtCustomFields.Rows
                    fieldName = dr("vcFieldName").ToString.Replace("'", "\'")
                    ClientID = dr("numFieldId").ToString & fieldName

                    If (CCommon.ToInteger(dr("bitIsRequired")) = 1 Or
                       CCommon.ToBool(dr("bitIsEmail")) = True Or
                        CCommon.ToBool(dr("bitIsNumeric")) = True Or
                        CCommon.ToBool(dr("bitIsAlphaNumeric")) = True Or
                        CCommon.ToBool(dr("bitIsLengthValidation")) = True) Then

                        If isFirstelement Then
                            isFirstelement = False
                        Else
                            sb.Append(",")
                        End If
                        If CStr(dr("fld_type")) = "SelectBox" Then
                            sb.Append("'ctl00$Content$" + dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString + "': {")
                        Else
                            sb.Append("'ctl00$Content$" + ClientID + "': {")
                        End If
                        If CCommon.ToInteger(dr("bitIsRequired")) = 1 Then
                            sb.Append("required: '" & fieldName & " is required'")
                        End If
                        'Email Address
                        If CCommon.ToBool(dr("bitIsEmail")) = True Then
                            sb.Append(",email: 'Enter valid " & fieldName)
                        End If
                        'Numeric
                        If CCommon.ToBool(dr("bitIsNumeric")) = True Then
                            sb.Append(",number: '" & fieldName & " allows only numeric value'")
                        End If
                        'AlphaNumeric
                        If CCommon.ToBool(dr("bitIsAlphaNumeric")) = True Then
                            sb.Append(",alphanumeric: '" & fieldName & " allows only alphanumeric value'")
                        End If
                        'Is Length Validation
                        If CCommon.ToBool(dr("bitIsLengthValidation")) = True Then
                            sb.Append("rangelength: '" & fieldName & " must be between " & dr("intMinLength").ToString & " to " & dr("intMaxLength").ToString & " characters long'")
                        End If

                        If (sb.ToString().Last = ",") Then
                            sb.Remove(sb.Length - 1, 1)
                        End If

                        sb.Append("}")

                    End If
                Next
                sb.Append("},")
                sb.Append("highlight: function (element) {")
                sb.Append("$(element).removeClass('success').addClass('error');")
                sb.Append("},")
                sb.Append("success: function (element) {")
                sb.Append("element.removeClass('error').addClass('success');")
                sb.Append("}")
                sb.Append("});")
                sb.Append("}")
                Return sb.ToString
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DisplayDynamicFldsTickler(ByVal RecordID As Long, ByVal DependentID As Long, ByVal DomainID As Long, ByVal LocationID As Integer, ByRef pnl As Panel, ByVal FormID As Long) As Boolean
            Try
                Dim strDate As String
                Dim bizCalendar As UserControl
                Dim _myUC_DueDate As PropertyInfo

                Dim ds As DataSet
                Dim dtTabs As DataTable

                objCustomFields.locId = LocationID
                objCustomFields.RelId = DependentID
                objCustomFields.DomainID = DomainID
                objCustomFields.RecordId = RecordID
                objCustomFields.UserCntID = CLng(Session("UserContactId"))
                objCustomFields.FormId = FormID

                ds = objCustomFields.GetCustFlds
                dtCustomFields = ds.Tables(0)
                dtTabs = ds.Tables(1)

                Dim aspTable As HtmlTable
                Dim objRow As New HtmlTableRow
                Dim objCell As HtmlTableCell

                'dtCustomFields = New DataTable
                Dim dtCustomTabField As DataTable

                For Each dr As DataRow In dtTabs.Rows

                    aspTable = New HtmlTable
                    aspTable.ID = "tblCustomFields"
                    aspTable.Attributes.Add("class", "tblTickler")
                    aspTable.Width = "100%"

                    objCustomFields.Mode = 1
                    objCustomFields.TabId = CLng(dr("TabId"))
                    ds = objCustomFields.GetCustomTabGetFields

                    dtCustomTabField = ds.Tables(0)
                    'dtCustomFields.Merge(dtCustomTabField)

                    If dtCustomTabField.Rows.Count > 0 Then
                        For Each drFields As DataRow In dtCustomTabField.Rows
                            If CType(drFields("TabId"), Integer) <> 0 Then
                                objCell = New HtmlTableCell
                                objCell.Align = "right"
                                objCell.Attributes.Add("class", "normal1")
                                If drFields("fld_type").ToString <> "Link" Then
                                    objCell.InnerText = drFields("fld_label").ToString
                                End If

                                If dtCustomFields.Columns.Contains("vcToolTip") Then
                                    If Not IsDBNull(drFields("vcToolTip")) And drFields("vcToolTip").ToString.Trim.Length > 0 Then
                                        Dim lblToolTip As Label
                                        lblToolTip = New Label
                                        lblToolTip.Text = "[?]"
                                        lblToolTip.CssClass = "tip"
                                        lblToolTip.ToolTip = drFields("vcToolTip").ToString.Trim
                                        objCell.Controls.Add(lblToolTip)
                                    End If
                                End If

                                objRow.Cells.Add(objCell)
                                If drFields("fld_type").ToString = "TextBox" Then
                                    objCell = New HtmlTableCell
                                    CreateTexBox(objRow, objCell, drFields("fld_id").ToString & drFields("fld_label").ToString, drFields("Value").ToString)
                                ElseIf drFields("fld_type").ToString = "SelectBox" Then
                                    objCell = New HtmlTableCell
                                    CreateDropdown(objRow, objCell, drFields("fld_id").ToString & drFields("fld_label").ToString & "~" & IIf(IsDBNull(drFields("numListId")), 0, drFields("numListId").ToString).ToString, CInt(drFields("Value")), CInt(drFields("numlistid")))
                                ElseIf drFields("fld_type").ToString = "CheckBox" Then
                                    objCell = New HtmlTableCell
                                    CreateChkBox(objRow, objCell, drFields("fld_id").ToString & drFields("fld_label").ToString, CInt(drFields("Value")))
                                ElseIf drFields("fld_type").ToString = "TextArea" Then
                                    objCell = New HtmlTableCell
                                    CreateTextArea(objRow, objCell, drFields("fld_id").ToString & drFields("fld_label").ToString, drFields("Value").ToString)
                                ElseIf drFields("fld_type").ToString = "DateField" Then
                                    objCell = New HtmlTableCell
                                    bizCalendar = CType(LoadControl("include/calandar.ascx"), UserControl)
                                    bizCalendar.ID = drFields("fld_id").ToString & drFields("fld_label").ToString
                                    bizCalendar.EnableViewState = False
                                    bizCalendar.ClientIDMode = UI.ClientIDMode.AutoID
                                    If Not IsDBNull(drFields("Value")) Then
                                        Dim _myControlType As Type = bizCalendar.GetType()
                                        _myUC_DueDate = _myControlType.GetProperty("SelectedDate")
                                        strDate = drFields("Value").ToString
                                        If strDate = "0" Then strDate = ""
                                        If strDate = "01/01/0001 00:00:00" Or strDate = "01/01/1753 00:00:00" Then strDate = ""
                                        If strDate <> "" Then
                                            _myUC_DueDate.SetValue(bizCalendar, strDate, Nothing)
                                        End If
                                    End If
                                    objCell.Controls.Add(bizCalendar)
                                    objRow.Cells.Add(objCell)
                                ElseIf drFields("fld_type").ToString = "Link" Then
                                    objCell = New HtmlTableCell
                                    CreateLink(objRow, objCell, drFields("fld_id").ToString & drFields("fld_label").ToString, drFields("vcURL").ToString, RecordID, drFields("fld_label").ToString)
                                ElseIf drFields("fld_type").ToString = "Frame" Then
                                    objCell = New HtmlTableCell
                                    Dim strFrame As String
                                    Dim URL As String
                                    URL = drFields("vcURL").ToString
                                    URL = URL.Replace("RecordID", RecordID.ToString)
                                    strFrame = "<iframe src ='" & URL & "' width='100%' frameborder='0' height= '800px'></iframe>"
                                    objCell.Controls.Add(New LiteralControl(strFrame))
                                    objRow.Cells.Add(objCell)
                                End If
                            End If
                        Next

                        aspTable.Rows.Add(objRow)
                    End If
                Next

                pnl.Controls.Add(aspTable)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class

End Namespace
