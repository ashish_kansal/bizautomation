﻿Imports System
Imports System.IO
Imports System.Web
Imports System.Text
Imports System.Security.Cryptography
Namespace BACRM.BusinessLogic.Common
    Public Class QueryEncryption
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private Const PARAMETER_NAME As String = "enc="
        Private Const ENCRYPTION_KEY As String = "tOtAlbIzV1"
        Private Shared ReadOnly SALT As Byte() = Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString)

        Public Shared Function EncryptQueryString(ByVal inputText As String) As String
            Dim rijndaelCipher As RijndaelManaged = New RijndaelManaged
            Dim plainText As Byte() = Encoding.Unicode.GetBytes(inputText)
            Dim SecretKey As PasswordDeriveBytes = New PasswordDeriveBytes(ENCRYPTION_KEY, SALT)
            ' Using
            Dim encryptor As ICryptoTransform = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16))
            Try
                ' Using
                Dim memoryStream As MemoryStream = New MemoryStream
                Try
                    ' Using
                    Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
                    Try
                        cryptoStream.Write(plainText, 0, plainText.Length)
                        cryptoStream.FlushFinalBlock()
                        Return "?" + PARAMETER_NAME + Convert.ToBase64String(memoryStream.ToArray)
                    Finally
                        CType(cryptoStream, IDisposable).Dispose()
                    End Try
                Finally
                    CType(memoryStream, IDisposable).Dispose()
                End Try
            Finally
                CType(encryptor, IDisposable).Dispose()
            End Try
        End Function
    End Class
End Namespace