﻿Option Explicit On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Common

    Public Class eChannelHub
        Inherits BACRM.BusinessLogic.CBusinessBase

        Public Function GeteChannelHubHtml() As String
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToString(SqlDAL.ExecuteScalar(connString, "USP_eChannelHub_GetAsHtml", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

End Namespace


