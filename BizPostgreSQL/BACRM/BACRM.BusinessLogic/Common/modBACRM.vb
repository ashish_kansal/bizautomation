﻿'Modified by Anoop Jayaraj

Option Explicit On
Option Strict On
Imports BACRM.BusinessLogic.Admin
Imports System.Web.HttpContext
Imports System.Web.UI.WebControls
Namespace BACRM.BusinessLogic.Common
    Public Module modBacrm

      
  
        Sub FillState(ByVal ddl As DropDownList, ByVal lngCountry As Object, ByVal lngDomainID As Long)
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = lngDomainID
            objUserAccess.Country = CCommon.ToLong(lngCountry)
            dtTable = objUserAccess.SelState
            ddl.DataSource = dtTable
            ddl.DataTextField = "vcState"
            ddl.DataValueField = "numStateID"

            ddl.Items.Clear()
            ddl.SelectedValue = Nothing
            ddl.SelectedIndex = -1
            ddl.ClearSelection()

            ddl.DataBind()
            ddl.Items.Insert(0, "--Select One--")
            ddl.Items.FindByText("--Select One--").Value = "0"
        End Sub


        Sub FillAllState(ByVal ddl As DropDownList, ByVal lngDomainID As Long)
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = lngDomainID
            dtTable = objUserAccess.SelAllStates
            ddl.DataSource = dtTable
            ddl.DataTextField = "vcState"
            ddl.DataValueField = "numStateID"
            ddl.DataBind()
        End Sub


        Sub FillState(ByVal lst As ListBox, ByVal lngCountry As Object, ByVal lngDomainID As Long)
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = lngDomainID
            objUserAccess.Country = CCommon.ToLong(lngCountry)
            dtTable = objUserAccess.SelState
            lst.DataSource = dtTable
            lst.DataTextField = "vcState"
            lst.DataValueField = "numStateID"
            lst.DataBind()
        End Sub

        Sub FillState(ByVal lst As Telerik.Web.UI.RadComboBox, ByVal lngCountry As Object, ByVal lngDomainID As Long)
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = lngDomainID
            objUserAccess.Country = CCommon.ToLong(lngCountry)
            dtTable = objUserAccess.SelState
            lst.DataSource = dtTable
            lst.DataTextField = "vcState"
            lst.DataValueField = "numStateID"
            lst.DataBind()
        End Sub

        Public Function FormattedDateFromDate(ByVal dtDate As Date, ByVal strFormat As String) As String
            Dim str4Yr As String = Year(dtDate).ToString
            'Set the Year in 2 Digits to a Variable
            Dim str2Yr As String = Mid(Year(dtDate).ToString, 3, 2)
            'Set the Month in 2 Digits to a Variable
            Dim strIntMonth As String = Month(dtDate).ToString

            If CInt(strIntMonth) <= 9 Then
                strIntMonth = "0" & strIntMonth
            End If
            'Set the Date in 2 Digits to a Variable
            Dim strDate As String = Day(dtDate).ToString

            If CInt(strDate) <= 9 Then
                strDate = "0" & strDate
            End If
            'Set the Abbrivated Month Name to a Variable
            Dim str3Month As String = MonthName(CInt(strIntMonth), True)
            'Set the Full Month Name to a Variable
            Dim strFullMonth As String = MonthName(CInt(strIntMonth), False)
            'Set the Nos of Hrs to a Variable
            Dim strHrs As String = Hour(dtDate).ToString
            'Set the Nos of Mins to a Variable
            Dim strMins As String = Minute(dtDate).ToString

            'As the Date Format will be one of the above mentioned formats,
            'we need to replace the required string to get the formatted date.
            strFormat = Replace(strFormat, "DD", strDate)
            strFormat = Replace(strFormat, "YYYY", str4Yr)
            strFormat = Replace(strFormat, "YY", str2Yr)
            strFormat = Replace(strFormat, "MM", strIntMonth)
            strFormat = Replace(strFormat, "MONTH", strFullMonth)
            strFormat = Replace(strFormat, "MON", str3Month)

            Return strFormat
        End Function


        Public Function FormattedDateTimeFromDate(ByVal dtDate As Date, ByVal strFormat As String) As String
            Dim str4Yr As String = Year(dtDate).ToString
            'Set the Year in 2 Digits to a Variable
            Dim str2Yr As String = Mid(Year(dtDate).ToString, 3, 2)
            'Set the Month in 2 Digits to a Variable
            Dim strIntMonth As String = Month(dtDate).ToString
            If CInt(strIntMonth) <= 9 Then
                strIntMonth = "0" & strIntMonth
            End If
            'Set the Date in 2 Digits to a Variable
            Dim strDate As String = Day(dtDate).ToString

            If CInt(strDate) <= 9 Then
                strDate = "0" & strDate
            End If
            Dim str3Month As String = MonthName(CInt(strIntMonth), True)
            'Set the Full Month Name to a Variable
            Dim strFullMonth As String = MonthName(CInt(strIntMonth), False)
            'Set the Nos of Hrs to a Variable
            Dim strHrs As String = Hour(dtDate).ToString
            'Set the Nos of Mins to a Variable
            Dim strMins As String = Minute(dtDate).ToString
            If CInt(strMins) <= 9 Then
                strMins = "0" & strMins
            End If

            strFormat = Replace(strFormat, "DD", strDate)
            strFormat = Replace(strFormat, "YYYY", str4Yr)
            strFormat = Replace(strFormat, "YY", str2Yr)
            strFormat = Replace(strFormat, "MM", strIntMonth)
            strFormat = Replace(strFormat, "MONTH", strFullMonth)
            strFormat = Replace(strFormat, "MON", str3Month)


            Dim offset As Integer = -(CInt(Current.Session("ClientMachineUTCTimeOffset")))
            Dim strNow As Date
            strNow = DateAdd(DateInterval.Minute, offset, Date.UtcNow)


            If Format(dtDate, "yyyyMMdd") = Format(strNow, "yyyyMMdd") Then
                strFormat = "<b><font color=red>Today</font></b>"
            ElseIf Format(dtDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, strNow), "yyyyMMdd") Then
                strFormat = "<b><font color=orange>" & strFormat & "</font></b>"
            ElseIf Format(dtDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, -1, strNow), "yyyyMMdd") Then
                strFormat = "<b><font color=purple>" & strFormat & "</font></b>"
            End If
            strFormat &= " " & IIf(CInt(strHrs) > 12, CInt(strHrs) - 12, strHrs).ToString & ":" & strMins & " " & IIf(CInt(strHrs) >= 12, "PM", "AM").ToString

            'Return the formatted date.        
            Return strFormat
        End Function

        Public Function fn_GetDateTimeFromNumber(ByVal dtDate As Date, ByVal strFormat As String) As String
            Dim str4Yr As String = Year(dtDate).ToString
            'Set the Year in 2 Digits to a Variable
            Dim str2Yr As String = Mid(Year(dtDate).ToString, 3, 2)
            'Set the Month in 2 Digits to a Variable
            Dim strIntMonth As String = Month(dtDate).ToString

            If CInt(strIntMonth) <= 9 Then
                strIntMonth = "0" & strIntMonth
            End If

            'Set the Date in 2 Digits to a Variable
            Dim strDate As String = Day(dtDate).ToString

            If CInt(strDate) <= 9 Then
                strDate = "0" & strDate
            End If

            'Set the Abbrivated Month Name to a Variable
            Dim str3Month As String = MonthName(CInt(strIntMonth), True)
            'Set the Full Month Name to a Variable
            Dim strFullMonth As String = MonthName(CInt(strIntMonth), False)
            'Set the Nos of Hrs to a Variable
            Dim strHrs As String = Hour(dtDate).ToString
            'Set the Nos of Mins to a Variable
            Dim strMins As String = Minute(dtDate).ToString

            'As the Date Format will be one of the above mentioned formats,
            'we need to replace the required string to get the formatted date.
            strFormat = Replace(strFormat, "DD", strDate)
            strFormat = Replace(strFormat, "YYYY", str4Yr)
            strFormat = Replace(strFormat, "YY", str2Yr)
            strFormat = Replace(strFormat, "MM", strIntMonth)
            strFormat = Replace(strFormat, "MONTH", strFullMonth)
            strFormat = Replace(strFormat, "MON", str3Month)

            If CInt(strHrs) = 0 Then
                strHrs = "12"
                strFormat &= " " & strHrs.ToString & ":" & strMins & " " & "AM"
            Else
                strFormat &= " " & IIf(CInt(strHrs) > 12, CInt(strHrs) - 12, strHrs).ToString & ":" & strMins & " " & IIf(CInt(strHrs) > 12, "PM", "AM").ToString
            End If

            Return strFormat
        End Function

      

        Public Function DateFromFormattedDate(ByVal strDate As String, ByVal strCrtFormat As String) As Date
            Dim dtDate As Date
            If strCrtFormat = "MM/DD/YYYY" Or strCrtFormat = "MM-DD-YYYY" Then
                'dtDate = New Date(CInt(Right(strDate, 4)), CInt(Left(strDate, 2)), CInt(Mid(strDate, 4, 2)))

                Dim str As String() = strDate.Split(New Char() {CChar("/"), CChar("-")})
                dtDate = New Date(CInt(str(2)), CInt(str(0)), CInt(str(1)))
            ElseIf strCrtFormat = "DD/MM/YYYY" Or strCrtFormat = "DD-MM-YYYY" Then
                ''DateFromFormattedDate = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & Right(strDate, 4)
                'dtDate = New Date(CInt(Right(strDate, 4)), CInt(Mid(strDate, 4, 2)), CInt(Left(strDate, 2)))

                Dim str As String() = strDate.Split(New Char() {CChar("/"), CChar("-")})
                dtDate = New Date(CInt(str(2)), CInt(str(1)), CInt(str(0)))
            Else
                dtDate = CDate(strDate)
            End If
            Return dtDate
        End Function

        Public Function GetDateFrmFormattedDateAddDays(ByVal strDate As String, ByVal strCrtFormat As String) As Date

            Dim dtDate As Date
            If strCrtFormat = "MM/DD/YYYY" Or strCrtFormat = "MM-DD-YYYY" Then
                ''str = Left(strDate, 2) & "/" & Mid(strDate, 4, 2) & "/" & Right(strDate, 4)

                dtDate = New Date(CInt(Right(strDate, 4)), CInt(Left(strDate, 2)), CInt(Mid(strDate, 4, 2)))
            ElseIf strCrtFormat = "DD/MM/YYYY" Or strCrtFormat = "DD-MM-YYYY" Then
                ''str = Mid(strDate, 4, 2) & "/" & Left(strDate, 2) & "/" & Right(strDate, 4)

                dtDate = New Date(CInt(Right(strDate, 4)), CInt(Mid(strDate, 4, 2)), CInt(Left(strDate, 2)))
            ElseIf strCrtFormat = "DD/MON/YYYY" Then
                dtDate = CDate(strDate)
            End If
            dtDate = DateAdd(DateInterval.Day, 1, dtDate)
            Return dtDate
        End Function

        Public Function GetQueryStringValOld(ByVal querystring As String, ByVal fldkey As String, Optional ByVal boolReturnComplete As Boolean = False) As String
            Dim objQSV As QueryStringValues
            If Current.Session("objQSV") Is Nothing Then
                objQSV = New QueryStringValues
                Current.Session("objQSV") = objQSV
            Else
                objQSV = CType(Current.Session("objQSV"), QueryStringValues)
            End If
            Return objQSV.GetQueryStringVal(querystring, fldkey, boolReturnComplete)
        End Function

        Public Function GetLocalDate(ByVal strDate As String) As Date
            Dim dtDate As Date
            Dim offset As Integer = -(CInt(Current.Session("ClientMachineUTCTimeOffset")))
            dtDate = DateAdd(DateInterval.Minute, offset, CDate(strDate))
            Return dtDate
        End Function

        <System.Runtime.CompilerServices.Extension()> _
        Public Function TrimLength(ByVal inputString As String, ByVal lengthToTrim As Integer) As String
            If inputString Is Nothing Then Return ""
            Return inputString.Substring(0, Math.Min(inputString.Length, lengthToTrim))
        End Function
    End Module

End Namespace