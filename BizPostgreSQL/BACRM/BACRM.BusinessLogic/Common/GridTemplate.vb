﻿Imports BACRM.BusinessLogic.Common
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Drawing
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.Web.UI.HtmlControls
Imports BACRM.BusinessLogic.Opportunities

Public Class GridTemplate
    Implements ITemplate

    Dim TemplateType As ListItemType
    Dim ColumnName, FieldName, DBColumnName, OrigDBColumnName, ControlType, LookBackTableName, tableAlias, ListType, SortColumnName, SortColumnOrder, FieldDataType, FieldMessage As String
    Dim Custom, AllowEdit, AllowSorting, AllowFiltering As Boolean
    Dim EditPermission, ColumnWidth As Integer
    Dim Grp_Id As Short
    Dim ListID, ListRelID, FormID, FormFieldId As Long
    Dim htGridColumnSearch As Hashtable
    Dim objCommon As New CCommon
    Dim isTelerikGrid As Boolean

    Sub New(ByVal type As ListItemType, ByVal drRow As DataRow, ByVal EPermission As Integer, GridColumnSearch As Hashtable, numFormID As Long, Optional ByVal vcSortColumnName As String = "", Optional ByVal vcSortColumnOrder As String = "", Optional ByVal isTelerik As Boolean = False)
        Try
            TemplateType = type

            FieldName = drRow("vcFieldName").ToString
            DBColumnName = drRow("vcDbColumnName").ToString
            OrigDBColumnName = drRow("vcOrigDbColumnName").ToString
            AllowSorting = CCommon.ToBool(drRow("bitAllowSorting"))
            FormFieldId = CCommon.ToLong(drRow("numFieldId"))
            AllowEdit = CCommon.ToBool(drRow("bitAllowEdit"))

            Custom = CCommon.ToBool(drRow("bitCustomField"))

            ControlType = drRow("vcAssociatedControlType").ToString
            ListType = drRow("vcListItemType").ToString
            ListID = CCommon.ToLong(drRow("numListID"))
            ListRelID = CCommon.ToLong(drRow("ListRelID"))
            ColumnWidth = CCommon.ToInteger(drRow("intColumnWidth"))
            LookBackTableName = drRow("vcLookBackTableName").ToString
            AllowFiltering = CCommon.ToBool(drRow("bitAllowFiltering"))
            FieldDataType = drRow("vcFieldDataType").ToString
            If drRow.Table.Columns.Contains("vcFieldMessage") Then
                FieldMessage = drRow("vcFieldMessage").ToString
            Else
                FieldMessage = ""
            End If

            If drRow.Table.Columns.Contains("Grp_Id") Then
                Grp_Id = CCommon.ToShort(drRow("Grp_Id"))
            Else
                Grp_Id = 0
            End If

            isTelerikGrid = isTelerik

            If LookBackTableName = "CompanyInfo" Then
                tableAlias = "cmp"
            ElseIf LookBackTableName = "DivisionMaster" Then
                tableAlias = "DM"
            ElseIf LookBackTableName = "AdditionalContactsInformation" Then
                tableAlias = "ADC"
            ElseIf LookBackTableName = "ProjectProgress" Then
                tableAlias = "PP"
            ElseIf LookBackTableName = "ProjectsMaster" Then
                tableAlias = "Pro"
            ElseIf LookBackTableName = "Cases" Then
                tableAlias = "cs"
            ElseIf LookBackTableName = "OpportunityMaster" Then
                tableAlias = "Opp"
            ElseIf LookBackTableName = "OpportunityRecurring" Then
                tableAlias = "OPR"
            ElseIf LookBackTableName = "AddressDetails" Then
                tableAlias = "AD"
            ElseIf LookBackTableName = "OpportunityBizDocs" Then
                tableAlias = "OBD"
            ElseIf LookBackTableName = "Item" Then
                tableAlias = "I"
            ElseIf LookBackTableName = "OpportunityBizDocItems" Then
                tableAlias = "OBDI"
            ElseIf LookBackTableName = "OpportunityItems" Then
                tableAlias = "OI"
            ElseIf LookBackTableName = "WareHouseItems" Then
                tableAlias = "WHI"
            ElseIf LookBackTableName = "Warehouses" Then
                tableAlias = "WH"
            ElseIf LookBackTableName = "OpportunityLinking" Then
                tableAlias = "OL"
            ElseIf LookBackTableName = "Vendor" Then
                tableAlias = "V"
            ElseIf LookBackTableName = "DivisionMasterShippingConfiguration" Then
                tableAlias = "DMSC"
            ElseIf LookBackTableName = "WorkOrder" Then
                tableAlias = "WO"
            ElseIf LookBackTableName = "Sales_process_List_Master" Then
                tableAlias = "SP"
            ElseIf Custom = True Then
                tableAlias = "CFW"
            End If

            ColumnName = DBColumnName & "~" & FormFieldId & "~" & IIf(Custom, 1, 0).ToString

            EditPermission = EPermission
            htGridColumnSearch = GridColumnSearch
            FormID = numFormID

            SortColumnName = vcSortColumnName
            SortColumnOrder = vcSortColumnOrder
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
        Try
            Dim lbl1 As Label = New Label()
            Dim lbl2 As Label = New Label()
            Dim lbl3 As Label = New Label()
            Dim lbl4 As Label = New Label()
            Dim lblToolTip As Label = New Label()
            Dim lnkButton As New LinkButton
            Dim lnk As New HyperLink

            Select Case TemplateType
                Case ListItemType.Header
                    Dim cell = If(isTelerikGrid, CType(Container, Telerik.Web.UI.GridTableHeaderCell), CType(Container, DataControlFieldHeaderCell))

                    If ControlType <> "DeleteCheckBox" AndAlso ControlType <> "SalesFulfillmentLog" Then
                        If AllowSorting = False Then

                            If DBColumnName = "StockQtyAdjust" AndAlso FormID = 21 Then
                                lbl1.ID = FormFieldId & DBColumnName
                                lbl1.Text = "<div class='divLeft'><Span class='labelWIdth'>Counted</span><Span class='labelWIdth'>Qty to Adjust</span><Span class='labelWIdth'>Unit Cost</span></div>"
                                'lbl1.ForeColor = Color.White
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "dtmProjectFinishDate" AndAlso FormID = 145 Then
                                lbl1.ID = FormFieldId & DBColumnName
                                lbl1.Text = "<a href='javascript:void(0)' title='Uses Actual Start Date (unless it doesn’t exist yet, in which case the Planned Start date is used), and Item Release date (if it exists, otherwise Requested Finish date is used) to project out the earliest date in which the process could be completed given the Capacity load based on a productive hour maximums and work schedule (for resources used within the process). Note: If no process is involved, the Projected Finish will simply use Item Release date if it exists (WO related), or Requested Finish date if it does not (Project related).'>[?]</a> <span id='0DtmProjectFinishDate'>Projected Finish</span> "
                                'lbl1.ForeColor = Color.White
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "ForecastDetails" AndAlso FormID = 145 Then
                                lbl1.ID = FormFieldId & DBColumnName
                                lbl1.Text = "<a href='javascript:void(0)' title='Projected Finish date compared to Item Release date (in absence of Item-Release,  Requested Finish is used). Days early or on time in green, or late in red. When process is Finished, the Projected Finish date is swapped out for the Actual Finish date (date when total progress gets to 100%)'>[?]</a> <span id='0ForecastDetails'>Forecast</span> "
                                'lbl1.ForeColor = Color.White
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "CapacityLoad" AndAlso FormID = 145 Then
                                lbl1.ID = FormFieldId & DBColumnName
                                lbl1.Text = "<a href='javascript:void(0)' title='Uses the average capacity load for all resources assigned tasks, for a period starting on the Actual Start date (unless it doesn’t exist yet, in which case the Planned Start date is used), through the Projected Finish Date.'>[?]</a> <span id='0CapacityLoad'> Capacity Load</span> "
                                'lbl1.ForeColor = Color.White
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "SchedulePerformance" AndAlso FormID = 145 Then
                                lbl1.ID = FormFieldId & DBColumnName
                                lbl1.Text = "<a href='javascript:void(0)' title='Actual finish date vs Item Release date (if it exists, otherwise Requested Finish date is used)'>[?]</a> <span id='0SchedulePerformance'>Schedule Performance</span> "
                                'lbl1.ForeColor = Color.White
                                Container.Controls.Add(lbl1)
                            Else
                                lbl1.ID = FormFieldId & DBColumnName
                                lbl1.Text = FieldName
                                'lbl1.ForeColor = Color.White
                                Container.Controls.Add(lbl1)
                            End If
                        ElseIf AllowSorting = True Then
                            If DBColumnName = "vcCompanyName" AndAlso (FormID = 34 Or FormID = 35 Or FormID = 36 Or FormID = 10) Then
                                Dim ul As New System.Web.UI.HtmlControls.HtmlGenericControl("ul")
                                ul.Attributes.Add("class", "list-inline")
                                Dim li As New System.Web.UI.HtmlControls.HtmlGenericControl("li")

                                lnkButton.ID = FormFieldId & DBColumnName
                                lnkButton.Text = FieldName
                                'lnkButton.ForeColor = Color.White
                                lnkButton.Attributes.Add("onclick", "return SortColumn('" & tableAlias & "." & OrigDBColumnName.Replace("ADC.", "") & "')")
                                li.Controls.Add(lnkButton)
                                If tableAlias & "." & OrigDBColumnName.Replace("ADC.", "") = SortColumnName Then
                                    li.Controls.Add(New LiteralControl(String.Format("&nbsp;<font color='black'>{0}</font>", IIf(SortColumnOrder = "Desc", "&#9660;", "&#9650;"))))
                                End If
                                ul.Controls.Add(li)

                                li = New System.Web.UI.HtmlControls.HtmlGenericControl("li")
                                Dim img1 As New System.Web.UI.HtmlControls.HtmlGenericControl("img")
                                img1.Attributes.Add("src", "../images/MasterList-16.gif")
                                img1.Style.Add("cursor", "pointer")
                                img1.Attributes.Add("onclick", "return SortColumn('VOA.OpenActionItemCount')")
                                li.Controls.Add(img1)
                                If "VOA.OpenActionItemCount" = SortColumnName Then
                                    li.Controls.Add(New LiteralControl(String.Format("&nbsp;<font color='black'>{0}</font>", IIf(SortColumnOrder = "Desc", "&#9660;", "&#9650;"))))
                                End If
                                ul.Controls.Add(li)

                                li = New System.Web.UI.HtmlControls.HtmlGenericControl("li")
                                Dim img2 As New System.Web.UI.HtmlControls.HtmlGenericControl("img")
                                img2.Attributes.Add("src", "../images/msg_unread_small.gif")
                                img2.Style.Add("cursor", "pointer")
                                img2.Attributes.Add("onclick", "return SortColumn('VIE.Total')")
                                li.Controls.Add(img2)
                                If "VIE.Total" = SortColumnName Then
                                    li.Controls.Add(New LiteralControl(String.Format("&nbsp;<font color='black'>{0}</font>", IIf(SortColumnOrder = "Desc", "&#9660;", "&#9650;"))))
                                End If
                                ul.Controls.Add(li)

                                Container.Controls.Add(ul)
                            ElseIf DBColumnName = "numItemGroup" AndAlso (FormID = 21) Then
                                Dim ul As New System.Web.UI.HtmlControls.HtmlGenericControl("ul")
                                ul.Attributes.Add("class", "list-inline")
                                ul.Style.Add("width", "120px")
                                Dim li As New System.Web.UI.HtmlControls.HtmlGenericControl("li")

                                lnkButton.ID = FormFieldId & DBColumnName
                                lnkButton.Text = FieldName
                                'lnkButton.ForeColor = Color.White
                                lnkButton.Attributes.Add("onclick", "return SortColumn('" & tableAlias & "." & OrigDBColumnName.Replace("ADC.", "") & "')")
                                li.Controls.Add(lnkButton)
                                If tableAlias & "." & OrigDBColumnName.Replace("ADC.", "") = SortColumnName Then
                                    li.Controls.Add(New LiteralControl(String.Format("&nbsp;<font color='black'>{0}</font>", IIf(SortColumnOrder = "Desc", "&#9660;", "&#9650;"))))
                                End If
                                ul.Controls.Add(li)

                                li = New System.Web.UI.HtmlControls.HtmlGenericControl("li")
                                Dim imgAttributes As New ImageButton
                                imgAttributes.ID = "imgOpenItemAttr"
                                imgAttributes.Attributes.Add("src", "../images/edit.png")
                                imgAttributes.Style.Add("cursor", "pointer")
                                imgAttributes.Visible = False
                                li.Controls.Add(imgAttributes)
                                If "VOA.OpenActionItemCount" = SortColumnName Then
                                    li.Controls.Add(New LiteralControl(String.Format("&nbsp;<font color='black'>{0}</font>", IIf(SortColumnOrder = "Desc", "&#9660;", "&#9650;"))))
                                End If
                                ul.Controls.Add(li)
                                Container.Controls.Add(ul)
                            ElseIf FormID = 139 Then
                                lnkButton.ID = FormFieldId & DBColumnName
                                lnkButton.Text = FieldName
                                Dim strsortexp As String
                                If Custom = True Then
                                    strsortexp = "[" & DBColumnName & "~" & FormFieldId & "~" & 1 & "]"
                                Else
                                    strsortexp = "[" & DBColumnName & "~" & FormFieldId & "~" & 0 & "]"
                                End If

                                lnkButton.Attributes.Add("onclick", "return SortColumn('" & strsortexp & "')")
                                Container.Controls.Add(lnkButton)

                                If strsortexp = SortColumnName Then
                                    Container.Controls.Add(New LiteralControl(String.Format("&nbsp;<font color='black'>{0}</font>", IIf(SortColumnOrder = "Desc", "&#9660;", "&#9650;"))))
                                End If
                            Else
                                lnkButton.ID = FormFieldId & DBColumnName
                                lnkButton.Text = FieldName
                                'lnkButton.ForeColor = Color.White
                                lnkButton.Attributes.Add("onclick", "return SortColumn('" & tableAlias & "." & OrigDBColumnName.Replace("ADC.", "") & "')")
                                Container.Controls.Add(lnkButton)

                                If tableAlias & "." & OrigDBColumnName.Replace("ADC.", "") = SortColumnName Then
                                    Container.Controls.Add(New LiteralControl(String.Format("&nbsp;<font color='black'>{0}</font>", IIf(SortColumnOrder = "Desc", "&#9660;", "&#9650;"))))
                                End If
                            End If
                        End If

                        Dim txtSearch As TextBox = New TextBox()
                        Dim ddlSearch As DropDownList = New DropDownList()
                        Dim lstBoxSearch As ListBox = New ListBox()
                        Dim chkSearch As CheckBox = New CheckBox()
                        Dim ddlItemListsearch As Telerik.Web.UI.RadComboBox = New Telerik.Web.UI.RadComboBox()

                        If AllowFiltering = True Then
                            If DBColumnName = "numFollowUpStatus" Then

                                Container.Controls.Add(New LiteralControl("<br />"))

                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()

                                ddlSearch.CssClass = "option-droup-multiSelection-Group"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ClientIDMode = ClientIDMode.Static
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType
                                ddlSearch.Attributes("multiple") = "multiple"
                                Dim dt As New DataTable()
                                objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
                                objCommon.ListID = CCommon.ToLong(30)
                                dt = objCommon.GetMasterListItemsWithRights()
                                Dim item As ListItem = New ListItem("-- All --", "0")
                                ddlSearch.Items.Add(item)
                                ddlSearch.AutoPostBack = False
                                For Each dr As DataRow In dt.Rows
                                    item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                                    item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                                    item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                                    ddlSearch.Items.Add(item)
                                Next
                                'ddlSearch.SelectedIndex = 0
                                'ddlSearch.SelectionMode = ListSelectionMode.Multiple
                                ddlSearch.ClearSelection()
                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    Dim stritemValues As String() = htGridColumnSearch(ddlSearch.ID).ToString.Split(",")
                                    If stritemValues.Length > 0 Then
                                        ddlSearch.ClearSelection()
                                    End If
                                    For Each indivItem As ListItem In ddlSearch.Items
                                        If stritemValues.Contains(indivItem.Value) Then
                                            indivItem.Selected = True
                                        End If
                                    Next
                                End If
                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "vcContractsItemClassificationName" Then
                                Container.Controls.Add(New LiteralControl("<br />"))

                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()

                                ddlSearch.CssClass = "option-droup-multiSelection-Group"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ClientIDMode = ClientIDMode.Static
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType
                                ddlSearch.Attributes("multiple") = "multiple"
                                Dim dt As New DataTable()
                                objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
                                objCommon.ListID = CCommon.ToLong(36)
                                dt = objCommon.GetMasterListItemsWithRights()
                                Dim item As ListItem = New ListItem("-- All --", "0")
                                ddlSearch.Items.Add(item)
                                ddlSearch.AutoPostBack = False
                                For Each dr As DataRow In dt.Rows
                                    item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                                    item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                                    item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                                    ddlSearch.Items.Add(item)
                                Next
                                'ddlSearch.SelectedIndex = 0
                                'ddlSearch.SelectionMode = ListSelectionMode.Multiple
                                ddlSearch.ClearSelection()
                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    Dim stritemValues As String() = htGridColumnSearch(ddlSearch.ID).ToString.Split(",")
                                    If stritemValues.Length > 0 Then
                                        ddlSearch.ClearSelection()
                                    End If
                                    For Each indivItem As ListItem In ddlSearch.Items
                                        If stritemValues.Contains(indivItem.Value) Then
                                            indivItem.Selected = True
                                        End If
                                    Next
                                End If
                                Container.Controls.Add(ddlSearch)

                            ElseIf DBColumnName = "vcInventoryStatus" Then

                                Container.Controls.Add(New LiteralControl("<br />"))

                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()

                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ClientIDMode = ClientIDMode.Static
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                ddlSearch.Items.Add(New ListItem("Not Applicable", "1"))
                                ddlSearch.Items.Add(New ListItem("Shipped", "2"))
                                ddlSearch.Items.Add(New ListItem("Back Order", "3"))
                                ddlSearch.Items.Add(New ListItem("Shippable", "4"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If

                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "vcSignatureType" Then
                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList

                                ddlSearch.Items.Clear()

                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ClientIDMode = ClientIDMode.Static
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Add(New ListItem("-- All --", "-1"))
                                ddlSearch.Items.Add(New ListItem("Service Default", "0"))
                                ddlSearch.Items.Add(New ListItem("Adult Signature Required", "1"))
                                ddlSearch.Items.Add(New ListItem("Direct Signature", "2"))
                                ddlSearch.Items.Add(New ListItem("InDirect Signature", "3"))
                                ddlSearch.Items.Add(New ListItem("No Signature Required", "4"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If

                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "tintEDIStatus" Then
                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList

                                ddlSearch.Items.Clear()
                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ClientIDMode = ClientIDMode.Static
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Add(New ListItem("-- All --", "-1"))
                                ddlSearch.Items.Add(New ListItem("850 Partially Created", "11"))
                                ddlSearch.Items.Add(New ListItem("850 SO Created", "12"))
                                ddlSearch.Items.Add(New ListItem("856 & 810 Sent", "7"))
                                ddlSearch.Items.Add(New ListItem("856 Acknowledged", "6"))
                                ddlSearch.Items.Add(New ListItem("856 Received", "5"))
                                ddlSearch.Items.Add(New ListItem("940 Sent", "3"))
                                ddlSearch.Items.Add(New ListItem("Send 856 & 810 Failed", "9"))
                                ddlSearch.Items.Add(New ListItem("Send 940 Failed", "8"))
                                ddlSearch.Items.Add(New ListItem("940 Acknowledged", "4"))
                                ' ddlSearch.Items.Add(New ListItem("850 Acknowledged", "2"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If

                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "tintInvoicing" Then

                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()
                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                ddlSearch.Items.Add(New ListItem("Fully Invoiced", "1"))
                                ddlSearch.Items.Add(New ListItem("Un-Invoiced", "2"))
                                ddlSearch.Items.Add(New ListItem("Partially Invoiced(Units)", "3"))
                                ddlSearch.Items.Add(New ListItem("Partially Invoiced(%)", "4"))
                                ddlSearch.Items.Add(New ListItem("Deferred Income Invoices", "5"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If
                                Container.Controls.Add(ddlSearch)

                                Container.Controls.Add(New LiteralControl("<br />"))
                                txtSearch.ClientIDMode = ClientIDMode.Static
                                txtSearch.ID = tableAlias & "." & ColumnName.Replace("tintInvoicing", "tintInvoicingTextbox") & "~" & "TextBox"
                                txtSearch.CssClass = "signup"
                                txtSearch.Width = Unit.Percentage(92)
                                If htGridColumnSearch.ContainsKey(txtSearch.ID) Then
                                    txtSearch.Text = htGridColumnSearch(txtSearch.ID).ToString
                                End If

                                Container.Controls.Add(txtSearch)
                                'Modified by Neelam - To make Amount Paid Filterable
                            ElseIf DBColumnName = "monAmountPaid" Then

                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()
                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                ddlSearch.Items.Add(New ListItem("Fully Paid", "1"))
                                ddlSearch.Items.Add(New ListItem("Partially Paid", "2"))
                                ddlSearch.Items.Add(New ListItem("Not Paid", "3"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If
                                If (FormID = 23) Then
                                    ddlSearch.ClientIDMode = ClientIDMode.Static
                                End If

                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "MaxBuildQty" Then

                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()
                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                ddlSearch.Items.Add(New ListItem("= >WO Qty", "1"))
                                ddlSearch.Items.Add(New ListItem("<WO Qty", "2"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If
                                Container.Controls.Add(ddlSearch)
                            ElseIf FormID = 39 AndAlso DBColumnName = "vcOrderedShipped" Then
                                Container.Controls.Add(New LiteralControl("<br />"))
                                Dim table As New System.Web.UI.HtmlControls.HtmlGenericControl("table")
                                Dim tr As New System.Web.UI.HtmlControls.HtmlGenericControl("tr")

                                Dim td1 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                Dim td2 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                td1.Style.Add("width", "50%")
                                td2.Style.Add("width", "50%")
                                td2.Style.Add("padding-left", "5px")
                                Dim strFieldIds As String()
                                Dim dtData As DataTable
                                strFieldIds = FieldMessage.Split("#")
                                ddlSearch = New DropDownList
                                ddlSearch.CssClass = "signup"
                                ddlSearch.ClientIDMode = ClientIDMode.Static
                                ddlSearch.Width = Unit.Percentage(100)
                                ListID = 82
                                dtData = GetDropDownData()
                                For i As Integer = 0 To dtData.Rows.Count - 1
                                    ddlSearch.Items.Add(New ListItem(dtData.Rows(i)(1).ToString, dtData.Rows(i)(0).ToString))
                                Next
                                ddlSearch.Items.Insert(0, New ListItem("Select", "0"))
                                ddlSearch.ID = "Opp.intUsedShippingCompany~" & strFieldIds(0) & "~0~SelectBox~0"
                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If
                                td1.Controls.Add(ddlSearch)
                                tr.Controls.Add(td1)
                                ddlSearch = New DropDownList
                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(100)
                                ddlSearch.ClientIDMode = ClientIDMode.Static
                                ddlSearch.ID = "Opp.numShippingService~" & strFieldIds(1) & "~0~SelectBox~0"
                                dtData = objCommon.GetDropDownValue(0, "PSS", "")
                                For i As Integer = 0 To dtData.Rows.Count - 1
                                    ddlSearch.Items.Add(New ListItem(dtData.Rows(i)(1).ToString, dtData.Rows(i)(0).ToString))
                                Next
                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If

                                td2.Controls.Add(ddlSearch)
                                tr.Controls.Add(td2)
                                table.Controls.Add(tr)
                                Container.Controls.Add(table)
                            ElseIf DBColumnName = "Slp_Name" Then
                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()
                                ddlSearch.CssClass = "signup"
                                ddlSearch.CssClass = "option-droup-multiSelection-Group"
                                ddlSearch.Attributes("multiple") = "multiple"
                                ddlSearch.AutoPostBack = False
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType
                                Dim objAdmin = New BACRM.BusinessLogic.Admin.CAdmin()
                                objAdmin.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                objAdmin.SalesProsID = 0
                                objAdmin.Mode = 3
                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                Dim dtConfiguration As DataTable
                                dtConfiguration = objAdmin.LoadProcessList()
                                For i As Integer = 0 To dtConfiguration.Rows.Count - 1
                                    ddlSearch.Items.Add(New ListItem(dtConfiguration.Rows(i)("Slp_Name").ToString, dtConfiguration.Rows(i)("Slp_Id").ToString))
                                Next

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    Dim stritemValues As String() = htGridColumnSearch(ddlSearch.ID).ToString.Split(",")
                                    If stritemValues.Length > 0 Then
                                        ddlSearch.ClearSelection()
                                    End If
                                    For Each indivItem As ListItem In ddlSearch.Items
                                        If stritemValues.Contains(indivItem.Value) Then
                                            indivItem.Selected = True
                                        End If
                                    Next
                                End If
                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "ForecastDetails" Or DBColumnName = "SchedulePerformance" Then

                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()
                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                ddlSearch.Items.Add(New ListItem("Late", "1"))
                                ddlSearch.Items.Add(New ListItem("Early", "2"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If
                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "ActualProjectDifference" Then
                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()
                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                ddlSearch.Items.Add(New ListItem("> Projected", "1"))
                                ddlSearch.Items.Add(New ListItem("< Projected", "2"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If
                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "vcPerformance" Then
                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList

                                ddlSearch.Items.Clear()
                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                ddlSearch.Items.Add(New ListItem("Last 3 MTD", "1"))
                                ddlSearch.Items.Add(New ListItem("Last 6 MTD", "2"))
                                ddlSearch.Items.Add(New ListItem("Last 1 YTD", "3"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If
                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "intTotalProgress" Then

                                Container.Controls.Add(New LiteralControl("<br />"))

                                ddlSearch = New DropDownList
                                ddlSearch.Items.Clear()

                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ClientIDMode = ClientIDMode.Static
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Insert(0, New ListItem("-- Values filter => --", "0"))
                                ddlSearch.Items.Add(New ListItem("50%", "50"))
                                ddlSearch.Items.Add(New ListItem("60%", "60"))
                                ddlSearch.Items.Add(New ListItem("70%", "70"))
                                ddlSearch.Items.Add(New ListItem("80%", "80"))
                                ddlSearch.Items.Add(New ListItem("90%", "90"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If

                                Container.Controls.Add(ddlSearch)
                            ElseIf DBColumnName = "vcWorkOrderStatus" Then
                                Container.Controls.Add(New LiteralControl("<br />"))
                                ddlSearch = New DropDownList

                                ddlSearch.Items.Clear()
                                ddlSearch.CssClass = "signup"
                                ddlSearch.Width = Unit.Percentage(92)
                                ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType

                                ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                ddlSearch.Items.Add(New ListItem("Fully Pickable", "1"))
                                ddlSearch.Items.Add(New ListItem("Partially Pickable", "2"))
                                ddlSearch.Items.Add(New ListItem("Fully Buildable", "3"))
                                ddlSearch.Items.Add(New ListItem("Partially Buildable", "4"))

                                If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                        ddlSearch.ClearSelection()
                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                    End If
                                End If
                                Container.Controls.Add(ddlSearch)

                            ElseIf ControlType = "TextBox" Or ControlType = "CheckBox" Or ControlType = "CheckBoxList" Or DBColumnName = "WorkOrder" Then
                                If DBColumnName = "vcPoppName" AndAlso FormID = 39 Then
                                    Container.Controls.Add(New LiteralControl("<br />"))
                                    Dim table As New System.Web.UI.HtmlControls.HtmlGenericControl("table")
                                    Dim tr As New System.Web.UI.HtmlControls.HtmlGenericControl("tr")

                                    Dim td1 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                    Dim td2 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                    td1.Style.Add("width", "50%")
                                    td2.Style.Add("width", "50%")
                                    td2.Style.Add("padding-left", "5px")

                                    txtSearch.ClientIDMode = ClientIDMode.Static
                                    txtSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType
                                    txtSearch.CssClass = "signup"
                                    txtSearch.Width = Unit.Percentage(100)
                                    'txtSearch.Attributes.CssStyle.Add("width", "100%;")
                                    If htGridColumnSearch.ContainsKey(txtSearch.ID) Then
                                        txtSearch.Text = htGridColumnSearch(txtSearch.ID).ToString
                                    End If
                                    td1.Controls.Add(txtSearch)
                                    ddlSearch = New DropDownList
                                    ddlSearch.Items.Clear()
                                    ddlSearch.CssClass = "signup"
                                    ddlSearch.Width = Unit.Percentage(100)
                                    ddlSearch.ID = "OBD.monAmountPaid~" & FieldMessage & "~0~Label"

                                    ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                    ddlSearch.Items.Add(New ListItem("Fully Paid", "1"))
                                    ddlSearch.Items.Add(New ListItem("Partially Paid", "2"))
                                    ddlSearch.Items.Add(New ListItem("Not Paid", "3"))

                                    If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                        If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                            ddlSearch.ClearSelection()
                                            ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                        End If
                                    End If
                                    If (FormID = 23) Then
                                        ddlSearch.ClientIDMode = ClientIDMode.Static
                                    End If
                                    td2.Controls.Add(ddlSearch)
                                    tr.Controls.Add(td1)
                                    tr.Controls.Add(td2)
                                    table.Controls.Add(tr)
                                    Container.Controls.Add(table)
                                Else

                                    Container.Controls.Add(New LiteralControl("<br />"))
                                    txtSearch.ClientIDMode = ClientIDMode.Static
                                    txtSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType
                                    txtSearch.CssClass = "signup"
                                    txtSearch.Width = Unit.Percentage(92)
                                    'txtSearch.Attributes.CssStyle.Add("width", "100%;")
                                    If htGridColumnSearch.ContainsKey(txtSearch.ID) Then
                                        txtSearch.Text = htGridColumnSearch(txtSearch.ID).ToString
                                    End If

                                    Container.Controls.Add(txtSearch)
                                End If
                            ElseIf ControlType = "SelectBox" Then
                                Dim dtData As DataTable

                                dtData = GetDropDownData()
                                Container.Controls.Add(New LiteralControl("<br />"))

                                If (FormID = 21 And DBColumnName <> "numItemGroup") Then
                                    ddlItemListsearch = New Telerik.Web.UI.RadComboBox

                                    ddlItemListsearch.Items.Clear()
                                    ddlItemListsearch.DropDownCssClass = "multipleRowsColumns"
                                    ddlItemListsearch.DropDownWidth = New Unit(200, UnitType.Pixel)
                                    ddlItemListsearch.ClientIDMode = ClientIDMode.Static
                                    ddlItemListsearch.Width = Unit.Percentage(92)
                                    ddlItemListsearch.CheckBoxes = True
                                    ddlItemListsearch.ID = tableAlias & "." & ColumnName & "~" & ControlType & "~" & Grp_Id
                                    ddlItemListsearch.FooterTemplate = New FooterTemplate

                                    ddlItemListsearch.Items.Insert(0, New Telerik.Web.UI.RadComboBoxItem("-- None --", "0"))

                                    If dtData IsNot Nothing AndAlso dtData.Rows.Count > 0 Then
                                        For i As Integer = 0 To dtData.Rows.Count - 1
                                            ddlItemListsearch.Items.Add(New Telerik.Web.UI.RadComboBoxItem(dtData.Rows(i)(1).ToString, dtData.Rows(i)(0).ToString))
                                        Next
                                    End If
                                    Container.Controls.Add(ddlItemListsearch)

                                    If htGridColumnSearch.ContainsKey(ddlItemListsearch.ID) Then

                                        Dim stritemValues As String() = htGridColumnSearch(ddlItemListsearch.ID).ToString.Split(",")

                                        For Each item As Telerik.Web.UI.RadComboBoxItem In ddlItemListsearch.Items

                                            If stritemValues.Contains(item.Value) Then
                                                item.Checked = True
                                            End If
                                        Next
                                    Else
                                        ddlItemListsearch.SelectedValue = ""
                                    End If

                                ElseIf DBColumnName = "numVendorID" And FormID = 139 Then
                                    Container.Controls.Add(New LiteralControl("<br />"))
                                    txtSearch.ClientIDMode = ClientIDMode.Static
                                    txtSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType
                                    txtSearch.CssClass = "form-control"
                                    If htGridColumnSearch.ContainsKey(txtSearch.ID) Then
                                        txtSearch.Text = htGridColumnSearch(txtSearch.ID).ToString
                                    End If

                                    Container.Controls.Add(txtSearch)
                                Else
                                    ddlSearch = New DropDownList

                                    ddlSearch.Items.Clear()
                                    ddlSearch.CssClass = "signup"
                                    ddlSearch.ClientIDMode = ClientIDMode.Static
                                    ddlSearch.Width = Unit.Percentage(92)
                                    ddlSearch.ID = tableAlias & "." & ColumnName & "~" & ControlType & "~" & Grp_Id
                                    If (DBColumnName = "vcAssignedTo") AndAlso LookBackTableName = "WorkOrder" Then
                                        ddlSearch.CssClass = "option-droup-multiSelection-Group"
                                        ddlSearch.Attributes("multiple") = "multiple"
                                        ddlSearch.AutoPostBack = False
                                    End If
                                    If (DBColumnName = "numPartner" Or DBColumnName = "numPartenerSource") AndAlso LookBackTableName = "OpportunityMaster" Then
                                        ddlSearch.Attributes.Add("onchange", "javascript:BindPartnerContact(" & CLng(HttpContext.Current.Session("DomainID")) & ");")
                                        ddlSearch.Attributes.Add("attr", "numPartner")
                                    End If

                                    If DBColumnName = "numPartenerContact" AndAlso LookBackTableName = "OpportunityMaster" Then
                                        ddlSearch.Attributes.Add("attr", "numPartenerContact")
                                    End If
                                    If DBColumnName = "numPartenerSource" AndAlso LookBackTableName = "DivisionMaster" Then
                                        ddlSearch.Attributes.Add("onchange", "javascript:BindPartnerContact(" & CLng(HttpContext.Current.Session("DomainID")) & ");")
                                        ddlSearch.Attributes.Add("attr", "numPartenerSource")
                                    End If
                                    If DBColumnName = "numPartenerContact" AndAlso LookBackTableName = "DivisionMaster" Then
                                        ddlSearch.Attributes.Add("attr", "numPartenerContact")
                                    End If

                                    If DBColumnName = "numDefaultShippingServiceID" Or DBColumnName = "numShippingService" Then
                                        OppBizDocs.LoadServiceTypes(0, ddlSearch)
                                        If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                            If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                                ddlSearch.ClearSelection()
                                                ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                            End If
                                        End If
                                    End If

                                    If DBColumnName = "numAssignedTo" Then
                                        ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                        ddlSearch.Items.Insert(1, New ListItem("Unassigned", "-1"))
                                    Else
                                        ddlSearch.Items.Insert(0, New ListItem("-- All --", "0"))
                                    End If

                                    If dtData IsNot Nothing AndAlso dtData.Rows.Count > 0 Then

                                            For i As Integer = 0 To dtData.Rows.Count - 1
                                                ddlSearch.Items.Add(New ListItem(dtData.Rows(i)(1).ToString, dtData.Rows(i)(0).ToString))
                                            Next

                                            If htGridColumnSearch.ContainsKey(ddlSearch.ID) Then
                                                If (DBColumnName = "vcAssignedTo") AndAlso LookBackTableName = "WorkOrder" Then
                                                    Dim stritemValues As String() = htGridColumnSearch(ddlSearch.ID).ToString.Split(",")
                                                    If stritemValues.Length > 0 Then
                                                        ddlSearch.ClearSelection()
                                                    End If
                                                    For Each indivItem As ListItem In ddlSearch.Items
                                                        If stritemValues.Contains(indivItem.Value) Then
                                                            indivItem.Selected = True
                                                        End If
                                                    Next
                                                Else
                                                    If ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString) IsNot Nothing Then
                                                        ddlSearch.ClearSelection()
                                                        ddlSearch.Items.FindByValue(htGridColumnSearch(ddlSearch.ID).ToString).Selected = True
                                                    End If
                                                End If
                                            End If

                                        End If
                                        Container.Controls.Add(ddlSearch)
                                    End If
                                    ElseIf ControlType = "TextBoxRangePercentage" Then
                                Container.Controls.Add(New LiteralControl("<br />"))

                                Dim table As New System.Web.UI.HtmlControls.HtmlGenericControl("table")
                                Dim tr As New System.Web.UI.HtmlControls.HtmlGenericControl("tr")

                                Dim td1 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                Dim td2 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                td2.Style.Add("padding-left", "5px")



                                Dim dtFrom As New TextBox
                                dtFrom.ID = tableAlias & "." & ColumnName & "~" & ControlType & "~RangeFrom"
                                dtFrom.ClientIDMode = ClientIDMode.Static
                                dtFrom.Width = New Unit(40, UnitType.Pixel)
                                If htGridColumnSearch.ContainsKey(dtFrom.ID) Then
                                    dtFrom.Text = htGridColumnSearch(dtFrom.ID).ToString
                                End If
                                td1.Controls.Add(dtFrom)
                                Dim dtFromLabel As New Label
                                dtFromLabel.Text = "to"
                                td1.Controls.Add(dtFromLabel)


                                Dim dtTo As New TextBox
                                dtTo.ClientIDMode = ClientIDMode.Static
                                dtTo.ID = tableAlias & "." & ColumnName & "~" & ControlType & "~RangeTo"
                                dtTo.Width = New Unit(40, UnitType.Pixel)
                                If htGridColumnSearch.ContainsKey(dtTo.ID) Then
                                    dtTo.Text = htGridColumnSearch(dtTo.ID).ToString
                                End If
                                td2.Controls.Add(dtTo)

                                Dim dtToLabel As New Label
                                dtToLabel.Text = "%"
                                td2.Controls.Add(dtToLabel)


                                tr.Controls.Add(td1)
                                tr.Controls.Add(td2)

                                table.Controls.Add(tr)

                                Container.Controls.Add(table)
                            ElseIf ControlType = "DateField" Then
                                Container.Controls.Add(New LiteralControl("<br />"))

                                Dim table As New System.Web.UI.HtmlControls.HtmlGenericControl("table")
                                Dim tr As New System.Web.UI.HtmlControls.HtmlGenericControl("tr")

                                Dim td1 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                'Dim td2 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                'td2.Style.Add("padding-left", "5px")

                                'td1.InnerHtml = "From<br/>"
                                'td2.InnerHtml = "To<br/>"

                                Dim dtFrom As New TextBox
                                dtFrom.ID = tableAlias & "." & ColumnName & "~" & ControlType & "~From"
                                dtFrom.ClientIDMode = ClientIDMode.Static

                                dtFrom.CssClass = "FromDateRangePicker"
                                dtFrom.Width = New Unit(80, UnitType.Pixel)
                                dtFrom.Attributes.Add("placeholder", "From - To")
                                Dim strDateRange As String = ""
                                If htGridColumnSearch.ContainsKey(dtFrom.ID) Then
                                    dtFrom.Text = htGridColumnSearch(dtFrom.ID).ToString
                                    strDateRange = dtFrom.Text
                                End If

                                'Dim ajaxFrom As New AjaxControlToolkit.CalendarExtender()
                                'ajaxFrom.TargetControlID = dtFrom.ID
                                'ajaxFrom.Format = "MM/dd/yyyy"
                                'td1.Controls.Add(ajaxFrom)

                                Dim dtTo As New TextBox
                                dtTo.ClientIDMode = ClientIDMode.Static
                                dtTo.ID = tableAlias & "." & ColumnName & "~" & ControlType & "~To"
                                dtTo.Width = New Unit(80, UnitType.Pixel)
                                dtTo.CssClass = "ToDateRangePicker"
                                If htGridColumnSearch.ContainsKey(dtTo.ID) Then
                                    dtTo.Text = htGridColumnSearch(dtTo.ID).ToString
                                    strDateRange = strDateRange & "#" & dtTo.Text
                                End If
                                dtFrom.Attributes.Add("DateRange", strDateRange)
                                td1.Controls.Add(dtFrom)
                                td1.Controls.Add(New LiteralControl("<br />"))
                                td1.Controls.Add(dtTo)

                                'Dim ajaxTo As New AjaxControlToolkit.CalendarExtender()
                                'ajaxTo.TargetControlID = dtTo.ID
                                'ajaxTo.Format = "MM/dd/yyyy"
                                'td1.Controls.Add(ajaxTo)

                                tr.Controls.Add(td1)
                                'tr.Controls.Add(td2)

                                table.Controls.Add(tr)

                                Container.Controls.Add(table)


                            End If


                        End If
                    ElseIf ControlType = "SalesFulfillmentLog" Then

                    Else
                        If FormID = 135 Then
                            Dim chk As New CheckBox
                            chk.ID = "chkAll"
                            chk.Attributes.Add("onclick", "return SelectAllPurchaseFulfillment('chkAll','chkSelect')")
                            Container.Controls.Add(chk)
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chkAll"
                            chk.Attributes.Add("onclick", "return SelectAll('chkAll','chkSelect')")
                            Container.Controls.Add(chk)
                        End If
                    End If

                    cell.VerticalAlign = VerticalAlign.Top
                    If ColumnWidth > 0 Then
                        cell.Width = ColumnWidth
                    Else
                        cell.Width = 10
                    End If
                    cell.Attributes.Add("id", FormID & "~" & FormFieldId & "~" & Custom)
                Case ListItemType.Item
                    'Dim cell As DataControlFieldCell = CType(Container, DataControlFieldCell)

                    'If ColumnWidth > 0 Then
                    '    cell.Width = ColumnWidth
                    'End If

                    If DBColumnName = "intTotalProgress" Then
                        AddHandler lbl1.DataBinding, AddressOf BindProgressBar ' make progressbar html
                        Container.Controls.Add(lbl1)
                    ElseIf DBColumnName = "vcCampaignAudit" Then
                        AddHandler lnk.DataBinding, AddressOf BindCampaignAudit
                        Container.Controls.Add(lnk)
                    ElseIf DBColumnName = "vcPricedBoxedTracked" Then
                        Dim imgButton As New ImageButton
                        imgButton.Style.Add("vertical-align", "middle")
                        imgButton.ID = "imgShippingReport"
                        imgButton.CommandName = "OpenLog"
                        imgButton.ImageUrl = "~/images/TruckGreen.png"
                        imgButton.Height = New Unit(28, UnitType.Pixel)
                        Container.Controls.Add(imgButton)

                        Dim lit As New Literal
                        lit.ID = "litShipping"
                        AddHandler lit.DataBinding, AddressOf BindSalesFulfillmentPricedBoxedTracked
                        Container.Controls.Add(lit)
                    ElseIf DBColumnName = "numQtyToShipReceive" AndAlso FormID = 135 Then 'Purchase Fulfillment
                        Dim ul As New HtmlGenericControl("ul")
                        ul.Attributes.Add("class", "list-inline")

                        Dim li1 As New HtmlGenericControl("li")
                        Dim li2 As New HtmlGenericControl("li")

                        Dim txtQtytoReceive As New TextBox
                        txtQtytoReceive.ID = "txtQtyFulfilled"
                        txtQtytoReceive.Width = New Unit(50, UnitType.Pixel)
                        txtQtytoReceive.CssClass = "signup"
                        txtQtytoReceive.Attributes.Add("onkeypress", "Javascript:CheckNumber(2,event)")
                        txtQtytoReceive.AutoPostBack = False
                        AddHandler txtQtytoReceive.DataBinding, AddressOf BindPurchaseFulfillmentQtyToReceive
                        li1.Controls.Add(txtQtytoReceive)

                        Dim radWarehouse As New Telerik.Web.UI.RadComboBox
                        radWarehouse.AutoPostBack = True
                        radWarehouse.ClientIDMode = UI.ClientIDMode.AutoID
                        radWarehouse.DataTextField = "vcWareHouse"
                        radWarehouse.DataValueField = "numWareHouseItemId"
                        radWarehouse.DropDownCssClass = "multipleRowsColumns"
                        radWarehouse.AutoPostBack = True
                        radWarehouse.DropDownWidth = New Unit(780, UnitType.Pixel)
                        radWarehouse.Height = New Unit(100, UnitType.Pixel)
                        radWarehouse.Width = New Unit(100, UnitType.Percentage)
                        radWarehouse.HeaderTemplate = New WarehouseTemplate(ListItemType.Header)
                        radWarehouse.ItemTemplate = New WarehouseTemplate(ListItemType.Item)
                        AddHandler radWarehouse.DataBinding, AddressOf BindWarehouseDropDown
                        li2.Controls.Add(radWarehouse)

                        ul.Controls.Add(li1)
                        ul.Controls.Add(li2)

                        Container.Controls.Add(ul)
                    ElseIf DBColumnName = "StockQtyAdjust" AndAlso FormID = 21 Then 'Item Qty Adjust
                        Dim ul As New HtmlGenericControl("ul")
                        ul.Attributes.Add("class", "list-inline")

                        Dim li1 As New HtmlGenericControl("li")
                        Dim li2 As New HtmlGenericControl("li")
                        Dim li3 As New HtmlGenericControl("li")


                        Dim txtCounted As New TextBox
                        txtCounted.ID = "txtCounted"
                        txtCounted.CssClass = "form-control txtClassQtyCount"
                        txtCounted.Width = New Unit(100, UnitType.Pixel)
                        txtCounted.AutoPostBack = False
                        AddHandler txtCounted.DataBinding, AddressOf BindStockOnHandQty
                        li1.Controls.Add(txtCounted)

                        Dim txtAdjQty As New TextBox
                        txtAdjQty.ID = "txtAdjQty"
                        txtAdjQty.CssClass = "form-control txtClassQtyAdjust"
                        txtAdjQty.Width = New Unit(100, UnitType.Pixel)
                        txtAdjQty.AutoPostBack = False
                        li2.Controls.Add(txtAdjQty)

                        Dim txtUnitCost As New TextBox
                        txtUnitCost.ID = "txtUnitCost"
                        txtUnitCost.CssClass = "form-control txtClassUnitCost"
                        txtUnitCost.Width = New Unit(100, UnitType.Pixel)
                        txtUnitCost.AutoPostBack = False
                        li3.Controls.Add(txtUnitCost)

                        Dim hdnWareHouseItemId As New HiddenField
                        hdnWareHouseItemId.ID = "hdnWareHouseItemId"
                        AddHandler hdnWareHouseItemId.DataBinding, AddressOf BindWareHouseItemId
                        li1.Controls.Add(hdnWareHouseItemId)

                        Dim hdnAvailability As New HiddenField
                        hdnAvailability.ID = "hdnAvailability"
                        AddHandler hdnAvailability.DataBinding, AddressOf BindAvailableQty
                        li1.Controls.Add(hdnAvailability)

                        Dim hdnInventoryAverageCost As New HiddenField
                        hdnInventoryAverageCost.ID = "hdnInventoryAverageCost"
                        AddHandler hdnInventoryAverageCost.DataBinding, AddressOf BindInventoryAverageCost
                        li1.Controls.Add(hdnInventoryAverageCost)

                        Dim hdnAssetChartAcntId As New HiddenField
                        hdnAssetChartAcntId.ID = "hdnAssetChartAcntId"
                        AddHandler hdnAssetChartAcntId.DataBinding, AddressOf BindAssetChartAcntId
                        li1.Controls.Add(hdnAssetChartAcntId)

                        Dim hdnItemName As New HiddenField
                        hdnItemName.ID = "hdnItemName"
                        AddHandler hdnItemName.DataBinding, AddressOf BindItemName
                        li1.Controls.Add(hdnItemName)

                        Dim hdnItemCode As New HiddenField
                        hdnItemCode.ID = "hdnItemCode"
                        AddHandler hdnItemCode.DataBinding, AddressOf BindItemCode
                        li1.Controls.Add(hdnItemCode)

                        Dim hdnItemType As New HiddenField
                        hdnItemType.ID = "hdnItemType"
                        AddHandler hdnItemType.DataBinding, AddressOf BindItemType
                        li1.Controls.Add(hdnItemType)

                        Dim hdnOnHandQty As New HiddenField
                        hdnOnHandQty.ID = "hdnOnHandQty"
                        AddHandler hdnOnHandQty.DataBinding, AddressOf BindOnHandQty
                        li1.Controls.Add(hdnOnHandQty)

                        Dim hdnbitSerialized As New HiddenField
                        hdnbitSerialized.ID = "hdnbitSerialized"
                        AddHandler hdnbitSerialized.DataBinding, AddressOf BindbitSerialized
                        li1.Controls.Add(hdnbitSerialized)

                        Dim hdnbitLotNo As New HiddenField
                        hdnbitLotNo.ID = "hdnbitLotNo"
                        AddHandler hdnbitLotNo.DataBinding, AddressOf BindbitLotNo
                        li1.Controls.Add(hdnbitLotNo)

                        ul.Controls.Add(li1)
                        ul.Controls.Add(li2)
                        ul.Controls.Add(li3)

                        Container.Controls.Add(ul)
                    ElseIf DBColumnName = "StockQtyCount" AndAlso FormID = 21 Then 'Item Qty Count

                    ElseIf DBColumnName = "SerialLotNo" AndAlso FormID = 135 Then 'Purchase Fulfillment
                        Dim txLot As New TextBox
                        txLot.ID = "txLot"
                        txLot.CssClass = "signup"
                        txLot.Width = New Unit(100, UnitType.Pixel)
                        txLot.AutoPostBack = False
                        AddHandler txLot.DataBinding, AddressOf BindPurchaseFulfillmentSerialLot
                        Container.Controls.Add(txLot)
                    ElseIf ControlType = "SalesFulfillmentLog" Then
                        AddHandler lbl1.DataBinding, AddressOf BindSalesFulfillmentLog
                        Container.Controls.Add(lbl1)
                    ElseIf DBColumnName = "numVendorID" And FormID = 139 Then
                        Dim ddlVendor As DropDownList = New DropDownList()
                        ddlVendor.Items.Clear()
                        ddlVendor.CssClass = "form-control"
                        ddlVendor.ID = "ddlVendor"
                        ddlVendor.AutoPostBack = False
                        Container.Controls.Add(ddlVendor)
                    ElseIf DBColumnName = "ShipmentMethod" And FormID = 139 Then
                        Dim ul As New HtmlGenericControl("ul")
                        ul.Attributes.Add("class", "list-inline")

                        Dim li1 As New HtmlGenericControl("li")
                        Dim li2 As New HtmlGenericControl("li")
                        Dim li3 As New HtmlGenericControl("li")

                        Dim ddlShipVia As DropDownList = New DropDownList()
                        ddlShipVia.CssClass = "form-control"
                        ddlShipVia.ID = "ddlShipVia"
                        ddlShipVia.AutoPostBack = False
                        li1.Controls.Add(ddlShipVia)

                        Dim ddlShipService As DropDownList = New DropDownList()
                        ddlShipService.CssClass = "form-control"
                        ddlShipService.ID = "ddlShipService"
                        ddlShipService.AutoPostBack = False
                        li1.Controls.Add(ddlShipService)

                        Dim lblLeadDays As New Label
                        lblLeadDays.ID = "lblLeadDays"
                        li3.Controls.Add(lblLeadDays)

                        ul.Controls.Add(li1)
                        ul.Controls.Add(li2)
                        ul.Controls.Add(li3)

                        Container.Controls.Add(ul)
                    ElseIf FormID = 139 And ControlType = "TextBox" And (DBColumnName <> "numItemCode" And DBColumnName <> "vcItemName" And DBColumnName <> "vcWareHouse" And DBColumnName <> "txtItemDesc") Then
                        Dim txt As TextBox = New TextBox()
                        txt.CssClass = "form-control"
                        txt.ID = "txt" + DBColumnName
                        Container.Controls.Add(txt)
                    ElseIf ControlType <> "DeleteCheckBox" Then
                        AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                        Container.Controls.Add(lbl1)
                    Else
                        Dim chk As New CheckBox
                        chk.ID = "chkSelect"
                        chk.CssClass = "chkSelect"
                        Container.Controls.Add(chk)

                        'chk.InputAttributes.Add("class", "chkSelect")
                        AddHandler lbl1.DataBinding, AddressOf Bindvalue
                        Container.Controls.Add(lbl1)
                        If FormID = 34 Or FormID = 35 Or FormID = 36 Or FormID = 39 Or FormID = 38 Or FormID = 40 Or FormID = 41 Then
                            Dim lblContactID As Label = New Label()
                            AddHandler lblContactID.DataBinding, AddressOf BindContactId
                            Container.Controls.Add(lblContactID)
                        End If
                        If FormID = 57 Then
                            AddHandler lbl2.DataBinding, AddressOf BindvalueOppBizDocsID
                            Container.Controls.Add(lbl2)

                            AddHandler lbl3.DataBinding, AddressOf BindOppBizDocItemID
                            Container.Controls.Add(lbl3)

                            AddHandler lbl4.DataBinding, AddressOf BindnumoppitemtCode
                            Container.Controls.Add(lbl4)
                        ElseIf FormID = 1 Then
                            Dim lblDivID As Label = New Label()
                            lblDivID.ID = "lblDivID"
                            lblDivID.Attributes.Add("style", "display:none;")
                            AddHandler lblDivID.DataBinding, AddressOf BindStringColumn1
                            Container.Controls.Add(lblDivID)

                        ElseIf FormID = 23 Then 'Sales Fulfillment
                            Dim litExecutionStatus As New Literal
                            AddHandler litExecutionStatus.DataBinding, AddressOf BindSalesFulfillmentExecutionStatus
                            Container.Controls.Add(litExecutionStatus)

                            Dim lblExecutionPending As New Label
                            Container.Controls.Add(lblExecutionPending)

                            Dim txtoppID As New HiddenField
                            txtoppID.ID = "txtoppID"
                            Container.Controls.Add(txtoppID)
                        ElseIf FormID = 135 Then 'Purchase Fulfillment
                            Dim lbl As New Label
                            lbl.ID = "lblOppItemtCode"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblOppID"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblWarehouseItmsID"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblQtyOrdered"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblQtyReceived"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblExchangeRate"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblCurrencyID"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblDivisionID"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblItemIncomeAccount"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblItemInventoryAsset"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblItemCoGs"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblDropShip"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblCharItemType"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblItemType"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblProjectID"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblClassID"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblItemCode"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblPrice"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblItemName"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblPOppName"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblVendor"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblPPVariance"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            lbl = New Label
                            lbl.ID = "lblRemaining"
                            lbl.Style.Add("display", "none")
                            Container.Controls.Add(lbl)

                            Dim hfbitLotNo As New HiddenField
                            hfbitLotNo.ID = "hfbitLotNo"
                            Container.Controls.Add(hfbitLotNo)

                            Dim hfbitSerialized As New HiddenField
                            hfbitSerialized.ID = "hfbitSerialized"
                            Container.Controls.Add(hfbitSerialized)

                            Dim hdnIsStockTransfer As New HiddenField
                            hdnIsStockTransfer.ID = "hdnIsStockTransfer"
                            Container.Controls.Add(hdnIsStockTransfer)
                        ElseIf FormID = 139 Then 'Planning & Procurement
                            Dim hfItemCode As New HiddenField
                            hfItemCode.ID = "hfItemCode"
                            Container.Controls.Add(hfItemCode)

                            Dim hfItemName As New HiddenField
                            hfItemName.ID = "hfItemName"
                            Container.Controls.Add(hfItemName)

                            Dim hfWarehouseID As New HiddenField
                            hfWarehouseID.ID = "hfWarehouseID"
                            Container.Controls.Add(hfWarehouseID)

                            Dim hfAssetAccountID As New HiddenField
                            hfAssetAccountID.ID = "hfAssetAccountID"
                            Container.Controls.Add(hfAssetAccountID)

                            Dim hfBusinessProcess As New HiddenField
                            hfBusinessProcess.ID = "hfBusinessProcess"
                            Container.Controls.Add(hfBusinessProcess)

                            Dim hfBusinessProcessID As New HiddenField
                            hfBusinessProcessID.ID = "hfBusinessProcessID"
                            Container.Controls.Add(hfBusinessProcessID)

                            Dim hfBuildManager As New HiddenField
                            hfBuildManager.ID = "hfBuildManager"
                            Container.Controls.Add(hfBuildManager)

                            Dim hfBuildManagerID As New HiddenField
                            hfBuildManagerID.ID = "hfBuildManagerID"
                            Container.Controls.Add(hfBuildManagerID)

                            Dim hfAverageCost As New HiddenField
                            hfAverageCost.ID = "hfAverageCost"
                            Container.Controls.Add(hfAverageCost)

                            Dim hfWarehouseItemID As New HiddenField
                            hfWarehouseItemID.ID = "hfWarehouseItemID"
                            Container.Controls.Add(hfWarehouseItemID)

                            Dim hfVendorID As New HiddenField
                            hfVendorID.ID = "hfVendorID"
                            Container.Controls.Add(hfVendorID)

                            Dim hfUOMName As New HiddenField
                            hfUOMName.ID = "hfUOMName"
                            Container.Controls.Add(hfUOMName)

                            Dim hfItemType As New HiddenField
                            hfItemType.ID = "hfItemType"
                            Container.Controls.Add(hfItemType)

                            Dim hfContactID As New HiddenField
                            hfContactID.ID = "hfContactID"
                            Container.Controls.Add(hfContactID)

                            Dim hfUOMID As New HiddenField
                            hfUOMID.ID = "hfUOMID"
                            Container.Controls.Add(hfUOMID)

                            Dim hfQtyToPurchase As New HiddenField
                            hfQtyToPurchase.ID = "hfQtyToPurchase"
                            Container.Controls.Add(hfQtyToPurchase)

                            Dim hfUOMConversionfactor As New HiddenField
                            hfUOMConversionfactor.ID = "hfUOMConversionfactor"
                            Container.Controls.Add(hfUOMConversionfactor)

                            Dim hfPrice As New HiddenField
                            hfPrice.ID = "hfPrice"
                            Container.Controls.Add(hfPrice)

                            Dim hfSKU As New HiddenField
                            hfSKU.ID = "hfSKU"
                            Container.Controls.Add(hfSKU)

                            Dim hfAttributes As New HiddenField
                            hfAttributes.ID = "hfAttributes"
                            Container.Controls.Add(hfAttributes)

                            Dim hfWarehouse As New HiddenField
                            hfWarehouse.ID = "hfWarehouse"
                            Container.Controls.Add(hfWarehouse)

                            Dim hfReleaseDate As New HiddenField
                            hfReleaseDate.ID = "hfReleaseDate"
                            Container.Controls.Add(hfReleaseDate)

                            Dim hdnQtyToBuyBasedOnPurchasePlan As New HiddenField
                            hdnQtyToBuyBasedOnPurchasePlan.ID = "hdnQtyToBuyBasedOnPurchasePlan"
                            Container.Controls.Add(hdnQtyToBuyBasedOnPurchasePlan)
                        ElseIf FormID = 145 Then 'Work Order List Page
                            Dim hfRequiredFinishDate As New HiddenField
                            hfRequiredFinishDate.ID = "hfRequiredFinishDate"
                            Container.Controls.Add(hfRequiredFinishDate)

                            Dim hdnWorkOrderName As New HiddenField
                            hdnWorkOrderName.ID = "hdnWorkOrderName"
                            Container.Controls.Add(hdnWorkOrderName)
                        Else
                            If Not (FormID = 15) And Not (FormID = 139) And Not (FormID = 145) Then
                                AddHandler lbl2.DataBinding, AddressOf BindvalueROwnr
                                AddHandler lbl3.DataBinding, AddressOf BindvalueTerr
                                Container.Controls.Add(lbl2)
                                Container.Controls.Add(lbl3)
                            End If
                        End If
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindWarehouseDropDown(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim radWarehouse As Telerik.Web.UI.RadComboBox = CType(Sender, Telerik.Web.UI.RadComboBox)
            radWarehouse.ID = "radWarehouse"
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindContactId(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lblContactId As Label = CType(Sender, Label)
            Dim Container = TryCast(lblContactId.NamingContainer, Telerik.Web.UI.GridDataItem)
            If Container Is Nothing Then
                Container = CType(lblContactId.NamingContainer, GridViewRow)
            End If

            lblContactId.ID = "lblContactId"
            If FormID = 34 Or FormID = 35 Or FormID = 36 Or FormID = 39 Or FormID = 38 Or FormID = 40 Or FormID = 41 Then
                lblContactId.Text = DataBinder.Eval(Container.DataItem, "numContactID").ToString
            End If
            lblContactId.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub Bindvalue(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container = TryCast(lbl1.NamingContainer, Telerik.Web.UI.GridDataItem)
            If Container Is Nothing Then
                Container = CType(lbl1.NamingContainer, GridViewRow)
            End If

            lbl1.ID = "lbl1"
            If FormID = 34 Or FormID = 35 Or FormID = 36 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numDivisionID").ToString
            ElseIf FormID = 10 Or FormID = 1 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numContactID").ToString
            ElseIf FormID = 12 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numCaseID").ToString
            ElseIf FormID = 13 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numProId").ToString
            ElseIf FormID = 38 Or FormID = 39 Or FormID = 40 Or FormID = 41 Or FormID = 57 Or FormID = 15 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numOppId").ToString
            ElseIf FormID = 21 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numItemCode").ToString
            ElseIf FormID = 145 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numWOID").ToString
            ElseIf FormID = 146 Then
                lbl1.Text = DataBinder.Eval(Container.DataItem, "numContractId").ToString
            End If
            lbl1.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindvalueOppBizDocsID(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl2 As Label = CType(Sender, Label)
            Dim Container = TryCast(lbl2.NamingContainer, Telerik.Web.UI.GridDataItem)
            If Container Is Nothing Then
                Container = CType(lbl2.NamingContainer, GridViewRow)
            End If
            lbl2.ID = "lbl2"
            lbl2.Text = DataBinder.Eval(Container.DataItem, "numOppBizDocsID").ToString
            lbl2.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindOppBizDocItemID(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl3 As Label = CType(Sender, Label)
            Dim Container = TryCast(lbl3.NamingContainer, Telerik.Web.UI.GridDataItem)
            If Container Is Nothing Then
                Container = CType(lbl3.NamingContainer, GridViewRow)
            End If
            lbl3.ID = "lbl3"
            lbl3.Text = DataBinder.Eval(Container.DataItem, "numOppBizDocItemID").ToString
            lbl3.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindnumoppitemtCode(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl4 As Label = CType(Sender, Label)
            Dim Container = TryCast(lbl4.NamingContainer, Telerik.Web.UI.GridDataItem)
            If Container Is Nothing Then
                Container = CType(lbl4.NamingContainer, GridViewRow)
            End If
            lbl4.ID = "lbl4"
            lbl4.Text = DataBinder.Eval(Container.DataItem, "numoppitemtCode").ToString
            lbl4.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindvalueTerr(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl3 As Label = CType(Sender, Label)
            Dim Container = TryCast(lbl3.NamingContainer, Telerik.Web.UI.GridDataItem)
            If Container Is Nothing Then
                Container = CType(lbl3.NamingContainer, GridViewRow)
            End If
            lbl3.ID = "lbl3"
            lbl3.Text = DataBinder.Eval(Container.DataItem, "numTerID").ToString
            lbl3.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindvalueROwnr(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl2 As Label = CType(Sender, Label)
            Dim Container = TryCast(lbl2.NamingContainer, Telerik.Web.UI.GridDataItem)
            If Container Is Nothing Then
                Container = CType(lbl2.NamingContainer, GridViewRow)
            End If
            lbl2.ID = "lbl2"
            lbl2.Text = DataBinder.Eval(Container.DataItem, "numRecOwner").ToString
            lbl2.Attributes.Add("style", "display:none")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindProgressBar(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container = TryCast(lbl1.NamingContainer, Telerik.Web.UI.GridDataItem)
            If Container Is Nothing Then
                Container = CType(lbl1.NamingContainer, GridViewRow)
            End If
            Dim i As Integer = 0
            i = Container.RowIndex

            lbl1.Text = CType(Container.DataItem, DataRowView).Row(ColumnName) & "% <div id=""TotalProgressContainer" & i.ToString & """ class=""ProgressContainer"">" &
                        "&nbsp;<div id=""TotalProgress" & i.ToString & """ class=""ProgressBar"" ></div></div>"

            'Container.Page.ClientScript.RegisterStartupScript(Me.GetType, "ProgressBar" & i.ToString, "<script language=javascript>progress(" & CType(Container.DataItem, DataRowView).Row(ColumnName) & ",'TotalProgressContainer" & i.ToString & "','TotalProgress" & i.ToString & "',90);</script>")

            'ScriptManager.RegisterStartupScript(Me, Me.GetType, "ProgressBar" & i.ToString, "<script language=javascript>progress(" & CType(Container.DataItem, DataRowView).Row(ColumnName) & ",'TotalProgressContainer" & i.ToString & "','TotalProgress" & i.ToString & "',90);</script>", False)
            ScriptManager.RegisterStartupScript(Container.Page, Me.GetType, "ProgressBar" & i.ToString, "progress(" & CType(Container.DataItem, DataRowView).Row(ColumnName) & ",'TotalProgressContainer" & i.ToString & "','TotalProgress" & i.ToString & "',90);", True)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim ControlClass As String = ""

            Dim Container = TryCast(lbl1.NamingContainer, Telerik.Web.UI.GridDataItem)
            If Container Is Nothing Then
                Container = CType(lbl1.NamingContainer, GridViewRow)
            End If

            'Change color of Row
            If DirectCast(Container.DataItem, System.Data.DataRowView).DataView.Table.Columns("vcColorScheme") IsNot Nothing Then
                If DataBinder.Eval(Container.DataItem, "vcColorScheme").ToString.Length > 0 Then
                    Container.CssClass = DataBinder.Eval(Container.DataItem, "vcColorScheme")
                End If
            End If

            If DBColumnName = "vcEmail" Or DBColumnName = "vcCompanyName" Or DBColumnName = "vcFirstName" Or DBColumnName = "vcLastName" _
                Or DBColumnName = "vcWebSite" Or DBColumnName = "numRecOwner" Or DBColumnName = "numContactId" Or DBColumnName = "vcCaseNumber" _
                Or DBColumnName = "vcProjectName" Or DBColumnName = "numCustPrjMgr" Or DBColumnName = "vcPoppName" _
                Or DBColumnName = "numShareWith" Or DBColumnName = "numShippingReportID" Or DBColumnName = "vcBizDocsList" Or DBColumnName = "vcOrderedShipped" _
                Or DBColumnName = "tintInvoicing" Or DBColumnName = "vcSignatureType" Then
                lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName).ToString), "", DataBinder.Eval(Container.DataItem, ColumnName).ToString).ToString

                Dim intermediatory As Integer
                intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)

                If DBColumnName = "vcCompanyName" Then
                    If CCommon.ToShort(DataBinder.Eval(Container.DataItem, "tintCRMType")) = 0 Then
                        lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/Leads/frmLeads.aspx?DivID=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numDivisionID"))) & "'>" & lbl1.Text & "</a>"
                    ElseIf CCommon.ToShort(DataBinder.Eval(Container.DataItem, "tintCRMType")) = 1 Then
                        lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/prospects/frmProspects.aspx?DivID=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numDivisionID"))) & "'>" & lbl1.Text & "</a>"
                    ElseIf CCommon.ToShort(DataBinder.Eval(Container.DataItem, "tintCRMType")) = 2 Then
                        lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/account/frmAccounts.aspx?DivID=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numDivisionID"))) & "'>" & lbl1.Text & "</a>"
                    End If

                    If FormID = 34 Or FormID = 35 Or FormID = 36 Or FormID = 10 Then
                        If DataBinder.Eval(Container.DataItem, "TotalActionItem") > 0 Then
                            lbl1.Text += "&nbsp;&nbsp;<img src='../images/MasterList-16.gif' align=""BASELINE"" style=""float:none;""/>(<a style=""color:Red""  href='javascript:OpenActionItem(" & DataBinder.Eval(Container.DataItem, "numDivisionID").ToString & ")'>" & DataBinder.Eval(Container.DataItem, "TotalActionItem").ToString & "</a>)"
                        End If

                        If DataBinder.Eval(Container.DataItem, "TotalEmail") > 0 Then
                            lbl1.Text += "&nbsp;&nbsp;<img src='../images/msg_unread_small.gif' align=""BASELINE"" style=""float:none;""/>(<a style=""color:Red""  href='javascript:OpenEmail(" & DataBinder.Eval(Container.DataItem, "numDivisionID").ToString & ")'>" & DataBinder.Eval(Container.DataItem, "TotalEmail").ToString & "</a>)"
                        End If
                        ' added by Priya(Followups)
                        If DataBinder.Eval(Container.DataItem, "FollowupFlag").ToString() = "GreenFlag" Then
                            lbl1.Text += "&nbsp;&nbsp;<img src='../images/GreenFlag.png' width='16px' height='16px' style=""float:none;""/>"
                        ElseIf DataBinder.Eval(Container.DataItem, "FollowupFlag").ToString() = "CheckeredFlag" Then
                            lbl1.Text += "&nbsp;&nbsp;<img src='../images/comflag.png' width='16px' height='16px' style=""float:none;""/>"
                        End If
                        ' added by Priya(Campaign Followups mail read/unread count and Campaign History) 
                        If DataBinder.Eval(Container.DataItem, "ReadUnreadCntNHstr").ToString() IsNot "" Then
                            lbl1.Text += "&nbsp;&nbsp;" + DataBinder.Eval(Container.DataItem, "ReadUnreadCntNHstr").ToString()
                        End If
                    End If
                ElseIf DBColumnName = "vcFirstName" Or DBColumnName = "vcLastName" Or DBColumnName = "numContactId" Then
                    lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/contact/frmContacts.aspx?CntId=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numContactID"))) & "'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "numRecOwner" Then
                    lbl1.Text = "<a  href='../contact/frmContacts.aspx?CntId=" & DataBinder.Eval(Container.DataItem, "numRecOwner").ToString & "'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "vcEmail" Then
                    lbl1.Attributes.Add("onclick", "return OpemEmail('" & lbl1.Text.ToString & "'," & DataBinder.Eval(Container.DataItem, "numContactID").ToString & ")")
                    lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "vcWebSite" Then
                    lbl1.Attributes.Add("onclick", "return fn_GoToURL('" & lbl1.Text.ToString & "')")
                    lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "vcCaseNumber" Then
                    lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/cases/frmCases.aspx?frm=Caselist&CaseID=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numCaseID"))) & "'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"

                    If CLng(DataBinder.Eval(Container.DataItem, "numRecOwner")) = -1 Or DataBinder.Eval(Container.DataItem, "numAssignedTo") = -1 Then
                        lbl1.Text += "<a href='javascript:AssignCaseOwner(" & DataBinder.Eval(Container.DataItem, "numCaseID") & ")'><img src='../images/hand.png'/></a> &nbsp;&nbsp;&nbsp;"
                    End If
                ElseIf DBColumnName = "numCustPrjMgr" Then
                    lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/contact/frmContacts.aspx?CntId=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numCustPrjMgr"))) & "'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "vcProjectName" Then
                    lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/projects/frmProjects.aspx?frm=ProjectList&ProId=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numProId"))) & "'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                ElseIf DBColumnName = "vcPoppName" Then
                    lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/opportunity/frmOpportunities.aspx?frm=deallist&OpID=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numOppID"))) & "'>" & lbl1.Text & "</a> "

                    If FormID = 57 Then
                        lbl1.Text += "<a  href='javascript:OpenEditBizDocs(" & DataBinder.Eval(Container.DataItem, "numOppID") & "," & DataBinder.Eval(Container.DataItem, "numOppBizDocsID") & ")'>(Edit)</a>"
                    ElseIf FormID = 23 Then
                        lbl1.Text += "&nbsp;/&nbsp;" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "vcOrderStatus"))
                    ElseIf FormID = 39 Then
                        lbl1.Text += "&nbsp;/&nbsp;" & ReturnMoney(DataBinder.Eval(Container.DataItem, "monDealAmount")) & "&nbsp;/&nbsp;"
                        If DataBinder.Eval(Container.DataItem, "monAmountPaid") >= DataBinder.Eval(Container.DataItem, "monDealAmount") Then
                            lbl1.Text += String.Format("<img src='../images/icons/amountPaid.png' align=""BASELINE"" style=""float:none;"" alt=""{0}"" title=""{0}""/>", ReturnMoney(DataBinder.Eval(Container.DataItem, ColumnName)))
                        Else
                            lbl1.Text += "<span style='color:red'>" & ReturnMoney(DataBinder.Eval(Container.DataItem, "monAmountPaid")) & "</span>"
                        End If
                    End If

                ElseIf DBColumnName = "numShippingReportID" Then

                    If CCommon.ToLong(lbl1.Text) <> 0 Then
                        lbl1.Text = "<a  href='javascript:OpenShippingReport(" & DataBinder.Eval(Container.DataItem, "numOppBizDocsID") & "," & DataBinder.Eval(Container.DataItem, DBColumnName & "~" & FormFieldId & "~0") & "," & DataBinder.Eval(Container.DataItem, "numOppID") & ")'>" & "Shipping Report" & "</a> &nbsp;&nbsp;&nbsp;"
                    Else
                        lbl1.Text = ""
                    End If
                ElseIf DBColumnName = "vcOrderedShipped" Then
                    If FormID = 39 Then

                        Dim strTables As String
                        strTables = "<table class='tableGrid'><tr><td>" & CCommon.GetCommaSeparatedOrderedShipped(lbl1.Text)


                        Dim strShippingIcons As String
                        Dim strShippingString As String
                        strShippingString = ""
                        strShippingIcons = Convert.ToString(DataBinder.Eval(Container.DataItem, "ShippingIcons"))
                        If (strShippingIcons.Length > 0) Then
                            Dim ArrIconsShipping As String()
                            ArrIconsShipping = strShippingIcons.Split(",")
                            If ArrIconsShipping.Count > 0 Then
                                For Each Str As String In ArrIconsShipping
                                    Dim StrIndivIcon As String()
                                    StrIndivIcon = Str.Split("~")
                                    If StrIndivIcon.Count > 0 Then

                                        If Convert.ToInt32(StrIndivIcon(1)) > 0 Then
                                            strShippingString = strShippingString + "&nbsp;<a style='text-decoration: none' href='javascript:void(0);' onclick='openShippingLabel(" & DataBinder.Eval(Container.DataItem, "numOppID") & "," & StrIndivIcon(0) & "," & StrIndivIcon(1) & "," & 0 & ");'><img src='../images/box_generated_22x22.png'></a>"
                                        Else
                                            strShippingString = strShippingString + "&nbsp;<a style='text-decoration: none' href='javascript:void(0);' onclick='openShippingLabel(" & DataBinder.Eval(Container.DataItem, "numOppID") & "," & StrIndivIcon(0) & "," & StrIndivIcon(1) & "," & CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numShippingService")) & ");'><img src='../images/box_not_generated_22x22.png'></a>"
                                        End If
                                        If Convert.ToString(DataBinder.Eval(Container.DataItem, "ISTrackingNumGenerated")) = "1" Then
                                            strShippingString = strShippingString + "&nbsp;<a style='text-decoration: none' href='javascript:void(0);' onclick='openReportList(" & DataBinder.Eval(Container.DataItem, "numOppID") & "," & StrIndivIcon(0) & "," & StrIndivIcon(1) & ");'><img src='../images/barcode_generated_22x22.png'></a>"
                                        End If
                                    End If
                                Next
                            End If
                        End If
                        If strShippingString.Length > 0 Then
                            strTables = strTables & strShippingString & "</td>"
                        Else
                            strTables = strTables & "</td>"
                        End If
                        strTables = strTables + "<td  id='Opp~" & DataBinder.Eval(Container.DataItem, "ShipViaFieldId") & "~False~" & DataBinder.Eval(Container.DataItem, "numOppId") & "~0~0~" & DataBinder.Eval(Container.DataItem, "numDivisionID") & "' class='editable_select' title='Double click To edit...' style='background-color: RGB(255, 255, 245);' > " & DataBinder.Eval(Container.DataItem, "ShipVia") & "</td></tr>"
                        strTables = strTables + "<tr><td style='height: 10px;' colspan='2' id='Opp~" & DataBinder.Eval(Container.DataItem, "ShippingServiceFieldId") & "~False~" & DataBinder.Eval(Container.DataItem, "numOppId") & "~0~0~" & DataBinder.Eval(Container.DataItem, "numDivisionID") & "' class='editable_select' title='Double click To edit...' style='background-color: RGB(255, 255, 245);' ><i>" & DataBinder.Eval(Container.DataItem, "vcShipmentService") & "</i></td>"
                        strTables = strTables + "</tr></table>"
                        lbl1.Text = strTables
                    End If
                ElseIf DBColumnName = "numShareWith" Then
                    If FormID = 34 Or FormID = 35 Or FormID = 36 Then
                        lbl1.Text = "<a  href='javascript:OpenShareRecord(" & DataBinder.Eval(Container.DataItem, "numDivisionID") & ",1)'>" & lbl1.Text & "</a>"
                    ElseIf FormID = 10 Then
                        lbl1.Text = "<a  href='javascript:OpenShareRecord(" & DataBinder.Eval(Container.DataItem, "numContactID") & ",2)'>" & lbl1.Text & "</a>"
                    ElseIf FormID = 12 Then
                        lbl1.Text = "<a  href='javascript:OpenShareRecord(" & DataBinder.Eval(Container.DataItem, "numCaseID") & ",7)'>" & lbl1.Text & "</a>"
                    ElseIf FormID = 13 Then
                        lbl1.Text = "<a  href='javascript:OpenShareRecord(" & DataBinder.Eval(Container.DataItem, "numProId") & ",5)'>" & lbl1.Text & "</a>"
                    ElseIf FormID = 38 Or FormID = 39 Or FormID = 40 Or FormID = 41 Then
                        lbl1.Text = "<a  href='javascript:OpenShareRecord(" & DataBinder.Eval(Container.DataItem, "numOppId") & ",3)'>" & lbl1.Text & "</a>"
                    End If
                ElseIf DBColumnName = "tintInvoicing" Then
                    lbl1.Text = ""
                    If DataBinder.Eval(Container.DataItem, ColumnName).ToString.Length > 0 Then
                        Dim strBizDocsList() As String = DataBinder.Eval(Container.DataItem, ColumnName).Split(New String() {"$^$"}, StringSplitOptions.None)
                        Dim strIDValue() As String
                        Dim EmailSent As String = ""
                        If strBizDocsList.Length > 0 Then
                            For i As Integer = 0 To strBizDocsList.Length - 1
                                strIDValue = strBizDocsList(i).Split(New String() {"#^#"}, StringSplitOptions.None)
                                If FormID = 39 Then
                                    Dim strInvoicePaidOrNot As String = ""
                                    Dim strBreak As String = ""
                                    If i = strBizDocsList.Length - 1 Then
                                        strBreak = ""
                                    Else
                                        strBreak = "<br/>"
                                    End If
                                    Dim OrderBalance As Decimal = 0
                                    Dim InvoiceBalance As Decimal = 0
                                    Dim IsPastDue As Int16 = 0
                                    InvoiceBalance = CCommon.ToDecimal(strIDValue(3)) - CCommon.ToDecimal(strIDValue(4))
                                    If DataBinder.Eval(Container.DataItem, "monAmountPaid") < DataBinder.Eval(Container.DataItem, "monDealAmount") Then
                                        OrderBalance = CCommon.ToDecimal(DataBinder.Eval(Container.DataItem, "monDealAmount")) - CCommon.ToDecimal(DataBinder.Eval(Container.DataItem, "monAmountPaid"))
                                    End If
                                    If CCommon.ToDecimal(strIDValue(3)) = CCommon.ToDecimal(strIDValue(4)) Then
                                        strInvoicePaidOrNot = " - <img src='../images/icons/amountPaid.png' align=""BASELINE"" style=""float:none;"" alt=""{0}"" title=""{0}""/>"
                                    ElseIf CCommon.ToSqlDate(strIDValue(5)) < System.DateTime.Now AndAlso OrderBalance > 0 Then
                                        IsPastDue = 1
                                        strInvoicePaidOrNot = " - <img src='../images/icons/pastdue.png' align=""BASELINE"" style=""float:none;"" alt=""{0}"" title=""{0}""/>"
                                    Else
                                        strInvoicePaidOrNot = " - " & ReturnMoney(CCommon.ToDecimal(strIDValue(3)))
                                    End If
                                    If IsPastDue = 0 Then
                                        If InvoiceBalance > 0 Then
                                            strInvoicePaidOrNot += "- <span style='color:red'><i>(" & ReturnMoney(InvoiceBalance) & ")</i></span>"
                                        ElseIf OrderBalance > 0 Then
                                            strInvoicePaidOrNot += "- <span style='color:red'><i>(" & ReturnMoney(OrderBalance) & ")</i></span>"
                                        End If
                                    End If
                                    If strIDValue(2) = "1" Then
                                        EmailSent = "<img src='../images/icons/EmailSend.png' align=""BASELINE"" style=""float:none;""/>"
                                        If strIDValue(3) IsNot Nothing Then
                                            lbl1.Text += " <a  href='javascript:OpenBizInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & strIDValue(0) & ")'>" & strIDValue(1) & "</a>&nbsp;" & ReturnMoney(CCommon.ToDecimal(strIDValue(3))) & " " & EmailSent & strInvoicePaidOrNot & strBreak
                                        Else
                                            lbl1.Text += " <a  href='javascript:OpenBizInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & strIDValue(0) & ")'>" & strIDValue(1) & "</a>" & " " & EmailSent & strInvoicePaidOrNot & strBreak
                                        End If
                                    Else
                                        lbl1.Text += " <a  href='javascript:OpenBizInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & strIDValue(0) & ")'>" & strIDValue(1) & "</a>" & strInvoicePaidOrNot & strBreak
                                    End If
                                Else
                                    If strIDValue(2) = "1" Then
                                        EmailSent = "<img src='../images/icons/EmailSend.png' align=""BASELINE"" style=""float:none;""/>"
                                        If strIDValue(3) IsNot Nothing Then
                                            lbl1.Text += " <a  href='javascript:OpenBizInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & strIDValue(0) & ")'>" & strIDValue(1) & "</a>&nbsp;" & ReturnMoney(CCommon.ToDecimal(strIDValue(3))) & " " & EmailSent
                                        Else
                                            lbl1.Text += " <a  href='javascript:OpenBizInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & strIDValue(0) & ")'>" & strIDValue(1) & "</a>" & " " & EmailSent
                                        End If
                                    Else
                                        If strIDValue(3) IsNot Nothing Then
                                            lbl1.Text += " <a  href='javascript:OpenBizInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & strIDValue(0) & ")'>" & strIDValue(1) & "</a>&nbsp;" & ReturnMoney(CCommon.ToDecimal(strIDValue(3)))
                                        Else
                                            lbl1.Text += " <a  href='javascript:OpenBizInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & strIDValue(0) & ")'>" & strIDValue(1) & "</a>"
                                        End If
                                    End If
                                End If

                                'lbl1.Text += " <a  href='javascript:OpenBizInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & strIDValue(0) & ")'>" & strIDValue(1) & "</a>" & APPEND YOUR IMAGE If strIDValue(2) = "1" & ","

                            Next
                        End If
                    End If
                ElseIf DBColumnName = "vcBizDocsList" Then
                    lbl1.Text = ""

                    If FormID = 38 Then
                        lbl1.Text = " <a  href='javascript:OpenMirrorBizDoc(" & DataBinder.Eval(Container.DataItem, "numOppId") & ",3)'><img src='../images/icons/cart.png' align=""BASELINE"" style=""float:none;""/></a>,"
                    ElseIf FormID = 39 Then
                        lbl1.Text = "<a  href='javascript:OpenMirrorBizDoc(" & DataBinder.Eval(Container.DataItem, "numOppId") & ",1)'><img src='../images/icons/cart.png' align=""BASELINE"" style=""float:none;""/></a>,"
                    ElseIf FormID = 40 Then
                        lbl1.Text = " <a  href='javascript:OpenMirrorBizDoc(" & DataBinder.Eval(Container.DataItem, "numOppId") & ",4)'><img src='../images/icons/cart.png' align=""BASELINE"" style=""float:none;""/></a>,"
                    ElseIf FormID = 41 Then
                        lbl1.Text = " <a  href='javascript:OpenMirrorBizDoc(" & DataBinder.Eval(Container.DataItem, "numOppId") & ",2)'><img src='../images/icons/cart.png' align=""BASELINE"" style=""float:none;""/></a>,"
                    End If

                    If DataBinder.Eval(Container.DataItem, ColumnName).ToString.Length > 0 Then
                        Dim strBizDocsList() As String = DataBinder.Eval(Container.DataItem, ColumnName).Split(New String() {"$^$"}, StringSplitOptions.None)
                        Dim strIDValue() As String

                        If strBizDocsList.Length > 0 Then
                            For i As Integer = 0 To strBizDocsList.Length - 1
                                strIDValue = strBizDocsList(i).Split(New String() {"#^#"}, StringSplitOptions.None)

                                lbl1.Text += " <a  href='javascript:OpenBizInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & strIDValue(0) & ")'>" & strIDValue(1) & "</a>,"
                            Next
                        End If
                    End If

                    lbl1.Text = lbl1.Text.Trim(",")
                    'If FormID = 39 Or FormID = 41 Then
                    If FormID = 41 Then
                        If Convert.ToInt32(DataBinder.Eval(Container.DataItem, "List_Item_Approval_UNIT")) > 0 Then
                            lbl1.Text += String.Format(" <img class='imgUnItem' onmouseover=OpenItemListForInvoice(" & DataBinder.Eval(Container.DataItem, "numOppId") & ")  src='../images/icons/error.png' alt=""{0}"" title=""{0}"" align=""BASELINE"" style=""float:none;""/>", "The following items are not yet invoiced:" & Convert.ToString(DataBinder.Eval(Container.DataItem, "List_Item_Approval_UNIT")))
                        End If
                    End If
                End If



                If AllowEdit = True Then
                    ControlClass = "click"
                End If
            ElseIf DBColumnName = "vcCompactContactDetails" Then
                Dim lbl2 As New Label
                lbl2.Text = "<a class='text-yellow' href='javascript:OpenContact(" & DataBinder.Eval(Container.DataItem, "numContactID").ToString & ",1)'>" & DataBinder.Eval(Container.DataItem, "vcContactName").ToString & "</a> &nbsp;"
                lbl2.Text += DataBinder.Eval(Container.DataItem, "numContactPhone").ToString & "&nbsp;"
                If DataBinder.Eval(Container.DataItem, "numContactPhoneExtension").ToString <> "" Then
                    lbl2.Text += "&nbsp;(" & DataBinder.Eval(Container.DataItem, "numContactPhoneExtension").ToString & ")"
                End If
                lbl1.Controls.Add(lbl2)
                If DataBinder.Eval(Container.DataItem, "vcContactEmail").ToString <> "" Then
                    Dim img1 As New System.Web.UI.HtmlControls.HtmlGenericControl("img")
                    If FormID = 12 Then
                        img1.Attributes.Add("onclick", "return OpenCaseEmail('" & DataBinder.Eval(Container.DataItem, "vcContactEmail").ToString & "'," & DataBinder.Eval(Container.DataItem, "numCaseID").ToString & ")")
                    Else
                        img1.Attributes.Add("onclick", "return OpemEmail('" & DataBinder.Eval(Container.DataItem, "vcContactEmail").ToString & "'," & DataBinder.Eval(Container.DataItem, "numContactID").ToString & ")")
                    End If

                    img1.Attributes.Add("src", "../images/msg_unread_small.gif")
                    img1.Style.Add("cursor", "pointer")
                    lbl1.Controls.Add(img1)
                End If
            ElseIf DBColumnName = "vcItemName" Then
                If FormID = 139 Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "vcItemName~189~0")), "", DataBinder.Eval(Container.DataItem, "vcItemName~189~0"))
                    lbl1.Attributes.Add("onclick", "return OpenItem('" & DataBinder.Eval(Container.DataItem, "numItemCode") & "')")
                    If (DataBinder.Eval(Container.DataItem, "vcAttributes").ToString() <> "") Then
                        lbl1.Text = "<a  href=#><img height='16' width='16' runat='server' src='../images/tag.png' />" & " " & lbl1.Text & " (" & DataBinder.Eval(Container.DataItem, "vcAttributes").ToString() & ")" & " </a> &nbsp;&nbsp;&nbsp;"
                    Else
                        lbl1.Text = "<a  href=#><img height='16' width='16' runat='server' src='../images/tag.png' />" & " " & lbl1.Text & " </a> &nbsp;&nbsp;&nbsp;"
                    End If

                Else
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "vcItemName~189~0")), "", DataBinder.Eval(Container.DataItem, "vcItemName~189~0"))
                    lbl1.Text = "<a  href='" & VirtualPathUtility.ToAbsolute("~/Items/frmKitDetails.aspx?frm=All Items&ItemCode=" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "numItemCode"))) & "'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                End If

            ElseIf DBColumnName = "vcBizDocID" AndAlso FormID = 23 Then
                lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName).ToString), "", DataBinder.Eval(Container.DataItem, ColumnName).ToString).ToString
                lbl1.Text = "<a href='javascript:void(0);' onclick='OpenBizInvoice(" & CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numOppID")) & "," & CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numoppbizdocsid")) & ");'>" & lbl1.Text & "</a>"
            ElseIf DBColumnName = "vcPathForTImage" Then
                Dim strImagePath As String = ""
                strImagePath = CCommon.GetDocumentPath(DataBinder.Eval(Container.DataItem, "numDomainID")) & IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "vcPathForTImage~351~0")), "", DataBinder.Eval(Container.DataItem, "vcPathForTImage~351~0"))

                If IsDBNull(DataBinder.Eval(Container.DataItem, "vcPathForTImage~351~0")) = True Then
                    lbl1.Text = ""
                Else
                    If DataBinder.Eval(Container.DataItem, "vcPathForTImage~351~0") = "" Then
                        lbl1.Text = ""
                    Else
                        lbl1.Text = "<img style=""vertical-align: middle;text-align:center"" height=""60px"" width=""60px"" src=""" & strImagePath & """ />"
                    End If
                End If
            ElseIf DBColumnName = "vcFulfillmentStatus" Then
                lbl1.Text = HttpUtility.HtmlDecode(CCommon.ToString(DataBinder.Eval(Container.DataItem, ColumnName)))
            ElseIf DBColumnName = "dtExpectedDelivery" Then
                lbl1.ID = "lblExpectedDelivery"
                lbl1.ClientIDMode = ClientIDMode.Static
            Else

                If ControlType = "CheckBox" Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName).ToString), "", IIf(CCommon.ToBool(DataBinder.Eval(Container.DataItem, ColumnName)), "Yes", "No").ToString).ToString
                Else
                    If FieldDataType = "M" Then
                        If (FormID = 39 Or FormID = 41 Or FormID = 23) And DBColumnName = "monAmountPaid" Then
                            'If DataBinder.Eval(Container.DataItem, "monDealAmount") = DataBinder.Eval(Container.DataItem, ColumnName) AndAlso CCommon.ToDouble(DataBinder.Eval(Container.DataItem, ColumnName)) > 0 Then
                            If DataBinder.Eval(Container.DataItem, ColumnName) >= DataBinder.Eval(Container.DataItem, "monDealAmount") Then
                                lbl1.Text = String.Format("<img src='../images/icons/amountPaid.png' align=""BASELINE"" style=""float:none;"" alt=""{0}"" title=""{0}""/>", ReturnMoney(DataBinder.Eval(Container.DataItem, ColumnName)))
                            Else
                                lbl1.Text = ReturnMoney(DataBinder.Eval(Container.DataItem, ColumnName))
                                lbl1.ForeColor = Color.Red
                            End If
                        Else
                            lbl1.Text = ReturnMoney(DataBinder.Eval(Container.DataItem, ColumnName))
                        End If
                    Else
                        If FormID = 12 And (DBColumnName = "textInternalComments" Or DBColumnName = "textDesc") Then
                            Dim strValue As String = ""
                            strValue = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString
                            lbl1.Text = strValue.ToString().Replace(Environment.NewLine, "<br />")

                        ElseIf DBColumnName = "numStatus" Then
                            Dim OrdStatus As String
                            OrdStatus = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString
                            If FormID = 39 Or FormID = 41 Or FormID = 38 Or FormID = 40 Then
                                If Convert.ToInt32(DataBinder.Eval(Container.DataItem, "ApprovalMarginCount")) > 0 Then
                                    OrdStatus += "<span>" + String.Format(" <img class='imgAUnItem' onmouseover=OpenItemListForPendingApproval(" & DataBinder.Eval(Container.DataItem, "numOppId") & ")  src='../images/icons/error.png' alt=""{0}"" title=""{0}"" align=""BASELINE"" style=""float:none;""/>", "Pending Price Approval from:" & Convert.ToString(DataBinder.Eval(Container.DataItem, "ApprovalMarginCount"))) + " </span>"
                                End If
                                If FormID = 38 Or FormID = 40 Then
                                    If Convert.ToInt32(DataBinder.Eval(Container.DataItem, "intPromotionApprovalStatus")) > 0 Then
                                        OrdStatus += String.Format(" <img class='imgPromotionApproval' onmouseover=OpenPromotionApprover(" & DataBinder.Eval(Container.DataItem, "numOppId") & ")  src='../images/icons/error.png' alt=""{0}"" title=""{0}"" align=""BASELINE"" style=""float:none;""/>", "Pending Promotion Approval from:" & Convert.ToString(DataBinder.Eval(Container.DataItem, "intPromotionApprovalStatus")))
                                    End If
                                End If
                            End If

                            lbl1.Text = OrdStatus
                        ElseIf DBColumnName = "numPartner" Then
                            Dim intermediatory As Integer
                            intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)
                            lbl1.Text = "<a  href='javascript:OpenPartner(" & IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString & "," & DataBinder.Eval(Container.DataItem, "tintCRMType").ToString & "," & intermediatory & ")'>" & DataBinder.Eval(Container.DataItem, "vcPartner").ToString & "</a>"
                        ElseIf DBColumnName = "numPartenerSource" Then
                            Dim intermediatory As Integer
                            intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)
                            lbl1.Text = "<a  href='javascript:OpenPartner(" & IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString & "," & DataBinder.Eval(Container.DataItem, "tintCRMType").ToString & "," & intermediatory & ")'>" & DataBinder.Eval(Container.DataItem, "vcPartenerSource").ToString & "</a>"

                        ElseIf DBColumnName = "numPartenerContact" Then
                            lbl1.Text = "<a  href='javascript:OpenContact(" & DataBinder.Eval(Container.DataItem, ColumnName).ToString & ",1)'>" & DataBinder.Eval(Container.DataItem, "vcPartenerContact").ToString & "</a>"

                        ElseIf ((DBColumnName = "numReorder" Or DBColumnName = "numOnHand" Or DBColumnName = "numOnOrder" Or DBColumnName = "numAllocation" Or DBColumnName = "numBackOrder") And FormID = 21) Then ' Item List Form
                            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", String.Format("{0:#,##0}", CCommon.ToDouble(DataBinder.Eval(Container.DataItem, ColumnName))))
                        Else
                            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString
                        End If
                    End If
                End If

                If AllowEdit = True And CCommon.ToBool(System.Web.HttpContext.Current.Session("InlineEdit")) = True Then
                    Select Case ControlType
                        Case "Website", "Email", "TextBox"
                            ControlClass = "click"
                        Case "SelectBox"
                            ControlClass = "editable_select"
                        Case "TextArea"
                            ControlClass = "editable_textarea"
                        Case "CheckBox"
                            ControlClass = "editable_CheckBox"
                        Case "DateField"
                            ControlClass = "editable_DateField"
                    End Select

                    If DBColumnName = "monPAmount" Then
                        ControlClass = "editable_Amount"
                    End If

                    If FormID = 38 Or FormID = 40 Then
                        If DBColumnName = "numPercentageComplete" AndAlso DataBinder.Eval(Container.DataItem, "numBusinessProcessID") > 0 Then
                            ControlClass = ""
                        End If
                    End If

                End If
            End If

            If ControlClass.Length > 0 Then
                If AllowEdit = True And CCommon.ToBool(System.Web.HttpContext.Current.Session("InlineEdit")) = True And EditPermission <> 0 Then

                    Dim vcFormName As String

                    Select Case FormID
                        Case 34, 35, 36
                            vcFormName = "Organization"
                        Case 10
                            vcFormName = "Contact"
                        Case 12
                            vcFormName = "Case"
                        Case 13
                            vcFormName = "Project"
                        Case 38, 39, 40, 41, 57, 23
                            vcFormName = "Opp"
                    End Select

                    Dim cell
                    If isTelerikGrid Then
                        cell = CType(lbl1.Parent, Telerik.Web.UI.GridTableCell)
                    Else
                        cell = CType(lbl1.Parent, DataControlFieldCell)
                    End If
                    If FormID = 34 Or FormID = 35 Or FormID = 36 Or FormID = 10 Then
                        cell.Attributes.Add("id", vcFormName & "~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numContactID").ToString & "~" & DataBinder.Eval(Container.DataItem, "numDivisionID").ToString & "~" & DataBinder.Eval(Container.DataItem, "numTerID").ToString & "~" & ListRelID)
                    ElseIf FormID = 12 Then
                        cell.Attributes.Add("id", vcFormName & "~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numCaseID") & "~" & DataBinder.Eval(Container.DataItem, "numDivisionID") & "~" & DataBinder.Eval(Container.DataItem, "numTerID") & "~" & ListRelID)
                    ElseIf FormID = 13 Then
                        cell.Attributes.Add("id", vcFormName & "~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numProID") & "~" & DataBinder.Eval(Container.DataItem, "numDivisionID") & "~" & DataBinder.Eval(Container.DataItem, "numTerID") & "~" & ListRelID)
                    ElseIf FormID = 38 Or FormID = 39 Or FormID = 40 Or FormID = 41 Or FormID = 57 Then
                        cell.Attributes.Add("id", vcFormName & "~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numOppID") & "~" & DataBinder.Eval(Container.DataItem, "numTerID") & "~" & ListRelID & "~" & DataBinder.Eval(Container.DataItem, "numDivisionID"))
                    ElseIf FormID = 21 Then
                        cell.Attributes.Add("id", "Item~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numItemCode") & "~" & DataBinder.Eval(Container.DataItem, "numTerID") & "~" & ListRelID)
                    ElseIf FormID = 23 Then ' Sales Fullfilment
                        cell.Attributes.Add("id", vcFormName & "~" & FormFieldId & "~" & Custom & "~" & DataBinder.Eval(Container.DataItem, "numOppID") & "~" & DataBinder.Eval(Container.DataItem, "numTerID") & "~" & ListRelID & "~" & DataBinder.Eval(Container.DataItem, "numDivisionID"))
                    End If

                    cell.Attributes.Add("class", ControlClass)

                    'cell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                    'cell.Attributes.Add("onmouseout", "bgColor=''")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function GetDropDownData() As DataTable
        Try
            Dim dtData As New DataTable

            If DBColumnName = "tintSource" AndAlso LookBackTableName = "OpportunityMaster" Then
                objCommon.DomainID = HttpContext.Current.Session("DomainID")
                dtData = objCommon.GetOpportunitySource()
            ElseIf (DBColumnName = "numPartner" Or DBColumnName = "numPartenerSource") AndAlso LookBackTableName = "OpportunityMaster" Then
                objCommon.DomainID = HttpContext.Current.Session("DomainID")
                dtData = objCommon.GetPartnerSource()
            ElseIf DBColumnName = "numPartenerContact" AndAlso LookBackTableName = "OpportunityMaster" Then
                objCommon.DomainID = HttpContext.Current.Session("DomainID")
                dtData = objCommon.GetPartnerAllContacts()
            ElseIf DBColumnName = "numProjectID" AndAlso LookBackTableName = "OpportunityMaster" Then
                Dim objProject As New BACRM.BusinessLogic.Projects.Project
                objProject.DomainID = HttpContext.Current.Session("DomainID")
                dtData = objProject.GetOpenProject()
            ElseIf DBColumnName = "numPartenerSource" AndAlso LookBackTableName = "DivisionMaster" Then
                objCommon.DomainID = HttpContext.Current.Session("DomainID")
                dtData = objCommon.GetPartnerSource()
            ElseIf DBColumnName = "numPartenerContact" AndAlso LookBackTableName = "DivisionMaster" Then
                objCommon.DomainID = HttpContext.Current.Session("DomainID")
                dtData = objCommon.GetDivPartnerAllContacts()
            ElseIf DBColumnName = "vcAssignedTo" AndAlso LookBackTableName = "WorkOrder" Then
                dtData = objCommon.ConEmpList(CCommon.ToLong(HttpContext.Current.Session("DomainID")), False, 0)
            ElseIf ListType = "U" Then
                dtData = objCommon.ConEmpList(CCommon.ToLong(HttpContext.Current.Session("DomainID")), False, 0)
                'ElseIf DBColumnName = "numDefaultShippingServiceID" Then
                '    If CCommon.ToLong(FormID) <> 36 Then
                '        objCommon.DomainID = HttpContext.Current.Session("DomainID")
                '        dtData = objCommon.GetDropDownValue(0, "PSS", "numDefaultShippingServiceID")
                '    End If

            ElseIf ListType = "AG" Then
                dtData = objCommon.GetGroups()
            ElseIf ListType = "C" Then
                Dim objCampaign As New BACRM.BusinessLogic.Reports.PredefinedReports
                objCampaign.byteMode = 2
                objCampaign.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                dtData = objCampaign.GetCampaign()
            ElseIf ListType = "DC" Then
                Dim objCampaign As New BACRM.BusinessLogic.Marketing.Campaign
                With objCampaign
                    .SortCharacter = "0"
                    .UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                    .PageSize = 100
                    .TotalRecords = 0
                    .DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    .columnSortOrder = "Asc"
                    .CurrentPage = 1
                    .columnName = "vcECampName"
                    dtData = objCampaign.ECampaignList
                    Dim drow As DataRow = dtData.NewRow
                    drow("numECampaignID") = -1
                    drow("vcECampName") = "-- Disengaged --"
                    dtData.Rows.Add(drow)
                End With
            ElseIf ListType = "SYS" Then
                dtData = New DataTable
                dtData.Columns.Add("Value")
                dtData.Columns.Add("Text")
                Dim dr1 As DataRow
                dr1 = dtData.NewRow
                dr1("Value") = "0"
                dr1("Text") = "Lead"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "1"
                dr1("Text") = "Prospect"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "2"
                dr1("Text") = "Account"
                dtData.Rows.Add(dr1)
            ElseIf ListType = "UOM" Then
                objCommon = New CCommon
                objCommon.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objCommon.UOMAll = True
                dtData = objCommon.GetItemUOM()
            ElseIf ListType = "WI" Then
                dtData = New DataTable
                dtData.Columns.Add("numListItemID", System.Type.GetType("System.String"))
                dtData.Columns.Add("vcData", System.Type.GetType("System.String"))

                Dim objAPI As New WebAPI
                Dim dtWebApi As DataTable
                objAPI.Mode = 1
                dtWebApi = objAPI.GetWebApi

                Dim row As DataRow = dtData.NewRow

                For Each dr As DataRow In dtWebApi.Rows
                    row = dtData.NewRow
                    row(0) = dr("WebApiID")
                    row(1) = dr("vcProviderName")
                    dtData.Rows.Add(row)
                Next

                'If FormID = 38 Or FormID = 39 Or FormID = 40 Or FormID = 41 Then
                Dim objSite As New Sites
                Dim dtSites As DataTable
                objSite.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                dtSites = objSite.GetSites()

                For Each dr As DataRow In dtSites.Rows
                    row = dtData.NewRow
                    row(0) = "Sites~" & dr("numSiteID")
                    row(1) = dr("vcSiteName")
                    dtData.Rows.Add(row)
                Next

                'End If

                dtData.AcceptChanges()
            ElseIf ListType = "IG" Then
                Dim objItems As New BACRM.BusinessLogic.Item.CItems
                objItems.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objItems.ItemCode = 0
                dtData = objItems.GetItemGroups.Tables(0)
            ElseIf ListID > 0 Then
                dtData = objCommon.GetMasterListItems(ListID, CCommon.ToLong(HttpContext.Current.Session("DomainID")))
            ElseIf DBColumnName = "numManagerID" Then
                objCommon.ContactID = objCommon.ContactID
                objCommon.charModule = "C"
                objCommon.GetCompanySpecificValues1()
                dtData = objCommon.GetManagers(CCommon.ToLong(HttpContext.Current.Session("DomainID")), objCommon.ContactID, objCommon.DivisionID)

            ElseIf DBColumnName = "vcInventoryStatus" Then
                dtData = objCommon.GetInventoryStatus()

            ElseIf DBColumnName = "charSex" Then
                dtData = New DataTable
                dtData.Columns.Add("Value")
                dtData.Columns.Add("Text")
                Dim dr1 As DataRow
                dr1 = dtData.NewRow
                dr1("Value") = "M"
                dr1("Text") = "Male"
                dtData.Rows.Add(dr1)

                dr1 = dtData.NewRow
                dr1("Value") = "F"
                dr1("Text") = "Female"
                dtData.Rows.Add(dr1)
            ElseIf DBColumnName = "tintOppStatus" Then
                dtData = New DataTable
                dtData.Columns.Add("numListItemID")
                dtData.Columns.Add("vcData")

                Dim row As DataRow = dtData.NewRow
                row("numListItemID") = 1
                row("vcData") = "Deal Won"
                dtData.Rows.Add(row)

                row = dtData.NewRow
                row("numListItemID") = 2
                row("vcData") = "Deal Lost"
                dtData.Rows.Add(row)
                dtData.AcceptChanges()
            ElseIf DBColumnName = "ShipmentMethod" And FormID = 139 Then
                dtData = New DataTable
                dtData.Columns.Add("numListItemID")
                dtData.Columns.Add("vcData")

                Dim objDemandForecast As New BACRM.BusinessLogic.DemandForecast.DemandForecast
                objDemandForecast.DomainID = CLng(HttpContext.Current.Session("DomainID"))
                Dim dsVendorDetail As DataSet = objDemandForecast.GetVendorDetail(0, 0, 0, 0)

                If dsVendorDetail.Tables.Count > 1 AndAlso dsVendorDetail.Tables(1).Rows.Count > 0 Then
                    dtData = dsVendorDetail.Tables(1)
                End If

            End If

            Return dtData
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub BindCampaignAudit(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hlp As HyperLink = CType(Sender, HyperLink)
            Dim Container = TryCast(hlp.NamingContainer, Telerik.Web.UI.GridDataItem)
            If Container Is Nothing Then
                Container = CType(hlp.NamingContainer, GridViewRow)
            End If
            Dim campaignAudit As String = DataBinder.Eval(Container.DataItem, ColumnName).ToString()
            campaignAudit = campaignAudit.Replace("#Completed#", "&nbsp;&nbsp;<img src='../images/comflag.png' align=""BASELINE"" style=""float:none;height:16px;width:16px""/>")
            campaignAudit = campaignAudit.Replace("#Running#", "&nbsp;&nbsp;<img src='../images/Circle_Green_16.png' align=""BASELINE"" style=""float:none;height:16px;width:16px""/>")
            hlp.ID = "hlpCampaignAudit"
            hlp.Text = campaignAudit
            hlp.Attributes.Add("onclick", "return openDrip(" & DataBinder.Eval(Container.DataItem, "numConEmailCampID").ToString & "," & DataBinder.Eval(Container.DataItem, "numContactId").ToString & ")")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindPriceLevelDropDownValue(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim ddlPriceLevel As DropDownList = CType(Sender, DropDownList)
            Dim Container As Telerik.Web.UI.GridDataItem = CType(ddlPriceLevel.NamingContainer, Telerik.Web.UI.GridDataItem)

            Dim dtItemsWarehousePrice As New DataTable
            Dim objItems As New BACRM.BusinessLogic.Item.CItems
            objItems.ItemCode = CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numItemCode"))
            objItems.DomainID = CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numDomainID"))
            objItems.WarehouseID = CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numWarehouseID"))
            objItems.DivisionID = 0

            Dim dsPriceLevel As DataSet = objItems.GetPriceLevelWithWarehouseItemPrice()
            dtItemsWarehousePrice = dsPriceLevel.Tables(0)
            ddlPriceLevel.Items.Clear()

            Dim Litem As ListItem
            If dtItemsWarehousePrice.Rows.Count > 0 Then
                For Each dr As DataRow In dtItemsWarehousePrice.Rows
                    Litem = New ListItem()
                    Litem.Text = If(CCommon.ToString(dr("vcWareHouse")).Length > 0,
                                    String.Format("{1}-{0}", CCommon.GetDecimalFormat(dr("decDiscount")), CCommon.ToString(dr("vcWareHouse"))),
                                    String.Format("{0}", CCommon.GetDecimalFormat(dr("decDiscount"))))

                    Litem.Value = CCommon.ToString(dr("KeyValue"))
                    Litem.Attributes("OptionGroup") = "Warehouse Price"
                    ddlPriceLevel.Items.Add(Litem)
                Next
            End If

            If objItems.WarehouseID > 0 Then
                Dim dtItemsPriceLevel As New DataTable
                dtItemsPriceLevel = dsPriceLevel.Tables(1)
                If dtItemsPriceLevel.Rows.Count > 0 Then
                    For Each dr As DataRow In dtItemsPriceLevel.Rows
                        Litem = New ListItem()
                        Litem.Text = (String.Format("{2} ({0}-{1})", CCommon.ToInteger(dr("intFromQty")), CCommon.ToInteger(dr("intToQty")), CCommon.GetDecimalFormat(dr("decDiscount"))))

                        Litem.Value = String.Format("{0}~{1}~{2}~{3}", CCommon.ToLong(dr("numWareHouseID")), CCommon.ToLong(dr("numWareHouseItemID")), CCommon.ToDecimal(dr("decDiscount")), CCommon.ToString(dr("vcWareHouse")))
                        Litem.Attributes("OptionGroup") = "Price Table"
                        ddlPriceLevel.Items.Add(Litem)
                    Next
                End If

                Dim dtItemsPriceRule As New DataTable
                dtItemsPriceRule = dsPriceLevel.Tables(2)
                If dtItemsPriceRule.Rows.Count > 0 Then
                    For Each dr As DataRow In dtItemsPriceRule.Rows
                        Litem = New ListItem()
                        Litem.Text = (String.Format("{2} ({0}-{1})", CCommon.ToInteger(dr("intFromQty")), CCommon.ToInteger(dr("intToQty")), CCommon.GetDecimalFormat(dr("decDiscount"))))

                        Litem.Value = String.Format("{0}~{1}~{2}~{3}", CCommon.ToLong(dr("numWareHouseID")), CCommon.ToLong(dr("numWareHouseItemID")), CCommon.ToDecimal(dr("decDiscount")), CCommon.ToString(dr("vcWareHouse")))
                        Litem.Attributes("OptionGroup") = "Price Rule"
                        ddlPriceLevel.Items.Add(Litem)
                    Next
                End If
            End If

            ddlPriceLevel.ClearSelection()
            If ddlPriceLevel.Items.Count = 0 Then
                ddlPriceLevel.Items.Insert(0, New ListItem("--Select One--", "0"))
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As Object = If(isTelerikGrid, CType(lbl1.NamingContainer, Telerik.Web.UI.GridDataItem), CType(lbl1.NamingContainer, GridViewRow))
            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "numDivisionID")), 0, DataBinder.Eval(Container.DataItem, "numDivisionID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Sub BindSalesFulfillmentExecutionStatus(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim litExecutionStatus As Literal = CType(Sender, Literal)
            Dim Container As Object = If(isTelerikGrid, CType(litExecutionStatus.NamingContainer, Telerik.Web.UI.GridDataItem), CType(litExecutionStatus.NamingContainer, GridViewRow))
            litExecutionStatus.Text = If(Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "bitPendingExecution")), "<i class=""fa fa-2x fa-check-circle"" style=""color:green"" aria-hidden=""true""></i><label style=""margin-left:5px;margin-bottom: 0px;"">Executing Workflow</lable>", "")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindSalesFulfillmentLog(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lblSalesFulfillmentLog As Label = CType(Sender, Label)
            Dim Container As Object = If(isTelerikGrid, CType(lblSalesFulfillmentLog.NamingContainer, Telerik.Web.UI.GridDataItem), CType(lblSalesFulfillmentLog.NamingContainer, GridViewRow))
            lblSalesFulfillmentLog.Text = "<a href='javascript:void(0);' onclick='OpenAutomationExecutionLog(" & CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numOppID")) & ");' style='text-decoration: none;'><img src='../images/GLReport.png' border='0' alt='Automation Execution Log' title='Automation Execution Log'></a>"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindSalesFulfillmentPricedBoxedTracked(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim litShipping As Literal = CType(Sender, Literal)
            Dim Container = If(isTelerikGrid, CType(litShipping.NamingContainer, Telerik.Web.UI.GridDataItem), CType(litShipping.NamingContainer, GridViewRow))

            If Not String.IsNullOrEmpty(CCommon.ToString(DataBinder.Eval(Container.DataItem, ColumnName))) Then
                Dim arrShippingReports As String() = CCommon.ToString(DataBinder.Eval(Container.DataItem, ColumnName)).Split(",")
                If arrShippingReports.Count > 0 AndAlso Not litShipping Is Nothing Then

                    For Each Str As String In arrShippingReports
                        Dim arrReport As String()
                        arrReport = Str.Split("~")
                        If arrReport.Count > 0 Then

                            If Convert.ToInt32(arrReport(3)) > 0 Then
                                litShipping.Text = litShipping.Text & "&nbsp;<a style='text-decoration: none' href='javascript:void(0);' onclick='openShippingLabel(" & arrReport(0) & "," & arrReport(1) & "," & arrReport(2) & "," & Convert.ToString(DataBinder.Eval(Container.DataItem, "intUsedShippingCompany")) & "," & Convert.ToString(DataBinder.Eval(Container.DataItem, "numShippingService")) & ");'><img src='../images/box_generated_22x22.png'></a>"
                            Else
                                litShipping.Text = litShipping.Text & "&nbsp;<a style='text-decoration: none' href='javascript:void(0);' onclick='openShippingLabel(" & arrReport(0) & "," & arrReport(1) & "," & arrReport(2) & "," & Convert.ToString(DataBinder.Eval(Container.DataItem, "intUsedShippingCompany")) & "," & Convert.ToString(DataBinder.Eval(Container.DataItem, "numShippingService")) & ");'><img src='../images/box_not_generated_22x22.png'></a>"
                            End If

                            litShipping.Text = litShipping.Text + "&nbsp;<a style='text-decoration: none' href='javascript:void(0);' onclick='openReportList(" & arrReport(0) & "," & arrReport(1) & "," & arrReport(2) & ");'>" & If(CCommon.ToBool(DataBinder.Eval(Container.DataItem, "bitTrackingNumGenerated")), "<img src='../images/barcode_generated_22x22.png'>", "<img src='../images/barcode_not_generated_22x22.png'>") & "</a>"
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindPurchaseFulfillmentQtyToReceive(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim txtQtyToReceive As TextBox = CType(Sender, TextBox)
            Dim Container = If(isTelerikGrid, CType(txtQtyToReceive.NamingContainer, Telerik.Web.UI.GridDataItem), CType(txtQtyToReceive.NamingContainer, GridViewRow))
            txtQtyToReceive.Text = Convert.ToDouble(DataBinder.Eval(Container.DataItem, "numUnitHour")) - Convert.ToInt32(DataBinder.Eval(Container.DataItem, "numUnitHourReceived"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindbitSerialized(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hdnbitSerialized As HiddenField = CType(Sender, HiddenField)
            Dim Container = If(isTelerikGrid, CType(hdnbitSerialized.NamingContainer, Telerik.Web.UI.GridDataItem), CType(hdnbitSerialized.NamingContainer, GridViewRow))
            hdnbitSerialized.Value = Convert.ToString(DataBinder.Eval(Container.DataItem, "bitSerialized"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindbitLotNo(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hdnbitLotNo As HiddenField = CType(Sender, HiddenField)
            Dim Container = If(isTelerikGrid, CType(hdnbitLotNo.NamingContainer, Telerik.Web.UI.GridDataItem), CType(hdnbitLotNo.NamingContainer, GridViewRow))
            hdnbitLotNo.Value = Convert.ToString(DataBinder.Eval(Container.DataItem, "bitLotNo"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindOnHandQty(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hdnOnHandQty As HiddenField = CType(Sender, HiddenField)
            Dim Container = If(isTelerikGrid, CType(hdnOnHandQty.NamingContainer, Telerik.Web.UI.GridDataItem), CType(hdnOnHandQty.NamingContainer, GridViewRow))
            hdnOnHandQty.Value = Convert.ToString(DataBinder.Eval(Container.DataItem, "numIndividualOnHandQty"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindItemName(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hdnItemName As HiddenField = CType(Sender, HiddenField)
            Dim Container = If(isTelerikGrid, CType(hdnItemName.NamingContainer, Telerik.Web.UI.GridDataItem), CType(hdnItemName.NamingContainer, GridViewRow))
            hdnItemName.Value = Convert.ToString(DataBinder.Eval(Container.DataItem, "vcItemName"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindAssetChartAcntId(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hdnAssetChartAcntId As HiddenField = CType(Sender, HiddenField)
            Dim Container = If(isTelerikGrid, CType(hdnAssetChartAcntId.NamingContainer, Telerik.Web.UI.GridDataItem), CType(hdnAssetChartAcntId.NamingContainer, GridViewRow))
            hdnAssetChartAcntId.Value = Convert.ToString(DataBinder.Eval(Container.DataItem, "numAssetChartAcntId"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindItemType(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hdnItemType As HiddenField = CType(Sender, HiddenField)
            Dim Container = If(isTelerikGrid, CType(hdnItemType.NamingContainer, Telerik.Web.UI.GridDataItem), CType(hdnItemType.NamingContainer, GridViewRow))
            hdnItemType.Value = Convert.ToString(DataBinder.Eval(Container.DataItem, "charItemType"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindItemCode(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hdnItemCode As HiddenField = CType(Sender, HiddenField)
            Dim Container = If(isTelerikGrid, CType(hdnItemCode.NamingContainer, Telerik.Web.UI.GridDataItem), CType(hdnItemCode.NamingContainer, GridViewRow))
            hdnItemCode.Value = Convert.ToString(DataBinder.Eval(Container.DataItem, "numItemCode"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindInventoryAverageCost(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hdnInventoryAverageCost As HiddenField = CType(Sender, HiddenField)
            Dim Container = If(isTelerikGrid, CType(hdnInventoryAverageCost.NamingContainer, Telerik.Web.UI.GridDataItem), CType(hdnInventoryAverageCost.NamingContainer, GridViewRow))
            hdnInventoryAverageCost.Value = Convert.ToString(DataBinder.Eval(Container.DataItem, "monAverageCost"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindWareHouseItemId(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hdnWareHouseItemId As HiddenField = CType(Sender, HiddenField)
            Dim Container = If(isTelerikGrid, CType(hdnWareHouseItemId.NamingContainer, Telerik.Web.UI.GridDataItem), CType(hdnWareHouseItemId.NamingContainer, GridViewRow))
            hdnWareHouseItemId.Value = Convert.ToString(DataBinder.Eval(Container.DataItem, "numWareHouseItemId"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindAvailableQty(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim hdnAvailability As HiddenField = CType(Sender, HiddenField)
            Dim Container = If(isTelerikGrid, CType(hdnAvailability.NamingContainer, Telerik.Web.UI.GridDataItem), CType(hdnAvailability.NamingContainer, GridViewRow))
            hdnAvailability.Value = Convert.ToString(DataBinder.Eval(Container.DataItem, "numAvailabilityQty"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindStockOnHandQty(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim txtCounted As TextBox = CType(Sender, TextBox)
            Dim Container = If(isTelerikGrid, CType(txtCounted.NamingContainer, Telerik.Web.UI.GridDataItem), CType(txtCounted.NamingContainer, GridViewRow))
            txtCounted.Text = Convert.ToString(DataBinder.Eval(Container.DataItem, "numAvailabilityQty"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Sub BindPurchaseFulfillmentSerialLot(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim txtLot As TextBox = CType(Sender, TextBox)
            Dim Container = If(isTelerikGrid, CType(txtLot.NamingContainer, Telerik.Web.UI.GridDataItem), CType(txtLot.NamingContainer, GridViewRow))
            txtLot.Text = Convert.ToString(DataBinder.Eval(Container.DataItem, ColumnName))
        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class

Public Class WarehouseTemplate
    Implements ITemplate

    Private templateType As ListItemType

    Sub New(ByVal type As ListItemType)
        Try
            templateType = type
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub InstantiateIn(container As Control) Implements ITemplate.InstantiateIn
        If templateType = ListItemType.Header Then
            Dim ul As New HtmlGenericControl("ul")

            Dim li As HtmlGenericControl

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col1")
            li.InnerText = "Warehouse"
            ul.Controls.Add(li)

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col2")
            li.InnerText = "Attributes"
            ul.Controls.Add(li)

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col3")
            li.InnerText = "On Hand"
            ul.Controls.Add(li)

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col4")
            li.InnerText = "On Order"
            ul.Controls.Add(li)

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col5")
            li.InnerText = "On Allocation"
            ul.Controls.Add(li)

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col6")
            li.InnerText = "BackOrder"
            ul.Controls.Add(li)

            container.Controls.Add(ul)
        ElseIf templateType.Item Or ListItemType.AlternatingItem Then
            Dim ul As New HtmlGenericControl("ul")

            Dim li As HtmlGenericControl

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col1")
            AddHandler li.DataBinding, AddressOf Me.BindWarehouse
            ul.Controls.Add(li)

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col2")
            AddHandler li.DataBinding, AddressOf Me.BindAttribures
            ul.Controls.Add(li)

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col3")
            AddHandler li.DataBinding, AddressOf Me.BindOnHand
            ul.Controls.Add(li)

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col4")
            AddHandler li.DataBinding, AddressOf Me.BindOnOrder
            ul.Controls.Add(li)

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col5")
            AddHandler li.DataBinding, AddressOf Me.BindOnAllocation
            ul.Controls.Add(li)

            li = New HtmlGenericControl("li")
            li.Attributes.Add("class", "col6")
            AddHandler li.DataBinding, AddressOf Me.BindOnBackOrder
            ul.Controls.Add(li)

            container.Controls.Add(ul)
        End If
    End Sub

    Sub BindWarehouse(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
            Dim Container As Telerik.Web.UI.RadComboBoxItem = CType(li.NamingContainer, Telerik.Web.UI.RadComboBoxItem)
            li.InnerText = IIf(String.IsNullOrEmpty(CCommon.ToString(DataBinder.Eval(Container.DataItem, "vcWarehouse"))), "-", DataBinder.Eval(Container.DataItem, "vcWarehouse"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindAttribures(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
            Dim Container As Telerik.Web.UI.RadComboBoxItem = CType(li.NamingContainer, Telerik.Web.UI.RadComboBoxItem)
            li.InnerText = IIf(String.IsNullOrEmpty(CCommon.ToString(DataBinder.Eval(Container.DataItem, "Attr"))), "-", DataBinder.Eval(Container.DataItem, "Attr"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindOnHand(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
            Dim Container As Telerik.Web.UI.RadComboBoxItem = CType(li.NamingContainer, Telerik.Web.UI.RadComboBoxItem)
            li.InnerText = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "numOnHand")), "0", DataBinder.Eval(Container.DataItem, "numOnHand"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindOnOrder(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
            Dim Container As Telerik.Web.UI.RadComboBoxItem = CType(li.NamingContainer, Telerik.Web.UI.RadComboBoxItem)
            li.InnerText = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "numOnOrder")), "0", DataBinder.Eval(Container.DataItem, "numOnOrder"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindOnAllocation(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
            Dim Container As Telerik.Web.UI.RadComboBoxItem = CType(li.NamingContainer, Telerik.Web.UI.RadComboBoxItem)
            li.InnerText = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "numAllocation")), "0", DataBinder.Eval(Container.DataItem, "numAllocation"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindOnBackOrder(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
            Dim Container As Telerik.Web.UI.RadComboBoxItem = CType(li.NamingContainer, Telerik.Web.UI.RadComboBoxItem)
            li.InnerText = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "numBackOrder")), "0", DataBinder.Eval(Container.DataItem, "numBackOrder"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


End Class

Class FooterTemplate
    Implements ITemplate

    Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
        Dim table As New HtmlTable()
        Dim row As New HtmlTableRow()
        Dim cell As New HtmlTableCell()
        Dim chkAll As New CheckBox
        chkAll.Text = "Select All"
        chkAll.Attributes.Add("onclick", "CheckAllRadComboxes('" & container.NamingContainer.ID & "',this)")
        cell.Controls.Add(chkAll)
        row.Controls.Add(cell)
        table.Controls.Add(row)
        container.Controls.Add(table)


    End Sub

End Class
