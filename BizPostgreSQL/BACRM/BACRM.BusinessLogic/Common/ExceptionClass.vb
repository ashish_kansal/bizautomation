﻿'Created by Anoop Jayaraj

Option Explicit On
Option Strict On
Imports MailBee
Imports MailBee.Security
Imports MailBee.Mime
Imports MailBee.SmtpMail
Imports System.Configuration
Imports System.Web.HttpContext
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Common

    Public Class ExceptionModule
        Inherits BACRM.BusinessLogic.CBusinessBase

        Public Shared Function ExceptionPublish(ByVal ex As Exception, ByVal DomainID As Long, ByVal UserContactID As Long, ByVal req As System.Web.HttpRequest) As Boolean
            Try



                If ex.Message = "Thread was being aborted." Then Exit Function
                Dim objQueryString As New QueryStringValues
                Dim objExceptionPublisher As New ExceptionPublisher
                objExceptionPublisher.DomainID = DomainID
                objExceptionPublisher.Message = ex.Message & "<br>" & ex.Source & "<br>" & ex.HelpLink & "<br>" & ex.StackTrace & "<br>" & ex.TargetSite.ToString
                Dim strURL As String = ""
                Dim strReferrer As String = ""
                Dim strUserAgent As String = ""


                If Not req Is Nothing Then
                    If Not req.Url Is Nothing Then
                        strURL = req.Url.AbsolutePath
                    End If

                    If Not req.QueryString("enc") Is Nothing Then
                        strURL = strURL + "?" + objQueryString.Decrypt(req.QueryString("enc").Trim)
                    Else
                        strURL = req.Url.AbsoluteUri
                    End If

                    If (Not req.UrlReferrer Is Nothing) Then
                        strReferrer = req.UrlReferrer.PathAndQuery
                    End If

                    If Not req.UserAgent Is Nothing Then
                        strUserAgent = req.UserAgent
                    End If
                End If

                objExceptionPublisher.URL = strURL
                objExceptionPublisher.Referrer = strReferrer
                objExceptionPublisher.UserCntID = UserContactID
                objExceptionPublisher.Browser = req.Browser.Type
                objExceptionPublisher.BrowserAgent = req.UserAgent & req.UserHostAddress
                objExceptionPublisher.ManageException()

                If ConfigurationManager.AppSettings("SendExceptionMail") = "1" Then

                    Dim strBodyHtmlText As String
                    strBodyHtmlText = ex.Message & "<br>" & ex.Source & "<br>" & ex.HelpLink & "<br>" & ex.StackTrace & "<br>"
                    If Not ex.TargetSite Is Nothing Then
                        strBodyHtmlText &= ex.TargetSite.ToString()
                    End If
                    If Not Current.Session("ContactName") Is Nothing Then
                        strBodyHtmlText &= "<br>User:" & Current.Session("ContactName").ToString
                    End If
                    If Not Current.Session("UserEmail") Is Nothing Then
                        strBodyHtmlText &= "<br>UserEmail:" & Current.Session("UserEmail").ToString
                    End If
                    If Not Current.Session("CompanyName") Is Nothing Then
                        strBodyHtmlText &= Current.Session("CompanyName").ToString
                    End If
                    If Not Current.Session("DomainName") Is Nothing Then
                        strBodyHtmlText &= "<br>DomainName:" & Current.Session("DomainName").ToString
                    End If

                    strBodyHtmlText &= "<br>URL:" & strURL & "<br>Referrer:" & strReferrer & "<br>Browser:" & req.Browser.Type & "<br>Browser Agent:" & strUserAgent

                    Dim objEmail As New Email
                    objEmail.SendSystemEmail(ex.Message, strBodyHtmlText, "", "noreply@bizautomation.com", ConfigurationManager.AppSettings("SendExceptionMailAddress"), "BizAutomation", "")

                End If

                Return True
            Catch exNon As Exception

            End Try
        End Function


        Public Shared Function ExceptionPublish(ByVal exceptionMessage As String, ByVal DomainID As Long, ByVal UserContactID As Long, ByVal req As System.Web.HttpRequest) As Boolean
            Try
                Dim objQueryString As New QueryStringValues
                Dim objExceptionPublisher As New ExceptionPublisher
                objExceptionPublisher.DomainID = DomainID
                objExceptionPublisher.Message = exceptionMessage
                Dim strURL As String = ""
                Dim strReferrer As String = ""
                Dim strBrowser As String = ""
                Dim strUserAgent As String = ""

                If Not req Is Nothing Then
                    strURL = req.Url.AbsolutePath
                    If Not req.QueryString("enc") Is Nothing Then
                        strURL = strURL + "?" + objQueryString.Decrypt(req.QueryString("enc").Trim)
                    Else
                        strURL = req.Url.AbsoluteUri
                    End If

                    If (Not req.UrlReferrer Is Nothing) Then
                        strReferrer = req.UrlReferrer.PathAndQuery
                    End If

                    If Not req.UserAgent Is Nothing Then
                        strUserAgent = req.UserAgent
                    End If

                    strBrowser = req.Browser.Type
                    strUserAgent = req.UserAgent & req.UserHostAddress
                End If
                

                objExceptionPublisher.URL = strURL
                objExceptionPublisher.Referrer = strReferrer
                objExceptionPublisher.UserCntID = UserContactID
                objExceptionPublisher.Browser = strBrowser
                objExceptionPublisher.BrowserAgent = strUserAgent
                objExceptionPublisher.ManageException()

                Return True
            Catch exNon As Exception

            End Try
        End Function

        'Used by ECommerce Project (BizCart)
        Public Shared Function ExceptionPublish(ByVal ex As Exception, ByVal DomainID As Long, ByVal UserContactID As Long, ByVal SiteId As Long, ByVal req As System.Web.HttpRequest) As String
            Dim strExceptionMessge As String
            strExceptionMessge = String.Empty
            Try
                If ex.Message = "Thread was being aborted." Then Exit Function
                Dim objQueryString As New QueryStringValues
                Dim objExceptionPublisher As New ExceptionPublisher
                objExceptionPublisher.DomainID = DomainID
                objExceptionPublisher.Message = ex.Message & "<br>" & ex.Source & "<br>" & ex.HelpLink & "<br>" & ex.StackTrace & "<br>" & ex.TargetSite.ToString
                Dim strURL As String = ""
                Dim strReferrer As String = ""
                Dim strUserAgent As String = ""
                If Not req Is Nothing Then
                    strURL = req.Url.AbsoluteUri
                    If Not req.QueryString("enc") Is Nothing Then
                        strURL = strURL + "?" + objQueryString.Decrypt(req.QueryString("enc").Trim)
                    End If

                    If (Not req.UrlReferrer Is Nothing) Then
                        strReferrer = req.UrlReferrer.PathAndQuery
                    End If

                    If Not req.UserAgent Is Nothing Then
                        strUserAgent = req.UserAgent
                    End If
                End If

                objExceptionPublisher.URL = strURL
                objExceptionPublisher.Referrer = strReferrer
                objExceptionPublisher.UserCntID = UserContactID
                objExceptionPublisher.Browser = If(Not req Is Nothing, req.Browser.Type, "")
                objExceptionPublisher.BrowserAgent = If(Not req Is Nothing, req.UserAgent & req.UserHostAddress, "")
                objExceptionPublisher.ManageException()

                If ConfigurationManager.AppSettings("SendExceptionMail") = "1" Then

                    Dim strBodyHtmlText As String
                    strBodyHtmlText = ex.Message & "<br>" & ex.Source & "<br>" & ex.HelpLink & "<br>" & ex.StackTrace & "<br>"
                    If Not ex.TargetSite Is Nothing Then
                        strBodyHtmlText &= ex.TargetSite.ToString()
                    End If
                    If Not Current.Session("ContactName") Is Nothing Then
                        strBodyHtmlText &= "<br>User:" & Current.Session("ContactName").ToString
                    End If
                    If Not Current.Session("UserEmail") Is Nothing Then
                        strBodyHtmlText &= "<br>UserEmail: " & Current.Session("UserEmail").ToString
                    End If
                    If Not Current.Session("CompanyName") Is Nothing Then
                        strBodyHtmlText &= Current.Session("CompanyName").ToString
                    End If
                    If Not Current.Session("DomainName") Is Nothing Then
                        strBodyHtmlText &= "<br>DomainName:" & Current.Session("DomainName").ToString
                    End If


                    strBodyHtmlText &= "<br>Site Id:" & SiteId & "<br>URL:" & strURL & "<br>Referrer:" & strReferrer & "<br>Browser:" & If(Not req Is Nothing, req.Browser.Type, "") & "<br>Browser Agent:" & strUserAgent


                    Dim objEmail As New Email
                    objEmail.SendSystemEmail(ex.Message, strBodyHtmlText, "", "noreply@bizautomation.com", ConfigurationManager.AppSettings("SendExceptionMailAddress"), "BizAutomation", "")

                End If

            Catch exNon As Exception

                strExceptionMessge = "Handler raised new exception"

            End Try
            Return strExceptionMessge
        End Function

        Public Shared Function ExceptionPublish(ByVal exceptionMessage As String, ByVal DomainID As Long, ByVal UserContactID As Long, ByVal SiteId As Long, ByVal req As System.Web.HttpRequest) As String
            Dim strExceptionMessge As String
            strExceptionMessge = String.Empty
            Try
                Dim objQueryString As New QueryStringValues
                Dim objExceptionPublisher As New ExceptionPublisher
                objExceptionPublisher.DomainID = DomainID
                objExceptionPublisher.Message = exceptionMessage
                Dim strURL As String = ""
                Dim strReferrer As String = ""
                Dim strUserAgent As String = ""
                If Not req Is Nothing Then
                    strURL = req.Url.AbsoluteUri
                    If Not req.QueryString("enc") Is Nothing Then
                        strURL = strURL + "?" + objQueryString.Decrypt(req.QueryString("enc").Trim)
                    End If

                    If (Not req.UrlReferrer Is Nothing) Then
                        strReferrer = req.UrlReferrer.PathAndQuery
                    End If

                    If Not req.UserAgent Is Nothing Then
                        strUserAgent = req.UserAgent
                    End If
                End If

                objExceptionPublisher.URL = strURL
                objExceptionPublisher.Referrer = strReferrer
                objExceptionPublisher.UserCntID = UserContactID
                objExceptionPublisher.Browser = If(Not req Is Nothing, req.Browser.Type, "")
                objExceptionPublisher.BrowserAgent = If(Not req Is Nothing, req.UserAgent & req.UserHostAddress, "")
                objExceptionPublisher.ManageException()
            Catch exNon As Exception

                strExceptionMessge = "Handler raised new exception"

            End Try
            Return strExceptionMessge
        End Function

        'Private Shared Function GetExceptionMessageBody(ByVal p_strExceptionMessage As String) As String
        '    Dim strFloatDiv As String

        '    strFloatDiv &= "<div id='FloatingLayer' style='position:absolute;width:655px;left:200px;top:100px;'>" _
        '           & "<table border='0' width='655' bgcolor='#FF6600' cellspacing='0' cellpadding='5'>" _
        '            & "<tr>" _
        '            & "<td width='100%'> <table border='0' width='100%' cellspacing='0' cellpadding='0' height='36'>" _
        '            & "<tr>" _
        '               & "<td id='titleBar'  width='100%'> Exception Message  </td>" _
        '                     & "<td style='cursor:hand' valign='top' onclick='closeMe();'> <font color='#ffffff' size='2' face='arial'  style='text-decoration:none'>X</font></a> " _
        '                    & "</td>" _
        '                  & "</tr>" _
        '                  & "<tr> " _
        '                    & "<td width='100%' bgcolor='#FFFFFF' style='padding:4px' colspan='2'> " _
        '                    & p_strExceptionMessage _
        '                   & " </td>" _
        '                  & "</tr>" _
        '                & "</table></td>" _
        '            & "</tr>" _
        '          & "</table>" _
        '        & "</div>" _
        '       & "<script language='JavaScript'> function closeMe(){document.getElementById('FloatingLayer').display='none';} </script>"
        '    Return strFloatDiv
        'End Function

        ''' <summary>
        ''' this method is made to be called from delegate where no Request and Session values doesn't retain so need to be passed through other mechanism
        ''' </summary>
        ''' <param name="ex"></param>
        ''' <param name="DomainID"></param>
        ''' <param name="UserContactID"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ExceptionPublish(ByVal ex As Exception, ByVal DomainID As Long, ByVal UserContactID As Long, ByRef SessionObject As Dictionary(Of String, Object)) As Boolean
            Try

                If ex.Message = "Thread was being aborted." Then Exit Function
                Dim objQueryString As New QueryStringValues
                Dim objExceptionPublisher As New ExceptionPublisher
                objExceptionPublisher.DomainID = DomainID
                objExceptionPublisher.Message = ex.Message & "<br>" & ex.Source & "<br>" & ex.HelpLink & "<br>" & ex.StackTrace & "<br>" & ex.TargetSite.ToString
                Dim strURL As String
                Dim strReferrer As String
                Dim strUserAgent As String

                strURL = CCommon.ToString(SessionObject("AbsolutePath"))
                strReferrer = CCommon.ToString(SessionObject("UrlReferrer"))
                strUserAgent = CCommon.ToString(SessionObject("UserAgent"))

                If CCommon.ToString(SessionObject("enc")).Length > 0 Then
                    strURL = strURL + "?" + objQueryString.Decrypt(CCommon.ToString(SessionObject("enc")).Trim)
                End If



                objExceptionPublisher.URL = strURL
                objExceptionPublisher.Referrer = strReferrer
                objExceptionPublisher.UserCntID = UserContactID
                objExceptionPublisher.Browser = CCommon.ToString(SessionObject("BrowserType"))
                objExceptionPublisher.BrowserAgent = strUserAgent & CCommon.ToString(SessionObject("UserHostAddress"))
                objExceptionPublisher.ManageException()

                If ConfigurationManager.AppSettings("SendExceptionMail") = "1" Then

                    Dim strBodyHtmlText As String
                    strBodyHtmlText = ex.Message & "<br>" & ex.Source & "<br>" & ex.HelpLink & "<br>" & ex.StackTrace & "<br>"
                    If Not ex.TargetSite Is Nothing Then
                        strBodyHtmlText &= ex.TargetSite.ToString()
                    End If
                    If Not SessionObject("ContactName") Is Nothing Then
                        strBodyHtmlText &= "<br>User:" & SessionObject("ContactName").ToString
                    End If
                    If Not SessionObject("UserEmail") Is Nothing Then
                        strBodyHtmlText &= "<br>UserEmail:" & SessionObject("UserEmail").ToString
                    End If
                    If Not SessionObject("CompanyName") Is Nothing Then
                        strBodyHtmlText &= SessionObject("CompanyName").ToString
                    End If
                    If Not SessionObject("DomainName") Is Nothing Then
                        strBodyHtmlText &= "<br>DomainName:" & SessionObject("DomainName").ToString
                    End If


                    strBodyHtmlText &= "<br>URL:" & strURL & "<br>Referrer:" & strReferrer & "<br>Browser:" & CCommon.ToString(SessionObject("BrowserType")) & "<br>Browser Agent:" & strUserAgent

                    Dim objEmail As New Email
                    objEmail.SendSystemEmail(ex.Message, strBodyHtmlText, "", "noreply@bizautomation.com", ConfigurationManager.AppSettings("SendExceptionMailAddress"), "BizAutomation", "")

                End If

                Return True
            Catch exNon As Exception

            End Try
        End Function
    End Class

End Namespace






