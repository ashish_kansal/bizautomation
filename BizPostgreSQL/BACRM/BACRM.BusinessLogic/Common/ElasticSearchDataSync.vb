﻿Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports System.Configuration
Imports Elasticsearch.Net
Imports Nest
Imports Newtonsoft.Json
Imports System.Collections.Generic

Public Class ElasticSearchDataSync
    Inherits BACRM.BusinessLogic.CBusinessBase

    Public Sub CreateItemIndexByDomain(ByVal domainID As Integer)
        Try
            Dim ds As New DataSet
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString

            Using connection As New Npgsql.NpgSqlConnection(connString)
                Dim cmd As New Npgsql.NpgsqlCommand()
                cmd.Connection = connection
                cmd.CommandType = System.Data.CommandType.StoredProcedure
                cmd.CommandText = "usp_elasticsearch_getitems"
                cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numdomainid", domainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("v_vcitemcodes", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Text))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter
                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    cmd.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In cmd.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()

                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim table As DataTable = ds.Tables(0)

                    Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                    Dim client As New ElasticClient(settings)
                    Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_item"))))

                    If result.Exists Then
                        client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_item"))))
                        client.Refresh(New RefreshRequest(Indices.All))
                    End If

                    Dim createIndexResponse = client.CreateIndex(String.Concat(domainID, "_item"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("standard").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                    If createIndexResponse.IsValid Then
                        Dim descriptor As New BulkDescriptor()

                        For Each dr As DataRow In table.Rows
                            descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_item")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                        Next

                        Dim res = client.Bulk(descriptor)
                    Else
                        Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                    End If
                End If
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub CreateOrganizationIndexByDomain(ByVal domainID As Integer)
        Try
            Dim ds As New DataSet
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString

            Using connection As New Npgsql.NpgSqlConnection(connString)
                Dim cmd As New Npgsql.NpgsqlCommand()
                cmd.Connection = connection
                cmd.CommandType = System.Data.CommandType.StoredProcedure
                cmd.CommandText = "usp_elasticsearch_getorganizations"
                cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numdomainid", domainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("v_vccompanyids", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Text))
                cmd.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                Dim sqlAdapter As Npgsql.NpgsqlDataAdapter
                Dim i As Int32 = 0
                connection.Open()
                Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                    cmd.ExecuteNonQuery()

                    For Each parm As Npgsql.NpgsqlParameter In cmd.Parameters
                        If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                            If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                ds.Tables.Add(parm.Value.ToString())
                                sqlAdapter.Fill(ds.Tables(i))
                                i += 1
                            End If
                        End If
                    Next

                    objTransaction.Commit()
                End Using
                connection.Close()


                If ds IsNot Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim table As DataTable = ds.Tables(0)

                    Dim settings As New Nest.ConnectionSettings(New Uri(ConfigurationManager.AppSettings("ElasticSearchURL")))
                    Dim client As New ElasticClient(settings)
                    Dim result = client.IndexExists(New IndexExistsRequest(Indices.Parse(String.Concat(domainID, "_organization"))))

                    If result.Exists Then
                        client.DeleteIndex(New DeleteIndexDescriptor(Indices.Parse(String.Concat(domainID, "_organization"))))
                        client.Refresh(New RefreshRequest(Indices.All))
                    End If

                    Dim createIndexResponse = client.CreateIndex(String.Concat(domainID, "_organization"), Function(c) c.Settings(Function(s) s.NumberOfShards(1).NumberOfReplicas(0).Analysis(Function(a) a.Analyzers(Function(anl) anl.Custom("default", Function(ca) ca.Tokenizer("standard").Filters(New List(Of String)() From {"lowercase"}))))).Mappings(Function(ms) ms.Map(Of Object)(Function(m) m.DynamicTemplates(Function(x) x.DynamicTemplate("test", Function(t) t.Match("Search_*").Mapping(Function(mp) mp.Text(Function(tx) tx.Index(True))))).Properties(Function(p) p.Number(Function(s) s.Name("id").Index(False)).Text(Function(s) s.Name("displaytext").Index(False)).Text(Function(s) s.Name("text").Index(False)).Text(Function(s) s.Name("url").Index(False))))))

                    If createIndexResponse.IsValid Then
                        Dim descriptor As New BulkDescriptor()

                        For Each dr As DataRow In table.Rows
                            descriptor.Index(Of Object)(Function(op) op.Index(String.Concat(domainID, "_organization")).Document(DataRowToJson(dr)).Id(dr("id").ToString()))
                        Next

                        Dim res = client.Bulk(descriptor)
                    Else
                        Throw New Exception(createIndexResponse.ServerError.[Error].ToString())
                    End If
                End If
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function DataRowToJson(datarow As DataRow) As Object
        Dim dict As New Dictionary(Of String, Object)()

        For Each col As DataColumn In datarow.Table.Columns
            dict.Add(col.ColumnName, datarow(col))
        Next

        Dim json As String = JsonConvert.SerializeObject(dict, Formatting.None, New JsonSerializerSettings() With {.ReferenceLoopHandling = ReferenceLoopHandling.Ignore})

        Return JsonConvert.DeserializeObject(Of Object)(json)
    End Function
End Class
