
Imports System.Web
'Imports System.Text.StringBuilder
'Imports System.IO.StreamWriter
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Reports
    Public Class PredefinedReports
        Inherits BACRM.BusinessLogic.CBusinessBase



#Region "Constructor"
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[administrator]	03/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Sub New()
            'Constructor
            MyBase.New()
        End Sub
#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        Private _UserRights As Integer
        Private _strTerritory As String
        Private _ReportType As Short
        Private _DealStatus As Integer
        Private _SalesPurchase As Integer
        Private _SortOrder As Integer
        Private _ChooseBizDoc As String
        Private _Customer As Integer
        Private _Division As Long
        Private _ListItem As Integer
        Private _CompanyFilter As String
        Private _firstStartDate As Date
        Private _firstEndDate As Date
        Private _secondStartDate As Date
        Private _secondEndDate As Date
        Private _compareFirst As Integer
        Private _compareSecond As Integer
        Private _campaignID As Integer
        Private _strListItem As String
        Private _caseStatus As Integer
        Private _ReportID As Integer
        Private _strLinks As String
        Private _byteMode As Short
        Private _ContactID As Long
        Private _ItemCode As Long
        Private _StartDate As Date = New Date(1753, 1, 1)
        Private _DepID As Long
        Private _CaseID As Long
        Private _ProID As Long
        Private _ClientOffsetTime As Integer

        Public Property ClientOffsetTime() As Integer
            Get
                Return _ClientOffsetTime
            End Get
            Set(ByVal Value As Integer)
                _ClientOffsetTime = Value
            End Set
        End Property


        Private _strUsers As String
        Public Property strUsers() As String
            Get
                Return _strUsers
            End Get
            Set(ByVal value As String)
                _strUsers = value
            End Set
        End Property

        Private _BizDocID As Long
        Public Property BizDocID() As Long
            Get
                Return _BizDocID
            End Get
            Set(ByVal value As Long)
                _BizDocID = value
            End Set
        End Property

        Public Property ProID() As Long
            Get
                Return _ProID
            End Get
            Set(ByVal Value As Long)
                _ProID = Value
            End Set
        End Property


        Public Property CaseID() As Long
            Get
                Return _CaseID
            End Get
            Set(ByVal Value As Long)
                _CaseID = Value
            End Set
        End Property

        Public Property DepID() As Long
            Get
                Return _DepID
            End Get
            Set(ByVal Value As Long)
                _DepID = Value
            End Set
        End Property


        Public Property StartDate() As Date
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As Date)
                _StartDate = Value
            End Set
        End Property


        Public Property ItemCode() As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal Value As Long)
                _ItemCode = Value
            End Set
        End Property


        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property


        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property


        Public Property strLinks() As String
            Get
                Return _strLinks
            End Get
            Set(ByVal Value As String)
                _strLinks = Value
            End Set
        End Property


        Public Property ReportID() As Long
            Get
                Return _ReportID
            End Get
            Set(ByVal Value As Long)
                _ReportID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the User id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the user id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private UserCntID As Long

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Territory id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the territory id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _TerritoryID As Long


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the From date.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Fromdate.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	01/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _FromDate As Date
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Todate.
        ''' </summary>
        ''' <remarks>
        '''     This holds the todate.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	01/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ToDate As Date


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	01/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Set and Gets the Date.
        ''' </summary>
        ''' <value>Returns the vlaue as integer.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Fromdate.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	01/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the To date.
        ''' </summary>
        ''' <value>Returns the value as integer.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the To date.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	01/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User id.
        ''' </summary>
        ''' <value>Returns the Territory id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the territory id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User id.
        ''' </summary>
        ''' <value>Returns the User id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the User id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	07/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property strTerritory() As String
            Get
                Return _strTerritory
            End Get
            Set(ByVal Value As String)
                _strTerritory = Value
            End Set
        End Property


        Public Property ReportType() As Short
            Get
                Return _ReportType
            End Get
            Set(ByVal Value As Short)
                _ReportType = Value
            End Set
        End Property

        Public Property UserRights() As Integer
            Get
                Return _UserRights
            End Get
            Set(ByVal Value As Integer)
                _UserRights = Value
            End Set
        End Property

        Public Property DealStatus() As Integer
            Get
                Return _DealStatus
            End Get
            Set(ByVal Value As Integer)
                _DealStatus = Value
            End Set
        End Property

        Public Property CaseStatus() As Integer
            Get
                Return _caseStatus
            End Get
            Set(ByVal Value As Integer)
                _caseStatus = Value
            End Set
        End Property

        Public Property SalesOrPurchase() As Integer
            Get
                Return _SalesPurchase
            End Get
            Set(ByVal Value As Integer)
                _SalesPurchase = Value
            End Set
        End Property

        Public Property SortOrder() As Integer
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Integer)
                _SortOrder = Value
            End Set
        End Property

        Public Property ChooseBizDoc()
            Get
                Return _ChooseBizDoc
            End Get
            Set(ByVal Value)
                _ChooseBizDoc = Value
            End Set
        End Property

        Public Property Customer()
            Get
                Return _Customer
            End Get
            Set(ByVal Value)
                _Customer = Value
            End Set
        End Property

        Public Property ListItem()
            Get
                Return _ListItem
            End Get
            Set(ByVal Value)
                _ListItem = Value
            End Set
        End Property

        Public Property Division() As Long
            Get
                Return _Division
            End Get
            Set(ByVal Value As Long)
                _Division = Value
            End Set
        End Property

        Public Property CompanyFilter()
            Get
                Return _CompanyFilter
            End Get
            Set(ByVal Value)
                _CompanyFilter = Value
            End Set
        End Property

        Public Property FirstStartDate() As Date
            Get
                Return _firstStartDate
            End Get
            Set(ByVal Value As Date)
                _firstStartDate = Value
            End Set
        End Property

        Public Property FirstEndDate() As Date
            Get
                Return _firstEndDate
            End Get
            Set(ByVal Value As Date)
                _firstEndDate = Value
            End Set
        End Property

        Public Property SecondStartDate() As Date
            Get
                Return _secondStartDate
            End Get
            Set(ByVal Value As Date)
                _secondStartDate = Value
            End Set
        End Property

        Public Property SecondEndDate() As Date
            Get
                Return _secondEndDate
            End Get
            Set(ByVal Value As Date)
                _secondEndDate = Value
            End Set
        End Property

        Public Property CompareFirst() As Integer
            Get
                Return _compareFirst
            End Get
            Set(ByVal Value As Integer)
                _compareFirst = Value
            End Set
        End Property

        Public Property CompareSecond() As Integer
            Get
                Return _compareSecond
            End Get
            Set(ByVal Value As Integer)
                _compareSecond = Value
            End Set
        End Property

        Public Property CampaignID() As Integer
            Get
                Return _campaignID
            End Get
            Set(ByVal Value As Integer)
                _campaignID = Value
            End Set
        End Property

        Public Property StrListItem() As String
            Get
                Return _strListItem
            End Get
            Set(ByVal Value As String)
                _strListItem = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get the details of top five accounts.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function is used to get the details of top five accounts.  It calls
        ''' the stored procedure with the parameters Domain id, From date, To date, User id,
        ''' Territory id, Rigthts and type.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetDealConclusion(ByVal strType As String) As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@intDealStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _DealStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                If strType = "Deal" Then
                    ds = SqlDAL.ExecuteDataset(connString, "usp_GetDealConclusion", arParms)
                Else
                    ds = SqlDAL.ExecuteDataset(connString, "usp_GetSourceConclusion", arParms)
                End If
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetTerritoriesForUsers() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ReportType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetReportTerritories", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageTerritoriesForForRept() As Boolean
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID


                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ReportType

                arParms(3) = New Npgsql.NpgsqlParameter("@strTerritory", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(3).Value = _strTerritory

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageTerritoryForRept", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetTerritoriesForForRep() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ReportType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetReportForTerritory", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAverageSalesCycle() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAverageSaleCycle", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportunityStatus() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _StartDate

                arParms(2) = New Npgsql.NpgsqlParameter("@intSalesPurchase", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _SalesPurchase

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetOpportunityStatusForReport", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportunityOpen() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connstring As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arparms(0).Value = DomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@intSalesPurchase", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arparms(1).Value = _SalesPurchase

                arparms(2) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arparms(2).Value = UserCntID

                arparms(3) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(3).Value = _ReportType

                arparms(4) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arparms(4).Value = _UserRights

                arparms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(5).Value = Nothing
                arparms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connstring, "usp_GetOpportunityOpenForReport", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetProjectStatus() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _StartDate

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _ReportType

                arParms(4) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _UserRights

                arParms(5) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _ClientOffsetTime

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetProjectStatusForReport", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetContactRole() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@intOppStatus", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _DealStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _ReportType

                arParms(6) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _UserRights

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetContactRole", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportunitySources() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetOpportunitySourcesForReport", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBestPartners() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetBestPartnersForReport", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBestAccounts() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetBestAccountsForReport", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function fn_GetDateFromNumber(ByVal lngDate As Long, ByVal strFormat As String) As String
            '================================================================================
            ' Purpose: This Function will take Date in Number format, the Date Format as 
            '           String and convert teh Number formatted date into String format supplied.
            '
            ' Parameters:
            '           lngDate = This will be the Date is number format. The Date in number 
            '                       format will always be YYYYMMDDhhmmss
            '           strFormat = This will be the Format for the Date required. 
            '                   The format can be anyone of the following:
            '           FORMAT			REPRESENTATION
            '           MM/DD/YYYY		03/12/2003
            '           MM/DD/YY		03/12/03
            '           MM-DD-YYYY		03-12-2003
            '           MM-DD-YY		03-12-03
            '           DD/MON/YYYY		12/MAR/2003
            '           DD/MON/YY		12/MAR/03
            '           DD-MON-YYYY		12-MAR-2003
            '           DD-MON-YY		12-MAR-03
            '           DD/MONTH/YYYY	12/MARCH/2003
            '           DD/MONTH/YY		12/MARCH/03
            '           DD-MONTH-YYYY	12-MARCH-2003
            '           DD-MONTH-YY		12-MARCH-03	
            '           DD/MM/YYYY		12/MAR/2003
            '           DD/MM/YY		12/03/03
            '           DD-MM-YYYY		12-03-2003
            '			DD-MM-YY		12-03-03
            '
            ' Return Value: The function returns a date in the format supplied by the above 
            '               string parameter.
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     13/03/2003          Anup Jishnu         Created
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================
            'Convert the Number format to simple string.
            Dim strGotDate As String = CStr(lngDate)
            'Set the Year in 4 digits to a variable.
            Dim str4Yr As String = Left(strGotDate, 4)
            'Set the Year in 2 Digits to a Variable
            Dim str2Yr As String = Mid(strGotDate, 3, 2)
            'Set the Month in 2 Digits to a Variable
            Dim strIntMonth As String = Mid(strGotDate, 5, 2)
            'Set the Date in 2 Digits to a Variable
            Dim strDate As String = Mid(strGotDate, 7, 2)
            'Set the Abbrivated Month Name to a Variable
            Dim str3Month As String = MonthName(strIntMonth, True)
            'Set the Full Month Name to a Variable
            Dim strFullMonth As String = MonthName(strIntMonth, False)

            'As the Date Format will be one of the above mentioned formats,
            'we need to replace the required string to get the formatted date.
            strFormat = Replace(strFormat, "DD", strDate)
            strFormat = Replace(strFormat, "YYYY", str4Yr)
            strFormat = Replace(strFormat, "YY", str2Yr)
            strFormat = Replace(strFormat, "MM", strIntMonth)
            strFormat = Replace(strFormat, "MONTH", strFullMonth)
            strFormat = Replace(strFormat, "MON", str3Month)

            'Return the formatted date.        
            fn_GetDateFromNumber = strFormat

        End Function


        Public Sub ExportToExcel(ByVal dsExcel As DataSet, ByVal strServerMapPath As String, ByVal strPath As String, ByVal strUserName As String, ByVal strUserID As String, ByVal strTblName As String, ByVal strDateFormat As String)

            Try


                If System.IO.Directory.Exists(strPath) = False Then
                    System.IO.Directory.CreateDirectory(strPath)
                End If



                Dim filename As String = strPath & "/" & strUserName & "_" & strUserID & "_" & strTblName & ".csv"

                Dim i As Integer = 0
                Dim j As Integer = 0
                Dim k As Integer = 0
                Dim countDs As Integer
                Dim oFile As System.IO.File
                Dim oWrite As System.IO.StreamWriter
                If oFile.Exists(filename) Then
                    oFile.Delete(filename)
                End If
                oWrite = oFile.CreateText(filename)
                oWrite.WriteLine("")
                'oWrite.WriteLine("Report")
                oWrite.WriteLine("              Report : " & strTblName & "       Date of Report : " & fn_GetDateFromNumber(CLng(DatePart(DateInterval.Year, Now) & Format(DatePart(DateInterval.Month, Now), "00") & Format(DatePart(DateInterval.Day, Now), "00")), strDateFormat))
                oWrite.WriteLine("")
                oWrite.WriteLine("")


                'For countDs = 0 To dsExcel.Tables(0).Rows.Count - 1
                '               If drdrCSV1.Read Then
                For k = 0 To dsExcel.Tables(0).Columns.Count - 1
                    oWrite.Write(dsExcel.Tables(0).Columns(k).ColumnName & ",")
                Next

                'Next
                '                End If
                oWrite.WriteLine("")
                oWrite.WriteLine("")
                Dim countCSV As Integer


                For countCSV = 0 To dsExcel.Tables(0).Rows.Count - 1
                    For i = 0 To dsExcel.Tables(0).Columns.Count - 1
                        ' If i = 0 Then
                        'oWrite.Write(CStr(IIf(IsDBNull(dsExcel.Tables(0).Rows(countCSV).Item(i)), "", Replace(Replace(CStr(dsExcel.Tables(0).Rows(countCSV).Item(i)), "<I>", ""), "</I>", ""))) & ",")
                        'Else
                        oWrite.Write(CStr(IIf(IsDBNull(dsExcel.Tables(0).Rows(countCSV).Item(i)), "", dsExcel.Tables(0).Rows(countCSV).Item(i))) & ",")
                        'End If
                    Next
                    oWrite.WriteLine("")
                Next
                'End While
                'ExportToCSV = a

                oWrite.Close()
            Catch ex As Exception
                'If any database error occurs then it will throw an error message
                Throw New Exception("Error occurs while fetching the  record", ex)

            End Try

        End Sub

        Public Function GetTopProducers() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetTopProducers", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocReport() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@intSortOrder", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _SortOrder

                arParms(2) = New Npgsql.NpgsqlParameter("@vcChooseBizDoc", NpgsqlTypes.NpgsqlDbType.VarChar, 30)
                arParms(2).Value = _ChooseBizDoc

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetBizDocReport", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetEcoSystemReport() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@intDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _Division

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _ReportType

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetEcosystemReport", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAsItsType() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAsItsType", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAsItsTypeForReport() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntid", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ReportType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetReportAsItsType", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageAsItsTypeForForRept() As Boolean
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID


                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ReportType

                arParms(3) = New Npgsql.NpgsqlParameter("@strTerritory", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(3).Value = _strTerritory

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAsItsTypeForRept", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCompanyName() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@intCompanyID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _Division

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCmpFilter", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _CompanyFilter

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCompany", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocType() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _BizDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = 0

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetBizDocType", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMonthToMonthComparison() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@FirstStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _firstStartDate

                arParms(3) = New Npgsql.NpgsqlParameter("@FirstEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _firstEndDate

                arParms(4) = New Npgsql.NpgsqlParameter("@SecondStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _secondStartDate

                arParms(5) = New Npgsql.NpgsqlParameter("@SecondEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = _secondEndDate

                arParms(6) = New Npgsql.NpgsqlParameter("@numCompareFirst", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _compareFirst

                arParms(7) = New Npgsql.NpgsqlParameter("@numCompareSecond", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = _compareSecond

                arParms(8) = New Npgsql.NpgsqlParameter("@tintTypeCompare", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _SortOrder

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetMonthToMonthComparison", arParms)

                Return ds

            Catch ex As Exception
                Throw ex
            End Try


        End Function

        Public Function GetCampaignForReport() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ReportType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _UserRights

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCampaignForReport", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCampaignDetailsSalesPersonel() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _UserRights

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCampaignSalesPersonel", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCampaign() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCampainNameForReport", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function ManageCampaignForForRept() As Boolean
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@strTerritory", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(1).Value = _strTerritory

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageCampaignForRept", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function




        Public Function GetCases() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@status", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _caseStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ReportCases", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMasterListItems() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcListName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = _strListItem

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMasterListItems", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetAgentForCases() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@intDateFrom", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@intDateTo", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _TerritoryID

                arParms(5) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _ReportType

                arParms(6) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _UserRights



                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAgentForCases", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function AddToMyReportList() As Boolean
            Try
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRPTId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_AddtoMyReports", arParms)


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMyReportList() As DataTable
            Try
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMyReports", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveReportsOrder() As Boolean
            Try
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@strLinks", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strLinks

                SqlDAL.ExecuteNonQuery(connString, "USP_SetMyReportsOrder", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetRepSelfServicePortal() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_RepPortal", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetRepItems() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@numSort", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _SortOrder

                arParms(6) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _Division

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_RepItems", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try



        End Function

        Public Function GetAvailablefields() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ReportFields", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageReportFields() As Boolean
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strFields", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(0).Value = _strLinks

                SqlDAL.ExecuteNonQuery(connString, "USP_RepManageFields", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetReptCasesForAgents() As DataTable
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _FromDate

                arParms(1) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _ToDate

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@intStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _caseStatus


                arParms(4) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _UserRights

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainId

                arParms(6) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _ReportType

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCaseReportForAgents", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetReptPortalConatcts() As DataTable
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _Division

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContactsLogged", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetReptBizDocsViewedByCont() As DataTable
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_RepBizDocsByCont", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRepProductRecievedorShipped() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@SortOrder", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _SortOrder

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_REpProductsReceivedOrShipped", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetRepItemDtl() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@SortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _SortOrder

                arParms(6) = New Npgsql.NpgsqlParameter("@ItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _ItemCode

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_RepItemsDtl", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetRepDealHstr() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _SortOrder

                arParms(6) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _UserRights

                arParms(7) = New Npgsql.NpgsqlParameter("@DivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _Division

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_RepDealsHstr", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetTimeAndExp() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@FromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@ToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ReportType

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ReptTimeExp", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetLeaveDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@FromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _FromDate

                arParms(3) = New Npgsql.NpgsqlParameter("@ToDate", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _ToDate

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetLeaveDetails", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetProfitLoss() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@fromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _FromDate

                arParms(1) = New Npgsql.NpgsqlParameter("@toDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _ToDate

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDepID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _DepID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ReptGrossProfit", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCaseProjectCosting() As DataTable
            Try
                Dim dt As New DataTable
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _Division

                arParms(1) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _CaseID

                arParms(2) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ProID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                dt = SqlDAL.ExecuteDataset(connString, "USP_CaseProjectCosting", arParms).Tables(0)
                Return dt
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWebSource() As DataTable
            Try
                Dim dt As New DataTable
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                dt = SqlDAL.ExecuteDataset(connString, "USP_GetWebSource", arParms).Tables(0)
                Return dt
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageUsersForForRept() As Boolean
            Try

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ReportType

                arParms(3) = New Npgsql.NpgsqlParameter("@strUsers", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(3).Value = _strUsers

                arParms(4) = New Npgsql.NpgsqlParameter("@tintUserType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _byteMode

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageUsersForRept", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetUsersForForRep() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ReportType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUsersForReport", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSalesConversionReport() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@RecordType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _SortOrder

                arParms(4) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _ContactID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _byteMode

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_SalesConversionReport", arParms)

                Return ds

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetProjectRevenueUtilization() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _FromDate

                arParms(3) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _ToDate

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetProjectRevenueUtilization", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemInventoryAndOrderdQtyReport(ByVal numItemCode As Long, ByVal intCurrentPage As Integer, ByVal intPageSize As Integer) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numCurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = intCurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@numPageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = intPageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_Item_InventoryAndOrderQty", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace