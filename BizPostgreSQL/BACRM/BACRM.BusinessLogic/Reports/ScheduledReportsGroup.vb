﻿Imports BACRM.BusinessLogic
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.CustomReports
Imports BACRM.BusinessLogic.Documents
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports ClosedXML.Excel
Imports HtmlAgilityPack
Imports Microsoft.VisualBasic
Imports Microsoft.VisualBasic.CompilerServices
Imports System
Imports System.Collections
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Data
Imports System.Diagnostics
Imports System.IO
Imports System.Linq
Imports System.Runtime.CompilerServices
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Threading.Tasks
Imports System.Web
Imports System.Data.SqlClient

Namespace BACRM.BusinessLogic.Reports
    Public Class ScheduledReportsGroup
        Inherits CBusinessBase

#Region "Public Properties"

        Public Property ID As Long
        Public Property Name As String
        Public Property Frequency As Short
        Public Property StartDate As DateTime
        Public Property EmailTemplate As Long
        Public Property SelectedTokens As String
        Public Property RecipientsEmail As String
        Public Property RecipientsContactID As String
        Public Property ClientTimeZoneOffset As Integer
        Public Property SelectedRecords As String
        Public Property DataCacheStatus As Short

#End Region

#Region "Public Methods"

        Public Function GetAll(ByVal mode As Short) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", Me.DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", Me.ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintMode", mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(getconnection.GetConnectionString, "USP_ScheduledReportsGroup_GetAll", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetByID() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", Me.DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSRGID", Me.ID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", Me.ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(getconnection.GetConnectionString, "USP_ScheduledReportsGroup_Get", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function Save() As Long
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", Me.DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", Me.UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSRGID", Me.ID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcName", Me.Name, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintFrequency", Me.Frequency, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@dtStartDate", Me.StartDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@numEmailTemplate", Me.EmailTemplate, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcRecipientsEmail", Me.SelectedTokens, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcSelectedTokens", Me.RecipientsEmail, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcReceipientsContactID", Me.RecipientsContactID, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", Me.ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return Convert.ToInt64(SqlDAL.ExecuteScalar(getconnection.GetConnectionString, "USP_ScheduledReportsGroup_Save", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub Delete()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", Me.DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcRecords", Me.SelectedRecords, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(getconnection.GetConnectionString, "USP_ScheduledReportsGroup_Delete", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub UpdateNextDate()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", Me.DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSRGID", Me.ID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(getconnection.GetConnectionString, "USP_ScheduledReportsGroup_UpdateNextDate", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub UpdateDataCacheStatus(ByVal message As String)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", Me.DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSRGID", Me.ID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintDataCacheStatus", Me.DataCacheStatus, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcExceptionMessage", message, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(getconnection.GetConnectionString, "USP_ScheduledReportsGroup_UpdateDataCacheStatus", sqlParams.ToArray())
            Catch ex As Exception
                'DO NOT THROW
            End Try
        End Sub

        Public Sub StartDataCache(ByVal isTestEmail As Boolean, ByVal mode As Short)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", Me.DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSRGID", Me.ID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintMode", mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dt As DataTable = SqlDAL.ExecuteDatable(getconnection.GetConnectionString, "USP_ScheduledReportsGroup_GetForDataCache", sqlParams.ToArray())

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If isTestEmail Then
                        Me.GenerateDataCache(True, dt.Rows(0))
                    Else
                        System.Threading.Tasks.Parallel.ForEach(dt.AsEnumerable(), Sub(dr) Me.GenerateDataCache(False, dr))
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub EmailScheduledReportsGroup(ByVal isTestEmail As Boolean)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", Me.DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSRGID", Me.ID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dt As DataTable = SqlDAL.ExecuteDatable(getconnection.GetConnectionString, "USP_ScheduledReportsGroup_GetForEmail", sqlParams.ToArray)
                If Not isTestEmail Then
                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        System.Threading.Tasks.Parallel.ForEach(dt.AsEnumerable(), Sub(dr) EmailScheduledReportsGroup(isTestEmail, dr))
                    End If
                ElseIf Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    dt.Rows(0)("tintDataCacheStatus") = 0
                    dt.AcceptChanges()
                    StartDataCache(True, 1)
                    dt.Rows(0)("tintDataCacheStatus") = 2
                    dt.AcceptChanges()
                    EmailScheduledReportsGroup(isTestEmail, dt.Rows(0))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub EmailScheduledReportsGroup(ByVal isTestEmail As Boolean, ByVal dr As DataRow)
            Dim objSRGL As New ScheduledReportsGroupLog
            objSRGL.SRGID = Convert.ToInt64(dr("ID"))
            Try
                If Convert.ToInt16(dr("tintDataCacheStatus")) = 2 Then
                    If Convert.ToBoolean(dr("bitPSMTPServer")) Then
                        Dim directoryPath As String = (ConfigurationManager.AppSettings("PortalPath") & Conversions.ToString(Convert.ToInt64(dr("numDomainID"))) & "\SRG" & Conversions.ToString(Convert.ToInt64(dr("ID"))))
                        If Not Directory.Exists(directoryPath) Then
                            Directory.CreateDirectory(directoryPath)
                        End If
                        Dim emailFileName As String = "Email.html"

                        Dim objDocumentList As New DocumentList
                        objDocumentList.GenDocID = Convert.ToInt64(dr("numEmailTemplate"))
                        objDocumentList.DomainID = Convert.ToInt64(dr("numDomainID"))
                        Dim dtDoc As DataTable = objDocumentList.GetDocByGenDocID

                        If Not dtDoc Is Nothing AndAlso dtDoc.Rows.Count > 0 Then
                            Dim subject As String = Convert.ToString(dtDoc.Rows(0)("vcSubject"))
                            Dim templateBody As String = Convert.ToString(dtDoc.Rows(0)("vcDocdesc"))

                            If File.Exists(directoryPath & "\" & emailFileName) Then
                                Dim body As String = "<div class=""row""><div class=""col-sm-12"" style=""background-color:#efefef;border-bottom:1px solid #989898;padding-left:15px;padding-right:15px;""><div style=""float:left"">"
                                If Not String.IsNullOrEmpty(Convert.ToString(dr("vcLogoForBizTheme"))) AndAlso System.IO.File.Exists(ConfigurationManager.AppSettings("PortalPath") & Conversions.ToString(Convert.ToInt64(dr("numDomainID"))) & "\" & Convert.ToString(dr("vcLogoForBizTheme"))) Then
                                    body += "<img style='max-height:60px;' src='" & ConfigurationManager.AppSettings("PortalURL").Replace("/Login.aspx", "") & "/Documents/Docs/" & Convert.ToInt64(dr("numDomainID")) & "/" & Convert.ToString(dr("vcLogoForBizTheme")) & "' />"
                                End If
                                body += "</div><div style=""text-align:right""><i style=""vertical-align: top; line-height: 38px; font-size: 14px;"">Powered By</i>&nbsp;&nbsp;<a href=""https://www.bizautomation.com/""><img itemprop=""image"" src=""https://www.bizautomation.com/images/logo.png"" alt=""BizAutomation"" /></a><br /><h6 style=""margin-top: 5px;margin-bottom: 5px;"">ONE system for your ENTIRE business</h6></div></div></div><div class=""row""><div class=""col-sm-12"" style=""padding-left:15px;padding-right:15px;""><br />" & templateBody & "<br /><br /></div></div><div class=""row""><div class=""col-sm-12"" style=""padding-left:15px;padding-right:15px;"">" & File.ReadAllText(directoryPath & "\" & emailFileName) & "</div></div>"


                                body = body.Replace("../images/", "https://login.bizautomation.com/images/")
                                body = "<html><head><style type=""text/css"">html{font-family:sans-serif;line-height:1.15;-webkit-text-size-adjust:100%;-webkit-tap-highlight-color:transparent}body{margin:0;font-family:-apple-system,BlinkMacSystemFont,""Segoe UI"",Roboto,""Helvetica Neue"",Arial,""Noto Sans"",sans-serif,""Apple Color Emoji"",""Segoe UI Emoji"",""Segoe UI Symbol"",""Noto Color Emoji"";font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background-color:transparent}a:active,a:hover{outline:0}abbr[title]{border-bottom:none;text-decoration:underline;text-decoration:underline dotted}b,strong{font-weight:700}dfn{font-style:italic}h1{font-size:2em;margin:.67em 0}mark{background:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:1em 40px}hr{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;height:0}pre{overflow:auto}code,kbd,pre,samp{font-family:monospace,monospace;font-size:1em}button,input,optgroup,select,textarea{color:inherit;font:inherit;margin:0}button{overflow:visible}button,select{text-transform:none}button,html input[type=button],input[type=reset],input[type=submit]{-webkit-appearance:button;cursor:pointer}button[disabled],html input[disabled]{cursor:default}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}input{line-height:normal}input[type=checkbox],input[type=radio]{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:0}input[type=number]::-webkit-inner-spin-button,input[type=number]::-webkit-outer-spin-button{height:auto}input[type=search]{-webkit-appearance:textfield;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}legend{border:0;padding:0}textarea{overflow:auto}optgroup{font-weight:700}table{border-collapse:collapse;border-spacing:0}td,th{padding:0}*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}:after,:before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}html{font-size:10px;-webkit-tap-highlight-color:transparent}body{font-family:""Helvetica Neue"",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff}button,input,select,textarea{font-family:inherit;font-size:inherit;line-height:inherit}a{color:#337ab7;text-decoration:none}a:focus,a:hover{color:#23527c;text-decoration:underline}a:focus{outline:5px auto -webkit-focus-ring-color;outline-offset:-2px}figure{margin:0}img{vertical-align:middle}.img-responsive{display:block;max-width:100%;height:auto}.img-rounded{border-radius:6px}.img-thumbnail{padding:4px;line-height:1.42857143;background-color:#fff;border:1px solid #ddd;border-radius:4px;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;transition:all .2s ease-in-out;display:inline-block;max-width:100%;height:auto}.img-circle{border-radius:50%}hr{margin-top:20px;margin-bottom:20px;border:0;border-top:1px solid #eee}.sr-only{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0}.sr-only-focusable:active,.sr-only-focusable:focus{position:static;width:auto;height:auto;margin:0;overflow:visible;clip:auto}[role=button]{cursor:pointer}.container{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}@media (min-width:768px){.container{width:750px}}@media (min-width:992px){.container{width:970px}}@media (min-width:1200px){.container{width:1170px}}.container-fluid{padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto}.row{margin-right:-15px;margin-left:-15px}.row-no-gutters{margin-right:0;margin-left:0}.row-no-gutters [class*=col-]{padding-right:0;padding-left:0}.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9,.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9,.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9,.col-xs-1,.col-xs-10,.col-xs-11,.col-xs-12,.col-xs-2,.col-xs-3,.col-xs-4,.col-xs-5,.col-xs-6,.col-xs-7,.col-xs-8,.col-xs-9{position:relative;min-height:1px;padding-right:15px;padding-left:15px}.col-xs-1,.col-xs-10,.col-xs-11,.col-xs-12,.col-xs-2,.col-xs-3,.col-xs-4,.col-xs-5,.col-xs-6,.col-xs-7,.col-xs-8,.col-xs-9{float:left}.col-xs-12{width:100%}.col-xs-11{width:91.66666667%}.col-xs-10{width:83.33333333%}.col-xs-9{width:75%}.col-xs-8{width:66.66666667%}.col-xs-7{width:58.33333333%}.col-xs-6{width:50%}.col-xs-5{width:41.66666667%}.col-xs-4{width:33.33333333%}.col-xs-3{width:25%}.col-xs-2{width:16.66666667%}.col-xs-1{width:8.33333333%}.col-xs-pull-12{right:100%}.col-xs-pull-11{right:91.66666667%}.col-xs-pull-10{right:83.33333333%}.col-xs-pull-9{right:75%}.col-xs-pull-8{right:66.66666667%}.col-xs-pull-7{right:58.33333333%}.col-xs-pull-6{right:50%}.col-xs-pull-5{right:41.66666667%}.col-xs-pull-4{right:33.33333333%}.col-xs-pull-3{right:25%}.col-xs-pull-2{right:16.66666667%}.col-xs-pull-1{right:8.33333333%}.col-xs-pull-0{right:auto}.col-xs-push-12{left:100%}.col-xs-push-11{left:91.66666667%}.col-xs-push-10{left:83.33333333%}.col-xs-push-9{left:75%}.col-xs-push-8{left:66.66666667%}.col-xs-push-7{left:58.33333333%}.col-xs-push-6{left:50%}.col-xs-push-5{left:41.66666667%}.col-xs-push-4{left:33.33333333%}.col-xs-push-3{left:25%}.col-xs-push-2{left:16.66666667%}.col-xs-push-1{left:8.33333333%}.col-xs-push-0{left:auto}.col-xs-offset-12{margin-left:100%}.col-xs-offset-11{margin-left:91.66666667%}.col-xs-offset-10{margin-left:83.33333333%}.col-xs-offset-9{margin-left:75%}.col-xs-offset-8{margin-left:66.66666667%}.col-xs-offset-7{margin-left:58.33333333%}.col-xs-offset-6{margin-left:50%}.col-xs-offset-5{margin-left:41.66666667%}.col-xs-offset-4{margin-left:33.33333333%}.col-xs-offset-3{margin-left:25%}.col-xs-offset-2{margin-left:16.66666667%}.col-xs-offset-1{margin-left:8.33333333%}.col-xs-offset-0{margin-left:0}@media (min-width:768px){.col-sm-1,.col-sm-10,.col-sm-11,.col-sm-12,.col-sm-2,.col-sm-3,.col-sm-4,.col-sm-5,.col-sm-6,.col-sm-7,.col-sm-8,.col-sm-9{float:left}.col-sm-12{width:100%}.col-sm-11{width:91.66666667%}.col-sm-10{width:83.33333333%}.col-sm-9{width:75%}.col-sm-8{width:66.66666667%}.col-sm-7{width:58.33333333%}.col-sm-6{width:50%}.col-sm-5{width:41.66666667%}.col-sm-4{width:33.33333333%}.col-sm-3{width:25%}.col-sm-2{width:16.66666667%}.col-sm-1{width:8.33333333%}.col-sm-pull-12{right:100%}.col-sm-pull-11{right:91.66666667%}.col-sm-pull-10{right:83.33333333%}.col-sm-pull-9{right:75%}.col-sm-pull-8{right:66.66666667%}.col-sm-pull-7{right:58.33333333%}.col-sm-pull-6{right:50%}.col-sm-pull-5{right:41.66666667%}.col-sm-pull-4{right:33.33333333%}.col-sm-pull-3{right:25%}.col-sm-pull-2{right:16.66666667%}.col-sm-pull-1{right:8.33333333%}.col-sm-pull-0{right:auto}.col-sm-push-12{left:100%}.col-sm-push-11{left:91.66666667%}.col-sm-push-10{left:83.33333333%}.col-sm-push-9{left:75%}.col-sm-push-8{left:66.66666667%}.col-sm-push-7{left:58.33333333%}.col-sm-push-6{left:50%}.col-sm-push-5{left:41.66666667%}.col-sm-push-4{left:33.33333333%}.col-sm-push-3{left:25%}.col-sm-push-2{left:16.66666667%}.col-sm-push-1{left:8.33333333%}.col-sm-push-0{left:auto}.col-sm-offset-12{margin-left:100%}.col-sm-offset-11{margin-left:91.66666667%}.col-sm-offset-10{margin-left:83.33333333%}.col-sm-offset-9{margin-left:75%}.col-sm-offset-8{margin-left:66.66666667%}.col-sm-offset-7{margin-left:58.33333333%}.col-sm-offset-6{margin-left:50%}.col-sm-offset-5{margin-left:41.66666667%}.col-sm-offset-4{margin-left:33.33333333%}.col-sm-offset-3{margin-left:25%}.col-sm-offset-2{margin-left:16.66666667%}.col-sm-offset-1{margin-left:8.33333333%}.col-sm-offset-0{margin-left:0}}@media (min-width:992px){.col-md-1,.col-md-10,.col-md-11,.col-md-12,.col-md-2,.col-md-3,.col-md-4,.col-md-5,.col-md-6,.col-md-7,.col-md-8,.col-md-9{float:left}.col-md-12{width:100%}.col-md-11{width:91.66666667%}.col-md-10{width:83.33333333%}.col-md-9{width:75%}.col-md-8{width:66.66666667%}.col-md-7{width:58.33333333%}.col-md-6{width:50%}.col-md-5{width:41.66666667%}.col-md-4{width:33.33333333%}.col-md-3{width:25%}.col-md-2{width:16.66666667%}.col-md-1{width:8.33333333%}.col-md-pull-12{right:100%}.col-md-pull-11{right:91.66666667%}.col-md-pull-10{right:83.33333333%}.col-md-pull-9{right:75%}.col-md-pull-8{right:66.66666667%}.col-md-pull-7{right:58.33333333%}.col-md-pull-6{right:50%}.col-md-pull-5{right:41.66666667%}.col-md-pull-4{right:33.33333333%}.col-md-pull-3{right:25%}.col-md-pull-2{right:16.66666667%}.col-md-pull-1{right:8.33333333%}.col-md-pull-0{right:auto}.col-md-push-12{left:100%}.col-md-push-11{left:91.66666667%}.col-md-push-10{left:83.33333333%}.col-md-push-9{left:75%}.col-md-push-8{left:66.66666667%}.col-md-push-7{left:58.33333333%}.col-md-push-6{left:50%}.col-md-push-5{left:41.66666667%}.col-md-push-4{left:33.33333333%}.col-md-push-3{left:25%}.col-md-push-2{left:16.66666667%}.col-md-push-1{left:8.33333333%}.col-md-push-0{left:auto}.col-md-offset-12{margin-left:100%}.col-md-offset-11{margin-left:91.66666667%}.col-md-offset-10{margin-left:83.33333333%}.col-md-offset-9{margin-left:75%}.col-md-offset-8{margin-left:66.66666667%}.col-md-offset-7{margin-left:58.33333333%}.col-md-offset-6{margin-left:50%}.col-md-offset-5{margin-left:41.66666667%}.col-md-offset-4{margin-left:33.33333333%}.col-md-offset-3{margin-left:25%}.col-md-offset-2{margin-left:16.66666667%}.col-md-offset-1{margin-left:8.33333333%}.col-md-offset-0{margin-left:0}}@media (min-width:1200px){.col-lg-1,.col-lg-10,.col-lg-11,.col-lg-12,.col-lg-2,.col-lg-3,.col-lg-4,.col-lg-5,.col-lg-6,.col-lg-7,.col-lg-8,.col-lg-9{float:left}.col-lg-12{width:100%}.col-lg-11{width:91.66666667%}.col-lg-10{width:83.33333333%}.col-lg-9{width:75%}.col-lg-8{width:66.66666667%}.col-lg-7{width:58.33333333%}.col-lg-6{width:50%}.col-lg-5{width:41.66666667%}.col-lg-4{width:33.33333333%}.col-lg-3{width:25%}.col-lg-2{width:16.66666667%}.col-lg-1{width:8.33333333%}.col-lg-pull-12{right:100%}.col-lg-pull-11{right:91.66666667%}.col-lg-pull-10{right:83.33333333%}.col-lg-pull-9{right:75%}.col-lg-pull-8{right:66.66666667%}.col-lg-pull-7{right:58.33333333%}.col-lg-pull-6{right:50%}.col-lg-pull-5{right:41.66666667%}.col-lg-pull-4{right:33.33333333%}.col-lg-pull-3{right:25%}.col-lg-pull-2{right:16.66666667%}.col-lg-pull-1{right:8.33333333%}.col-lg-pull-0{right:auto}.col-lg-push-12{left:100%}.col-lg-push-11{left:91.66666667%}.col-lg-push-10{left:83.33333333%}.col-lg-push-9{left:75%}.col-lg-push-8{left:66.66666667%}.col-lg-push-7{left:58.33333333%}.col-lg-push-6{left:50%}.col-lg-push-5{left:41.66666667%}.col-lg-push-4{left:33.33333333%}.col-lg-push-3{left:25%}.col-lg-push-2{left:16.66666667%}.col-lg-push-1{left:8.33333333%}.col-lg-push-0{left:auto}.col-lg-offset-12{margin-left:100%}.col-lg-offset-11{margin-left:91.66666667%}.col-lg-offset-10{margin-left:83.33333333%}.col-lg-offset-9{margin-left:75%}.col-lg-offset-8{margin-left:66.66666667%}.col-lg-offset-7{margin-left:58.33333333%}.col-lg-offset-6{margin-left:50%}.col-lg-offset-5{margin-left:41.66666667%}.col-lg-offset-4{margin-left:33.33333333%}.col-lg-offset-3{margin-left:25%}.col-lg-offset-2{margin-left:16.66666667%}.col-lg-offset-1{margin-left:8.33333333%}.col-lg-offset-0{margin-left:0}}table{background-color:transparent}table col[class*=col-]{position:static;display:table-column;float:none}table td[class*=col-],table th[class*=col-]{position:static;display:table-cell;float:none}caption{padding-top:8px;padding-bottom:8px;color:#777;text-align:left}th{text-align:left}.table-responsive{min-height:.01%;overflow-x:auto}@media screen and (max-width:767px){.table-responsive{width:100%;margin-bottom:15px;overflow-y:hidden;-ms-overflow-style:-ms-autohiding-scrollbar;border:1px solid #ddd}.table-responsive>.table{margin-bottom:0}.table-responsive>.table>tbody>tr>td,.table-responsive>.table>tbody>tr>th,.table-responsive>.table>tfoot>tr>td,.table-responsive>.table>tfoot>tr>th,.table-responsive>.table>thead>tr>td,.table-responsive>.table>thead>tr>th{white-space:nowrap}.table-responsive>.table-bordered{border:0}.table-responsive>.table-bordered>tbody>tr>td:first-child,.table-responsive>.table-bordered>tbody>tr>th:first-child,.table-responsive>.table-bordered>tfoot>tr>td:first-child,.table-responsive>.table-bordered>tfoot>tr>th:first-child,.table-responsive>.table-bordered>thead>tr>td:first-child,.table-responsive>.table-bordered>thead>tr>th:first-child{border-left:0}.table-responsive>.table-bordered>tbody>tr>td:last-child,.table-responsive>.table-bordered>tbody>tr>th:last-child,.table-responsive>.table-bordered>tfoot>tr>td:last-child,.table-responsive>.table-bordered>tfoot>tr>th:last-child,.table-responsive>.table-bordered>thead>tr>td:last-child,.table-responsive>.table-bordered>thead>tr>th:last-child{border-right:0}.table-responsive>.table-bordered>tbody>tr:last-child>td,.table-responsive>.table-bordered>tbody>tr:last-child>th,.table-responsive>.table-bordered>tfoot>tr:last-child>td,.table-responsive>.table-bordered>tfoot>tr:last-child>th{border-bottom:0}}.clearfix:after,.clearfix:before,.container-fluid:after,.container-fluid:before,.container:after,.container:before,.panel-body:after,.panel-body:before,.row:after,.row:before{display:table;content:"" ""}.clearfix:after,.container-fluid:after,.container:after,.panel-body:after,.row:after{clear:both}.center-block{display:block;margin-right:auto;margin-left:auto}.pull-right{float:right!important}.pull-left{float:left!important}.hide{display:none!important}.show{display:block!important}.invisible{visibility:hidden}.text-hide{font:0/0 a;color:transparent;text-shadow:none;background-color:transparent;border:0}.hidden{display:none!important}.affix{position:fixed}@-ms-viewport{width:device-width}.visible-lg,.visible-md,.visible-sm,.visible-xs{display:none!important}.visible-lg-block,.visible-lg-inline,.visible-lg-inline-block,.visible-md-block,.visible-md-inline,.visible-md-inline-block,.visible-sm-block,.visible-sm-inline,.visible-sm-inline-block,.visible-xs-block,.visible-xs-inline,.visible-xs-inline-block{display:none!important}@media (max-width:767px){.visible-xs{display:block!important}table.visible-xs{display:table!important}tr.visible-xs{display:table-row!important}td.visible-xs,th.visible-xs{display:table-cell!important}}@media (max-width:767px){.visible-xs-block{display:block!important}}@media (max-width:767px){.visible-xs-inline{display:inline!important}}@media (max-width:767px){.visible-xs-inline-block{display:inline-block!important}}@media (min-width:768px) and (max-width:991px){.visible-sm{display:block!important}table.visible-sm{display:table!important}tr.visible-sm{display:table-row!important}td.visible-sm,th.visible-sm{display:table-cell!important}}@media (min-width:768px) and (max-width:991px){.visible-sm-block{display:block!important}}@media (min-width:768px) and (max-width:991px){.visible-sm-inline{display:inline!important}}@media (min-width:768px) and (max-width:991px){.visible-sm-inline-block{display:inline-block!important}}@media (min-width:992px) and (max-width:1199px){.visible-md{display:block!important}table.visible-md{display:table!important}tr.visible-md{display:table-row!important}td.visible-md,th.visible-md{display:table-cell!important}}@media (min-width:992px) and (max-width:1199px){.visible-md-block{display:block!important}}@media (min-width:992px) and (max-width:1199px){.visible-md-inline{display:inline!important}}@media (min-width:992px) and (max-width:1199px){.visible-md-inline-block{display:inline-block!important}}@media (min-width:1200px){.visible-lg{display:block!important}table.visible-lg{display:table!important}tr.visible-lg{display:table-row!important}td.visible-lg,th.visible-lg{display:table-cell!important}}@media (min-width:1200px){.visible-lg-block{display:block!important}}@media (min-width:1200px){.visible-lg-inline{display:inline!important}}@media (min-width:1200px){.visible-lg-inline-block{display:inline-block!important}}@media (max-width:767px){.hidden-xs{display:none!important}}@media (min-width:768px) and (max-width:991px){.hidden-sm{display:none!important}}@media (min-width:992px) and (max-width:1199px){.hidden-md{display:none!important}}@media (min-width:1200px){.hidden-lg{display:none!important}}.visible-print{display:none!important}@media print{.visible-print{display:block!important}table.visible-print{display:table!important}tr.visible-print{display:table-row!important}td.visible-print,th.visible-print{display:table-cell!important}}.visible-print-block{display:none!important}@media print{.visible-print-block{display:block!important}}.visible-print-inline{display:none!important}@media print{.visible-print-inline{display:inline!important}}.visible-print-inline-block{display:none!important}@media print{.visible-print-inline-block{display:inline-block!important}}@media print{.hidden-print{display:none!important}}</style></head><body><div class=""container"" style=""border: 1px solid #989898;"">" & body & "</div></body></html>"

                                Dim objEmail As New Email()
                                objEmail.AdditionalMergeFields = New Hashtable()

                                Dim dtEmailBroadCast As DataTable = objEmail.GetEmailMergeData(45, Convert.ToString(dr("vcReceipientsContactID")), Convert.ToInt64(dr("numDomainID")), 0, 0, 0)

                                If body.Contains("##ContactOpt-OutLink##") Then
                                    objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                                End If

                                objEmail.AdditionalMergeFields.Add("Signature", "")

                                For Each iKey As Object In objEmail.AdditionalMergeFields.Keys
                                    If dtEmailBroadCast.Columns.Contains(iKey.ToString) = False Then
                                        dtEmailBroadCast.Columns.Add(iKey.ToString)
                                    End If
                                    'Assign values to rows in merge table
                                    For Each row As DataRow In dtEmailBroadCast.Rows
                                        If iKey.ToString() <> "ContactEmail" Then
                                            row(iKey.ToString) = CCommon.ToString(objEmail.AdditionalMergeFields(iKey))
                                        End If
                                    Next
                                Next

                                Dim dtCustomMergeFields As DataTable
                                dtCustomMergeFields = objEmail.GetCustomMergeFieldsData(45, Convert.ToInt64(dr("numDomainID")), Convert.ToString(dr("vcReceipientsContactID")), 0)
                                If (dtCustomMergeFields IsNot Nothing) Then
                                    For Each drow As DataRow In dtCustomMergeFields.Rows
                                        If body.Contains(drow("CustomField_Name").ToString()) Then
                                            body = body.Replace(drow("CustomField_Name").ToString(), drow("CustomField_Value").ToString())
                                        End If
                                    Next
                                End If

                                Dim doc As New HtmlDocument
                                doc.LoadHtml(body)

                                If Not doc.DocumentNode.SelectNodes("//table[contains(concat(' ', normalize-space(@class), ' '), ' table ')]") Is Nothing Then
                                    For Each table As HtmlNode In doc.DocumentNode.SelectNodes("//table[contains(concat(' ', normalize-space(@class), ' '), ' table ')]")
                                        If Not table.SelectNodes(".//th") Is Nothing Then
                                            For Each th As HtmlNode In table.SelectNodes(".//th")
                                                Dim existingStyle As String = Convert.ToString(th.GetAttributeValue("style", ""))
                                                th.SetAttributeValue("style", (existingStyle & If(existingStyle.Trim() = "" Or existingStyle.Trim.EndsWith(";"), "", ";") & "color:#495057;background-color:#e9ecef;border-color:#dee2e6;border:1px solid #dee2e6;border-bottom:2px solid #dee2e6;padding: .75rem;"))
                                            Next
                                        End If
                                        If Not table.SelectNodes(".//td") Is Nothing Then
                                            For Each td As HtmlNode In table.SelectNodes(".//td")
                                                Dim existingStyle As String = Convert.ToString(td.GetAttributeValue("style", ""))
                                                td.SetAttributeValue("style", (existingStyle & If(existingStyle.Trim() = "" Or existingStyle.Trim.EndsWith(";"), "", ";") & "border:1px solid #dee2e6;padding:.75rem;vertical-align: top;"))
                                            Next
                                        End If

                                        Dim tableStyle As String = Convert.ToString(table.GetAttributeValue("style", ""))
                                        table.SetAttributeValue("style", (tableStyle & If(tableStyle.Trim() = "" Or tableStyle.Trim.EndsWith(";"), "", ";") & "width:100%;max-width:100%;margin-bottom:1rem;background-color:transparent;border-collapse:collapse;border-spacing:0;"))
                                    Next
                                End If

                                If Not doc.DocumentNode.SelectNodes("//table[contains(concat(' ', normalize-space(@class), ' '), ' bg-red ')]") Is Nothing Then
                                    For Each th As HtmlNode In doc.DocumentNode.SelectNodes("//table[contains(concat(' ', normalize-space(@class), ' '), ' bg-red ')]")
                                        Dim existingStyle As String = Convert.ToString(th.GetAttributeValue("style", ""))
                                        th.SetAttributeValue("style", (existingStyle & If(existingStyle.Trim() = "" Or existingStyle.Trim.EndsWith(";"), "", ";") & "background-color:#dd4b39 !important;color:#fff;"))
                                    Next
                                End If
                                If Not doc.DocumentNode.SelectNodes("//table[contains(concat(' ', normalize-space(@class), ' '), ' bg-green ')]") Is Nothing Then
                                    For Each th As HtmlNode In doc.DocumentNode.SelectNodes("//table[contains(concat(' ', normalize-space(@class), ' '), ' bg-green ')]")
                                        Dim existingStyle As String = Convert.ToString(th.GetAttributeValue("style", ""))
                                        th.SetAttributeValue("style", (existingStyle & If(existingStyle.Trim() = "" Or existingStyle.Trim.EndsWith(";"), "", ";") & "background-color:#00a65a !important;color:#fff;"))
                                    Next
                                End If
                                If Not doc.DocumentNode.SelectNodes("//div[contains(concat(' ', normalize-space(@class), ' '), ' box ')]") Is Nothing Then
                                    For Each th As HtmlNode In doc.DocumentNode.SelectNodes("//div[contains(concat(' ', normalize-space(@class), ' '), ' box ')]")
                                        Dim existingStyle As String = Convert.ToString(th.GetAttributeValue("style", ""))
                                        th.SetAttributeValue("style", (existingStyle & If(existingStyle.Trim() = "" Or existingStyle.Trim.EndsWith(";"), "", ";") & "position:relative;border-radius:3px;background:#ffffff;border-top:3px solid #337ab7;width:100%;box-shadow:0 1px 1px rgba(0, 0, 0, 0.1);border-top-color:#337ab7;"))
                                    Next
                                End If
                                If Not doc.DocumentNode.SelectNodes("//div[contains(concat(' ', normalize-space(@class), ' '), ' box-header ')]") Is Nothing Then
                                    For Each th As HtmlNode In doc.DocumentNode.SelectNodes("//div[contains(concat(' ', normalize-space(@class), ' '), ' box-header ')]")
                                        Dim existingStyle As String = Convert.ToString(th.GetAttributeValue("style", ""))
                                        th.SetAttributeValue("style", (existingStyle & If(existingStyle.Trim() = "" Or existingStyle.Trim.EndsWith(";"), "", ";") & "color:#fff;display:block;padding:10px;position:relative;background-color:#337ab7;"))
                                    Next
                                End If
                                If Not doc.DocumentNode.SelectNodes("//h3[contains(concat(' ', normalize-space(@class), ' '), ' box-title ')]") Is Nothing Then
                                    For Each th As HtmlNode In doc.DocumentNode.SelectNodes("//h3[contains(concat(' ', normalize-space(@class), ' '), ' box-title ')]")
                                        Dim existingStyle As String = Convert.ToString(th.GetAttributeValue("style", ""))
                                        th.SetAttributeValue("style", (existingStyle & If(existingStyle.Trim() = "" Or existingStyle.Trim.EndsWith(";"), "", ";") & "margin: 0px;"))
                                    Next
                                End If
                                If Not doc.DocumentNode.SelectNodes("//div[contains(concat(' ', normalize-space(@class), ' '), ' box-body ')]") Is Nothing Then
                                    For Each th As HtmlNode In doc.DocumentNode.SelectNodes("//div[contains(concat(' ', normalize-space(@class), ' '), ' box-body ')]")
                                        Dim existingStyle As String = Convert.ToString(th.GetAttributeValue("style", ""))
                                        th.SetAttributeValue("style", (existingStyle & If(existingStyle.Trim() = "" Or existingStyle.Trim.EndsWith(";"), "", ";") & "border-top-left-radius:0;border-top-right-radius:0;border-bottom-right-radius:3px;border-bottom-left-radius:3px;padding:10px;border:1px solid #337ab7;"))
                                    Next
                                End If

                                If Not doc.DocumentNode.SelectNodes("//a") Is Nothing Then
                                    For Each th As HtmlNode In doc.DocumentNode.SelectNodes("//a")
                                        th.Attributes("href").Remove()
                                    Next
                                End If

                                body = doc.DocumentNode.OuterHtml

                                If Not String.IsNullOrEmpty(Convert.ToString(dr("vcThemeClass"))) Then
                                    body = body.Replace("#337ab7", "#" & Convert.ToString(dr("vcThemeClass")))
                                End If

                                Dim vcToEmails As String = Convert.ToString(dr("vcRecipientsEmail"))
                                If isTestEmail Then
                                    If Not HttpContext.Current Is Nothing Then
                                        vcToEmails = Conversions.ToString(HttpContext.Current.Session("UserEmail"))
                                    End If
                                End If


                                Dim listAttachments As New List(Of Tuple(Of String, String))
                                Dim objDirectoryInfo As New System.IO.DirectoryInfo(directoryPath)
                                For Each objFile As FileInfo In objDirectoryInfo.GetFiles
                                    If (objFile.Name <> "Email.html") Then
                                        listAttachments.Add(Tuple.Create(Of String, String)(objFile.Name, objFile.FullName))
                                    End If
                                Next
                                objEmail.ListAttachment = listAttachments

                                If objEmail.SendEmailWS(subject, body, "", Convert.ToString(dr("vcFromEmail")), vcToEmails, dtEmailBroadCast, Nothing, "", "", Convert.ToInt64(dr("numDomainID")), False, "", "") Then
                                    If Not isTestEmail Then
                                        Dim objSRG As New ScheduledReportsGroup
                                        objSRG.DomainID = Convert.ToInt64(dr("numDomainID"))
                                        objSRG.ID = Convert.ToInt64(dr("ID"))
                                        objSRG.UpdateNextDate()
                                        Try
                                            Dim strMessageTo As String = ""
                                            If Not String.IsNullOrEmpty(Convert.ToString(dr("vcSelectedTokens"))) Then
                                                Dim arrContacts As String() = Convert.ToString(dr("vcSelectedTokens")).Split(New Char() {"|"c})

                                                If Not arrContacts Is Nothing AndAlso arrContacts.Length > 0 Then
                                                    For Each strContact As String In arrContacts
                                                        If strContact.Split(New Char() {"~"c}).Length >= 5 Then
                                                            strMessageTo += If((strMessageTo.Length > 0), "#^#", "") & Convert.ToString(Convert.ToInt64(strContact.Split(New Char() {"~"c})(2))) & "$^$" & Convert.ToString(strContact.Split(New Char() {"~"c})(3)) & " " & Convert.ToString(strContact.Split(New Char() {"~"c})(4)) & "$^$" & Convert.ToString(strContact.Split(New Char() {"~"c})(1))
                                                        End If
                                                    Next
                                                End If
                                            End If

                                            Dim objContacts As New CContacts
                                            objContacts.strContactIDs = Convert.ToString(dr("vcReceipientsContactID"))
                                            objContacts.MessageTo = strMessageTo
                                            objContacts.MessageFrom = Conversions.ToString(Convert.ToInt64(dr("numCreatedBy"))) & "$^$" & Convert.ToString(dr("vcFromName")) & "$^$" & Convert.ToString(dr("vcFromEmail"))
                                            objContacts.MessageFromName = Convert.ToString(dr("vcFromName"))
                                            objContacts.Cc = ""
                                            objContacts.Bcc = ""
                                            objContacts.Subject = subject
                                            objContacts.Body = body
                                            objContacts.ItemId = ""
                                            objContacts.ChangeKey = ""
                                            objContacts.bitRead = False
                                            objContacts.Type = "B"
                                            objContacts.tinttype = 2
                                            objContacts.HasAttachment = True
                                            objContacts.CreatedOn = DateTime.UtcNow
                                            objContacts.MCategory = "B"
                                            objContacts.MessageFromName = ""
                                            objContacts.MessageToName = ""
                                            objContacts.CCName = ""
                                            objContacts.Size = New ASCIIEncoding().GetBytes(body).Length
                                            objContacts.AttachmentItemId = ""
                                            objContacts.AttachmentType = ""
                                            objContacts.FileName = ""
                                            objContacts.DomainID = Convert.ToInt64(dr("numDomainID"))
                                            objContacts.UserCntID = Convert.ToInt64(dr("numCreatedBy"))
                                            objContacts.BodyText = body
                                            objContacts.IsReplied = False
                                            objContacts.RepliedMailID = 0
                                            objContacts.bitInvisible = False
                                            objContacts.InsertIntoEmailHstr()
                                        Catch ex As Exception
                                            'DO NOT THROW
                                        End Try
                                    End If
                                Else
                                    If isTestEmail Then
                                        Throw New Exception("Error occured while sending email.")
                                    Else
                                        objSRGL.Exception = "Error occured while sending email."
                                        objSRGL.Save()
                                    End If
                                End If
                            Else
                                If isTestEmail Then
                                    Throw New Exception("Email report(s) html was not available.")
                                Else
                                    objSRGL.Exception = "Email report(s) html was not available."
                                    objSRGL.Save()
                                End If
                            End If
                        Else
                            If isTestEmail Then
                                Throw New Exception("Email template was not available.")
                            Else
                                objSRGL.Exception = "Email template was not available."
                                objSRGL.Save()
                            End If
                        End If
                    Else
                        If isTestEmail Then
                            Throw New Exception("Outbound SMTP was not configured in global settings -> General.")
                        Else
                            objSRGL.Exception = "Outbound SMTP was not configured in global settings -> General."
                            objSRGL.Save()
                        End If
                    End If
                Else
                    If isTestEmail Then
                        Throw New Exception("Data cache was not available.")
                    Else
                        objSRGL.Exception = "Data cache was not available."
                        objSRGL.Save()
                    End If
                End If
            Catch ex As Exception
                If isTestEmail Then
                    Throw
                Else
                    objSRGL.Exception = ex.Message
                    objSRGL.Save()
                End If
            End Try
        End Sub

        Public Sub GenerateDataCache(ByVal isTestEmail As Boolean, ByVal dr As DataRow)
            Try
                Dim path As String = (ConfigurationManager.AppSettings("PortalPath") & Conversions.ToString(Convert.ToInt64(dr("numDomainID"))) & "\SRG" & Conversions.ToString(Convert.ToInt64(dr("ID"))))
                If Not Directory.Exists(path) Then
                    Directory.CreateDirectory(path)
                Else
                    Dim objDirectoryInfo As New DirectoryInfo(path)
                    For Each objFile As FileInfo In objDirectoryInfo.GetFiles
                        objFile.Delete()
                    Next
                End If
                File.WriteAllText((path & "\Email.html"), "")

                Dim objSRGR As New ScheduledReportsGroupReports
                objSRGR.DomainID = Convert.ToInt64(dr("numDomainID"))
                Dim dtReports As DataTable = objSRGR.GetSRGReports(3, Convert.ToInt64(dr("ID")), 0, 0, "", 0, 0)

                If Not dtReports Is Nothing AndAlso dtReports.Rows.Count > 0 Then
                    For Each drReport As DataRow In dtReports.Rows
                        If (Convert.ToInt16(drReport("tintReportType")) = 1) Then
                            Me.GenerateCustomReportCache(Convert.ToInt64(dr("numDomainID")), Convert.ToInt32(dr("intTimeZoneOffset")), drReport)
                        End If
                        If (Convert.ToInt16(drReport("tintReportType")) = 2) Then
                            Me.GeneratePredefinedReportCache(Convert.ToInt64(dr("numDomainID")), Convert.ToInt32(dr("intTimeZoneOffset")), drReport)
                        End If
                        If ((Convert.ToInt16(drReport("tintReportType")) = 4) Or (Convert.ToInt16(drReport("tintReportType")) = 5)) Then
                            Me.GenerateSaveSearchCache(Convert.ToInt64(dr("numDomainID")), Convert.ToInt32(dr("intTimeZoneOffset")), drReport)
                        End If
                    Next
                End If

                Dim objSRG As New ScheduledReportsGroup()
                objSRG.DomainID = Convert.ToInt64(dr("numDomainID"))
                objSRG.ID = Convert.ToInt64(dr("ID"))
                objSRG.DataCacheStatus = 2
                objSRG.UpdateDataCacheStatus("")
            Catch exception1 As Exception
                If isTestEmail Then
                    Throw
                Else
                    Dim objSRG As New ScheduledReportsGroup()
                    objSRG.DomainID = Convert.ToInt64(dr("numDomainID"))
                    objSRG.ID = Convert.ToInt64(dr("ID"))
                    objSRG.DataCacheStatus = 0
                    objSRG.UpdateDataCacheStatus(exception1.Message)
                End If
            End Try
        End Sub

        Private Sub GenerateCustomReportCache(ByVal lngDomainID As Long, ByVal intTimeZoneOffset As Integer, ByVal dr As DataRow)
            Try
                Dim fileName As String = ""
                Dim isFileGenerated As Boolean = False
                Dim emailFilename As String = "Email.html"
                Dim directoryPath As String = (ConfigurationManager.AppSettings("PortalPath") & Conversions.ToString(lngDomainID) & "\SRG" & Conversions.ToString(Convert.ToInt64(dr("numSRGID"))))
                If Not Directory.Exists(directoryPath) Then
                    Directory.CreateDirectory(directoryPath)
                End If

                Dim reportHtml As String = "<div style=""margin-bottom:20px;background-color:#fff;border:1px solid #337ab7;border-radius:4px;-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);box-shadow:0 1px 1px rgba(0,0,0,.05);""><div style=""color:#fff;background-color:#337ab7;border-color:#337ab7;padding:10px 15px;border-bottom: 1px solid transparent;border-top-left-radius: 3px;border-top-right-radius: 3px;""><h3 class='box-title'>" & Convert.ToString(dr("vcReportName")) & "</h3></div><div style='padding:15px;'><table class='table table-responsive table-bordered'>"
                Dim objReportManage As New CustomReportsManage
                objReportManage.ReportID = Convert.ToInt64(dr("numReportID"))
                objReportManage.DomainID = lngDomainID
                objReportManage.ClientTimeZoneOffset = intTimeZoneOffset
                Dim ds As DataSet = objReportManage.GetReportListMasterDetail

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim objReport As ReportObject = objReportManage.GetDatasetToReportObject(ds)

                    Dim dsColumnList As DataSet
                    objReportManage.IsReportRun = True
                    dsColumnList = objReportManage.GetReportModuleGroupFieldMaster

                    objReportManage.textQuery = objReportManage.GetReportQuery(objReportManage, ds, objReport, False, True, dsColumnList, False, 0)
                    Dim dtData As DataTable = objReportManage.USP_ReportQueryExecute

                    If Not dtData Is Nothing AndAlso dtData.Rows.Count > 0 Then
                        Try
                            reportHtml += objReportManage.GetCustomReportHtml(ds, dsColumnList, dtData.AsEnumerable().Take(20).CopyToDataTable(), objReport, True)
                        Catch ex As Exception
                            ExceptionModule.ExceptionPublish(ex, lngDomainID, 0, 0, Nothing)
                            reportHtml += "<tr><td>Error ocurred while rendering this report</td></tr>"
                        End Try

                        If Not CCommon.ToInteger(ds.Tables(0).Rows(0)("tintReportType")) = 0 Then
                            fileName = Convert.ToString(dr("vcReportName")) & ".xlsx"
                            Dim html As String = ("<table class='table table-responsive table-bordered'>" & objReportManage.GetCustomReportHtml(ds, dsColumnList, dtData, objReport, True) & "</table>")
                            Dim fs As New MemoryStream()
                            Dim workbook As New XLWorkbook
                            Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Report Data")

                            Dim doc As New HtmlAgilityPack.HtmlDocument
                            doc.LoadHtml(reportHtml)

                            Dim i As Int32 = 1
                            Dim j As Int32 = 1
                            Dim nodeCollection As HtmlAgilityPack.HtmlNodeCollection
                            Dim htmlBody As HtmlNode = doc.DocumentNode.SelectSingleNode("//table//tr")
                            Dim TotalRecord As HtmlNode = HtmlNode.CreateNode("")

                            If reportHtml.Contains("Total Records") Then
                                Dim nodeTotalRecord As HtmlNode = HtmlNode.CreateNode("<th>Total Records</th>")
                                htmlBody.AppendChild(nodeTotalRecord)
                            End If

                            For Each row As HtmlAgilityPack.HtmlNode In doc.DocumentNode.SelectNodes("//table//tr")
                                Dim isTdcollection As Boolean
                                If Not row.SelectNodes("th") Is Nothing Then
                                    nodeCollection = row.SelectNodes("th")
                                Else
                                    nodeCollection = row.SelectNodes("td")
                                    isTdcollection = True
                                End If

                                If reportHtml.Contains("Total Records") And isTdcollection Then
                                    For Each node As HtmlNode In nodeCollection
                                        If node.InnerHtml.Contains("Total Records") Then
                                            Dim recordOwnerName As String = node.InnerHtml.Split(New Char() {":"c})(1)
                                            recordOwnerName = recordOwnerName.Remove(recordOwnerName.Length - 1, 1)
                                            TotalRecord = HtmlNode.CreateNode(recordOwnerName)
                                        End If
                                    Next
                                    nodeCollection.Add(TotalRecord)
                                End If

                                For Each col As HtmlAgilityPack.HtmlNode In nodeCollection
                                    If Double.TryParse(col.InnerText, Nothing) Then
                                        workSheet.Cell(i, j).SetValue(col.InnerText).SetDataType(XLCellValues.Number)
                                    ElseIf IsDate(col.InnerText) Then
                                        workSheet.Cell(i, j).SetValue(col.InnerText).SetDataType(XLCellValues.DateTime)
                                    Else
                                        workSheet.Cell(i, j).SetValue(col.InnerText).SetDataType(XLCellValues.Text)
                                        If isTdcollection Then
                                            If col.InnerText.Contains("Total Records") Then

                                                workSheet.Cell(i, j).SetValue(Regex.Replace(col.InnerText, "\([^)]*\)", "").Replace("&nbsp;", "").Trim()).SetDataType(XLCellValues.Text)
                                            Else
                                                workSheet.Cell(i, j).SetValue(col.InnerText).SetDataType(XLCellValues.Text)
                                            End If

                                        End If
                                    End If

                                    If i = 1 Then
                                        workSheet.Cell(i, j).Style.Font.Bold = True
                                        workSheet.Cell(i, j).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                                    End If

                                    j = j + 1
                                Next
                                i = i + 1
                                j = 1
                            Next
                            workSheet.Columns.AdjustToContents()

                            workbook.SaveAs(directoryPath & "\" & fileName)
                            isFileGenerated = True
                        Else
                            Dim stringWrite As IO.StringWriter = New System.IO.StringWriter()
                            fileName = Convert.ToString(dr("vcReportName")) & ".csv"

                            Dim listColumns As New List(Of DataColumn)
                            For Each column As DataColumn In dtData.Columns
                                If objReport.ColumnList.Contains(column.ColumnName) Then
                                    Dim strColumn As String() = column.ColumnName.Split(New Char() {"_"c})

                                    Dim arrDr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                                    If arrDr.Length > 0 Then
                                        column.ColumnName = CCommon.ToString(arrDr(0)("vcFieldname"))
                                    End If
                                Else
                                    listColumns.Add(column)
                                End If
                            Next

                            For Each column As DataColumn In listColumns
                                dtData.Columns.Remove(column)
                            Next

                            CSVExport.ProduceCSV(dtData, stringWrite, True)

                            File.WriteAllText((directoryPath & "\" & fileName), stringWrite.ToString())
                            isFileGenerated = True
                        End If
                    Else
                        reportHtml += "<tr><td>No data available</td></tr>"
                    End If
                End If

                reportHtml += "</table></div>"
                If isFileGenerated Then
                    reportHtml += "<div style=""padding: 10px 15px;color: #fff;background-color: #337ab7;border-color: #337ab7;border-top: 1px solid transparent;border-bottom-right-radius: 3px;border-bottom-left-radius: 3px;text-align:right;""><div class='form-inline' style='text-align:right'><label>For full report check attachement:</label>&nbsp;" & fileName & "</div></div>"
                End If
                reportHtml += "</div>"

                If Not File.Exists(directoryPath & "\" & emailFilename) Then
                    File.WriteAllText(directoryPath & "\" & emailFilename, reportHtml)
                Else
                    File.AppendAllText(directoryPath & "\" & emailFilename, reportHtml)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub GeneratePredefinedReportCache(ByVal lngDomainID As Long, ByVal intTimeZoneOffset As Integer, ByVal dr As DataRow)
            Try
                Dim objReport As New CustomReportsManage()
                objReport.DomainID = lngDomainID
                objReport.UserCntID = 0
                objReport.ClientTimeZoneOffset = intTimeZoneOffset
                objReport.DefaultReportID = CInt(Convert.ToInt64(dr("intDefaultReportID")))
                objReport.ReportID = CInt(Convert.ToInt64(dr("numReportID")))
                objReport.DashBoardID = CInt(Convert.ToInt64(dr("numDashBoardID")))
                Dim html As String = objReport.GetPrebuildReprtHtml(True)
                html += "<br />"
                Dim directoryPath As String = (ConfigurationManager.AppSettings("PortalPath") & Conversions.ToString(lngDomainID) & "\SRG" & Conversions.ToString(Convert.ToInt64(dr("numSRGID"))))
                If Not Directory.Exists(directoryPath) Then
                    Directory.CreateDirectory(directoryPath)
                End If

                If Not File.Exists(directoryPath & "\" & "Email.html") Then
                    File.WriteAllText(directoryPath & "\" & "Email.html", html)
                Else
                    File.AppendAllText(directoryPath & "\" & "Email.html", html)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub GenerateSaveSearchCache(ByVal lngDomainID As Long, ByVal intTimeZoneOffset As Integer, ByVal dr As DataRow)
            Try
                Dim isFileGenerated As Boolean = False
                Dim attachmentFileName As String = ""
                Dim emailFileName As String = "Email.html"
                Dim directoryPath As String = (ConfigurationManager.AppSettings("PortalPath") & Conversions.ToString(lngDomainID) & "\SRG" & Conversions.ToString(Convert.ToInt64(dr("numSRGID"))))
                If Not Directory.Exists(directoryPath) Then
                    Directory.CreateDirectory(directoryPath)
                End If

                Dim reportHtml As String = ("<div style=""margin-bottom:20px;background-color:#fff;border:1px solid #337ab7;border-radius:4px;-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);box-shadow:0 1px 1px rgba(0,0,0,.05);""><div style=""color:#fff;background-color:#337ab7;border-color:#337ab7;padding:10px 15px;border-bottom: 1px solid transparent;border-top-left-radius: 3px;border-top-right-radius: 3px;""><h3 class='box-title'>" & Convert.ToString(dr("vcReportName")) & "</h3></div><div style='padding:15px;'><table class='table table-responsive table-bordered'>")

                If (Convert.ToInt64(Convert.ToInt64(dr("numFormID"))) = 1) Then
                    attachmentFileName = Convert.ToString(dr("vcReportName")) & ".xlsx"
                    Dim objFormGenericAdvSearch As New FormGenericAdvSearch()
                    objFormGenericAdvSearch.byteMode = 1
                    objFormGenericAdvSearch.SearchID = Convert.ToInt64(dr("numReportID"))
                    objFormGenericAdvSearch.FormID = Convert.ToInt64(dr("numFormID"))
                    objFormGenericAdvSearch.DomainID = lngDomainID
                    objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                    Dim dtResult As DataTable = objFormGenericAdvSearch.ManageSavedSearch

                    If Not dtResult Is Nothing AndAlso dtResult.Rows.Count > 0 Then
                        Dim strSearchConditions As String = ""
                        Dim strTimeConditions As String = ""

                        If Not String.IsNullOrEmpty(Convert.ToString(dr("vcSearchConditionJson"))) Then
                            Dim obj As New FormGenericAdvSearch
                            strSearchConditions = obj.SearchOppOrders(lngDomainID, intTimeZoneOffset, Convert.ToString(dr("vcSearchConditionJson")))
                        Else
                            strSearchConditions = Convert.ToString(dtResult.Rows(0)("vcSearchQuery"))
                        End If

                        If Not String.IsNullOrEmpty(Convert.ToString(dtResult.Rows(0)("vcTimeExpression"))) Then
                            strTimeConditions = CCommon.ToString(dtResult.Rows(0)("vcTimeExpression")).Replace("{FROM}", DateTime.Today.AddDays(CDbl((0 - CCommon.ToInteger(dtResult.Rows(0)("intSlidingDays"))))).ToShortDateString).Replace("{TO}", Conversions.ToString(Conversions.ToDate((DateTime.Today.ToShortDateString & " 23:59:59"))))
                        End If

                        objFormGenericAdvSearch = New FormGenericAdvSearch
                        objFormGenericAdvSearch.QueryWhereCondition = (strSearchConditions & strTimeConditions)
                        objFormGenericAdvSearch.AreasOfInt = ""
                        objFormGenericAdvSearch.DomainID = lngDomainID
                        objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.UserRightType = 3
                        objFormGenericAdvSearch.FormID = 1
                        objFormGenericAdvSearch.DisplayColumns = Convert.ToString(dr("vcDisplayColumns"))
                        objFormGenericAdvSearch.SortCharacter = "a"c
                        objFormGenericAdvSearch.SortcolumnName = "DM.bintCreatedDate"
                        objFormGenericAdvSearch.SortcolumnName = objFormGenericAdvSearch.SortcolumnName.Replace("cmp", "c")
                        objFormGenericAdvSearch.GetAll = True
                        objFormGenericAdvSearch.columnSortOrder = ""
                        objFormGenericAdvSearch.columnSearch = ""
                        objFormGenericAdvSearch.columnName = ""
                        objFormGenericAdvSearch.RegularSearchCriteria = ""
                        objFormGenericAdvSearch.CustomSearchCriteria = ""
                        objFormGenericAdvSearch.ClientTimeZoneOffset = intTimeZoneOffset
                        Dim ds As DataSet = objFormGenericAdvSearch.AdvancedSearch()

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            Dim dtResults As DataTable = ds.Tables(0)
                            Dim iExportCount As Integer

                            Dim fs As New MemoryStream()
                            Dim workbook As New XLWorkbook
                            Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Data")

                            Dim k As Int32 = 1
                            Dim n As Int32 = 1
                            Dim columnIndex As Int32 = 1

                            reportHtml += "<tr>"
                            For Each column As DataColumn In dtResults.Columns
                                If column.ColumnName.Contains("~") Then
                                    If column.ColumnName.StartsWith("Cust") AndAlso column.ColumnName.EndsWith("~1") Then
                                        Dim arrRows As DataRow() = ds.Tables(1).Select("vcDbColumnName='" & CCommon.ToString(column.ColumnName.Split("~")(0)) & "' AND numFieldID=" & CCommon.ToLong(column.ColumnName.Split("~")(1)) & " AND bitCustomField=1")

                                        If Not arrRows Is Nothing AndAlso arrRows.Count > 0 Then
                                            If dtResults.Columns.Contains(CCommon.ToString(arrRows(0)("vcFieldName"))) Then
                                                column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName")) & n.ToString()
                                            Else
                                                column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName"))
                                            End If
                                        End If
                                    Else
                                        Dim arrRows As DataRow() = ds.Tables(1).Select("vcDbColumnName='" & CCommon.ToString(column.ColumnName.Split("~")(0)) & "' AND numFieldID=" & CCommon.ToLong(column.ColumnName.Split("~")(1)) & " AND bitCustomField=0")

                                        If Not arrRows Is Nothing AndAlso arrRows.Count > 0 Then
                                            column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName"))
                                        End If
                                    End If

                                    reportHtml += "<th>" & column.ColumnName & "</th>"
                                    workSheet.Cell(k, n).SetValue(column.ColumnName).SetDataType(XLCellValues.Text)
                                    workSheet.Cell(k, n).Style.Font.Bold = True
                                    workSheet.Cell(k, n).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

                                    n = n + 1
                                Else
                                    column.ColumnName = "RemovedColumn" & columnIndex
                                End If

                                columnIndex += 1
                            Next
                            reportHtml += "</tr>"

                            k = k + 1
                            n = 1
                            Dim rowCount As Int16 = 1

                            For Each drResult As DataRow In dtResults.Rows
                                If rowCount <= 10 Then
                                    reportHtml += "<tr>"
                                End If

                                For Each column As DataColumn In dtResults.Columns
                                    If Not column.ColumnName.StartsWith("RemovedColumn") Then
                                        If Double.TryParse(CCommon.ToString(drResult(column.ColumnName)), Nothing) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Number)
                                        ElseIf IsDate(CCommon.ToString(drResult(column.ColumnName))) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.DateTime)
                                        Else
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Text)
                                        End If

                                        If rowCount <= 10 Then
                                            reportHtml += "<td>" & CCommon.ToString(drResult(column.ColumnName)) & "</td>"
                                        End If

                                        n = n + 1
                                    End If
                                Next

                                If rowCount <= 10 Then
                                    reportHtml += "</tr>"
                                End If

                                k = k + 1
                                n = 1
                                rowCount += 1
                            Next

                            workSheet.Columns.AdjustToContents()

                            If Not Directory.Exists(directoryPath) Then
                                Directory.CreateDirectory(directoryPath)
                            End If
                            If File.Exists((directoryPath & "\" & attachmentFileName)) Then
                                File.Delete((directoryPath & "\" & attachmentFileName))
                            End If
                            workbook.SaveAs((directoryPath & "\" & attachmentFileName))
                            isFileGenerated = True
                        Else
                            reportHtml += "<tr><td>No data available</td></tr>"
                        End If
                    Else
                        reportHtml += "<tr><td>No data available</td></tr>"
                    End If
                ElseIf (Convert.ToInt64(Convert.ToInt64(dr("numFormID"))) = 15) Then
                    attachmentFileName = (Convert.ToString(dr("vcReportName")) & ".xlsx")
                    Dim strSearchConditions As String = ""
                    Dim strTimeConditions As String = ""

                    Dim objFormGenericAdvSearch As New FormGenericAdvSearch()
                    objFormGenericAdvSearch.byteMode = 1
                    objFormGenericAdvSearch.SearchID = Convert.ToInt64(dr("numReportID"))
                    objFormGenericAdvSearch.FormID = Convert.ToInt64(dr("numFormID"))
                    objFormGenericAdvSearch.DomainID = lngDomainID
                    objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                    Dim dtSearch As DataTable = objFormGenericAdvSearch.ManageSavedSearch

                    If Not dtSearch Is Nothing AndAlso dtSearch.Rows.Count > 0 Then
                        strSearchConditions = If(Not String.IsNullOrEmpty(Convert.ToString(dr("vcSearchConditionJson"))), New FormGenericAdvSearch().SearchOppOrders(lngDomainID, intTimeZoneOffset, Convert.ToString(dr("vcSearchConditionJson"))), Convert.ToString(dtSearch.Rows(0)("vcSearchQuery")))
                        If (Convert.ToString(dtSearch.Rows(0)("vcTimeExpression")) <> "") Then
                            strTimeConditions = CCommon.ToString(dtSearch.Rows(0)("vcTimeExpression")).Replace("{FROM}", DateTime.Today.AddDays(CDbl((0 - CCommon.ToInteger(dtSearch.Rows(0)("intSlidingDays"))))).ToShortDateString).Replace("{TO}", Conversions.ToString(Conversions.ToDate((DateTime.Today.ToShortDateString & " 23:59:59"))))
                        End If
                        objFormGenericAdvSearch = New FormGenericAdvSearch
                        objFormGenericAdvSearch.QueryWhereCondition = (strSearchConditions & strTimeConditions)
                        objFormGenericAdvSearch.DomainID = lngDomainID
                        objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.FormID = 15
                        objFormGenericAdvSearch.DisplayColumns = Convert.ToString(dr("vcDisplayColumns"))
                        objFormGenericAdvSearch.SortcolumnName = ""
                        objFormGenericAdvSearch.CurrentPage = 0
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.TotalRecords = 0
                        objFormGenericAdvSearch.columnSortOrder = "Asc"
                        objFormGenericAdvSearch.GetAll = True
                        objFormGenericAdvSearch.columnSortOrder = ""
                        objFormGenericAdvSearch.columnSearch = ""
                        objFormGenericAdvSearch.columnName = ""
                        objFormGenericAdvSearch.RegularSearchCriteria = ""
                        objFormGenericAdvSearch.CustomSearchCriteria = ""
                        objFormGenericAdvSearch.ClientTimeZoneOffset = intTimeZoneOffset
                        Dim ds As DataSet = objFormGenericAdvSearch.AdvancedSearchOpp

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            Dim dtResult As DataTable = ds.Tables(0)

                            Dim fs As New MemoryStream()
                            Dim workbook As New XLWorkbook
                            Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Data")

                            Dim k As Int32 = 1
                            Dim n As Int32 = 1
                            Dim columnIndex As Int32 = 1

                            reportHtml += "<tr>"
                            For Each column As DataColumn In dtResult.Columns
                                If column.ColumnName.Contains("~") Then
                                    If column.ColumnName.StartsWith("Cust") AndAlso column.ColumnName.EndsWith("~1") Then
                                        Dim arrRows As DataRow() = ds.Tables(1).Select("vcDbColumnName='" & CCommon.ToString(column.ColumnName.Split("~")(0)) & "' AND numFieldID=" & CCommon.ToLong(column.ColumnName.Split("~")(1)) & " AND bitCustomField=1")

                                        If Not arrRows Is Nothing AndAlso arrRows.Count > 0 Then
                                            If dtResult.Columns.Contains(CCommon.ToString(arrRows(0)("vcFieldName"))) Then
                                                column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName")) & n.ToString()
                                            Else
                                                column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName"))
                                            End If
                                        End If
                                    Else
                                        Dim arrRows As DataRow() = ds.Tables(1).Select("vcDbColumnName='" & CCommon.ToString(column.ColumnName.Split("~")(0)) & "' AND numFieldID=" & CCommon.ToLong(column.ColumnName.Split("~")(1)) & " AND bitCustomField=0")

                                        If Not arrRows Is Nothing AndAlso arrRows.Count > 0 Then
                                            column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName"))
                                        End If
                                    End If

                                    reportHtml += "<th>" & column.ColumnName & "</th>"
                                    workSheet.Cell(k, n).SetValue(column.ColumnName).SetDataType(XLCellValues.Text)
                                    workSheet.Cell(k, n).Style.Font.Bold = True
                                    workSheet.Cell(k, n).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

                                    n = n + 1
                                Else
                                    column.ColumnName = "RemovedColumn" & columnIndex
                                End If

                                columnIndex += 1
                            Next
                            reportHtml += "</tr>"

                            k = k + 1
                            n = 1
                            Dim rowCount As Int16 = 1

                            For Each drResult As DataRow In dtResult.Rows
                                If rowCount <= 10 Then
                                    reportHtml += "<tr>"
                                End If

                                For Each column As DataColumn In dtResult.Columns
                                    If Not column.ColumnName.StartsWith("RemovedColumn") Then
                                        If Double.TryParse(CCommon.ToString(drResult(column.ColumnName)), Nothing) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Number)
                                        ElseIf IsDate(CCommon.ToString(drResult(column.ColumnName))) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.DateTime)
                                        Else
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Text)
                                        End If

                                        If rowCount <= 10 Then
                                            reportHtml += "<td>" & CCommon.ToString(drResult(column.ColumnName)) & "</td>"
                                        End If

                                        n = n + 1
                                    End If
                                Next

                                If rowCount <= 10 Then
                                    reportHtml += "</tr>"
                                End If

                                k = k + 1
                                n = 1
                                rowCount += 1
                            Next

                            workSheet.Columns.AdjustToContents()

                            If Not Directory.Exists(directoryPath) Then
                                Directory.CreateDirectory(directoryPath)
                            End If
                            If File.Exists((directoryPath & "\" & attachmentFileName)) Then
                                File.Delete((directoryPath & "\" & attachmentFileName))
                            End If
                            workbook.SaveAs((directoryPath & "\" & attachmentFileName))
                            isFileGenerated = True
                        Else
                            reportHtml += "<tr><td>No data available</td></tr>"
                        End If
                    Else
                        reportHtml += "<tr><td>No data available</td></tr>"
                    End If
                ElseIf (Convert.ToInt64(Convert.ToInt64(dr("numFormID"))) = 29) Then
                    attachmentFileName = Convert.ToString(dr("vcReportName")) & ".xlsx"
                    Dim objFormGenericAdvSearch As New FormGenericAdvSearch()
                    objFormGenericAdvSearch.byteMode = 1
                    objFormGenericAdvSearch.SearchID = Convert.ToInt64(dr("numReportID"))
                    objFormGenericAdvSearch.FormID = Convert.ToInt64(dr("numFormID"))
                    objFormGenericAdvSearch.DomainID = lngDomainID
                    objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                    Dim dtSearch As DataTable = objFormGenericAdvSearch.ManageSavedSearch

                    If Not dtSearch Is Nothing AndAlso dtSearch.Rows.Count > 0 Then
                        Dim strSearchConditions As String = If(Not String.IsNullOrEmpty(Convert.ToString(dr("vcSearchConditionJson"))), New FormGenericAdvSearch().SearchItems(lngDomainID, intTimeZoneOffset, Convert.ToString(dr("vcSearchConditionJson"))), Convert.ToString(dtSearch.Rows(0)("vcSearchQuery")))
                        objFormGenericAdvSearch = New FormGenericAdvSearch
                        objFormGenericAdvSearch.QueryWhereCondition = strSearchConditions
                        objFormGenericAdvSearch.DomainID = lngDomainID
                        objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.FormID = 29
                        objFormGenericAdvSearch.DisplayColumns = Convert.ToString(dr("vcDisplayColumns"))
                        objFormGenericAdvSearch.SortcolumnName = ""
                        objFormGenericAdvSearch.CurrentPage = 0
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.TotalRecords = 0
                        objFormGenericAdvSearch.columnSortOrder = "Asc"
                        objFormGenericAdvSearch.GetAll = True
                        objFormGenericAdvSearch.columnSortOrder = ""
                        objFormGenericAdvSearch.columnSearch = ""
                        objFormGenericAdvSearch.columnName = ""
                        objFormGenericAdvSearch.RegularSearchCriteria = ""
                        objFormGenericAdvSearch.CustomSearchCriteria = ""
                        objFormGenericAdvSearch.ClientTimeZoneOffset = intTimeZoneOffset
                        Dim ds As DataSet = objFormGenericAdvSearch.AdvancedSearchItems

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            Dim dtResult As DataTable = ds.Tables(0)

                            Dim fs As New MemoryStream()
                            Dim workbook As New XLWorkbook
                            Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Data")

                            Dim k As Int32 = 1
                            Dim n As Int32 = 1
                            Dim columnIndex As Int32 = 1

                            reportHtml += "<tr>"
                            For Each column As DataColumn In dtResult.Columns
                                If column.ColumnName.Contains("~") Then
                                    If column.ColumnName.StartsWith("Cust") AndAlso column.ColumnName.EndsWith("~1") Then
                                        Dim arrRows As DataRow() = ds.Tables(1).Select("vcDbColumnName='" & CCommon.ToString(column.ColumnName.Split("~")(0)) & "' AND numFieldID=" & CCommon.ToLong(column.ColumnName.Split("~")(1)) & " AND bitCustomField=1")

                                        If Not arrRows Is Nothing AndAlso arrRows.Count > 0 Then
                                            If dtResult.Columns.Contains(CCommon.ToString(arrRows(0)("vcFieldName"))) Then
                                                column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName")) & n.ToString()
                                            Else
                                                column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName"))
                                            End If
                                        End If
                                    Else
                                        Dim arrRows As DataRow() = ds.Tables(1).Select("vcDbColumnName='" & CCommon.ToString(column.ColumnName.Split("~")(0)) & "' AND numFieldID=" & CCommon.ToLong(column.ColumnName.Split("~")(1)) & " AND bitCustomField=0")

                                        If Not arrRows Is Nothing AndAlso arrRows.Count > 0 Then
                                            column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName"))
                                        End If
                                    End If

                                    reportHtml += "<th>" & column.ColumnName & "</th>"
                                    workSheet.Cell(k, n).SetValue(column.ColumnName).SetDataType(XLCellValues.Text)
                                    workSheet.Cell(k, n).Style.Font.Bold = True
                                    workSheet.Cell(k, n).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

                                    n = n + 1
                                Else
                                    column.ColumnName = "RemovedColumn" & columnIndex
                                End If

                                columnIndex += 1
                            Next
                            reportHtml += "</tr>"

                            k = k + 1
                            n = 1
                            Dim rowCount As Int16 = 1

                            For Each drResult As DataRow In dtResult.Rows
                                If rowCount <= 10 Then
                                    reportHtml += "<tr>"
                                End If

                                For Each column As DataColumn In dtResult.Columns
                                    If Not column.ColumnName.StartsWith("RemovedColumn") Then
                                        If Double.TryParse(CCommon.ToString(drResult(column.ColumnName)), Nothing) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Number)
                                        ElseIf IsDate(CCommon.ToString(drResult(column.ColumnName))) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.DateTime)
                                        Else
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Text)
                                        End If

                                        If rowCount <= 10 Then
                                            reportHtml += "<td>" & CCommon.ToString(drResult(column.ColumnName)) & "</td>"
                                        End If

                                        n = n + 1
                                    End If
                                Next

                                If rowCount <= 10 Then
                                    reportHtml += "</tr>"
                                End If

                                k = k + 1
                                n = 1
                                rowCount += 1
                            Next

                            workSheet.Columns.AdjustToContents()

                            If Not Directory.Exists(directoryPath) Then
                                Directory.CreateDirectory(directoryPath)
                            End If
                            If File.Exists((directoryPath & "\" & attachmentFileName)) Then
                                File.Delete((directoryPath & "\" & attachmentFileName))
                            End If
                            workbook.SaveAs((directoryPath & "\" & attachmentFileName))
                            isFileGenerated = True
                        Else
                            reportHtml += "<tr><td>No data available</td></tr>"
                        End If
                    Else
                        reportHtml += "<tr><td>No data available</td></tr>"
                    End If
                ElseIf (Convert.ToInt64(Convert.ToInt64(dr("numFormID"))) = 17) Then
                    attachmentFileName = Convert.ToString(dr("vcReportName")) & ".xlsx"
                    Dim objFormGenericAdvSearch As New FormGenericAdvSearch()
                    objFormGenericAdvSearch.byteMode = 1
                    objFormGenericAdvSearch.SearchID = Convert.ToInt64(dr("numReportID"))
                    objFormGenericAdvSearch.FormID = Convert.ToInt64(dr("numFormID"))
                    objFormGenericAdvSearch.DomainID = lngDomainID
                    objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                    Dim dtSearch As DataTable = objFormGenericAdvSearch.ManageSavedSearch

                    If Not dtSearch Is Nothing AndAlso dtSearch.Rows.Count > 0 Then
                        Dim strSearchConditions As String = If(Not String.IsNullOrEmpty(Convert.ToString(dr("vcSearchConditionJson"))), New FormGenericAdvSearch().SearchItems(lngDomainID, intTimeZoneOffset, Convert.ToString(dr("vcSearchConditionJson"))), Convert.ToString(dtSearch.Rows(0)("vcSearchQuery")))
                        objFormGenericAdvSearch = New FormGenericAdvSearch
                        objFormGenericAdvSearch.QueryWhereCondition = strSearchConditions
                        objFormGenericAdvSearch.DomainID = lngDomainID
                        objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.FormID = 17
                        objFormGenericAdvSearch.DisplayColumns = Convert.ToString(dr("vcDisplayColumns"))
                        objFormGenericAdvSearch.SortCharacter = ChrW(0)
                        objFormGenericAdvSearch.SortcolumnName = ""
                        objFormGenericAdvSearch.CurrentPage = 0
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.TotalRecords = 0
                        objFormGenericAdvSearch.columnSortOrder = "Asc"
                        objFormGenericAdvSearch.GetAll = True
                        objFormGenericAdvSearch.columnSortOrder = ""
                        objFormGenericAdvSearch.columnSearch = ""
                        objFormGenericAdvSearch.columnName = ""
                        objFormGenericAdvSearch.RegularSearchCriteria = ""
                        objFormGenericAdvSearch.CustomSearchCriteria = ""
                        objFormGenericAdvSearch.ClientTimeZoneOffset = intTimeZoneOffset
                        Dim dtResult As DataTable = objFormGenericAdvSearch.AdvancedSearchCase

                        If Not dtResult Is Nothing AndAlso dtResult.Rows.Count > 0 Then
                            Dim fs As New MemoryStream()
                            Dim workbook As New XLWorkbook
                            Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Data")

                            Dim k As Int32 = 1
                            Dim n As Int32 = 1
                            Dim columnIndex As Int32 = 1
                            reportHtml += "<tr>"

                            For Each column As DataColumn In dtResult.Columns
                                If column.ColumnName.Contains("~") Then
                                    column.ColumnName = column.ColumnName.Split(New Char() {"~"c})(0)

                                    reportHtml += "<th>" & column.ColumnName & "</th>"
                                    workSheet.Cell(k, n).SetValue(column.ColumnName).SetDataType(XLCellValues.Text)
                                    workSheet.Cell(k, n).Style.Font.Bold = True
                                    workSheet.Cell(k, n).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

                                    n = n + 1
                                Else
                                    column.ColumnName = "RemovedColumn" & columnIndex
                                End If

                                columnIndex += 1
                            Next
                            reportHtml += "</tr>"

                            k = k + 1
                            n = 1
                            Dim rowCount As Int16 = 1

                            For Each drResult As DataRow In dtResult.Rows
                                If rowCount <= 10 Then
                                    reportHtml += "<tr>"
                                End If

                                For Each column As DataColumn In dtResult.Columns
                                    If Not column.ColumnName.StartsWith("RemovedColumn") Then
                                        If Double.TryParse(CCommon.ToString(drResult(column.ColumnName)), Nothing) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Number)
                                        ElseIf IsDate(CCommon.ToString(drResult(column.ColumnName))) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.DateTime)
                                        Else
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Text)
                                        End If

                                        If rowCount <= 10 Then
                                            reportHtml += "<td>" & CCommon.ToString(drResult(column.ColumnName)) & "</td>"
                                        End If

                                        n = n + 1
                                    End If
                                Next

                                If rowCount <= 10 Then
                                    reportHtml += "</tr>"
                                End If

                                k = k + 1
                                n = 1
                                rowCount += 1
                            Next

                            workSheet.Columns.AdjustToContents()

                            If Not Directory.Exists(directoryPath) Then
                                Directory.CreateDirectory(directoryPath)
                            End If
                            If File.Exists((directoryPath & "\" & attachmentFileName)) Then
                                File.Delete((directoryPath & "\" & attachmentFileName))
                            End If
                            workbook.SaveAs((directoryPath & "\" & attachmentFileName))
                            isFileGenerated = True
                        Else
                            reportHtml += "<tr><td>No data available</td></tr>"
                        End If
                    Else
                        reportHtml += "<tr><td>No data available</td></tr>"
                    End If
                ElseIf (Convert.ToInt64(Convert.ToInt64(dr("numFormID"))) = 18) Then
                    attachmentFileName = Convert.ToString(dr("vcReportName")) & ".xlsx"
                    Dim objFormGenericAdvSearch As New FormGenericAdvSearch()
                    objFormGenericAdvSearch.byteMode = 1
                    objFormGenericAdvSearch.SearchID = Convert.ToInt64(dr("numReportID"))
                    objFormGenericAdvSearch.FormID = Convert.ToInt64(dr("numFormID"))
                    objFormGenericAdvSearch.DomainID = lngDomainID
                    objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                    Dim dtSearch As DataTable = objFormGenericAdvSearch.ManageSavedSearch

                    If Not dtSearch Is Nothing AndAlso dtSearch.Rows.Count > 0 Then
                        Dim strSearchConditions As String = If(Not String.IsNullOrEmpty(Convert.ToString(dr("vcSearchConditionJson"))), New FormGenericAdvSearch().SearchItems(lngDomainID, intTimeZoneOffset, Convert.ToString(dr("vcSearchConditionJson"))), Convert.ToString(dtSearch.Rows(0)("vcSearchQuery")))
                        objFormGenericAdvSearch = New FormGenericAdvSearch
                        objFormGenericAdvSearch.QueryWhereCondition = strSearchConditions
                        objFormGenericAdvSearch.DomainID = lngDomainID
                        objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.FormID = 18
                        objFormGenericAdvSearch.DisplayColumns = Convert.ToString(dr("vcDisplayColumns"))
                        objFormGenericAdvSearch.SortcolumnName = ""
                        objFormGenericAdvSearch.CurrentPage = 0
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.TotalRecords = 0
                        objFormGenericAdvSearch.columnSortOrder = "Asc"
                        objFormGenericAdvSearch.GetAll = True
                        objFormGenericAdvSearch.columnSortOrder = ""
                        objFormGenericAdvSearch.columnSearch = ""
                        objFormGenericAdvSearch.columnName = ""
                        objFormGenericAdvSearch.RegularSearchCriteria = ""
                        objFormGenericAdvSearch.CustomSearchCriteria = ""
                        objFormGenericAdvSearch.ClientTimeZoneOffset = intTimeZoneOffset
                        Dim dtResult As DataTable = objFormGenericAdvSearch.AdvancedSearchProjects

                        If Not dtResult Is Nothing AndAlso dtResult.Rows.Count > 0 Then
                            Dim fs As New MemoryStream()
                            Dim workbook As New XLWorkbook
                            Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Data")

                            Dim k As Int32 = 1
                            Dim n As Int32 = 1
                            Dim columnIndex As Int32 = 1
                            reportHtml += "<tr>"

                            For Each column As DataColumn In dtResult.Columns
                                If column.ColumnName.Contains("~") Then
                                    column.ColumnName = column.ColumnName.Split(New Char() {"~"c})(0)

                                    reportHtml += "<th>" & column.ColumnName & "</th>"
                                    workSheet.Cell(k, n).SetValue(column.ColumnName).SetDataType(XLCellValues.Text)
                                    workSheet.Cell(k, n).Style.Font.Bold = True
                                    workSheet.Cell(k, n).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

                                    n = n + 1
                                Else
                                    column.ColumnName = "RemovedColumn" & columnIndex
                                End If

                                columnIndex += 1
                            Next
                            reportHtml += "</tr>"

                            k = k + 1
                            n = 1
                            Dim rowCount As Int16 = 1

                            For Each drResult As DataRow In dtResult.Rows
                                If rowCount <= 10 Then
                                    reportHtml += "<tr>"
                                End If

                                For Each column As DataColumn In dtResult.Columns
                                    If Not column.ColumnName.StartsWith("RemovedColumn") Then
                                        If Double.TryParse(CCommon.ToString(drResult(column.ColumnName)), Nothing) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Number)
                                        ElseIf IsDate(CCommon.ToString(drResult(column.ColumnName))) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.DateTime)
                                        Else
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Text)
                                        End If

                                        If rowCount <= 10 Then
                                            reportHtml += "<td>" & CCommon.ToString(drResult(column.ColumnName)) & "</td>"
                                        End If

                                        n = n + 1
                                    End If
                                Next

                                If rowCount <= 10 Then
                                    reportHtml += "</tr>"
                                End If

                                k = k + 1
                                n = 1
                                rowCount += 1
                            Next

                            workSheet.Columns.AdjustToContents()

                            If Not Directory.Exists(directoryPath) Then
                                Directory.CreateDirectory(directoryPath)
                            End If
                            If File.Exists((directoryPath & "\" & attachmentFileName)) Then
                                File.Delete((directoryPath & "\" & attachmentFileName))
                            End If
                            workbook.SaveAs((directoryPath & "\" & attachmentFileName))
                            isFileGenerated = True
                        Else
                            reportHtml += "<tr><td>No data available</td></tr>"
                        End If
                    Else
                        reportHtml += "<tr><td>No data available</td></tr>"
                    End If
                ElseIf (Convert.ToInt64(Convert.ToInt64(dr("numFormID"))) = 59) Then
                    attachmentFileName = Convert.ToString(dr("vcReportName")) & ".xlsx"
                    Dim objFormGenericAdvSearch As New FormGenericAdvSearch()
                    objFormGenericAdvSearch.byteMode = 1
                    objFormGenericAdvSearch.SearchID = Convert.ToInt64(dr("numReportID"))
                    objFormGenericAdvSearch.FormID = Convert.ToInt64(dr("numFormID"))
                    objFormGenericAdvSearch.DomainID = lngDomainID
                    objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                    Dim dtSearch As DataTable = objFormGenericAdvSearch.ManageSavedSearch

                    If Not dtSearch Is Nothing AndAlso dtSearch.Rows.Count > 0 Then
                        Dim strTimeConditions As String = ""
                        Dim strSearchConditions As String = If(Not String.IsNullOrEmpty(Convert.ToString(dr("vcSearchConditionJson"))), New FormGenericAdvSearch().SearchItems(lngDomainID, intTimeZoneOffset, Convert.ToString(dr("vcSearchConditionJson"))), Convert.ToString(dtSearch.Rows(0)("vcSearchQuery")))
                        If (Convert.ToString(dtSearch.Rows(0)("vcTimeExpression")) <> "") Then
                            strTimeConditions = CCommon.ToString(dtSearch.Rows(0)("vcTimeExpression")).Replace("{FROM}", DateTime.Today.AddDays(CDbl((0 - CCommon.ToInteger(dtSearch.Rows(0)("intSlidingDays"))))).ToShortDateString).Replace("{TO}", Conversions.ToString(Conversions.ToDate((DateTime.Today.ToShortDateString & " 23:59:59"))))
                        End If

                        objFormGenericAdvSearch = New FormGenericAdvSearch
                        objFormGenericAdvSearch.QueryWhereCondition = strSearchConditions & strTimeConditions
                        objFormGenericAdvSearch.DomainID = lngDomainID
                        objFormGenericAdvSearch.UserCntID = Convert.ToInt64(dr("numSearchUserCntID"))
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.FormID = 59
                        objFormGenericAdvSearch.DisplayColumns = Convert.ToString(dr("vcDisplayColumns"))
                        objFormGenericAdvSearch.SortCharacter = ChrW(0)
                        objFormGenericAdvSearch.SortcolumnName = ""
                        objFormGenericAdvSearch.CurrentPage = 0
                        objFormGenericAdvSearch.PageSize = 0
                        objFormGenericAdvSearch.TotalRecords = 0
                        objFormGenericAdvSearch.columnSortOrder = "Asc"
                        objFormGenericAdvSearch.GetAll = True
                        objFormGenericAdvSearch.columnSortOrder = ""
                        objFormGenericAdvSearch.columnSearch = ""
                        objFormGenericAdvSearch.columnName = ""
                        objFormGenericAdvSearch.RegularSearchCriteria = ""
                        objFormGenericAdvSearch.CustomSearchCriteria = ""
                        objFormGenericAdvSearch.ClientTimeZoneOffset = intTimeZoneOffset
                        Dim dtResult As DataTable = objFormGenericAdvSearch.AdvancedSearchFinancialSearch

                        If Not dtResult Is Nothing AndAlso dtResult.Rows.Count > 0 Then
                            Dim fs As New MemoryStream()
                            Dim workbook As New XLWorkbook
                            Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Data")

                            Dim k As Int32 = 1
                            Dim n As Int32 = 1
                            Dim columnIndex As Int32 = 1
                            reportHtml += "<tr>"

                            For Each column As DataColumn In dtResult.Columns
                                If column.ColumnName.Contains("~") Then
                                    column.ColumnName = column.ColumnName.Split(New Char() {"~"c})(0)

                                    reportHtml += "<th>" & column.ColumnName & "</th>"
                                    workSheet.Cell(k, n).SetValue(column.ColumnName).SetDataType(XLCellValues.Text)
                                    workSheet.Cell(k, n).Style.Font.Bold = True
                                    workSheet.Cell(k, n).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

                                    n = n + 1
                                Else
                                    column.ColumnName = "RemovedColumn" & columnIndex
                                End If

                                columnIndex += 1
                            Next

                            reportHtml += "</tr>"

                            k = k + 1
                            n = 1
                            Dim rowCount As Int16 = 1

                            For Each drResult As DataRow In dtResult.Rows
                                If rowCount <= 10 Then
                                    reportHtml += "<tr>"
                                End If

                                For Each column As DataColumn In dtResult.Columns
                                    If Not column.ColumnName.StartsWith("RemovedColumn") Then
                                        If Double.TryParse(CCommon.ToString(drResult(column.ColumnName)), Nothing) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Number)
                                        ElseIf IsDate(CCommon.ToString(drResult(column.ColumnName))) Then
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.DateTime)
                                        Else
                                            workSheet.Cell(k, n).SetValue(CCommon.ToString(drResult(column.ColumnName))).SetDataType(XLCellValues.Text)
                                        End If

                                        If rowCount <= 10 Then
                                            reportHtml += "<td>" & CCommon.ToString(drResult(column.ColumnName)) & "</td>"
                                        End If

                                        n = n + 1
                                    End If
                                Next

                                If rowCount <= 10 Then
                                    reportHtml += "<tr>"
                                End If

                                k = k + 1
                                n = 1
                                rowCount += 1
                            Next

                            workSheet.Columns.AdjustToContents()

                            If Not Directory.Exists(directoryPath) Then
                                Directory.CreateDirectory(directoryPath)
                            End If
                            If File.Exists((directoryPath & "\" & attachmentFileName)) Then
                                File.Delete((directoryPath & "\" & attachmentFileName))
                            End If
                            workbook.SaveAs((directoryPath & "\" & attachmentFileName))
                            isFileGenerated = True
                        Else
                            reportHtml += "<tr><td>No data available</td></tr>"
                        End If
                    Else
                        reportHtml += "<tr><td>No data available</td></tr>"
                    End If

                End If

                reportHtml += "</table></div>"
                If isFileGenerated Then
                    reportHtml += "<div style=""padding: 10px 15px;color: #fff;background-color: #337ab7;border-color: #337ab7;border-top: 1px solid transparent;border-bottom-right-radius: 3px;border-bottom-left-radius: 3px;text-align:right;""><div class='form-inline' style='text-align:right'><label>For full report check attachement:</label>&nbsp;" & attachmentFileName & "</div></div>"
                End If
                reportHtml += "</div>"

                If Not File.Exists((directoryPath & "\" & emailFileName)) Then
                    File.WriteAllText((directoryPath & "\" & emailFileName), reportHtml)
                Else
                    File.AppendAllText((directoryPath & "\" & emailFileName), reportHtml)
                End If
            Catch exception1 As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class
End Namespace
