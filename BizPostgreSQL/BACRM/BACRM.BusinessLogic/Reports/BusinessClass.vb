Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Namespace BACRM.BusinessLogic.Tracking

    Public Class BusinessClass
        Inherits BACRM.BusinessLogic.CBusinessBase
        'Private DomainId As Long
        Private _NewsID As Long
        Private _NewsHeading As String
        Private _NewsDate As String
        Private _NewsDesc As String
        Private _NewsURL As String
        Private _NewsImage As String
        Private _Active As Short
        Private _UserHostAddress As String
        Private _UserAgent As String
        Private _UserBrowser As String
        Private _UserCrawler As String
        Private _URL As String
        Private _Referrer As String
        Private _NoOfVisits As Long
        Private _OrgRef As String
        Private _OrgURL As String
        Private _NoofPages As Long
        Private _VisitedURL As String
        Private _ElapsedTime As String
        Private _TotalTime As String
        Private _strURL As String
        Private _UserDomain As String
        Private _SearchTerm As String
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _ToDate As Date
        Private _FromDate As Date
        Private _TrackingID As Long
        Private _IPNo As Long
        Private _Country As String
        Private _ReferrerName As String = ""
        Private _IsVisited As Boolean
        Private _Type As Short
        Private _DivisionID As Long
        Private _MinimumPages As Integer
        Private _ReportType As Integer
        Private _PageID As Integer
        Private _ClientZoneOffset As Integer

        Public Property ClientZoneOffset() As Integer
            Get
                Return _ClientZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientZoneOffset = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property Type() As Short
            Get
                Return _Type
            End Get
            Set(ByVal Value As Short)
                _Type = Value
            End Set
        End Property

        Public Property IsVisited() As Boolean
            Get
                Return _IsVisited
            End Get
            Set(ByVal Value As Boolean)
                _IsVisited = Value
            End Set
        End Property

        Public Property ReferrerName() As String
            Get
                Return _ReferrerName
            End Get
            Set(ByVal Value As String)
                _ReferrerName = Value
            End Set
        End Property

        Public Property Country() As String
            Get
                Return _Country
            End Get
            Set(ByVal Value As String)
                _Country = Value
            End Set
        End Property

        Public Property IPNo() As Long
            Get
                Return _IPNo
            End Get
            Set(ByVal Value As Long)
                _IPNo = Value
            End Set
        End Property

        Public Property TrackingID() As Long
            Get
                Return _TrackingID
            End Get
            Set(ByVal Value As Long)
                _TrackingID = Value
            End Set
        End Property

        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Property SearchTerm() As String
            Get
                Return _SearchTerm
            End Get
            Set(ByVal Value As String)
                _SearchTerm = Value
            End Set
        End Property

        Public Property UserDomain() As String
            Get
                Return _UserDomain
            End Get
            Set(ByVal Value As String)
                _UserDomain = Value
            End Set
        End Property

        Public Property strURL() As String
            Get
                Return _strURL
            End Get
            Set(ByVal Value As String)
                _strURL = Value
            End Set
        End Property

        Public Property TotalTime() As String
            Get
                Return _TotalTime
            End Get
            Set(ByVal Value As String)
                _TotalTime = Value
            End Set
        End Property

        Public Property ElapsedTime() As String
            Get
                Return _ElapsedTime
            End Get
            Set(ByVal Value As String)
                _ElapsedTime = Value
            End Set
        End Property

        Public Property VisitedURL() As String
            Get
                Return _VisitedURL
            End Get
            Set(ByVal Value As String)
                _VisitedURL = Value
            End Set
        End Property

        Public Property NoofPages() As Long
            Get
                Return _NoofPages
            End Get
            Set(ByVal Value As Long)
                _NoofPages = Value
            End Set
        End Property

        Public Property OrgURL() As String
            Get
                Return _OrgURL
            End Get
            Set(ByVal Value As String)
                _OrgURL = Value
            End Set
        End Property

        Public Property OrgRef() As String
            Get
                Return _OrgRef
            End Get
            Set(ByVal Value As String)
                _OrgRef = Value
            End Set
        End Property

        Public Property NoOfVisits() As Long
            Get
                Return _NoOfVisits
            End Get
            Set(ByVal Value As Long)
                _NoOfVisits = Value
            End Set
        End Property

        Public Property Referrer() As String
            Get
                Return _Referrer
            End Get
            Set(ByVal Value As String)
                _Referrer = Value
            End Set
        End Property

        Public Property URL() As String
            Get
                Return _URL
            End Get
            Set(ByVal Value As String)
                _URL = Value
            End Set
        End Property

        Public Property UserCrawler() As String
            Get
                Return _UserCrawler
            End Get
            Set(ByVal Value As String)
                _UserCrawler = Value
            End Set
        End Property

        Public Property UserBrowser() As String
            Get
                Return _UserBrowser
            End Get
            Set(ByVal Value As String)
                _UserBrowser = Value
            End Set
        End Property

        Public Property UserAgent() As String
            Get
                Return _UserAgent
            End Get
            Set(ByVal Value As String)
                _UserAgent = Value
            End Set
        End Property

        Public Property UserHostAddress() As String
            Get
                Return _UserHostAddress
            End Get
            Set(ByVal Value As String)
                _UserHostAddress = Value
            End Set
        End Property

        Public Property Active() As Short
            Get
                Return _Active
            End Get
            Set(ByVal Value As Short)
                _Active = Value
            End Set
        End Property

        Public Property NewsImage() As String
            Get
                Return _NewsImage
            End Get
            Set(ByVal Value As String)
                _NewsImage = Value
            End Set
        End Property

        Public Property NewsURL() As String
            Get
                Return _NewsURL
            End Get
            Set(ByVal Value As String)
                _NewsURL = Value
            End Set
        End Property

        Public Property NewsDesc() As String
            Get
                Return _NewsDesc
            End Get
            Set(ByVal Value As String)
                _NewsDesc = Value
            End Set
        End Property

        Public Property NewsDate() As String
            Get
                Return _NewsDate
            End Get
            Set(ByVal Value As String)
                _NewsDate = Value
            End Set
        End Property

        Public Property NewsHeading() As String
            Get
                Return _NewsHeading
            End Get
            Set(ByVal Value As String)
                _NewsHeading = Value
            End Set
        End Property

        Public Property NewsID() As Long
            Get
                Return _NewsID
            End Get
            Set(ByVal Value As Long)
                _NewsID = Value
            End Set
        End Property
        Public Property MinimumPages() As Integer
            Get
                Return _MinimumPages
            End Get
            Set(ByVal Value As Integer)
                _MinimumPages = Value
            End Set
        End Property
        Public Property ReportType() As Integer
            Get
                Return _ReportType
            End Get
            Set(ByVal Value As Integer)
                _ReportType = Value
            End Set
        End Property
        Public Property PageID() As Integer
            Get
                Return _PageID
            End Get
            Set(ByVal Value As Integer)
                _PageID = Value
            End Set
        End Property

        Private _SourceCampaign As String
        Public Property SourceCampaign() As String
            Get
                Return _SourceCampaign
            End Get
            Set(ByVal value As String)
                _SourceCampaign = value
            End Set
        End Property




#Region "Constructor"


        Public Sub New()
        End Sub



#End Region

        Public Function ManageNews(ByVal connString As String) As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numNewsID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _NewsID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcHeading", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = _NewsHeading

                arParms(2) = New Npgsql.NpgsqlParameter("@vcDate", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _NewsDate

                arParms(3) = New Npgsql.NpgsqlParameter("@vcDesc", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _NewsDesc

                arParms(4) = New Npgsql.NpgsqlParameter("@vcImagePath", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(4).Value = _NewsImage

                arParms(5) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(5).Value = _NewsURL

                arParms(6) = New Npgsql.NpgsqlParameter("@bitActive", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _Active

                arParms(7) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _Type

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetManageNews", objParam, True)
                _NewsID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteNews(ByVal connString As String) As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numNewsID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _NewsID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteNews", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ManageTrackingVisitors(ByVal connString As String) As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(17) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcUserHostAddress", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = _UserHostAddress

                arParms(1) = New Npgsql.NpgsqlParameter("@vcUserAgent", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = _UserDomain

                arParms(2) = New Npgsql.NpgsqlParameter("@vcUserAgent", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _UserAgent

                arParms(3) = New Npgsql.NpgsqlParameter("@vcBrowser", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _UserBrowser

                arParms(4) = New Npgsql.NpgsqlParameter("@vcCrawler", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _UserCrawler

                arParms(5) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(5).Value = _URL

                arParms(6) = New Npgsql.NpgsqlParameter("@vcReferrer", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(6).Value = _Referrer

                arParms(7) = New Npgsql.NpgsqlParameter("@numVisits", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _NoOfVisits

                arParms(8) = New Npgsql.NpgsqlParameter("@vcOrginalURL", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(8).Value = _OrgURL

                arParms(9) = New Npgsql.NpgsqlParameter("@vcOrginalRef", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(9).Value = _OrgRef

                arParms(10) = New Npgsql.NpgsqlParameter("@numVistedPages", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _NoofPages

                arParms(11) = New Npgsql.NpgsqlParameter("@vcTotalTime", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(11).Value = _TotalTime

                arParms(12) = New Npgsql.NpgsqlParameter("@strURL", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(12).Value = _strURL

                arParms(13) = New Npgsql.NpgsqlParameter("@vcCountry", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(13).Value = _Country

                arParms(14) = New Npgsql.NpgsqlParameter("@IsVisited", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _IsVisited

                arParms(15) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _DivisionID

                arParms(16) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = DomainId

                arParms(17) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(17).Value = _SourceCampaign

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageTrackingVisitors", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetVisitorsList(ByVal connString As String, ByVal flag As Boolean) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(13) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ReferringPage", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = _Referrer

                arParms(1) = New Npgsql.NpgsqlParameter("@searchTerm", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _SearchTerm

                arParms(2) = New Npgsql.NpgsqlParameter("@From", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _FromDate

                arParms(3) = New Npgsql.NpgsqlParameter("@To", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _ToDate

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _TotalRecords

                arParms(7) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _columnName

                arParms(8) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(8).Value = _columnSortOrder

                arParms(9) = New Npgsql.NpgsqlParameter("@strCrawler", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(9).Value = _UserCrawler

                arParms(10) = New Npgsql.NpgsqlParameter("@MinimumPages", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _MinimumPages

                arParms(11) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = DomainId

                arParms(12) = New Npgsql.NpgsqlParameter("@ClientOffsetTime", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(12).Value = _ClientZoneOffset

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet
                If (flag = False) Then
                    ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetReferringPage", objParam, True)
                    _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)
                    Return ds.Tables(0)
                End If
                If (flag = True) Then
                    ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetAllReferringPage", objParam, True)
                    _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)
                    Return ds.Tables(0)
                End If

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetDetailsOfVisitor(ByVal connString As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTrackingID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TrackingID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetVisitorDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function HowOftenPagesrVisited(ByVal connString As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@From", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _FromDate

                arParms(1) = New Npgsql.NpgsqlParameter("@To", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _ToDate

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _TotalRecords

                arParms(5) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _columnName

                arParms(6) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(6).Value = _columnSortOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = DomainId

                arParms(8) = New Npgsql.NpgsqlParameter("@ClientOffsetTime", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _ClientZoneOffset

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_HowOftenPagesVisited", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function HowOftenPagesrVisitedIP(ByVal connString As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@From", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _FromDate

                arParms(1) = New Npgsql.NpgsqlParameter("@To", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _ToDate

                arParms(2) = New Npgsql.NpgsqlParameter("@PageID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _PageID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_HowOftenPagesVisitedIP", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ReturningVisitors(ByVal connString As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@From", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _FromDate

                arParms(1) = New Npgsql.NpgsqlParameter("@To", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _ToDate

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _TotalRecords

                arParms(5) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _columnName

                arParms(6) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(6).Value = _columnSortOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = DomainId

                arParms(8) = New Npgsql.NpgsqlParameter("@ClientOffsetTime", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _ClientZoneOffset

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_ReturningVisitors", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function KeyWordsUsed(ByVal connString As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@From", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _FromDate

                arParms(1) = New Npgsql.NpgsqlParameter("@To", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _ToDate

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _TotalRecords

                arParms(5) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _columnName

                arParms(6) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(6).Value = _columnSortOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = DomainId

                arParms(8) = New Npgsql.NpgsqlParameter("@ClientOffSetTime", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _ClientZoneOffset

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_KeyWordsUsed", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCountryFromIPaddress(ByVal connString As String) As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@IPNo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _IPNo

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_GetCountryfromIP", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        '        IP Address to IP Number Conversion
        'If the IP address 161.132.13.1, then the IP number is 2709785857.
        'IP Number, X = 161 x (256*256*256) + 132 x (256*256) + 13 x (256) + 1= 2709785857
        'In general, this is the formula to convert an IP Address to IP Number.
        'Let assume the IP Address is A.B.C.D.
        'IP Number, X = A x (256*256*256) + B x (256*256) + C x 256 + D
        Public Function GetIPNumberFromIPAddress(ByVal strIP As String) As Long
            Try
                Dim arrIP As String() = strIP.Split(Char.Parse("."))
                If arrIP.Length = 4 Then
                    Return Common.CCommon.ToLong(arrIP(0)) * 256 * 256 * 256 + Common.CCommon.ToLong(arrIP(1)) * 256 * 256 + Common.CCommon.ToLong(arrIP(2)) * 256 + Common.CCommon.ToLong(arrIP(3))
                Else
                    Return 0
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SaveSettingsVisitor(ByVal connString As String) As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@MinimumPages", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _MinimumPages

                arParms(1) = New Npgsql.NpgsqlParameter("@ReportType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ReportType


                SqlDAL.ExecuteNonQuery(connString, "USP_SaveSettingsVisitor", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function LoadDropdown(ByVal connString As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Dim dr As DataSet
                dr = SqlDAL.ExecuteDataset(connString, "USP_GetSettingsVisitor", arParms)

                Return dr.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteInkTomi(ByVal connString As String) As Boolean
            Try

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteInktomi")
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetPageIps(ByVal connString As String) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@From", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _FromDate

                arParms(1) = New Npgsql.NpgsqlParameter("@To", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _ToDate

                arParms(2) = New Npgsql.NpgsqlParameter("@searchTerm", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(2).Value = _SearchTerm

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainId

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientOffsetTime", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _ClientZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPageIps", arParms)
                Return ds.Tables(0)

            Catch ex As Exception

            End Try
        End Function
    End Class

End Namespace