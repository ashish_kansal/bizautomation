﻿
Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.CustomReports
    Public Class ReportObject

        Private _vcReportName As String
        Public Property vcReportName() As String
            Get
                Return _vcReportName
            End Get
            Set(ByVal value As String)
                _vcReportName = value
            End Set
        End Property

        Private _vcReportDescription As String
        Public Property vcReportDescription() As String
            Get
                Return _vcReportDescription
            End Get
            Set(ByVal value As String)
                _vcReportDescription = value
            End Set
        End Property

        Private _vcGroupName As String
        Public Property vcGroupName() As String
            Get
                Return _vcGroupName
            End Get
            Set(ByVal value As String)
                _vcGroupName = value
            End Set
        End Property

        Private _ReportFormatType As Short
        Public Property ReportFormatType() As Short
            Get
                Return _ReportFormatType
            End Get
            Set(ByVal value As Short)
                _ReportFormatType = value
            End Set
        End Property

        Private _NoRows As String
        Public Property NoRows() As String
            Get
                Return _NoRows
            End Get
            Set(ByVal value As String)
                _NoRows = value
            End Set
        End Property

        Private _SortColumn As String
        Public Property SortColumn() As String
            Get
                Return _SortColumn
            End Get
            Set(ByVal value As String)
                _SortColumn = value
            End Set
        End Property

        Private _SortDirection As String
        Public Property SortDirection() As String
            Get
                Return _SortDirection
            End Get
            Set(ByVal value As String)
                _SortDirection = value
            End Set
        End Property

        Private _FilterLogic As String
        Public Property FilterLogic() As String
            Get
                Return _FilterLogic
            End Get
            Set(ByVal value As String)
                _FilterLogic = value
            End Set
        End Property

        Private _DateFieldFilter As String
        Public Property DateFieldFilter() As String
            Get
                Return _DateFieldFilter
            End Get
            Set(ByVal value As String)
                _DateFieldFilter = value
            End Set
        End Property

        Private _DateFieldValue As String
        Public Property DateFieldValue() As String
            Get
                Return _DateFieldValue
            End Get
            Set(ByVal value As String)
                _DateFieldValue = value
            End Set
        End Property

        Private _FromDate As String
        Public Property FromDate() As String
            Get
                Return _FromDate
            End Get
            Set(ByVal value As String)
                _FromDate = value
            End Set
        End Property

        Private _ToDate As String
        Public Property ToDate() As String
            Get
                Return _ToDate
            End Get
            Set(ByVal value As String)
                _ToDate = value
            End Set
        End Property

        Private _RecordFilter As Short
        Public Property RecordFilter() As Short
            Get
                Return _RecordFilter
            End Get
            Set(ByVal value As Short)
                _RecordFilter = value
            End Set
        End Property

        Private _ColumnList As List(Of String)
        Public Property ColumnList As List(Of String)
            Get
                Return _ColumnList
            End Get
            Set(ByVal value As List(Of String))
                _ColumnList = value
            End Set
        End Property

        Private _filters As List(Of filterObject)
        Public Property filters As List(Of filterObject)
            Get
                Return _filters
            End Get
            Set(ByVal value As List(Of filterObject))
                _filters = value
            End Set
        End Property

        Private _GroupColumn As List(Of String)
        Public Property GroupColumn As List(Of String)
            Get
                Return _GroupColumn
            End Get
            Set(ByVal value As List(Of String))
                _GroupColumn = value
            End Set
        End Property

        Private _RowBreaks As List(Of String)
        Public Property RowBreaks As List(Of String)
            Get
                Return _RowBreaks
            End Get
            Set(ByVal value As List(Of String))
                _RowBreaks = value
            End Set
        End Property

        Private _ColumnBreaks As List(Of String)
        Public Property ColumnBreaks As List(Of String)
            Get
                Return _ColumnBreaks
            End Get
            Set(ByVal value As List(Of String))
                _ColumnBreaks = value
            End Set
        End Property

        Private _Aggregate As List(Of AggregateObject)
        Public Property Aggregate As List(Of AggregateObject)
            Get
                Return _Aggregate
            End Get
            Set(ByVal value As List(Of AggregateObject))
                _Aggregate = value
            End Set
        End Property

        Private _KPIThresold As List(Of KPIThresoldObject)
        Public Property KPIThresold As List(Of KPIThresoldObject)
            Get
                Return _KPIThresold
            End Get
            Set(ByVal value As List(Of KPIThresoldObject))
                _KPIThresold = value
            End Set
        End Property

        Private _ResultData As String
        Public Property ResultData As String
            Get
                Return _ResultData
            End Get
            Set(ByVal value As String)
                _ResultData = value
            End Set
        End Property

        Private _KPIMeasureField As String
        Public Property KPIMeasureField() As String
            Get
                Return _KPIMeasureField
            End Get
            Set(ByVal value As String)
                _KPIMeasureField = value
            End Set
        End Property

        Private _bitHideSummaryDetail As Boolean
        Public Property bitHideSummaryDetail() As Boolean
            Get
                Return _bitHideSummaryDetail
            End Get
            Set(ByVal Value As Boolean)
                _bitHideSummaryDetail = Value
            End Set
        End Property
    End Class

    Public Class filterObject
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _Column As String
        Public Property Column() As String
            Get
                Return _Column
            End Get
            Set(ByVal value As String)
                _Column = value
            End Set
        End Property

        Private _ColValue As String
        Public Property ColValue() As String
            Get
                Return _ColValue
            End Get
            Set(ByVal value As String)
                _ColValue = value
            End Set
        End Property

        Private _OperatorType As String
        Public Property OperatorType() As String
            Get
                Return _OperatorType
            End Get
            Set(ByVal value As String)
                _OperatorType = value
            End Set
        End Property

        Private _ColText As String
        Public Property ColText() As String
            Get
                Return _ColText
            End Get
            Set(ByVal value As String)
                _ColText = value
            End Set
        End Property

    End Class

    Public Class AggregateObject
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _Column As String
        Public Property Column() As String
            Get
                Return _Column
            End Get
            Set(ByVal value As String)
                _Column = value
            End Set
        End Property

        Private _aggType As String
        Public Property aggType() As String
            Get
                Return _aggType
            End Get
            Set(ByVal value As String)
                _aggType = value
            End Set
        End Property

    End Class

    Public Class KPIThresoldObject
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _ThresoldType As String
        Public Property ThresoldType() As String
            Get
                Return _ThresoldType
            End Get
            Set(ByVal value As String)
                _ThresoldType = value
            End Set
        End Property

        Private _Value1 As Decimal?
        Public Property Value1() As Decimal?
            Get
                Return _Value1
            End Get
            Set(ByVal value As Decimal?)
                _Value1 = value
            End Set
        End Property

        Private _Value2 As Decimal?
        Public Property Value2() As Decimal?
            Get
                Return _Value2
            End Get
            Set(ByVal value As Decimal?)
                _Value2 = value
            End Set
        End Property

        Private _GoalValue As Decimal?
        Public Property GoalValue() As Decimal?
            Get
                Return _GoalValue
            End Get
            Set(ByVal value As Decimal?)
                _GoalValue = value
            End Set
        End Property
    End Class

End Namespace
