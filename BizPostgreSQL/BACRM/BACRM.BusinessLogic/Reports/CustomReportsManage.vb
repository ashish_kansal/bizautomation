﻿
Option Explicit On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.CustomReports
    Public Class CustomReportsManage
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _ReportID As Long
        Public Property ReportID() As Long
            Get
                Return _ReportID
            End Get
            Set(ByVal value As Long)
                _ReportID = value
            End Set
        End Property

        Private _ReportName As String
        Public Property ReportName() As String
            Get
                Return _ReportName
            End Get
            Set(ByVal value As String)
                _ReportName = value
            End Set
        End Property

        Private _ReportDescription As String
        Public Property ReportDescription() As String
            Get
                Return _ReportDescription
            End Get
            Set(ByVal value As String)
                _ReportDescription = value
            End Set
        End Property

        Private _ReportModuleID As Long
        Public Property ReportModuleID() As Long
            Get
                Return _ReportModuleID
            End Get
            Set(ByVal value As Long)
                _ReportModuleID = value
            End Set
        End Property

        Private _ReportModuleGroupID As Long
        Public Property ReportModuleGroupID() As Long
            Get
                Return _ReportModuleGroupID
            End Get
            Set(ByVal value As Long)
                _ReportModuleGroupID = value
            End Set
        End Property

        Private _tintReportType As Integer
        Public Property tintReportType() As Integer
            Get
                Return _tintReportType
            End Get
            Set(ByVal value As Integer)
                _tintReportType = value
            End Set
        End Property



        Private _boolActive As Boolean
        Public Property boolActive() As Boolean
            Get
                Return _boolActive
            End Get
            Set(ByVal value As Boolean)
                _boolActive = value
            End Set
        End Property

        Private _textQuery As String
        Public Property textQuery() As String
            Get
                Return _textQuery
            End Get
            Set(ByVal value As String)
                _textQuery = value
            End Set
        End Property

        Private _SortColumnmReportFieldGroupID As Long
        Public Property SortColumnmReportFieldGroupID() As Long
            Get
                Return _SortColumnmReportFieldGroupID
            End Get
            Set(ByVal value As Long)
                _SortColumnmReportFieldGroupID = value
            End Set
        End Property

        Private _SortColumnFieldID As Long
        Public Property SortColumnFieldID() As Long
            Get
                Return _SortColumnFieldID
            End Get
            Set(ByVal value As Long)
                _SortColumnFieldID = value
            End Set
        End Property

        Private _SortColumnDirection As String
        Public Property SortColumnDirection() As String
            Get
                Return _SortColumnDirection
            End Get
            Set(ByVal value As String)
                _SortColumnDirection = value
            End Set
        End Property

        Private _bitSortColumnCustom As Boolean
        Public Property bitSortColumnCustom() As Boolean
            Get
                Return _bitSortColumnCustom
            End Get
            Set(ByVal value As Boolean)
                _bitSortColumnCustom = value
            End Set
        End Property

        Private _FilterLogic As String
        Public Property FilterLogic() As String
            Get
                Return _FilterLogic
            End Get
            Set(ByVal value As String)
                _FilterLogic = value
            End Set
        End Property

        Private _strText As String
        Public Property strText() As String
            Get
                Return _strText
            End Get
            Set(ByVal value As String)
                _strText = value
            End Set
        End Property

        Private _ClientTimeZoneOffset As Int32
        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _DateReportFieldGroupID As Long
        Public Property DateReportFieldGroupID() As Long
            Get
                Return _DateReportFieldGroupID
            End Get
            Set(ByVal value As Long)
                _DateReportFieldGroupID = value
            End Set
        End Property

        Private _DateFieldID As Long
        Public Property DateFieldID() As Long
            Get
                Return _DateFieldID
            End Get
            Set(ByVal value As Long)
                _DateFieldID = value
            End Set
        End Property

        Private _bitDateFieldColumnCustom As Boolean
        Public Property bitDateFieldColumnCustom() As Boolean
            Get
                Return _bitDateFieldColumnCustom
            End Get
            Set(ByVal value As Boolean)
                _bitDateFieldColumnCustom = value
            End Set
        End Property

        Private _DateFieldValue As String
        Public Property DateFieldValue() As String
            Get
                Return _DateFieldValue
            End Get
            Set(ByVal value As String)
                _DateFieldValue = value
            End Set
        End Property

        Private _dtFromDate As DateTime = SqlTypes.SqlDateTime.MinValue.Value
        Public Property dtFromDate() As DateTime
            Get
                Return _dtFromDate
            End Get
            Set(ByVal value As DateTime)
                _dtFromDate = value
            End Set
        End Property

        Private _dtToDate As DateTime = SqlTypes.SqlDateTime.MaxValue.Value
        Public Property dtToDate() As DateTime
            Get
                Return _dtToDate
            End Get
            Set(ByVal value As DateTime)
                _dtToDate = value
            End Set
        End Property

        Private _tintRecordFilter As Short
        Public Property tintRecordFilter() As Short
            Get
                Return _tintRecordFilter
            End Get
            Set(ByVal value As Short)
                _tintRecordFilter = value
            End Set
        End Property

        Private _columnName As String
        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Private _columnSortOrder As String
        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Private _SortCharacter As Char
        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Private _TotalRecords As Integer
        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Private _PageSize As Integer
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Private _CurrentPage As Integer
        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Private _GroupId As Integer
        Public Property GroupId() As Long
            Get
                Return _GroupId
            End Get
            Set(ByVal Value As Long)
                _GroupId = Value
            End Set
        End Property

        Private _DashBoardID As Long
        Public Property DashBoardID() As Long
            Get
                Return _DashBoardID
            End Get
            Set(ByVal value As Long)
                _DashBoardID = value
            End Set
        End Property

        Private _tintChartType As Short
        Public Property tintChartType() As Short
            Get
                Return _tintChartType
            End Get
            Set(ByVal value As Short)
                _tintChartType = value
            End Set
        End Property

        Private _HeaderText As String
        Public Property HeaderText() As String
            Get
                Return _HeaderText
            End Get
            Set(ByVal Value As String)
                _HeaderText = Value
            End Set
        End Property

        Private _FooterText As String
        Public Property FooterText() As String
            Get
                Return _FooterText
            End Get
            Set(ByVal Value As String)
                _FooterText = Value
            End Set
        End Property

        Private _XAxis As String
        Public Property XAxis() As String
            Get
                Return _XAxis
            End Get
            Set(ByVal Value As String)
                _XAxis = Value
            End Set
        End Property

        Private _YAxis As String
        Public Property YAxis() As String
            Get
                Return _YAxis
            End Get
            Set(ByVal Value As String)
                _YAxis = Value
            End Set
        End Property

        Private _AggType As String
        Public Property AggType() As String
            Get
                Return _AggType
            End Get
            Set(ByVal Value As String)
                _AggType = Value
            End Set
        End Property

        Private _intNoRows As Integer
        Public Property intNoRows() As Integer
            Get
                Return _intNoRows
            End Get
            Set(ByVal value As Integer)
                _intNoRows = value
            End Set
        End Property

        Private _KPIMeasureFieldID As String
        Public Property KPIMeasureFieldID() As String
            Get
                Return _KPIMeasureFieldID
            End Get
            Set(ByVal Value As String)
                _KPIMeasureFieldID = Value
            End Set
        End Property

        Private _ReportKPIGroupID As Long
        Public Property ReportKPIGroupID() As Long
            Get
                Return _ReportKPIGroupID
            End Get
            Set(ByVal Value As Long)
                _ReportKPIGroupID = Value
            End Set
        End Property

        Private _KPIGroupName As String
        Public Property KPIGroupName() As String
            Get
                Return _KPIGroupName
            End Get
            Set(ByVal Value As String)
                _KPIGroupName = Value
            End Set
        End Property

        Private _KPIGroupDescription As String
        Public Property KPIGroupDescription() As String
            Get
                Return _KPIGroupDescription
            End Get
            Set(ByVal Value As String)
                _KPIGroupDescription = Value
            End Set
        End Property

        Private _tintKPIGroupReportType As Short
        Public Property tintKPIGroupReportType() As Short
            Get
                Return _tintKPIGroupReportType
            End Get
            Set(ByVal Value As Short)
                _tintKPIGroupReportType = Value
            End Set
        End Property

        Private _tintMode As Short
        Public Property tintMode() As Short
            Get
                Return _tintMode
            End Get
            Set(ByVal Value As Short)
                _tintMode = Value
            End Set
        End Property

        Private _strKPIGroupReportIds As String
        Public Property strKPIGroupReportIds() As String
            Get
                Return _strKPIGroupReportIds
            End Get
            Set(ByVal value As String)
                _strKPIGroupReportIds = value
            End Set
        End Property

        Private _tintReportCategory As Short
        Public Property tintReportCategory() As Short
            Get
                Return _tintReportCategory
            End Get
            Set(ByVal Value As Short)
                _tintReportCategory = Value
            End Set
        End Property

        Private _bitHideSummaryDetail As Boolean
        Public Property bitHideSummaryDetail() As Boolean
            Get
                Return _bitHideSummaryDetail
            End Get
            Set(ByVal Value As Boolean)
                _bitHideSummaryDetail = Value
            End Set
        End Property



        Public Property Perspective As Integer
        Public Property Timeline As String
        Public Property DashboardTemplateID As Long
        Public Property DefaultReportID As Integer
        Public Property strPrebuildReportIds As String
        Public Property IsReportRun As Boolean
        Public Property GroupBy As String
        Public Property Teritorry As String
        Public Property FilterBy As Short
        Public Property FilterValue As String
        Public Property FromDate As Date
        Public Property ToDate As Date
        Public Property Top As Integer
        Public Property TintControlField As Integer
        Public Property DealAmount As String
        Public Property TintOppType As Integer
        Public Property lngPConclAnalysis As String
        Public Property bitTask As String
        Public Property tintTotalProgress As Integer
        Public Property tintMinNumber As Integer = 0
        Public Property tintMaxNumber As Integer = 0
        Public Property tintQtyToDisplay As Integer = 0
        Public Property DueDate As String

        Public Property DealWon As Boolean
        Public Property GrossRevenue As Boolean
        Public Property RevenueGrossProfit As Boolean
        Public Property GrossProfit As Boolean
        Public Property bitCustomTimeline As Boolean
        Public Property Receivedbutnotbilled As Boolean
        Public Property Billedbutnotreceived As Boolean
        Public Property Soldbutnotshipped As Boolean

        'Public Property vcTeams As String
        'Public Property vcEmploees As String



        Public Enum PrebuildReportEnum
            AR = 1
            AP = 2
            MoneyInBank = 3
            AccountingReports = 4
            Top10ItemsByProfitAmountCompanyWide = 5
            Top10ItemsByRevenueSoldCompanyWide = 6
            ProfitMarginByItemClassificationCompanyWide = 7
            Top10CustomerByProfitMarginCompanyWide = 8
            Top10CustomerByProfitAmountCompanyWide = 9
            ActionItemsAndMeetingsCompanyWide = 10
            RPEYTDvsSamePeriodLastYear = 11
            RPELastMonthvsSamePeriodLastYear = 12
            SalesVsExpense = 13
            TopSourcesOfSalesOrders = 14
            Top10ItemsByProfitMarginCompanyWide = 15
            Top10SalesOppotunityByRevenue = 16
            Top10SalesOppotunityByProgress = 17
            Top10ItemsByReturnedVsSold = 18
            Top10SalesOpportunityByPastDue = 19
            Top10CampaignsByROI = 20
            TopCustomersByPortionOfTotalSales = 21
            KPI = 22
            LeadSource = 23
            Top10Email = 24
            Reminders = 25
            TopReasonsWinningDeals = 26
            TopReasonsLossingDeals = 27
            Top10ReasonsForSalesReturns = 28
            EmployeeSalesPerformancePanel1 = 29
            PartnerRMPLast12Months = 30
            EmployeeSalesPerformance1 = 31
            EmployeeBenefittoCompany = 32
            First10SavedSearches = 33
            SalesOppWonLost = 34
            SalesOppPipeLine = 35
            BackOrderValuation = 36
            InventoryValueByWareHouse = 37
            MonthToDateRevenue = 38
            PendingReportDetails = 39
        End Enum


        Function GetReportModuleANDGroupMaster() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                Dim objParam() As Object = sqlParams.ToArray()


                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetReportModuleANDGroupMaster", objParam, True)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetReportModuleGroupFieldMaster() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReportID", _ReportID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@bitIsReportRun", IsReportRun, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetReportModuleGroupFieldMaster", objParam, True)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageReportListMaster(Optional ByVal bitDuplicate As Boolean = False) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numReportID", _ReportID, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@vcReportName", _ReportName, NpgsqlTypes.NpgsqlDbType.VarChar, 200))

                    .Add(SqlDAL.Add_Parameter("@vcReportDescription", _ReportDescription, NpgsqlTypes.NpgsqlDbType.VarChar, 500))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReportModuleID", _ReportModuleID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReportModuleGroupID", _ReportModuleGroupID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintReportType", _tintReportType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitActive", _boolActive, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@textQuery", _textQuery, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@numSortColumnmReportFieldGroupID", _SortColumnmReportFieldGroupID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numSortColumnFieldID", _SortColumnFieldID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcSortColumnDirection", _SortColumnDirection, NpgsqlTypes.NpgsqlDbType.VarChar, 10))

                    .Add(SqlDAL.Add_Parameter("@bitSortColumnCustom", _bitSortColumnCustom, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@vcFilterLogic", _FilterLogic, NpgsqlTypes.NpgsqlDbType.VarChar, 200))

                    .Add(SqlDAL.Add_Parameter("@numDateReportFieldGroupID", _DateReportFieldGroupID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDateFieldID", _DateFieldID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitDateFieldColumnCustom", _bitDateFieldColumnCustom, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@vcDateFieldValue", _DateFieldValue, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@dtFromDate", _dtFromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@dtToDate", _dtToDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@tintRecordFilter", _tintRecordFilter, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@strText", _strText, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@intNoRows", _intNoRows, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@vcKPIMeasureFieldID", _KPIMeasureFieldID, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@bitDuplicate", bitDuplicate, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@bitHideSummaryDetail", _bitHideSummaryDetail, NpgsqlTypes.NpgsqlDbType.Bit))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageReportListMaster", objParam, True)
                _ReportID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetReportListMasterDetail() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numReportID", _ReportID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur4", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur5", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur6", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetReportListMasterDetail", objParam, True)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Function USP_ReportQueryExecute() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                connString = connString.TrimEnd(CChar(";")) & ";Timeout=300"  'in seconds 

                Dim ds As New DataSet
                Using conn As New Npgsql.NpgSqlConnection(connString)
                    Dim cmd As New Npgsql.NpgsqlCommand()
                    cmd.Connection = conn
                    cmd.CommandTimeout = 300
                    cmd.CommandText = "usp_reportqueryexecute"
                    cmd.CommandType = CommandType.StoredProcedure
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numreportid", ReportID, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_clienttimezoneoffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_textquery", _textQuery, NpgsqlTypes.NpgsqlDbType.Text))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("v_numcurrentpage", CurrentPage, NpgsqlTypes.NpgsqlDbType.Numeric))
                    cmd.Parameters.Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter
                    Dim i As Int32 = 0
                    conn.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = conn.BeginTransaction()
                        cmd.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In cmd.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), conn)
                                    ds.Tables.Add(parm.Value.ToString())
                                    sqlAdapter.Fill(ds.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    conn.Close()
                End Using

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Columns.Contains("TotalParentRecords") AndAlso ds.Tables(0).Rows.Count > 0 Then
                    _TotalRecords = CCommon.ToLong(ds.Tables(0).Rows(0)("TotalParentRecords"))
                Else
                    _TotalRecords = 0
                End If

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetReportListMaster() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numReportModuleID", _ReportModuleID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SortChar", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@columnName", _columnName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@SearchStr", _ReportName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintReportType", tintReportType, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintPerspective", Perspective, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcTimeline", Timeline, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetReportListMaster", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DeleteReportListMaster() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReportID", _ReportID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteReportListMaster", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageUserReportList() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numReportID", _ReportID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntId", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintReportType", _tintReportType, NpgsqlTypes.NpgsqlDbType.Smallint))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageUserReportList", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetUserReportList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntId", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetUserReportList", objParam, True)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetDashboardAllowedReports() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numGroupId", _GroupId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "Usp_GetDashboardAllowedReports", objParam, True)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageDashboardAllowedReports() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numGroupId", _GroupId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@strReportIds", _strText, NpgsqlTypes.NpgsqlDbType.VarChar, 2000))
                    .Add(SqlDAL.Add_Parameter("@strKPIGroupReportIds", _strKPIGroupReportIds, NpgsqlTypes.NpgsqlDbType.VarChar, 2000))
                    .Add(SqlDAL.Add_Parameter("@strPredefinedReportIds", strPrebuildReportIds, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageDashboardAllowedReports", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManagegeReportDashboard() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDashBoardID", _DashBoardID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReportID", _ReportID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintReportType", _tintReportType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@tintChartType", _tintChartType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@vcHeaderText", _HeaderText, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcFooterText", _FooterText, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcXAxis", _XAxis, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcYAxis", _YAxis, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@vcAggType", _AggType, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@tintReportCategory", _tintReportCategory, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDashboardTemplateID", DashboardTemplateID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcTimeLine", Timeline, NpgsqlTypes.NpgsqlDbType.VarChar, 50))
                    .Add(SqlDAL.Add_Parameter("@vcGroupBy", GroupBy, NpgsqlTypes.NpgsqlDbType.VarChar, 50))
                    .Add(SqlDAL.Add_Parameter("@vcTeritorry", Teritorry, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcFilterBy", FilterBy, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcFilterValue", FilterValue, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", FromDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@dtToDate", ToDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@numRecordCount", Top, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintControlField", TintControlField, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcDealAmount", DealAmount, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintOppType", TintOppType, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@lngPConclAnalysis", lngPConclAnalysis, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitTask", bitTask, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintTotalProgress", tintTotalProgress, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintMinNumber", tintMinNumber, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintMaxNumber", tintMaxNumber, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintQtyToDisplay", tintQtyToDisplay, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcDueDate", DueDate, NpgsqlTypes.NpgsqlDbType.VarChar, 50))
                    .Add(SqlDAL.Add_Parameter("@bitDealWon", DealWon, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitGrossRevenue", GrossRevenue, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitRevenue", RevenueGrossProfit, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitGrossProfit", GrossProfit, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitCustomTimeline", bitCustomTimeline, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitReceviedButNotBilled", Receivedbutnotbilled, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitBilledButNotReceived", Billedbutnotreceived, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitSoldButNotShipped", Soldbutnotshipped, NpgsqlTypes.NpgsqlDbType.Bit))




                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManagegeReportDashboard", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetReportDashboardDTL() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet = New DataSet()
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim dataAdatpter As Npgsql.NpgsqlDataAdapter
                Dim i As Integer = 0

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("v_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("v_numdashboardid", _DashBoardID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("v_numreportid", ReportID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Using connection As New Npgsql.NpgsqlConnection(connString)
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        Dim sqlCommand As New Npgsql.NpgsqlCommand
                        sqlCommand.CommandTimeout = 300
                        sqlCommand.Connection = connection
                        sqlCommand.Parameters.AddRange(sqlParams.ToArray())
                        sqlCommand.CommandText = "usp_getreportdashboarddtl"
                        sqlCommand.CommandType = CommandType.StoredProcedure
                        sqlCommand.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In sqlParams
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    dataAdatpter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    dataAdatpter.Fill(ds.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()
                End Using

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetReportDashBoard(ByVal numDashboardTemplateID As Long) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDashboardTemplateID", numDashboardTemplateID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetReportDashBoard", objParam, True)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageDashboardOpe(charOpe As String) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDashBoardID", _DashBoardID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@charOpe", charOpe, NpgsqlTypes.NpgsqlDbType.Char))

                    .Add(SqlDAL.Add_Parameter("@strText", _strText, NpgsqlTypes.NpgsqlDbType.Text))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageDashboardOpe", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageReportDashboardSize(tintColumn As Short, tintSize As Short) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintColumn", tintColumn, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@tintSize", tintSize, NpgsqlTypes.NpgsqlDbType.Smallint))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageReportDashboardSize", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetReportKPIGroupListMaster() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReportKPIGroupID", _ReportKPIGroupID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetReportKPIGroupListMaster", objParam, True)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageReportKPIGroupListMaster() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numReportKPIGroupID", _ReportKPIGroupID, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcKPIGroupName", _KPIGroupName, NpgsqlTypes.NpgsqlDbType.VarChar, 200))

                    .Add(SqlDAL.Add_Parameter("@vcKPIGroupDescription", _KPIGroupDescription, NpgsqlTypes.NpgsqlDbType.VarChar, 500))

                    .Add(SqlDAL.Add_Parameter("@tintKPIGroupReportType", _tintKPIGroupReportType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@strText", _strText, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numReportID", _ReportID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageReportKPIGroupListMaster", objParam, True)
                _ReportKPIGroupID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetDatasetToReportObject(ds As DataSet) As ReportObject
            Try
                Dim objReport As New ReportObject

                Dim dtReportList As DataTable = ds.Tables(0)
                Dim dtReportFields As DataTable = ds.Tables(1)
                Dim dtReportFilter As DataTable = ds.Tables(2)
                Dim dtReportSummaryGroup As DataTable = ds.Tables(3)
                'Dim dtReportMatrixBreak As DataTable = ds.Tables(4)
                Dim dtReportMatrixAggregate As DataTable = ds.Tables(4)
                Dim dtReportKPIThresold As DataTable = ds.Tables(5)

                'json = JsonConvert.SerializeObject(ds, Formatting.None)

                objReport = New ReportObject
                objReport.vcReportName = CCommon.ToString(dtReportList.Rows(0)("vcReportName"))
                objReport.vcReportDescription = CCommon.ToString(dtReportList.Rows(0)("vcReportDescription"))
                objReport.vcGroupName = CCommon.ToString(dtReportList.Rows(0)("vcGroupName"))
                objReport.ReportFormatType = CCommon.ToShort(dtReportList.Rows(0)("tintReportType"))
                objReport.SortColumn = IIf(CCommon.ToLong(dtReportList.Rows(0)("numSortColumnFieldID")) > 0, dtReportList.Rows(0)("numSortColumnmReportFieldGroupID").ToString() & "_" & dtReportList.Rows(0)("numSortColumnFieldID").ToString() & "_" & dtReportList.Rows(0)("bitSortColumnCustom").ToString(), "")
                objReport.SortDirection = CCommon.ToString(dtReportList.Rows(0)("vcSortColumnDirection"))
                objReport.FilterLogic = CCommon.ToString(dtReportList.Rows(0)("vcFilterLogic"))
                objReport.DateFieldFilter = IIf(CCommon.ToLong(dtReportList.Rows(0)("numDateFieldID")) > 0, dtReportList.Rows(0)("numDateReportFieldGroupID").ToString() & "_" & dtReportList.Rows(0)("numDateFieldID").ToString() & "_" & dtReportList.Rows(0)("bitDateFieldColumnCustom").ToString(), "")
                objReport.DateFieldValue = CCommon.ToString(dtReportList.Rows(0)("vcDateFieldValue"))
                If Not System.Web.HttpContext.Current Is Nothing Then
                    objReport.FromDate = FormattedDateFromDate(CDate(dtReportList.Rows(0)("dtFromDate")), System.Web.HttpContext.Current.Session("DateFormat"))
                    objReport.ToDate = FormattedDateFromDate(CDate(dtReportList.Rows(0)("dtToDate")), System.Web.HttpContext.Current.Session("DateFormat"))
                Else
                    objReport.FromDate = CDate(dtReportList.Rows(0)("dtFromDate"))
                    objReport.ToDate = CDate(dtReportList.Rows(0)("dtToDate"))
                End If
                objReport.RecordFilter = CCommon.ToShort(dtReportList.Rows(0)("tintRecordFilter"))
                objReport.NoRows = CCommon.ToInteger(dtReportList.Rows(0)("intNoRows"))
                objReport.KPIMeasureField = CCommon.ToString(dtReportList.Rows(0)("vcKPIMeasureFieldID"))
                objReport.bitHideSummaryDetail = CCommon.ToBool(dtReportList.Rows(0)("bitHideSummaryDetail"))

                objReport.ColumnList = New List(Of String)

                For Each dr As DataRow In dtReportFields.Rows
                    objReport.ColumnList.Add(dr("numReportFieldGroupID") & "_" & dr("numFieldID") & "_" & dr("bitCustom"))
                Next

                objReport.filters = New List(Of filterObject)

                For Each dr As DataRow In dtReportFilter.Rows
                    objReport.filters.Add(New filterObject() With {.Column = dr("numReportFieldGroupID") & "_" & dr("numFieldID") & "_" & dr("bitCustom"), .ColValue = dr("vcFilterValue"), .OperatorType = dr("vcFilterOperator"), .ColText = dr("vcFilterText")})
                Next

                objReport.ColumnBreaks = New List(Of String)
                objReport.RowBreaks = New List(Of String)
                objReport.GroupColumn = New List(Of String)

                For Each dr As DataRow In dtReportSummaryGroup.Rows
                    If dr("tintBreakType") = 1 Then
                        objReport.ColumnBreaks.Add(dr("numReportFieldGroupID") & "_" & dr("numFieldID") & "_" & dr("bitCustom"))
                    ElseIf dr("tintBreakType") = 2 Then
                        objReport.RowBreaks.Add(dr("numReportFieldGroupID") & "_" & dr("numFieldID") & "_" & dr("bitCustom"))
                    Else
                        objReport.GroupColumn.Add(dr("numReportFieldGroupID") & "_" & dr("numFieldID") & "_" & dr("bitCustom"))
                    End If
                Next

                objReport.Aggregate = New List(Of AggregateObject)

                For Each dr As DataRow In dtReportMatrixAggregate.Rows
                    objReport.Aggregate.Add(New AggregateObject() With {.Column = dr("numReportFieldGroupID") & "_" & dr("numFieldID") & "_" & dr("bitCustom"), .aggType = dr("vcAggType")})
                Next

                objReport.KPIThresold = New List(Of KPIThresoldObject)

                For Each dr As DataRow In dtReportKPIThresold.Rows
                    objReport.KPIThresold.Add(New KPIThresoldObject() With {.ThresoldType = dr("vcThresoldType"), .Value1 = dr("decValue1"), .Value2 = dr("decValue2"), .GoalValue = dr("decGoalValue")})
                Next

                Return objReport
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetReportQuery(objReportManage As CustomReportsManage, ds As DataSet, objReport As ReportObject, boolInit As Boolean, boolSave As Boolean, dsColumnList As DataSet, Optional ByVal boolChart As Boolean = False, Optional ByVal currentPage As Integer = 1) As String
            Try
                Dim strColumnList As New List(Of String)

                For Each str As String In objReport.ColumnList
                    strColumnList.Add(str)
                Next

                For Each str As String In objReport.GroupColumn
                    strColumnList.Add(str)
                Next

                For Each str As String In objReport.ColumnBreaks
                    strColumnList.Add(str)
                Next

                For Each str As String In objReport.RowBreaks
                    strColumnList.Add(str)
                Next

                For Each str As AggregateObject In objReport.Aggregate
                    strColumnList.Add(str.Column)
                Next

                If (objReport.ReportFormatType = 3 Or objReport.ReportFormatType = 4) AndAlso objReport.KPIMeasureField.Length > 0 Then
                    strColumnList.Add(objReport.KPIMeasureField)
                End If

                'Check whether we need to add logic for getting all types of credit memos.
                Dim IsAddCreditMemoLogic As Boolean = False
                For Each objRep As filterObject In objReport.filters
                    If objRep.ColText.ToLower().Contains("credit") Then
                        IsAddCreditMemoLogic = True
                        Exit For
                    End If
                Next

                ''Check whether we need to add logic for getting all types of RMAs.
                'Dim IsAddRMALogic As Boolean = False
                'For Each objRep As filterObject In objReport.filters
                '    If objRep.ColText.ToLower().Contains("rma") Then
                '        IsAddRMALogic = True
                '        Exit For
                '    End If
                'Next

                Dim Sql As New System.Text.StringBuilder
                Dim SqlColumn As New System.Text.StringBuilder
                Dim SqlFrom As New System.Text.StringBuilder
                Dim SqlCondition As New System.Text.StringBuilder
                Dim SqlSortColumn As New System.Text.StringBuilder
                Dim SqlGroupBy As New System.Text.StringBuilder
                Dim SqlKPIGroupBy As New System.Text.StringBuilder
                Dim SqlLimit As New System.Text.StringBuilder

                If strColumnList.Count = 0 AndAlso boolInit = False AndAlso boolSave = True Then
                    Throw New Exception("ReportNoColumnFound")
                Else
                    If strColumnList.Count > 0 Then
                        If (objReport.ReportFormatType = 3 Or objReport.ReportFormatType = 4 Or objReport.ReportFormatType = 5) Then
                            SqlColumn.Append("Select COUNT(*) OVER() AS TotalParentRecords, ")
                        ElseIf boolSave = True Then
                            If currentPage <> 0 AndAlso objReport.ReportFormatType = 0 AndAlso CCommon.ToInteger(objReport.NoRows) > 0 Then
                                SqlColumn.Append("Select COUNT(*) OVER() AS TotalParentRecords, ")
                                SqlLimit.Append(" LIMIT " & objReport.NoRows)
                            Else
                                SqlColumn.Append("Select COUNT(*) OVER() AS TotalParentRecords, ")
                            End If
                        Else
                            SqlColumn.Append("Select 15 AS TotalParentRecords, ")
                            SqlLimit.Append(" LIMIT 15")
                        End If
                        SqlFrom.Append(" FROM")
                        SqlCondition.Append(" WHERE 1=1")

                        Select Case CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID"))
                            Case 24
                                SqlFrom.Append(" ReturnHeader ")
                                SqlCondition.Append(" AND ReturnHeader.numDomainId = @numDomainID AND ReturnHeader.numReturnStatus = 303 AND COALESCE(ReturnHeader.numParentID,0) = 0")
                                SqlFrom.Append(" INNER JOIN DivisionMaster ON ReturnHeader.numDivisionId = DivisionMaster.numDivisionId")
                                SqlFrom.Append(" INNER JOIN CompanyInfo ON CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                                SqlFrom.Append(" INNER JOIN AdditionalContactsInformation ON ReturnHeader.numContactId = AdditionalContactsInformation.numContactId ")
                                SqlFrom.Append(" LEFT JOIN OpportunityMaster ON OpportunityMaster.numOppId = ReturnHeader.numOppId")
                                SqlFrom.Append(" LEFT JOIN OpportunityBizDocs ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId AND OpportunityBizDocs.numBizDocId IN (299)")

                                If strColumnList.Where(Function(x) x.StartsWith("4_") Or x.StartsWith("6_") Or x.StartsWith("7_") Or x.StartsWith("19_") Or x.StartsWith("20_") Or x.StartsWith("31_") Or x.StartsWith("33_")).Count() > 0 Then
                                    SqlFrom.Append(" LEFT JOIN ReturnItems ON ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID")
                                    SqlFrom.Append(" LEFT JOIN Item ON ReturnItems.numItemcode = Item.numItemcode")
                                    SqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")
                                    SqlFrom.Append(" LEFT JOIN OpportunityItems ON OpportunityMaster.numOppId = OpportunityItems.numOppId AND ReturnItems.numItemCode = OpportunityItems.numItemCode AND ReturnItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID ")
                                    SqlFrom.Append(" LEFT JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID")
                                    SqlFrom.Append(" LEFT JOIN WareHouseItems  ON WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = ReturnItems.numWareHouseItemID ")
                                    SqlFrom.Append(" LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID ")
                                    SqlFrom.Append(" LEFT JOIN Warehouses ON Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID")
                                    SqlFrom.Append(" LEFT JOIN OppWarehouseSerializedItem ON ReturnHeader.numReturnHeaderID=OppWarehouseSerializedItem.numReturnHeaderID AND OppWarehouseSerializedItem.numReturnItemID=ReturnItems.numReturnItemID")
                                    SqlFrom.Append(" LEFT JOIN WareHouseItmsDTL  ON OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID")
                                End If
                            Case 1 'Organization
                                SqlFrom.Append(" DivisionMaster join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")

                                SqlCondition.Append(" And DivisionMaster.numDomainId = @numDomainID")

                                If objReport.RecordFilter = 1 Then 'Assigned-To/Record Owner
                                    SqlCondition.Append(" And (DivisionMaster.numRecOwner = @numUserCntID Or DivisionMaster.numAssignedTo = @numUserCntID)")
                                ElseIf objReport.RecordFilter = 2 Then 'Team Scope
                                    SqlCondition.Append(" AND DivisionMaster.numRecOwner in (select numContactID from AdditionalContactsInformation where numDomainID=@numDomainID and numTeam in (select distinct numTeam from UserTeams where numusercntid=@numUserCntID))")
                                ElseIf objReport.RecordFilter = 3 Then 'Territory Scope
                                    SqlCondition.Append(" AND DivisionMaster.numTerId in (select distinct numTerritoryID from UserTerritory where numusercntid=@numUserCntID)")
                                ElseIf objReport.RecordFilter = 4 Then 'Assigned-To
                                    SqlCondition.Append(" AND DivisionMaster.numAssignedTo = @numUserCntID")
                                ElseIf objReport.RecordFilter = 5 Then 'Record Owner
                                    SqlCondition.Append(" AND DivisionMaster.numRecOwner = @numUserCntID")
                                End If

                            Case 5 'Contacts
                                SqlFrom.Append(" AdditionalContactsInformation join DivisionMaster on DivisionMaster.numDivisionID=AdditionalContactsInformation.numDivisionID")
                                SqlFrom.Append(" join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid")

                                SqlCondition.Append(" AND DivisionMaster.numDomainId = @numDomainID AND AdditionalContactsInformation.numDomainId = @numDomainID")

                                If objReport.RecordFilter = 1 Then 'Assigned-To/Record Owner
                                    SqlCondition.Append(" AND AdditionalContactsInformation.numRecOwner = @numUserCntID")
                                ElseIf objReport.RecordFilter = 2 Then 'Team Scope
                                    SqlCondition.Append(" AND AdditionalContactsInformation.numRecOwner in (select numContactID from AdditionalContactsInformation where numDomainID=@numDomainID and numTeam in (select distinct numTeam from UserTeams where numusercntid=@numUserCntID))")
                                ElseIf objReport.RecordFilter = 3 Then 'Territory Scope
                                    SqlCondition.Append(" AND DivisionMaster.numTerId in (select distinct numTerritoryID from UserTerritory where numusercntid=@numUserCntID)")
                                ElseIf objReport.RecordFilter = 4 Then 'Assigned-To
                                    SqlCondition.Append(" AND AdditionalContactsInformation.numRecOwner = @numUserCntID")
                                ElseIf objReport.RecordFilter = 5 Then 'Record Owner
                                    SqlCondition.Append(" AND AdditionalContactsInformation.numRecOwner = @numUserCntID")
                                End If

                            Case 13 'Opportunities/Orders
                                SqlFrom.Append(" OpportunityMaster inner join DivisionMaster on OpportunityMaster.numDivisionId=DivisionMaster.numDivisionId ")
                                SqlFrom.Append(" inner join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                                SqlFrom.Append(" inner join AdditionalContactsInformation on OpportunityMaster.numContactId=AdditionalContactsInformation.numContactId ")

                                If strColumnList.Where(Function(x) x.StartsWith("35_")).Count() > 0 Then
                                    SqlFrom.Append(" LEFT JOIN OpportunityContact ACOpportunityContact ON OpportunityMaster.numOppID=ACOpportunityContact.numOppId ")
                                    SqlFrom.Append(" LEFT JOIN AdditionalContactsInformation ACAdditionalContactsInformation ON ACOpportunityContact.numContactId = ACAdditionalContactsInformation.numContactId ")
                                    SqlFrom.Append(" LEFT JOIN DivisionMaster ACDivisionMaster ON ACAdditionalContactsInformation.numDivisionId = ACDivisionMaster.numDivisionId ")
                                    SqlFrom.Append(" LEFT JOIN CompanyInfo ACCompanyInfo ON ACDivisionMaster.numCompanyid = ACCompanyInfo.numCompanyid ")
                                End If

                                SqlCondition.Append(" AND OpportunityMaster.numDomainId = @numDomainID")

                                If objReport.RecordFilter = 1 Then 'Assigned-To/Record Owner
                                    SqlCondition.Append(" AND (OpportunityMaster.numRecOwner = @numUserCntID OR OpportunityMaster.numAssignedTo = @numUserCntID)")
                                ElseIf objReport.RecordFilter = 2 Then 'Team Scope
                                    SqlCondition.Append(" AND OpportunityMaster.numRecOwner in (select numContactID from AdditionalContactsInformation where numDomainID=@numDomainID and numTeam in (select distinct numTeam from UserTeams where numusercntid=@numUserCntID))")
                                ElseIf objReport.RecordFilter = 3 Then 'Territory Scope
                                    SqlCondition.Append(" AND DivisionMaster.numTerId in (select distinct numTerritoryID from UserTerritory where numusercntid=@numUserCntID)")
                                ElseIf objReport.RecordFilter = 4 Then 'Assigned-To
                                    SqlCondition.Append(" AND OpportunityMaster.numAssignedTo = @numUserCntID")
                                ElseIf objReport.RecordFilter = 5 Then 'Record Owner
                                    SqlCondition.Append(" AND OpportunityMaster.numRecOwner = @numUserCntID")
                                End If

                            Case 10 'Opportunities/Orders Items
                                SqlFrom.Append(" OpportunityMaster inner join DivisionMaster on OpportunityMaster.numDivisionId=DivisionMaster.numDivisionId ")
                                SqlFrom.Append(" inner join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                                SqlFrom.Append(" inner join AdditionalContactsInformation on OpportunityMaster.numContactId=AdditionalContactsInformation.numContactId ")

                                SqlFrom.Append(" inner Join OpportunityItems on OpportunityMaster.numOppId = OpportunityItems.numOppId")
                                SqlFrom.Append(" Left Join Item on OpportunityItems.numItemcode = Item.numItemcode")
                                SqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")

                                SqlFrom.Append(" Left Join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID")
                                SqlFrom.Append(" LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID ")
                                SqlFrom.Append(" Left Join Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID")

                                If strColumnList.Where(Function(x) x.StartsWith("35_")).Count() > 0 Then
                                    SqlFrom.Append(" LEFT JOIN OpportunityContact ACOpportunityContact ON OpportunityMaster.numOppID=ACOpportunityContact.numOppId ")
                                    SqlFrom.Append(" LEFT JOIN AdditionalContactsInformation ACAdditionalContactsInformation ON ACOpportunityContact.numContactId = ACAdditionalContactsInformation.numContactId ")
                                    SqlFrom.Append(" LEFT JOIN DivisionMaster ACDivisionMaster ON ACAdditionalContactsInformation.numDivisionId = ACDivisionMaster.numDivisionId ")
                                    SqlFrom.Append(" LEFT JOIN CompanyInfo ACCompanyInfo ON ACDivisionMaster.numCompanyid = ACCompanyInfo.numCompanyid ")
                                End If


                                'IF SERIAL NO IS SELECTED AS FIELD IN REPORT THEN AND ONLY THEN WE REPLACE ##SERIALNO### WITH JOIN TABLES ELSE CLEAR PLACEHOLDER
                                SqlFrom.Append(" ##SERIALNO## ")

                                SqlCondition.Append(" AND OpportunityMaster.numDomainId = @numDomainID AND Item.numDomainId = @numDomainID")

                                If objReport.RecordFilter = 1 Then 'Assigned-To/Record Owner
                                    SqlCondition.Append(" AND (OpportunityMaster.numRecOwner = @numUserCntID OR OpportunityMaster.numAssignedTo = @numUserCntID)")
                                ElseIf objReport.RecordFilter = 2 Then 'Team Scope
                                    SqlCondition.Append(" AND OpportunityMaster.numRecOwner in (select numContactID from AdditionalContactsInformation where numDomainID=@numDomainID and numTeam in (select distinct numTeam from UserTeams where numusercntid=@numUserCntID))")
                                ElseIf objReport.RecordFilter = 3 Then 'Territory Scope
                                    SqlCondition.Append(" AND DivisionMaster.numTerId in (select distinct numTerritoryID from UserTerritory where numusercntid=@numUserCntID)")
                                ElseIf objReport.RecordFilter = 4 Then 'Assigned-To
                                    SqlCondition.Append(" AND OpportunityMaster.numAssignedTo = @numUserCntID")
                                ElseIf objReport.RecordFilter = 5 Then 'Record Owner
                                    SqlCondition.Append(" AND OpportunityMaster.numRecOwner = @numUserCntID")
                                End If
                            Case 8 'Opportunities/Orders BizDocs
                                SqlFrom.Append(" OpportunityMaster inner join DivisionMaster on OpportunityMaster.numDivisionId=DivisionMaster.numDivisionId ")
                                SqlFrom.Append(" inner join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                                SqlFrom.Append(" inner join AdditionalContactsInformation on OpportunityMaster.numContactId=AdditionalContactsInformation.numContactId ")
                                SqlFrom.Append(" Left Join OpportunityBizDocs on OpportunityMaster.numOppId = OpportunityBizDocs.numOppId")

                                If strColumnList.Where(Function(x) x.StartsWith("35_")).Count() > 0 Then
                                    SqlFrom.Append(" LEFT JOIN OpportunityContact ACOpportunityContact ON OpportunityMaster.numOppID=ACOpportunityContact.numOppId ")
                                    SqlFrom.Append(" LEFT JOIN AdditionalContactsInformation ACAdditionalContactsInformation ON ACOpportunityContact.numContactId = ACAdditionalContactsInformation.numContactId ")
                                    SqlFrom.Append(" LEFT JOIN DivisionMaster ACDivisionMaster ON ACAdditionalContactsInformation.numDivisionId = ACDivisionMaster.numDivisionId ")
                                    SqlFrom.Append(" LEFT JOIN CompanyInfo ACCompanyInfo ON ACDivisionMaster.numCompanyid = ACCompanyInfo.numCompanyid ")
                                End If

                                If IsAddCreditMemoLogic Then
                                    SqlFrom.Append(" LEFT OUTER JOIN (SELECT DepositMaster.dtDepositDate,DepositMaster.vcReference,DepositMaster.numDepositId,DepositMaster.numDivisionID,DepositMaster.numDomainID,DepositMaster.tintDepositePage,DepositeDetails.numOppID,DepositMaster.monDepositAmount,DepositMaster.monAppliedAmount FROM DepositMaster LEFT OUTER JOIN DepositeDetails ON DepositeDetails.numDepositID = DepositMaster.numDepositId WHERE 1=1 AND DepositMaster.tintDepositePage = 3 ) AS T1 ON T1.numDivisionID = DivisionMaster.numDivisionID AND T1.numDomainID = DivisionMaster.numDomainID ")
                                End If

                                'If IsAddRMALogic Then
                                '    SqlFrom.Append(" LEFT JOIN ReturnHeader ON OpportunityMaster.numOppId = ReturnHeader.numOppId  AND OpportunityMaster.numdomainID = ReturnHeader.numDomainId AND DIvisionMaster.numdivisionID = ReturnHeader.numDivisionId LEFT JOIN ReturnItems ON ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID ")
                                'End If

                                SqlCondition.Append(" AND OpportunityMaster.numDomainId = @numDomainID")

                                If objReport.RecordFilter = 1 Then 'Assigned-To/Record Owner
                                    SqlCondition.Append(" AND (OpportunityMaster.numRecOwner = @numUserCntID OR OpportunityMaster.numAssignedTo = @numUserCntID)")
                                ElseIf objReport.RecordFilter = 2 Then 'Team Scope
                                    SqlCondition.Append(" AND OpportunityMaster.numRecOwner in (select numContactID from AdditionalContactsInformation where numDomainID=@numDomainID and numTeam in (select distinct numTeam from UserTeams where numusercntid=@numUserCntID))")
                                ElseIf objReport.RecordFilter = 3 Then 'Territory Scope
                                    SqlCondition.Append(" AND DivisionMaster.numTerId in (select distinct numTerritoryID from UserTerritory where numusercntid=@numUserCntID)")
                                ElseIf objReport.RecordFilter = 4 Then 'Assigned-To
                                    SqlCondition.Append(" AND OpportunityMaster.numAssignedTo = @numUserCntID")
                                ElseIf objReport.RecordFilter = 5 Then 'Record Owner
                                    SqlCondition.Append(" AND OpportunityMaster.numRecOwner = @numUserCntID")
                                End If

                            Case 9 'Opportunities/Orders BizDoc Items
                                SqlFrom.Append(" OpportunityMaster inner join DivisionMaster on OpportunityMaster.numDivisionId=DivisionMaster.numDivisionId ")
                                SqlFrom.Append(" inner join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")
                                SqlFrom.Append(" inner join AdditionalContactsInformation on OpportunityMaster.numContactId=AdditionalContactsInformation.numContactId ")

                                SqlFrom.Append(" Left Join OpportunityBizDocs on OpportunityMaster.numOppId = OpportunityBizDocs.numOppId")
                                SqlFrom.Append(" Left Join OpportunityBizDocItems on OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID")

                                SqlFrom.Append(" Left Join OpportunityItems on OpportunityMaster.numOppId = OpportunityItems.numOppId and OpportunityBizDocItems.numOppItemID=OpportunityItems.numoppitemtCode")

                                SqlFrom.Append(" Left Join Item on OpportunityItems.numItemcode = Item.numItemcode")
                                SqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")

                                SqlFrom.Append(" Left Join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID")
                                SqlFrom.Append(" LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID ")
                                SqlFrom.Append(" Left Join Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID")

                                If strColumnList.Where(Function(x) x.StartsWith("35_")).Count() > 0 Then
                                    SqlFrom.Append(" LEFT JOIN OpportunityContact ACOpportunityContact ON OpportunityMaster.numOppID=ACOpportunityContact.numOppId ")
                                    SqlFrom.Append(" LEFT JOIN AdditionalContactsInformation ACAdditionalContactsInformation ON ACOpportunityContact.numContactId = ACAdditionalContactsInformation.numContactId ")
                                    SqlFrom.Append(" LEFT JOIN DivisionMaster ACDivisionMaster ON ACAdditionalContactsInformation.numDivisionId = ACDivisionMaster.numDivisionId ")
                                    SqlFrom.Append(" LEFT JOIN CompanyInfo ACCompanyInfo ON ACDivisionMaster.numCompanyid = ACCompanyInfo.numCompanyid ")
                                End If

                                'IF SERIAL NO IS SELECTED AS FIELD IN REPORT THEN AND ONLY THEN WE REPLACE ##SERIALNO### WITH JOIN TABLES ELSE CLEAR PLACEHOLDER
                                SqlFrom.Append(" ##SERIALNO## ")

                                SqlCondition.Append(" AND OpportunityMaster.numDomainId = @numDomainID AND Item.numDomainId = @numDomainID")

                                If IsAddCreditMemoLogic Then
                                    SqlFrom.Append(" LEFT OUTER JOIN (SELECT DepositMaster.dtDepositDate,DepositMaster.vcReference,DepositMaster.numDepositId,DepositMaster.numDivisionID,DepositMaster.numDomainID,DepositMaster.tintDepositePage,DepositeDetails.numOppID,DepositMaster.monDepositAmount,DepositMaster.monAppliedAmount FROM DepositMaster LEFT OUTER JOIN DepositeDetails ON DepositeDetails.numDepositID = DepositMaster.numDepositId WHERE 1=1 AND DepositMaster.tintDepositePage = 3 ) AS T1 ON T1.numDivisionID = DivisionMaster.numDivisionID AND T1.numDomainID = DivisionMaster.numDomainID ")
                                End If

                                If objReport.RecordFilter = 1 Then 'Assigned-To/Record Owner
                                    SqlCondition.Append(" AND (OpportunityMaster.numRecOwner = @numUserCntID OR OpportunityMaster.numAssignedTo = @numUserCntID)")
                                ElseIf objReport.RecordFilter = 2 Then 'Team Scope
                                    SqlCondition.Append(" AND OpportunityMaster.numRecOwner in (select numContactID from AdditionalContactsInformation where numDomainID=@numDomainID and numTeam in (select distinct numTeam from UserTeams where numusercntid=@numUserCntID))")
                                ElseIf objReport.RecordFilter = 3 Then 'Territory Scope
                                    SqlCondition.Append(" AND DivisionMaster.numTerId in (select distinct numTerritoryID from UserTerritory where numusercntid=@numUserCntID)")
                                ElseIf objReport.RecordFilter = 4 Then 'Assigned-To
                                    SqlCondition.Append(" AND OpportunityMaster.numAssignedTo = @numUserCntID")
                                ElseIf objReport.RecordFilter = 5 Then 'Record Owner
                                    SqlCondition.Append(" AND OpportunityMaster.numRecOwner = @numUserCntID")
                                End If

                            Case 19 'Items
                                SqlFrom.Append(" Item  ")
                                SqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")
                                SqlFrom.Append(" LEFT JOIN LATERAL (SELECT SUM(numOnHand) numOnHand,SUM(numAllocation) numAllocation,SUM(numOnOrder) numOnOrder,SUM(numBackOrder) numBackOrder,SUM(numReorder) numReorder,SUM(numOnHand) + SUM(numAllocation) numAvailable FROM WarehouseItems WHERE WarehouseItems.numItemID=Item.numItemCode) WareHouseItems ON TRUE ")
                                SqlCondition.Append(" AND Item.numDomainId = @numDomainID")

                            Case 23 'Items WareHouse
                                SqlFrom.Append(" Item  ")
                                SqlFrom.Append(" Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode ")
                                SqlFrom.Append("  join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and Item.numItemCode=WareHouseItems.numItemID")
                                SqlFrom.Append(" LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID ")
                                SqlFrom.Append("  join Warehouses Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID=WareHouseItems.numWareHouseID")

                                SqlCondition.Append(" AND Item.numDomainId = @numDomainID")

                            Case 14 'Projects
                                SqlFrom.Append(" ProjectsMaster left join DivisionMaster on ProjectsMaster.numDivisionId=DivisionMaster.numDivisionId")
                                SqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid")

                                SqlCondition.Append(" AND ProjectsMaster.numDomainId = @numDomainID")

                                If objReport.RecordFilter = 1 Then 'Assigned-To/Record Owner
                                    SqlCondition.Append(" AND (ProjectsMaster.numRecOwner = @numUserCntID OR ProjectsMaster.numAssignedTo = @numUserCntID)")
                                ElseIf objReport.RecordFilter = 2 Then 'Team Scope
                                    SqlCondition.Append(" AND ProjectsMaster.numRecOwner in (select numContactID from AdditionalContactsInformation where numDomainID=@numDomainID and numTeam in (select distinct numTeam from UserTeams where numusercntid=@numUserCntID))")
                                ElseIf objReport.RecordFilter = 3 Then 'Territory Scope
                                    SqlCondition.Append(" AND DivisionMaster.numTerId in (select distinct numTerritoryID from UserTerritory where numusercntid=@numUserCntID)")
                                ElseIf objReport.RecordFilter = 4 Then 'Assigned-To
                                    SqlCondition.Append(" AND ProjectsMaster.numAssignedTo = @numUserCntID")
                                ElseIf objReport.RecordFilter = 5 Then 'Record Owner
                                    SqlCondition.Append(" AND ProjectsMaster.numRecOwner = @numUserCntID")
                                End If

                            Case 16 'Cases
                                SqlFrom.Append(" Cases left join DivisionMaster on Cases.numDivisionId=DivisionMaster.numDivisionId ")
                                SqlFrom.Append(" left join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid ")

                                SqlCondition.Append(" AND Cases.numDomainId = @numDomainID")

                                If objReport.RecordFilter = 1 Then 'Assigned-To/Record Owner
                                    SqlCondition.Append(" AND (Cases.numRecOwner = @numUserCntID OR Cases.numAssignedTo = @numUserCntID)")
                                ElseIf objReport.RecordFilter = 2 Then 'Team Scope
                                    SqlCondition.Append(" AND Cases.numRecOwner in (select numContactID from AdditionalContactsInformation where numDomainID=@numDomainID and numTeam in (select distinct numTeam from UserTeams where numusercntid=@numUserCntID))")
                                ElseIf objReport.RecordFilter = 3 Then 'Territory Scope
                                    SqlCondition.Append(" AND DivisionMaster.numTerId in (select distinct numTerritoryID from UserTerritory where numusercntid=@numUserCntID)")
                                ElseIf objReport.RecordFilter = 4 Then 'Assigned-To
                                    SqlCondition.Append(" AND Cases.numAssignedTo = @numUserCntID")
                                ElseIf objReport.RecordFilter = 5 Then 'Record Owner
                                    SqlCondition.Append(" AND Cases.numRecOwner = @numUserCntID")
                                End If
                            Case 25 'Stock Transfer
                                SqlFrom.Append(" (")
                                SqlFrom.Append(" SELECT dtTransferredDate,numFromWarehouseItemID,numToWarehouseItemID,numQty,numFromQtyBefore,numFromQtyAfter,numToQtyBefore,numToQtyAfter FROM MassStockTransfer WHERE MassStockTransfer.numDomainId = @numDomainID")
                                SqlFrom.Append(" UNION")
                                SqlFrom.Append(" SELECT bintCreatedDate ,numWarehouseItmsID AS numFromWarehouseItemID,numToWarehouseItemID,numUnitHour AS numQty,0 AS numFromQtyBefore,0 AS numFromQtyAfter,0 AS numToQtyBefore,0 AS numToQtyAfter FROM OpportunityMaster INNER JOIN	OpportunityItems ON	OpportunityMaster.numOppId = OpportunityItems.numOppId WHERE OpportunityMaster.numDomainId = @numDomainID AND OpportunityMaster.tintOppType=2 AND OpportunityMaster.bitStockTransfer=true AND numUnitHour = numUnitHourReceived")
                                SqlFrom.Append(" ) AS MassStockTransfer")
                                SqlFrom.Append(" INNER JOIN WarehouseItems WarehouseItemsFrom ON WarehouseItemsFrom.numWarehouseItemID=MassStockTransfer.numFromWarehouseItemID")
                                SqlFrom.Append(" INNER JOIN Item ON WarehouseItemsFrom.numItemID=Item.numItemCode LEFT JOIN Vendor ON Vendor.numDomainId = @numDomainID AND Vendor.numVendorID = Item.numVendorID AND Item.numItemCode= Vendor.numItemCode")
                                SqlFrom.Append(" INNER JOIN Warehouses WarehousesFrom ON WarehouseItemsFrom.numWarehouseID=WarehousesFrom.numWarehouseID")
                                SqlFrom.Append(" LEFT JOIN WarehouseLocation WarehouseLocationFrom ON WarehouseItemsFrom.numWLocationID=WarehouseLocationFrom.numWLocationID")
                                SqlFrom.Append(" INNER JOIN WarehouseItems WarehouseItemsTo ON WarehouseItemsTo.numWarehouseItemID=MassStockTransfer.numToWarehouseItemID")
                                SqlFrom.Append(" INNER JOIN Item ItemTo ON WarehouseItemsTo.numItemID=ItemTo.numItemCode")
                                SqlFrom.Append(" INNER JOIN Warehouses WarehousesTo ON WarehouseItemsTo.numWarehouseID=WarehousesTo.numWarehouseID")
                                SqlFrom.Append(" LEFT JOIN WarehouseLocation WarehouseLocationTo ON WarehouseItemsTo.numWLocationID=WarehouseLocationTo.numWLocationID")
                            Case 26 'Activities
                                SqlFrom.Append(" Communication ")
                                SqlFrom.Append("Join AdditionalContactsInformation On Communication.numContactId = AdditionalContactsInformation.numContactId  ")
                                SqlFrom.Append("Join DivisionMaster On AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID ")
                                SqlFrom.Append("Join CompanyInfo  On DivisionMaster.numCompanyID = CompanyInfo.numCompanyId ")
                                SqlFrom.Append("Left OUTER JOIN Correspondence ON Correspondence.numCommID=Communication.numCommId ")
                                SqlFrom.Append("Left JOIN CommunicationLinkedOrganization ON Communication.numCommId = CommunicationLinkedOrganization.numCommID ")
                                SqlCondition.Append(" AND Communication.numDomainId = @numDomainID ")
                                SqlCondition.Append(" AND (Communication.numAssign =@numUserCntID Or Communication.numCreatedBy=@numUserCntID)  And Communication.bitclosedflag=false ")

                        End Select

                        Dim strFiledList As New List(Of String)

                        Dim lstColumnList As IEnumerable(Of String) = Nothing
                        lstColumnList = strColumnList.Distinct(StringComparer.OrdinalIgnoreCase)

                        If objReport.ReportFormatType = 5 Then
                            Dim i As Integer = 1
                            For Each FieldName As String In lstColumnList
                                Dim numReportFieldGroupID As Long = CCommon.ToLong(FieldName.Split("_")(0))
                                Dim numFieldID As Long = CCommon.ToLong(FieldName.Split("_")(1))
                                Dim bitCustom As Boolean = CCommon.ToBool(FieldName.Split("_")(2))

                                Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & numReportFieldGroupID & " AND numFieldID=" & numFieldID & " AND bitCustom=" & bitCustom)

                                If dr.Length > 0 Then
                                    If dr(0)("numReportFieldGroupID") = 35 AndAlso Not CCommon.ToString(dr(0)("vcLookBackTableName")).StartsWith("AC") Then
                                        dr(0)("vcLookBackTableName") = "AC" & dr(0)("vcLookBackTableName")
                                    End If

                                    SqlKPIGroupBy.Append(If(i > 1, ",", "") & CCommon.ToString(dr(0)("vcLookBackTableName")) & "." & CCommon.ToString(dr(0)("vcOrigDbColumnName")))
                                    i += 1
                                End If
                            Next
                        End If

                        'Column List Creation
                        For Each FieldName As String In lstColumnList
                            Dim numReportFieldGroupID As Long = CCommon.ToLong(FieldName.Split("_")(0))
                            Dim numFieldID As Long = CCommon.ToLong(FieldName.Split("_")(1))
                            Dim bitCustom As Boolean = CCommon.ToBool(FieldName.Split("_")(2))

                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & numReportFieldGroupID & " AND numFieldID=" & numFieldID & " AND bitCustom=" & bitCustom)

                            If dr.Length > 0 Then
                                If dr(0)("numReportFieldGroupID") = 35 AndAlso Not CCommon.ToString(dr(0)("vcLookBackTableName")).StartsWith("AC") Then
                                    dr(0)("vcLookBackTableName") = "AC" & dr(0)("vcLookBackTableName")
                                End If

                                If CCommon.ToBool(dr(0)("bitCustom")) = False Then
                                    If objReport.ReportFormatType = 3 Then 'KPI
                                        If dr(0)("numReportFieldGroupID") = 3 AndAlso (dr(0)("vcOrigDbColumnName") = "monDealAmount" Or dr(0)("vcOrigDbColumnName") = "monAmountPaid") AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            SqlFrom.Append(" Join OpportunityBizDocs on OpportunityMaster.numOppId = OpportunityBizDocs.numOppId AND COALESCE(OpportunityBizDocs.bitAuthoritativeBizDocs,0)=1")
                                        End If

                                    ElseIf dr(0)("vcOrigDbColumnName") = "ItemRequiredDate" Then

                                        strFiledList.Add(String.Format("FormatedDateFromDate(OpportunityItems.ItemRequiredDate,@numDomainId) as ""{0}""", FieldName))

                                    ElseIf dr(0)("vcOrigDbColumnName") = "numPendingToBill" Then
                                        strFiledList.Add(String.Format("COALESCE((COALESCE(OpportunityItems.numUnitHourReceived,0)) - " &
                                                 "(COALESCE((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId" &
                                                " WHERE OpportunityBizDocs.numOppId= OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID=OpportunityItems.numoppitemtCode " &
                                                " AND COALESCE(bitAuthoritativeBizDocs,0)=1), 0)),0) as ""{0}""", FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "numPendingtoReceive" Then
                                        strFiledList.Add(String.Format(" COALESCE((COALESCE((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId" &
                                                        " WHERE OpportunityBizDocs.numOppId= OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID=OpportunityItems.numoppitemtCode " &
                                                        " AND COALESCE(bitAuthoritativeBizDocs,0)=1), 0)) - (COALESCE(OpportunityItems.numUnitHourReceived,0)),0)as ""{0}""", FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "numOrdered" Then
                                        strFiledList.Add(String.Format(" COALESCE(COALESCE(fn_UOMConversion(COALESCE(Item.numBaseUnit, 0), OpportunityItems.numItemCode,Item.numDomainId, COALESCE(OpportunityItems.numUOMId, 0)),1) * OpportunityItems.numUnitHour,0)as ""{0}""", FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "monPendingSales" Then
                                        strFiledList.Add(String.Format(" COALESCE((COALESCE(COALESCE(fn_UOMConversion(COALESCE(Item.numBaseUnit, 0), OpportunityItems.numItemCode,Item.numDomainId, COALESCE(OpportunityItems.numUOMId, 0)),1) * OpportunityItems.numUnitHour,0) -COALESCE(OpportunityItems.numQtyShipped,0) ) * (COALESCE(OpportunityItems.monPrice,'0')),0) as ""{0}""", FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "vcInvoiced" Then
                                        strFiledList.Add(String.Format(" COALESCE((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numOppId= OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID=OpportunityItems.numoppitemtCode AND numBizDocId=287), 0) as ""{0}""", FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "vcBilled" Then
                                        strFiledList.Add(String.Format(" COALESCE((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numOppId= OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID=OpportunityItems.numoppitemtCode AND COALESCE(bitAuthoritativeBizDocs,0)=1), 0) as ""{0}""", FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "vcInventoryStatus" Then
                                        strFiledList.Add(String.Format(" COALESCE(CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,0::SMALLINT),'') as ""{0}""", FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "tintPriceLevel" AndAlso dr(0)("vcLookBackTableName") = "DivisionMaster" Then
                                        strFiledList.Add(String.Format(" GetOrganizationPriceLevelName(DivisionMaster.numDomainID,DivisionMaster.tintPriceLevel) as ""{0}""", FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "vcBackordered" Then
                                        strFiledList.Add(String.Format(" (CASE WHEN OpportunityMaster.tintOppType = 1 AND OpportunityMaster.tintOppStatus = 1 THEN (CASE WHEN (Item.charItemType = 'P' AND COALESCE(OpportunityItems.bitDropShip, false) = false AND COALESCE(Item.bitAsset,false)=false) THEN CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,OpportunityItems.numUnitHour,OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN COALESCE(Item.bitKitParent,false) = true AND COALESCE(Item.bitAssembly,false) = true THEN false WHEN COALESCE(Item.bitKitParent,false) = true THEN true ELSE false END)) ELSE 0 END) ELSE 0 END) as ""{0}""", FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "vcOpenBalance" Then
                                        strFiledList.Add(String.Format("(CASE WHEN OpportunityMaster.tintOppType=2 THEN (COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numUnitHourReceived,0)) ELSE (COALESCE(OpportunityItems.numUnitHour,0) - COALESCE(OpportunityItems.numQtyShipped,0)) END) as ""{0}""", FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "numPartner" Then
                                        strFiledList.Add(String.Format(" COALESCE((SELECT D.vcPartnerCode+' - '+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=OpportunityMaster.numPartner), '-') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "OppMileStoneCompleted" Then
                                        strFiledList.Add(String.Format(" COALESCE((select vcMileStoneName from StagePercentageDetails where StagePercentageDetails.numOppID=OpportunityMaster.numOppID and StagePercentageDetails.tinProgressPercentage=100 order by StagePercentageDetails.bintModifiedDate DESC LIMIT 1), '-') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "OppStageCompleted" Then
                                        strFiledList.Add(String.Format(" COALESCE((select vcMileStoneName || ' - ' || vcStageName from StagePercentageDetails where StagePercentageDetails.numOppID=OpportunityMaster.numOppID and StagePercentageDetails.tinProgressPercentage=100 order by StagePercentageDetails.bintModifiedDate DESC LIMIT 1), '-') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "intTotalProgress" Then
                                        strFiledList.Add(String.Format(" COALESCE((select intTotalProgress from ProjectProgress where ProjectProgress.numProId=OpportunityMaster.numOppID LIMIT 1), '-') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                    ElseIf objReport.ReportFormatType = 4 Then 'ScoreCard
                                        If dr(0)("numReportFieldGroupID") = 3 AndAlso (dr(0)("vcOrigDbColumnName") = "monDealAmount" Or dr(0)("vcOrigDbColumnName") = "monAmountPaid") AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            SqlFrom.Append(" Join OpportunityBizDocs on OpportunityMaster.numOppId = OpportunityBizDocs.numOppId AND COALESCE(OpportunityBizDocs.bitAuthoritativeBizDocs,0)=1")
                                        End If
                                    ElseIf (dr(0)("vcOrigDbColumnName") = "vcStreet" Or dr(0)("vcOrigDbColumnName") = "vcCity" Or dr(0)("vcOrigDbColumnName") = "numState" Or dr(0)("vcOrigDbColumnName") = "vcPostalCode" Or dr(0)("vcOrigDbColumnName") = "numCountry") Then
                                        If dr(0)("vcDbColumnName").ToString.ToLower.Contains("ship") Or dr(0)("vcDbColumnName").ToString.ToLower.Contains("bill") Then
                                            Dim strAlias As String = IIf(dr(0)("vcDbColumnName").ToString.ToLower.Contains("ship"), "AD2", "AD1")

                                            If CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 13 _
                                                Or CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 10 _
                                                Or CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 8 _
                                                Or CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 9 Then
                                                If Not SqlFrom.ToString.Contains(strAlias) Then
                                                    SqlFrom.Append(String.Format(" LEFT JOIN LATERAL fn_getOPPAddressDetails(OpportunityMaster.numOppID,OpportunityMaster.numDomainId,{1}::SMALLINT) AS {0} ON TRUE " _
                                                                  , strAlias, IIf(strAlias = "AD2", 2, 1)))
                                                End If

                                                If dr(0)("vcOrigDbColumnName") = "numState" Then
                                                    strFiledList.Add(String.Format("COALESCE({2}.{1},'') as ""{0}""",
                                                                                                             FieldName, "vcState", strAlias))
                                                ElseIf dr(0)("vcOrigDbColumnName") = "numCountry" Then
                                                    strFiledList.Add(String.Format("COALESCE({2}.{1},'') as ""{0}""",
                                                                                                             FieldName, "vcCountry", strAlias))
                                                Else
                                                    strFiledList.Add(String.Format("COALESCE({2}.{1},'') as ""{0}""",
                                                                                                             FieldName, dr(0)("vcOrigDbColumnName"), strAlias))
                                                End If

                                            Else
                                                If Not SqlFrom.ToString.Contains("AddressDetails " & strAlias) Then
                                                    SqlFrom.Append(String.Format(" left Join AddressDetails {0} on {0}.numDomainID=DivisionMaster.numDomainID and {0}.numRecordID=DivisionMaster.numDivisionID" &
                                                                   " and {0}.tintAddressOf=2 AND {0}.tintAddressType={1}", strAlias, IIf(strAlias = "AD2", 2, 1)))
                                                End If

                                                If dr(0)("vcOrigDbColumnName") = "numState" Then
                                                    SqlFrom.Append(String.Format(" left Join State S{0} on S{0}.numStateID={1}.numState", FieldName, strAlias))

                                                    strFiledList.Add(String.Format("COALESCE(S{0}.vcState,'') as ""{0}""", FieldName))
                                                ElseIf dr(0)("vcOrigDbColumnName") = "numCountry" Then
                                                    SqlFrom.Append(String.Format(" left Join ListDetails L{0} on L{0}.numListItemID={1}.numCountry", FieldName, strAlias))

                                                    strFiledList.Add(String.Format("COALESCE(L{0}.vcData,'') as ""{0}""", FieldName))
                                                Else
                                                    strFiledList.Add(String.Format("COALESCE({2}.{1},'') as ""{0}""",
                                                                                                             FieldName, dr(0)("vcOrigDbColumnName"), strAlias))
                                                End If
                                            End If
                                        Else
                                            Dim strAlias As String = "AD0"

                                            If Not SqlFrom.ToString.Contains("AddressDetails " & strAlias) Then
                                                SqlFrom.Append(String.Format(" left Join AddressDetails {0} on {0}.numDomainID=AdditionalContactsInformation.numDomainID and {0}.numRecordID=AdditionalContactsInformation.numContactID" &
                                                               " and {0}.tintAddressOf=1 AND {0}.tintAddressType=0", strAlias))
                                            End If

                                            If dr(0)("vcOrigDbColumnName") = "numState" Then
                                                SqlFrom.Append(String.Format(" left Join State S{0} on S{0}.numStateID={1}.numState", FieldName, strAlias))

                                                strFiledList.Add(String.Format("COALESCE(S{0}.vcState,'') as ""{0}""", FieldName))
                                            ElseIf dr(0)("vcOrigDbColumnName") = "numCountry" Then
                                                SqlFrom.Append(String.Format(" left Join ListDetails L{0} on L{0}.numListItemID={1}.numCountry", FieldName, strAlias))

                                                strFiledList.Add(String.Format("COALESCE(L{0}.vcData,'') as ""{0}""", FieldName))
                                            Else
                                                strFiledList.Add(String.Format("COALESCE({2}.{1},'') as ""{0}""",
                                                                                                         FieldName, dr(0)("vcOrigDbColumnName"), strAlias))
                                            End If
                                        End If

                                    ElseIf dr(0)("vcOrigDbColumnName") = "charSex" Then
                                        strFiledList.Add(String.Format("Case {0}.{1} When 'M' then 'Male' When 'F' then 'Female' else  '-' end as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                    ElseIf dr(0)("vcOrigDbColumnName") = "tintSource" Then
                                        strFiledList.Add(String.Format("COALESCE(fn_GetOpportunitySourceValue(COALESCE({0}.tintSource,0),COALESCE({0}.tintSourceType,0)::SMALLINT,{0}.numDomainID),'')" &
                                                                       " as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                    ElseIf dr(0)("vcOrigDbColumnName") = "tintOppType" Then
                                        strFiledList.Add(String.Format("Case When {0}.tintOppType=1 and {0}.tintOppStatus=1 then 'Sales Order'" &
                                                                             " When {0}.tintOppType=2 and {0}.tintOppStatus=1 then 'Purchase Order'" &
                                                                             " When {0}.tintOppType=1 and {0}.tintOppStatus IN (0,2) then 'Sales Opportunity'" &
                                                                             " When {0}.tintOppType=2 and {0}.tintOppStatus IN (0,2) then 'Purchase Opportunity' END" &
                                                                        " as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        'ElseIf dr(0)("vcOrigDbColumnName") = "CalAmount" AndAlso dr(0)("vcLookBackTableName") = "OpportunityMaster" Then
                                        '    strFiledList.Add(String.Format("{0}.monDealAmount as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                    ElseIf dr(0)("vcOrigDbColumnName") = "CustomerPartNo" AndAlso dr(0)("vcLookBackTableName") = "CustomerPartNumber" Then
                                        SqlFrom.Append(" LEFT JOIN CustomerPartNumber ON CustomerPartNumber.numItemCode = Item.numItemCode AND CustomerPartNumber.numCompanyId=CompanyInfo.numCompanyId")
                                        strFiledList.Add(String.Format("COALESCE({0}.{1},'') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                    ElseIf dr(0)("numReportFieldGroupID") = 7 AndAlso CCommon.ToString(dr(0)("vcOrigDbColumnName")).StartsWith("monPriceLevel") AndAlso dr(0)("vcLookBackTableName") = "PricingTable" Then
                                        If Not SqlFrom.ToString().Contains("CustomItemPriceTable") Then
                                            SqlFrom.Append(" LEFT JOIN LATERAL (SELECT * FROM fn_ItemPriceLevelPrice(Item.numItemCode,Item.monListPrice,Vendor.monCost)) CustomItemPriceTable ON TRUE ")
                                            'SqlFrom.Append(" LEFT JOIN vw_item_pricelevel CustomItemPriceTable ON CustomItemPriceTable.numdomainid = Item.numdomainid AND CustomItemPriceTable.numItemCode = Item.numItemCode ")
                                        End If

                                        strFiledList.Add(String.Format(" COALESCE(CustomItemPriceTable.monPriceLevel" & CCommon.ToString(dr(0)("vcOrigDbColumnName")).Replace("monPriceLevel", "") & ",'') as ""{0}""", FieldName))
                                    ElseIf dr(0)("numReportFieldGroupID") = 7 AndAlso (dr(0)("vcOrigDbColumnName") = "vcPriceLevelType" Or dr(0)("vcOrigDbColumnName") = "vcDiscountType") AndAlso dr(0)("vcLookBackTableName") = "PricingRuleTable" Then
                                        If Not SqlFrom.ToString().Contains("CustomItemPriceTable") Then
                                            SqlFrom.Append(" LEFT JOIN LATERAL (SELECT * FROM fn_ItemPriceLevelPrice(Item.numItemCode,Item.monListPrice,Vendor.monCost)) CustomItemPriceTable ON TRUE")
                                            'SqlFrom.Append(" LEFT JOIN vw_item_pricelevel CustomItemPriceTable ON CustomItemPriceTable.numdomainid = Item.numdomainid AND CustomItemPriceTable.numItemCode = Item.numItemCode")
                                        End If

                                        strFiledList.Add(String.Format(" COALESCE(CustomItemPriceTable." & CCommon.ToString(dr(0)("vcOrigDbColumnName")) & ",'') as ""{0}""", FieldName))
                                    ElseIf dr(0)("numReportFieldGroupID") = 12 AndAlso dr(0)("vcOrigDbColumnName") = "vcFromWareHouse" AndAlso dr(0)("vcLookBackTableName") = "Warehouses" Then
                                        strFiledList.Add(String.Format(" CONCAT(Item.vcItemName,', ',COALESCE(WarehousesFrom.vcWarehouse,''),(CASE WHEN LENGTH(COALESCE(WarehouseLocationFrom.vcLocation,'')) > 0 THEN CONCAT(' (',WarehouseLocationFrom.vcLocation,')') ELSE '' END)) as ""{0}""", FieldName))
                                    ElseIf dr(0)("numReportFieldGroupID") = 12 AndAlso dr(0)("vcOrigDbColumnName") = "vcToWareHouse" AndAlso dr(0)("vcLookBackTableName") = "Warehouses" Then
                                        strFiledList.Add(String.Format(" CONCAT(ItemTo.vcItemName,', ',COALESCE(WarehousesTo.vcWarehouse,''),(CASE WHEN LENGTH(COALESCE(WarehouseLocationTo.vcLocation,'')) > 0 THEN CONCAT(' (',WarehouseLocationTo.vcLocation,')') ELSE '' END)) as ""{0}""", FieldName))
                                    ElseIf dr(0)("numReportFieldGroupID") = 11 AndAlso dr(0)("vcOrigDbColumnName") = "numAvailable" AndAlso dr(0)("vcLookBackTableName") = "WareHouseItems" Then
                                        strFiledList.Add(String.Format(" COALESCE(WareHouseItems.numOnHand,0) + COALESCE(WareHouseItems.numAllocation,0) as ""{0}""", FieldName))
                                    ElseIf dr(0)("numReportFieldGroupID") = 11 AndAlso dr(0)("vcOrigDbColumnName") = "monAdjustedValue" AndAlso dr(0)("vcLookBackTableName") = "WareHouseItems" Then
                                        strFiledList.Add(String.Format(" ((COALESCE(WareHouseItems.numOnHand,0) + COALESCE(WareHouseItems.numAllocation,0)) * COALESCE(Item.monAverageCost,0)) as ""{0}""", FieldName))
                                    ElseIf dr(0)("numReportFieldGroupID") = 3 AndAlso dr(0)("vcOrigDbColumnName") = "DiscAmt" AndAlso dr(0)("vcLookBackTableName") = "OpportunityItems" Then
                                        strFiledList.Add(String.Format(" COALESCE((SELECT SUM(COALESCE(OIInner.monTotAmtBefDiscount,0) - COALESCE(OIInner.monTotAmount,0)) FROM OpportunityItems OIInner WHERE  OIInner.numOppId = OpportunityMaster.numOppId),0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                    ElseIf dr(0)("numReportFieldGroupID") = 4 AndAlso dr(0)("vcOrigDbColumnName") = "DiscAmt" AndAlso dr(0)("vcLookBackTableName") = "OpportunityItems" Then
                                        strFiledList.Add(String.Format(" CASE WHEN OpportunityItems.fltDiscount > 0 THEN CONCAT(COALESCE(OpportunityItems.monTotAmtBefDiscount,0) - COALESCE(OpportunityItems.monTotAmount,0),(CASE WHEN OpportunityItems.bitDiscountType=false THEN CONCAT(' (',CAST(OpportunityItems.fltDiscount AS FLOAT),'%)') ELSE '' END)) ELSE '0' END  as ""{0}""", FieldName))
                                    ElseIf dr(0)("numReportFieldGroupID") = 3 AndAlso dr(0)("vcOrigDbColumnName") = "monDealAmount" AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        If CCommon.ToString(SqlFrom.ToString()).Contains("DepositMaster") Then
                                            strFiledList.Add(String.Format(" COALESCE((SELECT CASE WHEN T1.numOppId IS NOT NULL THEN SUM(COALESCE(monDealAmount,0)) ELSE T1.monDepositAmount END FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                            'ElseIf CCommon.ToString(SqlFrom.ToString()).ToLower().Contains("returnheader") Then
                                            '    strFiledList.Add(String.Format(" COALESCE((SELECT COALESCE(monAmount,0) FROM ReturnHeader RH WHERE RH.numOppId = OpportunityMaster.numOppId AND RH.tintReturnType IN (1,2)), 0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                        Else
                                            strFiledList.Add(String.Format(" COALESCE((SELECT SUM(COALESCE(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        End If

                                    ElseIf dr(0)("vcOrigDbColumnName") = "monAmount" AndAlso dr(0)("vcLookBackTableName") = "ReturnHeader" Then
                                        strFiledList.Add(String.Format(" COALESCE((SELECT COALESCE(monAmount,0) FROM ReturnHeader RH WHERE RH.numReturnHeaderID = ReturnHeader.numReturnHeaderID AND RH.tintReturnType IN (1,2)), 0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                    ElseIf (dr(0)("numReportFieldGroupID") = 3 Or dr(0)("numReportFieldGroupID") = 5) AndAlso dr(0)("vcOrigDbColumnName") = "vcBizDocID" AndAlso CCommon.ToString(SqlFrom.ToString()).ToLower().Contains("returnheader") Then
                                        strFiledList.Add(String.Format(" COALESCE(ReturnHeader.vcRMA,'') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                    ElseIf dr(0)("numReportFieldGroupID") = 3 AndAlso dr(0)("vcOrigDbColumnName") = "monAmountPaid" AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        If CCommon.ToString(SqlFrom.ToString()).Contains("DepositMaster") Then
                                            strFiledList.Add(String.Format(" COALESCE((SELECT CASE WHEN T1.numOppId IS NOT NULL THEN SUM(COALESCE(monAmountPaid,0)) ELSE T1.monAppliedAmount END  FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                        ElseIf CCommon.ToString(SqlFrom.ToString()).ToLower().Contains("returnheader") Then
                                            strFiledList.Add(String.Format(" COALESCE((SELECT COALESCE(monAmount,0) FROM ReturnHeader RH WHERE RH.numOppId = OpportunityMaster.numOppId AND RH.tintReturnType IN (1,2)), 0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                        Else
                                            strFiledList.Add(String.Format(" COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        End If

                                    ElseIf dr(0)("numReportFieldGroupID") = 5 AndAlso (dr(0)("vcOrigDbColumnName") = "monDealAmount" Or dr(0)("vcOrigDbColumnName") = "monAmountPaid") AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        If CCommon.ToString(SqlFrom.ToString()).Contains("DepositMaster") Then
                                            strFiledList.Add(String.Format("((CASE WHEN T1.numOppId IS NOT NULL THEN {0}.{1} ELSE T1.monDepositAmount END) * COALESCE(OpportunityBizDocs.fltExchangeRateBizDoc,COALESCE(OpportunityMaster.fltExchangeRate,1))) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                        ElseIf CCommon.ToString(SqlFrom.ToString()).ToLower().Contains("returnheader") Then
                                            strFiledList.Add(String.Format("(ReturnHeader.monAmount * COALESCE(OpportunityBizDocs.fltExchangeRateBizDoc,COALESCE(OpportunityMaster.fltExchangeRate,1))) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                        Else
                                            strFiledList.Add(String.Format("({0}.{1} * COALESCE(OpportunityBizDocs.fltExchangeRateBizDoc,COALESCE(OpportunityMaster.fltExchangeRate,1))) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                        End If

                                    ElseIf dr(0)("numReportFieldGroupID") = 5 AndAlso dr(0)("vcOrigDbColumnName") = "monAmountDue" AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        strFiledList.Add(String.Format("((COALESCE(OpportunityBizDocs.monDealAmount,0) - COALESCE(OpportunityBizDocs.monAmountPaid,0)) * COALESCE(OpportunityBizDocs.fltExchangeRateBizDoc,COALESCE(OpportunityMaster.fltExchangeRate,1))) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                    ElseIf dr(0)("numReportFieldGroupID") = 25 AndAlso dr(0)("vcLookBackTableName") = "TaxItems" Then 'BizDocs Items Tax Field
                                        strFiledList.Add(String.Format("fn_CalBizDocTaxAmt(@numDomainId,{3},OpportunityMaster.numOppId,OpportunityItems.numoppitemtCode,1::SMALLINT,OpportunityBizDocItems.monTotAmount,OpportunityBizDocItems.numUnitHour) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName, dr(0)("numFieldID")))
                                    ElseIf dr(0)("vcLookBackTableName") = "OpportunityItems" AndAlso dr(0)("vcOrigDbColumnName") = "vcItemReleaseDate" Then
                                        strFiledList.Add(String.Format("(CASE WHEN OpportunityItems.ItemReleaseDate IS NULL THEN '' ELSE FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID) END) as ""{0}""", FieldName, dr(0)("vcDbColumnName")))
                                    ElseIf dr(0)("numReportFieldGroupID") = 26 AndAlso dr(0)("vcLookBackTableName") = "TaxItems" Then 'BizDocs Tax Field
                                        strFiledList.Add(String.Format("fn_CalOppItemTotalTaxAmt(@numDomainId,{3},OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName, dr(0)("numFieldID")))

                                    ElseIf dr(0)("vcOrigDbColumnName") = "numProjectID" AndAlso dr(0)("vcLookBackTableName") = "OpportunityItems" Then
                                        strFiledList.Add(String.Format("COALESCE(GetProjectName({1}.{2}),'') as ""{0}""",
                                                                   FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                    ElseIf dr(0)("vcLookBackTableName") = "chart_of_accounts" AndAlso dr(0)("numReportFieldGroupID") = 7 Then 'Income Account/Asset Account/COGS Account
                                        strFiledList.Add(String.Format("fn_GetChart_Of_AccountsName(Item.{1}) as ""{0}""", FieldName, dr(0)("vcDbColumnName")))
                                    ElseIf dr(0)("vcLookBackTableName") = "PricingRuleTable" Then
                                        If Not SqlFrom.ToString.Contains("PricingTable PT") Then
                                            SqlFrom.Append(" LEFT JOIN PricingTable PT ON COALESCE(PT.numItemCode,0)=Item.numItemCode")
                                        End If

                                        If dr(0)("vcOrigDbColumnName") = "decDiscount" Then
                                            strFiledList.Add(String.Format("COALESCE((CASE WHEN tintRuleType=3 THEN 0 ELSE decDiscount END),'0') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        ElseIf dr(0)("vcOrigDbColumnName") = "vcDiscountType" Then
                                            strFiledList.Add(String.Format("COALESCE((CASE WHEN (tintRuleType=1 OR tintRuleType=2) AND tintDiscountType=1 THEN 'Percentage' WHEN (tintRuleType=1 OR tintRuleType=2) AND tintDiscountType=2 THEN 'Flat' Else '' END),'') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        ElseIf dr(0)("vcOrigDbColumnName") = "monPriceLevelPrice" Then
                                            If CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 23 Then 'Items WareHouse
                                                strFiledList.Add(String.Format("COALESCE(CAST(CASE WHEN tintRuleType=1 AND tintDiscountType=1 " &
                                                " THEN COALESCE((CASE WHEN Item.charItemType = 'P' THEN monWListPrice ELSE monListPrice END),0) - (COALESCE((CASE WHEN Item.charItemType = 'P' THEN monWListPrice ELSE monListPrice END),0) * ( decDiscount /100)) " &
                                             " WHEN tintRuleType=1 AND tintDiscountType=2" &
                                                " THEN COALESCE((CASE WHEN Item.charItemType = 'P' THEN monWListPrice ELSE monListPrice END),0) - decDiscount" &
                                             " WHEN tintRuleType=2 AND tintDiscountType=1" &
                                                " THEN COALESCE(Vendor.monCost,0) + (COALESCE(Vendor.monCost,0) * ( decDiscount /100))" &
                                             " WHEN tintRuleType=2 AND tintDiscountType=2" &
                                                " THEN COALESCE(Vendor.monCost,0) + decDiscount" &
                                             " WHEN tintRuleType=3" &
                                                " THEN decDiscount" &
                                                " End AS DECIMAL(20,5)),0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                            Else 'Items
                                                strFiledList.Add(String.Format("COALESCE(CAST(CASE WHEN tintRuleType=1 AND tintDiscountType=1 " &
                                                " THEN COALESCE(monListPrice,0) - (COALESCE(monListPrice,0) * ( decDiscount /100)) " &
                                             " WHEN tintRuleType=1 AND tintDiscountType=2" &
                                                " THEN COALESCE(monListPrice,0) - decDiscount" &
                                             " WHEN tintRuleType=2 AND tintDiscountType=1" &
                                                " THEN COALESCE(Vendor.monCost,0) + (COALESCE(Vendor.monCost,0) * ( decDiscount /100))" &
                                             " WHEN tintRuleType=2 AND tintDiscountType=2" &
                                                " THEN COALESCE(Vendor.monCost,0) + decDiscount" &
                                             " WHEN tintRuleType=3" &
                                                " THEN decDiscount" &
                                                " End AS DECIMAL(20,5)),0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                            End If
                                        ElseIf dr(0)("vcOrigDbColumnName") = "vcPriceLevelType" Then
                                            strFiledList.Add(String.Format("'Item Price Level' as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        ElseIf dr(0)("vcOrigDbColumnName") = "intFromQty" Then
                                            strFiledList.Add(String.Format("COALESCE(PT.intFromQty,'0') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        ElseIf dr(0)("vcOrigDbColumnName") = "intToQty" Then
                                            strFiledList.Add(String.Format("COALESCE(PT.intToQty,'0') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        ElseIf dr(0)("vcOrigDbColumnName") = "vcRuleType" Then
                                            strFiledList.Add(String.Format("COALESCE(CASE PT.tintRuleType WHEN 1 THEN 'Deduct from List price' WHEN 2 THEN 'Add to Primary Vendor Cost' WHEN 3 THEN 'Named Price' END,'') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        End If
                                    ElseIf (dr(0)("vcAssociatedControlType") = "SelectBox" Or dr(0)("vcAssociatedControlType") = "ListBox") And dr(0)("vcListItemType") <> "" Then
                                        If dr(0)("vcListItemType") = "LI" Or dr(0)("vcListItemType") = "T" Then
                                            If CCommon.ToString(SqlFrom.ToString()).Contains("DepositMaster") Then
                                                strFiledList.Add(String.Format(" CASE WHEN T1.numOppId IS NOT NULL THEN COALESCE(L{0}.vcData,'') ELSE 'Standalone Credit Memo' END  as ""{0}""", FieldName))

                                            ElseIf CCommon.ToString(SqlFrom.ToString()).ToLower().Contains("returnheader") Then
                                                If CCommon.ToString(dr(0)("vcDbColumnName")) = "numBizDocId" Then
                                                    strFiledList.Add(String.Format(" 'RMA' as ""{0}""", FieldName))

                                                ElseIf CCommon.ToString(dr(0)("vcDbColumnName")) = "numBizDocStatus" Then
                                                    strFiledList.Add(String.Format(" ReturnHeader.vcBizDocName as ""{0}""", FieldName))
                                                Else
                                                    strFiledList.Add(String.Format(" COALESCE(L{0}.vcData,'') as ""{0}""", FieldName))
                                                End If
                                            Else
                                                strFiledList.Add(String.Format(" COALESCE(L{0}.vcData,'') as ""{0}""", FieldName))
                                            End If


                                            SqlFrom.Append(String.Format(" left Join ListDetails L{0} on L{0}.numListItemID={1}.{2}", FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "C" Then
                                            strFiledList.Add(String.Format("COALESCE(C{0}.vcCampaignName,'') as ""{0}""",
                                                                       FieldName))

                                            SqlFrom.Append(String.Format(" left Join CampaignMaster C{0} on C{0}.numCampaignId={1}.{2}",
                                                                         FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "AG" Then
                                            strFiledList.Add(String.Format("COALESCE(AG{0}.vcGrpName,'') as ""{0}""",
                                                                       FieldName))

                                            SqlFrom.Append(String.Format(" left Join Groups AG{0} on AG{0}.numGrpId={1}.{2}",
                                                                         FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "DC" Then
                                            strFiledList.Add(String.Format("COALESCE(DC{0}.vcECampName,'') as ""{0}""",
                                                                       FieldName))

                                            SqlFrom.Append(String.Format(" left Join ECampaign DC{0} on DC{0}.numECampaignID={1}.{2}",
                                                                         FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "U" Then
                                            strFiledList.Add(String.Format("COALESCE(fn_GetContactName({1}.{2}),'') as ""{0}""",
                                                                       FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "OC" Then
                                            strFiledList.Add(String.Format("COALESCE(fn_GetCurrencyName({1}.{2}),'') as ""{0}""",
                                                                       FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "UOM" Then
                                            strFiledList.Add(String.Format("COALESCE(fn_GetUOMName({1}.{2}),'') as ""{0}""",
                                                                       FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "IG" Then
                                            strFiledList.Add(String.Format("COALESCE(fn_GetItemGroupsName({1}.{2}),'') as ""{0}""",
                                                                       FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "PP" Then
                                            strFiledList.Add(String.Format("case when Item.charItemType='P' then 'Inventory Item' when Item.charItemType='S' then 'Service' when Item.charItemType='N' then 'Non-Inventory Item' end as ""{0}""",
                                                                       FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "SYS" Then
                                            strFiledList.Add(String.Format("case {1}.{2} when 1 then 'Prospect' when 2 then 'Account' else 'Lead' end as ""{0}""",
                                                                       FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "V" Then
                                            strFiledList.Add(String.Format("COALESCE(fn_GetComapnyName({1}.{2}),'') as ""{0}""",
                                                                       FieldName, dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName")))

                                        ElseIf dr(0)("vcListItemType") = "IC" Then
                                            strFiledList.Add(String.Format("COALESCE({0}.{1},0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        End If

                                    ElseIf dr(0)("numReportFieldGroupID") = 5 AndAlso dr(0)("vcOrigDbColumnName") = "dtFromDate" AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        If CCommon.ToString(SqlFrom.ToString()).Contains("DepositMaster") Then
                                            strFiledList.Add(" CASE WHEN OpportunityBizDocs.dtFromDate IS NOT NULL THEN FormatedDateFromDate(" & dr(0)("vcLookBackTableName") & "." & dr(0)("vcOrigDbColumnName") & ",@numDomainId) ELSE FormatedDateFromDate(T1.dtDepositDate,@numDomainId) END as """ & FieldName & """")
                                        Else
                                            strFiledList.Add(" FormatedDateFromDate(" & dr(0)("vcLookBackTableName") & "." & dr(0)("vcOrigDbColumnName") & ",@numDomainId) as """ & FieldName & """")
                                        End If
                                    ElseIf dr(0)("numReportFieldGroupID") = 35 AndAlso dr(0)("vcOrigDbColumnName") = "dtCreatedDate" AndAlso dr(0)("vcLookBackTableName") = "ReturnHeader" Then
                                        strFiledList.Add(" CAST(FormatedDateFromDate(" & dr(0)("vcLookBackTableName") & "." & dr(0)("vcOrigDbColumnName") & ",@numDomainId) AS DATE) as """ & FieldName & """")

                                    ElseIf dr(0)("numReportFieldGroupID") = 5 AndAlso dr(0)("vcOrigDbColumnName") = "dtDueDate" AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                        strFiledList.Add("FormatedDateFromDate(OpportunityBizDocs.dtFromDate + make_interval(days => COALESCE((SELECT numNetDueInDays::INT FROM BillingTerms WHERE numTermsID = COALESCE(OpportunityMaster.intBillingDays,0)), 0)),@numDomainId) as """ & FieldName & """")

                                    ElseIf dr(0)("vcAssociatedControlType") = "DateField" Then
                                        strFiledList.Add("FormatedDateFromDate(" & dr(0)("vcLookBackTableName") & "." & dr(0)("vcOrigDbColumnName") & " + make_interval(mins => -@ClientTimeZoneOffset),@numDomainId) as """ & FieldName & """")

                                    ElseIf dr(0)("vcAssociatedControlType") = "CheckBox" Then
                                        strFiledList.Add(String.Format("Case COALESCE({0}.{1}::INT,0) When 1 then 'Yes' else  'No' end as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                    Else
                                        If dr(0)("vcFieldDataType") = "M" Then
                                            strFiledList.Add(String.Format("COALESCE({0}.{1},0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                        Else
                                            If CCommon.ToString(dr(0)("vcOrigDbColumnName")) = "vcPoppName" AndAlso CCommon.ToString(SqlFrom.ToString()).Contains("DepositMaster") Then
                                                strFiledList.Add(" CASE WHEN T1.numOppId IS NOT NULL THEN " & String.Format("COALESCE({0}.{1},'') ELSE '' END as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                            ElseIf CCommon.ToString(dr(0)("vcOrigDbColumnName")) = "vcModelID" AndAlso CCommon.ToString(SqlFrom.ToString()).Contains("DepositMaster") Then
                                                strFiledList.Add(" CASE WHEN T1.numOppId IS NOT NULL THEN " & String.Format("COALESCE({0}.{1},'') ELSE '' END  as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))

                                            ElseIf dr(0)("vcFieldDataType") = "V" Then
                                                If dr(0)("vcDbColumnName").ToString().ToLower() = "vcserialno" Then
                                                    Dim tempReportModuleGroupID As Integer = CCommon.ToInteger(ds.Tables(0).Rows(0)("numReportModuleGroupID"))

                                                    'Opportunities BizDocs Items or Opportunity Items
                                                    If tempReportModuleGroupID = 9 Or tempReportModuleGroupID = 10 Then
                                                        SqlFrom = SqlFrom.Replace("##SERIALNO##", "LEFT JOIN OppWarehouseSerializedItem ON OpportunityItems.numoppitemtCode = OppWarehouseSerializedItem.numOppItemID LEFT JOIN WareHouseItmsDTL ON OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID")
                                                        strFiledList.Add(String.Format("COALESCE({0}.{1},'') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                                    ElseIf tempReportModuleGroupID = 19 Then
                                                        'Items
                                                        strFiledList.Add(String.Format("COALESCE((SELECT string_agg( WareHouseItmsDTL.vcSerialNo,', ') FROM WareHouseItmsDTL WHERE WareHouseItmsDTL.numWareHouseItemID IN (SELECT COALESCE(numWareHouseItemID,-1) FROM WarehouseItems WHERE numItemID = Item.numItemCode) AND WareHouseItmsDTL.numQty > 0),'') as ""{0}""", FieldName))
                                                    ElseIf tempReportModuleGroupID = 23 Then
                                                        'Warehouse
                                                        strFiledList.Add(String.Format("COALESCE((SELECT string_agg( WareHouseItmsDTL.vcSerialNo,', ') FROM WareHouseItmsDTL WHERE WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID AND WareHouseItmsDTL.numQty > 0),'') as ""{0}""", FieldName))
                                                    Else
                                                        strFiledList.Add(String.Format("COALESCE({0}.{1},'') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                                    End If
                                                Else
                                                    strFiledList.Add(String.Format("COALESCE({0}.{1},'') as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                                End If
                                            Else
                                                strFiledList.Add(String.Format("COALESCE({0}.{1},0) as ""{2}""", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), FieldName))
                                            End If

                                        End If
                                    End If
                                Else 'Custom Fields
                                    If dr(0)("vcLookBackTableName") = "CFW_FLD_Values_Item" Then
                                        If (CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 10 Or CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 9) Then
                                            strFiledList.Add(String.Format("COALESCE(GetCustFldValueOppItems({0},OpportunityItems.numoppitemtCode,Item.numItemCode),'') as ""{1}""", dr(0)("numFieldID"), FieldName, dr(0)("vcLookBackTableName")))
                                        Else
                                            strFiledList.Add(String.Format("COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,Item.numItemCode)),'') as ""{1}""", dr(0)("numFieldID"), FieldName, dr(0)("vcLookBackTableName")))
                                        End If
                                    Else
                                        Select Case dr(0)("vcLookBackTableName")
                                            Case "CFW_FLD_Values"
                                                strFiledList.Add(String.Format("COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID)),'') as ""{1}""", dr(0)("numFieldID"), FieldName))
                                            Case "CFW_FLD_Values_Cont"
                                                strFiledList.Add(String.Format("COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,AdditionalContactsInformation.numContactID)),'') as ""{1}""", dr(0)("numFieldID"), FieldName))
                                            Case "CFW_FLD_Values_Case"
                                                strFiledList.Add(String.Format("COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,Cases.numCaseId)),'') as ""{1}""", dr(0)("numFieldID"), FieldName))
                                            Case "CFW_Fld_Values_Opp"
                                                strFiledList.Add(String.Format("COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,OpportunityMaster.numOppID)),'') as ""{1}""", dr(0)("numFieldID"), FieldName))
                                            Case "CFW_FLD_Values_Pro"
                                                strFiledList.Add(String.Format("COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,ProjectsMaster.numProID)),'') as ""{1}""", dr(0)("numFieldID"), FieldName))
                                            Case "CFW_Fld_Values_Serialized_Items"
                                                strFiledList.Add(String.Format("COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,Item.numItemCode)),'') as ""{1}""", dr(0)("numFieldID"), FieldName))
                                        End Select
                                    End If
                                End If
                            End If
                        Next

                        If objReport.ReportFormatType <> 5 AndAlso strFiledList.Count > 0 Then
                            SqlColumn.Append(String.Join(",", strFiledList))
                        End If

                        Dim tempStringColumns As New List(Of String)
                        ' Dim tempStringColumns As String = SqlColumn.ToString()

                        'Sales/Purchase Order
                        If SqlColumn.ToString().ToLower().IndexOf("vcpoppname") > 0 Then
                            tempStringColumns.Add(String.Format("COALESCE(OpportunityMaster.numOppId,0) AS OpID"))
                        End If
                        'Organization Name
                        If SqlColumn.ToString().ToLower().Contains("vccompanyname") Then
                            tempStringColumns.Add(String.Format("COALESCE(DivisionMaster.numDivisionId,0) AS DivID"))
                        End If
                        If SqlColumn.ToString().ToLower().Contains("vcitemname") Then
                            tempStringColumns.Add(String.Format("COALESCE(Item.numItemCode, 0) AS ItemCode"))
                        End If
                        If SqlColumn.ToString().ToLower().Contains("vcbizdocid") Or SqlColumn.ToString().ToLower().Contains("vcRMA") Then
                            'tempStringColumns.Add(String.Format("COALESCE(OpportunityBizDocs.numOppBizDocsId,0) AS BizDocId"))
                            tempStringColumns.Add(String.Format("COALESCE(OpportunityBizDocs.numOppId,0) AS BizDocId"))
                        End If
                        If SqlColumn.ToString().ToLower().Contains("vcbizdocid") AndAlso Not SqlColumn.ToString().ToLower().Contains("vcpoppname") Then
                            tempStringColumns.Add(String.Format("COALESCE(OpportunityBizDocs.numOppId,0) AS OpID"))
                        End If
                        If SqlColumn.ToString().ToLower().Contains("vccasenumber") Then
                            tempStringColumns.Add(String.Format("COALESCE(Cases.numCaseId,0) as CaseID"))
                        End If

                        If tempStringColumns.Count > 0 Then
                            SqlColumn.Append("," & String.Join(",", tempStringColumns))
                        End If

                        'IF PLACEHOLDER IS NOT REPLACED THAN CLEAR IF
                        SqlFrom = SqlFrom.Replace("##SERIALNO##", "")

                        'Filter Condition Creation
                        If objReport.filters.Count > 0 Then
                            Dim strAdvFilter As String = ""

                            'For Filter Logic
                            If objReport.FilterLogic.Trim.Length > 0 Then
                                Dim iRepalace As Integer
                                For iRepalace = 1 To objReport.filters.Count
                                    If iRepalace = 1 Then
                                        strAdvFilter = Text.RegularExpressions.Regex.Replace(objReport.FilterLogic, "\b" & iRepalace & "\b", "|" & iRepalace & "|")
                                    Else : strAdvFilter = Text.RegularExpressions.Regex.Replace(strAdvFilter, "\b" & iRepalace & "\b", "|" & iRepalace & "|")
                                    End If
                                Next
                            End If

                            Dim iFilterCount As Integer = 1
                            Dim tempCondition As New List(Of String)

                            For Each str As filterObject In objReport.filters

                                Dim numReportFieldGroupID As Long = CCommon.ToLong(str.Column.Split("_")(0))
                                Dim numFieldID As Long = CCommon.ToLong(str.Column.Split("_")(1))
                                Dim bitCustom As Boolean = CCommon.ToBool(str.Column.Split("_")(2))

                                Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & numReportFieldGroupID & " AND numFieldID=" & numFieldID & " AND bitCustom=" & bitCustom)

                                If dr.Length > 0 Then
                                    If dr(0)("numReportFieldGroupID") = 35 AndAlso Not CCommon.ToString(dr(0)("vcLookBackTableName")).StartsWith("AC") Then
                                        dr(0)("vcLookBackTableName") = "AC" & dr(0)("vcLookBackTableName")
                                    End If

                                    If CCommon.ToBool(dr(0)("bitCustom")) = False Then
                                        If (dr(0)("vcOrigDbColumnName") = "vcStreet" Or dr(0)("vcOrigDbColumnName") = "vcCity" Or dr(0)("vcOrigDbColumnName") = "numState" Or dr(0)("vcOrigDbColumnName") = "vcPostalCode" Or dr(0)("vcOrigDbColumnName") = "numCountry") Then
                                            If dr(0)("vcDbColumnName").ToString.ToLower.Contains("ship") Or dr(0)("vcDbColumnName").ToString.ToLower.Contains("bill") Then
                                                Dim strAlias As String = IIf(dr(0)("vcDbColumnName").ToString.ToLower.Contains("ship"), "AD2", "AD1")

                                                If CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 13 _
                                           Or CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 10 _
                                           Or CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 8 _
                                           Or CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 9 Then
                                                    If Not SqlFrom.ToString.Contains(strAlias) Then
                                                        SqlFrom.Append(String.Format(" LEFT JOIN LATERAL fn_getOPPAddressDetails(OpportunityMaster.numOppID,OpportunityMaster.numDomainId,{1}::SMALLINT) AS {0} ON TRUE " _
                                                                  , strAlias, IIf(strAlias = "AD2", 2, 1)))
                                                    End If

                                                    tempCondition.Add(String.Format("{0}.{1} {2}",
                                                                   strAlias, dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))

                                                Else

                                                    If Not SqlFrom.ToString.Contains("AddressDetails " & strAlias) Then
                                                        SqlFrom.Append(String.Format(" left Join AddressDetails {0} on {0}.numDomainID=DivisionMaster.numDomainID and {0}.numRecordID=DivisionMaster.numDivisionID" &
                                                                   " and {0}.tintAddressOf=2 AND {0}.tintAddressType={1}", strAlias, IIf(strAlias = "AD2", 2, 1)))
                                                    End If

                                                    tempCondition.Add(String.Format("{0}.{1} {2}",
                                                                   strAlias, dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                                End If

                                            Else
                                                Dim strAlias As String = "AD0"

                                                If Not SqlFrom.ToString.Contains("AddressDetails " & strAlias) Then
                                                    SqlFrom.Append(String.Format(" left Join AddressDetails {0} on {0}.numDomainID=AdditionalContactsInformation.numDomainID and {0}.numRecordID=AdditionalContactsInformation.numContactID" &
                                                               " and {0}.tintAddressOf=1 AND {0}.tintAddressType=0", strAlias))
                                                End If

                                                tempCondition.Add(String.Format("{0}.{1} {2}",
                                                              strAlias, dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                            End If
                                        ElseIf dr(0)("vcOrigDbColumnName") = "tintOppType" Then
                                            Dim strValue() As String = str.ColValue.Trim().Split(",")
                                            Dim tempCondition1 As New List(Of String)

                                            For i As Integer = 0 To strValue.Length - 1
                                                Select Case strValue(i)
                                                    Case 1 'Sales Order
                                                        tempCondition1.Add(String.Format("({0}.tintOppType=1 and {0}.tintOppStatus=1)", dr(0)("vcLookBackTableName")))
                                                    Case 2 'Purchase Order
                                                        tempCondition1.Add(String.Format("({0}.tintOppType=2 and {0}.tintOppStatus=1)", dr(0)("vcLookBackTableName")))
                                                    Case 3 'Sales Opportunity
                                                        tempCondition1.Add(String.Format("({0}.tintOppType=1 and {0}.tintOppStatus IN (0,2))", dr(0)("vcLookBackTableName")))
                                                    Case 4 'Purchase Opportunity
                                                        tempCondition1.Add(String.Format("({0}.tintOppType=2 and {0}.tintOppStatus IN (0,2))", dr(0)("vcLookBackTableName")))
                                                End Select
                                            Next

                                            tempCondition.Add("(" & String.Join(" OR ", tempCondition1) & ")")
                                        ElseIf dr(0)("vcOrigDbColumnName") = "tintSource" Then
                                            Dim strValue() As String = str.ColValue.Trim().Split(",")
                                            Dim tempCondition1 As New List(Of String)

                                            For i As Integer = 0 To strValue.Length - 1
                                                Dim strValue1() As String = strValue(i).Split("~")
                                                If strValue1.Length = 2 Then
                                                    tempCondition1.Add(String.Format("({0}.tintSource={1} and {0}.tintSourceType={2})", dr(0)("vcLookBackTableName"), strValue1(0), strValue1(1)))
                                                End If
                                            Next

                                            tempCondition.Add("(" & String.Join(" OR ", tempCondition1) & ")")
                                        ElseIf dr(0)("vcOrigDbColumnName") = "vcInventoryStatus" Then
                                            Dim strValue() As String = str.ColValue.Trim().Split(",")
                                            Dim tempCondition1 As New List(Of String)

                                            For i As Integer = 0 To strValue.Length - 1
                                                Select Case strValue(i)
                                                    Case 1 'Not Applicable
                                                        tempCondition1.Add("position('Not Applicable' in CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,0::SMALLINT)) > 0")
                                                    Case 2 'Shipped
                                                        tempCondition1.Add("position('Shipped' in CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,0::SMALLINT)) > 0")
                                                    Case 3 'Back Order
                                                        tempCondition1.Add("position('BO' in CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,0::SMALLINT)) > 0 AND position('Shippable' in CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,0::SMALLINT)) = 0")
                                                    Case 4 'Shippable
                                                        tempCondition1.Add("position('Shippable' in CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,0::SMALLINT)) > 0")
                                                End Select
                                            Next

                                            tempCondition.Add("(" & String.Join(" OR ", tempCondition1) & ")")
                                        ElseIf dr(0)("numReportFieldGroupID") = 12 AndAlso dr(0)("vcOrigDbColumnName") = "vcFromWareHouse" AndAlso dr(0)("vcLookBackTableName") = "Warehouses" Then
                                            tempCondition.Add(String.Format("WarehousesFrom.vcWareHouse {2}",
                                                              dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                        ElseIf dr(0)("numReportFieldGroupID") = 12 AndAlso dr(0)("vcOrigDbColumnName") = "vcToWareHouse" AndAlso dr(0)("vcLookBackTableName") = "Warehouses" Then
                                            tempCondition.Add(String.Format("WarehousesTo.vcWareHouse {2}",
                                                              dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                        ElseIf dr(0)("numReportFieldGroupID") = 11 AndAlso dr(0)("vcOrigDbColumnName") = "numAvailable" AndAlso dr(0)("vcLookBackTableName") = "WareHouseItems" Then
                                            tempCondition.Add(String.Format("COALESCE(WareHouseItems.numOnHand,0) + COALESCE(WareHouseItems.numAllocation,0) {0}", ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                        ElseIf dr(0)("numReportFieldGroupID") = 11 AndAlso dr(0)("vcOrigDbColumnName") = "monAdjustedValue" AndAlso dr(0)("vcLookBackTableName") = "WareHouseItems" Then
                                            tempCondition.Add(String.Format("((COALESCE(WareHouseItems.numOnHand,0) + COALESCE(WareHouseItems.numAllocation,0)) * COALESCE(Item.monAverageCost,0)) {0}", ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                        ElseIf dr(0)("numReportFieldGroupID") = 3 AndAlso dr(0)("vcOrigDbColumnName") = "monDealAmount" AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            tempCondition.Add(String.Format("COALESCE((SELECT SUM(COALESCE(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) {2}",
                                                                dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))

                                        ElseIf dr(0)("numReportFieldGroupID") = 3 AndAlso dr(0)("vcOrigDbColumnName") = "monAmountPaid" AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            tempCondition.Add(String.Format("COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0) {2}",
                                                                dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))

                                        ElseIf dr(0)("numReportFieldGroupID") = 5 AndAlso (dr(0)("vcOrigDbColumnName") = "monDealAmount" Or dr(0)("vcOrigDbColumnName") = "monAmountPaid") AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            tempCondition.Add(String.Format("({0}.{1} * COALESCE(OpportunityBizDocs.fltExchangeRateBizDoc,COALESCE(OpportunityMaster.fltExchangeRate,1))) {2}",
                                                         dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))

                                        ElseIf dr(0)("numReportFieldGroupID") = 5 AndAlso dr(0)("vcOrigDbColumnName") = "monAmountDue" AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            tempCondition.Add(String.Format("((COALESCE(OpportunityBizDocs.monDealAmount,0) - COALESCE(OpportunityBizDocs.monAmountPaid,0)) * COALESCE(OpportunityBizDocs.fltExchangeRateBizDoc,COALESCE(OpportunityMaster.fltExchangeRate,1))) {2}",
                                                         dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))

                                        ElseIf dr(0)("numReportFieldGroupID") = 25 AndAlso dr(0)("vcLookBackTableName") = "TaxItems" Then 'BizDocs Items Tax Field
                                            tempCondition.Add(String.Format("(fn_CalBizDocTaxAmt(@numDomainId,{3},OpportunityMaster.numOppId,OpportunityItems.numoppitemtCode,1::SMALLINT,OpportunityBizDocItems.monTotAmount,OpportunityBizDocItems.numUnitHour)) {2}",
                                                  dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom"))), dr(0)("numFieldID")))

                                        ElseIf dr(0)("numReportFieldGroupID") = 26 AndAlso dr(0)("vcLookBackTableName") = "TaxItems" Then 'BizDocs Tax Field
                                            tempCondition.Add(String.Format("(fn_CalOppItemTotalTaxAmt(@numDomainId,{3},OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId)) {2}",
                                                                                              dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom"))), dr(0)("numFieldID")))

                                        ElseIf dr(0)("numReportFieldGroupID") = 5 AndAlso dr(0)("vcOrigDbColumnName") = "dtFromDate" AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            tempCondition.Add(String.Format("FormatedDateFromDate({0}.{1},@numDomainID) {2}",
                                                   dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))

                                        ElseIf dr(0)("numReportFieldGroupID") = 5 AndAlso dr(0)("vcOrigDbColumnName") = "dtDueDate" AndAlso dr(0)("vcLookBackTableName") = "OpportunityBizDocs" Then
                                            tempCondition.Add(String.Format("FormatedDateFromDate(OpportunityBizDocs.dtFromDate + make_interval(days => COALESCE((SELECT numNetDueInDays::INT FROM BillingTerms WHERE numTermsID = COALESCE(OpportunityMaster.intBillingDays,0)),0)),OpportunityBizDocs.dtFromDate),@numDomainID) {2}",
                                                          dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))

                                        ElseIf dr(0)("numReportFieldGroupID") <> 12 AndAlso dr(0)("vcOrigDbColumnName") = "vcLocation" AndAlso dr(0)("vcLookBackTableName") = "WarehouseLocation" Then
                                            tempCondition.Add(String.Format("WarehouseLocation.numWLocationID {2}",
                                                               dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                        ElseIf dr(0)("numReportFieldGroupID") = 4 AndAlso dr(0)("vcOrigDbColumnName") = "vcItemName" Then 'BizDocs Tax Field
                                            Select Case str.OperatorType
                                                Case "eq"
                                                    tempCondition.Add("EXISTS (SELECT I.numItemCode FROM Item I WHERE I.numItemCode = opportunityitems.numItemCode And LOWER(I.vcItemName) = LOWER('" & str.ColValue.Trim() & "'))")
                                                Case "ne"
                                                    tempCondition.Add("NOT EXISTS (SELECT I.numItemCode FROM Item I WHERE I.numItemCode = opportunityitems.numItemCode And LOWER(I.vcItemName) = LOWER('" & str.ColValue.Trim() & "'))")
                                                Case "co"
                                                    tempCondition.Add("EXISTS (SELECT I.numItemCode FROM Item I WHERE I.numItemCode = opportunityitems.numItemCode And I.vcItemName ilike '%" & str.ColValue.Trim() & "%')")
                                                Case "nc"
                                                    tempCondition.Add("NOT EXISTS (SELECT I.numItemCode FROM Item I WHERE I.numItemCode = opportunityitems.numItemCode And I.vcItemName ilike '%" & str.ColValue.Trim() & "%')")
                                                Case "sw"
                                                    tempCondition.Add("EXISTS (SELECT I.numItemCode FROM Item I WHERE I.numItemCode = opportunityitems.numItemCode And I.vcItemName ilike '" & str.ColValue.Trim() & "%')")
                                                Case Else
                                                    tempCondition.Add("EXISTS (SELECT I.numItemCode FROM Item I WHERE I.numItemCode = opportunityitems.numItemCode And LOWER(I.vcItemName) = LOWER('" & str.ColValue.Trim() & "'))")
                                            End Select
                                        ElseIf dr(0)("vcAssociatedControlType") = "DateField" Then
                                            If dr(0)("vcOrigDbColumnName") = "dtTransferredDate" Then
                                                tempCondition.Add(String.Format("CAST({0}.{1} + make_interval(mins => -@ClientTimeZoneOffset) AS DATE) {2}",
                                                           dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                            Else
                                                tempCondition.Add(String.Format("{0}.{1} + make_interval(mins => -@ClientTimeZoneOffset) {2}",
                                                           dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                            End If

                                        ElseIf dr(0)("vcAssociatedControlType") = "SelectBox" Or dr(0)("vcAssociatedControlType") = "ListBox" Or dr(0)("vcAssociatedControlType") = "CheckBox" Then
                                            If (dr(0)("vcDbColumnName").ToString() = "numCategoryID") Then
                                                tempCondition.Add(String.Format("Item.numItemCode in ( SELECT ItemCategory.numItemID FROM ItemCategory WHERE ItemCategory.numCategoryID {0} )",
                                                                           ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType").ToString(), dr(0)("vcFieldDataType").ToString(), CCommon.ToBool(dr(0)("bitCustom")))))
                                            Else
                                                tempCondition.Add(String.Format("{0}.{1} {2}",
                                                               dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                            End If
                                        Else
                                            'If dr(0)("vcLookBackTableName") = "DivisionMaster" Or dr(0)("vcLookBackTableName") = "CompanyInfo" Then
                                            tempCondition.Add(String.Format("{0}.{1} {2}",
                                                              dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom")))))
                                            'End If
                                        End If
                                    Else 'Custom Fields
                                        If dr(0)("vcLookBackTableName") = "CFW_FLD_Values_Item" Then
                                            If (CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 10 Or CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 9) Then
                                                If dr(0)("vcAssociatedControlType") = "SelectBox" Then
                                                    tempCondition.Add(String.Format("(CASE 
	                                                                                    WHEN COALESCE(OpportunityItems.numoppitemtCode,0) > 0 AND EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE fld_id = {0}  AND RecId = OpportunityItems.numoppitemtCode)     
	                                                                                    THEN EXISTS (SELECT RecId FROM CFW_Fld_Values_OppItems WHERE fld_id = {0} AND RecId = OpportunityItems.numoppitemtCode AND COALESCE(Fld_Value,'') {1})	
	                                                                                    ELSE EXISTS (SELECT RecId FROM CFW_FLD_Values_Item WHERE Fld_ID = {0} AND RecId = Item.numItemCode AND COALESCE(Fld_Value,'') {1})
                                                                                    END)", dr(0)("numFieldID"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom"))), dr(0)("vcLookBackTableName")))
                                                ElseIf dr(0)("vcAssociatedControlType") = "CheckBoxList" Then
                                                    Dim arrValues = str.ColValue.Trim().Split(", ")
                                                    Dim vcCondition As String = "("

                                                    For Each vcValue As String In arrValues
                                                        If str.OperatorType = "eq" Then
                                                            If vcCondition.Length = 1 Then
                                                                vcCondition = vcCondition & String.Format("{1} IN (SELECT Id FROM SplitIDs(CASE 
	                                                                                            WHEN COALESCE(OpportunityItems.numoppitemtCode,0) > 0 AND EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE fld_id = {0}  AND RecId = OpportunityItems.numoppitemtCode)     
	                                                                                            THEN (SELECT COALESCE(Fld_Value,'') FROM CFW_Fld_Values_OppItems WHERE fld_id = {0} AND RecId = OpportunityItems.numoppitemtCode)	
	                                                                                            ELSE (SELECT COALESCE(Fld_Value,'') FROM CFW_FLD_Values_Item WHERE Fld_ID = {0} AND RecId = Item.numItemCode)
                                                                                            END,','))", dr(0)("numFieldID"), vcValue, dr(0)("vcLookBackTableName"))
                                                            Else
                                                                vcCondition = vcCondition & " OR " & String.Format("{1} IN (SELECT Id FROM SplitIDs(CASE 
	                                                                                            WHEN COALESCE(OpportunityItems.numoppitemtCode,0) > 0 AND EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE fld_id = {0}  AND RecId = OpportunityItems.numoppitemtCode)     
	                                                                                            THEN (SELECT COALESCE(Fld_Value,'') FROM CFW_Fld_Values_OppItems WHERE fld_id = {0} AND RecId = OpportunityItems.numoppitemtCode)	
	                                                                                            ELSE (SELECT COALESCE(Fld_Value,'') FROM CFW_FLD_Values_Item WHERE Fld_ID = {0} AND RecId = Item.numItemCode)
                                                                                            END,','))", dr(0)("numFieldID"), vcValue, dr(0)("vcLookBackTableName"))
                                                            End If
                                                        ElseIf str.OperatorType = "ne" Then
                                                            If vcCondition.Length = 1 Then
                                                                vcCondition = vcCondition & String.Format("{1} NOT IN (SELECT Id FROM SplitIDs(CASE 
	                                                                                            WHEN COALESCE(OpportunityItems.numoppitemtCode,0) > 0 AND EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE fld_id = {0}  AND RecId = OpportunityItems.numoppitemtCode)     
	                                                                                            THEN (SELECT COALESCE(Fld_Value,'') FROM CFW_Fld_Values_OppItems WHERE fld_id = {0} AND RecId = OpportunityItems.numoppitemtCode)	
	                                                                                            ELSE (SELECT COALESCE(Fld_Value,'') FROM CFW_FLD_Values_Item WHERE Fld_ID = {0} AND RecId = Item.numItemCode)
                                                                                            END,','))", dr(0)("numFieldID"), vcValue, dr(0)("vcLookBackTableName"))
                                                            Else
                                                                vcCondition = vcCondition & " OR " & String.Format("{1} NOT IN (SELECT Id FROM SplitIDs(CASE 
	                                                                                            WHEN COALESCE(OpportunityItems.numoppitemtCode,0) > 0 AND EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE fld_id = {0}  AND RecId = OpportunityItems.numoppitemtCode)     
	                                                                                            THEN (SELECT COALESCE(Fld_Value,'') FROM CFW_Fld_Values_OppItems WHERE fld_id = {0} AND RecId = OpportunityItems.numoppitemtCode)	
	                                                                                            ELSE (SELECT COALESCE(Fld_Value,'') FROM CFW_FLD_Values_Item WHERE Fld_ID = {0} AND RecId = Item.numItemCode)
                                                                                            END,','))", dr(0)("numFieldID"), vcValue, dr(0)("vcLookBackTableName"))
                                                            End If
                                                        End If
                                                    Next

                                                    vcCondition = vcCondition & ")"

                                                    tempCondition.Add(vcCondition)
                                                Else
                                                    tempCondition.Add(String.Format("COALESCE(GetCustFldValueOppItems({0},OpportunityItems.numoppitemtCode,Item.numItemCode),'') {1}", dr(0)("numFieldID"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom"))), dr(0)("vcLookBackTableName")))
                                                End If
                                            Else
                                                If dr(0)("vcAssociatedControlType") = "SelectBox" Then
                                                    tempCondition.Add(String.Format("EXISTS (SELECT RecId FROM CFW_FLD_Values_Item WHERE Fld_ID = {0} AND RecId = Item.numItemCode AND COALESCE(Fld_Value,'') {1})", dr(0)("numFieldID"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom"))), dr(0)("vcLookBackTableName")))
                                                ElseIf dr(0)("vcAssociatedControlType") = "CheckBoxList" Or dr(0)("vcAssociatedControlType") = "ListBox" Then
                                                    Dim arrValues = str.ColValue.Trim().Split(", ")
                                                    Dim vcCondition As String = "("


                                                    For Each vcValue As String In arrValues
                                                        If str.OperatorType = "eq" Then
                                                            If vcCondition.Length = 1 Then
                                                                vcCondition = vcCondition & String.Format("{1} IN (SELECT Id FROM SplitIDs((SELECT COALESCE(Fld_Value,'') FROM CFW_FLD_Values_Item WHERE Fld_ID = {0} AND RecId = Item.numItemCode),','))", dr(0)("numFieldID"), vcValue, dr(0)("vcLookBackTableName"))
                                                            Else
                                                                vcCondition = vcCondition & " OR " & String.Format("{1} IN (SELECT Id FROM SplitIDs((SELECT COALESCE(Fld_Value,'') FROM CFW_FLD_Values_Item WHERE Fld_ID = {0} AND RecId = Item.numItemCode),','))", dr(0)("numFieldID"), vcValue, dr(0)("vcLookBackTableName"))
                                                            End If
                                                        ElseIf str.OperatorType = "ne" Then
                                                            If vcCondition.Length = 1 Then
                                                                vcCondition = vcCondition & String.Format("{1} NOT IN (SELECT Id FROM SplitIDs((SELECT COALESCE(Fld_Value,'') FROM CFW_FLD_Values_Item WHERE Fld_ID = {0} AND RecId = Item.numItemCode),','))", dr(0)("numFieldID"), vcValue, dr(0)("vcLookBackTableName"))
                                                            Else
                                                                vcCondition = vcCondition & " OR " & String.Format("{1} NOT IN (SELECT Id FROM SplitIDs((SELECT COALESCE(Fld_Value,'') FROM CFW_FLD_Values_Item WHERE Fld_ID = {0} AND RecId = Item.numItemCode),','))", dr(0)("numFieldID"), vcValue, dr(0)("vcLookBackTableName"))
                                                            End If
                                                        End If
                                                    Next

                                                    vcCondition = vcCondition & ")"

                                                    tempCondition.Add(vcCondition)
                                                Else
                                                    tempCondition.Add(String.Format("COALESCE(GetCustFldValueOppItems({0},0,Item.numItemCode),'') {1}", dr(0)("numFieldID"), ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom"))), dr(0)("vcLookBackTableName")))
                                                End If
                                            End If
                                        Else
                                            Dim fieldToSearch As String = String.Format("{1}.{0}", dr(0)("numFieldID"), dr(0)("vcLookBackTableName"))
                                            Dim fieldValue As String = ""

                                            Select Case dr(0)("vcLookBackTableName")
                                                Case "CFW_FLD_Values"
                                                    fieldValue = String.Format("EXISTS (SELECT RecID FROM CFW_FLD_Values WHERE Fld_ID={0} AND RecID=DivisionMaster.numDivisionID AND ", dr(0)("numFieldID"))
                                                    fieldToSearch = fieldValue & "Fld_Value"
                                                Case "CFW_FLD_Values_Cont"
                                                    fieldValue = String.Format("EXISTS (SELECT RecID FROM CFW_FLD_Values_Cont WHERE Fld_ID={0} AND RecID=AdditionalContactsInformation.numContactID AND ", dr(0)("numFieldID"))
                                                    fieldToSearch = fieldValue & "Fld_Value"
                                                Case "CFW_FLD_Values_Case"
                                                    fieldValue = String.Format("EXISTS (SELECT RecID FROM CFW_FLD_Values_Case WHERE Fld_ID={0} AND RecID=Cases.numCaseId AND ", dr(0)("numFieldID"))
                                                    fieldToSearch = fieldValue & "Fld_Value"
                                                Case "CFW_Fld_Values_Opp"
                                                    fieldValue = String.Format("EXISTS (SELECT RecID FROM CFW_Fld_Values_Opp WHERE Fld_ID={0} AND RecID=OpportunityMaster.numOppID AND ", dr(0)("numFieldID"))
                                                    fieldToSearch = fieldValue & "Fld_Value"
                                                Case "CFW_FLD_Values_Pro"
                                                    fieldValue = String.Format("EXISTS (SELECT RecID FROM CFW_FLD_Values_Pro WHERE Fld_ID={0} AND RecID=ProjectsMaster.numProID AND ", dr(0)("numFieldID"))
                                                    fieldToSearch = fieldValue & "Fld_Value"
                                                Case "CFW_Fld_Values_Serialized_Items"
                                                    fieldValue = String.Format("EXISTS (SELECT RecID FROM CFW_Fld_Values_Serialized_Items WHERE Fld_ID={0} AND RecID=Item.numItemCode AND ", dr(0)("numFieldID"))
                                                    fieldToSearch = fieldValue & "Fld_Value"
                                            End Select
                                            Dim arrValues = Nothing
                                            Dim vcCondition As String
                                            If dr(0)("vcAssociatedControlType") = "SelectBox" Or dr(0)("vcAssociatedControlType") = "ListBox" Then
                                                arrValues = str.ColValue.Trim().Split(", ")
                                                vcCondition = "("
                                                For Each vcValue As String In arrValues
                                                    If str.OperatorType = "eq" Then
                                                        If vcCondition.Length = 1 Then
                                                            vcCondition = vcCondition & String.Format("{0} LOWER(Fld_Value)=LOWER('{1}'))", fieldValue, vcValue, dr(0)("vcLookBackTableName"))
                                                        Else
                                                            vcCondition = vcCondition & " OR " & String.Format("{0} LOWER(Fld_Value)=LOWER('{1}'))", fieldValue, vcValue, dr(0)("vcLookBackTableName"))
                                                        End If
                                                    ElseIf str.OperatorType = "ne" Then
                                                        If vcCondition.Length = 1 Then
                                                            vcCondition = vcCondition & String.Format("{0} LOWER(Fld_Value)<>LOWER('{1}'))", fieldValue, vcValue, dr(0)("vcLookBackTableName"))
                                                        Else
                                                            vcCondition = vcCondition & " AND " & String.Format("{0} LOWER(Fld_Value)<>LOWER('{1}'))", fieldValue, vcValue, dr(0)("vcLookBackTableName"))
                                                        End If
                                                    End If
                                                Next
                                                vcCondition = vcCondition & ")"
                                                tempCondition.Add(vcCondition)
                                            ElseIf dr(0)("vcAssociatedControlType") = "CheckBox" Then
                                                tempCondition.Add(String.Format("{0} {1})", fieldToSearch, ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom"))), dr(0)("vcLookBackTableName")))
                                            ElseIf dr(0)("vcAssociatedControlType") = "CheckBoxList" Then
                                                arrValues = str.ColValue.Trim().Split(", ")
                                                vcCondition = "("


                                                For Each vcValue As String In arrValues
                                                    If str.OperatorType = "eq" Then
                                                        If vcCondition.Length = 1 Then
                                                            vcCondition = vcCondition & String.Format("{0} position(CONCAT(','',{1},'',') IN CONCAT(',',Fld_Value,',')) > 0)", fieldValue, vcValue, dr(0)("vcLookBackTableName"))
                                                        Else
                                                            vcCondition = vcCondition & " OR " & String.Format("{0} position(CONCAT(','',{1},'',') IN CONCAT(',',Fld_Value,',')) > 0)", fieldValue, vcValue, dr(0)("vcLookBackTableName"))
                                                        End If
                                                    ElseIf str.OperatorType = "ne" Then
                                                        If vcCondition.Length = 1 Then
                                                            vcCondition = vcCondition & String.Format("{0} position(CONCAT(','',{1},'',') IN CONCAT(',',Fld_Value,',')) = 0)", fieldValue, vcValue, dr(0)("vcLookBackTableName"))
                                                        Else
                                                            vcCondition = vcCondition & " AND " & String.Format("{0} position(CONCAT(','',{1},'',') IN CONCAT(',',Fld_Value,',')) = 0)", fieldValue, vcValue, dr(0)("vcLookBackTableName"))
                                                        End If
                                                    End If
                                                Next

                                                vcCondition = vcCondition & ")"

                                                tempCondition.Add(vcCondition)
                                            ElseIf dr(0)("vcAssociatedControlType") = "DateField" Then
                                                tempCondition.Add(String.Format("{0} FormatedDateFromDate((CASE is_date(Fld_Value::VARCHAR) = true THEN Fld_Value ELSE NULL END)::TIMESTAMP,@numDomainId) {1})",
                                                                 fieldValue, ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom"))), dr(0)("vcLookBackTableName")))

                                            Else
                                                tempCondition.Add(String.Format("{0} {1})",
                                                                  fieldToSearch, ConditionOperator(str.OperatorType, str.ColValue.Trim(), dr(0)("vcAssociatedControlType"), dr(0)("vcFieldDataType"), CCommon.ToBool(dr(0)("bitCustom"))), dr(0)("vcLookBackTableName")))
                                            End If
                                        End If
                                    End If
                                End If

                                'If objReport.FilterLogic.Trim.Length > 0 Then
                                '    strAdvFilter = strAdvFilter.Replace("|" & iFilterCount & "|", tempCondition(iFilterCount - 1))
                                'Else
                                strAdvFilter = strAdvFilter.Replace("|" & iFilterCount & "|", tempCondition(iFilterCount - 1))
                                'End If

                                iFilterCount = iFilterCount + 1
                            Next

                            If objReport.FilterLogic.Trim.Length = 0 Then
                                strAdvFilter = String.Join(" AND ", tempCondition)
                            End If

                            SqlCondition.Append(" AND (" & strAdvFilter & ")")
                        End If

                        'DateField Filter
                        If objReport.DateFieldFilter.Contains("_") Then

                            Dim numReportFieldGroupID As Long = CCommon.ToLong(objReport.DateFieldFilter.Split("_")(0))
                            Dim numFieldID As Long = CCommon.ToLong(objReport.DateFieldFilter.Split("_")(1))
                            Dim bitCustom As Boolean = CCommon.ToBool(objReport.DateFieldFilter.Split("_")(2))

                            If numFieldID > 0 Then
                                Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & numReportFieldGroupID & " AND numFieldID=" & numFieldID & " AND bitCustom=" & bitCustom)

                                If dr.Length > 0 Then
                                    If dr(0)("numReportFieldGroupID") = 35 AndAlso Not CCommon.ToString(dr(0)("vcLookBackTableName")).StartsWith("AC") Then
                                        dr(0)("vcLookBackTableName") = "AC" & dr(0)("vcLookBackTableName")
                                    End If

                                    Dim strDateFieldCondition As String = ""
                                    Dim TodaysDate As String = GetTodaysDate()

                                    Select Case objReport.DateFieldValue
                                        Case "AllTime" : strDateFieldCondition = ""
                                        Case "Custom"
                                            If objReport.FromDate.Length > 0 AndAlso objReport.ToDate.Length > 0 Then
                                                Dim fromDate As DateTime = objReport.FromDate
                                                Dim toDate As DateTime = objReport.ToDate
                                                strDateFieldCondition = " between '" & fromDate.Year & "-" & fromDate.Month & "-" & fromDate.Day & "'::TIMESTAMP and '" & toDate.Year & "-" & toDate.Month & "-" & toDate.Day & "'::TIMESTAMP"
                                            End If
                                            'Year
                                        Case "CurYear" : strDateFieldCondition = " between '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) and '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(days => ufn_GetDaysInYear(" & TodaysDate & " + make_interval(years => 0))::INT) + make_interval(secs => -0.003)"
                                        Case "PreYear" : strDateFieldCondition = " between '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(years => -1) and '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(days => ufn_GetDaysInYear(" & TodaysDate & " + make_interval(years => 0))::INT) + make_interval(years => -1) + make_interval(secs => -0.003)"
                                        Case "Pre2Year" : strDateFieldCondition = " between '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(years => -2) and '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(days => ufn_GetDaysInYear(" & TodaysDate & " + make_interval(years => 0))::INT) + make_interval(years => -1) + make_interval(secs => -0.003)"
                                        Case "Ago2Year" : strDateFieldCondition = " < '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(years => -2) "
                                        Case "NextYear" : strDateFieldCondition = " between '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(years => 1) and '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(days => ufn_GetDaysInYear(" & TodaysDate & " + make_interval(years => 0))::INT) + make_interval(years => 1) + make_interval(secs => -0.003)"
                                        Case "CurPreYear" : strDateFieldCondition = " between '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(years => -1) and '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(days => ufn_GetDaysInYear(" & TodaysDate & " + make_interval(years => 0))::INT) + make_interval(secs => -0.003)"
                                        Case "CurPre2Year" : strDateFieldCondition = " between '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(years => -2) and '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(days => ufn_GetDaysInYear(" & TodaysDate & " + make_interval(years => 0))::INT) + make_interval(secs => -0.003)"
                                        Case "CurNextYear" : strDateFieldCondition = " between '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) and '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(days => ufn_GetDaysInYear(" & TodaysDate & " + make_interval(years => 0))::INT) + make_interval(years => 1) + make_interval(secs => -0.003)"
                                            'Quarter
                                        Case "CuQur" : strDateFieldCondition = " between to_char(date_trunc('quarter'," & TodaysDate & ")::date, 'yyyy-mm-dd')::TIMESTAMP and to_char(date_trunc('quarter'," & TodaysDate & ")::date, 'yyyy-mm-dd')::TIMESTAMP + make_interval (months => 3) + make_interval(secs => -0.003)"
                                        Case "CurNextQur" : strDateFieldCondition = " between to_char(date_trunc('quarter'," & TodaysDate & ")::date, 'yyyy-mm-dd')::TIMESTAMP and to_char(date_trunc('quarter'," & TodaysDate & ")::date, 'yyyy-mm-dd')::TIMESTAMP + make_interval (months => 6) + make_interval(secs => -0.003)"
                                        Case "CurPreQur" : strDateFieldCondition = " between to_char(date_trunc('quarter'," & TodaysDate & ")::date, 'yyyy-mm-dd')::TIMESTAMP + make_interval (months => -3) and to_char(date_trunc('quarter'," & TodaysDate & ")::date, 'yyyy-mm-dd')::TIMESTAMP + make_interval (months => 3) + make_interval(secs => -0.003)"
                                        Case "NextQur" : strDateFieldCondition = " between to_char(date_trunc('quarter'," & TodaysDate & ")::date, 'yyyy-mm-dd')::TIMESTAMP  + make_interval (months => 3) and to_char(date_trunc('quarter'," & TodaysDate & ")::date, 'yyyy-mm-dd')::TIMESTAMP + make_interval (months => 6) + make_interval(secs => -0.003)"
                                        Case "PreQur" : strDateFieldCondition = " between to_char(date_trunc('quarter'," & TodaysDate & ")::date - 1, 'yyyy-mm-dd')::TIMESTAMP + make_interval (days => 1) + make_interval (months => -3) and to_char(date_trunc('quarter'," & TodaysDate & ")::date - 1, 'yyyy-mm-dd')::TIMESTAMP + make_interval (days => 1) + make_interval(secs => -0.003)"
                                            'Month
                                        Case "LastMonth" : strDateFieldCondition = " between date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP + make_interval(months => -1) and date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP + make_interval(secs => -0.003)"
                                        Case "ThisMonth" : strDateFieldCondition = " between date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP and date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP + make_interval(months => 1) + make_interval(secs => -0.003)"
                                        Case "NextMonth" : strDateFieldCondition = " between date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP + make_interval(months => 1) and date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP + make_interval(months => 2) + make_interval(secs => -0.003)"
                                        Case "CurPreMonth" : strDateFieldCondition = " between date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP + make_interval(months => -1) and date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP + make_interval(months => 1) + make_interval(secs => -0.003)"
                                        Case "CurNextMonth" : strDateFieldCondition = " between date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP and date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP + make_interval(months => 2) + make_interval(secs => -0.003)"
                                            'Week
                                        Case "LastWeek" : strDateFieldCondition = " between date_trunc('week'," & TodaysDate & ") + make_interval(weeks => -1) + '-1 Day' and date_trunc('week'," & TodaysDate & ") + '-1 Day' + make_interval(secs => -0.003)"
                                        Case "ThisWeek" : strDateFieldCondition = " between date_trunc('week'," & TodaysDate & ") + '-1 Day' and date_trunc('week'," & TodaysDate & ") + '6 Day' + make_interval(secs => -0.003)"
                                        Case "NextWeek" : strDateFieldCondition = " between date_trunc('week'," & TodaysDate & ") + '6 Day' and date_trunc('week'," & TodaysDate & ") + make_interval(weeks => 1) + '6 Day' + make_interval(secs => -0.003)"
                                            'Day
                                        Case "Yesterday" : strDateFieldCondition = " between " & TodaysDate & " + make_interval(days => -1) and " & TodaysDate & " + make_interval(secs => -0.003)"
                                        Case "Today" : strDateFieldCondition = " between " & TodaysDate & " and " & TodaysDate & " + make_interval(days => 1) + make_interval(secs => -0.003)"
                                        Case "Tomorrow" : strDateFieldCondition = " between  " & TodaysDate & " + make_interval(days => 1) and  " & TodaysDate & " + make_interval(days => 2) + make_interval(secs => -0.003)"
                                        Case "Last7Day" : strDateFieldCondition = " between " & TodaysDate & " + make_interval(days => -7) and " & TodaysDate
                                        Case "Last30Day" : strDateFieldCondition = " between " & TodaysDate & " + make_interval(days => -30) and " & TodaysDate
                                        Case "Last60Day" : strDateFieldCondition = " between " & TodaysDate & " + make_interval(days => -60) and " & TodaysDate
                                        Case "Last90Day" : strDateFieldCondition = " between " & TodaysDate & " + make_interval(days => -90) and " & TodaysDate
                                        Case "Last120Day" : strDateFieldCondition = " between " & TodaysDate & " + make_interval(days => -120) and " & TodaysDate
                                        Case "Next7Day" : strDateFieldCondition = " between  " & TodaysDate & " and " & TodaysDate & " + make_interval(days => 7) "
                                        Case "Next30Day" : strDateFieldCondition = " between  " & TodaysDate & " and " & TodaysDate & " + make_interval(days => 30) "
                                        Case "Next60Day" : strDateFieldCondition = " between  " & TodaysDate & " and " & TodaysDate & " + make_interval(days => 60) "
                                        Case "Next90Day" : strDateFieldCondition = " between  " & TodaysDate & " and " & TodaysDate & " + make_interval(days => 90) "
                                        Case "Next120Day" : strDateFieldCondition = " between  " & TodaysDate & " and " & TodaysDate & " + make_interval(days => 120) "

                                            'KPI Period
                                        Case "Year" : strDateFieldCondition = " between '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) and '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP," & TodaysDate & ")::INT) + make_interval(days => ufn_GetDaysInYear(" & TodaysDate & " + make_interval(years => 0))::INT) + make_interval(secs => -0.003)"
                                        Case "Quarter" : strDateFieldCondition = " between '1900-01-01'::DATE + make_interval(months => (Datediff('month','1900-01-01'::TIMESTAMP," & TodaysDate & ")) - 2) and '1900-01-01'::DATE + make_interval(months => ((Datediff('month','1900-01-01'::TIMESTAMP," & TodaysDate & ")) + 1)) + make_interval(secs => -0.003)"
                                        Case "Month" : strDateFieldCondition = " between date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP and date_trunc('MONTH'," & TodaysDate & ")::TIMESTAMP + make_interval(months => 1) + make_interval(secs => -0.003)"
                                        Case "Week" : strDateFieldCondition = " between date_trunc('week'," & TodaysDate & ") + '-1 Day' and date_trunc('week'," & TodaysDate & ") + '6 Day' + make_interval(secs => -0.003)"
                                    End Select

                                    If strDateFieldCondition.Length > 0 Then
                                        If CCommon.ToBool(dr(0)("bitCustom")) = False Then
                                            If dr(0)("vcOrigDbColumnName") = "dtTransferredDate" Then
                                                SqlCondition.Append(String.Format(" AND (CAST({0}.{1} + make_interval(mins => -@ClientTimeZoneOffset) AS DATE) {2})", dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName"), strDateFieldCondition))
                                            Else
                                                SqlCondition.Append(String.Format(" AND ({0}.{1} {2})", dr(0)("vcLookBackTableName"), dr(0)("vcDbColumnName"), strDateFieldCondition))
                                            End If
                                        Else 'Custom Fields
                                            If dr(0)("vcLookBackTableName") = "CFW_FLD_Values_Item" Then
                                                If (CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 10 Or CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 9) Then
                                                    SqlCondition.Append(String.Format(" AND (GetCustFldValueOppItems({0},OpportunityItems.numoppitemtCode,Item.numItemCode)::TIMESTAMP {1})", dr(0)("numFieldID"), strDateFieldCondition, dr(0)("vcLookBackTableName")))
                                                Else
                                                    SqlCondition.Append(String.Format(" AND (GetCustFldValueOppItems({0},0,Item.numItemCode)::TIMESTAMP {1})", dr(0)("numFieldID"), strDateFieldCondition, dr(0)("vcLookBackTableName")))
                                                End If
                                            Else
                                                Select Case dr(0)("vcLookBackTableName")
                                                    Case "CFW_FLD_Values"
                                                        SqlCondition.Append(String.Format(" AND ((CASE WHEN is_date(GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID)) = true THEN GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID) ELSE NULL END)::TIMESTAMP {1})", dr(0)("numFieldID"), strDateFieldCondition, dr(0)("vcLookBackTableName")))
                                                    Case "CFW_FLD_Values_Cont"
                                                        SqlCondition.Append(String.Format(" AND ((CASE WHEN is_date(GetCustFldValue({0},-1::SMALLINT,AdditionalContactsInformation.numContactID)) = true THEN GetCustFldValue({0},-1::SMALLINT,AdditionalContactsInformation.numContactID) ELSE NULL END)::TIMESTAMP {1})", dr(0)("numFieldID"), strDateFieldCondition, dr(0)("vcLookBackTableName")))
                                                    Case "CFW_FLD_Values_Case"
                                                        SqlCondition.Append(String.Format(" AND ((CASE WHEN is_date(GetCustFldValue({0},-1::SMALLINT,Cases.numCaseId)) = true THEN GetCustFldValue({0},-1::SMALLINT,Cases.numCaseId) ELSE NULL END)::TIMESTAMP {1})", dr(0)("numFieldID"), strDateFieldCondition, dr(0)("vcLookBackTableName")))
                                                    Case "CFW_Fld_Values_Opp"
                                                        SqlCondition.Append(String.Format(" AND ((CASE WHEN is_date(GetCustFldValue({0},-1::SMALLINT,OpportunityMaster.numOppID)) = true THEN GetCustFldValue({0},-1::SMALLINT,OpportunityMaster.numOppID) ELSE NULL END)::TIMESTAMP {1})", dr(0)("numFieldID"), strDateFieldCondition, dr(0)("vcLookBackTableName")))
                                                    Case "CFW_FLD_Values_Pro"
                                                        SqlCondition.Append(String.Format(" AND ((CASE WHEN is_date(GetCustFldValue({0},-1::SMALLINT,ProjectsMaster.numProID)) = true THEN GetCustFldValue({0},-1::SMALLINT,ProjectsMaster.numProID) ELSE NULL END)::TIMESTAMP {1})", dr(0)("numFieldID"), strDateFieldCondition, dr(0)("vcLookBackTableName")))
                                                    Case "CFW_Fld_Values_Serialized_Items"
                                                        SqlCondition.Append(String.Format(" AND ((CASE WHEN is_date(GetCustFldValue({0},-1::SMALLINT,Item.numItemCode)) = true THEN GetCustFldValue({0},-1::SMALLINT,Item.numItemCode) ELSE NULL END)::TIMESTAMP {1})", dr(0)("numFieldID"), strDateFieldCondition, dr(0)("vcLookBackTableName")))
                                                End Select


                                            End If
                                        End If

                                        If objReport.ReportFormatType = 3 Or objReport.ReportFormatType = 5 Then 'KPI
                                            Dim vcKPIMeasureField As String = ""
                                            Dim drKPI() As DataRow

                                            If boolChart Then
                                                If objReport.KPIMeasureField = "0_0_False" Then
                                                    vcKPIMeasureField = "Count(1)"
                                                Else
                                                    drKPI = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & objReport.KPIMeasureField.Split("_")(0) & " AND numFieldID=" & objReport.KPIMeasureField.Split("_")(1) & " AND bitCustom=" & objReport.KPIMeasureField.Split("_")(2))

                                                    If drKPI(0)("numReportFieldGroupID") = 35 AndAlso Not CCommon.ToString(drKPI(0)("vcLookBackTableName")).StartsWith("AC") Then
                                                        drKPI(0)("vcLookBackTableName") = "AC" & drKPI(0)("vcLookBackTableName")
                                                    End If

                                                    vcKPIMeasureField = String.Format("SUM(COALESCE({0}.{1},0))", drKPI(0)("vcLookBackTableName"), drKPI(0)("vcOrigDbColumnName"))
                                                End If

                                                Select Case objReport.DateFieldValue
                                                    Case "Year"
                                                        strFiledList.Add(String.Format("Cast(""Year"" as varchar(20)) ""Year"",COALESCE(""1"",0) AS ""Jan"",COALESCE(""2"",0) AS ""Feb"",COALESCE(""3"",0) AS ""Mar"",COALESCE(""4"",0) AS ""Apr""," &
                                                                                    "COALESCE(""5"",0) AS ""May"",COALESCE(""6"",0) AS ""Jun"",COALESCE(""7"",0) AS ""Jul"",COALESCE(""8"",0) AS ""Aug""," &
                                                                                    "COALESCE(""9"",0) AS ""Sep"",COALESCE(""10"",0) AS ""Oct"",COALESCE(""11"",0) AS ""Nov"",COALESCE(""12"",0) AS ""Dec""" &
                                                                                    " FROM (Select EXTRACT(YEAR FROM CAST((CASE WHEN is_date({1}.{2}::VARCHAR) = true THEN {1}.{2} ELSE NULL END) AS TIMESTAMP))::INT ""Year"",EXTRACT(MONTH FROM CAST((CASE WHEN is_date({1}.{2}::VARCHAR) = true THEN {1}.{2} ELSE NULL END) AS TIMESTAMP))::INT ""Month"",{0} AS Total",
                                                           vcKPIMeasureField, dr(0)("vcLookBackTableName"), IIf(CCommon.ToBool(dr(0)("bitCustom")), dr(0)("numFieldID"), dr(0)("vcDbColumnName"))))

                                                        SqlGroupBy.Append(String.Format(" GROUP BY EXTRACT(YEAR FROM CAST((CASE WHEN is_date({0}.{1}::VARCHAR) = true THEN {0}.{1} ELSE NULL END) AS TIMESTAMP))::INT,EXTRACT(MONTH FROM CAST((CASE WHEN is_date({0}.{1}::VARCHAR) = true THEN {0}.{1} ELSE NULL END) AS TIMESTAMP))::INT AS t " &
                                                                            "PIVOT (SUM(Total) FOR ""Month"" IN(""1"",""2"",""3"",""4"",""5"",""6"",""7"",""8"",""9"",""10"",""11"",""12"")) AS p", dr(0)("vcLookBackTableName"), IIf(CCommon.ToBool(dr(0)("bitCustom")), dr(0)("numFieldID"), dr(0)("vcDbColumnName"))))

                                                    Case "Quarter"
                                                        strFiledList.Add(String.Format("Cast(CONCAT(""Year"",'-Q',""Quarter"") as varchar(20)) ""Quarter"",COALESCE(""1"",0) AS ""MONTH-1"",COALESCE(""2"",0) AS ""MONTH-2"",COALESCE(""0"",0) AS ""MONTH-3""" &
                                                                          " FROM (Select EXTRACT(YEAR FROM (CASE WHEN is_date({1}.{2}::VARCHAR) = true THEN {1}.{2} ELSE NULL END)::TIMESTAMP)) as ""Year"",EXTRACT(quarter FROM (CASE WHEN is_date({1}.{2}::VARCHAR) = true THEN {1}.{2} ELSE NULL END)::TIMESTAMP) ""Quarter"",EXTRACT(Month FROM (CASE WHEN is_date({1}.{2}::VARCHAR) = true THEN {1}.{2} ELSE NULL END)::TIMESTAMP) % 3 ""Month"",{0} AS Total",
                                                           vcKPIMeasureField, dr(0)("vcLookBackTableName"), IIf(CCommon.ToBool(dr(0)("bitCustom")), dr(0)("numFieldID"), dr(0)("vcDbColumnName"))))

                                                        SqlGroupBy.Append(String.Format(" GROUP BY (CASE WHEN is_date({0}.{1}::VARCHAR) = true THEN {0}.{1} ELSE NULL END)::DATE) AS t " &
                                                                            "PIVOT (SUM(Total) FOR ""Month"" IN(""1"",""2"",""0"")) AS p", dr(0)("vcLookBackTableName"), IIf(CCommon.ToBool(dr(0)("bitCustom")), dr(0)("numFieldID"), dr(0)("vcDbColumnName"))))

                                                    Case "Month"
                                                        strFiledList.Add(String.Format("Cast(""Month"" as varchar(20)) ""Month"",COALESCE(""1"",0) AS ""1"",COALESCE(""2"",0) AS ""2"",COALESCE(""3"",0) AS ""3"",COALESCE(""4"",0) AS ""4"",COALESCE(""5"",0) AS ""5""," &
                                                                                   " COALESCE(""6"",0) AS ""6"",COALESCE(""7"",0) AS ""7"",COALESCE(""8"",0) AS ""8"",COALESCE(""9"",0) AS ""9"",COALESCE(""10"",0) AS ""10""," &
                                                                                   " COALESCE(""11"",0) AS ""11"",COALESCE(""12"",0) AS ""12"",COALESCE(""13"",0) AS ""13"",COALESCE(""14"",0) AS ""14"",COALESCE(""15"",0) AS ""15""," &
                                                                                   " COALESCE(""16"",0) AS ""16"",COALESCE(""17"",0) AS ""17"",COALESCE(""18"",0) AS ""18"",COALESCE(""19"",0) AS ""19"",COALESCE(""20"",0) AS ""20""," &
                                                                                   " COALESCE(""21"",0) AS ""21"",COALESCE(""22"",0) AS ""22"",COALESCE(""23"",0) AS ""23"",COALESCE(""24"",0) AS ""24"",COALESCE(""25"",0) AS ""25""," &
                                                                                   " COALESCE(""26"",0) AS ""26"",COALESCE(""27"",0) AS ""27"",COALESCE(""28"",0) AS ""28"",COALESCE(""29"",0) AS ""29"",COALESCE(""30"",0) AS ""30"",COALESCE(""31"",0) AS ""31""" &
                                                                                   " FROM (Select TO_CHAR((CASE WHEN is_date({1}.{2}) = true THEN {1}.{2} ELSE NULL END)::TIMESTAMP,'Month') ""Month"",EXTRACT(Day FROM (CASE WHEN is_date({1}.{2}::VARCHAR) = true THEN {1}.{2} ELSE NULL END)::TIMESTAMP) ""Day"",{0} AS Total",
                                                        vcKPIMeasureField, dr(0)("vcLookBackTableName"), IIf(CCommon.ToBool(dr(0)("bitCustom")), dr(0)("numFieldID"), dr(0)("vcDbColumnName"))))

                                                        SqlGroupBy.Append(String.Format(" GROUP BY (CASE WHEN is_date({0}.{1}::VARCHAR) = true THEN {0}.{1} ELSE NULL END)::DATE) AS t " &
                                                                            "PIVOT (SUM(Total) FOR ""Day"" IN(""1"",""2"",""3"",""4"",""5"",""6"",""7"",""8"",""9"",""10"",""11"",""12"",""13"",""14"",""15"",""16"",""17"",""18"",""19"",""20"",""21"",""22"",""23"",""24"",""25"",""26"",""27"",""28"",""29"",""30"",""31"")) AS p", dr(0)("vcLookBackTableName"), IIf(CCommon.ToBool(dr(0)("bitCustom")), dr(0)("numFieldID"), dr(0)("vcDbColumnName"))))

                                                    Case "Week"
                                                        strFiledList.Add(String.Format("Cast(""Week"" as varchar(20)) ""Week"",COALESCE(""1"",0) AS ""Sunday"",COALESCE(""2"",0) AS ""Monday"",COALESCE(""3"",0) AS ""Tuesday"",COALESCE(""4"",0) AS ""Wednesday""," &
                                                                                    " COALESCE(""5"",0) AS ""Thursday"",COALESCE(""6"",0) AS ""Friday"",COALESCE(""7"",0) AS ""Saturday""" &
                                                                                    " FROM (Select EXTRACT(week FROM (CASE WHEN is_date({1}.{2}::VARCHAR) = true THEN {1}.{2} ELSE NULL END)::TIMESTAMP)) ""Week"",date_part('dow',(CASE WHEN is_date({1}.{2}::VARCHAR) = true THEN {1}.{2} ELSE NULL END)::TIMESTAMP) ""Day"",{0} AS Total",
                                                        vcKPIMeasureField, dr(0)("vcLookBackTableName"), IIf(CCommon.ToBool(dr(0)("bitCustom")), dr(0)("numFieldID"), dr(0)("vcDbColumnName"))))

                                                        SqlGroupBy.Append(String.Format(" GROUP BY (CASE WHEN is_date({0}.{1}::VARCHAR) = true THEN {0}.{1} ELSE NULL END)::DATE) AS t " &
                                                                            "PIVOT (SUM(Total) FOR ""Day"" IN(""1"",""2"",""3"",""4"",""5"",""6"",""7"")) AS p", dr(0)("vcLookBackTableName"), IIf(CCommon.ToBool(dr(0)("bitCustom")), dr(0)("numFieldID"), dr(0)("vcDbColumnName"))))
                                                End Select
                                            Else
                                                Dim strPreviousDate, strCueerntDate, strPeriod As String

                                                If objReport.DateFieldValue = "Year" Then
                                                    strPreviousDate = " between '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP, " & TodaysDate & ")-1) and '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP, " & TodaysDate & ")) + make_interval(secs => -0.003)"
                                                    strCueerntDate = " between '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP, " & TodaysDate & ")) and '1900-01-01'::DATE + make_interval(years => DATEDIFF('year','1900-01-01'::TIMESTAMP, " & TodaysDate & ")+1)"
                                                ElseIf objReport.DateFieldValue = "Quarter" Then
                                                    strPreviousDate = " between '1900-01-01'::DATE + make_interval(months => ((DATEDIFF('month','1900-01-01'::TIMESTAMP, " & TodaysDate & ")/3)-1) * 3) and '1900-01-01'::DATE + make_interval(months => (DATEDIFF('month','1900-01-01'::TIMESTAMP, " & TodaysDate & ") / 3) * 3) + make_interval(secs => -0.003)"
                                                    strCueerntDate = " between '1900-01-01'::DATE + make_interval(months => (DATEDIFF('month','1900-01-01'::TIMESTAMP, " & TodaysDate & ")/3) * 3) and '1900-01-01'::DATE + make_interval(months => ((DATEDIFF('month','1900-01-01'::TIMESTAMP, " & TodaysDate & ") / 3)+1) * 3)"
                                                ElseIf objReport.DateFieldValue = "Month" Then
                                                    strPreviousDate = " between '1900-01-01'::DATE + make_interval(months => DATEDIFF('month','1900-01-01'::TIMESTAMP, " & TodaysDate & ")-1) and '1900-01-01'::DATE + make_interval(months => DATEDIFF('month','1900-01-01'::TIMESTAMP, " & TodaysDate & ")) + make_interval(secs => -0.003)"
                                                    strCueerntDate = " between '1900-01-01'::DATE + make_interval(months => DATEDIFF('month','1900-01-01'::TIMESTAMP, " & TodaysDate & ")) and '1900-01-01'::DATE + make_interval(months => DATEDIFF('month','1900-01-01'::TIMESTAMP, " & TodaysDate & ")+1)"
                                                ElseIf objReport.DateFieldValue = "Week" Then
                                                    strPreviousDate = " between '1900-01-01'::DATE + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP, " & TodaysDate & ")-1) and '1900-01-01'::DATE + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP, " & TodaysDate & ")) + make_interval(secs => -0.003)"
                                                    strCueerntDate = " between '1900-01-01'::DATE + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP, " & TodaysDate & ")) and '1900-01-01'::DATE + make_interval(weeks => DATEDIFF('week','1900-01-01'::TIMESTAMP, " & TodaysDate & ")+1)"
                                                End If

                                                Select Case objReport.DateFieldValue
                                                    Case "Year"

                                                        strPeriod = "yy"
                                                    Case "Quarter"
                                                        strPeriod = "qq"
                                                    Case "Month"
                                                        strPeriod = "mm"
                                                    Case "Week"
                                                        strPeriod = "wk"
                                                End Select

                                                If objReport.KPIMeasureField = "0_0_False" Then

                                                    strFiledList.Add(String.Format("COALESCE(sum(Case When (CASE WHEN is_date({2}.{3}::VARCHAR) = true THEN {2}.{3} ELSE NULL END)::TIMESTAMP {4} then 1 else 0 end),0) as ""Previous""," &
                                                                          "COALESCE(sum(Case When (CASE WHEN is_date({2}.{3}::VARCHAR) = true THEN {2}.{3} ELSE NULL END)::TIMESTAMP {5} then 1 else 0 end),0) as ""Current""",
                                                          "", "", dr(0)("vcLookBackTableName"), IIf(CCommon.ToBool(dr(0)("bitCustom")), dr(0)("numFieldID"), dr(0)("vcDbColumnName")),
                                                           strPreviousDate, strCueerntDate))
                                                Else
                                                    drKPI = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & objReport.KPIMeasureField.Split("_")(0) & " AND numFieldID=" & objReport.KPIMeasureField.Split("_")(1) & " AND bitCustom=" & objReport.KPIMeasureField.Split("_")(2))

                                                    If drKPI(0)("numReportFieldGroupID") = 35 AndAlso Not CCommon.ToString(drKPI(0)("vcLookBackTableName")).StartsWith("AC") Then
                                                        drKPI(0)("vcLookBackTableName") = "AC" & drKPI(0)("vcLookBackTableName")
                                                    End If

                                                    strFiledList.Add(String.Format("COALESCE(sum(Case When (CASE WHEN is_date({2}.{3}::VARCHAR) = true THEN {2}.{3} ELSE NULL END)::TIMESTAMP {4} then COALESCE({0}.{1},0) else 0 end),0) as ""Previous""," &
                                                                          "COALESCE(sum(Case When (CASE WHEN is_date({2}.{3}::VARCHAR) = true THEN {2}.{3} ELSE NULL END)::TIMESTAMP {5} then COALESCE({0}.{1},0) else 0 end),0) as ""Current""",
                                                           drKPI(0)("vcLookBackTableName"), drKPI(0)("vcOrigDbColumnName"), dr(0)("vcLookBackTableName"), IIf(CCommon.ToBool(dr(0)("bitCustom")), dr(0)("numFieldID"), dr(0)("vcDbColumnName")),
                                                           strPreviousDate, strCueerntDate))
                                                End If

                                                If Not String.IsNullOrEmpty(SqlKPIGroupBy.ToString()) Then
                                                    SqlGroupBy.Append(" GROUP BY " & SqlKPIGroupBy.ToString())
                                                End If
                                            End If

                                            SqlColumn.Append(String.Join(",", strFiledList))

                                        End If
                                    End If
                                End If
                            End If
                        End If

                        If objReport.ReportFormatType = 4 Then 'ScoreCard
                            Dim drKPI() As DataRow

                            If objReport.KPIMeasureField = "0_0_False" Then
                                strFiledList.Add(String.Format("count(1) as RecordCount"))
                            ElseIf objReport.KPIMeasureField = "3_872_False" Then
                                strFiledList.Add(String.Format("COALESCE(AVG(COALESCE(OpportunityMaster.monDealAmount,0)),0) as ""{0}""", objReport.KPIMeasureField))
                            ElseIf objReport.KPIMeasureField = "3_873_False" Then
                                strFiledList.Add(String.Format("COALESCE(SUM(GetOrderProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityMaster.numOppID,1)),0) as ""{0}""", objReport.KPIMeasureField))
                            ElseIf objReport.KPIMeasureField = "3_874_False" Then
                                strFiledList.Add(String.Format("COALESCE(AVG(GetOrderProfitAmountOrMargin(OpportunityMaster.numDomainID,OpportunityMaster.numOppID,2)),0) as ""{0}""", objReport.KPIMeasureField))
                            Else
                                drKPI = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & objReport.KPIMeasureField.Split("_")(0) & " AND numFieldID=" & objReport.KPIMeasureField.Split("_")(1) & " AND bitCustom=" & objReport.KPIMeasureField.Split("_")(2))

                                If drKPI(0)("numReportFieldGroupID") = 35 AndAlso Not CCommon.ToString(drKPI(0)("vcLookBackTableName")).StartsWith("AC") Then
                                    drKPI(0)("vcLookBackTableName") = "AC" & drKPI(0)("vcLookBackTableName")
                                End If

                                strFiledList.Add(String.Format("COALESCE(sum(COALESCE({0}.{1},0)),0) as ""{2}""",
                                                              drKPI(0)("vcLookBackTableName"), drKPI(0)("vcOrigDbColumnName"), objReport.KPIMeasureField))
                            End If

                            SqlColumn.Append(String.Join(",", strFiledList))
                        End If

                        'Sorting Creation
                        If objReport.SortColumn.Contains("_") AndAlso strColumnList.Contains(objReport.SortColumn) Then

                            Dim numReportFieldGroupID As Long = CCommon.ToLong(objReport.SortColumn.Split("_")(0))
                            Dim numFieldID As Long = CCommon.ToLong(objReport.SortColumn.Split("_")(1))
                            Dim bitCustom As Boolean = CCommon.ToBool(objReport.SortColumn.Split("_")(2))

                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & numReportFieldGroupID & " AND numFieldID=" & numFieldID & " AND bitCustom=" & bitCustom)

                            If dr.Length > 0 Then
                                If bitCustom Then
                                    If dr(0)("numReportFieldGroupID") = 35 AndAlso Not CCommon.ToString(dr(0)("vcLookBackTableName")).StartsWith("AC") Then
                                        dr(0)("vcLookBackTableName") = "AC" & dr(0)("vcLookBackTableName")
                                    End If

                                    If dr(0)("vcAssociatedControlType") = "DateField" Then
                                        If dr(0)("vcLookBackTableName") = "CFW_FLD_Values_Item" Then
                                            If (CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 10 Or CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = 9) Then
                                                SqlSortColumn.Append(String.Format(" ORDER BY CAST(GetCustFldValueOppItems({0},OpportunityItems.numoppitemtCode,Item.numItemCode) AS DATE) {1}", numFieldID, objReport.SortDirection))
                                            Else
                                                SqlSortColumn.Append(String.Format(" ORDER BY COALESCE(GetCustFldValueOppItems({0},0,Item.numItemCode),'') {1}", numFieldID, objReport.SortDirection))
                                            End If
                                        Else
                                            Select Case dr(0)("vcLookBackTableName")
                                                Case "CFW_FLD_Values"
                                                    SqlSortColumn.Append(String.Format(" ORDER BY (CASE WHEN is_date(COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID)),'')) = true THEN COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID)),'') ELSE NULL END)::TIMESTAMP {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                                Case "CFW_FLD_Values_Cont"
                                                    SqlSortColumn.Append(String.Format(" ORDER BY (CASE WHEN is_date(COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID)),'')) = true THEN COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,AdditionalContactsInformation.numContactID)),'') ELSE NULL END)::TIMESTAMP {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                                Case "CFW_FLD_Values_Case"
                                                    SqlSortColumn.Append(String.Format(" ORDER BY (CASE WHEN is_date(COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID)),'')) = true THEN COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,Cases.numCaseId)),'') ELSE NULL END)::TIMESTAMP {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                                Case "CFW_Fld_Values_Opp"
                                                    SqlSortColumn.Append(String.Format(" ORDER BY (CASE WHEN is_date(COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID)),'')) = true THEN COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,OpportunityMaster.numOppID)),'') ELSE NULL END)::TIMESTAMP {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                                Case "CFW_FLD_Values_Pro"
                                                    SqlSortColumn.Append(String.Format(" ORDER BY (CASE WHEN is_date(COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID)),'')) = true THEN COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,ProjectsMaster.numProID)),'') ELSE NULL END)::TIMESTAMP {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                                Case "CFW_Fld_Values_Serialized_Items"
                                                    SqlSortColumn.Append(String.Format(" ORDER BY (CASE WHEN is_date(COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID)),'')) = true THEN COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,Item.numItemCode)),'') ELSE NULL END)::TIMESTAMP {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                            End Select
                                        End If
                                    Else
                                        Select Case dr(0)("vcLookBackTableName")
                                            Case "CFW_FLD_Values"
                                                SqlSortColumn.Append(String.Format(" ORDER BY COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,DivisionMaster.numDivisionID)),'') {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                            Case "CFW_FLD_Values_Cont"
                                                SqlSortColumn.Append(String.Format(" ORDER BY COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,AdditionalContactsInformation.numContactID)),'') {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                            Case "CFW_FLD_Values_Case"
                                                SqlSortColumn.Append(String.Format(" ORDER BY COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,Cases.numCaseId)),'') {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                            Case "CFW_Fld_Values_Opp"
                                                SqlSortColumn.Append(String.Format(" ORDER BY COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,OpportunityMaster.numOppID)),'') {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                            Case "CFW_FLD_Values_Pro"
                                                SqlSortColumn.Append(String.Format(" ORDER BY COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,ProjectsMaster.numProID)),'') {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                            Case "CFW_Fld_Values_Serialized_Items"
                                                SqlSortColumn.Append(String.Format(" ORDER BY COALESCE(fn_GetCustFldStringValue({0},0,GetCustFldValue({0},-1::SMALLINT,Item.numItemCode)),'') {1}", dr(0)("numFieldID"), objReport.SortDirection))
                                        End Select
                                    End If
                                Else
                                    If dr(0)("numReportFieldGroupID") = 35 AndAlso Not CCommon.ToString(dr(0)("vcLookBackTableName")).StartsWith("AC") Then
                                        dr(0)("vcLookBackTableName") = "AC" & dr(0)("vcLookBackTableName")
                                    End If

                                    If dr(0)("vcAssociatedControlType") = "DateField" Then
                                        SqlSortColumn.Append(String.Format(" ORDER BY {0}.{1} {2}", dr(0)("vcLookBackTableName"), dr(0)("vcOrigDbColumnName"), objReport.SortDirection))
                                    Else
                                        SqlSortColumn.Append(String.Format(" ORDER BY ""{0}"" {1}", objReport.SortColumn, objReport.SortDirection))
                                    End If
                                End If
                            End If
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "24" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY ReturnHeader.numReturnHeaderID ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "1" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY DivisionMaster.numDivisionID ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "5" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY AdditionalContactsInformation.numContactID ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "13" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY OpportunityMaster.numOppID ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "10" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY OpportunityMaster.numOppID ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "8" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY OpportunityMaster.numOppID ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "9" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY OpportunityMaster.numOppID ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "19" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY Item.numItemCode ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "23" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY Item.numItemCode ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "14" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY ProjectsMaster.numProID ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "16" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY Cases.numCaseID ASC ")
                        ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("numReportModuleGroupID")) = "25" AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 3 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 4 AndAlso CCommon.ToShort(ds.Tables(0).Rows(0)("tintReportType")) <> 5 Then
                            SqlSortColumn.Append(" ORDER BY MassStockTransfer.dtTransferredDate ASC ")
                        Else
                            objReport.SortColumn = ""
                            objReport.SortDirection = ""
                        End If

                        If CCommon.ToString(SqlFrom.ToString()).Contains("DepositMaster") _
                       AndAlso (CCommon.ToString(SqlCondition.ToString()).Contains("294") _
                                OrElse CCommon.ToString(SqlCondition.ToString()).Contains("295") _
                                OrElse CCommon.ToString(SqlCondition.ToString()).Contains("297") _
                                OrElse CCommon.ToString(SqlCondition.ToString()).Contains("299")) Then

                            Dim strTempCols As String = SqlSortColumn.ToString().ToLower()
                            If strTempCols.Length > 0 Then
                                strTempCols = CCommon.ToString(strTempCols.ToString()).Replace("order by", "")
                                strTempCols = CCommon.ToString(strTempCols.ToString()).Replace("desc", "")
                                strTempCols = CCommon.ToString(strTempCols.ToString()).Replace("asc", "")

                                Dim strTempSQL As String = SqlColumn.ToString().ToLower()
                                If strTempSQL.ToLower().StartsWith("select") _
                               AndAlso Not strTempSQL.ToLower().Contains("top") _
                               AndAlso Not strTempCols.ToLower().Contains("_") Then
                                    strTempSQL = strTempSQL.Replace(strTempSQL.Substring(0, "select".Length), "SELECT DISTINCT " & strTempCols & ",")
                                End If
                                SqlColumn.Clear()
                                SqlColumn.Append(strTempSQL)

                            End If
                        End If

                        Sql.Append(SqlColumn)
                        Sql.Append(SqlFrom)
                        Sql.Append(SqlCondition)
                        Sql.Append(SqlGroupBy)
                        Sql.Append(SqlSortColumn)
                        Sql.Append(SqlLimit)

                        Return Sql.ToString()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Function GetTodaysDate() As String
            Return "CAST(timezone('utc',now()) + make_interval(mins => -@ClientTimeZoneOffset) AS DATE) + make_interval(mins => @ClientTimeZoneOffset)"
        End Function

        Function ConditionOperator(ByVal OperatorType As String, ByVal FilterValue As String, ByVal vcAssociatedControlType As String, ByVal vcFieldDataType As String, bitCustom As Boolean) As String
            Try
                Dim strOperatorSign As String = "="

                If vcAssociatedControlType = "SelectBox" Or vcAssociatedControlType = "ListBox" Or vcAssociatedControlType = "CheckBox" Then
                    If vcFieldDataType = "V" Or vcFieldDataType = "Y" Then
                        Dim strValue() As String = FilterValue.Split(",")
                        For i As Integer = 0 To strValue.Length - 1
                            strValue(i) = String.Format("'{0}'", strValue(i))
                        Next

                        FilterValue = String.Join(",", strValue)
                    End If

                    Select Case OperatorType
                        Case "eq"
                            strOperatorSign = " in(" & FilterValue & ")"
                        Case "ne"
                            strOperatorSign = " not in(" & FilterValue & ")"
                    End Select
                ElseIf vcAssociatedControlType = "DateField" Then
                    Select Case OperatorType
                        Case "eq"
                            strOperatorSign = "='" & FilterValue & "'"
                        Case "ne"
                            strOperatorSign = "!='" & FilterValue & "'"
                        Case "gt"
                            strOperatorSign = ">'" & FilterValue & "'"
                        Case "ge"
                            strOperatorSign = ">='" & FilterValue & "'"
                        Case "lt"
                            strOperatorSign = "<'" & FilterValue & "'"
                        Case "le"
                            strOperatorSign = "<='" & FilterValue & "'"
                    End Select
                Else
                    Select Case OperatorType
                        Case "eq"
                            strOperatorSign = "='" & FilterValue & "'"
                        Case "ne"
                            strOperatorSign = "!='" & FilterValue & "'"
                        Case "gt"
                            strOperatorSign = ">" & FilterValue
                        Case "ge"
                            strOperatorSign = ">=" & FilterValue
                        Case "lt"
                            strOperatorSign = "<" & FilterValue
                        Case "le"
                            strOperatorSign = "<=" & FilterValue
                        Case "co"
                            strOperatorSign = " ilike '%" & FilterValue & "%'"
                        Case "nc"
                            strOperatorSign = " not ilike '%" & FilterValue & "%'"
                        Case "sw"
                            strOperatorSign = " ilike '" & FilterValue & "%'"
                    End Select
                End If

                Return strOperatorSign
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPrebuildReportData() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim ds As DataSet = New DataSet()
                With sqlParams
                    .Add(SqlDAL.Add_Parameter("v_numdomainid", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("v_numusercntid", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("v_clienttimezoneoffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("v_intdefaultreportid", DefaultReportID, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("v_numdashboardid", DashBoardID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("swv_refcur", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("swv_refcur2", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("swv_refcur3", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("swv_refcur4", DBNull.Value, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Using connection As New Npgsql.NpgSqlConnection(connString)
                    Dim sqlCommand As New Npgsql.NpgsqlCommand
                    sqlCommand.CommandTimeout = 300
                    sqlCommand.Connection = connection
                    sqlCommand.CommandType = CommandType.StoredProcedure
                    sqlCommand.Parameters.AddRange(sqlParams.ToArray())
                    sqlCommand.CommandText = "usp_reportlistmaster_runprebuildreport"
                    Dim sqlAdapter As Npgsql.NpgsqlDataAdapter
                    Dim i As Int32 = 0
                    connection.Open()
                    Using objTransaction As Npgsql.NpgsqlTransaction = connection.BeginTransaction()
                        sqlCommand.ExecuteNonQuery()

                        For Each parm As Npgsql.NpgsqlParameter In sqlCommand.Parameters
                            If parm.NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor Then

                                If Not String.IsNullOrEmpty(Convert.ToString(parm.Value)) Then
                                    Dim parm_val As String = String.Format("FETCH ALL IN ""{0}""", parm.Value.ToString())
                                    sqlAdapter = New Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection)
                                    ds.Tables.Add(parm.Value.ToString())
                                    sqlAdapter.Fill(ds.Tables(i))
                                    i += 1
                                End If
                            End If
                        Next

                        objTransaction.Commit()
                    End Using
                    connection.Close()
                End Using
                Return ds
                'Return SqlDAL.ExecuteDataset(connString, "USP_ReportListMaster_RunPrebuildReport", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetPrebuildReprtHtml(ByVal isForEmail As Boolean) As String
            Dim reportHtml As String = ""
            reportHtml += "<div class='box box-primary'>"
            reportHtml += "<div class='box-header handle'>"
            reportHtml += "<h3 class='box-title'>"
            reportHtml += "##REPORTHEADER##"
            reportHtml += "</h3>"
            If Not isForEmail Then
                reportHtml += "<div id='tdEdit'class='box-tools pull-right'>"
                reportHtml += "<a id='hplExport' class='btn btn-box-tool' href='../ReportDashboard/frmManageDashBoard.aspx?Move=EX&DID=" & Me.DashBoardID & "' ><img alt='' src='../images/excel.png' height='16'/></a>"
                reportHtml += "<a id='hplEdit' class='btn btn-box-tool' href='../ReportDashboard/frmAddDashboardReport.aspx?DID=" & Me.DashBoardID & "' ><img alt='' src='../images/pencil-24.gif' height='16'/></a>"
                reportHtml += "<a id='hplDelete' class='btn btn-box-tool' href='../ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & Me.DashBoardID & "' ><img alt='' src='../images/cancel.png'/></a>"
                reportHtml += "</div>"
            End If            
            reportHtml += "</div>"
            reportHtml += "<div class='box-body rptcontent'>"
            reportHtml += "##REPORTBODY##"
            reportHtml += "</div>"
            reportHtml += "##REPORTFOOTER##"
            reportHtml += "</div>"

            Try
                Dim dtReportData As DataTable = Me.GetReportDashboardDTL()
                If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                    reportHtml = reportHtml.Replace("##REPORTHEADER##", CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText")))

                    If Not String.IsNullOrEmpty(CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText"))) Then
                        reportHtml = reportHtml.Replace("##REPORTFOOTER##", "<div class='box-footer footer'>" & CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText")) & "</div>")
                    Else
                        reportHtml = reportHtml.Replace("##REPORTFOOTER##", "")
                    End If
                End If

                Dim ds As DataSet = Me.GetPrebuildReportData()

                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByProfitAmountCompanyWide Then
                    Dim reportBody As String = "<table class='table table-bordered table-striped' style='margin-bottom:0px'>"
                    reportBody += "<tr>"
                    reportBody += "<th>Item</th>"
                    reportBody += "<th>Profit</th>"
                    reportBody += "</tr>"

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            reportBody += "<tr>"
                            reportBody += "<td>" & Convert.ToString(dr("vcItemName")) & "</td>"
                            reportBody += "<td>" & String.Format("{0:#,##0.00###}", Convert.ToDouble(dr("Profit"))) & "</td>"
                            reportBody += "</tr>"
                        Next
                    End If
                    reportBody += "</table>"

                    reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByRevenueSoldCompanyWide Then
                    Dim reportBody As String = "<table class='table table-bordered table-striped' style='margin-bottom:0px'>"
                    reportBody += "<tr>"
                    reportBody += "<th>Item</th>"
                    reportBody += "<th>Amount</th>"
                    reportBody += "</tr>"

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            reportBody += "<tr>"
                            reportBody += "<td>" & Convert.ToString(dr("vcItemName")) & "</td>"
                            reportBody += "<td>" & String.Format("{0:#,##0.00###}", Convert.ToDouble(dr("TotalAmount"))) & "</td>"
                            reportBody += "</tr>"
                        Next
                    End If
                    reportBody += "</table>"

                    reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.ProfitMarginByItemClassificationCompanyWide Then
                    Dim reportBody As String = "<table class='table table-bordered table-striped' style='margin-bottom:0px'>"
                    reportBody += "<tr>"
                    reportBody += "<th>Item</th>"
                    reportBody += "<th>Profit Percent</th>"
                    reportBody += "</tr>"

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            reportBody += "<tr>"
                            reportBody += "<td>" & Convert.ToString(dr("vcItemClassification")) & "</td>"
                            reportBody += "<td>" & String.Format("{0:#,##0.##}", Convert.ToDouble(dr("BlendedProfit"))) & "%</td>"
                            reportBody += "</tr>"
                        Next
                    End If
                    reportBody += "</table>"

                    reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.SalesVsExpense Then
                    Dim reportBody As String = "<table class='table table-bordered table-striped' style='margin-bottom:0px'>"
                    reportBody += "<tr>"
                    reportBody += "<th>Month</th>"
                    reportBody += "<th>Sales</th>"
                    reportBody += "<th>Expense</th>"
                    reportBody += "</tr>"

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            reportBody += "<tr>"
                            reportBody += "<td>" & Convert.ToString(dr("MonthLabel")) & "</td>"
                            reportBody += "<td>" & String.Format("{0:#,##0.00###}", Convert.ToDouble(dr("MonthSales"))) & "</td>"
                            reportBody += "<td>" & String.Format("{0:#,##0.00###}", Convert.ToDouble(dr("MonthExpense"))) & "</td>"
                            reportBody += "</tr>"
                        Next
                    End If
                    reportBody += "</table>"

                    reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopCustomersByPortionOfTotalSales Then
                    Dim reportBody As String = "<table class='table table-bordered table-striped' style='margin-bottom:0px'>"
                    reportBody += "<tr>"
                    reportBody += "<th>Customer</th>"
                    reportBody += "<th>Sales Percent</th>"
                    reportBody += "</tr>"

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In ds.Tables(0).Rows
                            reportBody += "<tr>"
                            reportBody += "<td>" & Convert.ToString(dr("vcCompanyName")) & "</td>"
                            reportBody += "<td>" & String.Format("{0:#,##0.##}", Convert.ToDouble(dr("TotalSalesPercent"))) & "%</td>"
                            reportBody += "</tr>"
                        Next
                    End If
                    reportBody += "</table>"

                    reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.MoneyInBank Then
                    Try
                        reportHtml = "<table class='rptcontent bg-primary handle' style='white-space: nowrap; text-align: center;'>"
                        reportHtml += "<tr>"
                        reportHtml += "<td style='padding:10px'>"
                        reportHtml += "<img src='../images/MoneyInBank.png' height='60' /></td>"
                        reportHtml += "<td>"
                        reportHtml += "<h2 style='font-weight: 500;'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            reportHtml += String.Format("{0:$#,##0.00}", CCommon.ToDouble(ds.Tables(0).Rows(0)("MoneyInBank")))
                        Else
                            reportHtml += String.Format("{0:$#,##0.00}", 0)
                        End If
                        reportHtml += "</h2>"
                        reportHtml += "<p>Money in the Bank</p>"
                        reportHtml += "</td>"
                        If Not isForEmail Then
                            reportHtml += "<td style='vertical-align: top'>"
                            reportHtml += "<div class='box-tools pull-right'>"
                            reportHtml += "<a class='btn btn-box-tool' href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & DashBoardID & "'><img alt='' src='../images/cancel.png' /></a>"
                            reportHtml += "</div>"
                            reportHtml += "</td>"
                        End If
                        reportHtml += "</tr>"
                        reportHtml += "</table>"
                    Catch ex As Exception
                        reportHtml = "<label class='text-red'>Error ocurred while rendering this report</label>"
                    End Try
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.SalesOppWonLost Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/salesopportunity70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table table-bordered table-striped' style='margin-bottom:0px'>"
                        reportBody += "<tr>"
                        reportBody += "<th></th>"
                        reportBody += "<th>Sales Opp Amt</th>"
                        reportBody += "<th>Closed Won Amt</th>"
                        reportBody += "<th>Close Won%</th>"
                        reportBody += "<th>Closed Lost Amt</th>"
                        reportBody += "<th>Close Lost %</th>"
                        reportBody += "</tr>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<th>" & Convert.ToString(dr("GroupValue")) & "</th>"
                                reportBody += "<td>" & Convert.ToString(dr("SalesOppAmount")) & "</td>"
                                reportBody += "<td>" & Convert.ToString(dr("ClosedWonAmount")) & "</td>"
                                reportBody += "<td>" & Convert.ToString(dr("CloseWonPercent")) & "</td>"
                                reportBody += "<td>" & Convert.ToString(dr("CloseLostAmount")) & "</td>"
                                reportBody += "<td>" & Convert.ToString(dr("CloseLostPercent")) & "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.SalesOppPipeLine Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/salesopportunity70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table table-bordered table-striped' style='margin-bottom:0px'>"
                        reportBody += "<tr>"
                        reportBody += "<th></th>"
                        reportBody += "<th>Sales Opp Amt</th>"
                        reportBody += "<th>Total Progress%</th>"
                        reportBody += "</tr>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<th>" & Convert.ToString(dr("GroupValue")) & "</th>"
                                reportBody += "<td>" & Convert.ToString(dr("SalesOppAmount")) & "</td>"
                                reportBody += "<td>" & Convert.ToString(dr("numTotalProgress")) & "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.PendingReportDetails Then
                        Dim reportBody As String = "<table style='width:100%'>"
                        reportBody += "<tr>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label>Received But Not Billed</label>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;'>"

                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:13px;text-align:center;' >"
                                If isForEmail Then
                                    reportBody += "<span style='color:purple'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("PendingToBill"))) & "</span>"
                                Else
                                reportBody += "<a style='color:purple' href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "opportunity/frmMassPurchaseFulfillment.aspx?View=3&ShowReceivedButNotBilled=2" & "' target='_blank'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("PendingToBill"))) & "</a>"
                                End If
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "Billed But Not Received</td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;'>"
                            For Each dr As DataRow In ds.Tables(1).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:13px;text-align:center;'>"
                                If isForEmail Then
                                    reportBody += "<span style='color:blue'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("BilledButNotReceived"))) & "</span>"
                                Else
                                reportBody += "<a style='color:blue' href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "/opportunity/frmMassPurchaseFulfillment.aspx?View=1&ShowBilled=True" & "' target='_blank'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("BilledButNotReceived"))) & "</a>"
                                End If
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "Sold But Not Shipped</td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;'>"
                            For Each dr As DataRow In ds.Tables(2).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:13px;text-align:center;'>"
                                If isForEmail Then
                                    reportBody += "<span style='color:green'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("PendingSales"))) & "</span>"
                                Else
                                reportBody += "<a style='color:green' href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "opportunity/frmMassSalesFulfillment.aspx?View=2" & "' target='_blank'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("PendingSales"))) & "</a>"
                                End If
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If



                        reportBody += "</tr>"
                        reportBody += "</table>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.MonthToDateRevenue Then
                        Dim reportBody As String = "<div class='row'>"
                        reportBody += "<div class='col-xs-12'>"
                        reportBody += "<div class='table-responsive'>"
                        reportBody += "<table class='table table-bordered table-striped' style='margin-bottom: 0px;'>"
                        reportBody += "<thead>"
                        reportBody += "<tr>"
                        reportBody += "<th>"
                        reportBody += "Assigned To"
                        reportBody += "</th>"
                        reportBody += "<th>"
                        reportBody += "Total Records"
                        reportBody += "</th>"
                        reportBody += "<th>"
                        reportBody += "Invoice Grand Total(Rounded)"
                        reportBody += "</th>"
                        reportBody += "</tr>"
                        reportBody += "</thead>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                If isForEmail Then
                                    reportBody += CCommon.ToString(dr("AssignedTo"))
                                Else
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & CCommon.ToString(dr("URL")).Replace("~/", "") & "' target='_blank'>" & CCommon.ToString(dr("AssignedTo")) & "</a>"
                                End If
                                reportBody += "</td>"
                                reportBody += "<td>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("TotalRecords")))
                                reportBody += "</td>"
                                reportBody += "<td>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("InvoiceGrandtotal")))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                        End If


                        reportBody += "<tfoot>"
                        reportBody += "<tr>"
                        reportBody += "<td>"
                        reportBody += "Total"
                        reportBody += "</td>"
                        reportBody += "<td>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                            reportBody += String.Format("{0:#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(TotalRecords)", String.Empty)))
                        Else
                            reportBody += String.Format("{0:#,##0.##}", 0)
                        End If
                        reportBody += "</td>"
                        reportBody += "<td>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                            reportBody += String.Format("{0:#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(InvoiceGrandtotal)", String.Empty)))
                        Else
                            reportBody += String.Format("{0:#,##0.##}", 0)
                        End If
                        reportBody += "</td>"
                        reportBody += "</tr>"
                        reportBody += "</tfoot>"
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.BackOrderValuation Then
                        Dim reportBody As String = "<table class='table table-bordered table-striped' style='margin-bottom: 0px;'>"
                        reportBody += "<tr>"
                        reportBody += "<th>Resource</th>"
                        reportBody += "<th>Total Records</th>"
                        reportBody += "<th>Sales(Cost)</th>"
                        reportBody += "</tr>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                If isForEmail Then
                                    reportBody += CCommon.ToString(dr("RecordOwner"))
                                Else
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&InventorySort=3" & "&SelectedID=" & CCommon.ToInteger(dr("ID")) & "' target='_blank'>" & CCommon.ToString(dr("RecordOwner")) & "</a>"
                                End If
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("TotalRecords")))
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                If isForEmail Then
                                    reportBody += "<span style='color:green'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("SalesPrice"))) & "</span>&nbsp;<span style='color:red'>(" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("CostPrice"))) + ")</span>"
                                Else
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&InventorySort=3" & "&SelectedID=" & CCommon.ToInteger(dr("ID")) & "' target='_blank' style='color:green'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("SalesPrice"))) & "</a>"
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&InventorySort=3" & "&SelectedID=" & CCommon.ToInteger(dr("ID")) & "' target='_blank' style='color:red'>(" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("CostPrice"))) + ")</a>"
                                End If

                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If

                        reportBody += "<tr>"
                        reportBody += "<th>"
                        reportBody += "Total"
                        reportBody += "</th>"
                        reportBody += "<th style='text-align:right'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            reportBody += String.Format("{0:#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(TotalRecords)", String.Empty)))
                        Else
                            reportBody += String.Format("{0:#,##0.##}", 0)
                        End If
                        reportBody += "</th>"
                        reportBody += "<th style='text-align:right'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            If Not isForEmail Then
                            reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "Opportunity/frmDealList.aspx?type=1&FilterBy=" & ds.Tables(0).Rows(0).Item("vcFilterBy") & "&InventorySort=3" & "' target='_blank' style='color:green;font-weight:bold'>" & String.Format("{0:$#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(SalesPrice)", String.Empty))) & "</a>"
                            reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "Opportunity/frmDealList.aspx?type=1&FilterBy=" & ds.Tables(0).Rows(0).Item("vcFilterBy") & "&InventorySort=3" & "' target='_blank' style='color:red;font-weight:bold'>" & "(" & String.Format("{0:$#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(CostPrice)", String.Empty))) & ")" & "</a>"
                            Else
                                reportBody += "<span style='color:green'>" & String.Format("{0:$#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(SalesPrice)", String.Empty))) & "</span>&nbsp;<span style='color:red'>(" & String.Format("{0:$#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(CostPrice)", String.Empty))) + ")</span>"
                            End If
                        Else
                            reportBody += "<span style='color:green'>" & String.Format("{0:$#,##0.##}", 0) & "</span>&nbsp;<span style='color:red'>(" & String.Format("{0:$#,##0.##}", 0) + ")</span>"
                        End If

                        reportBody += "</th>"
                        reportBody += "</tr>"
                        reportBody += "</table>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.ActionItemsAndMeetingsCompanyWide Then
                        Dim reportBody As String = "<table class='table table-bordered'>"
                        reportBody += "<thead>"
                        reportBody += "<tr>"
                        reportBody += "<th></th>"
                        reportBody += "<th>Customer</th>"
                        reportBody += "<th>Activity</th>"
                        reportBody += "<th>Date/Time</th>"
                        reportBody += "</tr>"
                        reportBody += "</thead>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td rowspan='2' style='vertical-align:middle'><i class='fa fa-check-circle fa-6' style='font-size:30px; color:#00a65a' aria-hidden='true'></i></td>"
                                reportBody += "<td>"
                                reportBody += CCommon.ToString(dr("vcOrganizationName"))
                                reportBody += "</td>"
                                reportBody += "<td>"
                                If isForEmail Then
                                    reportBody += CCommon.ToString(dr("vcType"))
                                Else
                                    reportBody += "<a onclick='" & If(CCommon.ToShort(dr("tintType")) = 2, "return OpenCalItem(" & CCommon.ToLong(dr("ID")) & ");", "return OpenActionItem(" & CCommon.ToLong(dr("ID")) & ");") & "'>" & CCommon.ToString(dr("vcType")) & "</a>"
                                End If
                                reportBody += "</td>"
                                reportBody += "<td>"
                                reportBody += CCommon.ToString(dr("dtFormatted"))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                                reportBody += "<tr>"
                                reportBody += "<td colspan='4'>"
                                reportBody += CCommon.ToString(dr("vcComments"))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If

                        reportBody += "</table>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.AP Then
                        Try
                            If isForEmail Then
                                reportHtml = "<table class='rptcontent bg-red handle' style='width:100%'>"
                            Else
                                reportHtml = "<table class='rptcontent bg-red handle'>"
                            End If
                            reportHtml += "<tr>"
                            reportHtml += "<td>"
                            reportHtml += "<table style='white-space: nowrap; text-align: center; border-spacing: 5px; width: 100%'>"
                            reportHtml += "<tr>"
                            reportHtml += "<td rowspan='2' style='white-space:nowrap;padding-right:10px;'>"
                            reportHtml += "<table>"
                            reportHtml += "<tr>"
                            reportHtml += "<td>"
                            reportHtml += "<img src='../images/PiggyBank.png' width='100' /></td>"
                            reportHtml += "<td>"
                            reportHtml += "<h2 style='font-weight: 600;'>"
                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                reportHtml += String.Format("{0:$#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("TotalAP")))
                            Else
                                reportHtml += String.Format("{0:$#,##0.00}", 0)
                            End If
                            reportHtml += "</h2>"
                            reportHtml += "<p>Total A/P</p>"
                            reportHtml += "</td>"
                            reportHtml += "</tr>"
                            reportHtml += "</table>"
                            reportHtml += "</td>"
                            reportHtml += "<td style='width: 50%;padding-right:10px;padding-left:10px'>"
                            reportHtml += "<h4 style='font-weight: 600;'>"
                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                reportHtml += String.Format("{0:$#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("APWithin30Days")))
                            Else
                                reportHtml += String.Format("{0:$#,##0.00}", 0)
                            End If
                            reportHtml += "</h4>"
                            reportHtml += "<p>Within 30 days</p>"
                            reportHtml += "</td>"
                            reportHtml += "<td style='width: 50%;padding-left:10px'>"
                            reportHtml += "<h4 style='font-weight: 600;'>"
                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                reportHtml += String.Format("{0:$#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("APWithin31to60Days")))
                            Else
                                reportHtml += String.Format("{0:$#,##0.00}", 0)
                            End If
                            reportHtml += "</h4>"
                            reportHtml += "<p>31-60 Days</p>"
                            reportHtml += "</td>"
                            reportHtml += "</tr>"
                            reportHtml += "<tr>"
                            reportHtml += "<td>"
                            reportHtml += "<h4 style='font-weight: 600;'>"
                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                reportHtml += String.Format("{0:$#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("APWithin61to90Days")))
                            Else
                                reportHtml += String.Format("{0:$#,##0.00}", 0)
                            End If
                            reportHtml += "</h4>"
                            reportHtml += "<p>61-90 Days</p>"
                            reportHtml += "</td>"
                            reportHtml += "<td>"
                            reportHtml += "<h4 style='font-weight: 600;'>"
                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                reportHtml += String.Format("{0:$#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("APPastDue")))
                            Else
                                reportHtml += String.Format("{0:$#,##0.00}", 0)
                            End If
                            reportHtml += "</h4>"
                            reportHtml += "<p>Past Due</p>"
                            reportHtml += "</td>"
                            reportHtml += "</tr>"
                            reportHtml += "</table>"
                            reportHtml += "</td>"
                            If Not isForEmail Then
                                reportHtml += "<td style='vertical-align: top'>"
                                reportHtml += "<div class='box-tools pull-right'>"
                            reportHtml += "<a class='btn btn-box-tool' href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & DashBoardID & "'><img alt='' src='../images/cancel.png' /></asp:HyperLink>"
                                reportHtml += "</div>"
                                reportHtml += "</td>"
                            End If
                            reportHtml += "</tr>"
                            reportHtml += "</table>"

                            If Not isForEmail Then
                            reportHtml = ("<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "Accounting/frmAccountsPayable.aspx" & "' target='_blank'>" & reportHtml & "</a>")
                            End If
                        Catch ex As Exception
                            reportHtml = "<label class='text-red'>Error ocurred while rendering this report</label>"
                        End Try
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.AR Then
                        Try
                            If isForEmail Then
                                reportHtml = "<table class='rptcontent bg-green handle' style='width:100%'>"
                            Else
                                reportHtml = "<table class='rptcontent bg-green handle'>"
                            End If
                            reportHtml += "<tr>"
                            reportHtml += "<td>"
                            reportHtml += "<table style='white-space: nowrap; text-align: center; border-spacing: 5px; width: 100%'>"
                            reportHtml += "<tr>"
                            reportHtml += "<td rowspan='2' style='white-space: nowrap;padding-right:10px'>"
                            reportHtml += "<table>"
                            reportHtml += "<tr>"
                            reportHtml += "<td>"
                            reportHtml += "<img src='../images/PiggyBank.png' width='100' /></td>"
                            reportHtml += "<td>"
                            reportHtml += "<h2 style='font-weight: 600;'>"
                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                reportHtml += String.Format("{0:$#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("TotalAR")))
                            Else
                                reportHtml += String.Format("{0:$#,##0.00}", 0)
                            End If
                            reportHtml += "</h2>"
                            reportHtml += "<p>Total A/R</p>"
                            reportHtml += "</td>"
                            reportHtml += "</tr>"
                            reportHtml += "</table>"
                            reportHtml += "</td>"
                            reportHtml += "<td style='width: 50%;padding-right:10px;padding-left:10px'>"
                            reportHtml += "<h4 style='font-weight: 600;'>"
                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                reportHtml += String.Format("{0:$#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ARWithin30Days")))
                            Else
                                reportHtml += String.Format("{0:$#,##0.00}", 0)
                            End If
                            reportHtml += "</h4>"
                            reportHtml += "<p>Within 30 days</p>"
                            reportHtml += "</td>"
                            reportHtml += "<td style='width: 50%;padding-left:10px'>"
                            reportHtml += "<h4 style='font-weight: 600;'>"
                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                reportHtml += String.Format("{0:$#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ARWithin31to60Days")))
                            Else
                                reportHtml += String.Format("{0:$#,##0.00}", 0)
                            End If
                            reportHtml += "</h4>"
                            reportHtml += "<p>31-60 Days</p>"
                            reportHtml += "</td>"
                            reportHtml += "</tr>"
                            reportHtml += "<tr>"
                            reportHtml += "<td>"
                            reportHtml += "<h4 style='font-weight: 600;'>"
                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                reportHtml += String.Format("{0:$#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ARWithin61to90Days")))
                            Else
                                reportHtml += String.Format("{0:$#,##0.00}", 0)
                            End If
                            reportHtml += "</h4>"
                            reportHtml += "<p>61-90 Days</p>"
                            reportHtml += "</td>"
                            reportHtml += "<td>"
                            reportHtml += "<h4 style='font-weight: 600;'>"
                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                reportHtml += String.Format("{0:$#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ARPastDue")))
                            Else
                                reportHtml += String.Format("{0:$#,##0.00}", 0)
                            End If
                            reportHtml += "</h4>"
                            reportHtml += "<p>Past Due</p>"
                            reportHtml += "</td>"
                            reportHtml += "</tr>"
                            reportHtml += "</table>"
                            reportHtml += "</td>"
                            If Not isForEmail Then
                                reportHtml += "<td style='vertical-align: top'>"
                                reportHtml += "<div class='box-tools pull-right'>"
                            reportHtml += "<a class='btn btn-box-tool' href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & DashBoardID & "'><img alt='' src='../images/cancel.png' /></asp:HyperLink>"
                                reportHtml += "</div>"
                                reportHtml += "</td>"
                            End If
                            reportHtml += "</tr>"
                            reportHtml += "</table>"

                            If Not isForEmail Then
                            reportHtml = ("<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "Accounting/frmAccountsReceivable.aspx" & "' target='_blank'>" & reportHtml & "</a>")
                            End If
                        Catch ex As Exception
                            reportHtml = "<label class='text-red'>Error ocurred while rendering this report</label>"
                        End Try
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.InventoryValueByWareHouse Then
                        Dim reportBody As String = "<table class='table table-bordered'><tr><th>WareHouse</th><th>Inventory Value</th></tr>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                reportBody += CCommon.ToString(dr("WareHouse"))
                                reportBody += "</td>"
                                reportBody += "<td>"
                                If isForEmail Then
                                    reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("InventoryValue")))
                                Else
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & "Items/frmItemList.aspx?Page=+InventoryItems&ItemGroup=0&WID=" & CCommon.ToInteger(dr("WareHouseID")) & "&Export=True" & "' target='_blank'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("InventoryValue"))) & "</a>"
                                End If
                                reportBody += "</td></tr>"
                            Next
                        End If

                        reportBody += "<tr><th>Total</th><th>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                            If isForEmail Then
                                reportBody += String.Format("{0:$#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(InventoryValue)", String.Empty)))
                            Else
                                reportBody += "<a style='color:#00cc00;font-size:25px;height:0.5px' onclick='return OpenItemList();'>" & String.Format("{0:$#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(InventoryValue)", String.Empty))) & "</a>"
                            End If
                        Else
                            If isForEmail Then
                                reportBody += String.Format("{0:$#,##0.##}", 0)
                            Else
                                reportBody += "<a style='color:#00cc00;font-size:25px;height:0.5px' onclick='return OpenItemList();'>" & String.Format("{0:$#,##0.##}", 0) & "</a>"
                            End If
                        End If
                        reportBody += "</th></tr>"
                        reportBody += "</table>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.RPEYTDvsSamePeriodLastYear Then
                        Dim reportBody As String = "<table style='width: 100%;text-align: center;'>"
                        reportBody += "<tr>"
                        reportBody += "<td style='width:33.33%'>"
                        reportBody += "<h3 style='margin-top: 0px;margin-bottom: 0px;'>Revenue</h3>"
                        reportBody += "<br/>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            If CCommon.ToDouble(ds.Tables(0).Rows(0)("RevenueDifference")) > 0 Then
                                reportBody += "<label style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("RevenueDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></label>"
                            Else
                                reportBody += "<label style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("RevenueDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></label>"
                            End If
                        End If
                        reportBody += "<td>"
                        reportBody += "<td style='width:33.33%'>"
                        reportBody += "<h3 style='margin-top: 0px;margin-bottom: 0px;'>Profit</h3>"
                        reportBody += "<br/>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        If CCommon.ToDouble(ds.Tables(0).Rows(0)("ProfitDifference")) > 0 Then
                            reportBody += "<label style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ProfitDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></label>"
                        Else
                            reportBody += "<label style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ProfitDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></label>"
                        End If
                        End If
                        reportBody += "<td>"
                        reportBody += "<td style='width:33.33%'>"
                        reportBody += "<h3 style='margin-top: 0px;margin-bottom: 0px;'>Expense</h3>"
                        reportBody += "<br/>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        If CCommon.ToDouble(ds.Tables(0).Rows(0)("ExpenseDifference")) > 0 Then
                            reportBody += "<label style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ExpenseDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></label>"
                        Else
                            reportBody += "<label style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ExpenseDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></label>"
                        End If
                        End If
                        reportBody += "<td>"
                        reportBody += "</tr>"
                        reportBody += "</table>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.RPELastMonthvsSamePeriodLastYear Then
                        Dim reportBody As String = "<table style='width: 100%;text-align: center;'>"
                        reportBody += "<tr>"
                        reportBody += "<td style='width:33.33%'>"
                        reportBody += "<h3 style='margin-top: 0px;margin-bottom: 0px;'>Revenue</h3>"
                        reportBody += "<br/>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            If CCommon.ToDouble(ds.Tables(0).Rows(0)("RevenueDifference")) > 0 Then
                                reportBody += "<label style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("RevenueDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></label>"
                            Else
                                reportBody += "<label style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("RevenueDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></label>"
                            End If
                        End If
                        reportBody += "<td>"
                        reportBody += "<td style='width:33.33%'>"
                        reportBody += "<h3 style='margin-top: 0px;margin-bottom: 0px;'>Profit</h3>"
                        reportBody += "<br/>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        If CCommon.ToDouble(ds.Tables(0).Rows(0)("ProfitDifference")) > 0 Then
                            reportBody += "<label style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ProfitDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></label>"
                        Else
                            reportBody += "<label style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ProfitDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></label>"
                        End If
                        End If
                        reportBody += "<td>"
                        reportBody += "<td style='width:33.33%'>"
                        reportBody += "<h3 style='margin-top: 0px;margin-bottom: 0px;'>Expense</h3>"
                        reportBody += "<br/>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        If CCommon.ToDouble(ds.Tables(0).Rows(0)("ExpenseDifference")) > 0 Then
                            reportBody += "<label style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ExpenseDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></label>"
                        Else
                            reportBody += "<label style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ExpenseDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></label>"
                        End If
                        End If
                        reportBody += "<td>"
                        reportBody += "</tr>"
                        reportBody += "</table>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.KPI Then
                        Dim reportBody As String = "<table class='table table-bordered table-striped'>"
                        reportBody += "<tr>"
                        reportBody += "<th>Report</th>"
                        reportBody += "<th>Object(s) of Measure</th>"
                        reportBody += "<th>Period being compared</th>"
                        reportBody += "<th>Change</th>"
                        reportBody += "</tr>"
                        reportBody += "<tr>"
                        reportBody += "<td>Lead to Account Conversion</td>"
                        reportBody += "<td>Leads & Accounts</td>"
                        reportBody += "<td>YTD vs same period last year</td>"
                        reportBody += "<td class='text-center' style='white-space:nowrap;'>##1##</td>"
                        reportBody += "</tr>"
                        reportBody += "<tr>"
                        reportBody += "<td>Lead to Account Conversion</td>"
                        reportBody += "<td>Leads & Accounts</td>"
                        reportBody += "<td>MTD vs same period last month</td>"
                        reportBody += "<td class='text-center' style='white-space:nowrap;'>##2##</td>"
                        reportBody += "</tr>"
                        reportBody += "<tr>"
                        reportBody += "<td>Gross Profit</td>"
                        reportBody += "<td>Sales Orders</td>"
                        reportBody += "<td>YTD vs same period last year</td>"
                        reportBody += "<td class='text-center' style='white-space:nowrap;'>##3##</td>"
                        reportBody += "</tr>"
                        reportBody += "<tr>"
                        reportBody += "<td>Gross Profit</td>"
                        reportBody += "<td>Sales Orders</td>"
                        reportBody += "<td>MTD vs same period last Month</td>"
                        reportBody += "<td class='text-center' style='white-space:nowrap;'>##4##</td>"
                        reportBody += "</tr>"
                        reportBody += "<tr>"
                        reportBody += "<td>Sales Opportunities Converted</td>"
                        reportBody += "<td>Sales Opportunities / Orders</td>"
                        reportBody += "<td>YTD vs same period last Year</td>"
                        reportBody += "<td class='text-center' style='white-space:nowrap;'>##5##</td>"
                        reportBody += "</tr>"
                        reportBody += "<tr>"
                        reportBody += "<td>Sales Opportunities Amt Converted</td>"
                        reportBody += "<td>Sales Opportunities / Orders</td>"
                        reportBody += "<td>YTD vs same period last Year</td>"
                        reportBody += "<td class='text-center' style='white-space:nowrap;'>##6##</td>"
                        reportBody += "</tr>"
                        reportBody += "<tr>"
                        reportBody += "<td>Average Project Gross Profit</td>"
                        reportBody += "<td>Projects & Sales Orders</td>"
                        reportBody += "<td>YTD vs same period last Year</td>"
                        reportBody += "<td class='text-center' style='white-space:nowrap;'>##7##</td>"
                        reportBody += "</tr>"
                        reportBody += "</table>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            Dim num1 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=1")(0)("numChange"))
                            Dim num2 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=2")(0)("numChange"))
                            Dim num3 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=3")(0)("numChange"))
                            Dim num4 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=4")(0)("numChange"))
                            Dim num5 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=5")(0)("numChange"))
                            Dim num6 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=6")(0)("numChange"))
                            Dim num7 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=7")(0)("numChange"))

                            reportBody = reportBody.Replace("##1##", If(num1 = 0, "", "<label style='color:" & If(num1 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num1) & "% " & If(num1 = 0, "", "<i class=""fa " & If(num1 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</label>")
                            reportBody = reportBody.Replace("##2##", If(num2 = 0, "", "<label style='color:" & If(num2 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num2) & "% " & If(num2 = 0, "", "<i class=""fa " & If(num2 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</label>")
                            reportBody = reportBody.Replace("##3##", If(num3 = 0, "", "<label style='color:" & If(num3 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num3) & "% " & If(num3 = 0, "", "<i class=""fa " & If(num3 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</label>")
                            reportBody = reportBody.Replace("##4##", If(num4 = 0, "", "<label style='color:" & If(num4 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num4) & "% " & If(num4 = 0, "", "<i class=""fa " & If(num4 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</label>")
                            reportBody = reportBody.Replace("##5##", If(num5 = 0, "", "<label style='color:" & If(num5 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num5) & "% " & If(num5 = 0, "", "<i class=""fa " & If(num5 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</label>")
                            reportBody = reportBody.Replace("##6##", If(num6 = 0, "", "<label style='color:" & If(num6 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num6) & "% " & If(num6 = 0, "", "<i class=""fa " & If(num6 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</label>")
                            reportBody = reportBody.Replace("##7##", If(num7 = 0, "", "<label style='color:" & If(num7 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num7) & "% " & If(num7 = 0, "", "<i class=""fa " & If(num7 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</label>")
                        Else
                            reportBody = reportBody.Replace("##1##", "")
                            reportBody = reportBody.Replace("##2##", "")
                            reportBody = reportBody.Replace("##3##", "")
                            reportBody = reportBody.Replace("##4##", "")
                            reportBody = reportBody.Replace("##5##", "")
                            reportBody = reportBody.Replace("##6##", "")
                            reportBody = reportBody.Replace("##7##", "")
                        End If

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10Email Then
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            Dim tempString As String = "<table style=width:100%>"

                            For Each dr As DataRow In ds.Tables(0).Rows
                                tempString += "<tr>"
                                tempString += "<td style=""color:#ffeb3b;padding-right:10px;""><i class=""fa fa-envelope"" aria-hidden=""true""></i></td>"
                                tempString += "<td style=""white-space:nowrap;padding-right: 3px;"">" & CCommon.ToString(dr("vcCompanyName")) & " | </td>"
                                tempString += "<td style=""white-space:nowrap;padding-right: 3px;"">" & CCommon.ToString(dr("vcFrom")) & " | </td>"
                                tempString += "<td style=""white-space:nowrap;padding-right: 3px;"">" & CCommon.ToString(dr("vcSubject")) & " | </td>"
                                tempString += "<td style=""width:100%"">" & CCommon.ToString(dr("vcBodyText")) & "</td>"
                                tempString += "</tr>"
                            Next

                            tempString += "</table>"
                            reportHtml = reportHtml.Replace("##REPORTBODY##", tempString)
                        Else
                            reportHtml = reportHtml.Replace("##REPORTBODY##", "")
                        End If
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Reminders Then
                        Dim reportBody As String = ""
                        Dim divStyle As String = "font-size:14px;"

                        If isForEmail Then
                            reportBody = "<ul style='list-style:none'>"
                            divStyle = divStyle & "display:inline;padding-left:10px;"
                        Else
                            reportBody = "<ul class='todo-list ui-sortable'>"
                        End If


                        If isForEmail Then
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Billable time & expense entries to review</span>"
                            reportBody += "<div  style='" & divStyle & "' class='label label-primary pull-right'>##VALUE1##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Minimum price margin violations to review</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE2##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Promotions in sales orders to approve</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE3##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Sales Opportunity Stages to complete</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE4##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Purchase Opportunity Stages to complete</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE18##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Sales Order Stages to complete</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE19##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Project Stages to complete</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE5##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Cases to solve</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE6##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Pending Sales RMAs to review</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE7##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Payments to deposit</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE8##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Bills to pay</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE9##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Purchase Orders to bill</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE10##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Sales Orders to pick & pack</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE11##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Sales Orders to ship</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE12##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Sales Orders to invoice</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE13##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Sales Orders with items to purchase</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE14##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Sales Orders past their release date</span>"
                            reportBody += "<div style='" & divStyle & "' class='label ##LABELCLASS15## pull-right'>##VALUE15##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Sales Commissions to pay</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE16##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'>Items to buy where qty-on-hand is less then equal to the reorder qty</span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE17##</div>"
                            reportBody += "</li>"
                        Else
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../common/frmTicklerdisplay.aspx?IsFormDashboard=1&ReminderType=1'>Billable time & expense entries to review</a></span>"
                            reportBody += "<div  style='" & divStyle & "' class='label label-primary pull-right'>##VALUE1##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../common/frmTicklerdisplay.aspx?IsFormDashboard=1&ReminderType=2'>Minimum price margin violations to review</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE2##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../common/frmTicklerdisplay.aspx?IsFormDashboard=1&ReminderType=3'>Promotions in sales orders to approve</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE3##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Opportunity/frmOpportunityList.aspx?IsFormDashboard=1&ReminderType=4&SI=0'>Sales Opportunity Stages to complete</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE4##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Opportunity/frmOpportunityList.aspx?IsFormDashboard=1&ReminderType=18&SI=1'>Purchase Opportunity Stages to complete</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE18##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Opportunity/frmDealList.aspx?IsFormDashboard=1&ReminderType=19&type=1'>Sales Order Stages to complete</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE19##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../projects/frmProjectList.aspx?IsFormDashboard=1&ReminderType=5'>Project Stages to complete</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE5##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../cases/frmCaseList.aspx?IsFormDashboard=1&ReminderType=6'>Cases to solve</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE6##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Opportunity/frmSalesReturns.aspx?IsFormDashboard=1&ReminderType=7&type=1'>Pending Sales RMAs to review</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE7##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Accounting/frmMakeDeposit.aspx'>Payments to deposit</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE8##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Accounting/frmVendorPayment.aspx'>Bills to pay</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE9##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Opportunity/frmDealList.aspx?IsFormDashboard=1&ReminderType=10&type=2'>Purchase Orders to bill</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE10##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Opportunity/frmDealList.aspx?IsFormDashboard=1&ReminderType=11&type=1'>Sales Orders to pick & pack</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE11##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Opportunity/frmDealList.aspx?IsFormDashboard=1&ReminderType=12&type=1'>Sales Orders to ship</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE12##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Opportunity/frmDealList.aspx?IsFormDashboard=1&ReminderType=13&type=1'>Sales Orders to invoice</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE13##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Opportunity/frmDealList.aspx?IsFormDashboard=1&ReminderType=14&type=1'>Sales Orders with items to purchase</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE14##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Opportunity/frmDealList.aspx?IsFormDashboard=1&ReminderType=15&type=1'>Sales Orders past their release date</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label ##LABELCLASS15## pull-right'>##VALUE15##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='#'>Sales Commissions to pay</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE16##</div>"
                            reportBody += "</li>"
                            reportBody += "<li style='padding:3px;'>"
                            reportBody += "<span class='text'><a href='../Items/frmItemList.aspx?Page=Inventory Items&ItemGroup=0&IsFormDashboard=1&ReminderType=17'>Items to buy where qty-on-hand is =< the reorder qty</a></span>"
                            reportBody += "<div style='" & divStyle & "' class='label label-primary pull-right'>##VALUE17##</div>"
                            reportBody += "</li>"
                        End If



                        reportBody += "</ul>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            reportBody = reportBody.Replace("##VALUE1##", CCommon.ToInteger(ds.Tables(0).Select("ID=1")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE2##", CCommon.ToInteger(ds.Tables(0).Select("ID=2")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE3##", CCommon.ToInteger(ds.Tables(0).Select("ID=3")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE4##", CCommon.ToInteger(ds.Tables(0).Select("ID=4")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE5##", CCommon.ToInteger(ds.Tables(0).Select("ID=5")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE6##", CCommon.ToInteger(ds.Tables(0).Select("ID=6")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE7##", CCommon.ToInteger(ds.Tables(0).Select("ID=7")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE8##", CCommon.ToInteger(ds.Tables(0).Select("ID=8")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE9##", CCommon.ToInteger(ds.Tables(0).Select("ID=9")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE10##", CCommon.ToInteger(ds.Tables(0).Select("ID=10")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE11##", CCommon.ToInteger(ds.Tables(0).Select("ID=11")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE12##", CCommon.ToInteger(ds.Tables(0).Select("ID=12")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE13##", CCommon.ToInteger(ds.Tables(0).Select("ID=13")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE14##", CCommon.ToInteger(ds.Tables(0).Select("ID=14")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE15##", CCommon.ToInteger(ds.Tables(0).Select("ID=15")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE16##", CCommon.ToInteger(ds.Tables(0).Select("ID=16")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE17##", CCommon.ToInteger(ds.Tables(0).Select("ID=17")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE18##", CCommon.ToInteger(ds.Tables(0).Select("ID=18")(0)("Value")))
                            reportBody = reportBody.Replace("##VALUE19##", CCommon.ToInteger(ds.Tables(0).Select("ID=19")(0)("Value")))

                            If CCommon.ToInteger(ds.Tables(0).Select("ID=15")(0)("Value")) > 0 Then
                                reportBody = reportBody.Replace("##LABELCLASS15##", "bg-red")
                            Else
                                reportBody = reportBody.Replace("##LABELCLASS15##", "bg-primary")
                            End If
                        Else
                            reportBody = reportBody.Replace("##VALUE1##", "0")
                            reportBody = reportBody.Replace("##VALUE2##", "0")
                            reportBody = reportBody.Replace("##VALUE3##", "0")
                            reportBody = reportBody.Replace("##VALUE4##", "0")
                            reportBody = reportBody.Replace("##VALUE5##", "0")
                            reportBody = reportBody.Replace("##VALUE6##", "0")
                            reportBody = reportBody.Replace("##VALUE7##", "0")
                            reportBody = reportBody.Replace("##VALUE8##", "0")
                            reportBody = reportBody.Replace("##VALUE9##", "0")
                            reportBody = reportBody.Replace("##VALUE10##", "0")
                            reportBody = reportBody.Replace("##VALUE11##", "0")
                            reportBody = reportBody.Replace("##VALUE12##", "0")
                            reportBody = reportBody.Replace("##VALUE13##", "0")
                            reportBody = reportBody.Replace("##VALUE14##", "0")
                            reportBody = reportBody.Replace("##VALUE15##", "0")
                            reportBody = reportBody.Replace("##VALUE16##", "0")
                            reportBody = reportBody.Replace("##VALUE17##", "0")
                            reportBody = reportBody.Replace("##VALUE18##", "0")
                            reportBody = reportBody.Replace("##VALUE19##", "0")
                            reportBody = reportBody.Replace("##LABELCLASS15##", "bg-primary")
                            reportBody = reportBody.Replace("##DISPLAY16##", "none")
                        End If

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopSourcesOfSalesOrders Then
                        Dim reportBody As String = "<div class='row' style='margin-right:0px;margin-left:0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px;padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/salesorder70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 5px;padding-right: 5px;'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            reportBody += "<table class='table table-bordered'>"
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                reportBody += "<label>" & CCommon.ToString(dr("vcSource")) & "</label>"
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("TotalOrdersPercent"))) & "%"
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("TotalOrdersAmount")))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                            reportBody += "</table>"
                        End If
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByRevenue Or _
                        DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByProgress Or _
                        DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOpportunityByPastDue Then
                        Dim reportBody As String = "<div class='row' style='margin-right:0px;margin-left:0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px;padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/salesopportunity70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 5px;padding-right: 5px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        reportBody += "<tr><th>Name</th><th>Amount</th><th>Progress</th></tr>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                If Not isForEmail Then
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & CCommon.ToString(dr("URL")).Replace("~/", "") & "'>" & CCommon.ToString(dr("vcPOppName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcPOppName"))
                                End If
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("monTotAmount")))
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("numPercentageComplete"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitAmountCompanyWide Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/organization70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                If Not isForEmail Then
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & CCommon.ToString(dr("URL")).Replace("~/", "") & "' target='_blank'>" & CCommon.ToString(dr("vcCompanyName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcCompanyName"))
                                End If
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("BlendedProfit"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitMarginCompanyWide Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/organization70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                If Not isForEmail Then
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & CCommon.ToString(dr("URL")).Replace("~/", "") & "' target='_blank'>" & CCommon.ToString(dr("vcCompanyName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcCompanyName"))
                                End If
                                reportBody += "</td>"
                                reportBody += "<tr style='text-align:right'>"
                                reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("Profit")))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByProfitMarginCompanyWide Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/item70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                If Not isForEmail Then
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & CCommon.ToString(dr("URL")).Replace("~/", "") & "' target='_blank'>" & CCommon.ToString(dr("vcItemName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcItemName"))
                                End If
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right;'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("BlendedProfit"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByReturnedVsSold Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/item70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                If Not isForEmail Then
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & CCommon.ToString(dr("URL")).Replace("~/", "") & "' target='_blank'>" & CCommon.ToString(dr("vcItemName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcItemName"))
                                End If
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("ReturnPercent"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10CampaignsByROI Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/campaign70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                If Not isForEmail Then
                                reportBody += "<a href='" & Convert.ToString(System.Configuration.ConfigurationManager.AppSettings("BizBaseURL")) & CCommon.ToString(dr("URL")).Replace("~/", "") & "' target='_blank'>" & CCommon.ToString(dr("vcCampaignName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcCampaignName"))
                                End If
                                reportBody += "</td>"
                                If CCommon.ToDouble(dr("ROI")) > 0 Then
                                    reportBody += "<td style='text-align:right;color:#00a65a'>"
                                Else
                                    reportBody += "<td style='text-align:right;color:#dd4b39'>"
                                End If

                                reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("ROI")))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.LeadSource Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/organization70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                reportBody += CCommon.ToString(dr("vcData"))
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("LeadSourcePercent"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopReasonsWinningDeals Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/salesopportunity70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                reportBody += CCommon.ToString(dr("Reason"))
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("TotalDealWonPercent"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopReasonsLossingDeals Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/salesopportunity70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                reportBody += CCommon.ToString(dr("Reason"))
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("TotalDealLostPercent"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ReasonsForSalesReturns Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/salesopportunity70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                reportBody += CCommon.ToString(dr("Reason"))
                                reportBody += "</td>"
                                reportBody += "<td style='text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("TotalReturnPercent"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.First10SavedSearches Then
                        Dim reportBody As String = "<div class='row' style='margin-right: 0px; margin-left: 0px; align-items: center;'>"
                        reportBody += "<div class='col-xs-2 text-center' style='padding-left: 0px; padding-right: 0px;'>"
                        reportBody += "<img class='img-responsive' src='../images/Icon/search70x70.png' />"
                        reportBody += "</div>"
                        reportBody += "<div class='col-xs-10' style='padding-left: 15px; padding-right: 15px;'>"
                        reportBody += "<table class='table  table-bordered'>"
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td>"
                                If Not isForEmail Then
                                    reportBody += "<a href='#' onclick='OpenSearchResult(" & CCommon.ToLong(dr("numSearchID")) & "," & CCommon.ToLong(dr("numFormID")) & ");'>" & CCommon.ToString(dr("vcSearchName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcSearchName"))
                                End If
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next
                        End If
                        reportBody += "</table>"
                        reportBody += "</div>"
                        reportBody += "</div>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformancePanel1 Then
                        Dim reportBody As String = "<table style='width:100%'><tr>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 3 AndAlso ds.Tables(3).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Gross Revenue</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(3).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                If Not isForEmail Then
                                    reportBody += "<a href='../Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&GroupBy=0" & "&SelectedID=" & CCommon.ToInteger(dr("numUserDetailId")) & "&From=" & CCommon.ToString(dr("StartDate")).Split(" ")(0) & "&To=" & (CCommon.ToString(dr("EndDate")).Split(" "))(0) & "&Export=True' target='_blank'>" & CCommon.ToString(dr("vcUserName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcUserName"))
                                End If
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                If Not isForEmail Then
                                    reportBody += "<a href='../Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&GroupBy=0" & "&SelectedID=" & CCommon.ToInteger(dr("numUserDetailId")) & "&From=" & (CCommon.ToString(dr("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(dr("EndDate")).Split(" "))(0) & "&Export=True' target='_blank'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("InvoiceTotal"))) & "</a>"
                                Else
                                    reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("InvoiceTotal")))
                                End If
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Deals won %</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                If Not isForEmail Then
                                    reportBody += "<a href='..//Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(dr("numUserDetailId")) & "&From=" & (CCommon.ToString(dr("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(dr("EndDate")).Split(" "))(0) & "&Export=True' target='_blank'>" & CCommon.ToString(dr("vcUserName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcUserName"))
                                End If
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                If Not isForEmail Then
                                    reportBody += "<a href='../Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(dr("numUserDetailId")) & "&From=" & (CCommon.ToString(dr("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(dr("EndDate")).Split(" "))(0) & "&Export=True' target='_blank'>" & String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("WonPercent"))) & "%" & "</a>"
                                Else
                                    reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("WonPercent"))) & "%"
                                End If
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Revenue from Gross Profits</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(1).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                If Not isForEmail Then
                                    reportBody += "<a href='../Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(dr("numUserDetailId")) & "&From=" & (CCommon.ToString(dr("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(dr("EndDate")).Split(" "))(0) & "&Export=True' target='_blank'>" & CCommon.ToString(dr("vcUserName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcUserName"))
                                End If
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                If Not isForEmail Then
                                    reportBody += "<a href='../Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(dr("numUserDetailId")) & "&From=" & (CCommon.ToString(dr("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(dr("EndDate")).Split(" "))(0) & "&Export=True' target='_blank'>" & String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("Profit"))) & "</a>"
                                Else
                                    reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("Profit")))
                                End If
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Gross Profit Margin Avg.</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(2).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                If Not isForEmail Then
                                    reportBody += "<a href='../Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(dr("numUserDetailId")) & "&From=" & (CCommon.ToString(dr("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(dr("EndDate")).Split(" "))(0) & "&Export=True' target='_blank'>" & CCommon.ToString(dr("vcUserName")) & "</a>"
                                Else
                                    reportBody += CCommon.ToString(dr("vcUserName"))
                                End If
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                If Not isForEmail Then
                                    reportBody += "<a href='../Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(dr("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(dr("numUserDetailId")) & "&From=" & (CCommon.ToString(dr("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(dr("EndDate")).Split(" "))(0) & "&Export=True' target='_blank'>" & String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("AvgGrossProfitMargin"))) & "%" & "</a>"
                                Else
                                    reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("AvgGrossProfitMargin"))) & "%"
                                End If
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        reportBody += "</tr></table>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.PartnerRMPLast12Months Then
                        Dim reportBody As String = "<table style='width:100%'><tr>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Revenue by Partner</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                reportBody += "<label>" & CCommon.ToString(dr("vcCompanyName")) & "</label>"
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("monDealAmount")))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Profit Margin by Partner</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(1).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                reportBody += "<label>" & CCommon.ToString(dr("vcCompanyName")) & "</label>"
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("BlendedProfit"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Profit by Partner</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(2).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                reportBody += "<label>" & CCommon.ToString(dr("vcCompanyName")) & "</label>"
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("Profit")))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        reportBody += "</tr></table>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformance1 Then
                        Dim reportBody As String = "<table style='width:100%'><tr>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Lead to Prospects</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                reportBody += "<label>" & CCommon.ToString(dr("vcUsername")) & "</label>"
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("LeadsToProspect"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Lead to Accounts</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(1).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                reportBody += "<label>" & CCommon.ToString(dr("vcUsername")) & "</label>"
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("LeadsToAccount"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Prospects to Accounts</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(2).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                reportBody += "<label>" & CCommon.ToString(dr("vcUsername")) & "</label>"
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("ProspectToAccount"))) & "%"
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        reportBody += "</tr></table>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeBenefittoCompany Then
                        Dim reportBody As String = "<table style='width:100%'><tr>"

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Hours worked</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(0).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                reportBody += "<label>" & CCommon.ToString(dr("vcUsername")) & "</label>"
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                reportBody += String.Format("{0:#,##0.##}", CCommon.ToDouble(dr("TotalHrsWorked")))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Gross Payroll</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(1).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                reportBody += "<label>" & CCommon.ToString(dr("vcUsername")) & "</label>"
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("TotalPayroll")))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                            reportBody += "<td style='vertical-align:top;padding-right: 5px;'>"
                            reportBody += "<table style='width:100%'>"
                            reportBody += "<tr>"
                            reportBody += "<td style='text-align:center'>"
                            reportBody += "<label style='white-space:nowrap'>Profit Income minus Payroll</label></td>"
                            reportBody += "</tr>"
                            reportBody += "<tr>"
                            reportBody += "<td>"
                            reportBody += "<table style='width:100%;background-color: #f5f5f5;border: 1px solid #ddd;'>"

                            For Each dr As DataRow In ds.Tables(2).Rows
                                reportBody += "<tr>"
                                reportBody += "<td style='width:100%; padding:5px;'>"
                                reportBody += "<label>" & CCommon.ToString(dr("vcUsername")) & "</label>"
                                reportBody += "</td>"
                                reportBody += "<td style='white-space:nowrap; padding:5px; text-align:right'>"
                                reportBody += String.Format("{0:$#,##0.##}", CCommon.ToDouble(dr("ProfitMinusPayroll")))
                                reportBody += "</td>"
                                reportBody += "</tr>"
                            Next

                            reportBody += "</table>"
                            reportBody += "</td>"
                            reportBody += "</tr>"
                            reportBody += "</table>"
                            reportBody += "</td>"
                        End If

                        reportBody += "</tr></table>"

                        reportHtml = reportHtml.Replace("##REPORTBODY##", reportBody)
                End If

                reportHtml = reportHtml.Replace("##REPORTHEADER##", "")
                reportHtml = reportHtml.Replace("##EXPORTLINK##", "")
                reportHtml = reportHtml.Replace("##REPORTBODY##", "")
                reportHtml = reportHtml.Replace("##REPORTFOOTER##", "")
            Catch ex As Exception
                reportHtml = reportHtml.Replace("##REPORTHEADER##", "")
                reportHtml = reportHtml.Replace("##EXPORTLINK##", "")
                reportHtml = reportHtml.Replace("##REPORTBODY##", "<label class='text-red'>Error ocurred while rendering this report</label>")
                reportHtml = reportHtml.Replace("##REPORTFOOTER##", "")
            End Try

            Return reportHtml
        End Function

        Public Function GetCustomReportHtml(ByVal ds As DataSet, ByVal dsColumnList As DataSet, ByVal dtData As DataTable, ByVal objReport As ReportObject, ByVal isForEmail As Boolean) As String
            Try
                Dim html As String = ""
                Dim trHeader As String = ""
                Dim trRow As String = ""
                Dim cell As String = ""


                If ds.Tables.Count > 0 Then
                    Dim spaceCharacter As String = ""

                    If ds.Tables(0).Rows(0)("tintReportType") = 0 Then 'Tabular

                        trHeader = "<tr>"

                        For Each str As String In objReport.ColumnList
                            Dim strColumn As String() = str.Split("_")

                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                trHeader += ("<th>" & CCommon.ToString(dr(0)("vcFieldname")) & "</th>")
                            End If
                        Next

                        trHeader += "</tr>"
                        html += trHeader

                        Dim vcFieldDataType As String = ""
                        Dim vcOrigDbColumnName As String = ""
                        For i As Integer = 0 To dtData.Rows.Count - 1
                            trRow = "<tr>"

                            For Each str As String In objReport.ColumnList
                                vcFieldDataType = ""
                                vcOrigDbColumnName = ""

                                Dim strColumn As String() = str.Split("_")
                                Dim drColumn() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                                If Not drColumn Is Nothing AndAlso drColumn.Length > 0 Then
                                    vcFieldDataType = CCommon.ToString(drColumn(0)("vcFieldDataType"))
                                    vcOrigDbColumnName = CCommon.ToString(drColumn(0)("vcOrigDbColumnName"))
                                End If

                                cell = "<td>"
                                If IsNumeric(dtData.Rows(i)(str)) AndAlso (vcFieldDataType = "M" Or vcFieldDataType = "N") Then
                                    If vcFieldDataType = "M" Then
                                        cell += CCommon.GetDecimalFormat(CCommon.ToDouble(dtData.Rows(i)(str)))
                                    Else
                                        cell += String.Format("{0:#,##0.####################}", CCommon.ToDouble(dtData.Rows(i)(str)))
                                    End If
                                Else

                                    If isForEmail Then
                                        If vcOrigDbColumnName = "vcPoppName" AndAlso dtData.Columns.Contains("OpID") Then
                                            cell += CCommon.ToString(dtData.Rows(i)(str))
                                        ElseIf vcOrigDbColumnName = "vcCompanyName" AndAlso dtData.Columns.Contains("DivID") Then
                                            cell += CCommon.ToString(dtData.Rows(i)(str))
                                        ElseIf vcOrigDbColumnName = "vcItemName" AndAlso dtData.Columns.Contains("ItemCode") Then
                                            cell += CCommon.ToString(dtData.Rows(i)(str))
                                        ElseIf vcOrigDbColumnName = "vcBizDocID" AndAlso dtData.Columns.Contains("OpID") AndAlso dtData.Columns.Contains("BizDocId") Then
                                            cell += CCommon.ToString(dtData.Rows(i)(str))
                                        ElseIf vcOrigDbColumnName = "vcCaseNumber" AndAlso dtData.Columns.Contains("CaseID") Then
                                            cell += CCommon.ToString(dtData.Rows(i)(str))
                                        Else
                                            cell += CCommon.ToString(dtData.Rows(i)(str))
                                        End If
                                    Else
                                        If vcOrigDbColumnName = "vcPoppName" AndAlso dtData.Columns.Contains("OpID") Then
                                            cell += "<a href='../opportunity/frmOpportunities.aspx?opId=" & CCommon.ToString(dtData.Rows(i)("OpID")) & "' target='_blank'>" & CCommon.ToString(dtData.Rows(i)(str)) & "</a>"
                                        ElseIf vcOrigDbColumnName = "vcCompanyName" AndAlso dtData.Columns.Contains("DivID") Then
                                            cell += "<a href='../account/frmAccounts.aspx?DivID=" & CCommon.ToString(dtData.Rows(i)("DivID")) & "' target='_blank'>" & CCommon.ToString(dtData.Rows(i)(str)) & "</a>"
                                        ElseIf vcOrigDbColumnName = "vcItemName" AndAlso dtData.Columns.Contains("ItemCode") Then
                                            cell += "<a href='../Items/frmKitDetails.aspx?ItemCode=" & CCommon.ToString(dtData.Rows(i)("ItemCode")) & "&frm=All Items' target='_blank'>" & CCommon.ToString(dtData.Rows(i)(str)) & "</a>"
                                        ElseIf vcOrigDbColumnName = "vcBizDocID" AndAlso dtData.Columns.Contains("OpID") AndAlso dtData.Columns.Contains("BizDocId") Then
                                            cell += "<a href='../opportunity/frmBizInvoice.aspx?opId=" & CCommon.ToString(dtData.Rows(i)("OpID")) & "&OppBizId=" & CCommon.ToString(dtData.Rows(i)("BizDocId")) & "' target='_blank'>" & CCommon.ToString(dtData.Rows(i)(str)) & "</a>"
                                        ElseIf vcOrigDbColumnName = "vcCaseNumber" AndAlso dtData.Columns.Contains("CaseID") Then
                                            cell += "<a href='../cases/frmCases.aspx?frmCaselist&CaseID=" & CCommon.ToString(dtData.Rows(i)("CaseID")) & "' target='_blank'>" & CCommon.ToString(dtData.Rows(i)(str)) & "</a>"
                                        Else
                                            cell += CCommon.ToString(dtData.Rows(i)(str))
                                        End If
                                    End If
                                    
                                End If

                                cell += "</td>"
                                trRow += cell
                            Next

                            trRow += "</tr>"
                            html += trRow
                        Next
                    ElseIf ds.Tables(0).Rows(0)("tintReportType") = 1 Then 'Summary
                        trHeader = "<tr>"
                        Dim bitHideSummaryDetail As Boolean = CCommon.ToBool(ds.Tables(0).Rows(0)("bitHideSummaryDetail"))

                        If objReport.GroupColumn.Count > 0 Then
                            For Each groupColumn As String In objReport.GroupColumn
                                Dim strColumn As String() = groupColumn.Split("_")

                                Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " And numFieldID=" & strColumn(1) & " And bitCustom=" & strColumn(2))

                                If dr.Length > 0 Then
                                    trHeader += ("<th>" & CCommon.ToString(dr(0)("vcFieldname")) & "</th>")
                                End If
                            Next
                        End If

                        For Each str As String In objReport.ColumnList
                            Dim strColumn As String() = str.Split("_")

                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " And numFieldID=" & strColumn(1) & " And bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                trHeader += ("<th>" & CCommon.ToString(dr(0)("vcFieldname")) & "</th>")
                            End If
                        Next
                        trHeader += "</tr>"
                        html += trHeader

                        If objReport.GroupColumn.Count > 0 Then

                            Dim query As IEnumerable(Of ReportQueryResult) = From row In dtData.AsEnumerable()
                                        Group row By GroupColumns = New With {Key .Key1 = row.Field(Of Object)(objReport.GroupColumn(0))} Into GroupDetail = Group
                                        Select New ReportQueryResult() With {
                                            .Key1 = GroupColumns.Key1,
                                            .CountTotal = GroupDetail.Count(),
                                            .Detail = GroupDetail
                                       }

                            If objReport.SortColumn.Length > 0 Then
                                Dim AggField As IEnumerable(Of AggregateObject) = From row In objReport.Aggregate Where row.Column = objReport.SortColumn
                                If AggField.Count > 0 Then
                                    If objReport.SortDirection = "desc" Then
                                        query = From row In query.AsEnumerable() Order By (row.Detail.Sum(Function(r) r.Field(Of Decimal)(objReport.SortColumn))) Descending
                                    Else
                                        query = From row In query.AsEnumerable() Order By (row.Detail.Sum(Function(r) r.Field(Of Decimal)(objReport.SortColumn))) Ascending
                                    End If
                                End If
                            End If

                            If objReport.NoRows > 0 Then
                                query = query.Take(objReport.NoRows)
                            End If

                            For Each x As ReportQueryResult In query
                                If bitHideSummaryDetail Then
                                    trRow = "<tr>"
                                    cell = ("<td colspan='" & objReport.GroupColumn.Count & "'>" & String.Format("{0}&nbsp;(Total Records:{1})", x.Key1, x.CountTotal) & "</td>")
                                    trRow += cell
                                    'trRow += "</tr>"
                                    'html += trRow
                                Else
                                    trRow = "<tr>"
                                    cell = "<td colspan='" & objReport.GroupColumn.Count & "' class='Group0'>" & String.Format("<strong>{0}&nbsp;&nbsp;&nbsp;(Total Records:{1})</strong>", x.Key1, x.CountTotal) & "</td>"
                                    trRow += cell
                                    'trRow += "</tr>"
                                    'html += trRow
                                End If

                                If objReport.Aggregate.Count > 0 Then
                                    Dim i As Integer = 0
                                    For Each str As String In objReport.ColumnList
                                        cell = "<td class='tdAggregate " & IIf(bitHideSummaryDetail, "", "Group0") & "'>"
                                        Dim strCellSummary As String = ""

                                        Dim AggField As IEnumerable(Of AggregateObject) = From row In objReport.Aggregate Where row.Column = str
                                        If AggField.Count > 0 Then
                                            Dim total As Integer = x.CountTotal

                                            For Each str1 As AggregateObject In AggField

                                                Select Case str1.aggType
                                                    Case "sum"
                                                        If total > 0 Then
                                                            strCellSummary += String.Format("{0:#,###.#####}", x.Detail.Sum(Function(r) r(str1.Column))) & "<br/>"
                                                        End If

                                                    Case "avg"
                                                        If total > 0 Then
                                                            strCellSummary += String.Format("Avg {0:#,###.#####}", x.Detail.Average(Function(r) r(str1.Column))) & "<br/>"
                                                        End If

                                                    Case "max"
                                                        If total > 0 Then
                                                            strCellSummary += String.Format("Largest {0:#,###.#####}", x.Detail.Max(Function(r) r(str1.Column))) & "<br/>"
                                                        End If

                                                    Case "min"
                                                        If total > 0 Then
                                                            strCellSummary += String.Format("Smallest {0:#,###.#####}", x.Detail.Min(Function(r) r(str1.Column))) & "<br/>"
                                                        End If

                                                End Select
                                            Next
                                        End If

                                        cell += strCellSummary
                                        cell += "</td>"
                                        trRow += cell
                                    Next
                                End If

                                trRow += "</tr>"
                                html += trRow

                                If objReport.GroupColumn.Count > 1 AndAlso bitHideSummaryDetail = False Then
                                    Dim query1 As IEnumerable(Of ReportQueryResult) = From row In x.Detail.AsEnumerable()
                                                 Group row By GroupColumns = New With {Key .Key2 = row.Field(Of Object)(objReport.GroupColumn(1))} Into GroupDetail = Group
                                                 Select New ReportQueryResult() With {
                           .Key2 = GroupColumns.Key2,
                           .CountTotal = GroupDetail.Count(),
                           .Detail = GroupDetail
                                                              }

                                    For Each x1 As ReportQueryResult In query1
                                        trRow = "<tr>"
                                        cell = "<td>" & x.Key1 & "</td>"
                                        cell += "<td class='Group1' colspan='" & objReport.GroupColumn.Count - 1 & "'>" & String.Format("<strong>{0}&nbsp;&nbsp;&nbsp;(Total Records:{1})</strong>", x1.Key2, x1.CountTotal) & "</td>"
                                        trRow += cell

                                        If objReport.Aggregate.Count > 0 Then
                                            Dim i As Integer = 0
                                            For Each str As String In objReport.ColumnList
                                                cell = "<td class='tdAggregate " & IIf(bitHideSummaryDetail, "", "Group0") & "'>"
                                                Dim strCellSummary As String = ""

                                                Dim AggField As IEnumerable(Of AggregateObject) = From row In objReport.Aggregate Where row.Column = str
                                                If AggField.Count > 0 Then
                                                    Dim total As Integer = x1.CountTotal

                                                    For Each str1 As AggregateObject In AggField

                                                        Select Case str1.aggType
                                                            Case "sum"
                                                                If total > 0 Then
                                                                    strCellSummary += String.Format("{0:#,###.#####}", x1.Detail.Sum(Function(r) r(str1.Column))) & "<br/>"
                                                                End If

                                                            Case "avg"
                                                                If total > 0 Then
                                                                    strCellSummary += String.Format("Avg {0:#,###.#####}", x1.Detail.Average(Function(r) r(str1.Column))) & "<br/>"
                                                                End If

                                                            Case "max"
                                                                If total > 0 Then
                                                                    strCellSummary += String.Format("Largest {0:#,###.#####}", x1.Detail.Max(Function(r) r(str1.Column))) & "<br/>"
                                                                End If

                                                            Case "min"
                                                                If total > 0 Then
                                                                    strCellSummary += String.Format("Smallest {0:#,###.#####}", x1.Detail.Min(Function(r) r(str1.Column))) & "<br/>"
                                                                End If

                                                        End Select
                                                    Next
                                                End If

                                                cell += strCellSummary
                                                cell += "</td>"
                                                trRow += cell
                                            Next
                                        End If

                                        trRow += "</tr>"
                                        html += trRow

                                        If objReport.GroupColumn.Count > 2 Then
                                            Dim query2 As IEnumerable(Of ReportQueryResult) = From row In x1.Detail.AsEnumerable()
                                                         Group row By GroupColumns = New With {Key .Key3 = row.Field(Of Object)(objReport.GroupColumn(2))} Into GroupDetail = Group
                                                         Select New ReportQueryResult() With {
                        .Key3 = GroupColumns.Key3,
                        .CountTotal = GroupDetail.Count(),
                        .Detail = GroupDetail
                                                                      }

                                            Dim vcFieldDataType As String = ""

                                            For Each x2 As ReportQueryResult In query2
                                                trRow = "<tr>"
                                                cell = "<td>" & x.Key1 & "</td><td>" & x1.Key2 & "</td>"
                                                cell += "<td class='Group2' colspan='" & objReport.ColumnList.Count + objReport.GroupColumn.Count - 2 & "'>" & String.Format("<strong>{0}&nbsp;&nbsp;&nbsp;(Total Records:{1})</strong>", x2.Key3, x2.CountTotal) & "</td>"
                                                trRow += cell

                                                If objReport.Aggregate.Count > 0 Then
                                                    Dim i As Integer = 0
                                                    For Each str As String In objReport.ColumnList
                                                        cell = "<td class='tdAggregate " & IIf(bitHideSummaryDetail, "", "Group0") & "'>"
                                                        Dim strCellSummary As String = ""

                                                        Dim AggField As IEnumerable(Of AggregateObject) = From row In objReport.Aggregate Where row.Column = str
                                                        If AggField.Count > 0 Then
                                                            Dim total As Integer = x2.CountTotal

                                                            For Each str1 As AggregateObject In AggField

                                                                Select Case str1.aggType
                                                                    Case "sum"
                                                                        If total > 0 Then
                                                                            strCellSummary += String.Format("{0:#,###.#####}", x2.Detail.Sum(Function(r) r(str1.Column))) & "<br/>"
                                                                        End If

                                                                    Case "avg"
                                                                        If total > 0 Then
                                                                            strCellSummary += String.Format("Avg {0:#,###.#####}", x2.Detail.Average(Function(r) r(str1.Column))) & "<br/>"
                                                                        End If

                                                                    Case "max"
                                                                        If total > 0 Then
                                                                            strCellSummary += String.Format("Largest {0:#,###.#####}", x2.Detail.Max(Function(r) r(str1.Column))) & "<br/>"
                                                                        End If

                                                                    Case "min"
                                                                        If total > 0 Then
                                                                            strCellSummary += String.Format("Smallest {0:#,###.#####}", x2.Detail.Min(Function(r) r(str1.Column))) & "<br/>"
                                                                        End If

                                                                End Select
                                                            Next
                                                        End If

                                                        cell += strCellSummary
                                                        cell += "</td>"
                                                        trRow += cell
                                                    Next
                                                End If

                                                trRow += "</tr>"
                                                html += trRow

                                                For Each dr1 As DataRow In x2.Detail
                                                    trRow = "<tr>"

                                                    If objReport.GroupColumn.Count > 0 Then
                                                        For Each groupColumn As String In objReport.GroupColumn
                                                            Dim strColumn As String() = groupColumn.Split("_")

                                                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                                                            If dr.Length > 0 Then
                                                                cell = "<td>"
                                                                If IsNumeric(dr1(groupColumn)) AndAlso (vcFieldDataType = "M" Or vcFieldDataType = "N") Then
                                                                    If vcFieldDataType = "M" Then
                                                                        cell += CCommon.GetDecimalFormat(CCommon.ToDouble(dr1(groupColumn)))
                                                                    Else
                                                                        cell += String.Format("{0:#,##0.####################}", CCommon.ToDouble(dr1(groupColumn)))
                                                                    End If
                                                                Else
                                                                    cell += CCommon.ToString(dr1(groupColumn))
                                                                End If

                                                                cell += "</td>"
                                                                trRow += cell
                                                            End If
                                                        Next
                                                    End If

                                                    For Each str As String In objReport.ColumnList
                                                        vcFieldDataType = ""

                                                        Dim strColumnTemp As String() = str.Split("_")
                                                        Dim drColumn() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumnTemp(0) & " AND numFieldID=" & strColumnTemp(1) & " AND bitCustom=" & strColumnTemp(2))

                                                        If Not drColumn Is Nothing AndAlso drColumn.Length > 0 Then
                                                            vcFieldDataType = CCommon.ToString(drColumn(0)("vcFieldDataType"))
                                                        End If

                                                        cell = "<td>"
                                                        If IsNumeric(dr1(str)) AndAlso (vcFieldDataType = "M" Or vcFieldDataType = "N") Then
                                                            If vcFieldDataType = "M" Then
                                                                cell += CCommon.GetDecimalFormat(CCommon.ToDouble(dr1(str)))
                                                            Else
                                                                cell += String.Format("{0:#,##0.####################}", CCommon.ToDouble(dr1(str)))
                                                            End If
                                                        Else
                                                            cell += CCommon.ToString(dr1(str))
                                                        End If

                                                        cell += "</td>"
                                                        trRow += cell
                                                    Next

                                                    trRow += "</tr>"
                                                    html += trRow
                                                Next
                                            Next
                                        Else
                                            Dim vcFieldDataType As String = ""

                                            For Each dr1 As DataRow In x1.Detail
                                                trRow = "<tr>"

                                                If objReport.GroupColumn.Count > 0 Then

                                                    For Each groupColumn As String In objReport.GroupColumn
                                                        Dim strColumn As String() = groupColumn.Split("_")

                                                        Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                                                        If dr.Length > 0 Then
                                                            cell = "<td>"
                                                            If IsNumeric(dr1(groupColumn)) AndAlso (vcFieldDataType = "M" Or vcFieldDataType = "N") Then
                                                                If vcFieldDataType = "M" Then
                                                                    cell += CCommon.GetDecimalFormat(CCommon.ToDouble(dr1(groupColumn)))
                                                                Else
                                                                    cell += String.Format("{0:#,##0.####################}", CCommon.ToDouble(dr1(groupColumn)))
                                                                End If
                                                            Else
                                                                cell += CCommon.ToString(dr1(groupColumn))
                                                            End If
                                                            cell += "</td>"
                                                            trRow += cell
                                                        End If
                                                    Next

                                                End If

                                                For Each str As String In objReport.ColumnList
                                                    vcFieldDataType = ""

                                                    Dim strColumnTemp As String() = str.Split("_")
                                                    Dim drColumn() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumnTemp(0) & " AND numFieldID=" & strColumnTemp(1) & " AND bitCustom=" & strColumnTemp(2))

                                                    If Not drColumn Is Nothing AndAlso drColumn.Length > 0 Then
                                                        vcFieldDataType = CCommon.ToString(drColumn(0)("vcFieldDataType"))
                                                    End If

                                                    cell = "<td>"
                                                    If IsNumeric(dr1(str)) AndAlso (vcFieldDataType = "M" Or vcFieldDataType = "N") Then
                                                        If vcFieldDataType = "M" Then
                                                            cell += CCommon.GetDecimalFormat(CCommon.ToDouble(dr1(str)))
                                                        Else
                                                            cell += String.Format("{0:#,##0.####################}", CCommon.ToDouble(dr1(str)))
                                                        End If
                                                    Else
                                                        cell += CCommon.ToString(dr1(str))
                                                    End If

                                                    cell += "</td>"
                                                    trRow += cell
                                                Next

                                                trRow += "</tr>"
                                                html += trRow
                                            Next
                                        End If
                                    Next
                                ElseIf bitHideSummaryDetail = False Then
                                    Dim vcFieldDataType As String = ""
                                    Dim vcOrigDbColumnName As String = ""
                                    For Each dr1 As DataRow In x.Detail
                                        trRow = "<tr>"

                                        If objReport.GroupColumn.Count > 0 Then
                                            For Each groupColumn As String In objReport.GroupColumn
                                                Dim strColumn As String() = groupColumn.Split("_")

                                                Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                                                If dr.Length > 0 Then
                                                    cell = "<td>"
                                                    vcOrigDbColumnName = CCommon.ToString(dr(0)("vcOrigDbColumnName"))
                                                    If IsNumeric(dr1(groupColumn)) AndAlso (vcFieldDataType = "M" Or vcFieldDataType = "N") Then
                                                        If vcFieldDataType = "M" Then
                                                            cell += CCommon.GetDecimalFormat(CCommon.ToDouble(dr1(groupColumn)))
                                                        Else
                                                            cell += String.Format("{0:#,##0.####################}", CCommon.ToDouble(dr1(groupColumn)))
                                                        End If
                                                    Else

                                                        If isForEmail Then
                                                            If vcOrigDbColumnName = "vcPoppName" Then
                                                                cell += CCommon.ToString(dr1(groupColumn))
                                                            ElseIf vcOrigDbColumnName = "vcCompanyName" Then
                                                                cell += CCommon.ToString(dr1(groupColumn))
                                                            ElseIf vcOrigDbColumnName = "vcItemName" Then
                                                                cell += CCommon.ToString(dr1(groupColumn))
                                                            ElseIf vcOrigDbColumnName = "vcBizDocID" Then
                                                                cell += CCommon.ToString(dr1(groupColumn))
                                                            ElseIf vcOrigDbColumnName = "vcCaseNumber" Then
                                                                cell += CCommon.ToString(dr1(groupColumn))
                                                            Else
                                                                cell += CCommon.ToString(dr1(groupColumn))
                                                            End If
                                                        Else
                                                            If vcOrigDbColumnName = "vcPoppName" Then
                                                                cell += "<a href='../opportunity/frmOpportunities.aspx?opId=" & CCommon.ToString(dr1("OpID")) & "' target='_blank'>" & CCommon.ToString(dr1(groupColumn)) & "</a>"
                                                            ElseIf vcOrigDbColumnName = "vcCompanyName" Then
                                                                cell += "<a href='../account/frmAccounts.aspx?DivID=" & CCommon.ToString(dr1("DivID")) & "' target='_blank'>" & CCommon.ToString(dr1(groupColumn)) & "</a>"
                                                            ElseIf vcOrigDbColumnName = "vcItemName" Then
                                                                cell += "<a href='../Items/frmKitDetails.aspx?ItemCode=" & CCommon.ToString(dr1("ItemCode")) & "&frm=All Items' target='_blank'>" & CCommon.ToString(dr1(groupColumn)) & "</a>"
                                                            ElseIf vcOrigDbColumnName = "vcBizDocID" Then
                                                                cell += "<a href='../opportunity/frmBizInvoice.aspx?opId=" & CCommon.ToString(dr1("OpID")) & "&OppBizId=" & CCommon.ToString(dr1("BizDocId")) & "' target='_blank'>" & CCommon.ToString(dr1(groupColumn)) & "</a>"
                                                            ElseIf vcOrigDbColumnName = "vcCaseNumber" Then
                                                                cell += "<a href='../cases/frmCases.aspx?frmCaselist&CaseID=" + CCommon.ToString(dr1("CaseID")) & "' target='_blank'>" & CCommon.ToString(dr1(groupColumn)) & "</a>"
                                                            Else
                                                                cell += CCommon.ToString(dr1(groupColumn))
                                                            End If
                                                        End If
                                                    End If
                                                    cell += "</td>"
                                                    trRow += cell
                                                End If
                                            Next
                                        End If

                                        For Each str As String In objReport.ColumnList
                                            vcFieldDataType = ""
                                            vcOrigDbColumnName = ""
                                            Dim strColumnTemp As String() = str.Split("_")
                                            Dim drColumn() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumnTemp(0) & " AND numFieldID=" & strColumnTemp(1) & " AND bitCustom=" & strColumnTemp(2))

                                            If Not drColumn Is Nothing AndAlso drColumn.Length > 0 Then
                                                vcFieldDataType = CCommon.ToString(drColumn(0)("vcFieldDataType"))
                                                vcOrigDbColumnName = CCommon.ToString(drColumn(0)("vcOrigDbColumnName"))
                                            End If


                                            cell = "<td>"
                                            If IsNumeric(dr1(str)) AndAlso (vcFieldDataType = "M" Or vcFieldDataType = "N") Then
                                                If vcFieldDataType = "M" Then
                                                    cell += CCommon.GetDecimalFormat(CCommon.ToDouble(dr1(str)))
                                                Else
                                                    cell += String.Format("{0:#,##0.####################}", CCommon.ToDouble(dr1(str)))
                                                End If
                                            Else
                                                If isForEmail Then
                                                    If vcOrigDbColumnName = "vcPoppName" Then
                                                        cell += CCommon.ToString(dr1("OpID")) & "' target='_blank'>" & CCommon.ToString(dr1(str))
                                                    ElseIf vcOrigDbColumnName = "vcCompanyName" Then
                                                        cell += CCommon.ToString(dr1(str))
                                                    ElseIf vcOrigDbColumnName = "vcItemName" Then
                                                        cell += CCommon.ToString(dr1(str))
                                                    ElseIf vcOrigDbColumnName = "vcBizDocID" Then
                                                        cell += CCommon.ToString(dr1(str))
                                                    ElseIf vcOrigDbColumnName = "vcCaseNumber" Then
                                                        cell += CCommon.ToString(dr1(str))
                                                    Else
                                                        cell += CCommon.ToString(dr1(str))
                                                    End If
                                                Else
                                                    If vcOrigDbColumnName = "vcPoppName" Then
                                                        cell += "<a href='../opportunity/frmOpportunities.aspx?opId=" & CCommon.ToString(dr1("OpID")) & "' target='_blank'>" & CCommon.ToString(dr1(str)) & "</a>"
                                                    ElseIf vcOrigDbColumnName = "vcCompanyName" Then
                                                        cell += "<a href='../account/frmAccounts.aspx?DivID=" & CCommon.ToString(dr1("DivID")) & "' target='_blank'>" & CCommon.ToString(dr1(str)) & "</a>"
                                                    ElseIf vcOrigDbColumnName = "vcItemName" Then
                                                        cell += "<a href='../Items/frmKitDetails.aspx?ItemCode=" & CCommon.ToString(dr1("ItemCode")) + "&frm=All Items' target='_blank'>" & CCommon.ToString(dr1(str)) & "</a>"
                                                    ElseIf vcOrigDbColumnName = "vcBizDocID" Then
                                                        cell += "<a href='../opportunity/frmBizInvoice.aspx?opId=" & CCommon.ToString(dr1("OpID")) & "&OppBizId=" & CCommon.ToString(dr1("BizDocId")) & "' target='_blank'>" & CCommon.ToString(dr1(str)) & "</a>"
                                                    ElseIf vcOrigDbColumnName = "vcCaseNumber" Then
                                                        cell += "<a href='../cases/frmCases.aspx?frmCaselist&CaseID=" + CCommon.ToString(dr1("CaseID")) & "' target='_blank'>" & CCommon.ToString(dr1(str)) & "</a>"
                                                    Else
                                                        cell += CCommon.ToString(dr1(str))
                                                    End If
                                                End If
                                            End If

                                            cell += "</td>"
                                            trRow += cell
                                        Next
                                        trRow += "</tr>"
                                        html += trRow
                                    Next
                                End If
                            Next
                        Else
                            Dim vcFieldDataType As String = ""
                            Dim vcOrigDbColumnName As String = ""
                            For i As Integer = 0 To dtData.Rows.Count - 1
                                trRow = "<tr>"

                                For Each str As String In objReport.ColumnList
                                    vcFieldDataType = ""
                                    vcOrigDbColumnName = ""
                                    Dim strColumn As String() = str.Split("_")
                                    Dim drColumn() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                                    If Not drColumn Is Nothing AndAlso drColumn.Length > 0 Then
                                        vcFieldDataType = CCommon.ToString(drColumn(0)("vcFieldDataType"))
                                        vcOrigDbColumnName = CCommon.ToString(drColumn(0)("vcOrigDbColumnName"))
                                    End If

                                    cell = "<td>"
                                    If IsNumeric(dtData.Rows(i)(str)) AndAlso (vcFieldDataType = "M" Or vcFieldDataType = "N") Then
                                        If vcFieldDataType = "M" Then
                                            cell += CCommon.GetDecimalFormat(CCommon.ToDouble(dtData.Rows(i)(str)))
                                        Else
                                            cell += String.Format("{0:#,##0.####################}", CCommon.ToDouble(dtData.Rows(i)(str)))
                                        End If
                                    Else
                                        If isForEmail Then
                                            cell += CCommon.ToString(dtData.Rows(i)(str))
                                        Else
                                            If vcOrigDbColumnName = "vcPoppName" AndAlso dtData.Columns.Contains("OpID") Then
                                                cell += "<a href='../opportunity/frmOpportunities.aspx?opId=" & CCommon.ToString(dtData.Rows(i)("OpID")) & "' target='_blank'>" & CCommon.ToString(dtData.Rows(i)(str)) & "</a>"
                                            ElseIf vcOrigDbColumnName = "vcCompanyName" AndAlso dtData.Columns.Contains("DivID") Then
                                                cell += "<a href='../account/frmAccounts.aspx?DivID=" & CCommon.ToString(dtData.Rows(i)("DivID")) & "' target='_blank'>" & CCommon.ToString(dtData.Rows(i)(str)) & "</a>"
                                            ElseIf vcOrigDbColumnName = "vcItemName" AndAlso dtData.Columns.Contains("ItemCode") Then
                                                cell += "<a href='../Items/frmKitDetails.aspx?ItemCode=" & CCommon.ToString(dtData.Rows(i)("ItemCode")) & "&frm=All Items' target='_blank'>" & CCommon.ToString(dtData.Rows(i)(str)) & "</a>"
                                            ElseIf vcOrigDbColumnName = "vcBizDocID" AndAlso dtData.Columns.Contains("OpID") AndAlso dtData.Columns.Contains("BizDocId") Then
                                                cell += "<a href='../opportunity/frmBizInvoice.aspx?opId=" & CCommon.ToString(dtData.Rows(i)("OpID")) & "&OppBizId=" & CCommon.ToString(dtData.Rows(i)("BizDocId")) & "' target='_blank'>" & CCommon.ToString(dtData.Rows(i)(str)) & "</a>"
                                            ElseIf vcOrigDbColumnName = "vcCaseNumber" AndAlso dtData.Columns.Contains("CaseID") Then
                                                cell += "<a href='../cases/frmCases.aspx?frmCaselist&CaseID=" & CCommon.ToString(dtData.Rows(i)("CaseID")) & "' target='_blank'>" & CCommon.ToString(dtData.Rows(i)(str)) & "</a>"
                                            Else
                                                cell += CCommon.ToString(dtData.Rows(i)(str))
                                            End If
                                        End If
                                    End If

                                    cell += "</td>"
                                    trRow += cell
                                Next
                                trRow += "</tr>"
                                html += trRow
                            Next
                        End If

                    ElseIf ds.Tables(0).Rows(0)("tintReportType") = 2 Then 'Matrix
                        Dim columnCount As Integer = 1
                        Dim dtColumns = New DataTable()

                        trHeader = "<tr>"
                        trHeader += ("<th></th>")
                        Dim vcOrigDbColumnName As String = ""
                        If objReport.ColumnBreaks.Count > 0 Then
                            Dim NameGroups = dtData.AsEnumerable().GroupBy(Function(r) r.Field(Of Object)(objReport.ColumnBreaks(0)))

                            dtColumns.Columns.Add("ColumnName", GetType(String))
                            dtColumns.Columns.Add("OpId", GetType(Int32))

                            For Each NameGroup As Object In NameGroups

                                If NameGroup.Key Is Nothing Or CCommon.ToString(NameGroup.Key).Trim.Length = 0 Then
                                    dtColumns.Rows.Add("")
                                Else
                                    dtColumns.Rows.Add(NameGroup.Key)
                                End If
                            Next

                            Dim strColumn As String() = objReport.ColumnBreaks(0).Split("_")

                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))
                            vcOrigDbColumnName = CCommon.ToString(dr(0)("vcOrigDbColumnName"))

                            If dr.Length > 0 Then
                                trHeader += ("<th>" & CCommon.ToString(dr(0)("vcFieldname")) & "</th>")
                                columnCount += 1
                            End If

                            For Each dc As DataRow In dtColumns.Rows
                                trHeader += "<th>" & CCommon.ToString(dr(0)("vcFieldname"))
                                If vcOrigDbColumnName = "vcPoppName" Then
                                    If isForEmail Then
                                        trHeader += CCommon.ToString(dc(0))
                                    Else
                                        trHeader += ("<a href='../opportunity/frmOpportunities.aspx?opId=" & CCommon.ToString(NameGroups.ElementAt(0).ElementAt(0).ItemArray.ElementAt(3)) & "' target='_blank'>" & CCommon.ToString(dc(0)) & "</a>")
                                    End If
                                ElseIf vcOrigDbColumnName = "vcCompanyName" Then
                                    If isForEmail Then
                                        trHeader += CCommon.ToString(dc(0))
                                    Else
                                        trHeader += ("<a href='../opportunity/frmOpportunities.aspx?opId=" & CCommon.ToString(NameGroups.ElementAt(0).ElementAt(0).ItemArray.ElementAt(3)) & "' target='_blank'>" & CCommon.ToString(dc(0)) & "</a>")
                                    End If
                                Else
                                    trHeader += CCommon.ToString(dc(0))
                                End If

                                trHeader += "</th>"
                                columnCount += 1
                            Next
                        Else
                            trHeader += "<th></th>"
                            columnCount += 1
                        End If

                        trHeader += "<th class='grandTotal'>Grand Total</th>"

                        trHeader += "</tr>"
                        html += trHeader

                        If objReport.RowBreaks.Count > 0 Then
                            trHeader = "<tr>"
                            Dim strColumn As String() = objReport.RowBreaks(0).Split("_")

                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))
                            vcOrigDbColumnName = CCommon.ToString(dr(0)("vcOrigDbColumnName"))
                            If dr.Length > 0 Then
                                trHeader += "<th>" & CCommon.ToString(dr(0)("vcFieldname")) & "</th>"
                            End If

                            trHeader += "<th colspan='" & columnCount & "'></th>"

                            trHeader += "</tr>"
                            html += trHeader
                        End If

                        Dim strCell As String = ""
                        Dim strCellSummary As String = ""

                        For Each str As AggregateObject In objReport.Aggregate
                            Dim strColumn As String() = str.Column.Split("_")
                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                Select Case str.aggType

                                    Case "sum"
                                        strCell += "Sum of No. of " & dr(0)("vcFieldname") & "<br/>"

                                    Case "avg"
                                        strCell += "Average of No. of " & dr(0)("vcFieldname") & "<br/>"

                                    Case "max"
                                        strCell += "Largest of No. of " & dr(0)("vcFieldname") & "<br/>"

                                    Case "min"
                                        strCell += "Smallest of No. of " & dr(0)("vcFieldname") & "<br/>"

                                End Select
                            End If
                        Next

                        If objReport.RowBreaks.Count > 0 Then
                            Dim CompanyGroups As System.Collections.Generic.IEnumerable(Of System.Linq.IGrouping(Of String, System.Data.DataRow)) = dtData.AsEnumerable().GroupBy(Function(r) r.Field(Of String)(objReport.RowBreaks(0)))
                            Dim n As Integer = 0
                            For Each CompanyGroup As System.Linq.IGrouping(Of String, System.Data.DataRow) In CompanyGroups
                                trRow = "<tr>"

                                cell = "<td>"
                                If vcOrigDbColumnName = "vcCompanyName" Then
                                    If isForEmail Then
                                        cell += CompanyGroup.Key
                                    Else
                                        cell += "<a href='../account/frmAccounts.aspx?DivID=" & CCommon.ToString(CompanyGroups.ElementAt(n).ElementAt(0).ItemArray.ElementAt(3)) & "' target='_blank'>" & CompanyGroup.Key & "</a>"
                                    End If
                                ElseIf vcOrigDbColumnName = "vcPoppName" Then
                                    If isForEmail Then
                                        cell += CompanyGroup.Key
                                    Else
                                        cell += "<a href='../opportunity/frmOpportunities.aspx?opId=" & CCommon.ToString(CompanyGroups.ElementAt(n).ElementAt(0).ItemArray.ElementAt(3)) & "' target='_blank'>" & CompanyGroup.Key & "</a>"
                                    End If
                                Else
                                    cell += CompanyGroup.Key
                                End If
                                cell += "</td>"
                                trRow += cell

                                cell = "<td>"
                                cell += strCell & "Record Count"
                                cell += "</td>"
                                trRow += cell

                                If objReport.ColumnBreaks.Count > 0 Then
                                    For Each col As DataRow In dtColumns.Rows
                                        Dim queryTotal = CompanyGroup.Where(Function(g) g.Field(Of Object)(objReport.ColumnBreaks(0)) = col(0))
                                        strCellSummary = CalculateAggregateSummary(objReport, queryTotal)

                                        cell = "<td>"
                                        cell += strCellSummary
                                        cell += "</td>"
                                        trRow += cell
                                    Next
                                End If

                                strCellSummary = CalculateAggregateSummary(objReport, CompanyGroup)

                                cell = "<td class='grandTotal'>"
                                cell += strCellSummary
                                cell += "</td>"
                                trRow += cell

                                trRow += "</tr>"
                                html += trRow
                                n = n + 1
                            Next
                        End If

                        trRow = "<tr>"

                        cell = "<td class='grandTotal'>"
                        cell += "Grand Total"
                        cell += "</td>"
                        trRow += cell

                        cell = "<td class='grandTotal'>"
                        cell += (strCell & "Record Count")
                        cell += "</td>"
                        trRow += cell

                        If objReport.ColumnBreaks.Count > 0 Then
                            For Each col As DataRow In dtColumns.Rows
                                Dim queryGrandTotal = dtData.AsEnumerable().Where(Function(g) g.Field(Of Object)(objReport.ColumnBreaks(0)) = col(0))

                                strCellSummary = CalculateAggregateSummary(objReport, queryGrandTotal)

                                cell = "<td class='grandTotal'>"
                                cell += strCellSummary
                                cell += "</td>"
                                trRow += cell
                            Next
                        End If

                        strCellSummary = CalculateAggregateSummary(objReport, dtData.AsEnumerable())

                        cell = "<td class='grandTotal'>"
                        cell += strCellSummary
                        cell += "</td>"
                        trRow += cell

                        trRow += "</tr>"
                        html += trRow
                    ElseIf ds.Tables(0).Rows(0)("tintReportType") = 3 Then 'KPI
                        trHeader = "<tr>"
                        trHeader += "<th>Object of Measure</th>"
                        trHeader += "<th>Date Measure</th>"
                        trHeader += "<th>Type</th>"
                        trHeader += "<th>Period</th>"
                        trHeader += "<th>Current</th>"
                        trHeader += "<th>Previous</th>"
                        trHeader += "<th>Change</th>"
                        trHeader += "</tr>"
                        html += trHeader



                        Dim decValue1 As Decimal = CCommon.ToDecimal(ds.Tables(5).Rows(0)("decValue1"))
                        Dim strThresoldType As String = CCommon.ToString(ds.Tables(5).Rows(0)("vcThresoldType"))
                        Dim decPrevious As Decimal = CCommon.ToDecimal(dtData.Rows(0)(0))
                        Dim decCurrent As Decimal = CCommon.ToDecimal(dtData.Rows(0)(1))

                        If (strThresoldType = "lt" AndAlso decCurrent < decValue1) Or
                            (strThresoldType = "gt" AndAlso decCurrent > decValue1) Or
                            (strThresoldType = "le" AndAlso decCurrent <= decValue1) Or
                            (strThresoldType = "ge" AndAlso decCurrent >= decValue1) Then
                            trRow = "<tr style='font-weight:bold'>"
                            'Object of Measure
                            cell = "<td>"
                            cell += objReport.vcGroupName & " <img src='../images/star_16.png'/>"
                            cell += "</td>"
                            trRow += cell
                        Else
                            trRow = "<tr>"
                            'Object of Measure
                            cell = "<td>"
                            cell += objReport.vcGroupName
                            cell += "</td>"
                            trRow += cell
                        End If

                        'Date Measure
                        Dim strColumn As String() = objReport.DateFieldFilter.Split("_")
                        Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                        cell = "<td>"
                        If dr.Length > 0 Then
                            cell += dr(0)("vcFieldName")
                        End If
                        cell += "</td>"
                        trRow += cell

                        'Type
                        cell = "<td>"
                        If objReport.KPIMeasureField = "0_0_False" Then
                            cell += "Record Count"
                        Else
                            strColumn = objReport.KPIMeasureField.Split("_")
                            dr = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                cell += dr(0)("vcFieldName")
                            End If
                        End If
                        cell += "</td>"
                        trRow += cell

                        'Period
                        cell = "<td>"
                        cell += GetPeriodTitle(objReport)
                        cell += "</td>"
                        trRow += cell

                        'Current
                        cell = "<td>"
                        cell += String.Format("{0:#,###.00}", decCurrent)
                        cell += "</td>"
                        trRow += cell

                        'Previous
                        cell = "<td>"
                        cell += String.Format("{0:#,###.00}", decPrevious)
                        cell += "</td>"
                        trRow += cell

                        'Change
                        Dim decVariance As Decimal = (decCurrent - decPrevious) / IIf(decPrevious = 0, 1, decPrevious)
                        cell = "<td>"
                        cell += String.Format("<font color='{1}'>{0:###.00%}</font>", decVariance, IIf(decVariance < 0, "red", "green"))
                        cell += "</td>"
                        trRow += cell

                        trRow += "</tr>"
                        html += trRow
                    ElseIf ds.Tables(0).Rows(0)("tintReportType") = 4 Then 'ScoreCard
                        trHeader = "<tr>"
                        trHeader += "<th>Object of Measure</th>"
                        trHeader += "<th>Date Measure</th>"
                        trHeader += "<th>Type</th>"
                        trHeader += "<th>Period</th>"
                        trHeader += "<th>Actual</th>"
                        trHeader += "<th>Goal</th>"
                        trHeader += "<th>Variance</th>"
                        trHeader += "<th>Score</th>"
                        trHeader += "</tr>"
                        html += trHeader

                        trRow = "<tr>"

                        'Object of Measure
                        cell = "<td>"
                        cell += objReport.vcGroupName
                        cell += "</td>"
                        trRow += cell

                        'Date Measure
                        Dim strColumn As String() = objReport.DateFieldFilter.Split("_")
                        Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                        cell = "<td>"
                        If dr.Length > 0 Then
                            cell += dr(0)("vcFieldName")
                        End If
                        cell += "</td>"
                        trRow += cell

                        'Type
                        cell = "<td>"
                        If objReport.KPIMeasureField = "0_0_False" Then
                            cell += "Record Count"
                        Else
                            strColumn = objReport.KPIMeasureField.Split("_")
                            dr = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                cell += dr(0)("vcFieldName")
                            End If
                        End If
                        cell += "</td>"
                        trRow += cell

                        'Period
                        cell = "<td>"
                        cell += GetPeriodTitle(objReport)
                        cell += "</td>"
                        trRow += cell

                        Dim decActual As Decimal = CCommon.ToDecimal(dtData.Rows(0)(0))
                        Dim decValue1 As Decimal = CCommon.ToDecimal(ds.Tables(5).Rows(0)("decValue1"))
                        Dim decValue2 As Decimal = CCommon.ToDecimal(ds.Tables(5).Rows(0)("decValue2"))

                        'Actual
                        cell = "<td>"
                        cell += String.Format("{0:#,###.00}", decActual)
                        cell += "</td>"
                        trRow += cell

                        'Goal
                        cell = "<td>"
                        cell += String.Format("{0:#,###.00}", decValue1)
                        cell += "</td>"
                        trRow += cell

                        'Variance
                        Dim decVariance As Decimal = decActual - decValue1
                        cell = "<td>"
                        cell += String.Format("<font color='{1}'>{0:#,###.00}</font>", decVariance, IIf(decVariance < 0, "red", "green"))
                        cell += "</td>"
                        trRow += cell

                        'Score
                        cell = "<td>"

                        If decActual >= decValue1 Then
                            cell += " <img src='../images/bullet-green_16.png'/>"
                        ElseIf decActual < decValue1 AndAlso decActual >= decValue2 Then
                            cell += " <img src='../images/triangle_yellow_16.png'/>"
                        Else
                            cell += " <img src='../images/diamond_red_16.png'/>"
                        End If

                        cell += "</td>"
                        trRow += cell

                        trRow += "</tr>"
                        html += trRow
                    ElseIf ds.Tables(0).Rows(0)("tintReportType") = 5 Then 'KPI
                        trHeader = "<tr>"

                        If objReport.ColumnList.Count > 0 Then
                            For Each str As String In objReport.ColumnList
                                Dim strColumnKPI As String() = str.Split("_")

                                Dim drKPI() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumnKPI(0) & " AND numFieldID=" & strColumnKPI(1) & " AND bitCustom=" & strColumnKPI(2))

                                If drKPI.Length > 0 Then
                                    trHeader += ("<th>" & CCommon.ToString(drKPI(0)("vcFieldname")) & "</th>")
                                End If
                            Next

                            'trHeader += "</tr>"
                            'html += trHeader
                        Else
                            trHeader += "<th>Object of Measure</th>"
                            trHeader += "<th>Date Measure</th>"
                            trHeader += "<th>Type</th>"
                            trHeader += "<th>Period</th>"
                        End If
                        trHeader += "<th>Current</th>"
                        trHeader += "<th>Previous</th>"
                        trHeader += "<th>Change</th>"
                        trHeader += "</tr>"
                        html += trHeader

                        If objReport.ColumnList.Count > 0 Then
                            Dim vcFieldDataType As String = ""

                            For i As Integer = 0 To dtData.Rows.Count - 1
                                trRow = "<tr>"
                                Dim decPrevious As Decimal = CCommon.ToDecimal(dtData.Rows(i)("Previous"))
                                Dim decCurrent As Decimal = CCommon.ToDecimal(dtData.Rows(i)("Current"))
                                Dim decValue1 As Decimal = CCommon.ToDecimal(ds.Tables(5).Rows(0)("decValue1"))
                                Dim strThresoldType As String = CCommon.ToString(ds.Tables(5).Rows(0)("vcThresoldType"))

                                If (strThresoldType = "lt" AndAlso decCurrent < decValue1) Or
                                    (strThresoldType = "gt" AndAlso decCurrent > decValue1) Or
                                    (strThresoldType = "le" AndAlso decCurrent <= decValue1) Or
                                    (strThresoldType = "ge" AndAlso decCurrent >= decValue1) Then
                                    trRow = "<tr style='font-weight:bold'>"
                                Else
                                    trRow = "<tr>"
                                End If

                                Dim colIndex As Integer = 1

                                For Each str As String In objReport.ColumnList
                                    vcFieldDataType = ""

                                    Dim strColumn As String() = str.Split("_")
                                    Dim drColumn() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                                    If Not drColumn Is Nothing AndAlso drColumn.Length > 0 Then
                                        vcFieldDataType = CCommon.ToString(drColumn(0)("vcFieldDataType"))
                                    End If

                                    cell = "<td>"
                                    If IsNumeric(dtData.Rows(i)(str)) AndAlso (vcFieldDataType = "M" Or vcFieldDataType = "N") Then
                                        If vcFieldDataType = "M" Then
                                            cell += CCommon.GetDecimalFormat(CCommon.ToDouble(dtData.Rows(i)(str)))
                                        Else
                                            cell += String.Format("{0:#,##0.####################}", CCommon.ToDouble(dtData.Rows(i)(str)))
                                        End If
                                    Else
                                        cell += CCommon.ToString(dtData.Rows(i)(str))
                                    End If

                                    If colIndex = 1 Then
                                        If (strThresoldType = "lt" AndAlso decCurrent < decValue1) Or
                                           (strThresoldType = "gt" AndAlso decCurrent > decValue1) Or
                                           (strThresoldType = "le" AndAlso decCurrent <= decValue1) Or
                                           (strThresoldType = "ge" AndAlso decCurrent >= decValue1) Then
                                            cell += " <img src='../images/star_16.png'/>"
                                        End If
                                    End If

                                    cell += "</td>"
                                    trRow += cell

                                    colIndex += 1
                                Next

                                'Current
                                cell = "<td>"
                                cell += String.Format("{0:#,###.00}", decCurrent)
                                cell += "</td>"
                                trRow += cell

                                'Previous
                                cell = "<td>"
                                cell += String.Format("{0:#,###.00}", decPrevious)
                                cell += "</td>"
                                trRow += cell

                                'Change
                                Dim decVariance As Decimal = (decCurrent - decPrevious) / IIf(decPrevious = 0, 1, decPrevious)
                                cell = "<td>"
                                cell += String.Format("<font color='{1}'>{0:###.00%}</font>", decVariance, IIf(decVariance < 0, "red", "green"))
                                cell += "</td>"
                                trRow += cell

                                trRow += "</tr>"
                                html += trRow
                            Next
                        Else
                            trRow = "<tr>"

                            Dim decValue1 As Decimal = CCommon.ToDecimal(ds.Tables(5).Rows(0)("decValue1"))
                            Dim strThresoldType As String = CCommon.ToString(ds.Tables(5).Rows(0)("vcThresoldType"))
                            Dim decPrevious As Decimal = CCommon.ToDecimal(dtData.Rows(0)("Previous"))
                            Dim decCurrent As Decimal = CCommon.ToDecimal(dtData.Rows(0)("Current"))

                            If (strThresoldType = "lt" AndAlso decCurrent < decValue1) Or
                                (strThresoldType = "gt" AndAlso decCurrent > decValue1) Or
                                (strThresoldType = "le" AndAlso decCurrent <= decValue1) Or
                                (strThresoldType = "ge" AndAlso decCurrent >= decValue1) Then
                                trRow = "<tr style='font-weight:bold'>"

                                'Object of Measure
                                cell = "<td>"
                                cell += objReport.vcGroupName & " <img src='../images/star_16.png'/>"
                                cell += "</td>"
                                trRow += cell
                            Else
                                trRow = "<tr>"

                                'Object of Measure
                                cell = "<td>"
                                cell += objReport.vcGroupName
                                cell += "</td>"
                                trRow += cell
                            End If

                            'Date Measure
                            Dim strColumn As String() = objReport.DateFieldFilter.Split("_")
                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            cell = "<td>"
                            If dr.Length > 0 Then
                                cell += dr(0)("vcFieldName")
                            End If
                            cell += "</td>"
                            trRow += cell

                            'Type
                            cell = "<td>"
                            If objReport.KPIMeasureField = "0_0_False" Then
                                cell += "Record Count"
                            Else
                                strColumn = objReport.KPIMeasureField.Split("_")
                                dr = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                                If dr.Length > 0 Then
                                    cell += dr(0)("vcFieldName")
                                End If
                            End If
                            cell += "</td>"
                            trRow += cell

                            'Period
                            cell = "<td>"
                            cell += GetPeriodTitle(objReport)
                            cell += "</td>"
                            trRow += cell

                            'Current
                            cell = "<td>"
                            cell += String.Format("{0:#,###.00}", decCurrent)
                            cell += "</td>"
                            trRow += cell

                            'Previous
                            cell = "<td>"
                            cell += String.Format("{0:#,###.00}", decPrevious)
                            cell += "</td>"
                            trRow += cell

                            'Change
                            Dim decVariance As Decimal = (decCurrent - decPrevious) / IIf(decPrevious = 0, 1, decPrevious)
                            cell = "<td>"
                            cell += String.Format("<font color='{1}'>{0:###.00%}</font>", decVariance, IIf(decVariance < 0, "red", "green"))
                            cell += "</td>"
                            trRow += cell

                            trRow += "</tr>"
                            html += trRow
                        End If
                    End If
                End If

                Return html
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Function CalculateAggregateSummary(objReport As ReportObject, queryTotal As IEnumerable(Of System.Data.DataRow)) As String
            Try
                Dim strCellSummary As String = ""

                Dim total As Integer = queryTotal.Count()

                For Each str As AggregateObject In objReport.Aggregate

                    Select Case str.aggType
                        Case "sum"
                            If total = 0 Then
                                strCellSummary += "0" & "<br/>"
                            Else
                                strCellSummary += String.Format("{0:#.00}", queryTotal.Sum(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                            End If

                        Case "avg"
                            If total = 0 Then
                                strCellSummary += "0" & "<br/>"
                            Else
                                strCellSummary += String.Format("{0:#.00}", queryTotal.Average(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                            End If

                        Case "max"
                            If total = 0 Then
                                strCellSummary += "0" & "<br/>"
                            Else
                                strCellSummary += String.Format("{0:#.00}", queryTotal.Max(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                            End If

                        Case "min"
                            If total = 0 Then
                                strCellSummary += "0" & "<br/>"
                            Else
                                strCellSummary += String.Format("{0:#.00}", queryTotal.Min(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                            End If

                    End Select
                Next

                strCellSummary = strCellSummary & total
                Return strCellSummary
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function GetPeriodTitle(objReport As ReportObject)
            Try
                Select Case objReport.DateFieldValue
                    Case "AllTime" : GetPeriodTitle = "All Time"
                    Case "Custom"
                        If objReport.FromDate.Length > 0 AndAlso objReport.ToDate.Length > 0 Then
                            Dim fromDate As DateTime = objReport.FromDate
                            Dim toDate As DateTime = objReport.ToDate
                            GetPeriodTitle = String.Format("{0:MM/dd/YYYY} - {1:MM/dd/YYYY}", objReport.FromDate, objReport.ToDate)
                        End If
                        'Year
                    Case "CurYear" : GetPeriodTitle = "Current CY"
                    Case "PreYear" : GetPeriodTitle = "Previous CY"
                    Case "Pre2Year" : GetPeriodTitle = "Previous 2 CY"
                    Case "Ago2Year" : GetPeriodTitle = "2 FY Ago"
                    Case "NextYear" : GetPeriodTitle = "Next CY"
                    Case "CurPreYear" : GetPeriodTitle = "Current and Previous CY"
                    Case "CurPre2Year" : GetPeriodTitle = "Current and Previous 2 CY"
                    Case "CurNextYear" : GetPeriodTitle = "Current and Next CY"
                        'Quarter
                    Case "CuQur" : GetPeriodTitle = "Current CQ"
                    Case "CurNextQur" : GetPeriodTitle = "Current and Next CQ"
                    Case "CurPreQur" : GetPeriodTitle = "Current and Previous CQ"
                    Case "NextQur" : GetPeriodTitle = "Next CQ"
                    Case "PreQur" : GetPeriodTitle = "Previous CQ"
                        'Month
                    Case "LastMonth" : GetPeriodTitle = "Last Month"
                    Case "ThisMonth" : GetPeriodTitle = "This Month"
                    Case "NextMonth" : GetPeriodTitle = "Next Month"
                    Case "CurPreMonth" : GetPeriodTitle = "Current and Previous Month"
                    Case "CurNextMonth" : GetPeriodTitle = "Current and Next Month"
                        'Week
                    Case "LastWeek" : GetPeriodTitle = "Last Week"
                    Case "ThisWeek" : GetPeriodTitle = "This Week"
                    Case "NextWeek" : GetPeriodTitle = "Next Week"
                        'Day
                    Case "Yesterday" : GetPeriodTitle = "Yesterday"
                    Case "Today" : GetPeriodTitle = "Today"
                    Case "Tomorrow" : GetPeriodTitle = "Tomorrow"
                    Case "Last7Day" : GetPeriodTitle = "Last 7 Days"
                    Case "Last30Day" : GetPeriodTitle = "Last 30 Days"
                    Case "Last60Day" : GetPeriodTitle = "Last 60 Days"
                    Case "Last90Day" : GetPeriodTitle = "Last 90 Days"
                    Case "Last120Day" : GetPeriodTitle = "Last 120 Days"
                    Case "Next7Day" : GetPeriodTitle = "Next 7 Days"
                    Case "Next30Day" : GetPeriodTitle = "Next 30 Days"
                    Case "Next60Day" : GetPeriodTitle = "Next 60 Days"
                    Case "Next90Day" : GetPeriodTitle = "Next 90 Days"
                    Case "Next120Day" : GetPeriodTitle = "Next 120 Days"
                        'KPI Period
                    Case "Year" : GetPeriodTitle = "This Year vs. Last Year"
                    Case "Quarter" : GetPeriodTitle = "This Quarter vs. Last Quarter"
                    Case "Month" : GetPeriodTitle = "This Month vs. Last Month"
                    Case "Week" : GetPeriodTitle = "This Week vs. Last Week"

                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function StartDashboardReportDataCache()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Dim dtReports As DataTable = SqlDAL.ExecuteDatable(connString, "USP_ReportDashboard_GetForDataCache", arParms)

                If Not dtReports Is Nothing AndAlso dtReports.Rows.Count > 0 Then
                    For Each drReport As DataRow In dtReports.Rows
                        Try
                            Dim directoryPath As String = System.Configuration.ConfigurationManager.AppSettings("PortalPath") & Convert.ToInt64(drReport("numDomainID")) & "\Dashboard"
                            If Not System.IO.Directory.Exists(directoryPath) Then
                                System.IO.Directory.CreateDirectory(directoryPath)
                            End If

                            If CCommon.ToBool(drReport("bitDefault")) AndAlso (CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.AR Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.AP Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.MoneyInBank Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitMarginCompanyWide Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10CustomerByProfitAmountCompanyWide Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.ActionItemsAndMeetingsCompanyWide Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.RPEYTDvsSamePeriodLastYear Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.RPELastMonthvsSamePeriodLastYear Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.TopSourcesOfSalesOrders Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10ItemsByProfitMarginCompanyWide Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByRevenue Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByProgress Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10ItemsByReturnedVsSold Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10SalesOpportunityByPastDue Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10CampaignsByROI Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.KPI Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.LeadSource Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10Email Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Reminders Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.TopReasonsWinningDeals Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.TopReasonsLossingDeals Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.Top10ReasonsForSalesReturns Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformancePanel1 Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.PartnerRMPLast12Months Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformance1 Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.EmployeeBenefittoCompany Or _
                                                                               CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.First10SavedSearches Or _
                                                                               (CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.SalesOppWonLost) Or _
                                                                               (CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.SalesOppPipeLine) Or _
                                                                               (CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.BackOrderValuation) Or _
                                                                               (CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.MonthToDateRevenue) Or _
                                                                               (CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.InventoryValueByWareHouse) Or _
                                                                               (CCommon.ToInteger(drReport("intDefaultReportID")) = CustomReportsManage.PrebuildReportEnum.PendingReportDetails)) Then
                                Dim objReportManage As New CustomReportsManage
                                objReportManage.DomainID = CCommon.ToLong(drReport("numDomainID"))
                                objReportManage.UserCntID = CCommon.ToLong(drReport("numUserCntID"))
                                objReportManage.DashBoardID = CCommon.ToLong(drReport("numDashBoardID"))
                                objReportManage.DefaultReportID = CCommon.ToInteger(drReport("intDefaultReportID"))
                                objReportManage.ClientTimeZoneOffset = 0
                                Dim html As String = CCommon.ToString(objReportManage.GetPrebuildReprtHtml(False))

                                System.IO.File.WriteAllText(directoryPath & "\" & objReportManage.DashBoardID & ".html", html)

                                objReportManage.UpdateLastCacheDate()
                            ElseIf Not CCommon.ToBool(drReport("bitDefault")) Then
                                Dim ds As DataSet
                                Dim objReportManage As New CustomReportsManage
                                objReportManage.DomainID = CCommon.ToLong(drReport("numDomainID"))
                                objReportManage.DashBoardID = CCommon.ToLong(drReport("numDashBoardID"))
                                objReportManage.UserCntID = CCommon.ToLong(drReport("numUserCntID"))
                                objReportManage.ClientTimeZoneOffset = 0
                                objReportManage.ReportID = Convert.ToInt64(drReport("numReportID"))
                                ds = objReportManage.GetReportListMasterDetail

                                If ds.Tables.Count > 0 Then

                                    Dim objReport As ReportObject = objReportManage.GetDatasetToReportObject(ds)

                                    Dim dsColumnList As DataSet
                                    dsColumnList = objReportManage.GetReportModuleGroupFieldMaster

                                    Try
                                        objReportManage.CurrentPage = 1
                                        objReportManage.textQuery = objReportManage.GetReportQuery(objReportManage, ds, objReport, False, True, dsColumnList)

                                        Dim dtData As DataTable = objReportManage.USP_ReportQueryExecute()

                                        Dim html As String = "<table class='table table-responsive table-bordered'>"
                                        html += objReportManage.GetCustomReportHtml(ds, dsColumnList, dtData, objReport, False)
                                        html += "</table>"


                                        System.IO.File.WriteAllText(directoryPath & "\" & objReportManage.DashBoardID & ".html", html)

                                        objReportManage.UpdateLastCacheDate()
                                    Catch ex As Exception
                                        Throw
                                    End Try
                                End If
                            End If
                        Catch ex As Exception
                            ExceptionModule.ExceptionPublish(ex.Message, CCommon.ToLong(drReport("numDashBoardID")), CCommon.ToLong(drReport("numUserCntID")), Nothing)
                        End Try
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub UpdateLastCacheDate()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDashboardID", DashBoardID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ReportDashboard_UpdateLastCacheDate", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Class ReportQueryResult
            Public Property Key1 As String
            Public Property Key2 As String
            Public Property Key3 As String
            Public Property CountTotal As Integer
            Public Property Detail As System.Collections.Generic.IEnumerable(Of DataRow)
        End Class
    End Class
End Namespace
