﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Reports

    Public Class ScheduledReportsGroupLog
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property SRGID As Long
        Public Property Exception As String

#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numSRGID", SRGID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcException", Exception, NpgsqlTypes.NpgsqlDbType.Smallint))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ScheduledReportsGroupLog_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class

End Namespace
