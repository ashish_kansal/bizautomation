'Created by Sivaprakasam
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Reports
    Public Class LeadConversion
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _DomianId As Long
        'Private UserCntID As Long
        Private _StartDate As Date
        Private _EndDate As Date
        Private _PerType As Integer
        Private _LeadGroupId As Integer

        Public Property PerType() As Integer
            Get
                Return _PerType
            End Get
            Set(ByVal Value As Integer)
                _PerType = Value
            End Set
        End Property

        Public Property EndDate() As Date
            Get
                Return _EndDate
            End Get
            Set(ByVal Value As Date)
                _EndDate = Value
            End Set
        End Property

        Public Property StartDate() As Date
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As Date)
                _StartDate = Value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property DomianId() As Long
            Get
                Return _DomianId
            End Get
            Set(ByVal Value As Long)
                _DomianId = Value
            End Set
        End Property

        Public Property LeadGroupId() As Long
            Get
                Return _LeadGroupId
            End Get
            Set(ByVal Value As Long)
                _LeadGroupId = Value
            End Set
        End Property


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Sivaprakasam	DATE:29-Nov-06
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Sivaprakasam	DATE:29-Nov-06
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Public Function GetLeadConversionDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomianId

                arParms(2) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _StartDate

                arParms(3) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _EndDate

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PerType

                arParms(5) = New Npgsql.NpgsqlParameter("@numLeadGroupId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _LeadGroupId

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_LeadConversionReport", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


    End Class
End Namespace