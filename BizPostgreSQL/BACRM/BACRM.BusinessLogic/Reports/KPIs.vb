﻿'Imports Microsoft.AnalysisServices.AdomdClient
'Imports SQLMD

Public Class KPIs
    Dim _Account As String
    Dim _Period As String
    Dim _AccountCode As String
    Public Property AccountCode() As String
        Get
            Return _AccountCode
        End Get
        Set(ByVal value As String)
            _AccountCode = value
        End Set
    End Property
    Public Property Account() As String
        Get
            Return _Account
        End Get
        Set(ByVal value As String)
            _Account = value
        End Set
    End Property

    Public Property Period() As String
        Get
            Return _Period
        End Get
        Set(ByVal value As String)
            _Period = value
        End Set
    End Property

    'Public Function FillMDCellSet(ByVal strSql As String) As CellSet
    '    Dim MD As New SQLMD
    '    FillMDCellSet = MD.ExecuteMDCellSet(strSql)
    'End Function
    Public Function KPIAccounts() As DataTable

        Try
            Select Case _Account
                Case "Bank Balance"
                    AccountCode = "01010101"
                Case "Cash In Hand"
                    AccountCode = "01010102"
                Case "Expenses"
                    AccountCode = "0104"
                Case "Direct Expenses"
                    AccountCode = "010401"
                Case "In-Direct Expenses"
                    AccountCode = "010402"
                Case "Fixed Assets"
                    AccountCode = "010102"
                Case "Income"
                    AccountCode = "0103"
                Case "Long Term Liabilities"
                    AccountCode = "01020103"
                Case "Net Cash Flow"
                Case "Operating Expenses"
                    AccountCode = "1040103"
                Case "Other Current Assets"
                    AccountCode = "not in (01010101,01010102)" 'w/o bank and cash
                Case "Other Current Liabilities"
                    AccountCode = ""
                Case "Payables"
                    AccountCode = "01020102"
                Case "Payroll"
                Case "Profit"
                Case "Receivables"
                    AccountCode = "01010105"
                Case "Operating Cash Flow"
                Case "Revenue"
                    AccountCode = "010301"
                Case "Over Due Balance"
                Case "Total Bank Balance"
                    AccountCode = "01010101"
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
