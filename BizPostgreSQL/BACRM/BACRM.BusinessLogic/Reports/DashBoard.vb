''' -----------------------------------------------------------------------------
''' Project	 : BACRM.BusinessLogic
''' Class	 : DashBoard
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This class contains several methods and functions, which is used to 
''' display mini reports in the dashboard.
''' </summary>
''' <remarks>
'''     This class is used to access the data for displaying the mini reports in
''' dashboard.
''' </remarks>
''' <history>
''' 	[Maha]	03/06/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Option Explicit On 
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Reports

    Public Class DashBoard
        Inherits BACRM.BusinessLogic.CBusinessBase



        '#Region "Constructor"
        '        ''' -----------------------------------------------------------------------------
        '        ''' <summary>
        '        ''' 
        '        ''' </summary>
        '        ''' <param name="intUserId"></param>
        '        ''' <remarks>
        '        ''' </remarks>
        '        ''' <history>
        '        ''' 	[administrator]	03/06/2005	Created
        '        ''' </history>
        '        ''' -----------------------------------------------------------------------------
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub
        '        ''' -----------------------------------------------------------------------------
        '        ''' <summary>
        '        ''' 
        '        ''' </summary>
        '        ''' <remarks>
        '        ''' </remarks>
        '        ''' <history>
        '        ''' 	[administrator]	03/06/2005	Created
        '        ''' </history>
        '        ''' -----------------------------------------------------------------------------
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the User id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the user id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private UserCntID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Territory id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the territory id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _TerritoryID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the From date.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Fromdate.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _FromDate As Date
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Todate.
        ''' </summary>
        ''' <remarks>
        '''     This holds the todate.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ToDate As Date
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the User rights.
        ''' </summary>
        ''' <remarks>
        '''     This holds the User rights.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _UserRights As Integer
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the From month.
        ''' </summary>
        ''' <remarks>
        '''     This holds the from month.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _FromMonth As Integer
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the To month.
        ''' </summary>
        ''' <remarks>
        '''     This holds the To month.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ToMonth As Integer
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Year.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Year.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _Year As Integer
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Module Id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Module id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ModuleId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Group Id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Group id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _GroupId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Type id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Type id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _TypeId As Long

        Private _DashboardReptId As Long
        Private _ReportType As Short
        Private _Header As String
        Private _Footer As String
        Private _ChartType As Short
        Private _ReportID As Long
        Private _ColumnPos As Short
        Private _RowPos As Short
        Private _RepSize As Short
        Private _MoveDashboard As Char

        Public Property MoveDashboard() As Char
            Get
                Return _MoveDashboard
            End Get
            Set(ByVal Value As Char)
                _MoveDashboard = Value
            End Set
        End Property

        Public Property RepSize() As Short
            Get
                Return _RepSize
            End Get
            Set(ByVal Value As Short)
                _RepSize = Value
            End Set
        End Property

        Public Property RowPos() As Short
            Get
                Return _RowPos
            End Get
            Set(ByVal Value As Short)
                _RowPos = Value
            End Set
        End Property

        Public Property ColumnPos() As Short
            Get
                Return _ColumnPos
            End Get
            Set(ByVal Value As Short)
                _ColumnPos = Value
            End Set
        End Property

        Public Property ReportID() As Long
            Get
                Return _ReportID
            End Get
            Set(ByVal Value As Long)
                _ReportID = Value
            End Set
        End Property

        Public Property ChartType() As Short
            Get
                Return _ChartType
            End Get
            Set(ByVal Value As Short)
                _ChartType = Value
            End Set
        End Property

        Public Property Footer() As String
            Get
                Return _Footer
            End Get
            Set(ByVal Value As String)
                _Footer = Value
            End Set
        End Property

        Public Property Header() As String
            Get
                Return _Header
            End Get
            Set(ByVal Value As String)
                _Header = Value
            End Set
        End Property

        Public Property ReportType() As Short
            Get
                Return _ReportType
            End Get
            Set(ByVal Value As Short)
                _ReportType = Value
            End Set
        End Property

        Public Property DashboardReptId() As Long
            Get
                Return _DashboardReptId
            End Get
            Set(ByVal Value As Long)
                _DashboardReptId = Value
            End Set
        End Property



        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User id.
        ''' </summary>
        ''' <value>Returns the User id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the User id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	07/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	10/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User id.
        ''' </summary>
        ''' <value>Returns the Territory id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the territory id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User rights.
        ''' </summary>
        ''' <value>Returns the User rights as Integer.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the User rights.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property UserRights() As Integer
            Get
                Return _UserRights
            End Get
            Set(ByVal Value As Integer)
                _UserRights = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Set and Gets the Date.
        ''' </summary>
        ''' <value>Returns the vlaue as integer.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Fromdate.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the To date.
        ''' </summary>
        ''' <value>Returns the value as integer.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the To date.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the From month.
        ''' </summary>
        ''' <value>Returns the value as integer.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the From month.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FromMonth() As Integer
            Get
                Return _FromMonth
            End Get
            Set(ByVal Value As Integer)
                _FromMonth = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the To month.
        ''' </summary>
        ''' <value>Returns the value as integer.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the To month.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ToMonth() As Integer
            Get
                Return _ToMonth
            End Get
            Set(ByVal Value As Integer)
                _ToMonth = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Year.
        ''' </summary>
        ''' <value>Returns the value as integer.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Year.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property GetYear() As Integer
            Get
                Return _Year
            End Get
            Set(ByVal Value As Integer)
                _Year = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Module id.
        ''' </summary>
        ''' <value>Returns the value as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Module id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ModuleId() As Long
            Get
                Return _ModuleId
            End Get
            Set(ByVal Value As Long)
                _ModuleId = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Group id.
        ''' </summary>
        ''' <value>Returns the value as long.</value>
        ''' <remarks>   
        '''     This property is used to Set and Get the Group id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property GroupId() As Long
            Get
                Return _GroupId
            End Get
            Set(ByVal Value As Long)
                _GroupId = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Type id.
        ''' </summary>
        ''' <value>Returns the value as Type id.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Type id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property TypeId() As Long
            Get
                Return _TypeId
            End Get
            Set(ByVal Value As Long)
                _TypeId = Value
            End Set
        End Property

        Private _strItems As String
        Public Property strItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal value As String)
                _strItems = value
            End Set
        End Property


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get the Sales details.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function is used to get the Sales details and it passes the
        ''' parameters Domain id, From date, To date, User id and Type Id to database.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetMySales() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@intTypeId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _TypeId

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetMySales", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function gets the details of top five sales from database.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function is used to get the details of top five sales details.  It 
        ''' passes the parameters Domain id, From date, TO date, Territory id,Rights, type
        ''' and User id to the stored prcodeure.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetTop5Sales() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _TerritoryID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _UserRights

                arParms(5) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _TypeId

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetTop5SalesRep", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This funciton is used to get the details of Top five items sold.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function is used to get the details of top five items sold.  This 
        ''' function calls the stored procedure with the parameters Domain id,From date,
        ''' To date, Territory id, Rights and type id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	07/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetTop5ItemsSold() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _TerritoryID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _TypeId

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetTop5ItemsSold", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get the details of top five accounts.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function is used to get the details of top five accounts.  It calls
        ''' the stored procedure with the parameters Domain id, From date, To date, User id,
        ''' Territory id, Rigthts and type.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetTop5Accounts() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _TerritoryID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _TypeId

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetTop5Accounts", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get the details of top five deal source.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function gets the details of top five deal source. It
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetTop5DealSource() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _UserRights

                arParms(5) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _TypeId

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetTop5DealSource", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to Get the Sales Vs Quota details.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function calls the storedprocedure with set of parameters
        ''' and returns the result for Sales vs quota.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetSalesVsQuota() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@intYear", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _Year

                arParms(2) = New Npgsql.NpgsqlParameter("@intFromMonth", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _FromMonth

                arParms(3) = New Npgsql.NpgsqlParameter("@intToMonth", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _ToMonth

                arParms(4) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _FromDate

                arParms(5) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = _ToDate

                arParms(6) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _TerritoryID

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(8).Value = _UserRights

                arParms(9) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(9).Value = _TypeId

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSalesVsQuota", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch and return the details of top ten deals
        ''' in the pipeline.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function calls the storedprocedure and returns the value for
        ''' top ten deals in the pipeline.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetTop10DealsInPipeline() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _TerritoryID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _TypeId

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetTop10DealsInPipeline", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to return the values for deal history.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function calls the storedprocedure and it returns the deails
        ''' of deal history.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetDealHistory() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _FromDate

                arParms(2) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _ToDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _TerritoryID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRights", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _UserRights

                arParms(6) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _TypeId

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetDealHistory", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for Modules for dashboard.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        ''' dashboard modules.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function ModulesForDashboard() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(0).Value = _ModuleId

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(1).Value = _GroupId

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAllSelectedSystemModulesPagesAndAccessesDashBoard", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the last day of the month.
        ''' </summary>
        ''' <param name="aDate">Represents the date.</param>
        ''' <returns></returns>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        '''         ''' 	[Maha]	17/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetLastDay(ByVal aDate As String)
            Dim intMonth As Integer
            Dim dteFirstDayNextMonth As Object

            dteFirstDayNextMonth = DateSerial(Year(aDate), Month(aDate) + 1, 1)
            GetLastDay = Day(DateAdd("d", -1, dteFirstDayNextMonth))
        End Function

        Public Function GetDashBoard() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(1).Value = _GroupId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDashBoard", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try

        End Function


        Public Function GetReptDTL() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDashBoardReptID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(0).Value = _DashboardReptId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDashBoardReptDTL", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function ManageDashboard() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintReportType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ReportType

                arParms(3) = New Npgsql.NpgsqlParameter("@vcHeader", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _Header

                arParms(4) = New Npgsql.NpgsqlParameter("@vcFooter", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = _Footer

                arParms(5) = New Npgsql.NpgsqlParameter("@tintChartType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _ChartType

                arParms(6) = New Npgsql.NpgsqlParameter("@numDashBoardReptID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _DashboardReptId

                arParms(7) = New Npgsql.NpgsqlParameter("@numReportID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _ReportID

                arParms(8) = New Npgsql.NpgsqlParameter("@tintColumn", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _ColumnPos

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "USP_ManagegeDashboard", arParms)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function ManageDashboardSize() As Boolean
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintColumn", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _ColumnPos

                arParms(1) = New Npgsql.NpgsqlParameter("@tintSize", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _RepSize

                arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _GroupId

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_DashboardSize", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function


        Public Function ArrangeDashboard() As Boolean
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDashboardReptID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DashboardReptId

                arParms(1) = New Npgsql.NpgsqlParameter("@charMove", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(1).Value = _MoveDashboard

                arParms(2) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _strItems

                SqlDAL.ExecuteNonQuery(connString, "USP_ArrangeDashboard", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetCommissionContactsReport() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCommissionContactsReport", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try

        End Function

    End Class
End Namespace
