
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Reports
    Public Class LeadsActivity
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _GroupID As Long
        'Private UserCntID As Long
        Private _UserRightType As Long
        Private _columnSortOrder As String
        Private _FollowUpStatus As Long
        'Private DomainId As Long
        Private _TeamMemID As Long
        Private _SortCharacter As String
        Private _columnName As String
        Private _TotalRecords As Integer
        Private _PageSize As Integer
        Private _CurrentPage As Integer
        Private _TeamType As Short
        Private _ListType As Short
        Private _FromDate As Date
        Private _ToDate As Date
        Private _UserRights As Integer


        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property
        Public Property ListType() As Short
            Get
                Return _ListType
            End Get
            Set(ByVal Value As Short)
                _ListType = Value
            End Set
        End Property

        Public Property GroupID() As Long
            Get
                Return _GroupID
            End Get
            Set(ByVal Value As Long)
                _GroupID = Value
            End Set
        End Property
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property
        Public Property UserRightType() As Long
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Long)
                _UserRightType = Value
            End Set
        End Property
        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property
        Public Property UserRights() As Integer
            Get
                Return _UserRights
            End Get
            Set(ByVal Value As Integer)
                _UserRights = Value
            End Set
        End Property


        Public Property FollowUpStatus() As Long
            Get
                Return _FollowUpStatus
            End Get
            Set(ByVal Value As Long)
                _FollowUpStatus = Value
            End Set
        End Property
        Public Property TeamMemID() As Long
            Get
                Return _TeamMemID
            End Get
            Set(ByVal Value As Long)
                _TeamMemID = Value
            End Set
        End Property
        Public Property TeamType() As Short
            Get
                Return _TeamType
            End Get
            Set(ByVal Value As Short)
                _TeamType = Value
            End Set
        End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property
        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Public Function GetTeamMembers() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = DomainId


                arParms(2) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _TeamType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim dr As DataSet
                dr = SqlDAL.ExecuteDataset(connString, "USP_GetTeamMembers", arParms)
                Return dr.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAllLeadList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(13) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGrpID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRights


                arParms(3) = New Npgsql.NpgsqlParameter("@numFollowUpStatus", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _FollowUpStatus


                arParms(4) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _TotalRecords

                arParms(5) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _columnName

                arParms(6) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(6).Value = _columnSortOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@ListType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _ListType

                arParms(8) = New Npgsql.NpgsqlParameter("@TeamType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _TeamType

                arParms(9) = New Npgsql.NpgsqlParameter("@TeamMemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _TeamMemID

                arParms(10) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = DomainId

                arParms(11) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(11).Value = _FromDate

                arParms(12) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(12).Value = _ToDate

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                Dim dr As DataSet
                dr = SqlDAL.ExecuteDataset(connString, "USP_GetLeadListReport", arParms)
                _TotalRecords = arParms(10).Value
                Return dr.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace

