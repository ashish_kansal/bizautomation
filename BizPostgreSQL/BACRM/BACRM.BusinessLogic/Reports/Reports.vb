'**********************************************************************************
' <CReports.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************

Option Explicit On 
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Reports

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CAccount
    ' 
    ' Description  : This is the business logic classe for Accounts Module
    '**********************************************************************************
    Public Class CReports
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _ModuleID As Long
        Private _GroupID As Long

        Public Property GroupID() As Long
            Get
                Return _GroupID
            End Get
            Set(ByVal Value As Long)
                _GroupID = Value
            End Set
        End Property
        Public Property ModuleID() As Long
            Get
                Return _ModuleID
            End Get
            Set(ByVal Value As Long)
                _ModuleID = Value
            End Set
        End Property




        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region
        Public Function PopulatedGrids(ByVal strQuery As String, ByVal strSortField As String) As DataSet
            '================================================================================
            ' Purpose: This procedure will populate the values in to the DataGrid
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     19/04/2003          Mahalingam .B         Created
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================
            Dim fn_GetConnectionString As String
            Try
                fn_GetConnectionString = GetConnection.GetConnectionString
                Dim cnnReport As New Npgsql.NpgSqlConnection(fn_GetConnectionString)
                cnnReport.Open() ' Open the connection
                Dim daReport As New Npgsql.NpgsqlDataAdapter(strQuery, cnnReport)
                Dim dsReport As New DataSet   ' Data set object
                daReport.Fill(dsReport, "FillData")
                Return dsReport
            Catch ex As Exception
                Throw New Exception("Error occurs while fetching the  record", ex)
            End Try

        End Function

        Public Function FindPosition(ByVal strQuery As String, ByVal strSortField() As String) As String
            '================================================================================
            ' Purpose: This procedure will populate the values in to the DataGrid
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     19/04/2003          Mahalingam .B         Created
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================
            Dim strHeader As String
            Dim fn_GetConnectionString As String
            Try
                fn_GetConnectionString = GetConnection.GetConnectionString
                Dim cnnReport As New Npgsql.NpgSqlConnection(fn_GetConnectionString)
                cnnReport.Open() ' Open the connection
                Dim cmdReport As New Npgsql.NpgsqlCommand(strQuery, cnnReport)
                cmdReport.CommandType = CommandType.Text
                cmdReport.CommandText = strQuery
                Dim drdrReport As Npgsql.NpgsqlDataReader ' Data reader object
                drdrReport = cmdReport.ExecuteReader()
                Dim inti As Integer = 0
                'While drdrReport.Read()
                For inti = 0 To drdrReport.FieldCount - 1
                    strHeader = strHeader & drdrReport.GetName(inti) & ","
                Next

                'End While

                cnnReport.Close() ' Close the Connection
                drdrReport = Nothing ' Dispose the Object
                cnnReport = Nothing ' Dispose the Object
                FindPosition = strHeader
            Catch ex As Exception
                Throw New Exception("Error occurs while fetching the  record", ex)
            End Try

        End Function

        Public Function FindTotals(ByVal strTblName As String, ByVal strFldName As String, ByVal strType As String) As String
            '================================================================================
            ' Purpose: This procedure will return the Sum,Avg,Largest value and Smallest value
            '               based on the fields chosen by the user
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     21/04/2003          Mahalingam .B         Created
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================
            If (strFldName <> "") Then
                Dim fn_GetConnectionString As String
                Dim strQuery As String
                'If the given parameter is Sum Then
                If strType = "Sum" Then
                    strQuery = "select sum(" & strFldName & ") " & strTblName
                    'FindTotals = "select sum(" & strFldName & ") " & strTblName
                End If
                'If the given parameter is Avg Then
                If (strType = "Avg") Then
                    strQuery = "select avg(" & strFldName & ") " & strTblName
                End If
                'If the given parameter is Max Then
                If (strType = "Max") Then
                    strQuery = "select max(" & strFldName & ") " & strTblName
                End If
                'If the given parameter is Min Then
                If (strType = "Min") Then
                    strQuery = "select min(" & strFldName & ") " & strTblName
                End If

                Try
                    fn_GetConnectionString = GetConnection.GetConnectionString
                    Dim cnnReport As New Npgsql.NpgSqlConnection(fn_GetConnectionString)
                    cnnReport.Open() ' Open the Connection
                    Dim cmdReport As New Npgsql.NpgsqlCommand(strQuery, cnnReport) 'Command Object
                    'cmdReport.CommandType = CommandType.Text
                    'cmdReport.CommandText = strQuery
                    FindTotals = CStr(cmdReport.ExecuteScalar)
                    cnnReport.Close() ' Close the Connection
                    cnnReport = Nothing ' Dispose the object
                Catch ex As Exception
                    Throw New Exception("Error occurs while fetching the  record", ex)
                End Try
            Else
                Exit Function
            End If
        End Function

        Public Function GetHeaderForPredefinedRept() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(0).Value = _ModuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(1).Value = _GroupID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAllSelectedSystemModulesPagesAndAccessesReport", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function


        Public Function GetPredefinedReptlinks() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@rpTGroup", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(0).Value = _GroupID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_PreDefinedReptLinks", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetCustReportKPI(ByVal numCustomReportId As Integer) As DataSet
            Try

                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numCustomReportId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(0).Value = numCustomReportId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCustReportKPI", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function


    End Class
End Namespace

