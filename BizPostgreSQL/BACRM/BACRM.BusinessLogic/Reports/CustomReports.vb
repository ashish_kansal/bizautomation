
Option Explicit On
Imports System.Web
Imports System.IO
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Reports
    Public Class CustomReports
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _strReportIds As String
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        'Private UserCntID As Long
        Private _DrillDown As Boolean
        Private _SortRecCount As Boolean

        Public Property SortRecCount() As Boolean
            Get
                Return _SortRecCount
            End Get
            Set(ByVal Value As Boolean)
                _SortRecCount = Value
            End Set
        End Property
        Public Property DrillDown() As Boolean
            Get
                Return _DrillDown
            End Get
            Set(ByVal Value As Boolean)
                _DrillDown = Value
            End Set
        End Property

        Public Property strReportIds() As String
            Get
                Return _strReportIds
            End Get
            Set(ByVal Value As String)
                _strReportIds = Value
            End Set
        End Property
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property
        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property
        Private _numCustModuleId As Long
        Public Property numCustModuleId() As Long
            Get
                Return _numCustModuleId
            End Get
            Set(ByVal Value As Long)
                _numCustModuleId = Value
            End Set
        End Property

        Private _FilterRel As Boolean
        Public Property FilterRel() As Boolean
            Get
                Return _FilterRel
            End Get
            Set(ByVal Value As Boolean)
                _FilterRel = Value
            End Set
        End Property
        Private _FilterType As Integer
        Public Property FilterType() As Integer
            Get
                Return _FilterType
            End Get
            Set(ByVal Value As Integer)
                _FilterType = Value
            End Set
        End Property
        Private _AdvFilterStr As String
        Public Property AdvFilterStr() As String
            Get
                Return _AdvFilterStr
            End Get
            Set(ByVal Value As String)
                _AdvFilterStr = Value
            End Set
        End Property
        Private _GridType As Boolean
        Public Property GridType() As Boolean
            Get
                Return _GridType
            End Get
            Set(ByVal Value As Boolean)
                _GridType = Value
            End Set
        End Property
        Private _numReportOptTypeId As Long
        Public Property numReportOptTypeId() As Long
            Get
                Return _numReportOptTypeId
            End Get
            Set(ByVal Value As Long)
                _numReportOptTypeId = Value
            End Set
        End Property
        Private _numContactType As Long
        Public Property numContactType() As Long
            Get
                Return _numContactType
            End Get
            Set(ByVal Value As Long)
                _numContactType = Value
            End Set
        End Property
        Private _numOrgRel As Long
        Public Property numOrgRel() As Long
            Get
                Return _numOrgRel
            End Get
            Set(ByVal Value As Long)
                _numOrgRel = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/27/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the ReportID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Report Id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/27/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ReportID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Refresh Interval for the Custom Global Temp Tables.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Report Id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _RefreshInterval As Long

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Path to the XML file.
        ''' </summary>
        ''' <remarks>
        '''     This holds the path.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/03/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _sXMLFilePath As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets the XML file path.
        ''' </summary>
        ''' <value>Returns the path to the xml file as a string.</value>
        ''' <remarks>
        '''     This property is used to Get the path to the xml file which stores the column config/ group. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/03/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        ''' 
        Private _tintType As Integer
        Private _GroupId As Integer = 0
        Private _DivisionID As Integer
        Private _ContactID As Integer
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property
        Public Property GroupId() As Long
            Get
                Return _GroupId
            End Get
            Set(ByVal Value As Long)
                _GroupId = Value
            End Set
        End Property
        Public Property XMLFilePath() As String
            Get
                Return _sXMLFilePath
            End Get
            Set(ByVal Value As String)
                _sXMLFilePath = Value
            End Set
        End Property
        Private _MttId As Long
        Public Property MttId() As Long
            Get
                Return _MttId
            End Get
            Set(ByVal Value As Long)
                _MttId = Value
            End Set
        End Property
        Private _MttValue As String
        Public Property MttValue() As String
            Get
                Return _MttValue
            End Get
            Set(ByVal Value As String)
                _MttValue = Value
            End Set
        End Property
        Private _TerritoryID As Long
        Private _TableName As String
        Private _LocationID As Long
        Private _DynamicQuery As String
        Private _FileName As String
        Private _FileDescription As String
        Private _textQuery As String
        Private _Sum As String
        Private _Average As String
        Private _Large As String
        Private _Small As String

        Private _ReportName As String
        Private _strCheckedFields As String
        Private _strIsChecked As String
        Private _strOrderFieldsValue As String
        Private _strOrderFieldsText As String
        Private _ReportDesc As String
        Public Property ReportDesc() As String
            Get
                Return _ReportDesc
            End Get
            Set(ByVal Value As String)
                _ReportDesc = Value
            End Set
        End Property
        Private _OrderbyFld As String
        Public Property OrderbyFld() As String
            Get
                Return _OrderbyFld
            End Get
            Set(ByVal Value As String)
                _OrderbyFld = Value
            End Set
        End Property
        Private _Order As String
        Public Property Order() As String
            Get
                Return _Order
            End Get
            Set(ByVal Value As String)
                _Order = Value
            End Set
        End Property
        Public Property ReportName() As String
            Get
                Return _ReportName
            End Get
            Set(ByVal Value As String)
                _ReportName = Value
            End Set
        End Property
        Public Property strCheckedFields() As String
            Get
                Return _strCheckedFields
            End Get
            Set(ByVal Value As String)
                _strCheckedFields = Value
            End Set
        End Property
        Public Property strIsChecked() As String
            Get
                Return _strIsChecked
            End Get
            Set(ByVal Value As String)
                _strIsChecked = Value
            End Set
        End Property
        Public Property strOrderFieldsValue() As String
            Get
                Return _strOrderFieldsValue
            End Get
            Set(ByVal Value As String)
                _strOrderFieldsValue = Value
            End Set
        End Property
        Public Property strOrderFieldsText() As String
            Get
                Return _strOrderFieldsText
            End Get
            Set(ByVal Value As String)
                _strOrderFieldsText = Value
            End Set
        End Property
        Private _strGrpfld As String
        Private _strGrpOrd As String
        Private _strGrpflt As String
        Public Property strGrpfld() As String
            Get
                Return _strGrpfld
            End Get
            Set(ByVal Value As String)
                _strGrpfld = Value
            End Set
        End Property
        Public Property strGrpOrd() As String
            Get
                Return _strGrpOrd
            End Get
            Set(ByVal Value As String)
                _strGrpOrd = Value
            End Set
        End Property
        Public Property strGrpflt() As String
            Get
                Return _strGrpflt
            End Get
            Set(ByVal Value As String)
                _strGrpflt = Value
            End Set
        End Property

        Private _strFilterFieldsText As String
        Private _strFilterFieldsValue As String
        Private _strFilterFieldsOperator As String
        Private _strFilterValue As String

        Public Property strFilterFieldsText() As String
            Get
                Return _strFilterFieldsText
            End Get
            Set(ByVal Value As String)
                _strFilterFieldsText = Value
            End Set
        End Property
        Public Property strFilterFieldsValue() As String
            Get
                Return _strFilterFieldsValue
            End Get
            Set(ByVal Value As String)
                _strFilterFieldsValue = Value
            End Set
        End Property
        Public Property strFilterFieldsOperator() As String
            Get
                Return _strFilterFieldsOperator
            End Get
            Set(ByVal Value As String)
                _strFilterFieldsOperator = Value
            End Set
        End Property
        Public Property strFilterValue() As String
            Get
                Return _strFilterValue
            End Get
            Set(ByVal Value As String)
                _strFilterValue = Value
            End Set
        End Property
        Private _strsumFieldsText As String
        Private _strsumrFieldsValue As String
        Private _strsumFieldsOperator As String
        Public Property strsumFieldsText() As String
            Get
                Return _strsumFieldsText
            End Get
            Set(ByVal Value As String)
                _strsumFieldsText = Value
            End Set
        End Property
        Public Property strsumrFieldsValue() As String
            Get
                Return _strsumrFieldsValue
            End Get
            Set(ByVal Value As String)
                _strsumrFieldsValue = Value
            End Set
        End Property
        Public Property strsumFieldsOperator() As String
            Get
                Return _strsumFieldsOperator
            End Get
            Set(ByVal Value As String)
                _strsumFieldsOperator = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	01/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Refresh Interval for the Custom Report Temp Tables.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Refresh Interval. 
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	11/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property RefreshInterval() As Long
            Get
                Return _RefreshInterval
            End Get
            Set(ByVal Value As Long)
                _RefreshInterval = Value
            End Set
        End Property

        Public Property ReportID() As Long
            Get
                Return _ReportID
            End Get
            Set(ByVal Value As Long)
                _ReportID = Value
            End Set
        End Property

        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property

        Public Property TableName() As String
            Get
                Return _TableName
            End Get
            Set(ByVal Value As String)
                _TableName = Value
            End Set
        End Property

        Public Property LocationID() As String
            Get
                Return _LocationID
            End Get
            Set(ByVal Value As String)
                _LocationID = Value
            End Set
        End Property

        Public Property DynamicQuery() As String
            Get
                Return _DynamicQuery
            End Get
            Set(ByVal Value As String)
                _DynamicQuery = Value
            End Set
        End Property
        Private _sqlGrp As String
        Public Property sqlGrp() As String
            Get
                Return _sqlGrp
            End Get
            Set(ByVal Value As String)
                _sqlGrp = Value
            End Set
        End Property

        Public Property FileName() As String
            Get
                Return _FileName
            End Get
            Set(ByVal Value As String)
                _FileName = Value
            End Set
        End Property

        Public Property FileDescription() As String
            Get
                Return _FileDescription
            End Get
            Set(ByVal Value As String)
                _FileDescription = Value
            End Set
        End Property

        Public Property TextQuery() As String
            Get
                Return _textQuery
            End Get
            Set(ByVal Value As String)
                _textQuery = Value
            End Set
        End Property

        Public Property Sum() As String
            Get
                Return _Sum
            End Get
            Set(ByVal Value As String)
                _Sum = Value
            End Set
        End Property

        Public Property Average() As String
            Get
                Return _Average
            End Get
            Set(ByVal Value As String)
                _Average = Value
            End Set
        End Property

        Public Property Large() As String
            Get
                Return _Large
            End Get
            Set(ByVal Value As String)
                _Large = Value
            End Set
        End Property

        Public Property Small() As String
            Get
                Return _Small
            End Get
            Set(ByVal Value As String)
                _Small = Value
            End Set
        End Property
        Public Property tintType() As String
            Get
                Return _tintType
            End Get
            Set(ByVal Value As String)
                _tintType = Value
            End Set
        End Property

        Private _stdFieldId As String
        Public Property stdFieldId() As String
            Get
                Return _stdFieldId
            End Get
            Set(ByVal Value As String)
                _stdFieldId = Value
            End Set
        End Property
        Private _custFilter As String
        Public Property custFilter() As String
            Get
                Return _custFilter
            End Get
            Set(ByVal Value As String)
                _custFilter = Value
            End Set
        End Property
        Private _FromDate As String = ""
        Public Property FromDate() As String
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As String)
                _FromDate = Value
            End Set
        End Property
        Private _ToDate As String = ""
        Public Property ToDate() As String
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As String)
                _ToDate = Value
            End Set
        End Property
        Private _Rows As Long
        Public Property Rows() As Long
            Get
                Return _Rows
            End Get
            Set(ByVal Value As Long)
                _Rows = Value
            End Set
        End Property
        Private _ScheduleId As Long
        Public Property ScheduleId() As Long
            Get
                Return _ScheduleId
            End Get
            Set(ByVal Value As Long)
                _ScheduleId = Value
            End Set
        End Property
        Private _IntervalType As Char = CChar("A")
        Private _IntervalDays As Int32
        Private _MonthlyType As Int32
        Private _FirstDet As Int32
        Private _WeekDays As Int32
        Private _Months As Int32
        Private _EndType As Boolean
        Private _dtStartDate As Date
        Private _dtEndDate As Date = CDate("1/1/1753")
        Private _ScheduleName As String = String.Empty
        Private _EndTransactionType As Boolean
        Private _NoTransaction As Integer


        Public Property NoTransaction() As Integer
            Get
                Return _NoTransaction
            End Get
            Set(ByVal value As Integer)
                _NoTransaction = value
            End Set
        End Property
        Public Property EndTransactionType() As Boolean
            Get
                Return _EndTransactionType
            End Get
            Set(ByVal value As Boolean)
                _EndTransactionType = value
            End Set
        End Property
        Public Property ScheduleName() As String
            Get
                Return _ScheduleName
            End Get
            Set(ByVal Value As String)
                _ScheduleName = Value
            End Set
        End Property
        Public Property IntervalType() As Char
            Get
                Return _IntervalType
            End Get
            Set(ByVal Value As Char)
                _IntervalType = Value
            End Set
        End Property

        Public Property IntervalDays() As Int32
            Get
                Return _IntervalDays
            End Get
            Set(ByVal Value As Int32)
                _IntervalDays = Value
            End Set
        End Property

        Public Property MonthlyType() As Int32
            Get
                Return _MonthlyType
            End Get
            Set(ByVal Value As Int32)
                _MonthlyType = Value
            End Set
        End Property

        Public Property FirstDet() As Int32
            Get
                Return _FirstDet
            End Get
            Set(ByVal Value As Int32)
                _FirstDet = Value
            End Set
        End Property

        Public Property WeekDays() As Int32
            Get
                Return _WeekDays
            End Get
            Set(ByVal Value As Int32)
                _WeekDays = Value
            End Set
        End Property

        Public Property EndType() As Boolean
            Get
                Return _EndType
            End Get
            Set(ByVal Value As Boolean)
                _EndType = Value
            End Set
        End Property

        Public Property Months() As Int32
            Get
                Return _Months
            End Get
            Set(ByVal Value As Int32)
                _Months = Value
            End Set
        End Property

        Public Property dtStartDate() As Date
            Get
                Return _dtStartDate
            End Get
            Set(ByVal value As Date)
                _dtStartDate = value
            End Set
        End Property

        Public Property dtEndDate() As Date
            Get
                Return _dtEndDate
            End Get
            Set(ByVal value As Date)
                _dtEndDate = value
            End Set
        End Property
        Private _CompFilter1 As String
        Public Property CompFilter1() As String
            Get
                Return _CompFilter1
            End Get
            Set(ByVal Value As String)
                _CompFilter1 = Value
            End Set
        End Property
        Private _CompFilter2 As String
        Public Property CompFilter2() As String
            Get
                Return _CompFilter2
            End Get
            Set(ByVal Value As String)
                _CompFilter2 = Value
            End Set
        End Property
        Private _CompFilterOpr As String
        Public Property CompFilterOpr() As String
            Get
                Return _CompFilterOpr
            End Get
            Set(ByVal Value As String)
                _CompFilterOpr = Value
            End Set
        End Property
        Private _ClientTimeZoneOffset As Int32
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the Private Property representing the Local (Client) Time Zone Offset from GMT .
        ''' </summary>
        ''' <remarks>
        '''     
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Function DeleteReport()
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter

                arParms = New Npgsql.NpgsqlParameter("@numCustomReportID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms.Value = _ReportID


                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteCustomReport", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetSavedCustomReports() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _TerritoryID


                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSavedReports", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function PopulatedFields() As DictionaryEntry()
            Dim arrDEList() As DictionaryEntry
            Dim intCount As Integer = 0
            Dim inti As Integer = 0
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim drdrReport As Npgsql.NpgsqlDataReader

                Dim strQuery As String = "select * from " & _TableName


                drdrReport = SqlDAL.ExecuteReader(connString, CommandType.Text, strQuery)
                For inti = 0 To drdrReport.FieldCount - 1
                    ReDim Preserve arrDEList(intCount)
                    If (drdrReport.GetName(inti) <> "DomainID" And drdrReport.GetName(inti) <> "CreatedBy") Then
                        arrDEList(intCount) = New DictionaryEntry(drdrReport.GetName(inti), drdrReport.GetName(inti))
                        intCount += 1
                    End If
                Next inti

                drdrReport.Close()
                If (arrDEList Is Nothing) Then
                    Return Nothing
                End If
                Return arrDEList


            Catch ex As Exception
                Throw New Exception("Error occurs while fetching the  record", ex)
            End Try

        End Function
        Public Function GetGroupCount() As String
            Try
                Dim ds As String
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter

                arParms = New Npgsql.NpgsqlParameter("@numLocID", NpgsqlTypes.NpgsqlDbType.VarChar.BigInt, 9)
                arParms.Value = _LocationID


                ds = SqlDAL.ExecuteScalar(connString, "usp_GetGroupCount", arParms)
                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetCustomFields() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                'Dim arParms As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter

                'arParms = New Npgsql.NpgsqlParameter("@vcDynamicQuery", NpgsqlTypes.NpgsqlDbType.VarChar.VarChar, 2000)
                'arParms.Value = _DynamicQuery


                ds = SqlDAL.ExecuteDataset(connString, CommandType.Text, _DynamicQuery)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetCustomSavedReports() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCustomReportID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCustomSavedReports", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function InsertCustomReport()
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcFileName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = _FileName

                arParms(1) = New Npgsql.NpgsqlParameter("@vcFileDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(1).Value = _FileDescription

                arParms(2) = New Npgsql.NpgsqlParameter("@textQuery", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _textQuery

                arParms(3) = New Npgsql.NpgsqlParameter("@vcSum", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _Sum

                arParms(4) = New Npgsql.NpgsqlParameter("@vcAvg", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _Average

                arParms(5) = New Npgsql.NpgsqlParameter("@vcLarge", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _Large

                arParms(6) = New Npgsql.NpgsqlParameter("@vcSmall", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _Small

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(8).Value = DomainId

                ds = SqlDAL.ExecuteDataset(connString, "usp_InsertCustomReport", arParms)


            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetHeadersForCustomReport() As DictionaryEntry()
            Dim arrDEList() As DictionaryEntry
            Dim intCount As Integer = 0
            Try
                Dim drdrReport As Npgsql.NpgsqlDataReader
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim intRowCount As Integer
                drdrReport = SqlDAL.ExecuteReader(connString, CommandType.Text, _textQuery)
                'Return ds.Tables(0)


                For intRowCount = 0 To drdrReport.FieldCount - 1
                    ReDim Preserve arrDEList(intCount)
                    If drdrReport.FieldCount > 0 Then

                        arrDEList(intCount) = New DictionaryEntry(intRowCount, drdrReport.GetName(intRowCount))
                        intCount += 1
                    End If
                Next


                drdrReport.Close()
                If (arrDEList Is Nothing) Then
                    Return Nothing
                End If
                Return arrDEList
            Catch ex As Exception
                Throw New Exception("Error occurs while fetching the  record", ex)
            End Try

        End Function


        Public Function GetColumnValuesForCustomReport() As DataTable

            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim intRowCount As Integer
                ds = SqlDAL.ExecuteDataset(connString, CommandType.Text, _textQuery)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw New Exception("Error occurs while fetching the  record", ex)
            End Try

        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for all relationships for the dropdown.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of list details for Relationships
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/27/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getRelationshipsDetails(ByVal iListItemId As Integer) As DataTable
            Try
                Dim ds As DataSet                                                       'declare a dataset
                Dim getconnection As New GetConnection                                  'declare a connection
                Dim connString As String = getconnection.GetConnectionString            'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                  'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = iListItemId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcItemType", NpgsqlTypes.NpgsqlDbType.Char, 3)
                arParms(1).Value = "LI"                                                  'representing listmaster/ listdetails

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetMasterListDetails", arParms) 'execute and store to dataset

                Return ds.Tables(0)                                                     'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the columns to be grouped in the drop down.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the 
        '''     list of columns for the drop down
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        ''' 
        Public Function getColumnsForCustomReport(ByVal numReportOptionsTypeId As Integer) As DataTable
            Try
                Dim ds As DataSet                                                       'declare a dataset
                Dim getconnection As New GetConnection                                  'declare a connection
                Dim connString As String = getconnection.GetConnectionString            'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}                  'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numReportOptionsTypeId", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = numReportOptTypeId

                arParms(1) = New Npgsql.NpgsqlParameter("@DomainId", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactTypeId", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = numContactType

                arParms(3) = New Npgsql.NpgsqlParameter("@numRelationTypeId", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(3).Value = numOrgRel

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCustRptDefaultFields", arParms) 'execute and store to dataset

                Return ds.Tables(0)                                                     'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function getColumnsForCustomReport() As DataTable
            Try
                Dim ds As DataSet                                                       'declare a dataset
                Dim getconnection As New GetConnection                                  'declare a connection
                Dim connString As String = getconnection.GetConnectionString            'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}                  'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numReportOptionsTypeId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numReportOptTypeId

                arParms(1) = New Npgsql.NpgsqlParameter("@DomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactTypeId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = numContactType

                arParms(3) = New Npgsql.NpgsqlParameter("@numRelationTypeId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = numOrgRel

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCustRptDefaultFields", arParms) 'execute and store to dataset

                Return ds.Tables(0)                                                     'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the contact and Org custom columns to be grouped in the drop down.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the 
        '''     list of custom columns for the drop down
        ''' </remarks>
        ''' <param name="numContactTypeId">The Contact Type</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getCustomColumnsForOrgContactCustomReport(ByVal numRelationTypeId As Integer, ByVal numContactTypeId As Integer) As DataTable
            Try
                Dim ds As DataSet                                                       'declare a dataset
                Dim getconnection As New GetConnection                                  'declare a connection
                Dim connString As String = getconnection.GetConnectionString            'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                  'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactTypeId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numContactTypeId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRelationTypeId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = numRelationTypeId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCustRptOrgContactCustomFields", arParms) 'execute and store to dataset

                Return ds.Tables(0)                                                     'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the opportunity and items custom columns to be grouped in the drop down.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the 
        '''     list of custom columns for the drop down
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getCustomColumnsForOppItemCustomReport() As DataTable
            Try
                Dim ds As DataSet                                                       'declare a dataset
                Dim getconnection As New GetConnection                                  'declare a connection
                Dim connString As String = getconnection.GetConnectionString            'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                  'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCustRptOppItemCustomFields", arParms) 'execute and store to dataset

                Return ds.Tables(0)                                                     'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function retrieves the selected list of teams.
        ''' </summary>
        ''' <returns>The Table containing the Selected Teams for Custom Reports</returns>
        ''' <remarks>
        '''     This function calls the stored procedure that retrieves the list of teams selected by the user
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/04/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSelectedTeamsForUser() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcTerritoryTeamFlag", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = "Teams"

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getAdvSearchTeamTerritoryPreferences", arParms)     'execute and store to dataset
                ds.Tables(0).TableName = "SelectedTeamUsers"
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function retrieves the selected list of territories.
        ''' </summary>
        ''' <returns>The Table containing the Selected Territories for Custom Reports</returns>
        ''' <remarks>
        '''     This function calls the stored procedure that retrieves the list of territories selected by the user
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/04/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSelectedTerritoriesForUser() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcTerritoryTeamFlag", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = "Terri"

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getAdvSearchTeamTerritoryPreferences", arParms)     'execute and store to dataset
                ds.Tables(0).TableName = "SelectedTerritoriesUsers"
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the custom report config to an xml file
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <param name="CustomReportConfig">The String containing the custom report configuration</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/04/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function saveCustomReportConfig(ByVal sCustomReportConfig As String)
            Dim StrWriter As StreamWriter                                                       'Declare a StreamWriter object
            Try
                If Directory.Exists(XMLFilePath) = False Then                                   'If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(XMLFilePath)
                End If
                XMLFilePath = XMLFilePath & "\" & "CustomReportConfig_" & UserCntID & ".xml"       'Set the file name

                StrWriter = File.CreateText(XMLFilePath)                                        'create the flle
                StrWriter.Write(sCustomReportConfig)                                             'Write XML doument
                StrWriter.Close()                                                               'close the object
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the Custom Report Config from its temporary repository in XML file
        ''' </summary>
        ''' <remarks>
        '''     Returns the Config Information as a DataSet
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/04/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetCustomReportConfigFromXMLfile() As DataSet
            Try
                Dim dsConfigInformation As New DataSet                                                  'Create an instance of a dataset
                dsConfigInformation.ReadXml(XMLFilePath & "\CustomReportConfig_" & UserCntID & ".xml") 'Read DataSet into XML
                Return dsConfigInformation
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the Custom Report
        ''' </summary>
        ''' <param name="dsCustomReportConfig">The dataset containing the custom report configuration</param>
        ''' <remarks>
        '''     Returns the Config Information as a Set
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/04/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetCustomReportResultSet(ByVal dsCustomReportConfig As DataSet) As DataSet
            Try
                Dim dsCustomReportResultSet As DataSet                                          'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@vcCustomReportConfig", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = dsCustomReportConfig.GetXml()

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRefreshTimeInterval", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = RefreshInterval

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = tintType

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                dsCustomReportResultSet = SqlDAL.ExecuteDataset(connString, "usp_getCustomReportResultSet", arParms)     'execute and store to dataset
                Return dsCustomReportResultSet
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the Custom Report
        ''' </summary>
        ''' <param name="numCustReportID">The id of the Custom Report</param>
        ''' <remarks>
        '''     Returns the Custom Report Resultset as a DataSet
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/09/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetCustomReportResultSet(ByVal numCustReportID As Integer) As DataSet
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numCustReportID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = numCustReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRefreshTimeInterval", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = RefreshInterval

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_getCustomReportResultSetForMyReport", arParms)     'execute and store to dataset
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete the XML file containing the Report Config
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/05/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function DeleteCustomReportConfigXMLfile()
            Try
                XMLFilePath = XMLFilePath & "\CustomReportConfig_" & UserCntID & ".xml"             'Set the path to teh XML file
                If File.Exists(XMLFilePath) Then                                                        'Check if the file exists
                    File.Delete(XMLFilePath)                                                            'Delete the file
                End If
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to Save the Custom Report
        ''' </summary>
        ''' <param name="sCustomReportConfig">The String containing the custom report configuration</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveCustomReportAsMyReport(ByVal sCustomReportConfig As String)
            Try
                Dim dsCustomReportResultSet As DataSet                                          'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@vcCustomReportConfig", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = sCustomReportConfig

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                dsCustomReportResultSet = SqlDAL.ExecuteDataset(connString, "usp_getSaveCustomReport", arParms)     'execute and store to dataset
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the saved Custom Reports for My Reports.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the table of saved Custom Reports
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetCustomMyReportList() As DataTable
            Try
                Dim dsCustomReports As DataSet                                                  'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                dsCustomReports = SqlDAL.ExecuteDataset(connString, "usp_getCustomReports4MyReports", arParms)     'execute and store to dataset
                Return dsCustomReports.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to recreate the gloabl temporatry tables which are used for Custom Reports.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure and regenerates the tables for Custom Reports
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function RecreateGlobalCustomReportTables()
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numRefreshTimeInterval", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = RefreshInterval

                SqlDAL.ExecuteNonQuery(connString, "usp_RecreateGlobalCustomReportTables", arParms)      'execute the stored procedure
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to drop a custom Report.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure and regenerates the tables for Custom Reports
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function DeleteCustomReport(ByVal numCustReportID As Integer)
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numCustReportID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numCustReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainId

                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteCustomReport", arParms)           'execute the stored procedure to delete a custom report
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetModules() As DataSet
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@numcustModuleId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numCustModuleId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetCustomReportModules", arParms)           'execute the stored procedure to delete a custom report
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ExecuteDynamicSql() As DataSet
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@sql", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = DynamicQuery

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainId


                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _ClientTimeZoneOffset


                Return SqlDAL.ExecuteDataset(connString, "uspp_ExecteDynamicQuery", arParms)           'execute the stored procedure to delete a custom report
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SaveCustomReport() As Long
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(30) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@ReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@ReportName", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(1).Value = ReportName

                arParms(2) = New Npgsql.NpgsqlParameter("@ReportDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(2).Value = ReportDesc

                arParms(3) = New Npgsql.NpgsqlParameter("@sql", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = DynamicQuery

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@ModuleId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = numCustModuleId


                arParms(7) = New Npgsql.NpgsqlParameter("@GroupId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = GroupId

                arParms(8) = New Npgsql.NpgsqlParameter("@FilterRel", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = FilterRel

                arParms(9) = New Npgsql.NpgsqlParameter("@stdFieldId", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(9).Value = stdFieldId

                arParms(10) = New Npgsql.NpgsqlParameter("@custFilter", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(10).Value = custFilter

                arParms(11) = New Npgsql.NpgsqlParameter("@FromDate", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(11).Value = FromDate

                arParms(12) = New Npgsql.NpgsqlParameter("@ReportDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(12).Value = ToDate

                arParms(13) = New Npgsql.NpgsqlParameter("@Rows", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(13).Value = Rows

                arParms(14) = New Npgsql.NpgsqlParameter("@GridType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = GridType

                arParms(15) = New Npgsql.NpgsqlParameter("@strGrpfld", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(15).Value = strGrpfld

                arParms(16) = New Npgsql.NpgsqlParameter("@strGrpOrd", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(16).Value = strGrpOrd

                arParms(17) = New Npgsql.NpgsqlParameter("@strGrpflt", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(17).Value = strGrpflt

                arParms(18) = New Npgsql.NpgsqlParameter("@sqlGrp", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(18).Value = sqlGrp

                arParms(19) = New Npgsql.NpgsqlParameter("@FilterType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(19).Value = FilterType

                arParms(20) = New Npgsql.NpgsqlParameter("@AdvFilterStr", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(20).Value = AdvFilterStr

                arParms(21) = New Npgsql.NpgsqlParameter("@OrderbyFld", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(21).Value = OrderbyFld

                arParms(22) = New Npgsql.NpgsqlParameter("@Orderf", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(22).Value = Order

                arParms(23) = New Npgsql.NpgsqlParameter("@MttId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(23).Value = MttId

                arParms(24) = New Npgsql.NpgsqlParameter("@MttValue", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(24).Value = MttValue

                arParms(25) = New Npgsql.NpgsqlParameter("@CompFilter1", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(25).Value = CompFilter1

                arParms(26) = New Npgsql.NpgsqlParameter("@CompFilter2", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(26).Value = CompFilter2

                arParms(27) = New Npgsql.NpgsqlParameter("@CompFilterOpr", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(27).Value = CompFilterOpr

                arParms(28) = New Npgsql.NpgsqlParameter("@bitDrillDown", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(28).Value = _DrillDown

                arParms(29) = New Npgsql.NpgsqlParameter("@bitSortRecCount", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(29).Value = _SortRecCount

                arParms(30) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(30).Value = Nothing
                arParms(30).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "usp_SaveCustomReport", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub SaveCustomReportCheckedFields()
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@ReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@strCheckedFields", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = strCheckedFields

                SqlDAL.ExecuteNonQuery(connString, "usp_SaveCustomReportChkFields", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub SaveCustomReportOrderList()
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@ReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@strOrderList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = strOrderFieldsText

                SqlDAL.ExecuteNonQuery(connString, "usp_SaveCustomReportOrderList", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub SaveCustomReportFilterList()
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@ReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@strFilterList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = strOrderFieldsText

                SqlDAL.ExecuteNonQuery(connString, "usp_SaveCustomReportFilterList", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub SaveCustomReportSumList()
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@ReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@strSummationList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = strOrderFieldsText

                SqlDAL.ExecuteNonQuery(connString, "usp_SaveCustomReportSummationList", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Function getReportDetails() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@ReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_getCustReportDetails", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function getCheckedFields() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@ReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_getCustReportCheckedFields", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function getReportOrderList() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@ReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_getCustReportOrderlist", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function getReportFilterList() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@ReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_getCustReportFilterlist", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function getReportSummationList() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@ReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_getCustReportsummationlist", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetSchedulerDtl() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numScheduleId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ScheduleId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_SchedulerDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SaveScheduler() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(15) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numScheduleId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ScheduleId

                arParms(1) = New Npgsql.NpgsqlParameter("@varScheduleName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = _ScheduleName

                arParms(2) = New Npgsql.NpgsqlParameter("@chrIntervalType", NpgsqlTypes.NpgsqlDbType.Char)
                arParms(2).Value = _IntervalType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintIntervalDays", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _IntervalDays

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMonthlyType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _MonthlyType

                arParms(5) = New Npgsql.NpgsqlParameter("@tintFirstDet", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _FirstDet

                arParms(6) = New Npgsql.NpgsqlParameter("@tintWeekDays", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _WeekDays


                arParms(7) = New Npgsql.NpgsqlParameter("@tintMonths", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _Months


                arParms(8) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(8).Value = _dtStartDate

                arParms(9) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(9).Value = _dtEndDate

                arParms(10) = New Npgsql.NpgsqlParameter("@bitEndType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _EndType

                arParms(11) = New Npgsql.NpgsqlParameter("@bitEndTransactionType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _EndTransactionType

                arParms(12) = New Npgsql.NpgsqlParameter("@numtNoTransaction", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(12).Value = _NoTransaction

                arParms(13) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = DomainId

                arParms(14) = New Npgsql.NpgsqlParameter("@numReportID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _ReportID

                arParms(15) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(15).Value = Nothing
                arParms(15).Direction = ParameterDirection.InputOutput

                Dim reportid As Long
                reportid = SqlDAL.ExecuteScalar(connString, "USP_InsertScheduler", arParms)
                Return reportid
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function getCustomReportGridType() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@bitGridType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _GridType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_getCustomReportGridType", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function getCustomReportUser() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _GroupId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_getCustomReportUser", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function getSchedulerListRpt() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numReportID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_getCustomSchedulerRepList", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetContactInfo() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numScheduleId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ScheduleId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetContactsScheduler", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetContactScheduled() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numScheduleId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ScheduleId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetScheduledContacts", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Sub AddContactScheduled()
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}



                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numScheduleId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ScheduleId


                SqlDAL.ExecuteNonQuery(connString, "Usp_AddContactScheduled", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Sub
        Public Sub DelContactScheduled()
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}



                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numScheduleId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ScheduleId

                SqlDAL.ExecuteNonQuery(connString, "Usp_DelContactScheduled", arParms)


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Sub
        Public Sub DelSchedule()
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}



                arParms(0) = New Npgsql.NpgsqlParameter("@numScheduleId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ScheduleId

                SqlDAL.ExecuteNonQuery(connString, "Usp_DelScheduled", arParms)


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Sub
        Public Function getFieldName() As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strGrpfld", NpgsqlTypes.NpgsqlDbType.Varchar, 200)
                arParms(0).Value = _strGrpfld

                arParms(1) = New Npgsql.NpgsqlParameter("@GroupId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _GroupId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "Usp_getFieldName", arParms)
                Return ds.Tables(0).Rows(0).Item("vcScrFieldName")

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function getReportList() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _SortCharacter

                arParms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _TotalRecords

                arParms(6) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _columnName

                arParms(7) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(7).Value = _columnSortOrder

                arParms(8) = New Npgsql.NpgsqlParameter("@numModuleId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(8).Value = _numCustModuleId

                arParms(9) = New Npgsql.NpgsqlParameter("@SearchStr", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(9).Value = _FileDescription

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "Usp_getCustReportList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function getReportListAll() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _GroupId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "Usp_getManageCustReportList", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Sub saveAllowedReports()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}



                arParms(0) = New Npgsql.NpgsqlParameter("@strReportIds", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(0).Value = _strReportIds

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _GroupId

                SqlDAL.ExecuteNonQuery(connString, "Usp_saveCustAllowedDashBoardReports", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub AddToMyReports()
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}



                arParms(0) = New Npgsql.NpgsqlParameter("@numReportId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ReportID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                SqlDAL.ExecuteNonQuery(connString, "Usp_CustAddToMyReport", arParms)


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Sub
        Function getCustomReportTest() As DataSet
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array               

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_CustomReportTest", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace