''' -----------------------------------------------------------------------------
''' Project	 : BACRM.BusinessLogic
''' Class	 : ImplementLeadsAutoRoutingRules
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This class implements the Auto Routing Rules. It gets and owner based on rule parameter
'''     and then creates a lead
''' </summary>
''' <remarks>
'''     Generic class to implement an auto routing rule.
''' </remarks>
''' <history>
''' 	[Debasish Tapan Nag]	08/08/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Option Explicit On 
Imports System.Data.SqlClient
Imports System.IO
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Admin
    Public Class ImplementLeadsAutoRoutingRules
        Inherits BACRM.BusinessLogic.CBusinessBase
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the arraylist of RuleColumnAndValue objects, each having a name and a value pairs.
        ''' </summary>
        ''' <remarks>
        '''     This holds the conditions which has to be matched with the Auto Rules.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public arrRuleColumnAndValueArrayList As ArrayList
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Each RuleColumnAndValue object represents a field and value condition to be matched with the Auto Rules.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Name of the field and its value which has to be matched with the Auto Rules 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Class RuleColumnAndValue
            Private _name As String
            Private _value As String
            Private _NameValueString As String
            Public Sub New(ByVal Name As String, ByVal Value As String)
                _name = Name
                If Name.IndexOf("vcInterested", 0) > -1 Then
                    _value = "'" & Value & "'"
                Else
                    _value = "'" & Replace(Value, " ", "','") & "'"
                End If
                _NameValueString = Replace(Replace("'" & Replace(Replace(_value, ",'", " OR " & _name & " LIKE ($^#~!@@!~#^$)%"), "'", "%($^#~!@@!~#^$)", 2), "'", _name & " LIKE ($^#~!@@!~#^$)%"), "($^#~!@@!~#^$)", "'")
            End Sub
            Public ReadOnly Property Name() As String
                Get
                    Return _name
                End Get
            End Property
            Public ReadOnly Property Value() As String
                Get
                    Return _value
                End Get
            End Property
            Public Property NameValueFilterString() As String
                Get
                    Return _NameValueString
                End Get
                Set(ByVal Value As String)
                    _NameValueString = "'" & Value & "'"
                End Set
            End Property
        End Class

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function implements all the steps for a Routing Rule
        ''' </summary>
        ''' <remarks>
        '''     This function calls a series of steps to implement a auto routing rules
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function ImplementRoutingRule() As Integer
            Dim numRecOwner As Integer                                                          'Declare the RecordOwner variable
            Try
                Dim tbTabbedRoutingRules As DataTable                                           'Create a datatable
                tbTabbedRoutingRules = FetchTabbedRoutingRules()                                'Call to fetch the Auto Rules as Tabbed Data Table
                Dim dvTabbedRoutingRules As DataView                                            'Create a dataview
                dvTabbedRoutingRules = tbTabbedRoutingRules.DefaultView                         'Create the Dataview's data
                Dim numRoutId, iIndex As Integer                                               'The Rule Id, rowIndex and the Record Owner variable
                Dim sPriority As Short = 0
                For iIndex = 0 To arrRuleColumnAndValueArrayList.Count - 1
                    If tbTabbedRoutingRules.Columns.Contains(CType(arrRuleColumnAndValueArrayList.Item(iIndex), RuleColumnAndValue).Name) Then 'Check if the column exists
                        If Trim(CType(arrRuleColumnAndValueArrayList.Item(iIndex), RuleColumnAndValue).Value) <> "''" Then 'Blank values does not conform to auto rule searches
                            dvTabbedRoutingRules.RowFilter = Trim(CType(arrRuleColumnAndValueArrayList.Item(iIndex), RuleColumnAndValue).Name) & " IS NOT NULL and (" & CType(arrRuleColumnAndValueArrayList.Item(iIndex), RuleColumnAndValue).NameValueFilterString & ")" 'Set the Row filter
                            If dvTabbedRoutingRules.Count > 0 Then
                                If sPriority = 0 Then
                                    numRoutId = dvTabbedRoutingRules.Item(0).Item("numRoutId")
                                    sPriority = dvTabbedRoutingRules.Item(0).Item("tintPriority")
                                Else
                                    If sPriority > dvTabbedRoutingRules.Item(0).Item("tintPriority") Then
                                        numRoutId = dvTabbedRoutingRules.Item(0).Item("numRoutId")
                                        sPriority = dvTabbedRoutingRules.Item(0).Item("tintPriority")
                                    End If
                                End If
                                'Get the Rule Id if it matches the criteria
                                ' Exit For
                            Else
                                Dim str As String()
                                str = searchReverseWithinData(tbTabbedRoutingRules, CType(arrRuleColumnAndValueArrayList.Item(iIndex), RuleColumnAndValue).Name, CType(arrRuleColumnAndValueArrayList.Item(iIndex), RuleColumnAndValue).Value).Split("~") 'Get the Rule Id if it matches the criteria
                                If str.Length = 2 Then
                                    If sPriority = 0 Then
                                        numRoutId = str(0)
                                        sPriority = str(1)
                                    Else
                                        If sPriority > str(1) Then
                                            numRoutId = str(0)
                                            sPriority = str(1)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next
                If numRoutId = 0 Then                                                           'Only the default Rule Matches
                    numRoutId = FetchDefaultRuleId()                                            'Get the Default Rule Id
                End If
                If numRoutId <> 0 Then
                    numRecOwner = FetchRecordOwnerForTheRule(numRoutId)                         'Get the Record Owner
                Else
                    numRecOwner = 0                                                             'Default Rule is also not created
                End If
            Catch ex As Exception
                numRecOwner = 0                                                                 'Make Administrator the default record owner
            End Try
            Return numRecOwner
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is checks if the defautl rule is created
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure which returns the default auto routing rule id/ 0
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/13/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function checkForDefaultAutoRule() As Integer
            Return FetchDefaultRuleId()                                                     'Get the Default Rule Id
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the matching Rule id.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure which returns the auto routing rules
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Function FetchTabbedRoutingRules() As DataTable
            Try
                Dim ds As DataSet                                                                   'declare a dataset
                Dim getconnection As New getconnection                                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString                        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                ds = SqlDAL.ExecuteDataset(connString, "usp_crossTabForAutoRuleMatching", arParms)  'execute the stored procedure
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the record owner for a given rule, according to circular distribution rule.
        ''' </summary>
        ''' <param name="numRoutId">Represents the Auto rule Id.</param>
        ''' <remarks>
        '''     Retrieve the Record Owner according to Circular Distribution Rule
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Function FetchRecordOwnerForTheRule(ByVal numRoutId As Integer) As Integer
            Try
                Dim getconnection As New getconnection                                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString                        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numRoutID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numRoutId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "usp_GetLeadOwnerFromCurcularDistList", arParms))  'execute the stored procedure
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the record owner for a given rule, according to circular distribution rule.
        ''' </summary>
        ''' <remarks>
        '''     Retrieve the Record Owner according to Circular Distribution Rule
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function FetchDefaultRuleId() As Integer
            Try
                Dim getconnection As New GetConnection                                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString                        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "usp_GetDefaultRuleId", arParms))                     'execute the stored procedure
            Catch ex As Exception
                Return 0
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to search the array to find any matches (used here for matching auto rules).
        ''' </summary>
        ''' <param name="dtSearchableTable">Represents the table being searched.</param>
        ''' <param name="oSearableColumn">Represents the column within the table which is being searched.</param>
        ''' <param name="oSearableValue">Represents the value being searched within the given column.</param>
        ''' <remarks>
        '''     Matches the values for Auto Rule list and return the Id if a match is found
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/11/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function searchReverseWithinData(ByVal dtSearchableTable As DataTable, ByVal oSearableColumn As String, ByVal oSearableValue As String) As String
            Dim dvSearchableTable As DataView                                                           'Declare a DataView
            dvSearchableTable = dtSearchableTable.DefaultView                                           'Set the defaultview
            dvSearchableTable.RowFilter = oSearableColumn & " IS NOT NULL"                              'filter condition is that the value is not null
            If dvSearchableTable.Count = 0 Then Return 0
            Dim iDataViewCounter As Integer
            Dim oSearableContainer As String
            Dim iContainerLocationIndex, iSearchableLocationIndex As Integer                            'Declare a location variable
            Dim oSearchableContainerArray As Array                                                      'Declare an array
            For iDataViewCounter = 0 To dvSearchableTable.Count - 1
                oSearableContainer = dvSearchableTable.Item(iDataViewCounter).Item(oSearableColumn)
                oSearchableContainerArray = oSearableContainer.Split(",")                               'Split to get an array (if exists)
                For iContainerLocationIndex = 0 To UBound(oSearchableContainerArray)
                    If oSearableValue.IndexOf(oSearchableContainerArray(iContainerLocationIndex)) > 0 Then
                        Return dvSearchableTable.Item(iDataViewCounter).Item("numRoutId") + "~" + dvSearchableTable.Item(iDataViewCounter).Item("tintPriority")           'Match found so return the Auto Rule Id
                    End If
                Next
            Next
            Return 0
        End Function
    End Class
End Namespace
