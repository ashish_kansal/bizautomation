' created by anoop jayaraj

Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Admin

    Public Class ActionItem
        Inherits BACRM.BusinessLogic.CBusinessBase


        'Private DomainId As Long
        Private _CommID As Long
        Private _Task As Long
        Private _ContactID As Long
        Private _CaseID As Long = 0
        Private _CaseTimeId As Long = 0
        Private _CaseExpId As Long = 0
        Private _Details As String
        Private _OppID As Long
        Private _AssignedTo As Long
        Private _BitClosed As Short
        Private _CalendarName As String
        Private _EndTime As Date
        Private _StartTime As Date
        Private _DivisionID As Long
        Private _Activity As Long
        Private _Status As Long
        Private _Snooze As Short
        Private _SnoozeStatus As Short
        Private _Remainder As Short
        Private _RemainderStatus As Short
        Private _Email As String
        Private _Type As Short
        Private _bitOutlook As Short
        'Private UserCntID As Long

        ' Added for action Item template
        Private _RowID As Int32
        Private _TemplateName As String
        Private _DueDays As Int32
        Private _Priority As Int32
        Private _Comments As String
        Private _ActivityActionItem As Int32
        Private _TypeActionItem As String
        Private _StatusActionItem As Int32
        Private _SendEmailTemplate As Boolean
        Private _Alert As Boolean
        Private _EmailTemplate As Long
        Private _Hours As Short
        'added for BizCalendar

        Private _StartDateTime As DateTime
        Public Property StartDateTime() As DateTime
            Get
                Return _StartDateTime
            End Get
            Set(ByVal value As DateTime)
                _StartDateTime = value
            End Set
        End Property

        Private _Duration As Long
        Public Property Duration() As Long
            Get
                Return _Duration
            End Get
            Set(ByVal value As Long)
                _Duration = value
            End Set
        End Property

        Private _CorrenspondenceID As Long
        Public Property CorrespondenceID() As Long
            Get
                Return _CorrenspondenceID
            End Get
            Set(ByVal value As Long)
                _CorrenspondenceID = value
            End Set
        End Property

        Private _EmailHistoryID As Long
        Public Property EmailHistoryID() As Long
            Get
                Return _EmailHistoryID
            End Get
            Set(ByVal value As Long)
                _EmailHistoryID = value
            End Set
        End Property

        Private _CorrType As Short
        Public Property CorrType() As Short
            Get
                Return _CorrType
            End Get
            Set(ByVal value As Short)
                _CorrType = value
            End Set
        End Property


        Private _OpenRecordID As Long
        Public Property OpenRecordID() As Long
            Get
                Return _OpenRecordID
            End Get
            Set(ByVal value As Long)
                _OpenRecordID = value
            End Set
        End Property


        Private _ActivityId As Long
        Public Property ActivityId() As Long
            Get
                Return _ActivityId
            End Get
            Set(ByVal Value As Long)
                _ActivityId = Value
            End Set
        End Property
        Public Property Hours() As Short
            Get
                Return _Hours
            End Get
            Set(ByVal Value As Short)
                _Hours = Value
            End Set
        End Property

        Public Property EmailTemplate() As Long
            Get
                Return _EmailTemplate
            End Get
            Set(ByVal Value As Long)
                _EmailTemplate = Value
            End Set
        End Property

        Public Property Alert() As Boolean
            Get
                Return _Alert
            End Get
            Set(ByVal Value As Boolean)
                _Alert = Value
            End Set
        End Property

        Public Property SendEmailTemplate() As Boolean
            Get
                Return _SendEmailTemplate
            End Get
            Set(ByVal Value As Boolean)
                _SendEmailTemplate = Value
            End Set
        End Property

        Public Property RowID() As Int32
            Get
                Return _RowID
            End Get
            Set(ByVal Value As Int32)
                _RowID = Value
            End Set
        End Property

        Public Property DueDays() As Int32
            Get
                Return _DueDays
            End Get
            Set(ByVal Value As Int32)
                _DueDays = Value
            End Set
        End Property

        Public Property TemplateName() As String
            Get
                Return _TemplateName
            End Get
            Set(ByVal Value As String)
                _TemplateName = Value
            End Set
        End Property

        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal Value As String)
                _Comments = Value
            End Set
        End Property

        Public Property ActivityActionItem() As Int32
            Get
                Return _ActivityActionItem
            End Get
            Set(ByVal Value As Int32)
                _ActivityActionItem = Value
            End Set
        End Property

        Public Property TypeActionItem() As String
            Get
                Return _TypeActionItem
            End Get
            Set(ByVal Value As String)
                _TypeActionItem = Value
            End Set
        End Property

        Public Property StatusActionItem() As Int32
            Get
                Return _StatusActionItem
            End Get
            Set(ByVal Value As Int32)
                _StatusActionItem = Value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property bitOutlook() As Short
            Get
                Return _bitOutlook
            End Get
            Set(ByVal Value As Short)
                _bitOutlook = Value
            End Set
        End Property

        Public Property Type() As Short
            Get
                Return _Type
            End Get
            Set(ByVal Value As Short)
                _Type = Value
            End Set
        End Property

        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal Value As String)
                _Email = Value
            End Set
        End Property

        Public Property RemainderStatus() As Short
            Get
                Return _RemainderStatus
            End Get
            Set(ByVal Value As Short)
                _RemainderStatus = Value
            End Set
        End Property

        Public Property Remainder() As Short
            Get
                Return _Remainder
            End Get
            Set(ByVal Value As Short)
                _Remainder = Value
            End Set
        End Property

        Public Property SnoozeStatus() As Short
            Get
                Return _SnoozeStatus
            End Get
            Set(ByVal Value As Short)
                _SnoozeStatus = Value
            End Set
        End Property

        Public Property Snooze() As Short
            Get
                Return _Snooze
            End Get
            Set(ByVal Value As Short)
                _Snooze = Value
            End Set
        End Property

        Public Property Status() As Long
            Get
                Return _Status
            End Get
            Set(ByVal Value As Long)
                _Status = Value
            End Set
        End Property

        Public Property Activity() As Long
            Get
                Return _Activity
            End Get
            Set(ByVal Value As Long)
                _Activity = Value
            End Set
        End Property


        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property StartTime() As Date
            Get
                Return _StartTime
            End Get
            Set(ByVal Value As Date)
                _StartTime = Value
            End Set
        End Property

        Public Property EndTime() As Date
            Get
                Return _EndTime
            End Get
            Set(ByVal Value As Date)
                _EndTime = Value
            End Set
        End Property



        Public Property CalendarName() As String
            Get
                Return _CalendarName
            End Get
            Set(ByVal Value As String)
                _CalendarName = Value
            End Set
        End Property

        Public Property BitClosed() As Short
            Get
                Return _BitClosed
            End Get
            Set(ByVal Value As Short)
                _BitClosed = Value
            End Set
        End Property


        Public Property AssignedTo() As Long
            Get
                Return _AssignedTo
            End Get
            Set(ByVal Value As Long)
                _AssignedTo = Value
            End Set
        End Property

        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal Value As Long)
                _OppID = Value
            End Set
        End Property

        Public Property Details() As String
            Get
                Return _Details
            End Get
            Set(ByVal Value As String)
                _Details = Value
            End Set
        End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property
        Public Property CaseID() As Long
            Get
                Return _CaseID
            End Get
            Set(ByVal Value As Long)
                _CaseID = Value
            End Set
        End Property
        Public Property CaseExpId() As Long
            Get
                Return _CaseExpId
            End Get
            Set(ByVal Value As Long)
                _CaseExpId = Value
            End Set
        End Property
        Public Property CaseTimeId() As Long
            Get
                Return _CaseTimeId
            End Get
            Set(ByVal Value As Long)
                _CaseTimeId = Value
            End Set
        End Property
        Public Property Task() As Long
            Get
                Return _Task
            End Get
            Set(ByVal Value As Long)
                _Task = Value
            End Set
        End Property

        Public Property CommID() As Long
            Get
                Return _CommID
            End Get
            Set(ByVal Value As Long)
                _CommID = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Private _FollowUpAnyTime As Boolean

        Public Property FollowUpAnyTime() As Boolean
            Get
                Return _FollowUpAnyTime
            End Get
            Set(ByVal Value As Boolean)
                _FollowUpAnyTime = Value
            End Set
        End Property

        Private _strAttendee As String

        Public Property strAttendee() As String
            Get
                Return _strAttendee
            End Get
            Set(ByVal Value As String)
                _strAttendee = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Local (Client) Time Zone Offset from GMT .
        ''' </summary>
        ''' <remarks>
        '''     This holds the Client Time Offset which is being edited/ deleted
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ClientTimeZoneOffset As Int32
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the Private Property representing the Local (Client) Time Zone Offset from GMT .
        ''' </summary>
        ''' <remarks>
        '''     
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _MRItemAmount As Decimal

        Public Property MRItemAmount() As Decimal
            Get
                Return _MRItemAmount
            End Get
            Set(ByVal Value As Decimal)
                _MRItemAmount = Value
            End Set
        End Property


        Private _strNonBizContactID As String
        Public Property NonBizContactID() As String
            Get
                Return _strNonBizContactID
            End Get
            Set(ByVal value As String)
                _strNonBizContactID = value
            End Set
        End Property

        Private _dtDueDate As Date
        Public Property DueDate() As Date
            Get
                Return _dtDueDate
            End Get
            Set(ByVal value As Date)
                _dtDueDate = value
            End Set
        End Property

        Private _numLinkedOrganization As Long
        Public Property numLinkedOrganization() As Long
            Get
                Return _numLinkedOrganization
            End Get
            Set(ByVal Value As Long)
                _numLinkedOrganization = Value
            End Set
        End Property

        Private _strContactIds As String
        Public Property strContactIds() As String
            Get
                Return _strContactIds
            End Get
            Set(Value As String)
                _strContactIds = Value
            End Set
        End Property

        Private _numLinkedContact As Long
        Public Property numLinkedContact() As Long
            Get
                Return _numLinkedContact
            End Get
            Set(ByVal Value As Long)
                _numLinkedContact = Value
            End Set
        End Property

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Public Function GetActionItemAlert() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ActItemAlert", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ChangeSnoozeStatus() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCommID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CommID

                SqlDAL.ExecuteNonQuery(connString, "USP_ActsnoozeStatus", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try


        End Function



        Public Function SaveCommunicationinfo() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(34) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCommId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CommID

                arParms(1) = New Npgsql.NpgsqlParameter("@bitTask", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _Task

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@txtDetails", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _Details

                arParms(5) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _OppID

                arParms(6) = New Npgsql.NpgsqlParameter("@numAssigned", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _AssignedTo

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = DomainID

                arParms(9) = New Npgsql.NpgsqlParameter("@bitClosed", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = _BitClosed

                arParms(10) = New Npgsql.NpgsqlParameter("@vcCalendarName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(10).Value = _CalendarName

                arParms(11) = New Npgsql.NpgsqlParameter("@dtStartTime", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(11).Value = _StartTime

                arParms(12) = New Npgsql.NpgsqlParameter("@dtEndtime", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(12).Value = _EndTime

                arParms(13) = New Npgsql.NpgsqlParameter("@numActivity", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _Activity

                arParms(14) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(14).Value = _Status

                arParms(15) = New Npgsql.NpgsqlParameter("@intSnoozeMins", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(15).Value = _Snooze

                arParms(16) = New Npgsql.NpgsqlParameter("@tintSnoozeStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(16).Value = _SnoozeStatus

                arParms(17) = New Npgsql.NpgsqlParameter("@intRemainderMins", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(17).Value = _Remainder

                arParms(18) = New Npgsql.NpgsqlParameter("@tintRemStaus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(18).Value = _RemainderStatus

                arParms(19) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(19).Value = ClientTimeZoneOffset

                arParms(20) = New Npgsql.NpgsqlParameter("@bitOutLook", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(20).Value = bitOutlook

                arParms(21) = New Npgsql.NpgsqlParameter("@bitSendEmailTemplate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(21).Value = _SendEmailTemplate

                arParms(22) = New Npgsql.NpgsqlParameter("@bitAlert", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(22).Value = _Alert

                arParms(23) = New Npgsql.NpgsqlParameter("@numEmailTemplate", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(23).Value = _EmailTemplate

                arParms(24) = New Npgsql.NpgsqlParameter("@tintHours", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(24).Value = _Hours

                arParms(25) = New Npgsql.NpgsqlParameter("@CaseID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(25).Value = _CaseID

                arParms(26) = New Npgsql.NpgsqlParameter("@CaseTimeID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(26).Value = _CaseTimeId

                arParms(27) = New Npgsql.NpgsqlParameter("@CaseExpId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(27).Value = _CaseExpId

                arParms(28) = New Npgsql.NpgsqlParameter("@ActivityId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(28).Value = _ActivityId

                arParms(29) = New Npgsql.NpgsqlParameter("@bitFollowUpAnyTime", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(29).Value = _FollowUpAnyTime

                arParms(30) = New Npgsql.NpgsqlParameter("@strAttendee", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(30).Value = _strAttendee

                arParms(31) = New Npgsql.NpgsqlParameter("@numLinkedOrganization", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(31).Value = _numLinkedOrganization

                arParms(32) = New Npgsql.NpgsqlParameter("@numLinkedContact", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(32).Value = _numLinkedContact

                arParms(33) = New Npgsql.NpgsqlParameter("@strContactIds", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(33).Value = _strContactIds

                arParms(34) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(34).Value = Nothing
                arParms(34).Direction = ParameterDirection.InputOutput

                Dim numcommId As Long
                numcommId = CType(SqlDAL.ExecuteScalar(connString, "usp_InsertCommunication", arParms), Long)
                _CommID = numcommId

                Return numcommId

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function DeleteActionItem() As Boolean
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCommId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CommID

                ds = SqlDAL.ExecuteDataset(connString, "USP_DeleteActItem", arParms)

                Return True

            Catch ex As Exception
                Return False
                Throw ex
            End Try
        End Function

        Public Function GetActionItemDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCommid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CommID

                arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = ClientTimeZoneOffset

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCommunicationInfo", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetActivityDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numActivityid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CommID

                arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = ClientTimeZoneOffset

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetActivityInfo", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetEmpAvailability() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _StartDateTime 'If(_Duration = 0, _StartTime, _StartDateTime)

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@TeamType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _Type

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@Duration", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _Duration

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_EmpAvailability", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetEmpWeeklyAvailability() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@dtDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _StartDateTime

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@TeamType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _Type

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@Duration", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _Duration

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_EmpWeeklyAvailability", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetRecentItems() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetRecentItems", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        '''''''''''''''''''''''''''''''''''''' Added By tarun Juneja'''''''''''''''''''''''''

        Private Function GetThisActionTemplateData(ByVal RowID As Int32) As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim inputParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                inputParms(0) = New Npgsql.NpgsqlParameter("@rowID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParms(0).Value = RowID

                inputParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                inputParms(1).Value = Nothing
                inputParms(1).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteDataset(connString, "Select_Action_Template_Data", inputParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function GetActionTemplateUIData() As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "Select_Action_Template__UI_Data", sqlParams.ToArray())
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Sub UpdateActionTemplateData()
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim inputParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(15) {}

                inputParms(0) = New Npgsql.NpgsqlParameter("@templateName", NpgsqlTypes.NpgsqlDbType.Varchar)
                inputParms(0).Value = TemplateName

                inputParms(1) = New Npgsql.NpgsqlParameter("@dueDays", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParms(1).Value = DueDays

                inputParms(2) = New Npgsql.NpgsqlParameter("@status", NpgsqlTypes.NpgsqlDbType.Bigint)
                inputParms(2).Value = StatusActionItem

                inputParms(3) = New Npgsql.NpgsqlParameter("@type", NpgsqlTypes.NpgsqlDbType.Varchar)
                inputParms(3).Value = TypeActionItem

                inputParms(4) = New Npgsql.NpgsqlParameter("@activity", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParms(4).Value = ActivityActionItem

                inputParms(5) = New Npgsql.NpgsqlParameter("@comments", NpgsqlTypes.NpgsqlDbType.Varchar)
                inputParms(5).Value = Comments

                inputParms(6) = New Npgsql.NpgsqlParameter("@rowID", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParms(6).Value = RowID

                inputParms(7) = New Npgsql.NpgsqlParameter("@bitSendEmailTemplate", NpgsqlTypes.NpgsqlDbType.Bit)
                inputParms(7).Value = _SendEmailTemplate

                inputParms(8) = New Npgsql.NpgsqlParameter("@numEmailTemplate", NpgsqlTypes.NpgsqlDbType.Bigint)
                inputParms(8).Value = _EmailTemplate

                inputParms(9) = New Npgsql.NpgsqlParameter("@tintHours", NpgsqlTypes.NpgsqlDbType.Smallint)
                inputParms(9).Value = _Hours

                inputParms(10) = New Npgsql.NpgsqlParameter("@bitAlert", NpgsqlTypes.NpgsqlDbType.Bit)
                inputParms(10).Value = _Alert

                inputParms(11) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                inputParms(11).Value = DomainID

                inputParms(12) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                inputParms(12).Value = UserCntID

                inputParms(13) = New Npgsql.NpgsqlParameter("@numTaskType", NpgsqlTypes.NpgsqlDbType.Bigint)
                inputParms(13).Value = _Task

                inputParms(14) = New Npgsql.NpgsqlParameter("@bitRemind", NpgsqlTypes.NpgsqlDbType.Smallint)
                inputParms(14).Value = RemainderStatus

                inputParms(15) = New Npgsql.NpgsqlParameter("@numRemindBeforeMinutes", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParms(15).Value = Remainder

                SqlDAL.ExecuteNonQuery(connString, "Update_Action_Template_Data", inputParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub DeleteActionTemplateData()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim inputParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                inputParms(0) = New Npgsql.NpgsqlParameter("@rowID", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParms(0).Value = RowID

                SqlDAL.ExecuteNonQuery(connString, "Delete_This_Action_Template_UI_Data", inputParms)
            Catch ex As Exception

            End Try
        End Sub

        Public Sub InsertActionTemplateData()
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim inputParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}

                inputParms(0) = New Npgsql.NpgsqlParameter("@templateName", NpgsqlTypes.NpgsqlDbType.Varchar)
                inputParms(0).Value = TemplateName

                inputParms(1) = New Npgsql.NpgsqlParameter("@dueDays", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParms(1).Value = DueDays

                inputParms(2) = New Npgsql.NpgsqlParameter("@status", NpgsqlTypes.NpgsqlDbType.Bigint)
                inputParms(2).Value = StatusActionItem

                inputParms(3) = New Npgsql.NpgsqlParameter("@type", NpgsqlTypes.NpgsqlDbType.Varchar)
                inputParms(3).Value = TypeActionItem

                inputParms(4) = New Npgsql.NpgsqlParameter("@activity", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParms(4).Value = ActivityActionItem

                inputParms(5) = New Npgsql.NpgsqlParameter("@comments", NpgsqlTypes.NpgsqlDbType.Varchar)
                inputParms(5).Value = Comments

                inputParms(6) = New Npgsql.NpgsqlParameter("@bitSendEmailTemplate", NpgsqlTypes.NpgsqlDbType.Bit)
                inputParms(6).Value = _SendEmailTemplate

                inputParms(7) = New Npgsql.NpgsqlParameter("@numEmailTemplate", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParms(7).Value = _EmailTemplate

                inputParms(8) = New Npgsql.NpgsqlParameter("@tintHours", NpgsqlTypes.NpgsqlDbType.Smallint)
                inputParms(8).Value = _Hours

                inputParms(9) = New Npgsql.NpgsqlParameter("@bitAlert", NpgsqlTypes.NpgsqlDbType.Bit)
                inputParms(9).Value = _Alert

                inputParms(10) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParms(10).Value = DomainID

                inputParms(11) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParms(11).Value = UserCntID

                inputParms(12) = New Npgsql.NpgsqlParameter("@numTaskType", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParms(12).Value = _Task

                inputParms(13) = New Npgsql.NpgsqlParameter("@bitRemind", NpgsqlTypes.NpgsqlDbType.Smallint)
                inputParms(13).Value = RemainderStatus

                inputParms(14) = New Npgsql.NpgsqlParameter("@numRemindBeforeMinutes", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParms(14).Value = Remainder

                SqlDAL.ExecuteNonQuery(connString, "Insert_Action_Template_Data", inputParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ' We are calling from newaction.aspx page to load all action items name
        Public Function LoadActionItemTemplateData() As DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim inputParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                Dim _dsActionItemNameData As DataSet
                inputParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParms(0).Value = DomainID

                inputParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                inputParms(1).Value = UserCntID

                inputParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                inputParms(2).Value = Nothing
                inputParms(2).Direction = ParameterDirection.InputOutput

                _dsActionItemNameData = SqlDAL.ExecuteDataset(connString, "Select_Action_Item_Name", inputParms)
                Return _dsActionItemNameData.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ' We are calling from newaction.aspx page to load all action items name
        Public Function LoadThisActionItemTemplateData() As DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim _dsThisActionItemData As DataSet

                Dim inputParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                inputParms(0) = New Npgsql.NpgsqlParameter("@rowID", NpgsqlTypes.NpgsqlDbType.Integer)
                inputParms(0).Value = RowID

                inputParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                inputParms(1).Value = Nothing
                inputParms(1).Direction = ParameterDirection.InputOutput

                _dsThisActionItemData = SqlDAL.ExecuteDataset(connString, "Select_This_Action_Template_Data", inputParms)
                Return _dsThisActionItemData.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ' We are calling from newaction.aspx page to load all action items name
        Public Function GetMaxTemplateID() As Int32

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput
                Return CInt(SqlDAL.ExecuteScalar(connString, "Get_Max_Template_ID", arParms))
            Catch ex As Exception
                Throw ex
            End Try

        End Function


        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Public Function ManageCorrespondence() As Boolean

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCorrespondenceID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CorrenspondenceID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCommID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _CommID

                arParms(2) = New Npgsql.NpgsqlParameter("@numEmailHistoryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _EmailHistoryID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintCorrType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _CorrType

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@numOpenRecordID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _OpenRecordID

                arParms(6) = New Npgsql.NpgsqlParameter("@monMRItemAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(6).Value = _MRItemAmount

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageCorrespondence", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        Public Function GetBizDocActionItemDtls(ByVal numBizActionID As Int32) As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numBizActionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numBizActionID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocActionDetails", arParms)

                Return ds

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function UpdateBizDocActionItemDtls(ByVal numBizActionID As Int32, ByVal numOppBizDocsId As Int32, ByVal numStatus As Int16, ByVal btType As Int16, ByVal vcComment As String) As Boolean
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                UpdateBizDocActionItemDtls = False

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBizActionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numBizActionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numOppBizDocsId

                arParms(3) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = numStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@btType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = btType

                arParms(5) = New Npgsql.NpgsqlParameter("@vcComment", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = vcComment

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_UpdateBizDocActionItem", arParms)

                If ds.Tables(0).Rows.Count > 0 Then
                    If CType(ds.Tables(0).Rows(0).Item(0), String) = "I" Then Return True
                End If


            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCommunicationAttendees() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCommid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CommID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_SelectCommunicationAttendees", arParms)
                Return ds

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageCommunicationAttendees() As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCommid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CommID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@type", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _Type

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageCommunicationAttendees", arParms)
                Return ""
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function CommunicationAttendees_Detail() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCommid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CommID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_SelectCommunicationAttendees_Detail", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ChangeCommunicationAttendees_Status() As Boolean

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCommid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CommID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _Status

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateCommunicationAttendees_Status", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function



        Public Function GetCommunicationOpenRecord() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numOpenRecordID", _OpenRecordID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCommunicationOpenRecord", sqlParams.ToArray())

                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' Dismiss the reminder for action item by setting tintRemStatus=0 in table Communication
        ''' so its not available in reminder popup 
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub DismissReminder()
            Try
                Try
                    Dim getconnection As New GetConnection
                    Dim connString As String = GetConnection.GetConnectionString
                    Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                    arParms(0) = New Npgsql.NpgsqlParameter("@numCommID", NpgsqlTypes.NpgsqlDbType.Integer)
                    arParms(0).Value = CommID

                    SqlDAL.ExecuteNonQuery(connString, "USP_Communication_DismissReminder", arParms)
                Catch ex As Exception
                    Throw
                End Try
            Catch ex As Exception
                Throw
            End Try
        End Sub
        ''' <summary>
        ''' Soonzes reminder by sepecified time
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub SnoozeReminder(ByVal snoozeTime As DateTime)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCommID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = CommID

                arParms(1) = New Npgsql.NpgsqlParameter("@LastSnoozDateTimeUtc", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = snoozeTime

                SqlDAL.ExecuteNonQuery(connString, "USP_Communication_SnoozeReminder", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub FinishCommunication()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCommID", CommID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_Communication_Finish", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class

End Namespace