''' -----------------------------------------------------------------------------
''' Project	 : BACRM.BusinessLogic
''' Class	 : FormGenericLeadBox
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This class contains several methods and functions, which is used to 
'''     process and store the LeadBox information
''' </summary>
''' <remarks>
'''     This class is used to access the data and manage the same LeadBox 
''' </remarks>
''' <history>
''' 	[Debasish Tapan Nag]	07/31/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Option Explicit On
Imports System.Data.SqlClient
Imports System.IO
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Workflow
Namespace BACRM.BusinessLogic.Admin

    Public Class FormGenericLeadBox
        Inherits BACRM.BusinessLogic.CBusinessBase
        Public objLeads As New Leads.CLeads

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the Street Name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcStreet() As String
            Set(ByVal Value As String)
                objLeads.Street = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the postal code.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property intPostalCode() As String
            Set(ByVal Value As String)
                objLeads.PostalCode = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the state name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcState() As Long

            Set(ByVal Value As Long)
                objLeads.State = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the city name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcCity() As String
            Set(ByVal Value As String)
                objLeads.City = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the country name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcCountry() As Long
            Set(ByVal Value As Long)
                objLeads.Country = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the phone number.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numPhone() As String
            Set(ByVal Value As String)
                objLeads.ContactPhone = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the phone extension number.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numPhoneExtension() As String
            Set(ByVal Value As String)
                objLeads.PhoneExt = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the fax number.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numFax() As String
            Set(ByVal Value As String)
                objLeads.Fax = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the department name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcDepartment() As String
            Set(ByVal Value As String)
                objLeads.Department = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the company type id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numCompanyType() As Long
            Set(ByVal Value As Long)
                objLeads.CompanyType = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the company rating.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numCompanyRating() As Long
            Set(ByVal Value As Long)
                objLeads.CompanyRating = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the industry.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numIndustryType() As Long
            Set(ByVal Value As Long)
                objLeads.CompanyIndustry = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the company sic code information.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public WriteOnly Property vcCompanySicCode() As String
        '    Set(ByVal Value As String)
        '        objLeads.CompanySicCode = Value
        '    End Set
        'End Property '' Commented by anoop Search
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the CRMType information (0 for Leads).
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _numCRMType As Integer
        Public Property CRMType() As Integer
            Get
                Return _numCRMType
            End Get
            Set(ByVal Value As Integer)
                _numCRMType = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the Lead Box Flag (1 for Leadbox).
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property LeadBoxFlg() As Integer
            Get
                Return 1
            End Get
            Set(ByVal Value As Integer)
                objLeads.LeadBoxFlg = LeadBoxFlg
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the Public Flag (1 for Leadbox).
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property bitPublicFlag() As Integer
            Get
                Return 1
            End Get
            Set(ByVal Value As Integer)
                objLeads.PublicFlag = bitPublicFlag
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the territory information.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numTerritoryId() As Long
            Set(ByVal Value As Long)
                objLeads.TerritoryID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the follow up status information.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numFollowUpStatus() As Long
            Set(ByVal Value As Long)
                objLeads.FollowUpStatus = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the status information.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numStatusId() As Long
            Set(ByVal Value As Long)
                objLeads.StatusID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the profile information.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numProfile() As Long
            Set(ByVal Value As Long)
                objLeads.Profile = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the aoi 1.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcInterested1() As String
            Set(ByVal Value As String)
                'objLeads.Interested1 = Value
            End Set
        End Property '''Commented by anoop Search
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the aoi 2.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcInterested2() As String
            Set(ByVal Value As String)
                'objLeads.Interested2 = Value
            End Set
        End Property '''Commented by anoop Search
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the aoi 3.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcInterested3() As String
            Set(ByVal Value As String)
                'objLeads.Interested3 = Value
            End Set
        End Property '''Commented by anoop Search
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the aoi 4.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcInterested4() As String
            Set(ByVal Value As String)
                'objLeads.Interested4 = Value
            End Set
        End Property '''Commented by anoop Search
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the aoi 5.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcInterested5() As String
            Set(ByVal Value As String)
                'objLeads.Interested5 = Value
            End Set
        End Property '''Commented by anoop Search
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the aoi 6.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcInterested6() As String
            Set(ByVal Value As String)
                'objLeads.Interested6 = Value
            End Set
        End Property  '''Commented by anoop Search
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the aoi 7.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcInterested7() As String
            Set(ByVal Value As String)
                ' objLeads.Interested7 = Value
            End Set
        End Property '''Commented by anoop Search
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the aoi 8.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcInterested8() As String
            Set(ByVal Value As String)
                ' objLeads.Interested8 = Value
            End Set
        End Property '''Commented by anoop Search
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the annual revenue.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property monAnnualRevenue() As Double
            Set(ByVal Value As Double)
                objLeads.AnnualRevenue = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the number of employees.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numNumEmployees() As Integer
            Set(ByVal Value As Integer)
                objLeads.NumOfEmp = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the number of employees.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property numNumOfEmployee() As Integer
            Set(ByVal Value As Integer)
                objLeads.NumOfEmp = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the position.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcPosition() As String
            Set(ByVal Value As String)
                objLeads.Position = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the website.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcWebSite() As String
            Set(ByVal Value As String)
                objLeads.WebSite = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the notes information (if any).
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcNotes() As String
            Set(ByVal Value As String)
                objLeads.Notes = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the group name information.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcGroupName() As String
            Set(ByVal Value As String)
                objLeads.GroupName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the email information.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property vcEmail() As String
            Set(ByVal Value As String)
                objLeads.Email = Value
            End Set
            Get
                Return objLeads.Email
            End Get
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the record owner.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property recordOwner() As Long
            Set(ByVal Value As Long)
                objLeads.UserCntID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the division name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcDivisionName() As String
            Set(ByVal Value As String)
                objLeads.DivisionName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set/ gets of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the division id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _DivisionID As Long
        Public Property numDivisionID() As Long
            Set(ByVal Value As Long)
                objLeads.DivisionID = Value
                _DivisionID = Value
            End Set
            Get
                Return objLeads.DivisionID
            End Get
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set/ gets of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the contact id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ContactID As Long

        Public Property numContactID() As Long
            Set(ByVal Value As Long)
                objLeads.ContactID = Value
                _ContactID = Value
            End Set
            Get
                Return objLeads.ContactID
            End Get
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set/ gets of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the company id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property numCompanyID() As Long
            Set(ByVal Value As Long)
                objLeads.CompanyID = Value
            End Set
            Get
                Return objLeads.CompanyID
            End Get
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the customer name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property vcCustomerName() As String
            Set(ByVal Value As String)
                objLeads.CustName = Value
                objLeads.CompanyName = Value
            End Set
            Get
                Return objLeads.CompanyName
            End Get
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the first name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcFirstName() As String
            Set(ByVal Value As String)
                objLeads.FirstName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the last name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcLastName() As String
            Set(ByVal Value As String)
                objLeads.LastName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the company name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcCompanyName() As String
            Set(ByVal Value As String)
                objLeads.CompanyName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the company address.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcCompanyAddress() As String
            Set(ByVal Value As String)
                objLeads.Comments &= Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the contact type.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	11/23/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property numContactType() As Long
            Get
                Return objLeads.ContactType
            End Get
            Set(ByVal Value As Long)
                objLeads.ContactType = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the contact person address.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcAddress() As String
            Set(ByVal Value As String)
                objLeads.Notes &= Value
                objLeads.ContactLoc &= Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the country of shipment.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcShipCountry() As Long
            Set(ByVal Value As Long)
                objLeads.SCountry = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the shipment postal code.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcShipPostCode() As String
            Set(ByVal Value As String)
                objLeads.SPostalCode = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the shipment state.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcShipState() As Long
            Set(ByVal Value As Long)
                objLeads.SState = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the shipment city.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcShipCity() As String
            Set(ByVal Value As String)
                objLeads.SCity = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the shipment street.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcShipStreet() As String
            Set(ByVal Value As String)
                objLeads.SStreet = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This indicates if teh billing address is same as shipment address.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property bitSameAddr() As Integer
            Set(ByVal Value As Integer)
                objLeads.SameAddr = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This stores the billing street.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcBillStreet() As String
            Set(ByVal Value As String)
                objLeads.Street = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This stores the billing city.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcBillCity() As String
            Set(ByVal Value As String)
                objLeads.City = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This stores the billing state.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcBillState() As Long
            Set(ByVal Value As Long)
                objLeads.State = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This stores the billing post code.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public WriteOnly Property vcBillPostCode() As String
            Set(ByVal Value As String)
                objLeads.PostalCode = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This stores the billing country.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property vcBillCountry() As Long
            Get
                Return objLeads.Country
            End Get
            Set(ByVal Value As Long)
                objLeads.Country = Value
            End Set
        End Property

        Public Property vcComments() As String
            Get
                Return objLeads.Comments
            End Get
            Set(ByVal Value As String)
                objLeads.Comments = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     The following set/ gets of properties are interacting with the properties of Leads
        ''' </summary>
        ''' <remarks>
        '''     This holds the group id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property numGroupId() As Long
            Set(ByVal Value As Long)
                objLeads.GroupID = Value
            End Set
            Get
                Return objLeads.GroupID
            End Get
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '        objLeads.DomainID = Value
        '    End Set
        'End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Division Name
        ''' </summary>
        ''' <value>Returns the _DivisionName as String.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the _DivisionName. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property DivisionName() As String
            Get
                Return objLeads.DivisionName
            End Get
            Set(ByVal Value As String)
                objLeads.DivisionName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DivisionName.
        ''' </summary>
        ''' <remarks>
        '''     This holds the DivisionName
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _UserID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User id.
        ''' </summary>
        ''' <value>Returns the user id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the User id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property UserID() As Long
            Get
                Return _UserID
            End Get
            Set(ByVal Value As Long)
                _UserID = Value
                'objLeads.CurrentUserId = Value
                objLeads.UserCntID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Lead Box ID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Lead box id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _LeadBoxId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Lead Box Id.
        ''' </summary>
        ''' <value>Returns the leadbox id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the leadbox id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property LeadBoxID() As Long
            Get
                Return _LeadBoxId
            End Get
            Set(ByVal Value As Long)
                _LeadBoxId = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Lead Box Db Field Name.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Lead box Db Field Name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _dbFieldName As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Lead Box Db Field Name.
        ''' </summary>
        ''' <value>Returns the leadbox Db Field Name as string.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the leadbox Db Field Name. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property DbColumnName() As String
            Get
                Return _dbFieldName
            End Get
            Set(ByVal Value As String)
                _dbFieldName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Lead Box Db Field Value.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Lead box Db Field Value.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _dbFieldValue As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Lead Box Db Field Value.
        ''' </summary>
        ''' <value>Returns the leadbox Db Field Value as string.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the leadbox Db Field Value. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property DbColumnValue() As String
            Get
                Return _dbFieldValue
            End Get
            Set(ByVal Value As String)
                _dbFieldValue = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Lead Box label corresponding to the Db Field Value.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Lead box label.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _FieldLabel As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Lead Box Db Field label.
        ''' </summary>
        ''' <value>Returns the leadbox Db Field label as string.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the leadbox Db Field label. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FieldLabel() As String
            Get
                Return _FieldLabel
            End Get
            Set(ByVal Value As String)
                _FieldLabel = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the row number in which the label is present.
        ''' </summary>
        ''' <remarks>
        '''     This holds the row number.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _RowNum As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the row number in which the label exists.
        ''' </summary>
        ''' <value>Returns the row number as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the label row number. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property RowNum() As Long
            Get
                Return _RowNum
            End Get
            Set(ByVal Value As Long)
                _RowNum = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the column number in which the label is present.
        ''' </summary>
        ''' <remarks>
        '''     This holds the column number.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ColNum As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the column number in which the label exists.
        ''' </summary>
        ''' <value>Returns the column number as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the label column number. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ColNum() As Long
            Get
                Return _ColNum
            End Get
            Set(ByVal Value As Long)
                _ColNum = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the sXMLFilePath whihc is path to the xml file.
        ''' </summary>
        ''' <value>Returns the sXMLFilePath as String.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the sXMLFilePath. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _sXMLFilePath As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the sXMLFilePath whihc is path to the xml file.
        ''' </summary>
        ''' <value>Returns the sXMLFilePath as String.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the sXMLFilePath. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property sXMLFilePath() As String
            Get
                Return _sXMLFilePath
            End Get
            Set(ByVal Value As String)
                _sXMLFilePath = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This is the leadbox form data in XML format.
        ''' </summary>
        ''' <value>Returns the _sXMLLeadBoxData as String.</value>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/2/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _sXMLLeadBoxData As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the _sXMLLeadBoxData whihc is form data in XML format.
        ''' </summary>
        ''' <value>Returns the _sXMLLeadBoxData as String.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the sXMLFilePath. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property sXMLLeadBoxData() As String
            Get
                Return _sXMLLeadBoxData
            End Get
            Set(ByVal Value As String)
                _sXMLLeadBoxData = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the current date and time.
        ''' </summary>
        ''' <remarks>
        '''     This holds the current date and time.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _numCreationDate As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets the current date and time.
        ''' </summary>
        ''' <value>Returns the values of current date and time.</value>
        ''' <remarks>
        '''     This property is used to Get value of current date and time.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property CreationDate() As Long
        '    Get
        '        _numCreationDate = Format(Now(), "yyyyMMddhhmmss")
        '        Return _numCreationDate
        '    End Get
        '    Set(ByVal Value As Long)
        '        objLeads.CreatedDate = CreationDate
        '    End Set
        'End Property ''' Commented by Anoop.

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get the AOIs from teh database pertaining to teh LeadBox.
        ''' </summary>
        ''' <return>DataSet of AOI's</return>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the iAOI's in a Leadbox
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/1/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getLeadBoxAOI() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numLeadBoxID", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = LeadBoxID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetDynamicAOIFromLeadBox", arParms)  'execute and store to dataset                                                 'return datatable
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the xml into the server file
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <return>The name of the xml file</return>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function saveLeadBoxXML() As String
            Dim StrWriter As StreamWriter                                       'Declare a StreamWriter object
            Dim strFileName As String                                           'Declare a string object
            strFileName = Now.ToString & ".xml"                                 'Set the file name
            Try
                If Directory.Exists(sXMLFilePath) = False Then                  'If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(sXMLFilePath)
                End If

                StrWriter = File.CreateText(sXMLFilePath & "\" & strFileName)   'create the flle
                StrWriter.Write(sXMLLeadBoxData)                                'Write XML doument
                StrWriter.Close()                                               'close the object
                Return strFileName
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to get the LeadDetails information to the database.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the 
        '''     LeadBox information
        ''' </remarks>
        ''' <return>The datatable containg the data and the values of the LeadBox</return>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getLeadBoxDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numLeadBoxId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = LeadBoxID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetDynamicLeadBoxEntryForLeadGeneration", arParms)  'execute and store to dataset                                                 'return datatable

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is called to create a lead.
        ''' </summary>
        ''' <remarks>Code logic created by Anoop Jayaraj for creation of Leads</remarks>
        ''' <param name="objLeadAutoRules">The object which contains the property values set before</param>
        ''' <return>Indicating that the operation of creation of lead was successful or not</return>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function CreateLead(ByRef objLeadAutoRules As FormGenericLeadBox) As String
            Try
                objLeadAutoRules.DomainID = DomainID
                objLeadAutoRules.objLeads.GetConIDCompIDDivIDFromEmail()                                        'checking if the contact exists
                If Me.numContactID = 0 Then
                    Dim numDivisionId As Long                                                                           'Declare a long int to store the Div Id
                    numDivisionId = objLeadAutoRules.objLeads.GetDivisionfromCompName()                                 'Check if the Company and Division exists
                    If numDivisionId = 0 Then                                                                           'If the Division is new
                        Me.numCompanyID = objLeadAutoRules.objLeads.CreateRecordCompanyInfo()                       'Set the company Id                             
                        Me.numDivisionID = objLeadAutoRules.objLeads.CreateRecordDivisionsInfo()                    'Call toe create divisions
                        'Added By Sachin Sadhu||Date:22ndMay12014
                        'Purpose :To Added Organization data in work Flow queue based on created Rules
                        '          Using Change tracking
                        Dim objWfA As New Workflow.Workflow()
                        objWfA.DomainID = DomainID
                        objWfA.UserCntID = UserID
                        objWfA.RecordID = Me.numDivisionID
                        objWfA.SaveWFOrganizationQueue()
                        'end of code

                    Else
                        Me.numDivisionID = numDivisionId                                                                'Set the Division Id
                    End If
                    Me.numContactID = objLeadAutoRules.objLeads.CreateRecordAddContactInfo()                    'Call to create the contact information
                End If
                Return ""                                                                                           'Return blank as operation is successful
            Catch ex As Exception
                'Sit idle because this is a client posting the Lead, but internally send a notification to Admin
                Dim strFileName As String                                                                           'Declare a variable which will store the file datetime stamp
                strFileName = objLeadAutoRules.saveLeadBoxXML()                                                     'Call to save teh leadbox information
                Return strFileName                                                                                  'Return the file name
            End Try
        End Function
    End Class
End Namespace

