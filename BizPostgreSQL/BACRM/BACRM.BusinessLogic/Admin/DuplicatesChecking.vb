
Option Explicit On
Option Strict On
Imports System.Data.SqlClient
Imports System.IO
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Admin
    Public Class objDuplicates
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _SearchableEntity As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the entity being searched (Contact / Organizations).
        ''' </summary>
        ''' <value>Returns the entiry being searched.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the entity being searched. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SearchableEntity() As String
            Get
                Return _SearchableEntity
            End Get
            Set(ByVal Value As String)
                _SearchableEntity = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Protected _SearchableWithin As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the CRM Type for searching db records.
        ''' </summary>
        ''' <value>Returns the CRM List being searched.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the CRM Type being searched. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SearchWithin() As String
            Get
                Return _SearchableWithin
            End Get
            Set(ByVal Value As String)
                _SearchableWithin = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _FilterRelationship As Integer
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the relationship filter for searching db records.
        ''' </summary>
        ''' <value>Returns the relationship filter being searched.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the relationship filter being searched. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FilterRelationship() As Integer
            Get
                Return _FilterRelationship
            End Get
            Set(ByVal Value As Integer)
                _FilterRelationship = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _CreationDateStart As String = ""
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the start of the creation date for searching db records.
        ''' </summary>
        ''' <value>Returns the start date for creation of the record being searched.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the date when the record was created. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CreationDateStart() As String
            Get
                Return _CreationDateStart
            End Get
            Set(ByVal Value As String)
                _CreationDateStart = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _CreationDateEnd As String = ""
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the End of the creation date for searching db records.
        ''' </summary>
        ''' <value>Returns the End date for creation of the record being searched.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the End date range when the record was created. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CreationDateEnd() As String
            Get
                Return _CreationDateEnd
            End Get
            Set(ByVal Value As String)
                _CreationDateEnd = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Grid Sort Order.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Sort Order.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _SortOrder As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the Sort Order which was used for displayign records.
        ''' </summary>
        ''' <value>Returns the order of sorting.</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SortOrder() As String
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As String)
                _SortOrder = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Column which is used for Sorting.
        ''' </summary>
        ''' <remarks>
        '''     This holds the column which is used for sorting and displaying records in DataGrid.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _SortDbColumnName As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the column which is filtered for displaying records.
        ''' </summary>
        ''' <value>Returns the Columns which is sorted for displayign records.</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SortDbColumnName() As String
            Get
                Return "[" & _SortDbColumnName & "]"
            End Get
            Set(ByVal Value As String)
                _SortDbColumnName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Page Size for Custom Paging.
        ''' </summary>
        ''' <remarks>
        '''     Set in the Config file.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _PageSize As Integer
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets the Page Size for Custom Paging.
        ''' </summary>
        ''' <value>Returns the Page Size.</value>
        ''' <remarks>
        '''     This property is used to Get the Custom Page Size from the Congif file. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the CurrentPageIndex.
        ''' </summary>
        ''' <remarks>
        '''     This holds the CurrentPageIndex.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _CurrentPageIndex As Integer
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets the current page index for custom paging.
        ''' </summary>
        ''' <value>Returns the current page index.</value>
        ''' <remarks>
        '''     This property is used to Set/ Get the current apge index from Congif file. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CurrentPageIndex() As Integer
            Get
                Return _CurrentPageIndex
            End Get
            Set(ByVal Value As Integer)
                _CurrentPageIndex = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for all list details for the dropdown.
        ''' </summary>
        ''' <returns>Returns the value as DataSet.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of list details
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getListDetails(ByVal iListItemId As Integer) As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New getconnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = iListItemId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcItemType", NpgsqlTypes.NpgsqlDbType.Char, 3)
                arParms(1).Value = "LI"                                                          'representing listmaster/ listdetails

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetMasterListDetails", arParms)     'execute and store to dataset

                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
    Public Class SearchableOrgFilters
        Inherits objDuplicates
        ''' -----------------------------------------------------------------------------
        Private _OrgNoAssociationFlag As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the flag to indicate of we are searching for Organizations with no associations.
        ''' </summary>
        ''' <value>Returns the flag indicating association/ no association.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the association flag. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property OrgNoAssociationFlag() As Boolean
            Get
                Return _OrgNoAssociationFlag
            End Get
            Set(ByVal Value As Boolean)
                _OrgNoAssociationFlag = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _OrgSameDivisionFlag As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the flag to indicate of we are searching for Organizations with same division names.
        ''' </summary>
        ''' <value>Returns the flag indicating if we are searching for same division names within an organization.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the flag indication if we are searching for same division names in the same organization. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property OrgSameDivisionFlag() As Boolean
            Get
                Return _OrgSameDivisionFlag
            End Get
            Set(ByVal Value As Boolean)
                _OrgSameDivisionFlag = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _OrgSameWebsite As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the flag to indicate of we are searching for Organizations with same websites.
        ''' </summary>
        ''' <value>Returns the flag indicating if we are searching for same websites names within an organization.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the flag indication if we are searching for same websites in the organization list. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property OrgSameWebsite() As Boolean
            Get
                Return _OrgSameWebsite
            End Get
            Set(ByVal Value As Boolean)
                _OrgSameWebsite = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _OrgMatchingFields As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the string for searching duplicates in organization records.
        ''' </summary>
        ''' <value>Returns the field which is being searched for duplicates in the organization list.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the field which is being used to search duplicates. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property OrgMatchingFields() As String
            Get
                Return _OrgMatchingFields
            End Get
            Set(ByVal Value As String)
                _OrgMatchingFields = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to search for Organization and return Results.
        ''' </summary>
        ''' <returns>Returns the result as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the result of duplicate org. search
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function PerformDuplicateOrgSearch() As DataSet
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New getconnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@vcCRMTypes", NpgsqlTypes.NpgsqlDbType.VarChar, 5)
                arParms(0).Value = MyBase.SearchWithin

                arParms(1) = New Npgsql.NpgsqlParameter("@numCompanyType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = MyBase.FilterRelationship

                arParms(2) = New Npgsql.NpgsqlParameter("@vcOrgCreationStartDateRange", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(2).Value = MyBase.CreationDateStart

                arParms(3) = New Npgsql.NpgsqlParameter("@vcOrgCreationEndDateRange", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(3).Value = MyBase.CreationDateEnd

                arParms(4) = New Npgsql.NpgsqlParameter("@boolAssociation", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = OrgNoAssociationFlag

                arParms(5) = New Npgsql.NpgsqlParameter("@boolSameDivisionName", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = OrgSameDivisionFlag

                arParms(6) = New Npgsql.NpgsqlParameter("@boolSameWebsite", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = OrgSameWebsite

                arParms(7) = New Npgsql.NpgsqlParameter("@vcMatchingField", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = OrgMatchingFields

                arParms(8) = New Npgsql.NpgsqlParameter("@vcSortColumn", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = SortDbColumnName

                arParms(9) = New Npgsql.NpgsqlParameter("@vcSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(9).Value = SortOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@numNosRowsReturned", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = PageSize

                arParms(11) = New Npgsql.NpgsqlParameter("@numCurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Value = CurrentPageIndex

                arParms(12) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = DomainID

                ds = SqlDAL.ExecuteDataset(connString, "usp_PerformDuplicateOrgSearch", arParms) 'execute and store to dataset
                ds.Tables(0).Columns.Add(New DataColumn("<a href='javascript: ToggleDivContCBSelection();' style='color:white;'>Select</a>", System.Type.GetType("System.String"), "'<input Type=CheckBox ID=cbSR' + [numDivisionID] + ' value=' + [numDivisionID] + '>'")) 'The last column will be checkboxes
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class

    Public Class SearchableContFilters
        Inherits objDuplicates
        ''' -----------------------------------------------------------------------------
        Private _CntNoItemHistoryFlag As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the flag to indicate we are searching for contacts with Item History/ No Item History.
        ''' </summary>
        ''' <value>Returns the flag indicating item history/ no item history.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the item history flag. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CntNoItemHistoryFlag() As Boolean
            Get
                Return _CntNoItemHistoryFlag
            End Get
            Set(ByVal Value As Boolean)
                _CntNoItemHistoryFlag = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _CntSameParentFlag As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the flag to indicate we are searching for contacts with same parent org and contact email.
        ''' </summary>
        ''' <value>Returns the flag indicating that the parent org and contact email are same but records are different.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the same parent org flag. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CntSameParentFlag() As Boolean
            Get
                Return _CntSameParentFlag
            End Get
            Set(ByVal Value As Boolean)
                _CntSameParentFlag = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _CntSameDivisionFlag As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the flag to indicate of we are searching for contacts with same division names and same first and last name.
        ''' </summary>
        ''' <value>Returns the flag indicating if we are searching for same division names for records with same contact names.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the flag indication if we are searching for same division names for same contacts, but multiple records. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CntSameDivisionFlag() As Boolean
            Get
                Return _CntSameDivisionFlag
            End Get
            Set(ByVal Value As Boolean)
                _CntSameDivisionFlag = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _CntSamePhoneFlag As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the flag to indicate of we are searching for contacts with same first and last name abd same phone numbers.
        ''' </summary>
        ''' <value>Returns the flag indicating if we are searching for same phone numbers for records with same contact names.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the flag indication if we are searching for same phone for same contacts, but multiple records. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CntSamePhoneFlag() As Boolean
            Get
                Return _CntSamePhoneFlag
            End Get
            Set(ByVal Value As Boolean)
                _CntSamePhoneFlag = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _CntIgnoreNamesFlag As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the flag to indicate if we want to ignore some names in the search.
        ''' </summary>
        ''' <value>Returns the flag indicating if we want to ignore some contact names in the search.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the flag indication if we are trying to ignore some contact first and last names. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CntIgnoreNamesFlag() As Boolean
            Get
                Return _CntIgnoreNamesFlag
            End Get
            Set(ByVal Value As Boolean)
                _CntIgnoreNamesFlag = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _CntLastName As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the last name for ignoring while searching duplicates in contact records.
        ''' </summary>
        ''' <value>Returns the last name to eb ignored while searching for duplicates in the contact list.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the last name which is to be ignored searching duplicates in contacts. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CntLastName() As String
            Get
                Return _CntLastName
            End Get
            Set(ByVal Value As String)
                _CntLastName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _CntFirstName As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the first name for ignoring while searching duplicates in contact records.
        ''' </summary>
        ''' <value>Returns the first name to eb ignored while searching for duplicates in the contact list.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the first name which is to be ignored searching duplicates in contacts. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CntFirstName() As String
            Get
                Return _CntFirstName
            End Get
            Set(ByVal Value As String)
                _CntFirstName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        Private _CntMatchingFields As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the string for searching duplicates in contact records.
        ''' </summary>
        ''' <value>Returns the field which is being searched for duplicates in the contact list.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the field which is being used to search duplicates. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CntMatchingFields() As String
            Get
                Return _CntMatchingFields
            End Get
            Set(ByVal Value As String)
                _CntMatchingFields = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to search for Organization and return Results.
        ''' </summary>
        ''' <returns>Returns the result as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the result of duplicate org. search
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function PerformDuplicateCntSearch() As DataSet
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New getconnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}                         'create a param array                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@vcCRMTypes", NpgsqlTypes.NpgsqlDbType.VarChar, 5)
                arParms(0).Value = MyBase.SearchWithin

                arParms(1) = New Npgsql.NpgsqlParameter("@numCompanyType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = MyBase.FilterRelationship

                arParms(2) = New Npgsql.NpgsqlParameter("@vcOrgCreationStartDateRange", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(2).Value = MyBase.CreationDateStart

                arParms(3) = New Npgsql.NpgsqlParameter("@vcOrgCreationEndDateRange", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(3).Value = MyBase.CreationDateEnd

                arParms(4) = New Npgsql.NpgsqlParameter("@boolNoItemHistory", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = CntNoItemHistoryFlag

                arParms(5) = New Npgsql.NpgsqlParameter("@boolSameParentName", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = CntSameParentFlag

                arParms(6) = New Npgsql.NpgsqlParameter("@boolSameDivisionName", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = CntSameDivisionFlag

                arParms(7) = New Npgsql.NpgsqlParameter("@boolSamePhoneFlag", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = CntSamePhoneFlag

                arParms(8) = New Npgsql.NpgsqlParameter("@boolIgnoreNamesFlag", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = CntIgnoreNamesFlag

                arParms(9) = New Npgsql.NpgsqlParameter("@vcFirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(9).Value = CntFirstName

                arParms(10) = New Npgsql.NpgsqlParameter("@vcLastName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(10).Value = CntLastName

                arParms(11) = New Npgsql.NpgsqlParameter("@vcMatchingField", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(11).Value = CntMatchingFields

                arParms(12) = New Npgsql.NpgsqlParameter("@vcSortColumn", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(12).Value = SortDbColumnName

                arParms(13) = New Npgsql.NpgsqlParameter("@vcSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(13).Value = SortOrder

                arParms(14) = New Npgsql.NpgsqlParameter("@numNosRowsReturned", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(14).Value = PageSize

                arParms(15) = New Npgsql.NpgsqlParameter("@numCurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(15).Value = CurrentPageIndex

                arParms(16) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = DomainID

                ds = SqlDAL.ExecuteDataset(connString, "usp_PerformDuplicateCntSearch", arParms) 'execute and store to dataset
                ds.Tables(0).Columns.Add(New DataColumn("<a href='javascript: ToggleDivContCBSelection();' style='color:white;'>Select</a>", System.Type.GetType("System.String"), "'<input Type=CheckBox ID=cbSR' + [numContactID] + ' value=' + [numContactID] + '>'")) 'The last column will be checkboxes
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace