''' -----------------------------------------------------------------------------
''' Project	 : BACRM.BusinessLogic
''' Class	 : FormGenericLeadBox
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This class contains several methods and functions, which is used to 
'''     process and store the LeadBox information
''' </summary>
''' <remarks>
'''     This class is used to access the data and manage the same LeadBox 
''' </remarks>
''' <history>
''' 	[Debasish Tapan Nag]	07/31/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Option Explicit On 
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Admin
    Public Class FormGenericRegistration
        Inherits BACRM.BusinessLogic.CBusinessBase

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This call checks if the Email is duplicated
        ''' </summary>
        ''' <remarks>
        '''     When this registration form submits its is necessary to confirm that the email address is not duplicated
        '''     If Duplicated, then it alerts the user and requests if the password should be resent to the email address
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function CheckEmailDuplication(ByVal sContactEmail As String) As String
            Try
                Dim ds As DataSet                                                   'Declare a DataSet
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = sContactEmail

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_checkEmail", arParms))  'execute and store to string
            Catch Ex As Exception

            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This retireves the password
        ''' </summary>
        ''' <remarks>
        '''     User requests the password to be sent to the email address
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/29/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function GetPassword(ByVal sContactEmail As String) As String
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = sContactEmail

                Return CStr(SqlDAL.ExecuteScalar(connString, "usp_getPasswordFromEmail", arParms))  'execute and store to dataset                                                 'return datatable

            Catch Ex As Exception
            End Try
        End Function
    End Class
End Namespace
