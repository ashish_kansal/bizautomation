﻿Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Public Class EmailAlertConfig
    Inherits BACRM.BusinessLogic.CBusinessBase
    Private _AlertConfigId As Long
    'Private DomainId As Long
    Private _ContactId As Long
    Private _OpenActionItem As Boolean
    Private _OpenActionMsg As String
    Private _OpenCases As Boolean
    Private _OpenCasesMsg As String
    Private _OpenProject As Boolean
    Private _OpenProjectMsg As String
    Private _OpenSalesOpp As Boolean
    Private _OpenSalesOppMsg As String
    Private _OpenPurchaseOpp As Boolean
    Private _OpenPurchaseOppMsg As String
    Private _BalanceDue As Boolean
    Private _BalanceDueVal As Double
    Private _BalanceDueMsg As String
    Private _ARAccount As Boolean
    Private _ARAccountMsg As String
    Private _APAccount As Boolean
    Private _APAccountMsg As String
    Private _UnReadEmail As Boolean
    Private _UnReadEmailMsg As String
    Private _UnReadEmailVal As Integer
    Private _PurchasedPast As Boolean
    Private _PurchasePastVal As Double
    Private _PurchasedPastMsg As String
    Private _SoldPast As Boolean
    Private _SoldPastValue As Double
    Private _SoldPastMsg As String
    Private _CustomField As Boolean
    Private _CustomFieldId As Long
    Private _CustomFieldVal As Double
    Private _CustomFieldMsg As String
    Private _ShowCampaignAlert As Boolean
    'Private _UserContactID As Long

    Public Property AlertConfigId As Long
        Get
            Return _AlertConfigId
        End Get
        Set(ByVal value As Long)
            _AlertConfigId = value
        End Set
    End Property
    'Public Property DomainId As Long
    '    Get
    '        Return DomainId
    '    End Get
    '    Set(ByVal value As Long)
    '        DomainId = value
    '    End Set
    'End Property
    Public Property ContactId As Long
        Get
            Return _ContactId
        End Get
        Set(ByVal value As Long)
            _ContactId = value
        End Set
    End Property
    Public Property OpenActionItem As Boolean
        Get
            Return _OpenActionItem
        End Get
        Set(ByVal value As Boolean)
            _OpenActionItem = value
        End Set
    End Property
    Public Property OpenActionMsg As String
        Get
            Return _OpenActionMsg
        End Get
        Set(ByVal value As String)
            _OpenActionMsg = value
        End Set
    End Property
    Public Property OpenCases As Boolean
        Get
            Return _OpenCases
        End Get
        Set(ByVal value As Boolean)
            _OpenCases = value
        End Set
    End Property
    Public Property OpenCasesMsg As String
        Get
            Return _OpenCasesMsg
        End Get
        Set(ByVal value As String)
            _OpenCasesMsg = value
        End Set
    End Property
    Public Property OpenProject As Boolean
        Get
            Return _OpenProject
        End Get
        Set(ByVal value As Boolean)
            _OpenProject = value
        End Set
    End Property
    Public Property OpenProjectMsg As String
        Get
            Return _OpenProjectMsg
        End Get
        Set(ByVal value As String)
            _OpenProjectMsg = value
        End Set
    End Property
    Public Property OpenSalesOpp As Boolean
        Get
            Return _OpenSalesOpp
        End Get
        Set(ByVal value As Boolean)
            _OpenSalesOpp = value
        End Set
    End Property
    Public Property OpenSalesOppMsg As String
        Get
            Return _OpenSalesOppMsg
        End Get
        Set(ByVal value As String)
            _OpenSalesOppMsg = value
        End Set
    End Property
    Public Property OpenPurchaseOpp As Boolean
        Get
            Return _OpenPurchaseOpp
        End Get
        Set(ByVal value As Boolean)
            _OpenPurchaseOpp = value
        End Set
    End Property
    Public Property OpenPurchaseOppMsg As String
        Get
            Return _OpenPurchaseOppMsg
        End Get
        Set(ByVal value As String)
            _OpenPurchaseOppMsg = value
        End Set
    End Property
    Public Property BalanceDue As Boolean
        Get
            Return _BalanceDue
        End Get
        Set(ByVal value As Boolean)
            _BalanceDue = value
        End Set
    End Property
    Public Property BalanceDueVal As Double
        Get
            Return _BalanceDueVal
        End Get
        Set(ByVal value As Double)
            _BalanceDueVal = value
        End Set
    End Property
    Public Property BalanceDueMsg As String
        Get
            Return _BalanceDueMsg
        End Get
        Set(ByVal value As String)
            _BalanceDueMsg = value
        End Set
    End Property
    Public Property ARAccount As Boolean
        Get
            Return _ARAccount
        End Get
        Set(ByVal value As Boolean)
            _ARAccount = value
        End Set
    End Property
    Public Property ARAccountMsg As String
        Get
            Return _ARAccountMsg
        End Get
        Set(ByVal value As String)
            _ARAccountMsg = value
        End Set
    End Property
    Public Property APAccount As Boolean
        Get
            Return _APAccount
        End Get
        Set(ByVal value As Boolean)
            _APAccount = value
        End Set
    End Property
    Public Property APAccountMsg As String
        Get
            Return _ARAccountMsg
        End Get
        Set(ByVal value As String)
            _APAccountMsg = value
        End Set
    End Property
    Public Property UnReadEmail As Boolean
        Get
            Return _UnReadEmail
        End Get
        Set(ByVal value As Boolean)
            _UnReadEmail = value
        End Set
    End Property
    Public Property UnReadEmailVal As Integer
        Get
            Return _UnReadEmailVal
        End Get
        Set(ByVal value As Integer)
            _UnReadEmailVal = value
        End Set
    End Property
    Public Property UnReadEmailMsg As String
        Get
            Return _UnReadEmailMsg
        End Get
        Set(ByVal value As String)
            _UnReadEmailMsg = value
        End Set
    End Property
    Public Property PurchasedPast As Boolean
        Get
            Return _PurchasedPast
        End Get
        Set(ByVal value As Boolean)
            _PurchasedPast = value
        End Set
    End Property
    Public Property PurchasedPastVal As Double
        Get
            Return _PurchasePastVal
        End Get
        Set(ByVal value As Double)
            _PurchasePastVal = value
        End Set
    End Property
    Public Property PurchasedPastMsg As String
        Get
            Return _PurchasedPastMsg
        End Get
        Set(ByVal value As String)
            _PurchasedPastMsg = value
        End Set
    End Property
    Public Property SoldPast As Boolean
        Get
            Return _SoldPast
        End Get
        Set(ByVal value As Boolean)
            _SoldPast = value
        End Set
    End Property
    Public Property SoldPastValue As Double
        Get
            Return _SoldPastValue
        End Get
        Set(ByVal value As Double)
            _SoldPastValue = value
        End Set
    End Property
    Public Property SoldPastMsg As String
        Get
            Return _SoldPastMsg
        End Get
        Set(ByVal value As String)
            _SoldPastMsg = value
        End Set
    End Property
    Public Property CustomField As Boolean
        Get
            Return _CustomField
        End Get
        Set(ByVal value As Boolean)
            _CustomField = value
        End Set
    End Property
    Public Property CustomFieldVal As Double
        Get
            Return _CustomFieldVal
        End Get
        Set(ByVal value As Double)
            _CustomFieldVal = value
        End Set
    End Property
    Public Property CustomFieldMsg As String
        Get
            Return _CustomFieldMsg
        End Get
        Set(ByVal value As String)
            _CustomFieldMsg = value
        End Set
    End Property
    Public Property CustomFieldId As Long
        Get
            Return _CustomFieldId
        End Get
        Set(ByVal value As Long)
            _CustomFieldId = value
        End Set
    End Property
    Public Property ShowCampaignAlert As Boolean
        Get
            Return _ShowCampaignAlert
        End Get
        Set(ByVal value As Boolean)
            _ShowCampaignAlert = value
        End Set
    End Property

    Public Function ManageAlertConfig() As Boolean
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(29) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numAlertConfigId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Direction = ParameterDirection.InputOutput
            arParms(0).Value = _AlertConfigId

            arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = DomainID

            arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(2).Value = _ContactId

            arParms(3) = New Npgsql.NpgsqlParameter("@bitOpenActionItem", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(3).Value = _OpenActionItem

            arParms(4) = New Npgsql.NpgsqlParameter("@vcOpenActionMsg", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
            arParms(4).Value = _OpenActionMsg

            arParms(5) = New Npgsql.NpgsqlParameter("@bitOpenCases", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(5).Value = _OpenCases

            arParms(6) = New Npgsql.NpgsqlParameter("@vcOpenCasesMsg", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
            arParms(6).Value = _OpenCasesMsg

            arParms(7) = New Npgsql.NpgsqlParameter("@bitOpenProject", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(7).Value = _OpenProject

            arParms(8) = New Npgsql.NpgsqlParameter("@vcOpenProjectMsg", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
            arParms(8).Value = _OpenProjectMsg

            arParms(9) = New Npgsql.NpgsqlParameter("@bitOpenSalesOpp", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(9).Value = _OpenSalesOpp

            arParms(10) = New Npgsql.NpgsqlParameter("@vcOpenSalesOppMsg", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
            arParms(10).Value = _OpenSalesOppMsg

            arParms(11) = New Npgsql.NpgsqlParameter("@bitOpenPurchaseOpp", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(11).Value = _OpenPurchaseOpp

            arParms(12) = New Npgsql.NpgsqlParameter("@vcOpenPurchaseOppMsg", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
            arParms(12).Value = _OpenPurchaseOppMsg

            arParms(13) = New Npgsql.NpgsqlParameter("@bitBalancedue", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(13).Value = _BalanceDue

            arParms(14) = New Npgsql.NpgsqlParameter("@monBalancedue", NpgsqlTypes.NpgsqlDbType.Numeric)
            arParms(14).Value = _BalanceDueVal

            arParms(15) = New Npgsql.NpgsqlParameter("@vcBalancedueMsg", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
            arParms(15).Value = _BalanceDueMsg

            arParms(16) = New Npgsql.NpgsqlParameter("@bitUnreadEmail", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(16).Value = _UnReadEmail

            arParms(17) = New Npgsql.NpgsqlParameter("@intUnreadEmail", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(17).Value = _UnReadEmailVal

            arParms(18) = New Npgsql.NpgsqlParameter("@vcUnreadEmailMsg", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(18).Value = _UnReadEmailMsg

            arParms(19) = New Npgsql.NpgsqlParameter("@bitPurchasedPast", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(19).Value = _PurchasedPast

            arParms(20) = New Npgsql.NpgsqlParameter("@monPurchasedPast", NpgsqlTypes.NpgsqlDbType.Numeric)
            arParms(20).Value = _PurchasePastVal

            arParms(21) = New Npgsql.NpgsqlParameter("@vcPurchasedPastMsg", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(21).Value = _PurchasedPastMsg

            arParms(22) = New Npgsql.NpgsqlParameter("@bitSoldPast", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(22).Value = _SoldPast

            arParms(23) = New Npgsql.NpgsqlParameter("@monSoldPast", NpgsqlTypes.NpgsqlDbType.Numeric)
            arParms(23).Value = _SoldPastValue

            arParms(24) = New Npgsql.NpgsqlParameter("@vcSoldPastMsg", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(24).Value = _SoldPastMsg

            arParms(25) = New Npgsql.NpgsqlParameter("@bitCustomField", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(25).Value = _CustomField

            arParms(26) = New Npgsql.NpgsqlParameter("@numCustomFieldId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(26).Value = _CustomFieldId

            arParms(27) = New Npgsql.NpgsqlParameter("@vcCustomFieldValue", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(27).Value = _CustomFieldVal

            arParms(28) = New Npgsql.NpgsqlParameter("@vcCustomFieldMsg", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(28).Value = _CustomFieldMsg

            arParms(29) = New Npgsql.NpgsqlParameter("@bitCampaign", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(29).Value = _ShowCampaignAlert

            Dim objParam() As Object
            objParam = arParms.ToArray()
            SqlDAL.ExecuteNonQuery(connString, "USP_ManageAlertConfig", objParam, True)
            _AlertConfigId = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

            If _AlertConfigId <= 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetAlertConfig() As DataTable
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = DomainID

            arParms(1) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = _ContactId

            arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(2).Value = Nothing
            arParms(2).Direction = ParameterDirection.InputOutput

            Return SqlDAL.ExecuteDataset(connString, "USP_GetAlertConfig", arParms).Tables(0)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetEmailAlert() As DataTable
        Dim getconnection As New GetConnection
        Dim connString As String = getconnection.GetConnectionString
        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

        arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
        arParms(0).Value = DomainID

        arParms(1) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
        arParms(1).Value = _ContactId

        arParms(2) = New Npgsql.NpgsqlParameter("@numUserContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
        arParms(2).Value = UserCntID

        arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
        arParms(3).Value = Nothing
        arParms(3).Direction = ParameterDirection.InputOutput

        Return SqlDAL.ExecuteDataset(connString, "USP_GetEmailAlert", arParms).Tables(0)
    End Function
End Class
