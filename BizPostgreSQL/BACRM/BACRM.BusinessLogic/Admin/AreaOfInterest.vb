'created by Tarun juneja 
'Date 9/08/200

Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin

    Public Class AreaOfInterest
        Inherits BACRM.BusinessLogic.CBusinessBase

        Public Function GetAreaOfInterestList(ByVal domainID As String) As DataSet
            '================================================================================
            ' Purpose: This Procedure will query the DB to get the list of AOIs and then 
            '       display the same in the respective AOI List Box.
            '
            ' History
            ' Ver   Date        Author              Reason
            ' 1     29/04/2003  tarun juneja        created
            '
            ' Non Compliance (any deviation from standards)
            ' No deviations from the standards.
            '================================================================================

            Dim connString As String = ""
            Dim dsAOI As DataSet                                    ' holds all the list item from database
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

            ' exception block
            Try
                connString = GetConnection.GetConnectionString      ' get sql connection string

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = domainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                dsAOI = SqlDAL.ExecuteDataset(connString, "usp_GetAOIs", arParms)
            Catch ex As Exception
                Throw ex
            End Try

            Return dsAOI

        End Function

        Public Sub RenameAreaOfInterestList(ByVal numAOIID As String, ByVal vcNewName As String, ByVal numUserID As String)
            '================================================================================
            ' Purpose: This Procedure will query the DB to Rename the list of AOIs 
            '
            ' History
            ' Ver   Date        Author              Reason
            ' 1     29/04/2003  tarun juneja        created
            '
            ' Non Compliance (any deviation from standards)
            ' No deviations from the standards.
            '================================================================================

            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString

            Dim inputParams(3) As Npgsql.NpgsqlParameter

            Try
                connString = getconnection.GetConnectionString  '' get connection string
                ' load inputs params 
                inputParams(0) = New Npgsql.NpgsqlParameter()
                inputParams(0).ParameterName = "@numAOIID"
                inputParams(0).Value = numAOIID
                inputParams(1) = New Npgsql.NpgsqlParameter()
                inputParams(1).ParameterName = "@vcNewName"
                inputParams(1).Value = vcNewName
                inputParams(2) = New Npgsql.NpgsqlParameter()
                inputParams(2).ParameterName = "@numUserID"
                inputParams(2).Value = numUserID
                ' Update database      
                SqlDAL.ExecuteScalar(connString, "usp_RenameAOI", inputParams)
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Sub InsertIntoAreaOfInterestList(ByVal vcAOIName As String, ByVal numUserID As String, ByVal numDomainID As String)
            '================================================================================
            ' Purpose: This Procedure will insert the values to the DB 
            '
            ' History
            ' Ver   Date        Author              Reason
            ' 1     29/04/2003  tarun juneja        created
            '
            ' Non Compliance (any deviation from standards)
            ' No deviations from the standards.
            '================================================================================

            Dim getconnection As New GetConnection
            Dim connString As String = ""

            Dim inputParams(3) As Npgsql.NpgsqlParameter

            Try
                connString = getconnection.GetConnectionString  ' get connection
                ' create input params
                inputParams(0) = New Npgsql.NpgsqlParameter()
                inputParams(0).ParameterName = "@vcAOIName"
                inputParams(0).Value = vcAOIName
                inputParams(1) = New Npgsql.NpgsqlParameter()
                inputParams(1).ParameterName = "@numUserID"
                inputParams(1).Value = vcAOIName
                inputParams(2) = New Npgsql.NpgsqlParameter()
                inputParams(2).ParameterName = "@numDomainID"
                inputParams(2).Value = numDomainID
                ' update Database
                SqlDAL.ExecuteScalar(connString, "usp_InsertAOI", inputParams)
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Sub DeleteAreaOfInterestListValue() 'ByVal numAOIID As String, ByVal numUserID As String, ByVal bintTimeStamp As DateTime)
            '================================================================================
            ' Purpose: This Procedure will insert the area of interest values 
            '
            ' History
            ' Ver   Date        Author              Reason
            ' 1     29/04/2003  tarun juneja        created
            '
            ' Non Compliance (any deviation from standards)
            ' No deviations from the standards.
            '================================================================================

            Dim getconnection As New GetConnection
            Dim connString As String = ""
            'Dim inputParams(3) As Npgsql.NpgsqlParameter

            Try
                connString = getconnection.GetConnectionString  ' get connection
                ' load input params
                'inputParams(0) = New Npgsql.NpgsqlParameter()
                'inputParams(0).ParameterName = "@numAOIID"
                'inputParams(0).Value = numAOIID
                'inputParams(1) = New Npgsql.NpgsqlParameter()
                'inputParams(1).ParameterName = "@numUserID"
                'inputParams(1).Value = numUserID
                'inputParams(2) = New Npgsql.NpgsqlParameter()
                'inputParams(2).ParameterName = "@bintTimeStamp"
                'inputParams(2).Value = bintTimeStamp
                ' update database
                SqlDAL.ExecuteScalar(connString, "usp_DeleteAOI") ', inputParams)

            Catch ex As Exception
                Throw ex
            End Try

        End Sub

    End Class

End Namespace