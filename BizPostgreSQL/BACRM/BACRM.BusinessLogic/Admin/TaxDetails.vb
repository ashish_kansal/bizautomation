
Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Reflection

Namespace BACRM.BusinessLogic.Admin

    Public Class TaxDetails
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _Country As Long = 0
        Private _State As Long = 0
        Private _Tax As Decimal = 0
        Private _Ship As Boolean = False
        Private _TaxId As Long = 0
        'Private DomainId As Long
        Private _mode As Short = 0
        Private _TaxItemID As Long
        Private _TaxName As String
        Private _OppID As Long
        Private _DivisionID As Long
        Private _OppBizDocID As Long


        Public Property OppBizDocID() As Long
            Get
                Return _OppBizDocID
            End Get
            Set(ByVal Value As Long)
                _OppBizDocID = Value
            End Set
        End Property


        Private _AccountId As Long
        Public Property AccountId() As Long
            Get
                Return _AccountId
            End Get
            Set(ByVal value As Long)
                _AccountId = value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property


        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal Value As Long)
                _OppID = Value
            End Set
        End Property


        Public Property TaxName() As String
            Get
                Return _TaxName
            End Get
            Set(ByVal Value As String)
                _TaxName = Value
            End Set
        End Property


        Public Property TaxItemID() As Long
            Get
                Return _TaxItemID
            End Get
            Set(ByVal Value As Long)
                _TaxItemID = Value
            End Set
        End Property


        Public Property mode() As Short
            Get
                Return _mode
            End Get
            Set(ByVal Value As Short)
                _mode = Value
            End Set
        End Property
        Public Property Country() As Long
            Get
                Return _Country
            End Get
            Set(ByVal Value As Long)
                _Country = Value
            End Set
        End Property
        Public Property State() As Long
            Get
                Return _State
            End Get
            Set(ByVal Value As Long)
                _State = Value
            End Set
        End Property
        Public Property Tax() As Decimal
            Get
                Return _Tax
            End Get
            Set(ByVal Value As Decimal)
                _Tax = Value
            End Set
        End Property

        Public Property Ship() As Boolean
            Get
                Return _Ship
            End Get
            Set(ByVal Value As Boolean)
                _Ship = Value
            End Set
        End Property
        Public Property TaxId() As Long
            Get
                Return _TaxId
            End Get
            Set(ByVal Value As Long)
                _TaxId = Value
            End Set
        End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Private _City As String
        Public Property City() As String
            Get
                Return _City
            End Get
            Set(ByVal Value As String)
                _City = Value
            End Set
        End Property

        Private _ZipPostal As String
        Public Property ZipPostal() As String
            Get
                Return _ZipPostal
            End Get
            Set(ByVal Value As String)
                _ZipPostal = Value
            End Set
        End Property

        Private _BaseTax As Short
        Public Property BaseTax() As Short
            Get
                Return _BaseTax
            End Get
            Set(ByVal value As Short)
                _BaseTax = value
            End Set
        End Property

        Private _BaseTaxOnArea As Short
        Public Property BaseTaxOnArea() As Short
            Get
                Return _BaseTaxOnArea
            End Get
            Set(ByVal value As Short)
                _BaseTaxOnArea = value
            End Set
        End Property

        ''' <summary>
        ''' Tax Value Type
        ''' </summary>
        ''' <value>1 - Percentage, 2 - Flat Amount</value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property TaxValueType As Short

        Public Property TaxUniqueName As String

        Public Function ManageTaxDetails() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _Country

                arParms(1) = New Npgsql.NpgsqlParameter("@numState", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _State

                arParms(2) = New Npgsql.NpgsqlParameter("@decTax", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _Tax
                arParms(2).Precision = 18
                arParms(2).Scale = 4

                arParms(3) = New Npgsql.NpgsqlParameter("@numTaxId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _TaxId

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainId

                arParms(5) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _mode

                arParms(6) = New Npgsql.NpgsqlParameter("@numTaxItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _TaxItemID

                arParms(7) = New Npgsql.NpgsqlParameter("@vcCity", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = _City

                arParms(8) = New Npgsql.NpgsqlParameter("@vcZipPostal", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(8).Value = _ZipPostal

                arParms(9) = New Npgsql.NpgsqlParameter("@tintTaxType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = TaxValueType

                arParms(10) = New Npgsql.NpgsqlParameter("@vcTaxUniqueName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(10).Value = TaxUniqueName

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Return CType(SqlDAL.ExecuteScalar(connString, "usp_ManageTaxDetails", arParms), Integer)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetTaxDetails() As DataTable
            Try
                Dim ds As New DataSet
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numTaxItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _TaxItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _Country

                arParms(3) = New Npgsql.NpgsqlParameter("@numState", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _State

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetTaxDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ManageTaxItems() As Boolean
            Try
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTaxID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TaxItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcTaxName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _TaxName

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@numAccountID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _AccountId

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageTaxItem", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function GetTaxItems() As DataTable
            Try
                Dim ds As New DataSet
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetTaxItem", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function DeleteTaxItems() As Boolean
            Try
                Dim ds As New DataSet
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTaxItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TaxItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteTaxItem", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetTaxAmtOppForTaxItem1() As Decimal
            Try
                Dim ds As New DataSet
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numTaxItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _TaxItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _OppBizDocID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _mode

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return CDec(SqlDAL.ExecuteScalar(connString, "USP_GetTaxAmountOppForTaxItem", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ManageTaxCountryConfiguration(Of T)(Optional ByVal TaxCountryConfi As Long = 0) As DataTable
            Try
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _mode

                arParms(2) = New Npgsql.NpgsqlParameter("@numTaxCountryConfi", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = TaxCountryConfi

                arParms(3) = New Npgsql.NpgsqlParameter("@numCountry", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = _Country

                arParms(4) = New Npgsql.NpgsqlParameter("@tintBaseTax", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _BaseTax

                arParms(5) = New Npgsql.NpgsqlParameter("@tintBaseTaxOnArea", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _BaseTaxOnArea

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_ManageTaxCountryConfiguration", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCRVTaxTypes(ByVal itemCode As Long) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = itemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetCRVTaxDetails", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class
End Namespace