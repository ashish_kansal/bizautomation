'Created Anoop Jayaraj
Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin

    Public Class ApprovalConfig
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _UserId As Long
      
        Public Property UserId() As Long
            Get
                Return _UserId
            End Get
            Set(ByVal Value As Long)
                _UserId = Value
            End Set
        End Property

        
        Private _numDomainId As Long

        Public Property numDomainId() As Long
            Get
                Return _numDomainId
            End Get
            Set(ByVal Value As Long)
                _numDomainId = Value
            End Set
        End Property

        Private _numModuleId As Long

        Public Property numModuleId() As Long
            Get
                Return _numModuleId
            End Get
            Set(ByVal Value As Long)
                _numModuleId = Value
            End Set
        End Property

        Private _numConfigId As Long
        Public Property numConfigId() As Long
            Get
                Return _numConfigId
            End Get
            Set(ByVal Value As Long)
                _numConfigId = Value
            End Set
        End Property

        Private _ApprovalStatus As Long
        Public Property ApprovalStatus() As Long
            Get
                Return _ApprovalStatus
            End Get
            Set(ByVal Value As Long)
                _ApprovalStatus = Value
            End Set
        End Property

        Private _chrAction As String
        Public Property chrAction() As String
            Get
                Return _chrAction
            End Get
            Set(ByVal Value As String)
                _chrAction = Value
            End Set
        End Property

        Private _ClientTimeZoneOffset As Integer
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _strXml As String
        Public Property strXml() As String
            Get
                Return _strXml
            End Get
            Set(ByVal Value As String)
                _strXml = Value
            End Set
        End Property
        Private _strOutPut As String
        Public Property strOutPut() As String
            Get
                Return _strOutPut
            End Get
            Set(ByVal Value As String)
                _strOutPut = Value
            End Set
        End Property

        Private _numRecordId As Long
        Public Property numRecordId() As Long
            Get
                Return _numRecordId
            End Get
            Set(ByVal Value As Long)
                _numRecordId = Value
            End Set
        End Property
        Private _dtStartDate As New Nullable(Of Date)
        Public Property dtStartDate() As Nullable(Of Date)
            Get
                Return _dtStartDate
            End Get
            Set(ByVal Value As Nullable(Of Date))
                _dtStartDate = Value
            End Set
        End Property
        Private _dtEndDate As New Nullable(Of Date)
        Public Property dtEndDate() As Nullable(Of Date)
            Get
                Return _dtEndDate
            End Get
            Set(ByVal Value As Nullable(Of Date))
                _dtEndDate = Value
            End Set
        End Property
        Public Function GetAllUsers() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@chrAction", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(0).Value = _chrAction

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numDomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _UserId

                arParms(3) = New Npgsql.NpgsqlParameter("@numModuleId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _numModuleId

                arParms(4) = New Npgsql.NpgsqlParameter("@numConfigId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = _numConfigId

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetApprovalConfig", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SetAllUsers() As Boolean
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@chrAction", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(0).Value = _chrAction

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numDomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = _UserId

                arParms(3) = New Npgsql.NpgsqlParameter("@numModuleId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _numModuleId

                arParms(4) = New Npgsql.NpgsqlParameter("@numConfigId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = _numConfigId

                arParms(5) = New Npgsql.NpgsqlParameter("@vchXML", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _strXml

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "USP_SetApprovalConfig", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function PendingApproval() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numDomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@startDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _dtStartDate

                arParms(2) = New Npgsql.NpgsqlParameter("@endDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _dtEndDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _UserId

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetApprovalForTimeAndExpense", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateApprovalTransaction() As Boolean
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@chrAction", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(0).Value = _chrAction

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numDomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = _UserId

                arParms(3) = New Npgsql.NpgsqlParameter("@numModuleId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _numModuleId

                arParms(4) = New Npgsql.NpgsqlParameter("@numRecordId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = _numConfigId

                arParms(5) = New Npgsql.NpgsqlParameter("@numApprovedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _UserId

                arParms(6) = New Npgsql.NpgsqlParameter("@numApprovalComplete", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _ApprovalStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@vchoutput", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = _strOutPut
                arParms(7).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ApprovalProcess_Transaction", objParam, True)
                _strOutPut = Convert.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function PendingApprovalForTickler() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numDomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@startDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(1).Value = _dtStartDate

                arParms(2) = New Npgsql.NpgsqlParameter("@endDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _dtEndDate

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _UserId

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetApprovalForTickler", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetTimeExpenseByRecord() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numDomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRecordId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numRecordId

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetTimeAndExpensByRecordId", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
