﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Admin

    Public Class OrganizationRatingRule
        Inherits CBusinessBase

#Region "Public Methods"

        Public Sub Save(ByVal rules As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                    .Add(SqlDAL.Add_Parameter("@vcRules", rules, NpgsqlTypes.NpgsqlDbType.Text))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OrganizationRatingRule_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete(ByVal numORRID As Long)
            Try
                Try
                    Dim getconnection As New GetConnection
                    Dim connString As String = getconnection.GetConnectionString
                    Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                    With sqlParams
                        .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                        .Add(SqlDAL.Add_Parameter("@numORRID", numORRID, NpgsqlTypes.NpgsqlDbType.BigInt, 18))
                    End With

                    SqlDAL.ExecuteNonQuery(connString, "USP_OrganizationRatingRule_Delete", sqlParams.ToArray())
                Catch ex As Exception
                    Throw
                End Try
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetByDomainID() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 18))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_OrganizationRatingRule_Get", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

    End Class

End Namespace
