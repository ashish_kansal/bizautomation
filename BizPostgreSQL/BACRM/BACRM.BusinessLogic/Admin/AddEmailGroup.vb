Imports System.Data.SqlClient
Imports System.IO
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Admin
    Public Class BAddEmailGroup
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _ContactId As String = ""
        Private _CurrentPage As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _GetAll As Boolean
        Private _columnSearch As String
        Private _columnName As String
        Private _SortcolumnName As String
        Private _PageSize As Integer
        Private _WhereCondition As String
        Private _strContact As String = ""


        Property strContact() As String
            Get
                Return _strContact
            End Get
            Set(ByVal value As String)
                _strContact = value
            End Set
        End Property
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property
        Public Property ContactId() As String
            Get
                Return _ContactId
            End Get
            Set(ByVal Value As String)
                _ContactId = Value
            End Set
        End Property

        Public Property SortcolumnName() As String
            Get
                Return _SortcolumnName
            End Get
            Set(ByVal Value As String)
                _SortcolumnName = Value
            End Set
        End Property

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSearch() As String
            Get
                Return _columnSearch
            End Get
            Set(ByVal Value As String)
                _columnSearch = Value
            End Set
        End Property

        Public Property GetAll() As Boolean
            Get
                Return _GetAll
            End Get
            Set(ByVal Value As Boolean)
                _GetAll = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property
        Private _AreasOfInt As String

        Public Property AreasOfInt() As String
            Get
                Return _AreasOfInt
            End Get
            Set(ByVal Value As String)
                _AreasOfInt = Value
            End Set
        End Property
        Private _UserRightType As Short

        Public Property UserRightType() As Short
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Short)
                _UserRightType = Value
            End Set
        End Property
        Public Property QueryWhereCondition() As String
            Get
                Return _WhereCondition
            End Get
            Set(ByVal Value As String)
                _WhereCondition = Value
            End Set
        End Property
        'Private DomainId As Long

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        'Private UserCntID As Long

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property
        Private _AuthenticationGroupID As Long

        Public Property AuthenticationGroupID() As Long
            Get
                Return _AuthenticationGroupID
            End Get
            Set(ByVal Value As Long)
                _AuthenticationGroupID = Value
            End Set
        End Property
        Private _FormID As Long
        Public Property FormID() As Long
            Get
                Return _FormID
            End Get
            Set(ByVal Value As Long)
                _FormID = Value
            End Set
        End Property
        Private _ViewID As Int16
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the View ID.
        ''' </summary>
        ''' <value>Returns the ViewId as Int16.</value>
        ''' <remarks>
        '''     This property is used to Get/ Set the ViewId. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/24/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ViewID() As Int16
            Get
                Return _ViewID
            End Get
            Set(ByVal Value As Int16)
                _ViewID = Value
            End Set
        End Property
        Private _strGroupName As String
        Public Property strGroupName() As String
            Get
                Return _strGroupName
            End Get
            Set(ByVal Value As String)
                _strGroupName = Value
            End Set
        End Property
        Private _strGroupID As Integer = 0
        Public Property strGroupID() As Integer
            Get
                Return _strGroupID
            End Get
            Set(ByVal Value As Integer)
                _strGroupID = Value
            End Set
        End Property




        Public Function getContact() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ContactId", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = _ContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim dr As DataSet
                dr = SqlDAL.ExecuteDataset(connString, "USP_GetContactGroup", arParms)
                Return dr.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function getContactData() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strContact", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _ContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.Integer, 10)
                arParms(3).Value = _strGroupID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim dr As DataSet
                dr = SqlDAL.ExecuteDataset(connString, "USP_GetProfileEGroupData", arParms)
                Return dr.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function getEmailGroupContacts() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _strGroupID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetProfileEGroupContacts", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteEmailGroup() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numEmailGroup", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = strGroupID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteEmailGroup", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AdvancedSearch() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}                          'create a param array
                Dim ds As DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@WhereCondition", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(0).Value = _WhereCondition

                arParms(1) = New Npgsql.NpgsqlParameter("@ViewID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _ViewID                                                        'representing listmaster/ listdetails

                arParms(2) = New Npgsql.NpgsqlParameter("@AreasofInt", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _AreasOfInt

                arParms(3) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _UserRightType

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = DomainId

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _AuthenticationGroupID

                arParms(7) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _CurrentPage

                arParms(8) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _PageSize

                arParms(9) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Direction = ParameterDirection.InputOutput
                arParms(9).Value = _TotalRecords

                arParms(10) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(10).Value = _columnSortOrder

                arParms(11) = New Npgsql.NpgsqlParameter("@ColumnSearch", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(11).Value = _columnSearch

                arParms(12) = New Npgsql.NpgsqlParameter("@ColumnName", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(12).Value = _columnName

                arParms(13) = New Npgsql.NpgsqlParameter("@GetAll", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(13).Value = _GetAll

                arParms(14) = New Npgsql.NpgsqlParameter("@SortCharacter", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(14).Value = _SortCharacter

                arParms(15) = New Npgsql.NpgsqlParameter("@SortColumnName", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(15).Value = _SortcolumnName

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_AdvancedSearchGroup", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(9).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub Save()

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@strContact", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _ContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@strGroupName", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = _strGroupName

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.Integer, 10)
                arParms(4).Value = _strGroupID


                SqlDAL.ExecuteNonQuery(connString, "USP_SaveProfileEGroup", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Sub
        Public Function LoadDropdown() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId


                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim dr As DataSet
                dr = SqlDAL.ExecuteDataset(connString, "USP_ProfileGetGroup", arParms)

                Return dr.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function RemoveContact() As Integer
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString

            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strContact", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _ContactId


                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.Integer, 10)
                arParms(3).Value = _strGroupID

                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteContactGroupList", arParms)


            Catch ex As Exception
                Return False
                Throw ex
            Finally

            End Try

        End Function

    End Class
End Namespace

