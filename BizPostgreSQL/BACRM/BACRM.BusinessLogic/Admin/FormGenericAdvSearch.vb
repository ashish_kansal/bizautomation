''' -----------------------------------------------------------------------------
''' Project	 : BACRM.BusinessLogic
''' Class	 : FormGenericAdvSearch
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This class contains several methods and functions, which is used to 
'''     retirive and manage the Advance Search Results and Config.
''' </summary>
''' <remarks>
'''     This class is used to access the data and manage the same for Adv. Search.
''' </remarks>
''' <history>
''' 	[Debasish Tapan Nag]	08/12/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Option Explicit On
Imports System.Data.SqlClient
Imports System.IO
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Namespace BACRM.BusinessLogic.Admin
    Public Class FormGenericAdvSearch
        Private _CurrentPage As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _GetAll As Boolean
        Private _columnSearch As String
        Private _columnName As String
        Private _SortcolumnName As String
        Private _bitMassUpdate As Boolean
        Private _lookTable As String
        Private _strMassUpdate As String

        Public Property strMassUpdate() As String
            Get
                Return _strMassUpdate
            End Get
            Set(ByVal Value As String)
                _strMassUpdate = Value
            End Set
        End Property

        Public Property lookTable() As String
            Get
                Return _lookTable
            End Get
            Set(ByVal Value As String)
                _lookTable = Value
            End Set
        End Property

        Public Property bitMassUpdate() As Boolean
            Get
                Return _bitMassUpdate
            End Get
            Set(ByVal Value As Boolean)
                _bitMassUpdate = Value
            End Set
        End Property

        Public Property SortcolumnName() As String
            Get
                Return _SortcolumnName
            End Get
            Set(ByVal Value As String)
                _SortcolumnName = Value
            End Set
        End Property

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSearch() As String
            Get
                Return _columnSearch
            End Get
            Set(ByVal Value As String)
                _columnSearch = Value
            End Set
        End Property

        Public Property GetAll() As Boolean
            Get
                Return _GetAll
            End Get
            Set(ByVal Value As Boolean)
                _GetAll = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property
        Private _AreasOfInt As String

        Public Property AreasOfInt() As String
            Get
                Return _AreasOfInt
            End Get
            Set(ByVal Value As String)
                _AreasOfInt = Value
            End Set
        End Property
        Private _UserRightType As Short

        Public Property UserRightType() As Short
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Short)
                _UserRightType = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Advace search Columns's.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Advace search Columns's.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _strAreasInt As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Advace search Columns's.
        ''' </summary>
        ''' <value>Returns the Advace search Columns's as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Advace search Columns's. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property strAreasInt() As String
            Get
                Return _strAreasInt
            End Get
            Set(ByVal Value As String)
                _strAreasInt = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Advace search Columns's.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Advace search Columns's.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _strAdvSerCol As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Advace search Columns's.
        ''' </summary>
        ''' <value>Returns the Advace search Columns's as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Advace search Columns's. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property strAdvSerCol() As String
            Get
                Return _strAdvSerCol
            End Get
            Set(ByVal Value As String)
                _strAdvSerCol = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _DomainID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property DomainID() As Long
            Get
                Return _DomainID
            End Get
            Set(ByVal Value As Long)
                _DomainID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the userID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the user id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _UserCntID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User id.
        ''' </summary>
        ''' <value>Returns the user id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the User id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property UserCntID() As Long
            Get
                Return _UserCntID
            End Get
            Set(ByVal Value As Long)
                _UserCntID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the form id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the form id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _FormID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FormID() As Long
            Get
                Return _FormID
            End Get
            Set(ByVal Value As Long)
                _FormID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the AuthenticationGroupID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the AuthenticationGroupID.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _AuthenticationGroupID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets the Authentication Group ID.
        ''' </summary>
        ''' <value>Returns the AuthenticationGroupID as long.</value>
        ''' <remarks>
        '''     This property is used to Get the AuthenticationGroupID. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property AuthenticationGroupID() As Long
            Get
                Return _AuthenticationGroupID
            End Get
            Set(ByVal Value As Long)
                _AuthenticationGroupID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the View Id (1 to 5).
        ''' </summary>
        ''' <remarks>
        '''     This holds the View Id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/24/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ViewID As Int16
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the View ID.
        ''' </summary>
        ''' <value>Returns the ViewId as Int16.</value>
        ''' <remarks>
        '''     This property is used to Get/ Set the ViewId. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/24/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ViewID() As Int16
            Get
                Return _ViewID
            End Get
            Set(ByVal Value As Int16)
                _ViewID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Path to the XML file.
        ''' </summary>
        ''' <remarks>
        '''     This holds the path.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _sXMLFilePath As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets the XML file path.
        ''' </summary>
        ''' <value>Returns the path to the xml file as a string.</value>
        ''' <remarks>
        '''     This property is used to Get the path to the xml file which stores the column config/ group. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/12/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property XMLFilePath() As String
            Get
                Return _sXMLFilePath
            End Get
            Set(ByVal Value As String)
                _sXMLFilePath = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Page Size for Custom Paging.
        ''' </summary>
        ''' <remarks>
        '''     Set in the Config file.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _PageSize As Integer
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets the Page Size for Custom Paging.
        ''' </summary>
        ''' <value>Returns the Page Size.</value>
        ''' <remarks>
        '''     This property is used to Get the Custom Page Size from the Congif file. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the CurrentPageIndex.
        ''' </summary>
        ''' <remarks>
        '''     This holds the CurrentPageIndex.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _CurrentPageIndex As Integer
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets the current page index for custom paging.
        ''' </summary>
        ''' <value>Returns the current page index.</value>
        ''' <remarks>
        '''     This property is used to Set/ Get the current apge index from Congif file. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property CurrentPageIndex() As Integer
            Get
                Return _CurrentPageIndex
            End Get
            Set(ByVal Value As Integer)
                _CurrentPageIndex = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Filter String.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Filter String for displaying records in DataGrid.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _FilterString As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the filter string for displaying records.
        ''' </summary>
        ''' <value>Returns the string which is then matched for displayign records.</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FilterString() As String
            Get
                Return _FilterString
            End Get
            Set(ByVal Value As String)
                _FilterString = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Filter Column.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Filter Column for displaying records in DataGrid.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _FilterColumn As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the filter column for searching matching records for displaying records.
        ''' </summary>
        ''' <value>Returns the column where filter string is matched for displayign records.</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FilterColumn() As String
            Get
                Return _FilterColumn
            End Get
            Set(ByVal Value As String)
                _FilterColumn = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Grid Sort Order.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Sort Order.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _SortOrder As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the Sort Order which was used for displayign records.
        ''' </summary>
        ''' <value>Returns the order of sorting.</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SortOrder() As String
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As String)
                _SortOrder = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Column which is used for Sorting.
        ''' </summary>
        ''' <remarks>
        '''     This holds the column which is used for sorting and displaying records in DataGrid.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _SortDbColumnName As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the column which is filtered for displaying records.
        ''' </summary>
        ''' <value>Returns the Columns which is sorted for displayign records.</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SortDbColumnName() As String
            Get
                Return _SortDbColumnName
            End Get
            Set(ByVal Value As String)
                _SortDbColumnName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the columns to be returned by the query.
        ''' </summary>
        ''' <remarks>
        '''     This holds the columns to be returned by the query.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ColumnList As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the list of columns to be retiurned by the query.
        ''' </summary>
        ''' <value>The columsn as a string</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ColumnList() As String
            Get
                Return _ColumnList
            End Get
            Set(ByVal Value As String)
                _ColumnList = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the where clause for the query.
        ''' </summary>
        ''' <remarks>
        '''     This holds the where clause for the query.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/16/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _WhereCondition As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the where condition for the query (set from teh page frmGenericAdvanceSearch.aspx).
        ''' </summary>
        ''' <value>The where clause as a string</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property QueryWhereCondition() As String
            Get
                Return _WhereCondition
            End Get
            Set(ByVal Value As String)
                _WhereCondition = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the additional where condition for the query (set from teh page frmGenericAdvanceSearch.aspx).
        ''' </summary>
        ''' <value>The where clause as a string</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/29/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public ReadOnly Property QueryAdditionalWhereCondition() As String
            Get
                If Trim(FilterString) = "" Then
                    Return ""
                Else
                    Return " AND LTRIM(Convert(NVarchar, " & FilterColumn & ")) ILIKE '" & FilterString & "%'"
                End If
            End Get
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the search context.
        ''' </summary>
        ''' <remarks>
        '''     This holds the where clause for the query.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _SearchContext As Int16
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the search context for the query (set from teh page frmGenericAdvanceSearch.aspx).
        ''' </summary>
        ''' <value>The where clause as a string</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/19/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property SearchContext() As Int16
            Get
                Return _SearchContext
            End Get
            Set(ByVal Value As Int16)
                _SearchContext = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the flag to indicate if the Emails are to be opened in outlook or express.
        ''' </summary>
        ''' <remarks>
        '''     This holds the flag to decide the email client.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	05/12/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _EmailLink As Int16
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the flag to indicate if the Emails are to be opened in outlook or express.
        ''' </summary>
        ''' <value>This holds the flag to decide the email client.</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	05/12/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property EmailLink() As Int16
            Get
                Return _EmailLink
            End Get
            Set(ByVal Value As Int16)
                _EmailLink = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the custom field groups which .
        ''' </summary>
        ''' <remarks>
        '''     This holds the where clause for the query.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _CustomFieldAreas As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the areas for the query (set from teh page frmGenericAdvanceSearch.aspx).
        ''' </summary>
        ''' <value>The where clause as a string</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/19/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property QueryCustomFieldAreasCondition() As String
            Get
                Return _CustomFieldAreas
            End Get
            Set(ByVal Value As String)
                _CustomFieldAreas = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the custom field groups which .
        ''' </summary>
        ''' <remarks>
        '''     This holds the where clause for the query.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/19/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _CustomFieldKeyword As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the custom keyword for the query (set from teh page frmGenericAdvanceSearch.aspx).
        ''' </summary>
        ''' <value>The where clause as a string</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/19/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property QueryCustomFieldKeyword() As String
            Get
                Return _CustomFieldKeyword
            End Get
            Set(ByVal Value As String)
                _CustomFieldKeyword = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the flag to indicate if Primary Contacts are only included in Search Results or not
        ''' </summary>
        ''' <remarks>
        '''     This holds the boolean status: True if only Primary Contacts are included in search
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	03/21/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _IncludeOnlyPrimary As Boolean = False
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the status of the primary contact (only in search results) or all contacts.
        ''' </summary>
        ''' <value></value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	03/21/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property IncludeOnlyPrimary() As Boolean
            Get
                Return _IncludeOnlyPrimary
            End Get
            Set(ByVal Value As Boolean)
                _IncludeOnlyPrimary = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Survey Rating and Survey Ids for search.
        ''' </summary>
        ''' <remarks>
        '''     This holds the where clause for the query.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag] 03/24/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _RatingInSurveyIds As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets/ Sets the Survey Rating Information for the query (set from the page frmGenericAdvanceSearch.aspx).
        ''' </summary>
        ''' <value>The survey ids and rating clause as a string</value>
        ''' <remarks></remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag] 03/24/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property RatingInSurveyIds() As String
            Get
                Return _RatingInSurveyIds
            End Get
            Set(ByVal Value As String)
                _RatingInSurveyIds = Value
            End Set
        End Property

        Private _SearchQueryforDisplay As String
        Public Property SearchQueryforDisplay() As String
            Get
                Return _SearchQueryforDisplay
            End Get
            Set(ByVal value As String)
                _SearchQueryforDisplay = value
            End Set
        End Property

        Private _RegularSearchCriteria As String
        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property

        Private _CustomSearchCriteria As String
        Public Property CustomSearchCriteria() As String
            Get
                Return _CustomSearchCriteria
            End Get
            Set(ByVal Value As String)
                _CustomSearchCriteria = Value
            End Set
        End Property

        Private _SearchTxt As String
        Public Property SearchTxt() As String
            Get
                Return _SearchTxt
            End Get
            Set(ByVal Value As String)
                _SearchTxt = Value
            End Set
        End Property
        Public Property ClientTimeZoneOffset As Integer
        Public Property SearchConditions As String
        Public Property SharedWithUsers As String
        Public Property DisplayColumns As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the search fields for the seleted auth Group
        ''' </summary>
        ''' <returns>Returns the value as DataSet.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of selected fields.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/12/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSearchFieldList() As DataSet
            Try
                Dim ds As DataSet                                                                               'declare a dataset
                Dim getconnection As New GetConnection                                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}                                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@FormID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = FormID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcDisplayColumns", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(3).Value = DisplayColumns

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput


                ds = SqlDAL.ExecuteDataset(connString, "usp_getDynamicSearchFormFieldsForAGroup", arParms)      'execute and get the fields from teh dataset

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the search result in an XML file
        ''' </summary>
        ''' <param name="dsSearchResult">Represents the dataset containing the serch results.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/19/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveSearchResultDb(ByVal dsSearchResult As DataSet)
            Try
                Dim xmlOut As New System.IO.FileStream(XMLFilePath & "\AdvSearchResultRepository_" & UserCntID & ".xml", System.IO.FileMode.Create) 'Create A file system object
                Dim myXmlWriter As New System.Xml.XmlTextWriter(xmlOut, System.Text.Encoding.Unicode)   'create a xml writer
                dsSearchResult.WriteXml(myXmlWriter)                                                    'Write teh xml from teh dataser
                myXmlWriter.Close()                                                                     'Close the writer
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete the XML file containing the search result
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function DeleteSearchResultXML()
            Try
                XMLFilePath = XMLFilePath & "\AdvSearchResultRepository_" & UserCntID & ".xml"             'Set the path to teh XML file
                If File.Exists(XMLFilePath) Then                                                        'Check if the file exists
                    File.Delete(XMLFilePath)                                                            'Delete the file
                End If
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to read teh search result from the xml file
        ''' </summary>
        ''' <remarks>
        '''     Returns teh DataSet which contains teh search results
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/19/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function ReadSearchResultDb() As DataSet
            Try
                Dim dsXMLSearchResult As New DataSet                                                'Declare a Dataset object
                dsXMLSearchResult = getAdvSearchResult(getSelectedFieldList())                      'Add a default DataColumns
                dsXMLSearchResult.Tables(0).TableName = "SearchResult"                              'Set the name of the Search Result table
                Return dsXMLSearchResult                                                            'return dataset containign the search result
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the xml of fields into the server file
        ''' </summary>
        ''' <param name="sXMLString">Represents the xml string.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveSelectedFieldList(ByVal sXMLString As String)
            Dim StrWriter As StreamWriter                                       'Declare a StreamWriter object
            Dim strFileName As String                                           'Declare a string object
            strFileName = "DynamicSearchFieldConfig_" & DomainID & "_" & AuthenticationGroupID & "_" & ViewID & ".xml" 'Set the file name
            Try
                If Directory.Exists(XMLFilePath) = False Then                   'If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(XMLFilePath)
                End If

                StrWriter = File.CreateText(XMLFilePath & "\" & strFileName)    'create the flle
                StrWriter.Write(sXMLString)                                     'Write XML doument
                StrWriter.Close()                                               'close the object
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the xml of fields for simple into the server file
        ''' </summary>
        ''' <param name="sXMLString">Represents the xml string.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveSelectedSimpleSearchFieldList(ByVal sXMLString As String)
            Dim StrWriter As StreamWriter                                       'Declare a StreamWriter object
            Dim strFileName As String                                           'Declare a string object
            strFileName = "DynamicSimpleSearchFieldConfig_" & DomainID & "_" & AuthenticationGroupID & ".xml" 'Set the file name
            Try
                If Directory.Exists(XMLFilePath) = False Then                   'If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(XMLFilePath)
                End If

                StrWriter = File.CreateText(XMLFilePath & "\" & strFileName)    'create the flle
                StrWriter.Write(sXMLString)                                     'Write XML doument
                StrWriter.Close()                                               'close the object
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the field names of selected columns from the server file for Simple Search
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/08/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getSimpleSearchSelectedFieldList() As DataTable
            Dim dsXMLSearchColumnConfig As New DataSet                                          'Create a Dataset object
            Dim strFileName As String                                                           'Declare a string object
            strFileName = XMLFilePath & "\" & "DynamicFormConfig_" & DomainID & "_6_" & AuthenticationGroupID & ".xml" 'Set the file name
            If File.Exists(strFileName) = False Then
                strFileName = XMLFilePath & "\" & "DynamicFormConfig_" & DomainID & "_6_0.xml" 'Set the file name
            End If

            If File.Exists(strFileName) = False Then                                            'If file not exists exit
                Dim dtTable As New DataTable                                                    'create a new datatable
                dtTable.Columns.Add(New DataColumn("vcDbColumnName", System.Type.GetType("System.String"))) 'Add a column to store the db field name
                dtTable.Columns.Add(New DataColumn("vcNewFormFieldName", System.Type.GetType("System.String"))) 'Add a column to store the field label
                dsXMLSearchColumnConfig.Tables.Add(dtTable)                                     'Add table to the dataset
            ElseIf File.Exists(strFileName) = True Then
                Try
                    dsXMLSearchColumnConfig.ReadXml(strFileName)                                'Read DataSet into XML
                Catch ex As Exception                                                           'Error while reading file
                    Dim dtTable As New DataTable                                                'create a new datatable
                    dtTable.Columns.Add(New DataColumn("vcDbColumnName", System.Type.GetType("System.String"))) 'Add a column to store the db field name
                    dtTable.Columns.Add(New DataColumn("vcNewFormFieldName", System.Type.GetType("System.String"))) 'Add a column to store the field label
                    dsXMLSearchColumnConfig.Tables.Add(dtTable)                                 'Add table to the dataset
                End Try
            End If
            dsXMLSearchColumnConfig.Tables(0).TableName = "SimpleSearchFields4Group"            'Set the name of the table
            Return dsXMLSearchColumnConfig.Tables(0)                                            'return xml data table
        End Function

        Public Function GetSimpleSearchFields() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _AuthenticationGroupID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = _DomainID                                                         'representing listmaster/ listdetails

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_SimpleSearchfields", arParms)     'execute and store to dataset

                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try                                           'return xml data table
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the field names of selected columns from the server file
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/12/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getSelectedFieldList() As DataTable
            Dim dsXMLSearchColumnConfig As New DataSet                                          'Create a Dataset object
            Dim strFileName, strCustomFieldAreas As String                                      'Declare a string object
            strFileName = XMLFilePath & "\" & "DynamicSearchFieldConfig_" & DomainID & "_" & AuthenticationGroupID & "_" & IIf(ViewID = 0, 1, ViewID) & ".xml" 'Set the file name

            If File.Exists(strFileName) = False Then                                            'If file not exists exit
                Dim dtTable As New DataTable                                                    'create a new datatable
                dtTable.Columns.Add(New DataColumn("vcDbColumnName", System.Type.GetType("System.String"))) 'Add a column to store the db field name
                dtTable.Columns.Add(New DataColumn("vcFormFieldName", System.Type.GetType("System.String"))) 'Add a column to store the field label
                dsXMLSearchColumnConfig.Tables.Add(dtTable)                                     'Add table to the dataset
            ElseIf File.Exists(strFileName) = True Then
                Try
                    dsXMLSearchColumnConfig.ReadXml(strFileName)                                'Read DataSet into XML
                Catch ex As Exception                                                           'Error while reading file
                    Dim dtTable As New DataTable                                                'create a new datatable
                    dtTable.Columns.Add(New DataColumn("vcDbColumnName", System.Type.GetType("System.String"))) 'Add a column to store the db field name
                    dtTable.Columns.Add(New DataColumn("vcFormFieldName", System.Type.GetType("System.String"))) 'Add a column to store the field label
                    dsXMLSearchColumnConfig.Tables.Add(dtTable)                                 'Add table to the dataset
                End Try
            End If
            dsXMLSearchColumnConfig.Tables(0).TableName = "SearchColumns4Group"                 'Set the name of the table
            Return dsXMLSearchColumnConfig.Tables(0)                                            'return xml data table
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for the authentication groups dropdown for advance search.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the
        '''     list groups
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function getAuthenticationGroups() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAuthenticationGroupsBizForm", arParms) 'execute and store to dataset

                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for all list details for the dropdown.
        ''' </summary>
        ''' <returns>Returns the value as DataSet.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of list details
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/12/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getListDetails(ByVal iListItemId As Integer) As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = iListItemId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcItemType", NpgsqlTypes.NpgsqlDbType.Char, 3)
                arParms(1).Value = "LI"                                                          'representing listmaster/ listdetails

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetMasterListDetails", arParms)     'execute and store to dataset

                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function saves the selected list of teams.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>
        '''     This function calls the stored procedure that stores the list of teams selected by the user
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/13/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function saveTeamsForUser(ByVal sTeamContiningXML As String)
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcTerritoriesSelected", NpgsqlTypes.NpgsqlDbType.Text, 2000)
                arParms(2).Value = System.DBNull.Value.ToString()                               'representing the xml tring which contains the list of territories

                arParms(3) = New Npgsql.NpgsqlParameter("@vcTeamsSelected", NpgsqlTypes.NpgsqlDbType.Text, 2000)
                arParms(3).Value = sTeamContiningXML                                            'representing the xml tring which contains the list of teams

                Return SqlDAL.ExecuteNonQuery(connString, "usp_saveAdvSearchTeamTerritoryPreferences", arParms)     'execute and store to dataset
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function saves the selected list of territories.
        ''' </summary>
        ''' <param name="sTerritoriesContiningXML">XML containing the Territories selected by the user</param>
        ''' <returns></returns>
        ''' <remarks>
        '''     This function calls the stored procedure that stores the list of territories selected by the user
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/13/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function saveTerritoriesForUser(ByVal sTerritoriesContiningXML As String)
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcTerritoriesSelected", NpgsqlTypes.NpgsqlDbType.Text, 2000)
                arParms(2).Value = sTerritoriesContiningXML                                     'representing the xml tring which contains the list of territories

                arParms(3) = New Npgsql.NpgsqlParameter("@vcTeamsSelected", NpgsqlTypes.NpgsqlDbType.Text, 2000)
                arParms(3).Value = System.DBNull.Value.ToString()

                Return SqlDAL.ExecuteNonQuery(connString, "usp_saveAdvSearchTeamTerritoryPreferences", arParms)     'execute and store to dataset
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function retrieves the selected list of teams.
        ''' </summary>
        ''' <returns>The Table containing the Selected Teams for Adv Search</returns>
        ''' <remarks>
        '''     This function calls the stored procedure that retrieves the list of teams selected by the user
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/13/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSelectedTeamsForUser() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcTerritoryTeamFlag", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = "Teams"

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getAdvSearchTeamTerritoryPreferences", arParms)     'execute and store to dataset
                ds.Tables(0).TableName = "SelectedTeamUsers"
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function retrieves the selected list of territories.
        ''' </summary>
        ''' <returns>The Table containing the Selected Territories for Adv Search</returns>
        ''' <remarks>
        '''     This function calls the stored procedure that retrieves the list of territories selected by the user
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/13/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getSelectedTerritoriesForUser() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcTerritoryTeamFlag", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = "Terri"

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getAdvSearchTeamTerritoryPreferences", arParms)     'execute and store to dataset
                ds.Tables(0).TableName = "SelectedTerritoriesUsers"
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        '''<summary>
        '''     Removes duplicate rows from given DataTable
        '''</summary>
        '''<param name="tblOne">Table to scan for duplicate rows</param>
        '''<param name="tblTwo">Reference table for matchign duplicate rows</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Sub RemoveDuplicatesRows(ByRef tblOne As DataTable, ByVal tblTwo As DataTable, ByVal keyColumn As String)
            If tblTwo.Rows.Count > 0 Then                                                                                 'Do the following if there are records in the second table
                Dim objParentCol As DataColumn = tblOne.Columns(keyColumn)                                                'Create an object of the first column
                Dim objChildCol As DataColumn = tblTwo.Columns(keyColumn)                                                 'Create an object of the second column
                Dim objFieldIdRelation As DataRelation                                                                    'create relation object
                objFieldIdRelation = New DataRelation("FieldRelation", objParentCol, objChildCol, False)                  'create parent child relation 
                tblOne.DataSet.Relations.Add(objFieldIdRelation)                                                          'Add the relation to the dataset
                Dim objParentrow, objChildRow As DataRow
                For Each objChildRow In tblTwo.Rows                                                                       'loop through the rows in child table
                    Dim colParentRows() As DataRow = objChildRow.GetParentRows(objFieldIdRelation)                        'Create an array of parent rows
                    If colParentRows.Length > 0 Then
                        For Each objParentrow In colParentRows                                                            'Loop through the parent rows
                            tblOne.Rows.Remove(objParentrow)                                                              'Remove the Parent Row
                        Next
                    End If
                Next
            End If
        End Sub
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for AOI from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of AOI's.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getAOIList() As DataSet
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = AuthenticationGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetDynamicFormAOI", arParms) 'execute and store to dataset

                Return ds                                                    'return datatable

            Catch ex As Exception
                Throw ex
            End Try
        End Function





        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for follow up status from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the list
        '''     of follow up stauts.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/24/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getFollowUpStatus(ByVal numFollowUpStatus As Integer) As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ListID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = numFollowUpStatus

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMasterListItems", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values available employees from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the available employee list
        '''     in a datatable
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getAvailableEmployees() As DataTable
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRoutID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = -1

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCircularDistribution", arParms)  'execute and store to dataset

                Return ds.Tables(0)                                                 'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the search results for the datagrid
        ''' </summary>
        ''' <remarks>
        '''     This function performs a search according to the specified criteria and returns the result in a datatable
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/15/2005	Created
        ''' 	[Debasish Tapan Nag]	01/31/2006	Modified    Optimization Effort
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getAdvSearchResult(ByVal dtSearchColumns4Group As DataTable) As DataSet
            Dim dsSearchResults As DataSet                                                                      'Declare a DataSet to store both the Search Result and the nos of records
            Dim dtSearchResults As DataTable                                                                    'Declare a DataTable to store the Result of Search
            Dim dtSearchResultsCount As DataTable                                                               'Declare a DataTable to store the nos of records
            Dim dtSearchResultsCoalesce As DataTable                                                               'Declare a DataTable to store the nos of records
            If dtSearchColumns4Group.Rows.Count = 0 Then
                dtSearchResults = New DataTable                                                                 'Create an Instance of the DataTable
                dsSearchResults = New DataSet                                                                   'Instantiate teh dataset
                dtSearchResults.Columns.Add(New DataColumn("numCompanyId", System.Type.GetType("System.String"))) 'Create a new column
                dtSearchResults.Columns.Add(New DataColumn("numContactId", System.Type.GetType("System.String"))) 'Create a new column
                dtSearchResults.Columns.Add(New DataColumn("numDivisionId", System.Type.GetType("System.String"))) 'Create a new column
                dtSearchResults.Columns.Add(New DataColumn("tIntCrmType", System.Type.GetType("System.String"))) 'Create a new column
                dtSearchResults.Columns.Add(New DataColumn("The Search Results Layout has not been configured.", System.Type.GetType("System.String"))) 'Create a new column
                dsSearchResults.Tables.Add(dtSearchResults)                                                     'Add this table to the DataSet

                dtSearchResultsCount = New DataTable                                                            'Create an Instance of the DataTable
                dtSearchResultsCount.Columns.Add(New DataColumn("RowCountRecords", System.Type.GetType("System.String"))) 'Create a new column
                dtSearchResultsCount.Rows.Add(dtSearchResultsCount.NewRow())                                    'Add a new row
                dtSearchResultsCount.Rows(0).Item(0) = 0                                                        'No rows
                dsSearchResults.Tables.Add(dtSearchResultsCount)                                                'Add this table to the DataSet

                dtSearchResultsCoalesce = New DataTable                                                            'Create an Instance of the DataTable
                dtSearchResultsCoalesce.Columns.Add(New DataColumn("vcCoalesce", System.Type.GetType("System.String"))) 'Create a new column
                dsSearchResults.Tables.Add(dtSearchResultsCoalesce)                                                'Add this table to the DataSet

                Dim sColumnsForDisplay As New Text.StringBuilder                                                'Declare the variable which contains the columns for teh search result
                sColumnsForDisplay.Append("numCompanyId, numContactId, numDivisionId, tIntCrmType")             'Teh first columns are fixed and always picked up by default
                ColumnList = sColumnsForDisplay.ToString()                                                      'Set the Column List
            Else                                                                                                'Column configuraiton exists for the Group
                Dim dtViewColumnCollection As DataTable                                                         'Declare a DataTable which will contain the columns from the view
                Dim sColumnsForSearch As New Text.StringBuilder                                                 'Declare the variable which contains the columns from teh view
                dtViewColumnCollection = GetColumnsFromView()                                                   'Get the collection of columns
                Dim dcColumnList As DataColumn                                                                  'Declare a DataColumn
                For Each dcColumnList In dtViewColumnCollection.Columns
                    sColumnsForSearch.Append("," & dcColumnList.ColumnName)                                     'Create a comma separated list of columns
                Next
                Dim sColumnsArrayForSearch As String()                                                          'Declare the Array which contains the columns from teh view
                sColumnsArrayForSearch = sColumnsForSearch.ToString.Split(",")                                  'Create a array of column names
                Array.Sort(sColumnsArrayForSearch)                                                              'Sort of faster search

                Dim sColumnsForDisplay As New Text.StringBuilder                                                'Declare the variable which contains the columns for teh search result
                sColumnsForDisplay.Append("numCompanyId, numContactId, numDivisionId, tIntCrmType")             'Teh first columns are fixed and always picked up by default
                Dim drSearchResults As DataRow                                                                  'Declare a DataRow
                Dim vcDbColumnName As String                                                                    'Declare a String which will contain the db coluumn name
                For Each drSearchResults In dtSearchColumns4Group.Rows
                    If Array.BinarySearch(sColumnsArrayForSearch, drSearchResults.Item("vcDbColumnName")) > 0 Then
                        vcDbColumnName = Replace(Replace(Replace(Replace(Replace(Replace(drSearchResults.Item("vcDbColumnName"), "num", "vc"), "mon", "vc"), "tInt", "vc"), "int", "vc"), "char", "vc"), "bint", "vc") 'Pick up the db column name on a local variable
                        sColumnsForDisplay.Append("," & vcDbColumnName)                                         'Create a comma separated list of columns which will be displayed
                    End If
                Next
                ColumnList = sColumnsForDisplay.ToString()                                                      'Set the Column Listz
                'dsSearchResults = filterSearchData()                                                            'Get the Search Result
                dtSearchResults = dsSearchResults.Tables(0)                                                     'The first table contains the Search Results
                dtSearchResults.Columns("cbBox").ColumnName = "<input type=checkbox name=cbCheckAll onclick='javascript: CheckAll(this.checked);'>"
            End If
            dsSearchResults.Tables(0).TableName = "SearchResult"                                                'Give a understandable name to the table which contains the search results
            dsSearchResults.Tables(1).TableName = "SearchResultCount"                                       'Give a understandable name to the table which containg the nos of matching records
            dsSearchResults.Tables(2).TableName = "SearchResultCoalesce"                                    'Give a understandable name to the table which contains the coalesce ids
            Return dsSearchResults
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function deletes the selected records
        ''' </summary>
        ''' <remarks>
        '''     This function deletes the selected records
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/22/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function GetColumnsFromView() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(0).Value = Nothing
                arparms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSearchableColumns4SearchRecords", arparms)  'execute and get the columns from the view
                Return (ds.Tables(0))
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function changes the record owner for the selected records (Company Ids)
        ''' </summary>
        ''' <remarks>
        '''     This function saves the new owner for the selected records
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function ChangeRecordOwner(ByVal vcCompIdContIdDivIdList As String, ByVal numOwnerId As Integer, ByVal boolIncludeContactRecords As Boolean)
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcEntityIdList", NpgsqlTypes.NpgsqlDbType.Varchar, 3000)
                arParms(0).Value = vcCompIdContIdDivIdList

                arParms(1) = New Npgsql.NpgsqlParameter("@numOwnerId", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = numOwnerId

                arParms(2) = New Npgsql.NpgsqlParameter("@bitIncludeContactRecords", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = boolIncludeContactRecords

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "usp_SetRecordOwnersForSelectedRecords", arParms)  'execute and store to dataset

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function changes the followup status name for the selected records (Company Ids)
        ''' </summary>
        ''' <remarks>
        '''     This function saves the new follow up up status names for the selected records
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        ''' Obsolete
        Public Function SaveFollowUpStatus(ByVal vcCompIdContIdDivIdList As String, ByVal numFollowUpStatusId As Integer)
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcEntityIdList", NpgsqlTypes.NpgsqlDbType.Varchar, 3000)
                arParms(0).Value = vcCompIdContIdDivIdList

                arParms(1) = New Npgsql.NpgsqlParameter("@numFollowUpStatusId", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = numFollowUpStatusId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "usp_SetFollowUpStatusForSelectedRecords", arParms)  'execute and store to dataset

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SetDripCampaign(ByVal ContactIDList As String, ByVal ECampaignID As Long)
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcContactIDList", NpgsqlTypes.NpgsqlDbType.Varchar, 3000)
                arParms(0).Value = ContactIDList

                arParms(1) = New Npgsql.NpgsqlParameter("@numECampaignID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = ECampaignID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "usp_SetDripCampaign", arParms)  'execute and store to dataset

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function UpdateOrgCampaigns(ByVal DivIDList As String, ByVal CampaignID As Long)
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcContactIDList", NpgsqlTypes.NpgsqlDbType.Varchar, 3000)
                arParms(0).Value = DivIDList

                arParms(1) = New Npgsql.NpgsqlParameter("@numECampaignID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = CampaignID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = _DomainID

                Return SqlDAL.ExecuteScalar(connString, "USP_UpdateOrgCampaign", arParms)  'execute and store to dataset

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function deletes the selected records
        ''' </summary>
        ''' <remarks>
        '''     This function deletes the selected records
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/22/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function DeleteSearchRecords(ByVal vcCompIdContIdDivIdList As String)
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcEntityIdList", NpgsqlTypes.NpgsqlDbType.Varchar, 4000)
                arParms(0).Value = vcCompIdContIdDivIdList

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "usp_DeleteSelectedSearchRecords", arParms)  'execute and store to dataset

            Catch ex As Exception
                'Throw ex
                Return 0
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the custom field areas for the form and authentication group
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/17/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getSelectedCustomFieldGroups(ByVal sXMLFilePath As String) As DataTable
            Dim dsXMLCustomGroupsConfig As New DataSet                                                      'Create a Dataset object
            Dim strFileName, strFileNameAllGroups As String                                                 'Declare a string object
            strFileName = sXMLFilePath & "\" & "DynamicFormCustomGroupsConfig_" & DomainID & "_" & FormID & "_" & AuthenticationGroupID & ".xml"          'Set the file name
            strFileNameAllGroups = sXMLFilePath & "\" & "DynamicFormCustomGroupsConfig_" & DomainID & "_" & FormID & "_0.xml"  'Set the file name for all groups

            If File.Exists(strFileName) = True Then
                dsXMLCustomGroupsConfig.ReadXml(strFileName)                                                'Read into XML
            ElseIf File.Exists(strFileNameAllGroups) = True Then
                dsXMLCustomGroupsConfig.ReadXml(strFileNameAllGroups)                                       'Read into XML
            End If
            If dsXMLCustomGroupsConfig.Tables.Count = 0 Then
                Dim dtTable As New DataTable                                                                'create a new datatable
                dtTable.Columns.Add(New DataColumn("Loc_id", System.Type.GetType("System.String")))         'Add a column for teh Location Id
                dtTable.Columns.Add(New DataColumn("Loc_name", System.Type.GetType("System.String")))       'Add a column for the Location name
                dsXMLCustomGroupsConfig.Tables.Add(dtTable)                                                 'Add table to the dataset
            End If
            Return dsXMLCustomGroupsConfig.Tables(0)                                                        'return xml data table
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function gets the list of fields which can be updated from Adv Search
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	18/03/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getUpdatableFieldList() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetUpdatableFieldList", arParms) 'execute and get the columns from the view
                Return (ds.Tables(0))
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function Mass Updates the selected records
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	03/18/2006	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function MassUpdateSearchRecords(ByVal vcCompIdContIdDivIdList As String, ByVal vcValue As String, ByVal vcTableRef As String, ByVal vcTableColumn As String, ByVal vcFieldDataType As String)
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcEntityIdList", NpgsqlTypes.NpgsqlDbType.Varchar, 4000)
                arParms(0).Value = vcCompIdContIdDivIdList

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcValue", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(2).Value = vcValue

                arParms(3) = New Npgsql.NpgsqlParameter("@vcTableRef", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(3).Value = vcTableRef

                arParms(4) = New Npgsql.NpgsqlParameter("@vcTableColumn", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(4).Value = vcTableColumn

                arParms(5) = New Npgsql.NpgsqlParameter("@vcFieldDataType", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(5).Value = vcFieldDataType

                Return SqlDAL.ExecuteScalar(connString, "usp_UpdateSelectedSearchRecords", arParms)  'execute and store to dataset

            Catch ex As Exception
                'Throw ex
                Return 0
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for items for the dropdown.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of list details
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	06/22/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetItemList() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAllItems", arParms)              'execute and store to dataset
                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        ''Created by anoop jayaraj
        Public Function ManageAdvSerColnConf() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strAdvSerView", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _strAdvSerCol

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@FromID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _FormID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAdvSearchViews", arParms)     'execute and store to dataset

                Return True                                                           'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''Created by anoop jayaraj
        Public Function AdvancedSearch() As DataSet
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string

                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@WhereCondition", _WhereCondition, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@AreasofInt", _AreasOfInt, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@tintUserRightType", _UserRightType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.Bigint, 18, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@ColumnSearch", _columnSearch, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@ColumnName", _columnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@GetAll", _GetAll, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SortCharacter", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@SortColumnName", _SortcolumnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@bitMassUpdate", _bitMassUpdate, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@LookTable", _lookTable, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@strMassUpdate", _strMassUpdate, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@vcRegularSearchCriteria", _RegularSearchCriteria, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@vcCustomSearchCriteria", _CustomSearchCriteria, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcDisplayColumns", DisplayColumns, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_AdvancedSearch", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function AdvancedSearchOpp() As DataSet
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string

                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@WhereCondition", _WhereCondition, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.Integer, 18, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@ColumnName", _columnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SortCharacter", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@SortColumnName", _SortcolumnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@LookTable", _lookTable, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@strMassUpdate", _strMassUpdate, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@vcRegularSearchCriteria", _RegularSearchCriteria, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@vcCustomSearchCriteria", _CustomSearchCriteria, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@vcDisplayColumns", DisplayColumns, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@GetAll", _GetAll, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_AdvancedSearchOpp", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function AdvancedSearchCase() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@WhereCondition", _WhereCondition, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.Integer, 18, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@ColumnName", _columnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SortCharacter", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@SortColumnName", _SortcolumnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@LookTable", _lookTable, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@vcDisplayColumns", DisplayColumns, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@GetAll", _GetAll, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_AdvancedSearchCase", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function AdvancedSearchProjects() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@WhereCondition", _WhereCondition, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.Integer, 18, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@ColumnName", _columnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SortCharacter", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@SortColumnName", _SortcolumnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@LookTable", _lookTable, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@vcDisplayColumns", DisplayColumns, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@GetAll", _GetAll, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_AdvancedSearchPro", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function AdvancedSearchItems() As DataSet
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@WhereCondition", _WhereCondition, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.Integer, 18, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@ColumnSearch", _columnSearch, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@ColumnName", _columnName, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@GetAll", _GetAll, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SortCharacter", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@SortColumnName", _SortcolumnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@bitMassUpdate", _bitMassUpdate, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@LookTable", _lookTable, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@strMassUpdate", _strMassUpdate, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@vcDisplayColumns", DisplayColumns, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur1", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_AdvancedSearchItems", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _SearchID As Long
        Public Property SearchID() As Long
            Get
                Return _SearchID
            End Get
            Set(ByVal value As Long)
                _SearchID = value
            End Set
        End Property

        Private _SearchName As String
        Public Property SearchName() As String
            Get
                Return _SearchName
            End Get
            Set(ByVal value As String)
                _SearchName = value
            End Set
        End Property

        Private _SlidingDays As Int16
        Public Property SlidingDays() As Int16
            Get
                Return _SlidingDays
            End Get
            Set(ByVal value As Int16)
                _SlidingDays = value
            End Set
        End Property

        Private _TimeExpression As String
        Public Property TimeExpression() As String
            Get
                Return _TimeExpression
            End Get
            Set(ByVal value As String)
                _TimeExpression = value
            End Set
        End Property

        Private _byteMode As Short
        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal value As Short)
                _byteMode = value
            End Set
        End Property

        Public Function ManageSavedSearch() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(13) {}                          'create a param array
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numSearchID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _SearchID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcSearchName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(1).Value = _SearchName

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSearchQuery", NpgsqlTypes.NpgsqlDbType.Varchar, 8000)
                arParms(2).Value = _WhereCondition

                arParms(3) = New Npgsql.NpgsqlParameter("@intSlidingDays", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _SlidingDays

                arParms(4) = New Npgsql.NpgsqlParameter("@vcTimeExpression", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(4).Value = _TimeExpression

                arParms(5) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(5).Value = _FormID

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(6).Value = _UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(7).Value = _DomainID

                arParms(8) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _byteMode

                arParms(9) = New Npgsql.NpgsqlParameter("@vcSearchQueryDisplay", NpgsqlTypes.NpgsqlDbType.Varchar, 8000)
                arParms(9).Value = _SearchQueryforDisplay

                arParms(10) = New Npgsql.NpgsqlParameter("@vcSearchConditionJson", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(10).Value = SearchConditions

                arParms(11) = New Npgsql.NpgsqlParameter("@vcSharedWithUsers", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(11).Value = SharedWithUsers

                arParms(12) = New Npgsql.NpgsqlParameter("@vcDisplayColumns", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(12).Value = DisplayColumns

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageSavedSearch", arParms)
                _SearchID = arParms(0).Value
                If _byteMode = 1 Then
                    Return ds.Tables(0)
                Else
                    Return New DataTable
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _strItems As String
        Public Property strItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal value As String)
                _strItems = value
            End Set
        End Property

        Public Function ManageSearchCriteria() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}                          'create a param array
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = _FormID

                arParms(3) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Xml)
                arParms(3).Value = _strItems

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _byteMode

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageAdvSearchCriteria", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub SetSavedSearchQuery(ByVal lngSearchID As Long, ByVal lngFormID As Long)
            Try
                Dim objSearch As New FormGenericAdvSearch
                objSearch.byteMode = 1
                objSearch.SearchID = lngSearchID
                objSearch.FormID = lngFormID
                objSearch.DomainID = System.Web.HttpContext.Current.Session("DomainID")
                objSearch.UserCntID = System.Web.HttpContext.Current.Session("UserContactID")
                Dim dt As DataTable = objSearch.ManageSavedSearch()
                Dim intDays As Integer
                If dt.Rows.Count = 1 Then
                    If lngFormID = 15 Or lngFormID = 1 Then
                        If lngFormID = 1 Then
                            System.Web.HttpContext.Current.Session("WhereCondition") = dt.Rows(0)("vcSearchQuery")
                        Else
                            System.Web.HttpContext.Current.Session("WhereContditionOpp") = dt.Rows(0)("vcSearchQuery")
                        End If
                        If Common.CCommon.ToString(dt.Rows(0)("vcTimeExpression")) <> "" Then
                            intDays = Common.CCommon.ToInteger(dt.Rows(0)("intSlidingDays"))
                            System.Web.HttpContext.Current.Session("TimeQuery") = Common.CCommon.ToString(dt.Rows(0)("vcTimeExpression")).Replace("{FROM}", Date.Today.AddDays(-intDays).ToShortDateString).Replace("{TO}", CDate(Date.Today.ToShortDateString & " 23:59:59"))
                        End If
                    ElseIf lngFormID = 29 Then
                        System.Web.HttpContext.Current.Session("WhereConditionItem") = dt.Rows(0)("vcSearchQuery")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Shared Sub SaveSearchCriteria(ByVal strValue As String, ByVal lngFormID As Long)
            Try
                'save user's search criteria preference into database
                If strValue.IndexOf("^") > 0 Then
                    Dim strCriteria() As String = strValue.Split(New Char() {"^"}, StringSplitOptions.RemoveEmptyEntries)
                    Dim strValues() As String

                    Dim dtSearchCriteria As New DataTable
                    dtSearchCriteria.Columns.Add("vcFormFieldName")
                    dtSearchCriteria.Columns.Add("intSearchOperator")
                    Dim dr1 As DataRow

                    For j As Int16 = 0 To strCriteria.Length - 1
                        strValues = strCriteria(j).Split("~")
                        dr1 = dtSearchCriteria.NewRow
                        dr1("vcFormFieldName") = strValues(0)
                        dr1("intSearchOperator") = strValues(1)
                        dtSearchCriteria.Rows.Add(dr1)
                    Next

                    Dim ds As New DataSet
                    Dim strdetails As String
                    dtSearchCriteria.TableName = "Table"
                    ds.Tables.Add(dtSearchCriteria)
                    strdetails = ds.GetXml
                    ds.Tables.Remove(ds.Tables(0))

                    Dim objSearch As New FormGenericAdvSearch
                    objSearch.FormID = lngFormID
                    objSearch.DomainID = System.Web.HttpContext.Current.Session("DomainID")
                    objSearch.UserCntID = System.Web.HttpContext.Current.Session("UserContactID")
                    objSearch.strItems = strdetails
                    objSearch.byteMode = 1
                    objSearch.ManageSearchCriteria()

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Shared Function GetSearchCondition(ByVal strValues As String, ByVal strFieldName As String, ByVal strControlID As String, ByVal strFieldValue As String, ByVal strFieldDataType As String, Optional ByVal IsCustomField As Boolean = False, Optional ByVal TablePrefix As String = "", Optional ByVal boolStartingWith As Boolean = False) As String
            Try
                Dim str() As String = strValues.Split("^")
                If str.Length > 0 Then
                    For i As Integer = 0 To str.Length - 1
                        If str(i).Split("~")(0) = strControlID Then
                            If strFieldName = "numAge" Then strFieldName = "Year(bintDOB)"
                            strFieldName = TablePrefix & strFieldName
                            If IsCustomField Then strFieldName = "Fld_Value"
                            Select Case str(i).Split("~")(1)
                                Case 1 '"rbContains"
                                    Return strFieldName & " ILIKE '%" & strFieldValue & "%' "
                                Case 2 '"rbStartsWith"
                                    Return strFieldName & " ILIKE '" & strFieldValue & "%' "
                                Case 3 '"rbEndsWith"
                                    Return strFieldName & " ILIKE '%" & strFieldValue & "' "
                                Case 4 '"rbIsNull"
                                    Return "LENGTH(COALESCE(" & strFieldName & ",'')) = 0 "
                                Case 5 '"rbOrfilter"
                                    Dim strArr() As String = strFieldValue.Split(",")
                                    Dim strReturn As String = strFieldName & " in ("
                                    For j As Int16 = 0 To strArr.Length - 1
                                        If strArr(j).Length > 0 Then strReturn = strReturn + "'" + strArr(j) + "',"
                                    Next
                                    strReturn = strReturn.TrimEnd(",") + ") "
                                    Return strReturn
                                Case 6 '"rbEquals"
                                    Return strFieldName & " = '" & CCommon.ToDouble(strFieldValue) & "'"
                                Case 7 '"rbNotEquals"
                                    Return strFieldName & " <> '" & CCommon.ToDouble(strFieldValue) & "'"
                                Case 8 '"rbLessthan"
                                    Return strFieldName & " < " & CCommon.ToDouble(strFieldValue)
                                Case 9 '"rbGreaterThan"
                                    Return strFieldName & " > " & CCommon.ToDouble(strFieldValue)
                                Case 10 '"rbBetween"
                                    Return strFieldName & " between  " & CCommon.ToDouble(str(i).Split("~")(2).Trim) & " and " & CCommon.ToDouble(str(i).Split("~")(3).Trim)
                                Case 11 '"rbDateRange"
                                    Dim dtFrom As Date = DateFromFormattedDate(str(i).Split("~")(2), System.Web.HttpContext.Current.Session("DateFormat"))
                                    Dim dtTo As Date = DateFromFormattedDate(str(i).Split("~")(3), System.Web.HttpContext.Current.Session("DateFormat"))
                                    If IsCustomField Then
                                        Return strFieldName & " between  '" & dtFrom.Month.ToString + "/" + dtFrom.Day.ToString + "/" + dtFrom.Year.ToString + "'::TIMESTAMP and '" & dtTo.Month.ToString + "/" + dtTo.Day.ToString + "/" + dtTo.Year.ToString & "'::TIMESTAMP"
                                    Else
                                        Return strFieldName & " between  '" & dtFrom.Year.ToString + "-" + dtFrom.Month.ToString + "-" + dtFrom.Day.ToString & "'::TIMESTAMP and '" & dtTo.Year.ToString + "-" + dtTo.Month.ToString + "-" + dtTo.Day.ToString & "'::TIMESTAMP"
                                    End If
                                Case 12 '"rbEqualToDD"
                                    Return strFieldName & " = '" & strFieldValue & "'"
                                Case 13 '"rbNotEqualToDD"
                                    Return strFieldName & " <> '" & strFieldValue & "'"
                                Case 14 '"rbEqualToDate"
                                    Dim dtDate As Date = DateFromFormattedDate(strFieldValue, System.Web.HttpContext.Current.Session("DateFormat"))
                                    If IsCustomField Then
                                        Return strFieldName & " between  '" & dtDate.Month.ToString + "/" + dtDate.Day.ToString + "/" + dtDate.Year.ToString + "'::TIMESTAMP and '" & dtDate.Month.ToString + "/" + dtDate.Day.ToString + "/" + dtDate.Year.ToString & "'::TIMESTAMP"
                                    Else
                                        Return strFieldName & " between  '" & dtDate.Month.ToString + "/" + dtDate.Day.ToString + "/" + dtDate.Year.ToString + " 00:00:00'::TIMESTAMP and '" & dtDate.Month.ToString + "/" + dtDate.Day.ToString + "/" + dtDate.Year.ToString & " 23:59:59'::TIMESTAMP"
                                    End If
                                Case Else
                                    Return strFieldName & " ILIKE '%" & strFieldValue & "%' "
                            End Select
                        End If
                    Next
                End If
                strFieldName = TablePrefix & strFieldName
                If IsCustomField Then strFieldName = "Fld_Value"

                'default return string
                If strFieldDataType.ToUpper = "N" Or strFieldDataType.ToUpper = "M" Then 'Numeric or Money data type then
                    Return strFieldName & "::NUMERIC = " & strFieldValue & " "
                ElseIf strFieldDataType.ToUpper = "V" Then
                    Return strFieldName & " ILIKE '" & strFieldValue & "' "
                Else
                    If boolStartingWith = False Then
                        Return strFieldName & " ILIKE '%" & strFieldValue & "%' " 'default option
                    Else
                        Return strFieldName & " ILIKE '" & strFieldValue & "%' "
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Shared Function GetAdvancedSearchCondition(ByVal strFieldName As String, ByVal searchOperator As Integer, ByVal strFieldValue As String, ByVal strFieldValueFrom As String, ByVal strFieldValueTo As String, ByVal IsCustomField As Boolean, ByVal TablePrefix As String, ByVal ClientMachineTimezoneOffset As Integer) As String
            Try
                If strFieldName = "numAge" Then
                    Dim todayDate As Date = DateTime.UtcNow.AddMinutes(ClientMachineTimezoneOffset * -1)
                    strFieldName = "FLOOR(DATEDIFF('year', " & TablePrefix & "bintDOB::TIMESTAMP, CAST('" & todayDate.Month.ToString + "/" + todayDate.Day.ToString + "/" + todayDate.Year.ToString + "' AS TIMESTAMP)))"
                Else
                    strFieldName = TablePrefix & strFieldName
                End If

                If IsCustomField Then strFieldName = "Fld_Value"
                Select Case searchOperator
                    Case 1 '"rbContains"
                        Dim strArr() As String = strFieldValue.Split(",")
                        Dim strReturn As String = " ("
                        For j As Int16 = 0 To strArr.Length - 1
                            If strArr(j).Length > 0 Then strReturn = strReturn + If(j > 0, " OR ", "") + strFieldName & " ILIKE '%" & strArr(j) & "%' "
                        Next
                        strReturn = strReturn.TrimEnd(",") + ") "
                        Return strReturn
                    Case 2 '"rbStartsWith"
                        Dim strArr() As String = strFieldValue.Split(",")
                        Dim strReturn As String = " ("
                        For j As Int16 = 0 To strArr.Length - 1
                            If strArr(j).Length > 0 Then strReturn = strReturn + If(j > 0, " OR ", "") + strFieldName & " ILIKE '" & strArr(j) & "%' "
                        Next
                        strReturn = strReturn.TrimEnd(",") + ") "
                        Return strReturn
                    Case 3 '"rbEndsWith"
                        Dim strArr() As String = strFieldValue.Split(",")
                        Dim strReturn As String = " ("
                        For j As Int16 = 0 To strArr.Length - 1
                            If strArr(j).Length > 0 Then strReturn = strReturn + If(j > 0, " OR ", "") + strFieldName & " ILIKE '%" & strArr(j) & "' "
                        Next
                        strReturn = strReturn.TrimEnd(",") + ") "
                        Return strReturn
                    Case 4 '"rbIsNull"
                        Return "LENGTH(COALESCE(" & strFieldName & ",'')) = 0 "
                    Case 10 '"rbBetween"
                        If Not String.IsNullOrEmpty(strFieldValueFrom) AndAlso Not String.IsNullOrEmpty(strFieldValueTo) Then
                            Return strFieldName & " between  " & CCommon.ToDouble(strFieldValueFrom.Trim) & " and " & CCommon.ToDouble(strFieldValueTo.Trim)
                        ElseIf Not String.IsNullOrEmpty(strFieldValueFrom) Then
                            Return strFieldName & " >=  " & CCommon.ToDouble(strFieldValueFrom.Trim)
                        ElseIf Not String.IsNullOrEmpty(strFieldValueTo) Then
                            Return strFieldName & " <= " & CCommon.ToDouble(strFieldValueTo.Trim)
                        End If
                    Case 11 '"rbDateRange"
                        If Not String.IsNullOrEmpty(strFieldValueFrom) AndAlso Not String.IsNullOrEmpty(strFieldValueTo) Then
                            Dim dtFrom As Date = DateTime.UtcNow.AddDays(strFieldValueFrom * -1).AddMinutes(ClientMachineTimezoneOffset * -1)
                            Dim dtTo As Date = DateTime.UtcNow.AddDays(strFieldValueTo).AddMinutes(ClientMachineTimezoneOffset * -1)

                            If IsCustomField Then
                                Return "(" & strFieldName & ") <> '' AND CAST(" & strFieldName & "  AS TIMESTAMP)" & " between CAST('" & dtFrom.Month.ToString + "/" + dtFrom.Day.ToString + "/" + dtFrom.Year.ToString + "'  AS TIMESTAMP) and CAST('" & dtTo.Month.ToString + "/" + dtTo.Day.ToString + "/" + dtTo.Year.ToString & "'  AS TIMESTAMP)"
                            Else
                                Return "CAST(" & strFieldName & "  AS TIMESTAMP) between CAST('" & dtFrom.Year.ToString + "-" + dtFrom.Month.ToString + "-" + dtFrom.Day.ToString & "'  AS TIMESTAMP) and CAST('" & dtTo.Year.ToString + "-" + dtTo.Month.ToString + "-" + dtTo.Day.ToString & "'  AS TIMESTAMP)"
                            End If
                        ElseIf Not String.IsNullOrEmpty(strFieldValueFrom) Then
                            Dim dtFrom As Date = DateTime.UtcNow.AddDays(strFieldValueFrom * -1).AddMinutes(ClientMachineTimezoneOffset * -1)

                            If IsCustomField Then
                                Return "(" & strFieldName & ") <> '' AND CAST(" & strFieldName & "  AS TIMESTAMP)" & " >= CAST('" & dtFrom.Month.ToString + "/" + dtFrom.Day.ToString + "/" + dtFrom.Year.ToString + "'  AS TIMESTAMP)"
                            Else
                                Return "CAST(" & strFieldName & "  AS TIMESTAMP) >= CAST('" & dtFrom.Year.ToString + "-" + dtFrom.Month.ToString + "-" + dtFrom.Day.ToString & "'  AS TIMESTAMP)"
                            End If
                        ElseIf Not String.IsNullOrEmpty(strFieldValueTo) Then
                            Dim dtTo As Date = DateTime.UtcNow.AddDays(strFieldValueTo).AddMinutes(ClientMachineTimezoneOffset * -1)

                            If IsCustomField Then
                                Return "(" & strFieldName & ") <> '' AND CAST(" & strFieldName & "  AS TIMESTAMP)" & " <= CAST('" & dtTo.Month.ToString + "/" + dtTo.Day.ToString + "/" + dtTo.Year.ToString + "'  AS TIMESTAMP)"
                            Else
                                Return "CAST(" & strFieldName & "  AS TIMESTAMP) <= CAST('" & dtTo.Year.ToString + "-" + dtTo.Month.ToString + "-" + dtTo.Day.ToString & "'  AS TIMESTAMP)"
                            End If
                        End If
                    Case 15 'Not Contains (TextBox)
                        Dim strArr() As String = strFieldValue.Split(",")
                        Dim strReturn As String = " ("
                        For j As Int16 = 0 To strArr.Length - 1
                            If strArr(j).Length > 0 Then strReturn = strReturn + If(j > 0, " OR ", "") + strFieldName & " NOT ILIKE '%" & strArr(j) & "%' "
                        Next
                        strReturn = strReturn.TrimEnd(",") + ") "
                        Return strReturn
                    Case 16 'CheckBox
                        Return "COALESCE(" & strFieldName & ",'0') = '" & If(CCommon.ToBool(strFieldValue), "1", "0") & "'"
                    Case 17 'Includes (SelectBox, CheckBoxList, ListBox)
                        Return strFieldName & " IN (" & strFieldValue & ")"
                    Case 18 'Excludes (SelectBox, CheckBoxList, ListBox)
                        Return strFieldName & " NOT IN (" & strFieldValue & ")"
                    Case 19 '"Date Selection"
                        If Not String.IsNullOrEmpty(strFieldValueFrom) AndAlso Not String.IsNullOrEmpty(strFieldValueTo) Then
                            Dim dtFrom As Date = DateFromFormattedDate(strFieldValueFrom, "MM/dd/yyyy")
                            Dim dtTo As Date = DateFromFormattedDate(strFieldValueTo, "MM/dd/yyyy")
                            If IsCustomField Then
                                Return "(" & strFieldName & ") <> '' AND CAST(" & strFieldName & "  AS TIMESTAMP)" & " between  CAST('" & dtFrom.Month.ToString + "/" + dtFrom.Day.ToString + "/" + dtFrom.Year.ToString + "'  AS TIMESTAMP) and CAST('" & dtTo.Month.ToString + "/" + dtTo.Day.ToString + "/" + dtTo.Year.ToString & "'  AS TIMESTAMP)"
                            Else
                                Return "CAST(" & strFieldName & "  AS TIMESTAMP) between  CAST('" & dtFrom.Year.ToString + "-" + dtFrom.Month.ToString + "-" + dtFrom.Day.ToString & "'  AS TIMESTAMP) and CAST('" & dtTo.Year.ToString + "-" + dtTo.Month.ToString + "-" + dtTo.Day.ToString & "'  AS TIMESTAMP)"
                            End If
                        ElseIf Not String.IsNullOrEmpty(strFieldValueFrom) Then
                            Dim dtFrom As Date = DateFromFormattedDate(strFieldValueFrom, "MM/dd/yyyy")
                            If IsCustomField Then
                                Return "(" & strFieldName & ") <> '' AND CAST(" & strFieldName & "  AS TIMESTAMP)" & " >= CAST('" & dtFrom.Month.ToString + "/" + dtFrom.Day.ToString + "/" + dtFrom.Year.ToString + "'  AS TIMESTAMP)"
                            Else
                                Return "CAST(" & strFieldName & "  AS TIMESTAMP) >= CAST('" & dtFrom.Year.ToString + "-" + dtFrom.Month.ToString + "-" + dtFrom.Day.ToString & "'  AS TIMESTAMP)"
                            End If
                        ElseIf Not String.IsNullOrEmpty(strFieldValueTo) Then
                            Dim dtTo As Date = DateFromFormattedDate(strFieldValueTo, "MM/dd/yyyy")
                            If IsCustomField Then
                                Return "(" & strFieldName & ") <> '' AND CAST(" & strFieldName & "  AS TIMESTAMP)" & " <= CAST('" & dtTo.Month.ToString + "/" + dtTo.Day.ToString + "/" + dtTo.Year.ToString + "'  AS TIMESTAMP)"
                            Else
                                Return "CAST(" & strFieldName & "  AS TIMESTAMP) <= CAST('" & dtTo.Year.ToString + "-" + dtTo.Month.ToString + "-" + dtTo.Day.ToString & "'  AS TIMESTAMP)"
                            End If
                        End If
                    Case Else
                        Return strFieldName & " ILIKE '%" & strFieldValue & "%' "
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub GenerateUserFriendlySearchString(ByRef sbFriendlyQuery As System.Text.StringBuilder, ByVal strFriendlyName As String, ByVal strFieldName As String, ByVal strFieldValue As String, ByVal strSearchCriteria As String, ByVal DefaultSearchOperator As String)
            Try
                'Create User readable Query for saved search 
                Dim intSearchIndex As Integer = strSearchCriteria.IndexOf(strFieldName)
                Dim strTemp As String
                If intSearchIndex >= 0 Then
                    strTemp = strSearchCriteria.Substring(intSearchIndex)
                    strTemp = strTemp.Substring(0, strTemp.IndexOf("^"))
                    strTemp = [Enum].GetName(GetType(enmSearchCriteria), CCommon.ToInteger(strTemp.Split("~")(1))).Replace("_", " ")
                Else
                    strTemp = DefaultSearchOperator
                End If

                sbFriendlyQuery.Append(strFriendlyName)
                sbFriendlyQuery.Append(" ")
                sbFriendlyQuery.Append(strTemp)
                sbFriendlyQuery.Append(" ")
                sbFriendlyQuery.Append("'" & strFieldValue & "'")
                sbFriendlyQuery.Append(" and ")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function LoadDropdown() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _DomainID


                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = _UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim dr As DataSet
                dr = SqlDAL.ExecuteDataset(connString, "USP_ProfileGetGroup", arParms)

                Return dr.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function AdvancedSearchSurvey() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string

                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@WhereCondition", _WhereCondition, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", _UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@TotRecs", _TotalRecords, NpgsqlTypes.NpgsqlDbType.Integer, 18, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@ColumnName", _columnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SortCharacter", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@SortColumnName", _SortcolumnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@LookTable", _lookTable, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@GetAll", _GetAll, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcDisplayColumns", DisplayColumns, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_AdvancedSearchSurvey", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAdvancedSearchFieldList() As DataSet
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = GetConnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = AuthenticationGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAdvancedSearchFieldList", arParms) 'execute and store to dataset

                Return ds                                                  'return datatable

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AdvancedSearchFinancialSearch() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = GetConnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}                          'create a param array
                Dim ds As DataSet
                arParms(0) = New Npgsql.NpgsqlParameter("@WhereCondition", NpgsqlTypes.NpgsqlDbType.Varchar, 4000)
                arParms(0).Value = _WhereCondition

                arParms(1) = New Npgsql.NpgsqlParameter("@ViewID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _ViewID                                                        'representing listmaster/ listdetails

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(2).Value = _DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(3).Value = _UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(4).Value = _AuthenticationGroupID

                arParms(5) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _CurrentPage

                arParms(6) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _PageSize

                arParms(7) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Direction = ParameterDirection.InputOutput
                arParms(7).Value = _TotalRecords

                arParms(8) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(8).Value = _columnSortOrder

                arParms(9) = New Npgsql.NpgsqlParameter("@ColumnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(9).Value = _columnName

                arParms(10) = New Npgsql.NpgsqlParameter("@SortCharacter", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(10).Value = _SortCharacter

                arParms(11) = New Npgsql.NpgsqlParameter("@SortColumnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(11).Value = _SortcolumnName

                arParms(12) = New Npgsql.NpgsqlParameter("@LookTable", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(12).Value = _lookTable

                arParms(13) = New Npgsql.NpgsqlParameter("@GetAll", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(13).Value = _GetAll

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_AdvancedSearchFinancialSearch", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SearchOrganizations(ByVal lngDomainID As Long, ByVal timeZoneOffset As Integer, ByVal strSearchConditions As String) As String
            Try
                Dim str As String = ""
                Dim sbFriendlyQuery As New Text.StringBuilder


                FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Search in Leads", "cbSearchInLeads", "Checked", "", "Is")
                FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Search in Prospects", "cbSearchInProspects", "Checked", "", "Is")
                FormGenericAdvSearch.GenerateUserFriendlySearchString(sbFriendlyQuery, "Search in Accounts", "cbSearchInAccounts", "Checked", "", "Is")

                'reset values
                If Not HttpContext.Current Is Nothing Then
                    HttpContext.Current.Session("TimeExpression") = ""
                    HttpContext.Current.Session("TimeQuery") = ""
                    HttpContext.Current.Session("SlidingDays") = 0
                End If

                Dim ControlType, strFieldName, strPrefix, strFriendlyName As String
                Dim strFieldID As Long
                Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
                Dim listConditions As System.Collections.Generic.List(Of AdvancedSearchCondition) = serializer.Deserialize(Of System.Collections.Generic.List(Of AdvancedSearchCondition))(strSearchConditions)

                For Each searchCondition As AdvancedSearchCondition In listConditions
                    ControlType = searchCondition.AssociatedControlType
                    strFieldName = searchCondition.DbColumnName.Trim.Replace(" ", "_")
                    strFriendlyName = searchCondition.FieldName
                    strFieldID = searchCondition.FieldID

                    If searchCondition.LookBackTableName = "DivisionMaster" Then
                        strPrefix = "DM."
                    ElseIf searchCondition.LookBackTableName = "CompanyInfo" Then
                        strPrefix = "C."
                    ElseIf searchCondition.LookBackTableName = "AdditionalContactsInformation" Then
                        strPrefix = "ADC."
                    Else
                        strPrefix = ""
                    End If

                    If ControlType = "SelectBox" Then 'Dropdown
                        If searchCondition.FieldType = "R" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                Dim strSqlCondition As String = FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)

                                If strFieldName = "numCountry" Or strFieldName = "numShipCountry" Or strFieldName = "numBillCountry" Or strFieldName = "numBillShipCountry" Then
                                    If strFieldName = "numShipCountry" Then
                                        str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=2 and " & strSqlCondition.Replace("numShipCountry", "numCountry") & ")"
                                    ElseIf strFieldName = "numBillCountry" Then
                                        str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=1 and " & strSqlCondition.Replace("numBillCountry", "numCountry") & ")"
                                    ElseIf strFieldName = "numBillShipCountry" Then
                                        str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND " & strSqlCondition.Replace("numBillShipCountry", "numCountry") & ")"
                                    Else
                                        str = str & " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=1 AND tintAddressType=0 and " & strSqlCondition & ")"
                                    End If
                                Else
                                    str = str & " and " & strSqlCondition
                                End If
                            End If
                        ElseIf searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    ElseIf ControlType = "CheckBoxList" Then
                        If searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    ElseIf ControlType = "ListBox" Then
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            Dim strSqlCondition As String = FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)

                            If strFieldName = "numState" Or strFieldName = "numShipState" Or strFieldName = "numBillState" Or strFieldName = "numBillShipState" Then
                                If strFieldName = "numShipState" Then
                                    str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=2 and " & strSqlCondition.Replace("numShipState", "numState") & ")"
                                ElseIf strFieldName = "numBillState" Then
                                    str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=1 and " & strSqlCondition.Replace("numBillState", "numState") & ")"
                                ElseIf strFieldName = "numBillShipState" Then
                                    str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND " & strSqlCondition.Replace("numBillShipState", "numState") & ")"
                                Else
                                    str = str & " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=1 AND tintAddressType=0 and " & strSqlCondition & ")"
                                End If
                            Else
                                str = str & " and " & strSqlCondition
                            End If
                        End If
                    ElseIf ControlType = "CheckBox" Then 'Added by chintan, reason: custom field CheckBox code isn't handled
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If searchCondition.FieldType = "R" Then
                                If searchCondition.DbColumnName = "bitExcludeOrgWithNoSalesOrders" Then
                                    If CCommon.ToBool(searchCondition.SearchValue) Then
                                        str = str & "  AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND tintOppType=1 AND tintOppStatus = 1) > 0"
                                    End If
                                Else
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "C" Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "D" Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    ElseIf ControlType = "DateField" Then
                        If searchCondition.FieldType = "R" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then

                                Dim fromDate As DateTime = DateTime.MinValue
                                Dim toDate As DateTime = DateTime.MinValue

                                If searchCondition.SearchOperator = "19" Then 'fixed date
                                    If Not String.IsNullOrEmpty(searchCondition.SearchFrom) AndAlso Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                        fromDate = Convert.ToDateTime(searchCondition.SearchFrom).AddMinutes(-timeZoneOffset)
                                        toDate = Convert.ToDateTime(searchCondition.SearchTo).AddMinutes(-timeZoneOffset)
                                    ElseIf Not String.IsNullOrEmpty(searchCondition.SearchFrom) Then
                                        fromDate = Convert.ToDateTime(searchCondition.SearchFrom).AddMinutes(-timeZoneOffset)
                                    ElseIf Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                        toDate = Convert.ToDateTime(searchCondition.SearchTo).AddMinutes(-timeZoneOffset)
                                    End If
                                ElseIf searchCondition.SearchOperator = "11" Then 'Sliding date
                                    If Not String.IsNullOrEmpty(searchCondition.SearchFrom) AndAlso Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                        fromDate = DateTime.UtcNow.AddDays(searchCondition.SearchFrom * -1).AddMinutes(timeZoneOffset * -1)
                                        toDate = DateTime.UtcNow.AddDays(searchCondition.SearchTo).AddMinutes(timeZoneOffset * -1)
                                    ElseIf Not String.IsNullOrEmpty(searchCondition.SearchFrom) Then
                                        fromDate = DateTime.UtcNow.AddDays(searchCondition.SearchFrom * -1).AddMinutes(timeZoneOffset * -1)
                                    ElseIf Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                        toDate = DateTime.UtcNow.AddDays(searchCondition.SearchTo).AddMinutes(timeZoneOffset * -1)
                                    End If
                                End If

                                If searchCondition.DbColumnName = "dtExcludeOrgWithSalesCreatedDate" Then
                                    If Not fromDate = DateTime.MinValue AndAlso Not toDate = DateTime.MinValue Then
                                        str = str & "  AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND tintOppType=1 AND tintOppStatus = 1 AND CAST(bintCreatedDate + make_interval(mins => " & timeZoneOffset * -1 & ")  AS TIMESTAMP) BETWEEN CAST('" & fromDate & "'  AS TIMESTAMP) AND CAST('" & toDate & "'  AS TIMESTAMP)) = 0"
                                    ElseIf Not fromDate = DateTime.MinValue Then
                                        str = str & "  AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND tintOppType=1 AND tintOppStatus = 1 AND CAST(bintCreatedDate + make_interval(mins => " & timeZoneOffset * -1 & ")  AS TIMESTAMP) >= CAST('" & fromDate & "'  AS TIMESTAMP)) = 0"
                                    ElseIf Not toDate = DateTime.MinValue Then
                                        str = str & "  AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND tintOppType=1 AND tintOppStatus = 1 AND CAST(bintCreatedDate + make_interval(mins => " & timeZoneOffset * -1 & ")  AS TIMESTAMP) <= CAST('" & toDate & "'  AS TIMESTAMP)) = 0"
                                    End If
                                ElseIf searchCondition.DbColumnName = "bintVisitedDate" Then
                                    If Not fromDate = DateTime.MinValue AndAlso Not toDate = DateTime.MinValue Then
                                        str = str & "  AND (SELECT COUNT(*) FROM TrackingVisitorsHDR WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND CAST(dtCreated  AS TIMESTAMP) BETWEEN CAST('" & fromDate & "'  AS TIMESTAMP) AND CAST('" & toDate & "'  AS TIMESTAMP)) > 0"
                                    ElseIf Not fromDate = DateTime.MinValue Then
                                        str = str & "  AND (SELECT COUNT(*) FROM TrackingVisitorsHDR WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND CAST(dtCreated  AS TIMESTAMP) >= CAST('" & fromDate & "'  AS TIMESTAMP)) > 0"
                                    ElseIf Not toDate = DateTime.MinValue Then
                                        str = str & "  AND (SELECT COUNT(*) FROM TrackingVisitorsHDR WHERE numDomainID=DM.numDomainID AND numDivisionID=DM.numDivisionID AND CAST(dtCreated  AS TIMESTAMP) <= CAST('" & toDate & "'  AS TIMESTAMP)) > 0"
                                    End If
                                ElseIf searchCondition.DbColumnName = "vcLastSalesOrderDate" Then
                                    If Not fromDate = DateTime.MinValue AndAlso Not toDate = DateTime.MinValue Then
                                        str = str & "  AND CAST((SELECT MAX(bintCreatedDate) + make_interval(mins => " & timeZoneOffset * -1 & ") AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = " & CCommon.ToLong(lngDomainID) & " AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1)  AS TIMESTAMP) BETWEEN CAST('" & fromDate & "'  AS TIMESTAMP) AND CAST('" & toDate & "'  AS TIMESTAMP)"
                                    ElseIf Not fromDate = DateTime.MinValue Then
                                        str = str & "  AND CAST((SELECT MAX(bintCreatedDate) + make_interval(mins => " & timeZoneOffset * -1 & ") AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = " & CCommon.ToLong(lngDomainID) & " AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1)  AS TIMESTAMP) >= CAST('" & fromDate & "'  AS TIMESTAMP)"
                                    ElseIf Not toDate = DateTime.MinValue Then
                                        str = str & "  AND CAST((SELECT MAX(bintCreatedDate) + make_interval(mins => " & timeZoneOffset * -1 & ") AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = " & CCommon.ToLong(lngDomainID) & " AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1)  AS TIMESTAMP) <= CAST('" & toDate & "'  AS TIMESTAMP)"
                                    End If
                                Else
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                End If
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    Else 'Textbox
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Or Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                            If searchCondition.FieldType = "R" Then
                                If strFieldName = "vcPostalCode" Or strFieldName = "vcShipPostCode" Or strFieldName = "vcBillPostCode" Then 'ShipPostal/BillPostal
                                    Dim strSqlCondition As String = FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    If strFieldName = "vcShipPostCode" Then
                                        str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=2 and " & strSqlCondition.Replace("vcShipPostalCode", "vcPostalCode").Replace("vcShipPostCode", "vcPostalCode") & ")"
                                    ElseIf strFieldName = "vcBillPostCode" Then
                                        str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=1 and " & strSqlCondition.Replace("vcBillPostalCode", "vcPostalCode").Replace("vcBillPostCode", "vcPostalCode") & ")"
                                    Else
                                        str = str & " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=1 AND tintAddressType=0 and " & strSqlCondition & ")"
                                    End If
                                ElseIf strFieldName = "vcCity" Or strFieldName = "vcShipCity" Or strFieldName = "vcBillCity" Then
                                    Dim strSqlCondition As String = FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    If strFieldName = "vcShipCity" Then
                                        str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=2 and " & strSqlCondition.Replace("vcShipCity", "vcCity") & ")"
                                    ElseIf strFieldName = "vcBillCity" Then
                                        str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=1 and " & strSqlCondition.Replace("vcBillCity", "vcCity") & ")"
                                    Else
                                        str = str & " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=1 AND tintAddressType=0 and " & strSqlCondition & ")"
                                    End If
                                ElseIf strFieldName = "vcStreet" Or strFieldName = "vcShipStreet" Or strFieldName = "vcBillStreet" Then
                                    Dim strSqlCondition As String = FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    If strFieldName = "vcShipStreet" Then
                                        str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=2 and " & strSqlCondition.Replace("vcShipStreet", "vcStreet") & ")"
                                    ElseIf strFieldName = "vcBillStreet" Then
                                        str = str & " AND DM.numDivisionID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=1 and " & strSqlCondition.Replace("vcBillStreet", "vcStreet") & ")"
                                    Else
                                        str = str & " AND ADC.numContactID IN (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=1 AND tintAddressType=0 and " & strSqlCondition & ")"
                                    End If
                                Else : str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "C" Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "D" Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    End If
                Next

                Return str
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SearchOppOrders(ByVal lngDomainID As Long, ByVal timeZoneOffset As Integer, ByVal strSearchConditions As String) As String
            Try
                Dim str As String = ""
                Dim ControlType, strFieldName, strPrefix, strFriendlyName As String
                Dim strFieldID As Long
                Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
                Dim listConditions As System.Collections.Generic.List(Of AdvancedSearchCondition) = serializer.Deserialize(Of System.Collections.Generic.List(Of AdvancedSearchCondition))(strSearchConditions)

                For Each searchCondition As AdvancedSearchCondition In listConditions
                    ControlType = searchCondition.AssociatedControlType
                    strFieldName = searchCondition.DbColumnName.Trim.Replace(" ", "_")
                    strFriendlyName = searchCondition.FieldName
                    strFieldID = searchCondition.FieldID

                    If searchCondition.LookBackTableName = "DivisionMaster" Then
                        strPrefix = "DM."
                    ElseIf searchCondition.LookBackTableName = "AdditionalContactsInformation" Then
                        strPrefix = "ADC."
                    ElseIf searchCondition.LookBackTableName = "CompanyInfo" Then
                        strPrefix = "C."
                    ElseIf searchCondition.LookBackTableName = "OpportunityMaster" Then
                        strPrefix = "OppMas."
                    ElseIf searchCondition.LookBackTableName = "OpportunityBizDocs" Then
                        strPrefix = "OpportunityBizDocs."
                    Else
                        strPrefix = ""
                    End If

                    If ControlType = "SelectBox" Then
                        If searchCondition.FieldType = "R" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                If strFieldName = "numShipCountry" Or strFieldName = "numBillCountry" Then
                                    If strFieldName = "numShipCountry" Then
                                        str = str & " and oppMas.numOppID in "
                                        str = str & " ("
                                        str = str & " SELECT numOppID FROM OpportunityMaster Where COALESCE(tintShipToType,1)=0 and numDomainId= " & lngDomainID & " AND numDivisionID in  "
                                        str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=2 AND " & FormGenericAdvSearch.GetAdvancedSearchCondition("numCountry", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ") "

                                        str = str & " union "

                                        ''WHEN tintShipToType=1
                                        str = str & " SELECT OM.numOppId FROM OpportunityMaster OM INNER JOIN DivisionMaster DM"
                                        str = str & " ON OM.numDivisionId = DM.numDivisionID WHERE COALESCE(OM.tintShipToType,1)=1 AND OM.numDomainId= " & lngDomainID & " AND DM.numDivisionID in "
                                        str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=2 "
                                        str = str & " AND " & FormGenericAdvSearch.GetAdvancedSearchCondition("numCountry", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & " ) "

                                        str = str & " union "

                                        'WHEN tintShipToType=2
                                        str = str & " SELECT OM.numOppId FROM OpportunityAddress OA INNER JOIN OpportunityMaster OM ON OA.numOppID = OM.numOppId WHERE COALESCE(OM.tintShipToType,1)=2 AND OM.numDomainId= " & lngDomainID
                                        str = str & " AND " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "OA.", timeZoneOffset)
                                        str = str & " ) "

                                    ElseIf strFieldName = "numBillCountry" Then
                                        str = str & " and oppMas.numOppID in "
                                        str = str & " ("
                                        str = str & " SELECT numOppID FROM OpportunityMaster Where COALESCE(tintBillToType,1)=0 and numDomainId= " & lngDomainID & " AND numDivisionID in  "
                                        str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=1 AND " & FormGenericAdvSearch.GetAdvancedSearchCondition("numCountry", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ") "

                                        str = str & " union "

                                        ''WHEN tintBillToType=1
                                        str = str & " SELECT OM.numOppId FROM OpportunityMaster OM INNER JOIN DivisionMaster DM"
                                        str = str & " ON OM.numDivisionId = DM.numDivisionID WHERE COALESCE(OM.tintBillToType,1)=1 AND OM.numDomainId= " & lngDomainID & " AND DM.numDivisionID in "
                                        str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=1 "
                                        str = str & " AND " & FormGenericAdvSearch.GetAdvancedSearchCondition("numCountry", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & " ) "

                                        str = str & " union "

                                        'WHEN tintBillToType=2
                                        str = str & " SELECT OM.numOppId FROM OpportunityAddress OA INNER JOIN OpportunityMaster OM ON OA.numOppID = OM.numOppId WHERE COALESCE(OM.tintBillToType,1)=2 AND OM.numDomainId= " & lngDomainID
                                        str = str & " AND " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "OA.", timeZoneOffset)
                                        str = str & " ) "
                                    End If
                                ElseIf searchCondition.LookBackTableName = "Warehouses" Then '
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId)   from WareHouses WareHouse"
                                    str = str & " join WareHouseItems on WareHouseItems.numWareHouseID =WareHouse.numWareHouseID "
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId"
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode"
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId"
                                    str = str & " where OppMas.numDomainId= " & lngDomainID
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    str = str & " ) "
                                ElseIf searchCondition.LookBackTableName = "WareHouseItmsDTL" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId) from WareHouseItmsDTL WareHouseItemDTL"
                                    str = str & " join WareHouseItems on WareHouseItemDTL.numWareHouseItemID =WareHouseItems.numWareHouseItemID "
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId"
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode"
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId "
                                    str = str & " join OppWarehouseSerializedItem OppSerItm on (OppSerItm.numOppId = OppMas.numOppId and WareHouseItemDTL.numWarehouseItmsDTLID= OppSerItm.numWarehouseItmsDTLID) "
                                    str = str & " where OppMas.numDomainId= " & lngDomainID
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    str = str & " ) "
                                ElseIf searchCondition.LookBackTableName = "WareHouseItems" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId)    from WareHouseItems"
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId   "
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode  "
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId "
                                    str = str & " where OppMas.numDomainId= " & lngDomainID
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    str = str & " ) "
                                ElseIf searchCondition.LookBackTableName = "Item" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppItems.numOppId)   from item "
                                    str = str & "Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode "

                                    If strFieldName = "numBaseUnit" Or strFieldName = "numPurchaseUnit" Or strFieldName = "numSaleUnit" Then
                                        str = str & " and item." & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    ElseIf strFieldName = "charItemType" Then
                                        If strFieldName = "charItemType" Then
                                            searchCondition.SearchValue = "'" & searchCondition.SearchValue.Replace(",", "','") & "'"
                                        End If

                                        str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    Else
                                        str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "Item.", timeZoneOffset)
                                    End If
                                    str = str & " ) "
                                ElseIf searchCondition.LookBackTableName = "OpportunityContact" Then
                                    If strFieldName = "numOppContactId" Then
                                        str = str & " AND OppMas.numOppId in ( SELECT OC.numOppId FROM OpportunityContact OC INNER JOIN OpportunityMaster OM ON OC.numOppId = OM.numOppId WHERE numDomainId= " & lngDomainID & " AND " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & "  ) "
                                    End If
                                ElseIf searchCondition.LookBackTableName = "OpportunityBizDocsDetails" Then
                                    If strFieldName = "numPaymentMethod" Then
                                        str = str & " AND OppMas.numOppId in ( SELECT BD.numOppId FROM OpportunityBizDocsDetails BDD INNER JOIN OpportunityBizDocs BD ON  BDD.numBizDocsId = BD.numOppBizDocsId"
                                        str = str & " WHERE BDD.numDomainId = " & lngDomainID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & "  ) "
                                    End If
                                Else
                                    If strFieldName = "tintOppType" Then
                                        str = str & " and ("
                                        Dim values As String() = searchCondition.SearchValue.Split(",")
                                        Dim i As Integer = 0
                                        For Each value As String In values
                                            If i <> 0 Then
                                                str = str & " OR "
                                            End If

                                            If value = "1" Then
                                                str = str & "(COALESCE(OppMas.tintOppStatus,0)=0 AND COALESCE(OppMas.tintOppType,0)=1)"
                                            ElseIf value = "2" Then
                                                str = str & "(COALESCE(OppMas.tintOppStatus,0)=0 AND COALESCE(OppMas.tintOppType,0)=2)"
                                            ElseIf value = "3" Then
                                                str = str & "(COALESCE(OppMas.tintOppStatus,0)=1 AND COALESCE(OppMas.tintOppType,0)=1)"
                                            ElseIf value = "4" Then
                                                str = str & "(COALESCE(OppMas.tintOppStatus,0)=1 AND COALESCE(OppMas.tintOppType,0)=2)"
                                            End If

                                            i = i + 1
                                        Next

                                        str = str & ")"
                                    ElseIf strFieldName = "tintSource" Then
                                        searchCondition.SearchValue = "'" & searchCondition.SearchValue.Replace(",", "','") & "'"
                                        str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    Else
                                        str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    End If
                                End If
                            End If
                        ElseIf searchCondition.FieldType = "O" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then

                                Select Case searchCondition.SearchOperator
                                    Case 17 'Includes
                                        str = str & " AND OppMas.numOppId IN (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & strFieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                    Case 18 'Excludes
                                        str = str & " AND OppMas.numOppId NOT IN (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & strFieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                    Case Else
                                        str = str & " AND OppMas.numOppId IN (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & strFieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                End Select
                            End If
                        ElseIf searchCondition.FieldType = "I" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then

                                Select Case searchCondition.SearchOperator
                                    Case 17 'Includes
                                        str = str & " AND OppMas.numOppId in (select numOppId from OpportunityItems where numItemCode in  (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "'))))"
                                    Case 18 'Excludes                                                                                                                                                                               
                                        str = str & " AND OppMas.numOppId in (select numOppId from OpportunityItems where numItemCode in  (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "'))))"
                                    Case Else
                                        str = str & " AND OppMas.numOppId in (select numOppId from OpportunityItems where numItemCode in  (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "'))))"
                                End Select
                            End If
                        ElseIf searchCondition.FieldType = "IA" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                Select Case searchCondition.SearchOperator
                                    Case 17 'Includes
                                        str = str & " and OppMas.numOppId in (Select OI.numOppId FROM OpportunityItems OI"
                                        str = str & " INNER JOIN Item I ON OI.numItemCode = I.numItemCode"
                                        str = str & " INNER JOIN CFW_Fld_Values_Serialized_Items SI ON I.bitSerialized = SI.bitSerialized AND OI.numWarehouseItmsID = SI.RecId"
                                        str = str & " WHERE SI.Fld_ID=" & strFieldID & " and OI.numWarehouseItmsID > 0 And numDomainID = " & lngDomainID & " "
                                        str = str & " and LOWER(SI.Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                    Case 18 'Excludes
                                        str = str & " and OppMas.numOppId NOT IN (Select OI.numOppId FROM OpportunityItems OI"
                                        str = str & " INNER JOIN Item I ON OI.numItemCode = I.numItemCode"
                                        str = str & " INNER JOIN CFW_Fld_Values_Serialized_Items SI ON I.bitSerialized = SI.bitSerialized AND OI.numWarehouseItmsID = SI.RecId"
                                        str = str & " WHERE SI.Fld_ID=" & strFieldID & " and OI.numWarehouseItmsID > 0 And numDomainID = " & lngDomainID & " "
                                        str = str & " and LOWER(SI.Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                    Case Else
                                        str = str & " and OppMas.numOppId in (Select OI.numOppId FROM OpportunityItems OI"
                                        str = str & " INNER JOIN Item I ON OI.numItemCode = I.numItemCode"
                                        str = str & " INNER JOIN CFW_Fld_Values_Serialized_Items SI ON I.bitSerialized = SI.bitSerialized AND OI.numWarehouseItmsID = SI.RecId"
                                        str = str & " WHERE SI.Fld_ID=" & strFieldID & " and OI.numWarehouseItmsID > 0 And numDomainID = " & lngDomainID & " "
                                        str = str & " and LOWER(SI.Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                End Select
                            End If
                        ElseIf searchCondition.FieldType = "C" Then
                            str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                        ElseIf searchCondition.FieldType = "D" Then
                            str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                        End If
                    ElseIf ControlType = "ListBox" Then
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If strFieldName = "numShipState" Or strFieldName = "numBillState" Then
                                If strFieldName = "numShipState" Then
                                    str = str & " and oppMas.numOppID in "
                                    str = str & " ("
                                    str = str & " SELECT numOppID FROM OpportunityMaster Where COALESCE(tintShipToType,1)=0 and numDomainId= " & lngDomainID & " AND numDivisionID in  "
                                    str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=2 and numState in (" & searchCondition.SearchValue & ")) "

                                    str = str & " union "

                                    ''WHEN tintShipToType=1
                                    str = str & " SELECT OM.numOppId FROM OpportunityMaster OM INNER JOIN DivisionMaster DM"
                                    str = str & " ON OM.numDivisionId = DM.numDivisionID WHERE COALESCE(OM.tintShipToType,1)=1 AND OM.numDomainId= " & lngDomainID & " AND DM.numDivisionID in "
                                    str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=2 "
                                    str = str & " and numState in (" & searchCondition.SearchValue & ")) "

                                    str = str & " union "

                                    'WHEN tintShipToType=2
                                    str = str & " SELECT OM.numOppId FROM OpportunityAddress OA INNER JOIN OpportunityMaster OM ON OA.numOppID = OM.numOppId WHERE COALESCE(OM.tintShipToType,1)=2 AND OM.numDomainId= " & lngDomainID
                                    str = str & " AND OA.numShipState in( " & searchCondition.SearchValue & ") "
                                    str = str & " ) "
                                ElseIf strFieldName = "numBillState" Then
                                    str = str & " AND oppMas.numOppID in (SELECT numOppID FROM OpportunityMaster Where COALESCE(tintBillToType,1)=0 and numDomainId= " & lngDomainID & " AND numDivisionID in (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=1 and numState in (" & searchCondition.SearchValue & "))"

                                    str = str & " union "

                                    str = str & " SELECT OM.numOppId FROM OpportunityMaster OM INNER JOIN DivisionMaster DM"
                                    str = str & " ON OM.numDivisionId = DM.numDivisionID WHERE COALESCE(OM.tintBillToType,1)=1 AND OM.numDomainId= " & lngDomainID & " AND DM.numDivisionID in "
                                    str = str & " (SELECT numRecordID FROM AddressDetails WHERE numDomainID=" & lngDomainID & " and tintAddressOf=2 AND tintAddressType=1 "
                                    str = str & " and numState in (" & searchCondition.SearchValue & ")) "

                                    str = str & " union "

                                    'WHEN tintBillToType=2
                                    str = str & " SELECT OM.numOppId FROM OpportunityAddress OA INNER JOIN OpportunityMaster OM ON OA.numOppID = OM.numOppId WHERE COALESCE(OM.tintBillToType,1)=2 AND OM.numDomainId= " & lngDomainID
                                    str = str & " AND OA.numBillState in( " & searchCondition.SearchValue & ") "
                                    str = str & " ) "
                                End If
                            End If
                        End If
                    ElseIf ControlType = "CheckBoxList" Then
                        Dim i As Integer = 0

                        If searchCondition.FieldType = "O" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                Select Case searchCondition.SearchOperator
                                    Case 18 'Excludes

                                        str = str & " AND OppMas.numOppId NOT IN (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & strFieldID & " AND ("

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                            i = i + 1
                                        Next
                                        str = str & ")"
                                        str = str & ")"
                                    Case Else
                                        'Includes
                                        str = str & " AND OppMas.numOppId IN (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & strFieldID & " AND ("

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                            i = i + 1
                                        Next
                                        str = str & ")"
                                        str = str & ")"
                                End Select
                            End If
                        ElseIf searchCondition.FieldType = "I" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                Select Case searchCondition.SearchOperator
                                    Case 18 'Excludes

                                        str = str & " AND OppMas.numOppId NOT IN (select numOppId from OpportunityItems where numItemCode in  (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " AND ("

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                            i = i + 1
                                        Next
                                        str = str & ")"
                                        str = str & ")"
                                        str = str & ")"
                                    Case Else
                                        'Includes
                                        str = str & " AND OppMas.numOppId in (select numOppId from OpportunityItems where numItemCode in  (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " AND ("

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                            i = i + 1
                                        Next
                                        str = str & ")"
                                        str = str & ")"
                                        str = str & ")"
                                End Select
                            End If
                        ElseIf searchCondition.FieldType = "IA" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                Select Case searchCondition.SearchOperator
                                    Case 18 'Excludes
                                        str = str & " and OppMas.numOppId NOT IN (Select OI.numOppId FROM OpportunityItems OI"
                                        str = str & " INNER JOIN Item I ON OI.numItemCode = I.numItemCode"
                                        str = str & " INNER JOIN CFW_Fld_Values_Serialized_Items SI ON I.bitSerialized = SI.bitSerialized AND OI.numWarehouseItmsID = SI.RecId"
                                        str = str & " WHERE SI.Fld_ID=" & strFieldID & " and OI.numWarehouseItmsID > 0 And numDomainID = " & lngDomainID & " "
                                        str = str & " and ("

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',SI.Fld_Value,','))) > 0"
                                            i = i + 1
                                        Next
                                        str = str & ")"
                                        str = str & ")"
                                    Case Else
                                        str = str & " and OppMas.numOppId in (Select OI.numOppId FROM OpportunityItems OI"
                                        str = str & " INNER JOIN Item I ON OI.numItemCode = I.numItemCode"
                                        str = str & " INNER JOIN CFW_Fld_Values_Serialized_Items SI ON I.bitSerialized = SI.bitSerialized AND OI.numWarehouseItmsID = SI.RecId"
                                        str = str & " WHERE SI.Fld_ID=" & strFieldID & " and OI.numWarehouseItmsID > 0 And numDomainID = " & lngDomainID & " "
                                        str = str & " and ("

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',SI.Fld_Value,','))) > 0"
                                            i = i + 1
                                        Next
                                        str = str & ")"
                                        str = str & ")"
                                End Select
                            End If
                        ElseIf searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    ElseIf ControlType = "DateField" Then
                        If searchCondition.FieldType = "R" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "O" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & " and OppMas.numOppID in (select RecId from CFW_Fld_Values_Opp where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ")"
                            End If
                        ElseIf searchCondition.FieldType = "I" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & " and OppMas.numOppID in (SELECT numOppID FROM OpportunityItems INNER JOIN Item ON OpportunityItems.numItemCode = Item.numItemCode WHERE Item.numItemCode IN (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & "))"
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    Else 'EditBox
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Or Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                            If searchCondition.FieldType = "R" Then
                                ''Based On the Look Back Table Sub Queries 
                                If searchCondition.LookBackTableName.ToLower() = "warehouses" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId)   from WareHouses WareHouse"
                                    str = str & " join WareHouseItems on WareHouseItems.numWareHouseID =WareHouse.numWareHouseID "
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId"
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode"
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId"
                                    str = str & " where OppMas.numDomainId= " & lngDomainID
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    str = str & " ) "
                                ElseIf searchCondition.LookBackTableName.ToLower() = "warehouseitmsdtl" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId) from WareHouseItmsDTL WareHouseItemDTL"
                                    str = str & " join WareHouseItems on WareHouseItemDTL.numWareHouseItemID =WareHouseItems.numWareHouseItemID "
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId"
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode"
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId "
                                    str = str & " join OppWarehouseSerializedItem OppSerItm on (OppSerItm.numOppId = OppMas.numOppId and WareHouseItemDTL.numWarehouseItmsDTLID= OppSerItm.numWarehouseItmsDTLID) "
                                    str = str & " where OppMas.numDomainId= " & lngDomainID
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    str = str & " ) "
                                ElseIf searchCondition.LookBackTableName.ToLower() = "warehouseitems" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppMas.numOppId)    from WareHouseItems"
                                    str = str & " join Item on Item.numItemCode = WareHouseItems.numItemId   "
                                    str = str & " Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode  "
                                    str = str & " join OpportunityMaster OppMas on OppItems.numOppId = OppMas.numOppId "
                                    str = str & " where OppMas.numDomainId= " & lngDomainID
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    str = str & " ) "
                                ElseIf searchCondition.LookBackTableName.ToLower() = "item" Then
                                    str = str & " and oppMas.numOppId in "
                                    str = str & " (select distinct(OppItems.numOppId)   from item "
                                    str = str & "Join OpportunityItems OppItems on Item.numItemCode = OppItems.numItemCode AND Item.numDomainID = " & lngDomainID & " "
                                    If strFieldName = "vcItemName" Or strFieldName = "vcModelID" Or strFieldName = "vcManufacturer" Then
                                        str = str & " and ( " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "Item.", timeZoneOffset) & " or " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "OppItems.", timeZoneOffset) & " )"
                                    ElseIf strFieldName = "txtItemDesc" Then
                                        str = str & " and ( " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "Item.", timeZoneOffset) & " or " & FormGenericAdvSearch.GetAdvancedSearchCondition("vcItemDesc", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "OppItems.", timeZoneOffset) & " )"
                                    ElseIf strFieldName = "monPrice" Then
                                        str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "OppItems.", timeZoneOffset)
                                    Else
                                        str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "Item.", timeZoneOffset)
                                    End If
                                    str = str & " ) "
                                Else
                                    If strFieldName = "monDealAmount" And searchCondition.LookBackTableName = "OpportunityBizDocs" Then
                                        strPrefix = ""
                                        str = str & " AND " & FormGenericAdvSearch.GetAdvancedSearchCondition("COALESCE((SELECT SUM(COALESCE(monDealAmount,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    ElseIf strFieldName = "monAmountPaid" And searchCondition.LookBackTableName = "OpportunityBizDocs" Then
                                        strPrefix = ""
                                        str = str & " AND " & FormGenericAdvSearch.GetAdvancedSearchCondition("COALESCE((SELECT SUM(COALESCE(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND COALESCE(bitAuthoritativeBizDocs,0)=1),0)", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    ElseIf strFieldName = "CalAmount" And searchCondition.LookBackTableName = "OpportunityMaster" Then
                                        strPrefix = ""
                                        str = str & " AND " & FormGenericAdvSearch.GetAdvancedSearchCondition("getdealamount(OppMas.numOppId,timezone('utc', now()),0)", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    ElseIf strFieldName = "vcRefOrderNo" And searchCondition.LookBackTableName = "OpportunityBizDocs" Then
                                        str = str & " and (" & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                        strPrefix = "OppMas."
                                        str = str & " or " & FormGenericAdvSearch.GetAdvancedSearchCondition("vcOppRefOrderNo", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ")"
                                    ElseIf strFieldName = "dtShippedDate" And searchCondition.LookBackTableName = "OpportunityBizDocs" Then
                                        strPrefix = ""
                                        str = str & " AND oppMas.numOppId in (SELECT numOppID FROM OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND " & FormGenericAdvSearch.GetAdvancedSearchCondition("dtShippedDate", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "BD.", timeZoneOffset) & ")"
                                    Else
                                        str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                    End If
                                End If

                            ElseIf searchCondition.FieldType = "O" Then
                                str = str & " and OppMas.numOppId in (select RecId from CFW_FLD_Values_Opp where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ")"
                            ElseIf searchCondition.FieldType = "I" Then
                                str = str & " and OppMas.numOppId in (select numOppId from OpportunityItems where numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & "))"
                            ElseIf searchCondition.FieldType = "C" Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "D" Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    End If
                Next

                Return str
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SearchCases(ByVal lngDomainID As Long, ByVal timeZoneOffset As Integer, ByVal strSearchConditions As String) As String
            Try
                Dim str As String = ""
                Dim ControlType, strFieldName, strFieldID, strPrefix, strFriendlyName As String
                Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
                Dim listConditions As System.Collections.Generic.List(Of AdvancedSearchCondition) = serializer.Deserialize(Of System.Collections.Generic.List(Of AdvancedSearchCondition))(strSearchConditions)

                For Each searchCondition As AdvancedSearchCondition In listConditions
                    ControlType = searchCondition.AssociatedControlType
                    strFieldName = searchCondition.DbColumnName.Trim.Replace(" ", "_")
                    strFriendlyName = searchCondition.FieldName
                    strFieldID = searchCondition.FieldID

                    If searchCondition.LookBackTableName = "DivisionMaster" Then
                        strPrefix = "DM."
                    ElseIf searchCondition.LookBackTableName = "CompanyInfo" Then
                        strPrefix = "C."
                    ElseIf searchCondition.LookBackTableName = "AdditionalContactsInformation" Then
                        strPrefix = "ADC."
                    ElseIf Not strFieldName.EndsWith("_C") Then
                        strPrefix = searchCondition.LookBackTableName & "."
                    Else
                        strPrefix = ""
                    End If

                    If ControlType = "SelectBox" Then
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If searchCondition.FieldType = "R" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "C" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "D" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "CA" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    Select Case searchCondition.SearchOperator
                                        Case 17 'Includes
                                            str = str & " AND Cases.numCaseId IN (select RecId from CFW_FLD_Values_Case where Fld_ID =" & searchCondition.FieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                        Case 18 'Excludes
                                            str = str & " AND Cases.numCaseId NOT IN (select RecId from CFW_FLD_Values_Case where Fld_ID =" & searchCondition.FieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                        Case Else
                                            str = str & " AND Cases.numCaseId IN (select RecId from CFW_FLD_Values_Case where Fld_ID =" & searchCondition.FieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                    End Select
                                End If
                            End If
                        End If
                    ElseIf ControlType = "CheckBox" Then
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If searchCondition.FieldType = "R" Then
                                Dim strSqlCondition As String = FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                str = str & " and " & strSqlCondition
                            ElseIf searchCondition.FieldType = "C" Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "D" Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "CA" Then
                                str = str & " and Cases.numCaseId IN (select RecId from CFW_FLD_Values_Case where Fld_ID =" & strFieldID & " and COALESCE(Fld_Value,'0') = '" & If(CCommon.ToBool(searchCondition.SearchValue), "1", "0") & "')"
                            End If
                        End If
                    ElseIf ControlType = "DateField" Then
                        If searchCondition.FieldType = "R" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "CA" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & " and Cases.numCaseId in (select RecId from CFW_FLD_Values_Case where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ")"
                            End If
                        End If
                    ElseIf ControlType = "CheckBoxList" Then
                        If searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "CA" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                Dim i As Integer = 0
                                Select Case searchCondition.SearchOperator
                                    Case 18 'Excludes
                                        str = str & " AND Cases.numCaseId NOT IN (select RecId from CFW_FLD_Values_Case where Fld_ID =" & strFieldID & " AND ("

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                            i = i + 1
                                        Next
                                        str = str & ")"
                                        str = str & ")"
                                    Case Else
                                        str = str & " AND Cases.numCaseId IN (select RecId from CFW_FLD_Values_Case where Fld_ID =" & strFieldID & " AND ("

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                            i = i + 1
                                        Next
                                        str = str & ")"
                                        str = str & ")"
                                End Select
                            End If
                        End If
                    Else
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Or Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                            If searchCondition.FieldType = "R" Then
                                str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "C" Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "D" Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "CA" Then
                                str = str & " and Cases.numCaseId in (select RecId from CFW_FLD_Values_Case where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ")" ' Fld_Value ILIKE '" & IIf(rbListSearchContext.SelectedValue = 1, "%", "") & strFieldValue & "%' )"
                            End If
                        End If
                    End If
                Next

                Return str
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SearchProjects(ByVal lngDomainID As Long, ByVal timeZoneOffset As Integer, ByVal strSearchConditions As String) As String
            Try
                Dim str As String = ""
                Dim ControlType, strFieldName, strFieldID, strPrefix, strFriendlyName As String
                Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
                Dim listConditions As System.Collections.Generic.List(Of AdvancedSearchCondition) = serializer.Deserialize(Of System.Collections.Generic.List(Of AdvancedSearchCondition))(strSearchConditions)

                For Each searchCondition As AdvancedSearchCondition In listConditions
                    ControlType = searchCondition.AssociatedControlType
                    strFieldName = searchCondition.DbColumnName.Trim.Replace(" ", "_")
                    strFriendlyName = searchCondition.FieldName
                    strFieldID = searchCondition.FieldID

                    If searchCondition.LookBackTableName = "DivisionMaster" Then
                        strPrefix = "DM."
                    ElseIf searchCondition.LookBackTableName = "CompanyInfo" Then
                        strPrefix = "C."
                    ElseIf searchCondition.LookBackTableName = "AdditionalContactsInformation" Then
                        strPrefix = "ADC."
                    ElseIf searchCondition.LookBackTableName = "ProjectsMaster" Then
                        strPrefix = "ProMas."
                    ElseIf Not strFieldName.EndsWith("_C") Then
                        strPrefix = searchCondition.LookBackTableName & "."
                    Else
                        strPrefix = ""
                    End If

                    If ControlType = "SelectBox" Then
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If searchCondition.FieldType = "R" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "C" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "D" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "P" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    Select Case searchCondition.SearchOperator
                                        Case 17 'Includes
                                            str = str & " AND ProMas.numProId IN (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & searchCondition.FieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                        Case 18 'Excludes
                                            str = str & " AND ProMas.numProId NOT IN (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & searchCondition.FieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                        Case Else
                                            str = str & " AND ProMas.numProId IN (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & searchCondition.FieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))"
                                    End Select
                                End If
                            End If
                        End If
                    ElseIf ControlType = "CheckBox" Then
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If searchCondition.FieldType = "R" Then
                                Dim strSqlCondition As String = FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                str = str & " and " & strSqlCondition
                            ElseIf searchCondition.FieldType = "C" Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "D" Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "P" Then
                                str = str & " and ProMas.numProId IN (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & strFieldID & " and COALESCE(Fld_Value,'0') = '" & If(CCommon.ToBool(searchCondition.SearchValue), "1", "0") & "')"
                            End If
                        End If
                    ElseIf ControlType = "DateField" Then
                        If searchCondition.FieldType = "R" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "P" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & " and ProMas.numProId in (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ")"
                            End If
                        End If
                    ElseIf ControlType = "CheckBoxList" Then
                        If searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "P" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                Dim i As Integer = 0

                                Select Case searchCondition.SearchOperator
                                    Case 18 'Excludes
                                        str = str & " AND ProMas.numProId NOT IN (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & strFieldID & " AND ("

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                            i = i + 1
                                        Next
                                        str = str & ")"
                                        str = str & ")"
                                    Case Else
                                        str = str & " AND ProMas.numProId IN (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & strFieldID & " AND ("

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                            i = i + 1
                                        Next
                                        str = str & ")"
                                        str = str & ")"
                                End Select
                            End If
                        End If
                    Else
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Or Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                            If searchCondition.FieldType = "R" Then
                                If strFieldName = "numCustPrjMgr" Then
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition("fn_GetContactName(" & strFieldName & ")", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "", timeZoneOffset)
                                Else
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "C" Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "D" Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "P" Then
                                str = str & " and ProMas.numProId in (select RecId from CFW_FLD_Values_Pro where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ")" ' Fld_Value ILIKE '" & IIf(rbListSearchContext.SelectedValue = 1, "%", "") & strFieldValue & "%' )"
                            End If
                        End If
                    End If
                Next

                Return str
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SearchItems(ByVal lngDomainID As Long, ByVal timeZoneOffset As Integer, ByVal strSearchConditions As String) As String
            Try
                Dim sb As New Text.StringBuilder
                Dim ControlType, strFieldName, strFieldID, strPrefix, strFriendlyName As String
                Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
                Dim listConditions As System.Collections.Generic.List(Of AdvancedSearchCondition) = serializer.Deserialize(Of System.Collections.Generic.List(Of AdvancedSearchCondition))(strSearchConditions)

                For Each searchCondition As AdvancedSearchCondition In listConditions
                    ControlType = searchCondition.AssociatedControlType
                    strFieldName = searchCondition.DbColumnName.Trim.Replace(" ", "_")
                    strFriendlyName = searchCondition.FieldName
                    strFieldID = searchCondition.FieldID

                    If searchCondition.LookBackTableName.ToLower() = "divisionmaster" Then
                        strPrefix = "DM."
                    ElseIf searchCondition.LookBackTableName.ToLower() = "vendor" Then
                        strPrefix = "V."
                    ElseIf searchCondition.LookBackTableName.ToLower() = "companyinfo" Then
                        strPrefix = "C."
                    ElseIf searchCondition.LookBackTableName.ToLower() = "additionalcontactsinformation" Then
                        strPrefix = "ADC."
                    ElseIf searchCondition.LookBackTableName.ToLower() = "item" Then
                        strPrefix = "I."
                    ElseIf Not strFieldName.EndsWith("_C") Then
                        strPrefix = searchCondition.LookBackTableName & "."
                    Else
                        strPrefix = ""
                    End If

                    If ControlType = "SelectBox" Then 'Dropdown
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If searchCondition.FieldType = "R" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    If strFieldName.ToLower() = "numcategoryid" Then
                                        sb.Append(" AND I.numItemCode IN (SELECT numItemID FROM ItemCategory WHERE numCategoryID IN (" & searchCondition.SearchValue & "))")
                                    Else
                                        If strFieldName = "charItemType" Then
                                            searchCondition.SearchValue = "'" & searchCondition.SearchValue.Replace(",", "','") & "'"
                                        End If

                                        sb.Append(" and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset))
                                    End If
                                End If
                            ElseIf searchCondition.FieldType = "C" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    sb.Append(GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset))
                                End If
                            ElseIf searchCondition.FieldType = "D" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    sb.Append(GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset))
                                End If
                            ElseIf searchCondition.FieldType = "I" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    Select Case searchCondition.SearchOperator
                                        Case 17 'Includes
                                            sb.Append(" AND I.numItemCode IN (select RecId from CFW_FLD_Values_Item where Fld_ID =" & searchCondition.FieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))")
                                        Case 18 'Excludes
                                            sb.Append(" AND I.numItemCode NOT IN (select RecId from CFW_FLD_Values_Item where Fld_ID =" & searchCondition.FieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))")
                                        Case Else
                                            sb.Append(" AND I.numItemCode IN (select RecId from CFW_FLD_Values_Item where Fld_ID =" & searchCondition.FieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))")
                                    End Select
                                End If
                            ElseIf searchCondition.FieldType = "IA" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    Select Case searchCondition.SearchOperator
                                        Case 18 'Excludes
                                            sb.Append(" and I.numItemCode NOT IN (Select I.numItemCode from ")
                                            sb.Append(" Item I INNER JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode ")
                                            sb.Append(" INNER JOIN CFW_Fld_Values_Serialized_Items SI ON I.bitSerialized = SI.bitSerialized AND WI.numWareHouseItemID = SI.RecId")
                                            sb.Append(" WHERE SI.Fld_ID=" & strFieldID & " and WI.numWareHouseItemID > 0 And I.numDomainID = " & lngDomainID & " ")
                                            sb.Append(" and SI.LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))")
                                        Case Else
                                            'Includes
                                            sb.Append(" and I.numItemCode IN (Select I.numItemCode from ")
                                            sb.Append(" Item I INNER JOIN WareHouseItems WI ON WI.numItemID = I.numItemCode ")
                                            sb.Append(" INNER JOIN CFW_Fld_Values_Serialized_Items SI ON I.bitSerialized = SI.bitSerialized AND WI.numWareHouseItemID = SI.RecId")
                                            sb.Append(" WHERE SI.Fld_ID=" & strFieldID & " and WI.numWareHouseItemID > 0 And I.numDomainID = " & lngDomainID & " ")
                                            sb.Append(" and SI.LOWER(Fld_Value) IN (LOWER('" & searchCondition.SearchValue & "')))")
                                    End Select
                                End If
                            End If
                        End If
                    ElseIf ControlType = "ListBox" Then
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If strFieldName = "numVendorID" Then
                                sb.Append(" AND I.numItemCode IN (SELECT DISTINCT numItemCode FROM vendor WHERE numVendorID in (" & searchCondition.SearchValue & ") ) ")
                                If Not HttpContext.Current Is Nothing Then
                                    HttpContext.Current.Session("VendorIDs") = searchCondition.SearchValue
                                End If
                            End If
                        End If
                    ElseIf ControlType = "CheckBoxList" Then
                        If searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                sb.Append(GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset))
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                sb.Append(GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset))
                            End If
                        ElseIf searchCondition.FieldType = "I" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                Dim i As Integer = 0

                                Select Case searchCondition.SearchOperator
                                    Case 18 'Excludes
                                        sb.Append(" AND ProMas.numProId NOT IN (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " AND (")

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            sb.Append(If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0")
                                            i = i + 1
                                        Next
                                        sb.Append(")")
                                        sb.Append(")")
                                    Case Else
                                        sb.Append(" AND ProMas.numProId IN (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " AND (")

                                        Dim searchValues() As String = searchCondition.SearchValue.Split(",")

                                        For Each Item As Long In searchValues
                                            sb.Append(If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0")
                                            i = i + 1
                                        Next
                                        sb.Append(")")
                                        sb.Append(")")
                                End Select
                            End If
                        End If
                    ElseIf ControlType = "CheckBox" Then 'Added by chintan, reason: custom field CheckBox code isn't handled
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If searchCondition.FieldType = "R" Then
                                Dim strSqlCondition As String = FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                sb.Append(" and " & strSqlCondition)
                            ElseIf searchCondition.FieldType = "C" Then
                                sb.Append(GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset))
                            ElseIf searchCondition.FieldType = "D" Then
                                sb.Append(GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset))
                            ElseIf searchCondition.FieldType = "I" Then
                                sb.Append(" and I.numItemCode IN (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and COALESCE(Fld_Value,'0') = '" & If(CCommon.ToBool(searchCondition.SearchValue), "1", "0") & "')")
                            End If
                        End If
                    ElseIf ControlType = "DateField" Then
                        If searchCondition.FieldType = "R" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                sb.Append(" and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset))
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                sb.Append(GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset))
                            End If
                        ElseIf searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                sb.Append(GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset))
                            End If
                        ElseIf searchCondition.FieldType = "P" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                sb.Append(" and I.numItemCod in (select RecId from CFW_FLD_Values_Item where Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ")")
                            End If
                        End If
                    Else 'Textbox
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Or Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                            If searchCondition.FieldType = "R" Then
                                If strFieldName = "monListPrice" Then
                                    sb.Append(" AND I.numItemCode IN (SELECT distinct numItemID FROM WareHouseItems WHERE numDomainID=" & lngDomainID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset))
                                    sb.Append(" union select distinct numitemcode from Item Where numDomainID=" & lngDomainID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & " )")
                                ElseIf searchCondition.LookBackTableName = "WareHouseItems" Then
                                    If strFieldName = "vcBarCode" Then
                                        sb.Append(" AND I.numItemCode IN (SELECT numItemID FROM WareHouseItems WHERE numDomainID=" & lngDomainID & " AND " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & " group by numItemID)")
                                    ElseIf strFieldName = "vcWHSKU" Then
                                        sb.Append(" AND I.numItemCode IN (SELECT numItemID FROM WareHouseItems WHERE numDomainID=" & lngDomainID & " AND " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & " group by numItemID)")
                                    Else
                                        sb.Append(" AND I.numItemCode IN (SELECT numItemID FROM WareHouseItems WHERE numDomainID=" & lngDomainID & " group by numItemID HAVING " & FormGenericAdvSearch.GetAdvancedSearchCondition("SUM(" & strFieldName & ")", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "", timeZoneOffset) & ")")
                                    End If
                                ElseIf strFieldName = "vcSerialNo" Then
                                    sb.Append(" AND numWareHouseItemID IN (SELECT numWareHouseItemID FROM WareHouseItmsDTL WHERE " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & " AND COALESCE(numQty,0) > 0) AND WI.numDomainID=" & lngDomainID)
                                Else
                                    sb.Append(" and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset))
                                End If
                            ElseIf searchCondition.FieldType = "C" Then
                                sb.Append(GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset))
                            ElseIf searchCondition.FieldType = "D" Then
                                sb.Append(GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset))
                            ElseIf searchCondition.FieldType = "I" Then
                                sb.Append(" and I.numItemCode in (SELECT RecId FROM CFW_FLD_Values_Item WHERE Fld_ID =" & strFieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset) & ")")
                            End If
                        End If
                    End If
                Next

                Return sb.ToString()
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SearchFinancialTransactions(ByVal lngDomainID As Long, ByVal timeZoneOffset As Integer, ByVal strSearchConditions As String) As String
            Try
                Dim str As String = ""
                Dim ControlType, strFieldName, strFieldID, strPrefix, strFriendlyName As String
                Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer
                Dim listConditions As System.Collections.Generic.List(Of AdvancedSearchCondition) = serializer.Deserialize(Of System.Collections.Generic.List(Of AdvancedSearchCondition))(strSearchConditions)

                For Each searchCondition As AdvancedSearchCondition In listConditions
                    ControlType = searchCondition.AssociatedControlType
                    strFieldName = searchCondition.DbColumnName.Trim.Replace(" ", "_")
                    strFriendlyName = searchCondition.FieldName
                    strFieldID = searchCondition.FieldID

                    If searchCondition.LookBackTableName = "CompanyInfo" Then
                        strPrefix = "C."
                    ElseIf searchCondition.LookBackTableName = "DivisionMaster" Then
                        strPrefix = "DM."
                    ElseIf searchCondition.LookBackTableName = "General_Journal_Header" Then
                        strPrefix = "GJH."
                    ElseIf searchCondition.LookBackTableName = "General_Journal_Details" Then
                        strPrefix = "GJD."
                    ElseIf searchCondition.LookBackTableName = "CheckHeader" Then
                        strPrefix = "CH."
                    ElseIf Not strFieldName.EndsWith("_C") Then
                        strPrefix = searchCondition.LookBackTableName & "."
                    Else
                        strPrefix = ""
                    End If

                    If ControlType = "SelectBox" Then
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If searchCondition.FieldType = "R" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "C" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "D" Then
                                If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                                    str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                                End If
                            End If
                        End If
                    ElseIf ControlType = "CheckBox" Then
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            If searchCondition.FieldType = "R" Then
                                Dim strSqlCondition As String = FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                str = str & " and " & strSqlCondition
                            ElseIf searchCondition.FieldType = "C" Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "D" Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    ElseIf ControlType = "ListBox" Then
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Then
                            str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                        End If
                    ElseIf ControlType = "DateField" Then
                        If searchCondition.FieldType = "R" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "D" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        ElseIf searchCondition.FieldType = "C" Then
                            If Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    Else 'EditBox
                        If Not String.IsNullOrEmpty(searchCondition.SearchValue) Or Not String.IsNullOrEmpty(searchCondition.SearchFrom) Or Not String.IsNullOrEmpty(searchCondition.SearchTo) Then
                            If searchCondition.FieldType = "R" Then
                                If strFieldName = "monAmount" Then
                                    str = str & " and (" & FormGenericAdvSearch.GetAdvancedSearchCondition("numCreditAmt", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "GJD.", timeZoneOffset)
                                    str = str & " or " & FormGenericAdvSearch.GetAdvancedSearchCondition("numDebitAmt", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "GJD.", timeZoneOffset) & ")"
                                ElseIf strFieldName = "varDescription" Then
                                    str = str & " and (" & FormGenericAdvSearch.GetAdvancedSearchCondition("varDescription", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "GJD.", timeZoneOffset)
                                    str = str & " or " & FormGenericAdvSearch.GetAdvancedSearchCondition("varDescription", searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), "GJH.", timeZoneOffset) & ")"
                                Else
                                    str = str & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(strFieldName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(strFieldName.EndsWith("_C"), True, False), strPrefix, timeZoneOffset)
                                End If
                            ElseIf searchCondition.FieldType = "C" Then
                                str = str & GetContactCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            ElseIf searchCondition.FieldType = "D" Then
                                str = str & GetOrganizationCustomFieldSearchString(ControlType, searchCondition.SearchOperator, strFieldID, searchCondition.SearchValue, searchCondition, timeZoneOffset)
                            End If
                        End If
                    End If
                Next

                Return str
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Function GetOrganizationCustomFieldSearchString(ByVal controlType As String, ByVal searchOperator As Integer, ByVal fieldID As Long, ByVal searchText As String, ByVal searchCondition As AdvancedSearchCondition, ByVal timeZoneOffset As Integer) As String
            Try
                If controlType = "SelectBox" Then
                    Select Case searchOperator
                        Case 18 'Excludes
                            Return " AND DM.numDivisionID NOT IN (select RecId from CFW_FLD_Values where Fld_ID =" & fieldID & " AND LOWER(Fld_Value) IN ('" & searchText & "'))"
                        Case Else
                            'Includes
                            Return " AND DM.numDivisionID IN (select RecId from CFW_FLD_Values where Fld_ID =" & fieldID & " AND LOWER(Fld_Value) IN ('" & searchText & "'))"
                    End Select
                ElseIf controlType = "CheckBox" Then
                    Return " and DM.numDivisionID IN (select RecId from CFW_FLD_Values where Fld_ID =" & fieldID & " and COALESCE(Fld_Value,'0') = '" & If(CCommon.ToBool(searchText), "1", "0") & "')"
                ElseIf controlType = "DateField" Then
                    Return " and DM.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID =" & fieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(searchCondition.DbColumnName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(searchCondition.DbColumnName.EndsWith("_C"), True, False), "", timeZoneOffset) & ")"
                ElseIf controlType = "CheckBoxList" Then
                    Dim str As String = ""
                    Dim i As Integer = 0
                    Select Case searchOperator
                        Case 18 'Excludes
                            str = str & " AND DM.numDivisionID NOT IN (select RecId from CFW_FLD_Values where Fld_ID =" & fieldID & " AND ("

                            Dim searchValues() As String = searchText.Split(",")

                            For Each Item As Long In searchValues
                                str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                i = i + 1
                            Next
                            str = str.TrimEnd("OR")
                            str = str & ")"
                            str = str & ")"
                        Case Else
                            'Includes
                            str = str & " AND DM.numDivisionID IN (select RecId from CFW_FLD_Values where Fld_ID =" & fieldID & " AND ("

                            Dim searchValues() As String = searchText.Split(",")

                            For Each Item As Long In searchValues
                                str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                i = i + 1
                            Next
                            str = str.TrimEnd("OR")
                            str = str & ")"
                            str = str & ")"
                    End Select

                    Return str
                Else
                    Return " and DM.numDivisionID in (select RecId from CFW_FLD_Values where Fld_ID =" & fieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(searchCondition.DbColumnName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(searchCondition.DbColumnName.EndsWith("_C"), True, False), "", timeZoneOffset) & ")"
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Function GetContactCustomFieldSearchString(ByVal controlType As String, ByVal searchOperator As Integer, ByVal fieldID As Long, ByVal searchText As String, ByVal searchCondition As AdvancedSearchCondition, ByVal timeZoneOffset As Integer) As String
            Try
                If controlType = "SelectBox" Then
                    Select Case searchOperator
                        Case 18 'Excludes
                            Return " AND ADC.numContactID NOT IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & fieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchText & "')))"
                        Case Else
                            'Includes
                            Return " AND ADC.numContactID IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & fieldID & " AND LOWER(Fld_Value) IN (LOWER('" & searchText & "')))"
                    End Select
                ElseIf controlType = "CheckBox" Then
                    Return " and ADC.numContactID IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & fieldID & " and COALESCE(Fld_Value,'0') = '" & If(CCommon.ToBool(searchText), "1", "0") & "')"
                ElseIf controlType = "DateField" Then
                    Return " and ADC.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & fieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(searchCondition.DbColumnName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(searchCondition.DbColumnName.EndsWith("_C"), True, False), "", timeZoneOffset) & ")"
                ElseIf controlType = "CheckBoxList" Then
                    Dim str As String = ""
                    Dim i As Integer = 0
                    Select Case searchOperator
                        Case 18 'Excludes

                            str = str & " AND ADC.numContactID NOT IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & fieldID & " AND ("

                            Dim searchValues() As String = searchText.Split(",")

                            For Each Item As Long In searchValues
                                str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                i = i + 1
                            Next
                            str = str.TrimEnd("OR")
                            str = str & ")"
                            str = str & ")"
                        Case Else
                            'Includes
                            str = str & " AND ADC.numContactID IN (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & fieldID & " AND ("

                            Dim searchValues() As String = searchText.Split(",")

                            For Each Item As Long In searchValues
                                str = str & If(i > 0, " OR ", "") & "position(LOWER(CONCAT(','," & Item & ",',')) IN LOWER(CONCAT(',',Fld_Value,','))) > 0"
                                i = i + 1
                            Next
                            str = str.TrimEnd("OR")
                            str = str & ")"
                            str = str & ")"
                    End Select

                    Return str
                Else
                    Return " and ADC.numContactID in (select RecId from CFW_FLD_Values_Cont where Fld_ID =" & fieldID & " and " & FormGenericAdvSearch.GetAdvancedSearchCondition(searchCondition.DbColumnName, searchCondition.SearchOperator, searchCondition.SearchValue, searchCondition.SearchFrom, searchCondition.SearchTo, If(searchCondition.DbColumnName.EndsWith("_C"), True, False), "", timeZoneOffset) & ")"
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Class AdvancedSearchCondition
            Public Property FormID As Long
            Public Property FieldID As Long
            Public Property AssociatedControlType As String
            Public Property FieldName As String
            Public Property ListItemType As String
            Public Property LookBackTableName As String
            Public Property DbColumnName As String
            Public Property SearchOperator As Integer
            Public Property SearchValue As String
            Public Property SearchFrom As String
            Public Property SearchTo As String
            Public Property Prefix As String
            Public Property FieldType As String
        End Class
    End Class
End Namespace
