'Modified by Anoop Jayaraj
Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Web.UI.WebControls

Namespace BACRM.BusinessLogic.Admin
    Public Class CAdmin
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"

        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub


        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Private _Relationship As Long
        'Private DomainId As Long
        Private _ContactType As Long
        'Private _HomePageID As Long
        'Private _HomePage As String
        Private _AOIID As Long
        Private _AOIName As String
        'Private UserCntID As Long
        Private _bitDeleted As Boolean
        Private _SalesProsID As Long
        Private _SalesProsName As String
        Private _ProsType As Short
        Private _StagePerID As Long
        Private _Configuration As Integer
        Private _StageDetail As String
        Private _DelFlag As Integer
        Private _CLLostFlag As Integer
        Private _AssignTo As Long
        Private _TemplateId As Long
        Private _MileStoneName As String
        Private _strLead As String = ""
        Private _strContact As String = ""
        Private _strProspect As String = ""
        Private _strAccount As String = ""
        Private _Mode As Integer
        Private _ModeType As Integer


        Private _Percentage As Short
        Private _DivisionID As Long
        Private _AccessID As Long

        Private _numEvent As Integer
        Private _numReminder As Integer
        Private _numET As Integer
        Private _numChgStatus As Long
        Private _bitChgStatus As Boolean
        Private _bitClose As Boolean


        Private _strItems As String
        Public Property strItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal value As String)
                _strItems = value
            End Set
        End Property

        Private _byteMode As Short
        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal value As Short)
                _byteMode = value
            End Set
        End Property

        Public Property numChgStatus() As Long
            Get
                Return _numChgStatus
            End Get
            Set(ByVal value As Long)
                _numChgStatus = value
            End Set
        End Property

        Public Property bitChgStatus() As Boolean
            Get
                Return _bitChgStatus
            End Get
            Set(ByVal value As Boolean)
                _bitChgStatus = value
            End Set
        End Property

        Public Property bitClose() As Boolean
            Get
                Return _bitClose
            End Get
            Set(ByVal value As Boolean)
                _bitClose = value
            End Set
        End Property
        Public Property numEvent() As Integer
            Get
                Return _numEvent
            End Get
            Set(ByVal value As Integer)
                _numEvent = value
            End Set
        End Property
        Public Property numReminder() As Integer
            Get
                Return _numReminder
            End Get
            Set(ByVal value As Integer)
                _numReminder = value
            End Set
        End Property
        Public Property numET() As Integer
            Get
                Return _numET
            End Get
            Set(ByVal value As Integer)
                _numET = value
            End Set
        End Property
        Private _numActivity As Long
        Private _numSD As Integer
        Private _numSTT As Integer
        Private _numSTP As Integer

        Public Property numActivity() As Long
            Get
                Return _numActivity
            End Get
            Set(ByVal value As Long)
                _numActivity = value
            End Set
        End Property
        Public Property numSD() As Integer
            Get
                Return _numSD
            End Get
            Set(ByVal value As Integer)
                _numSD = value
            End Set
        End Property
        Public Property numSTT() As Integer
            Get
                Return _numSTT
            End Get
            Set(ByVal value As Integer)
                _numSTT = value
            End Set
        End Property
        Public Property numSTP() As Integer
            Get
                Return _numSTP
            End Get
            Set(ByVal value As Integer)
                _numSTP = value
            End Set
        End Property
        Private _numETT As Integer
        Private _numETP As Integer
        Private _Description As String
        Public Property numETT() As Integer
            Get
                Return _numETT
            End Get
            Set(ByVal value As Integer)
                _numETT = value
            End Set
        End Property
        Public Property numETP() As Integer
            Get
                Return _numETP
            End Get
            Set(ByVal value As Integer)
                _numETP = value
            End Set
        End Property
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal Value As String)
                _Description = Value
            End Set
        End Property


        Public Property AccessID() As Long
            Get
                Return _AccessID
            End Get
            Set(ByVal value As Long)
                _AccessID = value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal value As Long)
                _DivisionID = value
            End Set
        End Property

        Public Property Percentage() As Short
            Get
                Return _Percentage
            End Get
            Set(ByVal value As Short)
                _Percentage = value
            End Set
        End Property
        Public Property Mode() As Integer
            Get
                Return _Mode
            End Get
            Set(ByVal value As Integer)
                _Mode = value
            End Set
        End Property
        Public Property ModeType() As Integer
            Get
                Return _ModeType
            End Get
            Set(ByVal value As Integer)
                _ModeType = value
            End Set
        End Property
        Public Property strLead() As String
            Get
                Return _strLead
            End Get
            Set(ByVal Value As String)
                _strLead = Value
            End Set
        End Property

        Private _strLeadPlural As String
        Public Property strLeadPlural() As String
            Get
                Return _strLeadPlural
            End Get
            Set(ByVal value As String)
                _strLeadPlural = value
            End Set
        End Property

        Private _strProspectPlural As String
        Public Property strProspectPlural() As String
            Get
                Return _strProspectPlural
            End Get
            Set(ByVal value As String)
                _strProspectPlural = value
            End Set
        End Property

        Private _strAccountPlural As String
        Public Property strAccountPlural() As String
            Get
                Return _strAccountPlural
            End Get
            Set(ByVal value As String)
                _strAccountPlural = value
            End Set
        End Property

        Private _strContactPlural As String
        Public Property strContactPlural() As String
            Get
                Return _strContactPlural
            End Get
            Set(ByVal value As String)
                _strContactPlural = value
            End Set
        End Property

        Public Property strContact() As String
            Get
                Return _strContact
            End Get
            Set(ByVal Value As String)
                _strContact = Value
            End Set
        End Property
        Public Property strAccount() As String
            Get
                Return _strAccount
            End Get
            Set(ByVal Value As String)
                _strAccount = Value
            End Set
        End Property
        Public Property MileStoneName() As String
            Get
                Return _MileStoneName
            End Get
            Set(ByVal Value As String)
                _MileStoneName = Value
            End Set
        End Property
        Public Property strProspect() As String
            Get
                Return _strProspect
            End Get
            Set(ByVal Value As String)
                _strProspect = Value
            End Set
        End Property
        Public Property TemplateId() As Long
            Get
                Return _TemplateId
            End Get
            Set(ByVal value As Long)
                _TemplateId = value
            End Set
        End Property
        Public Property Assignto() As Long
            Get
                Return _AssignTo
            End Get
            Set(ByVal value As Long)
                _AssignTo = value
            End Set
        End Property
        Public Property CLLostFlag() As Integer
            Get
                Return _CLLostFlag
            End Get
            Set(ByVal Value As Integer)
                _CLLostFlag = Value
            End Set
        End Property

        Public Property DelFlag() As Integer
            Get
                Return _DelFlag
            End Get
            Set(ByVal Value As Integer)
                _DelFlag = Value
            End Set
        End Property

        Public Property StageDetail() As String
            Get
                Return _StageDetail
            End Get
            Set(ByVal Value As String)
                _StageDetail = Value
            End Set
        End Property

        Public Property Configuration() As Integer
            Get
                Return _Configuration
            End Get
            Set(ByVal Value As Integer)
                _Configuration = Value
            End Set
        End Property

        Public Property StagePerID() As Long
            Get
                Return _StagePerID
            End Get
            Set(ByVal Value As Long)
                _StagePerID = Value
            End Set
        End Property

        Public Property ProsType() As Short
            Get
                Return _ProsType
            End Get
            Set(ByVal Value As Short)
                _ProsType = Value
            End Set
        End Property

        Public Property SalesProsName() As String
            Get
                Return _SalesProsName
            End Get
            Set(ByVal Value As String)
                _SalesProsName = Value
            End Set
        End Property

        Public Property SalesProsID() As Long
            Get
                Return _SalesProsID
            End Get
            Set(ByVal Value As Long)
                _SalesProsID = Value
            End Set
        End Property

        Public Property bitDeleted() As Boolean
            Get
                Return _bitDeleted
            End Get
            Set(ByVal Value As Boolean)
                _bitDeleted = Value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property AOIName() As String
            Get
                Return _AOIName
            End Get
            Set(ByVal Value As String)
                _AOIName = Value
            End Set
        End Property

        Public Property AOIID() As Long
            Get
                Return _AOIID
            End Get
            Set(ByVal Value As Long)
                _AOIID = Value
            End Set
        End Property

        'Public Property HomePage() As String
        '    Get
        '        Return _HomePage
        '    End Get
        '    Set(ByVal Value As String)
        '        _HomePage = Value
        '    End Set
        'End Property

        'Public Property HomePageID() As Long
        '    Get
        '        Return _HomePageID
        '    End Get
        '    Set(ByVal Value As Long)
        '        _HomePageID = Value
        '    End Set
        'End Property

        Public Property ContactType() As Long
            Get
                Return _ContactType
            End Get
            Set(ByVal Value As Long)
                _ContactType = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property Relationship() As Long
            Get
                Return _Relationship
            End Get
            Set(ByVal Value As Long)
                _Relationship = Value
            End Set
        End Property
        Private _strStageDetailsIds As String
        Public Property strStageDetailsIds() As String
            Get
                Return _strStageDetailsIds
            End Get
            Set(ByVal Value As String)
                _strStageDetailsIds = Value
            End Set
        End Property

        Private _numStageDetailsId As Long
        Public Property numStageDetailsId() As Long
            Get
                Return _numStageDetailsId
            End Get
            Set(ByVal Value As Long)
                _numStageDetailsId = Value
            End Set
        End Property
        Private _numType As Long
        Public Property numType() As Long
            Get
                Return _numType
            End Get
            Set(ByVal Value As Long)
                _numType = Value
            End Set
        End Property

        Private _ParentStageID As Long
        Public Property ParentStageID() As Long
            Get
                Return _ParentStageID
            End Get
            Set(ByVal value As Long)
                _ParentStageID = value
            End Set
        End Property


        Private _intDueDays As Integer
        Public Property DueDays() As Integer
            Get
                Return _intDueDays
            End Get
            Set(ByVal value As Integer)
                _intDueDays = value
            End Set
        End Property

        Private _dtStartDate As DateTime
        Public Property StartDate() As DateTime
            Get
                Return _dtStartDate
            End Get
            Set(ByVal value As DateTime)
                _dtStartDate = value
            End Set
        End Property

        Private _dtEndDate As DateTime
        Public Property EndDate() As DateTime
            Get
                Return _dtEndDate
            End Get
            Set(ByVal value As DateTime)
                _dtEndDate = value
            End Set
        End Property

        Private _OppID As Long
        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal value As Long)
                _OppID = value
            End Set
        End Property

        Private _TaskValidatorId As Long
        Public Property TaskValidatorId() As Long
            Get
                Return _TaskValidatorId
            End Get
            Set(value As Long)
                _TaskValidatorId = value
            End Set
        End Property

        Private _ProjectID As Long
        Public Property ProjectID() As Long
            Get
                Return _ProjectID
            End Get
            Set(ByVal value As Long)
                _ProjectID = value
            End Set
        End Property
        Private _numDependantOnID As Long
        Public Property numDependantOnID() As Long
            Get
                Return _numDependantOnID
            End Get
            Set(ByVal value As Long)
                _numDependantOnID = value
            End Set
        End Property

        Private _bitTimeBudget As Boolean = False
        Public Property bitTimeBudget() As Boolean
            Get
                Return _bitTimeBudget
            End Get
            Set(ByVal value As Boolean)
                _bitTimeBudget = value
            End Set
        End Property

        Private _bitExpenseBudget As Boolean = False
        Public Property bitExpenseBudget() As Boolean
            Get
                Return _bitExpenseBudget
            End Get
            Set(ByVal value As Boolean)
                _bitExpenseBudget = value
            End Set
        End Property

        Private _monTimeBudget As Decimal = 0
        Public Property monTimeBudget() As Decimal
            Get
                Return _monTimeBudget
            End Get
            Set(ByVal value As Decimal)
                _monTimeBudget = value
            End Set
        End Property

        Private _monExpenseBudget As Decimal = 0
        Public Property monExpenseBudget() As Decimal
            Get
                Return _monExpenseBudget
            End Get
            Set(ByVal value As Decimal)
                _monExpenseBudget = value
            End Set
        End Property



        Private _AutomationID As Long
        Public Property AutomationID() As Long
            Get
                Return _AutomationID
            End Get
            Set(ByVal value As Long)
                _AutomationID = value
            End Set
        End Property


        Private _RuleID As Long
        Public Property RuleID() As Long
            Get
                Return _RuleID
            End Get
            Set(ByVal value As Long)
                _RuleID = value
            End Set
        End Property


        Private _BizDocStatus1 As Long
        Public Property BizDocStatus1() As Long
            Get
                Return _BizDocStatus1
            End Get
            Set(ByVal value As Long)
                _BizDocStatus1 = value
            End Set
        End Property


        Private _BizDocStatus2 As Long
        Public Property BizDocStatus2() As Long
            Get
                Return _BizDocStatus2
            End Get
            Set(ByVal value As Long)
                _BizDocStatus2 = value
            End Set
        End Property

        Private _UpdatedTrackingNo As Long
        Public Property UpdatedTrackingNo() As Long
            Get
                Return _UpdatedTrackingNo
            End Get
            Set(ByVal value As Long)
                _UpdatedTrackingNo = value
            End Set
        End Property

        Private _OppSource As Long
        Public Property OppSource() As Long
            Get
                Return _OppSource
            End Get
            Set(ByVal value As Long)
                _OppSource = value
            End Set
        End Property


        Private _OppType As Long
        Public Property OppType() As Long
            Get
                Return _OppType
            End Get
            Set(ByVal value As Long)
                _OppType = value
            End Set
        End Property


        Private _OrderStatus As Long
        Public Property OrderStatus() As Long
            Get
                Return _OrderStatus
            End Get
            Set(ByVal value As Long)
                _OrderStatus = value
            End Set
        End Property




        Private _EmailTemplate As Long
        Public Property EmailTemplate() As Long
            Get
                Return _EmailTemplate
            End Get
            Set(ByVal value As Long)
                _EmailTemplate = value
            End Set
        End Property


        Private _BizDocCreatedFrom As Long
        Public Property BizDocCreatedFrom() As Long
            Get
                Return _BizDocCreatedFrom
            End Get
            Set(ByVal value As Long)
                _BizDocCreatedFrom = value
            End Set
        End Property


        Private _OpenRecievePayment As Boolean
        Public Property OpenRecievePayment() As Boolean
            Get
                Return _OpenRecievePayment
            End Get
            Set(ByVal value As Boolean)
                _OpenRecievePayment = value
            End Set
        End Property


        Private _CreditCardOption As Integer
        Public Property CreditCardOption() As Integer
            Get
                Return _CreditCardOption
            End Get
            Set(ByVal value As Integer)
                _CreditCardOption = value
            End Set
        End Property


        Private _Created As DateTime
        Public Property Created() As DateTime
            Get
                Return _Created
            End Get
            Set(ByVal value As DateTime)
                _Created = value
            End Set
        End Property


        Private _Modified As DateTime
        Public Property Modified() As DateTime
            Get
                Return _Modified
            End Get
            Set(ByVal value As DateTime)
                _Modified = value
            End Set
        End Property


        Private _IsActive As Boolean
        Public Property IsActive() As Boolean
            Get
                Return _IsActive
            End Get
            Set(ByVal value As Boolean)
                _IsActive = value
            End Set
        End Property

        Private _BizDocTypeId As Long
        Public Property BizDocTypeId() As Long
            Get
                Return _BizDocTypeId
            End Get
            Set(ByVal value As Long)
                _BizDocTypeId = value
            End Set
        End Property

        Private _numSetPrevStageProgress As Long = 0
        Public Property numSetPrevStageProgress() As Long
            Get
                Return _numSetPrevStageProgress
            End Get
            Set(ByVal Value As Long)
                _numSetPrevStageProgress = Value
            End Set
        End Property

        Private _bitAssigntoTeams As Boolean = False
        Public Property bitAssigntoTeams() As Boolean
            Get
                Return _bitAssigntoTeams
            End Get
            Set(ByVal Value As Boolean)
                _bitAssigntoTeams = Value
            End Set
        End Property

        Private _bitAutomaticStartTimer As Boolean = False
        Public Property bitAutomaticStartTimer() As Boolean
            Get
                Return _bitAutomaticStartTimer
            End Get
            Set(ByVal Value As Boolean)
                _bitAutomaticStartTimer = Value
            End Set
        End Property


        Private _bitIsDueDaysUsed As Boolean = False
        Public Property bitIsDueDaysUsed() As Boolean
            Get
                Return _bitIsDueDaysUsed
            End Get
            Set(ByVal Value As Boolean)
                _bitIsDueDaysUsed = Value
            End Set
        End Property

        Private _bitRunningDynamicMode As Boolean = False
        Public Property bitRunningDynamicMode() As Boolean
            Get
                Return _bitRunningDynamicMode
            End Get
            Set(ByVal Value As Boolean)
                _bitRunningDynamicMode = Value
            End Set
        End Property

        Private _numStageOrder As Long = 0
        Public Property numStageOrder() As Long
            Get
                Return _numStageOrder
            End Get
            Set(ByVal Value As Long)
                _numStageOrder = Value
            End Set
        End Property

        Private _strTaskName As String
        Public Property strTaskName() As String
            Get
                Return _strTaskName
            End Get
            Set(ByVal Value As String)
                _strTaskName = Value
            End Set
        End Property

        Private _bitAlphabetical As Boolean
        Public Property bitAlphabetical() As Boolean
            Get
                Return _bitAlphabetical
            End Get
            Set(ByVal Value As Boolean)
                _bitAlphabetical = Value
            End Set
        End Property


        Private _numHours As Long = 0
        Public Property numHours() As Long
            Get
                Return _numHours
            End Get
            Set(ByVal Value As Long)
                _numHours = Value
            End Set
        End Property

        Private _numMinutes As Long = 0
        Public Property numMinutes() As Long
            Get
                Return _numMinutes
            End Get
            Set(ByVal Value As Long)
                _numMinutes = Value
            End Set
        End Property

        Private _numTeamId As Long = 0
        Public Property numTeamId() As Long
            Get
                Return _numTeamId
            End Get
            Set(ByVal Value As Long)
                _numTeamId = Value
            End Set
        End Property

        Private _numTaskId As Long = 0
        Public Property numTaskId() As Long
            Get
                Return _numTaskId
            End Get
            Set(ByVal Value As Long)
                _numTaskId = Value
            End Set
        End Property

        Private _ParentTaskID As Long = 0
        Public Property ParentTaskID() As Long
            Get
                Return _ParentTaskID
            End Get
            Set(value As Long)
                _ParentTaskID = value
            End Set
        End Property

        Public _IsTaskClosed As Boolean = False
        Public Property IsTaskClosed() As Boolean
            Get
                Return _IsTaskClosed
            End Get
            Set(value As Boolean)
                _IsTaskClosed = value
            End Set
        End Property

        Public _IsTaskSaved As Boolean = False
        Public Property IsTaskSaved() As Boolean
            Get
                Return _IsTaskSaved
            End Get
            Set(value As Boolean)
                _IsTaskSaved = value
            End Set
        End Property

        Public _IsAutoClosedTaskConfirmed As Boolean = False
        Public Property IsAutoClosedTaskConfirmed() As Boolean
            Get
                Return _IsAutoClosedTaskConfirmed
            End Get
            Set(value As Boolean)
                _IsAutoClosedTaskConfirmed = value
            End Set
        End Property

        Public _intTaskType As Int16 = 0

        Public Property intTaskType() As Int16
            Get
                Return _intTaskType
            End Get
            Set(value As Int16)
                _intTaskType = value
            End Set
        End Property

        Public Property WorkOrderID As Long
        Public Property BuildManager As Long
        'Public Function ManageHomePage() As Boolean
        '    Try

        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numRelationship", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = _Relationship

        '        arParms(1) = New Npgsql.NpgsqlParameter("@numContactType", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(1).Value = _ContactType

        '        arParms(2) = New Npgsql.NpgsqlParameter("@vcHomePage", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
        '        arParms(2).Value = _HomePage

        '        arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(3).Value = DomainId

        '        arParms(4) = New Npgsql.NpgsqlParameter("@numHomePageID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(4).Value = _HomePageID

        '        SqlDAL.ExecuteNonQuery(connString, "USP_ManageHomePage", arParms)

        '        Return True

        '    Catch ex As Exception
        '        ' Log exception details
        '        Throw ex
        '    End Try
        'End Function



        'Public Function GetHomePage() As DataTable
        '    Try
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = DomainId

        '        Return SqlDAL.ExecuteDataset(connString, "USP_GetHomePage", arParms).Tables(0)

        '    Catch ex As Exception
        '        ' Log exception details
        '        Throw ex
        '    End Try
        'End Function

        Public Function ManageAOIs() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAOIId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _AOIID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcAOIName", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _AOIName

                arParms(3) = New Npgsql.NpgsqlParameter("@bitDeleted", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _bitDeleted

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAOIs", arParms)

                Return True

            Catch ex As Exception
                Return False
            End Try
        End Function


        Public Function GetAOIs() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetAOIs", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageSalesProcessList() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesProsID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesProsID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcSlpName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _SalesProsName

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintProType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _ProsType

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@numStageNumber", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _Configuration

                arParms(6) = New Npgsql.NpgsqlParameter("@bitAssigntoTeams", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = bitAssigntoTeams

                arParms(7) = New Npgsql.NpgsqlParameter("@bitAutomaticStartTimer", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = bitAutomaticStartTimer

                arParms(8) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = OppID

                arParms(9) = New Npgsql.NpgsqlParameter("@numTaskValidatorId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = TaskValidatorId

                arParms(10) = New Npgsql.NpgsqlParameter("@numBuildManager", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = BuildManager

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageSalesprocessList", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function UpdateTaskOrder() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcSortedTask", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(1).Value = strTaskName

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_UpdateTaskOrder", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function InsertStageDetails() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(15) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numStagePercentageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _StagePerID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintConfiguration", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _Configuration

                arParms(2) = New Npgsql.NpgsqlParameter("@vcStageDetail", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _StageDetail

                arParms(3) = New Npgsql.NpgsqlParameter("@slpid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _SalesProsID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@numAssignTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _AssignTo

                arParms(7) = New Npgsql.NpgsqlParameter("@vcMileStoneName", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(7).Value = _MileStoneName

                arParms(8) = New Npgsql.NpgsqlParameter("@Percentage", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _Percentage

                arParms(9) = New Npgsql.NpgsqlParameter("@vcDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(9).Value = _Description

                arParms(10) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(10).Value = _dtStartDate

                arParms(11) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(11).Value = _dtEndDate

                arParms(12) = New Npgsql.NpgsqlParameter("@numParentStageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _ParentStageID

                arParms(13) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _ProjectID

                arParms(14) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _OppID

                arParms(15) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(15).Value = Nothing
                arParms(15).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "usp_InsertStagePercentageDetails", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function Update_StagePercentageDetails() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strStageDetail", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _StageDetail

                SqlDAL.ExecuteNonQuery(connString, "usp_Update_StagePercentageDetails", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DelStagePerDTLs() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@slpid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesProsID

                SqlDAL.ExecuteNonQuery(connString, "usp_DelStagePercentageDetails", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DelBusinessPros() As Integer
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@slpid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesProsID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "usp_DelBusinessProcess", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetDefaultTabName() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@v_numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "usp_getDefaultTabName", sqlParams.ToArray()).Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function AddLoggedInDetails() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_LoggedinDetails", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function LoggedOut() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numAccessID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _AccessID

                SqlDAL.ExecuteNonQuery(connString, "USP_LoggedOutDetails", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ManageDefaultTabName() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@vcLead", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _strLead

                arParms(3) = New Npgsql.NpgsqlParameter("@vcContact", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _strContact

                arParms(4) = New Npgsql.NpgsqlParameter("@vcProspect", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _strProspect

                arParms(5) = New Npgsql.NpgsqlParameter("@vcAccount", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _strAccount

                arParms(6) = New Npgsql.NpgsqlParameter("@vcLeadPlural", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _strLeadPlural

                arParms(7) = New Npgsql.NpgsqlParameter("@vcContactPlural", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _strContactPlural

                arParms(8) = New Npgsql.NpgsqlParameter("@vcProspectPlural", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _strProspectPlural

                arParms(9) = New Npgsql.NpgsqlParameter("@vcAccountPlural", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(9).Value = _strAccountPlural

                SqlDAL.ExecuteNonQuery(connString, "usp_ManageDefaultTabName", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function LoadProcessList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_BusinessProcessList", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetBusinessProcessListConfiguration() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@numSlpId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _SalesProsID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetBusinessProcessListConfiguration", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function StageItemDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@slpid", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _SalesProsID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = OppID

                arParms(4) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = ProjectID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetStageItemDetails", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetStageItemGradeDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numStageDetailsGradeId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numStageDetailsGradeId

                arParms(2) = New Npgsql.NpgsqlParameter("@numStageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numStageDetailsId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcAssignedId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = vcAssignedIds

                arParms(4) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = CreatedBy

                arParms(5) = New Npgsql.NpgsqlParameter("@numTaskId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = numTaskId

                arParms(6) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _Mode

                arParms(7) = New Npgsql.NpgsqlParameter("@vcGradeId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = GradeIds

                arParms(8) = New Npgsql.NpgsqlParameter("@status", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = Status
                arParms(8).Direction = ParameterDirection.Output

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Dim dt As DataTable
                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageStageGradeDetails", arParms)
                If ds.Tables.Count > 0 Then
                    dt = ds.Tables(0)
                End If
                Return dt

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageStageItemGradeDetails() As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numStageDetailsGradeId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numStageDetailsGradeId

                arParms(2) = New Npgsql.NpgsqlParameter("@numStageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numStageDetailsId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcAssignedId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = vcAssignedIds

                arParms(4) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = CreatedBy

                arParms(5) = New Npgsql.NpgsqlParameter("@numTaskId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = numTaskId

                arParms(6) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _Mode

                arParms(7) = New Npgsql.NpgsqlParameter("@vcGradeId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = GradeIds

                arParms(8) = New Npgsql.NpgsqlParameter("@status", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = Status
                arParms(8).Direction = ParameterDirection.Output

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageStageGradeDetails", objParam, True)
                Status = Convert.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                Return Status
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub DeleteSubStage()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strStageDetailsIds", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(1).Value = _strStageDetailsIds

                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteStage", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function UpdateStageDetails() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(24) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numStagePercentageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _StagePerID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintConfiguration", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _Configuration

                arParms(2) = New Npgsql.NpgsqlParameter("@vcStageDetail", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _StageDetail

                arParms(3) = New Npgsql.NpgsqlParameter("@slpid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _SalesProsID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@numAssignTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _AssignTo

                arParms(7) = New Npgsql.NpgsqlParameter("@vcMileStoneName", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(7).Value = _MileStoneName

                arParms(8) = New Npgsql.NpgsqlParameter("@Percentage", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _Percentage

                arParms(9) = New Npgsql.NpgsqlParameter("@vcDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(9).Value = _Description

                arParms(10) = New Npgsql.NpgsqlParameter("@numStageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _numStageDetailsId

                arParms(11) = New Npgsql.NpgsqlParameter("@bitClose", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = bitClose

                arParms(12) = New Npgsql.NpgsqlParameter("@numParentStageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _ParentStageID

                arParms(13) = New Npgsql.NpgsqlParameter("@intDueDays", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(13).Value = _intDueDays

                arParms(14) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(14).Value = _dtStartDate

                arParms(15) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(15).Value = _dtEndDate

                arParms(16) = New Npgsql.NpgsqlParameter("@bitTimeBudget", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(16).Value = _bitTimeBudget

                arParms(17) = New Npgsql.NpgsqlParameter("@bitExpenseBudget", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(17).Value = _bitExpenseBudget

                arParms(18) = New Npgsql.NpgsqlParameter("@monTimeBudget", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(18).Value = _monTimeBudget

                arParms(19) = New Npgsql.NpgsqlParameter("@monExpenseBudget", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(19).Value = _monExpenseBudget

                arParms(20) = New Npgsql.NpgsqlParameter("@numSetPrevStageProgress", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(20).Value = _numSetPrevStageProgress


                arParms(21) = New Npgsql.NpgsqlParameter("@bitIsDueDaysUsed", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(21).Value = _bitIsDueDaysUsed

                arParms(22) = New Npgsql.NpgsqlParameter("@numTeamId", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(22).Value = _numTeamId

                arParms(23) = New Npgsql.NpgsqlParameter("@bitRunningDynamicMode", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(23).Value = _bitRunningDynamicMode

                arParms(24) = New Npgsql.NpgsqlParameter("@numStageOrder", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(24).Value = numStageOrder

                SqlDAL.ExecuteNonQuery(connString, "usp_UpdateStagePercentageDetails", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetBillingTerms() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@nmDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_BillingTerms", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetStageItemDependency() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@numStageDetailID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _numStageDetailsId

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _Mode

                arParms(4) = New Npgsql.NpgsqlParameter("@tintModeType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _ModeType

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetStageItemDependency", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageStageItemDependency() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numStageDetailID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numStageDetailsId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDependantOnID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numDependantOnID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                SqlDAL.ExecuteNonQuery(connString, "usp_ManageStageItemDependency", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetGanttChartData() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _dtStartDate

                arParms(5) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(5).Value = _dtEndDate

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetGanttChart", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub DeleteMilestoneStage()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@numStageDetailsID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _numStageDetailsId

                arParms(3) = New Npgsql.NpgsqlParameter("@tintModeType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _ModeType

                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteMilestoneStage", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub CopyStagePercentageDetails()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@slp_id", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _SalesProsID

                arParms(3) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _OppID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "usp_CopyStagePercentageDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub RemoveStagePercentageDetails()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintModeType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ModeType

                SqlDAL.ExecuteNonQuery(connString, "usp_RemoveStagePercentageDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function ManageStageAccessDetail() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppID

                arParms(2) = New Npgsql.NpgsqlParameter("@numStageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _numStageDetailsId

                arParms(3) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _Mode

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "usp_ManageStageAccessDetail", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetStageAccessDetail() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppID

                arParms(2) = New Npgsql.NpgsqlParameter("@numStageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = 0

                arParms(3) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _Mode

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_ManageStageAccessDetail", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageNameTemplate(ByVal ModuleID As Short) As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}                          'create a param array
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strItems

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@tintModuleID", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = ModuleID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageNameTemplate", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function AssignProjectStageDate() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppID

                SqlDAL.ExecuteNonQuery(connString, "usp_AssignProjectStageDate", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetClass() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}                          'create a param array
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetClass", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageClass() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _strItems

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageClass", arParms)

                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Shared Sub sb_BindClassCombo(ByRef ddlClass As DropDownList, ByVal lngDomainID As Long)
            Try
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = lngDomainID
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()
                ddlClass.DataTextField = "ClassName"
                ddlClass.DataValueField = "numChildClassID"
                ddlClass.DataSource = dtClass
                ddlClass.DataBind()
                ddlClass.Items.Insert(0, "--Select One--")
                ddlClass.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function ManageOpportunityAutomationRules() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}                          'create a param array
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strItems

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _OppType

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageOpportunityAutomationRules", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageWorkflowAutomation() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RuleID

                arParms(2) = New Npgsql.NpgsqlParameter("@numBizDocTypeId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _BizDocTypeId

                arParms(3) = New Npgsql.NpgsqlParameter("@numBizDocStatus1", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _BizDocStatus1

                arParms(4) = New Npgsql.NpgsqlParameter("@numBizDocStatus2", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _BizDocStatus2

                arParms(5) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _OppType

                arParms(6) = New Npgsql.NpgsqlParameter("@numOrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _OrderStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = DomainID

                arParms(8) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = UserCntID

                arParms(9) = New Npgsql.NpgsqlParameter("@numEmailTemplate", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _EmailTemplate

                arParms(10) = New Npgsql.NpgsqlParameter("@numBizDocCreatedFrom", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _BizDocCreatedFrom

                arParms(11) = New Npgsql.NpgsqlParameter("@numOpenRecievePayment", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _OpenRecievePayment

                arParms(12) = New Npgsql.NpgsqlParameter("@numCreditCardOption", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _CreditCardOption

                arParms(13) = New Npgsql.NpgsqlParameter("@numBizDocTemplate", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _BizDocTemplate

                arParms(14) = New Npgsql.NpgsqlParameter("@numOppSource", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _OppSource

                arParms(15) = New Npgsql.NpgsqlParameter("@bitUpdatedTrackingNo", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = _UpdatedTrackingNo

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageWorkflowAutomationRules", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private _BizDocTemplate As Long
        Public Property BizDocTemplate() As Long
            Get
                Return _BizDocTemplate
            End Get
            Set(ByVal value As Long)
                _BizDocTemplate = value
            End Set
        End Property

        Public Function GetWorkflowAutomationRules() As DataTable
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}                          'create a param array
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@numAutomationID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _AutomationID

                arParms(3) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _RuleID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetWorkflowAutomationRules", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub SaveUnitPriceApprover(ByVal vcApprovers As String)
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcUnitPriceApprovers", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = vcApprovers

                SqlDAL.ExecuteNonQuery(connString, "USP_UnitPriceApprover_Save", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetNUpdateStageProgressPercentage(ByVal numRecID As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numStageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numStageDetailsId

                arParms(2) = New Npgsql.NpgsqlParameter("@numRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numRecID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _Mode

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetNUpdateStageProgressPercentage", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub AddStageTask()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(15) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numStageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numStageDetailsId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcTaskName", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(2).Value = _strTaskName

                arParms(3) = New Npgsql.NpgsqlParameter("@numHours", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _numHours

                arParms(4) = New Npgsql.NpgsqlParameter("@numMinutes", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _numMinutes

                arParms(5) = New Npgsql.NpgsqlParameter("@numAssignTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = Assignto

                arParms(6) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = OppID

                arParms(8) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = ProjectID

                arParms(9) = New Npgsql.NpgsqlParameter("@numParentTaskId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = ParentTaskID

                arParms(10) = New Npgsql.NpgsqlParameter("@bitTaskClosed", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = IsTaskClosed

                arParms(11) = New Npgsql.NpgsqlParameter("@numTaskId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = numTaskId

                arParms(12) = New Npgsql.NpgsqlParameter("@bitSavedTask", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(12).Value = IsTaskSaved

                arParms(13) = New Npgsql.NpgsqlParameter("@bitAutoClosedTaskConfirmed", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(13).Value = IsAutoClosedTaskConfirmed

                arParms(14) = New Npgsql.NpgsqlParameter("@intTaskType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = intTaskType

                arParms(15) = New Npgsql.NpgsqlParameter("@numWorkOrderID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = WorkOrderID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageStageTask", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetTaskByStageDetailsId() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numStageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numStageDetailsId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = OppID

                arParms(3) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = ProjectID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcSearchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = strTaskName

                arParms(5) = New Npgsql.NpgsqlParameter("@bitAlphabetical", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = bitAlphabetical

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTaskByStageDetailsId", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub DeleteTask()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numTaskId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numTaskId

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteTask", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ManageProcessForOpportunity()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProcessId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SalesProsID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = OppID

                arParms(4) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = ProjectID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageProcesstoOpportunity", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub UpdateStartDateProcesstoOpportunity()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numStageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numStageDetailsId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OppID

                arParms(2) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ProjectID

                arParms(3) = New Npgsql.NpgsqlParameter("@dtmStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = StartDate

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateStartDateProcesstoOpportunity", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub DeleteProcessFromOpportunity()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = OppID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ProjectID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteProcessFromOpportunity", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetTaskForGanttChart() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OppID

                arParms(2) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ProjectID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTaskDataForGanttChart", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace

