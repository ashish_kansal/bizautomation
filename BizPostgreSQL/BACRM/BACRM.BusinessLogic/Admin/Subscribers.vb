' Created by Anoop Jayaraj
Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Admin


    Public Class Subscribers
        Inherits BACRM.BusinessLogic.CBusinessBase


        'Private DomainId As Long
        Private _SearchWord As String
        Private _SubscriberID As Long
        Private _DivisionID As Long
        Private _NoOfUsers As Integer
        Private _NoOfPartners As Integer
        Private _SubStartDate As Date = New Date(1753, 1, 1)
        Private _SubEndDate As Date = New Date(1753, 1, 1)
        Private _Trial As Boolean
        Private _Active As Short
        Private _AdminCntID As Long
        Private _TargetDomainID As Long
        'Private UserCntID As Long
        Private _SuspendedReason As String
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _SortOrder As Short
        Private _strUsers As String
        Private _bitExists As Boolean = False
        Private _TargetGroupId As Long = 0
        Private _ClientTimeZoneOffset As Int32
        Private _EmailStartDate As DateTime
        Private _sourceDomainId As Decimal
        Private _destinationDomainId As Decimal
        Private _sourceGroupId As Decimal
        Private _destinationGroupId As Decimal
        Private _taskToPerform As String
        Private _isAllowToEditHelpFile As Boolean

        Private _VcOutput As String
        Public Property VcOutput() As String
            Get
                Return _VcOutput
            End Get
            Set(ByVal value As String)
                _VcOutput = value
            End Set
        End Property
        Public Property taskToPerform() As String
            Get
                Return _taskToPerform
            End Get
            Set(ByVal value As String)
                _taskToPerform = value
            End Set
        End Property

        Public Property destinationGroupId() As Decimal
            Get
                Return _destinationGroupId
            End Get
            Set(ByVal value As Decimal)
                _destinationGroupId = value
            End Set
        End Property

        Public Property sourceGroupId() As Decimal
            Get
                Return _sourceGroupId
            End Get
            Set(ByVal value As Decimal)
                _sourceGroupId = value
            End Set
        End Property
        Private _destinationSiteId As Decimal
        Public Property destinationSiteId() As Decimal
            Get
                Return _destinationSiteId
            End Get
            Set(ByVal value As Decimal)
                _destinationSiteId = value
            End Set
        End Property

        Private _sourceSiteId As Decimal
        Public Property sourceSiteId() As Decimal
            Get
                Return _sourceSiteId
            End Get
            Set(ByVal value As Decimal)
                _sourceSiteId = value
            End Set
        End Property

        Public Property destinationDomainId() As Decimal
            Get
                Return _destinationDomainId
            End Get
            Set(ByVal value As Decimal)
                _destinationDomainId = value
            End Set
        End Property

        Public Property sourceDomainId() As Decimal
            Get
                Return _sourceDomainId
            End Get
            Set(ByVal value As Decimal)
                _sourceDomainId = value
            End Set
        End Property
        Public Property EmailStartDate() As DateTime
            Get
                Return _EmailStartDate
            End Get
            Set(ByVal value As DateTime)
                _EmailStartDate = value
            End Set
        End Property
        Private _EmailEndDate As DateTime
        Public Property EmailEndDate() As DateTime
            Get
                Return _EmailEndDate
            End Get
            Set(ByVal value As DateTime)
                _EmailEndDate = value
            End Set
        End Property
        Private _NoOfEmails As Integer
        Public Property NoOfEmails() As Integer
            Get
                Return _NoOfEmails
            End Get
            Set(ByVal value As Integer)
                _NoOfEmails = value
            End Set
        End Property


        Private _TotalActiveUsers As Double
        Public Property TotalActiveUsers() As Double
            Get
                Return _TotalActiveUsers
            End Get
            Set(ByVal value As Double)
                _TotalActiveUsers = value
            End Set
        End Property

        Public Property TargetGroupId() As Long
            Get
                Return _TargetGroupId
            End Get
            Set(ByVal Value As Long)
                _TargetGroupId = Value
            End Set
        End Property

        Public Property bitExists() As Boolean
            Get
                Return _bitExists
            End Get
            Set(ByVal Value As Boolean)
                _bitExists = Value
            End Set
        End Property
        Public Property strUsers() As String
            Get
                Return _strUsers
            End Get
            Set(ByVal Value As String)
                _strUsers = Value
            End Set
        End Property


        Public Property SortOrder() As Short
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Short)
                _SortOrder = Value
            End Set
        End Property


        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Property SuspendedReason() As String
            Get
                Return _SuspendedReason
            End Get
            Set(ByVal Value As String)
                _SuspendedReason = Value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property TargetDomainID() As Long
            Get
                Return _TargetDomainID
            End Get
            Set(ByVal Value As Long)
                _TargetDomainID = Value
            End Set
        End Property

        Public Property AdminCntID() As Long
            Get
                Return _AdminCntID
            End Get
            Set(ByVal Value As Long)
                _AdminCntID = Value
            End Set
        End Property

        Public Property Active() As Short
            Get
                Return _Active
            End Get
            Set(ByVal Value As Short)
                _Active = Value
            End Set
        End Property

        Public Property Trial() As Boolean
            Get
                Return _Trial
            End Get
            Set(ByVal Value As Boolean)
                _Trial = Value
            End Set
        End Property

        Public Property SubEndDate() As Date
            Get
                Return _SubEndDate
            End Get
            Set(ByVal Value As Date)
                _SubEndDate = Value
            End Set
        End Property

        Public Property SubStartDate() As Date
            Get
                Return _SubStartDate
            End Get
            Set(ByVal Value As Date)
                _SubStartDate = Value
            End Set
        End Property

        Public Property NoOfPartners() As Integer
            Get
                Return _NoOfPartners
            End Get
            Set(ByVal Value As Integer)
                _NoOfPartners = Value
            End Set
        End Property

        Public Property NoOfUsers() As Integer
            Get
                Return _NoOfUsers
            End Get
            Set(ByVal Value As Integer)
                _NoOfUsers = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property SubscriberID() As Long
            Get
                Return _SubscriberID
            End Get
            Set(ByVal Value As Long)
                _SubscriberID = Value
            End Set
        End Property

        Public Property SearchWord() As String
            Get
                Return _SearchWord
            End Get
            Set(ByVal Value As String)
                _SearchWord = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal value As Int32)
                _ClientTimeZoneOffset = value
            End Set
        End Property

        Private _OrganizationLogin As Short
        Public Property OrganizationLogin() As Short
            Get
                Return _OrganizationLogin
            End Get
            Set(ByVal value As Short)
                _OrganizationLogin = value
            End Set
        End Property

        Private _NoOfPartialUsers As Integer
        Public Property NoOfPartialUsers() As Integer
            Get
                Return _NoOfPartialUsers
            End Get
            Set(ByVal Value As Integer)
                _NoOfPartialUsers = Value
            End Set
        End Property

        Private _NoOfMinimalUsers As Integer
        Public Property NoOfMinimalUsers() As Integer
            Get
                Return _NoOfMinimalUsers
            End Get
            Set(ByVal Value As Integer)
                _NoOfMinimalUsers = Value
            End Set
        End Property

        Private _IsEnabledAccountingAudit As Boolean
        Public Property IsEnabledAccountingAudit() As Boolean
            Get
                Return _IsEnabledAccountingAudit
            End Get
            Set(ByVal value As Boolean)
                _IsEnabledAccountingAudit = value
            End Set
        End Property

        Private _IsEnabledNotifyAdmin As Boolean
        Public Property IsEnabledNotifyAdmin() As Boolean
            Get
                Return _IsEnabledNotifyAdmin
            End Get
            Set(ByVal value As Boolean)
                _IsEnabledNotifyAdmin = value
            End Set
        End Property


        Public Property IsAllowToEditHelpFile() As Boolean
            Get
                Return _isAllowToEditHelpFile
            End Get
            Set(ByVal value As Boolean)
                _isAllowToEditHelpFile = value
            End Set
        End Property

        Public Property NoOfLimitedAccessUsers As Integer
        Public Property NoOfBusinessPortalUsers As Integer
        Public Property FullUserConcurrency As Integer
        Public Property FullUserCost As Double
        Public Property LimitedAccessUserCost As Double
        Public Property BusinessPortalUserCost As Double
        Public Property SiteCost As Double

        Public Property GroupType As Short
        Public Property LastLoginFilter As Short

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop  Jayaraj 	DATE:30-March-05

        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop  Jayaraj 	DATE:30-March-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region


        Public Function GetCompaniesForSubscribing() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@vcSearch", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SearchWord

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCompaniesForSubscribing", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ManageSubscribers() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(25) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _SubscriberID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@intNoofUsersSubscribed", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _NoOfUsers

                arParms(3) = New Npgsql.NpgsqlParameter("@dtSubStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _SubStartDate

                arParms(4) = New Npgsql.NpgsqlParameter("@dtSubEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _SubEndDate

                arParms(5) = New Npgsql.NpgsqlParameter("@bitTrial", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _Trial

                arParms(6) = New Npgsql.NpgsqlParameter("@numAdminContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _AdminCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@bitActive", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _Active

                arParms(8) = New Npgsql.NpgsqlParameter("@vcSuspendedReason", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(8).Value = _SuspendedReason

                arParms(9) = New Npgsql.NpgsqlParameter("@numTargetDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Direction = ParameterDirection.InputOutput
                arParms(9).Value = _TargetDomainID

                arParms(10) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = DomainID

                arParms(11) = New Npgsql.NpgsqlParameter("@numUserContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = UserCntID

                arParms(12) = New Npgsql.NpgsqlParameter("@strUsers", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(12).Value = _strUsers

                arParms(13) = New Npgsql.NpgsqlParameter("@bitExists", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(13).Direction = ParameterDirection.InputOutput
                arParms(13).Value = False

                arParms(14) = New Npgsql.NpgsqlParameter("@TargetGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Direction = ParameterDirection.InputOutput
                arParms(14).Value = 0

                arParms(15) = New Npgsql.NpgsqlParameter("@bitEnabledAccountingAudit", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = _IsEnabledAccountingAudit

                arParms(16) = New Npgsql.NpgsqlParameter("@bitEnabledNotifyAdmin", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(16).Value = _IsEnabledNotifyAdmin

                arParms(17) = New Npgsql.NpgsqlParameter("@intNoofLimitedAccessUsers", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(17).Value = NoOfLimitedAccessUsers

                arParms(18) = New Npgsql.NpgsqlParameter("@intNoofBusinessPortalUsers", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(18).Value = NoOfBusinessPortalUsers

                arParms(19) = New Npgsql.NpgsqlParameter("@monFullUserCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(19).Value = FullUserCost

                arParms(20) = New Npgsql.NpgsqlParameter("@monLimitedAccessUserCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(20).Value = LimitedAccessUserCost

                arParms(21) = New Npgsql.NpgsqlParameter("@monBusinessPortalUserCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(21).Value = BusinessPortalUserCost

                arParms(22) = New Npgsql.NpgsqlParameter("@monSiteCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(22).Value = SiteCost

                arParms(23) = New Npgsql.NpgsqlParameter("@intFullUserConcurrency", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(23).Value = FullUserConcurrency

                arParms(24) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(24).Value = Nothing
                arParms(24).Direction = ParameterDirection.InputOutput

                arParms(25) = New Npgsql.NpgsqlParameter("@bitAllowToEditHelpFile", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(25).Value = _isAllowToEditHelpFile

                Dim objParam() As Object = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageSubscribers", objParam, True)
                _TargetDomainID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(9).Value)
                _SubscriberID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                _bitExists = CBool(DirectCast(objParam, Npgsql.NpgsqlParameter())(13).Value)
                _TargetGroupId = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(14).Value)

                Return CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetSubscriberDetail() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SubscriberID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetSubscriberDetail", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteSubscriber() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SubscriberID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteSubscribers", arParms)
                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ContactsFromDomain() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContactsFromDomain", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetSubscribers() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _SortOrder

                arParms(2) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _SortCharacter

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SearchWord", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = _SearchWord

                arParms(5) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _CurrentPage

                arParms(6) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _PageSize

                arParms(7) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Direction = ParameterDirection.InputOutput
                arParms(7).Value = _TotalRecords

                arParms(8) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _columnName

                arParms(9) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(9).Value = _columnSortOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _ClientTimeZoneOffset

                arParms(11) = New Npgsql.NpgsqlParameter("@ActiveUsers", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(11).Direction = ParameterDirection.InputOutput
                arParms(11).Value = _TotalActiveUsers

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_SubScriberList", objParam, True)

                Dim dtTable As DataTable
                dtTable = ds.Tables(0)
                _TotalRecords = CInt(dtTable.Rows(0).Item(1))
                dtTable.Rows.RemoveAt(0)
                _TotalActiveUsers = Common.CCommon.ToDouble(DirectCast(objParam, Npgsql.NpgsqlParameter())(11).Value)
                Return dtTable
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function CreateNewSubscription() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SubscriberID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_CreateNewSubscription", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetSubscriptionHistory() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SubscriberID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetSubscriptionHistory", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ContactListFromDomain() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintGroupType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = GroupType

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContactsListFromDomain", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function UsageReport() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SearchWord", _SearchWord, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@tintLastLoggedIn", _SortOrder, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcSortColumn", columnName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcSortOrder", columnSortOrder, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUsageReport", sqlParams.ToArray())

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function AccessDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numSubscriberID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SubscriberID

                arParms(1) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ClientTimeZoneOffset

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAccessDetails", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Sub ManageExternalUsersAccess(ByVal vcRecords As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcRecords", vcRecords, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ExtranetAccountsDtl_SetAccess", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetSubscribtionCount() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Subscribers_GetSubscribtionCount", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetCustomerReport() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@bitPayingCustomerOnly", Trial, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SortChar", SortCharacter, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintSortOrder", SortOrder, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcSearchWord", SearchWord, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintLastLoggedIn", LastLoginFilter, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numPageIndex", CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numPageSize", PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcSortColumn", columnName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcSortOrder", columnSortOrder, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_Subscribers_GetCustomerReport", sqlParams.ToArray())

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    TotalRecords = Convert.ToInt32(dt.Rows(0)("numTotalRecords"))
                Else
                    TotalRecords = 0
                End If

                Return dt
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ManageDomainDataMigration() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@sourceDomainId", _sourceDomainId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@destinationDomainId", _destinationDomainId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@sourceGroupId", _sourceGroupId, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@destinationGroupId", _destinationGroupId, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@sourceSiteId", _sourceSiteId, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@destinationSiteId", _destinationSiteId, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@taskToPerform", _taskToPerform, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_MigrationDataFromDomainToDomain", sqlParams.ToArray())
                Return 1
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
    End Class

End Namespace
