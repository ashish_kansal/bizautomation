
''' -----------------------------------------------------------------------------
''' Project	 : BACRM.BusinessLogic
''' Class	 : FormConfigWizard
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This class contains several methods and functions, which is used to 
'''     retirive and manage the form configurations.
''' </summary>
''' <remarks>
'''     This class is used to access the data and manage the same for config of 
'''     forms.
''' </remarks>
''' <history>
''' 	[Debasish Tapan Nag]	07/09/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Option Explicit On
Imports System.Data.SqlClient
Imports System.IO
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Admin

    Public Class FormConfigWizard
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _bitFollowUpStatus As Boolean
        Public Property bitFollowUpStatus() As Long
            Get
                Return _bitFollowUpStatus
            End Get
            Set(value As Long)
                _bitFollowUpStatus = value
            End Set
        End Property


        Private _bitPriority As Boolean
        Public Property bitPriority() As Long
            Get
                Return _bitPriority
            End Get
            Set(value As Long)
                _bitPriority = value
            End Set
        End Property


        Private _bitActivity As Boolean
        Public Property bitActivity() As Long
            Get
                Return _bitActivity
            End Get
            Set(value As Long)
                _bitActivity = value
            End Set
        End Property

        Private _bitCustomField As Boolean
        Public Property bitCustomField() As Long
            Get
                Return _bitCustomField
            End Get
            Set(value As Long)
                _bitCustomField = value
            End Set
        End Property

        Private _bitAttendee As Boolean
        Public Property bitAttendee() As Long
            Get
                Return _bitAttendee
            End Get
            Set(value As Long)
                _bitAttendee = value
            End Set
        End Property

        Private _bitFollowupAnytime As Boolean
        Public Property bitFollowupAnytime() As Long
            Get
                Return _bitFollowupAnytime
            End Get
            Set(value As Long)
                _bitFollowupAnytime = value
            End Set
        End Property

        Private _numSubFormId As Long
        Public Property numSubFormId() As Long
            Get
                Return _numSubFormId
            End Get
            Set(value As Long)
                _numSubFormId = value
            End Set
        End Property

        Private _vcFormName As String
        Public Property vcFormName() As String
            Get
                Return _vcFormName
            End Get
            Set(value As String)
                _vcFormName = value
            End Set
        End Property


        Private _bitByPassRoutingRules As Boolean
        Public Property bitByPassRoutingRules() As Boolean
            Get
                Return _bitByPassRoutingRules
            End Get
            Set(value As Boolean)
                _bitByPassRoutingRules = value
            End Set
        End Property


        Private _numAssignTo As Long
        Public Property numAssignTo() As Long
            Get
                Return _numAssignTo
            End Get
            Set(value As Long)
                _numAssignTo = value
            End Set
        End Property


        Private _bitDripCampaign As Boolean
        Public Property bitDripCampaign() As Boolean
            Get
                Return _bitDripCampaign
            End Get
            Set(value As Boolean)
                _bitDripCampaign = value
            End Set
        End Property

        Private _numDripCampaign As Long
        Public Property numDripCampaign() As Long
            Get
                Return _numDripCampaign
            End Get
            Set(value As Long)
                _numDripCampaign = value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the strAOI's.
        ''' </summary>
        ''' <remarks>
        '''     This holds the AOI's.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _strFormFieldID As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the AOI's.
        ''' </summary>
        ''' <value>Returns the AOI's as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the AOI's. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property strFormFieldID() As String
            Get
                Return _strFormFieldID
            End Get
            Set(ByVal Value As String)
                _strFormFieldID = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the strAOI's.
        ''' </summary>
        ''' <remarks>
        '''     This holds the AOI's.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _BizDocID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the AOI's.
        ''' </summary>
        ''' <value>Returns the AOI's as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the AOI's. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property BizDocID() As Long
            Get
                Return _BizDocID
            End Get
            Set(ByVal Value As Long)
                _BizDocID = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the strAOI's.
        ''' </summary>
        ''' <remarks>
        '''     This holds the AOI's.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _strAOI As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the AOI's.
        ''' </summary>
        ''' <value>Returns the AOI's as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the AOI's. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property strAOI() As String
            Get
                Return _strAOI
            End Get
            Set(ByVal Value As String)
                _strAOI = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the userID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the user id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _UserID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User id.
        ''' </summary>
        ''' <value>Returns the user id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the User id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property UserID() As Long
            Get
                Return _UserID
            End Get
            Set(ByVal Value As Long)
                _UserID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the form id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the form id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _FormID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FormID() As Long
            Get
                Return _FormID
            End Get
            Set(ByVal Value As Long)
                _FormID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Path to tthe XML Config. File.
        ''' </summary>
        ''' <remarks>
        '''     This holds the AuthenticationGroupID.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _XMLFilePath As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets the Authentication Group ID.
        ''' </summary>
        ''' <value>Returns the AuthenticationGroupID as long.</value>
        ''' <remarks>
        '''     This property is used to Get the AuthenticationGroupID. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property XMLFilePath() As String
            Get
                Return _XMLFilePath
            End Get
            Set(ByVal Value As String)
                _XMLFilePath = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the AuthenticationGroupID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the AuthenticationGroupID.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _AuthenticationGroupID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Gets the Authentication Group ID.
        ''' </summary>
        ''' <value>Returns the AuthenticationGroupID as long.</value>
        ''' <remarks>
        '''     This property is used to Get the AuthenticationGroupID. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property AuthenticationGroupID() As Long
            Get
                Return _AuthenticationGroupID
            End Get
            Set(ByVal Value As Long)
                _AuthenticationGroupID = Value
            End Set
        End Property


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the OnlySurvey.
        ''' </summary>
        ''' <remarks>
        '''     This holds the OnlySurvey flag.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _OnlySurvey As Boolean
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the OnlySurvey.
        ''' </summary>
        ''' <value>Returns the OnlySurvey as boolean.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the OnlySurvey flag. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property OnlySurvey() As Boolean
            Get
                Return _OnlySurvey
            End Get
            Set(ByVal Value As Boolean)
                _OnlySurvey = Value
            End Set
        End Property


        Private _vcURL As String
        Public Property vcURL() As String
            Get
                Return _vcURL
            End Get
            Set(ByVal Value As String)
                _vcURL = Value
            End Set
        End Property

        Private _vcSuccessURL As String
        Public Property vcSuccessURL() As String
            Get
                Return _vcSuccessURL
            End Get
            Set(ByVal Value As String)
                _vcSuccessURL = Value
            End Set
        End Property

        Private _vcFailURL As String
        Public Property vcFailURL() As String
            Get
                Return _vcFailURL
            End Get
            Set(ByVal Value As String)
                _vcFailURL = Value
            End Set
        End Property

        Private _URLId As Long
        Public Property URLId() As Long
            Get
                Return _URLId
            End Get
            Set(ByVal Value As Long)
                _URLId = Value
            End Set
        End Property

        Private _tintType As Integer
        Public Property tintType() As Integer
            Get
                Return _tintType
            End Get
            Set(ByVal Value As Integer)
                _tintType = Value
            End Set
        End Property

        Private _FormFieldId As Long
        Public Property FormFieldId() As Long
            Get
                Return _FormFieldId
            End Get
            Set(ByVal Value As Long)
                _FormFieldId = Value
            End Set
        End Property

        Private _vcFormFieldName As String
        Public Property vcFormFieldName() As String
            Get
                Return _vcFormFieldName
            End Get
            Set(ByVal Value As String)
                _vcFormFieldName = Value
            End Set
        End Property

        Private _vcToolTip As String
        Public Property vcToolTip() As String
            Get
                Return _vcToolTip
            End Get
            Set(ByVal Value As String)
                _vcToolTip = Value
            End Set
        End Property

        Private _strLocationIDs As String
        Public Property LocationIds() As String
            Get
                Return _strLocationIDs
            End Get
            Set(ByVal value As String)
                _strLocationIDs = value
            End Set
        End Property

        Private _BizDocTemplateID As Long
        Public Property BizDocTemplateID() As Long
            Get
                Return _BizDocTemplateID
            End Get
            Set(ByVal value As Long)
                _BizDocTemplateID = value
            End Set
        End Property


        Private _boolWorkFlow As Boolean
        Public Property boolWorkFlow() As Boolean
            Get
                Return _boolWorkFlow
            End Get
            Set(ByVal value As Boolean)
                _boolWorkFlow = value
            End Set
        End Property

        Private _ModuleID As Long
        Public Property ModuleID() As Long
            Get
                Return _ModuleID
            End Get
            Set(ByVal Value As Long)
                _ModuleID = Value
            End Set
        End Property

        Private _CultureID As Long
        Public Property CultureID() As Long
            Get
                Return _CultureID
            End Get
            Set(ByVal Value As Long)
                _CultureID = Value
            End Set
        End Property

        Private _boolAllowGridColor As Boolean
        Public Property boolAllowGridColor() As Boolean
            Get
                Return _boolAllowGridColor
            End Get
            Set(ByVal value As Boolean)
                _boolAllowGridColor = value
            End Set
        End Property

        Private _FieldColorSchemeID As Long
        Public Property FieldColorSchemeID() As Long
            Get
                Return _FieldColorSchemeID
            End Get
            Set(ByVal value As Long)
                _FieldColorSchemeID = value
            End Set
        End Property

        Private _strFieldValue As String
        Public Property strFieldValue() As String
            Get
                Return _strFieldValue
            End Get
            Set(ByVal value As String)
                _strFieldValue = value
            End Set
        End Property

        Private _strFieldValue1 As String
        Public Property strFieldValue1() As String
            Get
                Return _strFieldValue1
            End Get
            Set(ByVal value As String)
                _strFieldValue1 = value
            End Set
        End Property

        Private _strColorScheme As String
        Public Property strColorScheme() As String
            Get
                Return _strColorScheme
            End Get
            Set(ByVal value As String)
                _strColorScheme = value
            End Set
        End Property

        Private _numBizFormModuleID As Integer
        Public Property numBizFormModuleID() As Integer
            Get
                Return _numBizFormModuleID
            End Get
            Set(ByVal Value As Integer)
                _numBizFormModuleID = Value
            End Set
        End Property

        Private _Title As String
        Public Property Title() As String
            Get
                Return _Title
            End Get
            Set(ByVal Value As String)
                _Title = Value
            End Set
        End Property

        Private _Description As String
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal Value As String)
                _Description = Value
            End Set
        End Property


        Private _Location As String
        Public Property Location() As String
            Get
                Return _Location
            End Get
            Set(ByVal Value As String)
                _Location = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for AOI from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of AOI's.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getAOIList() As DataSet
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = AuthenticationGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetDynamicFormAOI", arParms) 'execute and store to dataset

                Return ds                                                    'return datatable

            Catch ex As Exception
                Throw ex
            End Try

        End Function



        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for Activity Configuration from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of Activity Configuration's.
        ''' </remarks>
        ''' -----------------------------------------------------------------------------
        Public Function getActivityConfiguration() As DataSet
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = AuthenticationGroupID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserContId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_Get_ActivityFormConfiguration", arParms) 'execute and store to dataset

                Return ds                                                    'return datatable

            Catch ex As Exception
                Throw ex
            End Try

        End Function


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for Activity Configuration from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of Activity Configuration's.
        ''' </remarks>
        ''' -----------------------------------------------------------------------------
        Public Function setActivityConfiguration() As String
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = AuthenticationGroupID

                arParms(2) = New Npgsql.NpgsqlParameter("@bitFollowupStatus", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = bitFollowUpStatus

                arParms(3) = New Npgsql.NpgsqlParameter("@bitPriority ", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = bitPriority

                arParms(4) = New Npgsql.NpgsqlParameter("@bitActivity", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = bitActivity

                arParms(5) = New Npgsql.NpgsqlParameter("@bitCustomField", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = bitCustomField

                arParms(6) = New Npgsql.NpgsqlParameter("@bitFollowupAnytime", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = bitFollowupAnytime

                arParms(7) = New Npgsql.NpgsqlParameter("@bitAttendee", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = bitAttendee

                arParms(8) = New Npgsql.NpgsqlParameter("@bitTitle", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = Title

                arParms(9) = New Npgsql.NpgsqlParameter("@bitDescription", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = Description

                arParms(10) = New Npgsql.NpgsqlParameter("@bitLocation", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = Location

                arParms(11) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(11).Value = UserCntID

                Return SqlDAL.ExecuteScalar(connString, "USP_ActivityFormConfiguration_Save", arParms) 'execute and store to dataset

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the available forms from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the list
        '''     of forms.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getFormList() As DataTable
            Try
                Dim ds As DataSet                                                           'declare a dataset
                Dim getconnection As New GetConnection                                      'declare a connection
                Dim connString As String = getconnection.GetConnectionString                'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintFlag", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _tintType

                arParms(2) = New Npgsql.NpgsqlParameter("@bitWorkFlow", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _boolWorkFlow

                arParms(3) = New Npgsql.NpgsqlParameter("@bitAllowGridColor", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _boolAllowGridColor

                arParms(4) = New Npgsql.NpgsqlParameter("@numBizFormModuleID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _numBizFormModuleID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getDynamicFormList", arParms)   'execute and store to dataset

                Return ds.Tables(0)                                                         'return dtatable

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for all fields from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataSet.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of AOI's.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getFieldList(ByVal cCustomFieldsAssociated As String) As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@cCustomFieldsAssociated", NpgsqlTypes.NpgsqlDbType.VarChar, 9)
                arParms(1).Value = cCustomFieldsAssociated

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(2).Value = FormID

                arParms(3) = New Npgsql.NpgsqlParameter("@numSubFormId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(3).Value = numSubFormId

                arParms(4) = New Npgsql.NpgsqlParameter("@numAuthGroupId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(4).Value = AuthenticationGroupID

                arParms(5) = New Npgsql.NpgsqlParameter("@numBizDocTemplateID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(5).Value = _BizDocTemplateID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getDynamicFormFields", arParms)     'execute and store to dataset
                ds.Tables(0).TableName = "FieldList"                                            'set the name of the table
                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetLeadSubFormList() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_LeadeadSubformList", arParms)     'execute and store to dataset

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetLeadSubFormListDetails() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSubFormId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = numSubFormId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_LeadeadSubformDetails", arParms)     'execute and store to dataset

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SetLeadSubFormList() As String
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSubFormId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = numSubFormId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcFormName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = vcFormName

                arParms(3) = New Npgsql.NpgsqlParameter("@bitByPassRoutingRules", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = bitByPassRoutingRules

                arParms(4) = New Npgsql.NpgsqlParameter("@numAssignTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = numAssignTo

                arParms(5) = New Npgsql.NpgsqlParameter("@bitDripCampaign", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = bitDripCampaign

                arParms(6) = New Npgsql.NpgsqlParameter("@numDripCampaign", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = numDripCampaign

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteScalar(connString, "USP_ManageLeadSubform", arParms)     'execute and store to dataset

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFieldFormListForBizDocsSumm() As DataSet
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@BizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _BizDocID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _FormID

                arParms(3) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _BizDocTemplateID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetFieldFormListForBizDocsSumm", arParms)     'execute and store to dataset

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the field names from the server file
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getFieldNameList(ByVal sXMLFilePath As String) As DataTable
            Dim dsXMLConfig As New DataSet                                                      'Create a Dataset object
            Dim strFileName, strFileNameAllGroups As String                                     'Declare a string object
            strFileName = sXMLFilePath & "\" & "DynamicFormConfig_" & DomainID & "_" & FormID & "_" & AuthenticationGroupID & ".xml"          'Set the file name
            strFileNameAllGroups = sXMLFilePath & "\" & "DynamicFormConfig_" & DomainID & "_" & FormID & "_0.xml"                    'Set the file name for all groups
            Try
                If File.Exists(strFileName) = False And File.Exists(strFileNameAllGroups) = False Then 'If file not exists exit
                    Dim dtTable As New DataTable                                                    'create a new datatable
                    dtTable.Columns.Add(New DataColumn("numFormFieldId", System.Type.GetType("System.String")))      'Add a column
                    dtTable.Columns.Add(New DataColumn("vcNewFormFieldName", System.Type.GetType("System.String"))) 'Add another column
                    dtTable.Columns.Add(New DataColumn("numAuthGroupId", System.Type.GetType("System.String"))) 'Add another column
                    dsXMLConfig.Tables.Add(dtTable)                                                 'Add table to the dataset
                ElseIf File.Exists(strFileName) = True Then
                    dsXMLConfig.ReadXml(strFileName)                                                'Read into XML
                    dsXMLConfig.Tables(0).Columns.Add(New DataColumn("numAuthGroupId", System.Type.GetType("System.String"))) 'Add another column
                    dsXMLConfig.Tables(0).Columns("numAuthGroupId").Expression = CStr(AuthenticationGroupID) 'Set the default value as auth group id
                ElseIf File.Exists(strFileNameAllGroups) = True Then
                    dsXMLConfig.ReadXml(strFileNameAllGroups)                                       'Read into XML
                    dsXMLConfig.Tables(0).Columns.Add(New DataColumn("numAuthGroupId", System.Type.GetType("System.String"))) 'Add another column
                    dsXMLConfig.Tables(0).Columns("numAuthGroupId").Expression = "0"                'Set the default value as auth group id
                End If
            Catch
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(1).Value = FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@numAuthGroupId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(2).Value = AuthenticationGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@numBizDocTemplateID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(3).Value = 0

                arParms(4) = New Npgsql.NpgsqlParameter("@numSubFormId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(4).Value = numSubFormId

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                dsXMLConfig = SqlDAL.ExecuteDataset(connString, "usp_getFormConfigFields", arParms)     'execute and store to dataset

                If File.Exists(strFileName) = True Then
                    File.Delete(strFileName)                                                    'XML file has a problem so delete it
                ElseIf File.Exists(strFileNameAllGroups) = True Then
                    File.Delete(strFileNameAllGroups)                                           'XML file has a problem so delete it
                End If
            End Try
            dsXMLConfig.Tables(0).TableName = "FormFieldXML"                                    'Set the name of the table
            Return dsXMLConfig.Tables(0)                                                        'return xml data table
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the field names from the server file
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getMandatoryFieldNameList(ByVal sXMLFilePath As String) As DataTable
            Dim dsXMLConfig As New DataSet                                                      'Create a Dataset object
            Dim strFileName As String                                                           'Declare a string object
            strFileName = sXMLFilePath & "\" & "DynamicFormConfig_Mandatory.xml"                'Set the file name

            If File.Exists(strFileName) = False Then                                            'If file not exists exit
                Dim dtTable As New DataTable                                                    'create a new datatable
                dtTable.Columns.Add(New DataColumn("numFormFieldId", System.Type.GetType("System.String")))      'Add a column
                dtTable.Columns.Add(New DataColumn("vcFormFieldName", System.Type.GetType("System.String"))) 'Add another column
                dsXMLConfig.Tables.Add(dtTable)                                                 'Add table to the dataset
            Else
                dsXMLConfig.ReadXml(strFileName)                                                'Read into XML
            End If
            dsXMLConfig.Tables(0).TableName = "FormFieldMandatory"                              'Set the name of the table
            Return dsXMLConfig.Tables(0)                                                        'return xml data table
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the xml into the server file
        ''' </summary>
        ''' <param name="sXMLString">Represents the xml string.</param>
        ''' <param name="sXMLFilePath">Represents the path to the file.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveXMLConfig(ByVal sXMLString As String, ByVal sXMLFilePath As String) As Boolean
            Dim StrWriter As StreamWriter                                       'Declare a StreamWriter object
            Dim strFileName As String                                           'Declare a string object
            If FormID = 3 AndAlso numSubFormId > 0 Then
                strFileName = "DynamicFormConfig_" & DomainID & "_" & FormID & "_" & AuthenticationGroupID & "_" & numSubFormId & ".xml" 'Set the file name by Sub Form
            Else
                strFileName = "DynamicFormConfig_" & DomainID & "_" & FormID & "_" & AuthenticationGroupID & ".xml" 'Set the file name
            End If

            Try
                If Directory.Exists(sXMLFilePath) = False Then                  'If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(sXMLFilePath)
                End If
                'Remove read only attribute
                If File.Exists(sXMLFilePath & "\" & strFileName) Then
                    File.SetAttributes(sXMLFilePath & "\" & strFileName, FileAttributes.Normal)
                End If

                StrWriter = File.CreateText(sXMLFilePath & "\" & strFileName)   'create the flle
                StrWriter.Write(sXMLString)                                     'Write XML doument
                StrWriter.Close()                                               'close the object
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function




        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to match two tables and establish relationship
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function establishRelation(ByVal tbFirstTable As DataTable, ByVal tbSecondTable As DataTable, ByVal tbThirdTable As DataTable) As DataTable
            tbFirstTable.Columns.Add("vcNewFormFieldName", System.Type.GetType("System.String"))                           'add a new column 
            tbFirstTable.Columns("vcNewFormFieldName").ReadOnly = False                                                    'Make the column value writable
            tbFirstTable.Columns.Add("bitDefaultMandatory", System.Type.GetType("System.String"))                          'add a new column 
            tbFirstTable.Columns("bitDefaultMandatory").ReadOnly = False                                                   'Make the column value writable
            Dim dsCombinedDataset As DataSet = tbFirstTable.DataSet
            Try
                tbSecondTable.DataSet.Merge(tbThirdTable.DataSet, True)
                dsCombinedDataset.Merge(tbSecondTable.DataSet, True)
                Dim objParentCol As DataColumn = dsCombinedDataset.Tables(0).Columns("numFormFieldID")                    'Create an object of the first column
                Dim objChildCol As DataColumn = dsCombinedDataset.Tables(1).Columns("numFormFieldID")                     'Create an object of the second column
                Dim objChildColTwo As DataColumn = dsCombinedDataset.Tables(2).Columns("numFormFieldID")                  'Create an object of the second column
                Dim objFieldIdRelation As DataRelation                                                                    'create relation object
                objFieldIdRelation = New DataRelation("FieldRelation", objParentCol, objChildCol, False)                  'create parent child relation 
                dsCombinedDataset.Relations.Add(objFieldIdRelation)                                                       'Add the relation to the dataset
                Dim objrow, objChildRow As DataRow
                For Each objrow In dsCombinedDataset.Tables(0).Rows                                                       'loop through the rows in parent table
                    'Dim colChildRows() As DataRow = objrow.GetChildRows(objFieldIdRelation)                               'Create an array of child arrays
                    'If colChildRows.Length > 0 Then
                    '    For Each objChildRow In colChildRows
                    '        If objChildRow("vcDbColumnName") = objrow("vcDbColumnName") Then
                    '            If colChildRows(0)("boolRequired") = 1 And objrow.Item("vcFieldType") = colChildRows(0)("vcFieldType") Then
                    '                objrow.Item("vcNewFormFieldName") = "(*) " & colChildRows(0)("vcNewFormFieldName")   'add new name to the column item
                    '                Exit For
                    '            ElseIf colChildRows(0)("boolRequired") = 0 And objrow.Item("vcFieldType") = colChildRows(0)("vcFieldType") Then
                    '                objrow.Item("vcNewFormFieldName") = colChildRows(0)("vcNewFormFieldName")            'add new name to the column item
                    '                Exit For
                    '            End If
                    '        Else
                    '            objrow.Item("vcNewFormFieldName") = objrow.Item("vcFormFieldName")                      'add new name to the column item
                    '        End If
                    '    Next
                    'Else
                    '    objrow.Item("vcNewFormFieldName") = objrow.Item("vcFormFieldName")                              'add new name to the column item
                    'End If

                    If objrow.Item("boolRequired") = 1 Then
                        objrow.Item("vcNewFormFieldName") = "(*) " & objrow.Item("vcFormFieldName")  'add new name to the column item
                    Else
                        objrow.Item("vcNewFormFieldName") = objrow.Item("vcFormFieldName")       'add new name to the column item
                    End If

                Next
                Dim objFieldIdMandatoryRelation As DataRelation                                                           'create relation object
                objFieldIdMandatoryRelation = New DataRelation("FieldMandatoryRelation", objParentCol, objChildColTwo, False) 'create parent child relation 
                dsCombinedDataset.Relations.Add(objFieldIdMandatoryRelation)
                For Each objrow In dsCombinedDataset.Tables(0).Rows                                                       'loop through the rows in parent table
                    Dim colChildRows() As DataRow = objrow.GetChildRows(objFieldIdMandatoryRelation)                      'Create an array of child arrays
                    If colChildRows.Length > 0 Then
                        objrow.Item("bitDefaultMandatory") = 1                                                            'add new value to the column item
                    Else
                        objrow.Item("bitDefaultMandatory") = 0                                                            'add new value to the column item
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
            Return dsCombinedDataset.Tables(0)                                                                        'Return the table  
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the form additional paaramter and the group id
        ''' </summary>
        ''' <param name="sParamString">Represents the additional parameter for the form (relationship/ redirection url).</param>
        ''' <param name="numGrpId">Represents the group id for the form.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveFormConfigParams(ByVal sParamString As String, ByVal numGrpId As Integer)
            Try
                Dim iRowsAffected As Int16                                                      'declare an int to store the nos of rows affected
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(0).Value = _FormID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcAdditionalParam", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = sParamString

                arParms(2) = New Npgsql.NpgsqlParameter("@numGrpId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = numGrpId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = DomainID

                iRowsAffected = CShort(SqlDAL.ExecuteNonQuery(connString, "usp_saveDynamicFormAdditionalParam", arParms))   'execute to store the additional param
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the xml into the server file
        ''' </summary>
        ''' <param name="sXMLFilePath">Represents the path to the file.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveDatabaseConfig(ByVal sXMLString As String)
            Try
                'Dim dsXMLConfig As New DataSet                                                      'Create a Dataset object
                'Dim strFileName As String                                                           'Declare a string object
                'strFileName = sXMLFilePath & "\" & "DynamicFormConfig_" & DomainID & "_" & _FormID & "_" & AuthenticationGroupID & ".xml" 'Set the file name

                'If File.Exists(strFileName) = False Then                                            'If file not exists exit
                '    Exit Function
                'End If

                'dsXMLConfig.ReadXml(strFileName)                                                    'Read into XML
                Dim getconnection As New GetConnection                                              'create a new connection


                'for deleting all the entries of config
                Dim getConnectionOne As New GetConnection                                           'create a new connection
                Dim connString As String = getconnection.GetConnectionString                        'get the connection string
                Dim arParmsOne() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}                           'declare parameters array

                arParmsOne(0) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParmsOne(0).Value = FormID

                arParmsOne(1) = New Npgsql.NpgsqlParameter("@numAuthGroupId", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParmsOne(1).Value = AuthenticationGroupID

                arParmsOne(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParmsOne(2).Value = DomainID

                arParmsOne(3) = New Npgsql.NpgsqlParameter("@strFomFld", NpgsqlTypes.NpgsqlDbType.Text)
                arParmsOne(3).Value = sXMLString

                arParmsOne(4) = New Npgsql.NpgsqlParameter("@numBizDocTemplateID", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParmsOne(4).Value = _BizDocTemplateID

                arParmsOne(5) = New Npgsql.NpgsqlParameter("@numSubFormId", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParmsOne(5).Value = numSubFormId

                arParmsOne(6) = New Npgsql.NpgsqlParameter("@numAssignTo", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParmsOne(6).Value = numAssignTo

                arParmsOne(7) = New Npgsql.NpgsqlParameter("@numDripCampaign", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParmsOne(7).Value = numDripCampaign

                arParmsOne(8) = New Npgsql.NpgsqlParameter("@bitByPassRoutingRules", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParmsOne(8).Value = bitByPassRoutingRules

                arParmsOne(9) = New Npgsql.NpgsqlParameter("@bitDripCampaign", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParmsOne(9).Value = bitDripCampaign

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageDynamicFormFields", arParmsOne)
                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for all list details for the dropdown.
        ''' </summary>
        ''' <returns>Returns the value as DataSet.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of list details
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/12/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getListDetails(ByVal iListItemId As Integer) As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = iListItemId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcItemType", NpgsqlTypes.NpgsqlDbType.Char, 3)
                arParms(1).Value = "LI"                                                          'representing listmaster/ listdetails

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetMasterListDetails", arParms)     'execute and store to dataset

                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for the authentication groups dropdown for advance search.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the
        '''     list groups
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function getAuthenticationGroups() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAuthenticationGroupsBizForm", arParms) 'execute and store to dataset

                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for the groups dropdown.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the
        '''     list groups
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/18/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function getGroupNames() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserID                                                       'representing listmaster/ listdetails

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAllGroups", arParms)             'execute and store to dataset

                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the fields which are used in Auto Rules
        ''' </summary>
        ''' <returns>Returns the value as String.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the
        '''     fields referenced in Auto Rules
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/09/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function getAutoRuleReferencedFields() As String
            Try
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                Return SqlDAL.ExecuteScalar(connString, "usp_GetFieldsReferencedInAutoRules", arParms) 'execute and get the referenced fields

            Catch ex As Exception
                Return ""
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the custom field locations
        ''' </summary>
        ''' <returns>Returns the value as datatable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the
        '''     location ids and names of Custom fields
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/17/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetCustomFieldLocations() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(0).Value = Nothing
                arparms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CFWLocation", arparms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get the custom field areas for the form and authentication group
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	02/17/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getSelectedCustomFieldGroups(ByVal sXMLFilePath As String) As DataTable
            Dim dsXMLCustomGroupsConfig As New DataSet                                                      'Create a Dataset object
            Dim strFileName, strFileNameAllGroups As String                                                 'Declare a string object
            strFileName = sXMLFilePath & "\" & "DynamicFormCustomGroupsConfig_" & DomainID & "_" & FormID & "_" & AuthenticationGroupID & ".xml"          'Set the file name
            strFileNameAllGroups = sXMLFilePath & "\" & "DynamicFormCustomGroupsConfig_" & DomainID & "_" & FormID & "_0.xml"  'Set the file name for all groups

            If File.Exists(strFileName) = True Then
                dsXMLCustomGroupsConfig.ReadXml(strFileName)                                                'Read into XML
            ElseIf File.Exists(strFileNameAllGroups) = True Then
                dsXMLCustomGroupsConfig.ReadXml(strFileNameAllGroups)                                       'Read into XML
            End If
            If dsXMLCustomGroupsConfig.Tables.Count = 0 Then                                                'No Table found
                Dim dtTable As New DataTable                                                                'create a new datatable
                dtTable.Columns.Add(New DataColumn("Loc_id", System.Type.GetType("System.String")))         'Add a column for teh Location Id
                dtTable.Columns.Add(New DataColumn("Loc_name", System.Type.GetType("System.String")))       'Add a column for the Location name
                dsXMLCustomGroupsConfig.Tables.Add(dtTable)                                                 'Add table to the dataset
            End If
            Return dsXMLCustomGroupsConfig.Tables(0)                                                        'return xml data table
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the xml of Custom Areas into the server file
        ''' </summary>
        ''' <param name="sXMLString">Represents the xml string.</param>
        ''' <param name="sXMLFilePath">Represents the path to the file.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function SaveCustomAreasXMLConfig(ByVal sXMLString As String, ByVal sXMLFilePath As String)
            Dim StrWriter As StreamWriter                                       'Declare a StreamWriter object
            Dim strFileName As String                                           'Declare a string object
            strFileName = "DynamicFormCustomGroupsConfig_" & DomainID & "_" & FormID & "_" & AuthenticationGroupID & ".xml" 'Set the file name
            Try
                If Directory.Exists(sXMLFilePath) = False Then                  'If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(sXMLFilePath)
                End If
                'Remove read only attribute
                If File.Exists(sXMLFilePath & "\" & strFileName) Then
                    File.SetAttributes(sXMLFilePath & "\" & strFileName, FileAttributes.Normal)
                End If

                StrWriter = File.CreateText(sXMLFilePath & "\" & strFileName)   'create the flle
                StrWriter.Write(sXMLString)                                     'Write XML doument
                StrWriter.Close()                                               'close the object
            Catch ex As Exception
                Throw ex
                'Appropriate permission not given on the folder
            End Try
        End Function

        Public Function ManageDynamicAOI() As Boolean
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strAOI", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = strAOI

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = FormID

                arParms(3) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = AuthenticationGroupID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageDynamicAOI", arParms) 'execute and store to dataset

                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageBizDocSumm() As DataTable
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _BizDocID

                arParms(3) = New Npgsql.NpgsqlParameter("@strFormID", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(3).Value = _strFormFieldID

                arParms(4) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _BizDocTemplateID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ManageBizDocSummary", arParms).Tables(0) 'execute and store to dataset

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageBizDocFilter() As Boolean
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strAOI", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = strAOI

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageBizDocFilter", arParms) 'execute and store to dataset

                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageHTMLFormURL() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = FormID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = AuthenticationGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = vcURL

                arParms(4) = New Npgsql.NpgsqlParameter("@numURLId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = URLId

                arParms(5) = New Npgsql.NpgsqlParameter("@Type", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = tintType

                arParms(6) = New Npgsql.NpgsqlParameter("@vcSuccessURL", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = vcSuccessURL

                arParms(7) = New Npgsql.NpgsqlParameter("@vcFailURL", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = vcFailURL

                arParms(8) = New Npgsql.NpgsqlParameter("@numSubFormId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = numSubFormId

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageHTMLFormURL", arParms)     'execute and store to dataset

                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDynamicFormFieldList() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = tintType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getDynamicFormFieldsList", arParms)     'execute and store to dataset
                ds.Tables(0).TableName = "FieldList"                                            'set the name of the table
                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function saveDynamicFormFieldList() As Boolean
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormFieldId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = FormFieldId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcFormFieldName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = vcFormFieldName

                arParms(4) = New Npgsql.NpgsqlParameter("@vcToolTip", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = vcToolTip

                SqlDAL.ExecuteNonQuery(connString, "USP_saveDynamicFormFieldList", arParms) 'execute and store to dataset

                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFormFieldValidationDetails() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormFieldId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = FormFieldId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getFormFieldValidationDetails", arParms)     'execute and store to dataset
                ds.Tables(0).TableName = "FieldList"                                            'set the name of the table
                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageFormFieldValidation() As Boolean
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFormFieldId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = FormFieldId

                arParms(3) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = strFormFieldID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageFormFieldValidation", arParms) 'execute and store to dataset

                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCustomFormFields() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcLocID", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _strLocationIDs

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getCustomFormFields", arParms)     'execute and store to dataset
                ds.Tables(0).TableName = "FieldList"                                            'set the name of the table
                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function getFieldNameListFromDB() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(1).Value = FormID

                arParms(2) = New Npgsql.NpgsqlParameter("@numAuthGroupId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(2).Value = AuthenticationGroupID

                arParms(3) = New Npgsql.NpgsqlParameter("@numBizDocTemplateID", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(3).Value = _BizDocTemplateID

                arParms(4) = New Npgsql.NpgsqlParameter("@numSubFormId", NpgsqlTypes.NpgsqlDbType.Integer, 9)
                arParms(4).Value = numSubFormId

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getFormConfigFields", arParms)     'execute and store to dataset

                ds.Tables(0).TableName = "FormFieldXML"                                    'Set the name of the table
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDycModuleMaster() As DataTable
            Try
                Dim ds As DataSet                                                           'declare a dataset
                Dim getconnection As New GetConnection                                      'declare a connection
                Dim connString As String = getconnection.GetConnectionString                'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getDycModuleMaster", arParms)   'execute and store to dataset

                Return ds.Tables(0)                                                         'return dtatable

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDycFieldMaster() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = ModuleID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCultureID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = CultureID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getDycFieldMaster", arParms)     'execute and store to dataset
                ds.Tables(0).TableName = "FieldList"                                            'set the name of the table
                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveDycFieldMaster() As Boolean
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = ModuleID

                arParms(2) = New Npgsql.NpgsqlParameter("@numFieldId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = FormFieldId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcFieldName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = vcFormFieldName

                arParms(4) = New Npgsql.NpgsqlParameter("@vcToolTip", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = vcToolTip

                arParms(5) = New Npgsql.NpgsqlParameter("@numCultureID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = CultureID

                SqlDAL.ExecuteNonQuery(connString, "USP_saveDycFieldMaster", arParms) 'execute and store to dataset

                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDycFieldColorScheme() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numFieldColorSchemeID", _FieldColorSchemeID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numFieldID", _FormFieldId, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numFormID", _FormID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))

                    .Add(SqlDAL.Add_Parameter("@vcFieldValue", _strFieldValue, NpgsqlTypes.NpgsqlDbType.Varchar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcFieldValue1", _strFieldValue1, NpgsqlTypes.NpgsqlDbType.Varchar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcColorScheme", _strColorScheme, NpgsqlTypes.NpgsqlDbType.Varchar, 50))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ManageDycFieldColorScheme", sqlParams.ToArray())

                Return ds.Tables(0)                                                           'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageDycFieldColorScheme() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numFieldColorSchemeID", _FieldColorSchemeID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numFieldID", _FormFieldId, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numFormID", _FormID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@vcFieldValue", _strFieldValue, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcFieldValue1", _strFieldValue1, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcColorScheme", _strColorScheme, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageDycFieldColorScheme", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizFormWizardModuleList() As DataTable
            Try
                Dim ds As DataSet                                                           'declare a dataset
                Dim getconnection As New GetConnection                                      'declare a connection
                Dim connString As String = getconnection.GetConnectionString                'get the connection string

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_BizFormWizardModule_GetAll", arParms)   'execute and store to dataset

                Return ds.Tables(0)                                                         'return dtatable

            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace

