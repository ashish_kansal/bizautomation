'Created Anoop Jayaraj
Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Admin

    Public Class CustomFields
        Inherits CBusinessBase

        Private _locId As Long
        Private _GrpName As String
        Private _FieldType As String
        Private _FieldLabel As String
        Private _FieldOrder As Short
        Private _UserId As Long
        Private _TabId As Long
        Private _CusFldId As Long
        Private _strFldId As String
        Private _strDelFldId As String
        Private _RelId As Long
        Private _strEditedFldLst As String
        Private _ListId As Long
        Private _RecordId As Long
        Private _strDetails As String
        Private _CustDtlID As Long
        Private _byteMode As Short
        Private _TabName As String
        Private _CusValue As String
        Private _ContactID As Long
        Private _DivisionID As Long
        Private _LocationType As Long
        'Private DomainId As Long
        Private _URL As String

        Private _Type As Integer
        Public Property Type() As Integer
            Get
                Return _Type
            End Get
            Set(ByVal Value As Integer)
                _Type = Value
            End Set
        End Property

        Private _GroupID As Long
        Public Property GroupID() As Long
            Get
                Return _GroupID
            End Get
            Set(ByVal value As Long)
                _GroupID = value
            End Set
        End Property

        Public Property LocationType() As Long
            Get
                Return _LocationType
            End Get
            Set(value As Long)
                _LocationType = value
            End Set
        End Property

        Public Property URL() As String
            Get
                Return _URL
            End Get
            Set(ByVal Value As String)
                _URL = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property CusValue() As String
            Get
                Return _CusValue
            End Get
            Set(ByVal Value As String)
                _CusValue = Value
            End Set
        End Property

        Public Property TabName() As String
            Get
                Return _TabName
            End Get
            Set(ByVal Value As String)
                _TabName = Value
            End Set
        End Property

        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Public Property CustDtlID() As Long
            Get
                Return _CustDtlID
            End Get
            Set(ByVal Value As Long)
                _CustDtlID = Value
            End Set
        End Property

        Public Property strDetails() As String
            Get
                Return _strDetails
            End Get
            Set(ByVal Value As String)
                _strDetails = Value
            End Set
        End Property

        Public Property RecordId() As Long
            Get
                Return _RecordId
            End Get
            Set(ByVal Value As Long)
                If Value = 0 Then
                    _RecordId = -1
                Else
                    _RecordId = Value
                End If
            End Set
        End Property

        Public Property ListId() As Long
            Get
                Return _ListId
            End Get
            Set(ByVal Value As Long)
                _ListId = Value
            End Set
        End Property

        Public Property strEditedFldLst() As String
            Get
                Return _strEditedFldLst
            End Get
            Set(ByVal Value As String)
                _strEditedFldLst = Value
            End Set
        End Property

        Public Property strDelFldId() As String
            Get
                Return _strDelFldId
            End Get
            Set(ByVal Value As String)
                _strDelFldId = Value
            End Set
        End Property

        Public Property RelId() As Long
            Get
                Return _RelId
            End Get
            Set(ByVal Value As Long)
                _RelId = Value
            End Set
        End Property

        Public Property strFldId() As String
            Get
                Return _strFldId
            End Get
            Set(ByVal Value As String)
                _strFldId = Value
            End Set
        End Property

        Public Property CusFldId() As Long
            Get
                Return _CusFldId
            End Get
            Set(ByVal Value As Long)
                _CusFldId = Value
            End Set
        End Property

        Public Property TabId() As Long
            Get
                Return _TabId
            End Get
            Set(ByVal Value As Long)
                _TabId = Value
            End Set
        End Property

        Public Property UserId() As Long
            Get
                Return _UserId
            End Get
            Set(ByVal Value As Long)
                _UserId = Value
            End Set
        End Property

        Public Property FieldOrder() As Short
            Get
                Return _FieldOrder
            End Get
            Set(ByVal Value As Short)
                _FieldOrder = Value
            End Set
        End Property

        Public Property FieldLabel() As String
            Get
                Return _FieldLabel
            End Get
            Set(ByVal Value As String)
                _FieldLabel = Value
            End Set
        End Property

        Public Property FieldType() As String
            Get
                Return _FieldType
            End Get
            Set(ByVal Value As String)
                _FieldType = Value
            End Set
        End Property

        Public Property grpName() As String
            Get
                Return _GrpName
            End Get
            Set(ByVal Value As String)
                _GrpName = Value
            End Set
        End Property


        Public Property locId() As Long
            Get
                Return _locId
            End Get
            Set(ByVal Value As Long)
                _locId = Value
            End Set
        End Property

        Private _vcToolTip As String
        Public Property vcToolTip() As String
            Get
                Return _vcToolTip
            End Get
            Set(ByVal Value As String)
                _vcToolTip = Value
            End Set
        End Property

        Private _FormId As Long
        Public Property FormId() As Long
            Get
                Return _FormId
            End Get
            Set(ByVal Value As Long)
                _FormId = Value
            End Set
        End Property

        Private _Mode As Short
        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal Value As Short)
                _Mode = Value
            End Set
        End Property

        Private _ParentChildFieldID As Short
        Public Property ParentChildFieldID() As Short
            Get
                Return _ParentChildFieldID
            End Get
            Set(ByVal Value As Short)
                _ParentChildFieldID = Value
            End Set
        End Property

        Private _ParentModule As Short
        Public Property ParentModule() As Short
            Get
                Return _ParentModule
            End Get
            Set(ByVal Value As Short)
                _ParentModule = Value
            End Set
        End Property

        Private _ParentFieldID As Short
        Public Property ParentFieldID() As Short
            Get
                Return _ParentFieldID
            End Get
            Set(ByVal Value As Short)
                _ParentFieldID = Value
            End Set
        End Property

        Private _ChildModule As Short
        Public Property ChildModule() As Short
            Get
                Return _ChildModule
            End Get
            Set(ByVal Value As Short)
                _ChildModule = Value
            End Set
        End Property

        Private _ChildFieldID As Short
        Public Property ChildFieldID() As Short
            Get
                Return _ChildFieldID
            End Get
            Set(ByVal Value As Short)
                _ChildFieldID = Value
            End Set
        End Property
        Public Property IsCustomField As Boolean


        Private _AutoComplete As Boolean
        Public Property AutoComplete() As Boolean
            Get
                Return _AutoComplete
            End Get
            Set(ByVal Value As Boolean)
                _AutoComplete = Value
            End Set
        End Property

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region



        Public Function Tabs() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@locId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _locId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CLFTabs", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function CflSave() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@locid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _locId


                arParms(1) = New Npgsql.NpgsqlParameter("@fldtype", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _FieldType


                arParms(2) = New Npgsql.NpgsqlParameter("@fldlbl", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _FieldLabel


                arParms(3) = New Npgsql.NpgsqlParameter("@FieldOdr", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _FieldOrder

                arParms(4) = New Npgsql.NpgsqlParameter("@userId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _UserId


                arParms(5) = New Npgsql.NpgsqlParameter("@TabId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _TabId

                arParms(6) = New Npgsql.NpgsqlParameter("@fldId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _CusFldId

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = DomainID

                arParms(8) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(8).Value = _URL

                arParms(9) = New Npgsql.NpgsqlParameter("@vcToolTip", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(9).Value = _vcToolTip

                arParms(10) = New Npgsql.NpgsqlParameter("@ListId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Direction = ParameterDirection.InputOutput
                arParms(10).Value = _ListId

                arParms(11) = New Npgsql.NpgsqlParameter("@bitAutoComplete", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = AutoComplete

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_cflManage", objParam, True)
                _CusFldId = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)
                _ListId = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(10).Value)

                Return _CusFldId
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function CustomFieldList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@locId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _locId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Usp_cflList", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteCusFld() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@fld_id", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CusFldId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "Usp_cflDeleteField", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRelationship() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(0).Value = Nothing
                arparms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CFWLocation", arparms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetAdminRelationship() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CFWLocationAdmin", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetFieldDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@fldid", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CusFldId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "uso_cfwfldDetails", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetFieldDetailsByLocAndTabId() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@TabId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TabId


                arParms(1) = New Npgsql.NpgsqlParameter("@loc_id", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _locId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_cfwListFiels", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SortField() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strFiledId", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(0).Value = _strFldId

                arParms(1) = New Npgsql.NpgsqlParameter("@RelaionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RelId

                arParms(2) = New Npgsql.NpgsqlParameter("@TabId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _TabId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "Usp_cfwFieldSort", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AddCustomFldsToAll() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@FieldId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _CusFldId

                arParms(1) = New Npgsql.NpgsqlParameter("@RelaionId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _RelId

                SqlDAL.ExecuteNonQuery(connString, "Usp_AddCustomFldsToAll", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function GetAddedFieldDetailsByLocAndTabId() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@TabId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TabId


                arParms(1) = New Npgsql.NpgsqlParameter("@loc_id", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _locId

                arParms(2) = New Npgsql.NpgsqlParameter("@relId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RelId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_cfwListFieldsAdded", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetFieldListForEditing() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@TabId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TabId

                arParms(1) = New Npgsql.NpgsqlParameter("@loc_id", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _locId

                arParms(2) = New Npgsql.NpgsqlParameter("@relId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RelId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_cfwListFieldsDetails", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageCFValidation() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@strFieldList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _strEditedFldLst


                arParms(1) = New Npgsql.NpgsqlParameter("@numFieldID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _CusFldId

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageCFValidation", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetCustFlds() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@PageId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _locId


                arParms(1) = New Npgsql.NpgsqlParameter("@numRelation", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RelId

                arParms(2) = New Npgsql.NpgsqlParameter("@numRecordId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RecordId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@numLocationType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = LocationType

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_cfwGetFields", arParms)

                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCustFldsOppItems(ByVal OppItemID As Long, ByVal ItemCode As Long) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppItemId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OppItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_cfwgetfieldsoppitems", arParms)

                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetMasterListByListId() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ListId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ListId

                arParms(1) = New Npgsql.NpgsqlParameter("@bitDeleted", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(1).Value = False

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_MasterList", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SaveCustomFldsByRecId() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@RecordId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RecordId


                arParms(1) = New Npgsql.NpgsqlParameter("@strDetails", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strDetails

                arParms(2) = New Npgsql.NpgsqlParameter("@PageId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _locId
                If _RecordId > 0 Then 'Bug ID:  2043
                    SqlDAL.ExecuteNonQuery(connString, "USP_cfwSaveCusfld", arParms)
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveCustomFldsByLocationRecId() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@DomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID


                arParms(1) = New Npgsql.NpgsqlParameter("@strDetails", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strDetails

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "USP_cfwSaveCusfldLocation", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function SaveCustomFldsForLeadBox() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}



                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID


                arParms(2) = New Npgsql.NpgsqlParameter("@fld_id", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CusFldId

                arParms(3) = New Npgsql.NpgsqlParameter("@Fld_Value", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _CusValue

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveCusfldForLeadBox", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function DeleteDTLCustField() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFieldDtlID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CustDtlID

                SqlDAL.ExecuteNonQuery(connString, "USP_CFWDtlFieldDelete", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function ManageTabsInCustomFields() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@LocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _locId

                arParms(2) = New Npgsql.NpgsqlParameter("@TabName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _TabName

                arParms(3) = New Npgsql.NpgsqlParameter("@TabID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _TabId

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(5).Value = _URL

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageTabsInCuSFields", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function GetCustomFieldSubTabs() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim ds As DataSet
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}



                arParms(0) = New Npgsql.NpgsqlParameter("@LocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _locId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@Type", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _Type

                arParms(3) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _GroupID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCustomFieldsTab", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function GetCustomTabGetFields() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@PageId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _locId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRelation", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RelId

                arParms(2) = New Npgsql.NpgsqlParameter("@numRecordId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RecordId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _FormId

                arParms(6) = New Npgsql.NpgsqlParameter("@numTabID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _TabId

                arParms(7) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _Mode

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetCustomTabGetFields", arParms)

                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetParentChildCustomFieldMap() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint, 9))
                    .Add(SqlDAL.Add_Parameter("@tintChildModule", _locId, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetParentChildCustomFieldMap", sqlParams.ToArray())

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageParentChildCustomFieldMap() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numParentChildFieldID", _ParentChildFieldID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@tintParentModule", _ParentModule, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numParentFieldID", _ParentFieldID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@tintChildModule", _ChildModule, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numChildFieldID", _ChildFieldID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))
                    .Add(SqlDAL.Add_Parameter("@tintMode", _Mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@bitCustomField", IsCustomField, NpgsqlTypes.NpgsqlDbType.Bit))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageParentChildCustomFieldMap", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetCustomFieldListAndData() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@locId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _locId

                arParms(2) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ListId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetCustomFieldList", arParms)

                Return ds

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

    End Class

End Namespace