'Created Anoop Jayaraj
Option Explicit On 
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin

Public Class Tickler
        Inherits BACRM.BusinessLogic.CBusinessBase
        'Private DomainId As Long
        'Private UserCntID As Long
        Private _StartDate As Date
        Private _EndDate As Date
        Private _TicklerType As Long
        Private _CompanyID As Long
        Private _DivisionID As Long
        Private _OppID As Long
        Private _ContactID As Long
        Private _OppType As Short
        Private _TeamType As Short
        Private _ResourceName As String
        Private _RegularSearchCriteria As String
        Private _OppStatus As Short
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _TypeId As Integer
        Private _ViewId As Integer
        Public Property ViewId() As Integer
            Get
                Return _ViewId
            End Get
            Set(ByVal Value As Integer)
                _ViewId = Value
            End Set
        End Property
        Public Property TypeId() As Integer
            Get
                Return _TypeId
            End Get
            Set(ByVal Value As Integer)
                _TypeId = Value
            End Set
        End Property

        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property
        Public Property ResourceName() As String
            Get
                Return _ResourceName
            End Get
            Set(ByVal Value As String)
                _ResourceName = Value
            End Set
        End Property
        Public Property TeamType() As Short
            Get
                Return _TeamType
            End Get
            Set(ByVal Value As Short)
                _TeamType = Value
            End Set
        End Property

        Public Property OppType() As Short
            Get
                Return _OppType
            End Get
            Set(ByVal Value As Short)
                _OppType = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal Value As Long)
                _OppID = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property CompanyID() As Long
            Get
                Return _CompanyID
            End Get
            Set(ByVal Value As Long)
                _CompanyID = Value
            End Set
        End Property

        Public Property TicklerType() As Long
            Get
                Return _TicklerType
            End Get
            Set(ByVal Value As Long)
                _TicklerType = Value
            End Set
        End Property

        Public Property EndDate() As Date
            Get
                Return _EndDate
            End Get
            Set(ByVal Value As Date)
                _EndDate = Value
            End Set
        End Property

        Public Property StartDate() As Date
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As Date)
                _StartDate = Value
            End Set
        End Property

        Public Property OppStatus() As Short
            Get
                Return _OppStatus
            End Get
            Set(ByVal Value As Short)
                _OppStatus = Value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Local (Client) Time Zone Offset from GMT .
        ''' </summary>
        ''' <remarks>
        '''     This holds the Client Time Offset which is being edited/ deleted
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ClientTimeZoneOffset As Int32
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Manages the Private Property representing the Local (Client) Time Zone Offset from GMT .
        ''' </summary>
        ''' <remarks>
        '''     
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	12/08/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _columnName As String

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Private _columnSortOrder As String

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property CustomSearchCriteria As String
        Public Property FilterBy As Short
        Public Property FilterValues As String
        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region


        Public Function GetActionItems() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@startDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _StartDate

                arParms(3) = New Npgsql.NpgsqlParameter("@endDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _EndDate

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer) 'Param added by Deb (Since Date and Time must be displayed w.r.t. the client machine)
                arParms(4).Value = ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_TicklerActItem", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetActItem(Optional boolFilterRecord As Boolean = False, Optional strBProcessValue As String = "") As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(17) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@startDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _StartDate

                arParms(3) = New Npgsql.NpgsqlParameter("@endDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _EndDate

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer) 'Param added by Deb (Since Date and Time must be displayed w.r.t. the client machine)
                arParms(4).Value = ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@bitFilterRecord", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = boolFilterRecord

                arParms(6) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _columnName

                arParms(7) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(7).Value = _columnSortOrder

                arParms(8) = New Npgsql.NpgsqlParameter("@vcBProcessValue", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(8).Value = strBProcessValue

                arParms(9) = New Npgsql.NpgsqlParameter("@RegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(9).Value = RegularSearchCriteria

                arParms(10) = New Npgsql.NpgsqlParameter("@CustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(10).Value = CustomSearchCriteria

                arParms(11) = New Npgsql.NpgsqlParameter("@OppStatus", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Value = OppStatus

                arParms(12) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(12).Value = _CurrentPage

                arParms(13) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(13).Value = _PageSize

                arParms(14) = New Npgsql.NpgsqlParameter("@tintFilterBy", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(14).Value = FilterBy

                arParms(15) = New Npgsql.NpgsqlParameter("@vcFilterValues", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(15).Value = FilterValues

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                arParms(17) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(17).Value = Nothing
                arParms(17).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_TicklerActItemsV2", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    _TotalRecords = Convert.ToInt32(ds.Tables(0).Rows(0)("TotalRecords"))
                Else
                    _TotalRecords = 0
                End If


                Return ds

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetActivityDetails() As DataSet
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@ActivityID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = _ContactID

            arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(1).Value = Nothing
            arParms(1).Direction = ParameterDirection.InputOutput

            Return SqlDAL.ExecuteDataset(connString, "usp_GetActivityDetailsById", arParms)
        End Function
        Public Function GetOldActItem(Optional boolFilterRecord As Boolean = False, Optional strBProcessValue As String = "") As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@startDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _StartDate

                arParms(3) = New Npgsql.NpgsqlParameter("@endDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _EndDate

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer) 'Param added by Deb (Since Date and Time must be displayed w.r.t. the client machine)
                arParms(4).Value = ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@bitFilterRecord", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = boolFilterRecord

                arParms(6) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _columnName

                arParms(7) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(7).Value = _columnSortOrder

                arParms(8) = New Npgsql.NpgsqlParameter("@vcBProcessValue", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(8).Value = strBProcessValue

                arParms(9) = New Npgsql.NpgsqlParameter("@RegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(9).Value = RegularSearchCriteria

                arParms(10) = New Npgsql.NpgsqlParameter("@CustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(10).Value = CustomSearchCriteria

                arParms(11) = New Npgsql.NpgsqlParameter("@OppStatus", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Value = OppStatus

                arParms(12) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(12).Value = _CurrentPage

                arParms(13) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(13).Value = _PageSize

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_TicklerActItems", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    _TotalRecords = Convert.ToInt32(ds.Tables(0).Rows(0)("TotalRecords"))
                Else
                    _TotalRecords = 0
                End If


                Return ds

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetActItemButtonCount() As Integer
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@startDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _StartDate

                arParms(3) = New Npgsql.NpgsqlParameter("@endDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _EndDate

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer) 'Param added by Deb (Since Date and Time must be displayed w.r.t. the client machine)
                arParms(4).Value = ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@numViewID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _ViewId

                arParms(6) = New Npgsql.NpgsqlParameter("@numTotalRecords", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _TotalRecords
                arParms(6).Direction = ParameterDirection.Output

                arParms(7) = New Npgsql.NpgsqlParameter("@TypeId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _TypeId

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_TicklerActItemsButtonCount", objParam, True)
                _TotalRecords = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)

                Return _TotalRecords
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetUserActivity() As DataTable
            Try
                Dim ds As DataSet
                Dim dt As DataTable = Nothing
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetTaskForUserActivity", arParms)


                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    dt = ds.Tables(0)
                End If

                Return dt
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetOpportunity() As DataTable
            Try
                Dim ds As DataSet
                Dim dt As DataTable = Nothing
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@startDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _StartDate

                arParms(3) = New Npgsql.NpgsqlParameter("@endDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _EndDate

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_TicklerOpportuity", arParms)


                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    dt = ds.Tables(0)
                End If

                Return dt
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCases() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@startDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _StartDate

                arParms(3) = New Npgsql.NpgsqlParameter("@endDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _EndDate

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_TicklerCases", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function
        Public Function GetBizDocActionItem() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId


                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocAction", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function




        Public Function GetDivisionName() As String
            Try
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "usp_GetDivisionName", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetAssingedStages() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@startDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(2).Value = _StartDate

                arParms(3) = New Npgsql.NpgsqlParameter("@endDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _EndDate


                arParms(4) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _OppID

                arParms(5) = New Npgsql.NpgsqlParameter("@OppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _OppType

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_TicklerStages", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetEmployees() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _TeamType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_TicklerEmployees", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetListFilterSetting() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_TicklerListFilterConfiguration_Get", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub SaveListFilterSetting(ByVal vcSelectedEmployee As String, ByVal numCriteria As Integer, ByVal vcActionTypes As String,
                                         ByVal vcSelectedEmployeeOpp As String, ByVal vcSelectedEmployeeCases As String, ByVal vcAssignedStages As String,
                                         ByVal numCriteriaCases As Integer)
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSelectedEmployee", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = vcSelectedEmployee

                arParms(3) = New Npgsql.NpgsqlParameter("@numCriteria", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = numCriteria

                arParms(4) = New Npgsql.NpgsqlParameter("@vcActionTypes", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = vcActionTypes

                arParms(5) = New Npgsql.NpgsqlParameter("@vcSelectedEmployeeOpp", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = vcSelectedEmployeeOpp

                arParms(6) = New Npgsql.NpgsqlParameter("@vcSelectedEmployeeCases", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = vcSelectedEmployeeCases

                arParms(7) = New Npgsql.NpgsqlParameter("@vcAssignedStages", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = vcAssignedStages

                arParms(8) = New Npgsql.NpgsqlParameter("@numCriteriaCases", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = numCriteriaCases

                ds = SqlDAL.ExecuteDataset(connString, "USP_TicklerListFilterConfiguration_Save", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

    End Class

End Namespace
