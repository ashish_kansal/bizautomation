﻿'Created by Anoop Jayaraj
Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Admin
    Public Class CurrencyRates
        Inherits BACRM.BusinessLogic.CBusinessBase


        'Private DomainId As Long
        Private _CurrencyDesc As String
        Private _CurrencySymbol As String
        Private _CurrencyID As Long
        Private _Enabled As Boolean
        Private _ExchangeRate As Double
        Private _GetAll As Boolean

        Public Property GetAll() As Boolean
            Get
                Return _GetAll
            End Get
            Set(ByVal Value As Boolean)
                _GetAll = Value
            End Set
        End Property

        Public Property ExchangeRate() As Double
            Get
                Return _ExchangeRate
            End Get
            Set(ByVal Value As Double)
                _ExchangeRate = Value
            End Set
        End Property

        Public Property Enabled() As Boolean
            Get
                Return _Enabled
            End Get
            Set(ByVal Value As Boolean)
                _Enabled = Value
            End Set
        End Property

        Public Property CurrencyID() As Long
            Get
                Return _CurrencyID
            End Get
            Set(ByVal Value As Long)
                _CurrencyID = Value
            End Set
        End Property

        Public Property CurrencySymbol() As String
            Get
                Return _CurrencySymbol
            End Get
            Set(ByVal Value As String)
                _CurrencySymbol = Value
            End Set
        End Property

        Public Property CurrencyDesc() As String
            Get
                Return _CurrencyDesc
            End Get
            Set(ByVal Value As String)
                _CurrencyDesc = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Private _CountryID As Long
        Public Property CountryID() As Long
            Get
                Return _CountryID
            End Get
            Set(ByVal Value As Long)
                _CountryID = Value
            End Set
        End Property

        Private _SiteID As Long
        Public Property SiteID() As Long
            Get
                Return _SiteID
            End Get
            Set(ByVal Value As Long)
                _SiteID = Value
            End Set
        End Property
        Public Function GetCurrencyWithRates() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomanID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@bitAll", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _GetAll

                arParms(2) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CurrencyID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCurrencyRate", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCurrencyWithCountry() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomanID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCurrencyWithCountry", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function UpdateCurrencyRate() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomanID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@fltExchangeRate", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(1).Value = _ExchangeRate

                arParms(2) = New Npgsql.NpgsqlParameter("@varCurrSymbol", NpgsqlTypes.NpgsqlDbType.VarChar, 3)
                arParms(2).Value = _CurrencySymbol

                arParms(3) = New Npgsql.NpgsqlParameter("@numCurrencyId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _CurrencyID

                arParms(4) = New Npgsql.NpgsqlParameter("@bitEnabled", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _Enabled

                arParms(5) = New Npgsql.NpgsqlParameter("@vcCurrencyDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Value = _CurrencyDesc

                arParms(6) = New Npgsql.NpgsqlParameter("@numCountryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _CountryID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateCurrencyDetails", arParms)

                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetCurrencyFromCountry() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomanID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCountryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CountryID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCurrencyFromCountry", arParms).Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' Updated currency conversion rates fetached from yahoo api
        ''' </summary>
        ''' <param name="vcToCurrecy">Currency Code for which rate is fetched against Base Currency</param>
        ''' <param name="rate">Currency Conversion Rate</param>
        ''' <param name="vcDomains">Comma seperated strings of domain ids whose base currency is same</param>
        ''' <remarks></remarks>
        Public Sub UpdateExchangenRates(ByVal vcToCurrency As String, ByVal rate As Double, ByVal vcDomains As String)
            Try
                Dim ingAccountClassID As Long = 0
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@vcToCurrency", vcToCurrency, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@decRate", rate, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@vcDomains", vcDomains, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_Currency_UpdateExchangeRate", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

    End Class
End Namespace
