﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin

    Public Class DomainMarketplace
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property MarketplaceID As Long
        Public Property DivisionID As Long

#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numMarketplaceID", MarketplaceID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_DomainMarketplace_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class

End Namespace