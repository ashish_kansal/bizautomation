'Created Reji
'Option Explicit On
'Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common
Namespace BACRM.BusinessLogic.Admin
    Public Class ImportWizard

#Region "PRIVATE VARIABLES"

        Private _relationship As Long
        Private _contact As Long
        Private _fldId As Long
        Private _fldValue As String
        Private _recId As Long
        Private _DomainId As Long
        Private _StrXml As String
        Private _ImportType As Short

        Private _intImportMasterId As Integer
        Private _intMode As Integer

#End Region

#Region "PUBLIC PROPERTIES"

        Private _sIsCustomField As Short
        Public Property IsCustomField() As Short
            Get
                Return _sIsCustomField
            End Get
            Set(ByVal value As Short)
                _sIsCustomField = value
            End Set
        End Property


        Private _intCustomFieldPageID As Long
        Public Property CustomFieldPageID() As Long
            Get
                Return _intCustomFieldPageID
            End Get
            Set(ByVal value As Long)
                _intCustomFieldPageID = value
            End Set
        End Property


        Public Property ImportType() As Short
            Get
                Return _ImportType
            End Get
            Set(ByVal Value As Short)
                _ImportType = Value
            End Set
        End Property

        Public Property StrXml() As String
            Get
                Return _StrXml
            End Get
            Set(ByVal Value As String)
                _StrXml = Value
            End Set
        End Property

        Public Property DomainId() As Long
            Get
                Return _DomainId
            End Get
            Set(ByVal Value As Long)
                _DomainId = Value
            End Set
        End Property

        Public Property Relationship() As Long
            Get
                Return _relationship
            End Get
            Set(ByVal Value As Long)
                _relationship = Value
            End Set
        End Property

        Public Property Contact() As Long
            Get
                Return _contact
            End Get
            Set(ByVal Value As Long)
                _contact = Value
            End Set
        End Property

        Public Property FldId() As Long
            Get
                Return _fldId
            End Get
            Set(ByVal Value As Long)
                _fldId = Value
            End Set
        End Property

        Public Property FldValue() As String
            Get
                Return _fldValue
            End Get
            Set(ByVal Value As String)
                _fldValue = Value
            End Set
        End Property

        Public Property RecId() As Long
            Get
                Return _recId
            End Get
            Set(ByVal Value As Long)
                _recId = Value
            End Set
        End Property

        Public Property ImportMasterID() As Integer
            Get
                Return _intImportMasterId
            End Get
            Set(ByVal value As Integer)
                _intImportMasterId = value
            End Set
        End Property

        Public Property Mode() As Integer
            Get
                Return _intMode
            End Get
            Set(ByVal value As Integer)
                _intMode = value
            End Set
        End Property


        Private _intType As Integer
        Public Property Type() As Integer
            Get
                Return _intType
            End Get
            Set(ByVal value As Integer)
                _intType = value
            End Set
        End Property


        Private _sPageType As Short
        Public Property PageType() As Short
            Get
                Return _sPageType
            End Get
            Set(ByVal value As Short)
                _sPageType = value
            End Set
        End Property

        Public Property ItemLinkingID As Integer
        Public Property InsertUpdateType As Short

        Public Property IsSingleOrder As Boolean
        Public Property IsExistingOrganization As Boolean
        Public Property DivisionID As Long
        Public Property ContactID As Long
        Public Property CompanyName As String
        Public Property ContactFirstName As String
        Public Property ContactLastName As String
        Public Property ContactEmail As String
        Public Property IsMatchOrganizationID As Boolean
        Public Property MatchFieldID As Long
#End Region

#Region "ASSIGN/GET/UPDATE/SAVE FUNCTIONS"

        Public Function AssignUserDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetUserAssign", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDestination() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numRelation", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _relationship
                arParms(1) = New Npgsql.NpgsqlParameter("@numContactType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _contact
                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput
                ds = SqlDAL.ExecuteDataset(connString, "USP_PopulateCustomFlds", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateCustomeField() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numFldID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _fldId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcFldValue", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _fldValue

                arParms(2) = New Npgsql.NpgsqlParameter("@numRecId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _recId

                SqlDAL.ExecuteNonQuery(connString, "usp_InsertCustomeField", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetStateAndCountry(ByVal byteMode As Short, ByVal vcText As String, ByVal DomainID As Long, Optional ByVal ListID As Long = 0) As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@vcText", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = vcText

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = ListID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetStateAndCountryID", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetConfiguration() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numRelation", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _relationship

                arParms(1) = New Npgsql.NpgsqlParameter("@DomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@ImportType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ImportType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetImportConfiguration", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveImportConfg() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@FormId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _lngMasterID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@StrXml", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _StrXml

                arParms(4) = New Npgsql.NpgsqlParameter("@tintPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _sPageType

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveImportConfg", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetConfigurationTable() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numRelation", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _relationship

                arParms(1) = New Npgsql.NpgsqlParameter("@DomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@ImportType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ImportType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetImportConfigurationTable", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetImportMasterCategories() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@intImportMasterID", _intImportMasterId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@intMode", _intMode, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@numImportFileID", _intImportFileID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintInsertUpdateType", InsertUpdateType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@tintItemLinkingID", ItemLinkingID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GET_DYNAMIC_IMPORT_FIELDS", sqlParams.ToArray())

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "CONSTRUCTOR"
        Sub New()

        End Sub

        Sub New(ByVal intImportFileID As Long, ByVal intDomainID As Long)
            Dim dtImportData As New DataTable
            _intImportFileID = intImportFileID
            _DomainId = intDomainID
            dtImportData = LoadMappedData().Tables(0)

            ' TODO: Complete member initialization 
            _strImportFileName = CCommon.ToString(dtImportData.Rows(0)("ImportFileName"))
            '_dtImportDate = DateFromFormattedDate(dtImportData.Rows(0)("CreatedDate"), "MM-DD-YYYY")
            _intImportMasterId = CCommon.ToInteger(dtImportData.Rows(0)("MasterID"))
            '_dtCreatedDate = DateFromFormattedDate(dtImportData.Rows(0)("CreatedDate"), "MM-DD-YYYY")
            _strHistory_Added_Value = CCommon.ToString(dtImportData.Rows(0)("ItemCodes"))
        End Sub

#End Region

#Region "PRIVATE VARIABLES/PUBLIC PROPERTIES : SAVE IMPORT DATA/DATA MAPPING "

        Private _intImportFileID As Long
        Public Property ImportFileID() As Long
            Get
                Return _intImportFileID
            End Get
            Set(ByVal value As Long)
                _intImportFileID = value
            End Set
        End Property

        Private _strImportFileName As String
        Public Property ImportFileName() As String
            Get
                Return _strImportFileName
            End Get
            Set(ByVal value As String)
                _strImportFileName = value
            End Set
        End Property

        Private _lngMasterID As Long
        Public Property MasterID() As Long
            Get
                Return _lngMasterID
            End Get
            Set(ByVal value As Long)
                _lngMasterID = value
            End Set
        End Property

        Private _lngRecordAdded As Long
        Public Property RecordAdded() As Long
            Get
                Return _lngRecordAdded
            End Get
            Set(ByVal value As Long)
                _lngRecordAdded = value
            End Set
        End Property

        Private _lngRecordUpdated As Long
        Public Property RecordUpdated() As Long
            Get
                Return _lngRecordUpdated
            End Get
            Set(ByVal value As Long)
                _lngRecordUpdated = value
            End Set
        End Property

        Private _lngErrors As Long
        Public Property Errors() As Long
            Get
                Return _lngErrors
            End Get
            Set(ByVal value As Long)
                _lngErrors = value
            End Set
        End Property

        Private _lngDuplicates As Long
        Public Property Duplicates() As Long
            Get
                Return _lngDuplicates
            End Get
            Set(ByVal value As Long)
                _lngDuplicates = value
            End Set
        End Property

        Private _lngHistoryID As Long
        Public Property HistoryID() As Long
            Get
                Return _lngHistoryID
            End Get
            Set(ByVal value As Long)
                _lngHistoryID = value
            End Set
        End Property

        Private _dtCreatedDate As DateTime
        Public Property CreatedDate() As DateTime
            Get
                Return _dtCreatedDate
            End Get
            Set(ByVal value As DateTime)
                _dtCreatedDate = value
            End Set
        End Property

        Private _dtImportDate As DateTime
        Public Property ImportDate() As DateTime
            Get
                Return _dtImportDate
            End Get
            Set(ByVal value As DateTime)
                _dtImportDate = value
            End Set
        End Property

        Private _lngUserContactID As Long
        Public Property UserContactID() As Long
            Get
                Return _lngUserContactID
            End Get
            Set(ByVal value As Long)
                _lngUserContactID = value
            End Set
        End Property

        Private _intStatus As Short
        Public Property Status() As Short
            Get
                Return _intStatus
            End Get
            Set(ByVal value As Short)
                _intStatus = value
            End Set
        End Property

        Private _blnHasSendEmail As Boolean
        Public Property hasSendEmail() As Boolean
            Get
                Return _blnHasSendEmail
            End Get
            Set(ByVal value As Boolean)
                _blnHasSendEmail = value
            End Set
        End Property

        Private _dtMappedData As DataTable
        Public Property MappedData() As DataTable
            Get
                Return _dtMappedData
            End Get
            Set(ByVal value As DataTable)
                _dtMappedData = value
            End Set
        End Property

        Private _strHistory_Added_Value As String
        Public Property History_Added_Value() As String
            Get
                Return _strHistory_Added_Value
            End Get
            Set(ByVal value As String)
                _strHistory_Added_Value = value
            End Set
        End Property

        Private _dtHistoryDateTime As DateTime
        Public Property HistoryDateTime() As DateTime
            Get
                Return _dtHistoryDateTime
            End Get
            Set(ByVal value As DateTime)
                _dtHistoryDateTime = value
            End Set
        End Property

#End Region

#Region "SAVE DATA FUNCTION"

        Function SaveImportMappedData() As Integer
            Dim intResult As Integer = Nothing
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@intImportFileID", _intImportFileID, NpgsqlTypes.NpgsqlDbType.BigInt, 18, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@vcImportFileName", _strImportFileName, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@numMasterID", _lngMasterID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numRecordAdded", _lngRecordAdded, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numRecordUpdated", _lngRecordUpdated, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numErrors", _lngErrors, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDuplicates", _lngDuplicates, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@dtCreateDate", If(_dtCreatedDate = Nothing, DBNull.Value, _dtCreatedDate), NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@dtImportDate", If(_dtImportDate = Nothing, DBNull.Value, _dtImportDate), NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", _DomainId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserContactID", _lngUserContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintStatus", _intStatus, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@btHasSendEmail", _blnHasSendEmail, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@vcHistory_Added_Value", _strHistory_Added_Value, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@dtHistoryDateTime", If(_dtHistoryDateTime = Nothing, DBNull.Value, _dtHistoryDateTime), NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@intMode", _intMode, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@tintItemLinkingID", ItemLinkingID, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@tintImportType", InsertUpdateType, NpgsqlTypes.NpgsqlDbType.Smallint))


                    .Add(SqlDAL.Add_Parameter("@bitSingleOrder", IsSingleOrder, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitExistingOrganization", IsExistingOrganization, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numContactID", ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcCompanyName", CompanyName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcContactFirstName", ContactFirstName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcContactLastName", ContactLastName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcContactEmail", ContactEmail, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitMatchOrganizationID", IsMatchOrganizationID, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numMatchFieldID", MatchFieldID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                Dim objParam() As Object = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_MANAGE_IMPORT_FILE_DATA", objParam, True)

                If _intMode = 0 Then
                    _intImportFileID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                End If

                If _intImportFileID > 0 AndAlso _dtMappedData IsNot Nothing Then
                    Me.DeleteMappedChildData()

                    For intRow As Integer = 0 To _dtMappedData.Rows.Count - 1
                        sqlParams.Clear()

                        With sqlParams
                            .Add(SqlDAL.Add_Parameter("@numImportFileID", _intImportFileID, NpgsqlTypes.NpgsqlDbType.Numeric))
                            .Add(SqlDAL.Add_Parameter("@numFormFieldID", CCommon.ToLong(_dtMappedData.Rows(intRow)("FormFieldID")), NpgsqlTypes.NpgsqlDbType.Numeric))
                            .Add(SqlDAL.Add_Parameter("@intMapColumnNo", CCommon.ToInteger(_dtMappedData.Rows(intRow)("ImportFieldID")), NpgsqlTypes.NpgsqlDbType.Integer))
                            .Add(SqlDAL.Add_Parameter("@bitCustomField", CCommon.ToBool(_dtMappedData.Rows(intRow)("IsCustomField")), NpgsqlTypes.NpgsqlDbType.Boolean))
                        End With

                        intResult = CInt(SqlDAL.ExecuteNonQuery(connString, "USP_MANAGE_IMPORT_FILE_FIELD_MAPPING", sqlParams.ToArray()))
                    Next
                End If

            Catch ex As Exception
                Throw ex
            End Try
            Return intResult
        End Function

#End Region

#Region "LOAD MAPPED DATA FUNCTION"

        Public Function LoadMappedData() As DataSet
            Dim dsResult As New DataSet
            Try
                Dim SpName As String = "USP_GET_IMPORT_MAPPED_DATA"
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@intImportFileID", _intImportFileID, 0))
                sqlParams.Add(SqlDAL.Add_Parameter("@numDomainID", _DomainId, 0))
                sqlParams.Add(SqlDAL.Add_Parameter("@numFormID", _lngMasterID, 0))
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                dsResult = SqlDAL.ExecuteDataset(connString, SpName, sqlParams.ToArray())

            Catch ex As Exception
                dsResult = Nothing
                Throw ex
            End Try
            Return dsResult
        End Function

#End Region

#Region "DELETE MAPPED FILE/DATA FUNCTION"

        Public Function DeleteMappedFileData() As Integer
            Dim intResult As New Integer
            Try
                Dim SpName As String = "USP_DELETE_IMPORT_FILE"
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@intImportFileID", _intImportFileID, 0))

                sqlParams.Add(SqlDAL.Add_Parameter("@numDomainID", _DomainId, 0))

                sqlParams.Add(SqlDAL.Add_Parameter("@numUserCntID", _lngUserContactID, 0))

                intResult = SqlDAL.ExecuteNonQuery(connString, SpName, sqlParams.ToArray())

            Catch ex As Exception
                intResult = Nothing
                Throw ex
            End Try
            Return intResult
        End Function

        Public Function DeleteMappedChildData() As Integer
            Dim intResult As New Integer
            Try
                Dim SpName As String = "USP_DELETE_IMPORT_FILE_FIELD_MAPPING"
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@intImportFileID", _intImportFileID, 0))

                intResult = SqlDAL.ExecuteNonQuery(connString, SpName, sqlParams.ToArray())

            Catch ex As Exception
                intResult = Nothing
                Throw ex
            End Try
            Return intResult
        End Function

#End Region

#Region "GET DROP DOWN DATA FUNCTION"

        Public Function GetDropDownData(ByVal vcFields As String) As DataSet
            Dim dsResult As New DataSet
            Try
                Dim SpName As String = "USP_GET_DROPDOWN_DATA_FOR_IMPORT"
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                sqlParams.Add(SqlDAL.Add_Parameter("@intImportFileID", _intImportFileID, NpgsqlTypes.NpgsqlDbType.BigInt))
                sqlParams.Add(SqlDAL.Add_Parameter("@numDomainID", _DomainId, NpgsqlTypes.NpgsqlDbType.BigInt))
                sqlParams.Add(SqlDAL.Add_Parameter("@tintMode", Mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                sqlParams.Add(SqlDAL.Add_Parameter("@numFormID", MasterID, NpgsqlTypes.NpgsqlDbType.BigInt))
                sqlParams.Add(SqlDAL.Add_Parameter("@vcFields", vcFields, NpgsqlTypes.NpgsqlDbType.Varchar))
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                dsResult = SqlDAL.ExecuteDataset(connString, SpName, sqlParams.ToArray())
            Catch ex As Exception
                dsResult = Nothing
                Throw ex
            End Try
            Return dsResult
        End Function

#End Region

#Region "GET TOP 1 ROW FROM Import_File_Master"

        Public Function GetImportFileID() As DataSet
            Dim dsResult As DataSet = Nothing
            Try
                Dim SpName As String = "USP_GET_TOP_IMPORT_FILE_ID"
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                dsResult = SqlDAL.ExecuteDataset(connString, SpName, arParms)
            Catch ex As Exception
                dsResult = Nothing
                Throw ex
            End Try
            Return dsResult
        End Function

#End Region

#Region "GET DATA FOR ROLLBACK"

        Public Function GetImportDataForRollBack() As DataSet
            Dim dsResult As DataSet = Nothing
            Try
                Dim SpName As String = "USP_GET_DATA_FOR_ROLLBACK"
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                dsResult = SqlDAL.ExecuteDataset(connString, SpName, sqlParams.ToArray())
            Catch ex As Exception
                dsResult = Nothing
                Throw ex
            End Try
            Return dsResult
        End Function

#End Region

#Region "CLASS NAME CONSTANTS"

        Public Const ITEM_TYPE_CLASS As String = "CItems"

#End Region

#Region "REFLECTION INDEXER"

        Private _indexedInstanceProperty As New Dictionary(Of Integer, String)
        Default Public Property IndexedInstanceProperty(ByVal key As Integer) As String
            Get
                Dim returnValue As String = Nothing
                If _indexedInstanceProperty.TryGetValue(key, returnValue) Then
                    Return returnValue
                Else
                    Return Nothing
                End If
            End Get
            Set(ByVal value As String)
                If value Is Nothing Then
                    Throw New ApplicationException( _
                        "IndexedInstanceProperty value can be the empty string, but it cannot be Nothing.")
                Else
                    If _indexedInstanceProperty.ContainsKey(key) Then
                        _indexedInstanceProperty(key) = value
                    Else
                        _indexedInstanceProperty.Add(key, value)
                    End If
                End If
            End Set
        End Property

#End Region

        Public Sub SaveImportFileReport(ByVal rowNumber As Integer, ByVal vcMessage As String, ByVal recordID As Long, ByVal errorMessage As String, ByVal stackStrace As String)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@intImportFileID", ImportFileID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@intRowNumber", rowNumber, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcMessage", vcMessage, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", recordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcErrorMessage", errorMessage, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcStackStrace", stackStrace, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ImportFileReport_Insert", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetMappedFieldDetails(ByVal vcFields As String) As DataTable
            Try
                Dim SpName As String = "USP_GET_IMPORT_MAPPED_FIELDS_DATA"
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                sqlParams.Add(SqlDAL.Add_Parameter("@numDomainID", _DomainId, NpgsqlTypes.NpgsqlDbType.BigInt))
                sqlParams.Add(SqlDAL.Add_Parameter("@numFormID", _lngMasterID, NpgsqlTypes.NpgsqlDbType.BigInt))
                sqlParams.Add(SqlDAL.Add_Parameter("@vcFields", vcFields, NpgsqlTypes.NpgsqlDbType.Varchar))
                sqlParams.Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))

                Return SqlDAL.ExecuteDatable(connString, SpName, sqlParams.ToArray())

            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

End Namespace
