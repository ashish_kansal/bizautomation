﻿Imports Infragistics.WebUI.UltraWebTab
Imports BACRM.BusinessLogic
Public Class SubTabsManager
    Public Shared Sub ManageSubTabs(ByVal uwOppTab As UltraWebTab, ByVal DomainId As Long, ByVal ModuleId As Long)
        Dim objTabs As New Admin.Tabs
        Dim dtblSubTab As DataTable
        objTabs.DomainID = DomainId
        objTabs.ModuleId = ModuleId
        dtblSubTab = objTabs.GetSubTabsControlInformation()
        If Not dtblSubTab Is Nothing Then
            Dim intCounter As Integer = dtblSubTab.Rows.Count
            If uwOppTab.Tabs.Count < intCounter Then
                intCounter = uwOppTab.Tabs.Count
            End If
            Dim i As Integer
            For i = 0 To intCounter - 1
                uwOppTab.Tabs(i).Text = dtblSubTab.Rows(i)("numTabName").ToString()
                If Not dtblSubTab.Rows(i)("IsVisible") Then
                    uwOppTab.Tabs(i).Visible = False
                End If
            Next
        End If
    End Sub
End Class
