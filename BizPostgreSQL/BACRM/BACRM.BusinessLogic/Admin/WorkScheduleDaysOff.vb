﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin

    Public Class WorkScheduleDaysOff
        Inherits UserAccess

#Region "Public Properties"

        Public Property WorkScheduleID As Long
        Public Property DaysOffFrom As Date
        Public Property DaysOffTo As Date

#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWorkScheduleID", WorkScheduleID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtDayOffFrom", DaysOffFrom, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@dtDayOffTo", DaysOffTo, NpgsqlTypes.NpgsqlDbType.Date))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_WorkScheduleDaysOff_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numWorkScheduleID", WorkScheduleID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_WorkScheduleDaysOff_Delete", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class

End Namespace