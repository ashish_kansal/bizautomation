﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Admin

    Public Class UniversalSMTPIMAP
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property SMTPServerURL As String
        Public Property SMTPPort As Integer
        Public Property IsSMPSSL As Boolean
        Public Property IsSMTPUserAuthentication As Boolean
        Public Property IMAPServerURL As String
        Public Property IMAPPort As Integer
        Public Property IsIMAPSSL As Boolean
        Public Property IsIMAPUserAuthentication As Boolean

#End Region

#Region "Public Methods"

        Public Function GetByDomain() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_UniversalSMTPIMAP_Get", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub Save()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSMTPServer", SMTPServerURL, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numSMTPPort", SMTPPort, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitSMTPSSL", IsSMPSSL, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitSMTPAuth", IsSMTPUserAuthentication, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcIMAPServer", IMAPServerURL, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numIMAPPort", IMAPPort, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitIMAPSSL", IsIMAPSSL, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitIMAPAuth", IsIMAPUserAuthentication, NpgsqlTypes.NpgsqlDbType.Bit))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UniversalSMTPIMAP_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class

End Namespace