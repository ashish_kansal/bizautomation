''' -----------------------------------------------------------------------------
''' Project	 : BACRM.BusinessLogic
''' Class	 : UserGroups
''' 
''' -----------------------------------------------------------------------------
''' <summary>
''' This class has several propeties and methods.  These are called from the 
''' Presentation layer.
''' </summary>
''' <remarks>
''' This Class is used to provide the functionality to access the user rignts.
''' </remarks>
''' ''' <history>
''' 	[Maha]	16/06/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Option Explicit On 
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Admin

    Public Class UserGroups
        Inherits BACRM.BusinessLogic.CBusinessBase

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Domain Id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	19/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the User ID.
        ''' </summary>
        ''' <remarks>
        '''     This is used to stored the user id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _UserId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Module Id.
        ''' </summary>
        ''' <remarks>
        '''     This is used to store the Module Id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ModuleId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Group Id.
        ''' </summary>
        ''' <remarks>
        '''     This is used to store Group Id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _GroupId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Group Name.
        ''' </summary>
        ''' <remarks>
        '''     This is used to store the Group name.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _GroupName As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Page Id.
        ''' </summary>
        ''' <remarks>
        '''     This is used to store the Page id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _PageId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Column Name.
        ''' </summary>
        ''' <remarks>
        '''     This is used to hold the column name.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ColumnName As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Column value.
        ''' </summary>
        ''' <remarks>
        '''     This is to hold the Column value.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ColumnValue As Integer
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the BitStatus.
        ''' </summary>
        ''' <remarks>
        '''     This is to hold the Bit status.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _BitStatus As String

        Private _GroupType As Short
        Private _strTabs As String
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _CompanyName As String
        Private _UserName As String
        Private _DivisionID As Long
        Private _ExtranetID As Long
        Private _Password As String
        Private _strEmail As String
        Private _RelationShip As Long
        'Private _PartnerGroup As Long
        Private _Filter As Short
        Private _ContactID As Long

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Private _Type As Short
        Public Property Type() As Short
            Get
                Return _Type
            End Get
            Set(ByVal value As Short)
                _Type = value
            End Set
        End Property

        Private _LocationID As Long
        Public Property LocationID() As Long
            Get
                Return _LocationID
            End Get
            Set(ByVal value As Long)
                _LocationID = value
            End Set
        End Property


        Private _ProfileID As Long
        Public Property ProfileID() As Long
            Get
                Return _ProfileID
            End Get
            Set(ByVal value As Long)
                _ProfileID = value
            End Set
        End Property

        Public Property Filter() As Short
            Get
                Return _Filter
            End Get
            Set(ByVal Value As Short)
                _Filter = Value
            End Set
        End Property

        'Public Property PartnerGroup() As Long
        '    Get
        '        Return _PartnerGroup
        '    End Get
        '    Set(ByVal Value As Long)
        '        _PartnerGroup = Value
        '    End Set
        'End Property

        Public Property RelationShip() As Long
            Get
                Return _RelationShip
            End Get
            Set(ByVal Value As Long)
                _RelationShip = Value
            End Set
        End Property

        Public Property strEmail() As String
            Get
                Return _strEmail
            End Get
            Set(ByVal Value As String)
                _strEmail = Value
            End Set
        End Property

        Public Property Password() As String
            Get
                Return _Password
            End Get
            Set(ByVal Value As String)
                _Password = Value
            End Set
        End Property

        Public Property ExtranetID() As Long
            Get
                Return _ExtranetID
            End Get
            Set(ByVal Value As Long)
                _ExtranetID = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property UserName() As String
            Get
                Return _UserName
            End Get
            Set(ByVal Value As String)
                _UserName = Value
            End Set
        End Property

        Public Property CompanyName() As String
            Get
                Return _CompanyName
            End Get
            Set(ByVal Value As String)
                _CompanyName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Property strTabs() As String
            Get
                Return _strTabs
            End Get
            Set(ByVal Value As String)
                _strTabs = Value
            End Set
        End Property

        Public Property GroupType() As Short
            Get
                Return _GroupType
            End Get
            Set(ByVal Value As Short)
                _GroupType = Value
            End Set
        End Property


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the user id.
        ''' </summary>
        ''' <value>Returns the value in long.</value>
        ''' <remarks>
        '''     This is used to Set and Get the User id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	14/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property UserId() As Long
            Get
                Return _UserId
            End Get
            Set(ByVal Value As Long)
                _UserId = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Module id.
        ''' </summary>
        ''' <value>Returns the Module id as long.</value>
        ''' <remarks>
        '''     This is to Set and Get the Module id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	14/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ModuleId() As Long
            Get
                Return _ModuleId
            End Get
            Set(ByVal Value As Long)
                _ModuleId = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Group Id.
        ''' </summary>
        ''' <value> Returns the Group id in long.</value>
        ''' <remarks>
        '''     This is to Set and Get the Group Id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	14/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property GroupId() As Long
            Get
                Return _GroupId
            End Get
            Set(ByVal Value As Long)
                _GroupId = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Group Name.
        ''' </summary>
        ''' <value>Returns the Group name in string.</value>
        ''' <remarks>
        '''     This is to Set and Get the Group Name.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property GroupName() As String
            Get
                Return _GroupName
            End Get
            Set(ByVal Value As String)
                _GroupName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Page Id.
        ''' </summary>
        ''' <value>Returns the Page id as long.</value>
        ''' <remarks>
        '''     This is to Set and Get the Page id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property PageId() As Long
            Get
                Return _PageId
            End Get
            Set(ByVal Value As Long)
                _PageId = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain Id.
        ''' </summary>
        ''' <value>Returns the value as long.</value>
        ''' <remarks>
        '''     This represents the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	19/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Column name.
        ''' </summary>
        ''' <value>Returns the Column name as string.</value>
        ''' <remarks>
        '''     This is to Set and Get the Column name.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ColumnName() As String
            Get
                Return _ColumnName
            End Get
            Set(ByVal Value As String)
                _ColumnName = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Column value.
        ''' </summary>
        ''' <value>Returns the Column value as Integer.</value>
        ''' <remarks>
        '''     This is to Set and Get the Column value.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ColumnValue() As Integer
            Get
                Return _ColumnValue
            End Get
            Set(ByVal Value As Integer)
                _ColumnValue = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Bit Status.
        ''' </summary>
        ''' <value>Returns the BitStaus value as string.</value>
        ''' <remarks>
        '''     This is to Set and Get the Bit status.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property BitStatus() As String
            Get
                Return _BitStatus
            End Get
            Set(ByVal Value As String)
                _BitStatus = Value
            End Set
        End Property

        Public Property AccessAllowed As Short
        Public Property SelectedGroupTypes As String
        Public Property CustomSearch As String
        '#Region "Constructor"
        '        ''' -----------------------------------------------------------------------------
        '        ''' <summary>
        '        ''' 
        '        ''' </summary>
        '        ''' <param name="intUserId"></param>
        '        ''' <remarks>
        '        ''' </remarks>
        '        ''' <history>
        '        ''' 	[administrator]	14/06/2005	Created
        '        ''' </history>
        '        ''' -----------------------------------------------------------------------------
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub
        '        ''' -----------------------------------------------------------------------------
        '        ''' <summary>
        '        ''' 
        '        ''' </summary>
        '        ''' <remarks>
        '        ''' </remarks>
        '        ''' <history>
        '        ''' 	[administrator]	14/06/2005	Created
        '        ''' </history>
        '        ''' -----------------------------------------------------------------------------
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to get all the System modules.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function is used to return all the System modules, and it
        ''' accepts the input parameter Moduleid.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	14/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetAllSystemModules() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintGroupType", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = _GroupType

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAllSystemModules", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function




        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method returns the page level access.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        '''     This function accepts two parameters Module Id and Group id and
        ''' it returns the Module pages and accesses.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	14/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetAllSelectedSystemModulesPagesAndAccesses() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ModuleId

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _GroupId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAllSelectedSystemModulesPagesAndAccesses", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This deletes the System Group.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>
        '''     This function is used to delete the records of System group.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	14/06/2005	Created
        ''' </history>
        '''         ''' -----------------------------------------------------------------------------
        Public Function DeleteSystemGroup() As Integer
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupId

                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteSystemGroup", arParms)
                Return 1
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        ''' This method is used to delete the record from ModuleMaster.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>
        '''     This function is used to delete the System module.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	14/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function DeleteSystemModule() As Integer
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ModuleId

                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteSystemModule", arParms)
                Return 1
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This sets the authorization group name.
        ''' </summary>
        ''' <returns>Returns the value as Npgsql.NpgsqlDataReader.</returns>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function SetAuthorizationGroupName() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcGroupName", NpgsqlTypes.NpgsqlDbType.VarChar, 30)
                arParms(1).Value = _GroupName

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_SetAuthorizationGroupName", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This selects all the System Modules and its accesses.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>
        '''     This function is used to return all the System modules
        ''' and accesses and it accepts five parameters Module id,Group id,
        ''' Page id, Column name and column value.
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	16/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function SetAllSelectedSystemModulesPagesAndAccesses() As Integer
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ModuleId

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _GroupId

                arParms(2) = New Npgsql.NpgsqlParameter("@numPageId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _PageId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcColumnName", NpgsqlTypes.NpgsqlDbType.VarChar, 30)
                arParms(3).Value = _ColumnName

                arParms(4) = New Npgsql.NpgsqlParameter("@vcColumnValue", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(4).Value = _ColumnValue

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "usp_SetAllSelectedSystemModulesPagesAndAccesses", arParms)
                Return 1
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Used to create a new authorization group name
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Maha]	25/06/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function CreateAuthorizationGroupName() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcGroupName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = _GroupName


                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "usp_CreateAuthorizationGroupName", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetAutorizationGroup() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcGroupTypes", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = SelectedGroupTypes

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAuthorizationGroup", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageAuthorizationGroups() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _GroupId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcGroupName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _GroupName

                arParms(3) = New Npgsql.NpgsqlParameter("@tinTGroupType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _GroupType

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageAuthorizationGroup", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteAuthorizationGroups() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _GroupId


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteAuthorizationGroup", arParms)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function GetTabsForAuthorization() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRelationShip", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RelationShip

                arParms(2) = New Npgsql.NpgsqlParameter("@numProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ProfileID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetTabForAuthorization", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageTabsAuthorization() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@strTabs", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strTabs

                arParms(2) = New Npgsql.NpgsqlParameter("@numRelationships", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RelationShip

                arParms(3) = New Npgsql.NpgsqlParameter("@numProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProfileID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _Type

                arParms(5) = New Npgsql.NpgsqlParameter("@Loc_Id", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _LocationID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageTabs", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetUserList() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@columnName", _ColumnName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcCustomSearch", CustomSearch, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_UserList", sqlParams.ToArray())

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    _TotalRecords = Convert.ToInt32(dt.Rows(0)("numTotalRecords"))
                Else
                    _TotalRecords = 0
                End If

                Return dt
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetUserListAuthorisedApprovers() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@UserName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = _UserName

                arParms(1) = New Npgsql.NpgsqlParameter("@tintGroupType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _GroupType

                arParms(2) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _SortCharacter

                arParms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _TotalRecords

                arParms(6) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _ColumnName

                arParms(7) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(7).Value = _columnSortOrder

                arParms(8) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = DomainID

                arParms(9) = New Npgsql.NpgsqlParameter("@Status", NpgsqlTypes.NpgsqlDbType.VarChar, 1)
                arParms(9).Value = _BitStatus

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_UserListAuthorisedApprovers", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetExtranetCompany() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@columnName", _ColumnName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcCustomSearch", CustomSearch, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_ExtranetCompanies", sqlParams.ToArray())

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    _TotalRecords = Convert.ToInt32(dt.Rows(0)("numTotalRecords"))
                Else
                    _TotalRecords = 0
                End If

                Return dt
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetExtranetDetails() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numExtranetID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ExtranetID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ExtranetDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ManageExtranet() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numExtranetID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ExtranetID

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _GroupId

                arParms(2) = New Npgsql.NpgsqlParameter("@strEmail", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _strEmail

                'arParms(3) = New Npgsql.NpgsqlParameter("@numPartnerGroup", NpgsqlTypes.NpgsqlDbType.BigInt)
                'arParms(3).Value = _PartnerGroup

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageExtranetAccounts", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CreateExtranetAccount() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcPassword", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(1).Value = _Password

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintAccessAllowed", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = AccessAllowed

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_CreateExtranetAccountforContact", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBackOrderSelectedAutorizationGroup() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBackOrderSelectedAuthGroup", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveBackOrderAuthgroup(ByVal strAuthGroupID As String) As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strAuthGroupID", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = strAuthGroupID

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveBackOrderAuthGroup", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class

End Namespace