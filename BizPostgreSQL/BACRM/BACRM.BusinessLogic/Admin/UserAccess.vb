'Created Anoop Jayaraj
Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin

    Public Class UserAccess
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _UserId As Long
        Private _ContactID As Long
        Private _UserName As String
        Private _UserDesc As String
        Private _GroupID As Long
        'Private UserCntID As Long
        Private _StrTerritory As String
        'Private DomainId As Long
        Private _strTeam As String
        Private _GroupType As Short
        Private _Password As String
        Private _Email As String
        Private _NewPassword As String
        Private _TerritoryID As Long
        Private _byteMode As Short
        Private _TerritoryName As String
        Private _CreateOppStatus As Short
        Private _Status As Long
        Private _Relationship As Long
        Private _CreateBizDoc As Short
        Private _BizDocName As Long
        Private _AccessAllowed As Short
        Private _PageAfterSuccReg As String
        Private _PageAfterUnSuccReg As String
        Private _Country As Long
        Private _StateID As Long
        Private _StateName As String
        Private _strUsers As String
        Private _NoOfRows As Short
        Private _CompanyID As Long
        Private _DivisionID As Long
        Private _RelID As Long
        Private _ProfileID As Long
        Private _RelProID As Long
        Private _FollowID As Long
        Private _RelFollowID As Long
        Private _bitHourlyRate As Boolean
        Private _HourlyRate As Decimal
        Private _bitSalary As Boolean
        Private _DailyHours As Double
        Private _bitOverTime As Boolean
        Private _LimDailyHrs As Double
        Private _OverTimeRate As Decimal
        Private _bitMainComm As Boolean
        Private _MainCommPer As Decimal
        Private _bitRoleComm As Boolean
        Private _Roles As String
        Private _strEmail As String
        Private _ExcIntegration As Boolean
        Private _AccessMethod As Boolean
        Private _ExchUserName As String
        Private _ExchPassword As String
        Private _ExchPath As String
        Private _ExchDomain As String
        Private _DomainName As String
        Private _DomainDesc As String
        Private _ImagePath As String
        Private _CustomPagingRows As Short
        Private _DateFormat As String
        Private _DefCountry As Long
        Private _ComposeWindow As Short
        Private _StartDate As Short
        Private _IntervalDays As Short
        Private _PopulateUserCriteria As Short
        Private _bitIntMedPage As Boolean
        Private _FiscalStartMonth As Short
        Private _StrDivision As String
        Private _RecID As Long
        Private _BizDocImagePath As String
        Private _CompanyImagePath As String
        Private _BankImagePath As String
        'Private _DeferredIncome As Boolean
        Private _PayPeriod As Short
        Private _bitSMTPServer As Boolean
        Private _SMTPServer As String
        Private _SMTPPort As Integer
        Private _CurrencyID As Long
        Private _PaymentGateWay As Integer
        Private _PGWUserId As String
        Private _PGWPassword As String
        Private _boolShowOnhand As Boolean
        Private _boolShowInStock As Boolean
        Private _GoogleMerchantID As String
        Private _GoogleMerchantKey As String
        Private _IsSandbox As Boolean
        Private _chrUnitSystem As Char
        'Private _ItemColumns As Short
        Private _xmlStr As String
        Private _StyleSheetpath As String
        Private _PortalLogo As String
        Private _ChrForComSearch As String
        'Private _Category As Long
        Private _boolHideNewUsers As Boolean
        Private _ShipCompany As Long
        Private _boolCheckCreditStatus As Boolean
        Private _boolAutoPopulateAddress As Boolean
        Private _PoulateAddressTo As Short
        Private _boolMultiCurrency As Boolean
        'Private _boolMultiCompany As Boolean
        Private _boolUseUserName As Boolean
        Private _boolAmountPastDue As Boolean
        Private _AmountPastDue As Decimal
        Private _Class As Long
        Private _DefaultWarehouse As Long
        Private _AssetAccountID As Long
        Private _numCost As Integer
        Private _bitApprovalforTImeExpense As Boolean
        Private _bitApprovalforOpportunity As Boolean
        Private _numDefaultSalesShippingDoc As Long
        Private _numDefaultPurchaseShippingDoc As Long
        Private _bitMarginPriceViolated As Boolean
        Private _vcPreSellUp As String
        Private _vcPostSellUp As String
        Private _vcAbbreviations As String
        Private _bitchkOverRideAssignto As Boolean
        Private _vcRedirectThankYouUrl As String
        Private _numAuthorizePercentage As Long


        Private _bitDisplayCustomField As Boolean
        Private _bitFollowupAnytime As Boolean
        Private _bitpartycalendarTitle As Boolean
        Private _bitpartycalendarLocation As Boolean
        Private _bitpartycalendarDescription As Boolean
        Private _bitREQPOApproval As Boolean
        Private _bitARInvoiceDue As Boolean
        Private _bitAPBillsDue As Boolean
        Private _bitItemsToPickPackShip As Boolean
        Private _bitItemsToInvoice As Boolean
        Private _bitSalesOrderToClose As Boolean
        Private _bitItemsToPutAway As Boolean
        Private _bitItemsToBill As Boolean
        Private _bitPosToClose As Boolean
        Private _bitPOToClose As Boolean
        Private _bitBOMSToPick As Boolean

        Private _vchREQPOApprovalEmp As String
        Private _vchARInvoiceDue As String
        Private _vchAPBillsDue As String
        Private _vchItemsToPickPackShip As String
        Private _vchItemsToInvoice As String
        Private _vchPriceMarginApproval As String
        Private _vchSalesOrderToClose As String
        Private _vchItemsToPutAway As String
        Private _vchItemsToBill As String
        Private _vchPosToClose As String
        Private _vchPOToClose As String
        Private _vchBOMSToPick As String
        Private _decReqPOMinValue As Long
        Private _bitUseOnlyActionItems As Boolean
        Private _bitDisplayContractElement As Boolean
        Private _vcEmployeeForContractTimeElement As String




        Public Property bitDisplayContractElement As Boolean
            Get
                Return _bitDisplayContractElement
            End Get
            Set(ByVal value As Boolean)
                _bitDisplayContractElement = value
            End Set
        End Property

        Public Property vcEmployeeForContractTimeElement As String
            Get
                Return _vcEmployeeForContractTimeElement
            End Get
            Set(ByVal value As String)
                _vcEmployeeForContractTimeElement = value
            End Set
        End Property


        Public Property bitUseOnlyActionItems As Boolean
            Get
                Return _bitUseOnlyActionItems
            End Get
            Set(ByVal value As Boolean)
                _bitUseOnlyActionItems = value
            End Set
        End Property


        Public Property bitDisplayCustomField As Boolean
            Get
                Return _bitDisplayCustomField
            End Get
            Set(ByVal value As Boolean)
                _bitDisplayCustomField = value
            End Set
        End Property

        Public Property bitFollowupAnytime As Boolean
            Get
                Return _bitFollowupAnytime
            End Get
            Set(ByVal value As Boolean)
                _bitFollowupAnytime = value
            End Set
        End Property
        Public Property bitpartycalendarTitle As Boolean
            Get
                Return _bitpartycalendarTitle
            End Get
            Set(ByVal value As Boolean)
                _bitpartycalendarTitle = value
            End Set
        End Property
        Public Property bitpartycalendarLocation As Boolean
            Get
                Return _bitpartycalendarLocation
            End Get
            Set(ByVal value As Boolean)
                _bitpartycalendarLocation = value
            End Set
        End Property
        Public Property bitpartycalendarDescription As Boolean
            Get
                Return _bitpartycalendarDescription
            End Get
            Set(ByVal value As Boolean)
                _bitpartycalendarDescription = value
            End Set
        End Property
        Public Property bitREQPOApproval As Boolean
            Get
                Return _bitREQPOApproval
            End Get
            Set(ByVal value As Boolean)
                _bitREQPOApproval = value
            End Set
        End Property
        Public Property bitARInvoiceDue As Boolean
            Get
                Return _bitARInvoiceDue
            End Get
            Set(ByVal value As Boolean)
                _bitARInvoiceDue = value
            End Set
        End Property
        Public Property bitAPBillsDue As Boolean
            Get
                Return _bitAPBillsDue
            End Get
            Set(ByVal value As Boolean)
                _bitAPBillsDue = value
            End Set
        End Property
        Public Property bitItemsToPickPackShip As Boolean
            Get
                Return _bitItemsToPickPackShip
            End Get
            Set(ByVal value As Boolean)
                _bitItemsToPickPackShip = value
            End Set
        End Property
        Public Property bitItemsToInvoice As Boolean
            Get
                Return _bitItemsToInvoice
            End Get
            Set(ByVal value As Boolean)
                _bitItemsToInvoice = value
            End Set
        End Property
        Public Property bitSalesOrderToClose As Boolean
            Get
                Return _bitSalesOrderToClose
            End Get
            Set(ByVal value As Boolean)
                _bitSalesOrderToClose = value
            End Set
        End Property
        Public Property bitItemsToPutAway As Boolean
            Get
                Return _bitItemsToPutAway
            End Get
            Set(ByVal value As Boolean)
                _bitItemsToPutAway = value
            End Set
        End Property
        Public Property bitItemsToBill As Boolean
            Get
                Return _bitItemsToBill
            End Get
            Set(ByVal value As Boolean)
                _bitItemsToBill = value
            End Set
        End Property
        Public Property bitPosToClose As Boolean
            Get
                Return _bitPosToClose
            End Get
            Set(ByVal value As Boolean)
                _bitPosToClose = value
            End Set
        End Property
        Public Property bitPOToClose As Boolean
            Get
                Return _bitPOToClose
            End Get
            Set(ByVal value As Boolean)
                _bitPOToClose = value
            End Set
        End Property
        Public Property bitBOMSToPick As Boolean
            Get
                Return _bitBOMSToPick
            End Get
            Set(ByVal value As Boolean)
                _bitBOMSToPick = value
            End Set
        End Property

        Public Property vchREQPOApprovalEmp As String
            Get
                Return _vchREQPOApprovalEmp
            End Get
            Set(ByVal value As String)
                _vchREQPOApprovalEmp = value
            End Set
        End Property
        Public Property vchARInvoiceDue As String
            Get
                Return _vchARInvoiceDue
            End Get
            Set(ByVal value As String)
                _vchARInvoiceDue = value
            End Set
        End Property
        Public Property vchAPBillsDue As String
            Get
                Return _vchAPBillsDue
            End Get
            Set(ByVal value As String)
                _vchAPBillsDue = value
            End Set
        End Property
        Public Property vchItemsToPickPackShip As String
            Get
                Return _vchItemsToPickPackShip
            End Get
            Set(ByVal value As String)
                _vchItemsToPickPackShip = value
            End Set
        End Property
        Public Property vchItemsToInvoice As String
            Get
                Return _vchItemsToInvoice
            End Get
            Set(ByVal value As String)
                _vchItemsToInvoice = value
            End Set
        End Property
        Public Property vchPriceMarginApproval As String
            Get
                Return _vchPriceMarginApproval
            End Get
            Set(ByVal value As String)
                _vchPriceMarginApproval = value
            End Set
        End Property
        Public Property vchSalesOrderToClose As String
            Get
                Return _vchSalesOrderToClose
            End Get
            Set(ByVal value As String)
                _vchSalesOrderToClose = value
            End Set
        End Property
        Public Property vchItemsToPutAway As String
            Get
                Return _vchItemsToPutAway
            End Get
            Set(ByVal value As String)
                _vchItemsToPutAway = value
            End Set
        End Property
        Public Property vchItemsToBill As String
            Get
                Return _vchItemsToBill
            End Get
            Set(ByVal value As String)
                _vchItemsToBill = value
            End Set
        End Property
        Public Property vchPosToClose As String
            Get
                Return _vchPosToClose
            End Get
            Set(ByVal value As String)
                _vchPosToClose = value
            End Set
        End Property
        Public Property vchPOToClose As String
            Get
                Return _vchPOToClose
            End Get
            Set(ByVal value As String)
                _vchPOToClose = value
            End Set
        End Property
        Public Property vchBOMSToPick As String
            Get
                Return _vchBOMSToPick
            End Get
            Set(ByVal value As String)
                _vchBOMSToPick = value
            End Set
        End Property

        Public Property decReqPOMinValue As Long
            Get
                Return _decReqPOMinValue
            End Get
            Set(ByVal value As Long)
                _decReqPOMinValue = value
            End Set
        End Property

        Public Property vcRedirectThankYouUrl As String
            Get
                Return _vcRedirectThankYouUrl
            End Get
            Set(ByVal value As String)
                _vcRedirectThankYouUrl = value
            End Set
        End Property

        Public Property numAuthorizePercentage As Long
            Get
                Return _numAuthorizePercentage
            End Get
            Set(ByVal value As Long)
                _numAuthorizePercentage = value
            End Set
        End Property

        Public Property vcPreSellUp As String
            Get
                Return _vcPreSellUp
            End Get
            Set(ByVal value As String)
                _vcPreSellUp = value
            End Set
        End Property
        Public Property vcAbbreviations As String
            Get
                Return _vcAbbreviations
            End Get
            Set(ByVal value As String)
                _vcAbbreviations = value
            End Set
        End Property
        Public Property vcPostSellUp As String
            Get
                Return _vcPostSellUp
            End Get
            Set(ByVal value As String)
                _vcPostSellUp = value
            End Set
        End Property

        Public Property bitMarginPriceViolated As Boolean
            Get
                Return _bitMarginPriceViolated
            End Get
            Set(ByVal value As Boolean)
                _bitMarginPriceViolated = value
            End Set
        End Property
        Public Property numDefaultSalesShippingDoc As Long
            Get
                Return _numDefaultSalesShippingDoc
            End Get
            Set(ByVal value As Long)
                _numDefaultSalesShippingDoc = value
            End Set
        End Property

        Public Property numDefaultPurchaseShippingDoc As Long
            Get
                Return _numDefaultPurchaseShippingDoc
            End Get
            Set(ByVal value As Long)
                _numDefaultPurchaseShippingDoc = value
            End Set
        End Property

        Public Property numCost As Integer
            Get
                Return _numCost
            End Get
            Set(ByVal value As Integer)
                _numCost = value
            End Set
        End Property

        Public Property bitchkOverRideAssignto() As Boolean
            Get
                Return _bitchkOverRideAssignto
            End Get
            Set(ByVal value As Boolean)
                _bitchkOverRideAssignto = value
            End Set
        End Property

        Public Property bitApprovalforTImeExpense() As Boolean
            Get
                Return _bitApprovalforTImeExpense
            End Get
            Set(ByVal value As Boolean)
                _bitApprovalforTImeExpense = value
            End Set
        End Property

        Public Property AssetAccountID() As Long
            Get
                Return _AssetAccountID
            End Get
            Set(ByVal value As Long)
                _AssetAccountID = value
            End Set
        End Property

        Private _COGSAccountID As Long
        Public Property COGSAccountID() As Long
            Get
                Return _COGSAccountID
            End Get
            Set(ByVal value As Long)
                _COGSAccountID = value
            End Set
        End Property


        Private _IncomeAccountID As Long
        Public Property IncomeAccountID() As Long
            Get
                Return _IncomeAccountID
            End Get
            Set(ByVal value As Long)
                _IncomeAccountID = value
            End Set
        End Property

        Public Property DefaultWarehouse() As Long
            Get
                Return _DefaultWarehouse
            End Get
            Set(ByVal value As Long)
                _DefaultWarehouse = value
            End Set
        End Property

        Public Property DefaultClass() As Long
            Get
                Return _Class
            End Get
            Set(ByVal value As Long)
                _Class = value
            End Set
        End Property

        Private _strItems As String
        Public Property StrItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal value As String)
                _strItems = value
            End Set
        End Property

        Public Property boolUseUserName() As Boolean
            Get
                Return _boolUseUserName
            End Get
            Set(ByVal Value As Boolean)
                _boolUseUserName = Value
            End Set
        End Property

        Private _EnableHidePrice As Boolean
        Public Property HidePrice() As Boolean
            Get
                Return _EnableHidePrice
            End Get
            Set(ByVal value As Boolean)
                _EnableHidePrice = value
            End Set
        End Property

        Private _EnableHideAddtoCart As Boolean
        Public Property HideAddtoCart() As Boolean
            Get
                Return _EnableHideAddtoCart
            End Get
            Set(ByVal value As Boolean)
                _EnableHideAddtoCart = value
            End Set
        End Property

        Private _BizDocForCreditTerm As Long
        Public Property BizDocForCreditTerm() As Long
            Get
                Return _BizDocForCreditTerm
            End Get
            Set(ByVal value As Long)
                _BizDocForCreditTerm = value
            End Set
        End Property

        Private _BizDocForCreditCard As Long
        Public Property BizDocForCreditCard() As Long
            Get
                Return _BizDocForCreditCard
            End Get
            Set(ByVal value As Long)
                _BizDocForCreditCard = value
            End Set
        End Property

        Private _AuthOnlyCreditCard As Boolean
        Public Property AuthOnlyCreditCard() As Boolean
            Get
                Return _AuthOnlyCreditCard
            End Get
            Set(ByVal value As Boolean)
                _AuthOnlyCreditCard = value
            End Set
        End Property


        Private _boolPoral As Boolean
        Public Property boolPortal() As Boolean
            Get
                Return _boolPoral
            End Get
            Set(ByVal value As Boolean)
                _boolPoral = value
            End Set
        End Property


        Private _EnableEmbeddedCost As Boolean
        Public Property EnableEmbeddedCost() As Boolean
            Get
                Return _EnableEmbeddedCost
            End Get
            Set(ByVal value As Boolean)
                _EnableEmbeddedCost = value
            End Set
        End Property

        'Public Property boolMultiCompany() As Boolean
        '    Get
        '        Return _boolMultiCompany
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        _boolMultiCompany = Value
        '    End Set
        'End Property

        Public Property boolMultiCurrency() As Boolean
            Get
                Return _boolMultiCurrency
            End Get
            Set(ByVal Value As Boolean)
                _boolMultiCurrency = Value
            End Set
        End Property

        Public Property PoulateAddressTo() As Short
            Get
                Return _PoulateAddressTo
            End Get
            Set(ByVal Value As Short)
                _PoulateAddressTo = Value
            End Set
        End Property
        Public Property boolAutoPopulateAddress() As Boolean
            Get
                Return _boolAutoPopulateAddress
            End Get
            Set(ByVal Value As Boolean)
                _boolAutoPopulateAddress = Value
            End Set
        End Property

        Private _CreateInvoice As Boolean
        Public Property CreateInvoice() As Boolean
            Get
                Return _CreateInvoice
            End Get
            Set(ByVal value As Boolean)
                _CreateInvoice = value
            End Set
        End Property

        Private _DefaultSalesBizDocID As Long
        Public Property DefaultSalesBizDocID() As Long
            Get
                Return _DefaultSalesBizDocID
            End Get
            Set(ByVal value As Long)
                _DefaultSalesBizDocID = value
            End Set
        End Property

        Private _DefaultSalesOppBizDocID As Long
        Public Property DefaultSalesOppBizDocID() As Long
            Get
                Return _DefaultSalesOppBizDocID
            End Get
            Set(ByVal value As Long)
                _DefaultSalesOppBizDocID = value
            End Set
        End Property


        Private _DefaultPurchaseBizDocID As Long
        Public Property DefaultPurchaseBizDocID() As Long
            Get
                Return _DefaultPurchaseBizDocID
            End Get
            Set(ByVal value As Long)
                _DefaultPurchaseBizDocID = value
            End Set
        End Property

        Private _KeepCreditCardInfo As Boolean
        Public Property KeepCreditCardInfo() As Boolean
            Get
                Return _KeepCreditCardInfo
            End Get
            Set(ByVal value As Boolean)
                _KeepCreditCardInfo = value
            End Set
        End Property


        Private _LastViewedCount As Integer
        Public Property LastViewedCount() As Integer
            Get
                Return _LastViewedCount
            End Get
            Set(ByVal value As Integer)
                _LastViewedCount = value
            End Set
        End Property


        Private _CommissionType As Short
        Public Property CommissionType() As Short
            Get
                Return _CommissionType
            End Get
            Set(ByVal value As Short)
                _CommissionType = value
            End Set
        End Property


        Private _BaseTax As Short
        Public Property BaseTax() As Short
            Get
                Return _BaseTax
            End Get
            Set(ByVal value As Short)
                _BaseTax = value
            End Set
        End Property

        Private _SessionTimeout As Short
        Public Property SessionTimeout() As Short
            Get
                Return _SessionTimeout
            End Get
            Set(ByVal value As Short)
                _SessionTimeout = value
            End Set
        End Property

        Private _ShipToDefault As Short
        Public Property ShipToDefault() As Short
            Get
                Return _ShipToDefault
            End Get
            Set(ByVal value As Short)
                _ShipToDefault = value
            End Set
        End Property
        Private _BillToDefault As Short
        Public Property BillToDefault() As Short
            Get
                Return _BillToDefault
            End Get
            Set(ByVal value As Short)
                _BillToDefault = value
            End Set
        End Property

        Private _boolCustomizePortal As Boolean
        Public Property boolCustomizePortal() As Boolean
            Get
                Return _boolCustomizePortal
            End Get
            Set(ByVal value As Boolean)
                _boolCustomizePortal = value
            End Set
        End Property

        Private _DecimalPoints As Short
        Public Property DecimalPoints() As Short
            Get
                Return _DecimalPoints
            End Get
            Set(ByVal value As Short)
                _DecimalPoints = value
            End Set
        End Property

        Public Property boolCheckCreditStatus() As Boolean
            Get
                Return _boolCheckCreditStatus
            End Get
            Set(ByVal Value As Boolean)
                _boolCheckCreditStatus = Value
            End Set
        End Property

        Private _boolAutoSelectWarehouse As Boolean
        Public Property boolAutoSelectWarehouse() As Boolean
            Get
                Return _boolAutoSelectWarehouse
            End Get
            Set(ByVal value As Boolean)
                _boolAutoSelectWarehouse = value
            End Set
        End Property

        Public Property ShipCompany() As Long
            Get
                Return _ShipCompany
            End Get
            Set(ByVal Value As Long)
                _ShipCompany = Value
            End Set
        End Property


        'Public Property boolHideNewUsers() As Boolean
        '    Get
        '        Return _boolHideNewUsers
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        _boolHideNewUsers = Value
        '    End Set
        'End Property


        'Public Property Category() As Long
        '    Get
        '        Return _Category
        '    End Get
        '    Set(ByVal Value As Long)
        '        _Category = Value
        '    End Set
        'End Property

        Private _WareHouseID As Long
        Public Property WareHouseID() As Long
            Get
                Return _WareHouseID
            End Get
            Set(ByVal value As Long)
                _WareHouseID = value
            End Set
        End Property

        Private _COGSChartAcntId As Long
        Public Property COGSChartAcntId() As Long
            Get
                Return _COGSChartAcntId
            End Get
            Set(ByVal value As Long)
                _COGSChartAcntId = value
            End Set
        End Property

        Private _AssetChartAcntId As Long
        Public Property AssetChartAcntId() As Long
            Get
                Return _AssetChartAcntId
            End Get
            Set(ByVal value As Long)
                _AssetChartAcntId = value
            End Set
        End Property

        Private _IncomeChartAcntId As Long
        Public Property IncomeChartAcntId() As Long
            Get
                Return _IncomeChartAcntId
            End Get
            Set(ByVal value As Long)
                _IncomeChartAcntId = value
            End Set
        End Property

        Public Property ChrForComSearch() As String
            Get
                Return _ChrForComSearch
            End Get
            Set(ByVal Value As String)
                _ChrForComSearch = Value
            End Set
        End Property

        Public Property PortalLogo() As String
            Get
                Return _PortalLogo
            End Get
            Set(ByVal Value As String)
                _PortalLogo = Value
            End Set
        End Property

        Public Property StyleSheetpath() As String
            Get
                Return _StyleSheetpath
            End Get
            Set(ByVal Value As String)
                _StyleSheetpath = Value
            End Set
        End Property

        Public Property xmlStr() As String
            Get
                Return _xmlStr
            End Get
            Set(ByVal Value As String)
                _xmlStr = Value
            End Set
        End Property
        'Public Property ItemColumns() As Short
        '    Get
        '        Return _ItemColumns
        '    End Get
        '    Set(ByVal value As Short)
        '        _ItemColumns = value
        '    End Set
        'End Property

        Public Property chrUnitSystem() As Char
            Get
                Return _chrUnitSystem
            End Get
            Set(ByVal value As Char)
                _chrUnitSystem = value
            End Set
        End Property

        Public Property boolShowInStock() As Boolean
            Get
                Return _boolShowInStock
            End Get
            Set(ByVal value As Boolean)
                _boolShowInStock = value
            End Set
        End Property

        Public Property GoogleMerchantID() As String
            Get
                Return _GoogleMerchantID
            End Get
            Set(ByVal value As String)
                _GoogleMerchantID = value
            End Set
        End Property
        Public Property GoogleMerchantKey() As String
            Get
                Return _GoogleMerchantKey
            End Get
            Set(ByVal value As String)
                _GoogleMerchantKey = value
            End Set
        End Property
        Public Property IsSandbox() As Boolean
            Get
                Return _IsSandbox
            End Get
            Set(ByVal value As Boolean)
                _IsSandbox = value
            End Set
        End Property
        Private _IsPaypalSandbox As Boolean
        Public Property IsPaypalSandbox() As Boolean
            Get
                Return _IsPaypalSandbox
            End Get
            Set(ByVal value As Boolean)
                _IsPaypalSandbox = value
            End Set
        End Property

        Public Property boolShowOnhand() As Boolean
            Get
                Return _boolShowOnhand
            End Get
            Set(ByVal value As Boolean)
                _boolShowOnhand = value
            End Set
        End Property

        Public Property PaymentGateWay() As Integer
            Get
                Return _PaymentGateWay
            End Get
            Set(ByVal value As Integer)
                _PaymentGateWay = value
            End Set
        End Property
        Public Property PGWUserId() As String
            Get
                Return _PGWUserId
            End Get
            Set(ByVal value As String)
                _PGWUserId = value
            End Set
        End Property
        Public Property PGWPassword() As String
            Get
                Return _PGWPassword
            End Get
            Set(ByVal value As String)
                _PGWPassword = value
            End Set
        End Property
        Public Property CurrencyID() As Long
            Get
                Return _CurrencyID
            End Get
            Set(ByVal value As Long)
                _CurrencyID = value
            End Set
        End Property

        Public Property SMTPServer() As String
            Get
                Return _SMTPServer
            End Get
            Set(ByVal value As String)
                _SMTPServer = value
            End Set
        End Property

        Public Property SMTPPort() As Integer
            Get
                Return _SMTPPort
            End Get
            Set(ByVal value As Integer)
                _SMTPPort = value
            End Set
        End Property

        Public Property bitSMTPServer() As Boolean
            Get
                Return _bitSMTPServer
            End Get
            Set(ByVal value As Boolean)
                _bitSMTPServer = value
            End Set
        End Property


        Public Property RecID() As Long
            Get
                Return _RecID
            End Get
            Set(ByVal Value As Long)
                _RecID = Value
            End Set
        End Property

        Public Property FiscalStartMonth() As Short
            Get
                Return _FiscalStartMonth
            End Get
            Set(ByVal Value As Short)
                _FiscalStartMonth = Value
            End Set
        End Property

        Public Property bitIntMedPage() As Boolean
            Get
                Return _bitIntMedPage
            End Get
            Set(ByVal Value As Boolean)
                _bitIntMedPage = Value
            End Set
        End Property

        Public Property PopulateUserCriteria() As Short
            Get
                Return _PopulateUserCriteria
            End Get
            Set(ByVal Value As Short)
                _PopulateUserCriteria = Value
            End Set
        End Property

        Public Property IntervalDays() As Short
            Get
                Return _IntervalDays
            End Get
            Set(ByVal Value As Short)
                _IntervalDays = Value
            End Set
        End Property

        Public Property StartDate() As Short
            Get
                Return _StartDate
            End Get
            Set(ByVal Value As Short)
                _StartDate = Value
            End Set
        End Property

        Public Property ComposeWindow() As Short
            Get
                Return _ComposeWindow
            End Get
            Set(ByVal Value As Short)
                _ComposeWindow = Value
            End Set
        End Property

        Public Property DefCountry() As Long
            Get
                Return _DefCountry
            End Get
            Set(ByVal Value As Long)
                _DefCountry = Value
            End Set
        End Property

        Public Property DateFormat() As String
            Get
                Return _DateFormat
            End Get
            Set(ByVal Value As String)
                _DateFormat = Value
            End Set
        End Property

        Public Property CustomPagingRows() As Short
            Get
                Return _CustomPagingRows
            End Get
            Set(ByVal Value As Short)
                _CustomPagingRows = Value
            End Set
        End Property


        Public Property DomainDesc() As String
            Get
                Return _DomainDesc
            End Get
            Set(ByVal Value As String)
                _DomainDesc = Value
            End Set
        End Property

        Public Property DomainName() As String
            Get
                Return _DomainName
            End Get
            Set(ByVal Value As String)
                _DomainName = Value
            End Set
        End Property

        Public Property ExchDomain() As String
            Get
                Return _ExchDomain
            End Get
            Set(ByVal Value As String)
                _ExchDomain = Value
            End Set
        End Property

        Public Property ExchPath() As String
            Get
                Return _ExchPath
            End Get
            Set(ByVal Value As String)
                _ExchPath = Value
            End Set
        End Property

        Public Property ExchPassword() As String
            Get
                Return _ExchPassword
            End Get
            Set(ByVal Value As String)
                _ExchPassword = Value
            End Set
        End Property

        Public Property ExchUserName() As String
            Get
                Return _ExchUserName
            End Get
            Set(ByVal Value As String)
                _ExchUserName = Value
            End Set
        End Property

        Public Property AccessMethod() As Boolean
            Get
                Return _AccessMethod
            End Get
            Set(ByVal Value As Boolean)
                _AccessMethod = Value
            End Set
        End Property

        Public Property ExcIntegration() As Boolean
            Get
                Return _ExcIntegration
            End Get
            Set(ByVal Value As Boolean)
                _ExcIntegration = Value
            End Set
        End Property

        Public Property strEmail() As String
            Get
                Return _strEmail
            End Get
            Set(ByVal Value As String)
                _strEmail = Value
            End Set
        End Property
        Private _strSubscriptionId As String

        Public Property strSubscriptionId() As String
            Get
                Return _strSubscriptionId
            End Get
            Set(ByVal Value As String)
                _strSubscriptionId = Value
            End Set
        End Property


        Public Property Roles() As String
            Get
                Return _Roles
            End Get
            Set(ByVal Value As String)
                _Roles = Value
            End Set
        End Property

        Public Property bitRoleComm() As Boolean
            Get
                Return _bitRoleComm
            End Get
            Set(ByVal Value As Boolean)
                _bitRoleComm = Value
            End Set
        End Property

        Public Property MainCommPer() As Decimal
            Get
                Return _MainCommPer
            End Get
            Set(ByVal Value As Decimal)
                _MainCommPer = Value
            End Set
        End Property

        Public Property bitMainComm() As Boolean
            Get
                Return _bitMainComm
            End Get
            Set(ByVal Value As Boolean)
                _bitMainComm = Value
            End Set
        End Property

        Public Property OverTimeRate() As Decimal
            Get
                Return _OverTimeRate
            End Get
            Set(ByVal Value As Decimal)
                _OverTimeRate = Value
            End Set
        End Property

        Public Property LimDailyHrs() As Double
            Get
                Return _LimDailyHrs
            End Get
            Set(ByVal Value As Double)
                _LimDailyHrs = Value
            End Set
        End Property

        Public Property bitOverTime() As Boolean
            Get
                Return _bitOverTime
            End Get
            Set(ByVal Value As Boolean)
                _bitOverTime = Value
            End Set
        End Property

        Public Property DailyHours() As Double
            Get
                Return _DailyHours
            End Get
            Set(ByVal Value As Double)
                _DailyHours = Value
            End Set
        End Property

        Public Property bitSalary() As Boolean
            Get
                Return _bitSalary
            End Get
            Set(ByVal Value As Boolean)
                _bitSalary = Value
            End Set
        End Property

        Public Property HourlyRate() As Decimal
            Get
                Return _HourlyRate
            End Get
            Set(ByVal Value As Decimal)
                _HourlyRate = Value
            End Set
        End Property

        Public Property bitHourlyRate() As Boolean
            Get
                Return _bitHourlyRate
            End Get
            Set(ByVal Value As Boolean)
                _bitHourlyRate = Value
            End Set
        End Property

        Public Property RelFollowID() As Long
            Get
                Return _RelFollowID
            End Get
            Set(ByVal Value As Long)
                _RelFollowID = Value
            End Set
        End Property

        Public Property FollowID() As Long
            Get
                Return _FollowID
            End Get
            Set(ByVal Value As Long)
                _FollowID = Value
            End Set
        End Property

        Public Property RelProID() As Long
            Get
                Return _RelProID
            End Get
            Set(ByVal Value As Long)
                _RelProID = Value
            End Set
        End Property

        Public Property ProfileID() As Long
            Get
                Return _ProfileID
            End Get
            Set(ByVal Value As Long)
                _ProfileID = Value
            End Set
        End Property

        Public Property RelID() As Long
            Get
                Return _RelID
            End Get
            Set(ByVal Value As Long)
                _RelID = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property CompanyID() As Long
            Get
                Return _CompanyID
            End Get
            Set(ByVal Value As Long)
                _CompanyID = Value
            End Set
        End Property

        Public Property NoOfRows() As Short
            Get
                Return _NoOfRows
            End Get
            Set(ByVal Value As Short)
                _NoOfRows = Value
            End Set
        End Property

        Public Property strUsers() As String
            Get
                Return _strUsers
            End Get
            Set(ByVal Value As String)
                _strUsers = Value
            End Set
        End Property

        Public Property StateName() As String
            Get
                Return _StateName
            End Get
            Set(ByVal Value As String)
                _StateName = Value
            End Set
        End Property

        Public Property StateID() As Long
            Get
                Return _StateID
            End Get
            Set(ByVal Value As Long)
                _StateID = Value
            End Set
        End Property

        Public Property Country() As Long
            Get
                Return _Country
            End Get
            Set(ByVal Value As Long)
                _Country = Value
            End Set
        End Property

        Public Property PageAfterUnSuccReg() As String
            Get
                Return _PageAfterUnSuccReg
            End Get
            Set(ByVal Value As String)
                _PageAfterUnSuccReg = Value
            End Set
        End Property

        Public Property PageAfterSuccReg() As String
            Get
                Return _PageAfterSuccReg
            End Get
            Set(ByVal Value As String)
                _PageAfterSuccReg = Value
            End Set
        End Property

        Public Property AccessAllowed() As Short
            Get
                Return _AccessAllowed
            End Get
            Set(ByVal Value As Short)
                _AccessAllowed = Value
            End Set
        End Property

        Public Property BizDocName() As Long
            Get
                Return _BizDocName
            End Get
            Set(ByVal Value As Long)
                _BizDocName = Value
            End Set
        End Property

        Public Property CreateBizDoc() As Short
            Get
                Return _CreateBizDoc
            End Get
            Set(ByVal Value As Short)
                _CreateBizDoc = Value
            End Set
        End Property

        Public Property Relationship() As Long
            Get
                Return _Relationship
            End Get
            Set(ByVal Value As Long)
                _Relationship = Value
            End Set
        End Property

        Public Property Status() As Long
            Get
                Return _Status
            End Get
            Set(ByVal Value As Long)
                _Status = Value
            End Set
        End Property

        Public Property CreateOppStatus() As Short
            Get
                Return _CreateOppStatus
            End Get
            Set(ByVal Value As Short)
                _CreateOppStatus = Value
            End Set
        End Property

        Public Property TerritoryName() As String
            Get
                Return _TerritoryName
            End Get
            Set(ByVal Value As String)
                _TerritoryName = Value
            End Set
        End Property

        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property

        Public Property NewPassword() As String
            Get
                Return _NewPassword
            End Get
            Set(ByVal Value As String)
                _NewPassword = Value
            End Set
        End Property

        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal Value As String)
                _Email = Value
            End Set
        End Property

        Public Property Password() As String
            Get
                Return _Password
            End Get
            Set(ByVal Value As String)
                _Password = Value
            End Set
        End Property

        Public Property GroupType() As Short
            Get
                Return _GroupType
            End Get
            Set(ByVal Value As Short)
                _GroupType = Value
            End Set
        End Property


        Public Property strTeam() As String
            Get
                Return _strTeam
            End Get
            Set(ByVal Value As String)
                _strTeam = Value
            End Set
        End Property


        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property


        Public Property strTerritory() As String
            Get
                Return _StrTerritory
            End Get
            Set(ByVal Value As String)
                _StrTerritory = Value
            End Set
        End Property


        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property


        Public Property GroupID() As Long
            Get
                Return _GroupID
            End Get
            Set(ByVal Value As Long)
                _GroupID = Value
            End Set
        End Property

        Public Property UserDesc() As String
            Get
                Return _UserDesc
            End Get
            Set(ByVal Value As String)
                _UserDesc = Value
            End Set
        End Property

        Public Property UserName() As String
            Get
                Return _UserName
            End Get
            Set(ByVal Value As String)
                _UserName = Value
            End Set
        End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property UserId() As Long
            Get
                Return _UserId
            End Get
            Set(ByVal Value As Long)
                _UserId = Value
            End Set
        End Property

        Public Property ImagePath() As String
            Get
                Return _ImagePath
            End Get
            Set(ByVal value As String)
                _ImagePath = value
            End Set
        End Property

        Public Property StrDivision() As String
            Get
                Return _StrDivision
            End Get
            Set(ByVal value As String)
                _StrDivision = value
            End Set
        End Property

        Public Property BizDocImagePath() As String
            Get
                Return _BizDocImagePath
            End Get
            Set(ByVal value As String)
                _BizDocImagePath = value
            End Set
        End Property

        Public Property CompanyImagePath() As String
            Get
                Return _CompanyImagePath
            End Get
            Set(ByVal value As String)
                _CompanyImagePath = value
            End Set
        End Property
        Public Property BankImagePath() As String
            Get
                Return _BankImagePath
            End Get
            Set(ByVal value As String)
                _BankImagePath = value
            End Set
        End Property

        'Public Property DeferredIncome() As Boolean
        '    Get
        '        Return _DeferredIncome
        '    End Get
        '    Set(ByVal value As Boolean)
        '        _DeferredIncome = value
        '    End Set
        'End Property

        Public Property PayPeriod() As Short
            Get
                Return _PayPeriod
            End Get
            Set(ByVal value As Short)
                _PayPeriod = value
            End Set
        End Property


        Private _ImapIntegration As Boolean
        Public Property ImapIntegration() As Boolean
            Get
                Return _ImapIntegration
            End Get
            Set(ByVal value As Boolean)
                _ImapIntegration = value
            End Set
        End Property
        Private _ImapSSL As Boolean
        Public Property ImapSSL() As Boolean
            Get
                Return _ImapSSL
            End Get
            Set(ByVal value As Boolean)
                _ImapSSL = value
            End Set
        End Property
        Private _ImapServerUrl As String
        Public Property ImapServerUrl() As String
            Get
                Return _ImapServerUrl
            End Get
            Set(ByVal value As String)
                _ImapServerUrl = value
            End Set
        End Property
        Private _ImapPassWord As String
        Public Property ImapPassWord() As String
            Get
                Return _ImapPassWord
            End Get
            Set(ByVal value As String)
                _ImapPassWord = value
            End Set
        End Property
        Private _ImapSSLPort As Long
        Public Property ImapSSLPort() As Long
            Get
                Return _ImapSSLPort
            End Get
            Set(ByVal value As Long)
                _ImapSSLPort = value
            End Set
        End Property

        Private _SMTPPassWord As String
        Public Property SMTPPassWord() As String
            Get
                Return _SMTPPassWord
            End Get
            Set(ByVal value As String)
                _SMTPPassWord = value
            End Set
        End Property


        Private _ChrItemSearch As Short
        Public Property ChrItemSearch() As Short
            Get
                Return _ChrItemSearch
            End Get
            Set(ByVal value As Short)
                _ChrItemSearch = value
            End Set
        End Property

        Private _SMTPUserId As String
        Public Property SMTPUserId() As String
            Get
                Return _SMTPUserId
            End Get
            Set(ByVal value As String)
                _SMTPUserId = value
            End Set
        End Property

        Private _SMTPAuth As Boolean
        Public Property SMTPAuth() As Boolean
            Get
                Return _SMTPAuth
            End Get
            Set(ByVal value As Boolean)
                _SMTPAuth = value
            End Set
        End Property
        Private _SMTPSSL As Boolean
        Public Property SMTPSSL() As Boolean
            Get
                Return _SMTPSSL
            End Get
            Set(ByVal value As Boolean)
                _SMTPSSL = value
            End Set
        End Property

        Private _SMTPDisplayName As String
        Public Property SMTPDisplayName() As String
            Get
                Return _SMTPDisplayName
            End Get
            Set(ByVal value As String)
                _SMTPDisplayName = value
            End Set
        End Property

        Private _LastUID As Long
        Public Property LastUID() As Long
            Get
                Return _LastUID
            End Get
            Set(ByVal value As Long)
                _LastUID = value
            End Set
        End Property

        Private _OrganizationSearchCriteria As Short
        Public Property OrganizationSearchCriteria() As Short
            Get
                Return _OrganizationSearchCriteria
            End Get
            Set(ByVal value As Short)
                _OrganizationSearchCriteria = value
            End Set
        End Property
        Private _OrganizationLogin As Short
        Public Property OrganizationLogin() As Short
            Get
                Return _OrganizationLogin
            End Get
            Set(ByVal value As Short)
                _OrganizationLogin = value
            End Set
        End Property

        Private _PortalName As String
        Public Property PortalName() As String
            Get
                Return _PortalName
            End Get
            Set(ByVal value As String)
                _PortalName = value
            End Set
        End Property

        Private _numCommContactID As Long
        Public Property CommContactID() As Long
            Get
                Return _numCommContactID
            End Get
            Set(ByVal Value As Long)
                _numCommContactID = Value
            End Set
        End Property

        Private _IsIncludeTaxAndShippingInCommission As Boolean
        Public Property IsIncludeTaxAndShippingInCommission() As Boolean
            Get
                Return _IsIncludeTaxAndShippingInCommission
            End Get
            Set(ByVal value As Boolean)
                _IsIncludeTaxAndShippingInCommission = value
            End Set
        End Property

        Private _IsPurchaseTaxCredit As Boolean
        Public Property IsPurchaseTaxCredit() As Boolean
            Get
                Return _IsPurchaseTaxCredit
            End Get
            Set(ByVal value As Boolean)
                _IsPurchaseTaxCredit = value
            End Set
        End Property

        Private _IsLandedCost As Boolean
        Public Property IsLandedCost() As Boolean
            Get
                Return _IsLandedCost
            End Get
            Set(ByVal value As Boolean)
                _IsLandedCost = value
            End Set
        End Property

        Private _LanedCostDefault As String
        Public Property LanedCostDefault() As String
            Get
                Return _LanedCostDefault
            End Get
            Set(ByVal value As String)
                _LanedCostDefault = value
            End Set
        End Property
        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                



        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop  Jayaraj 	DATE:30-March-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region


        Property _vcSalesOrderTabs As String
        Public Property vcSalesOrderTabs() As String
            Get
                Return _vcSalesOrderTabs
            End Get
            Set(ByVal value As String)
                _vcSalesOrderTabs = value
            End Set
        End Property

        Property _vcSalesQuotesTabs As String
        Public Property vcSalesQuotesTabs() As String
            Get
                Return _vcSalesQuotesTabs
            End Get
            Set(ByVal value As String)
                _vcSalesQuotesTabs = value
            End Set
        End Property


        Property _vcItemPurchaseHistoryTabs As String
        Public Property vcItemPurchaseHistoryTabs() As String
            Get
                Return _vcItemPurchaseHistoryTabs
            End Get
            Set(ByVal value As String)
                _vcItemPurchaseHistoryTabs = value
            End Set
        End Property


        Property _vcItemsFrequentlyPurchasedTabs As String
        Public Property vcItemsFrequentlyPurchasedTabs() As String
            Get
                Return _vcItemsFrequentlyPurchasedTabs
            End Get
            Set(ByVal value As String)
                _vcItemsFrequentlyPurchasedTabs = value
            End Set
        End Property


        Property _vcOpenCasesTabs As String
        Public Property vcOpenCasesTabs() As String
            Get
                Return _vcOpenCasesTabs
            End Get
            Set(ByVal value As String)
                _vcOpenCasesTabs = value
            End Set
        End Property

        Property _vcOpenRMATabs As String
        Public Property vcOpenRMATabs() As String
            Get
                Return _vcOpenRMATabs
            End Get
            Set(ByVal value As String)
                _vcOpenRMATabs = value
            End Set
        End Property

        Property _bitSalesOrderTabs As Boolean
        Public Property bitSalesOrderTabs() As Boolean
            Get
                Return _bitSalesOrderTabs
            End Get
            Set(ByVal value As Boolean)
                _bitSalesOrderTabs = value
            End Set
        End Property

        Property _vcSupportTabs As String
        Public Property vcSupportTabs() As String
            Get
                Return _vcSupportTabs
            End Get
            Set(ByVal value As String)
                _vcSupportTabs = value
            End Set
        End Property

        Property _bitSupportTabs As Boolean
        Public Property bitSupportTabs() As Boolean
            Get
                Return _bitSupportTabs
            End Get
            Set(ByVal value As Boolean)
                _bitSupportTabs = value
            End Set
        End Property

        Property _bitSalesQuotesTabs As Boolean
        Public Property bitSalesQuotesTabs() As Boolean
            Get
                Return _bitSalesQuotesTabs
            End Get
            Set(ByVal value As Boolean)
                _bitSalesQuotesTabs = value
            End Set
        End Property

        Property _bitItemPurchaseHistoryTabs As Boolean
        Public Property bitItemPurchaseHistoryTabs() As Boolean
            Get
                Return _bitItemPurchaseHistoryTabs
            End Get
            Set(ByVal value As Boolean)
                _bitItemPurchaseHistoryTabs = value
            End Set
        End Property


        Property _bitItemsFrequentlyPurchasedTabs As Boolean
        Public Property bitItemsFrequentlyPurchasedTabs() As Boolean
            Get
                Return _bitItemsFrequentlyPurchasedTabs
            End Get
            Set(ByVal value As Boolean)
                _bitItemsFrequentlyPurchasedTabs = value
            End Set
        End Property


        Property _bitOpenCasesTabs As Boolean
        Public Property bitOpenCasesTabs() As Boolean
            Get
                Return _bitOpenCasesTabs
            End Get
            Set(ByVal value As Boolean)
                _bitOpenCasesTabs = value
            End Set
        End Property

        Property _bitOpenRMATabs As Boolean
        Public Property bitOpenRMATabs() As Boolean
            Get
                Return _bitOpenRMATabs
            End Get
            Set(ByVal value As Boolean)
                _bitOpenRMATabs = value
            End Set
        End Property

        Property _DefaultRemStatus As Short
        Public Property DefaultRemStatus() As Short
            Get
                Return _DefaultRemStatus
            End Get
            Set(ByVal value As Short)
                _DefaultRemStatus = value
            End Set
        End Property

        Property _DefaultTaskType As Long
        Public Property DefaultTaskType() As Long
            Get
                Return _DefaultTaskType
            End Get
            Set(ByVal value As Long)
                _DefaultTaskType = value
            End Set
        End Property

        Property _bitOutlook As Boolean
        Public Property bitOutlook() As Boolean
            Get
                Return _bitOutlook
            End Get
            Set(ByVal value As Boolean)
                _bitOutlook = value
            End Set
        End Property


        Private _EnableDocumentRepositary As Boolean
        Public Property EnableDocumentRepositary() As Boolean
            Get
                Return _EnableDocumentRepositary
            End Get
            Set(ByVal value As Boolean)
                _EnableDocumentRepositary = value
            End Set
        End Property

        Private _tintComAppliesTo As Short
        Public Property tintComAppliesTo() As Short
            Get
                Return _tintComAppliesTo
            End Get
            Set(ByVal value As Short)
                _tintComAppliesTo = value
            End Set
        End Property

        Private _OrderStatusID As Long
        Public Property OrderStatusID() As Long
            Get
                Return _OrderStatusID
            End Get
            Set(ByVal value As Long)
                _OrderStatusID = value
            End Set
        End Property


        Private _boolGtoBContact As Boolean
        Public Property boolGtoBContact() As Boolean
            Get
                Return _boolGtoBContact
            End Get
            Set(ByVal value As Boolean)
                _boolGtoBContact = value
            End Set
        End Property

        Private _boolBtoGContact As Boolean
        Public Property boolBtoGContact() As Boolean
            Get
                Return _boolBtoGContact
            End Get
            Set(ByVal value As Boolean)
                _boolBtoGContact = value
            End Set
        End Property

        Private _boolGtoBCalendar As Boolean
        Public Property boolGtoBCalendar() As Boolean
            Get
                Return _boolGtoBCalendar
            End Get
            Set(ByVal value As Boolean)
                _boolGtoBCalendar = value
            End Set
        End Property

        Private _boolBtoGCalendar As Boolean
        Public Property boolBtoGCalendar() As Boolean
            Get
                Return _boolBtoGCalendar
            End Get
            Set(ByVal value As Boolean)
                _boolBtoGCalendar = value
            End Set
        End Property

        Private _GoogleGroupID As String
        Public Property GoogleGroupID() As String
            Get
                Return _GoogleGroupID
            End Get
            Set(ByVal value As String)
                _GoogleGroupID = value
            End Set
        End Property

        Private _EnableNonInventoryItemExpense As Boolean
        Public Property EnableNonInventoryItemExpense() As Boolean
            Get
                Return _EnableNonInventoryItemExpense
            End Get
            Set(ByVal value As Boolean)
                _EnableNonInventoryItemExpense = value
            End Set
        End Property

        Private _InlineEdit As Boolean
        Public Property InlineEdit() As Boolean
            Get
                Return _InlineEdit
            End Get
            Set(ByVal value As Boolean)
                _InlineEdit = value
            End Set
        End Property

        Private _RemoveVendorPOValidation As Boolean
        Public Property RemoveVendorPOValidation() As Boolean
            Get
                Return _RemoveVendorPOValidation
            End Get
            Set(ByVal value As Boolean)
                _RemoveVendorPOValidation = value
            End Set
        End Property

        Private _SearchOrderCustomerHistory As Boolean
        Public Property SearchOrderCustomerHistory() As Boolean
            Get
                Return _SearchOrderCustomerHistory
            End Get
            Set(ByVal value As Boolean)
                _SearchOrderCustomerHistory = value
            End Set
        End Property

        Private _EmailStatus As Boolean
        Public Property EmailStatus As Boolean
            Get
                Return _EmailStatus
            End Get
            Set(ByVal value As Boolean)
                _EmailStatus = value
            End Set
        End Property

        Private _BaseTaxOnArea As Short
        Public Property BaseTaxOnArea() As Short
            Get
                Return _BaseTaxOnArea
            End Get
            Set(ByVal value As Short)
                _BaseTaxOnArea = value
            End Set
        End Property
        Public Property boolAmountPastDue As Boolean
            Get
                Return _boolAmountPastDue
            End Get
            Set(ByVal value As Boolean)
                _boolAmountPastDue = value
            End Set
        End Property
        Public Property AmountPastDue As Decimal
            Get
                Return _AmountPastDue
            End Get
            Set(ByVal value As Decimal)
                _AmountPastDue = value
            End Set
        End Property

        Private _boolRentalItem As Boolean
        Public Property boolRentalItem As Boolean
            Get
                Return _boolRentalItem
            End Get
            Set(ByVal value As Boolean)
                _boolRentalItem = value
            End Set
        End Property

        Private _RentalItemClass As Long
        Public Property RentalItemClass As Long
            Get
                Return _RentalItemClass
            End Get
            Set(ByVal value As Long)
                _RentalItemClass = value
            End Set
        End Property

        Private _RentalHourlyUOM As Long
        Public Property RentalHourlyUOM As Long
            Get
                Return _RentalHourlyUOM
            End Get
            Set(ByVal value As Long)
                _RentalHourlyUOM = value
            End Set
        End Property


        Private _RentalDailyUOM As Long
        Public Property RentalDailyUOM As Long
            Get
                Return _RentalDailyUOM
            End Get
            Set(ByVal value As Long)
                _RentalDailyUOM = value
            End Set
        End Property

        Private _RentalPriceBasedOn As Short
        Public Property RentalPriceBasedOn() As Short
            Get
                Return _RentalPriceBasedOn
            End Get
            Set(ByVal value As Short)
                _RentalPriceBasedOn = value
            End Set
        End Property

        Private _CalendarTimeFormat As Short
        Public Property CalendarTimeFormat() As Short
            Get
                Return _CalendarTimeFormat
            End Get
            Set(ByVal value As Short)
                _CalendarTimeFormat = value
            End Set
        End Property

        Private _PriceBookDiscount As Short
        Public Property PriceBookDiscount() As Short
            Get
                Return _PriceBookDiscount
            End Get
            Set(ByVal value As Short)
                _PriceBookDiscount = value
            End Set
        End Property

        Private _AutoSerialNoAssign As Boolean
        Public Property AutoSerialNoAssign() As Boolean
            Get
                Return _AutoSerialNoAssign
            End Get
            Set(ByVal value As Boolean)
                _AutoSerialNoAssign = value
            End Set
        End Property

        Private _bitIsShowBalance As Boolean
        Public Property IsShowBalance() As Boolean
            Get
                Return _bitIsShowBalance
            End Get
            Set(ByVal value As Boolean)
                _bitIsShowBalance = value
            End Set
        End Property

        Private _IsEnableProjectTracking As Boolean
        Public Property IsEnableProjectTracking() As Boolean
            Get
                Return _IsEnableProjectTracking
            End Get
            Set(ByVal value As Boolean)
                _IsEnableProjectTracking = value
            End Set
        End Property

        Private _IsEnableCampaignTracking As Boolean
        Public Property IsEnableCampaignTracking() As Boolean
            Get
                Return _IsEnableCampaignTracking
            End Get
            Set(ByVal value As Boolean)
                _IsEnableCampaignTracking = value
            End Set
        End Property

        Private _IsEnableResourceScheduling As Boolean
        Public Property IsEnableResourceScheduling() As Boolean
            Get
                Return _IsEnableResourceScheduling
            End Get
            Set(ByVal value As Boolean)
                _IsEnableResourceScheduling = value
            End Set
        End Property

        Private _ShippingServiceItem As Long
        Public Property ShippingServiceItem() As Long
            Get
                Return _ShippingServiceItem
            End Get
            Set(ByVal value As Long)
                _ShippingServiceItem = value
            End Set
        End Property

        Private _SOBizDocStatus As Long
        Public Property SOBizDocStatus() As Long
            Get
                Return _SOBizDocStatus
            End Get
            Set(ByVal Value As Long)
                _SOBizDocStatus = Value
            End Set
        End Property

        Private _AutolinkUnappliedPayment As Boolean
        Public Property AutolinkUnappliedPayment() As Boolean
            Get
                Return _AutolinkUnappliedPayment
            End Get
            Set(ByVal Value As Boolean)
                _AutolinkUnappliedPayment = Value
            End Set
        End Property

        Private _DiscountServiceItem As Long
        Public Property DiscountServiceItem() As Long
            Get
                Return _DiscountServiceItem
            End Get
            Set(ByVal value As Long)
                _DiscountServiceItem = value
            End Set
        End Property

        Private _ShipToPhoneNumber As String
        Public Property ShipToPhoneNumber() As String
            Get
                Return _ShipToPhoneNumber
            End Get
            Set(ByVal value As String)
                _ShipToPhoneNumber = value
            End Set
        End Property

        Private _SiteID As Long
        Public Property SiteID() As Long
            Get
                Return _SiteID
            End Get
            Set(ByVal value As Long)
                _SiteID = value
            End Set
        End Property

        Private _GAUserEmailId As String
        Public Property GAUserEmailId() As String
            Get
                Return _GAUserEmailId
            End Get
            Set(ByVal value As String)
                _GAUserEmailId = value
            End Set
        End Property

        Private _GAUserPassword As String
        Public Property GAUserPassword() As String
            Get
                Return _GAUserPassword
            End Get
            Set(ByVal value As String)
                _GAUserPassword = value
            End Set
        End Property

        Private _GAUserProfileId As String
        Public Property GAUserProfileId() As String
            Get
                Return _GAUserProfileId
            End Get
            Set(ByVal value As String)
                _GAUserProfileId = value
            End Set
        End Property


        'Private _AllowPPVariance As Boolean
        'Public Property AllowPPVariance() As Boolean
        '    Get
        '        Return _AllowPPVariance
        '    End Get
        '    Set(ByVal Value As Boolean)
        '        _AllowPPVariance = Value
        '    End Set
        'End Property

        Private _PaypalUserName As String
        Public Property PaypalUserName() As String
            Get
                Return _PaypalUserName
            End Get
            Set(ByVal value As String)
                _PaypalUserName = value
            End Set
        End Property

        Private _PaypalPassword As String
        Public Property PaypalPassword() As String
            Get
                Return _PaypalPassword
            End Get
            Set(ByVal value As String)
                _PaypalPassword = value
            End Set
        End Property

        Private _PaypalSignature As String
        Public Property PaypalSignature() As String
            Get
                Return _PaypalSignature
            End Get
            Set(ByVal value As String)
                _PaypalSignature = value
            End Set
        End Property

        Private _DiscountOnUnitPrice As Boolean
        Public Property DiscountOnUnitPrice() As Boolean
            Get
                Return _DiscountOnUnitPrice
            End Get
            Set(ByVal value As Boolean)
                _DiscountOnUnitPrice = value
            End Set
        End Property

        Private _intImageWidth As Integer
        Public Property ImageWidth() As Integer
            Get
                Return _intImageWidth
            End Get
            Set(ByVal value As Integer)
                _intImageWidth = value
            End Set
        End Property
        Private _intImageHeight As Integer
        Public Property ImageHeight() As Integer
            Get
                Return _intImageHeight
            End Get
            Set(ByVal value As Integer)
                _intImageHeight = value
            End Set
        End Property
        Private _dcTotalInsuredValue As Decimal
        Public Property TotalInsuredValue() As Decimal
            Get
                Return _dcTotalInsuredValue
            End Get
            Set(ByVal value As Decimal)
                _dcTotalInsuredValue = value
            End Set
        End Property
        Private _dcTotalCustomsValue As String
        Public Property TotalCustomsValue() As String
            Get
                Return _dcTotalCustomsValue
            End Get
            Set(ByVal value As String)
                _dcTotalCustomsValue = value
            End Set
        End Property

        Private _IsSkipStep2 As Boolean
        Public Property IsSkipStep2() As Boolean
            Get
                Return _IsSkipStep2
            End Get
            Set(ByVal value As Boolean)
                _IsSkipStep2 = value
            End Set
        End Property

        Private _IsDisplayCategories As Boolean
        Public Property IsDisplayCategories() As Boolean
            Get
                Return _IsDisplayCategories
            End Get
            Set(ByVal value As Boolean)
                _IsDisplayCategories = value
            End Set
        End Property

        Private _IsShowPriceUsingPriceLevel As Boolean
        Public Property IsShowPriceUsingPriceLevel() As Boolean
            Get
                Return _IsShowPriceUsingPriceLevel
            End Get
            Set(ByVal value As Boolean)
                _IsShowPriceUsingPriceLevel = value
            End Set
        End Property

        Private _strHideTabs As String
        Public Property HideTabs() As String
            Get
                Return _strHideTabs
            End Get
            Set(ByVal value As String)
                _strHideTabs = value
            End Set
        End Property

        Private _blnUseBizdocAmount As Boolean
        Public Property UseBizdocAmount() As Boolean
            Get
                Return _blnUseBizdocAmount
            End Get
            Set(ByVal value As Boolean)
                _blnUseBizdocAmount = value
            End Set
        End Property

        Private _IsEnableSorting As Boolean
        Public Property IsEnableSorting() As Boolean
            Get
                Return _IsEnableSorting
            End Get
            Set(ByVal value As Boolean)
                _IsEnableSorting = value
            End Set
        End Property
        'Author:Sachin Sadhu||Date:20-Jan-2013
        'Purpose:To add settings to change Price mode either ListPrice or Retail price 
        Private _IsChangeSortPriceMode As Boolean
        Public Property IsChangeSortPriceMode() As Boolean
            Get
                Return _IsChangeSortPriceMode
            End Get
            Set(ByVal value As Boolean)
                _IsChangeSortPriceMode = value
            End Set
        End Property
        'End of Code by Sachin

        'Author:Sachin Sadhu||Date:21-Jan-2014
        'Purpose:Dynamically Apply Paging Size and Variant 
        Private _numPageSize As Integer
        Private _numPageVariant As Integer

        Public Property numPageSize() As Integer
            Get
                Return _numPageSize
            End Get
            Set(ByVal value As Integer)
                _numPageSize = value
            End Set
        End Property
        Public Property numPageVariant() As Integer
            Get
                Return _numPageVariant
            End Get
            Set(ByVal value As Integer)
                _numPageVariant = value
            End Set
        End Property
        'End of Code

        Private _IsDefaultRateType As Boolean
        Public Property IsDefaultRateType() As Boolean
            Get
                Return _IsDefaultRateType
            End Get
            Set(ByVal value As Boolean)
                _IsDefaultRateType = value
            End Set
        End Property

        'Added by sandeep for min unit price rule in new sales opportunity and new sales order
        Private _IsMinUnitPriceRule As Boolean
        Public Property IsMinUnitPriceRule() As Boolean
            Get
                Return _IsMinUnitPriceRule
            End Get
            Set(ByVal value As Boolean)
                _IsMinUnitPriceRule = value
            End Set
        End Property

        Private _AbovePercent As Double
        Public Property AbovePercent() As Double
            Get
                Return _AbovePercent
            End Get
            Set(ByVal value As Double)
                _AbovePercent = value
            End Set
        End Property

        Private _BelowPercent As Double
        Public Property BelowPercent() As Double
            Get
                Return _BelowPercent
            End Get
            Set(ByVal value As Double)
                _BelowPercent = value
            End Set
        End Property

        Private _BelowPriceField As Long
        Public Property BelowPriceField() As Long
            Get
                Return _BelowPriceField
            End Get
            Set(ByVal value As Long)
                _BelowPriceField = value
            End Set
        End Property

        Private _numListItemId As Long
        Public Property ListItemId() As Long
            Get
                Return _numListItemId
            End Get
            Set(ByVal value As Long)
                _numListItemId = value
            End Set
        End Property

        Private _approvalRuleID As Long
        Public Property ApprovalRuleID() As Long
            Get
                Return _approvalRuleID
            End Get
            Set(ByVal value As Long)
                _approvalRuleID = value
            End Set
        End Property

        Private _UnitPriceApprover As String
        Public Property UnitPriceApprover() As String
            Get
                Return _UnitPriceApprover
            End Get
            Set(ByVal value As String)
                _UnitPriceApprover = value
            End Set
        End Property

        Private _numDefaultSalesPricing As Integer
        Public Property DefaultSalesPricingMethod() As Integer
            Get
                Return _numDefaultSalesPricing
            End Get
            Set(ByVal value As Integer)
                _numDefaultSalesPricing = value
            End Set
        End Property

        Private _bitPreUpSell As Boolean
        Public Property bitPreUpSell() As Boolean
            Get
                Return _bitPreUpSell
            End Get
            Set(ByVal value As Boolean)
                _bitPreUpSell = value
            End Set
        End Property
        Private _bitPostUpSell As Boolean
        Public Property bitPostUpSell() As Boolean
            Get
                Return _bitPostUpSell
            End Get
            Set(ByVal value As Boolean)
                _bitPostUpSell = value
            End Set
        End Property

        Private _bitReOrderPoint As Boolean
        Public Property bitReOrderPoint() As Boolean
            Get
                Return _bitReOrderPoint
            End Get
            Set(ByVal value As Boolean)
                _bitReOrderPoint = value
            End Set
        End Property

        Private _numReOrderPointOrderStatus As Long
        Public Property ReOrderPointOrderStatus() As Long
            Get
                Return _numReOrderPointOrderStatus
            End Get
            Set(ByVal value As Long)
                _numReOrderPointOrderStatus = value
            End Set
        End Property

        Private _numPODropShipBizDoc As Long
        Public Property PODropShipBizDoc() As Long
            Get
                Return _numPODropShipBizDoc
            End Get
            Set(ByVal value As Long)
                _numPODropShipBizDoc = value
            End Set
        End Property

        Private _numPODropShipBizDocTemplate As Long
        Public Property PODropShipBizDocTemplate() As Long
            Get
                Return _numPODropShipBizDocTemplate
            End Get
            Set(ByVal value As Long)
                _numPODropShipBizDocTemplate = value
            End Set
        End Property

        Private _vcLinkedinId As String
        Public Property LinkedinId() As String
            Get
                Return _vcLinkedinId
            End Get
            Set(value As String)
                _vcLinkedinId = value
            End Set
        End Property
        Private _vcProfile As String
        Public Property ProfilePic() As String
            Get
                Return _vcProfile
            End Get
            Set(value As String)
                _vcProfile = value
            End Set
        End Property

        Private _vcLogoForBizTheme As String
        Public Property vcLogoForBizTheme() As String
            Get
                Return _vcLogoForBizTheme
            End Get
            Set(value As String)
                _vcLogoForBizTheme = value
            End Set
        End Property

        Private _bitLogoAppliedToBizTheme As Boolean

        Public Property bitLogoAppliedToBizTheme() As Boolean
            Get
                Return _bitLogoAppliedToBizTheme
            End Get
            Set(value As Boolean)
                _bitLogoAppliedToBizTheme = value
            End Set
        End Property

        Private _vcLogoForLoginBizTheme As String
        Public Property vcLogoForLoginBizTheme() As String
            Get
                Return _vcLogoForLoginBizTheme
            End Get
            Set(value As String)
                _vcLogoForLoginBizTheme = value
            End Set
        End Property

        Private _vcElectiveItemFields As String
        Public Property vcElectiveItemFields() As String
            Get
                Return _vcElectiveItemFields
            End Get
            Set(value As String)
                _vcElectiveItemFields = value
            End Set
        End Property

        Private _vcLoginURL As String
        Public Property vcLoginURL() As String
            Get
                Return _vcLoginURL
            End Get
            Set(value As String)
                _vcLoginURL = value
            End Set
        End Property


        Private _bitLogoAppliedToLoginBizTheme As Boolean

        Public Property bitLogoAppliedToLoginBizTheme() As Boolean
            Get
                Return _bitLogoAppliedToLoginBizTheme
            End Get
            Set(value As Boolean)
                _bitLogoAppliedToLoginBizTheme = value
            End Set
        End Property

        Private _vcThemeClass As String
        Public Property vcThemeClass() As String
            Get
                Return _vcThemeClass
            End Get
            Set(value As String)
                _vcThemeClass = value
            End Set
        End Property
        Private _intAssociate As Integer
        Public Property intAssociate() As Integer
            Get
                Return _intAssociate
            End Get
            Set(value As Integer)
                _intAssociate = value
            End Set
        End Property

        Private _intTimeExpApprovalProcess As Integer
        Public Property intTimeExpApprovalProcess() As Integer
            Get
                Return _intTimeExpApprovalProcess
            End Get
            Set(value As Integer)
                _intTimeExpApprovalProcess = value
            End Set
        End Property
        Private _intOpportunityApprovalProcess As Integer
        Public Property intOpportunityApprovalProcess() As Integer
            Get
                Return _intOpportunityApprovalProcess
            End Get
            Set(value As Integer)
                _intOpportunityApprovalProcess = value
            End Set
        End Property
        Public Property IsEnableDeferredIncome As Boolean
        Public Property ShippingZoneID As Long
        Public Property PrinterIPAddress As String
        Public Property PrinterPort As String
        Public Property IsCostApproval As Boolean
        Public Property IsListPriceApproval As Boolean
        Public Property PreLoginPriceLevel As Short
        Public Property DefaultSite As Long
        Public Property UnitRecommendationType As Short
        Public Property IsRemoveGlobalWarehouse As Boolean
        Public Property bitEDI As Boolean
        Public Property bit3PL As Boolean
        Public Property IsAllowDuplicateLineItems As Boolean
        Public Property CommitAllocation As Short
        Public Property InventoryInvoicing As Short
        Public Property tintMarkupDiscountOption As Short
        Public Property tintMarkupDiscountValue As Short
        Public Property IsShowPromoDetailsLink As Boolean
        Public Property IsIncludeRequisitions As Boolean
        Public Property IsOnPayroll As Boolean
        Public Property IsCommissionBasedOn As Boolean
        Public Property CommissionBasedOn As Short
        Public Property IsDoNotShowDropshipWindow As Boolean
        Public Property IsInventoryInvoicing As Boolean
        Public Property tintReceivePaymentTo As Short
        Public Property numReceivePaymentBankAccount As Long
        Public Property WareHouseAvailability As Short
        Public Property DisplayQtyAvailable As Boolean
        Public Property PayrollType As Short
        Public Property HourType As Short
        Public Property OverheadServiceItem As Long
        Public Property OAuthImap As Boolean
        Public Property SelectedGroupTypes As String

        Public Property ARContactPosition As Long
        Public Property IsShowCardConnectLink As Boolean
        Public Property BluePayFormName As String
        Public Property BluePaySuccessURL As String
        Public Property BluePayDeclineURL As String
        Public Property IsUseDeluxeCheckStock As Boolean
        Public Property IsEnableSmartyStreets As Boolean
        Public Property SmartyStreetsAPIKeys As String
        Public Property IsUsePreviousEmailBizDoc As Boolean
        Private _bitElasticSearch As Boolean
        Public Property bitElasticSearch() As Boolean
            Get
                Return _bitElasticSearch
            End Get
            Set(ByVal value As Boolean)
                _bitElasticSearch = value
            End Set
        End Property
        Public Property MailProvider As Short
        Public Property CommissionVendorCostType As Short

        Public Function GetUserListForUserDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetUserListForUserDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetUsersParentCompany() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetContactsParComp", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function TransferOwnership() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRecID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _RecID

                arParms(2) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_TransferOwnerShip", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetGroups() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GroupID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcGroupTypes", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = SelectedGroupTypes

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAllAuthenticationGroups", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetGroupForUser() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "usp_GetAuthorizationGroupsForSelectedUser", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetUserAccessDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetUsersWithDomains", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateUserAccessDetails() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(23) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcUserName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _UserName

                arParms(2) = New Npgsql.NpgsqlParameter("@vcUserDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 250)
                arParms(2).Value = _UserDesc

                arParms(3) = New Npgsql.NpgsqlParameter("@numGroupId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _GroupID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserDetailID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _ContactID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@strTerritory", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(6).Value = _StrTerritory

                arParms(7) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = DomainID

                arParms(8) = New Npgsql.NpgsqlParameter("@strTeam", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(8).Value = _strTeam

                'arParms(9) = New Npgsql.NpgsqlParameter("@strEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                'arParms(9).Value = _strEmail

                arParms(9) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(9).Value = _Email

                arParms(10) = New Npgsql.NpgsqlParameter("@vcPassword", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(10).Value = _Password

                arParms(11) = New Npgsql.NpgsqlParameter("@Active", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = If(_AccessAllowed = 1, True, False)

                arParms(12) = New Npgsql.NpgsqlParameter("@numDefaultClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _Class

                arParms(13) = New Npgsql.NpgsqlParameter("@numDefaultWarehouse", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _DefaultWarehouse

                arParms(14) = New Npgsql.NpgsqlParameter("@vcLinkedinId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(14).Value = _vcLinkedinId

                arParms(15) = New Npgsql.NpgsqlParameter("@intAssociate", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(15).Value = _intAssociate

                arParms(16) = New Npgsql.NpgsqlParameter("@ProfilePic", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(16).Value = _vcProfile

                arParms(17) = New Npgsql.NpgsqlParameter("@bitPayroll", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(17).Value = IsOnPayroll

                arParms(18) = New Npgsql.NpgsqlParameter("@monHourlyRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(18).Value = HourlyRate

                arParms(19) = New Npgsql.NpgsqlParameter("@tintPayrollType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(19).Value = PayrollType

                arParms(20) = New Npgsql.NpgsqlParameter("@tintHourType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(20).Value = HourType

                arParms(21) = New Npgsql.NpgsqlParameter("@monOverTimeRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(21).Value = HourlyRate

                arParms(22) = New Npgsql.NpgsqlParameter("@bitOauthImap", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(22).Value = OAuthImap

                arParms(23) = New Npgsql.NpgsqlParameter("@tintMailProvider", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(23).Value = MailProvider

                SqlDAL.ExecuteNonQuery(connString, "usp_SetUsersWithDomains", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateUserAccessDetailsDefault() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@tintDefaultRemStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _DefaultRemStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@numDefaultTaskType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _DefaultTaskType

                arParms(5) = New Npgsql.NpgsqlParameter("@bitOutlook", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _bitOutlook

                SqlDAL.ExecuteNonQuery(connString, "usp_UpdateUserAccessDetailsDefault", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateUserSMTP() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@SMTPAuth", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _SMTPAuth

                arParms(2) = New Npgsql.NpgsqlParameter("@SMTPSSL", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _SMTPSSL

                arParms(3) = New Npgsql.NpgsqlParameter("@vcSMTPPassword", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(3).Value = _SMTPPassWord

                arParms(4) = New Npgsql.NpgsqlParameter("@SMTPServer", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(4).Value = _SMTPServer

                arParms(5) = New Npgsql.NpgsqlParameter("@SMTPPort", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _SMTPPort

                arParms(6) = New Npgsql.NpgsqlParameter("@bitSMTPServer", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _bitSMTPServer


                SqlDAL.ExecuteNonQuery(connString, "USp_UpdateUserSMTP", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetTerritoryForUsers() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetTerrForUsers", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDivisionForDomain() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDivisionForDomain", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDivisionForContactID() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDivisionForContactID", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTeamForUsers() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetTeamForUsrMst", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetHourlyRateForUsers() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetUserHourlyRate", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ExtranetLogin() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@Email", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = _Email

                arParms(1) = New Npgsql.NpgsqlParameter("@Password", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _Password

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ExtranetLogin", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ExtranetLoginDomains() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@Email", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = _Email

                arParms(1) = New Npgsql.NpgsqlParameter("@Password", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _Password

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ExtranetLoginDomains", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ChangePassword() As Integer
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@OldPassword", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _Password

                arParms(3) = New Npgsql.NpgsqlParameter("@NewPassword", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _NewPassword

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "USP_ExtChangePassword", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function SelState() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCountryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _Country

                arParms(2) = New Npgsql.NpgsqlParameter("@vcAbbreviations", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = vcAbbreviations

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetState", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SelAllStates() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAllStates", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageTerritory() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TerritoryID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcTerName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _TerritoryName

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _UserId

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageTerritory", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageState() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numStateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _StateID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcState", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _StateName

                arParms(3) = New Npgsql.NpgsqlParameter("@numCountryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _Country

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _UserId

                arParms(5) = New Npgsql.NpgsqlParameter("@vcAbbreviations", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(5).Value = _vcAbbreviations

                arParms(6) = New Npgsql.NpgsqlParameter("@numShippingZone", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = ShippingZoneID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageState", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function DelTerritory() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TerritoryID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteTerritory", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function DelState() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numStateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _StateID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteState", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function PortalWorkflow() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintCreateOppStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _CreateOppStatus

                arParms(1) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _Status

                arParms(2) = New Npgsql.NpgsqlParameter("@numRelationShip", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _Relationship

                arParms(3) = New Npgsql.NpgsqlParameter("@tintCreateBizDoc", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _CreateBizDoc

                arParms(4) = New Npgsql.NpgsqlParameter("@numBizDocName", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _BizDocName

                arParms(5) = New Npgsql.NpgsqlParameter("@tintAccessAllowed", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _AccessAllowed

                arParms(6) = New Npgsql.NpgsqlParameter("@vcOrderSuccessfullPage", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(6).Value = _PageAfterSuccReg

                arParms(7) = New Npgsql.NpgsqlParameter("@vcOrderUnsucessfullPage", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(7).Value = _PageAfterUnSuccReg


                SqlDAL.ExecuteNonQuery(connString, "USP_UpdatePortalWorkFlow", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function PortalWorkflowDtls() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_PortalWorkFlowDtl", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPortalDashboardReports() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRelationshipID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _Relationship

                arParms(2) = New Npgsql.NpgsqlParameter("@numProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ProfileID

                arParms(3) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ContactID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPortalDashboardReports", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManagePortalDashboardReports() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strItems

                arParms(2) = New Npgsql.NpgsqlParameter("@numRelationshipID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _Relationship

                arParms(3) = New Npgsql.NpgsqlParameter("@numProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProfileID

                arParms(4) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ContactID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManagePortalDashboardReports", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Public Function ManageEmailUsers() As Boolean
        '    Try

        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString

        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = _ContactID

        '        arParms(1) = New Npgsql.NpgsqlParameter("@strUsers", NpgsqlTypes.NpgsqlDbType.Text)
        '        arParms(1).Value = _strUsers

        '        SqlDAL.ExecuteNonQuery(connString, "USP_ManageEmailRights", arParms)
        '        Return True
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Function SendPassoword() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = _Email

                arParms(1) = New Npgsql.NpgsqlParameter("@vcPassword", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Direction = ParameterDirection.InputOutput
                arParms(1).Value = _Password

                arParms(2) = New Npgsql.NpgsqlParameter("@NoOfRows", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _NoOfRows

                arParms(3) = New Npgsql.NpgsqlParameter("@bitPortal", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _boolPoral

                arParms(4) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _ContactID

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_SendPassword", objParam, True)
                _Password = CStr(IIf(IsDBNull(DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value) = True, "", DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value))
                _NoOfRows = CShort(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
                _ContactID = CLng(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CreateExtAccessFromPortal() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numCompanyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CompanyID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcPassword", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _Password

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _Email

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_CreateExtUsersPortal", objParam, True)
                _Email = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRelProfileD() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRelationship", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RelID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetRelProfile", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRelFollowD() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRelationship", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RelID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetRelFollw", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageRelProfile() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRelationship", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RelID

                arParms(1) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProfileID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRelProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RelProID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID


                SqlDAL.ExecuteNonQuery(connString, "USP_ManageRelProfile", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function ManageRelFollow() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRelationship", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RelID

                arParms(1) = New Npgsql.NpgsqlParameter("@numFollow", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _FollowID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRelFolID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RelFollowID


                SqlDAL.ExecuteNonQuery(connString, "USP_ManageRelFollow", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function DeleteRelProfile() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}



                arParms(0) = New Npgsql.NpgsqlParameter("@numRelProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RelProID


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteRelProfile", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function DeleteRelFollow() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}



                arParms(0) = New Npgsql.NpgsqlParameter("@numRelFolID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RelFollowID


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteRelfolloow", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateSubscription() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@UserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@strSubscriptionId", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(1).Value = _strSubscriptionId

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateSubscription", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDivisionFromDomain() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_DomainDetails", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function UpdateDomainSMTP() As Boolean

            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SMTPAuth", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _SMTPAuth

                arParms(2) = New Npgsql.NpgsqlParameter("@SMTPSSL", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _SMTPSSL

                arParms(3) = New Npgsql.NpgsqlParameter("@vcSMTPPassword", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(3).Value = _SMTPPassWord

                arParms(4) = New Npgsql.NpgsqlParameter("@SMTPServer", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(4).Value = _SMTPServer

                arParms(5) = New Npgsql.NpgsqlParameter("@SMTPPort", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _SMTPPort

                arParms(6) = New Npgsql.NpgsqlParameter("@bitSMTPServer", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _bitSMTPServer

                arParms(7) = New Npgsql.NpgsqlParameter("@vcSMTPUserName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = _SMTPUserId

                arParms(8) = New Npgsql.NpgsqlParameter("@vcPSMTPDisplayName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _SMTPDisplayName

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdatedomainSMTP", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateBizThemeColorDetails() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcLogoForBizTheme", _vcLogoForBizTheme, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitLogoAppliedToBizTheme", _bitLogoAppliedToBizTheme, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcThemeClass", _vcThemeClass, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcLogoForLoginBizTheme", _vcLogoForLoginBizTheme, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitLogoAppliedToLoginBizTheme", _bitLogoAppliedToLoginBizTheme, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcLoginURL", _vcLoginURL, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateBizAutomationTheme", sqlParams.ToArray())
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetDomainDetailsByURL() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcLoginURL", _vcLoginURL, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_ValidateCustomizeLoginScreenURL", sqlParams.ToArray()).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function UpdateDomainDetails() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim strParameterJson As String = "{"

                strParameterJson = strParameterJson & """" & "v_vcDomainName" & """:""" & _DomainName & """"
                strParameterJson = strParameterJson & ",""" & "v_vcDomainDesc" & """:""" & _DomainDesc & """"
                strParameterJson = strParameterJson & ",""" & "v_bitExchangeIntegration" & """:" & _ExcIntegration
                strParameterJson = strParameterJson & ",""" & "v_bitAccessExchange" & """:" & _AccessMethod
                strParameterJson = strParameterJson & ",""" & "v_vcExchUserName" & """:""" & _ExchUserName & """"
                strParameterJson = strParameterJson & ",""" & "v_vcExchPassword" & """:""" & _ExchPassword & """"
                strParameterJson = strParameterJson & ",""" & "v_vcExchPath" & """:""" & _ExchPath & """"
                strParameterJson = strParameterJson & ",""" & "v_vcExchDomain" & """:""" & _ExchDomain & """"
                strParameterJson = strParameterJson & ",""" & "v_tintCustomPagingRows" & """:" & _CustomPagingRows
                strParameterJson = strParameterJson & ",""" & "v_vcDateFormat" & """:""" & _DateFormat & """"
                strParameterJson = strParameterJson & ",""" & "v_numDefCountry" & """:" & _DefCountry
                strParameterJson = strParameterJson & ",""" & "v_tintComposeWindow" & """:" & _ComposeWindow
                strParameterJson = strParameterJson & ",""" & "v_sintStartDate" & """:" & _StartDate
                strParameterJson = strParameterJson & ",""" & "v_sintNoofDaysInterval" & """:" & _IntervalDays
                strParameterJson = strParameterJson & ",""" & "v_tintAssignToCriteria" & """:" & _PopulateUserCriteria
                strParameterJson = strParameterJson & ",""" & "v_bitIntmedPage" & """:" & _bitIntMedPage
                strParameterJson = strParameterJson & ",""" & "v_tintFiscalStartMonth" & """:" & _FiscalStartMonth
                strParameterJson = strParameterJson & ",""" & "v_tintPayPeriod" & """:" & _PayPeriod
                strParameterJson = strParameterJson & ",""" & "v_numCurrencyID" & """:" & _CurrencyID
                strParameterJson = strParameterJson & ",""" & "v_charUnitSystem" & """:""" & _chrUnitSystem & """"
                strParameterJson = strParameterJson & ",""" & "v_vcPortalLogo" & """:""" & _PortalLogo & """"
                strParameterJson = strParameterJson & ",""" & "v_tintChrForComSearch" & """:" & _ChrForComSearch
                strParameterJson = strParameterJson & ",""" & "v_intPaymentGateWay" & """:" & _PaymentGateWay
                strParameterJson = strParameterJson & ",""" & "v_tintChrForItemSearch" & """:" & _ChrItemSearch
                strParameterJson = strParameterJson & ",""" & "v_ShipCompany" & """:" & _ShipCompany
                strParameterJson = strParameterJson & ",""" & "v_bitMultiCurrency" & """:" & _boolMultiCurrency
                strParameterJson = strParameterJson & ",""" & "v_bitCreateInvoice" & """:" & _CreateBizDoc
                strParameterJson = strParameterJson & ",""" & "v_numDefaultSalesBizDocID" & """:" & _DefaultSalesBizDocID
                strParameterJson = strParameterJson & ",""" & "v_numDefaultPurchaseBizDocID" & """:" & _DefaultPurchaseBizDocID
                strParameterJson = strParameterJson & ",""" & "v_bitSaveCreditCardInfo" & """:" & _KeepCreditCardInfo
                strParameterJson = strParameterJson & ",""" & "v_intLastViewedRecord" & """:" & _LastViewedCount
                strParameterJson = strParameterJson & ",""" & "v_tintCommissionType" & """:" & _CommissionType
                strParameterJson = strParameterJson & ",""" & "v_tintBaseTax" & """:" & _BaseTax
                strParameterJson = strParameterJson & ",""" & "v_bitEmbeddedCost" & """:" & _EnableEmbeddedCost
                strParameterJson = strParameterJson & ",""" & "v_numDefaultSalesOppBizDocId" & """:" & _DefaultSalesOppBizDocID
                strParameterJson = strParameterJson & ",""" & "v_tintSessionTimeOut" & """:" & _SessionTimeout
                strParameterJson = strParameterJson & ",""" & "v_tintDecimalPoints" & """:" & _DecimalPoints
                strParameterJson = strParameterJson & ",""" & "v_bitCustomizePortal" & """:" & _boolCustomizePortal
                strParameterJson = strParameterJson & ",""" & "v_tintBillToForPO" & """:" & _BillToDefault
                strParameterJson = strParameterJson & ",""" & "v_tintShipToForPO" & """:" & _ShipToDefault
                strParameterJson = strParameterJson & ",""" & "v_tintOrganizationSearchCriteria" & """:" & _OrganizationSearchCriteria
                strParameterJson = strParameterJson & ",""" & "v_bitDocumentRepositary" & """:" & _EnableDocumentRepositary
                strParameterJson = strParameterJson & ",""" & "v_tintComAppliesTo" & """:" & _tintComAppliesTo
                strParameterJson = strParameterJson & ",""" & "v_bitGtoBContact" & """:" & _boolGtoBContact
                strParameterJson = strParameterJson & ",""" & "v_bitBtoGContact" & """:" & _boolBtoGContact
                strParameterJson = strParameterJson & ",""" & "v_bitGtoBCalendar" & """:" & _boolGtoBCalendar
                strParameterJson = strParameterJson & ",""" & "v_bitBtoGCalendar" & """:" & _boolBtoGCalendar
                strParameterJson = strParameterJson & ",""" & "v_bitExpenseNonInventoryItem" & """:" & _EnableNonInventoryItemExpense
                strParameterJson = strParameterJson & ",""" & "v_bitInlineEdit" & """:" & _InlineEdit
                strParameterJson = strParameterJson & ",""" & "v_bitRemoveVendorPOValidation" & """:" & _RemoveVendorPOValidation
                strParameterJson = strParameterJson & ",""" & "v_tintBaseTaxOnArea" & """:" & _BaseTaxOnArea
                strParameterJson = strParameterJson & ",""" & "v_bitAmountPastDue" & """:" & _boolAmountPastDue
                strParameterJson = strParameterJson & ",""" & "v_monAmountPastDue" & """:" & _AmountPastDue
                strParameterJson = strParameterJson & ",""" & "v_bitSearchOrderCustomerHistory" & """:" & _SearchOrderCustomerHistory
                strParameterJson = strParameterJson & ",""" & "v_tintCalendarTimeFormat" & """:" & _CalendarTimeFormat
                strParameterJson = strParameterJson & ",""" & "v_tintDefaultClassType" & """:" & _Class
                strParameterJson = strParameterJson & ",""" & "v_tintPriceBookDiscount" & """:" & _PriceBookDiscount
                strParameterJson = strParameterJson & ",""" & "v_bitAutoSerialNoAssign" & """:" & _AutoSerialNoAssign
                strParameterJson = strParameterJson & ",""" & "v_bitIsShowBalance" & """:" & _bitIsShowBalance
                strParameterJson = strParameterJson & ",""" & "v_numIncomeAccID" & """:" & _IncomeAccountID
                strParameterJson = strParameterJson & ",""" & "v_numCOGSAccID" & """:" & _COGSAccountID
                strParameterJson = strParameterJson & ",""" & "v_numAssetAccID" & """:" & _AssetAccountID
                strParameterJson = strParameterJson & ",""" & "v_IsEnableProjectTracking" & """:" & _IsEnableProjectTracking
                strParameterJson = strParameterJson & ",""" & "v_IsEnableCampaignTracking" & """:" & _IsEnableCampaignTracking
                strParameterJson = strParameterJson & ",""" & "v_IsEnableResourceScheduling" & """:" & _IsEnableResourceScheduling
                strParameterJson = strParameterJson & ",""" & "v_ShippingServiceItem" & """:" & _ShippingServiceItem
                strParameterJson = strParameterJson & ",""" & "v_numSOBizDocStatus" & """:" & _SOBizDocStatus
                strParameterJson = strParameterJson & ",""" & "v_bitAutolinkUnappliedPayment" & """:" & _AutolinkUnappliedPayment
                strParameterJson = strParameterJson & ",""" & "v_numDiscountServiceItemID" & """:" & _DiscountServiceItem
                strParameterJson = strParameterJson & ",""" & "v_vcShipToPhoneNumber" & """:""" & _ShipToPhoneNumber & """"
                strParameterJson = strParameterJson & ",""" & "v_vcGAUserEmailId" & """:""" & _GAUserEmailId & """"
                strParameterJson = strParameterJson & ",""" & "v_vcGAUserPassword" & """:""" & _GAUserPassword & """"
                strParameterJson = strParameterJson & ",""" & "v_vcGAUserProfileId" & """:""" & _GAUserProfileId & """"
                strParameterJson = strParameterJson & ",""" & "v_bitDiscountOnUnitPrice" & """:" & _DiscountOnUnitPrice
                strParameterJson = strParameterJson & ",""" & "v_intShippingImageWidth" & """:" & _intImageWidth
                strParameterJson = strParameterJson & ",""" & "v_intShippingImageHeight" & """:" & _intImageHeight
                strParameterJson = strParameterJson & ",""" & "v_numTotalInsuredValue" & """:" & _dcTotalInsuredValue
                strParameterJson = strParameterJson & ",""" & "v_numTotalCustomsValue" & """:" & _dcTotalCustomsValue
                strParameterJson = strParameterJson & ",""" & "v_vcHideTabs" & """:""" & _strHideTabs & """"
                strParameterJson = strParameterJson & ",""" & "v_bitUseBizdocAmount" & """:" & _blnUseBizdocAmount
                strParameterJson = strParameterJson & ",""" & "v_bitDefaultRateType" & """:" & _IsDefaultRateType
                strParameterJson = strParameterJson & ",""" & "v_bitIncludeTaxAndShippingInCommission" & """:" & _IsIncludeTaxAndShippingInCommission
                strParameterJson = strParameterJson & ",""" & "v_bitPurchaseTaxCredit" & """:" & _IsPurchaseTaxCredit
                strParameterJson = strParameterJson & ",""" & "v_bitLandedCost" & """:" & _IsLandedCost
                strParameterJson = strParameterJson & ",""" & "v_vcLanedCostDefault" & """:""" & _LanedCostDefault & """"
                strParameterJson = strParameterJson & ",""" & "v_bitMinUnitPriceRule" & """:" & _IsMinUnitPriceRule
                strParameterJson = strParameterJson & ",""" & "v_numAbovePercent" & """:" & _AbovePercent
                strParameterJson = strParameterJson & ",""" & "v_numBelowPercent" & """:" & _BelowPercent
                strParameterJson = strParameterJson & ",""" & "v_numBelowPriceField" & """:" & _BelowPriceField
                strParameterJson = strParameterJson & ",""" & "v_vcUnitPriceApprover" & """:""" & _UnitPriceApprover & """"
                strParameterJson = strParameterJson & ",""" & "v_numDefaultSalesPricing" & """:" & _numDefaultSalesPricing
                strParameterJson = strParameterJson & ",""" & "v_bitReOrderPoint" & """:" & _bitReOrderPoint
                strParameterJson = strParameterJson & ",""" & "v_numReOrderPointOrderStatus" & """:" & _numReOrderPointOrderStatus
                strParameterJson = strParameterJson & ",""" & "v_numPODropShipBizDoc" & """:" & _numPODropShipBizDoc
                strParameterJson = strParameterJson & ",""" & "v_numPODropShipBizDocTemplate" & """:" & _numPODropShipBizDocTemplate
                strParameterJson = strParameterJson & ",""" & "v_IsEnableDeferredIncome" & """:" & IsEnableDeferredIncome
                strParameterJson = strParameterJson & ",""" & "v_bitApprovalforTImeExpense" & """:" & bitApprovalforTImeExpense
                strParameterJson = strParameterJson & ",""" & "v_numCost" & """:" & numCost
                strParameterJson = strParameterJson & ",""" & "v_intTimeExpApprovalProcess" & """:" & intTimeExpApprovalProcess
                strParameterJson = strParameterJson & ",""" & "v_bitApprovalforOpportunity" & """:" & 0 'Not in use now
                strParameterJson = strParameterJson & ",""" & "v_intOpportunityApprovalProcess" & """:" & intOpportunityApprovalProcess
                strParameterJson = strParameterJson & ",""" & "v_numDefaultSalesShippingDoc" & """:" & numDefaultSalesShippingDoc
                strParameterJson = strParameterJson & ",""" & "v_numDefaultPurchaseShippingDoc" & """:" & numDefaultPurchaseShippingDoc
                strParameterJson = strParameterJson & ",""" & "v_bitchkOverRideAssignto" & """:" & bitchkOverRideAssignto
                strParameterJson = strParameterJson & ",""" & "v_vcPrinterIPAddress" & """:""" & PrinterIPAddress & """"
                strParameterJson = strParameterJson & ",""" & "v_vcPrinterPort" & """:""" & PrinterPort & """"
                strParameterJson = strParameterJson & ",""" & "v_numDefaultSiteID" & """:" & DefaultSite
                strParameterJson = strParameterJson & ",""" & "v_bitRemoveGlobalLocation" & """:" & IsRemoveGlobalWarehouse
                strParameterJson = strParameterJson & ",""" & "v_numAuthorizePercentage" & """:" & numAuthorizePercentage
                strParameterJson = strParameterJson & ",""" & "v_bitEDI" & """:" & bitEDI
                strParameterJson = strParameterJson & ",""" & "v_bit3PL" & """:" & bit3PL
                strParameterJson = strParameterJson & ",""" & "v_bitAllowDuplicateLineItems" & """:" & IsAllowDuplicateLineItems
                strParameterJson = strParameterJson & ",""" & "v_tintCommitAllocation" & """:" & CommitAllocation
                strParameterJson = strParameterJson & ",""" & "v_tintInvoicing" & """:" & InventoryInvoicing
                strParameterJson = strParameterJson & ",""" & "v_tintMarkupDiscountOption" & """:" & tintMarkupDiscountOption
                strParameterJson = strParameterJson & ",""" & "v_tintMarkupDiscountValue" & """:" & tintMarkupDiscountValue
                strParameterJson = strParameterJson & ",""" & "v_bitCommissionBasedOn" & """:" & IsCommissionBasedOn
                strParameterJson = strParameterJson & ",""" & "v_tintCommissionBasedOn" & """:" & CommissionBasedOn
                strParameterJson = strParameterJson & ",""" & "v_bitDoNotShowDropshipPOWindow" & """:" & IsDoNotShowDropshipWindow
                strParameterJson = strParameterJson & ",""" & "v_tintReceivePaymentTo" & """:" & tintReceivePaymentTo
                strParameterJson = strParameterJson & ",""" & "v_numReceivePaymentBankAccount" & """:" & numReceivePaymentBankAccount
                strParameterJson = strParameterJson & ",""" & "v_numOverheadServiceItemID" & """:" & OverheadServiceItem
                strParameterJson = strParameterJson & ",""" & "v_bitDisplayCustomField" & """:" & bitDisplayCustomField
                strParameterJson = strParameterJson & ",""" & "v_bitFollowupAnytime" & """:" & bitFollowupAnytime
                strParameterJson = strParameterJson & ",""" & "v_bitpartycalendarTitle" & """:" & bitpartycalendarTitle
                strParameterJson = strParameterJson & ",""" & "v_bitpartycalendarLocation" & """:" & bitpartycalendarLocation
                strParameterJson = strParameterJson & ",""" & "v_bitpartycalendarDescription" & """:" & bitpartycalendarDescription
                strParameterJson = strParameterJson & ",""" & "v_bitREQPOApproval" & """:" & bitREQPOApproval
                strParameterJson = strParameterJson & ",""" & "v_bitARInvoiceDue" & """:" & bitARInvoiceDue
                strParameterJson = strParameterJson & ",""" & "v_bitAPBillsDue" & """:" & bitAPBillsDue
                strParameterJson = strParameterJson & ",""" & "v_bitItemsToPickPackShip" & """:" & bitItemsToPickPackShip
                strParameterJson = strParameterJson & ",""" & "v_bitItemsToInvoice" & """:" & bitItemsToInvoice
                strParameterJson = strParameterJson & ",""" & "v_bitSalesOrderToClose" & """:" & bitSalesOrderToClose
                strParameterJson = strParameterJson & ",""" & "v_bitItemsToPutAway" & """:" & bitItemsToPutAway
                strParameterJson = strParameterJson & ",""" & "v_bitItemsToBill" & """:" & bitItemsToBill
                strParameterJson = strParameterJson & ",""" & "v_bitPosToClose" & """:" & bitPosToClose
                strParameterJson = strParameterJson & ",""" & "v_bitPOToClose" & """:" & bitPOToClose
                strParameterJson = strParameterJson & ",""" & "v_bitBOMSToPick" & """:" & bitBOMSToPick
                strParameterJson = strParameterJson & ",""" & "v_vchREQPOApprovalEmp" & """:""" & vchREQPOApprovalEmp & """"
                strParameterJson = strParameterJson & ",""" & "v_vchARInvoiceDue" & """:""" & vchARInvoiceDue & """"
                strParameterJson = strParameterJson & ",""" & "v_vchAPBillsDue" & """:""" & vchAPBillsDue & """"
                strParameterJson = strParameterJson & ",""" & "v_vchItemsToPickPackShip" & """:""" & vchItemsToPickPackShip & """"
                strParameterJson = strParameterJson & ",""" & "v_vchItemsToInvoice" & """:""" & vchItemsToInvoice & """"
                strParameterJson = strParameterJson & ",""" & "v_vchPriceMarginApproval" & """:""" & vchPriceMarginApproval & """"
                strParameterJson = strParameterJson & ",""" & "v_vchSalesOrderToClose" & """:""" & vchSalesOrderToClose & """"
                strParameterJson = strParameterJson & ",""" & "v_vchItemsToPutAway" & """:""" & vchItemsToPutAway & """"
                strParameterJson = strParameterJson & ",""" & "v_vchItemsToBill" & """:""" & vchItemsToBill & """"
                strParameterJson = strParameterJson & ",""" & "v_vchPosToClose" & """:""" & vchPosToClose & """"
                strParameterJson = strParameterJson & ",""" & "v_vchPOToClose" & """:""" & vchPOToClose & """"
                strParameterJson = strParameterJson & ",""" & "v_vchBOMSToPick" & """:""" & vchBOMSToPick & """"
                strParameterJson = strParameterJson & ",""" & "v_decReqPOMinValue" & """:" & decReqPOMinValue
                strParameterJson = strParameterJson & ",""" & "v_bitUseOnlyActionItems" & """:" & bitUseOnlyActionItems
                strParameterJson = strParameterJson & ",""" & "v_bitDisplayContractElement" & """:" & bitDisplayContractElement
                strParameterJson = strParameterJson & ",""" & "v_vcEmployeeForContractTimeElement" & """:""" & vcEmployeeForContractTimeElement & """"
                strParameterJson = strParameterJson & ",""" & "v_numARContactPosition" & """:" & ARContactPosition
                strParameterJson = strParameterJson & ",""" & "v_bitShowCardConnectLink" & """:" & IsShowCardConnectLink
                strParameterJson = strParameterJson & ",""" & "v_vcBluePayFormName" & """:""" & BluePayFormName & """"
                strParameterJson = strParameterJson & ",""" & "v_vcBluePaySuccessURL" & """:""" & BluePaySuccessURL & """"
                strParameterJson = strParameterJson & ",""" & "v_vcBluePayDeclineURL" & """:""" & BluePayDeclineURL & """"
                strParameterJson = strParameterJson & ",""" & "v_bitUseDeluxeCheckStock" & """:" & IsUseDeluxeCheckStock
                strParameterJson = strParameterJson & ",""" & "v_bitEnableSmartyStreets" & """:" & IsEnableSmartyStreets
                strParameterJson = strParameterJson & ",""" & "v_vcSmartyStreetsAPIKeys" & """:""" & SmartyStreetsAPIKeys & """"
                strParameterJson = strParameterJson & ",""" & "v_bitUsePreviousEmailBizDoc" & """:" & IsUsePreviousEmailBizDoc
                strParameterJson = strParameterJson & ",""" & "v_bitInventoryInvoicing" & """:" & IsInventoryInvoicing
                strParameterJson = strParameterJson & ",""" & "v_tintCommissionVendorCostType" & """:" & CommissionVendorCostType

                strParameterJson = strParameterJson & "}"
                strParameterJson = strParameterJson.Replace(":False", ":0")
                strParameterJson = strParameterJson.Replace(":True", ":1")


                With sqlParams
                    .Add(SqlDAL.Add_Parameter("p_numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("p_parameterJson", strParameterJson, NpgsqlTypes.NpgsqlDbType.Json))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdatedomainDetails", sqlParams.ToArray())
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function UpdateRentalSetting() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitRentalItem", _boolRentalItem, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numRentalItemClass", _RentalItemClass, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRentalHourlyUOM", _RentalHourlyUOM, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRentalDailyUOM", _RentalDailyUOM, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintRentalPriceBasedOn", _RentalPriceBasedOn, NpgsqlTypes.NpgsqlDbType.Smallint))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdatedomainRentalSetting", sqlParams.ToArray())
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateElectiveItemFields() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcElectiveItemFields", _vcElectiveItemFields, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageElectiveItemFields", sqlParams.ToArray())
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function UpdateDomainDefaultAddress() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@bitAutoPopulateAddress", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _boolAutoPopulateAddress

                arParms(2) = New Npgsql.NpgsqlParameter("@tintPoulateAddressTo", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _PoulateAddressTo

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdatedomainDefaultContactAddress", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateDomainGoogleAnalyticsConfig() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcGAUserEmailId", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _GAUserEmailId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcGAUserPassword", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _GAUserPassword

                arParms(3) = New Npgsql.NpgsqlParameter("@vcGAUserProfileId", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _GAUserProfileId

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdatedomainGoogleAnalyticsConfig", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function UpdateApprovalProcessItemClassification() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numApprovalRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _approvalRuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numListItemId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numAbovePercent", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Value = _AbovePercent

                arParms(4) = New Npgsql.NpgsqlParameter("@numBelowPercent", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _BelowPercent

                arParms(5) = New Npgsql.NpgsqlParameter("@numBelowPriceField", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = _BelowPriceField

                arParms(6) = New Npgsql.NpgsqlParameter("@bitCostApproval", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = IsCostApproval

                arParms(7) = New Npgsql.NpgsqlParameter("@bitListPriceApproval", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = IsListPriceApproval

                arParms(8) = New Npgsql.NpgsqlParameter("@bitMarginPriceViolated", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = _bitMarginPriceViolated

                arParms(9) = New Npgsql.NpgsqlParameter("@vcUnitPriceApprover", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(9).Value = _UnitPriceApprover

                arParms(10) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(10).Value = _byteMode

                arParms(11) = New Npgsql.NpgsqlParameter("@IsMinUnitPriceRule", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _IsMinUnitPriceRule



                SqlDAL.ExecuteNonQuery(connString, "USP_UpdatedomainMinimumPriceUpdate", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function UpdateDomainAutoCreatePOConfig() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@bitReOrderPoint", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _bitReOrderPoint

                arParms(2) = New Npgsql.NpgsqlParameter("@numReOrderPointOrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _numReOrderPointOrderStatus

                arParms(3) = New Npgsql.NpgsqlParameter("@tintOppStautsForAutoPOBackOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = CreateOppStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@tintUnitsRecommendationForAutoPOBackOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = UnitRecommendationType

                arParms(5) = New Npgsql.NpgsqlParameter("@bitIncludeRequisitions", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = IsIncludeRequisitions

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdatedomainAutoCreatePOConfig", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateDomainChangeDefaultAccount() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numIncomeAccID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _IncomeAccountID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCOGSAccID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _COGSAccountID

                arParms(3) = New Npgsql.NpgsqlParameter("@numAssetAccID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _AssetAccountID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateDomainChangeDefaultAccount", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function UpdateECommSettings() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcPaymentGateWay", _PaymentGateWay, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcPGWUserId", _PGWUserId, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@vcPGWPassword", _PGWPassword, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@bitShowInStock", _boolShowInStock, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitShowQOnHand", _boolShowOnhand, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitCheckCreditStatus", _boolCheckCreditStatus, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numDefaultWareHouseID", _WareHouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRelationshipId", _Relationship, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProfileId", _ProfileID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitHidePriceBeforeLogin", _EnableHidePrice, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitAuthOnlyCreditCard", _AuthOnlyCreditCard, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitSendMail", EmailStatus, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcGoogleMerchantID", GoogleMerchantID, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcGoogleMerchantKey", GoogleMerchantKey, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@IsSandbox", IsSandbox, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numAuthrizedOrderStatus", _OrderStatusID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", _SiteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcPaypalUserName", _PaypalUserName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcPaypalPassword", _PaypalPassword, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcPaypalSignature", _PaypalSignature, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@IsPaypalSandbox", _IsPaypalSandbox, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitSkipStep2", _IsSkipStep2, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitDisplayCategory", _IsDisplayCategories, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitShowPriceUsingPriceLevel", _IsShowPriceUsingPriceLevel, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitEnableSecSorting", _IsEnableSorting, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitSortPriceMode", _IsChangeSortPriceMode, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numPageSize", _numPageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numPageVariant", _numPageVariant, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@bitAutoSelectWarehouse", _boolAutoSelectWarehouse, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitPreSellUp", _bitPreUpSell, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitPostSellUp", _bitPostUpSell, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numDefaultClass", DefaultClass, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcPreSellUp", vcPreSellUp, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcPostSellUp", vcPostSellUp, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcRedirectThankYouUrl", _vcRedirectThankYouUrl, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                    .Add(SqlDAL.Add_Parameter("@tintPreLoginProceLevel", PreLoginPriceLevel, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@bitHideAddtoCart", HideAddtoCart, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitShowPromoDetailsLink", IsShowPromoDetailsLink, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@tintWarehouseAvailability", WareHouseAvailability, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@bitDisplayQtyAvailable", DisplayQtyAvailable, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcSalesOrderTabs", vcSalesOrderTabs, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcSalesQuotesTabs", vcSalesQuotesTabs, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcItemPurchaseHistoryTabs", vcItemPurchaseHistoryTabs, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcItemsFrequentlyPurchasedTabs", vcItemsFrequentlyPurchasedTabs, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcOpenCasesTabs", vcOpenCasesTabs, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcOpenRMATabs", vcOpenRMATabs, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitSalesOrderTabs", bitSalesOrderTabs, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitSalesQuotesTabs", bitSalesQuotesTabs, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitItemPurchaseHistoryTabs", bitItemPurchaseHistoryTabs, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitItemsFrequentlyPurchasedTabs", bitItemsFrequentlyPurchasedTabs, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitOpenCasesTabs", bitOpenCasesTabs, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitOpenRMATabs", bitOpenRMATabs, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitSupportTabs", bitSupportTabs, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcSupportTabs", vcSupportTabs, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitElasticSearch", bitElasticSearch, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_EcommerceSettings", sqlParams.ToArray())
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function UpdateECommHeaderFooter() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strText", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _xmlStr

                arParms(2) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode


                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateEcommHeaderFooter", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDomainDetailsForPriceMargin() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _numListItemId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetDomainDetailsForPriceMargin", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetDomainDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetDomainDetails", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFromEmailDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GETEmailFromDetail", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function



        Public Function GetECommerceDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SiteID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetECommerceDetails", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function ManageEComPaymentConfig(ByVal siteid As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = siteid

                arParms(2) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Xml)
                arParms(2).Value = _strItems

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_ManageEComPaymentConfig", arParms).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPaymentGatewayDTLs() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                Return SqlDAL.ExecuteDataset(connString, "USP_PaymentGateWayBsdDomainID", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function SaveImagePathDetails() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcImagePath", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _ImagePath

                arParms(2) = New Npgsql.NpgsqlParameter("@sintbyteMode", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(2).Value = _byteMode

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveImagePath", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Added By: Sachin sadhu||Date:23rdDec2013
        'Purpose:To Save different  Logo  for bizDocs
        Public Function BizDocSaveLogo(ByVal intDocTemplateId As Int32) As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = intDocTemplateId



                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcImagePath", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _ImagePath

                arParms(3) = New Npgsql.NpgsqlParameter("@sintbyteMode", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(3).Value = _byteMode

                SqlDAL.ExecuteNonQuery(connString, "USP_BizDocTemplate_ManageLogo", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetImagePath() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcBizDocImagePath", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Direction = ParameterDirection.InputOutput
                arParms(1).Value = _BizDocImagePath

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCompanyImagePath", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _CompanyImagePath

                arParms(3) = New Npgsql.NpgsqlParameter("@vcBankImagePath", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _BankImagePath

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetImagePath", objParam, True)
                _BizDocImagePath = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value)
                _CompanyImagePath = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
                _BankImagePath = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub updateProfileListOrder()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@xmlStr", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _xmlStr

                SqlDAL.ExecuteNonQuery(connString, "USP_updateProfileListOrder", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function GetImapDtls() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID '

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CType(SqlDAL.ExecuteDataset(connString, "usp_GetImapDTl", arParms), DataSet).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetImapEnabledUsers(ByVal Mode As Short, Optional ByVal numDomainId As Long = 0) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@tinyMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = Mode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 0)
                arParms(1).Value = numDomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CType(SqlDAL.ExecuteDataset(connString, "usp_GetImapEnabledUsers", arParms), DataSet).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub ManageImapDtls()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcImapPassword", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(0).Value = _ImapPassWord

                arParms(1) = New Npgsql.NpgsqlParameter("@vcImapServerUrl", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(1).Value = _ImapServerUrl

                arParms(2) = New Npgsql.NpgsqlParameter("@numImapSSLPort", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ImapSSLPort

                arParms(3) = New Npgsql.NpgsqlParameter("@bitImapSsl", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _ImapSSL

                arParms(4) = New Npgsql.NpgsqlParameter("@bitImap", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _ImapIntegration

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _ContactID

                arParms(6) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = DomainID

                arParms(7) = New Npgsql.NpgsqlParameter("@bitUseUserName", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _boolUseUserName

                arParms(8) = New Npgsql.NpgsqlParameter("@numLastUID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(8).Value = _LastUID

                arParms(9) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = _byteMode

                arParms(10) = New Npgsql.NpgsqlParameter("@vcImapUserName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(10).Value = _UserName

                SqlDAL.ExecuteNonQuery(connString, "USP_InsertImapUserDtls", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub DisableImap()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DisableImap", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub DisableSMTP()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DisableSMTP", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function TransferOwnerAssigneAdv() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@xmlStr", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = _xmlStr

                arParms(2) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_TransferOwnerAssigneAdv", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CreateCommissionsContacts() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_CreateCommissionsContacts", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCommissionsContacts() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CType(SqlDAL.ExecuteDataset(connString, "usp_GetCommissionsContacts", arParms), DataSet).Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteCommissionsContacts() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTerID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numCommContactID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteCommissionsContacts", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CheckCommissionAppliesTo() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintComAppliesTo", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _tintComAppliesTo

                arParms(2) = New Npgsql.NpgsqlParameter("@tintCommissionType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _CommissionType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_CheckCommissionAppliesTo", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetExtranetUserDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetExtranetUserDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub ManageGoogleContactGroups()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _UserId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _GroupType

                arParms(3) = New Npgsql.NpgsqlParameter("@vcGroupId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _GoogleGroupID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _byteMode

                SqlDAL.ExecuteNonQuery(connString, "USP_InsertGoogleContactGroups", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetGoogleContactGroups() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _UserId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _GroupType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_SelectGoogleContactGroups", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub UpdateGoogleRefreshToken(ByVal GoogleRefreshToken As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcGoogleRefreshToken", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = GoogleRefreshToken

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateGoogleRefreshToken", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub UpdateMailRefreshToken(ByVal mailProvider As Short, ByVal mailAccessToken As String, ByVal mailRefreshToken As String, ByVal expiresIn As Integer)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMailProvider", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = mailProvider

                arParms(3) = New Npgsql.NpgsqlParameter("@vcEmail", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = Email

                arParms(4) = New Npgsql.NpgsqlParameter("@vcMailAccessToken", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = mailAccessToken

                arParms(5) = New Npgsql.NpgsqlParameter("@vcMailRefreshToken", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(5).Value = mailRefreshToken

                arParms(6) = New Npgsql.NpgsqlParameter("@intExpireInSeconds", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = expiresIn

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateMailRefreshToken", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub UpdateDefaultWarehouse()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDefaultWarehouse", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DefaultWarehouse

                SqlDAL.ExecuteNonQuery(connString, "USP_UserMaster_UpdateDefaultWarehouse", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetUserSMTPDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetUserSMTPDetails", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageUserSMTPDetails() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _UserId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSMTPUserName", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(2).Value = _UserName

                arParms(3) = New Npgsql.NpgsqlParameter("@vcSMTPPassword", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(3).Value = _SMTPPassWord

                arParms(4) = New Npgsql.NpgsqlParameter("@vcSMTPServer", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(4).Value = _SMTPServer

                arParms(5) = New Npgsql.NpgsqlParameter("@numSMTPPort", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _SMTPPort

                arParms(6) = New Npgsql.NpgsqlParameter("@bitSMTPSSL", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _SMTPSSL

                arParms(7) = New Npgsql.NpgsqlParameter("@bitSMTPServer", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _bitSMTPServer

                arParms(8) = New Npgsql.NpgsqlParameter("@bitSMTPAuth", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = _SMTPAuth

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageUserSMTPDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateShipPhoneNumber() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID
                arParms(1) = New Npgsql.NpgsqlParameter("@vcShipToPhoneNumber", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _ShipToPhoneNumber
                SqlDAL.ExecuteNonQuery(connString, "USP_Domain_UpdateShipPhoneNumber", arParms)
                Return True
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetDomainPrinterDetail() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Domain_GetPrinterDetail", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub UpdateDefaultDashboardTemplate(ByVal dashboardTemplateID As Long, ByVal vcDashboardTemplateIDs As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserID", UserId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTemplateID", dashboardTemplateID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcDashboardTemplateIDs", vcDashboardTemplateIDs, NpgsqlTypes.NpgsqlDbType.VarChar))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UserMaster_ChangeDashboardTemplate", sqlParams.ToArray(), True)
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Sub UpdateDefaultDashboardTemplate(ByVal dashboardTemplateID As Long)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTemplateID", dashboardTemplateID, NpgsqlTypes.NpgsqlDbType.BigInt))


                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UserMaster_UpdateDashboardTemplate", sqlParams.ToArray(), True)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function IsValidResetPasswordLink() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcResetID", Password, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return Convert.ToBoolean(SqlDAL.ExecuteScalar(connString, "USP_ExtranetAccountsDtl_ValidateResetLink", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub ResetPassword()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcResetID", Password, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcNewPassword", NewPassword, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ExtranetAccountsDtl_ResetPassword", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub UpdateUserEmailAlias()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserID", UserId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcEmailAlias", strEmail, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcSMTPPassword", SMTPPassWord, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UserMaster_UpdateEmailAlias", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub UpdateActiveUsers()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcUsers", xmlStr, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_UserMaster_UpdateActiveUsers", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Function GetFieldValue(ByVal fieldName As String) As Object
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcFieldName", fieldName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_UserMaster_GetFieldValue", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function GetMailRefreshToken() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetMailRefreshToken", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
End Namespace
