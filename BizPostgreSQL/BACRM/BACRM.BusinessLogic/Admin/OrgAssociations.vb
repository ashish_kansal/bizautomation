''' -----------------------------------------------------------------------------
''' Project	 : BACRM.BusinessLogic
''' Class	 : OrgAssociations
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     Contains important global functionality regarding Organization Associations.
''' </summary>
''' <remarks>
''' </remarks>
''' <history>
''' 	[Debasish Tapan Nag]	01/18/2006	Created
''' </history>
''' -----------------------------------------------------------------------------
Option Explicit On 
Imports System.Data.SqlClient
Imports System.IO
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Admin

    Public Class OrgAssociations
        Inherits BACRM.BusinessLogic.CBusinessBase
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Division ID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the Division ID.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _DivisionID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the  Division ID.
        ''' </summary>
        ''' <value>Returns the Division id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Division ID.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to return the parent Org of which the current org is a subsidary.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the org of which this org is a subsidary.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getParentOrgForCurrentOrg() As DataTable
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New getconnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numAssociateFromDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetParentOrgForCurrentOrg", arParms) 'execute and store to Datatable
                Return ds.Tables(ds.Tables.Count - 1)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace

