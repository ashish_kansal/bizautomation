﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin
    Public Class SalesFulfillmentConfiguration
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public SFCID As Long
        Public IsActive As Boolean
        Public IsRule1Active As Boolean
        Public Rule1BizDoc As Long
        Public Rule1Type As Short '1 = When Order Created, 2 = When Order Status Changed To
        Public Rule1OrderStatus As Long
        Public IsRule2Active As Boolean
        Public Rule2OrderStatus As Long
        Public Rule2SuccessOrderStatus As Long
        Public Rule2FailOrderStatus As Long
        Public IsRule3Active As Boolean
        Public Rule3OrderStatus As Long
        Public Rule3FailOrderStatus As Long
        Public IsRule4Active As Boolean
        Public Rule4OrderStatus As Long
        Public Rule4FailOrderStatus As Long
        Public IsRule5Active As Boolean
        Public Rule5OrderStatus As Long
        Public IsRule6Active As Boolean
        Public Rule6OrderStatus As Long
#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(20) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@bitActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = IsActive

                arParms(3) = New Npgsql.NpgsqlParameter("@bitRule1IsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = IsRule1Active

                arParms(4) = New Npgsql.NpgsqlParameter("@numRule1BizDoc", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = Rule1BizDoc

                arParms(5) = New Npgsql.NpgsqlParameter("@tintRule1Type", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = Rule1Type

                arParms(6) = New Npgsql.NpgsqlParameter("@numRule1OrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = Rule1OrderStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@bitRule2IsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = IsRule2Active

                arParms(8) = New Npgsql.NpgsqlParameter("@numRule2OrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = Rule2OrderStatus

                arParms(9) = New Npgsql.NpgsqlParameter("@numRule2SuccessOrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = Rule2SuccessOrderStatus

                arParms(10) = New Npgsql.NpgsqlParameter("@numRule2FailOrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = Rule2FailOrderStatus

                arParms(11) = New Npgsql.NpgsqlParameter("@bitRule3IsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = IsRule3Active

                arParms(12) = New Npgsql.NpgsqlParameter("@numRule3OrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = Rule3OrderStatus

                arParms(13) = New Npgsql.NpgsqlParameter("@numRule3FailOrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = Rule3FailOrderStatus

                arParms(14) = New Npgsql.NpgsqlParameter("@bitRule4IsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = IsRule4Active

                arParms(15) = New Npgsql.NpgsqlParameter("@numRule4OrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = Rule4OrderStatus

                arParms(16) = New Npgsql.NpgsqlParameter("@numRule4FailOrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = Rule4FailOrderStatus

                arParms(17) = New Npgsql.NpgsqlParameter("@bitRule5IsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(17).Value = IsRule5Active

                arParms(18) = New Npgsql.NpgsqlParameter("@numRule5OrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = Rule5OrderStatus

                arParms(19) = New Npgsql.NpgsqlParameter("@bitRule6IsActive", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(19).Value = IsRule6Active

                arParms(20) = New Npgsql.NpgsqlParameter("@numRule6OrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(20).Value = Rule6OrderStatus

                SqlDAL.ExecuteNonQuery(connString, "USP_SalesFulfillmentConfiguration_Save", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetByDomainID() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_SalesFulfillmentConfiguration_GetByDomainID", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function


#End Region

    End Class
End Namespace
