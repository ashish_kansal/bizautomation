﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin
    Public Class ListDetailsName
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property ListID As Long
        Public Property ListItemID As Long
        Public Property Name As String

#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ListID

                arParms(2) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ListItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = Name

                SqlDAL.ExecuteDataset(connString, "USP_ListDetailsName_Save", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class
End Namespace