'Created Anoop Jayaraj
Option Explicit On 
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin
    Public Class Tabs
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _UserId As Long
        Private _ListId As Long
        Private _GroupID As Long
        'Private DomainId As Long
        Private _RelationshipID As Long
        'Private _ModuleId As Long
        Private _TabType As Integer
        Private _TabID As Long
        Private _TabURL As String
        Private _TabName As String
        Private _mode As Boolean


        Private _intMode As Short
        Public Property intMode() As Short
            Get
                Return _intMode
            End Get
            Set(ByVal value As Short)
                _intMode = value
            End Set
        End Property

        Public Property mode() As Boolean
            Get
                Return _mode
            End Get
            Set(ByVal Value As Boolean)
                _mode = Value
            End Set
        End Property

        Private _ProfileID As Long
        Public Property ProfileID() As Long
            Get
                Return _ProfileID
            End Get
            Set(ByVal value As Long)
                _ProfileID = value
            End Set
        End Property

        'Public Property ModuleId() As Long
        '    Get
        '        Return _ModuleId

        '    End Get
        '    Set(ByVal Value As Long)
        '        _ModuleId = Value
        '    End Set
        'End Property

        Public Property TabName() As String
            Get
                Return _TabName
            End Get
            Set(ByVal Value As String)
                _TabName = Value
            End Set
        End Property

        Public Property TabURL() As String
            Get
                Return _TabURL
            End Get
            Set(ByVal Value As String)
                _TabURL = Value
            End Set
        End Property

        Public Property TabID() As Long
            Get
                Return _TabID
            End Get
            Set(ByVal Value As Long)
                _TabID = Value
            End Set
        End Property

        Public Property TabType() As Integer
            Get
                Return _TabType
            End Get
            Set(ByVal Value As Integer)
                _TabType = Value
            End Set
        End Property

        Public Property RelationshipID() As Long
            Get
                Return _RelationshipID
            End Get
            Set(ByVal Value As Long)
                _RelationshipID = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property GroupID() As Long
            Get
                Return _GroupID
            End Get
            Set(ByVal Value As Long)
                _GroupID = Value
            End Set
        End Property

        Public Property ListId() As Long
            Get
                Return _ListId
            End Get
            Set(ByVal Value As Long)
                _ListId = Value
            End Set
        End Property

        Public Property UserId() As Long
            Get
                Return _UserId
            End Get
            Set(ByVal Value As Long)
                _UserId = Value
            End Set
        End Property

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop  Jayaraj 	DATE:30-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop  Jayaraj 	DATE:30-March-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region


        Public Function GetTabData() As DataTable
            Try
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GroupID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRelationShip", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _RelationshipID

                arParms(2) = New Npgsql.NpgsqlParameter("@numProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ProfileID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = If(_intMode = Nothing, 0, 1)

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAllTabbedData", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetTabMenuItem() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ListId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@bitMode", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _mode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetTabMenu", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetTabDomainSpecific() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@tintTabType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _TabType

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_TabMasterDTLs", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageMasterTabs() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TabID

                arParms(1) = New Npgsql.NpgsqlParameter("@numTabName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(1).Value = _TabName

                arParms(2) = New Npgsql.NpgsqlParameter("@tintTabType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _TabType

                arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(3).Value = _TabURL


                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainId

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageMasterTabs", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Public Function DeleteMasterTabs() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TabID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteMasterTabs", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetFirstPage() As String
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _GroupID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRelationShip", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RelationshipID

                arParms(3) = New Npgsql.NpgsqlParameter("@numProfileID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProfileID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_GetFirstPage", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetMailCount() As Long
            Dim lngResult As Long = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                lngResult = CLng(SqlDAL.ExecuteScalar(connString, "Usp_GetMailCount", arParms))

            Catch ex As Exception
                ' Log exception details
                lngResult = 0
                Throw ex
            End Try

            Return lngResult

        End Function

    End Class
End Namespace