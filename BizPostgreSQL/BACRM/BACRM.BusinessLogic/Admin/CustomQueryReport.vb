﻿Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Public Class CustomQueryReport
    Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"
    Public Property ReportID As Long
    Public Property ReportDomainID As Long
    Public Property ReportName As String
    Public Property ReportDescription As String
    Public Property EmailTo As String
    Public Property EmailFrequency As Int16
    Public Property Query As String
    Public Property CSS As String
    Public Property EmailDate As Date
#End Region

#Region "Public Methods"
    Public Sub Save()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numReportID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = ReportID

            arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = ReportDomainID

            arParms(2) = New Npgsql.NpgsqlParameter("@vcReportName", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(2).Value = ReportName

            arParms(3) = New Npgsql.NpgsqlParameter("@vcReportDescription", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(3).Value = ReportDescription

            arParms(4) = New Npgsql.NpgsqlParameter("@vcEmailTo", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(4).Value = EmailTo

            arParms(5) = New Npgsql.NpgsqlParameter("@tintEmailFrequency", NpgsqlTypes.NpgsqlDbType.Smallint)
            arParms(5).Value = EmailFrequency

            arParms(6) = New Npgsql.NpgsqlParameter("@vcQuery", NpgsqlTypes.NpgsqlDbType.Text)
            arParms(6).Value = Query

            arParms(7) = New Npgsql.NpgsqlParameter("@vcCSS", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(7).Value = CSS

            SqlDAL.ExecuteNonQuery(connString, "USP_CustomQueryReport_Save", arParms)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub Delete()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numReportID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = ReportID

            SqlDAL.ExecuteNonQuery(connString, "USP_CustomQueryReport_Delete", arParms)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub SaveEmailChanges()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numReportID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = ReportID

            arParms(1) = New Npgsql.NpgsqlParameter("@vcReportName", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(1).Value = ReportName

            arParms(2) = New Npgsql.NpgsqlParameter("@vcReportDescription", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(2).Value = ReportDescription

            arParms(3) = New Npgsql.NpgsqlParameter("@vcEmailTo", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(3).Value = EmailTo

            SqlDAL.ExecuteNonQuery(connString, "USP_CustomQueryReport_SaveEmailChanges", arParms)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub LogEmailNotification()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numReportID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = ReportID

            arParms(1) = New Npgsql.NpgsqlParameter("@tintEmailFrequency", NpgsqlTypes.NpgsqlDbType.Smallint)
            arParms(1).Value = EmailFrequency

            arParms(2) = New Npgsql.NpgsqlParameter("@dtDateToSend", NpgsqlTypes.NpgsqlDbType.Date)
            arParms(2).Value = EmailDate

            SqlDAL.ExecuteNonQuery(connString, "USP_CustomQueryReport_LogEmailNotification", arParms)
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

    Public Function Execute() As DataTable
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numReportID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = ReportID

            Return SqlDAL.ExecuteDatable(connString, "USP_CustomQueryReport_Execute", arParms)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GetAll() As DataTable
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(0).Value = Nothing
            arParms(0).Direction = ParameterDirection.InputOutput

            Return SqlDAL.ExecuteDatable(connString, "USP_CustomQueryReport_GetAll", arParms)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GetByID() As CustomQueryReport
        Try
            Dim objCustomQueryReport As New CustomQueryReport

            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numReportID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = ReportID

            arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(1).Value = Nothing
            arParms(1).Direction = ParameterDirection.InputOutput

            Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_CustomQueryReport_GetByID", arParms)


            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                objCustomQueryReport.ReportID = Convert.ToInt64(If(dt.Rows(0)("numReportID") Is DBNull.Value, 0, dt.Rows(0)("numReportID")))
                objCustomQueryReport.ReportDomainID = Convert.ToInt64(If(dt.Rows(0)("numDomainID") Is DBNull.Value, 0, dt.Rows(0)("numDomainID")))
                objCustomQueryReport.ReportName = Convert.ToString(If(dt.Rows(0)("vcReportName") Is DBNull.Value, String.Empty, dt.Rows(0)("vcReportName")))
                objCustomQueryReport.ReportDescription = Convert.ToString(If(dt.Rows(0)("vcReportDescription") Is DBNull.Value, String.Empty, dt.Rows(0)("vcReportDescription")))
                objCustomQueryReport.EmailTo = Convert.ToString(If(dt.Rows(0)("vcEmailTo") Is DBNull.Value, String.Empty, dt.Rows(0)("vcEmailTo")))
                objCustomQueryReport.EmailFrequency = Convert.ToInt16(If(dt.Rows(0)("tintEmailFrequency") Is DBNull.Value, 0, dt.Rows(0)("tintEmailFrequency")))
                objCustomQueryReport.Query = Convert.ToString(If(dt.Rows(0)("vcQuery") Is DBNull.Value, String.Empty, dt.Rows(0)("vcQuery")))
                objCustomQueryReport.CSS = Convert.ToString(If(dt.Rows(0)("vcCSS") Is DBNull.Value, String.Empty, dt.Rows(0)("vcCSS")))
            End If

            Return objCustomQueryReport
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GetByDomainID() As DataTable
        Try
            Dim objCustomQueryReport As New CustomQueryReport

            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = ReportDomainID

            arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(1).Value = Nothing
            arParms(1).Direction = ParameterDirection.InputOutput

            Return SqlDAL.ExecuteDatable(connString, "USP_CustomQueryReport_GetByDomainID", arParms)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GetForEmailReport() As DataTable
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString

            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(0).Value = Nothing
            arParms(0).Direction = ParameterDirection.InputOutput

            Return SqlDAL.ExecuteDatable(connString, "USP_CustomQueryReport_EmailReport", arParms)
        Catch ex As Exception
            Throw
        End Try
    End Function
#End Region

End Class
