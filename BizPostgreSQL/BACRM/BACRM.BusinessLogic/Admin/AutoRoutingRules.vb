Option Explicit On
Option Strict On
Imports System.Data.SqlClient
Imports System.IO
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Admin

    Public Class AutoRoutingRules
        Inherits BACRM.BusinessLogic.CBusinessBase

        'Private UserCntID As Long
        Private _SelValue As Long
        Private _strValues As String
        Private _strDistributionList As String
        Private _vcDBCoulmnName As String
        Private _vcSelectedField As String

        Public Property vcSelectedField() As String
            Get
                Return _vcSelectedField
            End Get
            Set(ByVal Value As String)
                _vcSelectedField = Value
            End Set
        End Property

        Public Property vcDBCoulmnName() As String
            Get
                Return _vcDBCoulmnName
            End Get
            Set(ByVal Value As String)
                _vcDBCoulmnName = Value
            End Set
        End Property

        Public Property strDistributionList() As String
            Get
                Return _strDistributionList
            End Get
            Set(ByVal Value As String)
                _strDistributionList = Value
            End Set
        End Property
        Public Property strValues() As String
            Get
                Return _strValues
            End Get
            Set(ByVal Value As String)
                _strValues = Value
            End Set
        End Property
        Public Property SelValue() As Long
            Get
                Return _SelValue
            End Get
            Set(ByVal Value As Long)
                _SelValue = Value
            End Set
        End Property
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Private _Priority As Short
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Priority. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property Priority() As Short
            Get
                Return _Priority
            End Get
            Set(ByVal Value As Short)
                _Priority = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the userID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the user id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _UserID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User id.
        ''' </summary>
        ''' <value>Returns the user id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the User id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property UserID() As Long
            Get
                Return _UserID
            End Get
            Set(ByVal Value As Long)
                _UserID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Routing Rule ID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the _Routing Rule ID.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _RoutingRuleID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the  Routing Rule ID.
        ''' </summary>
        ''' <value>Returns the user id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Routing Rule ID.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property RoutingRuleID() As Long
            Get
                Return _RoutingRuleID
            End Get
            Set(ByVal Value As Long)
                _RoutingRuleID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the form id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the form id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _FormID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FormID() As Long
            Get
                Return _FormID
            End Get
            Set(ByVal Value As Long)
                _FormID = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the xml file path to the list of default options.
        ''' </summary>
        ''' <remarks>
        '''     This holds the filepath.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/22/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _sXMLFilePath As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the XML File Path which stores the options for IF drop down.
        ''' </summary>
        ''' <value>Returns the sXMLFilePath as String.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the sXMLFilePath.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/22/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property sXMLFilePath() As String
            Get
                Return _sXMLFilePath
            End Get
            Set(ByVal Value As String)
                _sXMLFilePath = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Indicates if the equal to flag is checked or not.
        ''' </summary>
        ''' <remarks>
        '''     This holds the bitEqualTo flag.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/22/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _tintEqualTo As Short
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the  bit equal to flag.
        ''' </summary>
        ''' <value>Returns the bit equal to as boolean.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the bit equal to .
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/23/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property tintEqualTo() As Short
            Get
                Return _tintEqualTo
            End Get
            Set(ByVal Value As Short)
                _tintEqualTo = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the name of the routing rule.
        ''' </summary>
        ''' <remarks>
        '''     This holds the name of the routing rule.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/23/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _vcRoutName As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the name of the routing rule.
        ''' </summary>
        ''' <value>Returns the name of the routing rule.</value>
        ''' <remarks>
        '''     This property is used to Set and Get routing rule name.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/23/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property vcRoutName() As String
            Get
                Return _vcRoutName
            End Get
            Set(ByVal Value As String)
                _vcRoutName = Value
            End Set
        End Property



        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the Auto Routing Rules from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the list
        '''     of auto routing rules.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getRoutingRulesList() As DataTable
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAutoRoutingRuleList", arParms)  'execute and store to dataset

                Return ds.Tables(0)                                                 'return datatable
            Catch ex As Exception
                Throw ex
            End Try

        End Function


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to delete the selected routing rule.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure to delete teh auto routing rules.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/28/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function deleteRule() As DataTable
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = RoutingRuleID
                SqlDAL.ExecuteNonQuery(connString, "usp_DeleteRule", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for Default Routing Rules from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataSet.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the Rule details
        '''     in a dataset
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/03/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getDefaultRoutingRuleDetails() As DataSet
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = RoutingRuleID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetDefaultAutoRoutingRuleDetail", arParms)  'execute and store to dataset
                ds.Tables(0).TableName = "RuleDetails"                              'set the tablename for rule details
                Return ds                                                           'return dataSet
            Catch ex As Exception
                Throw ex
            End Try

        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values available employees from the database.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the available employee list
        '''     in a datatable
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getAvailableEmployees() As DataTable
            Try
                Dim ds As DataSet                                                   'declare a dataset
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = RoutingRuleID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetCircularDistribution", arParms)  'execute and store to dataset

                Return ds.Tables(0)                                                 'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to update an existing Routing Rules in thedatabase.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure to update a rule details
        '''     in a database
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/23/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function UpdateRoutingRule() As Boolean
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRoutID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _RoutingRuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcRoutName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = vcRoutName

                arParms(3) = New Npgsql.NpgsqlParameter("@tintEqualTo", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _tintEqualTo

                arParms(4) = New Npgsql.NpgsqlParameter("@numValue", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _SelValue

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = UserCntID


                arParms(6) = New Npgsql.NpgsqlParameter("@tintPriority", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _Priority

                arParms(7) = New Npgsql.NpgsqlParameter("@strValues", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(7).Value = _strValues

                arParms(8) = New Npgsql.NpgsqlParameter("@strDistribitionList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(8).Value = _strDistributionList

                arParms(9) = New Npgsql.NpgsqlParameter("@vcDBCoulmnName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(9).Value = _vcDBCoulmnName

                arParms(10) = New Npgsql.NpgsqlParameter("@vcSelectedColumn", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(10).Value = _vcSelectedField

                SqlDAL.ExecuteNonQuery(connString, "usp_UpdateExistingRoutingRule", arParms)  'execute the stored procedure

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to insert a new Routing Rules in the database.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the stored procedure to update a rule details
        '''     in a database
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/23/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function InsertRoutingRule() As Long
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcRoutName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = vcRoutName

                arParms(2) = New Npgsql.NpgsqlParameter("@tintEqualTo", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _tintEqualTo

                arParms(3) = New Npgsql.NpgsqlParameter("@numValue", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _SelValue

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintPriority", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _Priority

                arParms(6) = New Npgsql.NpgsqlParameter("@strValues", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(6).Value = _strValues

                arParms(7) = New Npgsql.NpgsqlParameter("@strDistribitionList", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(7).Value = _strDistributionList

                arParms(8) = New Npgsql.NpgsqlParameter("@vcDBCoulmnName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(8).Value = _vcDBCoulmnName

                arParms(9) = New Npgsql.NpgsqlParameter("@vcSelectedColumn", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(9).Value = _vcSelectedField

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                RoutingRuleID = CInt(SqlDAL.ExecuteScalar(connString, "usp_InsertRoutingRule", arParms))  'execute the stored procedure

                Return RoutingRuleID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetLeadBoxfields() As DataTable
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetLeadboxFields", arParms).Tables(0)  'execute the stored procedure

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetAutoroutDTLs() As DataSet
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRoutID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _RoutingRuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "GetAutoRoutRulesData", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRecordOwner() As Long
            Try
                Dim getconnection As New GetConnection                              'declare a connection
                Dim connString As String = getconnection.GetConnectionString        'get the conneciton string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@strData", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strValues

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetRecOwnerUsingRules", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function


    End Class
End Namespace

