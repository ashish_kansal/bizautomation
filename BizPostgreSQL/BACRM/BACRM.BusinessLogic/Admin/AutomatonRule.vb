﻿Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Alerts

Namespace BACRM.BusinessLogic.Admin
    Public Class AutomatonRule
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _RuleID As Long
        Public Property RuleID() As Long
            Get
                Return _RuleID
            End Get
            Set(ByVal Value As Long)
                _RuleID = Value
            End Set
        End Property

        'Private DomainId As Long
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Private _RuleModuleType As Short
        Public Property RuleModuleType() As Short
            Get
                Return _RuleModuleType
            End Get
            Set(ByVal value As Short)
                _RuleModuleType = value
            End Set
        End Property

        Private _RuleName As String
        Public Property RuleName() As String
            Get
                Return _RuleName
            End Get
            Set(ByVal Value As String)
                _RuleName = Value
            End Set
        End Property

        Private _RuleDescription As String
        Public Property RuleDescription() As String
            Get
                Return _RuleDescription
            End Get
            Set(ByVal value As String)
                _RuleDescription = value
            End Set
        End Property

        Private _RuleBasedOn As Short
        Public Property RuleBasedOn() As Short
            Get
                Return _RuleBasedOn
            End Get
            Set(ByVal value As Short)
                _RuleBasedOn = value
            End Set
        End Property

        Private _RuleStatus As Short
        Public Property RuleStatus() As Short
            Get
                Return _RuleStatus
            End Get
            Set(ByVal value As Short)
                _RuleStatus = value
            End Set
        End Property

        Private _byteMode As Short
        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal value As Short)
                _byteMode = value
            End Set
        End Property

        Private _FormFieldIdChange As Long
        Public Property FormFieldIdChange() As Long
            Get
                Return _FormFieldIdChange
            End Get
            Set(ByVal Value As Long)
                _FormFieldIdChange = Value
            End Set
        End Property

        Private _RuleConditionID As Long
        Public Property RuleConditionID() As Long
            Get
                Return _RuleConditionID
            End Get
            Set(ByVal Value As Long)
                _RuleConditionID = Value
            End Set
        End Property

        Private _vcCondition As String
        Public Property vcCondition() As String
            Get
                Return _vcCondition
            End Get
            Set(ByVal Value As String)
                _vcCondition = Value
            End Set
        End Property

        Private _vcJSON As String
        Public Property vcJSON() As String
            Get
                Return _vcJSON
            End Get
            Set(ByVal Value As String)
                _vcJSON = Value
            End Set
        End Property

        Private _ConditionOrder As Short
        Public Property ConditionOrder() As Short
            Get
                Return _ConditionOrder
            End Get
            Set(ByVal Value As Short)
                _ConditionOrder = Value
            End Set
        End Property

        Private _ActionType As Short
        Public Property ActionType() As Short
            Get
                Return _ActionType
            End Get
            Set(ByVal Value As Short)
                _ActionType = Value
            End Set
        End Property

        Private _ActionTemplateID As Long
        Public Property ActionTemplateID() As Long
            Get
                Return _ActionTemplateID
            End Get
            Set(ByVal Value As Long)
                _ActionTemplateID = Value
            End Set
        End Property

        Private _vcFieldValue As String
        Public Property vcFieldValue() As String
            Get
                Return _vcFieldValue
            End Get
            Set(ByVal Value As String)
                _vcFieldValue = Value
            End Set
        End Property

        Private _ActionFrom As Short
        Public Property ActionFrom() As Short
            Get
                Return _ActionFrom
            End Get
            Set(ByVal Value As Short)
                _ActionFrom = Value
            End Set
        End Property

        Private _ActionTo As Short
        Public Property ActionTo() As Short
            Get
                Return _ActionTo
            End Get
            Set(ByVal Value As Short)
                _ActionTo = Value
            End Set
        End Property

        Private _vcActionFrom As String
        Public Property vcActionFrom() As String
            Get
                Return _vcActionFrom
            End Get
            Set(ByVal Value As String)
                _vcActionFrom = Value
            End Set
        End Property

        Private _vcActionTo As String
        Public Property vcActionTo() As String
            Get
                Return _vcActionTo
            End Get
            Set(ByVal Value As String)
                _vcActionTo = Value
            End Set
        End Property

        Private _ActionOrder As Short
        Public Property ActionOrder() As Short
            Get
                Return _ActionOrder
            End Get
            Set(ByVal Value As Short)
                _ActionOrder = Value
            End Set
        End Property

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Public Function GetRuleMaster() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@tintModuleType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _RuleModuleType

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetRuleMaster", arParms)

                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageRuleMaster() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintModuleType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _RuleModuleType

                arParms(3) = New Npgsql.NpgsqlParameter("@vcRuleName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _RuleName

                arParms(4) = New Npgsql.NpgsqlParameter("@vcRuleDescription", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(4).Value = _RuleDescription

                arParms(5) = New Npgsql.NpgsqlParameter("@tintBasedOn", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _RuleBasedOn

                arParms(6) = New Npgsql.NpgsqlParameter("@tintStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _RuleStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@numFormFieldIdChange", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _FormFieldIdChange

                arParms(8) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _byteMode

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageRuleMaster", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageRuleCondition() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCondition", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(2).Value = _vcCondition

                arParms(3) = New Npgsql.NpgsqlParameter("@tintConditionOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _ConditionOrder

                arParms(4) = New Npgsql.NpgsqlParameter("@vcJSON", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _vcJSON

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageRuleCondition", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageRuleAction() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numRuleConditionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RuleConditionID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintActionType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _ActionType

                arParms(4) = New Npgsql.NpgsqlParameter("@numActionTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ActionTemplateID

                arParms(5) = New Npgsql.NpgsqlParameter("@vcFieldValue", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(5).Value = _vcFieldValue

                arParms(6) = New Npgsql.NpgsqlParameter("@tintActionFrom", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _ActionFrom

                arParms(7) = New Npgsql.NpgsqlParameter("@tintActionTo", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = _ActionTo

                arParms(8) = New Npgsql.NpgsqlParameter("@vcActionFrom", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(8).Value = _vcActionFrom

                arParms(9) = New Npgsql.NpgsqlParameter("@vcActionTo", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(9).Value = _vcActionTo

                arParms(10) = New Npgsql.NpgsqlParameter("@tintActionOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(10).Value = _ActionOrder

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_ManageRuleAction", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CheckRuleConditonAction(ByVal RecordID As Long) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numRecordID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = RecordID

                arParms(2) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _RuleID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintModuleType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _RuleModuleType

                arParms(4) = New Npgsql.NpgsqlParameter("@numRuleConditionID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(4).Value = _RuleConditionID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_CheckRuleConditonAction", arParms)

                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Sub ExecuteAutomationRule(ByVal RuleModuleType As Long, ByVal RecordID As Long, ByVal OpType As Long)
            Try
                Dim dtRule As DataTable
                Dim dtCondition As DataTable
                Dim dtAction As DataTable
                Dim boolAllow As Boolean
                Dim ActionType As Short

                Dim dsRule As DataSet
                Dim dsAction As DataSet

                Dim objAutomatonRule As New AutomatonRule
                objAutomatonRule.DomainID = CCommon.ToInteger(System.Web.HttpContext.Current.Session("DomainId"))
                objAutomatonRule.RuleModuleType = CShort(RuleModuleType)
                objAutomatonRule.byteMode = 3

                dsRule = objAutomatonRule.GetRuleMaster
                dtRule = dsRule.Tables(0)

                If dtRule.Rows.Count > 0 Then
                    For Each dr As DataRow In dtRule.Rows
                        Dim mask As Short = CShort(dr("tintBasedOn"))
                        boolAllow = False

                        If (mask = 1 Or mask = 3 Or mask = 7) AndAlso OpType = 1 Then 'Add
                            boolAllow = True
                        ElseIf (mask = 2 Or mask = 3 Or mask = 6 Or mask = 7) AndAlso OpType = 2 Then 'Modified
                            boolAllow = True
                        ElseIf (mask = 4 Or mask = 6 Or mask = 7) AndAlso OpType = 4 Then 'Delete
                            boolAllow = True
                        End If

                        If boolAllow = True Then
                            objAutomatonRule.RuleID = CCommon.ToLong(dr("numRuleID"))
                            objAutomatonRule.byteMode = 4

                            dtCondition = objAutomatonRule.GetRuleMaster.Tables(0)

                            If dtCondition.Rows.Count > 0 Then
                                For Each drCondition As DataRow In dtCondition.Rows
                                    objAutomatonRule.RuleConditionID = CCommon.ToLong(drCondition("numRuleConditionID"))
                                    dsAction = objAutomatonRule.CheckRuleConditonAction(RecordID)

                                    If dsAction.Tables.Count > 0 Then
                                        dtAction = dsAction.Tables(0)

                                        If dtAction.Rows.Count > 0 Then
                                            For Each drAction As DataRow In dtAction.Rows
                                                ActionType = CShort(drAction("tintActionType"))

                                                Select Case ActionType
                                                    Case 2 '2:Tickler Action
                                                        Dim dtActionItem As DataTable
                                                        dtActionItem = LoadThisActionItemData(CInt(drAction("numActionTemplateID"))) 'Get Action Item Template data

                                                        If dtActionItem.Rows.Count > 0 Then
                                                            Dim objActionItem As New ActionItem
                                                            With objActionItem
                                                                .CommID = 0
                                                                .Task = CLng(dtActionItem.Rows(0)("numTaskType"))

                                                                .ContactID = CLng(dsAction.Tables(1).Rows(0)("numContactId"))
                                                                .DivisionID = CLng(dsAction.Tables(1).Rows(0)("numDivisionId"))

                                                                .Details = IIf(OpType = 1, "Created", IIf(OpType = 2, "Modified", "Deleted")).ToString & " BizDoc No: " & dsAction.Tables(1).Rows(0)("vcBizDocID").ToString()
                                                                .AssignedTo = CLng(drAction("vcFieldValue"))
                                                                .UserCntID = CLng(System.Web.HttpContext.Current.Session("UserContactID"))
                                                                .DomainID = CLng(System.Web.HttpContext.Current.Session("DomainID"))
                                                                If .Task = 973 Then
                                                                    .BitClosed = 1
                                                                Else : .BitClosed = 0
                                                                End If
                                                                .CalendarName = ""
                                                                Dim strDate As DateTime = DateAdd(DateInterval.Minute, -CInt(System.Web.HttpContext.Current.Session("ClientMachineUTCTimeOffset")), Date.UtcNow).AddDays(Convert.ToDouble(dtActionItem.Rows(0)("DueDays")))

                                                                .StartTime = CDate(strDate.ToString("MM/dd/yyyy 00:00:00") & "AM")
                                                                .EndTime = CDate(strDate.ToString("MM/dd/yyyy 23:59:59") & "PM")

                                                                .Activity = CLng(dtActionItem.Rows(0)("Activity"))
                                                                .Status = CLng(dtActionItem.Rows(0)("Priority"))
                                                                .Snooze = 0
                                                                .SnoozeStatus = 0
                                                                .Remainder = 0
                                                                .RemainderStatus = 0
                                                                .ClientTimeZoneOffset = CInt(System.Web.HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                                                                .bitOutlook = 0
                                                                .SendEmailTemplate = CBool(dtActionItem.Rows(0).Item("bitSendEmailTemp"))
                                                                .EmailTemplate = CLng(dtActionItem.Rows(0).Item("numEmailTemplate"))
                                                                .Hours = CShort(dtActionItem.Rows(0).Item("tintHours"))
                                                                .Alert = CBool(dtActionItem.Rows(0).Item("bitAlert"))
                                                                .ActivityId = 0
                                                                .FollowUpAnyTime = False
                                                            End With

                                                            Dim numcommId As Long = 0
                                                            numcommId = objActionItem.SaveCommunicationinfo()

                                                            CAlerts.SendAlertToAssignee(6, CLng(System.Web.HttpContext.Current.Session("UserContactID")), CLng(drAction("vcFieldValue")), numcommId, CLng(System.Web.HttpContext.Current.Session("DomainID")))
                                                        End If
                                                    Case 4 '4:Field value set
                                                        'Currently manage From Stored Procedure

                                                    Case 5 '5:BizDoc Approval Request
                                                        Dim objDocuments As New DocumentList
                                                        objDocuments.GenDocID = RecordID
                                                        objDocuments.ContactID = CLng(drAction("vcFieldValue"))
                                                        objDocuments.CDocType = "B"
                                                        objDocuments.byteMode = 1
                                                        objDocuments.UserCntID = CLng(System.Web.HttpContext.Current.Session("UserContactID"))
                                                        Try
                                                            objDocuments.ManageApprovers()
                                                        Catch ex As Exception
                                                            Throw ex
                                                        End Try

                                                        Try
                                                            Dim objSendEmail As New Email

                                                            objSendEmail.DomainID = CLng(System.Web.HttpContext.Current.Session("DomainID"))
                                                            objSendEmail.TemplateCode = "#SYS#DOC_APPROVAL_REQUEST"
                                                            objSendEmail.ModuleID = 1
                                                            objSendEmail.RecordIds = CStr(drAction("vcFieldValue"))

                                                            Dim objCommon As New CCommon
                                                            objCommon.DomainID = CLng(System.Web.HttpContext.Current.Session("DomainID"))

                                                            'Dim vcBizDocID As String
                                                            'objCommon.Mode = 14
                                                            'objCommon.Str = CStr(RecordID)
                                                            'vcBizDocID = CStr(objCommon.GetSingleFieldValue())

                                                            objSendEmail.AdditionalMergeFields.Add("DocumentName", dsAction.Tables(1).Rows(0)("vcBizDocID"))
                                                            objSendEmail.AdditionalMergeFields.Add("LoggedInUser", System.Web.HttpContext.Current.Session("ContactName"))
                                                            objSendEmail.AdditionalMergeFields.Add("PortalDirectLoginLink", GetPortalDirectLoginLink(CLng(drAction("vcFieldValue"))))
                                                            objSendEmail.FromEmail = CStr(System.Web.HttpContext.Current.Session("UserEmail"))
                                                            objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
                                                            objSendEmail.SendEmailTemplate()
                                                        Catch ex As Exception
                                                            Throw ex
                                                        End Try
                                                End Select
                                            Next
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ExecuteAutomationRuleBizAPI(ByVal RuleModuleType As Long, ByVal RecordID As Long, ByVal OpType As Long, ByVal DomainID As Long, ByVal userContactID As Long, ByVal timeZoneOffset As Integer, ByVal contactName As String, ByVal userEmail As String)
            Try
                Dim dtRule As DataTable
                Dim dtCondition As DataTable
                Dim dtAction As DataTable
                Dim boolAllow As Boolean
                Dim ActionType As Short

                Dim dsRule As DataSet
                Dim dsAction As DataSet

                Dim objAutomatonRule As New AutomatonRule
                objAutomatonRule.DomainID = DomainID
                objAutomatonRule.RuleModuleType = CShort(RuleModuleType)
                objAutomatonRule.byteMode = 3

                dsRule = objAutomatonRule.GetRuleMaster
                dtRule = dsRule.Tables(0)

                If dtRule.Rows.Count > 0 Then
                    For Each dr As DataRow In dtRule.Rows
                        Dim mask As Short = CShort(dr("tintBasedOn"))
                        boolAllow = False

                        If (mask = 1 Or mask = 3 Or mask = 7) AndAlso OpType = 1 Then 'Add
                            boolAllow = True
                        ElseIf (mask = 2 Or mask = 3 Or mask = 6 Or mask = 7) AndAlso OpType = 2 Then 'Modified
                            boolAllow = True
                        ElseIf (mask = 4 Or mask = 6 Or mask = 7) AndAlso OpType = 4 Then 'Delete
                            boolAllow = True
                        End If

                        If boolAllow = True Then
                            objAutomatonRule.RuleID = CCommon.ToLong(dr("numRuleID"))
                            objAutomatonRule.byteMode = 4

                            dtCondition = objAutomatonRule.GetRuleMaster.Tables(0)

                            If dtCondition.Rows.Count > 0 Then
                                For Each drCondition As DataRow In dtCondition.Rows
                                    objAutomatonRule.RuleConditionID = CCommon.ToLong(drCondition("numRuleConditionID"))
                                    dsAction = objAutomatonRule.CheckRuleConditonAction(RecordID)

                                    If dsAction.Tables.Count > 0 Then
                                        dtAction = dsAction.Tables(0)

                                        If dtAction.Rows.Count > 0 Then
                                            For Each drAction As DataRow In dtAction.Rows
                                                ActionType = CShort(drAction("tintActionType"))

                                                Select Case ActionType
                                                    Case 2 '2:Tickler Action
                                                        Dim dtActionItem As DataTable
                                                        dtActionItem = LoadThisActionItemData(CInt(drAction("numActionTemplateID"))) 'Get Action Item Template data

                                                        If dtActionItem.Rows.Count > 0 Then
                                                            Dim objActionItem As New ActionItem
                                                            With objActionItem
                                                                .CommID = 0
                                                                .Task = CLng(dtActionItem.Rows(0)("numTaskType"))

                                                                .ContactID = CLng(dsAction.Tables(1).Rows(0)("numContactId"))
                                                                .DivisionID = CLng(dsAction.Tables(1).Rows(0)("numDivisionId"))

                                                                .Details = IIf(OpType = 1, "Created", IIf(OpType = 2, "Modified", "Deleted")).ToString & " BizDoc No: " & dsAction.Tables(1).Rows(0)("vcBizDocID").ToString()
                                                                .AssignedTo = CLng(drAction("vcFieldValue"))
                                                                .UserCntID = userContactID
                                                                .DomainID = DomainID
                                                                If .Task = 973 Then
                                                                    .BitClosed = 1
                                                                Else : .BitClosed = 0
                                                                End If
                                                                .CalendarName = ""
                                                                Dim strDate As DateTime = DateAdd(DateInterval.Minute, -timeZoneOffset, Date.UtcNow).AddDays(Convert.ToDouble(dtActionItem.Rows(0)("DueDays")))

                                                                .StartTime = CDate(strDate.ToString("MM/dd/yyyy 00:00:00") & "AM")
                                                                .EndTime = CDate(strDate.ToString("MM/dd/yyyy 23:59:59") & "PM")

                                                                .Activity = CLng(dtActionItem.Rows(0)("Activity"))
                                                                .Status = CLng(dtActionItem.Rows(0)("Priority"))
                                                                .Snooze = 0
                                                                .SnoozeStatus = 0
                                                                .Remainder = 0
                                                                .RemainderStatus = 0
                                                                .ClientTimeZoneOffset = timeZoneOffset
                                                                .bitOutlook = 0
                                                                .SendEmailTemplate = CBool(dtActionItem.Rows(0).Item("bitSendEmailTemp"))
                                                                .EmailTemplate = CLng(dtActionItem.Rows(0).Item("numEmailTemplate"))
                                                                .Hours = CShort(dtActionItem.Rows(0).Item("tintHours"))
                                                                .Alert = CBool(dtActionItem.Rows(0).Item("bitAlert"))
                                                                .ActivityId = 0
                                                                .FollowUpAnyTime = False
                                                            End With

                                                            Dim numcommId As Long = 0
                                                            numcommId = objActionItem.SaveCommunicationinfo()

                                                            CAlerts.SendAlertToAssignee(6, userContactID, CLng(drAction("vcFieldValue")), numcommId, DomainID)
                                                        End If
                                                    Case 4 '4:Field value set
                                                        'Currently manage From Stored Procedure

                                                    Case 5 '5:BizDoc Approval Request
                                                        Dim objDocuments As New DocumentList
                                                        objDocuments.GenDocID = RecordID
                                                        objDocuments.ContactID = CLng(drAction("vcFieldValue"))
                                                        objDocuments.CDocType = "B"
                                                        objDocuments.byteMode = 1
                                                        objDocuments.UserCntID = userContactID
                                                        Try
                                                            objDocuments.ManageApprovers()
                                                        Catch ex As Exception
                                                            Throw ex
                                                        End Try

                                                        Try
                                                            Dim objSendEmail As New Email

                                                            objSendEmail.DomainID = DomainID
                                                            objSendEmail.TemplateCode = "#SYS#DOC_APPROVAL_REQUEST"
                                                            objSendEmail.ModuleID = 1
                                                            objSendEmail.RecordIds = CStr(drAction("vcFieldValue"))

                                                            Dim objCommon As New CCommon
                                                            objCommon.DomainID = DomainID

                                                            'Dim vcBizDocID As String
                                                            'objCommon.Mode = 14
                                                            'objCommon.Str = CStr(RecordID)
                                                            'vcBizDocID = CStr(objCommon.GetSingleFieldValue())

                                                            objSendEmail.AdditionalMergeFields.Add("DocumentName", dsAction.Tables(1).Rows(0)("vcBizDocID"))
                                                            objSendEmail.AdditionalMergeFields.Add("LoggedInUser", contactName)
                                                            objSendEmail.AdditionalMergeFields.Add("PortalDirectLoginLink", GetPortalDirectLoginLink(CLng(drAction("vcFieldValue"))))
                                                            objSendEmail.FromEmail = userEmail
                                                            objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
                                                            objSendEmail.SendEmailTemplate()
                                                        Catch ex As Exception
                                                            Throw ex
                                                        End Try
                                                End Select
                                            Next
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetPortalDirectLoginLink(ByVal ContactID As Long) As String
            Dim objCommon As New CCommon
            Dim objUserAccess As New UserAccess
            objUserAccess.ContactID = ContactID
            objUserAccess.DomainID = CLng(System.Web.HttpContext.Current.Session("DomainID"))
            Dim dtUser As DataTable = objUserAccess.GetExtranetUserDetails()

            If dtUser.Rows.Count = 1 Then
                Dim strPortalApprovalLink As String
                strPortalApprovalLink = CCommon.ToString(System.Web.HttpContext.Current.Session("PortalURL")) & QueryEncryption.EncryptQueryString("&from=email&u=" + CCommon.ToString(dtUser.Rows(0)("vcEmail")) + "&p=" + objCommon.Encrypt(CCommon.ToString(dtUser.Rows(0)("vcPassword"))))
                strPortalApprovalLink = "<a href=" & strPortalApprovalLink & " target='_blank' >click here</a>"
                Return strPortalApprovalLink
            End If
            Return ""
        End Function

        Private Function LoadThisActionItemData(ByVal actionItemID As Int32) As DataTable
            Try
                Dim objActionItem As New ActionItem
                With objActionItem
                    .RowID = actionItemID
                End With
                Return objActionItem.LoadThisActionItemTemplateData()
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace