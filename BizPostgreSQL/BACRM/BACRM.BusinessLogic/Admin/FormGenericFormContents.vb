''' -----------------------------------------------------------------------------
''' Project	 : BACRM.BusinessLogic
''' Class	 : FormGenericFormContents
''' 
''' -----------------------------------------------------------------------------
''' <summary>
'''     This class contains several methods and functions, which is used to 
'''     generate dynamic forms from the config done in the BizForm Wizard.
''' </summary>
''' <remarks>
'''     This class is used to access the data and manage the same for config of 
'''     forms.
''' </remarks>
''' <history>
''' 	[Debasish Tapan Nag]	07/19/2005	Created
''' </history>
''' -----------------------------------------------------------------------------
Option Explicit On 
Imports System.Data.SqlClient
Imports System.IO
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Admin
    Public Class FormGenericFormContents
        Inherits BACRM.BusinessLogic.CBusinessBase
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the DomainID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the domain id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Private DomainId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the userID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the user id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        ''' 
        Private _SubFormId As Long
        Public Property SubFormId As Long
            Get
                Return _SubFormId
            End Get
            Set(value As Long)
                _SubFormId = value
            End Set
        End Property

        Private _UserID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User id.
        ''' </summary>
        ''' <value>Returns the user id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the User id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property UserID() As Long
            Get
                Return _UserID
            End Get
            Set(ByVal Value As Long)
                _UserID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the form id.
        ''' </summary>
        ''' <remarks>
        '''     This holds the form id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _FormID As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the Domain id.
        ''' </summary>
        ''' <value>Returns the domain id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the Domain id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/09/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property FormID() As Long
            Get
                Return _FormID
            End Get
            Set(ByVal Value As Long)
                _FormID = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the Authentication Group ID.
        ''' </summary>
        ''' <remarks>
        '''     This holds the user Group id.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _AuthGroupId As Long
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the User's authenticaiton group id.
        ''' </summary>
        ''' <value>Returns the user authentication group id as long.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the User authentiation group id. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/30/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property AuthGroupId() As Long
            Get
                Return _AuthGroupId
            End Get
            Set(ByVal Value As Long)
                _AuthGroupId = Value
            End Set
        End Property

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     Represents the List Item Type.
        ''' </summary>
        ''' <remarks>
        '''     This holds the List Item Type.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private _ListItemType As String
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This Sets and Gets the List Item Type.
        ''' </summary>
        ''' <value>Returns the List Item Type as string.</value>
        ''' <remarks>
        '''     This property is used to Set and Get the List Item Type. 
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/21/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Property ListItemType() As String
            Get
                Return _ListItemType
            End Get
            Set(ByVal Value As String)
                _ListItemType = Value
            End Set
        End Property
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to save the xml into the server file
        ''' </summary>
        ''' <param name="sXMLFilePath">Represents the path to the file.</param>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	09/07/2005	Created
        ''' </history>
        '''  -----------------------------------------------------------------------------
        Public Function getDynamicFormConfig(ByVal sXMLFilePath As String) As DataTable
            Dim dsXMLConfig As New DataSet                                                                          'Create a Dataset object
            Dim strFileName As String                                                         'Declare a string object
            If FormID = 3 AndAlso SubFormId > 0 Then
                strFileName = sXMLFilePath & "\" & "DynamicFormConfig_" & DomainID & "_" & FormID & "_" & AuthGroupId & "_" & SubFormId & ".xml"           'Set the file name
            Else
                strFileName = sXMLFilePath & "\" & "DynamicFormConfig_" & DomainID & "_" & FormID & "_" & AuthGroupId & ".xml"           'Set the file name
            End If

            If File.Exists(strFileName) = False Then                  'If file not exists exit
                Dim dtTable As New DataTable("xmlTable")                                                            'create a new datatable
                dtTable.Columns.Add(New DataColumn("numFormId", System.Type.GetType("System.String")))              'Add acolumn for vcFormId
                dtTable.Columns.Add(New DataColumn("numFormFieldId", System.Type.GetType("System.String")))         'Add acolumn for vcNewFormFieldId
                dtTable.Columns.Add(New DataColumn("vcFormFieldName", System.Type.GetType("System.String")))        'Add acolumn for vcFormFieldName
                dtTable.Columns.Add(New DataColumn("vcNewFormFieldName", System.Type.GetType("System.String")))     'Add acolumn for vcNewFormFieldName
                dtTable.Columns.Add(New DataColumn("vcDbColumnName", System.Type.GetType("System.String")))         'Add another column for vcDbColumnName
                dtTable.Columns.Add(New DataColumn("vcListItemType", System.Type.GetType("System.String")))         'Add another column for vcDbColumnType
                dtTable.Columns.Add(New DataColumn("vcAssociatedControlType", System.Type.GetType("System.String"))) 'Add another column for vcAssociatedControlType
                dtTable.Columns.Add(New DataColumn("vcFieldType", System.Type.GetType("System.String")))            'Add another column for vcDbColumnType
                dtTable.Columns.Add(New DataColumn("vcFieldDataType", System.Type.GetType("System.String")))        'Add another column for vcDbColumnType
                dtTable.Columns.Add(New DataColumn("intColumnNum", System.Type.GetType("System.String")))           'Add another column for intColumnNum
                dtTable.Columns.Add(New DataColumn("intRowNum", System.Type.GetType("System.String")))              'Add another column for intRowNum
                dtTable.Columns.Add(New DataColumn("boolRequired", System.Type.GetType("System.String")))           'Add another column for boolRequired
                dtTable.Columns.Add(New DataColumn("numListID", System.Type.GetType("System.String")))              'Add another column for numListID
                dtTable.Columns.Add(New DataColumn("boolAOIField", System.Type.GetType("System.String")))           'Add another column for boolAOIField
                dtTable.Columns.Add(New DataColumn("vcLookBackTableName", System.Type.GetType("System.String")))     'Add another column for vcLookBackTableName
                dsXMLConfig.Tables.Add(dtTable)                                                                     'Add table to the dataset
            ElseIf File.Exists(strFileName) = True Then
                dsXMLConfig.ReadXml(strFileName)                                                                    'Read into XML from the selected groups file
            End If
            dsXMLConfig.Tables(0).Columns.Add("intRowNumVirtual", System.Type.GetType("System.Int64"), "[intRowNum]") 'This columsn has to be an integer to tryign to manipupate the datatype as Compute(Max) does not work on string datatypes
            Return dsXMLConfig.Tables(0)
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for all list details for the dropdown.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of list details
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/12/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function GetMasterListByListId(ByVal iListItemId As Integer) As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New getconnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numListID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = iListItemId

                arParms(1) = New Npgsql.NpgsqlParameter("@vcItemType", NpgsqlTypes.NpgsqlDbType.Char, 3)
                arParms(1).Value = ListItemType                                                'representing listmaster/ listdetails

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetMasterListDetailsDynamicForm", arParms)     'execute and store to dataset

                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to fetch the values for the form headers.
        ''' </summary>
        ''' <returns>Returns the value as DataTable.</returns>
        ''' <remarks>
        '''     This function calls the stored procedure and it returns the value
        '''     of form header
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	07/31/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Function getFormHeaderDetails() As DataTable
            Try
                Dim ds As DataSet                                                               'declare a dataset
                Dim getconnection As New GetConnection                                          'declare a connection
                Dim connString As String = getconnection.GetConnectionString                    'get the connection string
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@numFormId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = FormID                                                       'representing form id

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetDynamicFormHeader", arParms)     'execute and store to dataset

                Return ds.Tables(0)                                                             'return datatable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace
