﻿Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic

Public Class BizFormWizardMasterConfiguration
    Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Memeber Variables"

    Private _numFormID As Long
    Private _numFieldID As Long
    Private _intColumnNum As Integer
    Private _intRowNum As Integer
    Private _numGroupID As Long
    Private _numRelCntType As Long
    Private _tintPageType As Integer
    Private _bitCustom As Boolean
    Private _numPageID As Long
    Private _bitGridConfiguration As Boolean
    Private _OutputsStatus As Int64
    Private _numFormFieldGroupId As Long
    Private _vcGroupName As String

#End Region


#Region "Public Properties"

    Public Property numFormID() As Long
        Get
            Return _numFormID
        End Get
        Set(ByVal value As Long)
            _numFormID = value
        End Set
    End Property
    Public Property OutputsStatus() As Int64
        Get
            Return _OutputsStatus
        End Get
        Set(value As Int64)
            _OutputsStatus = value
        End Set
    End Property
    Public Property numFormFieldGroupId() As Long
        Get
            Return _numFormFieldGroupId
        End Get
        Set(value As Long)
            _numFormFieldGroupId = value
        End Set
    End Property
    Public Property vcGroupName() As String
        Get
            Return _vcGroupName
        End Get
        Set(value As String)
            _vcGroupName = value
        End Set
    End Property
    Public Property numFieldID() As Long
        Get
            Return _numFieldID
        End Get
        Set(ByVal value As Long)
            _numFieldID = value
        End Set
    End Property
    Public Property intColumnNum() As Integer
        Get
            Return _intColumnNum
        End Get
        Set(ByVal value As Integer)
            _intColumnNum = value
        End Set
    End Property
    Public Property intRowNum() As Integer
        Get
            Return _intRowNum
        End Get
        Set(ByVal value As Integer)
            _intRowNum = value
        End Set
    End Property
    Public Property numGroupID() As Long
        Get
            Return _numGroupID
        End Get
        Set(ByVal value As Long)
            _numGroupID = value
        End Set
    End Property
    Public Property numRelCntType() As Long
        Get
            Return _numRelCntType
        End Get
        Set(ByVal value As Long)
            _numRelCntType = value
        End Set
    End Property
    Public Property tintPageType() As Integer
        Get
            Return _tintPageType
        End Get
        Set(ByVal value As Integer)
            _tintPageType = value
        End Set
    End Property
    Public Property bitCustom() As Boolean
        Get
            Return _bitCustom
        End Get
        Set(ByVal value As Boolean)
            _bitCustom = value
        End Set
    End Property
    Public Property numPageID() As Long
        Get
            Return _numPageID
        End Get
        Set(ByVal value As Long)
            _numPageID = value
        End Set
    End Property
    Public Property bitGridConfiguration() As Boolean
        Get
            Return _bitGridConfiguration
        End Get
        Set(ByVal value As Boolean)
            _bitGridConfiguration = value
        End Set
    End Property
#End Region


#Region "Public Methods"

    Public Function ManageFormFieldGroup() As DataSet
        Try
            Dim ds As DataSet

            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = DomainID

            arParms(1) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = _numFormID

            arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(2).Value = _numGroupID

            arParms(3) = New Npgsql.NpgsqlParameter("@numFormFieldGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(3).Value = _numFormFieldGroupId

            arParms(4) = New Npgsql.NpgsqlParameter("@tintActionType", NpgsqlTypes.NpgsqlDbType.Smallint)
            arParms(4).Value = _tintPageType

            arParms(5) = New Npgsql.NpgsqlParameter("@vcGroupName", NpgsqlTypes.NpgsqlDbType.VarChar)
            arParms(5).Value = _vcGroupName

            arParms(6) = New Npgsql.NpgsqlParameter("@vcOutput", NpgsqlTypes.NpgsqlDbType.Integer)
            arParms(6).Direction = ParameterDirection.InputOutput
            arParms(6).Value = _OutputsStatus

            arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(7).Value = Nothing
            arParms(7).Direction = ParameterDirection.InputOutput

            Dim objParam() As Object
            objParam = arParms.ToArray()

            If _tintPageType = 4 Then
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_ManageFormFieldGroupConfigurarion", objParam, True)
            Else
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageFormFieldGroupConfigurarion", objParam, True)
            End If
            _OutputsStatus = Convert.ToInt32(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)

            Return ds
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function GetAvailableFields() As DataSet
        Try
            Dim ds As DataSet

            Dim getconnection As New getconnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = DomainID

            arParms(1) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = _numFormID

            arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(2).Value = _numGroupID

            arParms(3) = New Npgsql.NpgsqlParameter("@numRelCntType", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(3).Value = _numRelCntType

            arParms(4) = New Npgsql.NpgsqlParameter("@tintPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
            arParms(4).Value = _tintPageType

            arParms(5) = New Npgsql.NpgsqlParameter("@pageID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(5).Value = _numPageID

            arParms(6) = New Npgsql.NpgsqlParameter("@bitGridConfiguration", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(6).Value = _bitGridConfiguration

            arParms(7) = New Npgsql.NpgsqlParameter("@numFormFieldGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(7).Value = _numFormFieldGroupId

            arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(8).Value = Nothing
            arParms(8).Direction = ParameterDirection.InputOutput

            arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(9).Value = Nothing
            arParms(9).Direction = ParameterDirection.InputOutput

            ds = SqlDAL.ExecuteDataset(connString, "USP_BizFormWizardMasterConfiguration_GetAvailableFields", arParms)

            Return ds
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GetSelectedFields() As DataTable
        Try
            Dim ds As DataSet

            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = DomainID

            arParms(1) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = _numFormID

            arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(2).Value = _numGroupID

            arParms(3) = New Npgsql.NpgsqlParameter("@numRelCntType", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(3).Value = _numRelCntType

            arParms(4) = New Npgsql.NpgsqlParameter("@tintPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
            arParms(4).Value = _tintPageType

            arParms(5) = New Npgsql.NpgsqlParameter("@tintColumn", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(5).Value = intColumnNum

            arParms(6) = New Npgsql.NpgsqlParameter("@bitGridConfiguration", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(6).Value = _bitGridConfiguration

            arParms(7) = New Npgsql.NpgsqlParameter("@numFormFieldGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(7).Value = _numFormFieldGroupId

            arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(8).Value = Nothing
            arParms(8).Direction = ParameterDirection.InputOutput

            ds = SqlDAL.ExecuteDataset(connString, "USP_BizFormWizardMasterConfiguration_GetSelectedFields", arParms)

            Return ds.Tables(0)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub SaveFormConfiguration(ByVal strXml As String)
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(0).Value = DomainID

            arParms(1) = New Npgsql.NpgsqlParameter("@numFormID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(1).Value = _numFormID

            arParms(2) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(2).Value = _numGroupID

            arParms(3) = New Npgsql.NpgsqlParameter("@numRelCntType", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(3).Value = _numRelCntType

            arParms(4) = New Npgsql.NpgsqlParameter("@tintPageType", NpgsqlTypes.NpgsqlDbType.Smallint)
            arParms(4).Value = _tintPageType

            arParms(5) = New Npgsql.NpgsqlParameter("@tintColumn", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(5).Value = intColumnNum

            arParms(6) = New Npgsql.NpgsqlParameter("@bitGridConfiguration", NpgsqlTypes.NpgsqlDbType.Bit)
            arParms(6).Value = _bitGridConfiguration

            arParms(7) = New Npgsql.NpgsqlParameter("@strFomFld", NpgsqlTypes.NpgsqlDbType.Text)
            arParms(7).Value = strXml

            arParms(8) = New Npgsql.NpgsqlParameter("@numFormFieldGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arParms(8).Value = _numFormFieldGroupId

            SqlDAL.ExecuteNonQuery(connString, "USP_BizFormWizardMasterConfiguration_Save", arParms)
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region


End Class
