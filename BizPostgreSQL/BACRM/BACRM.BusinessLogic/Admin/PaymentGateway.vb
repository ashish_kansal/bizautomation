﻿'Created BY Anoop Jayaraj

Option Explicit On
Option Strict On


Imports nsoftware.InPay
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports System.IO
Imports System.Net
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer

'Imports Moneris
Namespace BACRM.BusinessLogic.Admin
    Public Class PaymentGateway
        Inherits BACRM.BusinessLogic.CBusinessBase

        Enum PGateWay As Int16
            USAEpay = 1
            BeanStream = 2
            MonerisCanada = 3
            AuthorizeNet_CardNotPresent = 4
            PayFlowPro = 5
            FirstData = 6
            AuthorizeNet_CardPresent = 7
            Transaction_Central = 8
            CardConnect1 = 9
            CardConnect2 = 10
        End Enum

        'Private DomainId As Long

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property


        Private _FirstName As String

        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property


        Private _LastName As String

        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property


        Private _Email As String

        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal Value As String)
                _Email = Value
            End Set
        End Property


        Private _Phone As String

        Public Property Phone() As String
            Get
                Return _Phone
            End Get
            Set(ByVal Value As String)
                _Phone = Value
            End Set
        End Property


        Private _Street As String

        Public Property Street() As String
            Get
                Return _Street
            End Get
            Set(ByVal Value As String)
                _Street = Value
            End Set
        End Property

        Private _City As String

        Public Property City() As String
            Get
                Return _City
            End Get
            Set(ByVal Value As String)
                _City = Value
            End Set
        End Property

        Private _PinCode As String

        Public Property PinCode() As String
            Get
                Return _PinCode
            End Get
            Set(ByVal Value As String)
                _PinCode = Value
            End Set
        End Property


        Private _State As String

        Public Property State() As String
            Get
                Return _State
            End Get
            Set(ByVal Value As String)
                _State = Value
            End Set
        End Property

        Private _Country As String

        Public Property Country() As String
            Get
                Return _Country
            End Get
            Set(ByVal Value As String)
                _Country = Value
            End Set
        End Property

        Public Property SiteID As Long

        ''' <summary>
        ''' Note: Use following KB for implementation of any new payment gateways http://www.nsoftware.com/kb/showentry.aspx?entry=01191001
        ''' </summary>
        ''' <param name="Amount"></param>
        ''' <param name="CHName"></param>
        ''' <param name="CardNo"></param>
        ''' <param name="CVV"></param>
        ''' <param name="Month"></param>
        ''' <param name="Year"></param>
        ''' <param name="ContactID"></param>
        ''' <param name="Message"></param>
        ''' <param name="ReturnTransactionID"></param>
        ''' <param name="CreditCardAuthOnly"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GatewayTransaction(ByVal Amount As Decimal, ByVal CHName As String, ByVal CardNo As String, ByVal CVV As String, ByVal Month As String, ByVal Year As String, ByVal ContactID As Long, ByRef Message As String, ByRef ReturnTransactionID As String, ByRef responseCode As String, Optional ByVal CreditCardAuthOnly As Boolean = False, Optional ByVal CreditCardTrack1 As String = "", Optional strInvoiceNumber As String = "", Optional SiteId As Integer = 0, Optional ByVal paymentGateway As Integer = 0) As Boolean
            Try
                '----------------------------------------------------------
                'Note: Use following KB for implementation of any new payment gateways
                'http://www.nsoftware.com/kb/showentry.aspx?entry=01191001
                '----------------------------------------------------------

                Dim ObjIcharge As New Icharge
                ObjIcharge.Config("AllowPartialAuths=False")
                Dim objCardvalidator As New Cardvalidator
                Dim InvoiceNo As String = strInvoiceNumber
                If InvoiceNo = "" Then InvoiceNo = Now.Year.ToString & Now.Month.ToString & Now.Day.ToString & Now.Hour.ToString & Now.Minute.ToString & Now.Second.ToString

                ObjIcharge.Card.Number = CardNo
                ObjIcharge.Card.ExpMonth = Int32.Parse(Month)
                ObjIcharge.Card.ExpYear = Int32.Parse(Year)
                If CVV <> "" Then
                    ObjIcharge.Card.CVVData = CVV
                End If
                Try
                    objCardvalidator.CardNumber = ObjIcharge.Card.Number
                    objCardvalidator.CardExpMonth = ObjIcharge.Card.ExpMonth
                    objCardvalidator.CardExpYear = ObjIcharge.Card.ExpYear
                    objCardvalidator.ValidateCard()
                Catch ex As nsoftware.InPay.InPayException
                    Message = "<font color='red'>Pre-Authorization checks failed: </font>" + ex.Message
                    Return False
                End Try

                Dim dtGateWayDTL As DataTable
                Dim objCommon As New CCommon
                objCommon.DomainID = DomainID
                objCommon.numSiteId = SiteId
                If paymentGateway > 0 Then
                    objCommon.PaymentGWId = paymentGateway
                End If
                dtGateWayDTL = objCommon.GetPaymentGatewayDetails

                ''''Address Details
                If ContactID > 0 Then
                    GetContactDetails(ContactID)
                End If

                If CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) >= 3 Then
                    ObjIcharge.Customer.FirstName = _FirstName
                    ObjIcharge.Customer.LastName = _LastName
                    ObjIcharge.Customer.Email = _Email
                    ObjIcharge.Customer.Phone = _Phone
                    ObjIcharge.Customer.Address = _Street
                    ObjIcharge.Customer.City = _City
                    ObjIcharge.Customer.Country = _Country
                    ObjIcharge.Customer.State = _State
                    ObjIcharge.Customer.Zip = _PinCode
                    ObjIcharge.InvoiceNumber = InvoiceNo
                    ObjIcharge.TransactionDesc = "Order" & InvoiceNo
                    ObjIcharge.TransactionAmount = CStr(Amount)
                End If

                If dtGateWayDTL.Rows.Count > 0 Then

                    If PGateWay.FirstData = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwFirstData
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "staging.linkpt.net:1129"
                        Else
                            ObjIcharge.GatewayURL = "secure.linkpt.net:1129"
                        End If
                        'This is your Store Number
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        'MerchantPassword is not used. Instead a SSL client certificate is used.
                        ObjIcharge.SSLCert = New Certificate(CertStoreTypes.cstPEMKeyFile, CCommon.GetDocumentPhysicalPath(DomainID) & CCommon.ToString(dtGateWayDTL.Rows(0).Item("vcSecndFldValue")), "", "*")
                    ElseIf PGateWay.AuthorizeNet_CardNotPresent = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Or PGateWay.AuthorizeNet_CardPresent = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwAuthorizeNet
                        ObjIcharge.TransactionAmount = String.Format("{0:#####0.00}", Amount)
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "https://test.authorize.net/gateway/transact.dll" ' is for developer Sandbox testing, not for actuall PG test mode
                            ObjIcharge.AddSpecialField("x_test_request", "true")
                        Else
                            ObjIcharge.GatewayURL = "https://secure.authorize.net/gateway/transact.dll"
                        End If

                        If CreditCardTrack1.Length > 2 Then
                            objCommon.PaymentGWId = PGateWay.AuthorizeNet_CardPresent
                            dtGateWayDTL = objCommon.GetPaymentGatewayDetails

                            If dtGateWayDTL.Rows.Count > 0 Then
                                'This corresponds to the x_login field and is your API Login ID
                                ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                                ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                                'ObjIcharge.AddSpecialField("x_tran_key", CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue")))
                                'ObjIcharge.AddSpecialField("x_cpversion", "1.0")

                                'The x_market_type option will need to be set to the value of '2', which indicates(the) 'Retail' market.
                                ObjIcharge.AddSpecialField("x_market_type", "2")
                                ObjIcharge.AddSpecialField("x_device_type", CStr(dtGateWayDTL.Rows(0).Item("vcThirdFldValue")))
                                ObjIcharge.AddSpecialField("x_Track1", CreditCardTrack1)
                            Else
                                Message = "Please Configure Payment Gateway for Authorize.net (CP)."
                                Return False
                            End If
                        Else
                            objCommon.PaymentGWId = PGateWay.AuthorizeNet_CardNotPresent
                            dtGateWayDTL = objCommon.GetPaymentGatewayDetails

                            If dtGateWayDTL.Rows.Count > 0 Then
                                'This corresponds to the x_login field and is your API Login ID
                                ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                                'This correspond to the x_tran_key field and is your Transaction Key value
                                ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcThirdFldValue"))
                            Else
                                Message = "Please Configure Payment Gateway for Authorize.net (CNP)."
                                Return False
                            End If
                        End If
                        'Transaction Central
                    ElseIf PGateWay.Transaction_Central = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwTransactionCentral
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.AddSpecialField("x_test_request", "true")
                        Else
                            ObjIcharge.AddSpecialField("x_test_request", "false")
                        End If

                        If CreditCardTrack1.Length > 2 Then
                            objCommon.PaymentGWId = PGateWay.Transaction_Central
                            dtGateWayDTL = objCommon.GetPaymentGatewayDetails

                            If dtGateWayDTL.Rows.Count > 0 Then
                                ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                                ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                                'The x_market_type option will need to be set to the value of '2', which indicates(the) 'Retail' market.
                                ObjIcharge.AddSpecialField("x_market_type", "2")
                                ObjIcharge.AddSpecialField("x_Track1", CreditCardTrack1)
                            Else
                                Message = "Please Configure Payment Gateway for Transaction Central"
                                Return False
                            End If
                        Else
                            objCommon.PaymentGWId = PGateWay.Transaction_Central
                            dtGateWayDTL = objCommon.GetPaymentGatewayDetails

                            If dtGateWayDTL.Rows.Count > 0 Then
                                ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                                ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                            Else
                                Message = "Please Configure Payment Gateway for Transaction Central."
                                Return False
                            End If
                        End If
                    ElseIf PGateWay.PayFlowPro = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwPayFlowPro
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "https://pilot-payflowpro.paypal.com"
                        Else
                            ObjIcharge.GatewayURL = "https://payflowpro.paypal.com"
                        End If
                        ObjIcharge.TransactionId = InvoiceNo
                        ObjIcharge.SpecialFields(1).Value = CStr(dtGateWayDTL.Rows(0).Item("vcThirdFldValue")) 'PARTNER
                        ObjIcharge.AddSpecialField("USER", CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue")))
                        ObjIcharge.AddSpecialField("VENDOR", CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue")))
                        ObjIcharge.AddSpecialField("PWD", CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue")))
                        ObjIcharge.AddSpecialField("TENDER", "C")
                        'TRXTYPE Type of transaction that should be processed. Allowed
                        'transaction types: Sale (S), Void (V), Inquiry (I).
                        ObjIcharge.AddSpecialField("TRXTYPE", "S")

                        ObjIcharge.TransactionAmount = String.Format("{0:#####0.00}", Amount) 'Only 2 decimal points are allowed othewise it throws error of invalid amount
                    ElseIf PGateWay.MonerisCanada = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        'Canadian.
                        ObjIcharge.Gateway = IchargeGateways.gwMoneris

                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            'Canadian Test URL
                            ObjIcharge.GatewayURL = "https://esqa.moneris.com/HPPDP/index.php"
                        Else
                            ObjIcharge.GatewayURL = "https://www3.moneris.com/HPPDP/index.php"
                        End If

                        'This is your DirectPost ID / Store ID
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))

                        'This is your DirectPost Token/Key
                        ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))

                        If ObjIcharge.Gateway = IchargeGateways.gwMoneris Then
                            'n.nn (2dec) This is the amount of the original transaction (i.e. min 0.01 & max 9999999.99)
                            ObjIcharge.TransactionAmount = String.Format("{0:#####0.00}", Amount)
                        End If

                    ElseIf PGateWay.USAEpay = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        Dim usaepay As New USAePayAPI.USAePay
                        usaepay.SourceKey = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        usaepay.Pin = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                        usaepay.Currency = "840"
                        usaepay.Amount = Amount
                        usaepay.Invoice = InvoiceNo
                        usaepay.Description = "Order" & InvoiceNo
                        usaepay.CardHolder = CStr(IIf(CHName <> "", CHName, _FirstName & " " & _LastName))
                        usaepay.CardNumber = CardNo
                        usaepay.CardExp = Month & Year
                        usaepay.Cvv2 = CVV
                        usaepay.IgnoreDupe = True
                        usaepay.UseSandbox = CBool(dtGateWayDTL.Rows(0).Item("bitTest"))
                        usaepay.BillingFirstName = _FirstName
                        usaepay.BillingLastName = _LastName
                        usaepay.BillingPhone = _Phone
                        usaepay.BillingStreet = _Street
                        usaepay.BillingCity = _City
                        usaepay.BillingCountry = _Country
                        usaepay.BillingState = _State
                        usaepay.BillingZip = _PinCode
                        usaepay.Email = _Email
                        If CreditCardAuthOnly Then
                            usaepay.AuthOnly()
                        Else
                            usaepay.Sale()
                        End If

                        If usaepay.ResultCode = "A" Then
                            Message = "Transaction approved" & vbLf _
                                & "Auth Code: " & usaepay.AuthCode & vbLf _
                                & "Ref Num: " & usaepay.ResultRefNum
                            ReturnTransactionID = usaepay.ResultRefNum
                            Return True

                        ElseIf usaepay.ResultCode = "D" Then
                            Message = "Transaction Declined" & vbLf _
                                & "Ref Num: " & usaepay.ResultRefNum

                            Return False
                        Else
                            Message = "Transaction Error" & vbLf _
                                & "Ref Num: " & usaepay.ResultRefNum & vbLf _
                                & "Error: " & usaepay.ErrorMesg & vbLf _
                                & "Error Code: " & usaepay.ErrorCode & vbLf
                            Return False
                        End If
                    ElseIf PGateWay.BeanStream = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        Dim trnResponse As String
                        Dim errorMessage As String
                        Dim rspHash As New Hashtable()
                        Dim trnRequest As String
                        trnRequest = "requestType=BACKEND" & _
                                "&merchant_id=" & CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue")) & _
                                "&ordName=" & "Order" & InvoiceNo & _
                                "&trnOrderNumber=" & InvoiceNo & _
                                "&ordPhoneNumber=" & _Phone & _
                                "&ordAddress1=" & _Street & " ," & _City & " ," & _State & " ," & _Country & " ," & _PinCode & _
                                "&ordCity=QC" & _
                                "&ordProvince=QC" & _
                                "&ordPostalCode=H1V 1L5" & _
                                "&ordCountry=CA" & _
                                "&ordEmailAddress=" & _Email & _
                                "&trnAmount=" & Amount & _
                                "&trnCardOwner=" & CStr(IIf(CHName <> "", CHName, _FirstName & " " & _LastName)) & _
                                "&trnCardNumber=" & CardNo & _
                                "&trnCardCvd=" & CVV & _
                                "&trnExpMonth=" & Month & _
                                "&trnExpYear=" & Year

                        Const PAYMENT_GATEWAY_URL As String = "https://www.beanstream.com/scripts/process_transaction.asp"

                        If SubmitTransaction(PAYMENT_GATEWAY_URL, trnRequest, trnResponse, errorMessage) = True Then

                            'Request successfully sent. Parse query string formated response string into a hash table for easy reference to response parameters.
                            rspHash = BuildResponseHashTable(trnResponse)

                            'DEBUG: Uncomment the following two lines to display the transaction request and response strings to the browser.
                            'lblRequest.Text = "<b>Request:</b><br>" & trnRequest
                            'lblResponse.Text = "<b>Response:</b><br>" & trnResponse

                            'Check the errorType response paramter to determine if any errors have been returned in the response.
                            Select Case CStr(rspHash("errorType"))
                                Case "N"
                                    'No errors returned in the response - Transaction has been processed successfully
                                    If CStr(rspHash("trnApproved")) = "1" Then
                                        Message = "Transaction approved" & vbLf _
                                & "Transaction ID: " & CStr(rspHash("trnId"))
                                        ReturnTransactionID = CStr(rspHash("trnId"))
                                        Return True
                                    Else
                                        'Transaction has been declined, redirect to the decline page passing all response parameters
                                        Message = "Transaction Declined" & vbLf _
                              & "Transaction ID: " & CStr(rspHash("messageText"))

                                        Return False
                                    End If
                                Case "U"
                                    Message = "<b>Please correct the following fields:</b> <UL>" & CStr(rspHash("messageText")) & "</UL>"
                                    Return False
                                Case "S"
                                    'System generated error detected due to merchant integration. Merchant must investigate the problem and correct their integration.
                                    Message = "<b>System Error:</b> " & CStr(rspHash("messageText")) & " Please contact support@merchantdomain.com<br><br>"
                                    Return False
                                Case Else
                                    'This condition may occur in cases where a response could not be received from the payment gateway.
                                    Message = "<b>Unexpected Response:</b> An unexpected response has been received processing your request.  Please contact support@merchantdomain.com.<br><br>"
                                    Return False
                            End Select
                        Else
                            'Request failed to send
                            Message = "<b>Unable to process payment:</b> " & errorMessage & "<br><br>"
                            Return False
                        End If
                    ElseIf PGateWay.CardConnect1 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Or PGateWay.CardConnect2 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwBluePay
                        ObjIcharge.MerchantLogin = CCommon.ToString(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        ObjIcharge.MerchantPassword = CCommon.ToString(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                        ObjIcharge.TransactionAmount = String.Format("{0:#####0.00}", Amount)

                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.TestMode = True
                        End If
                    End If

                End If


                If CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) >= 3 Then
                    Try
                        If CreditCardAuthOnly Then
                            ObjIcharge.AuthOnly() ' DO AUTHORIZATION Only
                        Else
                            ObjIcharge.Sale() ' DO Authorization and capture (Sales)
                        End If

                        If ObjIcharge.Response.Approved = True Then
                            Message = "Transaction Approved. No:" & ObjIcharge.Response.TransactionId    ' Evaluate Response Code
                            ReturnTransactionID = ObjIcharge.Response.TransactionId


                            If PGateWay.AuthorizeNet_CardNotPresent = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                                If ObjIcharge.Response.Code = "1" Then
                                    responseCode = "Approved"
                                ElseIf ObjIcharge.Response.Code = "3" Then
                                    responseCode = "Declined"
                                ElseIf ObjIcharge.Response.Code = "4" Then
                                    responseCode = "Held for review. (Approved is set to 'True', so transaction is assumed to be successful)"
                                End If
                            ElseIf PGateWay.CardConnect1 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Or PGateWay.CardConnect2 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                                If ObjIcharge.Response.Code = "0" Then
                                    responseCode = "Declined"
                                ElseIf ObjIcharge.Response.Code = "1" Then
                                    responseCode = "Approved"
                                End If
                            End If



                            Return True

                        Else
                            If PGateWay.FirstData = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                                If CCommon.ToString(ObjIcharge.Response.Code).ToLower() = "APPROVED".ToLower() Then
                                    Message = "Transaction Approved. No:" & ObjIcharge.Response.TransactionId    ' Evaluate Response Code
                                    ReturnTransactionID = ObjIcharge.Response.TransactionId
                                    Return True
                                Else
                                    Message = "<font color='red'>Transaction Declined:</font> Error Code: " + ObjIcharge.Response.Code + " " + ObjIcharge.Response.Text
                                    Return False
                                End If

                            ElseIf PGateWay.AuthorizeNet_CardNotPresent = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Or PGateWay.AuthorizeNet_CardPresent = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                                If CCommon.ToString(ObjIcharge.GetResponseVar("1")).ToLower() = "1".ToLower() AndAlso CCommon.ToString(ObjIcharge.Response.Text).ToLower().Contains("approved") = True Then
                                    Message = "Transaction Approved. No:" & ObjIcharge.Response.TransactionId    ' Evaluate Response Code
                                    ReturnTransactionID = ObjIcharge.Response.TransactionId
                                    Return True
                                Else
                                    Message = "<font color='red'>Transaction Declined:</font> Error Code: " + CCommon.ToString(ObjIcharge.GetResponseVar("1")) + " " + CCommon.ToString(ObjIcharge.GetResponseVar("3"))
                                    Return False
                                End If

                            ElseIf PGateWay.PayFlowPro = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                                If CCommon.ToString(ObjIcharge.Response.Code).ToLower() = "0".ToLower() Then
                                    Message = "Transaction Approved. No:" & ObjIcharge.Response.TransactionId    ' Evaluate Response Code
                                    ReturnTransactionID = ObjIcharge.Response.TransactionId
                                    Return True
                                Else
                                    Message = "<font color='red'>Transaction Declined:</font> Error Code: " + ObjIcharge.Response.Code + " " + ObjIcharge.Response.Text
                                    Return False
                                End If

                            ElseIf PGateWay.MonerisCanada = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                                If CCommon.ToString(ObjIcharge.Response.Code).ToLower() = "1".ToLower() Then
                                    Message = "Transaction Approved. No:" & ObjIcharge.Response.TransactionId    ' Evaluate Response Code
                                    ReturnTransactionID = ObjIcharge.Response.TransactionId
                                    Return True
                                Else
                                    Message = "<font color='red'>Transaction Declined:</font> Error Code: " + ObjIcharge.Response.Code + " " + ObjIcharge.Response.Text
                                    Return False
                                End If

                            ElseIf PGateWay.USAEpay = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                                If CCommon.ToString(ObjIcharge.Response.Code).ToLower() = "Approved".ToLower() Then
                                    Message = "Transaction Approved. No:" & ObjIcharge.Response.TransactionId    ' Evaluate Response Code
                                    ReturnTransactionID = ObjIcharge.Response.TransactionId
                                    Return True
                                Else
                                    Message = "<font color='red'>Transaction Declined:</font> Error Code: " + ObjIcharge.Response.Code + ": " + ObjIcharge.Response.Text
                                    Return False
                                End If

                            ElseIf PGateWay.BeanStream = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                                If CCommon.ToString(ObjIcharge.Response.Code).ToLower() = "1".ToLower() Then
                                    Message = "Transaction Approved. No:" & ObjIcharge.Response.TransactionId    ' Evaluate Response Code
                                    ReturnTransactionID = ObjIcharge.Response.TransactionId
                                    Return True
                                Else
                                    Message = "<font color='red'>Transaction Declined:</font> Error Code: " + ObjIcharge.Response.Code + " " + ObjIcharge.Response.Text
                                    Return False
                                End If
                            ElseIf PGateWay.Transaction_Central = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                                If CCommon.ToString(ObjIcharge.Response.Code).ToLower() = "Authorized".ToLower() Then
                                    Message = "Transaction Approved. No:" & ObjIcharge.Response.TransactionId    ' Evaluate Response Code
                                    ReturnTransactionID = ObjIcharge.Response.TransactionId
                                    Return True
                                Else
                                    Message = "<font color='red'>Transaction Declined:</font> Error Code: " + ObjIcharge.Response.Code + " " + ObjIcharge.Response.Text
                                    Return False
                                End If
                            ElseIf PGateWay.CardConnect1 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Or PGateWay.CardConnect2 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                                If ObjIcharge.Response.Approved Then
                                    Message = "Transaction Approved. No:" & ObjIcharge.Response.TransactionId    ' Evaluate Response Code
                                    ReturnTransactionID = ObjIcharge.Response.TransactionId
                                    Return True
                                Else
                                    Message = "<font color='red'>Transaction Declined:</font> Error Code: " + ObjIcharge.Response.Code + " " + ObjIcharge.Response.Text
                                    Return False
                                End If
                            End If
                        End If
                    Catch Ex As nsoftware.InPay.InPayException
                        Message = "Transaction Error: " + Ex.Message + "<br>"
                        Return False
                    End Try
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub GetContactDetails(ByVal ContactID As Long)
            Try
                Dim objContacts As New CContacts
                Dim dtAddContacts As DataTable
                objContacts.ContactID = ContactID
                objContacts.DomainID = DomainID
                'If ContactID = 0 Then
                '    objContacts.DivisionID = DivisionID
                'End If
                dtAddContacts = objContacts.GetBillOrgorContAdd


                _FirstName = CCommon.ToString(dtAddContacts.Rows(0).Item("vcFirstname"))
                _LastName = CCommon.ToString(dtAddContacts.Rows(0).Item("vcLastname"))
                _Email = CCommon.ToString(dtAddContacts.Rows(0).Item("vcEmail"))
                _Phone = CCommon.ToString(dtAddContacts.Rows(0).Item("vcPhone"))
                _Street = CCommon.ToString(dtAddContacts.Rows(0).Item("vcBillstreet"))
                _City = CCommon.ToString(dtAddContacts.Rows(0).Item("vcBillCity"))
                _Country = CCommon.ToString(dtAddContacts.Rows(0).Item("BillCountry"))
                _State = CCommon.ToString(dtAddContacts.Rows(0).Item("BillState"))
                _PinCode = CCommon.ToString(dtAddContacts.Rows(0).Item("vcBillPostCode"))

            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Private Function SubmitTransaction(ByVal url As String, ByVal trnRquest As String, ByRef trnResponse As String, ByRef errorMessage As String) As Boolean
            Try
                'Submit the query string formated transaction request to the payment gateway and retrieve the response.

                Dim myWriter As StreamWriter
                Dim objRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)

                objRequest.Method = "POST"
                objRequest.ContentLength = trnRquest.Length
                objRequest.ContentType = "application/x-www-form-urlencoded"

                Try
                    myWriter = New StreamWriter(objRequest.GetRequestStream())
                    myWriter.Write(trnRquest)
                Catch e As Exception
                    errorMessage = e.Message
                    Return False
                Finally
                    myWriter.Close()
                End Try

                Dim objResponse As HttpWebResponse = CType(objRequest.GetResponse(), HttpWebResponse)
                Dim sr As New StreamReader(objResponse.GetResponseStream())
                trnResponse = sr.ReadToEnd()
                sr.Close()
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function BuildResponseHashTable(ByVal qs As String) As Hashtable
            Try
                Dim rspHash As New Hashtable()
                Dim params As String()
                Dim nvPairs As String()
                Dim item As String
                Dim objServer As System.Web.HttpServerUtility

                'Parse query string to an array of name/value paramters
                params = Split(qs, "&")
                'Loop through each name/value paramters to add each to the hash table
                For Each item In params
                    'Split name/value into an array to be added to the hash table as key/value
                    nvPairs = Split(item, "=")
                    rspHash.Add(nvPairs(0), objServer.UrlDecode(nvPairs(1)))
                Next

                'Return the hash table generated from the passed query string paramter
                Return rspHash
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CaptureOrCreditAmount(ByVal strTransactionID As String, ByVal Amount As Decimal, ByVal IsCapture As Boolean, ByRef Message As String, Optional ByVal CardNo As String = "", Optional ByVal CVV As String = "", Optional ByVal Month As String = "", Optional ByVal Year As String = "", Optional ByVal paymentGateway As Integer = 0) As Boolean
            Try

                Dim strAmount As String = "0"
                Dim ObjIcharge As New Icharge
                ObjIcharge.Config("AllowPartialAuths=False")
                Dim dtGateWayDTL As DataTable
                Dim objCommon As New CCommon
                objCommon.DomainID = DomainID
                If paymentGateway > 0 Then
                    objCommon.PaymentGWId = paymentGateway
                End If
                dtGateWayDTL = objCommon.GetPaymentGatewayDetails

                strAmount = String.Format("{0:#####0.00}", Amount)
                If Not IsCapture Then
                    ObjIcharge.Card.Number = CardNo
                    ObjIcharge.Card.ExpMonth = Int32.Parse(Month)
                    ObjIcharge.Card.ExpYear = Int32.Parse(Year)
                    If CVV <> "" Then
                        ObjIcharge.Card.CVVData = CVV
                    End If
                End If

                If dtGateWayDTL.Rows.Count > 0 Then
                    If PGateWay.FirstData = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwFirstData
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "staging.linkpt.net:1129"
                        Else
                            ObjIcharge.GatewayURL = "secure.linkpt.net:1129"
                        End If
                        'This is your Store Number
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        'MerchantPassword is not used. Instead a SSL client certificate is used.
                        ObjIcharge.SSLCert = New Certificate(CertStoreTypes.cstPEMKeyFile, CCommon.GetDocumentPhysicalPath(DomainID) & CCommon.ToString(dtGateWayDTL.Rows(0).Item("vcSecndFldValue")), "", "*")
                    ElseIf PGateWay.AuthorizeNet_CardNotPresent = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwAuthorizeNet
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "https://test.authorize.net/gateway/transact.dll"
                            ObjIcharge.AddSpecialField("x_test_request", "true")
                        Else
                            ObjIcharge.GatewayURL = "https://secure.authorize.net/gateway/transact.dll"
                        End If
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcThirdFldValue"))
                    ElseIf PGateWay.Transaction_Central = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwTransactionCentral
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.AddSpecialField("x_test_request", "true")
                        Else
                            ObjIcharge.AddSpecialField("x_test_request", "false")
                        End If
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                    ElseIf PGateWay.PayFlowPro = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwPayFlowPro
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "https://pilot-payflowpro.paypal.com"
                        Else
                            ObjIcharge.GatewayURL = "https://payflowpro.paypal.com"
                        End If
                        ObjIcharge.TransactionId = strTransactionID
                        ObjIcharge.SpecialFields(1).Value = CStr(dtGateWayDTL.Rows(0).Item("vcThirdFldValue")) 'PARTNER
                        ObjIcharge.AddSpecialField("USER", CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue")))
                        ObjIcharge.AddSpecialField("VENDOR", CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue")))
                        ObjIcharge.AddSpecialField("PWD", CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue")))
                        ObjIcharge.AddSpecialField("TENDER", "C")
                        'TRXTYPE Type of transaction that should be processed. Allowed
                        'transaction types: Sale (S), Void (V), Inquiry (I).
                        ObjIcharge.AddSpecialField("TRXTYPE", "S")
                        strAmount = String.Format("{0:#####0.00}", Amount) 'Only 2 decimal points are allowed for payflow
                    ElseIf PGateWay.USAEpay = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        Dim usaepay As New USAePayAPI.USAePay
                        usaepay.SourceKey = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        usaepay.Pin = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                        usaepay.Currency = "840"
                        usaepay.IgnoreDupe = True
                        usaepay.UseSandbox = CBool(dtGateWayDTL.Rows(0).Item("bitTest"))

                        usaepay.Amount = Amount
                        usaepay.Invoice = strTransactionID

                        Return usaepay.Capture()
                    ElseIf PGateWay.BeanStream = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then

                    ElseIf PGateWay.MonerisCanada = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then

                    ElseIf PGateWay.CardConnect1 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Or PGateWay.CardConnect2 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwBluePay
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                        strAmount = String.Format("{0:#####0.00}", Amount) 'Only 2 decimal points for BluePay

                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.TestMode = True
                        End If
                    End If
                End If

                If CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) >= 3 Then
                    Try
                        If IsCapture Then
                            ObjIcharge.Capture(strTransactionID, strAmount)
                        Else
                            ObjIcharge.TransactionId = strTransactionID
                            ObjIcharge.TransactionAmount = strAmount
                            ObjIcharge.Credit()
                        End If

                        If (ObjIcharge.Response.Approved) Then
                            Return True
                        Else
                            Message = "<font color='red'>Transaction Declined:</font> " + ObjIcharge.Response.Text
                            Return False
                        End If
                    Catch Ex As nsoftware.InPay.InPayException
                        Message = "Transaction Error: " + Ex.Message + "<br>"
                        Return False
                    End Try
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function VoidTransaction(ByVal strTransactionID As String, ByRef Message As String) As Boolean
            Try
                'This method voids a transaction that has been previously authorized, but which has not yet gone to settlement, or been "captured". The TransactionId parameter indicates to the Gateway which transaction is to be voided, and should contain the TransactionId from the original transaction. 
                'Please see the gateway information in the table of contents to determine if your gateway supports Void transactions. 
                'To cancel a transaction which has already been captured, use the Credit method.
                'The LinkPoint gateway requires you send the InvoiceNumber rather than the TransactionId. For the PSIGateXML Gateway, send the TransactionId as normal. 


                'Dim strAmount As String = "0"
                Dim ObjIcharge As New Icharge
                ObjIcharge.Config("AllowPartialAuths=False")
                Dim dtGateWayDTL As DataTable
                Dim objCommon As New CCommon
                objCommon.DomainID = DomainID
                dtGateWayDTL = objCommon.GetPaymentGatewayDetails

                'strAmount = String.Format("{0:#####0.00}", Amount)
                If dtGateWayDTL.Rows.Count > 0 Then
                    If PGateWay.FirstData = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwFirstData
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "staging.linkpt.net:1129"
                        Else
                            ObjIcharge.GatewayURL = "secure.linkpt.net:1129"
                        End If
                        'This is your Store Number
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        'MerchantPassword is not used. Instead a SSL client certificate is used.
                        ObjIcharge.SSLCert = New Certificate(CertStoreTypes.cstPEMKeyFile, CCommon.GetDocumentPhysicalPath(DomainID) & CCommon.ToString(dtGateWayDTL.Rows(0).Item("vcSecndFldValue")), "", "*")

                        'throws error
                        'Dim tdate As String = ObjIcharge.GetResponseVar("/root/r_tdate")
                        'ObjIcharge.AddSpecialField("tdate", tdate)

                    ElseIf PGateWay.AuthorizeNet_CardNotPresent = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwAuthorizeNet
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "https://test.authorize.net/gateway/transact.dll"
                            ObjIcharge.AddSpecialField("x_test_request", "true")
                        Else
                            ObjIcharge.GatewayURL = "https://secure.authorize.net/gateway/transact.dll"
                        End If
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcThirdFldValue"))
                    ElseIf PGateWay.Transaction_Central = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwTransactionCentral
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.AddSpecialField("x_test_request", "true")
                        Else
                            ObjIcharge.AddSpecialField("x_test_request", "false")
                        End If
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                    ElseIf PGateWay.PayFlowPro = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwPayFlowPro
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "https://pilot-payflowpro.paypal.com"
                        Else
                            ObjIcharge.GatewayURL = "https://payflowpro.paypal.com"
                        End If
                        ObjIcharge.TransactionId = strTransactionID
                        ObjIcharge.SpecialFields(1).Value = CStr(dtGateWayDTL.Rows(0).Item("vcThirdFldValue")) 'PARTNER
                        ObjIcharge.AddSpecialField("USER", CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue")))
                        ObjIcharge.AddSpecialField("VENDOR", CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue")))
                        ObjIcharge.AddSpecialField("PWD", CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue")))
                        ObjIcharge.AddSpecialField("TENDER", "C")
                        'TRXTYPE Type of transaction that should be processed. Allowed
                        'transaction types: Sale (S), Void (V), Inquiry (I).
                        ObjIcharge.AddSpecialField("TRXTYPE", "S")
                        'strAmount = String.Format("{0:#####0.00}", Amount) 'Only 2 decimal points are allowed for payflow
                    ElseIf PGateWay.USAEpay = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        Dim usaepay As New USAePayAPI.USAePay
                        usaepay.SourceKey = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        usaepay.Pin = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                        usaepay.Currency = "840"
                        usaepay.IgnoreDupe = True
                        usaepay.UseSandbox = CBool(dtGateWayDTL.Rows(0).Item("bitTest"))

                        'usaepay.Amount = Amount
                        usaepay.Invoice = strTransactionID

                        Return usaepay.Void()
                    ElseIf PGateWay.BeanStream = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then

                    ElseIf PGateWay.MonerisCanada = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then

                    ElseIf PGateWay.CardConnect1 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Or PGateWay.CardConnect2 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwBluePay
                        ObjIcharge.MerchantLogin = CCommon.ToString(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        ObjIcharge.MerchantPassword = CCommon.ToString(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.TestMode = True
                        End If
                    End If
                End If

                If CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) >= 3 Then
                    Try
                        ObjIcharge.VoidTransaction(strTransactionID)
                        If (ObjIcharge.Response.Approved) Then
                            Return True
                        Else
                            Message = "<font color='red'>Transaction Declined:</font> " + ObjIcharge.Response.Text
                            Return False
                        End If
                    Catch Ex As nsoftware.InPay.InPayException
                        Message = "Transaction Error: " + Ex.Message + "<br>"
                        Return False
                    End Try
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'This method credits a transaction that has already been captured, or settled. If the transaction is still outstanding use the VoidTransaction method instead. The TransactionId parameter indicates to the Gateway which transaction is to be voided, and should contain the TransactionId from the original transaction. The CreditAmount parameter is the value to be credited back to the customer, and can be all or part of the original TransactionAmount 
        'Please see the gateway information in the table of contents to determine if your gateway supports Credit transactions. 
        'The gw3DSI gateway requires the following additional fields for Credit transactions:
        '    component.AddSpecialField "UserId", "my 3DSI-assigned UserId" '(Different than MerchantLogin)
        'component.MerchantPassword = "my 3DSI-assigned Pwd"
        'The LinkPoint and PSIGateXML gateways require you send the InvoiceNumber rather than the TransactionId.
        'Note: For the TrustCommerce Gateway, if you call the Credit method with an empty TransactionId parameter, a "credit2" transaction will be sent instead of a normal credit. This credit is not based on a previously authorized transaction, and so you must include the Number, ExpMonth, and ExpYear fields. This is a special feature on your account that must be activated by TrustCommerce. 


        Public Function ValidateCard(ByVal CardNo As String, ByVal Month As String, ByVal Year As String) As String
            Try
                Dim ObjIcharge As New Icharge
                Dim objCardvalidator As New Cardvalidator

                Try
                    ObjIcharge.Card.Number = CardNo
                    ObjIcharge.Card.ExpMonth = Int32.Parse(Month)
                    ObjIcharge.Card.ExpYear = Int32.Parse(Year)

                    objCardvalidator.CardNumber = ObjIcharge.Card.Number
                    objCardvalidator.CardExpMonth = ObjIcharge.Card.ExpMonth
                    objCardvalidator.CardExpYear = ObjIcharge.Card.ExpYear

                    objCardvalidator.ValidateCard()
                Catch ex As nsoftware.InPay.InPayException
                    Return "Card validation failed: " + ex.Message
                End Try
                Return ""
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function RefundAmount(ByVal strTransactionID As String, ByVal Amount As Decimal, ByRef Message As String, Optional ByVal CardNo As String = "", Optional ByVal CVV As String = "", Optional ByVal Month As String = "", Optional ByVal Year As String = "", Optional ByVal paymentGateway As Integer = 0) As Boolean
            Try

                Dim strAmount As String = String.Format("{0:#####0.00}", Amount)

                Dim ObjIcharge As New Icharge
                ObjIcharge.Config("AllowPartialAuths=False")
                ObjIcharge.Card.Number = CardNo
                ObjIcharge.Card.ExpMonth = Int32.Parse(Month)
                ObjIcharge.Card.ExpYear = Int32.Parse(Year)
                If CVV <> "" Then
                    ObjIcharge.Card.CVVData = CVV
                End If

                Dim objCommon As New CCommon
                objCommon.DomainID = DomainID
                If paymentGateway > 0 Then
                    objCommon.PaymentGWId = paymentGateway
                End If
                Dim dtGateWayDTL As DataTable = objCommon.GetPaymentGatewayDetails

                If dtGateWayDTL.Rows.Count > 0 Then
                    If PGateWay.FirstData = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwFirstData
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "staging.linkpt.net:1129"
                        Else
                            ObjIcharge.GatewayURL = "secure.linkpt.net:1129"
                        End If
                        'This is your Store Number
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        'MerchantPassword is not used. Instead a SSL client certificate is used.
                        ObjIcharge.SSLCert = New Certificate(CertStoreTypes.cstPEMKeyFile, CCommon.GetDocumentPhysicalPath(DomainID) & CCommon.ToString(dtGateWayDTL.Rows(0).Item("vcSecndFldValue")), "", "*")
                    ElseIf PGateWay.AuthorizeNet_CardNotPresent = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwAuthorizeNet
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "https://test.authorize.net/gateway/transact.dll"
                            ObjIcharge.AddSpecialField("x_test_request", "true")
                        Else
                            ObjIcharge.GatewayURL = "https://secure.authorize.net/gateway/transact.dll"
                        End If
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcThirdFldValue"))
                    ElseIf PGateWay.Transaction_Central = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwTransactionCentral
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.AddSpecialField("x_test_request", "true")
                        Else
                            ObjIcharge.AddSpecialField("x_test_request", "false")
                        End If
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                    ElseIf PGateWay.PayFlowPro = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwPayFlowPro
                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.GatewayURL = "https://pilot-payflowpro.paypal.com"
                        Else
                            ObjIcharge.GatewayURL = "https://payflowpro.paypal.com"
                        End If
                        ObjIcharge.TransactionId = strTransactionID
                        ObjIcharge.SpecialFields(1).Value = CStr(dtGateWayDTL.Rows(0).Item("vcThirdFldValue")) 'PARTNER
                        ObjIcharge.AddSpecialField("USER", CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue")))
                        ObjIcharge.AddSpecialField("VENDOR", CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue")))
                        ObjIcharge.AddSpecialField("PWD", CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue")))
                        ObjIcharge.AddSpecialField("TENDER", "C")
                        'TRXTYPE Type of transaction that should be processed. Allowed
                        'transaction types: Sale (S), Void (V), Inquiry (I).
                        ObjIcharge.AddSpecialField("TRXTYPE", "S")
                        strAmount = String.Format("{0:#####0.00}", Amount) 'Only 2 decimal points are allowed for payflow
                    ElseIf PGateWay.USAEpay = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        Dim usaepay As New USAePayAPI.USAePay
                        usaepay.SourceKey = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        usaepay.Pin = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                        usaepay.Currency = "840"
                        usaepay.IgnoreDupe = True
                        usaepay.UseSandbox = CBool(dtGateWayDTL.Rows(0).Item("bitTest"))

                        usaepay.Amount = Amount
                        usaepay.Invoice = strTransactionID

                        Return usaepay.Capture()
                    ElseIf PGateWay.BeanStream = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then

                    ElseIf PGateWay.MonerisCanada = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then

                    ElseIf PGateWay.CardConnect1 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Or PGateWay.CardConnect2 = CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) Then
                        ObjIcharge.Gateway = IchargeGateways.gwBluePay
                        ObjIcharge.MerchantLogin = CStr(dtGateWayDTL.Rows(0).Item("vcFirstFldValue"))
                        ObjIcharge.MerchantPassword = CStr(dtGateWayDTL.Rows(0).Item("vcSecndFldValue"))
                        strAmount = String.Format("{0:#####0.00}", Amount) 'Only 2 decimal points for BluePay

                        If CBool(dtGateWayDTL.Rows(0).Item("bitTest")) = True Then
                            ObjIcharge.TestMode = True
                        End If
                    End If
                End If

                If CInt(dtGateWayDTL.Rows(0).Item("intPaymentGateWay")) >= 3 Then
                    Try
                        ObjIcharge.Refund(strTransactionID, strAmount)

                        If (ObjIcharge.Response.Approved) Then
                            Return True
                        Else
                            Message = "<font color='red'>Transaction Declined:</font> " + ObjIcharge.Response.Text
                            Return False
                        End If
                    Catch Ex As nsoftware.InPay.InPayException
                        Message = "Transaction Error: " + Ex.Message + "<br>"
                        Return False
                    End Try
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDefaultGateway() As Integer
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", SiteID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToInteger(SqlDAL.ExecuteScalar(connString, "USP_PaymentGateway_GetDefault", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetCardConnectGateway() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", SiteID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_PaymentGateway_GetCardConnect", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
End Namespace
