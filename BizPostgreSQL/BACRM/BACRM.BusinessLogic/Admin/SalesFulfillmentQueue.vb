﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin
    Public Class SalesFulfillmentQueue
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"
        Public SFQID As Long
        Public Message As String
#End Region

        Public Function GetRecords() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_SalesFulfillmentQueue_Get", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub UpdateStatus(ByVal tintRule As Short, ByVal IsRuleExecutedSuccessfully As Boolean)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSFQID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = SFQID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcMessage", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = Message

                arParms(3) = New Npgsql.NpgsqlParameter("@tintRule", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = tintRule

                arParms(4) = New Npgsql.NpgsqlParameter("@bitSuccess", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = IsRuleExecutedSuccessfully

                SqlDAL.ExecuteNonQuery(connString, "USP_SalesFulfillmentQueue_UpdateStatus", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace
