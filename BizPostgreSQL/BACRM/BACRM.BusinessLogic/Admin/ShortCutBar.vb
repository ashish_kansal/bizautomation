Option Explicit On
Option Strict On
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Namespace BACRM.BusinessLogic.Admin

    Public Class CShortCutBar
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _GroupId As Long
        'Private DomainId As Long
        Private _StrFav As String
        Private _StrRec As String
        Private _strName As String
        Private _strUrl As String
        Private _ContactId As Long
        Private _LinkId As Long
        Private _ByteMode As Boolean
        Public Property ByteMode() As Boolean
            Get
                Return _ByteMode
            End Get
            Set(ByVal Value As Boolean)
                _ByteMode = Value
            End Set
        End Property
        Public Property LinkId() As Long
            Get
                Return _LinkId
            End Get
            Set(ByVal Value As Long)
                _LinkId = Value
            End Set
        End Property
        Public Property GroupId() As Long
            Get
                Return _GroupId
            End Get
            Set(ByVal Value As Long)
                _GroupId = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property StrFav() As String
            Get
                Return _StrFav
            End Get
            Set(ByVal Value As String)
                _StrFav = Value
            End Set
        End Property

        Public Property StrRec() As String
            Get
                Return _StrRec
            End Get
            Set(ByVal Value As String)
                _StrRec = Value
            End Set
        End Property
        Public Property strName() As String
            Get
                Return _strName
            End Get
            Set(ByVal Value As String)
                _strName = Value
            End Set
        End Property
        Public Property strUrl() As String
            Get
                Return _strUrl
            End Get
            Set(ByVal Value As String)
                _strUrl = Value
            End Set
        End Property
        Public Property ContactId() As Long
            Get
                Return _ContactId
            End Get
            Set(ByVal Value As Long)
                _ContactId = Value
            End Set
        End Property

        Private _TabId As Long
        Public Property TabId() As Long
            Get
                Return _TabId
            End Get
            Set(ByVal Value As Long)
                _TabId = Value
            End Set
        End Property
        Public Property DefaultTab As Boolean

        Public Function GetAvailableFields() As DataSet

            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _TabId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetFieldsShrtBar", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveAllowedFields() As Boolean

            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@strFav", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _StrFav

                arParms(3) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _TabId

                arParms(4) = New Npgsql.NpgsqlParameter("@bitDefaultTab", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = DefaultTab

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageShortCutGroupConf", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveCustomFields() As Boolean

            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@strName", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _strName

                arParms(3) = New Npgsql.NpgsqlParameter("@strUrl", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _strUrl

                arParms(4) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _ContactId

                arParms(5) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _TabId


                SqlDAL.ExecuteNonQuery(connString, "USP_SaveShrtCutBarCustomFields", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetAvailableFieldsUser() As DataSet

            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ContactId

                arParms(3) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _TabId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetAvailableFieldsUser", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveDisplayFieldsUser() As Boolean

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ContactId

                arParms(3) = New Npgsql.NpgsqlParameter("@strFav", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _StrFav

                arParms(4) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _TabId

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageShortCutUsrCnf", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function GetCustomFldsGrp() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ContactId

                arParms(3) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _TabId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetShrtBarCustomFldsGrp", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub EditDeleteCustomFldGrp()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ByteMode", NpgsqlTypes.NpgsqlDbType.Bit, 9)
                arParms(0).Value = _ByteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numLinkId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _LinkId

                arParms(2) = New Npgsql.NpgsqlParameter("@Name", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _strName

                arParms(3) = New Npgsql.NpgsqlParameter("@Link", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _strUrl

                SqlDAL.ExecuteNonQuery(connString, "USP_EditDelShortCutGrpCnf", arParms)
            Catch ex As Exception
                Throw ex
            End Try

        End Sub
        Function GetMenu() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ContactId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMenuShortCut", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetInitialPageDetails() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ContactId

                arParms(3) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _TabId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetInitialPageDetails", arParms)
                Return ds

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function SetInitialPageURL() As Boolean

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GroupId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numTabId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _TabId

                arParms(3) = New Npgsql.NpgsqlParameter("@numLinkID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _LinkId

                SqlDAL.ExecuteNonQuery(connString, "USP_ShortCutGrpConf_SetDefaultURL", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace