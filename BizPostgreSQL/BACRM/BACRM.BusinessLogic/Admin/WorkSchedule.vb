﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Admin

    Public Class WorkSchedule
        Inherits UserAccess

#Region "Public Properties"

        Public Property WorkScheduleID As Long
        Public Property WorkHours As Short
        Public Property WorkMinutes As Short
        Public Property ProductiveHours As Short
        Public Property ProductiveMinutes As Short
        Public Property StartOfDay As TimeSpan

        ''' <summary>
        ''' Employee working days
        ''' </summary>
        ''' <value>1,2,3,4,5,6,7</value>
        ''' <returns>1,2,3,4,5,6,7</returns>
        ''' <remarks>Comma seperated values for Days Monday(1) to Sunday(7)</remarks>
        Public Property WorkDays As String

#End Region

#Region "Public Methods"

        Public Function GetAll() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WorkSchedule_GetAll", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub Save()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numWorkScheduleID", WorkScheduleID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcWorkDays", WorkDays, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numWorkHours", WorkHours, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numWorkMinutes", WorkMinutes, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numProductiveHours", ProductiveHours, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numProductiveMinutes", ProductiveMinutes, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tmStartOfDay", StartOfDay, NpgsqlTypes.NpgsqlDbType.Time))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_WorkSchedule_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class

End Namespace


