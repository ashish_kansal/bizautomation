﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports System.Data.SqlClient

Namespace BACRM.BusinessLogic.Admin

    Public Class ShippingFieldValues
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Methods"

        Public Function GetAllByDomain() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_ShippingFieldValues_GetAllByDomain", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region


    End Class

End Namespace
