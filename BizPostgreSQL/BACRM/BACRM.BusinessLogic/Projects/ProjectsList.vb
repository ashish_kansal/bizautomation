'Created Anoop Jayaraj
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Projects
    Public Class ProjectsList
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _SortOrder As Short
        'Private DomainId As Long
        Private _UserRightType As Long
        'Private UserCntID As Long = 1
        Private _ListName As String
        Private _bitDeleted As Short
        Private _DivisionID As Long
        Private _TerritoryID As Long
        Private _ProType As Short
        Private _ContactID As Long
        Private _ContractID As Long
        Private _ProjectProcessId As Long
        Private _ContactType As Long
        Private _OpportunityId As Long
        Private _ProjectName As String
        Private _EstimatedCloseDate As Long
        Private _ProComments As String
        Private _PublicFlag As Short
        Private _strMilestone As String = ""
        Private _strSubStage As String = ""
        Private _bytemode As Short
        Private _OppStageID As Long
        Private _SubStageID As Long
        Private _ProjectId As Long = 0
        Private _IntPrjMgr As Long
        Private _CusPrjMgr As Long
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _CustName As String
        Private _FirstName As String
        Private _LastName As String
        Private _DueDate As Date = New Date(1753, 1, 1)
        Private _AssignedTo As Long
        Private _bitPartner As Boolean
        Private _strType As String
        Private _strOppSel As String
        Private _ContactRole As Long
        Private _ClientTimeZoneOffset As Integer
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public Property ContactRole() As Long
            Get
                Return _ContactRole
            End Get
            Set(ByVal Value As Long)
                _ContactRole = Value
            End Set
        End Property

        Public Property strOppSel() As String
            Get
                Return _strOppSel
            End Get
            Set(ByVal Value As String)
                _strOppSel = Value
            End Set
        End Property
        Public Property bitPartner() As Boolean
            Get
                Return _bitPartner
            End Get
            Set(ByVal Value As Boolean)
                _bitPartner = Value
            End Set
        End Property


        Public Property AssignedTo() As Long
            Get
                Return _AssignedTo
            End Get
            Set(ByVal Value As Long)
                _AssignedTo = Value
            End Set
        End Property

        Public Property DueDate() As Date
            Get
                Return _DueDate
            End Get
            Set(ByVal Value As Date)
                _DueDate = Value
            End Set
        End Property

        Public Property CustName() As String
            Get
                Return _CustName
            End Get
            Set(ByVal Value As String)
                _CustName = Value
            End Set
        End Property

        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property
        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property


        Public Property ProjectName() As String
            Get
                Return _ProjectName
            End Get
            Set(ByVal Value As String)
                _ProjectName = Value
            End Set
        End Property

        Public Property CusPrjMgr() As Long
            Get
                Return _CusPrjMgr
            End Get
            Set(ByVal Value As Long)
                _CusPrjMgr = Value
            End Set
        End Property

        Public Property IntPrjMgr() As Long
            Get
                Return _IntPrjMgr
            End Get
            Set(ByVal Value As Long)
                _IntPrjMgr = Value
            End Set
        End Property

        Public Property ProjectId() As Long
            Get
                Return _ProjectId
            End Get
            Set(ByVal Value As Long)
                _ProjectId = Value
            End Set
        End Property

        Public Property SubStageID() As Long
            Get
                Return _SubStageID
            End Get
            Set(ByVal Value As Long)
                _SubStageID = Value
            End Set
        End Property

        Public Property OppStageID() As Long
            Get
                Return _OppStageID
            End Get
            Set(ByVal Value As Long)
                _OppStageID = Value
            End Set
        End Property

        Public Property strSubStage() As String
            Get
                Return _strSubStage
            End Get
            Set(ByVal Value As String)
                _strSubStage = Value
            End Set
        End Property


        Public Property bytemode() As Short
            Get
                Return _bytemode
            End Get
            Set(ByVal Value As Short)
                _bytemode = Value
            End Set
        End Property




        Public Property strMilestone() As String
            Get
                Return _strMilestone
            End Get
            Set(ByVal Value As String)
                _strMilestone = Value
            End Set
        End Property


        Public Property SortOrder() As Short
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Short)
                _SortOrder = Value
            End Set
        End Property



        Public Property PublicFlag() As Short
            Get
                Return _PublicFlag
            End Get
            Set(ByVal Value As Short)
                _PublicFlag = Value
            End Set
        End Property

        Public Property ProComments() As String
            Get
                Return _ProComments
            End Get
            Set(ByVal Value As String)
                _ProComments = Value
            End Set
        End Property

        Public Property OpportunityId() As Long
            Get
                Return _OpportunityId
            End Get
            Set(ByVal Value As Long)
                _OpportunityId = Value
            End Set
        End Property

        Public Property ContactType() As Long
            Get
                Return _ContactType
            End Get
            Set(ByVal Value As Long)
                _ContactType = Value
            End Set
        End Property


        Public Property ProjectProcessId() As Long
            Get
                Return _ProjectProcessId
            End Get
            Set(ByVal Value As Long)
                _ProjectProcessId = Value
            End Set
        End Property

        Public Property ContractID() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property ProType() As Short
            Get
                Return _ProType
            End Get
            Set(ByVal Value As Short)
                _ProType = Value
            End Set
        End Property

        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property bitDeleted() As Short
            Get
                Return _bitDeleted
            End Get
            Set(ByVal Value As Short)
                _bitDeleted = Value
            End Set
        End Property

        Public Property ListName() As String
            Get
                Return _ListName
            End Get
            Set(ByVal Value As String)
                _ListName = Value
            End Set
        End Property


        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property UserRightType() As Long
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Long)
                _UserRightType = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property strType() As String
            Get
                Return _strType
            End Get
            Set(ByVal Value As String)
                _strType = Value
            End Set
        End Property

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region


        Public Function GetProjectList() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = DomainID


                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRightType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _SortOrder


                arParms(4) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(4).Value = _SortCharacter


                arParms(5) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(5).Value = IIf(_FirstName = "", "", Replace(_FirstName, "'", "''"))

                arParms(6) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(6).Value = IIf(_LastName = "", "", Replace(_LastName, "'", "''"))

                arParms(7) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(7).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(8) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _CurrentPage

                arParms(9) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _PageSize

                arParms(10) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Direction = ParameterDirection.InputOutput
                arParms(10).Value = _TotalRecords

                arParms(11) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(11).Value = _columnName

                arParms(12) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(12).Value = _columnSortOrder


                arParms(13) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(13).Value = _DivisionID


                arParms(14) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _bitPartner

                arParms(15) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(15).Value = _ClientTimeZoneOffset

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                Dim dtTable As DataTable
                ds = SqlDAL.ExecuteDataset(connString, "USP_ProjectList1", arParms)
                dtTable = ds.Tables(0)
                _TotalRecords = CInt(dtTable.Rows(0).Item(0))
                dtTable.Rows.RemoveAt(0)
                'End If

                Return dtTable

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function Save() As String()

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _ProjectId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numIntPrjMgr", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _IntPrjMgr

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _OpportunityId

                arParms(4) = New Npgsql.NpgsqlParameter("@numCustPrjMgr", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _CusPrjMgr

                arParms(5) = New Npgsql.NpgsqlParameter("@vcPProName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _ProjectName

                arParms(6) = New Npgsql.NpgsqlParameter("@Comments", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(6).Value = _ProComments

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@numAssignedTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _AssignedTo

                arParms(9) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = DomainId

                arParms(10) = New Npgsql.NpgsqlParameter("@dtDueDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(10).Value = _DueDate

                arParms(11) = New Npgsql.NpgsqlParameter("@numContractID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _ContractID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ProjectManage", objParam, True)

                Dim arrOutPut() As String = New String(2) {}
                arrOutPut(0) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                arrOutPut(1) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)
                Return arrOutPut

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ProjectDetail() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_Projects", arParms)

                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function AssCntsByProId() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ProContacts", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SalesProcessDtlByProId() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection

                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@ProcessID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectProcessId

                arParms(1) = New Npgsql.NpgsqlParameter("@ContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@DomianID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProjectId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ProMilestone", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function SalesProcessDtlByProcessId() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@ProcessID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectProcessId

                arParms(1) = New Npgsql.NpgsqlParameter("@ContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@DomianID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProjectId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ProMilestone", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function DeleteProjects() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId


                SqlDAL.ExecuteNonQuery(connString, "Usp_DeleteProject", arParms)
                Return True
            Catch ex As Exception
                Return False
                Throw ex
            End Try
        End Function
        Sub SaveProjectOpportunities()
            Try
                Dim getconnection As New GetConnection

                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomianID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@strOppid", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(2).Value = _strOppSel

                SqlDAL.ExecuteNonQuery(connString, "Usp_SaveProjectOpportunities", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function GetProOpportunities() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetProjectOpportunities", arParms)

                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function

    End Class
End Namespace