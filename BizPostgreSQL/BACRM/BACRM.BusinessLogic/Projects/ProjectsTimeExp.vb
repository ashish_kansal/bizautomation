'Created Anoop Jayaraj
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Projects

    Public Class ProjectsTimeExp
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _ProID As Long
        Private _ProStageID As Long
        Private _Rate As Decimal
        Private _Hour As Long
        Private _Min As Short
        Private _Amount As Long
        Private _Desc As String
        Private _ItemID As Long
        Private _Billable As Boolean = True
        Private _ContractID As Long = 0
        Private _FromDate As Date = New Date(1753, 1, 1)
        Private _ToDate As Date = New Date(1753, 1, 1)
        Private _ContactID As Long
        Private _OppID As Long = 0
        Private _ClientTimeZoneOffset As Integer = 0
        Private _DomainId As Long
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal Value As Long)
                _OppID = Value
            End Set
        End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property
        Public Property ContractID() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property
        Public Property Billable() As Boolean
            Get
                Return _Billable
            End Get
            Set(ByVal Value As Boolean)
                _Billable = Value
            End Set
        End Property
        Public Property ItemID() As Long
            Get
                Return _ItemID
            End Get
            Set(ByVal Value As Long)
                _ItemID = Value
            End Set
        End Property

        Public Property Desc() As String
            Get
                Return _Desc
            End Get
            Set(ByVal Value As String)
                _Desc = Value
            End Set
        End Property

        Public Property Amount() As Long
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Long)
                _Amount = Value
            End Set
        End Property


        Public Property Min() As Short
            Get
                Return _Min
            End Get
            Set(ByVal Value As Short)
                _Min = Value
            End Set
        End Property

        Public Property Hour() As Long
            Get
                Return _Hour
            End Get
            Set(ByVal Value As Long)
                _Hour = Value
            End Set
        End Property

        Public Property Rate() As Decimal
            Get
                Return _Rate
            End Get
            Set(ByVal Value As Decimal)
                _Rate = Value
            End Set
        End Property

        Public Property ProStageID() As Long
            Get
                Return _ProStageID
            End Get
            Set(ByVal Value As Long)
                _ProStageID = Value
            End Set
        End Property


        Public Property ProID() As Long
            Get
                Return _ProID
            End Get
            Set(ByVal Value As Long)
                _ProID = Value
            End Set
        End Property

        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        Public Property DomainId() As Long
            Get
                Return _DomainId
            End Get
            Set(ByVal value As Long)
                _DomainId = value
            End Set
        End Property

#Region "Constructor"
        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32               
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '**********************************************************************************
        Public Sub New(ByVal intUserId As Integer)
            'Constructor
            MyBase.New(intUserId)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32              
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '**********************************************************************************
        Public Sub New()
            'Constructor
            MyBase.New()
        End Sub
#End Region


        Public Function GetTimeDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As SqlParameter = New SqlParameter(14) {}


                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 1


                arParms(1) = New SqlParameter("@numProId", SqlDbType.BigInt)
                arParms(1).Value = _ProID

                arParms(2) = New SqlParameter("@numProStageID", SqlDbType.BigInt)
                arParms(2).Value = _ProStageID

                arParms(3) = New SqlParameter("@numRate", SqlDbType.BigInt)
                arParms(3).Value = _Rate

                'arParms(4) = New SqlParameter("@numHours", SqlDbType.BigInt)
                'arParms(4).Value = _Hour


                'arParms(5) = New SqlParameter("@numMins", SqlDbType.BigInt)
                'arParms(5).Value = _Min

                arParms(6) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(6).Value = _Desc

                arParms(7) = New SqlParameter("@numItemID", SqlDbType.BigInt, 9)
                arParms(7).Value = _ItemID

                arParms(8) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(8).Value = _Billable

                arParms(9) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(9).Value = _ContractID

                arParms(10) = New SqlParameter("@dtFromDate", SqlDbType.DateTime)
                arParms(10).Value = _FromDate

                arParms(11) = New SqlParameter("@dtToDate", SqlDbType.DateTime)
                arParms(11).Value = _ToDate

                arParms(12) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(12).Value = _ContactID

                arParms(13) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(13).Value = 0
                arParms(14) = New SqlParameter("@ClientTimeZoneOffset", SqlDbType.Int)
                arParms(14).Value = _ClientTimeZoneOffset
                ds = SqlDAL.ExecuteDataset(ConnString, "usp_ProTime", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function SaveTimeDetails() As Boolean
            Try
                Dim arParms() As SqlParameter = New SqlParameter(14) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString



                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 0


                arParms(1) = New SqlParameter("@numProId", SqlDbType.BigInt)
                arParms(1).Value = _ProID

                arParms(2) = New SqlParameter("@numProStageID", SqlDbType.BigInt)
                arParms(2).Value = _ProStageID

                arParms(3) = New SqlParameter("@numRate", SqlDbType.Decimal)
                arParms(3).Value = _Rate

                'arParms(4) = New SqlParameter("@numHours", SqlDbType.BigInt)
                'arParms(4).Value = _Hour


                'arParms(5) = New SqlParameter("@numMins", SqlDbType.BigInt)
                'arParms(5).Value = _Min

                arParms(6) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(6).Value = _Desc

                arParms(7) = New SqlParameter("@numItemID", SqlDbType.BigInt, 9)
                arParms(7).Value = _ItemID

                arParms(8) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(8).Value = _Billable

                arParms(9) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(9).Value = _ContractID

                arParms(10) = New SqlParameter("@dtFromDate", SqlDbType.DateTime)
                arParms(10).Value = _FromDate

                arParms(11) = New SqlParameter("@dtToDate", SqlDbType.DateTime)
                arParms(11).Value = _ToDate

                arParms(12) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(12).Value = _ContactID

                arParms(13) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(13).Value = _OppID
                arParms(14) = New SqlParameter("@ClientTimeZoneOffset", SqlDbType.Int)
                arParms(14).Value = _ClientTimeZoneOffset
                SqlDAL.ExecuteNonQuery(connString, "usp_ProTime", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function DeleteTimeDetails() As Boolean
            Try
                Dim arParms() As SqlParameter = New SqlParameter(14) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString



                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 2


                arParms(1) = New SqlParameter("@numProId", SqlDbType.BigInt)
                arParms(1).Value = _ProID

                arParms(2) = New SqlParameter("@numProStageID", SqlDbType.BigInt)
                arParms(2).Value = _ProStageID

                arParms(3) = New SqlParameter("@numRate", SqlDbType.Decimal)
                arParms(3).Value = _Rate

                'arParms(4) = New SqlParameter("@numHours", SqlDbType.BigInt)
                'arParms(4).Value = _Hour


                'arParms(5) = New SqlParameter("@numMins", SqlDbType.BigInt)
                'arParms(5).Value = _Min

                arParms(6) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(6).Value = _Desc

                arParms(7) = New SqlParameter("@numItemID", SqlDbType.BigInt, 9)
                arParms(7).Value = _ItemID

                arParms(8) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(8).Value = _Billable

                arParms(9) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(9).Value = _ContractID

                arParms(10) = New SqlParameter("@dtFromDate", SqlDbType.DateTime)
                arParms(10).Value = _FromDate

                arParms(11) = New SqlParameter("@dtToDate", SqlDbType.DateTime)
                arParms(11).Value = _ToDate

                arParms(12) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(12).Value = _ContactID

                arParms(13) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(13).Value = _OppID
                arParms(14) = New SqlParameter("@ClientTimeZoneOffset", SqlDbType.Int)
                arParms(14).Value = _ClientTimeZoneOffset
                SqlDAL.ExecuteNonQuery(connString, "usp_ProTime", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetExpenseDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As SqlParameter = New SqlParameter(9) {}


                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 1


                arParms(1) = New SqlParameter("@numProId", SqlDbType.BigInt)
                arParms(1).Value = _ProID

                arParms(2) = New SqlParameter("@numProStageID", SqlDbType.BigInt)
                arParms(2).Value = _ProStageID

                arParms(3) = New SqlParameter("@monAmount", SqlDbType.BigInt)
                arParms(3).Value = _Amount

                arParms(4) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(4).Value = _Desc

                arParms(5) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(5).Value = _Billable

                arParms(6) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ContractID

                arParms(7) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(7).Value = _ContactID

                arParms(8) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(8).Value = 0

                arParms(9) = New SqlParameter("@numDomainId", SqlDbType.BigInt, 9)
                arParms(9).Value = _DomainId
                ds = SqlDAL.ExecuteDataset(ConnString, "usp_ProExpense", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function SaveExpenseDetails() As Boolean
            Try
                Dim arParms() As SqlParameter = New SqlParameter(8) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 0


                arParms(1) = New SqlParameter("@numProId", SqlDbType.BigInt)
                arParms(1).Value = _ProID

                arParms(2) = New SqlParameter("@numProStageID", SqlDbType.BigInt)
                arParms(2).Value = _ProStageID

                arParms(3) = New SqlParameter("@monAmount", SqlDbType.BigInt)
                arParms(3).Value = _Amount

                arParms(4) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(4).Value = _Desc

                arParms(5) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(5).Value = _Billable

                arParms(6) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ContractID

                arParms(7) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(7).Value = _ContactID

                arParms(8) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(8).Value = _OppID

                SqlDAL.ExecuteNonQuery(ConnString, "usp_ProExpense", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function DeleteExpenseDetails() As Boolean
            Try
                Dim arParms() As SqlParameter = New SqlParameter(9) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 2


                arParms(1) = New SqlParameter("@numProId", SqlDbType.BigInt)
                arParms(1).Value = _ProID

                arParms(2) = New SqlParameter("@numProStageID", SqlDbType.BigInt)
                arParms(2).Value = _ProStageID

                arParms(3) = New SqlParameter("@monAmount", SqlDbType.BigInt)
                arParms(3).Value = _Amount

                arParms(4) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(4).Value = _Desc

                arParms(5) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(5).Value = _Billable

                arParms(6) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ContractID

                arParms(7) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(7).Value = _ContactID

                arParms(8) = New SqlParameter("@numOppId", SqlDbType.BigInt, 9)
                arParms(8).Value = 0

                arParms(9) = New SqlParameter("@numDomainId", SqlDbType.BigInt, 9)
                arParms(9).Value = _DomainId


                SqlDAL.ExecuteNonQuery(connString, "usp_ProExpense", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetProjectOpp() As DataTable
            Try
                Dim arParms() As SqlParameter = New SqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 0

                arParms(1) = New SqlParameter("@numProId", SqlDbType.BigInt)
                arParms(1).Value = _ProID

                arParms(2) = New SqlParameter("@numCaseId", SqlDbType.BigInt)
                arParms(2).Value = 0

                ds = SqlDAL.ExecuteDataset(connString, "usp_ProCaseOppLinked", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class

End Namespace