'Created Anoop Jayaraj
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Projects

    Public Class ProjectDependency
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _ProcessID As Long
        Private _MileStoneID As Long
        Private _strDependency As String
        Private _ProID As Long
        Private _ProStageId As Long

        Public Property ProStageId() As Long
            Get
                Return _ProStageId
            End Get
            Set(ByVal Value As Long)
                _ProStageId = Value
            End Set
        End Property

        Public Property ProID() As Long
            Get
                Return _ProID
            End Get
            Set(ByVal Value As Long)
                _ProID = Value
            End Set
        End Property


        Public Property ProcessID() As Long
            Get
                Return _ProcessID
            End Get
            Set(ByVal Value As Long)
                _ProcessID = Value
            End Set
        End Property

        Public Property MileStoneID() As Long
            Get
                Return _MileStoneID
            End Get
            Set(ByVal Value As Long)
                _MileStoneID = Value
            End Set
        End Property

        Public Property strDependency() As String
            Get
                Return _strDependency
            End Get
            Set(ByVal Value As String)
                _strDependency = Value
            End Set
        End Property


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region



        Public Function SaveDependency() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 0

                arParms(1) = New Npgsql.NpgsqlParameter("@strDependency", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strDependency

                arParms(2) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ProID

                arParms(3) = New Npgsql.NpgsqlParameter("@numProStageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProStageId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(ConnString, "USP_ProDependManage", arParms)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetDependencyDtls() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 1

                arParms(1) = New Npgsql.NpgsqlParameter("@strDependency", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strDependency

                arParms(2) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ProID

                arParms(3) = New Npgsql.NpgsqlParameter("@numProStageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProStageId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(ConnString, "USP_ProDependManage", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
    End Class

End Namespace
