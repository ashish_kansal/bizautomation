﻿'Created Anoop Jayaraj
Option Explicit On
Option Strict On

Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Projects
    Public Class ProjectIP
        Inherits Project

        Private _DocCount As Integer

        Public Property DocCount() As Integer
            Get
                Return _DocCount
            End Get
            Set(ByVal Value As Integer)
                _DocCount = Value
            End Set
        End Property

        Private _LinkedOrderCount As Integer
        Public Property LinkedOrderCount() As Integer
            Get
                Return _LinkedOrderCount
            End Get
            Set(ByVal value As Integer)
                _LinkedOrderCount = value
            End Set
        End Property


        Private _TimeAndMaterial As String
        Public Property TimeAndMaterial() As String
            Get
                Return _TimeAndMaterial
            End Get
            Set(ByVal value As String)
                _TimeAndMaterial = value
            End Set
        End Property


        Private _AssignedToName As String
        Private _IntPrjMgrName As String
        Private _CusPrjMgrName As String
        Private _MilestoneProgress As String
        Private _TaskContractId As Long
        Private _numSecounds As Long
        Private _balanceContractTime As Long
        Public Property balanceContractTime() As Long
            Get
                Return _balanceContractTime
            End Get
            Set(value As Long)
                _balanceContractTime = value
            End Set
        End Property
        Public Property TaskContractId() As Long
            Get
                Return _TaskContractId
            End Get
            Set(ByVal Value As Long)
                _TaskContractId = Value
            End Set
        End Property
        Public Property numSecounds() As Long
            Get
                Return _numSecounds
            End Get
            Set(ByVal Value As Long)
                _numSecounds = Value
            End Set
        End Property

        Private Property _isProjectActivityEmail As Int16
        Public Property isProjectActivityEmail() As Int16
            Get
                Return _isProjectActivityEmail
            End Get
            Set(value As Int16)
                _isProjectActivityEmail = value
            End Set
        End Property

        Public Property CusPrjMgrName() As String
            Get
                Return _CusPrjMgrName
            End Get
            Set(ByVal Value As String)
                _CusPrjMgrName = Value
            End Set
        End Property

        Public Property IntPrjMgrName() As String
            Get
                Return _IntPrjMgrName
            End Get
            Set(ByVal Value As String)
                _IntPrjMgrName = Value
            End Set
        End Property

        Public Property AssignedToName() As String
            Get
                Return _AssignedToName
            End Get
            Set(ByVal Value As String)
                _AssignedToName = Value
            End Set
        End Property

        Public Property MilestoneProgress() As String
            Get
                Return _MilestoneProgress
            End Get
            Set(ByVal Value As String)
                _MilestoneProgress = Value
            End Set
        End Property

        Private _ContractIDName As String
        Public Property ContractIDName() As String
            Get
                Return _ContractIDName
            End Get
            Set(ByVal value As String)
                _ContractIDName = value
            End Set
        End Property

        Private _ProjectTypeName As String
        Public Property ProjectTypeName() As String
            Get
                Return _ProjectTypeName
            End Get
            Set(ByVal value As String)
                _ProjectTypeName = value
            End Set
        End Property
        Private _ProjectedFinish As String
        Public Property ProjectedFinish() As String
            Get
                Return _ProjectedFinish
            End Get
            Set(ByVal value As String)
                _ProjectedFinish = value
            End Set
        End Property
        Private _ActualStartDate As Date
        Public Property ActualStartDate() As Date
            Get
                Return _ActualStartDate
            End Get
            Set(ByVal value As Date)
                _ActualStartDate = value
            End Set
        End Property

        Private _ShippingAddress As String
        Public Property ShippingAddress() As String
            Get
                Return _ShippingAddress
            End Get
            Set(ByVal value As String)
                _ShippingAddress = value
            End Set
        End Property
        Public Property MilestoneName As String
        Public Property StageDetailsID As Int64
        Public Property numClientBudget As Decimal
        Public Property ClientBudget As String
        Public Property ClientBudgetBalance As String
        Public Property MilestoneID As Long

        Public Function GetProjectDetails(ByVal DomainID As Long, ByVal ProjectID As Long, ByVal ClientZoneOffset As Integer) As Boolean
            Dim objIdataReader As IDataReader
            Me.DomainID = DomainID
            Me.ProjectID = ProjectID
            Me.ClientTimeZoneOffset = ClientZoneOffset
            objIdataReader = CType(ProjectDetail(Of IDataReader)(), IDataReader)

            Me.ProjectName = objIdataReader.Item("vcProjectName").ToString
            Me.AssignedTo = CType(objIdataReader.Item("numAssignedby"), Long)
            Me.IntPrjMgr = CType(objIdataReader.Item("numintPrjMgr"), Long)
            Me.CusPrjMgr = CType(objIdataReader.Item("numCustPrjMgr"), Long)
            Me.ProComments = objIdataReader.Item("txtComments").ToString
            Me.DueDate = CType(objIdataReader.Item("intDueDate"), Date)
            _DocCount = CType(objIdataReader.Item("DocumentCount"), Integer)
            _LinkedOrderCount = CType(objIdataReader.Item("LinkedOrderCount"), Integer)

        End Function


        Public Function ProjectDetailIP() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = Me.ProjectID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = Me.DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = Me.ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_ProjectsDTLPL", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If Not IsDBNull(dr("vcProjectName")) Then
                            Me.ProjectName = dr("vcProjectName").ToString
                        End If
                        If Not IsDBNull(dr("vcProjectID")) Then
                            Me.strProjectID = dr("vcProjectID").ToString
                        End If
                        If Not IsDBNull(dr("numAssignedTo")) Then
                            Me.AssignedTo = CType(dr("numAssignedTo"), Long)
                        End If
                        If Not IsDBNull(dr("numintPrjMgr")) Then
                            Me.IntPrjMgr = CType(dr("numintPrjMgr"), Long)
                        End If
                        If Not IsDBNull(dr("numCustPrjMgr")) Then
                            Me.CusPrjMgr = CType(dr("numCustPrjMgr"), Long)
                        End If
                        If Not IsDBNull(dr("intDueDate")) Then
                            Me.DueDate = CType(dr("intDueDate"), Date)
                        Else
                            Me.DueDate = Nothing
                        End If
                        If Not IsDBNull(dr("txtComments")) Then
                            Me.ProComments = dr("txtComments").ToString
                        End If
                        If Not IsDBNull(dr("DocumentCount")) Then
                            _DocCount = CType(dr("DocumentCount"), Integer)
                        End If
                        If Not IsDBNull(dr("numAssignedToName")) Then
                            _AssignedToName = dr("numAssignedToName").ToString
                        End If
                        If Not IsDBNull(dr("numintPrjMgrName")) Then
                            _IntPrjMgrName = dr("numintPrjMgrName").ToString
                        End If
                        If Not IsDBNull(dr("numCustPrjMgrName")) Then
                            _CusPrjMgrName = dr("numCustPrjMgrName").ToString
                        End If
                        If Not IsDBNull(dr("TProgress")) Then
                            _MilestoneProgress = dr("TProgress").ToString
                        End If
                        If Not IsDBNull(dr("LinkedOrderCount")) Then
                            _LinkedOrderCount = CType(dr("LinkedOrderCount"), Integer)
                        End If
                        If Not IsDBNull(dr("numContractId")) Then
                            Me.ContractID = CType(dr("numContractId"), Long)
                        End If
                        If Not IsDBNull(dr("numContractIdName")) Then
                            _ContractIDName = CType(dr("numContractIdName"), String)
                        End If
                        If Not IsDBNull(dr("numProjectType")) Then
                            Me.ProjectType = CType(dr("numProjectType"), Long)
                        End If
                        If Not IsDBNull(dr("vcProjectTypeName")) Then
                            Me.ProjectTypeName = dr("vcProjectTypeName").ToString
                        End If
                        If Not IsDBNull(dr("numProjectStatus")) Then
                            Me.ProjectStatus = CType(dr("numProjectStatus"), Long)
                        End If
                        If Not IsDBNull(dr("vcProjectStatusName")) Then
                            Me.ProjectStatusName = dr("vcProjectStatusName").ToString
                        End If
                        If Not IsDBNull(dr("numShareWith")) Then
                            Me.ShareRecordWith = CType(dr("numShareWith"), String)
                        End If
                        If Not IsDBNull(dr("ShippingAddress")) Then
                            Me.ShippingAddress = CType(dr("ShippingAddress"), String)
                        End If
                        If Not IsDBNull(dr("dtmStartDate")) Then
                            Me.PlannedStartDate = CType(dr("dtmStartDate"), Date)
                        Else
                            Me.PlannedStartDate = Nothing
                        End If
                        If Not IsDBNull(dr("dtmEndDate")) Then
                            Me.RequestedFinish = CType(dr("dtmEndDate"), Date)
                        Else
                            Me.RequestedFinish = Nothing
                        End If
                        If Not IsDBNull(dr("dtActualStartDate")) Then
                            Me.ActualStartDate = CType(dr("dtActualStartDate"), Date)
                        Else
                            Me.ActualStartDate = Nothing
                        End If
                        If Not IsDBNull(dr("vcProjectedFinish")) Then
                            Me.ProjectedFinish = CType(dr("vcProjectedFinish"), String)
                        End If
                        If Not IsDBNull(dr("numClientBudget")) Then
                            Me.numClientBudget = CType(dr("numClientBudget"), Decimal)
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetMilestones() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Project_GetMilestones", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetStages() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numMileStoneID", MilestoneID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcMilestoneName", MilestoneName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Project_GetStages", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetTasks(ByVal mode As Short) As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numMileStoneID", MilestoneID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numStageDetailsId", StageDetailsID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintMode", mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Project_GetTasks", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetProjectDetailsByTaskId(ByVal TaskIdId As Long) As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTaskId", TaskIdId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_Project_GetProjectDetailsByTaskId", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetEmployeesWithCapacityLoad(ByVal ProjectId As Long, ByVal dateRange As Short, ByVal fromDate As DateTime) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintDateRange", dateRange, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", fromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetEmployeesWithCapacityLoadForProject", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetTeamsWithCapacityLoad(ByVal ProjectId As Long, ByVal dateRange As Short, ByVal fromDate As DateTime) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintDateRange", dateRange, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", fromDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetTeamsWithCapacityLoadForProject", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetProjectSummaryDetails() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetProjectSummary", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsTaskPending() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_Project_IsTaskPending", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function UpdateDisplayTimeToExternalUsers() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitDisplayTimeToExternalUsers", bitDisplayTimeToExternalUsers, NpgsqlTypes.NpgsqlDbType.Bit))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_ProjectsUpdateAccessTimeByExternalUser", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function IsTaskStarted() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_Project_IsTaskStarted", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsCompleted() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_Project_IsCompleted", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SaveClientBudget() As Integer
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numClientBudget", numClientBudget, NpgsqlTypes.NpgsqlDbType.Numeric))
                End With

                Return SqlDAL.ExecuteNonQuery(connString, "usp_SaveProjectClientBudget", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SaveProjectID() As Integer
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcProjectID", ProjectName, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                Return SqlDAL.ExecuteNonQuery(connString, "usp_SaveProjectID", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function AddRemoveTimeFromContract() As Integer
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisonId", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSecounds", numSecounds, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@intType", strType, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numTaskID", TaskContractId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@outPut", ProjectID, NpgsqlTypes.NpgsqlDbType.BigInt, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@balanceContractTime", balanceContractTime, NpgsqlTypes.NpgsqlDbType.BigInt, ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@isProjectActivityEmail", isProjectActivityEmail, NpgsqlTypes.NpgsqlDbType.Integer))
                End With
                Dim objParam() As Object = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "usp_UpdateTimeContract", objParam, True)
                _balanceContractTime = CCommon.ToInteger(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)
                Return CCommon.ToInteger(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)
            Catch ex As Exception
                Return -1
                Throw
            End Try
        End Function

        Public Function UpdateProjectStatus() As Integer
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ProjectStatus", ProjectStatus, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numContactId", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                Return SqlDAL.ExecuteNonQuery(connString, "usp_SaveProjectStatus", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
End Namespace
