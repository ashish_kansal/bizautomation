'Created Anoop Jayaraj
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Reflection
Namespace BACRM.BusinessLogic.Projects
    Public Class Project
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _SortOrder As Short
        'Private DomainId As Long
        Private _UserRightType As Long
        'Private UserCntID As Long = 1
        Private _ListName As String
        Private _bitDeleted As Short
        Private _DivisionID As Long
        Private _TerritoryID As Long
        Private _ProType As Short
        Private _ContactID As Long
        Private _ContractID As Long
        Private _ProjectProcessId As Long
        Private _ContactType As Long
        Private _OpportunityId As Long
        Private _ProjectName As String
        Private _EstimatedCloseDate As Long
        Private _ProComments As String
        Private _PublicFlag As Short
        Private _strMilestone As String = ""
        Private _strSubStage As String = ""
        Private _bytemode As Short
        Private _StageID As Long
        Private _SubStageID As Long
        Private _ProjectID As Long = 0
        Private _IntPrjMgr As Long
        Private _CusPrjMgr As Long
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _CustName As String
        Private _FirstName As String
        Private _LastName As String
        Private _DueDate As Date = New Date(1753, 1, 1)
        Private _AssignedTo As Long
        Private _bitPartner As Boolean
        Private _strType As String
        Private _strOppSel As String
        Private _ContactRole As Long
        Private _ClientTimeZoneOffset As Integer

        Private _bitSubscribedEmailAlert As Boolean
        Public Property bitSubscribedEmailAlert() As Boolean
            Get
                Return _bitSubscribedEmailAlert
            End Get
            Set(ByVal Value As Boolean)
                _bitSubscribedEmailAlert = Value
            End Set
        End Property
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _OppBizDocID As Long
        Public Property OppBizDocID() As Long
            Get
                Return _OppBizDocID
            End Get
            Set(ByVal value As Long)
                _OppBizDocID = value
            End Set
        End Property

        Private _Mode As Short
        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal value As Short)
                _Mode = value
            End Set
        End Property


        Private _AccountID As Long
        Public Property AccountID() As Long
            Get
                Return _AccountID
            End Get
            Set(ByVal value As Long)
                _AccountID = value
            End Set
        End Property


        Private _BillID As Long
        Public Property BillID() As Long
            Get
                Return _BillID
            End Get
            Set(ByVal value As Long)
                _BillID = value
            End Set
        End Property

        Public Property ContactRole() As Long
            Get
                Return _ContactRole
            End Get
            Set(ByVal Value As Long)
                _ContactRole = Value
            End Set
        End Property

        Public Property strOppSel() As String
            Get
                Return _strOppSel
            End Get
            Set(ByVal Value As String)
                _strOppSel = Value
            End Set
        End Property
        Public Property bitPartner() As Boolean
            Get
                Return _bitPartner
            End Get
            Set(ByVal Value As Boolean)
                _bitPartner = Value
            End Set
        End Property


        Public Property AssignedTo() As Long
            Get
                Return _AssignedTo
            End Get
            Set(ByVal Value As Long)
                _AssignedTo = Value
            End Set
        End Property

        Public Property DueDate() As Date
            Get
                Return _DueDate
            End Get
            Set(ByVal Value As Date)
                _DueDate = Value
            End Set
        End Property

        Public Property CustName() As String
            Get
                Return _CustName
            End Get
            Set(ByVal Value As String)
                _CustName = Value
            End Set
        End Property

        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property
        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property


        Public Property ProjectName() As String
            Get
                Return _ProjectName
            End Get
            Set(ByVal Value As String)
                _ProjectName = Value
            End Set
        End Property

        Public Property CusPrjMgr() As Long
            Get
                Return _CusPrjMgr
            End Get
            Set(ByVal Value As Long)
                _CusPrjMgr = Value
            End Set
        End Property

        Public Property IntPrjMgr() As Long
            Get
                Return _IntPrjMgr
            End Get
            Set(ByVal Value As Long)
                _IntPrjMgr = Value
            End Set
        End Property

        Public Property ProjectID() As Long
            Get
                Return _ProjectID
            End Get
            Set(ByVal Value As Long)
                _ProjectID = Value
            End Set
        End Property
        Private _bitDisplayTimeToExternalUsers As Boolean
        Public Property bitDisplayTimeToExternalUsers() As Boolean
            Get
                Return _bitDisplayTimeToExternalUsers
            End Get
            Set(value As Boolean)
                _bitDisplayTimeToExternalUsers = value
            End Set
        End Property

        Public Property SubStageID() As Long
            Get
                Return _SubStageID
            End Get
            Set(ByVal Value As Long)
                _SubStageID = Value
            End Set
        End Property

        Public Property StageID() As Long
            Get
                Return _StageID
            End Get
            Set(ByVal Value As Long)
                _StageID = Value
            End Set
        End Property

        Public Property strSubStage() As String
            Get
                Return _strSubStage
            End Get
            Set(ByVal Value As String)
                _strSubStage = Value
            End Set
        End Property


        Public Property bytemode() As Short
            Get
                Return _bytemode
            End Get
            Set(ByVal Value As Short)
                _bytemode = Value
            End Set
        End Property




        Public Property strMilestone() As String
            Get
                Return _strMilestone
            End Get
            Set(ByVal Value As String)
                _strMilestone = Value
            End Set
        End Property


        Public Property SortOrder() As Short
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Short)
                _SortOrder = Value
            End Set
        End Property



        Public Property PublicFlag() As Short
            Get
                Return _PublicFlag
            End Get
            Set(ByVal Value As Short)
                _PublicFlag = Value
            End Set
        End Property

        Public Property ProComments() As String
            Get
                Return _ProComments
            End Get
            Set(ByVal Value As String)
                _ProComments = Value
            End Set
        End Property

        Public Property OpportunityId() As Long
            Get
                Return _OpportunityId
            End Get
            Set(ByVal Value As Long)
                _OpportunityId = Value
            End Set
        End Property

        Public Property ContactType() As Long
            Get
                Return _ContactType
            End Get
            Set(ByVal Value As Long)
                _ContactType = Value
            End Set
        End Property


        Public Property ProjectProcessId() As Long
            Get
                Return _ProjectProcessId
            End Get
            Set(ByVal Value As Long)
                _ProjectProcessId = Value
            End Set
        End Property

        Public Property ContractID() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property ProType() As Short
            Get
                Return _ProType
            End Get
            Set(ByVal Value As Short)
                _ProType = Value
            End Set
        End Property

        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property bitDeleted() As Short
            Get
                Return _bitDeleted
            End Get
            Set(ByVal Value As Short)
                _bitDeleted = Value
            End Set
        End Property

        Public Property ListName() As String
            Get
                Return _ListName
            End Get
            Set(ByVal Value As String)
                _ListName = Value
            End Set
        End Property


        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property UserRightType() As Long
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Long)
                _UserRightType = Value
            End Set
        End Property

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property strType() As String
            Get
                Return _strType
            End Get
            Set(ByVal Value As String)
                _strType = Value
            End Set
        End Property

        Private _ProjOppID As Long
        Public Property ProjOppID() As Long
            Get
                Return _ProjOppID
            End Get
            Set(ByVal value As Long)
                _ProjOppID = value
            End Set
        End Property

        Private _strProjectID As String
        Public Property strProjectID() As String
            Get
                Return _strProjectID
            End Get
            Set(ByVal value As String)
                _strProjectID = value
            End Set
        End Property

        Private _ContactList As String
        Public Property ContactList() As String
            Get
                Return _ContactList
            End Get
            Set(ByVal Value As String)
                _ContactList = Value
            End Set
        End Property
        Private _ProjectTeamId As Long
        Public Property ProjectTeamId() As Long
            Get
                Return _ProjectTeamId
            End Get
            Set(ByVal value As Long)
                _ProjectTeamId = value
            End Set
        End Property

        Private _ProjectType As Long
        Public Property ProjectType() As Long
            Get
                Return _ProjectType
            End Get
            Set(ByVal value As Long)
                _ProjectType = value
            End Set
        End Property

        Private _OppType As Integer
        Public Property OppType() As Integer
            Get
                Return _OppType
            End Get
            Set(ByVal Value As Integer)
                _OppType = Value
            End Set
        End Property

        Private _ProjectStatus As Long
        Public Property ProjectStatus() As Long
            Get
                Return _ProjectStatus
            End Get
            Set(ByVal value As Long)
                _ProjectStatus = value
            End Set
        End Property

        Private _ProjectStatusName As String
        Public Property ProjectStatusName() As String
            Get
                Return _ProjectStatusName
            End Get
            Set(ByVal value As String)
                _ProjectStatusName = value
            End Set
        End Property

        Private _RegularSearchCriteria As String
        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property

        Private _CustomSearchCriteria As String
        Public Property CustomSearchCriteria() As String
            Get
                Return _CustomSearchCriteria
            End Get
            Set(ByVal Value As String)
                _CustomSearchCriteria = Value
            End Set
        End Property

        Private _ShareRecordWith As String
        Public Property ShareRecordWith() As String
            Get
                Return _ShareRecordWith
            End Get
            Set(ByVal Value As String)
                _ShareRecordWith = Value
            End Set
        End Property
        Private _SearchText As String
        Public Property SearchText() As String
            Get
                Return _SearchText
            End Get
            Set(ByVal Value As String)
                _SearchText = Value
            End Set
        End Property

        Private _sMode As Short
        Public Property sMode() As Short
            Get
                Return _sMode
            End Get
            Set(ByVal value As Short)
                _sMode = value
            End Set
        End Property
        Public Property DashboardReminderType As Short
        Public Property PlannedStartDate As DateTime
        Public Property RequestedFinish As DateTime


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region


        Public Function GetProjectList() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(17) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRightType

                arParms(3) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(3).Value = _SortCharacter

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _TotalRecords

                arParms(7) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(7).Value = _columnName

                arParms(8) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar, 10)
                arParms(8).Value = _columnSortOrder

                arParms(9) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(9).Value = _DivisionID

                arParms(10) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _bitPartner

                arParms(11) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Value = _ClientTimeZoneOffset

                arParms(12) = New Npgsql.NpgsqlParameter("@numProjectStatus", NpgsqlTypes.NpgsqlDbType.Bigint, 9)
                arParms(12).Value = _ProjectStatus

                arParms(13) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(13).Value = _RegularSearchCriteria

                arParms(14) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.Varchar, 1000)
                arParms(14).Value = _CustomSearchCriteria

                arParms(15) = New Npgsql.NpgsqlParameter("@tintDashboardReminderType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(15).Value = DashboardReminderType

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                arParms(17) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(17).Value = Nothing
                arParms(17).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_ProjectList1", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetProjectListPortal(Optional ByVal bitOpenProjects As Boolean = False) As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(19) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRightType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _SortOrder

                arParms(4) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(4).Value = _SortCharacter

                arParms(5) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Value = IIf(_FirstName = "", "", Replace(_FirstName, "'", "''"))

                arParms(6) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(6).Value = IIf(_LastName = "", "", Replace(_LastName, "'", "''"))

                arParms(7) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(8) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _CurrentPage

                arParms(9) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _PageSize

                arParms(10) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Direction = ParameterDirection.InputOutput
                arParms(10).Value = _TotalRecords

                arParms(11) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(11).Value = _columnName

                arParms(12) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(12).Value = _columnSortOrder

                arParms(13) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(13).Value = _DivisionID

                arParms(14) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _bitPartner

                arParms(15) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(15).Value = _ClientTimeZoneOffset

                arParms(16) = New Npgsql.NpgsqlParameter("@numProjectStatus", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(16).Value = _ProjectStatus

                arParms(17) = New Npgsql.NpgsqlParameter("@bitOpenProjects", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(17).Value = bitOpenProjects

                arParms(18) = New Npgsql.NpgsqlParameter("@intMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(18).Value = _sMode

                arParms(19) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(19).Value = Nothing
                arParms(19).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ProjectList", arParms)

                Dim dtTable As DataTable
                dtTable = ds.Tables(0)
                If dtTable.Rows.Count > 0 AndAlso _sMode = 0 Then
                    _TotalRecords = Common.CCommon.ToInteger(dtTable.Rows(0).Item("TotalRowCount"))
                End If

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportunities() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _bytemode

                arParms(1) = New Npgsql.NpgsqlParameter("@domainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID


                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProjectID

                arParms(4) = New Npgsql.NpgsqlParameter("@numCaseId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = 0

                arParms(5) = New Npgsql.NpgsqlParameter("@numStageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _StageID

                arParms(6) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _OppType

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_ProjectOpp", arParms)

                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function



        Public Function Save() As String()

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _ProjectID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numIntPrjMgr", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _IntPrjMgr

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _OpportunityId

                arParms(4) = New Npgsql.NpgsqlParameter("@numCustPrjMgr", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _CusPrjMgr

                arParms(5) = New Npgsql.NpgsqlParameter("@vcPProName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _strProjectID

                arParms(6) = New Npgsql.NpgsqlParameter("@Comments", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(6).Value = _ProComments

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@numAssignedTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _AssignedTo

                arParms(9) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = DomainId

                arParms(10) = New Npgsql.NpgsqlParameter("@dtDueDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(10).Value = _DueDate

                arParms(11) = New Npgsql.NpgsqlParameter("@numContractID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _ContractID

                arParms(12) = New Npgsql.NpgsqlParameter("@vcProjectName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(12).Value = _ProjectName

                arParms(13) = New Npgsql.NpgsqlParameter("@numProjectType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _ProjectType

                arParms(14) = New Npgsql.NpgsqlParameter("@numProjectStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _ProjectStatus

                arParms(15) = New Npgsql.NpgsqlParameter("@dtmStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(15).Value = PlannedStartDate

                arParms(16) = New Npgsql.NpgsqlParameter("@dtmEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(16).Value = RequestedFinish

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ProjectManage", objParam, True)

                Dim arrOutPut() As String = New String(2) {}
                arrOutPut(0) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                arrOutPut(1) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)
                Return arrOutPut

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ProjectDetail(Of T)() As Object
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim pInfo As PropertyInfo() = GetType(T).GetProperties
                If pInfo(0).ReflectedType.Name = "DataTable" Then
                    Return SqlDAL.ExecMethod(Of DataTable)(connString, "USP_Projects", arParms)
                ElseIf pInfo(0).ReflectedType.Name = "IDataReader" Then
                    Return SqlDAL.ExecMethod(Of IDataReader)(connString, "USP_Projects", arParms)
                End If

            Catch ex As Exception

                Throw ex
            End Try
        End Function



        Public Function AssCntsByProId() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ProContacts", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SalesProcessDtlByProId() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection

                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@ProcessID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectProcessId

                arParms(1) = New Npgsql.NpgsqlParameter("@ContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@DomianID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProjectID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ProMilestone", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function SalesProcessDtlByProcessId() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@ProcessID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectProcessId

                arParms(1) = New Npgsql.NpgsqlParameter("@ContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@DomianID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ProjectID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ProMilestone", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function DeleteProjects() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId


                SqlDAL.ExecuteNonQuery(connString, "Usp_DeleteProject", arParms)
                Return True
            Catch ex As Exception
                Return False
                Throw ex
            End Try
        End Function
        Sub SaveProjectOpportunities()
            Try
                Dim getconnection As New GetConnection

                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OpportunityId

                arParms(3) = New Npgsql.NpgsqlParameter("@numBillID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _BillID

                arParms(4) = New Npgsql.NpgsqlParameter("@numStageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _StageID

                SqlDAL.ExecuteNonQuery(connString, "Usp_SaveProjectOpportunities", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function DeleteProjectsOpp() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProjOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjOppID

                SqlDAL.ExecuteNonQuery(connString, "Usp_DeleteProjectBizDoc", arParms)

                Return True
            Catch ex As Exception
                Return False
                Throw ex
            End Try
        End Function
        Public Function GetProOpportunities() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numProjectId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetProjectOpportunities", arParms)

                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function


        Public Function AddProContacts() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@numRole", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ContactRole

                arParms(4) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _bitPartner

                arParms(5) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _bytemode

                arParms(6) = New Npgsql.NpgsqlParameter("@bitSubscribedEmailAlert", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _bitSubscribedEmailAlert

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_ManageProAssociatedContact", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetOpenProject() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOpenProjects", arParms)

                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function
        Public Function UpdateProjectAccount() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@numAccountID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _AccountID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateProjectAccount", arParms)

                Return True
            Catch ex As Exception
                Return False
                Throw ex
            End Try
        End Function
        Public Function GetProjectIncomeExpense() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@Mode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur4", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetProjectIncomeExpense", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetProjectTeamContact() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numProjectTeamId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectTeamId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetProjectTeamContact", arParms)

                Return ds.Tables(0)
            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function ManageProjectTeamContact() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numProjectTeamId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ProjectTeamId

                arParms(1) = New Npgsql.NpgsqlParameter("@strContactId", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _ContactList

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageProjectTeamContact", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetProjectTeamRights() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _bytemode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetProjectTeamRights", arParms).Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function ManageProjectTeamRights() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@strContactId", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _ContactList

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainId

                arParms(2) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ProjectID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintUserType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _bytemode

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _Mode

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageProjectTeamRights", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetProjectStageHierarchy() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _Mode

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetProjectStageHierarchy", arParms)

            Catch ex As Exception

                Throw ex
            End Try
        End Function
        Public Function GetProjectChildStage() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numStageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _StageID

                arParms(2) = New Npgsql.NpgsqlParameter("@strType", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = _strType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetProjectChildStage", arParms).Tables(0)

            Catch ex As Exception

                Throw ex
            End Try
        End Function

        Public Function GetProjectExpenseReports() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProjectID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetProjectExpenseReports", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFieldValue(ByVal fieldName As String) As Object
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcFieldName", fieldName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_ProjectsMaster_GetFieldValue", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub ChangeOrganization(ByVal newDivisionID As Long, ByVal newContactID As Long)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numNewDivisionID", newDivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numNewContactID", newContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_ProjectsMaster_ChangeOrganization", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetAssociatedContacts() As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProId", ProjectID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_ProjectsMaster_GetAssociatedContacts", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Function StageItemDetails(ByVal numDomainid As Long, ByVal slpid As Long, ByVal tintMode As Long) As DataTable
        '    Try
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numDomainid", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = numDomainid

        '        arParms(1) = New Npgsql.NpgsqlParameter("@slpid", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(1).Value = slpid

        '        arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(2).Value = tintMode

        '        Return SqlDAL.ExecuteDataset(connString, "usp_GetStageItemDetails", arParms).Tables(0)

        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        'Public Function UpdateStagePercentageDetails_User(ByVal lngDomainID As Long, ByVal numProjectID As Long, ByVal lngStageDetailsId As Long, ByVal vcStageName As String,
        '                                                  ByVal tintPercentage As Long, ByVal dtStartDate As DateTime, ByVal dtEndDate As DateTime, ByVal vcDescription As String, ByVal numModifiedBy As Long) As Boolean
        '    Try
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString

        '        arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(0).Value = lngDomainID

        '        arParms(1) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(1).Value = numProjectID

        '        arParms(2) = New Npgsql.NpgsqlParameter("@numStageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(2).Value = lngStageDetailsId

        '        arParms(3) = New Npgsql.NpgsqlParameter("@vcStageName", NpgsqlTypes.NpgsqlDbType.VarChar)
        '        arParms(3).Value = vcStageName

        '        arParms(4) = New Npgsql.NpgsqlParameter("@numModifiedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(4).Value = numModifiedBy

        '        arParms(5) = New Npgsql.NpgsqlParameter("@tintPercentage", NpgsqlTypes.NpgsqlDbType.Smallint)
        '        arParms(5).Value = tintPercentage

        '        arParms(6) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
        '        arParms(6).Value = dtStartDate

        '        arParms(7) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
        '        arParms(7).Value = dtEndDate

        '        arParms(8) = New Npgsql.NpgsqlParameter("@vcDescription", NpgsqlTypes.NpgsqlDbType.VarChar)
        '        arParms(8).Value = vcDescription

        '        SqlDAL.ExecuteNonQuery(connString, "usp_UpdateStagePercentageDetails_User", arParms)
        '        Return True

        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
    End Class
End Namespace