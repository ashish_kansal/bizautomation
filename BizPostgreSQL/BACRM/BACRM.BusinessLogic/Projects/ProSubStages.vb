'Created Anoop Jayaraj
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Projects

    Public Class ProSubStages
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _ProId As Long
        Private _ProStageID As Long
        Private _strSubStage As String
        Private _SubStageID As Long
        Private _SalesProcessId As Long
        Private _SubStageName As String

        Public Property SubStageName() As String
            Get
                Return _SubStageName
            End Get
            Set(ByVal Value As String)
                _SubStageName = Value
            End Set
        End Property

        Public Property SalesProcessId() As Long
            Get
                Return _SalesProcessId
            End Get
            Set(ByVal Value As Long)
                _SalesProcessId = Value
            End Set
        End Property

        Public Property SubStageID() As Long
            Get
                Return _SubStageID
            End Get
            Set(ByVal Value As Long)
                _SubStageID = Value
            End Set
        End Property

        Public Property ProStageID() As Long
            Get
                Return _ProStageID
            End Get
            Set(ByVal Value As Long)
                _ProStageID = Value
            End Set
        End Property

        Public Property ProId() As Long
            Get
                Return _ProId
            End Get
            Set(ByVal Value As Long)
                _ProId = Value
            End Set
        End Property

        Public Property strSubStages() As String
            Get
                Return _strSubStage
            End Get
            Set(ByVal Value As String)
                _strSubStage = Value
            End Set
        End Property

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Public Function GetSubStage() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 1

                arParms(1) = New Npgsql.NpgsqlParameter("@numProID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProId

                arParms(2) = New Npgsql.NpgsqlParameter("@stageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ProStageID

                arParms(3) = New Npgsql.NpgsqlParameter("@strSubStage", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _strSubStage

                arParms(4) = New Npgsql.NpgsqlParameter("@numSubStageHdrID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _SalesProcessId

                arParms(5) = New Npgsql.NpgsqlParameter("@numSubStageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _SubStageID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(ConnString, "USP_ProSaveSubStage", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


    End Class

End Namespace