﻿Public Class DemandForecastOrderItem
    Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Member Variables"

    Private _numItemCode As Long
    Private _vcItemType As String
    Private _vcItemName As String
    Private _numWarehouseItemID As Long
    Private _numUnits As Double
    Private _monPrice As Double
    Private _numUOMID As Long

#End Region

#Region "Public Property"

    Public Property ItemCode() As Long
        Get
            Return _numItemCode
        End Get
        Set(ByVal value As Long)
            _numItemCode = value
        End Set
    End Property

    Public Property ItemType() As String
        Get
            Return _vcItemType
        End Get
        Set(ByVal value As String)
            _vcItemType = value
        End Set
    End Property

    Public Property ItemName() As String
        Get
            Return _vcItemName
        End Get
        Set(ByVal value As String)
            _vcItemName = value
        End Set
    End Property

    Public Property WarehouseItemID() As Long
        Get
            Return _numWarehouseItemID
        End Get
        Set(ByVal value As Long)
            _numWarehouseItemID = value
        End Set
    End Property

    Public Property Quantity() As Double
        Get
            Return _numUnits
        End Get
        Set(ByVal value As Double)
            _numUnits = value
        End Set
    End Property

    Public Property Price() As Double
        Get
            Return _monPrice
        End Get
        Set(ByVal value As Double)
            _monPrice = value
        End Set
    End Property

    Public Property UOMID() As Long
        Get
            Return _numUOMID
        End Get
        Set(ByVal value As Long)
            _numUOMID = value
        End Set
    End Property

#End Region
End Class
