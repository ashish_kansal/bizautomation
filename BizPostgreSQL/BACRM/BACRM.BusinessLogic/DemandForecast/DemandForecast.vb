﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.BusinessLogic.DemandForecast
    Public Class DemandForecast
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Member Variables"

        Private _numDFID As Long
        Private _bitIncludeOpenReleaseDates As Boolean
        Private _listItemClassification As List(Of Long)
        Private _listItemGroup As List(Of Long)
        Private _listWarehouse As List(Of Long)
        Private _bitLastYear As Boolean
        Private _numAnalysisPattern As Integer
        Private _numForecastDays As Integer
        Private _numTotalRecords As Integer
        Private _numCurrentPage As Integer
        Private _numPageSize As Integer
        Private _vcSelectedIds As String
        Private _ClientTimeZoneOffset As Integer
        Private _dtFromDate As DateTime?
        Private _dtToDate As DateTime?
        Private _listDemandForecastOrder As List(Of DemandForecastOrder)
        Private _columnName As String
        Private _columnSortOrder As String

#End Region

#Region "Public Properties"

        Public Property numDFID() As Long
            Get
                Return _numDFID
            End Get
            Set(ByVal value As Long)
                _numDFID = value
            End Set
        End Property

        Public Property bitIncludeOpenReleaseDates() As Boolean
            Get
                Return _bitIncludeOpenReleaseDates
            End Get
            Set(ByVal value As Boolean)
                _bitIncludeOpenReleaseDates = value
            End Set
        End Property

        Public Property listItemClassification() As List(Of Long)
            Get
                Return _listItemClassification
            End Get
            Set(ByVal value As List(Of Long))
                _listItemClassification = value
            End Set
        End Property

        Public Property listItemGroup() As List(Of Long)
            Get
                Return _listItemGroup
            End Get
            Set(ByVal value As List(Of Long))
                _listItemGroup = value
            End Set
        End Property

        Public Property listWarehouse() As List(Of Long)
            Get
                Return _listWarehouse
            End Get
            Set(ByVal value As List(Of Long))
                _listWarehouse = value
            End Set
        End Property

        Public Property bitLastYear() As Boolean
            Get
                Return _bitLastYear
            End Get
            Set(ByVal value As Boolean)
                _bitLastYear = value
            End Set
        End Property

        Public Property numAnalysisPattern() As Integer
            Get
                Return _numAnalysisPattern
            End Get
            Set(ByVal value As Integer)
                _numAnalysisPattern = value
            End Set
        End Property

        Public Property numForecastDays() As Integer
            Get
                Return _numForecastDays
            End Get
            Set(ByVal value As Integer)
                _numForecastDays = value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _numTotalRecords
            End Get
            Set(ByVal value As Integer)
                _numTotalRecords = value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _numCurrentPage
            End Get
            Set(ByVal value As Integer)
                _numCurrentPage = value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _numPageSize
            End Get
            Set(ByVal value As Integer)
                _numPageSize = value
            End Set
        End Property

        Public Property SelectedIDs() As String
            Get
                Return _vcSelectedIds
            End Get
            Set(ByVal value As String)
                _vcSelectedIds = value
            End Set
        End Property

        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Public Property FromDate() As DateTime?
            Get
                Return _dtFromDate
            End Get
            Set(ByVal value As DateTime?)
                _dtFromDate = value
            End Set
        End Property

        Public Property ToDate() As DateTime?
            Get
                Return _dtToDate
            End Get
            Set(ByVal value As DateTime?)
                _dtToDate = value
            End Set
        End Property

        Public Property ListOrders() As List(Of DemandForecastOrder)
            Get
                Return _listDemandForecastOrder
            End Get
            Set(ByVal value As List(Of DemandForecastOrder))
                _listDemandForecastOrder = value
            End Set
        End Property

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Private _RegularSearchCriteria As String
        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property

        Public Property ShowAllItems As Boolean
        Public Property ShowHistoricalSales As Boolean
        Public Property AnalysisPattern As Long
        Public Property IsBasedOnLastYear As Boolean
        Public Property IsIncludeOpportunity As Boolean
        Public Property OpportunityPercentComplete As Long
        Public Property ReleaseDate As Date
        Public Property DemandPlanBasedOn As Short
        Public Property PlanType As Short
#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDFID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(0).Value = numDFID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@UserContactID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@bitIncludeOpenReleaseDates", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = bitIncludeOpenReleaseDates

                arParms(4) = New Npgsql.NpgsqlParameter("@vcItemClassification", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(4).Value = String.Join(",", _listItemClassification.ToArray())

                arParms(5) = New Npgsql.NpgsqlParameter("@vcItemGroup", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(5).Value = String.Join(",", _listItemGroup.ToArray())

                arParms(6) = New Npgsql.NpgsqlParameter("@vcWarehouse", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(6).Value = String.Join(",", _listWarehouse.ToArray())

                arParms(7) = New Npgsql.NpgsqlParameter("@bitLastYear", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = _bitLastYear

                arParms(8) = New Npgsql.NpgsqlParameter("@numAnalysisPattern", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(8).Value = _numAnalysisPattern

                arParms(9) = New Npgsql.NpgsqlParameter("@numForecastDays", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(9).Value = _numForecastDays

                SqlDAL.ExecuteNonQuery(connString, "USP_DemandForecast_Save", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcSelectedIDs", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(1).Value = SelectedIDs

                SqlDAL.ExecuteNonQuery(connString, "USP_DemandForecast_Delete", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function Execute() As DataSet
            Try
                Dim ds As DataSet
                Dim dt As New DataTable

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
     
                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDFID", numDFID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSelectedIDs", SelectedIDs, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", CurrentPage, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@PageSize", PageSize, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@columnName", _columnName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcRegularSearchCriteria", _RegularSearchCriteria, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@bitShowAllItems", ShowAllItems, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitShowHistoricSales", ShowHistoricalSales, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numHistoricalAnalysisPattern", AnalysisPattern, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitBasedOnLastYear", IsBasedOnLastYear, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitIncludeOpportunity", IsIncludeOpportunity, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numOpportunityPercentComplete", OpportunityPercentComplete, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintDemandPlanBasedOn", DemandPlanBasedOn, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintPlanType", PlanType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_DemandForecast_Execute", objParam, True)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    _numTotalRecords = CCommon.ToInteger(ds.Tables(0).Rows(0)("TotalRowsCount"))
                Else
                    _numTotalRecords = 0
                End If

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetByDomain() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numPageIndex", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = CurrentPage

                arParms(2) = New Npgsql.NpgsqlParameter("@numPageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = PageSize

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_DemandForecast_Get", arParms)


                Dim dtTable As DataTable
                dtTable = ds.Tables(0)

                If Not dtTable.Columns("TotalRowCount") Is Nothing Then
                    If dtTable.Rows.Count > 0 Then
                        _numTotalRecords = CInt(dtTable.Rows(0).Item("TotalRowCount"))
                    End If
                    dtTable.Columns.Remove("TotalRowCount")
                    ds.AcceptChanges()
                End If

                Return dtTable
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetByID() As DemandForecast
            Try
                Dim demandForecastEdit As New DemandForecast
                demandForecastEdit.numDFID = -1

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDFID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = numDFID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur4", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_DemandForecast_GetByID", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count = 4 Then
                    ' Table - DemandForecast
                    If ds.Tables(0).Rows.Count > 0 Then
                        demandForecastEdit.numDFID = CCommon.ToLong(ds.Tables(0).Rows(0)("numDFID"))
                        demandForecastEdit.numAnalysisPattern = CCommon.ToInteger(ds.Tables(0).Rows(0)("numDFAPID"))
                        demandForecastEdit.bitLastYear = CCommon.ToBool(ds.Tables(0).Rows(0)("bitLastYear"))
                        demandForecastEdit.numForecastDays = CCommon.ToInteger(ds.Tables(0).Rows(0)("numDFDaysID"))
                        demandForecastEdit.bitIncludeOpenReleaseDates = CCommon.ToBool(ds.Tables(0).Rows(0)("bitIncludeOpenReleaseDates"))
                    End If

                    ' Table - DemandForecastItemClassification
                    If ds.Tables(1).Rows.Count > 0 Then
                        listItemClassification = New List(Of Long)

                        For Each dr As DataRow In ds.Tables(1).Rows
                            listItemClassification.Add(CCommon.ToLong(dr("numItemClassificationID")))
                        Next

                        demandForecastEdit.listItemClassification = listItemClassification
                    End If

                    ' Table - DemandForecastItemGroup
                    If ds.Tables(2).Rows.Count > 0 Then
                        listItemGroup = New List(Of Long)

                        For Each dr As DataRow In ds.Tables(2).Rows
                            listItemGroup.Add(CCommon.ToLong(dr("numItemGroupID")))
                        Next

                        demandForecastEdit.listItemGroup = listItemGroup
                    End If

                    ' Table - DemandForecastWarehouse
                    If ds.Tables(3).Rows.Count > 0 Then
                        listWarehouse = New List(Of Long)

                        For Each dr As DataRow In ds.Tables(3).Rows
                            listWarehouse.Add(CCommon.ToLong(dr("numWarehouseID")))
                        Next

                        demandForecastEdit.listWarehouse = listWarehouse
                    End If
                End If

                Return demandForecastEdit
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetForecastDays() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_DemandForecastDays_GetAll", arParms).Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetAnalysisPattern() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_DemandForecastAnalysisPattern_GetAll", arParms).Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function CreateOrder() As Boolean
            Dim isErrorOccured As Boolean = False

            Try
                If Not ListOrders Is Nothing AndAlso ListOrders.Count() > 0 Then
                    For Each order As DemandForecastOrder In ListOrders
                        Try
                            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                Dim lngOppID As Long

                                'Insert Opportunity
                                Dim objOpportunity As New MOpportunity
                                objOpportunity.OppType = 2
                                objOpportunity.OpportunityId = 0
                                objOpportunity.OpportunityName = order.OrderName
                                objOpportunity.DivisionID = order.VendorID
                                objOpportunity.ContactID = order.ContactID
                                objOpportunity.UserCntID = UserCntID
                                objOpportunity.EstimatedCloseDate = Now
                                objOpportunity.DomainID = DomainID
                                objOpportunity.Active = 1
                                objOpportunity.SourceType = 1
                                objOpportunity.DealStatus = MOpportunity.EnmDealStatus.DealWon
                                objOpportunity.OppComments = order.Comment

                                Dim objAdmin As New CAdmin
                                Dim dtBillingTerms As DataTable
                                objAdmin.DivisionID = order.VendorID
                                dtBillingTerms = objAdmin.GetBillingTerms()

                                If Not dtBillingTerms Is Nothing AndAlso dtBillingTerms.Rows.Count > 0 Then
                                    objOpportunity.boolBillingTerms = If(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
                                    objOpportunity.BillingDays = CCommon.ToInteger(dtBillingTerms.Rows(0).Item("numBillingDays"))
                                    objOpportunity.boolInterestType = If(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
                                    objOpportunity.Interest = CCommon.ToDecimal(dtBillingTerms.Rows(0).Item("fltInterest"))
                                End If

                                lngOppID = CCommon.ToLong(objOpportunity.Save()(0))

                                objOpportunity.OpportunityId = lngOppID


                                ''Added By Sachin Sadhu||Date:29thApril12014
                                ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                                ''          Using Change tracking
                                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                                objWfA.DomainID = DomainID
                                objWfA.UserCntID = UserCntID
                                objWfA.RecordID = lngOppID
                                objWfA.SaveWFOrderQueue()
                                'end of code

                                'Insert Opportunity Items
                                For Each Item As DemandForecastOrderItem In order.OrderItems
                                    objOpportunity.ItemCode = Item.ItemCode
                                    objOpportunity.ItemName = Item.ItemName
                                    objOpportunity.ItemType = Item.ItemType
                                    objOpportunity.Units = CCommon.ToDecimal(Item.Quantity)
                                    objOpportunity.UnitPrice = CCommon.ToDecimal(Item.Price)
                                    objOpportunity.WarehouseItemID = Item.WarehouseItemID
                                    objOpportunity.numUOM = Item.UOMID
                                    objOpportunity.UpdateOpportunityItems()
                                Next

                                'Update Inventory
                                UpdateInventory(lngOppID)

                                objOpportunity.UpdateOpportunityLinkingDtls()

                                If Not order.ReleaseItems Is Nothing AndAlso order.ReleaseItems.Count > 0 Then
                                    Dim listInserted As New List(Of OpportunityItemsReleaseDates)


                                    Dim objOpportunityItemsReleaseDates As New OpportunityItemsReleaseDates
                                    objOpportunityItemsReleaseDates.DomainID = DomainID

                                    For Each objReleaseItem As OpportunityItemsReleaseDates In order.ReleaseItems
                                        objOpportunityItemsReleaseDates.ID = If(CCommon.ToLong(objReleaseItem.ID) = 0, -1, objReleaseItem.ID)
                                        objOpportunityItemsReleaseDates.OppID = objReleaseItem.OppID
                                        objOpportunityItemsReleaseDates.OppItemID = objReleaseItem.OppItemID
                                        objOpportunityItemsReleaseDates.ReleaseDate = DateTime.Now
                                        objOpportunityItemsReleaseDates.Quantity = CCommon.ToLong(objReleaseItem.OriginalQuantity)
                                        objOpportunityItemsReleaseDates.ReleaseStatus = 2

                                        If objReleaseItem.ID = 0 Then
                                            If listInserted.Where(Function(x) x.ID = 0 AndAlso x.OppID = objOpportunityItemsReleaseDates.OppID AndAlso x.OppItemID = objOpportunityItemsReleaseDates.OppItemID).Count() = 0 Then
                                                'IN CASE OF KIT OR MATRIX ITEM CHILD ITEMS HAVE SAME OPPID AND OPPITEMID SO NO NEED TO INSERT MULTIPLE RECORDS
                                                objOpportunityItemsReleaseDates.UpdateItemReleaseStatus(lngOppID)
                                                listInserted.Add(New OpportunityItemsReleaseDates With {.ID = 0, .OppID = objOpportunityItemsReleaseDates.OppID, .OppItemID = objOpportunityItemsReleaseDates.OppItemID})
                                            End If
                                        Else
                                            objOpportunityItemsReleaseDates.UpdateItemReleaseStatus(lngOppID)
                                        End If
                                    Next

                                    objOpportunityItemsReleaseDates.UpdateOrderReleaseStatus()
                                End If

                                objTransactionScope.Complete()
                            End Using
                        Catch ex As Exception
                            isErrorOccured = True
                        End Try
                    Next
                End If

                Return isErrorOccured
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetVendorDetail(ByVal numVendorID As Long, ByVal numItemCode As Long, ByVal numWarehouseItemID As Long, ByVal numQtyToPurchase As Decimal) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", numItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numVendorID", numVendorID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", numWarehouseItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numQty", numQtyToPurchase, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_DemandForecast_GetByItemVendor", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetReleaseItems() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDFID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numDFID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_DemandForecast_GetReleaseDatesItems", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetTotalProgressItems() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDFID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numDFID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_DemandForecast_GetTotalProgressItems", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetDemandForecastRecords(ByVal type As Short, ByVal itemCode As Long, ByVal warehouseItemID As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintType", type, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", itemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", warehouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numForecastDays", numForecastDays, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numAnalysisDays", AnalysisPattern, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitBasedOnLastYear", IsBasedOnLastYear, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOpportunityPercentComplete", OpportunityPercentComplete, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", CurrentPage, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@PageSize", PageSize, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_DemandForecast_GetRecordDetails", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetDemandForecastReleaseDateDetails(ByVal itemCode As Long, ByVal warehouseItemID As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", itemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", warehouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtReleaseDate", ReleaseDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", CurrentPage, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@PageSize", PageSize, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_DemandForecast_ReleaseDateDetails", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

#Region "Private Methods"
        Private Sub UpdateInventory(ByVal oppID As Long)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = oppID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_DemandForecast_UpdateInventory", arParms)

            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DeleteInserted(ByVal vcOppID As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@vcOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = vcOppID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_DemandForecast_DeleteOpp", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

    End Class
End Namespace
