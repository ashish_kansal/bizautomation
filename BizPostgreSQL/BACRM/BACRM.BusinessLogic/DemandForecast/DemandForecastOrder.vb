﻿Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Opportunities

Public Class DemandForecastOrder
    Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Member Variables"

    Private _vcOrderName As String
    Private _vcComment As String
    Private _numVendorID As Long
    Private _numContactID As Long
    Private _numShipmentMethodID As Long
    Private _listOrderItems As List(Of DemandForecastOrderItem)
    Private _listReleaseItems As List(Of OpportunityItemsReleaseDates)

#End Region

#Region "Public Property"
    Public Property OrderName() As String
        Get
            Return _vcOrderName
        End Get
        Set(ByVal value As String)
            _vcOrderName = value
        End Set
    End Property

    Public Property Comment() As String
        Get
            Return _vcComment
        End Get
        Set(ByVal value As String)
            _vcComment = value
        End Set
    End Property

    Public Property VendorID() As Long
        Get
            Return _numVendorID
        End Get
        Set(ByVal value As Long)
            _numVendorID = value
        End Set
    End Property

    Public Property ContactID() As Long
        Get
            Return _numContactID
        End Get
        Set(ByVal value As Long)
            _numContactID = value
        End Set
    End Property

    Public Property ShipmentMethod() As Long
        Get
            Return _numShipmentMethodID
        End Get
        Set(ByVal value As Long)
            _numShipmentMethodID = value
        End Set
    End Property

    Public Property OrderItems() As List(Of DemandForecastOrderItem)
        Get
            Return _listOrderItems
        End Get
        Set(ByVal value As List(Of DemandForecastOrderItem))
            _listOrderItems = value
        End Set
    End Property

    Public Property ReleaseItems() As List(Of OpportunityItemsReleaseDates)
        Get
            Return _listReleaseItems
        End Get
        Set(ByVal value As List(Of OpportunityItemsReleaseDates))
            _listReleaseItems = value
        End Set
    End Property

#End Region

End Class
