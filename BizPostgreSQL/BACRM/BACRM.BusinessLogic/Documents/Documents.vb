'Created Anoop Jayaraj
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Documents

    Public Class DocumentList
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _GenDocID As Long
        Private _DocCategory As Long
        Private _DocumentStatus As Long
        Private _DocName As String
        Private _FileType As String
        Private _DocDesc As String
        Private _FileName As String
        Private _UrlType As String
        Private _KeyWord As String
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _UserRightType As Short
        Private _SortOrder As Short
        Private _DivisionID As Long
        Private _ContactID As Long
        Private _SpecDocID As Long
        Private _byteMode As Short
        Private _strSpecDocDetails As String
        Private _strSQL As String
        Private _ListItemID As Long
        Private _ManagerID As Long
        Private _RecID As Long
        Private _strType As String
        Private _TerritoryID As Long
        Private _GroupID As Long
        Private _Subject As String
        Private _DocumentSection As String
        Private _FileLocation As String
        Private _EmailHstrID As Long
        Private _IsForMarketingDept As Int32
        Private _CDocType As String
        Private _Comments As String
        Private _ClientTimeZoneOffset As Int32
        Private _FileSize As String = ""

        Private _strRecordIDs As String
        Public Property RecordIds() As String
            Get
                Return _strRecordIDs
            End Get
            Set(ByVal value As String)
                _strRecordIDs = value
            End Set
        End Property


        Private _DocumentType As Short
        Public Property DocumentType() As Short
            Get
                Return _DocumentType
            End Get
            Set(ByVal value As Short)
                _DocumentType = value
            End Set
        End Property

        Public Property ClientTimeZoneOffset() As Int32
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Int32)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _input As String
        Public Property input() As String
            Get
                Return _input
            End Get
            Set(ByVal value As String)
                _input = value
            End Set
        End Property

        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal Value As String)
                _Comments = Value
            End Set
        End Property
        Public Property CDocType() As String
            Get
                Return _CDocType
            End Get
            Set(ByVal Value As String)
                _CDocType = Value
            End Set
        End Property

        Public Property IsForMarketingDept() As Int32
            Get
                Return _IsForMarketingDept
            End Get
            Set(ByVal Value As Int32)
                _IsForMarketingDept = Value
            End Set
        End Property


        Public Property EmailHstrID() As Long
            Get
                Return _EmailHstrID
            End Get
            Set(ByVal Value As Long)
                _EmailHstrID = Value
            End Set
        End Property

        Public Property FileLocation() As String
            Get
                Return _FileLocation
            End Get
            Set(ByVal Value As String)
                _FileLocation = Value
            End Set
        End Property

        Public Property DocumentSection() As String
            Get
                Return _DocumentSection
            End Get
            Set(ByVal Value As String)
                _DocumentSection = Value
            End Set
        End Property

        Public Property Subject() As String
            Get
                Return _Subject
            End Get
            Set(ByVal Value As String)
                _Subject = Value
            End Set
        End Property

        Public Property GroupID() As Long
            Get
                Return _GroupID
            End Get
            Set(ByVal Value As Long)
                _GroupID = Value
            End Set
        End Property

        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property

        Public Property strType() As String
            Get
                Return _strType
            End Get
            Set(ByVal Value As String)
                _strType = Value
            End Set
        End Property

        Public Property RecID() As Long
            Get
                Return _RecID
            End Get
            Set(ByVal Value As Long)
                _RecID = Value
            End Set
        End Property

        Public Property ManagerID() As Long
            Get
                Return _ManagerID
            End Get
            Set(ByVal Value As Long)
                _ManagerID = Value
            End Set
        End Property

        Public Property ListItemID() As Long
            Get
                Return _ListItemID
            End Get
            Set(ByVal Value As Long)
                _ListItemID = Value
            End Set
        End Property

        Public Property strSQL() As String
            Get
                Return _strSQL
            End Get
            Set(ByVal Value As String)
                _strSQL = Value
            End Set
        End Property

        Public Property strSpecDocDetails() As String
            Get
                Return _strSpecDocDetails
            End Get
            Set(ByVal Value As String)
                _strSpecDocDetails = Value
            End Set
        End Property

        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Public Property SpecDocID() As Long
            Get
                Return _SpecDocID
            End Get
            Set(ByVal Value As Long)
                _SpecDocID = Value
            End Set
        End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property SortOrder() As Short
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Short)
                _SortOrder = Value
            End Set
        End Property

        Public Property UserRightType() As Short
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Short)
                _UserRightType = Value
            End Set
        End Property



        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Property KeyWord() As String
            Get
                Return _KeyWord
            End Get
            Set(ByVal Value As String)
                _KeyWord = Value
            End Set
        End Property

        Public Property UrlType() As String
            Get
                Return _UrlType
            End Get
            Set(ByVal Value As String)
                _UrlType = Value
            End Set
        End Property

        Public Property FileName() As String
            Get
                Return _FileName
            End Get
            Set(ByVal Value As String)
                _FileName = Value
            End Set
        End Property

        Public Property DocDesc() As String
            Get
                Return _DocDesc
            End Get
            Set(ByVal Value As String)
                _DocDesc = Value
            End Set
        End Property

        Public Property FileType() As String
            Get
                Return _FileType
            End Get
            Set(ByVal Value As String)
                _FileType = Value
            End Set
        End Property

        Public Property DocumentStatus() As Long
            Get
                Return _DocumentStatus
            End Get
            Set(ByVal Value As Long)
                _DocumentStatus = Value
            End Set
        End Property

        Public Property DocCategory() As Long
            Get
                Return _DocCategory
            End Get
            Set(ByVal Value As Long)
                _DocCategory = Value
            End Set
        End Property


        Public Property DocName() As String
            Get
                Return _DocName
            End Get
            Set(ByVal Value As String)
                _DocName = Value
            End Set
        End Property




        Public Property GenDocID() As Long
            Get
                Return _GenDocID
            End Get
            Set(ByVal Value As Long)
                _GenDocID = Value
            End Set
        End Property

        Private _ModuleID As Long
        Public Property ModuleID() As Long
            Get
                Return _ModuleID
            End Get
            Set(ByVal value As Long)
                _ModuleID = value
            End Set
        End Property

        Private _byteModuleType As Short = 0
        Public Property ModuleType() As Short
            Get
                Return _byteModuleType
            End Get
            Set(ByVal value As Short)
                _byteModuleType = value
            End Set
        End Property
        'Private Property _FileSize As String = ""
        Public Property FileSize As String
            Get
                Return _FileSize
            End Get
            Set(ByVal value As String)
                _FileSize = value
            End Set
        End Property

        Private _ContactPosition As String
        Public Property ContactPosition As String
            Get
                Return _ContactPosition
            End Get
            Set(ByVal value As String)
                _ContactPosition = value
            End Set
        End Property

        Private _bizDocOppType As String
        Public Property BizDocOppType() As String
            Get
                Return _bizDocOppType
            End Get
            Set(ByVal Value As String)
                _bizDocOppType = Value
            End Set
        End Property

        Private _bizDocType As String
        Public Property BizDocType() As String
            Get
                Return _bizDocType
            End Get
            Set(ByVal Value As String)
                _bizDocType = Value
            End Set
        End Property

        Private _bizDocTemplate As String
        Public Property BizDocTemplate() As String
            Get
                Return _bizDocTemplate
            End Get
            Set(ByVal Value As String)
                _bizDocTemplate = Value
            End Set
        End Property

        Private _bitLastFollowupUpdate As Boolean
        Public Property bitLastFollowupUpdate() As Boolean
            Get
                Return _bitLastFollowupUpdate
            End Get
            Set(ByVal Value As Boolean)
                _bitLastFollowupUpdate = Value
            End Set
        End Property

        Private _numCategoryId As Long
        Public Property numCategoryId() As Long
            Get
                Return _numCategoryId
            End Get
            Set(ByVal Value As Long)
                _numCategoryId = Value
            End Set
        End Property

        Private _bitUpdateFollowupStatus As Boolean
        Public Property bitUpdateFollowupStatus() As Boolean
            Get
                Return _bitUpdateFollowupStatus
            End Get
            Set(ByVal Value As Boolean)
                _bitUpdateFollowupStatus = Value
            End Set
        End Property

        Private _numFollowUpStatusId As Long
        Public Property numFollowUpStatusId() As Long
            Get
                Return _numFollowUpStatusId
            End Get
            Set(ByVal Value As Long)
                _numFollowUpStatusId = Value
            End Set
        End Property
        Private _vcGroupsPermission As String
        Public Property vcGroupsPermission() As String
            Get
                Return _vcGroupsPermission
            End Get
            Set(ByVal Value As String)
                _vcGroupsPermission = Value
            End Set
        End Property

        Public Property FormFieldGroupId As Long
        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region



        Public Function SaveDocuments() As String()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcDocDesc", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _DocDesc

                arParms(2) = New Npgsql.NpgsqlParameter("@VcFileName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _FileName

                arParms(3) = New Npgsql.NpgsqlParameter("@vcDocName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _DocName

                arParms(4) = New Npgsql.NpgsqlParameter("@numDocCategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _DocCategory

                arParms(5) = New Npgsql.NpgsqlParameter("@vcfiletype", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(5).Value = _FileType

                arParms(6) = New Npgsql.NpgsqlParameter("@numDocStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _DocumentStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@numGenDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _GenDocID


                arParms(9) = New Npgsql.NpgsqlParameter("@cUrlType", NpgsqlTypes.NpgsqlDbType.VarChar, 1)
                arParms(9).Value = _UrlType

                arParms(10) = New Npgsql.NpgsqlParameter("@vcSubject", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(10).Value = _Subject

                arParms(11) = New Npgsql.NpgsqlParameter("@vcDocumentSection", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(11).Value = _DocumentSection

                arParms(12) = New Npgsql.NpgsqlParameter("@numRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _RecID

                arParms(13) = New Npgsql.NpgsqlParameter("@tintDocumentType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _DocumentType

                arParms(14) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt) 'for email template
                arParms(14).Value = _ModuleID

                arParms(15) = New Npgsql.NpgsqlParameter("@vcContactPosition", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(15).Value = _ContactPosition

                arParms(16) = New Npgsql.NpgsqlParameter("@numFormFieldGroupId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = FormFieldGroupId

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "usp_GenDocManage", objParam, True)

                Dim arrOutPut() As String = New String(1) {}
                arrOutPut(0) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)
                Return arrOutPut
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDocuments() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(13) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRightType

                arParms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _columnName

                arParms(6) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(6).Value = _columnSortOrder

                arParms(7) = New Npgsql.NpgsqlParameter("@DocCategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _DocCategory

                arParms(8) = New Npgsql.NpgsqlParameter("@numDocStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _DocumentStatus

                arParms(9) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _ClientTimeZoneOffset

                arParms(10) = New Npgsql.NpgsqlParameter("@numGenericDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = GenDocID

                arParms(11) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(11).Value = _SortCharacter

                arParms(12) = New Npgsql.NpgsqlParameter("@tintDocumentType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(12).Value = DocumentType

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "usp_GenDocList", arParms)

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    _TotalRecords = CInt(dt.Rows(0)("numTotalRecords"))
                Else
                    _TotalRecords = 0
                End If

                Return dt
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteDocumentOrLink() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGenericDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GenDocID


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteGenericDocuments", arParms)


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetDocByGenDocID() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 0

                arParms(1) = New Npgsql.NpgsqlParameter("@GenDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _GenDocID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GenDocDetails", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function DelDocumentsByGenDocID() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 1

                arParms(1) = New Npgsql.NpgsqlParameter("@GenDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _GenDocID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "usp_GenDocDetails", arParms)
                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetSpecificDocuments1() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RecID

                arParms(1) = New Npgsql.NpgsqlParameter("@strType", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(1).Value = _strType

                arParms(2) = New Npgsql.NpgsqlParameter("@numCategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DocCategory

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetSpecificDocuments", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetDocumentFlow() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(17) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RecID

                arParms(1) = New Npgsql.NpgsqlParameter("@strType", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(1).Value = _strType

                arParms(2) = New Npgsql.NpgsqlParameter("@numCategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DocCategory

                arParms(3) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _UserRightType

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _SortOrder

                arParms(6) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(6).Value = _SortCharacter

                arParms(7) = New Npgsql.NpgsqlParameter("@KeyWord", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = Replace(_KeyWord, "'", "''")

                arParms(8) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _CurrentPage

                arParms(9) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _PageSize

                arParms(10) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(10).Value = _columnName

                arParms(11) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(11).Value = _columnSortOrder

                arParms(12) = New Npgsql.NpgsqlParameter("@numDocStatus", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(12).Value = _DocumentStatus

                arParms(13) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(13).Direction = ParameterDirection.InputOutput
                arParms(13).Value = _TotalRecords

                arParms(14) = New Npgsql.NpgsqlParameter("@numFilter", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(14).Value = _GroupID

                arParms(15) = New Npgsql.NpgsqlParameter("@numUserId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(15).Value = UserCntID

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                arParms(17) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(17).Value = Nothing
                arParms(17).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetDocumentFlow", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(13).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <Obsolete()>
        Public Function SaveSpecDocuments() As String()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcDocDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = _DocDesc

                arParms(2) = New Npgsql.NpgsqlParameter("@VcFileName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _FileName

                arParms(3) = New Npgsql.NpgsqlParameter("@vcDocName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _DocName

                arParms(4) = New Npgsql.NpgsqlParameter("@numDocCategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _DocCategory

                arParms(5) = New Npgsql.NpgsqlParameter("@vcfiletype", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(5).Value = _FileType

                arParms(6) = New Npgsql.NpgsqlParameter("@numDocStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _DocumentStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _ContactID

                arParms(8) = New Npgsql.NpgsqlParameter("@numSpecDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _SpecDocID


                arParms(9) = New Npgsql.NpgsqlParameter("@cUrlType", NpgsqlTypes.NpgsqlDbType.VarChar, 1)
                arParms(9).Value = _UrlType

                arParms(10) = New Npgsql.NpgsqlParameter("@numRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _RecID

                arParms(11) = New Npgsql.NpgsqlParameter("@strType", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(11).Value = _strType

                arParms(12) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(12).Value = _byteMode

                arParms(13) = New Npgsql.NpgsqlParameter("@numGenericDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _GenDocID

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "usp_SpecDocManage", objParam, True)

                Dim arrOutPut() As String = New String(1) {}
                arrOutPut(0) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)
                Return arrOutPut
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetGenericDocList() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDocCategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DocCategory

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetGenDocList", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetDocListForAttachment() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcInput", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = _input

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetDocListForAttachment", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <Obsolete()>
        Public Function DelSpecDocuments() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSpecificDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SpecDocID


                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteSpecDocuments", arParms)


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        <Obsolete()>
        Public Function UpdateSpecDocuments() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@VcFileName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(0).Value = _FileName

                arParms(1) = New Npgsql.NpgsqlParameter("@numSpecificDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SpecDocID


                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateSpecDocuments", arParms)


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function EformConfiguration() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSpecDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SpecDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@strSpecDocDetails", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strSpecDocDetails


                SqlDAL.ExecuteNonQuery(connString, "USP_EFormConfiguration", arParms)


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetEformConfiguration() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSpecDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SpecDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetEFormConfiguration", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetEformDetails() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSpecDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SpecDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@strSql", NpgsqlTypes.NpgsqlDbType.VarChar, 5000)
                arParms(1).Value = _strSQL

                arParms(2) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@vcColumnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _columnName

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_EformDetails", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetListItem() As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ListItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_GetListItem", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetManager() As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numManagerID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ManagerID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_GetManager", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetTerritoryName() As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numTerritoryID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _TerritoryID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_TerritoryName", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetGroupName() As String
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GroupID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CStr(SqlDAL.ExecuteScalar(connString, "USP_GroupName", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetSpecdocDtls1() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGenericDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SpecDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetSpecificDocDtls", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ExtGetDocuments() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSearchCondition", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _DocCategory

                arParms(2) = New Npgsql.NpgsqlParameter("@cSection", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _DocumentSection

                arParms(3) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(3).Value = _SortCharacter

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _TotalRecords

                arParms(7) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _columnName

                arParms(8) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(8).Value = _columnSortOrder

                arParms(9) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = _byteMode

                arParms(10) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = UserCntID

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_ExtDocumentList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function InsertAttachemnt() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _EmailHstrID

                arParms(1) = New Npgsql.NpgsqlParameter("@fileName", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(1).Value = _FileName

                arParms(2) = New Npgsql.NpgsqlParameter("@vcLocation", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _FileLocation

                arParms(3) = New Npgsql.NpgsqlParameter("@vcFileSize", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(3).Value = _FileSize

                arParms(4) = New Npgsql.NpgsqlParameter("@vcFileType", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(4).Value = _FileType

                SqlDAL.ExecuteNonQuery(connString, "USP_InsertAttachmentIntoEmailHistory", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <Obsolete("")>
        Public Function CheckIfIsForMarketingDept() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim _DataTable As DataTable

                arParms(0) = New Npgsql.NpgsqlParameter("@numGenericDocID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = GenDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                _DataTable = SqlDAL.ExecuteDataset(connString, "SP_Get_If_For_Marketing_Dept", arParms).Tables(0)

                If Convert.ToInt32(_DataTable.Rows(0)(0).ToString()) = 1 Then
                    Return True
                Else
                    Return False
                End If

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetApprovers() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GenDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@cDocType", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(1).Value = _CDocType

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetDocApprovers", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetDocumentStatus() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "usp_GetDocumentStatus", arParms))

            Catch ex As Exception

            End Try
        End Function
        Public Function GetApproveStatus(ByVal numStatus As Int16) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GenDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@btDocType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = Val(_CDocType)

                arParms(3) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = numStatus

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetBizDocApproveStatus", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ManageApprovers() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GenDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@cDocType", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(2).Value = _CDocType

                arParms(3) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@vcComment", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(4).Value = _Comments

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_DocumentWorkFlow", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetDocumentsAttachment() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@cDocType", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _DocCategory

                arParms(3) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getDocumentsContCmpOpp", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        <Obsolete()>
        Public Function GetSpecficDocDtl() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@RecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _RecID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSpecficDocDtl", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function getCustomTags() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}                          'create a param array

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_getCustomTags", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageDocumentRepositary() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _byteMode

                arParms(1) = New Npgsql.NpgsqlParameter("@numGenericDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _GenDocID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageDocumentRepositary", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetMergeFieldsByModuleID() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ModuleID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@tintModuleType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteModuleType

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetMergeFieldsByModuleId", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub BindEmailTemplateMergeFields(ByVal RadEditor1 As Telerik.Web.UI.RadEditor, ByVal ModuleID As Long, Optional ByVal ddlDrop As System.Web.UI.WebControls.DropDownList = Nothing)
            Try
                Dim AlertDTLID As Long = 0
                Dim dtTable As DataTable
                _ModuleID = ModuleID
                _byteMode = 0
                dtTable = GetMergeFieldsByModuleID()
                If Not ddlDrop Is Nothing Then
                    ddlDrop.Items.Clear()
                    For Each row As DataRow In dtTable.Rows
                        ddlDrop.Items.Add(New System.Web.UI.WebControls.ListItem(row("vcMergeField").ToString, row("vcMergeFieldValue").ToString))
                    Next
                    ddlDrop.Items.Insert(0, New System.Web.UI.WebControls.ListItem("--Merge Field--", ""))
                Else
                    Dim ddl As Telerik.Web.UI.EditorDropDown = CType(RadEditor1.FindTool("MergeField"), Telerik.Web.UI.EditorDropDown)
                    ddl.Items.Clear()
                    For Each row As DataRow In dtTable.Rows
                        ddl.Items.Add(row("vcMergeField").ToString, row("vcMergeFieldValue").ToString)
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Public Function GetEmailTemplateContactPostionEmail() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@GenDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _GenDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetEmailTemplateContactPostionEmail", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        'Added By Priya(23 Feb 2018) (Email Template Screen)
        Public Function SaveDocumentsForEmailTemplate() As String()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(23) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcDocDesc", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _DocDesc

                arParms(2) = New Npgsql.NpgsqlParameter("@VcFileName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _FileName

                arParms(3) = New Npgsql.NpgsqlParameter("@vcDocName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _DocName

                arParms(4) = New Npgsql.NpgsqlParameter("@numDocCategory", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _DocCategory

                arParms(5) = New Npgsql.NpgsqlParameter("@vcfiletype", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(5).Value = _FileType

                arParms(6) = New Npgsql.NpgsqlParameter("@numDocStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _DocumentStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@numGenDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _GenDocID

                arParms(9) = New Npgsql.NpgsqlParameter("@cUrlType", NpgsqlTypes.NpgsqlDbType.VarChar, 1)
                arParms(9).Value = _UrlType

                arParms(10) = New Npgsql.NpgsqlParameter("@vcSubject", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(10).Value = _Subject

                arParms(11) = New Npgsql.NpgsqlParameter("@vcDocumentSection", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(11).Value = _DocumentSection

                arParms(12) = New Npgsql.NpgsqlParameter("@numRecID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _RecID

                arParms(13) = New Npgsql.NpgsqlParameter("@tintDocumentType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _DocumentType

                arParms(14) = New Npgsql.NpgsqlParameter("@numModuleID", NpgsqlTypes.NpgsqlDbType.BigInt) 'for email template
                arParms(14).Value = _ModuleID

                arParms(15) = New Npgsql.NpgsqlParameter("@vcContactPosition", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(15).Value = _ContactPosition

                arParms(16) = New Npgsql.NpgsqlParameter("@BizDocOppType", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(16).Value = _bizDocOppType

                arParms(17) = New Npgsql.NpgsqlParameter("@BizDocType", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(17).Value = _bizDocType

                arParms(18) = New Npgsql.NpgsqlParameter("@BizDocTemplate", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(18).Value = _bizDocTemplate

                arParms(19) = New Npgsql.NpgsqlParameter("@numCategoryId", NpgsqlTypes.NpgsqlDbType.BigInt, 200)
                arParms(19).Value = _numCategoryId

                arParms(20) = New Npgsql.NpgsqlParameter("@bitLastFollowupUpdate", NpgsqlTypes.NpgsqlDbType.Bit, 200)
                arParms(20).Value = _bitLastFollowupUpdate

                arParms(21) = New Npgsql.NpgsqlParameter("@vcGroupsPermission", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(21).Value = _vcGroupsPermission

                arParms(22) = New Npgsql.NpgsqlParameter("@numFollowUpStatusId", NpgsqlTypes.NpgsqlDbType.BigInt, 200)
                arParms(22).Value = _numFollowUpStatusId

                arParms(23) = New Npgsql.NpgsqlParameter("@bitUpdateFollowupStatus", NpgsqlTypes.NpgsqlDbType.Bit, 200)
                arParms(23).Value = _bitUpdateFollowupStatus

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_SaveEmailTemplates", objParam, True)

                Dim arrOutPut() As String = New String(1) {}
                arrOutPut(0) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)
                Return arrOutPut
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetByFileName() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@VcFileName", FileName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GenericDocuments_GetByFileName", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function SearchGenericDocuments() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDocCategory", DocCategory, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintDocType", DocumentType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcSearchText", KeyWord, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numPageIndex", CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numPageSize", PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_GenericDocuments_Search", sqlParams.ToArray())

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.TotalRecords = Convert.ToInt32(dt.Rows(0)("numTotalRecords"))
                Else
                    Me.TotalRecords = 0
                End If

                Return dt
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

End Namespace