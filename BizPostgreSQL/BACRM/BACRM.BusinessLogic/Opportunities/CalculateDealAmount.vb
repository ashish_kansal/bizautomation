﻿'Created BY Anoop Jayaraj

Option Explicit On
Option Strict On
Imports BACRM.BusinessLogic.Admin
Imports System.Web
Imports BACRM.BusinessLogic.Common
Namespace BACRM.BusinessLogic.Opportunities

    Public Class CalculateDealAmount
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _TotalAmount As Decimal
        Private _TotalTaxAmount As Decimal
        Private _TotalCRVTaxAmount As Decimal
        Private _TotalDiscount As Decimal
        Private _ShippingAmount As Decimal
        Private _TotalLateCharges As Decimal
        Private _GrandTotal As Decimal
        Private _DivisionID As Long

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property GrandTotal() As Decimal
            Get
                Return _GrandTotal
            End Get
            Set(ByVal Value As Decimal)
                _GrandTotal = Value
            End Set
        End Property

        Public Property TotalLateCharges() As Decimal
            Get
                Return _TotalLateCharges
            End Get
            Set(ByVal Value As Decimal)
                _TotalLateCharges = Value
            End Set
        End Property

        Public Property ShippingAmount() As Decimal
            Get
                Return _ShippingAmount
            End Get
            Set(ByVal Value As Decimal)
                _ShippingAmount = Value
            End Set
        End Property

        Public Property TotalDiscount() As Decimal
            Get
                Return _TotalDiscount
            End Get
            Set(ByVal Value As Decimal)
                _TotalDiscount = Value
            End Set
        End Property

        Public Property TotalCRVTaxAmount() As Decimal
            Get
                Return _TotalCRVTaxAmount
            End Get
            Set(ByVal Value As Decimal)
                _TotalCRVTaxAmount = Value
            End Set
        End Property

        Public Property TotalTaxAmount() As Decimal
            Get
                Return _TotalTaxAmount
            End Get
            Set(ByVal Value As Decimal)
                _TotalTaxAmount = Value
            End Set
        End Property

        Public Property TotalAmount() As Decimal
            Get
                Return _TotalAmount
            End Get
            Set(ByVal Value As Decimal)
                _TotalAmount = Value
            End Set
        End Property

        Private _CreditAmount As Decimal
        Public Property CreditAmount() As Decimal
            Get
                Return _CreditAmount
            End Get
            Set(ByVal value As Decimal)
                _CreditAmount = value
            End Set
        End Property

        Function CalculateDealAmount(ByVal OppID As Long, ByVal OppBizDocID As Long, ByVal OppType As Short, ByVal DomainID As Long, ByVal dtOppBiDocItems As DataTable, Optional ByVal FromBizInvoice As Boolean = False) As Boolean
            Try



                If dtOppBiDocItems.Rows.Count > 0 Then
                    If FromBizInvoice Then
                        _TotalAmount = CDec(dtOppBiDocItems.Compute("SUM(monTotAmount)", ""))
                    Else
                        _TotalAmount = CDec(dtOppBiDocItems.Compute("SUM(amount)", ""))
                    End If
                End If
                Dim dtOppBiDocDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.OppBizDocId = OppBizDocID
                objOppBizDocs.DomainID = DomainID
                If Not HttpContext.Current Is Nothing Then
                    objOppBizDocs.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Else
                    objOppBizDocs.ClientTimeZoneOffset = 0
                End If
                dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
                _DivisionID = CLng(dtOppBiDocDtl.Rows(0).Item("numDivisionID"))
                Dim decDisc, decInterest, decDiscAmount As Decimal
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monShipCost")) Then
                    _ShippingAmount = CDec(dtOppBiDocDtl.Rows(0).Item("monShipCost"))
                Else
                    _ShippingAmount = 0
                End If
                If CBool(dtOppBiDocDtl.Rows(0).Item("tintBillingTerms")) = True Then
                    Dim strDate1, strDate2 As Date
                    strDate1 = DateAdd(DateInterval.Day, CInt(dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")), CDate(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")))

                    If CInt(dtOppBiDocDtl.Rows(0).Item("tintshipped")) = 1 Then
                        strDate2 = CDate(dtOppBiDocDtl.Rows(0).Item("bintShippedDate"))
                    Else
                        strDate2 = Now
                    End If

                    If CBool(dtOppBiDocDtl.Rows(0).Item("tintInterestType")) = False Then
                        If strDate1 > strDate2 Then
                            decDisc = 0
                        Else
                            decDisc = CDec(dtOppBiDocDtl.Rows(0).Item("fltInterest"))
                        End If
                    Else
                        If strDate1 > strDate2 Then
                            decInterest = 0
                        Else
                            decInterest = CDec(dtOppBiDocDtl.Rows(0).Item("fltInterest"))
                        End If
                    End If
                End If

                'If OppType = 1 Then

                Dim objTaxDetails As New TaxDetails
                objTaxDetails.DomainID = DomainID
                objTaxDetails.OppID = OppID
                objTaxDetails.DivisionID = _DivisionID
                objTaxDetails.OppBizDocID = OppBizDocID
                objTaxDetails.TaxItemID = 0


                _TotalTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1()

                'Get CRV Tax Amount
                objTaxDetails.TaxItemID = 1
                _TotalCRVTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1()

                _TotalTaxAmount = _TotalTaxAmount + _TotalCRVTaxAmount

                Dim k As Integer
                Dim dtSalesTax As DataTable
                dtSalesTax = objTaxDetails.GetTaxItems()
                For k = 0 To dtSalesTax.Rows.Count - 1
                    objTaxDetails.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                    _TotalTaxAmount = _TotalTaxAmount + objTaxDetails.GetTaxAmtOppForTaxItem1()
                Next

                'End If


                ''  Dim decDiscAmount, decTotalDiscount, decTotalAmtDisc As Decimal
                If CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) > 0 Then
                    Try
                        If CBool(dtOppBiDocDtl.Rows(0).Item("bitDiscountType")) = False Then
                            decDiscAmount = _TotalAmount * (100 - CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))) / 100
                        Else
                            decDiscAmount = _TotalAmount - CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))
                        End If

                        If decDisc > 0 Then
                            If CBool(dtOppBiDocDtl.Rows(0).Item("bitDiscountType")) = False Then
                                _TotalDiscount = _TotalAmount * CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) / 100 + decDiscAmount * decDisc / 100
                            Else
                                _TotalDiscount = CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) + decDiscAmount * decDisc / 100
                            End If
                            _GrandTotal = _TotalAmount - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                        Else
                            If CBool(dtOppBiDocDtl.Rows(0).Item("bitDiscountType")) = False Then
                                _TotalDiscount = _TotalAmount * CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) / 100
                            Else
                                _TotalDiscount = CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))
                            End If

                            If decInterest * decDiscAmount <> 0 Then
                                _TotalLateCharges = decInterest * decDiscAmount / 100
                            End If
                            _GrandTotal = _TotalAmount + decInterest * decDiscAmount / 100 - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                        End If
                    Catch ex As Exception

                    End Try
                Else
                    If decDisc > 0 Then
                        _TotalDiscount = _TotalAmount * decDisc / 100
                        _GrandTotal = _TotalAmount - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                    Else
                        _TotalDiscount = 0
                        _TotalLateCharges = _TotalAmount * decInterest / 100
                        _GrandTotal = _TotalAmount + _TotalLateCharges + _ShippingAmount + _TotalTaxAmount
                    End If
                End If

                If CDec(dtOppBiDocDtl.Rows(0).Item("monCreditAmount")) > 0 Then
                    _CreditAmount = CDec(dtOppBiDocDtl.Rows(0).Item("monCreditAmount"))
                    '    _GrandTotal = _GrandTotal - _CreditAmount 'This is commented by chintan: Reason: It creates imbalance in journal entries, as well when we issue credit to customer it is already accounted on Account Receivable.
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Added by sandeep for BizAPI. please change logic of this method if changes are made in CalculateDealAmount method
        Function CalculateDealAmountBizAPI(ByVal OppID As Long, ByVal OppBizDocID As Long, ByVal OppType As Short, ByVal DomainID As Long, ByVal dtOppBiDocItems As DataTable, ByVal utcOffset As Integer, Optional ByVal FromBizInvoice As Boolean = False) As Boolean
            Try
                If dtOppBiDocItems.Rows.Count > 0 Then
                    If FromBizInvoice Then
                        _TotalAmount = CDec(dtOppBiDocItems.Compute("SUM(monTotAmount)", ""))
                    Else
                        _TotalAmount = CDec(dtOppBiDocItems.Compute("SUM(amount)", ""))
                    End If
                End If
                Dim dtOppBiDocDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.OppBizDocId = OppBizDocID
                objOppBizDocs.DomainID = DomainID
                objOppBizDocs.ClientTimeZoneOffset = utcOffset
                dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
                _DivisionID = CLng(dtOppBiDocDtl.Rows(0).Item("numDivisionID"))
                Dim decDisc, decInterest, decDiscAmount As Decimal
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monShipCost")) Then
                    _ShippingAmount = CDec(dtOppBiDocDtl.Rows(0).Item("monShipCost"))
                Else
                    _ShippingAmount = 0
                End If
                If CBool(dtOppBiDocDtl.Rows(0).Item("tintBillingTerms")) = True Then
                    Dim strDate1, strDate2 As Date
                    strDate1 = DateAdd(DateInterval.Day, CInt(dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")), CDate(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")))

                    If CInt(dtOppBiDocDtl.Rows(0).Item("tintshipped")) = 1 Then
                        strDate2 = CDate(dtOppBiDocDtl.Rows(0).Item("bintShippedDate"))
                    Else
                        strDate2 = Now
                    End If

                    If CBool(dtOppBiDocDtl.Rows(0).Item("tintInterestType")) = False Then
                        If strDate1 > strDate2 Then
                            decDisc = 0
                        Else
                            decDisc = CDec(dtOppBiDocDtl.Rows(0).Item("fltInterest"))
                        End If
                    Else
                        If strDate1 > strDate2 Then
                            decInterest = 0
                        Else
                            decInterest = CDec(dtOppBiDocDtl.Rows(0).Item("fltInterest"))
                        End If
                    End If
                End If

                If OppType = 1 Then

                    Dim objTaxDetails As New TaxDetails
                    objTaxDetails.DomainID = DomainID
                    objTaxDetails.OppID = OppID
                    objTaxDetails.DivisionID = _DivisionID
                    objTaxDetails.OppBizDocID = OppBizDocID
                    objTaxDetails.TaxItemID = 0
                    _TotalTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1()

                    'Get CRV Tax Amount
                    objTaxDetails.TaxItemID = 1
                    _TotalCRVTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1()

                    _TotalTaxAmount = _TotalTaxAmount + _TotalCRVTaxAmount

                    Dim k As Integer
                    Dim dtSalesTax As DataTable
                    dtSalesTax = objTaxDetails.GetTaxItems()
                    For k = 0 To dtSalesTax.Rows.Count - 1
                        objTaxDetails.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                        _TotalTaxAmount = _TotalTaxAmount + objTaxDetails.GetTaxAmtOppForTaxItem1()
                    Next

                End If


                ''  Dim decDiscAmount, decTotalDiscount, decTotalAmtDisc As Decimal
                If CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) > 0 Then
                    Try
                        If CBool(dtOppBiDocDtl.Rows(0).Item("bitDiscountType")) = False Then
                            decDiscAmount = _TotalAmount * (100 - CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))) / 100
                        Else
                            decDiscAmount = _TotalAmount - CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))
                        End If

                        If decDisc > 0 Then
                            If CBool(dtOppBiDocDtl.Rows(0).Item("bitDiscountType")) = False Then
                                _TotalDiscount = _TotalAmount * CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) / 100 + decDiscAmount * decDisc / 100
                            Else
                                _TotalDiscount = CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) + decDiscAmount * decDisc / 100
                            End If
                            _GrandTotal = _TotalAmount - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                        Else
                            If CBool(dtOppBiDocDtl.Rows(0).Item("bitDiscountType")) = False Then
                                _TotalDiscount = _TotalAmount * CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) / 100
                            Else
                                _TotalDiscount = CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))
                            End If

                            If decInterest * decDiscAmount <> 0 Then
                                _TotalLateCharges = decInterest * decDiscAmount / 100
                            End If
                            _GrandTotal = _TotalAmount + decInterest * decDiscAmount / 100 - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                        End If
                    Catch ex As Exception

                    End Try
                Else
                    If decDisc > 0 Then
                        _TotalDiscount = _TotalAmount * decDisc / 100
                        _GrandTotal = _TotalAmount - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                    Else
                        _TotalDiscount = 0
                        _TotalLateCharges = _TotalAmount * decInterest / 100
                        _GrandTotal = _TotalAmount + _TotalLateCharges + _ShippingAmount + _TotalTaxAmount
                    End If
                End If

                If CDec(dtOppBiDocDtl.Rows(0).Item("monCreditAmount")) > 0 Then
                    _CreditAmount = CDec(dtOppBiDocDtl.Rows(0).Item("monCreditAmount"))
                    '    _GrandTotal = _GrandTotal - _CreditAmount 'This is commented by chintan: Reason: It creates imbalance in journal entries, as well when we issue credit to customer it is already accounted on Account Receivable.
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function CalculateDealAmountForMirrorBizDoc(ByVal ReferenceID As Long, ByVal ReferenceType As Short, ByVal OppType As Short, _
                                                    ByVal DomainID As Long, ByVal dtItems As DataTable, ByVal dtOppBiDocDtl As DataTable, _
                                                    Optional ByVal FromBizInvoice As Boolean = False) As Boolean
            Try

                If dtItems.Rows.Count > 0 Then
                    If FromBizInvoice Then
                        _TotalAmount = CDec(dtItems.Compute("SUM(monTotAmount)", ""))
                    Else
                        _TotalAmount = CDec(dtItems.Compute("SUM(amount)", ""))
                    End If
                End If
                
                _DivisionID = CLng(dtOppBiDocDtl.Rows(0).Item("numDivisionID"))
                Dim decDisc, decInterest, decDiscAmount As Decimal
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monShipCost")) Then
                    _ShippingAmount = CDec(dtOppBiDocDtl.Rows(0).Item("monShipCost"))
                Else
                    _ShippingAmount = 0
                End If
                If CBool(dtOppBiDocDtl.Rows(0).Item("tintBillingTerms")) = True Then
                    Dim strDate1, strDate2 As Date
                    strDate1 = DateAdd(DateInterval.Day, CInt(dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")), CDate(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")))

                    strDate2 = Now

                    If CBool(dtOppBiDocDtl.Rows(0).Item("tintInterestType")) = False Then
                        If strDate1 > strDate2 Then
                            decDisc = 0
                        Else
                            decDisc = CDec(dtOppBiDocDtl.Rows(0).Item("fltInterest"))
                        End If
                    Else
                        If strDate1 > strDate2 Then
                            decInterest = 0
                        Else
                            decInterest = CDec(dtOppBiDocDtl.Rows(0).Item("fltInterest"))
                        End If
                    End If
                End If

                'If OppType = 1 Then

                Dim objTaxDetails As New TaxDetails
                objTaxDetails.DomainID = DomainID
                objTaxDetails.OppID = ReferenceID
                objTaxDetails.DivisionID = _DivisionID
                objTaxDetails.OppBizDocID = 0
                objTaxDetails.TaxItemID = 0
                objTaxDetails.mode = CShort(IIf(ReferenceType = 1 Or ReferenceType = 3, 0, ReferenceType))
                _TotalTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1()

                ' GET CRV TAX
                objTaxDetails.TaxItemID = 1
                _TotalCRVTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1()

                _TotalTaxAmount = _TotalTaxAmount + _TotalCRVTaxAmount

                Dim k As Integer
                Dim dtSalesTax As DataTable
                dtSalesTax = objTaxDetails.GetTaxItems()
                For k = 0 To dtSalesTax.Rows.Count - 1
                    objTaxDetails.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                    _TotalTaxAmount = _TotalTaxAmount + objTaxDetails.GetTaxAmtOppForTaxItem1()
                Next

                'End If

                If (ReferenceType = 5 Or ReferenceType = 6 Or ReferenceType = 7 Or ReferenceType = 8 Or ReferenceType = 9 Or ReferenceType = 10) Then
                    decDiscAmount = _TotalAmount - CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))
                    _TotalDiscount = CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))
                    _GrandTotal = _TotalAmount - _TotalDiscount + _TotalTaxAmount
                ElseIf CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) > 0 Then
                    decDiscAmount = _TotalAmount - CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))
                    _TotalDiscount = CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) + decDiscAmount * decDisc / 100
                    _GrandTotal = _TotalAmount - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                    _TotalDiscount = CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))
                    If decInterest * decDiscAmount <> 0 Then
                        _TotalLateCharges = decInterest * decDiscAmount / 100
                    End If
                    _GrandTotal = _TotalAmount + decInterest * decDiscAmount / 100 - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                    Try
                        If CBool(dtOppBiDocDtl.Rows(0).Item("bitDiscountType")) = False Then
                            decDiscAmount = _TotalAmount * (100 - CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))) / 100
                        Else
                            decDiscAmount = _TotalAmount - CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))
                        End If

                        If decDisc > 0 Then
                            If CBool(dtOppBiDocDtl.Rows(0).Item("bitDiscountType")) = False Then
                                _TotalDiscount = _TotalAmount * CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) / 100 + decDiscAmount * decDisc / 100
                            Else
                                _TotalDiscount = CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) + decDiscAmount * decDisc / 100
                            End If
                            _GrandTotal = _TotalAmount - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                        Else
                            If CBool(dtOppBiDocDtl.Rows(0).Item("bitDiscountType")) = False Then
                                _TotalDiscount = _TotalAmount * CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount")) / 100
                            Else
                                _TotalDiscount = CDec(dtOppBiDocDtl.Rows(0).Item("fltDiscount"))
                            End If

                            If decInterest * decDiscAmount <> 0 Then
                                _TotalLateCharges = decInterest * decDiscAmount / 100
                            End If
                            _GrandTotal = _TotalAmount + decInterest * decDiscAmount / 100 - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                        End If
                    Catch ex As Exception

                    End Try
                Else
                    If decDisc > 0 Then
                        _TotalDiscount = _TotalAmount * decDisc / 100
                        _GrandTotal = _TotalAmount - _TotalDiscount + _ShippingAmount + _TotalTaxAmount
                    Else
                        _TotalDiscount = 0
                        _TotalLateCharges = _TotalAmount * decInterest / 100
                        _GrandTotal = _TotalAmount + _TotalLateCharges + _ShippingAmount + _TotalTaxAmount
                    End If
                End If

                If CDec(dtOppBiDocDtl.Rows(0).Item("monCreditAmount")) > 0 Then
                    _CreditAmount = CDec(dtOppBiDocDtl.Rows(0).Item("monCreditAmount"))
                    '    _GrandTotal = _GrandTotal - _CreditAmount 'This is commented by chintan: Reason: It creates imbalance in journal entries, as well when we issue credit to customer it is already accounted on Account Receivable.
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


    End Class

End Namespace
