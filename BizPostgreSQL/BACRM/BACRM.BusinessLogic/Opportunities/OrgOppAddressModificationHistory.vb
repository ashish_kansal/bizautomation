﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Opportunities

    Public Class OrgOppAddressModificationHistory
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Members"

        Public Property AddressOfRecord As Short '1: Organization, 2:Opp/Order
        Public Property AddressType As Short '1: Billing Address, 2: Shipping Address
        Public Property RecordID As Long
        Public Property AddressID As Long

#End Region

#Region "Public Methods"

        Public Function GetModificationDetail() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", RecordID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numAddressID", AddressID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintAddressOf", AddressOfRecord, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintAddressType", AddressType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", AddressOfRecord, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_OrgOppAddressModificationHistory_Get", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

    End Class
End Namespace
