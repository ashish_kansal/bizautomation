'Created Anoop Jayaraj
Option Explicit On 
Option Strict On

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Opportunities
    Public Class OppTimeExpense
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _OppID As Long
        Private _OppStageID As Long
        Private _BizDocID As Long
        Private _Rate As Decimal
        Private _Hour As Long
        Private _Min As Short
        Private _Amount As Long
        Private _Desc As String
        Private _ItemID As Long
        Private _FromDate As Date = New Date(1753, 1, 1)
        Private _ToDate As Date = New Date(1753, 1, 1)
        Private _Billable As Boolean = True
        Private _ContractID As Long = 0
        Private _ClientTimeZoneOffset As Integer = 0
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public Property ContractID() As Long
            Get
                Return _ContractID
            End Get
            Set(ByVal Value As Long)
                _ContractID = Value
            End Set
        End Property
        Public Property Billable() As Boolean
            Get
                Return _Billable
            End Get
            Set(ByVal Value As Boolean)
                _Billable = Value
            End Set
        End Property

        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        Public Property ItemID() As Long
            Get
                Return _ItemID
            End Get
            Set(ByVal Value As Long)
                _ItemID = Value
            End Set
        End Property

        Public Property Desc() As String
            Get
                Return _Desc
            End Get
            Set(ByVal Value As String)
                _Desc = Value
            End Set
        End Property

        Public Property Amount() As Long
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Long)
                _Amount = Value
            End Set
        End Property


        Public Property Min() As Short
            Get
                Return _Min
            End Get
            Set(ByVal Value As Short)
                _Min = Value
            End Set
        End Property

        Public Property Hour() As Long
            Get
                Return _Hour
            End Get
            Set(ByVal Value As Long)
                _Hour = Value
            End Set
        End Property

        Public Property Rate() As Decimal
            Get
                Return _Rate
            End Get
            Set(ByVal Value As Decimal)
                _Rate = Value
            End Set
        End Property

        Public Property BizDocID() As Long
            Get
                Return _BizDocID
            End Get
            Set(ByVal Value As Long)
                _BizDocID = Value
            End Set
        End Property

        Public Property OppStageID() As Long
            Get
                Return _OppStageID
            End Get
            Set(ByVal Value As Long)
                _OppStageID = Value
            End Set
        End Property


        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal Value As Long)
                _OppID = Value
            End Set
        End Property

#Region "Constructor"
        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32               
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '**********************************************************************************
        Public Sub New(ByVal intUserId As Integer)
            'Constructor
            MyBase.New(intUserId)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32              
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '**********************************************************************************
        Public Sub New()
            'Constructor
            MyBase.New()
        End Sub
#End Region

        Public Function GetBizDoc() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As SqlParameter = New SqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New SqlParameter("@ListName", SqlDbType.VarChar)
                arParms(0).Value = "BizDocName"


                arParms(1) = New SqlParameter("@bitDeleted", SqlDbType.Bit)
                arParms(1).Value = 0

                ds = SqlDAL.ExecuteDataset(ConnString, "USP_OPPMasterList", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetTimeDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As SqlParameter = New SqlParameter(12) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 1


                arParms(1) = New SqlParameter("@numOppId", SqlDbType.BigInt)
                arParms(1).Value = _OppID

                arParms(2) = New SqlParameter("@numOppStageID", SqlDbType.BigInt)
                arParms(2).Value = _OppStageID

                arParms(3) = New SqlParameter("@numBizDocID", SqlDbType.BigInt)
                arParms(3).Value = _BizDocID

                arParms(4) = New SqlParameter("@numRate", SqlDbType.BigInt)
                arParms(4).Value = _Rate

                'arParms(5) = New SqlParameter("@numHours", SqlDbType.BigInt)
                'arParms(5).Value = _Hour


                'arParms(6) = New SqlParameter("@numMins", SqlDbType.BigInt)
                'arParms(6).Value = _Min

                arParms(5) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(5).Value = _Desc

                arParms(6) = New SqlParameter("@numItemID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ItemID

                arParms(7) = New SqlParameter("@dtStartTime", SqlDbType.DateTime)
                arParms(7).Value = _FromDate

                arParms(8) = New SqlParameter("@dtEndTime  ", SqlDbType.DateTime)
                arParms(8).Value = _ToDate

                arParms(9) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(9).Value = _Billable

                arParms(10) = New SqlParameter("@numContractID", SqlDbType.BigInt)
                arParms(10).Value = _ContractID

                arParms(11) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(11).Value = _intUserId
                arParms(12) = New SqlParameter("@ClientTimeZoneOffset", SqlDbType.Int)
                arParms(12).Value = _ClientTimeZoneOffset
                ds = SqlDAL.ExecuteDataset(ConnString, "usp_OppTime", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function SaveTimeDetails() As Boolean
            Try
                Dim arParms() As SqlParameter = New SqlParameter(12) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 0


                arParms(1) = New SqlParameter("@numOppId", SqlDbType.BigInt)
                arParms(1).Value = _OppID

                arParms(2) = New SqlParameter("@numOppStageID", SqlDbType.BigInt)
                arParms(2).Value = _OppStageID

                arParms(3) = New SqlParameter("@numBizDocID", SqlDbType.BigInt)
                arParms(3).Value = _BizDocID

                arParms(4) = New SqlParameter("@numRate", SqlDbType.Decimal)
                arParms(4).Value = _Rate

                'arParms(5) = New SqlParameter("@numHours", SqlDbType.BigInt)
                'arParms(5).Value = _Hour


                'arParms(6) = New SqlParameter("@numMins", SqlDbType.BigInt)
                'arParms(6).Value = _Min

                arParms(5) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(5).Value = _Desc

                arParms(6) = New SqlParameter("@numItemID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ItemID

                arParms(7) = New SqlParameter("@dtStartTime", SqlDbType.DateTime)
                arParms(7).Value = _FromDate

                arParms(8) = New SqlParameter("@dtEndTime  ", SqlDbType.DateTime)
                arParms(8).Value = _ToDate

                arParms(9) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(9).Value = _Billable

                arParms(10) = New SqlParameter("@numContractID", SqlDbType.BigInt)
                arParms(10).Value = _ContractID

                arParms(11) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(11).Value = _intUserId
                arParms(12) = New SqlParameter("@ClientTimeZoneOffset", SqlDbType.Int)
                arParms(12).Value = _ClientTimeZoneOffset
                SqlDAL.ExecuteNonQuery(ConnString, "usp_OppTime", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteTimeDetails() As Boolean
            Try
                Dim arParms() As SqlParameter = New SqlParameter(12) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 2


                arParms(1) = New SqlParameter("@numOppId", SqlDbType.BigInt)
                arParms(1).Value = _OppID

                arParms(2) = New SqlParameter("@numOppStageID", SqlDbType.BigInt)
                arParms(2).Value = _OppStageID

                arParms(3) = New SqlParameter("@numBizDocID", SqlDbType.BigInt)
                arParms(3).Value = _BizDocID

                arParms(4) = New SqlParameter("@numRate", SqlDbType.Decimal)
                arParms(4).Value = _Rate

                'arParms(5) = New SqlParameter("@numHours", SqlDbType.BigInt)
                'arParms(5).Value = _Hour


                'arParms(6) = New SqlParameter("@numMins", SqlDbType.BigInt)
                'arParms(6).Value = _Min

                arParms(5) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(5).Value = _Desc

                arParms(6) = New SqlParameter("@numItemID", SqlDbType.BigInt, 9)
                arParms(6).Value = _ItemID

                arParms(7) = New SqlParameter("@dtStartTime", SqlDbType.DateTime)
                arParms(7).Value = _FromDate

                arParms(8) = New SqlParameter("@dtEndTime  ", SqlDbType.DateTime)
                arParms(8).Value = _ToDate

                arParms(9) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(9).Value = _Billable

                arParms(10) = New SqlParameter("@numContractID", SqlDbType.BigInt)
                arParms(10).Value = _ContractID

                arParms(11) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(11).Value = _intUserId
                arParms(12) = New SqlParameter("@ClientTimeZoneOffset", SqlDbType.Int)
                arParms(12).Value = _ClientTimeZoneOffset
                SqlDAL.ExecuteNonQuery(connString, "usp_OppTime", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetExpenseDetails() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As SqlParameter = New SqlParameter(8) {}

                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 1


                arParms(1) = New SqlParameter("@numOppId", SqlDbType.BigInt)
                arParms(1).Value = _OppID

                arParms(2) = New SqlParameter("@numOppStageID", SqlDbType.BigInt)
                arParms(2).Value = _OppStageID

                arParms(3) = New SqlParameter("@numBizDocID", SqlDbType.BigInt)
                arParms(3).Value = _BizDocID

                arParms(4) = New SqlParameter("@monAmount", SqlDbType.BigInt)
                arParms(4).Value = _Amount

                arParms(5) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(5).Value = _Desc

                arParms(6) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(6).Value = _Billable

                arParms(7) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(7).Value = _ContractID

                arParms(8) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(8).Value = _intUserId

                ds = SqlDAL.ExecuteDataset(ConnString, "usp_OppExpense", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function SaveExpenseDetails() As Boolean
            Try
                Dim arParms() As SqlParameter = New SqlParameter(8) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 0


                arParms(1) = New SqlParameter("@numOppId", SqlDbType.BigInt)
                arParms(1).Value = _OppID

                arParms(2) = New SqlParameter("@numOppStageID", SqlDbType.BigInt)
                arParms(2).Value = _OppStageID

                arParms(3) = New SqlParameter("@numBizDocID", SqlDbType.BigInt)
                arParms(3).Value = _BizDocID

                arParms(4) = New SqlParameter("@monAmount", SqlDbType.BigInt)
                arParms(4).Value = _Amount

                arParms(5) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(5).Value = _Desc

                arParms(6) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(6).Value = _Billable

                arParms(7) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(7).Value = _ContractID

                arParms(8) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(8).Value = _intUserId
                SqlDAL.ExecuteNonQuery(ConnString, "usp_OppExpense", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteExpenseDetails() As Boolean
            Try
                Dim arParms() As SqlParameter = New SqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New SqlParameter("@byteMode", SqlDbType.TinyInt)
                arParms(0).Value = 2


                arParms(1) = New SqlParameter("@numOppId", SqlDbType.BigInt)
                arParms(1).Value = _OppID

                arParms(2) = New SqlParameter("@numOppStageID", SqlDbType.BigInt)
                arParms(2).Value = _OppStageID

                arParms(3) = New SqlParameter("@numBizDocID", SqlDbType.BigInt)
                arParms(3).Value = _BizDocID

                arParms(4) = New SqlParameter("@monAmount", SqlDbType.BigInt)
                arParms(4).Value = _Amount

                arParms(5) = New SqlParameter("@vcDesc", SqlDbType.VarChar, 1000)
                arParms(5).Value = _Desc


                arParms(6) = New SqlParameter("@bitBillable", SqlDbType.Bit)
                arParms(6).Value = _Billable

                arParms(7) = New SqlParameter("@numContractID", SqlDbType.BigInt, 9)
                arParms(7).Value = _ContractID
                arParms(8) = New SqlParameter("@numUserCntId", SqlDbType.BigInt, 9)
                arParms(8).Value = _intUserId
                SqlDAL.ExecuteNonQuery(connString, "usp_OppExpense", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

    End Class
End Namespace