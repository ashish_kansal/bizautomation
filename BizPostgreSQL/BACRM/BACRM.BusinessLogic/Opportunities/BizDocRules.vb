﻿Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Web.UI.WebControls
Namespace BACRM.BusinessLogic.Opportunities
    Public Class BizDocRules
        Inherits BACRM.BusinessLogic.CBusinessBase
        Dim DomainId As Int16
        Dim _ModeID As Int16
        Dim _BizDocAppID As Integer
        Dim _BizDocTypeID As Integer
        Dim _RangeFrom As Double
        Dim _RangeTo As Double
        Dim _BizDocCreated As Int16
        Dim _BizDocPaidFull As Int16
        Dim _ActionTypeID As Integer
        Dim _ApproveDocStatusID As Integer
        Dim _DeclainDocStatusID As Integer
        Dim _BizDocStatusID As Integer
        Dim _BizDocStatus As String
        Dim _BizDocRuleDt As DataTable
        Dim _BizDocStatusDt As DataTable
        Dim _EmployeeID As Integer
        Dim _Employee As String
        Dim _BizDocType As String
        Dim _ActionType As String
        Dim _BizDocApprovalStatus As String
        Dim _BizDocDeclainStatus As String
        Dim _SelectedEmployeeID As String
        Dim _SelectedEmployee As String
        Dim _BizDocTypeValue As String
        Dim _BizDocStatusValue As String
        Dim _EmployeeValue As String
        Dim _ActionTypeValue As String
        Public Property BizDocTypeValue() As String
            Get
                Return _BizDocTypeValue
            End Get
            Set(ByVal value As String)
                _BizDocTypeValue = value
            End Set
        End Property

        Public Property ActionTypeValue() As String
            Get
                Return _ActionTypeValue
            End Get
            Set(ByVal value As String)
                _ActionTypeValue = value
            End Set
        End Property

        Public Property EmployeeValue() As String
            Get
                Return _EmployeeValue
            End Get
            Set(ByVal value As String)
                _EmployeeValue = value
            End Set
        End Property

        Public Property BizDocStatusValue() As String
            Get
                Return _BizDocStatusValue
            End Get
            Set(ByVal value As String)
                _BizDocStatusValue = value
            End Set
        End Property

        Public Property Employee() As String
            Get
                Return _Employee
            End Get
            Set(ByVal value As String)
                _Employee = value
            End Set
        End Property
        Public Property BizDocStatus() As String
            Get
                Return _BizDocStatus
            End Get
            Set(ByVal value As String)
                _BizDocStatus = value
            End Set
        End Property
        Public Property SelectedEmployeeID() As String
            Get
                Return _SelectedEmployeeID
            End Get
            Set(ByVal value As String)
                _SelectedEmployeeID = value
            End Set
        End Property
        Public Property SelectedEmployee() As String
            Get
                Return _SelectedEmployee
            End Get
            Set(ByVal value As String)
                _SelectedEmployee = value
            End Set
        End Property

        Public Property BizDocApprovalStatus() As String
            Get
                Return _BizDocApprovalStatus
            End Get
            Set(ByVal value As String)
                _BizDocApprovalStatus = value
            End Set
        End Property
        Public Property BizDocType() As String
            Get
                Return _BizDocType
            End Get
            Set(ByVal value As String)
                _BizDocType = value
            End Set
        End Property
        Public Property ActionType() As String
            Get
                Return _ActionType
            End Get
            Set(ByVal value As String)
                _ActionType = value
            End Set
        End Property
        Public Property BizDocDeclainStatus() As String
            Get
                Return _BizDocDeclainStatus
            End Get
            Set(ByVal value As String)
                _BizDocDeclainStatus = value
            End Set
        End Property



        Public Property EmployeeID() As Integer
            Get
                Return _EmployeeID
            End Get
            Set(ByVal value As Integer)
                _EmployeeID = value
            End Set
        End Property

        Public Property BizDocStatusID() As Integer
            Get
                Return _BizDocStatusID
            End Get
            Set(ByVal value As Integer)
                _BizDocStatusID = value
            End Set
        End Property

        Public Property BizDocRuleDt() As DataTable
            Get
                Return _BizDocRuleDt
            End Get
            Set(ByVal value As DataTable)
                _BizDocRuleDt = value
            End Set
        End Property

        Public Property BizDocStatusDt() As DataTable
            Get
                Return _BizDocStatusDt
            End Get
            Set(ByVal value As DataTable)
                _BizDocStatusDt = value
            End Set
        End Property

        Public Property DeclainDocStatusID() As Integer
            Get
                Return _DeclainDocStatusID
            End Get
            Set(ByVal value As Integer)
                _DeclainDocStatusID = value
            End Set
        End Property

        Public Property ApproveDocStatusID() As Integer
            Get
                Return _ApproveDocStatusID
            End Get
            Set(ByVal value As Integer)
                _ApproveDocStatusID = value
            End Set
        End Property

        Public Property ActionTypeID() As Integer
            Get
                Return _ActionTypeID
            End Get
            Set(ByVal value As Integer)
                _ActionTypeID = value
            End Set
        End Property

        'Public Property DomainId() As Int16
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Int16)
        '        DomainId = value
        '    End Set
        'End Property
        Public Property ModeID() As Int16
            Get
                Return _ModeID
            End Get
            Set(ByVal value As Int16)
                _ModeID = value
            End Set
        End Property
        Public Property BizDocAppID() As Integer
            Get
                Return _BizDocAppID
            End Get
            Set(ByVal value As Integer)
                _BizDocAppID = value
            End Set
        End Property

        Public Property BizDocTypeID() As Integer
            Get
                Return _BizDocTypeID
            End Get
            Set(ByVal value As Integer)
                _BizDocTypeID = value
            End Set
        End Property
        Public Property RangeFrom() As Double
            Get
                Return _RangeFrom
            End Get
            Set(ByVal value As Double)
                _RangeFrom = value
            End Set
        End Property
        Public Property RangeTo() As Double
            Get
                Return _RangeTo
            End Get
            Set(ByVal value As Double)
                _RangeTo = value
            End Set
        End Property

        Public Property BizDocCreated() As Int16
            Get
                Return _BizDocCreated
            End Get
            Set(ByVal value As Int16)
                _BizDocCreated = value
            End Set
        End Property

        Public Property BizDocPaidFull() As Int16
            Get
                Return _BizDocPaidFull
            End Get
            Set(ByVal value As Int16)
                _BizDocPaidFull = value
            End Set
        End Property
        Public Sub GetBizStatusRule()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(1).Value = Nothing
                arparms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocStatusApprove", arparms)
                BizDocStatusDt = ds.Tables(0)
                'If ds.Tables(0).Rows.Count = 0 Then Exit Sub
                'BizDocTypeID = CType(ds.Tables(0).Rows(0).Item("numBizDocTypeID"), Integer)
                'BizDocType = CType(ds.Tables(0).Rows(0).Item("BizDocType"), String)
                'BizDocStatusID = CType(ds.Tables(0).Rows(0).Item("numBizDocStatusID"), Integer)
                'BizDocStatus = CType(ds.Tables(0).Rows(0).Item("BizDocStatus"), String)
                'EmployeeID = CType(ds.Tables(0).Rows(0).Item("numEmployeeID"), Integer)
                'Employee = CType(ds.Tables(0).Rows(0).Item("Employee"), String)
                'ActionTypeID = CType(ds.Tables(0).Rows(0).Item("numActionTypeID"), Integer)
                'ActionType = CType(ds.Tables(0).Rows(0).Item("ActionType"), String)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function SaveBizStatusRule() As Integer
            Try
                SaveBizStatusRule = 0

                If BizDocStatusDt.Rows.Count = 0 Then Return 1


                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim objRow As DataRow

                For Each objRow In BizDocStatusDt.Rows

                    Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                    arparms(0) = New Npgsql.NpgsqlParameter("@numBizDocStatusAppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(0).Value = 0

                    arparms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(1).Value = DomainId

                    arparms(2) = New Npgsql.NpgsqlParameter("@numBizDocTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(2).Value = objRow.Item("BizDocTypeID")

                    arparms(3) = New Npgsql.NpgsqlParameter("@numBizDocStatusID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(3).Value = objRow.Item("BizDocStatusID")

                    arparms(4) = New Npgsql.NpgsqlParameter("@numEmployeeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(4).Value = objRow.Item("EmployeeID")

                    arparms(5) = New Npgsql.NpgsqlParameter("@numActionTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(5).Value = objRow.Item("ActionTypeID")

                    arparms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                    arparms(6).Value = Nothing
                    arparms(6).Direction = ParameterDirection.InputOutput

                    Dim ds As New DataSet
                    ds = SqlDAL.ExecuteDataset(connString, "USP_ManagerBizDocStatusApprove", arparms)
                    If ds.Tables(0).Rows.Count > 0 Then SaveBizStatusRule = CType(ds.Tables(0).Rows(0).Item(0), Integer)

                Next

                Return SaveBizStatusRule

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ValidationBizDocStatus() As Boolean
            Try
                ValidationBizDocStatus = True
                Dim objRow() As DataRow
                Dim intLoop As Integer

                ValidationBizDocStatus = True

                objRow = BizDocStatusDt.Select("BizDocTypeID='" & _BizDocTypeID & "' AND BizDocStatusID=" & _BizDocStatusID & " AND EmployeeID=" & _EmployeeID & " AND ActionTypeID=" & _ActionTypeID)
                If objRow.Length > 0 Then ValidationBizDocStatus = False
                Exit Function


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub AddEditBizDocStatusGrid()
            Try
                Dim objRow As DataRow
                objRow = _BizDocStatusDt.NewRow
                objRow.Item("BizDocTypeID") = _BizDocTypeID
                objRow.Item("BizDocStatusID") = _BizDocStatusID
                objRow.Item("EmployeeID") = _EmployeeID
                objRow.Item("ActionTypeID") = _ActionTypeID
                objRow.Item("BizDocTypeValue") = _BizDocTypeValue
                objRow.Item("BizDocStatusValue") = _BizDocStatusValue
                objRow.Item("EmployeeValue") = _EmployeeValue
                objRow.Item("ActionTypeValue") = _ActionTypeValue
                objRow.Item("numBizDocStatusID") = 0
                _BizDocStatusDt.Rows.Add(objRow)
                _BizDocStatusDt.AcceptChanges()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub AddEditBizDocGrid()
            Try
                Dim objRow As DataRow
                Dim strCondition As String

                objRow = _BizDocRuleDt.NewRow

                objRow.Item("BizDocType") = BizDocType
                objRow.Item("numBizDocTypeID") = BizDocTypeID
                objRow.Item("numRangeFrom") = RangeFrom
                objRow.Item("numRangeTo") = RangeTo
                objRow.Item("Range") = CType(RangeFrom, String) & " to " & CType(RangeTo, String)

                If BizDocCreated = 1 Then
                    strCondition = "The moment BizDoc is created "
                End If

                If BizDocPaidFull = 1 Then
                    strCondition = strCondition & "When the BizDoc has been paid in full "
                End If

                strCondition = strCondition & "AND create an action item with the following type value " & ActionType
                strCondition = strCondition & " for each approver (requesting approval or declination)."

                If ApproveDocStatusID > 0 Then
                    strCondition = strCondition & " And  WHEN ALL the approvers have approved the BizDoc (and none have declined) change the BiizDoc Status value to " _
                    & """" & BizDocApprovalStatus & "."""
                End If
                If DeclainDocStatusID > 0 Then
                    strCondition = strCondition & " If any approver decline the BizDoc change the status value to " _
                    & """" & BizDocDeclainStatus & "."""
                End If

                objRow.Item("Condition") = strCondition
                objRow.Item("Employee") = SelectedEmployee
                objRow.Item("btBizDocCreated") = BizDocCreated
                objRow.Item("btBizDocPaidFull") = BizDocPaidFull
                objRow.Item("Action Type") = ActionType
                objRow.Item("Approve Status") = BizDocApprovalStatus
                objRow.Item("Decline Status") = BizDocDeclainStatus
                objRow.Item("numBizDocTypeID") = BizDocTypeID
                objRow.Item("numActionTypeID") = ActionTypeID
                objRow.Item("numApproveDocStatusID") = ApproveDocStatusID
                objRow.Item("numDeclainDocStatusID") = DeclainDocStatusID
                objRow.Item("EmployeeID") = SelectedEmployeeID
                objRow.Item("numBizDocAppID") = 0
                _BizDocRuleDt.Rows.Add(objRow)
                _BizDocRuleDt.AcceptChanges()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function DeleteBizDocRule() As Integer
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                DeleteBizDocRule = 0

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(1).Value = Nothing
                arparms(1).Direction = ParameterDirection.InputOutput

                Dim ds As New DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_DeleteBizDocApproval", arparms)
                If ds.Tables(0).Rows.Count > 0 Then
                    DeleteBizDocRule = 1
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocRuleDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(1).Value = Nothing
                arparms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocRule", arparms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ValidationRulesBizDoc() As Boolean
            Try
                ValidationRulesBizDoc = True
                Dim objRow As DataRow
                Dim intLoop As Integer

                ValidationRulesBizDoc = True

                '            objRow = BizDocRuleDt.Select("EmployeeID='" & SelectedEmployeeID & "' AND numBizDocTypeID=" & BizDocTypeID & " AND (numRangeFrom>=" & RangeFrom & " OR numRangeTo <=" & RangeFrom & ") AND ( numRangeFrom<=" & RangeTo & " OR numRangeTo >=" & RangeTo & ")")
                'objRow = BizDocRuleDt.Select("numBizDocTypeID=" & BizDocTypeID & " AND ((numRangeFrom >= " & RangeFrom & " OR  numRangeTo >=" & RangeTo & ") AND (numRangeFrom >= " & RangeTo & " OR  numRangeTo >= " & RangeTo & "))")
                'If objRow.Length > 0 Then ValidationRulesBizDoc = False
                For Each objRow In BizDocRuleDt.Rows
                    If CType(objRow("numBizDocTypeID"), Integer) = BizDocTypeID Then

                        If RangeFrom <= CType(objRow("numRangeFrom"), Double) And RangeTo >= CType(objRow("numRangeFrom"), Double) Then
                            ValidationRulesBizDoc = False
                            Exit Function
                        ElseIf RangeFrom >= CType(objRow("numRangeFrom"), Double) And RangeTo <= CType(objRow("numRangeTo"), Double) Then
                            ValidationRulesBizDoc = False
                            Exit Function
                        ElseIf RangeFrom <= CType(objRow("numRangeTo"), Double) And RangeTo >= CType(objRow("numRangeTo"), Double) Then
                            ValidationRulesBizDoc = False
                            Exit Function
                        End If
                    End If
                Next


                Exit Function


            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteBizDocStatus() As Boolean
            Try
                DeleteBizDocStatus = False

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@numBizDocTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = _BizDocTypeID

                arparms(2) = New Npgsql.NpgsqlParameter("@numEmployeeId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(2).Value = _EmployeeID

                arparms(3) = New Npgsql.NpgsqlParameter("@numActionTypeId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = _ActionTypeID

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_DeleteDocStatus", arparms)

                If ds.Tables(0).Rows.Count > 0 Then
                    DeleteBizDocStatus = True
                End If

                Exit Function

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteBizDocRuleTran() As Boolean
            Try
                DeleteBizDocRuleTran = False

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(0).Value = DomainId

                arparms(1) = New Npgsql.NpgsqlParameter("@numBizDocTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(1).Value = _BizDocTypeID

                arparms(2) = New Npgsql.NpgsqlParameter("@RangeFrom", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(2).Value = _RangeFrom

                arparms(3) = New Npgsql.NpgsqlParameter("@RangeTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arparms(3).Value = _RangeTo

                arparms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arparms(4).Value = Nothing
                arparms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_DeleteBizDocEmpRule", arparms)

                If ds.Tables(0).Rows.Count > 0 Then
                    DeleteBizDocRuleTran = True
                End If

                Exit Function

            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace