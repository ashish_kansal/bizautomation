﻿Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Opportunities

    Public Class MassSalesFulfillmentBatch
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property BatchID As Long
        Public Property BatchName As String
        Public Property BatchRecords As String
        Public Property Mode As Short

#End Region

#Region "Public Methods"

        Public Function GetAll() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillmentBatch_GetAll", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub Save()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintMode", Mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numBatchID", BatchID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcBatchName", BatchName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcBatchRecords", BatchRecords, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_MassSalesFulfillmentBatch_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class

End Namespace