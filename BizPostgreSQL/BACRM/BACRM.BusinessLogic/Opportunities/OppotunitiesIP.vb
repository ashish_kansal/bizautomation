﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic


Namespace BACRM.BusinessLogic.Opportunities
    Public Class OppotunitiesIP
        Inherits MOpportunity

        Private _CalcAmount As String
        Private _DocCount As Integer
        Private _NoofProjects As Integer
        Private _AssignedToName As String
        Private _ContactIDName As String
        Private _SourceName As String
        Private _SalesorPurTypeName As String
        Private _ConAnalysisName As String
        Private _RecurringIdName As String
        Private _CampaignIDName As String
        Private _MilestoneProgress As String
        Private _numPartner As String
        Private _numPartenerContact As String
        Private _numPartenerContactId As Long
        Private _Phone As String
        Private _vcEmail As String
        Private _PhoneExtension As String
        Private _intReqPOApproved As Integer
        Private _vcReqPOApproved As String
        Public Property Phone() As String
            Get
                Return _Phone
            End Get
            Set(ByVal Value As String)
                _Phone = Value
            End Set
        End Property

        Public Property vcReqPOApproved() As String
            Get
                Return _vcReqPOApproved
            End Get
            Set(ByVal Value As String)
                _vcReqPOApproved = Value
            End Set
        End Property
        Public Property intReqPOApproved() As Integer
            Get
                Return _intReqPOApproved
            End Get
            Set(ByVal Value As Integer)
                _intReqPOApproved = Value
            End Set
        End Property
        Private Property _vcDropShip As String
        Private Property _intDropShip As Integer
        Public Property vcDropShip() As String
            Get
                Return _vcDropShip
            End Get
            Set(ByVal Value As String)
                _vcDropShip = Value
            End Set
        End Property
        Public Property intDropShip() As Integer
            Get
                Return _intDropShip
            End Get
            Set(ByVal Value As Integer)
                _intDropShip = Value
            End Set
        End Property
        Public Property vcEmail() As String
            Get
                Return _vcEmail
            End Get
            Set(ByVal Value As String)
                _vcEmail = Value
            End Set
        End Property
        Public Property PhoneExtension() As String
            Get
                Return _PhoneExtension
            End Get
            Set(ByVal Value As String)
                _PhoneExtension = Value
            End Set
        End Property

        Public Property numPartenerContactId() As Long
            Get
                Return _numPartenerContactId
            End Get
            Set(ByVal Value As Long)
                _numPartenerContactId = Value
            End Set
        End Property
        Public Property numPartner() As String
            Get
                Return _numPartner
            End Get
            Set(ByVal Value As String)
                _numPartner = Value
            End Set
        End Property

        Public Property numPartenerContact() As String
            Get
                Return _numPartenerContact
            End Get
            Set(ByVal Value As String)
                _numPartenerContact = Value
            End Set
        End Property
        Public Property CampaignIDName() As String
            Get
                Return _CampaignIDName
            End Get
            Set(ByVal Value As String)
                _CampaignIDName = Value
            End Set
        End Property

        Public Property RecurringIdName() As String
            Get
                Return _RecurringIdName
            End Get
            Set(ByVal Value As String)
                _RecurringIdName = Value
            End Set
        End Property

        Public Property ConAnalysisName() As String
            Get
                Return _ConAnalysisName
            End Get
            Set(ByVal Value As String)
                _ConAnalysisName = Value
            End Set
        End Property

        Public Property SalesorPurTypeName() As String
            Get
                Return _SalesorPurTypeName
            End Get
            Set(ByVal Value As String)
                _SalesorPurTypeName = Value
            End Set
        End Property

        Public Property SourceName() As String
            Get
                Return _SourceName
            End Get
            Set(ByVal Value As String)
                _SourceName = Value
            End Set
        End Property

        Public Property ContactIDName() As String
            Get
                Return _ContactIDName
            End Get
            Set(ByVal Value As String)
                _ContactIDName = Value
            End Set
        End Property

        Public Property AssignedToName() As String
            Get
                Return _AssignedToName
            End Get
            Set(ByVal Value As String)
                _AssignedToName = Value
            End Set
        End Property

        Public Property NoofProjects() As Integer
            Get
                Return _NoofProjects
            End Get
            Set(ByVal Value As Integer)
                _NoofProjects = Value
            End Set
        End Property

        Public Property DocCount() As Integer
            Get
                Return _DocCount
            End Get
            Set(ByVal Value As Integer)
                _DocCount = Value
            End Set
        End Property

        Public Property CalcAmount() As String
            Get
                Return _CalcAmount
            End Get
            Set(ByVal Value As String)
                _CalcAmount = Value
            End Set
        End Property

        Public Property MilestoneProgress() As String
            Get
                Return _MilestoneProgress
            End Get
            Set(ByVal Value As String)
                _MilestoneProgress = Value
            End Set
        End Property

        Private _OrderStatusName As String
        Public Property OrderStatusName() As String
            Get
                Return _OrderStatusName
            End Get
            Set(ByVal value As String)
                _OrderStatusName = value
            End Set
        End Property
        Private _PercentageCompleteName As String
        Public Property PercentageCompleteName() As String
            Get
                Return _PercentageCompleteName
            End Get
            Set(ByVal value As String)
                _PercentageCompleteName = value
            End Set
        End Property

        Private _DealStatusName As String
        Public Property DealStatusName() As String
            Get
                Return _DealStatusName
            End Get
            Set(ByVal value As String)
                _DealStatusName = value
            End Set
        End Property

        Private _RefOrderNoName As String
        Public Property RefOrderNoName() As String
            Get
                Return _RefOrderNoName
            End Get
            Set(ByVal value As String)
                _RefOrderNoName = value
            End Set
        End Property

        Private _CurrSymbol As String
        Public Property CurrSymbol() As String
            Get
                Return _CurrSymbol
            End Get
            Set(ByVal value As String)
                _CurrSymbol = value
            End Set
        End Property

        Private _ShareRecordWith As String
        Public Property ShareRecordWith() As String
            Get
                Return _ShareRecordWith
            End Get
            Set(ByVal Value As String)
                _ShareRecordWith = Value
            End Set
        End Property

        Private _SignatureType As String
        Public Property SignatureType() As String
            Get
                Return _SignatureType
            End Get
            Set(ByVal Value As String)
                _SignatureType = Value
            End Set
        End Property

        Private _TotalDiscount As Decimal
        Public Property TotalDiscount() As Decimal
            Get
                Return _TotalDiscount
            End Get
            Set(ByVal value As Decimal)
                _TotalDiscount = value
            End Set
        End Property

        Private _vcOrderedShipped As String
        Public Property vcOrderedShipped() As String
            Get
                Return _vcOrderedShipped
            End Get
            Set(ByVal Value As String)
                _vcOrderedShipped = Value
            End Set
        End Property

        Private _vcOrderedReceived As String
        Public Property vcOrderedReceived() As String
            Get
                Return _vcOrderedReceived
            End Get
            Set(ByVal Value As String)
                _vcOrderedReceived = Value
            End Set
        End Property

        Private _bitIsInitalSalesOrder As Boolean
        Public Property bitIsInitalSalesOrder() As Boolean
            Get
                Return _bitIsInitalSalesOrder
            End Get
            Set(ByVal Value As Boolean)
                _bitIsInitalSalesOrder = Value
            End Set
        End Property


        Public Property ReleaseStatusName As String
        Public Property CustomerPO As String
        Public Property EDIStatus As Short
        Public Property EDIStatusName As String
        Public Property ExpectedDate As Date
        Public Property TrackingDetail As String
        Public Property ProjectIDName As String
        Public Property DealWonDate As String

        Public Function OpportunityDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@OpportunityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = Me.OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = Me.DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = Me.ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_OPPDetailsDTLPL", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If Not IsDBNull(dr("numDivisionID")) Then
                            Me.DivisionID = CType(dr("numDivisionID"), Long)
                        End If

                        If Not IsDBNull(dr("VcPoppName")) Then
                            Me.OpportunityName = CType(dr("VcPoppName"), String)
                        End If
                        If Not IsDBNull(dr("numAssignedTo")) Then
                            Me.AssignedTo = CType(dr("numAssignedTo"), Long)
                        End If
                        If Not IsDBNull(dr("numContactId")) Then
                            Me.ContactID = CType(dr("numContactId"), Long)
                        End If
                        If Not IsDBNull(dr("intpEstimatedCloseDate")) Then
                            Me.EstimatedCloseDate = CType(dr("intpEstimatedCloseDate"), Date)
                        End If
                        If Not IsDBNull(dr("tintSource")) Then
                            Me.Source = CType(dr("tintSource"), Long)
                        End If
                        If Not IsDBNull(dr("numSalesOrPurType")) Then
                            Me.SalesorPurType = CType(dr("numSalesOrPurType"), Long)
                        End If
                        If Not IsDBNull(dr("lngPConclAnalysis")) Then
                            Me.ConAnalysis = CType(dr("lngPConclAnalysis"), Long)
                        End If
                        If Not IsDBNull(dr("tintActive")) Then
                            Me.Active = CType(dr("tintActive"), Boolean)
                        End If
                        If Not IsDBNull(dr("CalAmount")) Then
                            Me.CalcAmount = ReturnMoney(dr("CalAmount"))
                        End If
                        If Not IsDBNull(dr("monPAmount")) Then
                            Me.Amount = CType(dr("monPAmount"), Decimal)
                        End If
                        If Not IsDBNull(dr("DocumentCount")) Then
                            _DocCount = CType(dr("DocumentCount"), Integer)
                        End If
                        If Not IsDBNull(dr("NoOfProjects")) Then
                            _NoofProjects = CType(dr("NoOfProjects"), Integer)
                        End If
                        If Not IsDBNull(dr("NoOfProjects")) Then
                            _NoofProjects = CType(dr("NoOfProjects"), Integer)
                        End If
                        If Not IsDBNull(dr("numCampainID")) Then
                            Me.CampaignID = CType(dr("numCampainID"), Long)
                        End If
                        If Not IsDBNull(dr("numRecurringId")) Then
                            Me.RecurringId = CType(dr("numRecurringId"), Long)
                        End If
                        If Not IsDBNull(dr("numAssignedToName")) Then
                            Me.AssignedToName = CType(dr("numAssignedToName"), String)
                        End If
                        If Not IsDBNull(dr("numContactIdName")) Then
                            Me.ContactIDName = CType(dr("numContactIdName"), String)
                        End If
                        If Not IsDBNull(dr("tintSourceName")) Then
                            Me.SourceName = CType(dr("tintSourceName"), String)
                        End If
                        If Not IsDBNull(dr("numSalesOrPurTypeName")) Then
                            Me.SalesorPurTypeName = CType(dr("numSalesOrPurTypeName"), String)
                        End If
                        If Not IsDBNull(dr("lngPConclAnalysisName")) Then
                            Me.ConAnalysisName = CType(dr("lngPConclAnalysisName"), String)
                        End If
                        If Not IsDBNull(dr("numRecurringIdName")) Then
                            Me.RecurringIdName = CType(dr("numRecurringIdName"), String)
                        End If
                        If Not IsDBNull(dr("numCampainIDName")) Then
                            Me.CampaignIDName = CType(dr("numCampainIDName"), String)
                        End If
                        If Not IsDBNull(dr("txtComments")) Then
                            Me.OppComments = CType(dr("txtComments"), String)
                        End If
                        If Not IsDBNull(dr("TProgress")) Then
                            _MilestoneProgress = CType(dr("TProgress"), String)
                        End If
                        If Not IsDBNull(dr("numStatus")) Then
                            Me.OrderStatus = CType(dr("numStatus"), Long)
                        End If
                        If Not IsDBNull(dr("numStatusName")) Then
                            _OrderStatusName = CType(dr("numStatusName"), String)
                        End If
                        If Not IsDBNull(dr("tintOppStatus")) Then
                            Me.DealStatus = CType(dr("tintOppStatus"), Long)
                        End If
                        If Not IsDBNull(dr("vcDealStatus")) Then
                            Me.DealStatusName = CType(dr("vcDealStatus"), String)
                        End If
                        If Not IsDBNull(dr("vcOppRefOrderNo")) Then
                            Me.OppRefOrderNo = CType(dr("vcOppRefOrderNo"), String)
                        End If
                        'If Not IsDBNull(dr("vcRefOrderNo")) Then
                        '    Me.RefOrderNoName = CType(dr("vcRefOrderNo"), String)
                        'End If
                        If Not IsDBNull(dr("varCurrSymbol")) Then
                            Me.CurrSymbol = CType(dr("varCurrSymbol"), String)
                        End If
                        If Not IsDBNull(dr("numShareWith")) Then
                            Me.ShareRecordWith = CType(dr("numShareWith"), String)
                        End If
                        If Not IsDBNull(dr("monDealAmount")) Then
                            Me.InvoiceGrandTotal = ReturnMoney(dr("monDealAmount"))
                        End If
                        If Not IsDBNull(dr("monAmountPaid")) Then
                            Me.TotalAmountPaid = ReturnMoney(dr("monAmountPaid"))
                        End If

                        If Not IsDBNull(dr("tintTaxOperator")) Then
                            Me.TaxOperator = CType(dr("tintTaxOperator"), Short)
                        End If
                        If Not IsDBNull(dr("numBillingDays")) Then
                            Me.BillingDays = CType(dr("numBillingDays"), Integer)
                        End If
                        If Not IsDBNull(dr("bitInterestType")) Then
                            Me.boolInterestType = CType(dr("bitInterestType"), Boolean)
                        End If
                        If Not IsDBNull(dr("fltInterest")) Then
                            Me.Interest = ReturnMoney(dr("fltInterest"))
                        End If
                        If Not IsDBNull(dr("fltDiscount")) Then
                            Me.Discount = ReturnMoney(dr("fltDiscount"))
                        End If
                        If Not IsDBNull(dr("bitDiscountType")) Then
                            Me.boolDiscType = CType(dr("bitDiscountType"), Boolean)
                        End If
                        If Not IsDBNull(dr("bitBillingTerms")) Then
                            Me.boolBillingTerms = CType(dr("bitBillingTerms"), Boolean)
                        End If
                        If Not IsDBNull(dr("BillingAddress")) Then
                            Me.BillingAddress = CType(dr("BillingAddress"), String)
                        End If
                        If Not IsDBNull(dr("ShippingAddress")) Then
                            Me.ShippingAddress = CType(dr("ShippingAddress"), String)
                        End If
                        If Not IsDBNull(dr("numDiscountAcntType")) Then
                            Me.DiscountAcntType = CType(dr("numDiscountAcntType"), Long)
                        End If
                        If Not IsDBNull(dr("tintSourceType")) Then
                            Me.SourceType = CType(dr("tintSourceType"), Long)
                        End If
                        If Not IsDBNull(dr("vcTrackingNo")) Then
                            Me.TrackingNo = CType(dr("vcTrackingNo"), String)
                        End If
                        If Not IsDBNull(dr("vcMarketplaceOrderID")) Then
                            Me.MarketplaceOrderID = CType(dr("vcMarketplaceOrderID"), String)
                        End If
                        If Not IsDBNull(dr("numPercentageComplete")) Then
                            Me.PercentageComplete = CType(dr("numPercentageComplete"), Long)
                        End If
                        If Not IsDBNull(dr("PercentageCompleteName")) Then
                            Me.PercentageCompleteName = CType(dr("PercentageCompleteName"), String)
                        End If
                        If Not IsDBNull(dr("numBusinessProcessID")) Then
                            Me.SalesProcessId = CType(dr("numBusinessProcessID"), String)
                        End If
                        If Not IsDBNull(dr("vcInventoryStatus")) Then
                            Me.InventoryStatus = CType(dr("vcInventoryStatus"), String)
                        End If

                        If Not IsDBNull(dr("bitUseShippersAccountNo")) Then
                            Me.UseShippersAccountNo = CType(dr("bitUseShippersAccountNo"), Boolean)
                        End If

                        If Not IsDBNull(dr("numTotalDiscount")) Then
                            Me.TotalDiscount = ReturnMoney(dr("numTotalDiscount"))
                        End If

                        If Not IsDBNull(dr("vcOrderedShipped")) Then
                            Me.vcOrderedShipped = CType(dr("vcOrderedShipped"), String)
                        End If

                        If Not IsDBNull(dr("vcOrderedReceived")) Then
                            Me.vcOrderedReceived = CType(dr("vcOrderedReceived"), String)
                        End If

                        If Not IsDBNull(dr("numPartner")) Then
                            Me.numPartner = CType(dr("numPartner"), String)
                        End If

                        If Not IsDBNull(dr("numPartnerId")) Then
                            Me.numPartnerId = CType(dr("numPartnerId"), Long)
                        End If

                        If Not IsDBNull(dr("dtReleaseDate")) Then
                            Me.ReleaseDate = CType(dr("dtReleaseDate"), Date?)
                        End If

                        If Not IsDBNull(dr("numReleaseStatus")) Then
                            Me.ReleaseStatus = CType(dr("numReleaseStatus"), Short)
                        End If

                        If Not IsDBNull(dr("numReleaseStatusName")) Then
                            Me.ReleaseStatusName = CType(dr("numReleaseStatusName"), String)
                        End If

                        If Not IsDBNull(dr("bitIsInitalSalesOrder")) Then
                            Me.bitIsInitalSalesOrder = CType(dr("bitIsInitalSalesOrder"), Boolean)
                        End If
                        If Not IsDBNull(dr("numPartenerContact")) Then
                            Me.numPartenerContact = CType(dr("numPartenerContact"), String)
                        End If
                        If Not IsDBNull(dr("numPartenerContactId")) Then
                            Me.numPartenerContactId = CType(dr("numPartenerContactId"), Long)
                        End If
                        If Not IsDBNull(dr("numShipmentMethod")) Then
                            Me.ShipmentMethod = CType(dr("numShipmentMethod"), Long)
                        End If
                        If Not IsDBNull(dr("vcShipmentMethod")) Then
                            Me.ShipmentMethodName = CType(dr("vcShipmentMethod"), String)
                        End If
                        If Not IsDBNull(dr("intUsedShippingCompany")) Then
                            Me.ShippingCompany = CType(dr("intUsedShippingCompany"), Long)
                            Me.intUsedShippingCompany = CType(dr("intUsedShippingCompany"), Long)
                        End If
                        If Not IsDBNull(dr("vcShippingCompany")) Then
                            Me.ShippingCompanyName = CType(dr("vcShippingCompany"), String)
                        End If
                        If Not IsDBNull(dr("numShippingService")) Then
                            Me.ShippingService = CType(dr("numShippingService"), Long)
                        End If
                        If Not IsDBNull(dr("vcShippingService")) Then
                            Me.ShippingServiceName = CType(dr("vcShippingService"), String)
                        End If
                        If Not IsDBNull(dr("vcSignatureType")) Then
                            Me.SignatureTypeName = CType(dr("vcSignatureType"), String)
                        End If
                        If Not IsDBNull(dr("tintOppType")) Then
                            Me.OppType = CType(dr("tintOppType"), Integer)
                        End If
                        If Not IsDBNull(dr("vcCustomerPO#")) Then
                            Me.CustomerPO = CType(dr("vcCustomerPO#"), String)
                        End If
                        If Not IsDBNull(dr("tintEDIStatusName")) Then
                            Me.EDIStatusName = CType(dr("tintEDIStatusName"), String)
                        End If
                        If Not IsDBNull(dr("vcShipState")) Then
                            Me.ShipStateName = CType(dr("vcShipState"), String)
                        End If

                        If Not IsDBNull(dr("vcEmail")) Then
                            Me.vcEmail = CType(dr("vcEmail"), String)
                        End If

                        If Not IsDBNull(dr("Phone")) Then
                            Me.Phone = CType(dr("Phone"), String)
                        End If

                        If Not IsDBNull(dr("PhoneExtension")) Then
                            Me.PhoneExtension = CType(dr("PhoneExtension"), String)
                        End If

                        If Not IsDBNull(dr("dtExpectedDate")) Then
                            Me.ExpectedDate = CType(dr("dtExpectedDate"), String)
                        End If

                        If Not IsDBNull(dr("vcTrackingDetail")) Then
                            Me.TrackingDetail = CType(dr("vcTrackingDetail"), String)
                        End If

                        If Not IsDBNull(dr("numProjectID")) Then
                            Me.ProjectID = CType(dr("numProjectID"), Long)
                        End If

                        If Not IsDBNull(dr("numProjectIDName")) Then
                            Me.ProjectIDName = CType(dr("numProjectIDName"), String)
                        End If

                        If Not IsDBNull(dr("bintOppToOrder")) Then
                            Me.DealWonDate = CType(dr("bintOppToOrder"), String)
                        End If

                        If Not IsDBNull(dr("intReqPOApproved")) Then
                            Me.intReqPOApproved = CType(dr("intReqPOApproved"), Integer)
                        End If

                        If Not IsDBNull(dr("vcReqPOApproved")) Then
                            Me.vcReqPOApproved = CType(dr("vcReqPOApproved"), String)
                        End If

                        If Not IsDBNull(dr("intDropShip")) Then
                            Me.intDropShip = CType(dr("intDropShip"), Integer)
                        End If

                        If Not IsDBNull(dr("vcDropShip")) Then
                            Me.vcDropShip = CType(dr("vcDropShip"), String)
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CheckIfItemPurchasedToGlobalLocation() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = Me.DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = Me.OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return Convert.ToBoolean(SqlDAL.ExecuteScalar(connString, "USP_OpportunityMaster_GlobalLocationFulfilled", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetOpportunityAddress(ByVal addressType As Short) As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintAddressType", addressType, NpgsqlTypes.NpgsqlDbType.Smallint)) '1: Billing Address, 2:Shipping Address
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_GetAddress", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub DisassociatePO(ByVal oppItemIds As String)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcOppItemIds", oppItemIds, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_DisassociatePO", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

#Region "Terms Region"

#Region "Properties"

        Private _lngTermsID As Long
        Public Property TermsID() As Long
            Get
                Return _lngTermsID
            End Get
            Set(ByVal value As Long)
                _lngTermsID = value
            End Set
        End Property

        Private _strTermName As String
        Public Property Terms() As String
            Get
                Return _strTermName
            End Get
            Set(ByVal value As String)
                _strTermName = value
            End Set
        End Property

        Private _tintApplyTo As Short
        Public Property ApplyTo() As Short
            Get
                Return _tintApplyTo
            End Get
            Set(ByVal value As Short)
                _tintApplyTo = value
            End Set
        End Property

        Private _dblNetDueInDays As Double
        Public Property NetDueInDays() As Double
            Get
                Return _dblNetDueInDays
            End Get
            Set(ByVal value As Double)
                _dblNetDueInDays = value
            End Set
        End Property

        Private _dblDiscount As Decimal
        Public Property DiscountForTerms() As Decimal
            Get
                Return _dblDiscount
            End Get
            Set(ByVal value As Decimal)
                _dblDiscount = value
            End Set
        End Property


        Private _dblDiscountPaidInDays As Decimal
        Public Property DiscountPaidInDays() As Decimal
            Get
                Return _dblDiscountPaidInDays
            End Get
            Set(ByVal value As Decimal)
                _dblDiscountPaidInDays = value
            End Set
        End Property

        Private _lngListItemID As Long
        Public Property ListItemID() As Long
            Get
                Return _lngListItemID
            End Get
            Set(ByVal value As Long)
                _lngListItemID = value
            End Set
        End Property

        Private _boolActive As Boolean
        Public Property IsActive() As String
            Get
                Return _boolActive
            End Get
            Set(ByVal value As String)
                _boolActive = value
            End Set
        End Property

        Private _dcInterestPercentage As Decimal
        Public Property InterestPercentage() As Decimal
            Get
                Return _dcInterestPercentage
            End Get
            Set(ByVal value As Decimal)
                _dcInterestPercentage = value
            End Set
        End Property

        Private _intInterestPercentageIfPaidAfterDays As Integer
        Public Property InterestPercentageIfPaidAfterDays() As Integer
            Get
                Return _intInterestPercentageIfPaidAfterDays
            End Get
            Set(ByVal value As Integer)
                _intInterestPercentageIfPaidAfterDays = value
            End Set
        End Property

        Private _lngCreatedBy As Long
        Public Property CreatedBy() As Long
            Get
                Return _lngCreatedBy
            End Get
            Set(ByVal value As Long)
                _lngCreatedBy = value
            End Set
        End Property

        Private _dtCreatedDate As DateTime
        Public Property dtCreatedDate() As DateTime
            Get
                Return _dtCreatedDate
            End Get
            Set(ByVal value As DateTime)
                _dtCreatedDate = value
            End Set
        End Property

        Private _lngModifiedBy As Long
        Public Property ModifiedBy() As Long
            Get
                Return _lngModifiedBy
            End Get
            Set(ByVal value As Long)
                _lngModifiedBy = value
            End Set
        End Property

        Private _dtModifiedDate As DateTime
        Public Property dtModifiedDate() As DateTime
            Get
                Return _dtModifiedDate
            End Get
            Set(ByVal value As DateTime)
                _dtModifiedDate = value
            End Set
        End Property

#End Region

#Region "Functions & Methods"

        Public Function SaveTerms() As String
            Dim strResult As String = ""
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numTermsID", _lngTermsID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcTerms", _strTermName, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@tintApplyTo", _tintApplyTo, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numNetDueInDays", _dblNetDueInDays, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numDiscount", _dblDiscount, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numDiscountPaidInDays", _dblDiscountPaidInDays, NpgsqlTypes.NpgsqlDbType.Integer))

                    .Add(SqlDAL.Add_Parameter("@numListItemID", _lngListItemID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitActive", _boolActive, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@numInterestPercentage", _dcInterestPercentage, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numInterestPercentageIfPaidAfterDays", _intInterestPercentageIfPaidAfterDays, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numCreatedBy", _lngCreatedBy, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@dtCreatedDate", _dtCreatedDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@numModifiedBy", _lngModifiedBy, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@dtModifiedDate", _dtModifiedDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                End With

                SqlDAL.ExecuteNonQuery(connString, "Usp_ManageTerms", sqlParams.ToArray())
            Catch ex As Exception
                If ex.Message.ToString.Contains("Term details already exists") Then
                    strResult = ex.Message.ToString
                Else
                    Throw ex
                End If
            End Try

            Return strResult
        End Function

        Public Function GetTerms() As DataTable
            Dim dtResult As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numTermsID", _lngTermsID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintApplyTo", ApplyTo, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dsResult As DataSet
                dsResult = SqlDAL.ExecuteDataset(connString, "Usp_GetBillingTerms", sqlParams.ToArray())
                If dsResult IsNot Nothing AndAlso dsResult.Tables.Count > 0 Then
                    dtResult = dsResult.Tables(0)
                End If

            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Public Function DeleteTerms() As String
            Dim strResult As String = ""
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numTermsID", _lngTermsID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "Usp_DeleteBillingTerms", sqlParams.ToArray())
            Catch ex As Exception
                If ex.Message.ToString.Contains("DEPENDANT") Then
                    strResult = "Dependant records are found. Can not delete term details."
                Else
                    Throw ex
                End If
            End Try
            Return strResult
        End Function

        Public Function DeleteLeadSourceReferringURL() As String
            Dim strResult As String = ""
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numTrackingID", TrackingID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "Usp_DeleteTrackingVisitorsHDR", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
            Return strResult
        End Function

        'Added by Neelam on 02/16/2018 - Fetch and displays document and link name*/
        Public Function GetDocumentFilesLink(ByVal calledFrom As Char) As DataTable
            Dim dtResult As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numRecID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@calledFrom", calledFrom, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dsResult As DataSet
                dsResult = SqlDAL.ExecuteDataset(connString, "USP_GetDocumentFilesLink", sqlParams.ToArray())
                If dsResult IsNot Nothing AndAlso dsResult.Tables.Count > 0 Then
                    dtResult = dsResult.Tables(0)
                    Return dtResult
                Else
                    Return Nothing
                End If

            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try
        End Function
#End Region



#End Region
    End Class
End Namespace

