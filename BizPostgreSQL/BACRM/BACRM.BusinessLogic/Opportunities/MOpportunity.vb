'Created Anoop Jayaraj
Option Explicit On
Option Strict On

Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common
Imports Npgsql

Namespace BACRM.BusinessLogic.Opportunities
    Public Class MOpportunity
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _DomainID As Long
        Private _UserRightType As Long
        Private _UserCntID As Long = 0
        Private _Filter As String = "%"
        Private _ComanyID As Long
        Private _ListName As String
        Private _bitDeleted As Short
        Private _DivisionID As Long
        Private _TerritoryID As Long
        Private _ProType As Short
        Private _ContactID As Long
        Private _ItemCode As Long
        Private _SalesProcessId As Long
        Private _OpportunityId As Long
        Private _OpportunityName As String
        Private _EstimatedCloseDate As Date
        Private _OppComments As String
        Private _PublicFlag As Short
        Private _Source As Long
        Private _Amount As Decimal
        Private _CRMType As Long
        Private _strItems As String = ""
        Private _strMilestone As String = ""
        Private _strSubStage As String = ""
        Private _Mode As Short
        Private _CampaignID As Long
        Private _OppStageID As Long
        Private _SubStageID As Long
        Private _ConAnalysis As Long
        Private _EmailMessage As String
        Private _OppType As Integer
        Private _Active As Boolean
        Private _SalesorPurType As Long
        Private _AssignedTo As Long
        Private _strType As String
        Private _ItemGroup As Long
        Private _Units As Decimal
        Private _UnitsReturned As Long
        Private _UnitPrice As Decimal
        Private _Desc As String
        Private _ItemType As String
        Private _WarehouseItemID As Long
        Private _BillToType As Short
        Private _ShipToType As Short
        Private _BillStreet As String
        Private _BillCity As String
        Private _BillState As Long
        Private _BillPostal As String
        Private _BillCountry As Long
        Private _ShipStreet As String
        Private _ShipCity As String
        Private _ShipState As Long
        Private _ShipPostal As String
        Private _ShipCountry As Long
        Private _ContactRole As Long
        Private _bitPartner As Boolean
        Private _ClientTimeZoneOffset As Integer
        Private _RecurringId As Long
        Private _bitVisible As Boolean
        Private _BillCompanyName As String
        Private _ShipCompanyName As String
        Private _PartNo As String
        Private _VendorCost As Decimal
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _ParentOppID As Long
        Private _Dropship As Boolean
        Private _CurrencyID As Long
        Private _DealStatusEnm As EnmDealStatus
        Private _numCost As Decimal
        Private _tintClickBtn As Integer

        Private _bitSubscribedEmailAlert As Boolean
        Public Property bitSubscribedEmailAlert() As Boolean
            Get
                Return _bitSubscribedEmailAlert
            End Get
            Set(ByVal Value As Boolean)
                _bitSubscribedEmailAlert = Value
            End Set
        End Property
        Enum EnmDealStatus As Short
            DealWon = 1
            DealLost = 2
            DealOpen = 0
        End Enum
        Public Property tintClickBtn As Integer
            Get
                Return _tintClickBtn
            End Get
            Set(ByVal value As Integer)
                _tintClickBtn = value
            End Set
        End Property
        'Public Property DealStatusEnm() As EnmDealStatus
        '    Get
        '        Return _DealStatusEnm
        '    End Get
        '    Set(ByVal value As EnmDealStatus)
        '        _DealStatusEnm = value
        '    End Set
        'End Property

        Private _numPartnerId As Long
        Public Property numPartnerId() As Long
            Get
                Return _numPartnerId
            End Get
            Set(ByVal value As Long)
                _numPartnerId = value
            End Set
        End Property
        Private _numPartenerContactIn As Long
        Public Property numPartenerContactIn() As Long
            Get
                Return _numPartenerContactIn
            End Get
            Set(ByVal value As Long)
                _numPartenerContactIn = value
            End Set
        End Property
        Private _numPartenerContactId As Long
        Public Property numPartenerContactId() As Long
            Get
                Return _numPartenerContactId
            End Get
            Set(ByVal value As Long)
                _numPartenerContactId = value
            End Set
        End Property
        'Private _numPartenerContact As Long
        'Public Property numPartenerContact() As Long
        '    Get
        '        Return _numPartenerContact
        '    End Get
        '    Set(ByVal value As Long)
        '        _numPartenerContact = value
        '    End Set
        'End Property
        Private _ReturnHeaderID As Long
        Public Property ReturnHeaderID() As Long
            Get
                Return _ReturnHeaderID
            End Get
            Set(ByVal value As Long)
                _ReturnHeaderID = value
            End Set
        End Property
        Public Property numCost As Decimal
            Get
                Return _numCost
            End Get
            Set(ByVal value As Decimal)
                _numCost = value
            End Set
        End Property
        Private _IsStockTransfer As Boolean
        Public Property IsStockTransfer() As Boolean
            Get
                Return _IsStockTransfer
            End Get
            Set(ByVal value As Boolean)
                _IsStockTransfer = value
            End Set
        End Property


        Private _ProjectID As Long
        Public Property ProjectID() As Long
            Get
                Return _ProjectID
            End Get
            Set(ByVal value As Long)
                _ProjectID = value
            End Set
        End Property

        Private _MarketplaceOrderID As String
        Public Property MarketplaceOrderID() As String
            Get
                Return _MarketplaceOrderID
            End Get
            Set(ByVal value As String)
                _MarketplaceOrderID = value
            End Set
        End Property

        Private _ShippingServiceName As String
        Public Property ShippingServiceName() As String
            Get
                Return _ShippingServiceName
            End Get
            Set(ByVal value As String)
                _ShippingServiceName = value
            End Set
        End Property

        Private _SignatureTypeName As String
        Public Property SignatureTypeName() As String
            Get
                Return _SignatureTypeName
            End Get
            Set(ByVal Value As String)
                _SignatureTypeName = Value
            End Set
        End Property


        Public Property CurrencyID() As Long
            Get
                Return _CurrencyID
            End Get
            Set(ByVal value As Long)
                _CurrencyID = value
            End Set
        End Property

        Public Property Dropship() As Boolean
            Get
                Return _Dropship
            End Get
            Set(ByVal value As Boolean)
                _Dropship = value
            End Set
        End Property


        Private _ParentBizDocID As Long
        Public Property ParentBizDocID() As Long
            Get
                Return _ParentBizDocID
            End Get
            Set(ByVal value As Long)
                _ParentBizDocID = value
            End Set
        End Property

        Private _ReasonForReturn As Long
        Public Property ReasonForReturn() As Long
            Get
                Return _ReasonForReturn
            End Get
            Set(ByVal value As Long)
                _ReasonForReturn = value
            End Set
        End Property

        Private _ReturnStatus As Long
        Public Property ReturnStatus() As Long
            Get
                Return _ReturnStatus
            End Get
            Set(ByVal value As Long)
                _ReturnStatus = value
            End Set
        End Property


        Private _ReturnID As Long
        Public Property ReturnID() As Long
            Get
                Return _ReturnID
            End Get
            Set(ByVal value As Long)
                _ReturnID = value
            End Set
        End Property

        Private _UserID As Long
        Public Property UserID() As Long
            Get
                Return _UserID
            End Get
            Set(ByVal value As Long)
                _UserID = value
            End Set
        End Property


        Private _Reference As String
        Public Property Reference() As String
            Get
                Return _Reference
            End Get
            Set(ByVal value As String)
                _Reference = value
            End Set
        End Property


        Private _Memo As String
        Public Property Memo() As String
            Get
                Return _Memo
            End Get
            Set(ByVal value As String)
                _Memo = value
            End Set
        End Property

        '       Payment Type
        '1- Purchase Order(While BizDoc) 
        '2- Bill(while Entered from Create Bill)  
        '3-Sales Returns(wihle comes from Sales return Queue)
        Private _PaymentType As Int32
        Public Property PaymentType() As Int32
            Get
                Return _PaymentType
            End Get
            Set(ByVal value As Int32)
                _PaymentType = value
            End Set
        End Property


        Private _PaymentMethod As Int32
        Public Property PaymentMethod() As Int32
            Get
                Return _PaymentMethod
            End Get
            Set(ByVal value As Int32)
                _PaymentMethod = value
            End Set
        End Property


        Private _ReturnDate As Date
        Public Property ReturnDate() As Date
            Get
                Return _ReturnDate
            End Get
            Set(ByVal value As Date)
                _ReturnDate = value
            End Set
        End Property


        Private _SearchText As String
        Public Property SearchText() As String
            Get
                Return _SearchText
            End Get
            Set(ByVal value As String)
                _SearchText = value
            End Set
        End Property

        Private _SortCharacter As Char
        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property


        Private _OppItemCode As Long
        Public Property OppItemCode() As Long
            Get
                Return _OppItemCode
            End Get
            Set(ByVal value As Long)
                _OppItemCode = value
            End Set
        End Property


        Private _bitCheck As Boolean
        Public Property bitCheck() As Boolean
            Get
                Return _bitCheck
            End Get
            Set(ByVal value As Boolean)
                _bitCheck = value
            End Set
        End Property

        Private _OrderStatus As Long
        Public Property OrderStatus() As Long
            Get
                Return _OrderStatus
            End Get
            Set(ByVal value As Long)
                _OrderStatus = value
            End Set
        End Property
        Private _PercentageComplete As Long
        Public Property PercentageComplete() As Long
            Get
                Return _PercentageComplete
            End Get
            Set(ByVal value As Long)
                _PercentageComplete = value
            End Set
        End Property

        'Keep it Long bcz custom field converts it to int.. if short then thrws error
        Private _DealStatus As Long
        Public Property DealStatus() As Long
            Get
                Return _DealStatus
            End Get
            Set(ByVal value As Long)
                _DealStatus = value
            End Set
        End Property


        Private _EmbeddedCostID As Long
        Public Property EmbeddedCostID() As Long
            Get
                Return _EmbeddedCostID
            End Get
            Set(ByVal value As Long)
                _EmbeddedCostID = value
            End Set
        End Property

        Private _CostCategoryID As Long
        Public Property CostCategoryID() As Long
            Get
                Return _CostCategoryID
            End Get
            Set(ByVal value As Long)
                _CostCategoryID = value
            End Set
        End Property

        Private _AccountID As Long
        Public Property AccountID() As Long
            Get
                Return _AccountID
            End Get
            Set(ByVal value As Long)
                _AccountID = value
            End Set
        End Property

        Private _OppBizDocID As Long
        Public Property OppBizDocID() As Long
            Get
                Return _OppBizDocID
            End Get
            Set(ByVal value As Long)
                _OppBizDocID = value
            End Set
        End Property
        Private _WebApiId As Integer

        Public Property WebApiId() As Integer
            Get
                Return _WebApiId
            End Get
            Set(ByVal Value As Integer)
                _WebApiId = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Property ParentOppID() As Long
            Get
                Return _ParentOppID
            End Get
            Set(ByVal value As Long)
                _ParentOppID = value
            End Set
        End Property


        Public Property VendorCost() As Decimal
            Get
                Return _VendorCost
            End Get
            Set(ByVal value As Decimal)
                _VendorCost = value
            End Set
        End Property

        Public Property PartNo() As String
            Get
                Return _PartNo
            End Get
            Set(ByVal value As String)
                _PartNo = value
            End Set
        End Property

        Public Property ShipCompanyName() As String
            Get
                Return _ShipCompanyName
            End Get
            Set(ByVal value As String)
                _ShipCompanyName = value
            End Set
        End Property

        Public Property BillCompanyName() As String
            Get
                Return _BillCompanyName
            End Get
            Set(ByVal value As String)
                _BillCompanyName = value
            End Set
        End Property

        Public Property bitVisible() As Boolean
            Get
                Return _bitVisible
            End Get
            Set(ByVal value As Boolean)
                _bitVisible = value
            End Set
        End Property

        Public Property RecurringId() As Long
            Get
                Return _RecurringId
            End Get
            Set(ByVal Value As Long)
                _RecurringId = Value
            End Set
        End Property
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public Property bitPartner() As Boolean
            Get
                Return _bitPartner
            End Get
            Set(ByVal Value As Boolean)
                _bitPartner = Value
            End Set
        End Property

        Public Property ContactRole() As Long
            Get
                Return _ContactRole
            End Get
            Set(ByVal Value As Long)
                _ContactRole = Value
            End Set
        End Property

        Public Property ShipCountry() As Long
            Get
                Return _ShipCountry
            End Get
            Set(ByVal Value As Long)
                _ShipCountry = Value
            End Set
        End Property

        Public Property ShipPostal() As String
            Get
                Return _ShipPostal
            End Get
            Set(ByVal Value As String)
                _ShipPostal = Value
            End Set
        End Property

        Public Property ShipState() As Long
            Get
                Return _ShipState
            End Get
            Set(ByVal Value As Long)
                _ShipState = Value
            End Set
        End Property

        Public Property ShipStateName As String

        Public Property ShipCity() As String
            Get
                Return _ShipCity
            End Get
            Set(ByVal Value As String)
                _ShipCity = Value
            End Set
        End Property

        Public Property ShipStreet() As String
            Get
                Return _ShipStreet
            End Get
            Set(ByVal Value As String)
                _ShipStreet = Value
            End Set
        End Property

        Public Property BillCountry() As Long
            Get
                Return _BillCountry
            End Get
            Set(ByVal Value As Long)
                _BillCountry = Value
            End Set
        End Property

        Public Property BillPostal() As String
            Get
                Return _BillPostal
            End Get
            Set(ByVal Value As String)
                _BillPostal = Value
            End Set
        End Property

        Public Property BillState() As Long
            Get
                Return _BillState
            End Get
            Set(ByVal Value As Long)
                _BillState = Value
            End Set
        End Property

        Public Property BillCity() As String
            Get
                Return _BillCity
            End Get
            Set(ByVal Value As String)
                _BillCity = Value
            End Set
        End Property

        Public Property BillStreet() As String
            Get
                Return _BillStreet
            End Get
            Set(ByVal Value As String)
                _BillStreet = Value
            End Set
        End Property

        Public Property ShipToType() As Short
            Get
                Return _ShipToType
            End Get
            Set(ByVal Value As Short)
                _ShipToType = Value
            End Set
        End Property

        Public Property BillToType() As Short
            Get
                Return _BillToType
            End Get
            Set(ByVal Value As Short)
                _BillToType = Value
            End Set
        End Property

        Public Property WarehouseItemID() As Long
            Get
                Return _WarehouseItemID
            End Get
            Set(ByVal Value As Long)
                _WarehouseItemID = Value
            End Set
        End Property

        Public Property ItemType() As String
            Get
                Return _ItemType
            End Get
            Set(ByVal Value As String)
                _ItemType = Value
            End Set
        End Property

        Public Property Desc() As String
            Get
                Return _Desc
            End Get
            Set(ByVal Value As String)
                _Desc = Value
            End Set
        End Property

        Public Property UnitPrice() As Decimal
            Get
                Return _UnitPrice
            End Get
            Set(ByVal Value As Decimal)
                _UnitPrice = Value
            End Set
        End Property

        Public Property Units() As Decimal
            Get
                Return _Units
            End Get
            Set(ByVal Value As Decimal)
                _Units = Value
            End Set
        End Property

        Public Property UnitsReturned() As Long
            Get
                Return _UnitsReturned
            End Get
            Set(ByVal Value As Long)
                _UnitsReturned = Value
            End Set
        End Property

        Public Property ItemGroup() As Long
            Get
                Return _ItemGroup
            End Get
            Set(ByVal Value As Long)
                _ItemGroup = Value
            End Set
        End Property

        Public Property strType() As String
            Get
                Return _strType
            End Get
            Set(ByVal Value As String)
                _strType = Value
            End Set
        End Property


        Public Property AssignedTo() As Long
            Get
                Return _AssignedTo
            End Get
            Set(ByVal Value As Long)
                _AssignedTo = Value
            End Set
        End Property


        Public Property SalesorPurType() As Long
            Get
                Return _SalesorPurType
            End Get
            Set(ByVal Value As Long)
                _SalesorPurType = Value
            End Set
        End Property


        Public Property Active() As Boolean
            Get
                Return _Active
            End Get
            Set(ByVal Value As Boolean)
                _Active = Value
            End Set
        End Property



        Public Property OppType() As Integer
            Get
                Return _OppType
            End Get
            Set(ByVal Value As Integer)
                _OppType = Value
            End Set
        End Property



        Public Property EmailMessage() As String
            Get
                Return _EmailMessage
            End Get
            Set(ByVal Value As String)
                _EmailMessage = Value
            End Set
        End Property

        Public Property ConAnalysis() As Long
            Get
                Return _ConAnalysis
            End Get
            Set(ByVal Value As Long)
                _ConAnalysis = Value
            End Set
        End Property

        Public Property SubStageID() As Long
            Get
                Return _SubStageID
            End Get
            Set(ByVal Value As Long)
                _SubStageID = Value
            End Set
        End Property

        Public Property OppStageID() As Long
            Get
                Return _OppStageID
            End Get
            Set(ByVal Value As Long)
                _OppStageID = Value
            End Set
        End Property

        Public Property strSubStage() As String
            Get
                Return _strSubStage
            End Get
            Set(ByVal Value As String)
                _strSubStage = Value
            End Set
        End Property

        Public Property CampaignID() As Long
            Get
                Return _CampaignID
            End Get
            Set(ByVal Value As Long)
                _CampaignID = Value
            End Set
        End Property

        Public Property Mode() As Short
            Get
                Return _Mode
            End Get
            Set(ByVal Value As Short)
                _Mode = Value
            End Set
        End Property

        Public Property strItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal Value As String)
                _strItems = Value
            End Set
        End Property

        Public Property strMilestone() As String
            Get
                Return _strMilestone
            End Get
            Set(ByVal Value As String)
                _strMilestone = Value
            End Set
        End Property



        Public Property CRMType() As Long
            Get
                Return _CRMType
            End Get
            Set(ByVal Value As Long)
                _CRMType = Value
            End Set
        End Property

        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal Value As Decimal)
                _Amount = Value
            End Set
        End Property

        Public Property Source() As Long
            Get
                Return _Source
            End Get
            Set(ByVal Value As Long)
                _Source = Value
            End Set
        End Property
        Private _vcSource As String
        Public Property vcSource() As String
            Get
                Return _vcSource
            End Get
            Set(ByVal Value As String)
                _vcSource = Value
            End Set
        End Property

        Public Property PublicFlag() As Short
            Get
                Return _PublicFlag
            End Get
            Set(ByVal Value As Short)
                _PublicFlag = Value
            End Set
        End Property

        Public Property OppComments() As String
            Get
                Return _OppComments
            End Get
            Set(ByVal Value As String)
                _OppComments = Value
            End Set
        End Property

        Public Property EstimatedCloseDate() As Date
            Get
                Return _EstimatedCloseDate
            End Get
            Set(ByVal Value As Date)
                _EstimatedCloseDate = Value
            End Set
        End Property

        Public Property OpportunityName() As String
            Get
                Return _OpportunityName
            End Get
            Set(ByVal Value As String)
                _OpportunityName = Value
            End Set
        End Property

        Public Property OpportunityId() As Long
            Get
                Return _OpportunityId
            End Get
            Set(ByVal Value As Long)
                _OpportunityId = Value
            End Set
        End Property



        Public Property SalesProcessId() As Long
            Get
                Return _SalesProcessId
            End Get
            Set(ByVal Value As Long)
                _SalesProcessId = Value
            End Set
        End Property

        Private _ItemName As String
        Public Property ItemName() As String
            Get
                Return _ItemName
            End Get
            Set(ByVal value As String)
                _ItemName = value
            End Set
        End Property

        Private _ModelName As String
        Public Property ModelName() As String
            Get
                Return _ModelName
            End Get
            Set(ByVal value As String)
                _ModelName = value
            End Set
        End Property

        Private _ItemDesc As String
        Public Property ItemDesc() As String
            Get
                Return _ItemDesc
            End Get
            Set(ByVal value As String)
                _ItemDesc = value
            End Set
        End Property

        Private _ItemThumbImage As String
        Public Property ItemThumbImage() As String
            Get
                Return _ItemThumbImage
            End Get
            Set(ByVal value As String)
                _ItemThumbImage = value
            End Set
        End Property

        Public Property ItemCode() As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal Value As Long)
                _ItemCode = Value
            End Set
        End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property ProType() As Short
            Get
                Return _ProType
            End Get
            Set(ByVal Value As Short)
                _ProType = Value
            End Set
        End Property

        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        Public Property bitDeleted() As Short
            Get
                Return _bitDeleted
            End Get
            Set(ByVal Value As Short)
                _bitDeleted = Value
            End Set
        End Property

        Public Property ListName() As String
            Get
                Return _ListName
            End Get
            Set(ByVal Value As String)
                _ListName = Value
            End Set
        End Property

        Public Property CompanyID() As Long
            Get
                Return _ComanyID
            End Get
            Set(ByVal Value As Long)
                _ComanyID = Value
            End Set
        End Property

        Public Property Filter() As String
            Get
                Return _Filter
            End Get
            Set(ByVal Value As String)
                _Filter = Value
            End Set
        End Property

        Public Property UserCntID() As Long
            Get
                Return _UserCntID
            End Get
            Set(ByVal Value As Long)
                _UserCntID = Value
            End Set
        End Property

        Public Property UserRightType() As Long
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Long)
                _UserRightType = Value
            End Set
        End Property

        Public Property DomainID() As Long
            Get
                Return _DomainID
            End Get
            Set(ByVal Value As Long)
                _DomainID = Value
            End Set
        End Property

        Private _OppRefOrderNo As String
        Public Property OppRefOrderNo() As String
            Get
                Return _OppRefOrderNo
            End Get
            Set(ByVal value As String)
                _OppRefOrderNo = value
            End Set
        End Property

        Private _WebApiOrderNo As String
        Public Property WebApiOrderNo() As String
            Get
                Return _WebApiOrderNo
            End Get
            Set(ByVal value As String)
                _WebApiOrderNo = value
            End Set
        End Property



        Private _numUOM As String
        Public Property numUOM() As String
            Get
                Return _numUOM
            End Get
            Set(ByVal value As String)
                _numUOM = value
            End Set
        End Property
        Private _QtyShipped As Double

        Public Property QtyShipped() As Double
            Get
                Return _QtyShipped
            End Get
            Set(ByVal Value As Double)
                _QtyShipped = Value
            End Set
        End Property
        Private _QtyReceived As Double

        Public Property QtyReceived() As Double
            Get
                Return _QtyReceived
            End Get
            Set(ByVal Value As Double)
                _QtyReceived = Value
            End Set
        End Property

        Private _ItemClassification As Long

        Public Property ItemClassification() As Long
            Get
                Return _ItemClassification
            End Get
            Set(ByVal Value As Long)
                _ItemClassification = Value
            End Set
        End Property


        Private _BillToAddressID As Long
        Public Property BillToAddressID() As Long
            Get
                Return _BillToAddressID
            End Get
            Set(ByVal value As Long)
                _BillToAddressID = value
            End Set
        End Property

        Private _ShipToAddressID As Long
        Public Property ShipToAddressID() As Long
            Get
                Return _ShipToAddressID
            End Get
            Set(ByVal value As Long)
                _ShipToAddressID = value
            End Set
        End Property

        Private _AddressName As String
        Public Property AddressName() As String
            Get
                Return _AddressName
            End Get
            Set(ByVal value As String)
                _AddressName = value
            End Set
        End Property

        Private _ClassID As Long
        Public Property ClassID() As Long
            Get
                Return _ClassID
            End Get
            Set(ByVal value As Long)
                _ClassID = value
            End Set
        End Property

        Private _Manufacturer As String
        Public Property Manufacturer() As String
            Get
                Return _Manufacturer
            End Get
            Set(ByVal value As String)
                _Manufacturer = value
            End Set
        End Property
        Private _IsUnit As Boolean
        Public Property IsUnit As Boolean
            Get
                Return _IsUnit
            End Get
            Set(ByVal value As Boolean)
                _IsUnit = value
            End Set
        End Property

        Private _InvoiceGrandTotal As Decimal
        Public Property InvoiceGrandTotal() As Decimal
            Get
                Return _InvoiceGrandTotal
            End Get
            Set(ByVal Value As Decimal)
                _InvoiceGrandTotal = Value
            End Set
        End Property

        Private _TotalAmountPaid As Decimal
        Public Property TotalAmountPaid() As Decimal
            Get
                Return _TotalAmountPaid
            End Get
            Set(ByVal Value As Decimal)
                _TotalAmountPaid = Value
            End Set
        End Property

        Private _SourceType As Short
        Public Property SourceType() As Short
            Get
                Return _SourceType
            End Get
            Set(ByVal Value As Short)
                _SourceType = Value
            End Set
        End Property

        Private _Discount As Decimal
        Public Property Discount() As Decimal
            Get
                Return _Discount
            End Get
            Set(ByVal Value As Decimal)
                _Discount = Value
            End Set
        End Property

        Private _boolDiscType As Boolean
        Public Property boolDiscType() As Boolean
            Get
                Return _boolDiscType
            End Get
            Set(ByVal value As Boolean)
                _boolDiscType = value
            End Set
        End Property

        Private _boolBillingTerms As Boolean
        Public Property boolBillingTerms() As Boolean
            Get
                Return _boolBillingTerms
            End Get
            Set(ByVal value As Boolean)
                _boolBillingTerms = value
            End Set
        End Property

        Private _boolInterestType As Boolean
        Public Property boolInterestType() As Boolean
            Get
                Return _boolInterestType
            End Get
            Set(ByVal value As Boolean)
                _boolInterestType = value
            End Set
        End Property

        Private _BillingDays As Integer
        Public Property BillingDays() As Integer
            Get
                Return _BillingDays
            End Get
            Set(ByVal value As Integer)
                _BillingDays = value
            End Set
        End Property

        Private _Interest As Decimal
        Public Property Interest() As Decimal
            Get
                Return _Interest
            End Get
            Set(ByVal value As Decimal)
                _Interest = value
            End Set
        End Property

        Private _TaxOperator As Short
        Public Property TaxOperator() As Short
            Get
                Return _TaxOperator
            End Get
            Set(ByVal Value As Short)
                _TaxOperator = Value
            End Set
        End Property

        Private _BillingAddress As String
        Public Property BillingAddress() As String
            Get
                Return _BillingAddress
            End Get
            Set(ByVal value As String)
                _BillingAddress = value
            End Set
        End Property

        Private _ShippingAddress As String
        Public Property ShippingAddress() As String
            Get
                Return _ShippingAddress
            End Get
            Set(ByVal value As String)
                _ShippingAddress = value
            End Set
        End Property

        Private _DiscountAcntType As Long
        Public Property DiscountAcntType() As Long
            Get
                Return _DiscountAcntType
            End Get
            Set(ByVal value As Long)
                _DiscountAcntType = value
            End Set
        End Property

        Private _ShipCost As Decimal
        Public Property ShipCost() As Decimal
            Get
                Return _ShipCost
            End Get
            Set(ByVal value As Decimal)
                _ShipCost = value
            End Set
        End Property

        Private _fltExchageRate As Decimal
        Public Property fltExchageRate() As Decimal
            Get
                Return _fltExchageRate
            End Get
            Set(ByVal value As Decimal)
                _fltExchageRate = value
            End Set
        End Property

        Private _TrackingNo As String
        Public Property TrackingNo() As String
            Get
                Return _TrackingNo
            End Get
            Set(ByVal Value As String)
                _TrackingNo = Value
            End Set
        End Property

        Private _TrackingID As Long
        Public Property TrackingID() As Long
            Get
                Return _TrackingID
            End Get
            Set(ByVal Value As Long)
                _TrackingID = Value
            End Set
        End Property

        Private _BizDocStatus As Long
        Public Property BizDocStatus() As Long
            Get
                Return _BizDocStatus
            End Get
            Set(ByVal value As Long)
                _BizDocStatus = value
            End Set
        End Property

        Private _strCouponCode As String
        Public Property CouponCode() As String
            Get
                Return _strCouponCode
            End Get
            Set(ByVal value As String)
                _strCouponCode = value
            End Set
        End Property

        Private _dtItemReceivedDate As DateTime
        Public Property dtItemReceivedDate() As DateTime
            Get
                Return _dtItemReceivedDate
            End Get
            Set(ByVal value As DateTime)
                _dtItemReceivedDate = value
            End Set
        End Property

        Private _MarketplaceOrderDate As DateTime
        Public Property MarketplaceOrderDate() As DateTime
            Get
                Return _MarketplaceOrderDate
            End Get
            Set(ByVal value As DateTime)
                _MarketplaceOrderDate = value
            End Set
        End Property

        Private _MarketplaceOrderReportId As String
        Public Property MarketplaceOrderReportId() As String
            Get
                Return _MarketplaceOrderReportId
            End Get
            Set(ByVal value As String)
                _MarketplaceOrderReportId = value
            End Set
        End Property

        Private _InventoryStatus As String
        Public Property InventoryStatus() As String
            Get
                Return _InventoryStatus
            End Get
            Set(ByVal value As String)
                _InventoryStatus = value
            End Set
        End Property

        Private _bitUseShippersAccountNo As Boolean
        Public Property UseShippersAccountNo() As Boolean
            Get
                Return _bitUseShippersAccountNo
            End Get
            Set(ByVal value As Boolean)
                _bitUseShippersAccountNo = value
            End Set
        End Property
        Private Property _numShipmentMethod As Integer
        Public Property ShipmentMethod() As Integer
            Get
                Return _numShipmentMethod
            End Get
            Set(value As Integer)
                _numShipmentMethod = value
            End Set
        End Property

        Public Property ShippingCompany As Long
        Public Property ShippingCompanyName As String
        Public Property ShippingService As Long
        Public Property ShipmentMethodName() As String

        Private _bitUseMarkupShippingRate As Boolean
        Public Property UseMarkupShippingRate() As Boolean
            Get
                Return _bitUseMarkupShippingRate
            End Get
            Set(ByVal value As Boolean)
                _bitUseMarkupShippingRate = value
            End Set
        End Property

        Private _dcMarkupShippingRate As Decimal
        Public Property dcMarkupShippingRate() As Decimal
            Get
                Return _dcMarkupShippingRate
            End Get
            Set(ByVal value As Decimal)
                _dcMarkupShippingRate = value
            End Set
        End Property

        Private _intUsedShippingCompany As Integer
        Public Property intUsedShippingCompany() As Integer
            Get
                Return _intUsedShippingCompany
            End Get
            Set(ByVal value As Integer)
                _intUsedShippingCompany = value
            End Set
        End Property

        Private _numRecurConfigID As Long
        Public Property RecurConfigID() As Long
            Get
                Return _numRecurConfigID
            End Get
            Set(ByVal value As Long)
                _numRecurConfigID = value
            End Set
        End Property

        Private _bitDisable As Boolean
        Public Property DisableRecur() As Boolean
            Get
                Return _bitDisable
            End Get
            Set(ByVal value As Boolean)
                _bitDisable = value
            End Set
        End Property

        Private _bitRecur As Boolean
        Public Property Recur() As Boolean
            Get
                Return _bitRecur
            End Get
            Set(ByVal value As Boolean)
                _bitRecur = value
            End Set
        End Property

        Private _dtStartDate As Date
        Public Property StartDate() As Date
            Get
                Return _dtStartDate
            End Get
            Set(ByVal value As Date)
                _dtStartDate = value
            End Set
        End Property

        Private _dtEndDate As Date
        Public Property EndDate() As Date
            Get
                Return _dtEndDate
            End Get
            Set(ByVal value As Date)
                _dtEndDate = value
            End Set
        End Property

        Private _numFrequency As Short
        Public Property Frequency() As Short
            Get
                Return _numFrequency
            End Get
            Set(ByVal value As Short)
                _numFrequency = value
            End Set
        End Property

        Private _vcFrequency As String
        Public Property FrequencyName() As String
            Get
                Return _vcFrequency
            End Get
            Set(ByVal value As String)
                _vcFrequency = value
            End Set
        End Property

        Private _vcRecurrenceType As String
        Public Property RecurrenceType() As String
            Get
                Return _vcRecurrenceType
            End Get
            Set(ByVal value As String)
                _vcRecurrenceType = value
            End Set
        End Property

        Private _vcRMASearchText As String
        Public Property RMASearchText() As String
            Get
                Return _vcRMASearchText
            End Get
            Set(ByVal value As String)
                _vcRMASearchText = value
            End Set
        End Property

        Private _vcSortColumn As String
        Public Property SortColumn() As String
            Get
                Return _vcSortColumn
            End Get
            Set(ByVal value As String)
                _vcSortColumn = value
            End Set
        End Property

        Private _intSortDirection As Short
        Public Property SortDirection() As Short
            Get
                Return _intSortDirection
            End Get
            Set(ByVal value As Short)
                _intSortDirection = value
            End Set
        End Property
        Private Property _vcNotes As String
        Public Property Notes() As String
            Get
                Return _vcNotes
            End Get
            Set(value As String)
                _vcNotes = value
            End Set
        End Property

        Private _CustomerPartNo As String
        Public Property CustomerPartNo() As String
            Get
                Return _CustomerPartNo
            End Get
            Set(ByVal value As String)
                _CustomerPartNo = value
            End Set
        End Property

        Private _OppStatus As Int16
        Public Property OppStatus() As Int16
            Get
                Return _OppStatus
            End Get
            Set(value As Int16)
                _OppStatus = value
            End Set
        End Property


        Public Property ReleaseDate As Date?
        Public Property ReleaseStatus As Short
        Public Property WillCallWarehouseID As Long
        Public Property VendorAddressID As Long
        Public Property IsAltContact As Boolean
        Public Property AltContact As String

        Public Property BillingContact As Long
        Public Property IsAltBillingContact As Boolean
        Public Property AltBillingContact As String
        Public Property ShippingContact As Long
        Public Property IsAltShippingContact As Boolean
        Public Property AltShippingContact As String
        Public Property ShipFromLocation As Long
        Public Property Attributes As String
        Public Property AttributeIDs As String

        Private _vcCustomerPO As String
        Public Property vcCustomerPO() As String
            Get
                Return _vcCustomerPO
            End Get
            Set(ByVal Value As String)
                _vcCustomerPO = Value
            End Set
        End Property

        Private _bitDropShipAddress As Boolean = False
        Public Property bitDropShipAddress() As Boolean
            Get
                Return _bitDropShipAddress
            End Get
            Set(ByVal Value As Boolean)
                _bitDropShipAddress = Value
            End Set
        End Property

        Public Property ItemRequiredDate As Date?

        Private _PromotionCouponCode As String
        Public Property PromotionCouponCode() As String
            Get
                Return _PromotionCouponCode
            End Get
            Set(ByVal Value As String)
                _PromotionCouponCode = Value
            End Set
        End Property

        Private _numPromotionId As Long
        Public Property numPromotionId() As Long
            Get
                Return _numPromotionId
            End Get
            Set(ByVal Value As Long)
                _numPromotionId = Value
            End Set
        End Property

        Private _dtExpectedDate As Nullable(Of Date)
        Public Property dtExpectedDate() As Nullable(Of Date)
            Get
                Return _dtExpectedDate
            End Get
            Set(value As Nullable(Of Date))
                _dtExpectedDate = value
            End Set
        End Property


#Region "Constructor"
        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32               
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '**********************************************************************************
        'Public Sub New(ByVal intUserId As Integer)
        '    'Constructor
        '    MyBase.New(intUserId)
        'End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal intUserId As Int32              
        ' Description  : The constructor                
        ' Notes        : N/A                
        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '**********************************************************************************
        Public Sub New()
            'Constructor
            MyBase.New()
        End Sub
#End Region

        Public Function ItemByDomainID() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPByDomainID", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function BusinessProcess() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numProType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _ProType

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPSalesProcess", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ContactInfo() As DataTable
            Try
                Dim ds As DataSet
                Dim dt As DataTable
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@OpportunityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_OPPContactInfo", arParms)
                dt = ds.Tables(0)

                Return dt
                ds.Tables.Remove(dt)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function getPrimaryContactInfoOpp() As DataTable
            Try
                Dim ds As DataSet
                Dim dt As DataTable
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@OpportunityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPrimaryContactOPP", arParms)
                dt = ds.Tables(0)

                Return dt
                ds.Tables.Remove(dt)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function Save() As String()

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(63) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintSource", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _Source

                arParms(4) = New Npgsql.NpgsqlParameter("@vcPOppName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _OpportunityName

                arParms(5) = New Npgsql.NpgsqlParameter("@Comments", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(5).Value = _OppComments

                arParms(6) = New Npgsql.NpgsqlParameter("@bitPublicFlag", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = If(_PublicFlag = 1, True, False)

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@monPAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(8).Value = _Amount

                arParms(9) = New Npgsql.NpgsqlParameter("@numAssignedTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _AssignedTo

                arParms(10) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _DomainID

                arParms(11) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(11).Value = _strItems

                arParms(12) = New Npgsql.NpgsqlParameter("@strMilestone", NpgsqlTypes.NpgsqlDbType.Text)
                _strMilestone = _strMilestone.Replace("false", "0")
                _strMilestone = _strMilestone.Replace("true", "1")
                arParms(12).Value = _strMilestone

                arParms(13) = New Npgsql.NpgsqlParameter("@dtEstimatedCloseDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(13).Value = _EstimatedCloseDate

                arParms(14) = New Npgsql.NpgsqlParameter("@CampaignID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _CampaignID

                arParms(15) = New Npgsql.NpgsqlParameter("@lngPConclAnalysis", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _ConAnalysis

                arParms(16) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(16).Value = _OppType

                arParms(17) = New Npgsql.NpgsqlParameter("@tintActive", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(17).Value = If(_Active, 1, 0)

                arParms(18) = New Npgsql.NpgsqlParameter("@numSalesOrPurType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = _SalesorPurType

                arParms(19) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = _RecurringId

                arParms(20) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(20).Value = _CurrencyID

                arParms(21) = New Npgsql.NpgsqlParameter("@DealStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(21).Value = _DealStatus

                arParms(22) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(22).Value = _OrderStatus

                arParms(23) = New Npgsql.NpgsqlParameter("@vcOppRefOrderNo", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(23).Value = _OppRefOrderNo

                arParms(24) = New Npgsql.NpgsqlParameter("@numBillAddressId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(24).Value = _BillToAddressID

                arParms(25) = New Npgsql.NpgsqlParameter("@numShipAddressId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(25).Value = _ShipToAddressID

                arParms(26) = New Npgsql.NpgsqlParameter("@bitStockTransfer", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(26).Value = _IsStockTransfer

                arParms(27) = New Npgsql.NpgsqlParameter("@WebApiId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(27).Value = _WebApiId

                arParms(28) = New Npgsql.NpgsqlParameter("@tintSourceType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(28).Value = _SourceType

                arParms(29) = New Npgsql.NpgsqlParameter("@bitDiscountType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(29).Value = _boolDiscType

                arParms(30) = New Npgsql.NpgsqlParameter("@fltDiscount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(30).Value = _Discount

                arParms(31) = New Npgsql.NpgsqlParameter("@bitBillingTerms", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(31).Value = _boolBillingTerms

                arParms(32) = New Npgsql.NpgsqlParameter("@intBillingDays", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(32).Value = _BillingDays

                arParms(33) = New Npgsql.NpgsqlParameter("@bitInterestType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(33).Value = _boolInterestType

                arParms(34) = New Npgsql.NpgsqlParameter("@fltInterest", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(34).Value = _Interest

                arParms(35) = New Npgsql.NpgsqlParameter("@tintTaxOperator", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(35).Value = _TaxOperator

                arParms(36) = New Npgsql.NpgsqlParameter("@numDiscountAcntType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(36).Value = _DiscountAcntType

                arParms(37) = New Npgsql.NpgsqlParameter("@vcWebApiOrderNo", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(37).Value = _WebApiOrderNo

                arParms(38) = New Npgsql.NpgsqlParameter("@vcCouponCode", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(38).Value = _strCouponCode

                arParms(39) = New Npgsql.NpgsqlParameter("@vcMarketplaceOrderID", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(39).Value = _MarketplaceOrderID

                arParms(40) = New Npgsql.NpgsqlParameter("@vcMarketplaceOrderDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(40).Value = IIf(_MarketplaceOrderDate = Nothing, DateTime.Now.Date, _MarketplaceOrderDate)

                arParms(41) = New Npgsql.NpgsqlParameter("@vcMarketplaceOrderReportId", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(41).Value = _MarketplaceOrderReportId

                arParms(42) = New Npgsql.NpgsqlParameter("@numPercentageComplete", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(42).Value = _PercentageComplete

                arParms(43) = New Npgsql.NpgsqlParameter("@bitUseShippersAccountNo", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(43).Value = _bitUseShippersAccountNo

                arParms(44) = New Npgsql.NpgsqlParameter("@bitUseMarkupShippingRate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(44).Value = _bitUseMarkupShippingRate

                arParms(45) = New Npgsql.NpgsqlParameter("@numMarkupShippingRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(45).Value = _dcMarkupShippingRate

                arParms(46) = New Npgsql.NpgsqlParameter("@intUsedShippingCompany", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(46).Value = _intUsedShippingCompany

                arParms(47) = New Npgsql.NpgsqlParameter("@numShipmentMethod", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(47).Value = _numShipmentMethod

                arParms(48) = New Npgsql.NpgsqlParameter("@dtReleaseDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(48).Value = ReleaseDate

                arParms(49) = New Npgsql.NpgsqlParameter("@numPartner", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(49).Value = _numPartnerId

                arParms(50) = New Npgsql.NpgsqlParameter("@tintClickBtn", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(50).Value = _tintClickBtn

                arParms(51) = New Npgsql.NpgsqlParameter("@numPartenerContactId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(51).Value = _numPartenerContactIn

                arParms(52) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(52).Value = _ClassID

                arParms(53) = New Npgsql.NpgsqlParameter("@numWillCallWarehouseID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(53).Value = WillCallWarehouseID

                arParms(54) = New Npgsql.NpgsqlParameter("@numVendorAddressID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(54).Value = VendorAddressID

                arParms(55) = New Npgsql.NpgsqlParameter("@numShipFromWarehouse", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(55).Value = ShipFromLocation

                arParms(56) = New Npgsql.NpgsqlParameter("@numShippingService", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(56).Value = ShippingService

                arParms(57) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(57).Value = _ClientTimeZoneOffset

                arParms(58) = New Npgsql.NpgsqlParameter("@vcCustomerPO", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(58).Value = _vcCustomerPO

                arParms(59) = New Npgsql.NpgsqlParameter("@bitDropShipAddress", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(59).Value = _bitDropShipAddress

                arParms(60) = New Npgsql.NpgsqlParameter("@PromCouponCode", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(60).Value = _PromotionCouponCode

                arParms(61) = New Npgsql.NpgsqlParameter("@numPromotionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(61).Value = _numPromotionId

                arParms(62) = New Npgsql.NpgsqlParameter("@dtExpectedDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(62).Value = _dtExpectedDate

                arParms(63) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(63).Value = ProjectID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_OppManage", objParam, True)

                Dim arrOutPut() As String = New String(2) {}
                arrOutPut(0) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                arrOutPut(1) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                Return arrOutPut
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveOrderSettings() As Integer
            Dim intResult As Integer = 0
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@bitBillingTerms", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _boolBillingTerms

                arParms(3) = New Npgsql.NpgsqlParameter("@intBillingDays", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _BillingDays

                arParms(4) = New Npgsql.NpgsqlParameter("@bitInterestType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _boolInterestType

                arParms(5) = New Npgsql.NpgsqlParameter("@fltInterest", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = _Interest

                arParms(6) = New Npgsql.NpgsqlParameter("@tintTaxOperator", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _TaxOperator

                arParms(7) = New Npgsql.NpgsqlParameter("@fltExchangeRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(7).Value = _fltExchageRate

                arParms(8) = New Npgsql.NpgsqlParameter("@dtItemReceivedDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(8).Value = _dtItemReceivedDate

                arParms(9) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _UserCntID

                arParms(10) = New Npgsql.NpgsqlParameter("@bitRecur", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _bitRecur

                arParms(11) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(11).Value = _dtStartDate

                arParms(12) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(12).Value = _dtEndDate

                arParms(13) = New Npgsql.NpgsqlParameter("@numFrequency", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(13).Value = _numFrequency

                arParms(14) = New Npgsql.NpgsqlParameter("@vcFrequency", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(14).Value = _vcFrequency

                arParms(15) = New Npgsql.NpgsqlParameter("@numRecConfigID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _numRecurConfigID

                arParms(16) = New Npgsql.NpgsqlParameter("@bitDisable", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(16).Value = _bitDisable

                intResult = SqlDAL.ExecuteNonQuery(connString, "USP_ManageOppSettings", arParms)

            Catch ex As Exception
                intResult = Nothing
                Throw
            End Try

            Return intResult
        End Function
        <Obsolete("Obsolete method do not use")>
        Public Function SaveFromAPI() As String()

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(21) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintSource", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _Source

                arParms(4) = New Npgsql.NpgsqlParameter("@vcPOppName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _OpportunityName

                arParms(5) = New Npgsql.NpgsqlParameter("@Comments", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(5).Value = _OppComments

                arParms(6) = New Npgsql.NpgsqlParameter("@bitPublicFlag", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = If(_PublicFlag = 1, True, False)

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@monPAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(8).Value = _Amount

                arParms(9) = New Npgsql.NpgsqlParameter("@numAssignedTo", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _AssignedTo


                arParms(10) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _DomainID

                arParms(11) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(11).Value = _strItems

                arParms(12) = New Npgsql.NpgsqlParameter("@strMilestone", NpgsqlTypes.NpgsqlDbType.Text)
                _strMilestone = _strMilestone.Replace("false", "0")
                _strMilestone = _strMilestone.Replace("true", "1")
                arParms(12).Value = _strMilestone

                arParms(13) = New Npgsql.NpgsqlParameter("@dtEstimatedCloseDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(13).Value = _EstimatedCloseDate

                arParms(14) = New Npgsql.NpgsqlParameter("@CampaignID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _CampaignID

                arParms(15) = New Npgsql.NpgsqlParameter("@lngPConclAnalysis", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _ConAnalysis

                arParms(16) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(16).Value = _OppType

                arParms(17) = New Npgsql.NpgsqlParameter("@tintActive", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(17).Value = If(_Active, 1, 0)

                arParms(18) = New Npgsql.NpgsqlParameter("@numSalesOrPurType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = _SalesorPurType

                arParms(19) = New Npgsql.NpgsqlParameter("@numRecurringId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = _RecurringId

                arParms(20) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(20).Value = _CurrencyID

                arParms(21) = New Npgsql.NpgsqlParameter("@vcSource", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(21).Value = _vcSource


                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_OppManage_API", objParam, True)

                Dim arrOutPut() As String = New String(2) {}
                arrOutPut(0) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
                arrOutPut(1) = CStr(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                Return arrOutPut

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function CreateOppItem() As Long

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@vcType", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _ItemType

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_OppAddItem", arParms))


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function UpdateOpportunityItems() As Long

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(18) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numUnitHour", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _Units

                arParms(3) = New Npgsql.NpgsqlParameter("@monPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Value = _UnitPrice

                arParms(4) = New Npgsql.NpgsqlParameter("@vcItemName", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(4).Value = _ItemName

                arParms(5) = New Npgsql.NpgsqlParameter("@vcModelID", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(5).Value = _ModelName

                arParms(6) = New Npgsql.NpgsqlParameter("@vcPathForTImage", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(6).Value = _ItemThumbImage

                arParms(7) = New Npgsql.NpgsqlParameter("@vcItemDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(7).Value = _Desc

                arParms(8) = New Npgsql.NpgsqlParameter("@numWarehouseItmsID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _WarehouseItemID

                arParms(9) = New Npgsql.NpgsqlParameter("@ItemType", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(9).Value = _ItemType

                arParms(10) = New Npgsql.NpgsqlParameter("@DropShip", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(10).Value = _Dropship

                arParms(11) = New Npgsql.NpgsqlParameter("@numUOM", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = CCommon.ToLong(_numUOM)

                arParms(12) = New Npgsql.NpgsqlParameter("@numClassID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _ClassID

                arParms(13) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _ProjectID

                arParms(14) = New Npgsql.NpgsqlParameter("@vcNotes", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(14).Value = Notes

                arParms(15) = New Npgsql.NpgsqlParameter("@dtItemRequiredDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(15).Value = ItemRequiredDate

                arParms(16) = New Npgsql.NpgsqlParameter("@vcAttributes", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(16).Value = Attributes

                arParms(17) = New Npgsql.NpgsqlParameter("@vcAttributeIDs", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(17).Value = AttributeIDs

                arParms(18) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(18).Value = Nothing
                arParms(18).Direction = ParameterDirection.InputOutput

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_UpdateOppItems", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Obsolete method replaced by new milestone and stages
        <Obsolete("Obsolete method replaced by new milestone and stages")>
        Public Function BusinessProcessByOpID() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@ProcessID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesProcessId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@DomianID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@OpportunityID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _OpportunityId



                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPMilestone", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ItemsByOppId() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@OpportunityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _ClientTimeZoneOffset

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_OPPItemDetails", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        '''  Use this method only to bind grid in Order Details (Products & services), for rest use GetOrderItems
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetOrderItemsProductServiceGrid() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@OpportunityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _ClientTimeZoneOffset

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_GetItems", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOrderItems() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = _Mode

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OpportunityId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@vcOppItemIDs", NpgsqlTypes.NpgsqlDbType.VarChar, 2000)
                arParms(5).Value = _strItems

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetOrderItems", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocItemsForReturn() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", _OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocID", OppBizDocID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_OpportunityBizDocs_GetItemsToReturn", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AssociatedbyOppID() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_OPPContactInfo", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Private _sMode As Short
        Public Property sMode() As Short
            Get
                Return _sMode
            End Get
            Set(ByVal value As Short)
                _sMode = value
            End Set
        End Property

        Public Function CheckServiceItemInOrder() As Boolean
            Dim blnResult As Boolean = False
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim dsOrder As New DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numOppId", _OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numItemCode", _ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@sMode", _sMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                dsOrder = SqlDAL.ExecuteDataset(connString, "Usp_CheckServiceItemInOrder", objParam)
                If dsOrder IsNot Nothing AndAlso dsOrder.Tables.Count > 0 Then
                    If CCommon.ToInteger(dsOrder.Tables(0).Rows(0)("Result")) > 0 Then
                        blnResult = True
                    Else
                        blnResult = False
                    End If
                End If


            Catch ex As Exception
                Throw ex
            End Try
            Return blnResult
        End Function

        Public Function OpportunityDTL() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_OPPDetails", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        <Obsolete("This module has been rewritten.")>
        Public Function SaveMilestone() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@strMilestone", NpgsqlTypes.NpgsqlDbType.Text)
                _strMilestone = _strMilestone.Replace("false", "0")
                _strMilestone = _strMilestone.Replace("true", "1")
                arParms(1).Value = _strMilestone

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_OppSaveMilestone", arParms)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function SaveSubStage() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 0

                arParms(1) = New Npgsql.NpgsqlParameter("@OppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@stageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppStageID

                arParms(3) = New Npgsql.NpgsqlParameter("@strSubStage", NpgsqlTypes.NpgsqlDbType.Text)
                _strSubStage = _strSubStage.Replace("true", "1")
                _strSubStage = _strSubStage.Replace("false", "0")
                arParms(3).Value = _strSubStage

                arParms(4) = New Npgsql.NpgsqlParameter("@numSubStageHdrID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _SalesProcessId

                arParms(5) = New Npgsql.NpgsqlParameter("@numSubStageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _SubStageID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "USP_OppSaveSubStage", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetSubStage() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 1

                arParms(1) = New Npgsql.NpgsqlParameter("@OppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@stageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppStageID

                arParms(3) = New Npgsql.NpgsqlParameter("@strSubStage", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _strSubStage

                arParms(4) = New Npgsql.NpgsqlParameter("@numSubStageHdrID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _SalesProcessId

                arParms(5) = New Npgsql.NpgsqlParameter("@numSubStageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _SubStageID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OppSaveSubStage", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetSubStageBySalesProId() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 2

                arParms(1) = New Npgsql.NpgsqlParameter("@OppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@stageDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppStageID

                arParms(3) = New Npgsql.NpgsqlParameter("@strSubStage", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _strSubStage

                arParms(4) = New Npgsql.NpgsqlParameter("@numSubStageHdrID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _SalesProcessId

                arParms(5) = New Npgsql.NpgsqlParameter("@numSubStageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _SubStageID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OppSaveSubStage", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function ShippingOrReceived() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@OppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_OppShippingorReceiving", arParms)
                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function CheckCanbeShipped() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@intOpportunityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@bitCheck", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _bitCheck

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPCheckCanBeShipped", arParms)

                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ReOpenOppertunity() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@OppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_ReOpenOppertunity", arParms)
                Return True

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function CheckCanbeReOpenOppertunity() As Integer
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@intOpportunityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@bitCheck", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _bitCheck

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "USP_CheckCanbeReOpenOppertunity", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetLinkedProjects() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@ByteMode", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = 0

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_LinkedProjects", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ItemForOptAndAcc() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemGroupID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemGroup

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ItemforOptions", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function LoadItemsFromOppToCreateOpp() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetItemsForCreatingOpp", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function UpdateOpportunityLinkingDtls() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(25) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId


                arParms(1) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _OppType

                arParms(2) = New Npgsql.NpgsqlParameter("@tintBillToType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _BillToType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintShipToType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _ShipToType

                arParms(4) = New Npgsql.NpgsqlParameter("@vcBillStreet", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = _BillStreet

                arParms(5) = New Npgsql.NpgsqlParameter("@vcBillCity", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(5).Value = _BillCity

                arParms(6) = New Npgsql.NpgsqlParameter("@numBillState", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _BillState

                arParms(7) = New Npgsql.NpgsqlParameter("@vcBillPostCode", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(7).Value = _BillPostal

                arParms(8) = New Npgsql.NpgsqlParameter("@numBillCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _BillCountry

                arParms(9) = New Npgsql.NpgsqlParameter("@vcShipStreet", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(9).Value = _ShipStreet

                arParms(10) = New Npgsql.NpgsqlParameter("@vcShipCity", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(10).Value = _ShipCity

                arParms(11) = New Npgsql.NpgsqlParameter("@numShipState", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _ShipState

                arParms(12) = New Npgsql.NpgsqlParameter("@vcShipPostCode", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(12).Value = _ShipPostal

                arParms(13) = New Npgsql.NpgsqlParameter("@numShipCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _ShipCountry

                arParms(14) = New Npgsql.NpgsqlParameter("@vcBillCompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(14).Value = _BillCompanyName

                arParms(15) = New Npgsql.NpgsqlParameter("@vcShipCompanyName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(15).Value = _ShipCompanyName

                arParms(16) = New Npgsql.NpgsqlParameter("@numParentOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = _ParentOppID

                arParms(17) = New Npgsql.NpgsqlParameter("@numParentOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _ParentBizDocID

                arParms(18) = New Npgsql.NpgsqlParameter("@numBillToAddressID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = _BillToAddressID

                arParms(19) = New Npgsql.NpgsqlParameter("@numShipToAddressID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = _ShipToAddressID

                arParms(20) = New Npgsql.NpgsqlParameter("@numBillingContact", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(20).Value = BillingContact

                arParms(21) = New Npgsql.NpgsqlParameter("@bitAltBillingContact", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(21).Value = IsAltBillingContact

                arParms(22) = New Npgsql.NpgsqlParameter("@vcAltBillingContact", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(22).Value = AltBillingContact

                arParms(23) = New Npgsql.NpgsqlParameter("@numShippingContact", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(23).Value = ShippingContact

                arParms(24) = New Npgsql.NpgsqlParameter("@bitAltShippingContact", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(24).Value = IsAltShippingContact

                arParms(25) = New Npgsql.NpgsqlParameter("@vcAltShippingContact", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(25).Value = AltShippingContact

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateOppLinkingDTls", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        ' Modified By Ajit Singh on 01/10/2008 for updating the Existing Address in DivisionMaster Table.
        Public Function UpdateOpportunityAddress() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@numAddressID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = BillToAddressID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcStreet", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(3).Value = _BillStreet

                arParms(4) = New Npgsql.NpgsqlParameter("@vcCity", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(4).Value = _BillCity

                arParms(5) = New Npgsql.NpgsqlParameter("@vcPostalCode", NpgsqlTypes.NpgsqlDbType.Varchar, 15)
                arParms(5).Value = _BillPostal

                arParms(6) = New Npgsql.NpgsqlParameter("@numState", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(6).Value = _BillState

                arParms(7) = New Npgsql.NpgsqlParameter("@numCountry", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(7).Value = _BillCountry

                arParms(8) = New Npgsql.NpgsqlParameter("@vcCompanyName", NpgsqlTypes.NpgsqlDbType.Varchar, 100)
                arParms(8).Value = _BillCompanyName

                arParms(9) = New Npgsql.NpgsqlParameter("@numCompanyId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(9).Value = _ComanyID

                arParms(10) = New Npgsql.NpgsqlParameter("@vcAddressName", NpgsqlTypes.NpgsqlDbType.Varchar, 50)
                arParms(10).Value = _AddressName

                arParms(11) = New Npgsql.NpgsqlParameter("@numReturnHeaderID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(11).Value = _ReturnHeaderID

                arParms(12) = New Npgsql.NpgsqlParameter("@bitCalledFromProcedure", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(12).Value = False

                arParms(13) = New Npgsql.NpgsqlParameter("@numContact", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(13).Value = ContactID

                arParms(14) = New Npgsql.NpgsqlParameter("@bitAltContact", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = IsAltContact

                arParms(15) = New Npgsql.NpgsqlParameter("@vcAltContact", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(15).Value = AltContact

                arParms(16) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(16).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateOppAddress", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function UpdateOpportunityAddressType() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@tintBillToType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _BillToType

                arParms(2) = New Npgsql.NpgsqlParameter("@tintShipToType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _ShipToType

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateOppLinkingDTls", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetPartNo() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID


                arParms(2) = New Npgsql.NpgsqlParameter("@vcPartNo", NpgsqlTypes.NpgsqlDbType.Char, 100)
                arParms(2).Direction = ParameterDirection.InputOutput
                'arParms(2).Value = _PartNo

                arParms(3) = New Npgsql.NpgsqlParameter("@monCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Direction = ParameterDirection.InputOutput
                'arParms(3).Value = _VendorCost

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetPartNo", objParam, True)
                _VendorCost = Convert.ToDecimal(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)
                _PartNo = Convert.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetOpportunityAddress() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _Mode

                arParms(2) = New Npgsql.NpgsqlParameter("@numReturnHeaderID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ReturnHeaderID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetOppAddress", arParms).Tables(0))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function AddOppContacts() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ContactID

                arParms(3) = New Npgsql.NpgsqlParameter("@numRole", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ContactRole

                arParms(4) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _bitPartner

                arParms(5) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _Mode

                arParms(6) = New Npgsql.NpgsqlParameter("@bitSubscribedEmailAlert", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = _bitSubscribedEmailAlert

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_ManageOppAssociatedContact", arParms).Tables(0))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetRecurringDetails() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID


                arParms(2) = New Npgsql.NpgsqlParameter("@bitVisible", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = _bitVisible

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetRecurringPart", objParam, True)
                _bitVisible = Convert.ToBoolean(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function InsertReturns() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@numTotalQty", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _Units

                arParms(4) = New Npgsql.NpgsqlParameter("@numQtyToReturn", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = _UnitsReturned

                arParms(5) = New Npgsql.NpgsqlParameter("@numQtyReturned", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = 0

                arParms(6) = New Npgsql.NpgsqlParameter("@monPrice", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _UnitPrice

                arParms(7) = New Npgsql.NpgsqlParameter("@numReasonForReturnID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(7).Value = _ReasonForReturn

                arParms(8) = New Npgsql.NpgsqlParameter("@numReturnStatus", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(8).Value = _ReturnStatus

                arParms(9) = New Npgsql.NpgsqlParameter("@numCreateBy", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(9).Value = _UserID

                arParms(10) = New Npgsql.NpgsqlParameter("@vcReferencePO", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(10).Value = _Reference

                SqlDAL.ExecuteNonQuery(connString, "Usp_AddReturn", arParms)

                Return True
            Catch ex As Exception
                Throw ex
                Return False
            Finally

            End Try

        End Function

        Public Function GetReturns() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numReturnId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = IIf(_ReturnID = 0, DBNull.Value, _ReturnID)

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numReturnStatus", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = IIf(_ReturnStatus = 0, DBNull.Value, _ReturnStatus)

                arParms(4) = New Npgsql.NpgsqlParameter("@numReturnReason", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = IIf(_ReasonForReturn = 0, DBNull.Value, _ReasonForReturn)

                arParms(5) = New Npgsql.NpgsqlParameter("@vcOrgSearchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = _SearchText

                arParms(6) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _CurrentPage

                arParms(7) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _PageSize

                arParms(8) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Direction = ParameterDirection.InputOutput
                arParms(8).Value = _TotalRecords

                arParms(9) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _ClientTimeZoneOffset

                arParms(10) = New Npgsql.NpgsqlParameter("@tintReturnType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(10).Value = _OppType

                arParms(11) = New Npgsql.NpgsqlParameter("@vcRMASearchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(11).Value = _vcRMASearchText

                arParms(12) = New Npgsql.NpgsqlParameter("@vcSortColumn", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(12).Value = _vcSortColumn

                arParms(13) = New Npgsql.NpgsqlParameter("@tintSortDirection", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _intSortDirection

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim lSqlDALExecuteDataset As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "Usp_GetReturns", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(8).Value)

                Return (lSqlDALExecuteDataset.Tables(0))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetReturnDetails() As DataSet

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetReturnDetails", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function UpdateReturns() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numReturnID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _ReturnID

                'arParms(1) = New Npgsql.NpgsqlParameter("@numQtyReturned", NpgsqlTypes.NpgsqlDbType.Integer)
                'arParms(1).Value = _UnitsReturned

                arParms(1) = New Npgsql.NpgsqlParameter("@numReturnStatus", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _ReturnStatus

                arParms(2) = New Npgsql.NpgsqlParameter("@numModifiedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcReferencePO", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(3).Value = Reference

                SqlDAL.ExecuteNonQuery(connString, "Usp_UpdateReturns", arParms)

                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Function AddSubmitForPayment(ByVal PaymentAction As Short) As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numPaymentMethod", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _PaymentMethod

                arParms(1) = New Npgsql.NpgsqlParameter("@numPaymentType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _PaymentType

                arParms(2) = New Npgsql.NpgsqlParameter("@monAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _Amount

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@vcReference", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = _Reference

                arParms(5) = New Npgsql.NpgsqlParameter("@vcMemo", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = _Memo

                arParms(6) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@numReturnID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _ReturnID

                arParms(8) = New Npgsql.NpgsqlParameter("@dtReturnDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(8).Value = _ReturnDate

                arParms(9) = New Npgsql.NpgsqlParameter("@tintPaymentAction", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = PaymentAction

                arParms(10) = New Npgsql.NpgsqlParameter("@numQtyReturned", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _UnitsReturned

                arParms(11) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(11).Value = _OppType

                If SqlDAL.ExecuteNonQuery(connString, "USP_SubmitSalseReturnPayment", arParms) > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function
        Public Function DeleteSalesReturn() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numReturnHeaderID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ReturnID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _DomainID

                If SqlDAL.ExecuteNonQuery(connString, "Usp_DeleteSalesReturn", arParms) > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Function GetReturnsReport() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _CurrentPage

                arParms(2) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _PageSize

                arParms(3) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _TotalRecords

                arParms(4) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _Mode

                arParms(5) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _DivisionID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim lSqlDALExecuteDataset As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "Usp_getReturnsReport", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

                Return (lSqlDALExecuteDataset.Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetChildOpportunity() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "Usp_GetChildOpportunity", arParms).Tables(0))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function ManageOppLinking() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numParentOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ParentOppID

                arParms(1) = New Npgsql.NpgsqlParameter("@numChildOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSource", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(2).Value = _vcSource

                arParms(3) = New Npgsql.NpgsqlParameter("@numParentOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ParentBizDocID

                arParms(4) = New Npgsql.NpgsqlParameter("@numParentProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ProjectID

                arParms(5) = New Npgsql.NpgsqlParameter("@numWebApiId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _WebApiId

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageOpportunityLinking", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetOpenRecords() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@DomainID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@bytemode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _Mode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetOpenRecords", arParms).Tables(0))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateOpportunityItemLabel() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numoppitemtCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@vcItemName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = _ItemName

                arParms(3) = New Npgsql.NpgsqlParameter("@vcModelID", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = _ModelName

                arParms(4) = New Npgsql.NpgsqlParameter("@vcItemDesc", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = _ItemDesc

                arParms(5) = New Npgsql.NpgsqlParameter("@vcManufacturer", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = _Manufacturer

                arParms(6) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _ProjectID

                arParms(7) = New Npgsql.NpgsqlParameter("@numClassID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _ClassID

                arParms(8) = New Npgsql.NpgsqlParameter("@vcNotes", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = _vcNotes

                arParms(9) = New Npgsql.NpgsqlParameter("@numCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(9).Value = numCost

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateOppItemLabel", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetEmbeddedCostDefault() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numEmbeddedCostID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _EmbeddedCostID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCostCatID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _CostCategoryID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetEmbeddedCostDefaults", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageEmbeddedCostDefaults() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCostCatID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _CostCategoryID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _strItems

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageEmbeddedCostDefaults", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetEmbeddedCost() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppBizDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numEmbeddedCostID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _EmbeddedCostID

                arParms(2) = New Npgsql.NpgsqlParameter("@numCostCatID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _CostCategoryID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetEmbeddedCost", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageEmbeddedCost() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppBizDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCostCatID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _CostCategoryID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _strItems

                arParms(5) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _Mode

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageEmbeddedCost", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function DeleteEmbeddedCost() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppBizDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCostCatID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _CostCategoryID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteEmbeddedCost", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageEmbeddedCostConfig() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppBizDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numLastViewedCostCatID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _CostCategoryID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageEmbeddedCostConfig", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetEmbeddedCostConfig() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppBizDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetEmbeddedCostConfig", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ManageEmbeddedCostItems() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numEmbeddedCostID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _EmbeddedCostID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _strItems

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageEmbeddedCostItems", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetEmbeddedCostItems() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numEmbeddedCostID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _EmbeddedCostID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "USP_GetEmbeddedCostItems", arParms).Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function CopyEmbeddedCost(ByVal FromBizDocID As Long, ByVal ToBizDocID As Long, ByVal DomainID As Long) As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numFromBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = FromBizDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numToBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = ToBizDocID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_CopyEmbeddedCost", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ManageOppItemSerialNo(objTransaction As NpgsqlTransaction) As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(0).Value = _strItems

                If Not objTransaction Is Nothing Then
                    SqlDAL.ExecuteNonQuery(objTransaction, "USP_ManageOppItemSerialNo", arParms)
                Else
                    SqlDAL.ExecuteNonQuery(connString, "USP_ManageOppItemSerialNo", arParms)
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function ValidateOppSerializLot() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_ValidateOppSerializLot", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetDomainIDFromOppID() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_OpportunityMaster_GetDomainID", arParms))
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetSalesOppSerializLot() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "usp_GetSalesOppSerializLot", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function CheckWOStatus() As Integer
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@intOpportunityId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "USP_OPPCheckWOStatus", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageInventory() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@itemcode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWareHouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _WarehouseItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@monAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _Amount

                arParms(3) = New Npgsql.NpgsqlParameter("@tintOpptype", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _OppType

                arParms(4) = New Npgsql.NpgsqlParameter("@numUnits", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _Units

                arParms(5) = New Npgsql.NpgsqlParameter("@QtyShipped", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = _QtyShipped

                arParms(6) = New Npgsql.NpgsqlParameter("@QtyReceived", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(6).Value = _QtyReceived

                arParms(7) = New Npgsql.NpgsqlParameter("@monPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(7).Value = _UnitPrice

                '1: SO/PO insert/edit 2:SO/PO Close
                arParms(8) = New Npgsql.NpgsqlParameter("@tintFlag", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(8).Value = _PublicFlag

                SqlDAL.ExecuteNonQuery(connString, "usp_ManageInventory", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemTransit(ByVal mode As Short, Optional ByVal numVendorid As Long = 0, Optional ByVal numAddressID As Long = 0) As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemcode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = mode

                arParms(3) = New Npgsql.NpgsqlParameter("@numVendorID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = numVendorid

                arParms(4) = New Npgsql.NpgsqlParameter("@numAddressID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = numAddressID

                arParms(5) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _ClientTimeZoneOffset

                arParms(6) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(6).Value = WarehouseItemID

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput


                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetItemTransit", arParms)

                Return (ds.Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetItemTransitForSO() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcItemcode", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = _strItems

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetItemTransitforSO", arParms)

                Return (ds.Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetVendorCostTable() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemcode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetVendorCostTable", arParms)

                Return (ds.Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetGrossProfitEstimate() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetGrossProfitEstimate", arParms)

                Return (ds.Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteOpportunityItem_TimeExpense() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@numoppitemtCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppItemCode

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteOpportunityItem_TimeExpense", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetTopSalesItems() As DataTable
            Try

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@IsUnit", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = _IsUnit

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetTopItemSales", arParms)

                Return (ds.Tables(0))
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetOppKitItemsList() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetOppKitItemsList", arParms)

                Return (ds.Tables(0))

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' 1= Issue credit (adds to the credit balance) 2 = Reverse issued credit  (subtracts from the credit balance)
        ''' </summary>
        ''' <param name="PaymentAction">1= Issue credit (adds to the credit balance) 2 = Reverse issued credit  (subtracts from the credit balance)   </param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function ManageCreditBalanceHistory(ByVal PaymentAction As Short) As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numCreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@numReturnID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _ReturnID

                arParms(4) = New Npgsql.NpgsqlParameter("@monAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _Amount

                arParms(5) = New Npgsql.NpgsqlParameter("@vcReference", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = _Reference

                arParms(6) = New Npgsql.NpgsqlParameter("@vcMemo", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = _Memo

                arParms(7) = New Npgsql.NpgsqlParameter("@tintPaymentAction", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = PaymentAction

                arParms(8) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(8).Value = _OppType


                If SqlDAL.ExecuteNonQuery(connString, "USP_ManageCreditBalanceHistory", arParms) > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
                Return False
            End Try
        End Function

        Public Function AllowFulfillOrder() As Boolean
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim boolAllowFulfillOrder As Boolean = False

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppId", _OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitAllowFulfillOrder", boolAllowFulfillOrder, NpgsqlTypes.NpgsqlDbType.Bit, ParameterDirection.InputOutput))

                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_AllowFulfillOrder", objParam, True)
                boolAllowFulfillOrder = CCommon.ToBool(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)

                Return boolAllowFulfillOrder

            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try
        End Function

        Public Function GetCampaignForOrganization() As Long
            Dim lngResult As Long = 0
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "Usp_getCampaignIdOfOrganization", sqlParams.ToArray())
                If dtResult IsNot Nothing AndAlso dtResult.Rows.Count > 0 Then
                    lngResult = CCommon.ToLong(dtResult.Rows(0)("numCampaignID"))
                End If

            Catch ex As Exception
                lngResult = 0
                Throw ex
            End Try

            Return lngResult
        End Function

        Public Function GetShippingDetailsForOrder() As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return (SqlDAL.ExecuteDataset(connString, "Usp_GetShippingDetailsForOrder", arParms).Tables(0))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        'Added by:Sachin Sadhu||Date:18thJune2014
        'Purpose:To get OrderType:Sales/Purchase as per Order ID(numOPPID)

        Public Function GetOrderType() As DataSet


            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
            arParms(0).Value = _OpportunityId

            arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParms(1).Value = Nothing
            arParms(1).Direction = ParameterDirection.InputOutput

            Return (SqlDAL.ExecuteDataset(connString, "USP_GetOrderType", arParms))

        End Function


        Public Function CheckIfUnitPriceApprovalRequired(ByRef ds As DataSet) As Boolean
            Dim isApprovalRequired As Boolean = False
            Dim dtItem As DataTable
            dtItem = ds.Tables(0)

            If Not dtItem Is Nothing AndAlso dtItem.Rows.Count > 0 Then

                For Each dr As DataRow In dtItem.Rows
                    If dtItem.Columns.Contains("bitPromotionDiscount") AndAlso CCommon.ToBool(dr("bitPromotionDiscount")) Then
                        Continue For
                    ElseIf CCommon.ToLong(dr("numPromotionID")) > 0 Then
                        Continue For
                    Else
                        Dim tempIsApprovalRequired As Boolean = False
                        Try
                            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}
                            Dim getconnection As New GetConnection
                            Dim connString As String = GetConnection.GetConnectionString

                            arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                            arParms(0).Value = _DomainID

                            arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                            arParms(1).Value = _DivisionID

                            arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                            arParms(2).Value = CCommon.ToLong(dr("numItemCode"))

                            arParms(3) = New Npgsql.NpgsqlParameter("@numQuantity", NpgsqlTypes.NpgsqlDbType.Numeric)
                            arParms(3).Value = CCommon.ToDouble(dr("numUnitHour"))

                            arParms(4) = New Npgsql.NpgsqlParameter("@numUnitPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                            arParms(4).Value = CCommon.ToDouble(dr("monPrice"))

                            arParms(5) = New Npgsql.NpgsqlParameter("@numTotalAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                            arParms(5).Value = CCommon.ToDouble(dr("monTotAmount"))

                            arParms(6) = New Npgsql.NpgsqlParameter("@numCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                            arParms(6).Value = CCommon.ToDouble(dr("numCost"))

                            arParms(7) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                            arParms(7).Value = CCommon.ToLong(dr("numWarehouseItmsID"))

                            arParms(8) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                            arParms(8).Value = CurrencyID

                            arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                            arParms(9).Value = Nothing
                            arParms(9).Direction = ParameterDirection.InputOutput

                            tempIsApprovalRequired = CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_ItemUnitPriceApproval_Check", arParms))

                            dr("bitItemPriceApprovalRequired") = tempIsApprovalRequired

                            If tempIsApprovalRequired Then
                                isApprovalRequired = True
                            End If

                        Catch ex As Exception
                            Throw ex
                        End Try
                    End If
                Next

            End If

            Return isApprovalRequired
        End Function

        Public Function CheckIfMinOrderAmountRuleMeets(ByVal _totalAmount As Decimal) As DataTable

            Dim dsReturnValue As DataSet
            Dim dtReturnValue As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@numTotalAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _totalAmount

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                dsReturnValue = SqlDAL.ExecuteDataset(connString, "USP_GetEnforcedMinOrderSubTotal", arParms)
                dtReturnValue = dsReturnValue.Tables(0)
                Return dtReturnValue

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub ApproveOpportunityItemsPrice()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ApprovalType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _SourceType

                SqlDAL.ExecuteNonQuery(connString, "USP_ApproveOpportunityItemsPrice", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub ApprovePORequistion()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@ApprovalType", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _SourceType

                SqlDAL.ExecuteNonQuery(connString, "USP_ApprovePORequistion", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        'Added by:Sachin Sadhu||Date:25thJuly2014
        'Purpose:Update Order Status||Used in Purchase FullFillment
        Public Function UpdateOrderStatus() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OrderStatus

                arParms(3) = New Npgsql.NpgsqlParameter("@numModifiedBy", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID


                ds = SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_UpdateStatus", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        'end of code

        Public Function CanApplyLandedCost() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_CheckInventoryForLandedCost", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function CheckOrderedAndInvoicedOrBilledQty() As DataSet
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput


                Return SqlDAL.ExecuteDataset(connString, "USP_CheckOrderedAndInvoicedOrBilledQty", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetUsedPromotions() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityMaster_GetUsedPromotion", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub SaveBulkOrderSalesTemp(ByVal numContactId As Long, ByVal numDivisionId As Long, ByVal numOppId As Long, ByVal IsCreated As Boolean)
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numContactId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numDivisionId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numOppId

                arParms(3) = New Npgsql.NpgsqlParameter("@IsCreated", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = IsCreated

                SqlDAL.ExecuteNonQuery(connString, "USP_BulkOppTemp", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub RunBulkOrderSalesBackground()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "USP_Opportunity_BulkSales", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetMaxOppItemID() As Long
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_OpportunityItems_GetMaxID", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub ChangeOrderStatus()
            Try
                Dim ingAccountClassID As Long = 0
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numStatus", OrderStatus, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcOppIds", OpportunityId, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_ChangeOrderStatus", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function UpdateDropShipToAddressForOrder() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportunityId

                arParms(1) = New Npgsql.NpgsqlParameter("@tintShipToType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _ShipToType

                arParms(2) = New Npgsql.NpgsqlParameter("@vcShipStreet", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _ShipStreet

                arParms(3) = New Npgsql.NpgsqlParameter("@vcShipCity", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _ShipCity

                arParms(4) = New Npgsql.NpgsqlParameter("@numShipState", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ShipState

                arParms(5) = New Npgsql.NpgsqlParameter("@vcShipPostCode", NpgsqlTypes.NpgsqlDbType.VarChar, 15)
                arParms(5).Value = _ShipPostal

                arParms(6) = New Npgsql.NpgsqlParameter("@numShipCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _ShipCountry

                arParms(7) = New Npgsql.NpgsqlParameter("@bitAltShippingContact", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = IsAltShippingContact

                arParms(8) = New Npgsql.NpgsqlParameter("@vcAltShippingContact", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = AltShippingContact

                arParms(9) = New Npgsql.NpgsqlParameter("@numShipToAddressID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _ShipToAddressID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateDropShipAddressForOrder", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function UpdateDealStatusConclusion() As Boolean

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OpportunityId

                arParms(2) = New Npgsql.NpgsqlParameter("@tintOppStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = OppStatus

                arParms(3) = New Npgsql.NpgsqlParameter("@lngPConclAnalysis", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = ConAnalysis

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateOpportunityDealStatusConclusion", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Sub UpdateParentKit()
            Try
                Dim ingAccountClassID As Long = 0
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", OppItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numParentOppItemID", ParentOppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_UpdateParentKit", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub SetAsWinningBid()
            Try
                Dim ingAccountClassID As Long = 0
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", OppItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_SetAsWinningBid", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub CloneOrderLineItem()
            Try
                Dim ingAccountClassID As Long = 0
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", OppItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_CloneOrderLineItem", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetFieldValue(ByVal fieldName As String) As Object
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcFieldName", fieldName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_OpportunityMaster_GetFieldValue", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub ChangeOrganization(ByVal newDivisionID As Long, ByVal newContactID As Long)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numNewDivisionID", newDivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numNewContactID", newContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_ChangeOrganization", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetOrderDetailForBluePayHPF() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocID", OppBizDocID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetOrderDetailForBluePayHPF", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub AddAutoCalculatedShippingRate()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtItemReleaseDate", ReleaseDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@numShipFromWarehouse", ShipFromLocation, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@monShipRate", UnitPrice, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@vcDescription", Desc, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_AddAutoCalculatedShippingRate", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub AddMappedItem(ByVal itemToAdd As Long)
            Try
                Dim arrOutPut() As String
                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.OpportunityId = Me.OpportunityId
                objOpportunity.DomainID = Me.DomainID
                objOpportunity.UserCntID = Me.UserCntID
                objOpportunity.ClientTimeZoneOffset = Me.ClientTimeZoneOffset
                objOpportunity.OpportunityDetails()

                objOpportunity.PublicFlag = 0
                objOpportunity.CouponCode = ""
                objOpportunity.Discount = 0
                objOpportunity.boolDiscType = False

                'LOAD ITEMS DATASET
                objOpportunity.Mode = 4
                Dim dsTemp As DataSet = objOpportunity.GetOrderItems()
                Dim dtItem As DataTable = dsTemp.Tables(0)
                dtItem.TableName = "Item"
                dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}

                Dim dtSerItem As DataTable = dsTemp.Tables(1)
                dtSerItem.TableName = "SerialNo"

                Dim dtChildItems As New DataTable
                dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
                dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtChildItems.Columns.Add("numItemCode")
                dtChildItems.Columns.Add("numQtyItemsReq")
                dtChildItems.Columns.Add("vcItemName")
                dtChildItems.Columns.Add("monListPrice")
                dtChildItems.Columns.Add("UnitPrice")
                dtChildItems.Columns.Add("charItemType")
                dtChildItems.Columns.Add("txtItemDesc")
                dtChildItems.Columns.Add("numWarehouseItmsID")
                dtChildItems.Columns.Add("Op_Flag")
                dtChildItems.Columns.Add("ItemType")
                dtChildItems.Columns.Add("Attr")
                dtChildItems.Columns.Add("vcWareHouse")
                dtChildItems.TableName = "ChildItems"
                dsTemp.Tables.Add(dtChildItems)

                If dtItem.Columns("bitTaxable0") Is Nothing Then
                    dtItem.Columns.Add("bitTaxable0")
                End If

                If dtItem.Columns("Tax0") Is Nothing Then
                    dtItem.Columns.Add("Tax0", GetType(Decimal))
                End If

                If dtItem.Columns("TotalTax") Is Nothing Then
                    dtItem.Columns.Add("TotalTax", GetType(Decimal))
                End If

                If dtSerItem.ParentRelations.Count = 0 Then dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
                dsTemp.Tables(2).Rows.Clear()


                Dim k As Integer
                Dim intCheck As Integer = 0
                For k = 0 To dtItem.Rows.Count - 1
                    If CCommon.ToString(dtItem.Rows(k).Item("charItemType")) <> "S" Then intCheck = 1
                    dtItem.Rows(k)("numUnitHour") = Math.Abs(CCommon.ToDouble(dtItem.Rows(k)("numUnitHour")) / CCommon.ToDouble(dtItem.Rows(k)("UOMConversionFactor")))
                Next

                dtItem.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
                dtItem.Columns.Add("vcInstruction")
                dtItem.Columns.Add("bintCompliationDate")
                dtItem.Columns.Add("dtPlannedStart")
                dtItem.Columns.Add("numWOAssignedTo")
                dtItem.Columns("ItemReleaseDate").DateTimeMode = System.Data.DataSetDateTime.Unspecified

                'Remove existing unmapped item & add new
                Dim result() As DataRow = dtItem.Select("numoppitemtCode = " & Me.OppItemCode)

                Dim qty As Double
                Dim price As Decimal
                Dim itemName As String
                Dim sku As String
                Dim asin As String
                Dim description As String
                Dim shipToState As Long = 0
                Dim shipToCounty As Long = 0

                If Not result Is Nothing AndAlso result.Count > 0 Then
                    For Each dr As DataRow In result
                        qty = CCommon.ToDouble(dr("numUnitHour"))
                        price = CCommon.ToDecimal(dr("monPrice"))
                        itemName = CCommon.ToString(dr("vcItemName"))
                        sku = CCommon.ToString(dr("vcSKUOpp"))
                        asin = CCommon.ToString(dr("vcASIN"))
                        description = CCommon.ToString(dr("vcItemDesc"))
                    Next
                End If

                dtItem.AcceptChanges()
                dsTemp.AcceptChanges()

                'Add mapped item
                Dim objEDIQueue As New EDIQueue
                Dim dtBizItem As DataTable = objEDIQueue.GetItemDtls850ForSO(4, Me.DomainID, 0, 0, 0, Convert.ToString(itemToAdd))

                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.OppId = Me.OpportunityId
                objOppBizDocs.DomainID = Me.DomainID
                Dim dsAddress As DataSet = objOppBizDocs.GetOPPGetOppAddressDetails

                If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 1 Then
                    shipToState = CCommon.ToLong(dsAddress.Tables(1).Rows(0)("numState"))
                    shipToCounty = CCommon.ToLong(dsAddress.Tables(1).Rows(0)("numCountry"))
                End If

                objEDIQueue.GetItemDetailsFor850Item(True, dsTemp, dtBizItem, "", qty, price, Me.DomainID, objOpportunity.DivisionID, "", 0, shipToState, shipToCounty, False, itemName, sku, asin, description)

                'Remove existing unmapped item & add new
                result = dtItem.Select("numoppitemtCode = " & Me.OppItemCode)
                If Not result Is Nothing AndAlso result.Count > 0 Then
                    For Each dr As DataRow In result
                        dtItem.Rows.Remove(dr)
                    Next
                End If
                dtItem.AcceptChanges()
                dsTemp.AcceptChanges()

                objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    arrOutPut = objOpportunity.Save()

                    If objOpportunity.SourceType = 4 AndAlso Not dtBizItem Is Nothing AndAlso dtBizItem.Rows.Count > 0 Then
                        Dim objIMM As New ItemMarketplaceMapping
                        objIMM.DomainID = Me.DomainID
                        objIMM.ItemCode = itemToAdd
                        objIMM.MarketplaceID = objOpportunity.Source
                        If (objOpportunity.Source = 1 Or objOpportunity.Source = 2 Or objOpportunity.Source = 3 Or objOpportunity.Source = 4) Then
                            objIMM.MarketplaceUniqueID = CCommon.ToString(dtBizItem.Rows(0)("vcASIN")).Trim()
                        Else
                            objIMM.MarketplaceUniqueID = CCommon.ToString(dtBizItem.Rows(0)("vcSKU")).Trim()
                        End If

                        If Not String.IsNullOrEmpty(objIMM.MarketplaceUniqueID) Then
                            objIMM.Save()
                        End If
                    End If

                    objTransactionScope.Complete()
                End Using
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetARContact() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityMaster_GetARContact", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub UpdateOrderedItemQuantity()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportunityId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", OppItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@tinMode", Mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numUnitHour", Units, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@monPrice", UnitPrice, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@vcNotes", Notes, NpgsqlTypes.NpgsqlDbType.Text))
                End With

                SqlDAL.ExecuteNonQuery(connString, "usp_opportunityitems_updateqty", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace
