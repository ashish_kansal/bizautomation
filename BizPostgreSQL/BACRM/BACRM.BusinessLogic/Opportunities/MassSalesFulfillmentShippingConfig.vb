﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic


Namespace BACRM.BusinessLogic.Opportunities

    Public Class MassSalesFulfillmentShippingConfig
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property ID As Long
        Public Property OrderSource As Long
        Public Property SourceType As Short
        Public Property Type As Short
        Public Property Priorities As String
        Public Property ShipVia As Long
        Public Property ShipService As Long
        Public Property IsOverride As Boolean
        Public Property Weight As Double
        Public Property ShipViaOverride As Long

#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderSource", OrderSource, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintSourceType", SourceType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintType", Type, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcPriorities", Priorities, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numShipVia", ShipVia, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numShipService", ShipService, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitOverride", IsOverride, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@fltWeight", Weight, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numShipViaOverride", ShipViaOverride, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_MassSalesFulfillmentShippingConfig_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetByOrderSource() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderSource", OrderSource, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintSourceType", SourceType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillmentShippingConfig_Get", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

    End Class

End Namespace