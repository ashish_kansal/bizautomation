﻿Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.Opportunities
    Public Class OpportunityItemsReleaseDates
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"
        Public Property ID As Long
        Public Property ItemCode As Long
        Public Property ItemName As String
        Public Property ItemType As String
        Public Property SKU As String
        Public Property Attributes As String
        Public Property VendorID As Long
        Public Property ShipmentMethod As Long
        Public Property Warehouse As String
        Public Property WarehouseID As Long
        Public Property AssetAccountID As Long
        Public Property WarehouseItemID As Long
        Public Property OppID As Long
        Public Property OppItemID As Long
        Public Property ReleaseDate As Date
        Public Property OriginalQuantity As Long
        Public Property Quantity As Double
        Public Property UOMConversionFactor As Double
        Public Property ReleaseStatus As Short '1 = Open, 2=Purchased
        Public Property ReleaseDates As String
        Public Property RequiredDates As String
        Public Property BusinessProcess As String
        Public Property BusinessProcessID As Long
        Public Property AverageCost As Double
        Public Property BuildManager As String
        Public Property BuildManagerID As Long
#End Region

#Region "Public Methods"
        Public Sub Save()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@ID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = OppID

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = OppItemID

                arParms(4) = New Npgsql.NpgsqlParameter("@dtReleaseDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(4).Value = ReleaseDate

                arParms(5) = New Npgsql.NpgsqlParameter("@numQuantity", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = Quantity

                arParms(6) = New Npgsql.NpgsqlParameter("@tintReleaseStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = ReleaseStatus

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityItemsReleaseDates_Save", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@ID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ID

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityItemsReleaseDates_Delete", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub UpdateItemReleaseStatus(ByVal purchasedOrderID As Long)
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@ID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = ID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = OppID

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = OppItemID

                arParms(4) = New Npgsql.NpgsqlParameter("@dtReleaseDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(4).Value = ReleaseDate

                arParms(5) = New Npgsql.NpgsqlParameter("@numQuantity", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = Quantity

                arParms(6) = New Npgsql.NpgsqlParameter("@tintReleaseStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = ReleaseStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@numPurchasedOrderID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = purchasedOrderID

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityItemsReleaseDates_UpdateItemReleaseStatus", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub UpdateOrderReleaseStatus()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityItemsReleaseDates_UpdateOrderReleaseStatus", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetByOppItemID() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OppID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = OppItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityItemsReleaseDates_GetByOppItemID", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function


#End Region

    End Class
End Namespace