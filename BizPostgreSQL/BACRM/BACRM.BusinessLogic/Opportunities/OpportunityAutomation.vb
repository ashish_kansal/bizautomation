﻿Option Explicit On
Option Strict On
Imports BACRM.BusinessLogic.Admin
Imports System.Web
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Opportunities
Imports System.Collections.Generic
Imports System.Linq

Namespace BACRM.BusinessLogic.Opportunities
    Public Class OpportunityAutomation
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property BluePaySuccessURL As String
        Public Property BluePayDeclineURL As String

#End Region

        Sub OpportunityAutomationExecution()
            Try
                Dim objOppBizDocs As New OppBizDocs

                Dim objSalesFulfillmentQueue As New SalesFulfillmentQueue
                Dim dtQueue As DataTable = objSalesFulfillmentQueue.GetRecords()

                If Not dtQueue Is Nothing AndAlso dtQueue.Rows.Count > 0 Then

                    For Each dr As DataRow In dtQueue.Rows
                        'Important: Rule 1 is removed now
                        Select Case CCommon.ToInteger(dr("tintRule"))
                            Case 2 'Pay Invoice - Credit Card
                                Try
                                    ChargeCreditCard(CCommon.ToLong(dr("numSFQID")), CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numUserCntID")), CCommon.ToLong(dr("numDivisionId")), CCommon.ToLong(dr("numContactID")), CCommon.ToLong(dr("numOppID")), CCommon.ToLong(dr("numCurrencyID")))
                                Catch ex As Exception
                                    UpdateSalesFulfillmentStatusAndLog(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numSFQID")), ex.Message, 2, False)
                                End Try
                            Case 3 'Create Shipping Label and Tracking Number
                                Try
                                    ' CHECK IF DEFAULT FULFILLMENT ORDER BIZDOC IS SELECTED OR NOT
                                    If CCommon.ToLong(dr("numDefaultSalesShippingDoc")) > 0 Then
                                        ' CHECK IF CONTAINERS FOR ALL INVENTORY ITEMS
                                        Dim objOpp As New COpportunities
                                        objOpp.DomainID = CCommon.ToLong(dr("numDomainID"))
                                        objOpp.OpportID = CCommon.ToLong(dr("numOppID"))
                                        Dim isAddedEnoughContainer As Boolean = objOpp.ValidateContainerQty()

                                        If isAddedEnoughContainer Then
                                            'FIRST CHECK IF ALL QTY OF ALL ORDER ITEMS IS ADDED TO DEFAULT SALES SHIPPING BIZDOC TYPE
                                            objOpp.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            objOpp.OpportID = CCommon.ToLong(dr("numOppID"))
                                            objOpp.BizDocId = CCommon.ToLong(dr("numDefaultSalesShippingDoc"))
                                            Dim isAddedQtyForAllItemsToBizDocType As Boolean = objOpp.ValidateAllQtyAddedToBizDocType()

                                            If Not isAddedQtyForAllItemsToBizDocType Then 'IF NOT ALL QTY IS ADDED THEN WE HAVE TO GENERATE IT
                                                objOppBizDocs = New OppBizDocs()
                                                objOppBizDocs.OppId = CCommon.ToLong(dr("numOppID"))
                                                objOppBizDocs.OppType = 1
                                                objOppBizDocs.UserCntID = CCommon.ToLong(dr("numUserCntID"))
                                                objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                objOppBizDocs.vcPONo = ""
                                                objOppBizDocs.vcComments = ""
                                                objOppBizDocs.FromDate = DateTime.UtcNow
                                                objOppBizDocs.ClientTimeZoneOffset = 0
                                                objOppBizDocs.BizDocId = CCommon.ToLong(dr("numDefaultSalesShippingDoc"))
                                                objOppBizDocs.BizDocTemplateID = 0
                                                objOppBizDocs.bitPartialShipment = True

                                                Dim objCommon As New CCommon
                                                objCommon.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                objCommon.Mode = 33
                                                objCommon.Str = CCommon.ToString(dr("numDefaultSalesShippingDoc"))
                                                objOppBizDocs.SequenceId = CCommon.ToString(objCommon.GetSingleFieldValue())

                                                objCommon = New CCommon
                                                objCommon.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                objCommon.Mode = 34
                                                objCommon.Str = CCommon.ToString(dr("numOppID"))
                                                objOppBizDocs.RefOrderNo = CCommon.ToString(objCommon.GetSingleFieldValue())

                                                Dim lintAuthorizativeBizDocsId As Long
                                                objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                objOppBizDocs.OppId = CCommon.ToLong(dr("numOppID"))
                                                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                                                Dim OppBizDocID As Long = 0
                                                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                                    OppBizDocID = objOppBizDocs.SaveBizDoc()
                                                    '
                                                    'Create Journals only for authoritative bizdocs
                                                    '-------------------------------------------------
                                                    If lintAuthorizativeBizDocsId = CCommon.ToLong(dr("numDefaultSalesShippingDoc")) Or CCommon.ToLong(dr("numDefaultSalesShippingDoc")) = 304 Then ' 304 - Deferred Income
                                                        Dim ds As New DataSet
                                                        Dim dtOppBiDocItems As DataTable

                                                        objOppBizDocs.OppBizDocId = OppBizDocID
                                                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                                                        dtOppBiDocItems = ds.Tables(0)

                                                        Dim objCalculateDealAmount As New CalculateDealAmount

                                                        objCalculateDealAmount.CalculateDealAmount(CCommon.ToLong(dr("numOppID")), OppBizDocID, 1, CCommon.ToLong(dr("numDomainID")), dtOppBiDocItems)

                                                        ''---------------------------------------------------------------------------------
                                                        Dim JournalId As Long = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=CCommon.ToString(ds.Tables(1).Rows(0).Item("vcBizDocID")))
                                                        Dim objJournalEntries As New JournalEntry

                                                        If CCommon.ToLong(dr("numDefaultSalesShippingDoc")) = 304 AndAlso CCommon.ToBool(dr("IsEnableDeferredIncome")) Then 'Deferred Revenue
                                                            objJournalEntries.SaveJournalEntriesDeferredIncome(CCommon.ToLong(dr("numOppID")), CCommon.ToLong(dr("numDomainID")), dtOppBiDocItems, JournalId, CCommon.ToLong(dr("numDivisionId")), OppBizDocID, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), objCalculateDealAmount.GrandTotal)
                                                        Else
                                                            If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                                                objJournalEntries.SaveJournalEntriesSales(CCommon.ToLong(dr("numOppID")), CCommon.ToLong(dr("numDomainID")), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, CCommon.ToLong(dr("numDivisionId")), CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), 0, bitInvoiceAgainstDeferredIncomeBizDoc:=objOppBizDocs.bitInvoiceForDeferred)
                                                            Else
                                                                objJournalEntries.SaveJournalEntriesSalesNew(CCommon.ToLong(dr("numOppID")), CCommon.ToLong(dr("numDomainID")), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, CCommon.ToLong(dr("numDivisionId")), CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), 0, bitInvoiceAgainstDeferredIncomeBizDoc:=objOppBizDocs.bitInvoiceForDeferred)
                                                            End If

                                                            If CCommon.ToBool(dr("bitAutolinkUnappliedPayment")) Then
                                                                Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                                                                objSalesFulfillment.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                                objSalesFulfillment.UserCntID = CCommon.ToLong(dr("numUserCntID"))
                                                                objSalesFulfillment.ClientTimeZoneOffset = 0
                                                                objSalesFulfillment.OppId = CCommon.ToLong(dr("numOppID"))
                                                                objSalesFulfillment.OppBizDocId = OppBizDocID

                                                                objSalesFulfillment.AutoLinkUnappliedPayment()
                                                            End If
                                                        End If
                                                    ElseIf CCommon.ToLong(dr("numDefaultSalesShippingDoc")) = 296 Then 'Fulfillment BizDoc
                                                        'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                                                        Dim objOppBizDoc As New OppBizDocs
                                                        objOppBizDoc.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                        objOppBizDoc.UserCntID = CCommon.ToLong(dr("numUserCntID"))
                                                        objOppBizDoc.OppId = CCommon.ToLong(dr("numOppID"))
                                                        objOppBizDoc.OppBizDocId = OppBizDocID
                                                        objOppBizDoc.AutoFulfillBizDoc()


                                                        'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                                                        '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                                                        'CREATE SALES CLEARING ENTERIES
                                                        'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                                                        '-------------------------------------------------
                                                        Dim ds As New DataSet
                                                        Dim dtOppBiDocItems As DataTable

                                                        objOppBizDocs.OppBizDocId = OppBizDocID
                                                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                                                        dtOppBiDocItems = ds.Tables(0)

                                                        Dim objCalculateDealAmount As New CalculateDealAmount
                                                        objCalculateDealAmount.CalculateDealAmount(CCommon.ToLong(dr("numOppID")), OppBizDocID, 1, CCommon.ToLong(dr("numDomainID")), dtOppBiDocItems)

                                                        'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                                                        Dim JournalId As Long = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                                                        Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                                                        If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                                            objJournalEntries.SaveJournalEnteriesSalesClearing(CCommon.ToLong(dr("numOppID")), CCommon.ToLong(dr("numDomainID")), dtOppBiDocItems, JournalId, CCommon.ToLong(dr("numDivisionId")), OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                                                        Else
                                                            objJournalEntries.SaveJournalEnteriesSalesClearingNew(CCommon.ToLong(dr("numOppID")), CCommon.ToLong(dr("numDomainID")), dtOppBiDocItems, JournalId, CCommon.ToLong(dr("numDivisionId")), OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                                                        End If
                                                        '---------------------------------------------------------------------------------
                                                    End If

                                                    objTransactionScope.Complete()
                                                End Using

                                                ''Added By Sachin Sadhu||Date:1stMay2014
                                                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                                                ''          Using Change tracking
                                                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                                                objWfA.DomainID = CCommon.ToLong(dr("numDomainID"))
                                                objWfA.UserCntID = CCommon.ToLong(dr("numUserCntID"))
                                                objWfA.RecordID = OppBizDocID
                                                objWfA.SaveWFBizDocQueue()
                                                'end of code
                                            End If

                                            objOppBizDocs.DomainID = CCommon.ToLong(dr("numDomainID"))
                                            objOppBizDocs.OppId = CCommon.ToLong(dr("numOppID"))
                                            objOppBizDocs.BizDocId = CCommon.ToLong(dr("numDefaultSalesShippingDoc"))
                                            Dim dtShippingBizDocs As DataTable = objOppBizDocs.GetShippingBizDocWithNoShippingLabelAndTracking()

                                            If Not dtShippingBizDocs Is Nothing AndAlso dtShippingBizDocs.Rows.Count > 0 Then
                                                Dim isErrorOcurred As Boolean = False
                                                For Each drBizDocs As DataRow In dtShippingBizDocs.Rows
                                                    Try
                                                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                                            GenerateShippingLabelAndTrackingNo(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numUserCntID")), CCommon.ToLong(dr("numOppID")), CCommon.ToLong(drBizDocs("numOppBizDocsId")))
                                                            objTransactionScope.Complete()
                                                        End Using
                                                    Catch ex As Exception
                                                        'DO NOT THROW ERROR
                                                        isErrorOcurred = True
                                                        UpdateSalesFulfillmentStatusAndLog(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numSFQID")), ex.Message, 3, False)
                                                    End Try

                                                    If Not isErrorOcurred Then
                                                        UpdateSalesFulfillmentStatusAndLog(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numSFQID")), "Shipping Label and Tracking Number Generated", 3, True)
                                                    End If
                                                Next
                                            Else
                                                UpdateSalesFulfillmentStatusAndLog(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numSFQID")), "Shipping Label and Tracking Number already Generated or Default shipping bizdoc is not added to order.", 3, True)
                                            End If
                                        Else
                                            UpdateSalesFulfillmentStatusAndLog(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numSFQID")), "Insufficient 'Containers' for all inventory items within order", 3, False)
                                        End If
                                    Else
                                        UpdateSalesFulfillmentStatusAndLog(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numSFQID")), "Select Sales BizDoc responsible for Shipping Labels & Trackings #. Go to Administration -> Global Settings -> Order Management", 3, False)
                                    End If
                                Catch ex As Exception
                                    UpdateSalesFulfillmentStatusAndLog(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numSFQID")), ex.Message, 3, False)
                                End Try
                            Case 4 'Close Order
                                Try
                                    If CCommon.ToShort(dr("tintOppStatus")) = 1 AndAlso CCommon.ToShort(dr("tintshipped")) <> 1 Then
                                        CloseOrder(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numUserCntID")), CCommon.ToLong(dr("numOppID")), CCommon.ToShort(dr("tintOppType")), CCommon.ToShort(dr("tintOppStatus")))
                                        UpdateSalesFulfillmentStatusAndLog(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numSFQID")), "Sales Order Closed", 4, True)
                                    Else
                                        UpdateSalesFulfillmentStatusAndLog(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numSFQID")), "Sales Order Is Already Closed", 4, True)
                                    End If
                                Catch ex As Exception
                                    UpdateSalesFulfillmentStatusAndLog(CCommon.ToLong(dr("numDomainID")), CCommon.ToLong(dr("numSFQID")), ex.Message, 4, False)
                                End Try
                        End Select
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function CheckIfApprovalRequired(ByVal oppID As Long, ByVal domainID As Long, ByVal userCntID As Long) As Boolean
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = oppID
                objOpportunity.DomainID = domainID
                objOpportunity.UserCntID = userCntID
                objOpportunity.ClientTimeZoneOffset = 0
                Dim dsTemp As DataSet = objOpportunity.ItemsByOppId

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 AndAlso dsTemp.Tables(0).Select("bitItemPriceApprovalRequired=1").Length > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub UpdateSalesFulfillmentStatusAndLog(ByVal domainID As Long, ByVal numSFQID As Long, ByVal vcMessage As String, ByVal tintRule As Short, ByVal isSuccess As Boolean)
            Try
                Dim objSalesFulfillmentQueue As New SalesFulfillmentQueue
                objSalesFulfillmentQueue.DomainID = domainID
                objSalesFulfillmentQueue.SFQID = numSFQID
                objSalesFulfillmentQueue.Message = vcMessage
                objSalesFulfillmentQueue.UpdateStatus(tintRule, isSuccess)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub CloseOrder(DomainID As Long, UserCntID As Long, OppID As Long, OppType As Short, OppStatus As Short)
            Try
                Dim strErrorMessage As String = ""

                Dim objOpportunity As New OppotunitiesIP

                objOpportunity.OpportunityId = OppID

                Dim dtTable As DataTable
                dtTable = objOpportunity.ValidateOppSerializLot()

                If dtTable.Rows.Count > 0 Then
                    Dim drErrors As DataRow()

                    drErrors = dtTable.Select("Error=1")

                    If drErrors.Length > 0 Then
                        strErrorMessage = "Following items could not be saved (your option is to provide respective no of Lot/Serial #s):"

                        For Each dr As DataRow In drErrors
                            If CShort(dr("Error")) = 1 Then
                                strErrorMessage += "<br/>- " + CStr(dr("vcItemName"))
                                Exit Sub
                            End If
                        Next

                        Throw New Exception(strErrorMessage)
                    End If
                End If

                If objOpportunity.CheckWOStatus > 0 Then
                    strErrorMessage = "You can't ship at this time because Work Order for this Sales Order is pending."
                    Throw New Exception(strErrorMessage)
                End If

                Dim dtItemList As DataTable
                dtItemList = objOpportunity.CheckCanbeShipped()
                If dtItemList.Rows.Count > 0 Then
                    strErrorMessage = "You can't ship at this time because you don't have enough quantity on hand to support your shipment. Your options are to modify your order, or replenish inventory (to check inventory click on the edit link within the line item, then the value in the 'Products/Services' column):"

                    For Each dr As DataRow In dtItemList.Rows
                        strErrorMessage += "<br/>- " + CStr(dr("vcItemName"))
                    Next

                    Throw New Exception(strErrorMessage)
                End If

                'If there are Fulfillment BizDoc(s) available which are not yet marked as fulfilled than auto fulfil those bizdocs.
                Dim objCOpportunity As New COpportunities
                objCOpportunity.OpportID = OppID
                Dim dtFulfillmentBizDoc As DataTable = objCOpportunity.GetPendingFulfillmentBizDocs()

                If Not dtFulfillmentBizDoc Is Nothing AndAlso dtFulfillmentBizDoc.Rows.Count > 0 Then
                    For Each drBizDoc As DataRow In dtFulfillmentBizDoc.Rows
                        objCOpportunity.AutoFulfillOrder(DomainID, UserCntID, OppID, CCommon.ToLong(drBizDoc("numOppBizDocsId")))
                    Next
                End If

                Dim dsInvoiceBilled As DataSet = objOpportunity.CheckOrderedAndInvoicedOrBilledQty()
                Dim bitFulfillmentBizDocPending As Boolean = CCommon.ToBool(dsInvoiceBilled.Tables(0).Select("ID=1")(0)("bitTrue"))
                Dim bitOrderedAndShippedBilledQtyNotSame As Boolean = CCommon.ToBool(dsInvoiceBilled.Tables(0).Select("ID=2")(0)("bitTrue"))
                Dim bitOrderedAndFulfilledQtyNotSame As Boolean = CCommon.ToBool(dsInvoiceBilled.Tables(0).Select("ID=3")(0)("bitTrue"))
                Dim bitDeferredAndInvoicedQtyNotSame As Boolean = CCommon.ToBool(dsInvoiceBilled.Tables(0).Select("ID=4")(0)("bitTrue"))


                If OppType = 1 AndAlso OppStatus = 1 AndAlso bitDeferredAndInvoicedQtyNotSame Then
                    strErrorMessage = "Order contains diferred income bizdoc(s) against which invoice(s) are not generated."
                    Throw New Exception(strErrorMessage)
                ElseIf OppType = 1 AndAlso OppStatus = 1 AndAlso bitFulfillmentBizDocPending Then
                    strErrorMessage = "Order contains fulfillment bizdoc(s) which is not fulfilled. Please fulfill bizdoc(s) first."
                    Throw New Exception(strErrorMessage)
                ElseIf OppType = 1 AndAlso OppStatus = 1 AndAlso (bitOrderedAndShippedBilledQtyNotSame Or bitOrderedAndFulfilledQtyNotSame) Then
                    CreateInvoiceAndFulfillmentOrder(DomainID, UserCntID, OppID, True)

                    Try
                        objOpportunity.UserCntID = UserCntID
                        objOpportunity.ShippingOrReceived()
                    Catch ex As Exception
                        If ex.Message.Contains("ORDER_CLOSED") Then
                            'Order is already closed do not throw error
                        Else
                            Throw
                        End If
                    End Try
                Else
                    Try
                        objOpportunity.UserCntID = UserCntID
                        objOpportunity.ShippingOrReceived()
                    Catch ex As Exception
                        If ex.Message.Contains("ORDER_CLOSED") Then
                            'Order is already closed do not throw error
                        Else
                            Throw
                        End If
                    End Try
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Function CreateInvoiceAndFulfillmentOrder(ByVal DomainID As Long, ByVal UserCntID As Long, ByVal lngOppId As Long, ByVal isReleaseAllocation As Boolean) As Boolean
            Try
                Dim isInvoiceCreated As Boolean = False
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = lngOppId
                Dim dt As DataSet = objOpportunity.CheckOrderedAndInvoicedOrBilledQty()

                Dim bitOrderedAndInvoicedBilledQtyNotSame As Boolean = CCommon.ToBool(dt.Tables(0).Select("ID=2")(0)("bitTrue"))
                Dim bitOrderedAndFulfilledQtyNotSame As Boolean = CCommon.ToBool(dt.Tables(0).Select("ID=3")(0)("bitTrue"))

                'CREATE INVOICE FOR REMAINING QTY WHICH NOT YET INVOICED
                If bitOrderedAndInvoicedBilledQtyNotSame Then
                    Try
                        CreateInvoice(DomainID, UserCntID, lngOppId)
                        isInvoiceCreated = True
                    Catch ex As Exception
                        If ex.Message = "NOT_ALLOWED" Then
                            Throw New Exception("Not able to create invoice. To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!")
                        ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                            Throw New Exception("Not able to create invoice. Only a Sales Order can create a Fulfillment Order")
                        ElseIf ex.Message = "FY_CLOSED" Then
                            Throw New Exception("Not able to create invoice. This transaction can not be posted,Because transactions date belongs to closed financial year.")
                        ElseIf ex.Message = "AlreadyInvoice" Then
                            Throw New Exception("Not able to create invoice. You can’t create a Invoice against a Fulfillment Order that already contains a Invoice")
                        ElseIf ex.Message = "AlreadyPackingSlip" Then
                            Throw New Exception("Not able to create invoice. You can’t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip")
                        ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                            Throw New Exception("Not able to create invoice. You can’t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip")
                        ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                            Throw New Exception("Not able to create invoice. Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section")
                        Else
                            Throw New Exception("Not able to create invoice. Unknown error ocurred.")
                        End If

                        Exit Function
                    End Try
                End If

                'CREATE FULFILLMENT BIZ DOC OF REMAINING QTY AND MARK IF AS SHIPPED (RELEASE FROM ALLOCATION)
                If bitOrderedAndFulfilledQtyNotSame Then
                    Try
                        Dim objCOpportunity As New COpportunities
                        objCOpportunity.CreateFulfillmentBizDoc(DomainID, UserCntID, lngOppId)
                    Catch ex As Exception
                        If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                            Throw New Exception("Not able to add fulfillment bizdoc because some work orders are still not completed.")
                        ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                            Throw New Exception("Not able to add fulfillment bizdoc because qty is not same as required qty.<br />")
                        ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                            Throw New Exception("Not able to add fulfillment bizdoc because required number of serials are not provided.<br />")
                        ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                            Throw New Exception("Not able to add fulfillment bizdoc because invalid serials are provided.<br />")
                        ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                            Throw New Exception("Not able to add fulfillment bizdoc because required number of Lots are not provided.<br />")
                        ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                            Throw New Exception("Not able to add fulfillment bizdoc because invalid Lots are provided.<br />")
                        ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                            Throw New Exception("Not able to add fulfillment bizdoc because Lot number do not have enough qty to fulfill.<br />")
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                            Throw New Exception("Not able to add fulfillment bizdoc because warehouse allocation is invlid.<br />")
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ONHAND") Then
                            Throw New Exception("Not able to add fulfillment bizdoc because warehouse does not have enought on hand quantity.<br />")
                        Else
                            Throw New Exception("Not able to add fulfillment order due to unknown error.<br />")
                        End If
                        Exit Function
                    End Try
                End If

                Return isInvoiceCreated
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub CreateInvoice(DomainID As Long, UserCntID As Long, lngOppId As Long)
            Try
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0

                Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    Dim dtDetails, dtBillingTerms As DataTable

                    Dim objPageLayout As New CPageLayout
                    objPageLayout.OpportunityId = lngOppId
                    objPageLayout.DomainID = DomainID
                    dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                    lngDivId = CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID"))
                    lngCntID = CCommon.ToLong(dtDetails.Rows(0).Item("numContactID"))


                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.UserCntID = UserCntID
                    objOppBizDocs.DomainID = DomainID

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", DomainID, lngDivId) = 0 Then
                        Throw New Exception("Not able to create invoice. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                        Exit Sub
                    End If

                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.OppType = 1
                    objOppBizDocs.FromDate = DateTime.UtcNow
                    objOppBizDocs.ClientTimeZoneOffset = 0


                    'SETS BIZDOC ID
                    '--------------
                    Dim lintAuthorizativeBizDocsId As Long
                    lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                    objOppBizDocs.BizDocId = lintAuthorizativeBizDocsId
                    '--------------

                    'SETS BIZDOC TEMPLETE NEEDS TO BE USED
                    '-------------------------------------
                    objOppBizDocs.byteMode = 1
                    Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                    objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))
                    '-------------------------------------

                    Dim objCommon As New CCommon
                    objCommon.DomainID = DomainID
                    objCommon.Mode = 33
                    objCommon.Str = CStr(lintAuthorizativeBizDocsId)
                    objOppBizDocs.SequenceId = CCommon.ToString(objCommon.GetSingleFieldValue())

                    objCommon = New CCommon
                    objCommon.DomainID = DomainID
                    objCommon.Mode = 34
                    objCommon.Str = CCommon.ToString(lngOppId)
                    objOppBizDocs.RefOrderNo = CCommon.ToString(objCommon.GetSingleFieldValue())
                    objOppBizDocs.bitPartialShipment = True

                    OppBizDocID = objOppBizDocs.SaveBizDoc()

                    If OppBizDocID = 0 Then
                        Throw New Exception("Not able to create invoice. A BizDoc by the same name is already created. Please select another BizDoc !")
                        Exit Sub
                    End If

                    'CREATE JOURNALS
                    '---------------
                    Dim ds As New DataSet
                    Dim dtOppBiDocItems As DataTable

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting

                    dtOppBiDocItems = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, DomainID, dtOppBiDocItems)

                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=CCommon.ToString(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                    Dim objJournalEntries As New JournalEntry
                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                        objJournalEntries.SaveJournalEntriesSales(lngOppId, DomainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), 0)
                    Else
                        objJournalEntries.SaveJournalEntriesSalesNew(lngOppId, DomainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), 0)
                    End If

                    If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitAutolinkUnappliedPayment")) Then
                        Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                        objSalesFulfillment.DomainID = DomainID
                        objSalesFulfillment.UserCntID = UserCntID
                        objSalesFulfillment.ClientTimeZoneOffset = 0
                        objSalesFulfillment.OppId = lngOppId
                        objSalesFulfillment.OppBizDocId = OppBizDocID

                        objSalesFulfillment.AutoLinkUnappliedPayment()
                    End If
                    '---------------

                    objTransaction.Complete()
                End Using


                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow.Workflow()
                objWfA.DomainID = DomainID
                objWfA.UserCntID = UserCntID
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Function GenerateShippingBoxes(DomainID As Long, UserCntID As Long, OppID As Long, FromOppBizDocID As Long) As Long
            Try
                Dim strErrorOppID As String = ""
                Dim strSuccessOppID As String = ""

                Dim objShipping As New Shipping
                Dim objShippingRule As New ShippingRule
                Dim objOppBizDocs As New OppBizDocs

                Dim dblTotalWeight As Double = 0
                Dim lngShippingReportID As Long = 0
                Dim lngBizDocItemId As Long = 0

                objOppBizDocs.OppId = OppID
                objOppBizDocs.BizDocId = FromOppBizDocID
                objOppBizDocs.BizDocItemId = lngBizDocItemId
                objOppBizDocs.ItemClassID = 0
                objOppBizDocs.DomainID = DomainID

                If objShipping Is Nothing Then objShipping = New Shipping()
                Dim ds As New DataSet
                Dim dsFinal As New DataSet
                Dim intBoxCounter As Integer = 0
                Dim blnSameCountry As Boolean = False

                Dim dtBox As New DataTable("Box")
                dtBox.Columns.Add("vcBoxName")
                dtBox.Columns.Add("fltTotalWeight")
                dtBox.Columns.Add("fltHeight")
                dtBox.Columns.Add("fltWidth")
                dtBox.Columns.Add("fltLength")
                dtBox.Columns.Add("numPackageTypeID")
                dtBox.Columns.Add("numServiceTypeID")
                dtBox.Columns.Add("fltDimensionalWeight")

                Dim dtItem As New DataTable("Item")
                dtItem.Columns.Add("vcBoxName")
                dtItem.Columns.Add("numOppBizDocItemID")
                dtItem.Columns.Add("intBoxQty")
                dtItem.Columns.Add("UnitWeight")

                Dim dsItems As New DataSet
                Dim dtItems As New DataTable
                Dim dtShipDetail As DataTable = GetOrderItemShippingRules(OppID, FromOppBizDocID, DomainID)
                If dtShipDetail Is Nothing Then
                    Throw New Exception("Shipping Rule has not been found.")
                End If

                dsItems.Tables.Add(dtShipDetail.Copy)
                If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then

                    If OppID > 0 AndAlso FromOppBizDocID > 0 Then
                        dtItems = dsItems.Tables(0).Clone
                        Dim objOpportunity As New MOpportunity

                        'Dim lngTotalItems As Long = dsItems.Tables(0).Rows.Count
                        Dim bitError As Boolean = False
                        'Dim dtDistinct As DataTable = dsItems.Tables(0).DefaultView.ToTable(True, "numShipClass")
                        For i As Integer = 0 To dsItems.Tables(0).Rows.Count - 1

                            objOpportunity.OpportunityId = 0
                            objOpportunity.DomainID = DomainID
                            objOpportunity.ItemCode = CCommon.ToLong(dsItems.Tables(0).Rows(i)("ItemCode"))
                            If objOpportunity.CheckServiceItemInOrder() = True Then
                                Continue For
                            End If
                            Dim objOpp As New MOpportunity()
                            Dim objCommon As New CCommon()
                            objOpp.OpportunityId = OppID
                            objOpp.Mode = 1
                            Dim dtTable As DataTable = Nothing
                            dtTable = objOpp.GetOpportunityAddress()

                            Dim dtShipFrom As DataTable = Nothing
                            Dim objContacts As New CContacts()
                            objContacts.ContactID = UserCntID
                            objContacts.DomainID = DomainID
                            dtShipFrom = objContacts.GetBillOrgorContAdd()
                            If objCommon Is Nothing Then
                                objCommon = New CCommon()
                            End If

                            ' GET SHIPPING COMPANY INFORMATION
                            Dim dtShipCompany As DataTable = Nothing
                            objOppBizDocs.OppId = OppID
                            objOppBizDocs.ShipClassID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass"))
                            objOppBizDocs.BizDocItemId = CCommon.ToLong(dsItems.Tables(0).Rows(i)("OppBizDocItemID"))

                            'dtShipCompany = objOppBizDocs.GetShippingRuleInfo()
                            'If dtShipCompany IsNot Nothing AndAlso dtShipCompany.Rows.Count > 0 Then

                            'IF Shipping Detail is not available continue process with another order detail
                            If dtTable.Rows.Count = 0 OrElse dtShipFrom.Rows.Count = 0 Then
                                Continue For
                            End If

                            If CCommon.ToLong(dtTable.Rows(0)("Country")) = CCommon.ToLong(dtShipFrom.Rows(0)("vcShipCountry")) Then
                                objOppBizDocs.ShipCompany = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShippingCompanyID1"))
                                'objOppBizDocs.Value2 = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) ' CCommon.ToString(dtShipCompany.Rows(0)("numDomesticShipID"))
                                objOppBizDocs.BoxServiceTypeID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                            Else
                                objOppBizDocs.ShipCompany = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShippingCompanyID2"))
                                'objOppBizDocs.Value2 = CCommon.ToString(dsItems.Tables(0).Rows(i)("InternationalServiceID"))
                                objOppBizDocs.BoxServiceTypeID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                            End If
                            'End If

                            'Get Order Items and Process Shipping Label Generating Logic
                            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs

                            objOppBizDocs.ShippingReportId = 0
                            objOppBizDocs.DomainID = DomainID
                            objOppBizDocs.OppBizDocId = FromOppBizDocID

                            ' FROM DETAIL
                            objOppBizDocs.FromName = CCommon.ToString(dtShipFrom.Rows(0)("vcFirstname")) & " " & CCommon.ToString(dtShipFrom.Rows(0)("vcLastname"))
                            objOppBizDocs.FromCompany = CCommon.ToString(dtShipFrom.Rows(0)("vcCompanyName"))
                            objOppBizDocs.FromPhone = CCommon.ToString(dtShipFrom.Rows(0)("vcPhone"))
                            If dtShipFrom.Rows(0)("vcShipStreet").ToString().Contains(Environment.NewLine) Then
                                Dim strAddressLines As String() = dtShipFrom.Rows(0)("vcShipStreet").ToString().Split(CChar(System.Environment.NewLine)) ' dtShipFrom.Rows(0)("vcShipStreet").ToString().Split(New Char() {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                                objOppBizDocs.FromAddressLine1 = (If(strAddressLines.Length >= 0, strAddressLines(0).ToString(), "")).ToString()
                                objOppBizDocs.FromAddressLine2 = (If(strAddressLines.Length >= 1, strAddressLines(1).ToString(), "")).ToString()
                            Else
                                objOppBizDocs.FromAddressLine1 = CCommon.ToString(dtShipFrom.Rows(0)("vcShipStreet"))
                                objOppBizDocs.FromAddressLine2 = ""
                            End If

                            'objOppBizDocs.FromAddressLine1 = dtShipFrom.Rows[0]["vcShipStreet"].ToString().Substring(dtShipFrom.Rows[0]["vcShipStreet"].ToString().IndexOf(Environment.NewLine)).Replace(Environment.NewLine, " ");
                            'objOppBizDocs.FromAddressLine2 = dtShipFrom.Rows[0]["vcShipStreet"].ToString().Substring(0, dtShipFrom.Rows[0]["vcShipStreet"].ToString().IndexOf(Environment.NewLine));
                            objOppBizDocs.FromCountry = CCommon.ToString(dtShipFrom.Rows(0)("vcShipCountry"))
                            objOppBizDocs.FromState = CCommon.ToString(dtShipFrom.Rows(0)("vcShipState"))
                            objOppBizDocs.FromCity = CCommon.ToString(dtShipFrom.Rows(0)("vcShipCity"))
                            objOppBizDocs.FromZip = CCommon.ToString(dtShipFrom.Rows(0)("vcShipPostCode"))
                            objOppBizDocs.IsFromResidential = False

                            objOppBizDocs.UserCntID = UserCntID
                            ' Order Record Owner
                            objOppBizDocs.strText = Nothing

                            'TO DETAILS
                            objOppBizDocs.ToName = CCommon.ToString(dtTable.Rows(0)("Name"))
                            objOppBizDocs.ToCompany = CCommon.ToString(dtTable.Rows(0)("Company"))
                            objOppBizDocs.ToPhone = CCommon.ToString(dtTable.Rows(0)("Phone"))

                            If dtTable.Rows(0)("Street").ToString().Contains(Environment.NewLine) Then
                                Dim strAddressLines As String() = dtTable.Rows(0)("Street").ToString().Split(CChar(System.Environment.NewLine)) 'dtTable.Rows(0)("Street").ToString().Split(New Char() {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                                objOppBizDocs.ToAddressLine1 = (If(strAddressLines.Length >= 0, strAddressLines(0).ToString(), "")).ToString()
                                objOppBizDocs.ToAddressLine2 = (If(strAddressLines.Length >= 1, strAddressLines(1).ToString(), "")).ToString()
                            Else
                                objOppBizDocs.ToAddressLine1 = CCommon.ToString(dtTable.Rows(0)("Street"))
                                objOppBizDocs.ToAddressLine2 = ""
                            End If

                            objOppBizDocs.ToCity = CCommon.ToString(dtTable.Rows(0)("City"))
                            objOppBizDocs.ToState = CCommon.ToString(dtTable.Rows(0)("State"))
                            objOppBizDocs.ToCountry = CCommon.ToString(dtTable.Rows(0)("Country"))
                            objOppBizDocs.ToZip = CCommon.ToString(dtTable.Rows(0)("PostCode"))
                            objOppBizDocs.IsToResidential = False

                            ' SPECIAL SERVICES
                            objOppBizDocs.IsCOD = False
                            objOppBizDocs.IsDryIce = False
                            objOppBizDocs.IsHoldSaturday = False
                            objOppBizDocs.IsHomeDelivery = False
                            objOppBizDocs.IsInsideDelivery = False
                            objOppBizDocs.IsInsidePickup = False
                            objOppBizDocs.IsReturnShipment = False
                            objOppBizDocs.IsSaturdayDelivery = False
                            objOppBizDocs.IsSaturdayPickup = False
                            objOppBizDocs.CODAmount = CCommon.ToString(0)
                            objOppBizDocs.CODType = ""
                            objOppBizDocs.TotalInsuredValue = 0
                            objOppBizDocs.IsAdditionalHandling = False
                            objOppBizDocs.LargePackage = False
                            objOppBizDocs.DeliveryConfirmation = ""
                            objOppBizDocs.Description = ""

                            Try
                                If lngShippingReportID <= 0 Then
                                    lngShippingReportID = objOppBizDocs.ManageShippingReport()
                                End If

                            Catch ex As Exception
                                strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                Continue For
                            End Try

                            'Update Shipping report address values and create shipping report items 
                            objShipping.DomainID = DomainID
                            objShipping.UserCntID = UserCntID
                            objShipping.ShippingReportId = lngShippingReportID
                            objShipping.OpportunityId = OppID
                            objShipping.OppBizDocId = objOppBizDocs.OppBizDocId
                            objShipping.InvoiceNo = CCommon.ToString(objOppBizDocs.OppBizDocId)

                            'If dtShipCompany IsNot Nothing AndAlso dtShipCompany.Rows.Count > 0 Then
                            objShipping.Provider = CCommon.ToInteger(dsItems.Tables(0).Rows(i)("numShippingCompanyID1"))
                            objShipping.PackagingType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("PackageTypeID"))

                            If CCommon.ToLong(dtTable.Rows(0)("Country")) = CCommon.ToLong(dtShipFrom.Rows(0)("vcShipCountry")) Then
                                objShipping.ServiceType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                                blnSameCountry = True
                            Else
                                objShipping.ServiceType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("InternationalServiceID"))
                            End If
                            'End If

                            'FOR FOLLOWING SERVICE TYPES,IN FEDEX THERE IS ONLY ONE "YOUR PACKAGING" IS AVAILABLE
                            If objShipping.Provider = 91 AndAlso _
                               (objShipping.ServiceType = 15 OrElse objShipping.ServiceType = 16 OrElse _
                                objShipping.ServiceType = 17 OrElse objShipping.ServiceType = 18 OrElse _
                                objShipping.ServiceType = 19) Then

                                objShipping.PackagingType = 21

                            End If
                            '------------------------------------------------------------------------------------

                            Dim dtFields As New DataTable
                            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                            'dtFields.Columns.Add("numItemCode")
                            dtFields.Columns.Add("numBoxID")
                            dtFields.Columns.Add("tintServiceType")
                            dtFields.Columns.Add("dtDeliveryDate")
                            dtFields.Columns.Add("monShippingRate")
                            dtFields.Columns.Add("fltTotalWeight")
                            dtFields.Columns.Add("intNoOfBox")
                            dtFields.Columns.Add("fltHeight")
                            dtFields.Columns.Add("fltWidth")
                            dtFields.Columns.Add("fltLength")
                            dtFields.Columns.Add("fltDimensionalWeight")
                            'dtFields.Columns.Add("numOppBizDocItemID")
                            dtFields.TableName = "Box"

                            Dim lngBoxID As Long = 0
                            Dim strBoxID As String = "0"
                            Dim lngTotalQty As Long = 0
                            Dim dblWidth As Double = 0
                            Dim dblHeight As Double = 0
                            Dim dblLength As Double = 0
                            Dim dblWeight As Double = 0

                            Dim lngPackageTypeID As Long = 0
                            Dim lngServiceTypeID As Long = 0

                            If dtItems.Select("numShipClass = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass")) & _
                                              " AND PackageTypeID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("PackageTypeID")) & _
                                              " AND DomesticServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) & _
                                              " AND InternationalServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("InternationalServiceID"))).Length > 0 Then
                                Continue For
                            Else
                                dtItems.Rows.Clear()
                            End If

                            lngTotalQty = 0

                            'ShippingRuleID
                            'DomesticServiceID"
                            'InternationalServiceID"
                            'PackageTypeID"
                            'numShipClass"
                            objCommon = New CCommon
                            objCommon.DomainID = DomainID
                            objCommon.Mode = 37
                            Dim lngShippingServiceItemID As Long = CCommon.ToLong(objCommon.GetSingleFieldValue())

                            Dim drClass() As DataRow = dsItems.Tables(0).Select("numShipClass = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass")) & _
                                                                                " AND PackageTypeID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("PackageTypeID")) & _
                                                                                " AND DomesticServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) & _
                                                                                " AND ItemCode <> " & lngShippingServiceItemID & _
                                                                                " AND InternationalServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("InternationalServiceID")))
                            For Each dr As DataRow In drClass
                                dtItems.ImportRow(dr)
                                lngTotalQty = lngTotalQty + CCommon.ToLong(dr("numUnitHour"))
                            Next

                            Dim dblBoxQty As Double
                            Dim dblItemQty As Double
                            Dim dblCarryFwdQty As Double = 0
                            Dim dblQtyToBox As Double = 0
                            Dim dblWeightToBox As Double = 0
                            Dim dblDimensionalWeight As Double = 0

                            For iItem As Integer = 0 To dtItems.Rows.Count - 1

                                If objOpportunity Is Nothing Then objOpportunity = New MOpportunity
                                objOpportunity.OpportunityId = 0
                                objOpportunity.DomainID = DomainID
                                objOpportunity.ItemCode = CCommon.ToLong(dsItems.Tables(0).Rows(i)("ItemCode"))
                                If objOpportunity.CheckServiceItemInOrder() = True Then
                                    Continue For
                                End If
                                'If dtItems.Rows.Count > 1 Then
                                '    intCarryFwdQty = 0
                                'End If

                                objOppBizDocs.OppId = OppID
                                objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                objOppBizDocs.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                objOppBizDocs.UnitHour = CDec(IIf(lngTotalQty = 0, CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour")), lngTotalQty))
                                objOppBizDocs.PackageTypeID = CCommon.ToLong(dtItems.Rows(iItem)("PackageTypeID"))
                                objOppBizDocs.DomainID = DomainID
                                lngPackageTypeID = CLng(objOppBizDocs.PackageTypeID)
                                lngServiceTypeID = objShipping.ServiceType

                                Dim dtPackages As DataTable = Nothing
                                Dim intBoxCount As Integer = 0
                                If objShipping.PackagingType <> 21 Then
                                    'ADD ITEMS INTO PACKAGE AS PER PACKAGE RULES
                                    dtPackages = objOppBizDocs.GetPackageInfo()
                                    'If package rule not found then set default 
                                    If dtPackages IsNot Nothing AndAlso dtPackages.Rows.Count > 0 Then
                                        intBoxCount = CInt(Math.Ceiling(objOppBizDocs.UnitHour / If(CInt(dtPackages.Rows(0)("numFromQty")) = 0, 1, CCommon.ToDouble(dtPackages.Rows(0)("numFromQty")))))

                                        dblWeight = CCommon.ToDouble(dtPackages.Rows(0)("fltTotalWeight"))
                                        dblWidth = CCommon.ToDouble(dtPackages.Rows(0)("fltWidth"))
                                        dblHeight = CCommon.ToDouble(dtPackages.Rows(0)("fltHeight"))
                                        dblLength = CCommon.ToDouble(dtPackages.Rows(0)("fltLength"))
                                        dblDimensionalWeight = (If(dblWidth = 0, 1, dblWidth) * If(dblHeight = 0, 1, dblHeight) * If(dblLength = 0, 1, dblLength)) / 166

                                    Else
                                        If strErrorOppID.Contains(objOppBizDocs.OppId.ToString()) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                        'Throw New Exception("Package Rule has not been found.")
                                        bitError = True

                                        Continue For
                                    End If
                                Else
                                    intBoxCount = 1
                                    dblWeight = 1
                                    dblWidth = 1
                                    dblHeight = 1
                                    dblLength = 1
                                    dblDimensionalWeight = (If(dblWidth = 0, 1, dblWidth) * If(dblHeight = 0, 1, dblHeight) * If(dblLength = 0, 1, dblLength)) / 166

                                End If

                                If objOppBizDocs.UnitHour <= 0 Then
                                    If strErrorOppID.Contains(objOppBizDocs.OppId.ToString()) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                    'Throw New Exception("Quantity can not be zero.")

                                    Continue For
                                End If

                                ''Create Blank shipping report and use Shipreportid in reference of box
                                If FromOppBizDocID > 0 And OppID > 0 And objOppBizDocs.ShipCompany > 0 AndAlso lngShippingReportID <= 0 Then
                                    With objOppBizDocs
                                        .DomainID = DomainID
                                        .OppBizDocId = FromOppBizDocID
                                        .ShipCompany = objOppBizDocs.ShipCompany
                                        .UserCntID = UserCntID
                                        ds.Tables.Add(New DataTable())
                                        .strText = ds.GetXml()

                                        Try
                                            .ShippingReportId = .ManageShippingReport
                                        Catch ex As Exception
                                            Continue For
                                        End Try


                                        lngShippingReportID = .ShippingReportId
                                    End With

                                End If

                                '--------------------------------------------------------------------------------

                                'For iCnt As Integer = 0 To dtItems.Rows.Count - 1
                                'Next
                                'dblWeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltWeight"))
                                'dblWidth = CCommon.ToInteger(dtItems.Rows(iItem)("fltWidth"))
                                'dblHeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltHeight"))
                                'dblLength = CCommon.ToInteger(dtItems.Rows(iItem)("fltLength"))

                                dblBoxQty = CCommon.ToDouble(dtPackages.Rows(0)("numFromQty"))
                                dblItemQty = CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour"))

                                If lngTotalQty <> CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour")) Then
                                    dblItemQty = dblCarryFwdQty + CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour"))
                                End If

                                If intBoxCounter = intBoxCount - 1 AndAlso intBoxCounter > 1 Then
                                    Exit For
                                Else
                                    For intCount As Integer = 0 To intBoxCount - 1

                                        Dim dr As DataRow
                                        If dblItemQty <= 0 Then
                                            If dblItemQty = 0 Then dblCarryFwdQty = 0
                                            intBoxCounter = intBoxCounter - 1
                                            Exit For

                                        ElseIf dblItemQty < dblBoxQty Then
                                            intBoxCounter = intBoxCounter + 1
                                            dblCarryFwdQty = dblItemQty

                                            If dblCarryFwdQty > 0 AndAlso dtItems.Rows.Count <> iItem + 1 Then
                                                dr = dtItem.NewRow
                                                dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                                dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))
                                                dr("intBoxQty") = dblCarryFwdQty
                                                dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) ' dblWeight

                                                dtItem.Rows.Add(dr)
                                                dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                                dblQtyToBox = dblCarryFwdQty
                                            Else
                                                ''Check for other Shipping Rule / Package Rule available for Remaining Quantity or not.

                                                Dim dtShipInfoNew As DataTable
                                                objOppBizDocs.OppId = OppID
                                                objOppBizDocs.OppBizDocId = FromOppBizDocID
                                                objOppBizDocs.BizDocItemId = lngBizDocItemId
                                                objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                                objOppBizDocs.UnitHour = CDec(dblCarryFwdQty)
                                                dtShipInfoNew = objOppBizDocs.GetShippingRuleInfo()

                                                ''If available then use that package otherwise same package will be used.
                                                If dtShipInfoNew IsNot Nothing AndAlso dtShipInfoNew.Rows.Count > 0 Then
                                                    objOppBizDocs.OppId = OppID
                                                    objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                                    objOppBizDocs.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                                    objOppBizDocs.UnitHour = CDec(dblCarryFwdQty) 'IIf(lngTotalQty = 0, CCommon.ToLong(dtItems.Rows(iItem)("numUnitHour")), lngTotalQty)
                                                    objOppBizDocs.PackageTypeID = CCommon.ToLong(dtShipInfoNew.Rows(0)("numPackageTypeID")) 'objShipping.PackagingType
                                                    objOppBizDocs.DomainID = DomainID

                                                    'ADD ITEMS INTO PACKAGE AS PER PACKAGE RULES
                                                    Dim dtCarryFPackage As DataTable = Nothing
                                                    dtCarryFPackage = objOppBizDocs.GetPackageInfo()
                                                    'If package rule not found then set default 
                                                    If dtPackages IsNot Nothing AndAlso dtCarryFPackage.Rows.Count > 0 Then
                                                        lngPackageTypeID = CCommon.ToLong(dtCarryFPackage.Rows(0)("numCustomPackageID"))
                                                        lngServiceTypeID = If(blnSameCountry = True, CCommon.ToShort(dtShipInfoNew.Rows(0)("numDomesticShipID")), CCommon.ToShort(dtShipInfoNew.Rows(0)("numInternationalShipID")))

                                                        'Else
                                                        '    dr = dtItem.NewRow
                                                        '    dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                                        '    dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iCnt)("OppBizDocItemID"))
                                                        '    dr("intBoxQty") = intCarryFwdQty
                                                        '    dr("UnitWeight") = dblWeight
                                                        '    dtItem.Rows.Add(dr)
                                                        '    dblTotalWeight = dblTotalWeight + dblWeight
                                                        '    intQtyToBox = intCarryFwdQty
                                                    End If
                                                End If


                                                dr = dtItem.NewRow
                                                dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                                dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))

                                                If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 0 Then
                                                    'Dim drQty As DataRow() = dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                    Dim intExistBoxQty As Integer
                                                    For Each drQty As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                        intExistBoxQty = CInt(intExistBoxQty + CCommon.ToDouble(drQty("intBoxQty")))
                                                    Next
                                                    If dblBoxQty <= intExistBoxQty Then
                                                        Continue For
                                                    Else
                                                        dblCarryFwdQty = dblBoxQty - intExistBoxQty
                                                    End If
                                                End If

                                                dr("intBoxQty") = dblCarryFwdQty
                                                dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight
                                                dtItem.Rows.Add(dr)

                                                'dblWeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltWeight"))
                                                'dblWidth = CCommon.ToInteger(dtItems.Rows(iItem)("fltWidth"))
                                                'dblHeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltHeight"))
                                                'dblLength = CCommon.ToInteger(dtItems.Rows(iItem)("fltLength"))

                                                dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))
                                                'dblTotalWeight = dblTotalWeight + dblWeight
                                                dblQtyToBox = dblCarryFwdQty
                                            End If

                                            dblWeightToBox = dblQtyToBox * CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight
                                        Else

                                            intBoxCounter = intBoxCounter + 1
                                            If intBoxCounter = intBoxCount - 1 AndAlso iItem = dtItems.Rows.Count - 1 Then
                                                'intCarryFwdQty = 0
                                            End If

                                            dr = dtItem.NewRow
                                            dr("vcBoxName") = "Box" & intBoxCounter '(intCount + i + 1).ToString()
                                            dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))

                                            Dim dblExistBoxQty As Double
                                            If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 0 Then

                                                For Each drQty As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                    dblExistBoxQty = dblExistBoxQty + CCommon.ToDouble(drQty("intBoxQty"))
                                                Next
                                                If dblBoxQty <= dblExistBoxQty Then
                                                    Continue For
                                                Else
                                                    dblExistBoxQty = dblBoxQty - dblExistBoxQty
                                                End If
                                            Else
                                                dblExistBoxQty = dblBoxQty - dblCarryFwdQty
                                            End If

                                            dr("intBoxQty") = dblExistBoxQty ' intBoxQty - intCarryFwdQty ' If(intItemQty > intBoxQty AndAlso intCarryFwdQty = 0, intBoxQty - intCarryFwdQty, intItemQty - intBoxQty)
                                            dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                            dtItem.Rows.Add(dr)
                                            dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                            dblQtyToBox = dblBoxQty - dblCarryFwdQty

                                            If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 1 Then
                                                For Each drI As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                    dblWeightToBox = dblWeightToBox + (CCommon.ToDouble(drI("intBoxQty")) * CCommon.ToDouble(drI("UnitWeight")))
                                                Next
                                            Else
                                                dblWeightToBox = (dblQtyToBox * CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")))
                                                'intWeightToBox = (intQtyToBox * dblWeight)
                                            End If

                                        End If

                                        ''Creating Box Entry
                                        'Dim dblFinalWeight As Double
                                        'dblFinalWeight = dblTotalWeight * intQtyToBox 'IIf(intItemQty > intBoxQty, intBoxQty, intItemQty)

                                        dr = dtBox.NewRow
                                        dr("vcBoxName") = "Box" & intBoxCounter '(intCount + i + 1).ToString()
                                        dr("fltTotalWeight") = dblWeightToBox 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltWeight"))
                                        dr("fltLength") = dblLength  'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltLength"))
                                        dr("fltWidth") = dblWidth 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltWidth"))
                                        dr("fltHeight") = dblHeight 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltHeight"))
                                        dr("numPackageTypeID") = lngPackageTypeID
                                        dr("numServiceTypeID") = lngServiceTypeID
                                        dr("fltDimensionalWeight") = dblDimensionalWeight

                                        If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 1 Then
                                            dtBox.Rows.Remove(dtBox.Select("vcBoxName='" & "Box" & intBoxCounter & "'")(0))
                                        End If
                                        dtBox.Rows.Add(dr)

                                        dblWeightToBox = 0
                                        dblTotalWeight = 0
                                        dblItemQty = dblItemQty - dblQtyToBox 'intBoxQty
                                    Next
                                End If
                            Next
                            'lngTotalItems -= 1
                        Next

                        ds.Tables.Add(dtBox)
                        ds.Tables.Add(dtItem)

                        With objOppBizDocs
                            .ShippingReportId = lngShippingReportID
                            .strText = ds.GetXml()
                            .UserCntID = UserCntID
                            .byteMode = 0
                            .PageMode = 1
                            Try
                                .ManageShippingBox()
                            Catch ex As Exception
                                If strErrorOppID.Contains(objOppBizDocs.OppId.ToString()) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                Throw New Exception(objShipping.ErrorMsg)
                            End Try

                        End With

                        If bitError Then ' lngTotalItems <> 0 Then
                            Throw New Exception("Not_AllItemsCreateBox")
                        End If
                    End If
                End If

                Return lngShippingReportID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function GetOrderItemShippingRules(ByVal OppId As Long, _
                                                       ByVal OppBizDocID As Long, _
                                                       ByVal DomainId As Long) As DataTable
            Dim dtResult As DataTable = Nothing
            Try
                Dim dsItems As New DataSet
                Dim dtOrderDetail As New DataTable
                Dim objOppBizDocs As New OppBizDocs

                objOppBizDocs.OppId = OppId
                objOppBizDocs.OppBizDocId = OppBizDocID
                objOppBizDocs.DomainID = DomainId

                dsItems = objOppBizDocs.GetOppInItems()

                'Tracing Part
                '------------------------------------------------------------------------------------------------------
                'Trace.Write("Start Tracing Items detail.")
                'Trace.Write("Is Item Dataset is initialized or not ? : " & IIf(dsItems Is Nothing, "NO", "YES"))
                'Trace.Write("Is Item Dataset has tables or not? : " & IIf(dsItems.Tables.Count > 0, dsItems.Tables.Count, 0))
                'Trace.Write("End Tracing Items detail.")

                '------------------------------------------------------------------------------------------------------

                If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                    Dim dvItems As DataView = dsItems.Tables(0).Copy.DefaultView
                    dvItems.RowFilter = "vcItemType = 'Inventory'"
                    dsItems.Tables.Clear()
                    dsItems.Tables.Add(dvItems.ToTable())
                End If

                If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                    dtOrderDetail.Columns.Add("OppId", GetType(Long))
                    dtOrderDetail.Columns.Add("OppBizDocItemID", GetType(Long))
                    dtOrderDetail.Columns.Add("ItemCode", GetType(Long))
                    dtOrderDetail.Columns.Add("ShippingRuleID", GetType(Long))
                    dtOrderDetail.Columns.Add("numShippingCompanyID1", GetType(Long))
                    dtOrderDetail.Columns.Add("numShippingCompanyID2", GetType(Long))
                    dtOrderDetail.Columns.Add("DomesticServiceID", GetType(Integer))
                    dtOrderDetail.Columns.Add("InternationalServiceID", GetType(Integer))
                    dtOrderDetail.Columns.Add("PackageTypeID", GetType(Integer))
                    dtOrderDetail.Columns.Add("numShipClass", GetType(Long))
                    dtOrderDetail.Columns.Add("numUnitHour", GetType(Long))
                    dtOrderDetail.Columns.Add("fltWeight", GetType(Double))
                    dtOrderDetail.Columns.Add("fltWidth", GetType(Double))
                    dtOrderDetail.Columns.Add("fltHeight", GetType(Double))
                    dtOrderDetail.Columns.Add("fltLength", GetType(Double))

                    Trace.Write("Start Tracing Rules detail.")
                    ' GET SHIPPING RULES AS PER ITEM'S NEEDS
                    Dim dtShipInfo As New DataTable
                    For Each drItem As DataRow In dsItems.Tables(0).Rows
                        objOppBizDocs.OppId = OppId
                        objOppBizDocs.OppBizDocId = OppBizDocID
                        objOppBizDocs.BizDocItemId = CCommon.ToLong(drItem("OppBizDocItemID"))
                        objOppBizDocs.ShipClassID = CCommon.ToLong(drItem("numShipClass"))
                        objOppBizDocs.UnitHour = CCommon.ToLong(drItem("numUnitHour"))
                        dtShipInfo = objOppBizDocs.GetShippingRuleInfo()

                        Trace.Write("Shipping Rule dataset available or not: " & If(dtShipInfo Is Nothing, "NO", "YES, Then its row count is :" & dtShipInfo.Rows.Count))

                        If dtShipInfo IsNot Nothing AndAlso dtShipInfo.Rows.Count > 0 Then
                            Dim drOrderDetail As DataRow
                            drOrderDetail = dtOrderDetail.NewRow

                            drOrderDetail("OppId") = OppId
                            drOrderDetail("OppBizDocItemID") = CCommon.ToLong(drItem("OppBizDocItemID"))
                            drOrderDetail("ItemCode") = CCommon.ToLong(drItem("ItemCode"))
                            drOrderDetail("ShippingRuleID") = CCommon.ToLong(dtShipInfo.Rows(0)("numShippingRuleID"))
                            drOrderDetail("numShippingCompanyID1") = CCommon.ToInteger(dtShipInfo.Rows(0)("numShippingCompanyID1"))
                            drOrderDetail("numShippingCompanyID2") = CCommon.ToInteger(dtShipInfo.Rows(0)("numShippingCompanyID2"))

                            drOrderDetail("DomesticServiceID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numDomesticShipID"))
                            drOrderDetail("InternationalServiceID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numInternationalShipID"))
                            drOrderDetail("PackageTypeID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numPackageTypeID"))
                            drOrderDetail("numShipClass") = CCommon.ToLong(drItem("numShipClass"))
                            drOrderDetail("numUnitHour") = CCommon.ToLong(drItem("numUnitHour"))

                            drOrderDetail("fltWeight") = CCommon.ToDouble(drItem("fltWeight"))
                            drOrderDetail("fltWidth") = CCommon.ToDouble(drItem("fltWidth"))
                            drOrderDetail("fltHeight") = CCommon.ToDouble(drItem("fltHeight"))
                            drOrderDetail("fltLength") = CCommon.ToDouble(drItem("fltLength"))

                            dtOrderDetail.Rows.Add(drOrderDetail)
                            dtOrderDetail.AcceptChanges()
                        Else
                            dtOrderDetail = Nothing
                        End If

                    Next

                    If dtOrderDetail IsNot Nothing Then
                        Dim dtView As DataView = dtOrderDetail.DefaultView
                        dtView.Sort = "numShipClass"
                        dtResult = dtView.ToTable()
                    Else
                        dtResult = Nothing
                    End If
                Else
                    dtResult = Nothing
                End If

                Trace.Write("End Tracing Rules Detail.")

            Catch ex As Exception
                dtResult = Nothing
                Throw ex
                'Response.Write(ex)
            End Try

            Return dtResult
        End Function

        Private Sub ChargeCreditCard(ByVal sfqID As Long, ByVal domainID As Long, ByVal userCntID As Long, ByVal divisionID As Long, ByVal contactID As Long, ByVal oppID As Long, ByVal baseCurrencyID As Long)
            Try
                'TODO: GET DEFAULT CREDIT CARD VALUE
                Dim dtCCInfo As DataTable
                Dim objOppInvoice As New OppInvoice
                objOppInvoice.DomainID = domainID
                objOppInvoice.DivisionID = divisionID
                objOppInvoice.numCCInfoID = 0
                objOppInvoice.IsDefault = True
                objOppInvoice.bitflag = True
                dtCCInfo = objOppInvoice.GetCustomerCCInfo(0, CCommon.ToInteger(contactID), 1)



                If Not dtCCInfo Is Nothing AndAlso dtCCInfo.Rows.Count > 0 Then
                    Dim IsErrorOccured As Boolean = False
                    Dim objQueryStringValues As New QueryStringValues

                    Dim cardHolderName As String = objQueryStringValues.Decrypt(CCommon.ToString(dtCCInfo.Rows(0)("vcCardHolder")))
                    Dim creditCardNo As String = objQueryStringValues.Decrypt(CCommon.ToString(dtCCInfo.Rows(0)("vcCreditCardNo")))
                    Dim cvv2 As String = objQueryStringValues.Decrypt(CCommon.ToString(dtCCInfo.Rows(0)("vcCVV2")))
                    Dim expiryYear As String = CCommon.ToString(dtCCInfo.Rows(0)("intValidYear"))
                    Dim expiryMonth As String = CCommon.ToString(dtCCInfo.Rows(0)("tintValidMonth"))
                    Dim cardTye As Long = CCommon.ToLong(dtCCInfo.Rows(0)("numCardTypeID"))

                    'IF INVOICES ARE NOT CREATED THAN CREATE INVOICE
                    Dim objOpportunity As New MOpportunity
                    objOpportunity.OpportunityId = oppID
                    Dim dsInvoiceCreated As DataSet = objOpportunity.CheckOrderedAndInvoicedOrBilledQty()

                    Dim bitOrderedAndInvoicedBilledQtyNotSame As Boolean = CCommon.ToBool(dsInvoiceCreated.Tables(0).Select("ID=2")(0)("bitTrue"))

                    'CREATE INVOICE FOR REMAINING QTY WHICH NOT YET INVOICED
                    If bitOrderedAndInvoicedBilledQtyNotSame Then
                        Try
                            CreateInvoice(domainID, userCntID, oppID)
                        Catch ex As Exception
                            If ex.Message = "NOT_ALLOWED" Then
                                Throw New Exception("Not able to create invoice. To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!")
                            ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                                Throw New Exception("Not able to create invoice. Only a Sales Order can create a Fulfillment Order")
                            ElseIf ex.Message = "FY_CLOSED" Then
                                Throw New Exception("Not able to create invoice. This transaction can not be posted,Because transactions date belongs to closed financial year.")
                            ElseIf ex.Message = "AlreadyInvoice" Then
                                Throw New Exception("Not able to create invoice. You can’t create a Invoice against a Fulfillment Order that already contains a Invoice")
                            ElseIf ex.Message = "AlreadyPackingSlip" Then
                                Throw New Exception("Not able to create invoice. You can’t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip")
                            ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                                Throw New Exception("Not able to create invoice. You can’t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip")
                            ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                                Throw New Exception("Not able to create invoice. Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section")
                            Else
                                Throw New Exception("Not able to create invoice. Unknown error ocurred.")
                            End If
                        End Try
                    End If


                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.DomainID = domainID
                    objOppBizDocs.OppId = oppID
                    Dim dtInvoices As DataTable = objOppBizDocs.GetUnPaidInvoices()

                    If Not dtInvoices Is Nothing AndAlso dtInvoices.Rows.Count > 0 Then
                        'validate account mapping
                        Dim UndepositedFundAccountID As Long = ChartOfAccounting.GetDefaultAccount("UF", domainID) 'Undeposited Fund
                        If Not UndepositedFundAccountID > 0 Then
                            Throw New Exception("Please Map Default Undeposited Funds Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"".")
                        End If

                        Dim lngDefaultAPAccount As Long = ChartOfAccounting.GetDefaultAccount("PL", domainID)
                        If Not lngDefaultAPAccount > 0 Then
                            Throw New Exception("Please Map Default Payroll Liablity Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"".")
                        End If

                        Dim lngEmpPayRollAccount As Long = ChartOfAccounting.GetDefaultAccount("EP", domainID)
                        If Not lngEmpPayRollAccount > 0 Then
                            Throw New Exception("Please Map Default Employee Payroll Expense Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"" .")
                        End If

                        Dim lngCustomerARAccount As Long = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", domainID, divisionID)
                        If lngCustomerARAccount = 0 Then
                            Throw New Exception("Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""  To Save Amount.")
                        End If

                        Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", domainID)
                        If lngDefaultForeignExchangeAccountID = 0 Then
                            Throw New Exception("Please Map Default Foreign Exchange Gain/Loss Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"" .")
                        End If



                        'If ProcessCard = True Then
                        Dim dtTable As DataTable
                        Dim objCommon As New CCommon
                        objCommon.DomainID = domainID
                        dtTable = objCommon.GetPaymentGatewayDetails
                        If dtTable Is Nothing Or dtTable.Rows.Count = 0 Then
                            Throw New Exception("Please Configure Payment Gateway")
                        Else
                            Dim objPaymentGateway As New PaymentGateway
                            objPaymentGateway.DomainID = domainID

                            For Each drInvoice As DataRow In dtInvoices.Rows
                                Try
                                    Dim ResponseMessage As String = ""
                                    Dim ReturnTransactionID As String = ""
                                    Dim responseCode As String = ""
                                    Dim oppBizDocID As Long = CCommon.ToLong(drInvoice("numOppBizDocsId"))
                                    Dim decAmount As Decimal = CCommon.ToDecimal(drInvoice("monAmountToPay"))

                                    If objPaymentGateway.GatewayTransaction(decAmount, cardHolderName, creditCardNo, cvv2, expiryMonth, expiryYear, contactID, ResponseMessage, ReturnTransactionID, responseCode, False, "") Then

                                        'Insert Transaction details into TransactionHistory table
                                        Dim objTransactionHistory As New TransactionHistory
                                        objTransactionHistory.TransHistoryID = 0
                                        objTransactionHistory.DivisionID = divisionID
                                        objTransactionHistory.ContactID = contactID
                                        objTransactionHistory.OppID = oppID
                                        objTransactionHistory.OppBizDocsID = oppBizDocID
                                        objTransactionHistory.TransactionID = CCommon.ToString(ReturnTransactionID)
                                        objTransactionHistory.TransactionStatus = 2 'Authorize & Charge
                                        objTransactionHistory.PGResponse = ResponseMessage
                                        objTransactionHistory.Type = 2 '1= bizcart & 2 =Internal
                                        objTransactionHistory.CapturedAmt = decAmount
                                        objTransactionHistory.RefundAmt = 0
                                        objTransactionHistory.CardHolder = objCommon.Encrypt(cardHolderName)
                                        objTransactionHistory.CreditCardNo = objCommon.Encrypt(creditCardNo)
                                        objTransactionHistory.Cvv2 = objCommon.Encrypt(cvv2)
                                        objTransactionHistory.ValidMonth = CCommon.ToShort(expiryMonth)
                                        objTransactionHistory.ValidYear = CCommon.ToShort(expiryYear)
                                        objTransactionHistory.SignatureFile = ""
                                        objTransactionHistory.UserCntID = userCntID
                                        objTransactionHistory.DomainID = domainID
                                        objTransactionHistory.CardType = cardTye
                                        objTransactionHistory.ResponseCode = responseCode
                                        Dim lngTransactionHistoryID As Long = objTransactionHistory.ManageTransactionHistory()

                                        Dim lngDepositeID1 As Integer
                                        If decAmount > 0 Then
                                            Try
                                                Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})

                                                    Dim exchangeRate As Double = 1
                                                    Dim objCurrency As New CurrencyRates
                                                    objCurrency.DomainID = domainID
                                                    objCurrency.CurrencyID = CCommon.ToLong(drInvoice("numCurrencyID"))
                                                    objCurrency.GetAll = False
                                                    Dim dt As DataTable = objCurrency.GetCurrencyWithRates()
                                                    If dt.Rows.Count > 0 Then
                                                        exchangeRate = CCommon.ToDouble(dt.Rows(0)("fltExchangeRate"))
                                                    End If

                                                    exchangeRate = If(exchangeRate = 0, 1, exchangeRate)

                                                    Dim dtInvoiceAmount As DataTable = GetItems(oppID, oppBizDocID, decAmount)

                                                    Dim depositID As Long = SaveDeposite(domainID, userCntID, divisionID, oppBizDocID, lngTransactionHistoryID, CCommon.ToLong(drInvoice("numCurrencyID")), dtInvoiceAmount, decAmount, exchangeRate)

                                                    If depositID > 0 Then
                                                        Dim lngJournalID As Long = SaveDataToHeader(domainID, userCntID, depositID, decAmount, 0, 0, exchangeRate)
                                                        SaveDataToGeneralJournalDetailsForCashAndCheCks(domainID, divisionID, depositID, CCommon.ToLong(drInvoice("numCurrencyID")), decAmount, exchangeRate, lngJournalID, baseCurrencyID, oppBizDocID)

                                                        Dim objRule As New OrderAutoRules
                                                        objRule.GenerateAutoPO(oppBizDocID)
                                                    End If

                                                    objTransaction.Complete()
                                                End Using
                                            Catch ex As Exception
                                                UpdateSalesFulfillmentStatusAndLog(domainID, sfqID, "Error ocuured while creating journal entries", 2, False)
                                                IsErrorOccured = True
                                            End Try
                                        Else
                                            UpdateSalesFulfillmentStatusAndLog(domainID, sfqID, "Amount to pay must be greater than 0.", 2, False)
                                            IsErrorOccured = True
                                        End If
                                    Else
                                        UpdateSalesFulfillmentStatusAndLog(domainID, sfqID, "Error ocuured while charging credit card." & ResponseMessage, 2, False)
                                        IsErrorOccured = True
                                    End If
                                Catch ex As Exception
                                    UpdateSalesFulfillmentStatusAndLog(domainID, sfqID, "Error ocuured while processing payment.", 2, False)
                                    IsErrorOccured = True
                                End Try
                            Next
                        End If

                        If Not IsErrorOccured Then
                            UpdateSalesFulfillmentStatusAndLog(domainID, sfqID, "Credit card charged against invoice(s).", 2, True)
                        End If
                    Else
                        UpdateSalesFulfillmentStatusAndLog(domainID, sfqID, "Credit card alredy charged against invoice(s).", 2, True)
                    End If
                Else
                    Throw New Exception("Credit card information is not available for sales order contact.")
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub ChargeCreditCardMassSalesFulfillment(ByVal domainID As Long, ByVal userCntID As Long, ByVal divisionID As Long, ByVal contactID As Long, ByVal oppID As Long, ByVal oppBizDocID As Long, ByVal monAmountToPay As Decimal, ByVal baseCurrencyID As Long, ByVal numCurrencyID As Long)
            Try
                'TODO: GET DEFAULT CREDIT CARD VALUE
                Dim dtCCInfo As DataTable
                Dim objOppInvoice As New OppInvoice
                objOppInvoice.DomainID = domainID
                objOppInvoice.DivisionID = divisionID
                objOppInvoice.numCCInfoID = 0
                objOppInvoice.IsDefault = True
                objOppInvoice.bitflag = True
                dtCCInfo = objOppInvoice.GetCustomerCCInfo(0, CCommon.ToInteger(contactID), 1)

                If Not dtCCInfo Is Nothing AndAlso dtCCInfo.Rows.Count > 0 Then
                    Dim IsErrorOccured As Boolean = False
                    Dim objQueryStringValues As New QueryStringValues

                    Dim cardHolderName As String = objQueryStringValues.Decrypt(CCommon.ToString(dtCCInfo.Rows(0)("vcCardHolder")))
                    Dim creditCardNo As String = objQueryStringValues.Decrypt(CCommon.ToString(dtCCInfo.Rows(0)("vcCreditCardNo")))
                    Dim cvv2 As String = objQueryStringValues.Decrypt(CCommon.ToString(dtCCInfo.Rows(0)("vcCVV2")))
                    Dim expiryYear As String = CCommon.ToString(dtCCInfo.Rows(0)("intValidYear"))
                    Dim expiryMonth As String = CCommon.ToString(dtCCInfo.Rows(0)("tintValidMonth"))
                    Dim cardTye As Long = CCommon.ToLong(dtCCInfo.Rows(0)("numCardTypeID"))


                    'If ProcessCard = True Then
                    Dim dtTable As DataTable
                    Dim objCommon As New CCommon
                    objCommon.DomainID = domainID
                    dtTable = objCommon.GetPaymentGatewayDetails
                    If dtTable Is Nothing Or dtTable.Rows.Count = 0 Then
                        Throw New Exception("Please Configure Payment Gateway")
                    Else
                        Dim objPaymentGateway As New PaymentGateway
                        objPaymentGateway.DomainID = domainID

                        Try
                            Dim ResponseMessage As String = ""
                            Dim ReturnTransactionID As String = ""
                            Dim responseCode As String = ""
                            Dim decAmount As Decimal = monAmountToPay

                            If objPaymentGateway.GatewayTransaction(decAmount, cardHolderName, creditCardNo, cvv2, expiryMonth, expiryYear, contactID, ResponseMessage, ReturnTransactionID, responseCode, False, "") Then

                                'Insert Transaction details into TransactionHistory table
                                Dim objTransactionHistory As New TransactionHistory
                                objTransactionHistory.TransHistoryID = 0
                                objTransactionHistory.DivisionID = divisionID
                                objTransactionHistory.ContactID = contactID
                                objTransactionHistory.OppID = oppID
                                objTransactionHistory.OppBizDocsID = oppBizDocID
                                objTransactionHistory.TransactionID = CCommon.ToString(ReturnTransactionID)
                                objTransactionHistory.TransactionStatus = 2 'Authorize & Charge
                                objTransactionHistory.PGResponse = ResponseMessage
                                objTransactionHistory.Type = 2 '1= bizcart & 2 =Internal
                                objTransactionHistory.CapturedAmt = decAmount
                                objTransactionHistory.RefundAmt = 0
                                objTransactionHistory.CardHolder = objCommon.Encrypt(cardHolderName)
                                objTransactionHistory.CreditCardNo = objCommon.Encrypt(creditCardNo)
                                objTransactionHistory.Cvv2 = objCommon.Encrypt(cvv2)
                                objTransactionHistory.ValidMonth = CCommon.ToShort(expiryMonth)
                                objTransactionHistory.ValidYear = CCommon.ToShort(expiryYear)
                                objTransactionHistory.SignatureFile = ""
                                objTransactionHistory.UserCntID = userCntID
                                objTransactionHistory.DomainID = domainID
                                objTransactionHistory.CardType = cardTye
                                objTransactionHistory.ResponseCode = responseCode
                                Dim lngTransactionHistoryID As Long = objTransactionHistory.ManageTransactionHistory()

                                Dim lngDepositeID1 As Integer
                                If decAmount > 0 Then
                                    Try
                                        Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})

                                            Dim exchangeRate As Double = 1
                                            Dim objCurrency As New CurrencyRates
                                            objCurrency.DomainID = domainID
                                            objCurrency.CurrencyID = numCurrencyID
                                            objCurrency.GetAll = False
                                            Dim dt As DataTable = objCurrency.GetCurrencyWithRates()
                                            If dt.Rows.Count > 0 Then
                                                exchangeRate = CCommon.ToDouble(dt.Rows(0)("fltExchangeRate"))
                                            End If

                                            exchangeRate = If(exchangeRate = 0, 1, exchangeRate)

                                            Dim dtInvoiceAmount As DataTable = GetItems(oppID, oppBizDocID, decAmount)

                                            Dim depositID As Long = SaveDeposite(domainID, userCntID, divisionID, oppBizDocID, lngTransactionHistoryID, numCurrencyID, dtInvoiceAmount, decAmount, exchangeRate)

                                            If depositID > 0 Then
                                                Dim lngJournalID As Long = SaveDataToHeader(domainID, userCntID, depositID, decAmount, 0, 0, exchangeRate)
                                                SaveDataToGeneralJournalDetailsForCashAndCheCks(domainID, divisionID, depositID, numCurrencyID, decAmount, exchangeRate, lngJournalID, baseCurrencyID, oppBizDocID)

                                                Dim objRule As New OrderAutoRules
                                                objRule.GenerateAutoPO(oppBizDocID)
                                            End If

                                            objTransaction.Complete()
                                        End Using
                                    Catch ex As Exception
                                        Throw New Exception("Error ocuured while creating journal entries")
                                    End Try
                                Else
                                    Throw New Exception("Amount to pay must be greater than 0.")
                                End If
                            Else
                                Throw New Exception("Error ocuured while charging credit card:" & ResponseMessage)
                            End If
                        Catch ex As Exception
                            Throw New Exception("Error ocuured while processing payment.")
                        End Try
                    End If
                Else
                    Throw New Exception("Credit card information is not available for sales order contact.")
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Function GetItems(ByVal oppID As Long, ByVal oppBizDocID As Long, ByVal decAmount As Decimal) As DataTable
            Try
                Dim dr As DataRow
                Dim dtMain As New DataTable
                Dim dtCredits As New DataTable
                Dim dtDeposit As New DataTable

                CCommon.AddColumnsToDataTable(dtMain, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid")
                CCommon.AddColumnsToDataTable(dtCredits, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid")
                CCommon.AddColumnsToDataTable(dtDeposit, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid")

                dr = dtMain.NewRow
                dr("numDepositeDetailID") = 0
                dr("numOppBizDocsID") = oppBizDocID
                dr("numOppID") = oppID
                dr("monAmountPaid") = decAmount
                dtMain.Rows.Add(dr)

                For Each drMain As DataRow In dtMain.Rows
                    If decAmount > 0 Then
                        Dim monAmountPaid As Decimal = CCommon.ToDecimal(drMain("monAmountPaid"))

                        If monAmountPaid > 0 Then
                            dr = dtDeposit.NewRow

                            dr("numDepositeDetailID") = 0
                            dr("numOppBizDocsID") = oppBizDocID
                            dr("numOppID") = oppID

                            If decAmount > monAmountPaid Then
                                dr("monAmountPaid") = monAmountPaid
                                decAmount = decAmount - monAmountPaid
                            Else
                                dr("monAmountPaid") = decAmount
                                decAmount = 0
                            End If

                            drMain("monAmountPaid") = monAmountPaid - CCommon.ToDecimal(dr("monAmountPaid"))
                            dtDeposit.Rows.Add(dr)
                        End If
                    Else
                        Exit For
                    End If
                Next

                dtMain.AcceptChanges()


                Return dtDeposit

            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Function SaveDataToHeader(ByVal domainID As Long, ByVal userCntID As Long, ByVal depositeID As Long, ByVal p_Amount As Decimal, ByVal p_OppBizDocId As Integer, ByVal p_OppId As Integer, ByVal fltExchangeRate As Double) As Long
            Try
                Dim objJEHeader As New JournalEntryHeader
                Dim lngJournalID As Long
                With objJEHeader
                    .JournalId = 0
                    .RecurringId = 0
                    .EntryDate = New DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0)
                    .Description = "Payment received:"
                    .Amount = p_Amount * CCommon.ToDecimal(IIf(fltExchangeRate = 0.0, 1, fltExchangeRate))
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0
                    .OppId = p_OppId
                    .OppBizDocsId = p_OppBizDocId
                    .DepositId = depositeID
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = False
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = userCntID
                    .DomainID = domainID
                End With

                Return objJEHeader.Save()
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub SaveDataToGeneralJournalDetailsForCashAndCheCks(ByVal domainID As Long, ByVal divisionID As Long, ByVal depositeID As Long, ByVal currencyID As Long, ByVal p_Amount As Decimal, ByVal FltExchangeRate As Double, ByVal lngJournalID As Long, ByVal baseCurrencyID As Long, ByVal lngBizDocID As Long, Optional ByVal paymentMethod As Long = 1)
            Try

                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()
                Dim TotalAmountAtExchangeRateOfOrder As Decimal
                Dim TotalAmountVariationDueToCurrencyExRate As Decimal = 0


                'i.e foreign currency payment
                If FltExchangeRate <> 1 Then
                    TotalAmountAtExchangeRateOfOrder = 0
                    TotalAmountAtExchangeRateOfOrder = TotalAmountAtExchangeRateOfOrder + (p_Amount * CCommon.ToDecimal(FltExchangeRate))

                    If TotalAmountAtExchangeRateOfOrder > 0 Then
                        TotalAmountVariationDueToCurrencyExRate = TotalAmountAtExchangeRateOfOrder - (p_Amount * CCommon.ToDecimal(FltExchangeRate))
                        If Math.Abs(TotalAmountVariationDueToCurrencyExRate) < 0.0 Then
                            TotalAmountVariationDueToCurrencyExRate = 0
                        End If
                    End If
                End If



                'Debit : Undeposit Fund (Amount)
                objJE = New JournalEntryNew()


                objJE.TransactionId = 0
                objJE.DebitAmt = p_Amount * CCommon.ToDecimal(FltExchangeRate) 'total amount at current exchange rate
                objJE.CreditAmt = 0
                objJE.Description = ""
                objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("UF", domainID)
                objJE.CustomerId = divisionID
                objJE.MainDeposit = True
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = paymentMethod
                objJE.Reconcile = False
                objJE.CurrencyID = currencyID
                objJE.FltExchangeRate = FltExchangeRate
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositHeader)
                objJE.ReferenceID = depositeID

                objJEList.Add(objJE)

                'For Each dr As DataRow In dtItems.Rows
                'Credit: Customer A/R With (Amount)
                Dim objOppBizDocs As New OppBizDocs
                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = CCommon.ToDecimal(IIf(TotalAmountAtExchangeRateOfOrder > 0, TotalAmountAtExchangeRateOfOrder, p_Amount * CCommon.ToDecimal(FltExchangeRate)))

                objOppBizDocs.OppBizDocId = lngBizDocID
                Dim lngARAccountId As Long = CCommon.ToLong(objOppBizDocs.GetBizDocARAccountID())

                If lngARAccountId = 0 Then
                    lngARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", domainID, divisionID)
                End If

                objJE.ChartAcntId = lngARAccountId
                objJE.Description = "Payment received-Credited to Customer A/R" '"Credit Customer's AR account"
                objJE.CustomerId = divisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = ""
                objJE.Reference = ""
                objJE.PaymentMethod = paymentMethod
                objJE.Reconcile = False
                objJE.CurrencyID = currencyID
                objJE.FltExchangeRate = FltExchangeRate
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositDetail)
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                If currencyID > 0 And Math.Abs(TotalAmountVariationDueToCurrencyExRate) > 0 Then
                    If currencyID <> baseCurrencyID And FltExchangeRate > 0.0 Then 'Foreign Currency Transaction

                        Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", domainID)

                        'Credit: Customer A/R With (Amount)
                        objJE = New JournalEntryNew()
                        objJE.TransactionId = 0

                        ' difference in amount due to exchange rate variation between order placed exchange rate with Entered exchange rate
                        If TotalAmountVariationDueToCurrencyExRate < 0 Then
                            objJE.DebitAmt = 0
                            objJE.CreditAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                        ElseIf TotalAmountVariationDueToCurrencyExRate > 0 Then
                            objJE.DebitAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                            objJE.CreditAmt = 0
                        End If

                        objJE.ChartAcntId = lngDefaultForeignExchangeAccountID
                        objJE.Description = "Exchange Gain/Loss"
                        objJE.CustomerId = divisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = ""
                        objJE.Reference = ""
                        objJE.PaymentMethod = paymentMethod
                        objJE.Reconcile = False
                        objJE.CurrencyID = currencyID
                        objJE.FltExchangeRate = FltExchangeRate
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositDetail)
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If
                End If

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, domainID)

            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Function SaveDeposite(ByVal domainID As Long, ByVal userCntID As Long, ByVal divisionID As Long, ByVal oppBizDocID As Long, ByVal lngTransactionHistoryID As Long, ByVal currencyID As Long, ByVal dtInvoices As DataTable, ByVal decAmount As Decimal, ByVal exchangeRate As Double, Optional ByVal paymentMethod As Long = 1) As Long
            Try
                Dim objCommon As New CCommon
                objCommon.DomainID = domainID
                objCommon.UserCntID = userCntID
                objCommon.DivisionID = divisionID


                Dim objMakeDeposit As New MakeDeposit

                objMakeDeposit.DivisionId = CCommon.ToInteger(divisionID)
                objMakeDeposit.Entry_Date = DateTime.Now.Date
                objMakeDeposit.TransactionHistoryID = lngTransactionHistoryID
                objMakeDeposit.PaymentMethod = paymentMethod
                objMakeDeposit.DepositeToType = 2
                objMakeDeposit.numAmount = decAmount
                objMakeDeposit.AccountId = CCommon.ToInteger(ChartOfAccounting.GetDefaultAccount("UF", domainID))
                objMakeDeposit.RecurringId = 0
                objMakeDeposit.DepositId = 0
                objMakeDeposit.UserCntID = userCntID
                objMakeDeposit.DomainID = domainID

                Dim ds As New DataSet
                Dim str As String = ""

                If dtInvoices.Rows.Count > 0 Then
                    ds.Tables.Add(dtInvoices.Copy)
                    ds.Tables(0).TableName = "Item"
                    str = ds.GetXml()
                End If

                objMakeDeposit.StrItems = str
                objMakeDeposit.Mode = 0
                objMakeDeposit.DepositePage = 2
                objMakeDeposit.CurrencyID = currencyID
                objMakeDeposit.ExchangeRate = exchangeRate
                objMakeDeposit.AccountClass = objCommon.GetAccountingClass()
                Dim lngDepositID As Long = objMakeDeposit.SaveDataToMakeDepositDetails()

                ''AUthor   :sachin sadhu||Date:22ndAug2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking

                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                objWfA.DomainID = domainID
                objWfA.UserCntID = userCntID
                objWfA.RecordID = oppBizDocID
                objWfA.SaveWFBizDocQueue()

                'end of code

                Return lngDepositID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub GenerateShippingLabelAndTrackingNo(DomainID As Long, UserCntID As Long, OppID As Long, BizDocID As Long)
            Try
                Dim objShippingReport As New BACRM.BusinessLogic.ItemShipping.ShippingReport
                objShippingReport.DomainID = DomainID
                objShippingReport.UserCntID = UserCntID
                objShippingReport.OppID = OppID
                objShippingReport.OppBizDocId = BizDocID
                Dim lngShippingReportID As Long = objShippingReport.Save()

                Dim objShipping As New Shipping
                objShipping.GenerateShippingLabel(OppID, BizDocID, lngShippingReportID, DomainID, UserCntID)

                If CCommon.ToString(objShipping.ErrorMsg).Trim.Length > 0 Then
                    Throw New Exception(CCommon.ToString(objShipping.ErrorMsg))
                End If

            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub BluepayCustomPaymentForm(ByVal oppID As Long, ByVal oppBizDocID As Long, ByVal paymentType As String, ByVal transactionID As String, ByVal decAmount As Decimal, ByVal message As String, ByVal cardType As String)
            Try
                'paymentType: ACH - ACH transactions, CREDIT - credit card 

                Dim objMOpportunity As New MOpportunity
                objMOpportunity.OpportunityId = oppID
                objMOpportunity.OppBizDocID = oppBizDocID
                Dim dt As DataTable = objMOpportunity.GetOrderDetailForBluePayHPF()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim lngDomainID As Long = CCommon.ToLong(dt.Rows(0)("numDomainId"))
                    Dim divisionID As Long = CCommon.ToLong(dt.Rows(0)("numDivisionId"))
                    Dim contactID As Long = CCommon.ToLong(dt.Rows(0)("numContactId"))
                    Dim numCurrencyID As Long = CCommon.ToLong(dt.Rows(0)("numCurrencyID"))
                    Dim baseCurrencyID As Long = CCommon.ToLong(dt.Rows(0)("numBaseCurrencyID"))
                    Dim exchangeRate As Double = CCommon.ToDouble(dt.Rows(0)("fltExchangeRate"))
                    Me.BluePaySuccessURL = CCommon.ToString(dt.Rows(0)("vcBluePaySuccessURL"))
                    Me.BluePayDeclineURL = CCommon.ToString(dt.Rows(0)("vcBluePayDeclineURL"))
                    Dim lngTransactionHistoryID As Long = 0
                  
                    If decAmount > 0 Then
                        Try
                            Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                If paymentType = "CREDIT" Then
                                    Dim objTransactionHistory As New TransactionHistory
                                    objTransactionHistory.TransHistoryID = 0
                                    objTransactionHistory.DivisionID = divisionID
                                    objTransactionHistory.ContactID = contactID
                                    objTransactionHistory.OppID = oppID
                                    objTransactionHistory.OppBizDocsID = oppBizDocID
                                    objTransactionHistory.TransactionID = transactionID

                                    objTransactionHistory.PGResponse = ""
                                    objTransactionHistory.Type = 3
                                    objTransactionHistory.TransactionStatus = 2 'Authorize & Charge
                                    objTransactionHistory.CapturedAmt = decAmount
                                    objTransactionHistory.RefundAmt = 0
                                    objTransactionHistory.CardHolder = ""
                                    objTransactionHistory.CreditCardNo = ""
                                    objTransactionHistory.Cvv2 = ""
                                    objTransactionHistory.ValidMonth = 0
                                    objTransactionHistory.ValidYear = 0
                                    objTransactionHistory.SignatureFile = ""
                                    objTransactionHistory.UserCntID = 0
                                    objTransactionHistory.DomainID = lngDomainID

                                    If cardType.ToLower() = "visa" Then
                                        objTransactionHistory.CardType = 14882
                                    ElseIf cardType.ToLower() = "mastercard" Then
                                        objTransactionHistory.CardType = 14883
                                    ElseIf cardType.ToLower() = "amex" Then
                                        objTransactionHistory.CardType = 14884
                                    ElseIf cardType.ToLower() = "discover" Then
                                        objTransactionHistory.CardType = 14885
                                    End If

                                    objTransactionHistory.ResponseCode = ""
                                    lngTransactionHistoryID = objTransactionHistory.ManageTransactionHistory()
                                End If

                                exchangeRate = If(exchangeRate = 0, 1, exchangeRate)

                                Dim dtInvoiceAmount As DataTable = GetItems(oppID, oppBizDocID, decAmount)

                                Dim depositID As Long = SaveDeposite(lngDomainID, 0, divisionID, oppBizDocID, lngTransactionHistoryID, numCurrencyID, dtInvoiceAmount, decAmount, exchangeRate, paymentMethod:=CLng(IIf(paymentType = "ACH", 2, 1)))

                                If depositID > 0 Then
                                    Dim lngJournalID As Long = SaveDataToHeader(lngDomainID, 0, depositID, decAmount, 0, 0, exchangeRate)
                                    SaveDataToGeneralJournalDetailsForCashAndCheCks(lngDomainID, divisionID, depositID, numCurrencyID, decAmount, exchangeRate, lngJournalID, baseCurrencyID, oppBizDocID, paymentMethod:=CLng(IIf(paymentType = "ACH", 2, 1)))

                                    Dim objRule As New OrderAutoRules
                                    objRule.GenerateAutoPO(oppBizDocID)
                                End If

                                objTransaction.Complete()
                            End Using
                        Catch ex As Exception
                            Throw New Exception("Error ocuured while creating journal entries")
                        End Try
                    Else
                        Throw New Exception("Amount to pay must be greater than 0.")
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace