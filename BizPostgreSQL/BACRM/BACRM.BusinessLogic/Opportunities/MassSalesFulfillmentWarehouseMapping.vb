﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic


Namespace BACRM.BusinessLogic.Opportunities

    Public Class MassSalesFulfillmentWarehouseMapping
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property ID As Long
        Public Property OrderSource As Long
        Public Property SourceType As Short
        Public Property CountryID As Long
        Public Property StateIDs As String
        Public Property WarehousePriorities As String
        Public Property PageIndex As Integer
        Public Property PageSize As Integer
        Public Property TotalRecords As Integer
        Public Property ShipToCountry As Long
        Public Property ShipToState As Long

#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderSource", OrderSource, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintSourceType", SourceType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numCountryID", CountryID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcStateIDs", StateIDs, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcWarehousePriorities", WarehousePriorities, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_MassSalesFulfillmentWM_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numID", ID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_MassSalesFulfillmentWM_Delete", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetByDomain(ByVal filterSources As String, ByVal filterCountries As String, ByVal filterStates As String) As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPageIndex", PageIndex, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numPageSize", PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@vcSources", filterSources, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcCountries", filterCountries, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcStates", filterStates, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillmentWM_Get", sqlParams.ToArray())

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Me.TotalRecords = CCommon.ToInteger(dt.Rows(0)("numTotalRecords"))
                End If

                Return dt
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetWarehousePriority() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderSource", OrderSource, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintSourceType", SourceType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numCountryID", ShipToCountry, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numStateID", ShipToState, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_MassSalesFulfillmentWM_GetWarehousePriority", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
#End Region

    End Class

End Namespace


