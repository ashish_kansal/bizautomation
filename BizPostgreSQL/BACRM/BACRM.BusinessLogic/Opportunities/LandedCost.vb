﻿Option Explicit On
Option Strict On
Imports BACRM.BusinessLogic.Admin
Imports System.Web
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Accounting
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer

Namespace BACRM.BusinessLogic.Opportunities
    Public Class LandedCost
        Inherits BACRM.BusinessLogic.CBusinessBase
        Private _LanedCostDefault As String
        Public Property LanedCostDefault() As String
            Get
                Return _LanedCostDefault
            End Get
            Set(value As String)
                _LanedCostDefault = value
            End Set
        End Property
        Private _LCVendorId As Long
        Public Property LCVendorId() As Long
            Get
                Return _LCVendorId
            End Get
            Set(ByVal value As Long)
                _LCVendorId = value
            End Set
        End Property

        Private _VendorId As Long
        Public Property VendorId() As Long
            Get
                Return _VendorId
            End Get
            Set(ByVal value As Long)
                _VendorId = value
            End Set
        End Property

        Private _Description As String
        Public Property Description() As String
            Get
                Return _Description
            End Get
            Set(ByVal value As String)
                _Description = value
            End Set
        End Property

        Private _AccountId As Long
        Public Property AccountId() As Long
            Get
                Return _AccountId
            End Get
            Set(ByVal value As Long)
                _AccountId = value
            End Set
        End Property

        Private _strItems As String
        Public Property strItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal value As String)
                _strItems = value
            End Set
        End Property

        Private _OppID As Long
        Public Property OppID() As Long
            Get
                Return _OppID
            End Get
            Set(ByVal value As Long)
                _OppID = value
            End Set
        End Property

#Region "Landed Cost Expense"
        Function GetLandedCostExpenseAccount() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_LandedCostExpenseAccountGet", objParam, True)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function SaveLandedCostExpenseAccount() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@strItems", strItems, NpgsqlTypes.NpgsqlDbType.Text))
                    .Add(SqlDAL.Add_Parameter("@vcLanedCostDefault", LanedCostDefault, NpgsqlTypes.NpgsqlDbType.Text))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_LandedCostExpenseAccountSave", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "Laned Cost Vendors"
        Function GetLandedCostVendors() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_LandedCostVendorsGet", objParam, True)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function SaveLandedCostVendors() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numVendorId", _VendorId, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_LandedCostVendorsSave", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DeleteLandedCostVendors() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numLCVendorId", _LCVendorId, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_LandedCostVendorsDelete", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

#Region "Landed Cost Bill"
        Function GetLandedCostBillDetails() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", _OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_LandedCostBillDetails", objParam, True)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function LandedCostOpportunityUpdate(ByVal vcLanedCost As String) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppID", _OppID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcLanedCost", vcLanedCost, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                End With
                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_LandedCostOpportunityUpdate", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

        Sub LandedCoseOpportunityUpdate(p1 As String)
            Throw New NotImplementedException
        End Sub

    End Class
End Namespace
