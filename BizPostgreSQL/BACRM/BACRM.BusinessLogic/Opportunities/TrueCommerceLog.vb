﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports System.Data.SqlClient

Public Class TrueCommerceLog
    Inherits BACRM.BusinessLogic.CBusinessBase


#Region "Public Properties"

    Public Property TrueCommerceQueueID As Long
    Public Property OppID As Long
    Public Property FileName As String
    Public Property PurchaseOrder As String
    Public Property Message As String
    Public Property EDIType As Integer
    Public Property ClientTimeZoneOffset As Integer

#End Region


    Public Sub Insert()
        Try
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            With sqlParams
                ' .Add(SqlDAL.Add_Parameter("@numTCQueueID", TrueCommerceQueueID, NpgsqlTypes.NpgsqlDbType.BigInt))
                ' .Add(SqlDAL.Add_Parameter("@EDIType", EDIType, NpgsqlTypes.NpgsqlDbType.Integer))
                .Add(SqlDAL.Add_Parameter("@FileName", FileName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))
                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@PO", PurchaseOrder, NpgsqlTypes.NpgsqlDbType.VarChar, 300))
                .Add(SqlDAL.Add_Parameter("@vcMessage", Message, NpgsqlTypes.NpgsqlDbType.Text))
            End With

            SqlDAL.ExecuteNonQuery(connString, "USP_TrueCommerceQueueLog_Insert", sqlParams.ToArray())
        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class
