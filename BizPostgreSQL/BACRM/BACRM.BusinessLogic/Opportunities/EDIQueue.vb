﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Linq
Imports System.Xml.Linq
Imports System.IO
Imports BACRM.BusinessLogic.Common
Imports nsoftware
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports BACRM.BusinessLogic.Leads

Namespace BACRM.BusinessLogic.Opportunities
    Public Class EDIQueue
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private components As System.ComponentModel.IContainer
        Friend WithEvents ftp1 As nsoftware.IPWorks.Ftp
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
            Me.ftp1 = New nsoftware.IPWorks.Ftp(Me.components)
            Me.ftp1.About = ""
            Me.ftp1.Firewall.Port = 1080
            Me.ftp1.Firewall.FirewallType = nsoftware.IPWorks.FirewallTypes.fwNone
            Me.ftp1.TransferMode = nsoftware.IPWorks.FtpTransferModes.tmDefault
        End Sub

        Enum EnmDealStatus As Short
            DealWon = 1
            DealLost = 2
            DealOpen = 0
        End Enum

        Private shippingReportID As Long
        Private EDI850Status As Integer = 1

#Region "Public Methods"

        Public Sub Insert(ByVal numDomainID As Long, ByVal numUserCntID As Long, ByVal numOppID As Long, ByVal numOrderStatus As Long)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", numDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", numUserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", numOppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatus", numOrderStatus, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_EDIQueue_Insert", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Process()
            Try
                'Process Outbound EDI Requests (TOP 50)
                Dim dtPendingRecords As DataTable = GetPendingRecords()
                If Not dtPendingRecords Is Nothing AndAlso dtPendingRecords.Rows.Count > 0 Then
                    Dim dtDomain As DataTable = dtPendingRecords.DefaultView.ToTable(True, "numDomainID")

                    If Not dtDomain Is Nothing AndAlso dtDomain.Rows.Count > 0 Then
                        For Each drDomain As DataRow In dtDomain.Rows
                            'Check if directory for domain is created or not in SFTP Server, If not than create it.
                            If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID"))) Then
                                Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID")))

                                If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID")) & "\Get") Then
                                    Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID")) & "\Get")
                                End If

                                If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID")) & "\Put") Then
                                    Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID")) & "\Put")
                                End If
                            Else
                                If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID")) & "\Get") Then
                                    Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID")) & "\Get")
                                End If

                                If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID")) & "\Put") Then
                                    Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID")) & "\Put")
                                End If
                            End If

                            If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID"))) Then
                                Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")))

                                If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")) & "\Get") Then
                                    Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")) & "\Get")
                                End If

                                If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")) & "\Put") Then
                                    Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")) & "\Put")
                                End If
                            Else
                                If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")) & "\Get") Then
                                    Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")) & "\Get")
                                End If

                                If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")) & "\Put") Then
                                    Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")) & "\Put")
                                End If
                            End If

                            Dim arrRows As DataRow() = dtPendingRecords.Select("numDomainID=" & Convert.ToString(drDomain("numDomainID")))

                            If Not arrRows Is Nothing AndAlso arrRows.Length > 0 Then
                                For Each drRow As DataRow In arrRows
                                    If drRow("EDIType") = "940" Then 'Generate EDI 940 document and upload to SFTP Server
                                        Try
                                            Dim filePath As String = System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & Convert.ToString(drDomain("numDomainID")) & "\Get\" & Convert.ToString(drRow("numOppID")) & ".xml"
                                            Generate940(Convert.ToInt64(drRow("ID")), Convert.ToInt64(drRow("numDomainID")), Convert.ToInt64(drRow("numOppID")), filePath)

                                            Dim objEDIQueueLog As New EDIQueueLog
                                            objEDIQueueLog.EDIQueueID = Convert.ToInt64(drRow("ID"))
                                            objEDIQueueLog.DomainID = Convert.ToInt64(drRow("numDomainID"))
                                            objEDIQueueLog.OppID = Convert.ToInt64(drRow("numOppID"))
                                            objEDIQueueLog.EDIType = 940
                                            objEDIQueueLog.Log = "940 Sent"
                                            objEDIQueueLog.IsSuccess = True
                                            objEDIQueueLog.ExceptionMessage = ""
                                            objEDIQueueLog.Insert()
                                        Catch ex As Exception
                                            Dim objEDIQueueLog As New EDIQueueLog
                                            objEDIQueueLog.EDIQueueID = Convert.ToInt64(drRow("ID"))
                                            objEDIQueueLog.DomainID = Convert.ToInt64(drRow("numDomainID"))
                                            objEDIQueueLog.OppID = Convert.ToInt64(drRow("numOppID"))
                                            objEDIQueueLog.EDIType = 940
                                            objEDIQueueLog.Log = "Error occurred while sending 940"
                                            objEDIQueueLog.IsSuccess = False
                                            objEDIQueueLog.ExceptionMessage = ex.Message
                                            objEDIQueueLog.Insert()
                                        End Try
                                    ElseIf drRow("EDIType") = "856" Then 'Generate EDI 856 document and upload to SFTP Server
                                        Try
                                            Dim dtTCDomains As DataTable = GetTrueCommerceUsers()
                                            If (dtTCDomains.Rows.Count > 0) Then
                                                For Each drTCDomain As DataRow In dtTCDomains.Rows

                                                    If (Convert.ToInt64(drTCDomain("numDomainID")) = Convert.ToInt64(drRow("numDomainID"))) Then  ' Checking whether Order Domain and TC doamin matches
                                                        Dim result As Boolean
                                                        result = Generate856ForTC(Convert.ToInt64(drRow("ID")), Convert.ToInt64(drRow("numDomainID")), Convert.ToInt64(drRow("numOppID")))
                                                        If (result = False) Then
                                                            Dim objEDIQueueLog As New EDIQueueLog
                                                            objEDIQueueLog.EDIQueueID = Convert.ToInt64(drRow("ID"))
                                                            objEDIQueueLog.DomainID = Convert.ToInt64(drRow("numDomainID"))
                                                            objEDIQueueLog.OppID = Convert.ToInt64(drRow("numOppID"))
                                                            objEDIQueueLog.EDIType = 8566
                                                            objEDIQueueLog.Log = "Error occurred while sending 856 document for EDI processing"
                                                            objEDIQueueLog.IsSuccess = False
                                                            objEDIQueueLog.ExceptionMessage = ""
                                                            objEDIQueueLog.Insert()
                                                        End If
                                                        result = CreateInvoice810(Convert.ToInt64(drRow("ID")), Convert.ToInt64(drRow("numDomainID")), Convert.ToInt64(drRow("numOppID")))
                                                        If (result = False) Then
                                                            Dim objEDIQueueLog As New EDIQueueLog
                                                            objEDIQueueLog.EDIQueueID = Convert.ToInt64(drRow("ID"))
                                                            objEDIQueueLog.DomainID = Convert.ToInt64(drRow("numDomainID"))
                                                            objEDIQueueLog.OppID = Convert.ToInt64(drRow("numOppID"))
                                                            objEDIQueueLog.EDIType = 8566
                                                            objEDIQueueLog.Log = "Error occurred while sending 810 document for EDI processing"
                                                            objEDIQueueLog.IsSuccess = False
                                                            objEDIQueueLog.ExceptionMessage = ""
                                                            objEDIQueueLog.Insert()
                                                        End If
                                                    End If
                                                Next
                                            End If
                                        Catch ex As Exception
                                            Dim objEDIQueueLog As New EDIQueueLog
                                            objEDIQueueLog.EDIQueueID = Convert.ToInt64(drRow("ID"))
                                            objEDIQueueLog.DomainID = Convert.ToInt64(drRow("numDomainID"))
                                            objEDIQueueLog.OppID = Convert.ToInt64(drRow("numOppID"))
                                            objEDIQueueLog.EDIType = 8566
                                            objEDIQueueLog.Log = "Error occurred while sending 856 or 810 document for EDI processing"
                                            objEDIQueueLog.IsSuccess = False
                                            objEDIQueueLog.ExceptionMessage = ex.Message
                                            objEDIQueueLog.Insert()
                                        End Try
                                    End If
                                Next
                            End If
                        Next
                    End If
                End If

                'Process InBound EDI Requests (TOP 50)
                Dim counter As Integer = 0
                Dim subdirectoryEntries As String() = Directory.GetDirectories(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL")

                If Not subdirectoryEntries Is Nothing AndAlso subdirectoryEntries.Length > 0 Then
                    For Each subdirectory As String In subdirectoryEntries
                        If Directory.Exists(subdirectory & "\Put") Then
                            Dim fileEntries As String() = Directory.GetFiles(subdirectory & "\Put")

                            If Not fileEntries Is Nothing AndAlso fileEntries.Length > 0 Then
                                For Each file As String In fileEntries
                                    If Path.GetExtension(file).ToLower() = ".xml" Then
                                        Dim xmldoc As New XmlDataDocument()
                                        Dim xmlnode As XmlNodeList
                                        Dim i As Integer
                                        Dim str As String
                                        Dim fs As New FileStream(file, FileMode.Open, FileAccess.Read)
                                        xmldoc.Load(fs)
                                        fs.Close()

                                        If xmldoc.DocumentElement.Name.ToLower() = "acknowledgement" Then '997
                                            Dim nodeAckOfType As XmlNode = xmldoc.DocumentElement.SelectSingleNode("descendant::AckOfDocumentType")
                                            Dim nodeOrderID As XmlNode = xmldoc.DocumentElement.SelectSingleNode("descendant::ID")
                                            Dim orderID As Long = 0
                                            Dim ackOfType As String
                                            Dim numDomainID As Long = 0

                                            If Not nodeAckOfType Is Nothing AndAlso Not nodeOrderID Is Nothing AndAlso Int64.TryParse(nodeOrderID.InnerText.Replace("NF", ""), orderID) Then
                                                If nodeAckOfType.InnerText = "940" Then 'Acknowledgement of 940 request sent
                                                    Dim objOpportunity As New OppotunitiesIP
                                                    objOpportunity.OpportunityId = orderID
                                                    numDomainID = objOpportunity.GetDomainIDFromOppID()

                                                    If numDomainID > 0 Then
                                                        Dim objEDIQueueLog As New EDIQueueLog
                                                        objEDIQueueLog.EDIQueueID = 0
                                                        objEDIQueueLog.DomainID = numDomainID
                                                        objEDIQueueLog.OppID = orderID
                                                        objEDIQueueLog.EDIType = 940997
                                                        objEDIQueueLog.Log = "940 Acknowledged"
                                                        objEDIQueueLog.IsSuccess = True
                                                        objEDIQueueLog.ExceptionMessage = ""
                                                        objEDIQueueLog.Insert()

                                                        Try
                                                            System.IO.File.Delete(file)
                                                        Catch ex As Exception
                                                            objEDIQueueLog.EDIQueueID = 0
                                                            objEDIQueueLog.DomainID = numDomainID
                                                            objEDIQueueLog.OppID = orderID
                                                            objEDIQueueLog.EDIType = 940997
                                                            objEDIQueueLog.Log = "Error occurred while sending 940 document for EDI processing"
                                                            objEDIQueueLog.IsSuccess = False
                                                            objEDIQueueLog.ExceptionMessage = ex.Message
                                                            objEDIQueueLog.Insert()
                                                        End Try
                                                    Else
                                                        'ORDER ID OS WRONG OR ORDER IS DELETED IN BIZ
                                                        MoveFileToErrorFolder(file, Path.GetFileName(file))
                                                    End If
                                                End If
                                            Else
                                                MoveFileToErrorFolder(file, Path.GetFileName(file))
                                            End If
                                        ElseIf xmldoc.DocumentElement.Name.ToLower() = "asnpickandpack" Then '856
                                            Try
                                                Dim orderList As XmlNodeList = xmldoc.DocumentElement.SelectNodes("descendant::Order")

                                                If Not orderList Is Nothing AndAlso orderList.Count > 0 Then
                                                    For Each orderNode As XmlNode In orderList
                                                        Dim nodeOrderID As XmlNode = orderNode.SelectSingleNode("descendant::PO")
                                                        Dim PackList As XmlNodeList = orderNode.SelectNodes("descendant::Pack")


                                                        Dim orderID As Long = 0
                                                        Dim ackOfType As String
                                                        Dim numDomainID As Long = 0

                                                        If Not nodeOrderID Is Nothing AndAlso Int64.TryParse(nodeOrderID.InnerText.Replace("NF", ""), orderID) Then
                                                            Try
                                                                Dim objOpportunity As New OppotunitiesIP
                                                                objOpportunity.OpportunityId = orderID
                                                                numDomainID = objOpportunity.GetDomainIDFromOppID()

                                                                If numDomainID > 0 Then
                                                                    Dim objEDIQueueLog As New EDIQueueLog
                                                                    objEDIQueueLog.EDIQueueID = 0
                                                                    objEDIQueueLog.DomainID = numDomainID
                                                                    objEDIQueueLog.OppID = orderID
                                                                    objEDIQueueLog.EDIType = 8565
                                                                    objEDIQueueLog.Log = "856 Received"
                                                                    objEDIQueueLog.IsSuccess = True
                                                                    objEDIQueueLog.ExceptionMessage = ""
                                                                    objEDIQueueLog.Insert()

                                                                    'Commented By Richa 09052018
                                                                    'Try
                                                                    '    Generate856Acknowledge(numDomainID, orderID)

                                                                    '    objEDIQueueLog = New EDIQueueLog
                                                                    '    objEDIQueueLog.EDIQueueID = 0
                                                                    '    objEDIQueueLog.DomainID = numDomainID
                                                                    '    objEDIQueueLog.OppID = orderID
                                                                    '    objEDIQueueLog.EDIType = 856997
                                                                    '    objEDIQueueLog.Log = "856 acknowledgement sent to 3PL"
                                                                    '    objEDIQueueLog.IsSuccess = True
                                                                    '    objEDIQueueLog.ExceptionMessage = ""
                                                                    '    objEDIQueueLog.Insert()
                                                                    'Catch ex As Exception
                                                                    '    objEDIQueueLog = New EDIQueueLog
                                                                    '    objEDIQueueLog.EDIQueueID = 0
                                                                    '    objEDIQueueLog.DomainID = numDomainID
                                                                    '    objEDIQueueLog.OppID = orderID
                                                                    '    objEDIQueueLog.EDIType = 856997
                                                                    '    objEDIQueueLog.Log = "Error occured in sending 856 acknowledgement to 3PL"
                                                                    '    objEDIQueueLog.IsSuccess = False
                                                                    '    objEDIQueueLog.ExceptionMessage = ex.Message
                                                                    '    objEDIQueueLog.Insert()
                                                                    'End Try


                                                                    Try
                                                                        ReleaseAllocation(numDomainID, orderID, PackList)
                                                                    Catch ex As Exception
                                                                        objEDIQueueLog = New EDIQueueLog
                                                                        objEDIQueueLog.EDIQueueID = 0
                                                                        objEDIQueueLog.DomainID = numDomainID
                                                                        objEDIQueueLog.OppID = orderID
                                                                        objEDIQueueLog.EDIType = 8565
                                                                        objEDIQueueLog.Log = "Error occured in release allocation"
                                                                        objEDIQueueLog.IsSuccess = False
                                                                        objEDIQueueLog.ExceptionMessage = ex.Message
                                                                        objEDIQueueLog.Insert()

                                                                        Throw New Exception("856_RELEASE_ALLOCATION_FAILED")
                                                                    End Try

                                                                    Try
                                                                        'Send 856 to EDI Integrator
                                                                        System.IO.File.Move(file, file.Replace("3PL", "EDI").Replace("Put", "Get"))

                                                                        objEDIQueueLog = New EDIQueueLog
                                                                        objEDIQueueLog.EDIQueueID = 0
                                                                        objEDIQueueLog.DomainID = numDomainID
                                                                        objEDIQueueLog.OppID = orderID
                                                                        objEDIQueueLog.EDIType = 8566
                                                                        objEDIQueueLog.Log = "856 Sent"
                                                                        objEDIQueueLog.IsSuccess = True
                                                                        objEDIQueueLog.ExceptionMessage = ""
                                                                        objEDIQueueLog.Insert()
                                                                    Catch ex As Exception
                                                                        Throw New Exception("ADD_856_TO_QUEUE_FAILED")
                                                                    End Try
                                                                Else
                                                                    MoveFileToErrorFolder(file, Path.GetFileName(file))
                                                                End If
                                                            Catch ex As Exception
                                                                If ex.Message.Contains("ADD_856_TO_QUEUE_FAILED") Then
                                                                    Dim objEDIQueueLog As New EDIQueueLog
                                                                    objEDIQueueLog.EDIQueueID = 0
                                                                    objEDIQueueLog.DomainID = numDomainID
                                                                    objEDIQueueLog.OppID = orderID
                                                                    objEDIQueueLog.EDIType = 8566
                                                                    objEDIQueueLog.Log = "Allocation released but not able to send 856 document for EDI processing"
                                                                    objEDIQueueLog.IsSuccess = False
                                                                    objEDIQueueLog.ExceptionMessage = ex.Message
                                                                    objEDIQueueLog.Insert()
                                                                ElseIf Not ex.Message.Contains("856_RELEASE_ALLOCATION_FAILED") Then
                                                                    Dim objEDIQueueLog As New EDIQueueLog
                                                                    objEDIQueueLog.EDIQueueID = 0
                                                                    objEDIQueueLog.DomainID = numDomainID
                                                                    objEDIQueueLog.OppID = orderID
                                                                    objEDIQueueLog.EDIType = 856
                                                                    objEDIQueueLog.Log = "Error occurred while processing received 856 document from 3PL"
                                                                    objEDIQueueLog.IsSuccess = False
                                                                    objEDIQueueLog.ExceptionMessage = ex.Message
                                                                    objEDIQueueLog.Insert()
                                                                End If

                                                                MoveFileToErrorFolder(file, Path.GetFileName(file))
                                                            End Try
                                                        Else
                                                            MoveFileToErrorFolder(file, Path.GetFileName(file))
                                                        End If
                                                    Next
                                                Else
                                                    MoveFileToErrorFolder(file, Path.GetFileName(file))
                                                End If
                                            Catch ex As Exception
                                                MoveFileToErrorFolder(file, Path.GetFileName(file))
                                            End Try
                                        Else
                                            MoveFileToErrorFolder(file, Path.GetFileName(file))
                                        End If

                                        counter = counter + 1
                                    Else
                                        MoveFileToErrorFolder(file, Path.GetFileName(file))
                                    End If

                                    If counter >= 50 Then
                                        Exit For
                                    End If
                                Next
                            End If
                        End If

                        If counter >= 50 Then
                            Exit For
                        End If
                    Next
                End If

                Dim dtTCUsers As DataTable = GetTrueCommerceUsers()
                If Not dtTCUsers Is Nothing AndAlso dtTCUsers.Rows.Count > 0 Then

                    For Each drDomain As DataRow In dtTCUsers.Rows
                        If Not Directory.Exists(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID"))) Then
                            Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")))
                        End If

                        Dim filePath As String = System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")) & "\Put\"

                        Dim TCcounter As Integer = 0
                        Dim TCsubdirectoryEntries As String() = Directory.GetDirectories(System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & Convert.ToString(drDomain("numDomainID")))

                        If Not TCsubdirectoryEntries Is Nothing AndAlso TCsubdirectoryEntries.Length > 0 Then
                            For Each subdirectory As String In TCsubdirectoryEntries
                                If subdirectory.EndsWith("\Put") Then
                                    Dim fileEntries As String() = Directory.GetFiles(subdirectory)

                                    If Not fileEntries Is Nothing AndAlso fileEntries.Length > 0 Then
                                        For Each file As String In fileEntries
                                            Try
                                                If Path.GetExtension(file).ToLower() = ".xml" Then

                                                    Dim xmldoc As New XmlDataDocument()
                                                    Dim xmlnode As XmlNode
                                                    Dim i As Integer
                                                    Dim str As String

                                                    Using fs As New FileStream(file, FileMode.Open, FileAccess.Read)
                                                        xmldoc.Load(fs)
                                                        fs.Close()
                                                    End Using

                                                    If xmldoc.DocumentElement.Name.ToLower() = "acknowledgement" Then '997 
                                                        Dim nodeAckOfType As XmlNode = xmldoc.DocumentElement.SelectSingleNode("descendant::AckOfDocumentType")
                                                        Dim nodeOrderID As XmlNode = xmldoc.DocumentElement.SelectSingleNode("descendant::ID")
                                                        Dim orderID As Long = 0
                                                        Dim ackOfType As String
                                                        Dim numDomainID As Long = 0

                                                        If Not nodeAckOfType Is Nothing AndAlso Not nodeOrderID Is Nothing AndAlso Int64.TryParse(nodeOrderID.InnerText.Replace("NF", ""), orderID) Then
                                                            If nodeAckOfType.InnerText = "856/810" Then 'Acknowledgement of 856/810 request sent
                                                                Dim objOpportunity As New OppotunitiesIP
                                                                objOpportunity.OpportunityId = orderID
                                                                numDomainID = objOpportunity.GetDomainIDFromOppID()

                                                                If numDomainID > 0 Then
                                                                    Dim objEDIQueueLog As New EDIQueueLog
                                                                    objEDIQueueLog.EDIQueueID = 0
                                                                    objEDIQueueLog.DomainID = numDomainID
                                                                    objEDIQueueLog.OppID = orderID
                                                                    objEDIQueueLog.EDIType = 856810997
                                                                    objEDIQueueLog.Log = "856/810 Acknowledged"
                                                                    objEDIQueueLog.IsSuccess = True
                                                                    objEDIQueueLog.ExceptionMessage = ""
                                                                    objEDIQueueLog.Insert()

                                                                    Try
                                                                        System.IO.File.Delete(file)
                                                                    Catch ex As Exception
                                                                        objEDIQueueLog.EDIQueueID = 0
                                                                        objEDIQueueLog.DomainID = numDomainID
                                                                        objEDIQueueLog.OppID = orderID
                                                                        objEDIQueueLog.EDIType = 856810997
                                                                        objEDIQueueLog.Log = "Error occurred while sending 856/810 document for EDI processing"
                                                                        objEDIQueueLog.IsSuccess = False
                                                                        objEDIQueueLog.ExceptionMessage = ex.Message
                                                                        objEDIQueueLog.Insert()
                                                                    End Try
                                                                Else
                                                                    'ORDER ID OS WRONG OR ORDER IS DELETED IN BIZ
                                                                    MoveFileToErrorFolder(file, Path.GetFileName(file))
                                                                End If
                                                            End If
                                                        Else
                                                            MoveFileToErrorFolder(file, Path.GetFileName(file))
                                                        End If
                                                    Else
                                                        xmlnode = xmldoc.SelectSingleNode("descendant::TransactionType")
                                                        If (xmlnode.InnerText = "850") Then
                                                            GenerateSO(xmldoc, file, Convert.ToString(drDomain("numDomainID")))

                                                        Else
                                                            MoveFileToErrorFolder(file, Path.GetFileName(file))
                                                            Dim objTCQueueLog As New TrueCommerceLog
                                                            objTCQueueLog.DomainID = CCommon.ToLong(DomainID)
                                                            objTCQueueLog.FileName = Path.GetFileName(file)
                                                            objTCQueueLog.PurchaseOrder = "850"
                                                            objTCQueueLog.Message = "SO Creation Fails : File without .xml extension"
                                                            objTCQueueLog.Insert()
                                                        End If
                                                    End If

                                                End If

                                            Catch ex As Exception
                                                Dim objTCQueueLog As New TrueCommerceLog
                                                objTCQueueLog.DomainID = CCommon.ToLong(DomainID)
                                                objTCQueueLog.FileName = Path.GetFileName(file)
                                                objTCQueueLog.PurchaseOrder = "850"
                                                objTCQueueLog.Message = "SO Creation Fails :" + ex.Message.ToString
                                                objTCQueueLog.Insert()
                                            End Try
                                        Next
                                    End If
                                End If

                            Next
                        End If
                    Next
                End If

            Catch ex As Exception
                Dim objTCQueueLog As New TrueCommerceLog
                ' objTCQueueLog.TrueCommerceQueueID = Convert.ToInt64(PONum)
                objTCQueueLog.DomainID = CCommon.ToLong(DomainID)
                objTCQueueLog.FileName = ""
                objTCQueueLog.PurchaseOrder = ""
                objTCQueueLog.Message = ex.Message.ToString
                objTCQueueLog.Insert()
            End Try
        End Sub

        Public Function GetItemDtls850ForSO(ByVal Inbound850PickItem As Long, ByVal numDomainId As Long, ByVal numCompanyID As Long, ByVal numDivisionID As Long, ByVal orderSource As Long, ByVal ItemIdentification As String) As DataTable
            Dim dtBizItem As DataTable = Nothing
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@Inbound850PickItem", Inbound850PickItem, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItemIdentification", ItemIdentification, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numDivisionId", numDivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCompanyId", numCompanyID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", numDomainId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderSource", orderSource, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetItemDetailsFor850", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub GetItemDetailsFor850Item(ByVal isFromMapping As Boolean, ByRef dsTemp As DataSet, ByVal dtBizItem As DataTable, File As String, ByVal Units As Double, ByVal UnitPrice As Decimal, ByVal numDomainId As Long, ByVal numDivisionID As Long, ByVal PONum As String, ByVal orderSource As Long, ByVal shipToState As Long, ByVal shipToCountry As Long, ByVal isMappingRequired As Boolean, ByVal xmlItemName As String, ByVal xmlASIN As String, ByVal xmlSKU As String, ByVal xmlDescription As String)
            Try
                If dsTemp Is Nothing Then
                    dsTemp = createSet()
                End If

                Dim objCommon As New CCommon
                Dim objItem As New CItems
                Dim numItemCode As Long = CCommon.ToLong(dtBizItem.Rows(0)("numItemCode"))

                objItem.DomainID = numDomainId
                objItem.DivisionID = numDivisionID
                objItem.byteMode = 1
                objItem.ItemCode = numItemCode
                objItem.SourceType = 4
                Dim dtWarehouse As DataTable = GetWareHouseItemIDfor850(numDomainId, numItemCode, Units, orderSource, 4, shipToState, shipToCountry)

                If (dtBizItem.Rows(0)("charItemType").ToString <> "P" AndAlso dtWarehouse.Rows.Count = 0) Then
                    objItem.WareHouseItemID = Nothing
                ElseIf (dtBizItem.Rows(0)("charItemType").ToString = "P" AndAlso dtWarehouse.Rows.Count = 0) Then
                    If isFromMapping Then
                        Throw New Exception("WareHouse not available")
                    Else
                        Dim objTCLog As New TrueCommerceLog
                        objTCLog.DomainID = CCommon.ToLong(DomainID)
                        objTCLog.FileName = Path.GetFileName(File)
                        objTCLog.PurchaseOrder = "850_" + Path.GetFileName(File)
                        objTCLog.Message = "WareHouse not available for Inventory Item in system : " + numItemCode.ToString
                        objTCLog.Insert()
                        Exit Sub
                    End If
                Else
                    objItem.WareHouseItemID = CCommon.ToLong(dtWarehouse.Rows(0)("numWarehouseItemID").ToString)
                End If

                objItem.GetItemAndWarehouseDetails()

                If Not String.IsNullOrEmpty(xmlItemName) Then
                    objItem.ItemName = xmlItemName
                End If
                If Not String.IsNullOrEmpty(xmlSKU) Then
                    objItem.SKU = xmlSKU
                End If
                If Not String.IsNullOrEmpty(xmlASIN) Then
                    objItem.ASIN = xmlASIN
                End If
                If Not String.IsNullOrEmpty(xmlDescription) Then
                    objItem.ItemDesc = xmlDescription
                End If

                objItem.CustomerPartNo = "" 'txtCustomerPartNo.Text
                objItem.bitHasKitAsChild = False 'objSelectedItem.bitHasKitAsChild
                objItem.vcKitChildItems = "" 'hdnKitChildItems.Value
                objItem.SelectedUOMID = 0 'radcmbUOM.SelectedValue
                objItem.SelectedUOM = "" 'IIf(radcmbUOM.SelectedValue > 0, radcmbUOM.Text, "")
                objItem.AttributeIDs = "" 'objSelectedItem.vcAttributeIDs
                objItem.Attributes = "" 'objSelectedItem.vcAttributes
                ' MaxRowOrder = MaxRowOrder + 1
                ' objItem.numSortOrder = MaxRowOrder
                Dim saleprice As Decimal = CalculateItemSalePrice(UnitPrice, Units, 0, True, 1)

                dsTemp = AddItemtoDataTable(objCommon, dsTemp, True, objItem.ItemType, "", False, objItem.KitParent, objItem.ItemCode,
                        Units,
                        UnitPrice,
                        objItem.ItemDesc,
                        objItem.WareHouseItemID, objItem.ItemName, "", 0,
                        objItem.ModelID, CCommon.ToLong(objItem.SelectedUOMID), If(CCommon.ToLong(objItem.SelectedUOMID) > 0, objItem.SelectedUOM, "-"),
                        1, "Sales", False, 0, "",
                        "", "",
                        False,
                        "",
                        "",
                        0,
                        0,
                        0,
                        0,
                        vcBaseUOMName:="",
                        numPrimaryVendorID:=CCommon.ToLong(0),
                        strSKU:=objItem.SKU, objItem:=objItem, primaryVendorCost:=0,
                        salePrice:=saleprice,
                        numMaxWOQty:=objItem.numMaxWOQty, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=CCommon.ToInteger(objItem.numContainer), numContainerQty:=CCommon.ToInteger(objItem.numNoItemIntoContainer),
                        numItemClassification:=0,
                        numPromotionID:=0,
                        IsPromotionTriggered:=False,
                        vcPromotionDetail:="",
                        numSortOrder:=objItem.numSortOrder, numCost:=0,
                        itemReleaseDate:=Date.Today.ToString("MM/dd/yyyy"), ShipFromLocation:="",
                        numShipToAddressID:=0,
                        ShipToFullAddress:="",
                        InclusionDetail:="",
                        OnHandAllocation:="",
                        isMappingRequired:=isMappingRequired,
                        vcASIN:=objItem.ASIN)

            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub GenerateSO(xmldoc As XmlDataDocument, file As String, domainID As String)
            Dim PONum As String = ""
            Dim marketPlaceName As String = ""
            Dim marketplaceSettingId As String = ""

            Try
                EDI850Status = 1
                Dim dsTemp As DataSet = Nothing

                Dim lngDomainID As Long = CCommon.ToLong(xmldoc.SelectSingleNode("descendant::EmployerID").InnerText())
                PONum = xmldoc.SelectSingleNode("descendant::PurchaseOrderNumber").InnerText()
                Dim numDivisionId As Long = CCommon.ToLong(xmldoc.SelectSingleNode("descendant::CustomerID").InnerText())
                marketPlaceName = CCommon.ToString(xmldoc.SelectSingleNode("descendant::MarketplaceName").InnerText())
                marketplaceSettingId = CCommon.ToString(xmldoc.SelectSingleNode("descendant::MarketplaceSettingId").InnerText())
                Dim firstName As String = CCommon.ToString(xmldoc.SelectSingleNode("descendant::BillFirstName").InnerText())
                Dim lastName As String = CCommon.ToString(xmldoc.SelectSingleNode("descendant::BillLastName").InnerText())
                Dim email As String = CCommon.ToString(xmldoc.SelectSingleNode("descendant::BillEmailid").InnerText())

                Dim shipStateID As Long = 0
                Dim shipCountryID As Long = 0

                Dim strShipState As String
                If (xmldoc.SelectSingleNode("descendant::ShipToState") IsNot Nothing) Then
                    strShipState = xmldoc.SelectSingleNode("descendant::ShipToState").InnerText()
                Else
                    strShipState = ""
                End If

                Dim strShipCountry As String
                If (xmldoc.SelectSingleNode("descendant::ShipToCountry") IsNot Nothing) Then
                    strShipCountry = xmldoc.SelectSingleNode("descendant::ShipToCountry").InnerText()
                Else
                    strShipCountry = ""
                End If

                Dim dtStateCountry As DataTable = GetStateNCountryIds(strShipCountry, strShipState, domainID)

                If (dtStateCountry IsNot Nothing And dtStateCountry.Rows.Count > 0) Then
                    shipStateID = CCommon.ToLong(dtStateCountry.Rows(0)("StateId"))
                    shipCountryID = CCommon.ToLong(dtStateCountry.Rows(0)("CountryId"))
                End If

                Dim dtCmpDetails As DataTable = GetCompanyDetailsFrom850(lngDomainID, numDivisionId, firstName, lastName, email, marketPlaceName)

                If (dtCmpDetails IsNot Nothing AndAlso dtCmpDetails.Rows.Count > 0) Then

                    If CCommon.ToLong(dtCmpDetails.Rows(0)("numDivisionID")) = 0 AndAlso CCommon.ToBool(dtCmpDetails.Rows(0)("bitUsePredefinedCustomer")) Then
                        Dim objTCQueueLog As New TrueCommerceLog
                        objTCQueueLog.DomainID = CCommon.ToLong(domainID)
                        objTCQueueLog.FileName = Path.GetFileName(file)
                        objTCQueueLog.PurchaseOrder = "850_" + Path.GetFileName(file)
                        objTCQueueLog.Message = "Customer is not mapped or is not present in system"
                        objTCQueueLog.Insert()

                        If Not String.IsNullOrEmpty(PONum.Trim()) Then
                            Generate855Acknowledge(False, marketPlaceName, marketplaceSettingId, "Customer is not mapped or is not present in system", Path.GetFileName(file), PONum, CCommon.ToLong(domainID), 0)
                        End If

                        MoveFileToErrorFolder(file, Path.GetFileName(file))

                        Exit Sub
                    Else
                        Dim strItemIdentity As String = ""

                        Dim lngDivisionID As Long = CCommon.ToLong(dtCmpDetails.Rows(0)("numDivisionID"))
                        Dim lngCompanyId As Long = CCommon.ToLong(dtCmpDetails.Rows(0)("numCompanyId"))
                        Dim vcCompanyName As String = CCommon.ToString(dtCmpDetails.Rows(0)("vcCompanyName"))
                        Dim numContactId As Long = CCommon.ToLong(dtCmpDetails.Rows(0)("numContactID"))
                        Dim numAssignedTo As Long = CCommon.ToLong(dtCmpDetails.Rows(0)("numAssignedTo"))

                        Dim orderSource As Long = CCommon.ToLong(dtCmpDetails.Rows(0)("numMarketplaceID"))
                        Dim lngInbound850PickItem As Long = CCommon.ToLong(dtCmpDetails.Rows(0)("tintInbound850PickItem"))

                        Dim ItemsList As XmlNodeList = xmldoc.SelectNodes("descendant::Item")
                        Dim isReceiveOrderWithNonMappedItem As Boolean = CCommon.ToBool(dtCmpDetails.Rows(0)("bitReceiveOrderWithNonMappedItem"))
                        Dim itemToUseForNonMappedItem As Long = CCommon.ToLong(dtCmpDetails.Rows(0)("numItemToUseForNonMappedItem"))
                        Dim isMappingRequired As Boolean = False

                        If (lngInbound850PickItem > 0) Then

                            If Not ItemsList Is Nothing AndAlso ItemsList.Count > 0 Then

                                For Each Item As XmlNode In ItemsList
                                    isMappingRequired = False

                                    If (lngInbound850PickItem = 1) Then
                                        strItemIdentity = Item.SelectSingleNode("descendant::BuyerPart").InnerText
                                    ElseIf lngInbound850PickItem = 2 Then
                                        strItemIdentity = Item.SelectSingleNode("descendant::UPC").InnerText
                                    ElseIf lngInbound850PickItem = 3 Then
                                        strItemIdentity = Item.SelectSingleNode("descendant::ItemName").InnerText
                                    ElseIf lngInbound850PickItem = 4 Then
                                        strItemIdentity = Item.SelectSingleNode("descendant::BizItemId").InnerText
                                    ElseIf lngInbound850PickItem = 5 Then
                                        strItemIdentity = Item.SelectSingleNode("descendant::ASIN").InnerText
                                    ElseIf lngInbound850PickItem = 6 Then
                                        If (Item.SelectSingleNode("descendant::CustomerPart") IsNot Nothing) Then
                                            strItemIdentity = Item.SelectSingleNode("descendant::CustomerPart").InnerText
                                        Else
                                            strItemIdentity = ""
                                        End If
                                    ElseIf lngInbound850PickItem = 7 Then
                                        strItemIdentity = Item.SelectSingleNode("descendant::VendorPart").InnerText
                                    End If

                                    If strItemIdentity IsNot Nothing And strItemIdentity <> "" Then
                                        Dim Quantity As Double = CCommon.ToDouble(Item.SelectSingleNode("descendant::Quantity").InnerText)
                                        Dim UnitPrice As Decimal = CCommon.ToDecimal(Item.SelectSingleNode("descendant::UnitPrice").InnerText)

                                        Dim dtBizItem As DataTable = GetItemDtls850ForSO(lngInbound850PickItem, CCommon.ToLong(domainID), lngCompanyId, lngDivisionID, orderSource, strItemIdentity)

                                        If (dtBizItem Is Nothing Or dtBizItem.Rows.Count = 0) AndAlso isReceiveOrderWithNonMappedItem AndAlso itemToUseForNonMappedItem > 0 Then
                                            isMappingRequired = True
                                            dtBizItem = GetItemDtls850ForSO(4, CCommon.ToLong(domainID), lngCompanyId, lngDivisionID, orderSource, itemToUseForNonMappedItem.ToString())
                                        End If

                                        If (dtBizItem IsNot Nothing And dtBizItem.Rows.Count > 0) Then
                                            Try
                                                Dim xmlItemName As String = ""
                                                Dim xmlSKU As String = ""
                                                Dim xmlASIN As String = ""
                                                Dim xmlDescription As String = ""

                                                If Not Item.SelectSingleNode("descendant::ItemName") Is Nothing Then
                                                    xmlItemName = Item.SelectSingleNode("descendant::ItemName").InnerText
                                                End If
                                                If Not Item.SelectSingleNode("descendant::BuyerPart") Is Nothing Then
                                                    xmlSKU = Item.SelectSingleNode("descendant::BuyerPart").InnerText
                                                End If
                                                If Not Item.SelectSingleNode("descendant::ASIN") Is Nothing Then
                                                    xmlASIN = Item.SelectSingleNode("descendant::ASIN").InnerText
                                                End If
                                                If Not Item.SelectSingleNode("descendant::Description") Is Nothing Then
                                                    xmlDescription = Item.SelectSingleNode("descendant::Description").InnerText
                                                End If

                                                GetItemDetailsFor850Item(False, dsTemp, dtBizItem, file, Quantity, UnitPrice, CCommon.ToLong(domainID), lngDivisionID, PONum, orderSource, shipStateID, shipCountryID, isMappingRequired, xmlItemName, xmlASIN, xmlSKU, xmlDescription)
                                            Catch ex As Exception
                                                Dim objTCLog As New TrueCommerceLog
                                                objTCLog.DomainID = CCommon.ToLong(domainID)
                                                objTCLog.FileName = Path.GetFileName(file)
                                                objTCLog.PurchaseOrder = "850_" + Path.GetFileName(file)
                                                objTCLog.Message = ex.Message
                                                objTCLog.Insert()

                                                If Not String.IsNullOrEmpty(PONum.Trim()) Then
                                                    Generate855Acknowledge(False, marketPlaceName, marketplaceSettingId, ex.Message, Path.GetFileName(file), PONum, CCommon.ToLong(domainID), 0)
                                                End If

                                                MoveFileToErrorFolder(file, Path.GetFileName(file))

                                                Exit Sub
                                            End Try
                                        Else
                                            If isReceiveOrderWithNonMappedItem Then
                                                EDI850Status = 0
                                            Else
                                                Dim objTCLog As New TrueCommerceLog
                                                objTCLog.DomainID = CCommon.ToLong(domainID)
                                                objTCLog.FileName = Path.GetFileName(file)
                                                objTCLog.PurchaseOrder = "850_" + Path.GetFileName(file)
                                                objTCLog.Message = "Item not available in system : " + strItemIdentity
                                                objTCLog.Insert()
                                                EDI850Status = 0

                                                If Not String.IsNullOrEmpty(PONum.Trim()) Then
                                                    Generate855Acknowledge(False, marketPlaceName, marketplaceSettingId, "Item not available in system : " + strItemIdentity, Path.GetFileName(file), PONum, CCommon.ToLong(domainID), 0)
                                                End If

                                                MoveFileToErrorFolder(file, Path.GetFileName(file))

                                                Exit Sub
                                            End If
                                        End If
                                    Else
                                        Dim objTCLog As New TrueCommerceLog
                                        objTCLog.DomainID = CCommon.ToLong(domainID)
                                        objTCLog.FileName = Path.GetFileName(file)
                                        objTCLog.PurchaseOrder = "850_" + Path.GetFileName(file)
                                        objTCLog.Message = "Inbound Item Mapping field missing."
                                        objTCLog.Insert()

                                        If Not String.IsNullOrEmpty(PONum.Trim()) Then
                                            Generate855Acknowledge(False, marketPlaceName, marketplaceSettingId, "Inbound Item Mapping field missing.", Path.GetFileName(file), PONum, CCommon.ToLong(domainID), 0)
                                        End If

                                        MoveFileToErrorFolder(file, Path.GetFileName(file))

                                        Exit Sub
                                    End If
                                Next

                                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                    If lngDivisionID = 0 Then
                                        Dim objLeads As New CLeads

                                        Dim billStreet As String = ""
                                        Dim billCity As String = ""
                                        Dim billPostalCode As String = ""
                                        Dim billCountry As Long = 0
                                        Dim billState As Long = 0

                                        If (xmldoc.SelectSingleNode("descendant::BillToAddress-LineOne") IsNot Nothing) Then
                                            billStreet = xmldoc.SelectSingleNode("descendant::BillToAddress-LineOne").InnerText()
                                        Else
                                            billStreet = ""
                                        End If

                                        If (xmldoc.SelectSingleNode("descendant::BillToAddress-LineTwo") IsNot Nothing) Then
                                            billStreet = billStreet & " " & xmldoc.SelectSingleNode("descendant::BillToAddress-LineTwo").InnerText()
                                        Else
                                            billStreet = billStreet + ""
                                        End If

                                        If (xmldoc.SelectSingleNode("descendant::BillToCity") IsNot Nothing) Then
                                            billCity = xmldoc.SelectSingleNode("descendant::BillToCity").InnerText()
                                        Else
                                            billCity = ""
                                        End If

                                        If (xmldoc.SelectSingleNode("descendant::BillToZipcode") IsNot Nothing) Then
                                            billPostalCode = xmldoc.SelectSingleNode("descendant::BillToZipcode").InnerText()
                                        Else
                                            billPostalCode = ""
                                        End If

                                        Dim strBillState As String
                                        If (xmldoc.SelectSingleNode("descendant::BillToState") IsNot Nothing) Then
                                            strBillState = xmldoc.SelectSingleNode("descendant::BillToState").InnerText()
                                        Else
                                            strBillState = ""
                                        End If

                                        Dim strBillCountry As String
                                        If (xmldoc.SelectSingleNode("descendant::BillToCountry") IsNot Nothing) Then
                                            strBillCountry = xmldoc.SelectSingleNode("descendant::BillToCountry").InnerText()
                                        Else
                                            strBillCountry = ""
                                        End If

                                        Dim dtContactStateCountry As New DataTable
                                        dtContactStateCountry = GetStateNCountryIds(strBillCountry, strBillState, lngDomainID)

                                        If (dtContactStateCountry IsNot Nothing And dtContactStateCountry.Rows.Count > 0) Then
                                            billState = CCommon.ToLong(dtContactStateCountry.Rows(0)("StateId"))
                                            billCountry = CCommon.ToLong(dtContactStateCountry.Rows(0)("CountryId"))
                                        Else
                                            billState = 0
                                            billCountry = 0
                                        End If

                                        objLeads.GroupID = 1
                                        objLeads.ContactType = 70
                                        objLeads.PrimaryContact = True
                                        objLeads.DivisionName = "-"
                                        objLeads.FirstName = firstName
                                        objLeads.LastName = lastName
                                        objLeads.Email = email
                                        objLeads.PStreet = billStreet
                                        objLeads.PCity = billCity
                                        objLeads.PPostalCode = billPostalCode
                                        objLeads.PState = billState
                                        objLeads.PCountry = billCountry
                                        objLeads.CompanyName = firstName & " " & lastName
                                        objLeads.Street = billStreet
                                        objLeads.City = billCity
                                        objLeads.PostalCode = billPostalCode
                                        objLeads.CompanyType = 46
                                        objLeads.State = billState
                                        objLeads.Country = billCountry
                                        objLeads.SStreet = billStreet
                                        objLeads.SCity = billCity
                                        objLeads.SPostalCode = billPostalCode
                                        objLeads.SState = billState
                                        objLeads.SCountry = billCountry
                                        objLeads.DomainID = lngDomainID
                                        objLeads.CRMType = 1
                                        objLeads.CompanyType = 46
                                        objLeads.Profile = 0
                                        objLeads.AccountClass = 0
                                        lngCompanyId = objLeads.CreateRecordCompanyInfo()

                                        objLeads.CompanyID = lngCompanyId
                                        lngDivisionID = objLeads.CreateRecordDivisionsInfo()

                                        objLeads.DivisionID = lngDivisionID
                                        numContactId = objLeads.CreateRecordAddContactInfo()

                                        vcCompanyName = firstName & " " & lastName
                                    End If

                                    Dim objOpportunity As New MOpportunity
                                    objOpportunity.OpportunityId = 0
                                    If numAssignedTo > 0 Then
                                        objOpportunity.UserCntID = numAssignedTo
                                        objOpportunity.AssignedTo = numAssignedTo
                                    Else
                                        Dim dtAdminUser As DataTable = GetAdminUser(CCommon.ToLong(domainID))
                                        objOpportunity.UserCntID = CCommon.ToLong(dtAdminUser.Rows(0)("numAdminID"))
                                        objOpportunity.AssignedTo = CCommon.ToLong(dtAdminUser.Rows(0)("numAdminID"))
                                        numAssignedTo = CCommon.ToLong(dtAdminUser.Rows(0)("numAdminID"))
                                    End If
                                    objOpportunity.DomainID = domainID
                                    objOpportunity.ContactID = numContactId
                                    objOpportunity.DivisionID = lngDivisionID

                                    If (xmldoc.SelectSingleNode("descendant::ReleaseDate") IsNot Nothing) Then
                                        objOpportunity.ReleaseDate = CDate(xmldoc.SelectSingleNode("descendant::ReleaseDate").InnerText())
                                    Else
                                        objOpportunity.ReleaseDate = Date.Today
                                    End If

                                    objOpportunity.OpportunityName = vcCompanyName & "-SO-" & Format(Now(), "MMMM")
                                    objOpportunity.EstimatedCloseDate = Date.Today ' DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                                    objOpportunity.PublicFlag = 0


                                    objOpportunity.BillToAddressID = 0 'CCommon.ToLong(hdnBillAddressID.Value)
                                    objOpportunity.ShipToAddressID = 0 'CCommon.ToLong(hdnShipAddressID.Value)

                                    objOpportunity.CouponCode = ""
                                    objOpportunity.Discount = 0
                                    objOpportunity.boolDiscType = False

                                    objOpportunity.CurrencyID = 0 'Session("BaseCurrencyID")
                                    objOpportunity.OppType = 1 '
                                    objOpportunity.DealStatus = 1 'objOpportunity.EnmDealStatus.DealWon
                                    objOpportunity.Source = orderSource
                                    objOpportunity.SourceType = 4

                                    Dim TotalAmount As Decimal = 0

                                    If dsTemp IsNot Nothing Then
                                        If dsTemp.Tables(0).Rows.Count > 0 Then
                                            TotalAmount = CType(dsTemp.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)
                                            objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml
                                        End If
                                    End If

                                    Dim dtBillingTerms As DataTable

                                    Dim objAdmin As New CAdmin
                                    objAdmin.DivisionID = lngDivisionID
                                    dtBillingTerms = objAdmin.GetBillingTerms()

                                    objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
                                    objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
                                    objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
                                    objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")
                                    objOpportunity.UseShippersAccountNo = False 'CCommon.ToBool(chkUseCustomerShippingAccount.Checked)
                                    objOpportunity.UseMarkupShippingRate = False ' CCommon.ToBool(chkMarkupShippingCharges.Checked)
                                    objOpportunity.dcMarkupShippingRate = 0 'CCommon.ToDecimal(txtMarkupShippingRate.Text)
                                    objOpportunity.intUsedShippingCompany = 0 'CCommon.ToInteger(radShipVia.SelectedValue)
                                    objOpportunity.ShippingService = 0 'CCommon.ToLong(hdnShippingService.Value)
                                    objOpportunity.ClassID = 0 'CCommon.ToLong(ddlClass.SelectedValue)
                                    objOpportunity.WillCallWarehouseID = 0 'CCommon.ToLong(hdnWillCallWarehouseID.Value)

                                    If (xmldoc.SelectSingleNode("descendant::PurchaseOrderNumber") IsNot Nothing) Then
                                        objOpportunity.OppRefOrderNo = xmldoc.SelectSingleNode("descendant::PurchaseOrderNumber").InnerText()
                                    Else
                                        objOpportunity.OppRefOrderNo = ""
                                    End If

                                    Dim arrOutPut() As String
                                    Dim lngOppId As Long
                                    arrOutPut = objOpportunity.Save
                                    lngOppId = arrOutPut(0)

                                    UpdateSOAddress(xmldoc, lngOppId, lngCompanyId, vcCompanyName, numContactId, domainID, file, numAssignedTo)

                                    '' Insert Custom Fields data for Order generated through 850
                                    Dim objCommon As New CCommon
                                    objCommon.DomainID = domainID

                                    Dim dsCFOrder As DataSet = objCommon.GetEDICustomFields(19, lngOppId)
                                    If (dsCFOrder IsNot Nothing) Then
                                        Dim dtCFOrder As DataTable = dsCFOrder.Tables(0)
                                        If (dtCFOrder.Rows.Count > 0) Then
                                            For Each dr As DataRow In dtCFOrder.Rows
                                                Dim strNodeName As String = "descendant::" + dr("Fld_label").ToString
                                                Dim OrderNode As XmlNode = xmldoc.SelectSingleNode(strNodeName)
                                                If (OrderNode IsNot Nothing) Then

                                                    objCommon.UpdateEDICustomFields(19, lngOppId, CCommon.ToLong(dr("Fld_id")), OrderNode.InnerText, 0, 1, Date.Today.ToString("MM/dd/yyyy"))
                                                End If
                                            Next
                                        End If
                                    End If

                                    '' Insert Custom Fields data for Items for order generated through 850

                                    Dim dtOppItemCodes As DataTable = GetOppItemCodes850(lngOppId)

                                    If dtOppItemCodes.Rows.Count > 0 Then

                                        For i As Integer = 0 To dtOppItemCodes.Rows.Count - 1

                                            Dim dsCFItem As DataSet = objCommon.GetEDICustomFields(21, CCommon.ToLong(dtOppItemCodes.Rows(i)("numoppitemtcode")))
                                            If (dsCFItem IsNot Nothing) Then
                                                Dim dtCFItem As DataTable = dsCFItem.Tables(0)
                                                If (dtCFItem.Rows.Count > 0) Then
                                                    For Each dr As DataRow In dtCFItem.Rows
                                                        For j As Integer = 0 To ItemsList.Count - 1
                                                            If (i = j) Then
                                                                Dim strItemNodeName As String = "descendent::" + dr("Fld_label").ToString
                                                                Dim itemNode As XmlNode = ItemsList(i).SelectSingleNode(dr("Fld_label").ToString)
                                                                If (itemNode IsNot Nothing) Then

                                                                    objCommon.UpdateEDICustomFields(21, CCommon.ToLong(dtOppItemCodes.Rows(i)("numoppitemtcode")), CCommon.ToLong(dr("Fld_id")), itemNode.InnerText, 0, 1, Date.Today.ToString("MM/dd/yyyy"))
                                                                End If
                                                            End If

                                                        Next

                                                    Next
                                                End If
                                            End If
                                        Next
                                    End If

                                    If (EDI850Status = 0) Then
                                        Dim objTCQueueLog As New TrueCommerceLog
                                        objTCQueueLog.DomainID = CCommon.ToLong(domainID)
                                        objTCQueueLog.FileName = Path.GetFileName(file)
                                        objTCQueueLog.PurchaseOrder = "SO: " + CCommon.ToString(lngOppId)
                                        objTCQueueLog.Message = "SO Partially Created"
                                        objTCQueueLog.Insert()

                                        Dim objEDIQueueLog As New EDIQueueLog
                                        objEDIQueueLog.EDIQueueID = 0
                                        objEDIQueueLog.DomainID = CCommon.ToLong(domainID)
                                        objEDIQueueLog.OppID = lngOppId
                                        objEDIQueueLog.EDIType = 85011
                                        objEDIQueueLog.Log = "850 Partially Created"
                                        objEDIQueueLog.IsSuccess = True
                                        objEDIQueueLog.ExceptionMessage = ""
                                        objEDIQueueLog.Insert()

                                        Generate855Acknowledge(True, marketPlaceName, marketplaceSettingId, "", Path.GetFileName(file), PONum, CCommon.ToLong(domainID), lngOppId)

                                        MoveFileToErrorFolder(file, Path.GetFileName(file))
                                    ElseIf (EDI850Status = 1) Then

                                        Dim objTCQueueLog As New TrueCommerceLog
                                        objTCQueueLog.DomainID = CCommon.ToLong(domainID)
                                        objTCQueueLog.FileName = Path.GetFileName(file)
                                        objTCQueueLog.PurchaseOrder = "SO: " + CCommon.ToString(lngOppId)
                                        objTCQueueLog.Message = "SO created successfully"
                                        objTCQueueLog.Insert()

                                        Dim objEDIQueueLog As New EDIQueueLog
                                        objEDIQueueLog.EDIQueueID = 0
                                        objEDIQueueLog.DomainID = CCommon.ToLong(domainID)
                                        objEDIQueueLog.OppID = lngOppId
                                        objEDIQueueLog.EDIType = 85012
                                        objEDIQueueLog.Log = "850 SO Created"
                                        objEDIQueueLog.IsSuccess = True
                                        objEDIQueueLog.ExceptionMessage = ""
                                        objEDIQueueLog.Insert()

                                        Generate855Acknowledge(True, marketPlaceName, marketplaceSettingId, "", Path.GetFileName(file), PONum, CCommon.ToLong(domainID), lngOppId)

                                        System.IO.File.Delete(file)
                                    End If

                                    objTransactionScope.Complete()
                                End Using
                            End If
                        Else
                            Dim objTCQueueLog As New TrueCommerceLog
                            objTCQueueLog.DomainID = CCommon.ToLong(domainID)
                            objTCQueueLog.FileName = Path.GetFileName(file)
                            objTCQueueLog.PurchaseOrder = "850_" + Path.GetFileName(file)
                            objTCQueueLog.Message = "Inbound Item Mapping is not set."
                            objTCQueueLog.Insert()

                            If Not String.IsNullOrEmpty(PONum.Trim()) Then
                                Generate855Acknowledge(False, marketPlaceName, marketplaceSettingId, "Inbound Item Mapping is not set.", Path.GetFileName(file), PONum, CCommon.ToLong(domainID), 0)
                            End If

                            MoveFileToErrorFolder(file, Path.GetFileName(file))
                        End If
                    End If
                Else
                    Dim objTCQueueLog As New TrueCommerceLog
                    objTCQueueLog.DomainID = CCommon.ToLong(domainID)
                    objTCQueueLog.FileName = Path.GetFileName(file)
                    objTCQueueLog.PurchaseOrder = "850_" + Path.GetFileName(file)
                    objTCQueueLog.Message = "Invalid domain data."
                    objTCQueueLog.Insert()

                    If Not String.IsNullOrEmpty(PONum.Trim()) Then
                        Generate855Acknowledge(False, marketPlaceName, marketplaceSettingId, "Invalid domain data", Path.GetFileName(file), PONum, CCommon.ToLong(domainID), 0)
                    End If

                    MoveFileToErrorFolder(file, Path.GetFileName(file))
                End If

            Catch ex As Exception
                Dim objTCQueueLog As New TrueCommerceLog
                objTCQueueLog.DomainID = CCommon.ToLong(domainID)
                objTCQueueLog.FileName = Path.GetFileName(file)
                objTCQueueLog.PurchaseOrder = "850_" + Path.GetFileName(file)
                objTCQueueLog.Message = "SO Creation fails: " + ex.Message.ToString
                objTCQueueLog.Insert()

                If Not String.IsNullOrEmpty(PONum.Trim()) Then
                    Generate855Acknowledge(False, marketPlaceName, marketplaceSettingId, ex.Message, Path.GetFileName(file), PONum, CCommon.ToLong(domainID), 0)
                End If

                MoveFileToErrorFolder(file, Path.GetFileName(file))
            End Try
        End Sub

        Private Function createSet() As DataSet
            Try
                Dim dsTemp As New DataSet
                Dim dtItem As New DataTable
                Dim dtSerItem As New DataTable
                Dim dtChildItems As New DataTable
                dtItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtItem.Columns.Add("numItemCode")
                dtItem.Columns.Add("numItemClassification")
                dtItem.Columns.Add("numUnitHour", GetType(Decimal))
                dtItem.Columns.Add("monPrice", GetType(Decimal))
                dtItem.Columns.Add("monSalePrice", GetType(Decimal))
                dtItem.Columns.Add("numUOM")
                dtItem.Columns.Add("vcUOMName")
                dtItem.Columns.Add("UOMConversionFactor")

                dtItem.Columns.Add("monTotAmount", GetType(Decimal))
                dtItem.Columns.Add("numSourceID")
                dtItem.Columns.Add("vcItemDesc")
                dtItem.Columns.Add("vcModelID")
                dtItem.Columns.Add("numWarehouseID")
                dtItem.Columns.Add("vcItemName")
                dtItem.Columns.Add("Warehouse")
                dtItem.Columns.Add("numWarehouseItmsID")
                dtItem.Columns.Add("ItemType")
                dtItem.Columns.Add("Attributes")
                dtItem.Columns.Add("AttributeIDs")
                dtItem.Columns.Add("Op_Flag")
                dtItem.Columns.Add("bitWorkOrder")
                dtItem.Columns.Add("numMaxWorkOrderQty")
                dtItem.Columns.Add("vcInstruction")
                dtItem.Columns.Add("bintCompliationDate")
                dtItem.Columns.Add("numWOAssignedTo")

                dtItem.Columns.Add("DropShip", GetType(Boolean))
                dtItem.Columns.Add("bitDiscountType", GetType(Boolean))
                dtItem.Columns.Add("fltDiscount", GetType(Decimal))
                dtItem.Columns.Add("TotalDiscountAmount", GetType(Decimal))
                dtItem.Columns.Add("monTotAmtBefDiscount", GetType(Decimal))

                dtItem.Columns.Add("bitTaxable0")
                dtItem.Columns.Add("Tax0", GetType(Decimal))
                dtItem.Columns.Add("TotalTax", GetType(Decimal))

                dtItem.Columns.Add("numVendorWareHouse")
                dtItem.Columns.Add("numShipmentMethod")
                dtItem.Columns.Add("numSOVendorId")

                dtItem.Columns.Add("numProjectID")
                dtItem.Columns.Add("numProjectStageID")
                dtItem.Columns.Add("charItemType")
                dtItem.Columns.Add("numToWarehouseItemID") 'added by chintan for stock transfer
                dtItem.Columns.Add("bitIsAuthBizDoc", GetType(Boolean))
                dtItem.Columns.Add("numUnitHourReceived")
                dtItem.Columns.Add("numQtyShipped")
                dtItem.Columns.Add("vcBaseUOMName")
                dtItem.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
                dtItem.Columns.Add("fltItemWeight")
                dtItem.Columns.Add("IsFreeShipping")
                dtItem.Columns.Add("fltHeight")
                dtItem.Columns.Add("fltWidth")
                dtItem.Columns.Add("fltLength")
                dtItem.Columns.Add("vcSKU", GetType(String))
                dtItem.Columns.Add("bitCouponApplied")
                dtItem.Columns.Add("bitItemPriceApprovalRequired")
                dtItem.Columns.Add("bitHasKitAsChild", GetType(Boolean))
                dtItem.Columns.Add("KitChildItems", GetType(String))
                dtItem.Columns.Add("numContainer")
                dtItem.Columns.Add("numContainerQty")
                dtItem.Columns.Add("numPromotionID")
                dtItem.Columns.Add("bitPromotionTriggered")
                dtItem.Columns.Add("vcPromotionDetail")
                dtItem.Columns.Add("numCost", GetType(Decimal))
                dtItem.Columns.Add("numSortOrder")
                dtItem.Columns.Add("vcVendorNotes")
                dtItem.Columns.Add("CustomerPartNo")
                dtItem.Columns.Add("ItemReleaseDate")
                dtItem.Columns.Add("Location")
                dtItem.Columns.Add("numShipToAddressID")
                dtItem.Columns.Add("ShipToFullAddress")
                dtItem.Columns.Add("InclusionDetail")
                dtItem.Columns.Add("OnHandAllocation")
                dtItem.Columns.Add("bitMappingRequired", GetType(Boolean))
                dtItem.Columns.Add("vcASIN", GetType(String))

                'If (pageType = PageTypeEnum.Sales) Then
                '    '''Loading Other Taxes
                '    Dim dtTaxTypes As DataTable
                '    Dim ObjTaxItems As New TaxDetails
                '    ObjTaxItems.DomainID = Session("DomainID")
                '    dtTaxTypes = ObjTaxItems.GetTaxItems

                '    For Each drTax As DataRow In dtTaxTypes.Rows
                '        dtItem.Columns.Add("Tax" & drTax("numTaxItemID"), GetType(Decimal))
                '        dtItem.Columns.Add("bitTaxable" & drTax("numTaxItemID"))
                '    Next

                '    Dim dr As DataRow
                '    dr = dtTaxTypes.NewRow
                '    dr("numTaxItemID") = 0
                '    dr("vcTaxName") = "Sales Tax(Default)"
                '    dtTaxTypes.Rows.Add(dr)

                '    chkTaxItems.DataTextField = "vcTaxName"
                '    chkTaxItems.DataValueField = "numTaxItemID"
                '    chkTaxItems.DataSource = dtTaxTypes
                '    chkTaxItems.DataBind()
                'End If

                dtSerItem.Columns.Add("numWarehouseItmsDTLID")
                dtSerItem.Columns.Add("numWItmsID")
                dtSerItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtSerItem.Columns.Add("vcSerialNo")
                dtSerItem.Columns.Add("Comments")
                dtSerItem.Columns.Add("Attributes")

                dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
                dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtChildItems.Columns.Add("numItemCode")
                dtChildItems.Columns.Add("numQtyItemsReq")
                dtChildItems.Columns.Add("vcItemName")
                dtChildItems.Columns.Add("monListPrice")
                dtChildItems.Columns.Add("UnitPrice")
                dtChildItems.Columns.Add("charItemType")
                dtChildItems.Columns.Add("txtItemDesc")
                dtChildItems.Columns.Add("numWarehouseItmsID")
                dtChildItems.Columns.Add("Op_Flag")
                dtChildItems.Columns.Add("ItemType")
                dtChildItems.Columns.Add("Attr")
                dtChildItems.Columns.Add("vcWareHouse")

                dtItem.TableName = "Item"
                dtSerItem.TableName = "SerialNo"
                dtChildItems.TableName = "ChildItems"

                dsTemp.Tables.Add(dtItem)
                dsTemp.Tables.Add(dtSerItem)
                dsTemp.Tables.Add(dtChildItems)
                dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
                dtChildItems.PrimaryKey = New DataColumn() {dsTemp.Tables(2).Columns("numOppChildItemID")}

                Return dsTemp
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function CalculateItemSalePrice(ByVal price As Decimal, ByVal quantity As Decimal, ByVal discount As Decimal, ByVal isPercent As Boolean, ByVal uomConversionFactor As Decimal) As Decimal
            Try
                Dim SalePrice As Decimal

                If isPercent Then
                    SalePrice = price - (price * (discount / 100))
                Else
                    If quantity > 0 Then
                        SalePrice = ((price * (quantity * uomConversionFactor)) - discount) / (quantity * uomConversionFactor)
                    Else
                        SalePrice = 0
                    End If
                End If
                Return SalePrice
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function AddItemtoDataTable(ByRef objCommon As BACRM.BusinessLogic.Common.CCommon,
                            ByRef dsTemp As DataSet,
                            ByVal AddMode As Boolean,
                            ByVal ItemTypeValue As Char,
                            ByVal ItemTypeText As String,
                            ByVal DropShip As Boolean,
                            ByVal IsKit As String,
                            ByVal ItemCode As Long,
                            ByVal Units As Double,
                            ByVal Price As Decimal,
                            ByVal Desc As String,
                            ByVal WareHouseItemID As Long,
                            ByVal ItemName As String,
                            ByVal Warehouse As String,
                            ByVal OppItemID As Long,
                            ByVal ModelId As String,
                            ByVal UOM As Long,
                            ByVal vcUOMName As String,
                            ByVal UOMConversionFactor As Decimal,
                            ByVal PageType As String,
                            ByVal IsDiscountInPer As Boolean,
                            ByVal Discount As Decimal,
                            ByVal Taxable As String,
                            ByVal Tax As String,
                            ByVal CustomerPartNo As String,
                            Optional ByVal bitWorkOrder As Boolean = False,
                            Optional ByVal vcInstruction As String = "",
                            Optional ByVal bintCompliationDate As String = "",
                            Optional ByVal numWOAssignedTo As Long = 0,
                            Optional ByVal numVendorWareHouse As Long = 0,
                            Optional ByVal numShipmentMethod As Long = 0,
                            Optional ByVal numSOVendorId As Long = 0,
                            Optional ByVal numProjectID As Long = 0,
                            Optional ByVal numProjectStageID As Long = 0,
                            Optional ByVal bDirectAdd As Boolean = False,
                            Optional ByVal ToWarehouseItemID As Long = 0,
                            Optional ByVal IsPOS As Boolean = False,
                            Optional ByVal vcBaseUOMName As String = "",
                            Optional ByVal numPrimaryVendorID As Long = 0,
                            Optional ByVal fltItemWeight As Double = 0,
                            Optional ByVal IsFreeShipping As Boolean = False,
                            Optional ByVal fltHeight As Double = 0,
                            Optional ByVal fltWidth As Double = 0,
                            Optional ByVal fltLength As Double = 0,
                            Optional ByVal strSKU As String = "",
                            Optional ByVal dtItemColumnTableInfo As DataTable = Nothing,
                            Optional ByVal objItem As CItems = Nothing,
                            Optional ByVal primaryVendorCost As Decimal = 0,
                            Optional ByVal salePrice As Decimal = 0,
                            Optional ByVal numMaxWOQty As Integer = 0,
                            Optional ByVal vcAttributes As String = "",
                            Optional ByVal vcAttributeIDs As String = "",
                            Optional ByVal numContainer As Integer = -1,
                            Optional ByVal numContainerQty As Integer = -1,
                            Optional ByVal numItemClassification As Long = 0,
                            Optional ByVal numPromotionID As Long = 0,
                            Optional ByVal IsPromotionTriggered As Boolean = False,
                            Optional ByVal vcPromotionDetail As String = "",
                            Optional ByVal numSortOrder As Long = 0,
                            Optional ByVal numCost As Double = 0,
                            Optional ByVal vcVendorNotes As String = "",
                            Optional ByVal itemReleaseDate As String = "",
                            Optional ByVal ShipFromLocation As String = "",
                            Optional ByVal numShipToAddressID As Long = 0,
                            Optional ByVal ShipToFullAddress As String = "",
                            Optional ByVal InclusionDetail As String = "",
                            Optional ByVal OnHandAllocation As String = "",
                            Optional ByVal isMappingRequired As Boolean = False,
                            Optional ByVal vcASIN As String = ""
                             ) As DataSet
            Try

                Dim dtItem As DataTable
                Dim dtSerItem As DataTable
                Dim dtChildItems As DataTable

                dtItem = dsTemp.Tables(0)
                dtSerItem = dsTemp.Tables(1)
                dtChildItems = dsTemp.Tables(2)

                If Not dtItem.Columns.Contains("fltItemWeight") Then dtItem.Columns.Add("fltItemWeight")

                If Not dtItem.Columns.Contains("IsFreeShipping") Then dtItem.Columns.Add("IsFreeShipping")
                If Not dtItem.Columns.Contains("fltHeight") Then dtItem.Columns.Add("fltHeight")
                If Not dtItem.Columns.Contains("fltWidth") Then dtItem.Columns.Add("fltWidth")
                If Not dtItem.Columns.Contains("fltLength") Then dtItem.Columns.Add("fltLength")
                If Not dtItem.Columns.Contains("numSortOrder") Then dtItem.Columns.Add("numSortOrder")


                Dim dr, dr2 As DataRow
                Dim lngOppItemId As Long

                If AddMode = True Then
                    'If Not (dsTemp.Tables(0).Select("numItemCode='" & ItemCode & "'" & IIf(WareHouseItemID > 0, " and numWarehouseItmsID = '" & WareHouseItemID & "'", "") & " and DropShip ='" & DropShip & "'").Length > 0) Then
                    '    If (HttpContext.Current.Session("ShippingItem") IsNot Nothing) Then
                    '    Else
                    '        Return dsTemp
                    '    End If

                    'End If

                    dr = dtItem.NewRow
                    dr("numSortOrder") = numSortOrder
                    dr("Op_Flag") = 1

                    Dim objOpportunity As New MOpportunity
                    Dim maxOppItemID As Long = objOpportunity.GetMaxOppItemID()

                    Dim rnd As New Random
                    Dim tempOppItemID As Long
                    Do
                        tempOppItemID = rnd.Next(maxOppItemID, maxOppItemID + 20000)
                    Loop While dtItem.Select("numoppitemtCode=" & tempOppItemID).Length > 0

                    dr("numoppitemtCode") = tempOppItemID
                    dr("numItemCode") = ItemCode
                    dr("vcModelID") = ModelId

                    If dtItem.Columns.Contains("vcASIN") Then
                        dr("vcASIN") = vcASIN
                    End If
                    If dtItem.Columns.Contains("bitMappingRequired") Then
                        dr("bitMappingRequired") = isMappingRequired
                    End If

                    ' dr("CustomerPartNo") = CustomerPartNo

                    dr("ItemType") = ItemTypeText
                    dr("vcItemName") = ItemName

                    If numProjectID > 0 AndAlso numProjectStageID > 0 Then
                        dr("numProjectID") = numProjectID
                        dr("numProjectStageID") = numProjectStageID
                    Else
                        dr("numProjectID") = 0
                        dr("numProjectStageID") = 0
                    End If

                    dr("charItemType") = ItemTypeValue
                    dr("bitIsAuthBizDoc") = False
                    dr("numUnitHourReceived") = 0
                    dr("numQtyShipped") = 0
                    dr("numItemClassification") = numItemClassification
                ElseIf IsPOS = True Then
                    If OppItemID > 0 Then
                        dr = dtItem.Rows.Find(OppItemID)
                        lngOppItemId = dr("numoppitemtCode")

                    End If

                Else
                    If OppItemID > 0 Then
                        dr = dtItem.Rows.Find(OppItemID)
                        lngOppItemId = dr("numoppitemtCode")
                    End If
                End If

                If dtItem.Columns.Contains("CustomerPartNo") Then
                    dr("CustomerPartNo") = CustomerPartNo
                End If

                If dtItem.Columns.Contains("numOnHand") AndAlso Not objItem Is Nothing Then
                    dr("numOnHand") = objItem.OnHand
                End If

                If dtItem.Columns.Contains("numMaxWorkOrderQty") Then
                    dr("numMaxWorkOrderQty") = numMaxWOQty
                End If

                If dtItem.Columns.Contains("ItemReleaseDate") Then
                    dr("ItemReleaseDate") = itemReleaseDate
                End If

                If dtItem.Columns.Contains("Location") Then
                    dr("Location") = ShipFromLocation
                End If

                If dtItem.Columns.Contains("numShipToAddressID") Then
                    dr("numShipToAddressID") = numShipToAddressID
                End If

                If dtItem.Columns.Contains("ShipToFullAddress") Then
                    dr("ShipToFullAddress") = ShipToFullAddress
                End If

                If dtItem.Columns.Contains("InclusionDetail") Then
                    dr("InclusionDetail") = InclusionDetail
                End If

                If dtItem.Columns.Contains("OnHandAllocation") Then
                    dr("OnHandAllocation") = OnHandAllocation
                End If

                'Strat - Changed By Sandeep
                'Reason - Following code is removed from above if condition because it is also required when item is updated.
                dr("bitWorkOrder") = bitWorkOrder

                If bitWorkOrder = True Then
                    dr("vcInstruction") = vcInstruction
                    dr("bintCompliationDate") = bintCompliationDate
                    dr("numWOAssignedTo") = numWOAssignedTo
                End If
                'End - Changed By Sandeep

                If objItem IsNot Nothing Then
                    dr("fltItemWeight") = If(fltItemWeight = 0, CCommon.ToDecimal(objItem.Weight), fltItemWeight)
                    dr("IsFreeShipping") = IsFreeShipping
                    dr("fltHeight") = If(fltHeight = 0, CCommon.ToDecimal(objItem.Height), fltHeight)
                    dr("fltWidth") = If(fltWidth = 0, CCommon.ToDecimal(objItem.Width), fltWidth)
                    dr("fltLength") = If(fltLength = 0, CCommon.ToDecimal(objItem.Length), fltLength)

                    If dtItem.Columns.Contains("bitHasKitAsChild") Then
                        dr("bitHasKitAsChild") = objItem.bitHasKitAsChild
                    End If

                    If dtItem.Columns.Contains("KitChildItems") Then
                        dr("KitChildItems") = objItem.vcKitChildItems
                    End If
                End If

                dr("vcItemDesc") = Replace(Desc, "'", "''")

                If dtItem.Columns.Contains("vcVendorNotes") Then
                    dr("vcVendorNotes") = vcVendorNotes
                End If
                dr("numUnitHour") = Units

                If dtItem.Columns.Contains("numOrigUnitHour") Then
                    dr("numOrigUnitHour") = Units
                End If

                dr("monPrice") = Price

                If dtItem.Columns.Contains("monSalePrice") Then
                    dr("monSalePrice") = salePrice
                Else
                    salePrice = Price
                End If

                dr("numUOM") = UOM
                dr("vcUOMName") = vcUOMName
                dr("UOMConversionFactor") = UOMConversionFactor
                dr("monTotAmtBefDiscount") = Math.Truncate(10000 * CCommon.ToDouble((Units * UOMConversionFactor) * Price)) / 10000
                dr("vcBaseUOMName") = vcBaseUOMName

                If dtItem.Columns.Contains("bitItemPriceApprovalRequired") Then
                    dr("bitItemPriceApprovalRequired") = False
                End If

                dr("bitDiscountType") = IIf(IsDiscountInPer = True, 0, 1)
                dr("fltDiscount") = Discount

                If IsDiscountInPer = True Then
                    If Discount = 0 Then
                        dr("monTotAmount") = Math.Truncate(10000 * CCommon.ToDouble((Units * UOMConversionFactor) * salePrice)) / 10000
                    Else
                        dr("monTotAmount") = Math.Truncate(10000 * (CCommon.ToDouble(dr("monTotAmtBefDiscount")) - CCommon.ToDouble(dr("monTotAmtBefDiscount") * dr("fltDiscount") / 100))) / 10000
                    End If
                Else
                    If Discount = 0 Then
                        dr("monTotAmount") = Math.Truncate(10000 * CCommon.ToDouble((Units * UOMConversionFactor) * salePrice)) / 10000
                    Else
                        dr("monTotAmount") = CCommon.ToDouble(dr("monTotAmtBefDiscount") - dr("fltDiscount"))
                    End If
                End If

                If dtItem.Columns.Contains("TotalDiscountAmount") Then
                    If Not IsDiscountInPer Then
                        dr("TotalDiscountAmount") = dr("fltDiscount")
                    Else
                        If dr("monTotAmount") > dr("monTotAmtBefDiscount") Then
                            dr("TotalDiscountAmount") = 0
                        Else
                            dr("TotalDiscountAmount") = dr("monTotAmtBefDiscount") - dr("monTotAmount")
                        End If
                    End If
                End If

                dr("numVendorWareHouse") = numVendorWareHouse
                dr("numShipmentMethod") = numShipmentMethod
                dr("numSOVendorId") = numSOVendorId
                dr("vcSKU") = strSKU

                'If PageType = "Sales" AndAlso chkTaxItems IsNot Nothing Then
                '    Dim k As Integer
                '    Dim strApplicable(), strTax() As String
                '    strApplicable = Taxable.Split(",")
                '    strTax = Tax.Split(",")

                '    For k = 0 To chkTaxItems.Items.Count - 1
                '        If strApplicable.Length > k Then
                '            If CCommon.ToBool(strApplicable(k)) = True Then
                '                Dim decTaxValue As Decimal = CCommon.ToDecimal(strTax(k).Split("#")(0))
                '                Dim tintTaxType As Short = CCommon.ToShort(strTax(k).Split("#")(1))

                '                If tintTaxType = 2 Then 'FLAT AMOUNT
                '                    dr("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * (dr("numUnitHour") * dr("UOMConversionFactor"))
                '                Else 'PERCENTAGE
                '                    dr("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * dr("monTotAmount") / 100
                '                End If

                '                dr("bitTaxable" & chkTaxItems.Items(k).Value) = True
                '            Else
                '                dr("Tax" & chkTaxItems.Items(k).Value) = 0
                '                dr("bitTaxable" & chkTaxItems.Items(k).Value) = False
                '            End If
                '        End If
                '    Next
                'End If

                If ItemTypeValue = "P" Then
                    If DropShip = False Then
                        Dim objItems As New CItems
                        objItems.WareHouseItemID = WareHouseItemID
                        dr("numWarehouseID") = objItems.GetWarehouseID
                        dr("Warehouse") = Warehouse
                        dr("numWarehouseItmsID") = WareHouseItemID
                        If dtItem.Columns.Contains("Attributes") Then
                            dr("Attributes") = objCommon.GetAttributesForWarehouseItem(dr("numWarehouseItmsID"), False)
                        End If
                        dr("DropShip") = False
                        dr("numToWarehouseItemID") = ToWarehouseItemID
                        dr("numVendorID") = 0
                    Else
                        dr("numWarehouseID") = DBNull.Value
                        dr("numWarehouseItmsID") = DBNull.Value
                        dr("Warehouse") = ""
                        If dtItem.Columns.Contains("Attributes") Then
                            dr("Attributes") = ""
                        End If
                        dr("DropShip") = True
                        dr("numVendorID") = numPrimaryVendorID
                    End If
                Else
                    dr("numWarehouseID") = DBNull.Value
                    dr("numWarehouseItmsID") = DBNull.Value
                    dr("numVendorID") = numPrimaryVendorID
                    dr("DropShip") = DropShip
                    If AddMode Then
                        If dtItem.Columns.Contains("Attributes") Then
                            dr("Attributes") = vcAttributes
                        End If

                        If dtItem.Columns.Contains("AttributeIDs") Then
                            dr("AttributeIDs") = vcAttributeIDs
                        End If
                    End If
                End If
                If numContainer <> -1 Then
                    If dtItem.Columns.Contains("numContainer") Then
                        dr("numContainer") = numContainer
                    End If
                End If
                If numContainerQty <> -1 Then
                    If dtItem.Columns.Contains("numContainerQty") Then
                        dr("numContainerQty") = numContainerQty

                    End If
                End If

                If dtItem.Columns.Contains("numPromotionID") Then
                    dr("numPromotionID") = numPromotionID
                End If
                If dtItem.Columns.Contains("bitPromotionTriggered") Then
                    dr("bitPromotionTriggered") = IsPromotionTriggered
                End If
                If dtItem.Columns.Contains("vcPromotionDetail") Then
                    dr("vcPromotionDetail") = vcPromotionDetail
                End If
                If dtItem.Columns.Contains("numCost") Then
                    dr("numCost") = numCost
                End If

                If AddMode = True Then
                    dtItem.Rows.Add(dr)
                End If

                If bDirectAdd = False Then
                    If dtChildItems.Rows.Count > 0 Then
                        For i As Int32 = 0 To dtChildItems.Rows.Count - 1
                            If (dsTemp.Tables(0).Select("numItemCode='" & dtChildItems.Rows(i)("numItemCode") & "'").Length = 0) Then
                                dr = dtItem.NewRow

                                dr("Op_Flag") = 1
                                dr("numoppitemtCode") = CType(IIf(IsDBNull(dtItem.Compute("MAX(numoppitemtCode)", "")), 0, dtItem.Compute("MAX(numoppitemtCode)", "")), Integer) + 1 'dtItem.Rows.Count + 1
                                dr("numItemCode") = dtChildItems.Rows(i)("numItemCode")

                                If IsDBNull(dtChildItems.Rows(i)("numQtyItemsReq")) Then
                                    dr("numUnitHour") = 0
                                ElseIf dtChildItems.Rows(i)("numQtyItemsReq").ToString.Length = 0 Then
                                    dr("numUnitHour") = 0
                                Else
                                    dr("numUnitHour") = Math.Abs(CCommon.ToDecimal(dtChildItems.Rows(i)("numQtyItemsReq").ToString()))
                                End If

                                dr("monPrice") = CCommon.ToDecimal(dtChildItems.Rows(i)("monListPrice").ToString())
                                If dtItem.Columns.Contains("monSalePrice") Then
                                    dr("monSalePrice") = CCommon.ToDecimal(dtChildItems.Rows(i)("monListPrice").ToString())
                                End If

                                dr("bitDiscountType") = False
                                dr("fltDiscount") = 0
                                dr("monTotAmtBefDiscount") = IIf(IsDBNull(dr("numUnitHour")), 0, dr("numUnitHour")) * IIf(IsDBNull(dr("monPrice")), 0, dr("monPrice"))

                                dr("monTotAmount") = dr("monTotAmtBefDiscount") - dr("fltDiscount")

                                If PageType = "Sales" Then
                                    dr("Tax0") = 0
                                    dr("bitTaxable0") = False
                                End If

                                dr("vcItemDesc") = dtChildItems.Rows(i)("txtItemDesc")

                                If dtChildItems.Rows(i)("numWarehouseItmsID") <> 0 Then
                                    dr("numWarehouseItmsID") = dtChildItems.Rows(i)("numWarehouseItmsID")
                                    dr("Warehouse") = dtChildItems.Rows(i)("vcWareHouse")
                                Else
                                    dr("numWarehouseItmsID") = DBNull.Value
                                    dr("Warehouse") = DBNull.Value
                                End If

                                dr("DropShip") = False
                                dr("bitIsAuthBizDoc") = False
                                dr("numUnitHourReceived") = 0
                                dr("numQtyShipped") = 0

                                dr("vcItemName") = dtChildItems.Rows(i)("vcItemName")
                                dr("ItemType") = dtChildItems.Rows(i)("charItemType")

                                dr("UOMConversionFactor") = 1
                                dr("numUOM") = 0
                                dr("vcUOMName") = ""
                                dr("bitWorkOrder") = False
                                dr("vcBaseUOMName") = ""

                                dr("numVendorWareHouse") = "0"
                                dr("numShipmentMethod") = "0"
                                dr("numSOVendorId") = "0"

                                If numProjectID > 0 AndAlso numProjectStageID > 0 Then
                                    dr("numProjectID") = numProjectID
                                    dr("numProjectStageID") = numProjectStageID
                                Else
                                    dr("numProjectID") = 0
                                    dr("numProjectStageID") = 0
                                End If
                                dr("charItemType") = dtChildItems.Rows(i)("charItemType")

                                dtItem.Rows.Add(dr)
                            End If
                        Next
                    End If
                End If

                dr.AcceptChanges()
                dsTemp.AcceptChanges()

                If PageType = "Sales" Or AddMode = False Then
                    If dtSerItem.ParentRelations.Count = 0 Then dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
                End If

                Return dsTemp
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub UpdateSOAddress(ByVal AddressList As XmlDocument, ByVal numOppId As Long, ByVal CompanyId As Long, ByVal CompanyName As String, ByVal numContactId As Long, ByVal numDomainID As Long, file As String, ByVal UserContactId As Long)
            Dim objOpp As New MOpportunity

            Try
                ''''' Update Bill Address'''''''''''''''''
                objOpp.OpportunityId = numOppId
                objOpp.Mode = 0
                objOpp.CompanyID = CompanyId

                If (AddressList.SelectSingleNode("descendant::BillToName") IsNot Nothing) Then
                    objOpp.AddressName = AddressList.SelectSingleNode("descendant::BillToName").InnerText()
                    objOpp.BillCompanyName = objOpp.AddressName
                Else
                    objOpp.AddressName = ""
                    objOpp.BillCompanyName = CompanyName
                End If

                If (AddressList.SelectSingleNode("descendant::BillToAddress-LineOne") IsNot Nothing) Then
                    objOpp.BillStreet = AddressList.SelectSingleNode("descendant::BillToAddress-LineOne").InnerText()
                Else
                    objOpp.BillStreet = ""
                End If

                If (AddressList.SelectSingleNode("descendant::BillToAddress-LineTwo") IsNot Nothing) Then
                    objOpp.BillStreet = objOpp.BillStreet + " " + AddressList.SelectSingleNode("descendant::BillToAddress-LineTwo").InnerText()
                Else
                    objOpp.BillStreet = objOpp.BillStreet + ""
                End If

                If (AddressList.SelectSingleNode("descendant::BillToCity") IsNot Nothing) Then
                    objOpp.BillCity = AddressList.SelectSingleNode("descendant::BillToCity").InnerText()
                Else
                    objOpp.BillCity = ""
                End If

                If (AddressList.SelectSingleNode("descendant::BillToZipcode") IsNot Nothing) Then
                    objOpp.BillPostal = AddressList.SelectSingleNode("descendant::BillToZipcode").InnerText()
                Else
                    objOpp.BillPostal = ""
                End If

                Dim strBillState As String
                If (AddressList.SelectSingleNode("descendant::BillToState") IsNot Nothing) Then
                    strBillState = AddressList.SelectSingleNode("descendant::BillToState").InnerText()
                Else
                    strBillState = ""
                End If

                Dim strBillCountry As String
                If (AddressList.SelectSingleNode("descendant::BillToCountry") IsNot Nothing) Then
                    strBillCountry = AddressList.SelectSingleNode("descendant::BillToCountry").InnerText()
                Else
                    strBillCountry = ""
                End If

                Dim dtStateCountry As New DataTable
                dtStateCountry = GetStateNCountryIds(strBillCountry, strBillState, numDomainID)

                If (dtStateCountry IsNot Nothing And dtStateCountry.Rows.Count > 0) Then
                    objOpp.BillState = CCommon.ToLong(dtStateCountry.Rows(0)("StateId"))
                    objOpp.BillCountry = CCommon.ToLong(dtStateCountry.Rows(0)("CountryId"))
                Else
                    objOpp.BillState = 0
                    objOpp.BillCountry = 0
                End If

                objOpp.DomainID = numDomainID
                objOpp.UserCntID = UserContactId
                objOpp.ContactID = numContactId
                objOpp.IsAltContact = False

                objOpp.UpdateOpportunityAddress()
                ''''''''''''''''''''''''''''''

                ''''''''''''Update Ship Address ''''''''''''''

                objOpp.OpportunityId = numOppId
                objOpp.Mode = 1
                objOpp.CompanyID = CompanyId
                objOpp.ShipCompanyName = CompanyName

                If (AddressList.SelectSingleNode("descendant::ShipToName") IsNot Nothing) Then
                    objOpp.AddressName = AddressList.SelectSingleNode("descendant::ShipToName").InnerText()
                    objOpp.ShipCompanyName = objOpp.AddressName
                Else
                    objOpp.ShipCompanyName = CompanyName
                    objOpp.AddressName = ""
                End If

                If (AddressList.SelectSingleNode("descendant::ShipToAddress") IsNot Nothing) Then
                    objOpp.BillStreet = AddressList.SelectSingleNode("descendant::ShipToAddress").InnerText()
                Else
                    objOpp.BillStreet = ""
                End If

                If (AddressList.SelectSingleNode("descendant::ShipToCity") IsNot Nothing) Then
                    objOpp.BillCity = AddressList.SelectSingleNode("descendant::ShipToCity").InnerText()
                Else
                    objOpp.BillCity = ""
                End If

                If (AddressList.SelectSingleNode("descendant::ShipToZipcode") IsNot Nothing) Then
                    objOpp.BillPostal = AddressList.SelectSingleNode("descendant::ShipToZipcode").InnerText()
                Else
                    objOpp.BillPostal = ""
                End If

                Dim strShipState As String
                If (AddressList.SelectSingleNode("descendant::ShipToState") IsNot Nothing) Then
                    strShipState = AddressList.SelectSingleNode("descendant::ShipToState").InnerText()
                Else
                    strShipState = ""
                End If

                Dim strShipCountry As String
                If (AddressList.SelectSingleNode("descendant::ShipToCountry") IsNot Nothing) Then
                    strShipCountry = AddressList.SelectSingleNode("descendant::ShipToCountry").InnerText()
                Else
                    strShipCountry = ""
                End If

                dtStateCountry = GetStateNCountryIds(strShipCountry, strShipState, numDomainID)

                If (dtStateCountry IsNot Nothing And dtStateCountry.Rows.Count > 0) Then
                    objOpp.BillState = CCommon.ToLong(dtStateCountry.Rows(0)("StateId"))
                    objOpp.BillCountry = CCommon.ToLong(dtStateCountry.Rows(0)("CountryId"))
                Else
                    objOpp.BillState = 0
                    objOpp.BillCountry = 0
                End If

                objOpp.DomainID = numDomainID
                objOpp.UserCntID = UserContactId 'Session("UserContactID")
                objOpp.ContactID = numContactId
                objOpp.IsAltContact = False

                objOpp.UpdateOpportunityAddress()

                '''''''''''''''''''''''''''''''''''''''''''''''''

            Catch ex As Exception
                Dim objTCQueueLog As New TrueCommerceLog
                ' objTCQueueLog.TrueCommerceQueueID = Convert.ToInt64(PONum)
                objTCQueueLog.DomainID = CCommon.ToLong(DomainID)
                objTCQueueLog.FileName = Path.GetFileName(file)
                objTCQueueLog.PurchaseOrder = "850"
                objTCQueueLog.Message = ex.Message.ToString
                objTCQueueLog.Insert()
            End Try
        End Sub

        Private Function GetPendingRecords() As DataTable
            Dim dtPendingRecords As DataTable = Nothing

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtPendingRecords = SqlDAL.ExecuteDatable(connString, "USP_EDIQueue_GetPendingRecords", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try

            Return dtPendingRecords
        End Function

        Private Function GetTrueCommerceUsers() As DataTable
            Dim dtTCUsers As DataTable = Nothing

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                dtTCUsers = SqlDAL.ExecuteDatable(connString, "USP_GetTrueCommerceUsers", arParms)
            Catch ex As Exception
                Throw
            End Try

            Return dtTCUsers
        End Function

        Private Function GetCompanyDetailsFrom850(ByVal numDomainID As Long, ByVal numDivisionID As Long, ByVal vcFirstName As String, ByVal vcLastName As String, ByVal vcEmail As String, ByVal vcMarketplace As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", numDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", numDivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcFirstName", vcFirstName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcLastName", vcLastName, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcEmail", vcEmail, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcMarketplace", vcMarketplace, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetCompanyDetailsFrom850", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try

        End Function

        Private Function GetAdminUser(ByVal numDomainId As Long) As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numDomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetAdminUserForDomain", arParms)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Private Function GetWareHouseItemIDfor850(ByVal numDomainId As Long, ByVal numItemCode As Long, ByVal units As Double, ByVal orderSource As Long, ByVal sourceType As Short, ByVal shipToState As Long, ByVal shipToCountry As Long) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", numDomainId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", numItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQty", units, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numOrderSource", orderSource, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintSourceType", sourceType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numShipToCountry", shipToCountry, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numShipToState", shipToState, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetWarehouseItemIdFor850", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try

        End Function

        Private Function GetStateNCountryIds(ByVal vcCountry As String, ByVal vcState As String, ByVal numDomainId As Long) As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@vcState", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(0).Value = vcState

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCountryName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = vcCountry

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numDomainId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetStateCountryIDs", arParms)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Private Function GetOppItemCodes850(ByVal numOppId As Long) As DataTable

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numOppId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetOppItemCodesFor850", arParms)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Private Function Generate856ForTC(ByVal EDIQueueID As Long, ByVal numDomainID As Long, ByVal numOppID As Long) As Boolean
            Try
                shippingReportID = 0
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", numDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", numOppID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_GetDataForEDI856", sqlParams.ToArray())

                Dim filePath As String

                If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(0).Rows.Count > 0 Then

                    For Each drShippingReport As DataRow In ds.Tables(0).Rows
                        shippingReportID = Convert.ToInt64(drShippingReport("numShippingReportId"))
                        filePath = System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & numDomainID & "\Get\" & numOppID & "-" & shippingReportID & ".xml"
                        Dim xmlDoc As New XmlDocument
                        Dim xmlDeclaration As System.Xml.XmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", Nothing)
                        Dim root As System.Xml.XmlElement = xmlDoc.DocumentElement
                        xmlDoc.InsertBefore(xmlDeclaration, root)
                        Dim elementPickAndPack As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ASNPickAndPack", String.Empty)
                        xmlDoc.AppendChild(elementPickAndPack)

                        Dim elementShipment As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Shipment", String.Empty)
                        elementPickAndPack.AppendChild(elementShipment)

                        Dim xmString As String = "<RowType></RowType>" & vbCrLf &
                                                            "<TransactionType></TransactionType>" & vbCrLf &
                                                            "<AccountingID></AccountingID>" & vbCrLf &
                                                            "<ShipmentID></ShipmentID>" & vbCrLf &
                                                            "<SCAC></SCAC>" & vbCrLf &
                                                            "<CarrierPro></CarrierPro>" & vbCrLf &
                                                            "<BillofLading></BillofLading>" & vbCrLf &
                                                            "<ScheduledDelivery></ScheduledDelivery>" & vbCrLf &
                                                            "<ShipDate>" & Convert.ToString(drShippingReport("vcShipDate")) & "</ShipDate>" & vbCrLf &
                                                            "<ShipToName>" & Convert.ToString(drShippingReport("vcToName")) & "</ShipToName>" & vbCrLf &
                                                            "<ShipToAddress-LineOne>" & Convert.ToString(drShippingReport("vcToAddressLine1")) & "</ShipToAddress-LineOne>" & vbCrLf &
                                                            "<ShipToAddress-LineTwo>" & Convert.ToString(drShippingReport("vcToAddressLine2")) & "</ShipToAddress-LineTwo>" & vbCrLf &
                                                            "<ShipToCity>" & Convert.ToString(drShippingReport("vcToCity")) & "</ShipToCity>" & vbCrLf &
                                                            "<ShipToState>" & Convert.ToString(drShippingReport("vcToState")) & "</ShipToState>" & vbCrLf &
                                                            "<ShipToZip>" & Convert.ToString(drShippingReport("vcToZip")) & "</ShipToZip>" & vbCrLf &
                                                            "<ShipToCountry>" & Convert.ToString(drShippingReport("vcToCountry")) & "</ShipToCountry>" & vbCrLf &
                                                            "<ShiptoAddressCode></ShiptoAddressCode>" & vbCrLf &
                                                            "<ShipVia>" & Convert.ToString(drShippingReport("vcShipVia")) & "</ShipVia>" & vbCrLf &
                                                            "<ShipToType></ShipToType>" & vbCrLf &
                                                            "<PackagingType></PackagingType>" & vbCrLf &
                                                            "<GrossWeight>" & Convert.ToString(Convert.ToDouble(drShippingReport("fltTotalRegularWeight")) + Convert.ToDouble(drShippingReport("fltTotalDimensionalWeight"))) & "</GrossWeight>" & vbCrLf &
                                                            "<GrossWeightUOM>lbs</GrossWeightUOM>" & vbCrLf &
                                                            "<ofCartonsShipped></ofCartonsShipped>" & vbCrLf &
                                                            "<CarrierTrailer></CarrierTrailer>" & vbCrLf &
                                                            "<TrailerInitital></TrailerInitital>" & vbCrLf &
                                                            "<ShipFromName>" & Convert.ToString(drShippingReport("vcFromName")) & "</ShipFromName>" & vbCrLf &
                                                            "<ShipFromAddress-LineOne>" & Convert.ToString(drShippingReport("vcFromAddressLine1")) & "</ShipFromAddress-LineOne>" & vbCrLf &
                                                            "<ShipFromAddress-LineTwo>" & Convert.ToString(drShippingReport("vcFromAddressLine2")) & "</ShipFromAddress-LineTwo>" & vbCrLf &
                                                            "<ShipFromCity>" & Convert.ToString(drShippingReport("vcFromCity")) & "</ShipFromCity>" & vbCrLf &
                                                            "<ShipFromState>" & Convert.ToString(drShippingReport("vcFromState")) & "</ShipFromState>" & vbCrLf &
                                                            "<ShipFromZip>" & Convert.ToString(drShippingReport("vcFromZip")) & "</ShipFromZip>" & vbCrLf &
                                                            "<ShipFromCountry>" & Convert.ToString(drShippingReport("vcFromCountry")) & "</ShipFromCountry>" & vbCrLf &
                                                            "<ShipFromAddressCode></ShipFromAddressCode>" & vbCrLf &
                                                            "<Vendor></Vendor>" & vbCrLf &
                                                            "<DCCode></DCCode>" & vbCrLf &
                                                            "<TransportationMethod></TransportationMethod>" & vbCrLf &
                                                            "<ProductGroup></ProductGroup>" & vbCrLf &
                                                            "<Status></Status>" & vbCrLf &
                                                            "<TimeShipped></TimeShipped>" & vbCrLf &
                                                            "<Order>" & vbCrLf &
                                                                "<RowType></RowType>" & vbCrLf &
                                                                "<PO>" & Convert.ToString(drShippingReport("vcCustomerPO#")) & "</PO>" & vbCrLf &
                                                                "<PODate></PODate>" & vbCrLf &
                                                                "<Invoice></Invoice>" & vbCrLf &
                                                                "<OrderWeight></OrderWeight>" & vbCrLf &
                                                                "<StoreName></StoreName>" & vbCrLf &
                                                                "<StoreNumber></StoreNumber>" & vbCrLf &
                                                                "<MarkForCode></MarkForCode>" & vbCrLf &
                                                                "<Department></Department>" & vbCrLf &
                                                                "<OrderLadingQuantity></OrderLadingQuantity>" & vbCrLf &
                                                                "<PackagingType></PackagingType>"

                        ' Adding EDI Custom Fields to Order     'By Priya
                        Dim objCommon As New CCommon
                        objCommon.DomainID = numDomainID
                        Dim dsEDIOrder As DataSet = objCommon.GetEDICustomFields(19, numOppID)
                        If (dsEDIOrder IsNot Nothing) Then
                            Dim dtEDIOrder As DataTable = dsEDIOrder.Tables(0)
                            If (dtEDIOrder.Rows.Count > 0) Then
                                For Each dr As DataRow In dtEDIOrder.Rows
                                    xmString = xmString & "<" & dr("Fld_label").ToString & ">" & dr("Fld_Value").ToString & "</" & dr("Fld_label").ToString & ">"
                                Next
                            End If
                        End If

                        Dim arrShippingBox As DataRow() = ds.Tables(1).Select("numShippingReportId=" & shippingReportID)

                        If Not arrShippingBox Is Nothing AndAlso arrShippingBox.Length > 0 Then
                            For Each drBox As DataRow In arrShippingBox
                                xmString = xmString & "<Pack>" & vbCrLf &
                                                            "<RowType></RowType>" & vbCrLf &
                                                            "<UCC-128></UCC-128>" & vbCrLf &
                                                            "<PackSize></PackSize>" & vbCrLf &
                                                            "<InnerPackPerOuterPack></InnerPackPerOuterPack>" & vbCrLf &
                                                            "<TrackingNumber>" & Convert.ToString(drBox("vcTrackingNumber")) & "</TrackingNumber>" & vbCrLf &
                                                            "<PackHeight>" & Convert.ToString(drBox("fltHeight")) & "</PackHeight>" & vbCrLf &
                                                            "<PackLength>" & Convert.ToString(drBox("fltLength")) & "</PackLength>" & vbCrLf &
                                                            "<PackWidth>" & Convert.ToString(drBox("fltWidth")) & "</PackWidth>" & vbCrLf &
                                                            "<PackWeight>" & Convert.ToString(drBox("fltTotalWeight")) & "</PackWeight>" & vbCrLf &
                                                            "<QtyofUPCswithinPack>" & Convert.ToString(drBox("Qty")) & "</QtyofUPCswithinPack>" & vbCrLf &
                                                            "<UOMofUPCs></UOMofUPCs>" & vbCrLf &
                                                            "<StoreName></StoreName>" & vbCrLf &
                                                            "<StoreNumber></StoreNumber>"


                                Dim shippingBoxID As Long = Convert.ToInt64(drBox("numBoxID"))
                                Dim arrBoxItems As DataRow() = ds.Tables(2).Select("numBoxID=" & shippingBoxID)
                                If Not arrBoxItems Is Nothing AndAlso arrBoxItems.Length > 0 Then
                                    For Each drBoxItem As DataRow In arrBoxItems
                                        xmString = xmString & "<Item>" & vbCrLf &
                                                                        "<Rowtype></Rowtype>" & vbCrLf &
                                                                        "<Line></Line>" & vbCrLf &
                                                                        "<VendorPart></VendorPart>" & vbCrLf &
                                                                        "<BuyerPart></BuyerPart>" & vbCrLf &
                                                                        "<SKU>" & Convert.ToString(drBoxItem("vcSKU")) & "</SKU>" & vbCrLf &
                                                                        "<UPC>" & Convert.ToString(drBoxItem("vcUPC")) & "</UPC>" & vbCrLf &
                                                                        "<ItemType>" & Convert.ToString(drBoxItem("vcItemType")) & "</ItemType>" & vbCrLf &
                                                                        "<ItemDescription></ItemDescription>" & vbCrLf &
                                                                        "<QuantityShipped>" & Convert.ToString(drBoxItem("numShippedUnits")) & "</QuantityShipped>" & vbCrLf &
                                                                        "<UOM>" & Convert.ToString(drBoxItem("vcUOM")) & "</UOM>" & vbCrLf &
                                                                        "<OrderedQuantity>" & Convert.ToString(drBoxItem("numOrderedUnits")) & "</OrderedQuantity>" & vbCrLf &
                                                                        "<UnitPrice></UnitPrice>" & vbCrLf &
                                                                        "<PackSize></PackSize>" & vbCrLf &
                                                                        "<PackUOM></PackUOM>" & vbCrLf &
                                                                        "<InnerPacksperOuterPack></InnerPacksperOuterPack>" & vbCrLf


                                        ' Adding EDI Custom Fields to Item     'By Priya

                                        Dim dsEDIItem As DataSet = objCommon.GetEDICustomFields(21, CCommon.ToLong(drBoxItem("numoppitemtCode")))
                                        If (dsEDIItem IsNot Nothing) Then
                                            Dim dtEDIItem As DataTable = dsEDIItem.Tables(0)
                                            If (dtEDIItem.Rows.Count > 0) Then
                                                For Each dr As DataRow In dtEDIItem.Rows
                                                    xmString = xmString & "<" & dr("Fld_label").ToString & ">" & dr("Fld_Value").ToString & "</" & dr("Fld_label").ToString & ">"
                                                Next
                                            End If
                                        End If
                                        xmString = xmString & "</Item>"
                                    Next
                                End If

                                xmString = xmString & "</Pack>"
                            Next
                        Else
                            xmString = "<Pack></Pack>"
                        End If

                        xmString = xmString & "</Order>"

                        Dim xfrag As XmlDocumentFragment = xmlDoc.CreateDocumentFragment()
                        xfrag.InnerXml = xmString
                        xmlDoc.DocumentElement.FirstChild.AppendChild(xfrag)

                        xmlDoc.Save(filePath)

                        'Dim objTCQueueLog As New TrueCommerceLog
                        'objTCQueueLog.DomainID = CCommon.ToLong(DomainID)
                        'objTCQueueLog.FileName = "856-" & numOppID & "-" & shippingReportID & ".xml"
                        'objTCQueueLog.PurchaseOrder = "OppID: " + CCommon.ToString(numOppID)
                        'objTCQueueLog.Message = "856 successfully uploaded to TC"
                        'objTCQueueLog.Insert()

                        'System.IO.File.Delete(filePath)
                    Next

                Else
                    Dim objTCQueueLog As New TrueCommerceLog
                    objTCQueueLog.DomainID = CCommon.ToLong(DomainID)
                    objTCQueueLog.FileName = CCommon.ToString(numOppID)
                    objTCQueueLog.PurchaseOrder = "856"
                    objTCQueueLog.Message = "856 Upload Fails :Shipping BizDoc(s) not found to generate 856 document"
                    objTCQueueLog.Insert()
                    Return False
                End If
            Catch ex As Exception
                Dim objTCQueueLog As New TrueCommerceLog
                objTCQueueLog.DomainID = CCommon.ToLong(DomainID)
                objTCQueueLog.FileName = CCommon.ToString(numOppID)
                objTCQueueLog.PurchaseOrder = "856"
                objTCQueueLog.Message = "856 Upload Fails: " + ex.Message.ToString
                objTCQueueLog.Insert()
            End Try
            Return True
        End Function

        Private Function CreateInvoice810(ByVal EDIQueueID As Long, ByVal numDomainID As Long, ByVal numOppID As Long) As Boolean
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", numDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", numOppID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_GetDataForEDIInvoice810", sqlParams.ToArray())

                Dim filePath As String = System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & numDomainID & "\Get\" & "Invoice-" & numOppID & "-" & ".xml"

                If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(1).Rows.Count > 0 Then

                    Dim xmlDoc As New XmlDocument
                    Dim xmlDeclaration As System.Xml.XmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", Nothing)
                    Dim root As System.Xml.XmlElement = xmlDoc.DocumentElement
                    xmlDoc.InsertBefore(xmlDeclaration, root)

                    Dim elementInvoice As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Invoice", String.Empty)
                    xmlDoc.AppendChild(elementInvoice)

                    Dim elementHeader As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Header", String.Empty)
                    elementInvoice.AppendChild(elementHeader)

                    Dim elementRowType As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "RowType", String.Empty)
                    elementHeader.AppendChild(elementRowType)

                    Dim elementTransactionType As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "TransactionType", String.Empty)
                    Dim xtTransactionType As XmlText = xmlDoc.CreateTextNode("810")
                    elementTransactionType.AppendChild(xtTransactionType)
                    elementHeader.AppendChild(elementTransactionType)

                    Dim elementAccountingID As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "AccountingID", String.Empty)
                    Dim xtAccountingID As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("AccountingID")))
                    elementAccountingID.AppendChild(xtAccountingID)
                    elementHeader.AppendChild(elementAccountingID)

                    Dim elementInvoiceName As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Invoice", String.Empty)
                    Dim xtelementInvoice As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("Invoice")))
                    elementInvoiceName.AppendChild(xtelementInvoice)
                    elementHeader.AppendChild(elementInvoiceName)

                    Dim elementInvoiceDate As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "InvoiceDate", String.Empty)
                    Dim xtInvoiceDate As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("InvoiceDate")))
                    elementInvoiceDate.AppendChild(xtInvoiceDate)
                    elementHeader.AppendChild(elementInvoiceDate)

                    Dim elementPO As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "PO", String.Empty)
                    Dim xtPO As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("PO")))
                    elementPO.AppendChild(xtPO)
                    elementHeader.AppendChild(elementPO)

                    Dim elementPODate As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "PODate", String.Empty)
                    Dim xtPODate As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("PODate")))
                    elementPODate.AppendChild(xtPODate)
                    elementHeader.AppendChild(elementPODate)

                    Dim elementDepartmentNumber As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "DepartmentNumber", String.Empty)
                    Dim xtDepartmentNumber As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("DepartmentNumber")))
                    elementDepartmentNumber.AppendChild(xtDepartmentNumber)
                    elementHeader.AppendChild(elementDepartmentNumber)

                    'Dim elementBillofLading As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BillofLading", String.Empty)
                    'Dim xtBillofLading As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("BillofLading")))
                    'elementBillofLading.AppendChild(xtBillofLading)
                    'elementHeader.AppendChild(elementBillofLading)

                    'Dim elementCarrierPro As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "CarrierPro", String.Empty)
                    'Dim xtCarrierPro As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("CarrierPro")))
                    'elementCarrierPro.AppendChild(xtCarrierPro)
                    'elementHeader.AppendChild(elementCarrierPro)

                    'Dim elementSCAC As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "SCAC", String.Empty)
                    'Dim xtSCAC As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("SCAC")))
                    'elementSCAC.AppendChild(xtSCAC)
                    'elementHeader.AppendChild(elementSCAC)

                    Dim elementShipVia As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShipVia", String.Empty)
                    Dim xtShipVia As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ShipVia")))
                    elementShipVia.AppendChild(xtShipVia)
                    elementHeader.AppendChild(elementShipVia)

                    Dim elementShipToName As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShipToName", String.Empty)
                    Dim xtShipToName As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ShipToName")))
                    elementShipToName.AppendChild(xtShipToName)
                    elementHeader.AppendChild(elementShipToName)

                    Dim elementShipToAddressLineOne As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShipToAddress-LineOne", String.Empty)
                    Dim xtShipToAddressLineOne As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ShipToAddress-LineOne")))
                    elementShipToAddressLineOne.AppendChild(xtShipToAddressLineOne)
                    elementHeader.AppendChild(elementShipToAddressLineOne)

                    Dim elementShipToAddressLineTwo As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShipToAddress-LineTwo", String.Empty)
                    Dim xtShipToAddressLineTwo As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ShipToAddress-LineTwo")))
                    elementShipToAddressLineTwo.AppendChild(xtShipToAddressLineTwo)
                    elementHeader.AppendChild(elementShipToAddressLineTwo)

                    Dim elementShipToCity As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShipToCity", String.Empty)
                    Dim xtShipToCity As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ShipToCity")))
                    elementShipToCity.AppendChild(xtShipToCity)
                    elementHeader.AppendChild(elementShipToCity)

                    Dim elementShipToState As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShipToState", String.Empty)
                    Dim xtShipToState As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ShipToState")))
                    elementShipToState.AppendChild(xtShipToState)
                    elementHeader.AppendChild(elementShipToState)

                    Dim elementShipToZipcode As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShipToZipcode", String.Empty)
                    Dim xtShipToZipcode As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ShipToZipcode")))
                    elementShipToZipcode.AppendChild(xtShipToZipcode)
                    elementHeader.AppendChild(elementShipToZipcode)

                    Dim elementShiptoCountry As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShiptoCountry", String.Empty)
                    Dim xtShiptoCountry As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ShiptoCountry")))
                    elementShiptoCountry.AppendChild(xtShiptoCountry)
                    elementHeader.AppendChild(elementShiptoCountry)

                    Dim elementStore As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Store", String.Empty)
                    Dim xtStore As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("Store")))
                    elementStore.AppendChild(xtStore)
                    elementHeader.AppendChild(elementStore)

                    Dim elementBilltoName As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BilltoName", String.Empty)
                    Dim xtBilltoName As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("BilltoName")))
                    elementBilltoName.AppendChild(xtBilltoName)
                    elementHeader.AppendChild(elementBilltoName)

                    Dim elementBilltoAddressLineOne As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BilltoAddress-LineOne", String.Empty)
                    Dim xtBilltoAddressLineOne As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("BilltoAddress-LineOne")))
                    elementBilltoAddressLineOne.AppendChild(xtBilltoAddressLineOne)
                    elementHeader.AppendChild(elementBilltoAddressLineOne)

                    Dim elementBilltoAddressLineTwo As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BilltoAddress-LineTwo", String.Empty)
                    Dim xtBilltoAddressLineTwo As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("BilltoAddress-LineTwo")))
                    elementBilltoAddressLineTwo.AppendChild(xtBilltoAddressLineTwo)
                    elementHeader.AppendChild(elementBilltoAddressLineTwo)

                    Dim elementBilltoCity As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BilltoCity", String.Empty)
                    Dim xtBilltoCity As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("BilltoCity")))
                    elementBilltoCity.AppendChild(xtBilltoCity)
                    elementHeader.AppendChild(elementBilltoCity)

                    Dim elementBilltoState As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BilltoState", String.Empty)
                    Dim xtBilltoState As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("BilltoState")))
                    elementBilltoState.AppendChild(xtBilltoState)
                    elementHeader.AppendChild(elementBilltoState)

                    Dim elementBilltoZipcode As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BilltoZipcode", String.Empty)
                    Dim xtBilltoZipcode As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("BilltoZipcode")))
                    elementBilltoZipcode.AppendChild(xtBilltoZipcode)
                    elementHeader.AppendChild(elementBilltoZipcode)

                    Dim elementBilltoCountry As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BilltoCountry", String.Empty)
                    Dim xtBilltoCountry As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("BilltoCountry")))
                    elementBilltoCountry.AppendChild(xtBilltoCountry)
                    elementHeader.AppendChild(elementBilltoCountry)

                    Dim elementBilltoCode As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BilltoCode", String.Empty)
                    Dim xtBilltoCode As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("BilltoCode")))
                    elementBilltoCode.AppendChild(xtBilltoCode)
                    elementHeader.AppendChild(elementBilltoCode)

                    Dim elementShipDate As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShipDate", String.Empty)
                    Dim xtShipDate As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ShipDate")))
                    elementShipDate.AppendChild(xtShipDate)
                    elementHeader.AppendChild(elementShipDate)

                    Dim elementTermsDescription As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "TermsDescription", String.Empty)
                    Dim xtTermsDescription As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("TermsDescription")))
                    elementTermsDescription.AppendChild(xtTermsDescription)
                    elementHeader.AppendChild(elementTermsDescription)

                    Dim elementNetDaysDue As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "NetDaysDue", String.Empty)
                    Dim xtNetDaysDue As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("NetDaysDue")))
                    elementNetDaysDue.AppendChild(xtNetDaysDue)
                    elementHeader.AppendChild(elementNetDaysDue)

                    Dim elementDiscountDaysDue As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "DiscountDaysDue", String.Empty)
                    Dim xtDiscountDaysDue As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("DiscountDaysDue")))
                    elementDiscountDaysDue.AppendChild(xtDiscountDaysDue)
                    elementHeader.AppendChild(elementDiscountDaysDue)

                    Dim elementDiscountPercent As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "DiscountPercent", String.Empty)
                    Dim xtDiscountPercent As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("DiscountPercent")))
                    elementDiscountPercent.AppendChild(xtDiscountPercent)
                    elementHeader.AppendChild(elementDiscountPercent)

                    Dim elementNote As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Note", String.Empty)
                    Dim xtNote As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("Note")))
                    elementNote.AppendChild(xtNote)
                    elementHeader.AppendChild(elementNote)

                    Dim elementWeight As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Weight", String.Empty)
                    Dim xtWeight As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("Weight")))
                    elementWeight.AppendChild(xtWeight)
                    elementHeader.AppendChild(elementWeight)

                    Dim elementTotalCasesShipped As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "TotalCasesShipped", String.Empty)
                    Dim xtTotalCasesShipped As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("TotalCasesShipped")))
                    elementTotalCasesShipped.AppendChild(xtTotalCasesShipped)
                    elementHeader.AppendChild(elementTotalCasesShipped)

                    Dim elementTaxAmount As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "TaxAmount", String.Empty)
                    Dim xtTaxAmount As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("TaxAmount")))
                    elementTaxAmount.AppendChild(xtTaxAmount)
                    elementHeader.AppendChild(elementTaxAmount)

                    'Dim elementChargeAmount1 As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ChargeAmount1", String.Empty)
                    'Dim xtChargeAmount1 As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ChargeAmount1")))
                    'elementChargeAmount1.AppendChild(xtChargeAmount1)
                    'elementHeader.AppendChild(elementChargeAmount1)

                    'Dim elementChargeAmount2 As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ChargeAmount2", String.Empty)
                    'Dim xtChargeAmount2 As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("ChargeAmount2")))
                    'elementChargeAmount2.AppendChild(xtChargeAmount2)
                    'elementHeader.AppendChild(elementChargeAmount2)

                    'Dim elementAllowancePercent1 As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "AllowancePercent1", String.Empty)
                    'Dim xtAllowancePercent1 As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("AllowancePercent1")))
                    'elementAllowancePercent1.AppendChild(xtAllowancePercent1)
                    'elementHeader.AppendChild(elementAllowancePercent1)

                    'Dim elementAllowanceAmount1 As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "AllowanceAmount1", String.Empty)
                    'Dim xtAllowanceAmount1 As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("AllowanceAmount1")))
                    'elementAllowanceAmount1.AppendChild(xtAllowanceAmount1)
                    'elementHeader.AppendChild(elementAllowanceAmount1)

                    'Dim elementAllowancePercent2 As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "AllowancePercent2", String.Empty)
                    'Dim xtAllowancePercent2 As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("AllowancePercent2")))
                    'elementAllowancePercent2.AppendChild(xtAllowancePercent2)
                    'elementHeader.AppendChild(elementAllowancePercent2)

                    'Dim elementAllowanceAmount2 As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "AllowanceAmount2", String.Empty)
                    'Dim xtAllowanceAmount2 As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("AllowanceAmount2")))
                    'elementAllowanceAmount2.AppendChild(xtAllowanceAmount2)
                    'elementHeader.AppendChild(elementAllowanceAmount2)

                    'EDI Custom Fields for Order

                    Dim objCommon As New CCommon
                    objCommon.DomainID = numDomainID
                    Dim dsEdiFields As DataSet = objCommon.GetEDICustomFields(19, numOppID)
                    If (dsEdiFields IsNot Nothing And dsEdiFields.Tables.Count > 0) Then
                        Dim dtEDI As DataTable = dsEdiFields.Tables(0)
                        If (dtEDI.Rows.Count > 0) Then
                            For Each dr As DataRow In dtEDI.Rows
                                Dim elementEDIField As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, dr("Fld_label").ToString, String.Empty)
                                Dim xtEDIField As XmlText = xmlDoc.CreateTextNode(dr("Fld_Value").ToString)
                                elementEDIField.AppendChild(xtEDIField)
                                elementHeader.AppendChild(elementEDIField)
                            Next
                        End If
                    End If

                    ''Billing Address
                    'Dim elementBillAddress As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BillingAddress", String.Empty)

                    'Dim elementBillStreet As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Street", String.Empty)
                    'Dim xtBillStreet As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcBillStreet")))
                    'elementBillStreet.AppendChild(xtBillStreet)
                    'elementBillAddress.AppendChild(elementBillStreet)

                    'Dim elementBillCity As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "City", String.Empty)
                    'Dim xtBillCity As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcBillCity")))
                    'elementBillCity.AppendChild(xtBillCity)
                    'elementBillAddress.AppendChild(elementBillCity)

                    'Dim elementBillState As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "State", String.Empty)
                    'Dim xtBillState As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcBillState")))
                    'elementBillState.AppendChild(xtBillState)
                    'elementBillAddress.AppendChild(elementBillState)

                    'Dim elementBillCountry As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Country", String.Empty)
                    'Dim xtBillCountry As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcBillCountry")))
                    'elementBillCountry.AppendChild(xtBillCountry)
                    'elementBillAddress.AppendChild(elementBillCountry)

                    'Dim elementBillPinCode As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ZipCode", String.Empty)
                    'Dim xtBillPinCode As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcBillPostalCode")))
                    'elementBillPinCode.AppendChild(xtBillPinCode)
                    'elementBillAddress.AppendChild(elementBillPinCode)

                    'elementOrder.AppendChild(elementBillAddress)

                    ''Shipping Address
                    'Dim elementShipAddress As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShippingAddress", String.Empty)

                    'Dim elementShipStreet As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Street", String.Empty)
                    'Dim xtShipStreet As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShipStreet")))
                    'elementShipStreet.AppendChild(xtShipStreet)
                    'elementShipAddress.AppendChild(elementShipStreet)

                    'Dim elementShipCity As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "City", String.Empty)
                    'Dim xtShipCity As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShipCity")))
                    'elementShipCity.AppendChild(xtShipCity)
                    'elementShipAddress.AppendChild(elementShipCity)

                    'Dim elementShipState As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "State", String.Empty)
                    'Dim xtShipState As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShipState")))
                    'elementShipState.AppendChild(xtShipState)
                    'elementShipAddress.AppendChild(elementShipState)

                    'Dim elementShipCountry As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Country", String.Empty)
                    'Dim xtShipCountry As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShipCountry")))
                    'elementShipCountry.AppendChild(xtShipCountry)
                    'elementShipAddress.AppendChild(elementShipCountry)

                    'Dim elementShipPinCode As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ZipCode", String.Empty)
                    'Dim xtShipPinCode As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShipPostalCode")))
                    'elementShipPinCode.AppendChild(xtShipPinCode)
                    'elementShipAddress.AppendChild(elementShipPinCode)

                    'elementOrder.AppendChild(elementShipAddress)


                    'Dim elementComments As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Comments", String.Empty)
                    'Dim xtComments As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcComments")))
                    'elementComments.AppendChild(xtComments)
                    'elementOrder.AppendChild(elementComments)


                    'Order Items
                    'Dim elementItems As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Items", String.Empty)
                    For Each drItem As DataRow In ds.Tables(1).Rows
                        Dim elementItem As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Item", String.Empty)

                        Dim elementItemRowType As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "RowType", String.Empty)
                        Dim xtItemRowType As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("RowType")))
                        elementItemRowType.AppendChild(xtItemRowType)

                        Dim elementLine As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Line", String.Empty)
                        Dim xtLine As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("Line")))
                        elementLine.AppendChild(xtLine)

                        Dim elementVendorPart As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "VendorPart", String.Empty)
                        Dim xtVendorPart As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("VendorPart")))
                        elementVendorPart.AppendChild(xtVendorPart)

                        Dim elementBuyerPart As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BuyerPart", String.Empty)
                        Dim xtBuyerPart As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("BuyerPart")))
                        elementBuyerPart.AppendChild(xtBuyerPart)

                        Dim elementUPC As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "UPC", String.Empty)
                        Dim xtUPC As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("UPC")))
                        elementUPC.AppendChild(xtUPC)

                        Dim elementDescription As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Description", String.Empty)
                        Dim xtDescription As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("Description")))
                        elementDescription.AppendChild(xtDescription)

                        Dim elementQuantityShipped As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "QuantityShipped", String.Empty)
                        Dim xtQuantityShipped As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("QuantityShipped")))
                        elementQuantityShipped.AppendChild(xtQuantityShipped)

                        Dim elementUOM As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "UOM", String.Empty)
                        Dim xtUOM As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("UOM")))
                        elementUOM.AppendChild(xtUOM)

                        Dim elementUnitPrice As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "UnitPrice", String.Empty)
                        Dim xtUnitPrice As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("UnitPrice")))
                        elementUnitPrice.AppendChild(xtUnitPrice)

                        Dim elementQuantityOrdered As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "QuantityOrdered", String.Empty)
                        Dim xtQuantityOrdered As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("QuantityOrdered")))
                        elementQuantityOrdered.AppendChild(xtQuantityOrdered)

                        'Dim elementPackSize As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "PackSize", String.Empty)
                        'Dim xtPackSize As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("PackSize")))
                        'elementPackSize.AppendChild(xtPackSize)

                        'Dim elementofInnerPacks As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ofInnerPacks", String.Empty)
                        'Dim xtofInnerPacks As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("ofInnerPacks")))
                        'elementofInnerPacks.AppendChild(xtofInnerPacks)

                        'Dim elementItemAllowancePercent As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ItemAllowancePercent", String.Empty)
                        'Dim xtItemAllowancePercent As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("ItemAllowancePercent")))
                        'elementItemAllowancePercent.AppendChild(xtItemAllowancePercent)

                        'Dim elementItemAllowanceAmount As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ItemAllowanceAmount", String.Empty)
                        'Dim xtItemAllowanceAmount As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("ItemAllowanceAmount")))
                        'elementItemAllowanceAmount.AppendChild(xtItemAllowanceAmount)

                        elementItem.AppendChild(elementItemRowType)
                        elementItem.AppendChild(elementLine)
                        elementItem.AppendChild(elementVendorPart)
                        elementItem.AppendChild(elementBuyerPart)
                        elementItem.AppendChild(elementUPC)
                        elementItem.AppendChild(elementDescription)
                        elementItem.AppendChild(elementQuantityShipped)
                        elementItem.AppendChild(elementUOM)
                        elementItem.AppendChild(elementUnitPrice)
                        elementItem.AppendChild(elementQuantityOrdered)
                        'elementItem.AppendChild(elementPackSize)
                        'elementItem.AppendChild(elementofInnerPacks)
                        'elementItem.AppendChild(elementItemAllowancePercent)
                        'elementItem.AppendChild(elementItemAllowanceAmount)

                        'EDI Custom Fields for Items     
                        Dim dsEdiOppItem As DataSet = objCommon.GetEDICustomFields(21, CCommon.ToLong(drItem("BizOppItemID")))

                        If (dsEdiOppItem IsNot Nothing And dsEdiOppItem.Tables.Count > 0) Then
                            Dim dtEDI As DataTable = dsEdiOppItem.Tables(0)
                            If (dtEDI.Rows.Count > 0) Then
                                For Each dr As DataRow In dtEDI.Rows
                                    Dim elementEDIItemField As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, dr("Fld_label").ToString, String.Empty)
                                    Dim xtEDIItemField As XmlText = xmlDoc.CreateTextNode(dr("Fld_Value").ToString)
                                    elementEDIItemField.AppendChild(xtEDIItemField)

                                    elementItem.AppendChild(elementEDIItemField)
                                Next
                            End If
                        End If

                        elementHeader.AppendChild(elementItem)
                    Next

                    xmlDoc.Save(filePath)

                Else
                    Dim objTCQueueLog As New TrueCommerceLog
                    objTCQueueLog.DomainID = CCommon.ToLong(DomainID)
                    objTCQueueLog.FileName = CCommon.ToString(numOppID)
                    objTCQueueLog.PurchaseOrder = "Invoice 810"
                    objTCQueueLog.Message = "Upload 810(Invoice) Fails: Sales Order not found or dosen't have item(s)"
                    objTCQueueLog.Insert()
                    Return False
                End If
            Catch ex As Exception
                Dim objTCQueueLog As New TrueCommerceLog
                objTCQueueLog.DomainID = CCommon.ToLong(DomainID)
                objTCQueueLog.FileName = CCommon.ToString(numOppID)
                objTCQueueLog.PurchaseOrder = "Invoice 810"
                objTCQueueLog.Message = "Upload 810(Invoice) Fails: " + ex.Message
                objTCQueueLog.Insert()
            End Try
            Return True
        End Function

        Private Sub Generate940(ByVal EDIQueueID As Long, ByVal numDomainID As Long, ByVal numOppID As Long, ByVal filePath As String)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", numDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", numOppID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_GetDataForEDI940", sqlParams.ToArray())

                If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(1).Rows.Count > 0 Then

                    Dim xmlDoc As New XmlDocument
                    Dim xmlDeclaration As System.Xml.XmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", Nothing)
                    Dim root As System.Xml.XmlElement = xmlDoc.DocumentElement
                    xmlDoc.InsertBefore(xmlDeclaration, root)

                    Dim elementOrders As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Orders", String.Empty)
                    xmlDoc.AppendChild(elementOrders)

                    Dim elementOrder As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Order", String.Empty)
                    elementOrders.AppendChild(elementOrder)

                    Dim elementOrderID As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ID", String.Empty)
                    Dim xtOrderID As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("numOppID")))
                    elementOrderID.AppendChild(xtOrderID)
                    elementOrder.AppendChild(elementOrderID)

                    Dim elementOrderName As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Name", String.Empty)
                    Dim xtOrderName As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcPOppName")))
                    elementOrderName.AppendChild(xtOrderName)
                    elementOrder.AppendChild(elementOrderName)

                    Dim elementCustomerPO As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "CustomerPO", String.Empty)
                    Dim xtCustomerPO As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcCustomerPO#")))
                    elementCustomerPO.AppendChild(xtCustomerPO)
                    elementOrder.AppendChild(elementCustomerPO)

                    Dim elementCustomer As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Customer", String.Empty)
                    Dim xtCustomer As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcCompanyName")))
                    elementCustomer.AppendChild(xtCustomer)
                    elementOrder.AppendChild(elementCustomer)

                    Dim elementEmployer As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Employer", String.Empty)
                    Dim xtEmployer As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcDomainName")))
                    elementEmployer.AppendChild(xtEmployer)
                    elementOrder.AppendChild(elementEmployer)

                    Dim elementEmployerID As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "EmployerID", String.Empty)
                    Dim xtEmployerID As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("numDomainID")))
                    elementEmployerID.AppendChild(xtEmployerID)
                    elementOrder.AppendChild(elementEmployerID)

                    Dim elementCustomerID As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "CustomerID", String.Empty)
                    Dim xtCustomerID As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("numDivisionID")))
                    elementCustomerID.AppendChild(xtCustomerID)
                    elementOrder.AppendChild(elementCustomerID)

                    Dim elementEmployerCustomerID As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "EmployerCustomerID", String.Empty)
                    Dim xtEmployerCustomerID As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcDomainDivisionID")))
                    elementEmployerCustomerID.AppendChild(xtEmployerCustomerID)
                    elementOrder.AppendChild(elementEmployerCustomerID)

                    Dim elementShippingLabel As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "SpecialShippingRequired", String.Empty)
                    Dim xtShippingLabel As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("bitShippingLabelRequired")))
                    elementShippingLabel.AppendChild(xtShippingLabel)
                    elementOrder.AppendChild(elementShippingLabel)

                    Dim elementShippingInstruction As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShippingInstructions", String.Empty)
                    Dim xtShippingInstruction As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShippingInstruction")))
                    elementShippingInstruction.AppendChild(xtShippingInstruction)
                    elementOrder.AppendChild(elementShippingInstruction)

                    Dim elementContactFirstName As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ContactFirstName", String.Empty)
                    Dim xtContactFirstName As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcFirstName")))
                    elementContactFirstName.AppendChild(xtContactFirstName)
                    elementOrder.AppendChild(elementContactFirstName)

                    Dim elementContactLastName As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ContactLastName", String.Empty)
                    Dim xtContactLastName As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcLastName")))
                    elementContactLastName.AppendChild(xtContactLastName)
                    elementOrder.AppendChild(elementContactLastName)

                    Dim elementContactPhone As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ContactPhone", String.Empty)
                    Dim xtContactPhone As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcPhone")))
                    elementContactPhone.AppendChild(xtContactPhone)
                    elementOrder.AppendChild(elementContactPhone)

                    'Billing Address
                    Dim elementBillAddress As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BillingAddress", String.Empty)

                    Dim elementBillStreet As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Street", String.Empty)
                    Dim xtBillStreet As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcBillStreet")))
                    elementBillStreet.AppendChild(xtBillStreet)
                    elementBillAddress.AppendChild(elementBillStreet)

                    Dim elementBillCity As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "City", String.Empty)
                    Dim xtBillCity As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcBillCity")))
                    elementBillCity.AppendChild(xtBillCity)
                    elementBillAddress.AppendChild(elementBillCity)

                    Dim elementBillState As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "State", String.Empty)
                    Dim xtBillState As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcBillState")))
                    elementBillState.AppendChild(xtBillState)
                    elementBillAddress.AppendChild(elementBillState)

                    Dim elementBillCountry As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Country", String.Empty)
                    Dim xtBillCountry As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcBillCountry")))
                    elementBillCountry.AppendChild(xtBillCountry)
                    elementBillAddress.AppendChild(elementBillCountry)

                    Dim elementBillPinCode As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ZipCode", String.Empty)
                    Dim xtBillPinCode As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcBillPostalCode")))
                    elementBillPinCode.AppendChild(xtBillPinCode)
                    elementBillAddress.AppendChild(elementBillPinCode)

                    elementOrder.AppendChild(elementBillAddress)

                    'Shipping Address
                    Dim elementShipAddress As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ShippingAddress", String.Empty)

                    Dim elementShipStreet As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Street", String.Empty)
                    Dim xtShipStreet As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShipStreet")))
                    elementShipStreet.AppendChild(xtShipStreet)
                    elementShipAddress.AppendChild(elementShipStreet)

                    Dim elementShipCity As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "City", String.Empty)
                    Dim xtShipCity As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShipCity")))
                    elementShipCity.AppendChild(xtShipCity)
                    elementShipAddress.AppendChild(elementShipCity)

                    Dim elementShipState As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "State", String.Empty)
                    Dim xtShipState As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShipState")))
                    elementShipState.AppendChild(xtShipState)
                    elementShipAddress.AppendChild(elementShipState)

                    Dim elementShipCountry As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Country", String.Empty)
                    Dim xtShipCountry As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShipCountry")))
                    elementShipCountry.AppendChild(xtShipCountry)
                    elementShipAddress.AppendChild(elementShipCountry)

                    Dim elementShipPinCode As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ZipCode", String.Empty)
                    Dim xtShipPinCode As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcShipPostalCode")))
                    elementShipPinCode.AppendChild(xtShipPinCode)
                    elementShipAddress.AppendChild(elementShipPinCode)

                    elementOrder.AppendChild(elementShipAddress)

                    Dim elementComments As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Comments", String.Empty)
                    Dim xtComments As XmlText = xmlDoc.CreateTextNode(Convert.ToString(ds.Tables(0).Rows(0)("vcComments")))
                    elementComments.AppendChild(xtComments)
                    elementOrder.AppendChild(elementComments)

                    ' EDI Custom Fields  'Added by Priya
                    Dim objCommon As New CCommon
                    objCommon.DomainID = numDomainID
                    Dim dsEdiFields As DataSet = objCommon.GetEDICustomFields(19, numOppID)
                    If (dsEdiFields IsNot Nothing And dsEdiFields.Tables.Count > 0) Then
                        Dim dtEDI As DataTable = dsEdiFields.Tables(0)
                        If (dtEDI.Rows.Count > 0) Then
                            For Each dr As DataRow In dtEDI.Rows
                                Dim elementEDIField As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, dr("Fld_label").ToString, String.Empty)
                                Dim xtEDIField As XmlText = xmlDoc.CreateTextNode(dr("Fld_Value").ToString)
                                elementEDIField.AppendChild(xtEDIField)
                                elementOrder.AppendChild(elementEDIField)

                            Next
                        End If
                    End If

                    'Order Items
                    Dim elementItems As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Items", String.Empty)
                    For Each drItem As DataRow In ds.Tables(1).Rows
                        Dim elementItem As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Item", String.Empty)

                        'Added By Richa 09052018
                        Dim elementBizOppItemID As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "BizOppItemID", String.Empty)
                        Dim xtBizOppItemID As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("BizOppItemID")))
                        elementBizOppItemID.AppendChild(xtBizOppItemID)

                        Dim elementItemName As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Name", String.Empty)
                        Dim xtItemName As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("vcItemName")))
                        elementItemName.AppendChild(xtItemName)

                        Dim elementSKU As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "SKU", String.Empty)
                        Dim xtSKU As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("vcSKU")))
                        elementSKU.AppendChild(xtSKU)

                        Dim elementType As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ItemType", String.Empty)
                        Dim xtItemType As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("vcItemType")))
                        elementType.AppendChild(xtItemType)

                        Dim elementUnits As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Units", String.Empty)
                        Dim xtUnits As XmlText = xmlDoc.CreateTextNode(Convert.ToDouble(drItem("numUnits")))
                        elementUnits.AppendChild(xtUnits)

                        Dim elementUOM As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "UOM", String.Empty)
                        Dim xtUOM As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("vcUOM")))
                        elementUOM.AppendChild(xtUOM)

                        Dim elementNotes As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Notes", String.Empty)
                        Dim xtNotes As XmlText = xmlDoc.CreateTextNode(Convert.ToString(drItem("vcNotes")))
                        elementNotes.AppendChild(xtNotes)

                        elementItem.AppendChild(elementBizOppItemID) 'Added By Richa 09052018
                        elementItem.AppendChild(elementItemName)
                        elementItem.AppendChild(elementSKU)
                        elementItem.AppendChild(elementType)
                        elementItem.AppendChild(elementUnits)
                        elementItem.AppendChild(elementUOM)
                        elementItem.AppendChild(elementNotes)

                        'EDI Custom Fields for Items         'Added by Priya
                        Dim dsEdiOppItem As DataSet = objCommon.GetEDICustomFields(21, CCommon.ToLong(drItem("BizOppItemID")))

                        If (dsEdiOppItem IsNot Nothing And dsEdiOppItem.Tables.Count > 0) Then
                            Dim dtEDI As DataTable = dsEdiOppItem.Tables(0)
                            If (dtEDI.Rows.Count > 0) Then
                                For Each dr As DataRow In dtEDI.Rows
                                    Dim elementEDIItemField As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, dr("Fld_label").ToString, String.Empty)
                                    Dim xtEDIItemField As XmlText = xmlDoc.CreateTextNode(dr("Fld_Value").ToString)
                                    elementEDIItemField.AppendChild(xtEDIItemField)

                                    elementItem.AppendChild(elementEDIItemField)
                                Next
                            End If
                        End If

                        elementItems.AppendChild(elementItem)
                    Next

                    elementOrder.AppendChild(elementItems)

                    xmlDoc.Save(filePath)
                Else
                    Dim objEDIQueueLog As New EDIQueueLog
                    objEDIQueueLog.EDIQueueID = EDIQueueID
                    objEDIQueueLog.DomainID = numDomainID
                    objEDIQueueLog.OppID = numOppID
                    objEDIQueueLog.EDIType = 940
                    objEDIQueueLog.Log = "Sales Order not found or dosen't have item(s)"
                    objEDIQueueLog.IsSuccess = False
                    objEDIQueueLog.ExceptionMessage = ""
                    objEDIQueueLog.Insert()
                End If
            Catch ex As Exception
                Dim objEDIQueueLog As New EDIQueueLog
                objEDIQueueLog.EDIQueueID = EDIQueueID
                objEDIQueueLog.DomainID = numDomainID
                objEDIQueueLog.OppID = numOppID
                objEDIQueueLog.EDIType = 940
                objEDIQueueLog.Log = "Error while generating 940"
                objEDIQueueLog.IsSuccess = False
                objEDIQueueLog.ExceptionMessage = ex.Message
                objEDIQueueLog.Insert()
            End Try
        End Sub

        Private Sub ReleaseAllocation(ByVal numDomainID As Long, ByVal OppID As Long, ByVal PackList As XmlNodeList)
            Try
                Dim strErrorMessage As String
                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.OpportunityId = OppID

                Dim dtTable As DataTable
                dtTable = objOpportunity.ValidateOppSerializLot()

                If dtTable.Rows.Count > 0 Then
                    Dim drErrors As DataRow()

                    drErrors = dtTable.Select("Error=1")

                    If drErrors.Length > 0 Then
                        strErrorMessage = "Lot/Serial #s are not provided for following item(s) :"

                        For Each dr As DataRow In drErrors
                            If CShort(dr("Error")) = 1 Then
                                strErrorMessage += "<br/>- " + CStr(dr("vcItemName"))
                                Exit Sub
                            End If
                        Next

                        Throw New Exception(strErrorMessage)
                    End If
                End If

                If objOpportunity.CheckWOStatus > 0 Then
                    strErrorMessage = "You can't ship at this time because Work Order for this Sales Order is pending."
                    Throw New Exception(strErrorMessage)
                End If

                Dim dtItemList As DataTable
                dtItemList = objOpportunity.CheckCanbeShipped()

                If dtItemList.Rows.Count > 0 Then
                    strErrorMessage = "You can't ship at this time because you don't have enough quantity on hand to support your shipment. Your options are to modify your order, or replenish inventory (to check inventory click on the edit link within the line item, then the value in the 'Products/Services' column):"

                    For Each dr As DataRow In dtItemList.Rows
                        strErrorMessage += "<br/>- " + CStr(dr("vcItemName"))
                    Next

                    Throw New Exception(strErrorMessage)
                End If

                'If there are Fulfillment BizDoc(s) available which are not yet marked as fulfilled than auto fulfil those bizdocs.
                Dim objCOpportunity As New COpportunities
                objCOpportunity.OpportID = OppID
                Dim dtFulfillmentBizDoc As DataTable = objCOpportunity.GetPendingFulfillmentBizDocs()

                If Not dtFulfillmentBizDoc Is Nothing AndAlso dtFulfillmentBizDoc.Rows.Count > 0 Then
                    For Each drBizDoc As DataRow In dtFulfillmentBizDoc.Rows
                        objCOpportunity.AutoFulfillOrder(numDomainID, 0, OppID, Convert.ToInt64(drBizDoc("numOppBizDocsId")))
                    Next
                End If

                objOpportunity.OpportunityId = OppID
                Dim dt As DataSet = objOpportunity.CheckOrderedAndInvoicedOrBilledQty()

                Dim bitOrderedAndFulfilledQtyNotSame As Boolean = Convert.ToBoolean(dt.Tables(0).Select("ID=3")(0)("bitTrue"))

                'CREATE FULFILLMENT BIZ DOC OF REMAINING QTY AND MARK IF AS SHIPPED (RELEASE FROM ALLOCATION)
                If bitOrderedAndFulfilledQtyNotSame Then
                    Try
                        'objCOpportunity.CreateFulfillmentBizDoc(numDomainID, 0, OppID, True)
                        If Not PackList Is Nothing AndAlso PackList.Count > 0 Then
                            For Each packnode As XmlNode In PackList
                                Dim ItemsList As XmlNodeList = packnode.SelectNodes("descendant::Item")
                                If Not ItemsList Is Nothing AndAlso ItemsList.Count > 0 Then
                                    CreateFulfillmentBizDocFor850(ItemsList, OppID, numDomainID)
                                End If
                            Next
                        End If


                    Catch ex As Exception
                        If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                            Throw New Exception("Not able to fulfillment bizdoc because some work orders are still not completed.")
                        ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                            Throw New Exception("Not able to fulfillment bizdoc because qty is not same as required qty.<br />")
                        ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                            Throw New Exception("Not able to fulfillment bizdoc because required number of serials are not provided.<br />")
                        ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                            Throw New Exception("Not able to fulfillment bizdoc because invalid serials are provided.<br />")
                        ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                            Throw New Exception("Not able to fulfillment bizdoc because required number of Lots are not provided.<br />")
                        ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                            Throw New Exception("Not able to fulfillment bizdoc because invalid Lots are provided.<br />")
                        ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                            Throw New Exception("Not able to fulfillment bizdoc because Lot number do not have enough qty to fulfill.<br />")
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                            Throw New Exception("Not able to fulfillment bizdoc because warehouse allocation is invlid.<br />")
                        Else
                            Throw New Exception("Not able to add fulfillment order due to unknown error." & ex.Message & "<br />")
                        End If
                        Exit Sub
                    End Try
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub Generate856(ByVal EDIQueueID As Long, ByVal numDomainID As Long, ByVal numOppID As Long)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", numDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", numOppID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_GetDataForEDI856", sqlParams.ToArray())

                If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(0).Rows.Count > 0 Then

                    For Each drShippingReport As DataRow In ds.Tables(0).Rows
                        Dim shippingReportID As Long = Convert.ToInt64(drShippingReport("numShippingReportId"))
                        Dim filePath As String = System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & numDomainID & "\Get\" & numOppID & "-" & shippingReportID & ".xml"
                        Dim xmlDoc As New XmlDocument
                        Dim xmlDeclaration As System.Xml.XmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", Nothing)
                        Dim root As System.Xml.XmlElement = xmlDoc.DocumentElement
                        xmlDoc.InsertBefore(xmlDeclaration, root)
                        Dim elementPickAndPack As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "ASNPickAndPack", String.Empty)
                        xmlDoc.AppendChild(elementPickAndPack)

                        Dim elementShipment As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Shipment", String.Empty)
                        elementPickAndPack.AppendChild(elementShipment)

                        Dim xmString As String = "<RowType></RowType>" & vbCrLf &
                                                            "<TransactionType></TransactionType>" & vbCrLf &
                                                            "<AccountingID></AccountingID>" & vbCrLf &
                                                            "<ShipmentID></ShipmentID>" & vbCrLf &
                                                            "<SCAC></SCAC>" & vbCrLf &
                                                            "<CarrierPro></CarrierPro>" & vbCrLf &
                                                            "<BillofLading></BillofLading>" & vbCrLf &
                                                            "<ScheduledDelivery></ScheduledDelivery>" & vbCrLf &
                                                            "<ShipDate>" & Convert.ToString(drShippingReport("vcShipDate")) & "</ShipDate>" & vbCrLf &
                                                            "<ShipToName>" & Convert.ToString(drShippingReport("vcToName")) & "</ShipToName>" & vbCrLf &
                                                            "<ShipToAddress-LineOne>" & Convert.ToString(drShippingReport("vcToAddressLine1")) & "</ShipToAddress-LineOne>" & vbCrLf &
                                                            "<ShipToAddress-LineTwo>" & Convert.ToString(drShippingReport("vcToAddressLine2")) & "</ShipToAddress-LineTwo>" & vbCrLf &
                                                            "<ShipToCity>" & Convert.ToString(drShippingReport("vcToCity")) & "</ShipToCity>" & vbCrLf &
                                                            "<ShipToState>" & Convert.ToString(drShippingReport("vcToState")) & "</ShipToState>" & vbCrLf &
                                                            "<ShipToZip>" & Convert.ToString(drShippingReport("vcToZip")) & "</ShipToZip>" & vbCrLf &
                                                            "<ShipToCountry>" & Convert.ToString(drShippingReport("vcToCountry")) & "</ShipToCountry>" & vbCrLf &
                                                            "<ShiptoAddressCode></ShiptoAddressCode>" & vbCrLf &
                                                            "<ShipVia>" & Convert.ToString(drShippingReport("vcShipVia")) & "</ShipVia>" & vbCrLf &
                                                            "<ShipToType></ShipToType>" & vbCrLf &
                                                            "<PackagingType></PackagingType>" & vbCrLf &
                                                            "<GrossWeight>" & Convert.ToString(Convert.ToDouble(drShippingReport("fltTotalRegularWeight")) + Convert.ToDouble(drShippingReport("fltTotalDimensionalWeight"))) & "</GrossWeight>" & vbCrLf &
                                                            "<GrossWeightUOM>lbs</GrossWeightUOM>" & vbCrLf &
                                                            "<ofCartonsShipped></ofCartonsShipped>" & vbCrLf &
                                                            "<CarrierTrailer></CarrierTrailer>" & vbCrLf &
                                                            "<TrailerInitital></TrailerInitital>" & vbCrLf &
                                                            "<ShipFromName>" & Convert.ToString(drShippingReport("vcFromName")) & "</ShipFromName>" & vbCrLf &
                                                            "<ShipFromAddress-LineOne>" & Convert.ToString(drShippingReport("vcFromAddressLine1")) & "</ShipFromAddress-LineOne>" & vbCrLf &
                                                            "<ShipFromAddress-LineTwo>" & Convert.ToString(drShippingReport("vcFromAddressLine2")) & "</ShipFromAddress-LineTwo>" & vbCrLf &
                                                            "<ShipFromCity>" & Convert.ToString(drShippingReport("vcFromCity")) & "</ShipFromCity>" & vbCrLf &
                                                            "<ShipFromState>" & Convert.ToString(drShippingReport("vcFromState")) & "</ShipFromState>" & vbCrLf &
                                                            "<ShipFromZip>" & Convert.ToString(drShippingReport("vcFromZip")) & "</ShipFromZip>" & vbCrLf &
                                                            "<ShipFromCountry>" & Convert.ToString(drShippingReport("vcFromCountry")) & "</ShipFromCountry>" & vbCrLf &
                                                            "<ShipFromAddressCode></ShipFromAddressCode>" & vbCrLf &
                                                            "<Vendor></Vendor>" & vbCrLf &
                                                            "<DCCode></DCCode>" & vbCrLf &
                                                            "<TransportationMethod></TransportationMethod>" & vbCrLf &
                                                            "<ProductGroup></ProductGroup>" & vbCrLf &
                                                            "<Status></Status>" & vbCrLf &
                                                            "<TimeShipped></TimeShipped>" & vbCrLf &
                                                            "<Order>" & vbCrLf &
                                                                "<RowType></RowType>" & vbCrLf &
                                                                "<PO>" & Convert.ToString(drShippingReport("vcCustomerPO#")) & "</PO>" & vbCrLf &
                                                                "<PODate></PODate>" & vbCrLf &
                                                                "<Invoice></Invoice>" & vbCrLf &
                                                                "<OrderWeight></OrderWeight>" & vbCrLf &
                                                                "<StoreName></StoreName>" & vbCrLf &
                                                                "<StoreNumber></StoreNumber>" & vbCrLf &
                                                                "<MarkForCode></MarkForCode>" & vbCrLf &
                                                                "<Department></Department>" & vbCrLf &
                                                                "<OrderLadingQuantity></OrderLadingQuantity>" & vbCrLf &
                                                                "<PackagingType></PackagingType>"

                        Dim arrShippingBox As DataRow() = ds.Tables(1).Select("numShippingReportId=" & shippingReportID)

                        If Not arrShippingBox Is Nothing AndAlso arrShippingBox.Length > 0 Then
                            For Each drBox As DataRow In arrShippingBox
                                xmString = xmString & "<Pack>" & vbCrLf &
                                                            "<RowType></RowType>" & vbCrLf &
                                                            "<UCC-128></UCC-128>" & vbCrLf &
                                                            "<PackSize></PackSize>" & vbCrLf &
                                                            "<InnerPackPerOuterPack></InnerPackPerOuterPack>" & vbCrLf &
                                                            "<PackHeight>" & Convert.ToString(drBox("fltHeight")) & "</PackHeight>" & vbCrLf &
                                                            "<PackLength>" & Convert.ToString(drBox("fltLength")) & "</PackLength>" & vbCrLf &
                                                            "<PackWidth>" & Convert.ToString(drBox("fltWidth")) & "</PackWidth>" & vbCrLf &
                                                            "<PackWeight>" & Convert.ToString(drBox("fltTotalWeight")) & "</PackWeight>" & vbCrLf &
                                                            "<QtyofUPCswithinPack>" & Convert.ToString(drBox("Qty")) & "</QtyofUPCswithinPack>" & vbCrLf &
                                                            "<UOMofUPCs></UOMofUPCs>" & vbCrLf &
                                                            "<StoreName></StoreName>" & vbCrLf &
                                                            "<StoreNumber></StoreNumber>"


                                Dim shippingBoxID As Long = Convert.ToInt64(drBox("numBoxID"))
                                Dim arrBoxItems As DataRow() = ds.Tables(2).Select("numBoxID=" & shippingBoxID)
                                If Not arrBoxItems Is Nothing AndAlso arrBoxItems.Length > 0 Then
                                    For Each drBoxItem As DataRow In arrBoxItems
                                        xmString = xmString & "<Item>" & vbCrLf &
                                                                        "<Rowtype></Rowtype>" & vbCrLf &
                                                                        "<Line></Line>" & vbCrLf &
                                                                        "<VendorPart></VendorPart>" & vbCrLf &
                                                                        "<BuyerPart></BuyerPart>" & vbCrLf &
                                                                        "<SKU>" & Convert.ToString(drBoxItem("vcSKU")) & "</SKU>" & vbCrLf &
                                                                        "<UPC>" & Convert.ToString(drBoxItem("vcUPC")) & "</UPC>" & vbCrLf &
                                                                        "<ItemType>" & Convert.ToString(drBoxItem("vcItemType")) & "</ItemType>" & vbCrLf &
                                                                        "<ItemDescription></ItemDescription>" & vbCrLf &
                                                                        "<QuantityShipped>" & Convert.ToString(drBoxItem("numShippedUnits")) & "</QuantityShipped>" & vbCrLf &
                                                                        "<UOM>" & Convert.ToString(drBoxItem("vcUOM")) & "</UOM>" & vbCrLf &
                                                                        "<OrderedQuantity>" & Convert.ToString(drBoxItem("numOrderedUnits")) & "</OrderedQuantity>" & vbCrLf &
                                                                        "<UnitPrice></UnitPrice>" & vbCrLf &
                                                                        "<PackSize></PackSize>" & vbCrLf &
                                                                        "<PackUOM></PackUOM>" & vbCrLf &
                                                                        "<InnerPacksperOuterPack></InnerPacksperOuterPack>" & vbCrLf &
                                                                    "</Item>"
                                    Next
                                End If

                                xmString = xmString & "</Pack>"
                            Next
                        Else
                            xmString = "<Pack></Pack>"
                        End If


                        xmString = xmString & "</Order>"


                        Dim xfrag As XmlDocumentFragment = xmlDoc.CreateDocumentFragment()
                        xfrag.InnerXml = xmString
                        xmlDoc.DocumentElement.FirstChild.AppendChild(xfrag)

                        xmlDoc.Save(filePath)
                    Next
                Else
                    Dim objEDIQueueLog As New EDIQueueLog
                    objEDIQueueLog.EDIQueueID = EDIQueueID
                    objEDIQueueLog.DomainID = numDomainID
                    objEDIQueueLog.OppID = numOppID
                    objEDIQueueLog.EDIType = 8566
                    objEDIQueueLog.Log = "Shipping BizDoc(s) not found to generate 856 document"
                    objEDIQueueLog.IsSuccess = False
                    objEDIQueueLog.ExceptionMessage = ""
                    objEDIQueueLog.Insert()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub Generate855Acknowledge(ByVal isSuccess As Boolean, ByVal marketPlaceName As String, ByVal marketplaceSettingId As String, ByVal errorMessage As String, ByVal fileName As String, ByVal customerPO As String, ByVal lngDomainID As Long, ByVal lngOppID As Long)
            Try
                Dim filePath As String = System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "EDI\" & lngDomainID & "\Get\" & fileName
                Dim xmlDoc As New XmlDocument
                Dim xmlDeclaration As System.Xml.XmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", Nothing)
                Dim root As System.Xml.XmlElement = xmlDoc.DocumentElement
                xmlDoc.InsertBefore(xmlDeclaration, root)
                Dim elementPurchaseOrder As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "PurchaseOrder", String.Empty)
                xmlDoc.AppendChild(elementPurchaseOrder)

                Dim elementHeader As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Header", String.Empty)
                elementPurchaseOrder.AppendChild(elementHeader)

                Dim bizOrderName As String = ""
                If lngOppID > 0 Then
                    Dim objOpportunity As New MOpportunity
                    objOpportunity.DomainID = lngDomainID
                    objOpportunity.OpportunityId = lngOppID
                    bizOrderName = CCommon.ToString(objOpportunity.GetFieldValue("vcPOppName"))
                End If

                Dim xmString As String = "<TransactionType>855</TransactionType><MarketplaceName>" & marketPlaceName & "</MarketplaceName><MarketplaceSettingId>" & marketplaceSettingId & "</MarketplaceSettingId><PurchaseOrderNumber>" & customerPO & "</PurchaseOrderNumber><Status>" & If(isSuccess, "Accepted", "Rejected") & "</Status><BizOrderName>" & bizOrderName & "</BizOrderName><Error>" & errorMessage & "</Error>"

                Dim xfrag As XmlDocumentFragment = xmlDoc.CreateDocumentFragment()
                xfrag.InnerXml = xmString
                elementHeader.AppendChild(xfrag)

                xmlDoc.Save(filePath)
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Sub

        Private Sub Generate856Acknowledge(ByVal numDomainID As Long, ByVal OppID As Long)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", numDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_GetDataForEDI856Acknowledgement", sqlParams.ToArray())

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim filePath As String = System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "3PL\" & numDomainID & "\Get\" & OppID & "-856-997" & ".xml"
                    Dim xmlDoc As New XmlDocument
                    Dim xmlDeclaration As System.Xml.XmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", Nothing)
                    Dim root As System.Xml.XmlElement = xmlDoc.DocumentElement
                    xmlDoc.InsertBefore(xmlDeclaration, root)
                    Dim elementAcknowledgement As System.Xml.XmlElement = xmlDoc.CreateElement(String.Empty, "Acknowledgement", String.Empty)
                    xmlDoc.AppendChild(elementAcknowledgement)

                    Dim xmString As String = "<AckOfDocumentType>856</AckOfDocumentType><ID>" & OppID & "</ID><Name>" & Convert.ToString(ds.Tables(0).Rows(0)("vcPOppName")) & "</Name>"

                    Dim xfrag As XmlDocumentFragment = xmlDoc.CreateDocumentFragment()
                    xfrag.InnerXml = xmString
                    elementAcknowledgement.AppendChild(xfrag)

                    xmlDoc.Save(filePath)
                End If
            Catch ex As Exception
                Dim objEDIQueueLog As New EDIQueueLog
                objEDIQueueLog.EDIQueueID = 0
                objEDIQueueLog.DomainID = numDomainID
                objEDIQueueLog.OppID = OppID
                objEDIQueueLog.EDIType = 856
                objEDIQueueLog.Log = "Error while generating 856 Acknowledge"
                objEDIQueueLog.IsSuccess = False
                objEDIQueueLog.ExceptionMessage = ex.Message
                objEDIQueueLog.Insert()
            End Try
        End Sub

        Private Sub MoveFileToErrorFolder(sourceFile, destinationFileName)
            Try
                System.IO.File.Move(sourceFile, System.Configuration.ConfigurationManager.AppSettings("SFTPServerDirectory") & "DocNotAbleToProcess\" & destinationFileName)
            Catch ex As Exception
                Try
                    System.IO.File.Delete(sourceFile)
                Catch x As Exception
                    'DO NOT THROW
                End Try
            End Try
        End Sub

        'Added By Richa : To handle Partial Orders in EDI Transactions
        Public Function CreateFulfillmentBizDocFor850(ByVal ItemsList As XmlNodeList, ByVal OppID As Long, ByVal numDomainID As Long) As Boolean
            Try
                Dim ItemXML As String
                ItemXML = GetBizDocItems(ItemsList, OppID, numDomainID)
                If (ItemXML <> "") Then
                    Dim OppBizDocID, lngDivId, JournalId As Long
                    Dim lngCntID As Long = 0

                    Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        Dim dtDetails, dtBillingTerms As DataTable

                        Dim objPageLayout As New BACRM.BusinessLogic.Contacts.CPageLayout
                        objPageLayout.OpportunityId = OppID
                        objPageLayout.DomainID = numDomainID
                        dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                        lngDivId = CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID"))
                        lngCntID = CCommon.ToLong(dtDetails.Rows(0).Item("numContactID"))

                        Dim objOppBizDocs As New OppBizDocs
                        objOppBizDocs.UserCntID = UserCntID
                        objOppBizDocs.DomainID = numDomainID

                        If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", numDomainID, lngDivId) = 0 Then
                            Throw New Exception("Not able to fulfillment bizdoc. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                        End If

                        objOppBizDocs.OppId = OppID
                        objOppBizDocs.OppType = 1
                        objOppBizDocs.FromDate = DateTime.UtcNow
                        objOppBizDocs.ClientTimeZoneOffset = 0
                        objOppBizDocs.BizDocId = 296 'Fulfillment Bizdoc
                        objOppBizDocs.byteMode = 1
                        Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                        objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))

                        objOppBizDocs.numSourceBizDocId = 0 'TOASK

                        Dim objCommon As New CCommon
                        objCommon.DomainID = numDomainID
                        objCommon.Mode = 33
                        objCommon.Str = CCommon.ToString(objOppBizDocs.BizDocId) 'DEFUALT BIZ DOC ID
                        objOppBizDocs.SequenceId = CCommon.ToString(objCommon.GetSingleFieldValue())

                        objCommon = New CCommon
                        objCommon.DomainID = numDomainID
                        objCommon.Mode = 34
                        objCommon.Str = CCommon.ToString(OppID)
                        objOppBizDocs.RefOrderNo = CCommon.ToString(objCommon.GetSingleFieldValue())
                        objOppBizDocs.bitPartialShipment = True

                        objOppBizDocs.strBizDocItems = ItemXML
                        Dim dt As New DataTable
                        dt = objCommon.GetMasterListItems(11, numDomainID)

                        Dim dv As New DataView(dt)
                        dv.RowFilter = "vcData = 'Pending Shipping'"
                        If (dv.Count > 0) Then
                            objOppBizDocs.BizDocStatus = CCommon.ToLong(dv.Item(0)("numListItemID"))
                        End If
                        objOppBizDocs.bitShippingBizDoc = False 'hdnShippingBizDoc.Value 'TOASK
                        OppBizDocID = objOppBizDocs.SaveBizDoc()
                        'lngBizDocId = OppBizDocID

                        If OppBizDocID = 0 Then
                            Throw New Exception("Not able to create fulfillment bizdoc. A BizDoc by the same name is already created. Please select another BizDoc !")
                        End If

                        'If isReleaseAllocation Then
                        'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                        Dim objOppBizDoc As New OppBizDocs
                        objOppBizDoc.DomainID = numDomainID
                        objOppBizDoc.UserCntID = UserCntID
                        objOppBizDoc.OppId = OppID
                        objOppBizDoc.OppBizDocId = OppBizDocID
                        objOppBizDoc.AutoFulfillBizDoc()


                        'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                        '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                        'CREATE SALES CLEARING ENTERIES
                        'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                        '-------------------------------------------------
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                        dtOppBiDocItems = ds.Tables(0)

                        Dim objCalculateDealAmount As New CalculateDealAmount
                        objCalculateDealAmount.CalculateDealAmount(OppID, OppBizDocID, 1, numDomainID, dtOppBiDocItems)

                        JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs

                        'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                        Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                        If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                            objJournalEntries.SaveJournalEnteriesSalesClearing(OppID, numDomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                        Else
                            objJournalEntries.SaveJournalEnteriesSalesClearingNew(OppID, numDomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                        End If

                        '---------------------------------------------------------------------------------
                        'End If

                        objTransaction.Complete()
                    End Using

                    ''Added By Sachin Sadhu||Date:1stMay2014
                    ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                    ''          Using Change tracking
                    Dim objWfA As New Workflow.Workflow()
                    objWfA.DomainID = numDomainID
                    objWfA.UserCntID = UserCntID
                    objWfA.RecordID = OppBizDocID
                    objWfA.SaveWFBizDocQueue()
                    'end of code
                    Return True
                Else
                    Return False

                End If
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Added By Richa : To handle Partial Orders in EDI Transactions
        Public Function GetBizDocItems(ByVal ItemsList As XmlNodeList, ByVal OppID As Long, ByVal numDomainID As Long) As String
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = OppID
                objOpportunity.DomainID = numDomainID
                objOpportunity.UserCntID = 0
                objOpportunity.ClientTimeZoneOffset = 0
                Dim dsTemp As DataSet = objOpportunity.GetOrderItemsProductServiceGrid

                Dim dtBizDocItems As New DataTable
                Dim strOppBizDocItems As String

                dtBizDocItems.TableName = "BizDocItems"
                dtBizDocItems.Columns.Add("OppItemID")
                dtBizDocItems.Columns.Add("Quantity")
                dtBizDocItems.Columns.Add("Notes")
                dtBizDocItems.Columns.Add("monPrice")
                Dim dr As DataRow
                For Each Item As XmlNode In ItemsList
                    dr = dtBizDocItems.NewRow
                    dr("OppItemID") = CCommon.ToLong(Item.SelectSingleNode("descendant::BizOppItemID").InnerText())
                    dr("Notes") = ""
                    dr("Quantity") = CCommon.ToDouble(Item.SelectSingleNode("descendant::QuantityShipped").InnerText())
                    Dim qty As Double
                    qty = CCommon.ToDouble(Item.SelectSingleNode("descendant::QuantityShipped").InnerText())
                    If qty = 0 Then
                        Throw New Exception("Not able to create fulfillment bizdoc. A BizDoc by the same name is already created. Please select another BizDoc !")
                        Exit Function
                    End If
                    '        Dim txtmonPriceUOM As TextBox = DirectCast(dgBizGridItem.FindControl("txtmonPriceUOM"), TextBox)
                    '        If Not txtmonPriceUOM Is Nothing Then
                    '            dr("monPrice") = CCommon.ToDecimal(CType(dgBizGridItem.FindControl("txtmonPriceUOM"), TextBox).Text)
                    '        End If

                    If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                        Dim StringBizOppItemID As String = CCommon.ToString(Item.SelectSingleNode("descendant::BizOppItemID").InnerText())
                        Dim result() As DataRow = dsTemp.Tables(0).Select("numoppitemtCode = " + StringBizOppItemID)

                        If result.Count > 0 Then
                            dr("monPrice") = result(0)("monPriceUOM")
                        End If

                    End If
                    dtBizDocItems.Rows.Add(dr)
                Next

                If dtBizDocItems.Rows.Count = 0 Then
                    Return String.Empty
                Else
                    Dim dsNew As New DataSet
                    dsNew.Tables.Add(dtBizDocItems)
                    strOppBizDocItems = dsNew.GetXml
                    dsNew.Tables.Remove(dsNew.Tables(0))
                    Return strOppBizDocItems
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

#End Region

    End Class
End Namespace