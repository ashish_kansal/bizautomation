﻿Option Explicit On
Option Strict On
Imports BACRM.BusinessLogic.Admin
Imports System.Web
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.BusinessLogic.Opportunities
    Public Class SalesFulfillmentWorkflow
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _OppId As Long
        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal Value As Long)
                _OppId = Value
            End Set
        End Property

        Private _ClientTimeZoneOffset As Integer
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _OppBizDocId As Long
        Public Property OppBizDocId() As Long
            Get
                Return _OppBizDocId
            End Get
            Set(ByVal Value As Long)
                _OppBizDocId = Value
            End Set
        End Property

        Private _BizDocId As Long
        Public Property BizDocId() As Long
            Get
                Return _BizDocId
            End Get
            Set(ByVal Value As Long)
                _BizDocId = Value
            End Set
        End Property

        Private _BizDocStatus As Long
        Public Property BizDocStatus() As Long
            Get
                Return _BizDocStatus
            End Get
            Set(ByVal Value As Long)
                _BizDocStatus = Value
            End Set
        End Property

        Private _dtFromDate As DateTime
        Public Property dtFromDate() As DateTime
            Get
                Return _dtFromDate
            End Get
            Set(ByVal Value As DateTime)
                _dtFromDate = Value
            End Set
        End Property

        Private _boolAutolinkUnappliedPayment As Boolean
        Public Property boolAutolinkUnappliedPayment() As Boolean
            Get
                Return _boolAutolinkUnappliedPayment
            End Get
            Set(ByVal Value As Boolean)
                _boolAutolinkUnappliedPayment = Value
            End Set
        End Property

        Sub ManageSalesFulfillmentOrder(ByVal Mode As Long, Optional ByVal boolCreateInvoice As Boolean = False,
                                   Optional ByVal boolCreatePackingSlip As Boolean = False,
                                   Optional ByVal boolReleaseInventory As Boolean = False,
                                   Optional ByVal boolRevertInventory As Boolean = False,
                                   Optional ByVal boolCompleteSF As Boolean = False)
            'Try
            '    Dim objAdmin As New CAdmin
            '    objAdmin.DomainID = DomainID

            '    Dim lngFulfillmentOrderBizDoc As Long = 296

            '    Dim objOppBizDocs As New OppBizDocs
            '    Dim OppBizDocID, lngDivId, JournalId As Long
            '    Dim objCommon As New CCommon

            '    OppBizDocID = 0
            '    objOppBizDocs = New OppBizDocs
            '    objOppBizDocs.OppBizDocId = 0
            '    objOppBizDocs.OppId = _OppId
            '    objOppBizDocs.OppType = 1

            '    objOppBizDocs.UserCntID = UserCntID
            '    objOppBizDocs.DomainID = DomainID
            '    objOppBizDocs.vcPONo = "" 'txtPO.Text
            '    objOppBizDocs.vcComments = "" 'txtComments.Text

            '    objOppBizDocs.FromDate = dtFromDate
            '    objOppBizDocs.ClientTimeZoneOffset = _ClientTimeZoneOffset

            '    objOppBizDocs.BizDocId = _BizDocId
            '    objOppBizDocs.BizDocTemplateID = 0

            '    objCommon = New CCommon
            '    objCommon.DomainID = DomainID
            '    objCommon.Mode = 33
            '    objCommon.Str = CStr(_BizDocId)
            '    objOppBizDocs.SequenceId = CStr(objCommon.GetSingleFieldValue())

            '    objCommon = New CCommon
            '    objCommon.DomainID = DomainID
            '    objCommon.Mode = 34
            '    objCommon.Str = CStr(_OppId)
            '    objOppBizDocs.RefOrderNo = CStr(objCommon.GetSingleFieldValue())

            '    objOppBizDocs.bitPartialShipment = True
            '    objOppBizDocs.bitNotValidateBizDocFulfillment = True
            '    OppBizDocID = objOppBizDocs.SaveBizDoc()

            '    If OppBizDocID > 0 Then
            '        If lngFulfillmentOrderBizDoc = _BizDocId Then

            '            objAdmin.byteMode = 0
            '            Dim dtRules As DataTable = objAdmin.ManageOpportunityAutomationRules()

            '            If dtRules.Rows.Count > 0 Then
            '                Dim foundRows As DataRow()

            '                If Mode = 1 Then
            '                    boolCreateInvoice = boolCreatePackingSlip = boolReleaseInventory = boolRevertInventory = boolCompleteSF = False

            '                    foundRows = dtRules.Select("numBizDocStatus1=" & _BizDocStatus)
            '                    If foundRows.Length > 0 Then
            '                        Select Case CLng(foundRows(0)("numRuleID"))
            '                            Case 1
            '                                boolCreateInvoice = True
            '                            Case 2
            '                                boolCreatePackingSlip = True
            '                            Case 3
            '                                boolReleaseInventory = True
            '                            Case 4
            '                                boolRevertInventory = True
            '                            Case 5
            '                                boolCompleteSF = True
            '                        End Select
            '                    End If
            '                End If

            '                If boolCreateInvoice Then
            '                    'Rule 1
            '                    foundRows = dtRules.Select("numRuleID=1")
            '                    If foundRows.Length > 0 Then
            '                        objOppBizDocs.OppBizDocId = OppBizDocID
            '                        objOppBizDocs.BizDocStatus = CLng(foundRows(0)("numBizDocStatus1"))
            '                        Dim lngOppAuthBizDocsId As Long = objOppBizDocs.SalesFulfillmentWorkflow(bitNotValidateBizDocFulfillment:=True)

            '                        If lngOppAuthBizDocsId > 0 Then
            '                            'Create Journals only for authoritative bizdocs
            '                            '-------------------------------------------------
            '                            Dim lintAuthorizativeBizDocsId As Long
            '                            lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

            '                            Dim dtOppBiDocDtl As DataTable
            '                            objOppBizDocs.OppBizDocId = lngOppAuthBizDocsId
            '                            objOppBizDocs.ClientTimeZoneOffset = _ClientTimeZoneOffset
            '                            dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl

            '                            If lintAuthorizativeBizDocsId = CLng(dtOppBiDocDtl.Rows(0).Item("numBizDocId")) Then

            '                                Dim ds As New DataSet
            '                                Dim dtOppBiDocItems As DataTable

            '                                objOppBizDocs.OppBizDocId = lngOppAuthBizDocsId
            '                                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
            '                                dtOppBiDocItems = ds.Tables(0)

            '                                Dim objCalculateDealAmount As New CalculateDealAmount

            '                                objCalculateDealAmount.CalculateDealAmount(_OppId, lngOppAuthBizDocsId, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), DomainID, dtOppBiDocItems)

            '                                lngDivId = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numDivisionID"))
            '                                ''---------------------------------------------------------------------------------
            '                                JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs
            '                                'When From COA someone deleted journals then we need to create new journal header entry
            '                                If JournalId = 0 Then
            '                                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=CDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate")), Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))
            '                                Else
            '                                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, lngJournalId:=JournalId, Entry_Date:=CDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate")), Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))
            '                                End If

            '                                Dim objJournalEntries As New JournalEntry

            '                                'If CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 2 Then
            '                                '    If CBool(HttpContext.Current.Session("AllowPPVariance")) = True Then
            '                                '        objJournalEntries.SaveJournalEntriesPurchaseVariance(_OppId, DomainID, dtOppBiDocItems, JournalId, lngDivId, lngOppAuthBizDocsId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc")))
            '                                '    Else
            '                                '        objJournalEntries.SaveJournalEntriesPurchase(_OppId, DomainID, dtOppBiDocItems, JournalId, lngDivId, lngOppAuthBizDocsId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
            '                                '    End If
            '                                'Else
            '                                If CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 1 Then
            '                                    objJournalEntries.SaveJournalEntriesSales(_OppId, DomainID, dtOppBiDocItems, JournalId, lngOppAuthBizDocsId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")), 0)

            '                                    If _boolAutolinkUnappliedPayment Then
            '                                        Dim objSalesFulfillment As New SalesFulfillmentWorkflow
            '                                        objSalesFulfillment.DomainID = DomainID
            '                                        objSalesFulfillment.UserCntID = UserCntID
            '                                        objSalesFulfillment.ClientTimeZoneOffset = ClientTimeZoneOffset
            '                                        objSalesFulfillment.OppId = _OppId
            '                                        objSalesFulfillment.OppBizDocId = lngOppAuthBizDocsId

            '                                        objSalesFulfillment.AutoLinkUnappliedPayment()
            '                                    End If
            '                                End If
            '                            End If
            '                        End If

            '                        objCommon.Mode = 27
            '                        objCommon.DomainID = DomainID
            '                        objCommon.UpdateRecordID = OppBizDocID
            '                        objCommon.UpdateValueID = CLng(foundRows(0)("numBizDocStatus1"))
            '                        objCommon.UpdateSingleFieldValue()
            '                    End If
            '                End If

            '                If boolCreatePackingSlip Then
            '                    'Rule 2
            '                    foundRows = dtRules.Select("numRuleID=2")
            '                    If foundRows.Length > 0 Then
            '                        objOppBizDocs.OppBizDocId = OppBizDocID
            '                        objOppBizDocs.BizDocStatus = CLng(foundRows(0)("numBizDocStatus1"))
            '                        objOppBizDocs.SalesFulfillmentWorkflow(bitNotValidateBizDocFulfillment:=True)

            '                        objCommon.Mode = 27
            '                        objCommon.DomainID = DomainID
            '                        objCommon.UpdateRecordID = OppBizDocID
            '                        objCommon.UpdateValueID = CLng(foundRows(0)("numBizDocStatus1"))
            '                        objCommon.UpdateSingleFieldValue()
            '                    End If
            '                End If

            '                If boolReleaseInventory Then
            '                    'Rule 3
            '                    foundRows = dtRules.Select("numRuleID=3")
            '                    If foundRows.Length > 0 Then
            '                        objOppBizDocs.OppBizDocId = OppBizDocID
            '                        objOppBizDocs.BizDocStatus = CLng(foundRows(0)("numBizDocStatus1"))
            '                        objOppBizDocs.SalesFulfillmentWorkflow()

            '                        objCommon.Mode = 27
            '                        objCommon.DomainID = DomainID
            '                        objCommon.UpdateRecordID = OppBizDocID
            '                        objCommon.UpdateValueID = CLng(foundRows(0)("numBizDocStatus1"))
            '                        objCommon.UpdateSingleFieldValue()
            '                    End If
            '                End If

            '                If boolRevertInventory Then
            '                    'Rule 4
            '                    foundRows = dtRules.Select("numRuleID=4")
            '                    If foundRows.Length > 0 Then
            '                        objOppBizDocs.OppBizDocId = OppBizDocID
            '                        objOppBizDocs.BizDocStatus = CLng(foundRows(0)("numBizDocStatus1"))
            '                        objOppBizDocs.SalesFulfillmentWorkflow()

            '                        objCommon.Mode = 27
            '                        objCommon.DomainID = DomainID
            '                        objCommon.UpdateRecordID = OppBizDocID
            '                        objCommon.UpdateValueID = CLng(foundRows(0)("numBizDocStatus1"))
            '                        objCommon.UpdateSingleFieldValue()
            '                    End If
            '                End If

            '                If boolCompleteSF Then
            '                    'Rule 5
            '                    foundRows = dtRules.Select("numRuleID=5")
            '                    If foundRows.Length > 0 Then
            '                        objOppBizDocs.OppBizDocId = OppBizDocID
            '                        objOppBizDocs.BizDocStatus = CLng(foundRows(0)("numBizDocStatus1"))
            '                        objOppBizDocs.SalesFulfillmentWorkflow()

            '                        objCommon.Mode = 27
            '                        objCommon.DomainID = DomainID
            '                        objCommon.UpdateRecordID = OppBizDocID
            '                        objCommon.UpdateValueID = CLng(foundRows(0)("numBizDocStatus1"))
            '                        objCommon.UpdateSingleFieldValue()
            '                    End If
            '                End If
            '            End If
            '        End If

            '        If boolCreateInvoice = False AndAlso boolCreatePackingSlip = False AndAlso boolReleaseInventory = False AndAlso boolRevertInventory = False AndAlso boolCompleteSF = False Then
            '            objCommon.Mode = 27
            '            objCommon.DomainID = DomainID
            '            objCommon.UpdateRecordID = OppBizDocID
            '            objCommon.UpdateValueID = _BizDocStatus
            '            objCommon.UpdateSingleFieldValue()
            '        End If
            '    End If
            'Catch ex As Exception
            '    Throw ex
            'End Try
        End Sub

        Sub AutoLinkUnappliedPayment()
            Try
                Dim objOppBizDocs As New OppBizDocs
                Dim lintAuthorizativeBizDocsId As Long
                objOppBizDocs.DomainID = DomainID
                objOppBizDocs.OppId = _OppId
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                Dim dtOppBiDocDtl As DataTable
                objOppBizDocs.OppBizDocId = _OppBizDocId
                objOppBizDocs.OppId = _OppId
                objOppBizDocs.DomainID = DomainID
                objOppBizDocs.UserCntID = UserCntID
                objOppBizDocs.ClientTimeZoneOffset = _ClientTimeZoneOffset
                dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl

                If lintAuthorizativeBizDocsId = CLng(dtOppBiDocDtl.Rows(0).Item("numBizDocId")) Then
                    Dim monDueAmount As Decimal = CCommon.ToDecimal(dtOppBiDocDtl.Rows(0).Item("monPAmount")) - CCommon.ToDecimal(dtOppBiDocDtl.Rows(0).Item("monAmountPaid"))

                    If monDueAmount > 0 Then
                        Dim lngDivID As Long = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numDivisionID"))

                        Dim dtCredits As New DataTable
                        Dim objReturns As New Returns
                        With objReturns
                            .DivisionId = lngDivID
                            .DomainID = DomainID
                            .ReferenceID = 0
                            .Mode = 1
                            .CurrencyID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numCurrencyID"))
                            dtCredits = .GetCustomerCredits
                        End With

                        If dtCredits.Rows.Count > 0 Then
                            Dim dtDeposit As New DataTable
                            CCommon.AddColumnsToDataTable(dtDeposit, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid")
                            Dim dr As DataRow

                            For Each drMain As DataRow In dtCredits.Rows
                                If monDueAmount > 0 Then
                                    dtDeposit.Rows.Clear()

                                    Dim decCreditAmount As Decimal = CCommon.ToDecimal(drMain("monNonAppliedAmount"))
                                    If decCreditAmount > 0 Then
                                        dr = dtDeposit.NewRow

                                        dr("numDepositeDetailID") = 0
                                        dr("numOppBizDocsID") = _OppBizDocId
                                        dr("numOppID") = _OppId

                                        If decCreditAmount > monDueAmount Then
                                            dr("monAmountPaid") = monDueAmount
                                        Else
                                            dr("monAmountPaid") = decCreditAmount
                                        End If

                                        monDueAmount = monDueAmount - CCommon.ToDecimal(dr("monAmountPaid"))
                                        dtDeposit.Rows.Add(dr)
                                    End If

                                    If dtDeposit.Rows.Count > 0 Then
                                        Dim objMakeDeposit As New MakeDeposit
                                        With objMakeDeposit
                                            .DepositId = CInt(CCommon.ToLong(drMain("numReferenceID")))
                                            .UserCntID = UserCntID
                                            .DomainID = DomainID

                                            Dim ds As New DataSet
                                            Dim str As String = ""

                                            If dtDeposit.Rows.Count > 0 Then
                                                ds.Tables.Add(dtDeposit.Copy)
                                                ds.Tables(0).TableName = "Item"
                                                str = ds.GetXml()
                                            End If

                                            .StrItems = str
                                            .Mode = 2
                                            .DepositePage = 2
                                            .SaveDataToMakeDepositDetails()
                                        End With
                                    End If
                                End If
                            Next
                        End If
                    End If
                    'added by :Sachin Sadhu||Date:22thAug2014
                    'Purpose : To make entry in BizDoc Queue
                    Dim objWfA As New Workflow.Workflow()
                    objWfA.DomainID = DomainID
                    objWfA.UserCntID = UserCntID
                    objWfA.RecordID = _OppBizDocId
                    objWfA.SaveWFBizDocQueue()
                    'end of code
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class
End Namespace
