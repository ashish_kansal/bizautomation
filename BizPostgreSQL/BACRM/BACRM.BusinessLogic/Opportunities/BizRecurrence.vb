﻿Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports System.Data.SqlClient
Imports BACRM.BusinessLogic.Common
Namespace BACRM.BusinessLogic.Opportunities
    Public Class BizRecurrence
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Methods"

        Public Sub Execute()
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(0).Value = Nothing
                arParms(0).Direction = ParameterDirection.InputOutput

                'Get all the order or bizdoc that needs to be recurred today
                ds = SqlDAL.ExecuteDataset(connString, "USP_BizRecurrence_Execute", arParms)

                Dim recConfigID As Long
                Dim domianID As Long
                Dim userCntID As Long
                Dim recurType As Integer
                Dim oppID As Long
                Dim oppBizDocID As Long
                Dim endDate As Date
                Dim recurFrequency As Integer
                Dim dtNextRecurrenceDate As Date
                Dim autoLinkUnAppliedPayment As Boolean
                Dim oppType As Integer
                Dim divisionID As Long

                Dim recurredOppID As Integer
                Dim recurredBizDocID As Integer

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    '@numType = 1 = Sales Order
                    '@numType = 2 = Invoice   

                    Try
                        For Each dr As DataRow In ds.Tables(0).Rows
                            Try
                                recConfigID = CCommon.ToLong(dr("numRecConfigID"))
                                domianID = CCommon.ToLong(dr("numDomainID"))
                                userCntID = CCommon.ToLong(dr("numUserCntID"))
                                recurType = CCommon.ToInteger(dr("numType"))
                                dtNextRecurrenceDate = CCommon.ToSqlDate(dr("dtNextRecurrenceDate"))
                                oppID = CCommon.ToLong(dr("numOppID"))
                                oppBizDocID = CCommon.ToLong(dr("numOppBizDocID"))

                                If Not dr("dtEndDate") Is DBNull.Value Then
                                    endDate = CCommon.ToSqlDate(dr("dtEndDate"))
                                End If

                                recurFrequency = CCommon.ToInteger(dr("numFrequency"))
                                autoLinkUnAppliedPayment = CCommon.ToBool(dr("bitAutolinkUnappliedPayment"))
                                oppType = CCommon.ToInteger(dr("tintOppType"))
                                divisionID = CCommon.ToLong(dr("numDivisionId"))

                                recurredOppID = 0
                                recurredBizDocID = 0

                                If CCommon.ToInteger(dr("numType")) = 1 Then
                                    Try
                                        arParms = New Npgsql.NpgsqlParameter(7) {}

                                        arParms(0) = New Npgsql.NpgsqlParameter("@numRecConfigID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                        arParms(0).Value = recConfigID

                                        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                        arParms(1).Value = domianID

                                        arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                        arParms(2).Value = userCntID

                                        arParms(3) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                        arParms(3).Value = oppID

                                        arParms(4) = New Npgsql.NpgsqlParameter("@numRecurOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                        arParms(4).Value = recurredOppID
                                        arParms(4).Direction = ParameterDirection.Output

                                        arParms(5) = New Npgsql.NpgsqlParameter("@numFrequency", NpgsqlTypes.NpgsqlDbType.SmallInt)
                                        arParms(5).Value = recurFrequency

                                        arParms(6) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Date)
                                        arParms(6).Value = endDate

                                        arParms(7) = New Npgsql.NpgsqlParameter("@Date", NpgsqlTypes.NpgsqlDbType.Date)
                                        arParms(7).Value = dtNextRecurrenceDate

                                        Dim objParam() As Object
                                        objParam = arParms.ToArray()
                                        SqlDAL.ExecuteNonQuery(connString, "USP_BizRecurrence_Order", objParam, True)
                                        recurredOppID = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                                        If recurredOppID > 0 Then
                                            'Create BizDocs for new oppoertunity
                                            Dim dtBizDocs As DataTable
                                            Dim objBizDocs As New OppBizDocs
                                            objBizDocs.OppId = oppID
                                            objBizDocs.DomainID = domianID
                                            objBizDocs.UserCntID = userCntID
                                            Dim dsBizDocs As DataSet = objBizDocs.GetBizDocsByOOpId

                                            If Not dsBizDocs Is Nothing AndAlso dsBizDocs.Tables.Count > 1 Then
                                                dtBizDocs = dsBizDocs.Tables(1)
                                            End If

                                            If Not dtBizDocs Is Nothing AndAlso dtBizDocs.Rows.Count > 0 Then

                                                For Each drBizDoc As DataRow In dtBizDocs.Rows
                                                    Dim bitAuthorativeBizDoc As Boolean = False

                                                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                                        arParms = New Npgsql.NpgsqlParameter(9) {}

                                                        arParms(0) = New Npgsql.NpgsqlParameter("@numRecConfigID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                                        arParms(0).Value = recConfigID

                                                        arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                                        arParms(1).Value = userCntID

                                                        arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                                        arParms(2).Value = recurredOppID

                                                        arParms(3) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                                        arParms(3).Value = CCommon.ToLong(drBizDoc("numOppBizDocsId"))

                                                        arParms(4) = New Npgsql.NpgsqlParameter("@numRecurBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                                        arParms(4).Value = recurredBizDocID
                                                        arParms(4).Direction = ParameterDirection.Output

                                                        arParms(5) = New Npgsql.NpgsqlParameter("@bitAuthorative", NpgsqlTypes.NpgsqlDbType.Bit)
                                                        arParms(5).Direction = ParameterDirection.Output

                                                        arParms(6) = New Npgsql.NpgsqlParameter("@bitBizDocRecur", NpgsqlTypes.NpgsqlDbType.Bit)
                                                        arParms(6).Value = False

                                                        arParms(7) = New Npgsql.NpgsqlParameter("@numFrequency", NpgsqlTypes.NpgsqlDbType.SmallInt)
                                                        arParms(7).Value = recurFrequency

                                                        arParms(8) = New Npgsql.NpgsqlParameter("@numRecurParentOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                                        arParms(8).Value = oppID

                                                        arParms(9) = New Npgsql.NpgsqlParameter("@Date", NpgsqlTypes.NpgsqlDbType.Date)
                                                        arParms(9).Value = dtNextRecurrenceDate

                                                        objParam = arParms.ToArray()
                                                        SqlDAL.ExecuteNonQuery(connString, "USP_BizRecurrence_BizDoc", objParam, True)
                                                        recurredBizDocID = CCommon.ToInteger(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)
                                                        bitAuthorativeBizDoc = CCommon.ToBool(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                                                        If recurredBizDocID > 0 AndAlso bitAuthorativeBizDoc Then
                                                            CreateJournalEntry(recurredOppID, recurredBizDocID, domianID, userCntID, oppType, autoLinkUnAppliedPayment, 0, divisionID)
                                                        End If

                                                        objTransactionScope.Complete()
                                                    End Using
                                                Next
                                            End If
                                        End If
                                    Catch ex As Exception
                                        LogException(ex, recConfigID, domianID, True)
                                    End Try
                                ElseIf CCommon.ToInteger(dr("numType")) = 2 Then
                                    Try
                                        Dim bitAuthorativeBizDoc As Boolean = False
                                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                            arParms = New Npgsql.NpgsqlParameter(9) {}

                                            arParms(0) = New Npgsql.NpgsqlParameter("@numRecConfigID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                            arParms(0).Value = recConfigID

                                            arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                            arParms(1).Value = userCntID

                                            arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                            arParms(2).Value = oppID

                                            arParms(3) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                            arParms(3).Value = oppBizDocID

                                            arParms(4) = New Npgsql.NpgsqlParameter("@numRecurBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                            arParms(4).Value = recurredBizDocID
                                            arParms(4).Direction = ParameterDirection.Output

                                            arParms(5) = New Npgsql.NpgsqlParameter("@bitAuthorative", NpgsqlTypes.NpgsqlDbType.Bit)
                                            arParms(5).Direction = ParameterDirection.Output

                                            arParms(6) = New Npgsql.NpgsqlParameter("@bitBizDocRecur", NpgsqlTypes.NpgsqlDbType.Bit)
                                            arParms(6).Value = True

                                            arParms(7) = New Npgsql.NpgsqlParameter("@numFrequency", NpgsqlTypes.NpgsqlDbType.SmallInt)
                                            arParms(7).Value = recurFrequency

                                            arParms(8) = New Npgsql.NpgsqlParameter("@numRecurParentOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                                            arParms(8).Value = oppID

                                            arParms(9) = New Npgsql.NpgsqlParameter("@Date", NpgsqlTypes.NpgsqlDbType.Date)
                                            arParms(9).Value = dtNextRecurrenceDate

                                            Dim objParam() As Object
                                            objParam = arParms.ToArray()
                                            SqlDAL.ExecuteNonQuery(connString, "USP_BizRecurrence_BizDoc", objParam, True)
                                            recurredBizDocID = CCommon.ToInteger(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)
                                            bitAuthorativeBizDoc = CCommon.ToBool(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                                            If recurredBizDocID > 0 Then
                                                If bitAuthorativeBizDoc Then
                                                    CreateJournalEntry(oppID, recurredBizDocID, domianID, userCntID, oppType, autoLinkUnAppliedPayment, 0, divisionID)
                                                End If
                                            Else
                                                Throw New Exception(String.Format("Something worng with bizdoc recurrence. New bizdoc id is 0 and no exception returned from USP_BizRecurrence_BizDoc. ParentOrderID: {0} & ParentBizDocID = {1}", oppID, oppBizDocID))
                                            End If

                                            objTransactionScope.Complete()
                                        End Using
                                    Catch ex As Exception
                                        LogException(ex, recConfigID, domianID, True)
                                    End Try
                                End If
                            Catch ex As Exception
                                LogException(ex, 0, 0)
                            End Try
                        Next
                    Catch ex As Exception
                        LogException(ex, 0, 0)
                    End Try
                End If
            Catch ex As Exception
                LogException(ex, 0, 0)
            End Try
        End Sub

        Public Sub SetDisable(ByVal numRecConfigID As Long)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRecConfigID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numRecConfigID

                SqlDAL.ExecuteNonQuery(connString, "USP_RecurrenceConfiguration_SetDisable", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Delete(ByVal numRecConfigID As Long, ByVal numDomainID As Long, ByVal numUserCntID As Long)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRecConfigID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numRecConfigID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numDomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = numUserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_RecurrenceConfiguration_Delete", arParms)

            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetRecurrenceDetail(ByVal numRecConfigID As Long) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numRecConfigID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numRecConfigID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_RecurrenceConfiguration_GetDetail", arParms)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        'type - 1 = Order
        'type - 2 = BizDoc
        Public Function GetParentDetail(ByVal numID As Long, ByVal type As Short) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numID

                arParms(1) = New Npgsql.NpgsqlParameter("@numType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = type

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_RecurrenceConfiguration_GetParentDetail", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

#Region "Private Methods"
        Private Sub LogException(ByVal ex As Exception, ByVal recurConfigID As Long, ByVal domainID As Long, Optional ByVal bitUpdateTransaction As Boolean = False)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = domainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRecurConfigID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = recurConfigID

                arParms(2) = New Npgsql.NpgsqlParameter("@Type", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = ex.GetType().Name

                arParms(3) = New Npgsql.NpgsqlParameter("@Source", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = ex.Source

                arParms(4) = New Npgsql.NpgsqlParameter("@Message", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = ex.Message

                arParms(5) = New Npgsql.NpgsqlParameter("@StackStrace", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(5).Value = ex.StackTrace

                arParms(6) = New Npgsql.NpgsqlParameter("@bitUpdateTransaction", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = bitUpdateTransaction

                SqlDAL.ExecuteNonQuery(connString, "USP_RecurrenceErrorLog_Insert", arParms)
            Catch

            End Try
        End Sub

        Private Function CreateJournalEntry(ByVal lngOppID As Long,
                                            ByVal OppBizDocID As Long,
                                            ByVal domainID As Long,
                                            ByVal userCntID As Long,
                                            ByVal OppType As Integer,
                                            ByVal autolinkUnappliedPayment As Boolean,
                                            ByVal clientMachineOffset As Integer,
                                            ByVal lngDivId As Long) As String
            Dim strResult As String = ""
            Try
                Dim ds As New DataSet
                Dim dtOppBiDocItems As DataTable
                Dim objOppBizDocs As New OppBizDocs
                Dim JournalId As Long

                objOppBizDocs.DomainID = domainID
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.UserCntID = userCntID
                objOppBizDocs.OppBizDocId = OppBizDocID
                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                dtOppBiDocItems = ds.Tables(0)

                Dim objCalculateDealAmount As New CalculateDealAmount

                objCalculateDealAmount.CalculateDealAmount(lngOppID, OppBizDocID, OppType, domainID, dtOppBiDocItems)


                JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                Dim objJournalEntries As New JournalEntry

                If OppType = 2 Then
                    If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                        objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppID, domainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                    Else
                        objJournalEntries.SaveJournalEntriesPurchase(lngOppID, domainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                    End If
                ElseIf OppType = 1 Then
                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                        objJournalEntries.SaveJournalEntriesSales(lngOppID, domainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                    Else
                        objJournalEntries.SaveJournalEntriesSalesNew(lngOppID, domainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                    End If

                    If autolinkUnappliedPayment Then
                        Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                        objSalesFulfillment.DomainID = domainID
                        objSalesFulfillment.UserCntID = userCntID
                        objSalesFulfillment.ClientTimeZoneOffset = clientMachineOffset
                        objSalesFulfillment.OppId = lngOppID
                        objSalesFulfillment.OppBizDocId = OppBizDocID

                        objSalesFulfillment.AutoLinkUnappliedPayment()
                    End If
                End If

            Catch ex As Exception
                If ex.Message = "NOT_ALLOWED" Then
                    Throw New Exception("CreateJournalEntry - To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!")
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    Throw New Exception("CreateJournalEntry - Only a Sales Order can create a Fulfillment Order")
                ElseIf ex.Message = "FY_CLOSED" Then
                    Throw New Exception("CreateJournalEntry - This transaction can not be posted,Because transactions date belongs to closed financial year.")
                ElseIf ex.Message = "AlreadyInvoice" Then
                    Throw New Exception("CreateJournalEntry - You can’t create a Invoice against a Fulfillment Order that already contains a Invoice.")
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    Throw New Exception("CreateJournalEntry - You can’t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip.")
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    Throw New Exception("CreateJournalEntry - You can’t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip.")
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    Throw New Exception("CreateJournalEntry - Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section.")
                Else
                    Throw
                End If
            End Try

            Return strResult
        End Function
#End Region

    End Class
End Namespace