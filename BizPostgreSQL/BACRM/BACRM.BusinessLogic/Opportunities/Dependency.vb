'Created Anoop Jayaraj
Option Explicit On 
Option Strict On

Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Opportunities


    Public Class Dependency
        Inherits BACRM.BusinessLogic.CBusinessBase



        Private _ProcessID As Long
        Private _MileStoneID As Long
        Private _strDependency As String
        Private _OpporunityID As Long
        Private _OppStageId As Long

        Public Property OppStageId() As Long
            Get
                Return _OppStageId
            End Get
            Set(ByVal Value As Long)
                _OppStageId = Value
            End Set
        End Property

        Public Property OpporunityID() As Long
            Get
                Return _OpporunityID
            End Get
            Set(ByVal Value As Long)
                _OpporunityID = Value
            End Set
        End Property


        Public Property ProcessID() As Long
            Get
                Return _ProcessID
            End Get
            Set(ByVal Value As Long)
                _ProcessID = Value
            End Set
        End Property

        Public Property MileStoneID() As Long
            Get
                Return _MileStoneID
            End Get
            Set(ByVal Value As Long)
                _MileStoneID = Value
            End Set
        End Property

        Public Property strDependency() As String
            Get
                Return _strDependency
            End Get
            Set(ByVal Value As String)
                _strDependency = Value
            End Set
        End Property

        'Private DomainId As Long
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Long)
        '        DomainId = value
        '    End Set
        'End Property



        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Public Function GetDependencyDtls() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 1

                arParms(1) = New Npgsql.NpgsqlParameter("@strDependency", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strDependency

                arParms(2) = New Npgsql.NpgsqlParameter("@OppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OpporunityID

                arParms(3) = New Npgsql.NpgsqlParameter("@OppStageID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _OppStageId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPDependManage", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetOPPPro() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOPPPro", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

    End Class
End Namespace
