﻿Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.Opportunities

    Public Class WorkOrder
        Inherits BACRM.BusinessLogic.CBusinessBase
#Region "Public Properties"
        Public Property WorkOrderID As Long
        Public Property ClientTimeZoneOffset As Integer
        Public Property OpportunityName As String
        Public Property WarehouseName As String
        Public Property Comments As String
        Public Property AssignedTo As Long
        Public Property AssignedToName As String
        Public Property PlannedStartDate As Date
        Public Property RequestedFinish As Date
        Public Property ActualStartDate As String
        Public Property ProjectedFinish As String
        Public Property MilestoneName As String
        Public Property StageDetailsID As Long
        Public Property Records As String
        Public Property BuildPlan As Long
        Public Property BuildPlanName As String
        Public Property Quantity As Double
#End Region

#Region "Public Methods"
        Public Function GetRecursiveByID() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWOID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = WorkOrderID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_WorkOrder_GetRecursiveByID", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetWorkOrderDetailByID() As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_WorkOrder_GetWorkOrderDetailByID", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub GetDetailPageFieldsData()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_WorkOrder_GetDetailPageFieldsData", sqlParams.ToArray())

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If Not IsDBNull(dr("numWOId")) Then
                            Me.WorkOrderID = CType(dr("numWOId"), Long)
                        End If
                        If Not IsDBNull(dr("vcPoppName")) Then
                            Me.OpportunityName = CType(dr("vcPoppName"), String)
                        End If
                        If Not IsDBNull(dr("numAssignedTo")) Then
                            Me.AssignedTo = CType(dr("numAssignedTo"), Long)
                        End If
                        If Not IsDBNull(dr("numAssignedToName")) Then
                            Me.AssignedToName = CType(dr("numAssignedToName"), String)
                        End If
                        If Not IsDBNull(dr("vcWarehouse")) Then
                            Me.WarehouseName = CType(dr("vcWarehouse"), String)
                        End If
                        If Not IsDBNull(dr("vcInstruction")) Then
                            Me.Comments = CType(dr("vcInstruction"), String)
                        End If
                        If Not IsDBNull(dr("dtmStartDate")) Then
                            Me.PlannedStartDate = CType(dr("dtmStartDate"), Date)
                        End If
                        If Not IsDBNull(dr("dtmEndDate")) Then
                            Me.RequestedFinish = CType(dr("dtmEndDate"), Date)
                        End If
                        If Not IsDBNull(dr("dtActualStartDate")) Then
                            Me.ActualStartDate = CType(dr("dtActualStartDate"), DateTime).ToString(CCommon.GetValidationDateFormat() & " " & "hh:mm tt")
                        End If
                        If Not IsDBNull(dr("vcProjectedFinish")) Then
                            Me.ProjectedFinish = CType(dr("vcProjectedFinish"), String)
                        End If
                        If Not IsDBNull(dr("vcWorkOrderStatus")) Then
                            Me.Status = CType(dr("vcWorkOrderStatus"), String)
                        End If
                        If Not IsDBNull(dr("numBuildPlan")) Then
                            Me.BuildPlan = CType(dr("numBuildPlan"), Long)
                        End If
                        If Not IsDBNull(dr("numBuildPlanName")) Then
                            Me.BuildPlanName = CType(dr("numBuildPlanName"), String)
                        End If
                        If Not IsDBNull(dr("numQtyItemsReq")) Then
                            Me.Quantity = CType(dr("numQtyItemsReq"), Double)
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetMilestones() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WorkOrder_GetMilestones", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetStages() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcMilestoneName", MilestoneName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WorkOrder_GetStages", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetTasks(ByVal mode As Short) As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numStageDetailsId", StageDetailsID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@tintMode", mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WorkOrder_GetTasks", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Sub Disassembled()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numWOID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = WorkOrderID

                SqlDAL.ExecuteNonQuery(connString, "USP_WorkOrder_Disassembled", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetBOMDetail() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WorkOrder_GetBOMDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsTaskPending() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_WorkOrder_IsTaskPending", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsTaskStarted() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_WorkOrder_IsTaskStarted", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function IsCompleted() As Boolean
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_WorkOrder_IsCompleted", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetProjectedFinish() As DateTime
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return Convert.ToDateTime(SqlDAL.ExecuteScalar(connString, "USP_WorkOrder_GetProjectedFinish", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetCapacityLoad() As Long
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_WorkOrder_GetCapacityLoad", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetWorkOrderStatus() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcWorkOrderIds", Records, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WorkOrder_GetStatus", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function PickItems() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcPickItems", Records, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WorkOrder_PickItems", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetBOMForPick() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcWorkOrders", Records, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WorkOrder_GetBOMForPick", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetFieldValue(ByVal fieldName As String) As Object
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcFieldName", fieldName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_WorkOrder_GetFieldValue", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function UpdateQtyBuilt() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_WorkOrder_UpdateQtyBuilt", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub PutAway(ByVal mode As Short, ByVal receivedDate As DateTime, ByVal vcWarehouses As String)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWOID", WorkOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQtyReceived", Quantity, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@dtItemReceivedDate", receivedDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@vcWarehouses", vcWarehouses, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintMode", mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_WorkOrder_PutAway", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

    End Class

End Namespace


