'Created Anoop Jayaraj
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports System.Web.UI.WebControls
Imports System.Collections.Generic
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.BusinessLogic.Opportunities
    Public Class OppBizDocs
        Inherits BACRM.BusinessLogic.CBusinessBase


        Private _OppId As Long
        Private _BizDocId As Long
        Private _OppBizDocId As Long
        'Private UserCntID As Long
        Private _PurchaseNo As String
        'Private _Discount As Decimal
        Private _DivisionID As Long
        Private _ItemCode As Long
        Private _DueDate As Date = New Date(1753, 1, 1)
        Private _TotalAmount As Decimal
        Private _TotalAmountPaid As Decimal
        Private _TotalBalance As Decimal
        Private _strBizDocID As String
        Private _ContactID As Long
        'Private DomainId As Long
        Private _byteMode As Short
        Private _vcFooter As String
        Private _vcURL As String
        Private _vcDocName As String
        Private _BizDocAtchID As Long
        Private _vcOrder As String
        Private _OppType As Int32
        Private _strText As String = String.Empty
        Private _DeferredIncome As Boolean
        Private _bitBizDocId As Boolean
        Private _RecurringId As Long
        Private _vcPONo As String
        Private _vcComments As String
        Private _bitPartialShipment As Boolean
        Private _strBizDocItems As String
        'Private _boolDiscType As Boolean
        'Private _ShipCost As Decimal
        'Private _Interest As Decimal
        Private _ShipCompany As Long
        Private _ShipDoc As Long
        'Private _boolBillingTerms As Boolean
        'Private _boolInterestType As Boolean
        'Private _BillingDays As Integer
        Private _ShippedDate As Date = New Date(1753, 1, 1)
        Private _BizDocStatus As Long
        Private _ShipFromState As Long
        Private _ShipFromCountry As Long
        Private _ShipToCountry As Long
        Private _ShipToState As Long
        Private _strShipToState As String
        Private _strShipToCountry As String
        Private _strShipFromCountry As String
        Private _SalesTaxRate As Decimal
        Private _bitDisplayKitChild As Boolean


        Private _ReferenceID As Long
        Friend ShipCost As Integer

        Public Property ReferenceID() As Long
            Get
                Return _ReferenceID
            End Get
            Set(ByVal value As Long)
                _ReferenceID = value
            End Set
        End Property

        Private _ReferenceType As Short
        Public Property ReferenceType() As Short
            Get
                Return _ReferenceType
            End Get
            Set(ByVal value As Short)
                _ReferenceType = value
            End Set
        End Property

        Private _strBoxID As String
        Public Property StrBoxID() As String
            Get
                Return _strBoxID
            End Get
            Set(ByVal value As String)
                _strBoxID = value
            End Set
        End Property

        Private _intPageMode As Short
        Public Property PageMode() As Short
            Get
                Return _intPageMode
            End Get
            Set(ByVal value As Short)
                _intPageMode = value
            End Set
        End Property


        Private _ClientTimeZoneOffset As Integer
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal value As Integer)
                _ClientTimeZoneOffset = value
            End Set
        End Property

        Private _CategoryHDRID As Long
        Public Property CategoryHDRID() As Long
            Get
                Return _CategoryHDRID
            End Get
            Set(ByVal value As Long)
                _CategoryHDRID = value
            End Set
        End Property

        Private _strShipFromState As String

        Public Property strShipFromState() As String
            Get
                Return _strShipFromState
            End Get
            Set(ByVal value As String)
                _strShipFromState = value
            End Set
        End Property

        Private _UnitHour As Double
        Public Property UnitHour() As Double
            Get
                Return _UnitHour
            End Get
            Set(ByVal value As Double)
                _UnitHour = value
            End Set
        End Property

        Private _Price As Decimal
        Public Property Price() As Decimal
            Get
                Return _Price
            End Get
            Set(ByVal value As Decimal)
                _Price = value
            End Set
        End Property

        Private _ItemDesc As String
        Public Property ItemDesc() As String
            Get
                Return _ItemDesc
            End Get
            Set(ByVal value As String)
                _ItemDesc = value
            End Set
        End Property

        Private _Notes As String
        Public Property Notes() As String
            Get
                Return _Notes
            End Get
            Set(ByVal value As String)
                _Notes = value
            End Set
        End Property


        Private _OppItemID As Long
        Public Property OppItemID() As Long
            Get
                Return _OppItemID
            End Get
            Set(ByVal value As Long)
                _OppItemID = value
            End Set
        End Property


        Private _BoxID As Long
        Public Property BoxID() As Long
            Get
                Return _BoxID
            End Get
            Set(ByVal value As Long)
                _BoxID = value
            End Set
        End Property

        Private _BizDocCSS As String
        Public Property BizDocCSS() As String
            Get
                Return _BizDocCSS
            End Get
            Set(ByVal value As String)
                _BizDocCSS = value
            End Set
        End Property

        Public Property strShipFromCountry() As String
            Get
                Return _strShipFromCountry
            End Get
            Set(ByVal value As String)
                _strShipFromCountry = value
            End Set
        End Property

        Public Property strShipToCountry() As String
            Get
                Return _strShipToCountry
            End Get
            Set(ByVal value As String)
                _strShipToCountry = value
            End Set
        End Property

        Public Property strShipToState() As String
            Get
                Return _strShipToState
            End Get
            Set(ByVal value As String)
                _strShipToState = value
            End Set
        End Property

        Public Property ShipToState() As Long
            Get
                Return _ShipToState
            End Get
            Set(ByVal value As Long)
                _ShipToState = value
            End Set
        End Property

        Public Property ShipToCountry() As Long
            Get
                Return _ShipToCountry
            End Get
            Set(ByVal value As Long)
                _ShipToCountry = value
            End Set
        End Property

        Public Property ShipFromCountry() As Long
            Get
                Return _ShipFromCountry
            End Get
            Set(ByVal value As Long)
                _ShipFromCountry = value
            End Set
        End Property

        Public Property ShipFromState() As Long
            Get
                Return _ShipFromState
            End Get
            Set(ByVal value As Long)
                _ShipFromState = value
            End Set
        End Property

        Public Property BizDocStatus() As Long
            Get
                Return _BizDocStatus
            End Get
            Set(ByVal value As Long)
                _BizDocStatus = value
            End Set
        End Property

        Private _BizDocName As String
        Public Property BizDocName() As String
            Get
                Return _BizDocName
            End Get
            Set(ByVal value As String)
                _BizDocName = value
            End Set
        End Property

        Private _FromDate As Date
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal value As Date)
                _FromDate = value
            End Set
        End Property

        Private _boolRecurringBizDoc As Boolean
        Public Property boolRecurringBizDoc() As Boolean
            Get
                Return _boolRecurringBizDoc
            End Get
            Set(ByVal value As Boolean)
                _boolRecurringBizDoc = value
            End Set
        End Property

        Private _ShippingReportId As Long
        Public Property ShippingReportId() As Long
            Get
                Return _ShippingReportId
            End Get
            Set(ByVal value As Long)
                _ShippingReportId = value
            End Set
        End Property

        Private _ShippingReportItemId As Long
        Public Property ShippingReportItemId() As Long
            Get
                Return _ShippingReportItemId
            End Get
            Set(ByVal value As Long)
                _ShippingReportItemId = value
            End Set
        End Property

        Private _Value1 As String
        Public Property Value1() As String
            Get
                Return _Value1
            End Get
            Set(ByVal value As String)
                _Value1 = value
            End Set
        End Property

        Private _Value2 As String
        Public Property Value2() As String
            Get
                Return _Value2
            End Get
            Set(ByVal value As String)
                _Value2 = value
            End Set
        End Property

        Private _Value3 As String
        Public Property Value3() As String
            Get
                Return _Value3
            End Get
            Set(ByVal value As String)
                _Value3 = value
            End Set
        End Property

        Private _Value4 As String
        Public Property Value4() As String
            Get
                Return _Value4
            End Get
            Set(ByVal value As String)
                _Value4 = value
            End Set
        End Property

        Private _FromState As String
        Public Property FromState() As String
            Get
                Return _FromState
            End Get
            Set(ByVal value As String)
                _FromState = value
            End Set
        End Property

        Private _FromZip As String
        Public Property FromZip() As String
            Get
                Return _FromZip
            End Get
            Set(ByVal value As String)
                _FromZip = value
            End Set
        End Property

        Private _FromCountry As String
        Public Property FromCountry() As String
            Get
                Return _FromCountry
            End Get
            Set(ByVal value As String)
                _FromCountry = value
            End Set
        End Property

        Private _ToState As String
        Public Property ToState() As String
            Get
                Return _ToState
            End Get
            Set(ByVal value As String)
                _ToState = value
            End Set
        End Property

        Private _ToZip As String
        Public Property ToZip() As String
            Get
                Return _ToZip
            End Get
            Set(ByVal value As String)
                _ToZip = value
            End Set
        End Property

        Private _ToCountry As String
        Public Property ToCountry() As String
            Get
                Return _ToCountry
            End Get
            Set(ByVal value As String)
                _ToCountry = value
            End Set
        End Property


        Private _TrackingNumber As String
        Public Property TrackingNumber() As String
            Get
                Return _TrackingNumber
            End Get
            Set(ByVal value As String)
                _TrackingNumber = value
            End Set
        End Property

        Private _ShippingLableImage As String
        Public Property ShippingLableImage() As String
            Get
                Return _ShippingLableImage
            End Get
            Set(ByVal value As String)
                _ShippingLableImage = value
            End Set
        End Property

        Private _IsAuthoritativeBizDoc As Boolean
        Public Property IsAuthoritativeBizDoc() As Boolean
            Get
                Return _IsAuthoritativeBizDoc
            End Get
            Set(ByVal value As Boolean)
                _IsAuthoritativeBizDoc = value
            End Set
        End Property

        Public Property ShippedDate() As Date
            Get
                Return _ShippedDate
            End Get
            Set(ByVal value As Date)
                _ShippedDate = value
            End Set
        End Property

        'Public Property BillingDays() As Integer
        '    Get
        '        Return _BillingDays
        '    End Get
        '    Set(ByVal value As Integer)
        '        _BillingDays = value
        '    End Set
        'End Property

        'Public Property boolInterestType() As Boolean
        '    Get
        '        Return _boolInterestType
        '    End Get
        '    Set(ByVal value As Boolean)
        '        _boolInterestType = value
        '    End Set
        'End Property

        'Public Property boolBillingTerms() As Boolean
        '    Get
        '        Return _boolBillingTerms
        '    End Get
        '    Set(ByVal value As Boolean)
        '        _boolBillingTerms = value
        '    End Set
        'End Property

        Public Property ShipDoc() As Long
            Get
                Return _ShipDoc
            End Get
            Set(ByVal value As Long)
                _ShipDoc = value
            End Set
        End Property

        Public Property ShipCompany() As Long
            Get
                Return _ShipCompany
            End Get
            Set(ByVal value As Long)
                _ShipCompany = value
            End Set
        End Property

        'Public Property Interest() As Decimal
        '    Get
        '        Return _Interest
        '    End Get
        '    Set(ByVal value As Decimal)
        '        _Interest = value
        '    End Set
        'End Property

        'Public Property ShipCost() As Decimal
        '    Get
        '        Return _ShipCost
        '    End Get
        '    Set(ByVal value As Decimal)
        '        _ShipCost = value
        '    End Set
        'End Property

        'Public Property boolDiscType() As Boolean
        '    Get
        '        Return _boolDiscType
        '    End Get
        '    Set(ByVal value As Boolean)
        '        _boolDiscType = value
        '    End Set
        'End Property

        Public Property strBizDocItems() As String
            Get
                Return _strBizDocItems
            End Get
            Set(ByVal value As String)
                _strBizDocItems = value
            End Set
        End Property

        Public Property bitPartialShipment() As Boolean
            Get
                Return _bitPartialShipment
            End Get
            Set(ByVal value As Boolean)
                _bitPartialShipment = value
            End Set
        End Property

        Public Property vcComments() As String
            Get
                Return _vcComments
            End Get
            Set(ByVal value As String)
                _vcComments = value
            End Set
        End Property

        Public Property vcPONo() As String
            Get
                Return _vcPONo
            End Get
            Set(ByVal value As String)
                _vcPONo = value
            End Set
        End Property

        Public Property RecurringId() As Long
            Get
                Return _RecurringId
            End Get
            Set(ByVal value As Long)
                _RecurringId = value
            End Set
        End Property

        Public Property vcOrder() As String
            Get
                Return _vcOrder
            End Get
            Set(ByVal Value As String)
                _vcOrder = Value
            End Set
        End Property

        Public Property BizDocAtchID() As Long
            Get
                Return _BizDocAtchID
            End Get
            Set(ByVal Value As Long)
                _BizDocAtchID = Value
            End Set
        End Property

        Public Property vcDocName() As String
            Get
                Return _vcDocName
            End Get
            Set(ByVal Value As String)
                _vcDocName = Value
            End Set
        End Property

        Public Property vcURL() As String
            Get
                Return _vcURL
            End Get
            Set(ByVal Value As String)
                _vcURL = Value
            End Set
        End Property

        Public Property vcFooter() As String
            Get
                Return _vcFooter
            End Get
            Set(ByVal Value As String)
                _vcFooter = Value
            End Set
        End Property

        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property



        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property strBizDocID() As String
            Get
                Return _strBizDocID
            End Get
            Set(ByVal Value As String)
                _strBizDocID = Value
            End Set
        End Property

        Public Property TotalBalance() As Decimal
            Get
                Return _TotalBalance
            End Get
            Set(ByVal Value As Decimal)
                _TotalBalance = Value
            End Set
        End Property

        Public Property TotalAmountPaid() As Decimal
            Get
                Return _TotalAmountPaid
            End Get
            Set(ByVal Value As Decimal)
                _TotalAmountPaid = Value
            End Set
        End Property

        Public Property TotalAmount() As Decimal
            Get
                Return _TotalAmount
            End Get
            Set(ByVal Value As Decimal)
                _TotalAmount = Value
            End Set
        End Property

        Public Property DueDate() As Date
            Get
                Return _DueDate
            End Get
            Set(ByVal Value As Date)
                _DueDate = Value
            End Set
        End Property

        Public Property ItemCode() As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal Value As Long)
                _ItemCode = Value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Long)
                _DivisionID = Value
            End Set
        End Property

        'Public Property Discount() As Decimal
        '    Get
        '        Return _Discount
        '    End Get
        '    Set(ByVal Value As Decimal)
        '        _Discount = Value
        '    End Set
        'End Property

        Public Property PurchaseNo() As String
            Get
                Return _PurchaseNo
            End Get
            Set(ByVal Value As String)
                _PurchaseNo = Value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Long)
        '        UserCntID = Value
        '    End Set
        'End Property

        Public Property OppBizDocId() As Long
            Get
                Return _OppBizDocId
            End Get
            Set(ByVal Value As Long)
                _OppBizDocId = Value
            End Set
        End Property

        Public Property BizDocId() As Long
            Get
                Return _BizDocId
            End Get
            Set(ByVal Value As Long)
                _BizDocId = Value
            End Set
        End Property

        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal Value As Long)
                _OppId = Value
            End Set
        End Property

        Public Property OppType() As Int32
            Get
                Return _OppType
            End Get
            Set(ByVal Value As Int32)
                _OppType = Value
            End Set
        End Property

        Public Property strText() As String
            Get
                Return _strText
            End Get
            Set(ByVal value As String)
                _strText = value
            End Set
        End Property

        Public Property DeferredIncome() As Boolean
            Get
                Return _DeferredIncome
            End Get
            Set(ByVal value As Boolean)
                _DeferredIncome = value
            End Set
        End Property


        Public Property bitBizDocId() As Boolean
            Get
                Return _bitBizDocId
            End Get
            Set(ByVal value As Boolean)
                _bitBizDocId = value
            End Set
        End Property

        Private _TemplateType As Short

        Public Property TemplateType() As Short
            Get
                Return _TemplateType
            End Get
            Set(ByVal value As Short)
                _TemplateType = value
            End Set
        End Property

        Private _ProID As Long

        Public Property ProID() As Long
            Get
                Return _ProID
            End Get
            Set(ByVal Value As Long)
                _ProID = Value
            End Set
        End Property

        Private _StageId As Long
        Public Property StageId() As Long
            Get
                Return _StageId
            End Get
            Set(ByVal Value As Long)
                _StageId = Value
            End Set
        End Property

        Private _ClassId As Long
        Public Property ClassId() As Long
            Get
                Return _ClassId
            End Get
            Set(ByVal Value As Long)
                _ClassId = Value
            End Set
        End Property

        Private _Deferred As Short
        Public Property Deferred() As Short
            Get
                Return _Deferred
            End Get
            Set(ByVal Value As Short)
                _Deferred = Value
            End Set
        End Property

        Private _CreditAmount As Decimal
        Public Property CreditAmount() As Decimal
            Get
                Return _CreditAmount
            End Get
            Set(ByVal value As Decimal)
                _CreditAmount = value
            End Set
        End Property

        Private _DealAmount As Decimal
        Public Property DealAmount() As Decimal
            Get
                Return _DealAmount
            End Get
            Set(ByVal value As Decimal)
                _DealAmount = value
            End Set
        End Property

        Private _boolRentalBizDoc As Boolean
        Public Property boolRentalBizDoc() As Boolean
            Get
                Return _boolRentalBizDoc
            End Get
            Set(ByVal value As Boolean)
                _boolRentalBizDoc = value
            End Set
        End Property
        Private _intAddReferenceDocument As Long
        Public Property intAddReferenceDocument() As Long
            Get
                Return _intAddReferenceDocument
            End Get
            Set(ByVal value As Long)
                _intAddReferenceDocument = value
            End Set
        End Property

        Private _BizDocTemplateID As Long
        Public Property BizDocTemplateID() As Long
            Get
                Return _BizDocTemplateID
            End Get
            Set(ByVal value As Long)
                _BizDocTemplateID = value
            End Set
        End Property

        Private _TemplateName As String
        Public Property TemplateName() As String
            Get
                Return _TemplateName
            End Get
            Set(ByVal value As String)
                _TemplateName = value
            End Set
        End Property

        Private _boolDefault As Boolean
        Public Property boolDefault() As Boolean
            Get
                Return _boolDefault
            End Get
            Set(ByVal value As Boolean)
                _boolDefault = value
            End Set
        End Property

        Private _PayorCountryCode As String
        Public Property PayorCountryCode() As String
            Get
                Return _PayorCountryCode
            End Get
            Set(ByVal value As String)
                _PayorCountryCode = value
            End Set
        End Property
        Private _PayorZipCode As String
        Public Property PayorZipCode() As String
            Get
                Return _PayorZipCode
            End Get
            Set(ByVal value As String)
                _PayorZipCode = value
            End Set
        End Property
        Private _PayorType As Short
        Public Property PayorType() As Short
            Get
                Return _PayorType
            End Get
            Set(ByVal value As Short)
                _PayorType = value
            End Set
        End Property
        Private _PayorAccountNo As String
        Public Property PayorAccountNo() As String
            Get
                Return _PayorAccountNo
            End Get
            Set(ByVal value As String)
                _PayorAccountNo = value
            End Set
        End Property
        Private _FromCity As String
        Public Property FromCity() As String
            Get
                Return _FromCity
            End Get
            Set(ByVal value As String)
                _FromCity = value
            End Set
        End Property
        Private _FromAddressLine1 As String
        Public Property FromAddressLine1() As String
            Get
                Return _FromAddressLine1
            End Get
            Set(ByVal value As String)
                _FromAddressLine1 = value
            End Set
        End Property
        Private _FromAddressLine2 As String
        Public Property FromAddressLine2() As String
            Get
                Return _FromAddressLine2
            End Get
            Set(ByVal value As String)
                _FromAddressLine2 = value
            End Set
        End Property
        Private _ToCity As String
        Public Property ToCity() As String
            Get
                Return _ToCity
            End Get
            Set(ByVal value As String)
                _ToCity = value
            End Set
        End Property

        Private _ToAddressLine1 As String
        Public Property ToAddressLine1() As String
            Get
                Return _ToAddressLine1
            End Get
            Set(ByVal value As String)
                _ToAddressLine1 = value
            End Set
        End Property
        Private _ToAddressLine2 As String
        Public Property ToAddressLine2() As String
            Get
                Return _ToAddressLine2
            End Get
            Set(ByVal value As String)
                _ToAddressLine2 = value
            End Set
        End Property

        Private _strFromName As String
        Public Property FromName() As String
            Get
                Return _strFromName
            End Get
            Set(ByVal value As String)
                _strFromName = value
            End Set
        End Property

        Private _strFromCompany As String
        Public Property FromCompany() As String
            Get
                Return _strFromCompany
            End Get
            Set(ByVal value As String)
                _strFromCompany = value
            End Set
        End Property

        Private _strFromPhone As String
        Public Property FromPhone() As String
            Get
                Return _strFromPhone
            End Get
            Set(ByVal value As String)
                _strFromPhone = value
            End Set
        End Property

        Private _IsFromResidential As Boolean
        Public Property IsFromResidential() As Boolean
            Get
                Return _IsFromResidential
            End Get
            Set(ByVal value As Boolean)
                _IsFromResidential = value
            End Set
        End Property

        Private _strToName As String
        Public Property ToName() As String
            Get
                Return _strToName
            End Get
            Set(ByVal value As String)
                _strToName = value
            End Set
        End Property

        Private _strToCompany As String
        Public Property ToCompany() As String
            Get
                Return _strToCompany
            End Get
            Set(ByVal value As String)
                _strToCompany = value
            End Set
        End Property

        Private _strToPhone As String
        Public Property ToPhone() As String
            Get
                Return _strToPhone
            End Get
            Set(ByVal value As String)
                _strToPhone = value
            End Set
        End Property

        Private _IsToResidential As Boolean
        Public Property IsToResidential() As Boolean
            Get
                Return _IsToResidential
            End Get
            Set(ByVal value As Boolean)
                _IsToResidential = value
            End Set
        End Property

        Private _IsCOD As Boolean
        Public Property IsCOD() As Boolean
            Get
                Return _IsCOD
            End Get
            Set(ByVal value As Boolean)
                _IsCOD = value
            End Set
        End Property

        Private _IsDryIce As Boolean
        Public Property IsDryIce() As Boolean
            Get
                Return _IsDryIce
            End Get
            Set(ByVal value As Boolean)
                _IsDryIce = value
            End Set
        End Property

        Private _IsHoldSaturday As Boolean
        Public Property IsHoldSaturday() As Boolean
            Get
                Return _IsHoldSaturday
            End Get
            Set(ByVal value As Boolean)
                _IsHoldSaturday = value
            End Set
        End Property

        Private _IsHomeDelivery As Boolean
        Public Property IsHomeDelivery() As Boolean
            Get
                Return _IsHomeDelivery
            End Get
            Set(ByVal value As Boolean)
                _IsHomeDelivery = value
            End Set
        End Property

        Private _IsInsideDelivery As Boolean
        Public Property IsInsideDelivery() As Boolean
            Get
                Return _IsInsideDelivery
            End Get
            Set(ByVal value As Boolean)
                _IsInsideDelivery = value
            End Set
        End Property

        Private _IsInsidePickup As Boolean
        Public Property IsInsidePickup() As Boolean
            Get
                Return _IsInsidePickup
            End Get
            Set(ByVal value As Boolean)
                _IsInsidePickup = value
            End Set
        End Property

        Private _IsReturnShipment As Boolean
        Public Property IsReturnShipment() As Boolean
            Get
                Return _IsReturnShipment
            End Get
            Set(ByVal value As Boolean)
                _IsReturnShipment = value
            End Set
        End Property

        Private _IsSaturdayDelivery As Boolean
        Public Property IsSaturdayDelivery() As Boolean
            Get
                Return _IsSaturdayDelivery
            End Get
            Set(ByVal value As Boolean)
                _IsSaturdayDelivery = value
            End Set
        End Property

        Private _IsSaturdayPickup As Boolean
        Public Property IsSaturdayPickup() As Boolean
            Get
                Return _IsSaturdayPickup
            End Get
            Set(ByVal value As Boolean)
                _IsSaturdayPickup = value
            End Set
        End Property

        Private _CODAmount As String
        Public Property CODAmount() As String
            Get
                Return _CODAmount
            End Get
            Set(ByVal value As String)
                _CODAmount = value
            End Set
        End Property

        Private _strCODType As String
        Public Property CODType() As String
            Get
                Return _strCODType
            End Get
            Set(ByVal value As String)
                _strCODType = value
            End Set
        End Property

        Private _dblTotalInsuredValue As Double
        Public Property TotalInsuredValue() As Double
            Get
                Return _dblTotalInsuredValue
            End Get
            Set(ByVal value As Double)
                _dblTotalInsuredValue = value
            End Set
        End Property

        Private _IsAdditionalHandling As Boolean
        Public Property IsAdditionalHandling() As Boolean
            Get
                Return _IsAdditionalHandling
            End Get
            Set(ByVal value As Boolean)
                _IsAdditionalHandling = value
            End Set
        End Property

        Private _IsLargePackage As Boolean
        Public Property LargePackage() As Boolean
            Get
                Return _IsLargePackage
            End Get
            Set(ByVal value As Boolean)
                _IsLargePackage = value
            End Set
        End Property

        Private _strDeliveryConfirmation As String
        Public Property DeliveryConfirmation() As String
            Get
                Return _strDeliveryConfirmation
            End Get
            Set(ByVal value As String)
                _strDeliveryConfirmation = value
            End Set
        End Property

        Private _strDescription As String
        Public Property Description() As String
            Get
                Return _strDescription
            End Get
            Set(ByVal value As String)
                _strDescription = value
            End Set
        End Property

        Private _intShipBasedOn As Integer
        Public Property ShipBasedOn() As Integer
            Get
                Return _intShipBasedOn
            End Get
            Set(ByVal value As Integer)
                _intShipBasedOn = value
            End Set
        End Property

        Private _intItemClassID As Long
        Public Property ItemClassID() As Long
            Get
                Return _intItemClassID
            End Get
            Set(ByVal value As Long)
                _intItemClassID = value
            End Set
        End Property

        Private _intType As Integer
        Public Property Type() As Integer
            Get
                Return _intType
            End Get
            Set(ByVal value As Integer)
                _intType = value
            End Set
        End Property

        Private _lngBizDocItemId As Long
        Public Property BizDocItemId() As Long
            Get
                Return _lngBizDocItemId
            End Get
            Set(ByVal value As Long)
                _lngBizDocItemId = value
            End Set
        End Property

        Private _PackageTypeID As Decimal
        Public Property PackageTypeID() As Decimal
            Get
                Return _PackageTypeID
            End Get
            Set(ByVal value As Decimal)
                _PackageTypeID = value
            End Set
        End Property

        Private _lngShipClassID As Long
        Public Property ShipClassID() As Long
            Get
                Return _lngShipClassID
            End Get
            Set(ByVal value As Long)
                _lngShipClassID = value
            End Set
        End Property


        Public Property SalesTaxRate() As Decimal
            Get
                Return _SalesTaxRate
            End Get
            Set(ByVal value As Decimal)
                _SalesTaxRate = value
            End Set
        End Property
        Private _lngBoxServiceTypeID As Long
        Public Property BoxServiceTypeID() As Long
            Get
                Return _lngBoxServiceTypeID
            End Get
            Set(ByVal value As Long)
                _lngBoxServiceTypeID = value
            End Set
        End Property

        Private _tintMode As Short
        Public Property tintMode() As Short
            Get
                Return _tintMode
            End Get
            Set(ByVal value As Short)
                _tintMode = value
            End Set
        End Property

        Private _strOrderIDs As String
        Public Property strOrderID() As String
            Get
                Return _strOrderIDs
            End Get
            Set(ByVal value As String)
                _strOrderIDs = value
            End Set
        End Property

        Private _strOppBizDocIds As String
        Public Property OppBizDocIds() As String
            Get
                Return _strOppBizDocIds
            End Get
            Set(ByVal value As String)
                _strOppBizDocIds = value
            End Set
        End Property
        'Private _DeliveryDate As Date
        'Public Property DeliveryDate() As Date
        '    Get
        '        Return _DeliveryDate
        '    End Get
        '    Set(ByVal value As Date)
        '        _DeliveryDate = value
        '    End Set
        'End Property

        Private _TrackingNo As String
        Public Property TrackingNo() As String
            Get
                Return _TrackingNo
            End Get
            Set(ByVal value As String)
                _TrackingNo = value
            End Set
        End Property

        'Private _ShippingMethod As String
        'Public Property ShippingMethod() As String
        '    Get
        '        Return _ShippingMethod
        '    End Get
        '    Set(ByVal value As String)
        '        _ShippingMethod = value
        '    End Set
        'End Property

        Private _RefOrderNo As String
        Public Property RefOrderNo() As String
            Get
                Return _RefOrderNo
            End Get
            Set(ByVal value As String)
                _RefOrderNo = value
            End Set
        End Property

        Private _SequenceId As String
        Public Property SequenceId() As String
            Get
                Return _SequenceId
            End Get
            Set(ByVal value As String)
                _SequenceId = value
            End Set
        End Property

        Private _bitNotValidateBizDocFulfillment As Boolean
        Public Property bitNotValidateBizDocFulfillment() As Boolean
            Get
                Return _bitNotValidateBizDocFulfillment
            End Get
            Set(ByVal value As Boolean)
                _bitNotValidateBizDocFulfillment = value
            End Set
        End Property

        Private _fltExchangeRateBizDoc As Decimal
        Public Property fltExchangeRateBizDoc() As Decimal
            Get
                Return _fltExchangeRateBizDoc
            End Get
            Set(ByVal value As Decimal)
                _fltExchangeRateBizDoc = value
            End Set
        End Property

        Private _OppQueueID As Long
        Public Property OppQueueID() As Long
            Get
                Return _OppQueueID
            End Get
            Set(ByVal value As Long)
                _OppQueueID = value
            End Set
        End Property

        Private _tintProcessStatus As Short
        Public Property tintProcessStatus() As Short
            Get
                Return _tintProcessStatus
            End Get
            Set(ByVal value As Short)
                _tintProcessStatus = value
            End Set
        End Property

        Private _numRuleID As Long
        Public Property numRuleID() As Long
            Get
                Return _numRuleID
            End Get
            Set(ByVal value As Long)
                _numRuleID = value
            End Set
        End Property

        Private _bitSuccess As Boolean
        Public Property bitSuccess() As Boolean
            Get
                Return _bitSuccess
            End Get
            Set(ByVal value As Boolean)
                _bitSuccess = value
            End Set
        End Property

        Private _FromOppBizDocsId As Long
        Public Property FromOppBizDocsId() As Long
            Get
                Return _FromOppBizDocsId
            End Get
            Set(ByVal value As Long)
                _FromOppBizDocsId = value
            End Set
        End Property

        Private _OrderStatus As Long
        Public Property OrderStatus() As Long
            Get
                Return _OrderStatus
            End Get
            Set(ByVal value As Long)
                _OrderStatus = value
            End Set
        End Property

        Private _CurrentPage As Integer = 0
        Private _PageSize As Integer = 0
        Private _TotalRecords As Integer = 0
        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Private _dblTotalCustomsValue As Double
        Public Property TotalCustomsValue() As Double
            Get
                Return _dblTotalCustomsValue
            End Get
            Set(ByVal value As Double)
                _dblTotalCustomsValue = value
            End Set
        End Property
        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-March-05

        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Private _numOrientation As Integer
        Public Property numOrientation() As Integer
            Get
                Return _numOrientation
            End Get
            Set(ByVal value As Integer)
                _numOrientation = value
            End Set
        End Property

        Private _bitKeepFooterBottom As Boolean
        Public Property bitKeepFooterBottom() As Boolean
            Get
                Return _bitKeepFooterBottom
            End Get
            Set(ByVal value As Boolean)
                _bitKeepFooterBottom = value
            End Set
        End Property

        Private _numRecurConfigID As Long
        Public Property RecurConfigID() As Long
            Get
                Return _numRecurConfigID
            End Get
            Set(ByVal value As Long)
                _numRecurConfigID = value
            End Set
        End Property

        Private _bitDisable As Boolean
        Public Property DisableRecur() As Boolean
            Get
                Return _bitDisable
            End Get
            Set(ByVal value As Boolean)
                _bitDisable = value
            End Set
        End Property

        Private _bitRecur As Boolean
        Public Property Recur() As Boolean
            Get
                Return _bitRecur
            End Get
            Set(ByVal value As Boolean)
                _bitRecur = value
            End Set
        End Property

        Private _dtStartDate As Date
        Public Property StartDate() As Date
            Get
                Return _dtStartDate
            End Get
            Set(ByVal value As Date)
                _dtStartDate = value
            End Set
        End Property

        Private _dtEndDate As Date
        Public Property EndDate() As Date
            Get
                Return _dtEndDate
            End Get
            Set(ByVal value As Date)
                _dtEndDate = value
            End Set
        End Property

        Private _numFrequency As Short
        Public Property Frequency() As Short
            Get
                Return _numFrequency
            End Get
            Set(ByVal value As Short)
                _numFrequency = value
            End Set
        End Property

        Private _vcFrequency As String
        Public Property FrequencyName() As String
            Get
                Return _vcFrequency
            End Get
            Set(ByVal value As String)
                _vcFrequency = value
            End Set
        End Property

        Private _numRelationship As Long
        Public Property Relationship() As Long
            Get
                Return _numRelationship
            End Get
            Set(ByVal value As Long)
                _numRelationship = value
            End Set
        End Property

        Private _numProfile As Long
        Public Property Profile() As Long
            Get
                Return _numProfile
            End Get
            Set(ByVal value As Long)
                _numProfile = value
            End Set
        End Property

        Public Property bitDisplayKitChild() As Boolean
            Get
                Return _bitDisplayKitChild
            End Get
            Set(ByVal value As Boolean)
                _bitDisplayKitChild = value
            End Set
        End Property
        Private _numSourceBizDocId As Integer
        Public Property numSourceBizDocId() As Integer
            Get
                Return _numSourceBizDocId
            End Get
            Set(ByVal value As Integer)
                _numSourceBizDocId = value
            End Set
        End Property

        Private _bitShippingBizDoc As Boolean

        Public Property bitShippingBizDoc() As Boolean
            Get
                Return _bitShippingBizDoc
            End Get
            Set(value As Boolean)
                _bitShippingBizDoc = value
            End Set
        End Property

        Private _vendorInvoiceName As String
        Public Property VendorInvoiceName() As String
            Get
                Return _vendorInvoiceName
            End Get
            Set(ByVal value As String)
                _vendorInvoiceName = value
            End Set
        End Property

        Public Property bitInvoiceForDeferred As Boolean
        Public Property AccountClass As Long
        Public Property SignatureType As Long
        Public Property ARAccountID As Long

        Public Function SaveBizDoc() As Integer
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(38) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numBizDocId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = _BizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = _OppBizDocId
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@vcComments", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(4).Value = _vcComments

                arParms(5) = New Npgsql.NpgsqlParameter("@bitPartialFulfillment", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _bitPartialShipment

                arParms(6) = New Npgsql.NpgsqlParameter("@strBizDocItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(6).Value = _strBizDocItems

                arParms(7) = New Npgsql.NpgsqlParameter("@numShipVia", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _ShipCompany

                arParms(8) = New Npgsql.NpgsqlParameter("@vcTrackingURL", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(8).Value = _vcURL

                arParms(9) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(9).Value = IIf(_FromDate = Nothing, DateTime.UtcNow, _FromDate)

                arParms(10) = New Npgsql.NpgsqlParameter("@numBizDocStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Value = _BizDocStatus

                arParms(11) = New Npgsql.NpgsqlParameter("@bitRecurringBizDoc", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = _boolRecurringBizDoc

                arParms(12) = New Npgsql.NpgsqlParameter("@numSequenceId", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(12).Value = _SequenceId

                arParms(13) = New Npgsql.NpgsqlParameter("@tintDeferred", NpgsqlTypes.NpgsqlDbType.Smallint) 'flag Will be used for Deferred : Not Create Account Entries
                arParms(13).Value = _Deferred

                arParms(14) = New Npgsql.NpgsqlParameter("@monCreditAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(14).Value = _CreditAmount
                arParms(14).Direction = ParameterDirection.InputOutput

                arParms(15) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(15).Value = _ClientTimeZoneOffset

                arParms(16) = New Npgsql.NpgsqlParameter("@monDealAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(16).Value = _DealAmount
                arParms(16).Direction = ParameterDirection.Output

                arParms(17) = New Npgsql.NpgsqlParameter("@bitRentalBizDoc", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(17).Value = _boolRentalBizDoc

                arParms(18) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = _BizDocTemplateID

                arParms(19) = New Npgsql.NpgsqlParameter("@vcTrackingNo", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(19).Value = _TrackingNo

                arParms(20) = New Npgsql.NpgsqlParameter("@vcRefOrderNo", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(20).Value = _RefOrderNo

                arParms(21) = New Npgsql.NpgsqlParameter("@OMP_SalesTaxPercent", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(21).Value = _SalesTaxRate

                arParms(22) = New Npgsql.NpgsqlParameter("@numFromOppBizDocsId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(22).Value = _FromOppBizDocsId

                arParms(23) = New Npgsql.NpgsqlParameter("@bitTakeSequenceId", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(23).Value = False

                arParms(24) = New Npgsql.NpgsqlParameter("@bitAllItems", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(24).Value = False

                arParms(25) = New Npgsql.NpgsqlParameter("@bitNotValidateBizDocFulfillment", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(25).Value = _bitNotValidateBizDocFulfillment

                arParms(26) = New Npgsql.NpgsqlParameter("@fltExchangeRateBizDoc", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(26).Value = _fltExchangeRateBizDoc

                arParms(27) = New Npgsql.NpgsqlParameter("@bitRecur", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(27).Value = _bitRecur

                arParms(28) = New Npgsql.NpgsqlParameter("@dtStartDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(28).Value = _dtStartDate

                arParms(29) = New Npgsql.NpgsqlParameter("@dtEndDate", NpgsqlTypes.NpgsqlDbType.Date)
                arParms(29).Value = _dtEndDate

                arParms(30) = New Npgsql.NpgsqlParameter("@numFrequency", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(30).Value = _numFrequency

                arParms(31) = New Npgsql.NpgsqlParameter("@vcFrequency", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(31).Value = _vcFrequency

                arParms(32) = New Npgsql.NpgsqlParameter("@numRecConfigID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(32).Value = _numRecurConfigID

                arParms(33) = New Npgsql.NpgsqlParameter("@bitDisable", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(33).Value = _bitDisable

                arParms(34) = New Npgsql.NpgsqlParameter("@bitInvoiceForDeferred", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(34).Value = bitInvoiceForDeferred

                arParms(35) = New Npgsql.NpgsqlParameter("@numSourceBizDocId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(35).Value = numSourceBizDocId

                arParms(36) = New Npgsql.NpgsqlParameter("@bitShippingBizDoc", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(36).Value = bitShippingBizDoc

                arParms(37) = New Npgsql.NpgsqlParameter("@vcVendorInvoiceName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(37).Value = VendorInvoiceName

                arParms(38) = New Npgsql.NpgsqlParameter("@numARAccountID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(38).Value = ARAccountID

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_CreateBizDocs", objParam, True)

                _OppBizDocId = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)
                _CreditAmount = CDec(DirectCast(objParam, Npgsql.NpgsqlParameter())(14).Value)
                _DealAmount = CDec(DirectCast(objParam, Npgsql.NpgsqlParameter())(16).Value)

                Return CInt(_OppBizDocId)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetTotalReceivedItemByBizDocType() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = OppItemID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetTotalReceivedItemByBizDocType", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetBizDocsByOOpId() As DataSet
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 2

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _OppBizDocId

                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _ClientTimeZoneOffset

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OppBizDocs", arParms)

                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        <Obsolete("Not used anywhere, remove it")>
        Public Function GetExtBizDocsByOOpId() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_ExtOppBizDocs", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DeleteBizDoc() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 1

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _OppBizDocId

                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _ClientTimeZoneOffset

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = UserCntID

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "USP_OppBizDocs", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetOppInItems() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPBizDocItems", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMirrorBizDocItems() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numReferenceID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ReferenceID

                arParms(1) = New Npgsql.NpgsqlParameter("@numReferenceType", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _ReferenceType

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMirrorBizDocItems", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocInventoryItems() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppBizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocInventoryItems", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingReport() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@ShippingReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _ShippingReportId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetShippingReport", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingReportList() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetShippingReportList", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingBoxes() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@ShippingReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ShippingReportId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetShippingBox", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateBizDocsViewdDtls() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocs", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                SqlDAL.ExecuteNonQuery(connString, "USP_BizDocsViewedDtls", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetOppAddressDetail() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 1

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _OppBizDocId

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _ClientTimeZoneOffset

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPGetINItems", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetOppBizDocDtl() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 2

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _OppBizDocId


                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _ClientTimeZoneOffset

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPGetINItems", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetBizDocForPackingSlip() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocForPackingSlip", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetMirrorBizDocDtls() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(11) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numReferenceID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _ReferenceID

                arParms(1) = New Npgsql.NpgsqlParameter("@numReferenceType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _ReferenceType

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _ClientTimeZoneOffset

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur4", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur5", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur6", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                arParms(11) = New Npgsql.NpgsqlParameter("@SWV_RefCur7", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(11).Value = Nothing
                arParms(11).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetMirrorBizDocDetails", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocsInOpp() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@bitAuth", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(1).Value = _IsAuthoritativeBizDoc

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _OppBizDocId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "Usp_GetBizDocsInOpp", arParms)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetBizDocsDetails() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppBizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetBizDocDetail", arParms)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetPortalBizDocs() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetPortalBizDocs", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManagePortalBizDocs() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@strFiledId", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(0).Value = _strBizDocID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_ManagePortalBizDocs", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageBizDocFooter() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@vcBizDocFooter", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(2).Value = _vcFooter

                SqlDAL.ExecuteNonQuery(connString, "USP_BizDocFooter", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        'Added By: Sachin sadhu||Date:23rdDec2013
        'Purpose:To Save different Footer Logo  for bizDocs
        Public Function SaveBizDocFooter(ByVal intDocTemplateId As Int32) As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(0).Value = intDocTemplateId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@vcBizDocFooter", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(3).Value = _vcFooter

                SqlDAL.ExecuteNonQuery(connString, "USP_BizDocTemplate_ManageFooter", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetBizDocAttachments() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numAttachmntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _BizDocAtchID

                arParms(3) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _BizDocTemplateID

                arParms(4) = New Npgsql.NpgsqlParameter("@intAddReferenceDocument", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _intAddReferenceDocument

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim dsBizData As New DataSet
                dsBizData = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocAttachmnts", arParms)
                If dsBizData.Tables.Count > 0 Then
                    Return dsBizData.Tables(0)
                Else
                    Return Nothing
                End If
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function AddBizDocAttachments() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@numBizDocAtID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _BizDocAtchID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcURL", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _vcURL

                arParms(4) = New Npgsql.NpgsqlParameter("@vcDocName", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _vcDocName

                arParms(5) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _BizDocTemplateID

                SqlDAL.ExecuteNonQuery(connString, "USP_BizDocAttachments", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function SortBizDocAttachments() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@vcOrder", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(0).Value = _vcOrder

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _BizDocId

                arParms(3) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _BizDocTemplateID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateBizDocAtch", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function DelBizDocAtchmnts() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocAtchID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BizDocAtchID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteBizDocAtch", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetOppInItemsForAuthorizativeAccounting() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId


                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPGetINItemsForAuthorizativeAccounting", arParms)
                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function GetOppInItemsForAccounting() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOppItemsForAccounting", arParms)
                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Sub DeleteComissionJournal()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppBizDocId


                SqlDAL.ExecuteScalar(connString, "DeleteComissionDetails", arParms)


            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Sub

        Public Function GetAuthorizativeOpportuntiy() As Long
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID


                ''arParms(1) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                ''arParms(1).Value = _OppType

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetAuthorizativeOpportuntiy", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetJournalIdForAuthorizativeBizDocs() As Long
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetJournalIdForAuthorizativeBizDocs", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetAuthorizativeBizDocsId() As Long
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppBizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetAuthorizativeBizDocsId", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetOpportunityType() As Int32
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CInt(SqlDAL.ExecuteScalar(connString, "USP_GetOpportunityType", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetDivisionId() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetDivisionId", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetBizDocId() As Long
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppBizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetBizDocId", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetJournalIdForAuthorizativeAccounting() As Long
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetJournalIdForAuthorizativeAccounting", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function GetTransactionDetailsForAuthorizativeBizDocs() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetJournalDetailsForAuthorizativeBizDocs", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        <Obsolete()>
        Public Function UPdateAuthorizativeAccounting() As Long
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@strText", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _strText

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateAuthorizativeBizDocs", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        <Obsolete()>
        Public Function UPdateAuthorizativeAccountingForPurchase() As Long
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@strText", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _strText

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateAuthorizativeBizDocsForPurchase", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function GetOpportunityBizDocsCount() As Long
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _OppBizDocId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetOpportunityBizDocsCount", arParms))

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function GetContractDdlList() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetContractListForBizDocs", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetOppItemsForBizDocs() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _BizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _OppId

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _OppBizDocId

                arParms(4) = New Npgsql.NpgsqlParameter("@bitRentalBizDoc", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _boolRentalBizDoc

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOppItemBizDocs", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function GetBizDocItemsForPickPackSlip(ByVal forBizDocID As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppBizDocId

                arParms(3) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = BizDocId

                arParms(4) = New Npgsql.NpgsqlParameter("@numForBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = forBizDocID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocItemsForPickPackSlip", arParms)
                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GeBizDocsByOppId(ByVal warehouseID As Long, ByVal shipToAddressID As Long, ByVal isFirstIteration As Boolean) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numBizDocTypeID", _BizDocId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseID", warehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numShipToAddressID", shipToAddressID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitFirstIteration", isFirstIteration, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetCreatePickPackSlip", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveInvoiceDtl() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppBizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@UserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@Comments", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _vcComments

                arParms(3) = New Npgsql.NpgsqlParameter("@numShipCompany", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ShipCompany

                arParms(4) = New Npgsql.NpgsqlParameter("@vcTrackingURL", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(4).Value = _vcURL

                SqlDAL.ExecuteNonQuery(connString, "USP_OppUpDateBizDocsInvoiceDetails", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageShippingReport() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(53) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numShippingReportId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Direction = ParameterDirection.InputOutput
                arParms(1).Value = _ShippingReportId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _OppBizDocId

                arParms(3) = New Npgsql.NpgsqlParameter("@numShippingCompany", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _ShipCompany

                arParms(4) = New Npgsql.NpgsqlParameter("@Value1", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = _Value1

                arParms(5) = New Npgsql.NpgsqlParameter("@Value2", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(5).Value = _Value2

                arParms(6) = New Npgsql.NpgsqlParameter("@Value3", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(6).Value = _Value3

                arParms(7) = New Npgsql.NpgsqlParameter("@Value4", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = _Value4

                arParms(8) = New Npgsql.NpgsqlParameter("@vcFromState", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _FromState

                arParms(9) = New Npgsql.NpgsqlParameter("@vcFromZip", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(9).Value = _FromZip

                arParms(10) = New Npgsql.NpgsqlParameter("@vcFromCountry", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(10).Value = _FromCountry

                arParms(11) = New Npgsql.NpgsqlParameter("@vcToState", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(11).Value = _ToState

                arParms(12) = New Npgsql.NpgsqlParameter("@vcToZip", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(12).Value = _ToZip

                arParms(13) = New Npgsql.NpgsqlParameter("@vcToCountry", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(13).Value = _ToCountry

                arParms(14) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(14).Value = UserCntID

                arParms(15) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(15).Value = _strText

                arParms(16) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(16).Value = _byteMode

                arParms(17) = New Npgsql.NpgsqlParameter("@tintPayorType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(17).Value = _PayorType

                arParms(18) = New Npgsql.NpgsqlParameter("@vcPayorAccountNo", NpgsqlTypes.NpgsqlDbType.VarChar, 20)
                arParms(18).Value = _PayorAccountNo

                arParms(19) = New Npgsql.NpgsqlParameter("@numPayorCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = CCommon.ToLong(_PayorCountryCode)

                arParms(20) = New Npgsql.NpgsqlParameter("@vcPayorZip", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(20).Value = _PayorZipCode

                arParms(21) = New Npgsql.NpgsqlParameter("@vcFromCity", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(21).Value = _FromCity

                arParms(22) = New Npgsql.NpgsqlParameter("@vcFromAddressLine1", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(22).Value = _FromAddressLine1

                arParms(23) = New Npgsql.NpgsqlParameter("@vcFromAddressLine2", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(23).Value = _FromAddressLine2

                arParms(24) = New Npgsql.NpgsqlParameter("@vcToCity", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(24).Value = _ToCity

                arParms(25) = New Npgsql.NpgsqlParameter("@vcToAddressLine1", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(25).Value = _ToAddressLine1

                arParms(26) = New Npgsql.NpgsqlParameter("@vcToAddressLine2", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(26).Value = _ToAddressLine2

                arParms(27) = New Npgsql.NpgsqlParameter("@vcFromName", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(27).Value = _strFromName

                arParms(28) = New Npgsql.NpgsqlParameter("@vcFromCompany", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(28).Value = _strFromCompany

                arParms(29) = New Npgsql.NpgsqlParameter("@vcFromPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(29).Value = _strFromPhone

                arParms(30) = New Npgsql.NpgsqlParameter("@bitFromResidential", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(30).Value = _IsFromResidential

                arParms(31) = New Npgsql.NpgsqlParameter("@vcToName", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(31).Value = _strToName

                arParms(32) = New Npgsql.NpgsqlParameter("@vcToCompany", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(32).Value = _strToCompany

                arParms(33) = New Npgsql.NpgsqlParameter("@vcToPhone", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(33).Value = _strToPhone

                arParms(34) = New Npgsql.NpgsqlParameter("@bitToResidential", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(34).Value = _IsToResidential

                arParms(35) = New Npgsql.NpgsqlParameter("@IsCOD", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(35).Value = _IsCOD

                arParms(36) = New Npgsql.NpgsqlParameter("@IsDryIce", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(36).Value = _IsDryIce

                arParms(37) = New Npgsql.NpgsqlParameter("@IsHoldSaturday", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(37).Value = _IsHoldSaturday

                arParms(38) = New Npgsql.NpgsqlParameter("@IsHomeDelivery", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(38).Value = _IsHomeDelivery

                arParms(39) = New Npgsql.NpgsqlParameter("@IsInsideDelevery", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(39).Value = _IsInsideDelivery

                arParms(40) = New Npgsql.NpgsqlParameter("@IsInsidePickup", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(40).Value = _IsInsidePickup

                arParms(41) = New Npgsql.NpgsqlParameter("@IsReturnShipment", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(41).Value = _IsReturnShipment

                arParms(42) = New Npgsql.NpgsqlParameter("@IsSaturdayDelivery", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(42).Value = _IsSaturdayDelivery

                arParms(43) = New Npgsql.NpgsqlParameter("@IsSaturdayPickup", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(43).Value = _IsSaturdayPickup

                arParms(44) = New Npgsql.NpgsqlParameter("@numCODAmount", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(44).Value = IIf(_CODAmount = "" OrElse _CODAmount Is Nothing, 0, CCommon.ToDouble(_CODAmount))

                arParms(45) = New Npgsql.NpgsqlParameter("@vcCODType", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(45).Value = _strCODType

                arParms(46) = New Npgsql.NpgsqlParameter("@numTotalInsuredValue", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(46).Value = _dblTotalInsuredValue

                arParms(47) = New Npgsql.NpgsqlParameter("@IsAdditionalHandling", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(47).Value = _IsAdditionalHandling

                arParms(48) = New Npgsql.NpgsqlParameter("@IsLargePackage", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(48).Value = _IsLargePackage

                arParms(49) = New Npgsql.NpgsqlParameter("@vcDeliveryConfirmation", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(49).Value = _strDeliveryConfirmation

                arParms(50) = New Npgsql.NpgsqlParameter("@vcDescription", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(50).Value = _strDescription

                arParms(51) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(51).Value = _OppId

                arParms(52) = New Npgsql.NpgsqlParameter("@numTotalCustomsValue", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(52).Value = _dblTotalCustomsValue

                arParms(53) = New Npgsql.NpgsqlParameter("@tintSignatureType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(53).Value = SignatureType

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageShippingReport", objParam, True)
                _ShippingReportId = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(1).Value)

                Return ShippingReportId
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        <Obsolete("Use DeleteShippingBox instead")>
        Public Function DeleteShippingItem() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@ShippingReportItemId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ShippingReportItemId

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteShippingReportItem", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteShippingBox() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBoxID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BoxID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteShippingBox", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteShippingReport() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@ShippingReportId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ShippingReportId

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteShippingReport", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageShippingLabel() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strText

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageShippingLabel", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Public Function AddShippingLabel() As Boolean
        '    Try
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString

        '        arParms(0) = New Npgsql.NpgsqlParameter("@ShippingReportItemId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
        '        arParms(0).Value = _ShippingReportItemId

        '        arParms(1) = New Npgsql.NpgsqlParameter("@vcShippingLabelImage", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
        '        arParms(1).Value = _ShippingLableImage

        '        arParms(2) = New Npgsql.NpgsqlParameter("@vcTrackingNumber", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
        '        arParms(2).Value = _TrackingNumber

        '        arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
        '        arParms(3).Value = UserCntID

        '        SqlDAL.ExecuteNonQuery(connString, "USP_AddShippingLabel", arParms)
        '        Return True
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        'Public Function GetShipReportFieldValues() As DataTable
        '    Try
        '        Dim ds As DataSet
        '        Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
        '        Dim getconnection As New GetConnection
        '        Dim connString As String = getconnection.GetConnectionString

        '        arParms(0) = New Npgsql.NpgsqlParameter("@ShippingReportId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
        '        arParms(0).Value = _ShippingReportId

        '        arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
        '        arParms(1).Value = DomainId

        '        ds = SqlDAL.ExecuteDataset(connString, "USP_GetShipReportFieldValues", arParms)
        '        Return ds.Tables(0)
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        'Used from FedEx Screen to update final shipping cost

        Public Function UpdateBizDocsShippingFields() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppBizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@numShippingReportId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ShippingReportId

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateBizDocsShippingCost", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function UpdateShippingItemPrice() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _Price

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateShippingItemPrice", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetShippingLabelList() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numBoxID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _BoxID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcOrderIds", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = _strOrderIDs

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(3).Value = _tintMode

                arParms(4) = New Npgsql.NpgsqlParameter("@vcOppBizDocIds", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = _strOppBizDocIds

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GETShippingLabels", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Used to get FedEx Shipping States.- In future we Can add parameter For get States by Shipping Service provider
        Public Function GetShippingAbbreviation() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numShipFromState", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ShipFromState

                arParms(1) = New Npgsql.NpgsqlParameter("@numShipFromCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ShipFromCountry


                arParms(2) = New Npgsql.NpgsqlParameter("@numShipToState", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ShipToState

                arParms(3) = New Npgsql.NpgsqlParameter("@numShipToCountry", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _ShipToCountry


                arParms(4) = New Npgsql.NpgsqlParameter("@vcFromState", NpgsqlTypes.NpgsqlDbType.VarChar, 5)
                arParms(4).Direction = ParameterDirection.Output
                arParms(4).Value = _strShipFromState


                arParms(5) = New Npgsql.NpgsqlParameter("@vcFromCountry", NpgsqlTypes.NpgsqlDbType.VarChar, 5)
                arParms(5).Direction = ParameterDirection.Output
                arParms(5).Value = _strShipFromCountry

                arParms(6) = New Npgsql.NpgsqlParameter("@vcToState", NpgsqlTypes.NpgsqlDbType.VarChar, 5)
                arParms(6).Direction = ParameterDirection.Output
                arParms(6).Value = _strShipToState

                arParms(7) = New Npgsql.NpgsqlParameter("@vcToCountry", NpgsqlTypes.NpgsqlDbType.VarChar, 5)
                arParms(7).Direction = ParameterDirection.Output
                arParms(7).Value = _strShipToCountry

                arParms(8) = New Npgsql.NpgsqlParameter("@numShipCompany", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _ShipCompany

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_GetShippingAbbreviation", objParam, True)

                'Set value only when abbrivation found, Because Artison Medical(domainid-134) already renamed their state name to state code
                If CCommon.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value) <> "" Then
                    _strShipFromState = DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value.ToString
                End If
                If CCommon.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value) <> "" Then
                    _strShipFromCountry = DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value.ToString
                End If
                If CCommon.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value) <> "" Then
                    _strShipToState = DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value.ToString
                End If
                If CCommon.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value) <> "" Then
                    _strShipToCountry = DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value.ToString
                End If


                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageShippingBox() As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numShippingReportId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ShippingReportId

                arParms(1) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strText

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@numBoxID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Direction = ParameterDirection.InputOutput
                arParms(4).Value = _BoxID

                arParms(5) = New Npgsql.NpgsqlParameter("@PageMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = If(_intPageMode = Nothing, 0, _intPageMode)

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageShippingBox", objParam, True)
                _BoxID = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(4).Value)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ValidateCustomerAR_APAccounts(ByVal Account As String, ByVal DomainID As Long, ByVal DivisionID As Long) As Long
            Try
                'Validate if AR Account is mapped from MasterList Admin ->RelationShip Mapping(Button)
                Dim objCOA As New Accounting.ChartOfAccounting
                Dim ds As DataSet
                With objCOA
                    .COARelationshipID = 0
                    .DivisionID = DivisionID
                    .DomainID = DomainID
                    ds = .GetCOARelationship()
                End With
                If ds.Tables(0).Rows.Count > 0 Then
                    Select Case Account
                        Case "AR"
                            Return CCommon.ToLong(ds.Tables(0).Rows(0).Item("numARAccountId"))
                        Case "AP"
                            Return CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAPAccountId"))
                        Case Else
                            Return CCommon.ToLong(ds.Tables(0).Rows(0).Item("numARAccountId"))
                    End Select
                Else
                    Return 0
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Validate if Relationship/Division is mapped to AR and AP Accounts, if not then show message to set
        Public Function ValidateARAP(ByVal DivisionID As Long, ByVal RelationshipID As Long, ByVal DomainID As Long) As Boolean
            Try
                Dim objCOA As New Accounting.ChartOfAccounting
                Dim ds As DataSet
                If DivisionID > 0 Then
                    With objCOA
                        .COARelationshipID = 0
                        .DivisionID = DivisionID
                        .DomainID = DomainID
                        ds = .GetCOARelationship()
                    End With
                ElseIf RelationshipID > 0 Then
                    With objCOA
                        .COARelationshipID = 0
                        .RelationshipID = RelationshipID
                        .DomainID = DomainID
                        ds = .GetCOARelationship()
                    End With
                End If

                'Changed by sandeep to handle null dataset
                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    If Not (CCommon.ToLong(ds.Tables(0).Rows(0).Item("numARAccountId")) > 0 And CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAPAccountId")) > 0) Then
                        Return False
                    End If
                Else
                    Return False
                End If
                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Public Function AddItemToExistingInvoice() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@numUnitHour", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(3).Value = _UnitHour

                arParms(4) = New Npgsql.NpgsqlParameter("@monPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(4).Value = _Price

                arParms(5) = New Npgsql.NpgsqlParameter("@vcItemDesc", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(5).Value = _ItemDesc

                'arParms(6) = New Npgsql.NpgsqlParameter("@vcNotes", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                'arParms(6).Value = _Notes

                arParms(6) = New Npgsql.NpgsqlParameter("@numCategoryHDRID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _CategoryHDRID

                arParms(7) = New Npgsql.NpgsqlParameter("@numProId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _ProID

                arParms(8) = New Npgsql.NpgsqlParameter("@numStageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _StageId

                arParms(9) = New Npgsql.NpgsqlParameter("@numClassID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _ClassId

                SqlDAL.ExecuteNonQuery(connString, "USP_AddItemToExistingInvoice", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdateQtyShipped() As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numQtyShipped", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(0).Value = _UnitHour

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppItemID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@vcError", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = ""

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateQtyShipped", objParam, True)

                Return CCommon.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Shared Sub LoadServiceTypes(ByVal lngShippingCompany As Long, ByRef ddl As DropDownList, Optional ByVal DefaultShipCompany As Integer = 91)
            Try
                ' Added logic to bind with Shipping rules if any(Priya 28 March 2018)
                Dim objShippingRule As New BACRM.BusinessLogic.ShioppingCart.ShippingRule
                objShippingRule.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                Dim dt As DataTable = objShippingRule.EstimateShipping()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If lngShippingCompany = 91 Then 'FedEx
                        If dt.Select("numShippingCompanyID=91").Length > 0 Then
                            Dim arrRows As DataRow() = dt.Select("numShippingCompanyID=91")
                            For Each row As DataRow In arrRows
                                ddl.Items.Add(New ListItem(CCommon.ToString(row("vcServiceName")), CCommon.ToString(row("intNsoftEnum"))))
                            Next
                        Else
                            ddl.Items.Add(New ListItem("FedEx Priority Overnight", "10"))
                            ddl.Items.Add(New ListItem("FedEx Standard Overnight", "11"))
                            ddl.Items.Add(New ListItem("FedEx Overnight", "12"))
                            ddl.Items.Add(New ListItem("FedEx 2nd Day", "13"))
                            ddl.Items.Add(New ListItem("FedEx Express Saver", "14"))
                            ddl.Items.Add(New ListItem("FedEx Ground", "15"))
                            ddl.Items.Add(New ListItem("FedEx Ground Home Delivery", "16"))
                            ddl.Items.Add(New ListItem("FedEx 1 Day Freight", "17"))
                            ddl.Items.Add(New ListItem("FedEx 2 Day Freight", "18"))
                            ddl.Items.Add(New ListItem("FedEx 3 Day Freight", "19"))
                            ddl.Items.Add(New ListItem("FedEx International Priority", "20"))
                            ddl.Items.Add(New ListItem("FedEx International Priority Distribution", "21"))
                            ddl.Items.Add(New ListItem("FedEx International Economy", "22"))
                            ddl.Items.Add(New ListItem("FedEx International Economy Distribution", "23"))
                            ddl.Items.Add(New ListItem("FedEx International First", "24"))
                            ddl.Items.Add(New ListItem("FedEx International Priority Freight", "25"))
                            ddl.Items.Add(New ListItem("FedEx International Economy Freight", "26"))
                            ddl.Items.Add(New ListItem("FedEx International Distribution Freight", "27"))
                            ddl.Items.Add(New ListItem("FedEx Europe International Priority", "28"))
                        End If

                        ddl.Items.Add(New ListItem("Set Manually", "-1"))
                    ElseIf lngShippingCompany = 90 Then 'USPS
                        If dt.Select("numShippingCompanyID=90").Length > 0 Then
                            Dim arrRows As DataRow() = dt.Select("numShippingCompanyID=90")
                            For Each row As DataRow In arrRows
                                ddl.Items.Add(New ListItem(CCommon.ToString(row("vcServiceName")), CCommon.ToString(row("intNsoftEnum"))))
                            Next
                        Else
                            ddl.Items.Add(New ListItem("USPS Express", "70"))
                            ddl.Items.Add(New ListItem("USPS First Class", "71"))
                            ddl.Items.Add(New ListItem("USPS Priority", "72"))
                            ddl.Items.Add(New ListItem("USPS Parcel Post", "73"))
                            ddl.Items.Add(New ListItem("USPS Bound Printed Matter", "74"))
                            ddl.Items.Add(New ListItem("USPS Media", "75"))
                            ddl.Items.Add(New ListItem("USPS Library", "76"))
                        End If

                        ddl.Items.Add(New ListItem("Set Manually", "-1"))
                    ElseIf lngShippingCompany = 88 Then 'UPS
                        If dt.Select("numShippingCompanyID=88").Length > 0 Then
                            Dim arrRows As DataRow() = dt.Select("numShippingCompanyID=88")
                            For Each row As DataRow In arrRows
                                ddl.Items.Add(New ListItem(CCommon.ToString(row("vcServiceName")), CCommon.ToString(row("intNsoftEnum"))))
                            Next
                        Else
                            ddl.Items.Add(New ListItem("UPS Next Day Air", "40"))
                            ddl.Items.Add(New ListItem("UPS 2nd Day Air", "42"))
                            ddl.Items.Add(New ListItem("UPS Ground", "43"))
                            ddl.Items.Add(New ListItem("UPS 3Day Select", "48"))
                            ddl.Items.Add(New ListItem("UPS Next Day Air Saver", "49"))
                            ddl.Items.Add(New ListItem("UPS Saver", "50"))
                            ddl.Items.Add(New ListItem("UPS Next Day Air Early A.M.", "51"))
                            ddl.Items.Add(New ListItem("UPS 2nd Day Air AM", "55"))
                        End If

                        ddl.Items.Add(New ListItem("Set Manually", "-1"))
                    Else
                        For Each row As DataRow In dt.Rows
                            ddl.Items.Add(New ListItem(CCommon.ToString(row("vcServiceName")), CCommon.ToString(row("intNsoftEnum"))))
                        Next

                        ddl.Items.Add(New ListItem("Set Manually", "-1"))
                    End If
                Else
                    Select Case lngShippingCompany
                        Case 91 'FedEx
                            ddl.Items.Add(New ListItem("FedEx Priority Overnight", "10"))
                            ddl.Items.Add(New ListItem("FedEx Standard Overnight", "11"))
                            ddl.Items.Add(New ListItem("FedEx Overnight", "12"))
                            ddl.Items.Add(New ListItem("FedEx 2nd Day", "13"))
                            ddl.Items.Add(New ListItem("FedEx Express Saver", "14"))
                            ddl.Items.Add(New ListItem("FedEx Ground", "15"))
                            ddl.Items.Add(New ListItem("FedEx Ground Home Delivery", "16"))
                            ddl.Items.Add(New ListItem("FedEx 1 Day Freight", "17"))
                            ddl.Items.Add(New ListItem("FedEx 2 Day Freight", "18"))
                            ddl.Items.Add(New ListItem("FedEx 3 Day Freight", "19"))
                            ddl.Items.Add(New ListItem("FedEx International Priority", "20"))
                            ddl.Items.Add(New ListItem("FedEx International Priority Distribution", "21"))
                            ddl.Items.Add(New ListItem("FedEx International Economy", "22"))
                            ddl.Items.Add(New ListItem("FedEx International Economy Distribution", "23"))
                            ddl.Items.Add(New ListItem("FedEx International First", "24"))
                            ddl.Items.Add(New ListItem("FedEx International Priority Freight", "25"))
                            ddl.Items.Add(New ListItem("FedEx International Economy Freight", "26"))
                            ddl.Items.Add(New ListItem("FedEx International Distribution Freight", "27"))
                            ddl.Items.Add(New ListItem("FedEx Europe International Priority", "28"))
                            ddl.Items.Add(New ListItem("Set Manually", "-1"))
                            ddl.Items.FindByValue("15").Selected = True
                        Case 90 'USPS
                            ddl.Items.Add(New ListItem("USPS Express", "70"))
                            ddl.Items.Add(New ListItem("USPS First Class", "71"))
                            ddl.Items.Add(New ListItem("USPS Priority", "72"))
                            ddl.Items.Add(New ListItem("USPS Parcel Post", "73"))
                            ddl.Items.Add(New ListItem("USPS Bound Printed Matter", "74"))
                            ddl.Items.Add(New ListItem("USPS Media", "75"))
                            ddl.Items.Add(New ListItem("USPS Library", "76"))
                            ddl.Items.Add(New ListItem("Set Manually", "-1"))
                            ddl.Items.FindByValue("72").Selected = True
                        Case 88 'UPS
                            ddl.Items.Add(New ListItem("UPS Next Day Air", "40"))
                            ddl.Items.Add(New ListItem("UPS 2nd Day Air", "42"))
                            ddl.Items.Add(New ListItem("UPS Ground", "43"))
                            ddl.Items.Add(New ListItem("UPS 3Day Select", "48"))
                            ddl.Items.Add(New ListItem("UPS Next Day Air Saver", "49"))
                            ddl.Items.Add(New ListItem("UPS Saver", "50"))
                            ddl.Items.Add(New ListItem("UPS Next Day Air Early A.M.", "51"))
                            ddl.Items.Add(New ListItem("UPS 2nd Day Air AM", "55"))
                            ddl.Items.Add(New ListItem("Set Manually", "-1"))
                            ddl.Items.FindByValue("43").Selected = True
                        Case Else
                            ddl.Items.Add(New ListItem("FedEx Priority Overnight", "10"))
                            ddl.Items.Add(New ListItem("FedEx Standard Overnight", "11"))
                            ddl.Items.Add(New ListItem("FedEx Overnight", "12"))
                            ddl.Items.Add(New ListItem("FedEx 2nd Day", "13"))
                            ddl.Items.Add(New ListItem("FedEx Express Saver", "14"))
                            ddl.Items.Add(New ListItem("FedEx Ground", "15"))
                            ddl.Items.Add(New ListItem("FedEx Ground Home Delivery", "16"))
                            ddl.Items.Add(New ListItem("FedEx 1 Day Freight", "17"))
                            ddl.Items.Add(New ListItem("FedEx 2 Day Freight", "18"))
                            ddl.Items.Add(New ListItem("FedEx 3 Day Freight", "19"))
                            ddl.Items.Add(New ListItem("FedEx International Priority", "20"))
                            ddl.Items.Add(New ListItem("FedEx International Priority Distribution", "21"))
                            ddl.Items.Add(New ListItem("FedEx International Economy", "22"))
                            ddl.Items.Add(New ListItem("FedEx International Economy Distribution", "23"))
                            ddl.Items.Add(New ListItem("FedEx International First", "24"))
                            ddl.Items.Add(New ListItem("FedEx International Priority Freight", "25"))
                            ddl.Items.Add(New ListItem("FedEx International Economy Freight", "26"))
                            ddl.Items.Add(New ListItem("FedEx International Distribution Freight", "27"))
                            ddl.Items.Add(New ListItem("FedEx Europe International Priority", "28"))

                            ddl.Items.Add(New ListItem("UPS Next Day Air", "40"))
                            ddl.Items.Add(New ListItem("UPS 2nd Day Air", "42"))
                            ddl.Items.Add(New ListItem("UPS Ground", "43"))
                            ddl.Items.Add(New ListItem("UPS 3Day Select", "48"))
                            ddl.Items.Add(New ListItem("UPS Next Day Air Saver", "49"))
                            ddl.Items.Add(New ListItem("UPS Saver", "50"))
                            ddl.Items.Add(New ListItem("UPS Next Day Air Early A.M.", "51"))
                            ddl.Items.Add(New ListItem("UPS 2nd Day Air AM", "55"))

                            ddl.Items.Add(New ListItem("USPS Express", "70"))
                            ddl.Items.Add(New ListItem("USPS First Class", "71"))
                            ddl.Items.Add(New ListItem("USPS Priority", "72"))
                            ddl.Items.Add(New ListItem("USPS Parcel Post", "73"))
                            ddl.Items.Add(New ListItem("USPS Bound Printed Matter", "74"))
                            ddl.Items.Add(New ListItem("USPS Media", "75"))
                            ddl.Items.Add(New ListItem("USPS Library", "76"))

                            ddl.Items.Add(New ListItem("Set Manually", "-1"))
                    End Select
                End If

                'If DefaultShipCompany = 91 AndAlso Not ddl.Items.FindByValue("15") Is Nothing Then
                '    ddl.Items.FindByValue("15").Selected = True
                'ElseIf DefaultShipCompany = 88 AndAlso Not ddl.Items.FindByValue("43") Is Nothing Then
                '    ddl.Items.FindByValue("43").Selected = True
                'ElseIf DefaultShipCompany = 90 AndAlso Not ddl.Items.FindByValue("72") Is Nothing Then
                '    ddl.Items.FindByValue("72").Selected = True
                'Else
                If ddl.Items.FindByValue(CCommon.ToString(DefaultShipCompany)) IsNot Nothing Then
                    ddl.Items.FindByValue(CCommon.ToString(DefaultShipCompany)).Selected = True
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub LoadServiceTypes(ByVal lngShippingCompany As Long, ByRef ddl As RadComboBox, Optional ByVal DefaultShipCompany As Integer = 91)
            Try
                Dim objShippingRule As New BACRM.BusinessLogic.ShioppingCart.ShippingRule
                objShippingRule.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                Dim dt As DataTable = objShippingRule.EstimateShipping()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If lngShippingCompany = 91 Then 'FedEx
                        If dt.Select("numShippingCompanyID=91").Length > 0 Then
                            Dim arrRows As DataRow() = dt.Select("numShippingCompanyID=91")
                            For Each row As DataRow In arrRows
                                ddl.Items.Add(New RadComboBoxItem(CCommon.ToString(row("vcServiceName")), CCommon.ToString(row("intNsoftEnum"))))
                            Next
                        Else
                            ddl.Items.Add(New RadComboBoxItem("FedEx Priority Overnight", "10"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Standard Overnight", "11"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Overnight", "12"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 2nd Day", "13"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Express Saver", "14"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Ground", "15"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Ground Home Delivery", "16"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 1 Day Freight", "17"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 2 Day Freight", "18"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 3 Day Freight", "19"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Priority", "20"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Priority Distribution", "21"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Economy", "22"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Economy Distribution", "23"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International First", "24"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Priority Freight", "25"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Economy Freight", "26"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Distribution Freight", "27"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Europe International Priority", "28"))
                        End If

                        ddl.Items.Add(New RadComboBoxItem("Set Manually", "-1"))
                    ElseIf lngShippingCompany = 90 Then 'USPS
                        If dt.Select("numShippingCompanyID=90").Length > 0 Then
                            Dim arrRows As DataRow() = dt.Select("numShippingCompanyID=90")
                            For Each row As DataRow In arrRows
                                ddl.Items.Add(New RadComboBoxItem(CCommon.ToString(row("vcServiceName")), CCommon.ToString(row("intNsoftEnum"))))
                            Next
                        Else
                            ddl.Items.Add(New RadComboBoxItem("USPS Express", "70"))
                            ddl.Items.Add(New RadComboBoxItem("USPS First Class", "71"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Priority", "72"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Parcel Post", "73"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Bound Printed Matter", "74"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Media", "75"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Library", "76"))
                        End If

                        ddl.Items.Add(New RadComboBoxItem("Set Manually", "-1"))
                    ElseIf lngShippingCompany = 88 Then 'UPS
                        If dt.Select("numShippingCompanyID=88").Length > 0 Then
                            Dim arrRows As DataRow() = dt.Select("numShippingCompanyID=88")
                            For Each row As DataRow In arrRows
                                ddl.Items.Add(New RadComboBoxItem(CCommon.ToString(row("vcServiceName")), CCommon.ToString(row("intNsoftEnum"))))
                            Next
                        Else
                            ddl.Items.Add(New RadComboBoxItem("UPS Next Day Air", "40"))
                            ddl.Items.Add(New RadComboBoxItem("UPS 2nd Day Air", "42"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Ground", "43"))
                            ddl.Items.Add(New RadComboBoxItem("UPS 3Day Select", "48"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Next Day Air Saver", "49"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Saver", "50"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Next Day Air Early A.M.", "51"))
                            ddl.Items.Add(New RadComboBoxItem("UPS 2nd Day Air AM", "55"))
                        End If

                        ddl.Items.Add(New RadComboBoxItem("Set Manually", "-1"))
                    Else
                        For Each row As DataRow In dt.Rows
                            ddl.Items.Add(New RadComboBoxItem(CCommon.ToString(row("vcServiceName")), CCommon.ToString(row("intNsoftEnum"))))
                        Next

                        ddl.Items.Add(New RadComboBoxItem("Set Manually", "-1"))
                    End If
                Else
                    Select Case lngShippingCompany
                        Case 91 'FedEx
                            ddl.Items.Add(New RadComboBoxItem("FedEx Priority Overnight", "10"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Standard Overnight", "11"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Overnight", "12"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 2nd Day", "13"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Express Saver", "14"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Ground", "15"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Ground Home Delivery", "16"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 1 Day Freight", "17"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 2 Day Freight", "18"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 3 Day Freight", "19"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Priority", "20"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Priority Distribution", "21"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Economy", "22"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Economy Distribution", "23"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International First", "24"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Priority Freight", "25"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Economy Freight", "26"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Distribution Freight", "27"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Europe International Priority", "28"))
                            ddl.Items.Add(New RadComboBoxItem("Set Manually", "-1"))
                            ddl.Items.FindItemByValue("15").Selected = True
                        Case 90 'USPS
                            ddl.Items.Add(New RadComboBoxItem("USPS Express", "70"))
                            ddl.Items.Add(New RadComboBoxItem("USPS First Class", "71"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Priority", "72"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Parcel Post", "73"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Bound Printed Matter", "74"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Media", "75"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Library", "76"))
                            ddl.Items.Add(New RadComboBoxItem("Set Manually", "-1"))
                            ddl.Items.FindItemByValue("72").Selected = True
                        Case 88 'UPS
                            ddl.Items.Add(New RadComboBoxItem("UPS Next Day Air", "40"))
                            ddl.Items.Add(New RadComboBoxItem("UPS 2nd Day Air", "42"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Ground", "43"))
                            ddl.Items.Add(New RadComboBoxItem("UPS 3Day Select", "48"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Next Day Air Saver", "49"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Saver", "50"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Next Day Air Early A.M.", "51"))
                            ddl.Items.Add(New RadComboBoxItem("UPS 2nd Day Air AM", "55"))
                            ddl.Items.Add(New RadComboBoxItem("Set Manually", "-1"))
                            ddl.Items.FindItemByValue("43").Selected = True
                        Case Else
                            ddl.Items.Add(New RadComboBoxItem("FedEx Priority Overnight", "10"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Standard Overnight", "11"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Overnight", "12"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 2nd Day", "13"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Express Saver", "14"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Ground", "15"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Ground Home Delivery", "16"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 1 Day Freight", "17"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 2 Day Freight", "18"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx 3 Day Freight", "19"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Priority", "20"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Priority Distribution", "21"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Economy", "22"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Economy Distribution", "23"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International First", "24"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Priority Freight", "25"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Economy Freight", "26"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx International Distribution Freight", "27"))
                            ddl.Items.Add(New RadComboBoxItem("FedEx Europe International Priority", "28"))

                            ddl.Items.Add(New RadComboBoxItem("UPS Next Day Air", "40"))
                            ddl.Items.Add(New RadComboBoxItem("UPS 2nd Day Air", "42"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Ground", "43"))
                            ddl.Items.Add(New RadComboBoxItem("UPS 3Day Select", "48"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Next Day Air Saver", "49"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Saver", "50"))
                            ddl.Items.Add(New RadComboBoxItem("UPS Next Day Air Early A.M.", "51"))
                            ddl.Items.Add(New RadComboBoxItem("UPS 2nd Day Air AM", "55"))

                            ddl.Items.Add(New RadComboBoxItem("USPS Express", "70"))
                            ddl.Items.Add(New RadComboBoxItem("USPS First Class", "71"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Priority", "72"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Parcel Post", "73"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Bound Printed Matter", "74"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Media", "75"))
                            ddl.Items.Add(New RadComboBoxItem("USPS Library", "76"))

                            ddl.Items.Add(New RadComboBoxItem("Set Manually", "-1"))
                    End Select
                End If

                If DefaultShipCompany = 91 AndAlso Not ddl.Items.FindItemByValue("15") Is Nothing Then
                    ddl.Items.FindItemByValue("15").Selected = True
                ElseIf DefaultShipCompany = 88 AndAlso Not ddl.Items.FindItemByValue("43") Is Nothing Then
                    ddl.Items.FindItemByValue("43").Selected = True
                ElseIf DefaultShipCompany = 90 AndAlso Not ddl.Items.FindItemByValue("72") Is Nothing Then
                    ddl.Items.FindItemByValue("72").Selected = True
                ElseIf ddl.Items.FindItemByValue(CCommon.ToString(DefaultShipCompany)) IsNot Nothing Then
                    ddl.Items.FindItemByValue(CCommon.ToString(DefaultShipCompany)).Selected = True
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Sub LoadPackagingType(ByVal lngShippingCompany As Long, ByRef ddl As DropDownList, Optional ByVal DefaultShipCompany As Integer = 91, Optional ByVal DomainId As Long = 0)
            Try
                Dim isEndicia As Boolean = False

                'First check if USPS is configured for Endicia
                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                objItem.DomainID = DomainId
                objItem.ShippingCMPID = 90
                Dim dsShippingDtl As DataSet = objItem.ShippingDtls()

                If Not dsShippingDtl Is Nothing AndAlso dsShippingDtl.Tables.Count > 0 AndAlso dsShippingDtl.Tables(0).Rows.Count > 0 Then
                    If dsShippingDtl.Tables(0).Select("intShipFieldID=21").Length > 0 Then
                        Dim drEndicia As DataRow = dsShippingDtl.Tables(0).Select("intShipFieldID=21")(0)

                        If CCommon.ToString(drEndicia("vcShipFieldValue")) = "1" Then
                            isEndicia = True
                        End If
                    End If
                End If

                If lngShippingCompany = 88 Or lngShippingCompany = 90 Or lngShippingCompany = 91 Or lngShippingCompany = 0 Then
                    Dim objShippingPackageType As New BACRM.BusinessLogic.ItemShipping.ShippingPackageType
                    objShippingPackageType.ShippingCompanyID = lngShippingCompany
                    objShippingPackageType.IsEndicia = If(lngShippingCompany = 90 AndAlso isEndicia, True, False)
                    Dim dtPackageType As DataTable = objShippingPackageType.GetShippingPackageTypeByShippingCompany()

                    If Not dtPackageType Is Nothing AndAlso dtPackageType.Rows.Count > 0 Then
                        ddl.DataSource = dtPackageType
                        ddl.DataTextField = "vcPackageName"
                        ddl.DataValueField = "ID"
                        ddl.DataBind()
                    Else
                        ddl.Items.Add(New ListItem("None", "0"))
                    End If
                Else
                    ddl.Items.Add(New ListItem("None", "0"))
                End If

                If lngShippingCompany = 91 Or DefaultShipCompany = 91 Then
                    If Not ddl.Items.FindByValue("3") Is Nothing Then
                        ddl.Items.FindByValue("3").Selected = True
                    ElseIf Not ddl.Items.FindByValue("7") Is Nothing Then
                        ddl.Items.FindByValue("7").Selected = True
                    End If
                ElseIf DefaultShipCompany = 88 Then
                    If Not ddl.Items.FindByValue("11") Is Nothing Then
                        ddl.Items.FindByValue("11").Selected = True
                    ElseIf Not ddl.Items.FindByValue("19") Is Nothing Then
                        ddl.Items.FindByValue("19").Selected = True
                    End If
                ElseIf DefaultShipCompany = 90 Then
                    If Not ddl.Items.FindByValue("18") Is Nothing Then
                        ddl.Items.FindByValue("18").Selected = True
                    ElseIf Not ddl.Items.FindByValue("19") Is Nothing Then
                        ddl.Items.FindByValue("19").Selected = True
                    ElseIf Not ddl.Items.FindByValue("50") Is Nothing Then
                        ddl.Items.FindByValue("50").Selected = True
                    ElseIf Not ddl.Items.FindByValue("69") Is Nothing Then
                        ddl.Items.FindByValue("69").Selected = True
                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Shared Function LoadAllPackages(Optional ByRef DomainID As Long = 0) As DataSet
            Dim dsResult As DataSet
            Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                arParms(0) = New Npgsql.NpgsqlParameter("@DomainId", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = DomainID
                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                dsResult = SqlDAL.ExecuteDataset(connString, "USP_GET_PACKAGES", arParms)

            Catch ex As Exception
                dsResult = Nothing
                Throw ex
            End Try

            Return dsResult

        End Function

        Public Sub UpdateDueDate()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@dtDueDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(0).Value = _DueDate

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppId

                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateDueDate", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Public Function SaveDataToHeader(ByVal GrandTotal As Decimal, Optional ByVal lngJournalId As Long = 0) As Long
        '    Try
        '        Dim lntJournalId As Long
        '        Dim objAuthoritativeBizDocs As New Accounting.JournalEntry

        '        With objAuthoritativeBizDocs
        '            .Amount = GrandTotal
        '            .Entry_Date = Date.UtcNow
        '            .DomainID = DomainID
        '            .JournalId = CInt(lngJournalId)
        '            .UserCntID = UserCntID
        '            .OppBIzDocID = OppBizDocId
        '            .OppId = _OppId
        '            lntJournalId = .SaveDataToJournalEntryHeader()
        '            Return lntJournalId
        '        End With
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Public Function SaveDataToHeader(ByVal GrandTotal As Decimal, ByVal Entry_Date As DateTime, Optional ByVal lngJournalId As Long = 0, Optional ByVal Description As String = "") As Long
            Try
                Dim objJEHeader As New Accounting.JournalEntryHeader
                With objJEHeader
                    .JournalId = CInt(lngJournalId)
                    .RecurringId = 0
                    .EntryDate = CDate(CDate(IIf(Entry_Date = Nothing, Date.UtcNow, Entry_Date)).Date & " 12:00:00")
                    .Description = Description
                    .Amount = GrandTotal
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0


                    .OppId = _OppId
                    .OppBizDocsId = OppBizDocId
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = False
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = UserCntID
                    .DomainID = DomainID
                End With
                lngJournalId = objJEHeader.Save()
                Return lngJournalId
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageBizDocTemplate() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(16) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _BizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppType", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OppType

                arParms(3) = New Npgsql.NpgsqlParameter("@txtBizDocTemplate", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(3).Value = _strText

                arParms(4) = New Npgsql.NpgsqlParameter("@txtCSS", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _BizDocCSS

                arParms(5) = New Npgsql.NpgsqlParameter("@bitEnabled", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = If(_byteMode = 1, True, False)

                arParms(6) = New Npgsql.NpgsqlParameter("@tintTemplateType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(6).Value = _TemplateType

                arParms(7) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = _BizDocTemplateID

                arParms(8) = New Npgsql.NpgsqlParameter("@vcTemplateName", NpgsqlTypes.NpgsqlDbType.Text, 50)
                arParms(8).Value = _TemplateName

                arParms(9) = New Npgsql.NpgsqlParameter("@bitDefault", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = _boolDefault

                arParms(10) = New Npgsql.NpgsqlParameter("@numOrientation", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _numOrientation

                arParms(11) = New Npgsql.NpgsqlParameter("@bitKeepFooterBottom", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(11).Value = _bitKeepFooterBottom

                arParms(12) = New Npgsql.NpgsqlParameter("@numRelationship", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _numRelationship

                arParms(13) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _numProfile

                arParms(14) = New Npgsql.NpgsqlParameter("@bitDisplayKitChild", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _bitDisplayKitChild

                arParms(15) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = AccountClass

                arParms(16) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(16).Value = Nothing
                arParms(16).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageBizDocTemplate", arParms)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocTemplate() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintTemplateType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _TemplateType

                arParms(2) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _BizDocTemplateID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocTemplate", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocTemplateList() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _BizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _OppType

                arParms(3) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@numRelationship", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _numRelationship

                arParms(5) = New Npgsql.NpgsqlParameter("@numProfile", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _numProfile

                arParms(6) = New Npgsql.NpgsqlParameter("@numAccountClass", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = AccountClass

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocTemplateList", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Added By:Sachin Sadhu||Date:11thmarch2014
        'Purpose:To get  duplicate records of Default templates
        Public Function GetDefautlBizDocTemplateList() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _BizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppType", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _OppType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintTemplateType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _TemplateType

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "Usp_BizDocTemplate_GetDefaultTemplates", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'End of code by sachin

        Public Function DeleteBizDocTemplate() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _BizDocTemplateID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteBizDocTemplate", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeletePOSOrder() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _OppBizDocId

                SqlDAL.ExecuteNonQuery(connString, "USP_DeletePOSOrder", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetShippingRuleInfo() As DataTable
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numOrderID", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numBizDocItemID", _lngBizDocItemId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numShipClass", _lngShipClassID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numQty", _UnitHour, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_GET_RULE_AS_PER_ORDER", sqlParams.ToArray())
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Public Function GetPackageInfo() As DataTable
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numShipClass", _lngShipClassID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numPackageTypeID", _PackageTypeID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numQty", _UnitHour, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS", sqlParams.ToArray())
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Public Function GetAllOppBizDocItems() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAllOppBizDocItems", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ValidateBizDocSequenceId() As DataTable
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppId", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", _OppBizDocId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numBizDocId", _BizDocId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numSequenceId", _SequenceId, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_ValidateBizDocSequenceId", sqlParams.ToArray())
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Public Function SalesFulfillmentWorkflow(Optional ByVal bitNotValidateBizDocFulfillment As Boolean = False) As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppId", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", _OppBizDocId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numBizDocStatus", _BizDocStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppAuthBizDocsId", 0, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@bitNotValidateBizDocFulfillment", bitNotValidateBizDocFulfillment, NpgsqlTypes.NpgsqlDbType.Bit))

                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_SalesFulfillmentWorkflow", objParam, True)
                Dim OppAuthBizDocsId As Long = CCommon.ToLong(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return OppAuthBizDocsId
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportunityKitItemsForAuthorizativeAccounting() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OpportunityKitItemsForAuthorizativeAccounting", arParms)
                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try

        End Function

        Public Function GetOpportunityBizDocKitItemsForAuthorizativeAccounting() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OpportunityBizDocKitItemsForAuthorizativeAccounting", arParms)
                Return ds
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function OpportunityBizDocStatusChange() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppId", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", _OppBizDocId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numBizDocStatus", _BizDocStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityBizDocStatusChange", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportunityAutomationQueue() As DataTable
            Dim dtResult As New DataTable

            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numOppQueueID", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppId", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numBizDocStatus", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatus", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintProcessStatus", 0, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcDescription", "", NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numRuleID", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitSuccess", 0, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@tintMode", 2, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numShippingReportID", 0, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                dtResult = SqlDAL.ExecuteDatable(connString, "USP_ManageOpportunityAutomationQueue", sqlParams.ToArray())
            Catch ex As Exception
                dtResult = Nothing
                Throw ex
            End Try

            Return dtResult
        End Function

        Public Function ManageOpportunityAutomationQueue(Optional ByVal lngShippingReportID As Long = 0) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numOppQueueID", _OppQueueID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppId", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", _OppBizDocId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numBizDocStatus", _BizDocStatus, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatus", _OrderStatus, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintProcessStatus", _tintProcessStatus, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcDescription", _strDescription, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))
                    .Add(SqlDAL.Add_Parameter("@numRuleID", _numRuleID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitSuccess", _bitSuccess, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numShippingReportID", lngShippingReportID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageOpportunityAutomationQueue", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportunityAutomationQueueExecutionLog() As DataTable
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim ds As DataSet
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _ClientTimeZoneOffset

                arParms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _TotalRecords

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                ds = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetOpportunityAutomationQueueExecutionLog", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            Finally

            End Try

        End Function

        Public Shared Function GetServiceTypes(ByVal strShippingService As String) As Integer
            Dim intResult As Integer = 0
            Try

                Select Case strShippingService
                    Case "FedEx Priority Overnight"
                        intResult = 91
                    Case "FedEx Standard Overnight"
                        intResult = 91
                    Case "FedEx Overnight"
                        intResult = 91
                    Case "FedEx 2nd Day"
                        intResult = 91
                    Case "FedEx Express Saver"
                        intResult = 91
                    Case "FedEx Ground"
                        intResult = 91
                    Case "FedEx Ground Home Delivery"
                        intResult = 91
                    Case "FedEx 1 Day Freight"
                        intResult = 91
                    Case "FedEx 2 Day Freight"
                        intResult = 91
                    Case "FedEx 3 Day Freight"
                        intResult = 91
                    Case "FedEx International Priority"
                        intResult = 91
                    Case "International Priority Distribution"
                        intResult = 91
                    Case "FedEx International Economy"
                        intResult = 91
                    Case "FedEx International Economy Distribution"
                        intResult = 91
                    Case "FedEx International First"
                        intResult = 91
                    Case "FedEx International Priority Freight"
                        intResult = 91
                    Case "FedEx International Economy Freight"
                        intResult = 91
                    Case "FedEx International Distribution Freight"
                        intResult = 91
                    Case "FedEx Europe International Priority"
                        intResult = 91
                    Case "UPS Next Day Air"
                        intResult = 88
                    Case "UPS 2nd Day Air"
                        intResult = 88
                    Case "UPS Ground"
                        intResult = 88
                    Case "UPS 3Day Select"
                        intResult = 88
                    Case "UPS Next Day Air Saver"
                        intResult = 88
                    Case "UPS Saver"
                        intResult = 88
                    Case "UPS Next Day Air Early A.M."
                        intResult = 88
                    Case "UPS 2nd Day Air AM"
                        intResult = 88

                    Case "FedEx Letter"
                        intResult = 91
                    Case "FedEx Pak"
                        intResult = 91
                    Case "FedEx Box"
                        intResult = 91
                    Case "FedEx 10kg Box"
                        intResult = 91
                    Case "FedEx 25kg Box "
                        intResult = 91
                    Case "FedEx Tube"
                        intResult = 91

                    Case "UPS letter"
                        intResult = 88
                    Case "UPS Pak"
                        intResult = 88
                    Case "UPS 10kg Box"
                        intResult = 88
                    Case "UPS 25kg Box"
                        intResult = 88
                    Case "Small Express Box"
                        intResult = 88
                    Case "Medium Express Box"
                        intResult = 88
                    Case "Large Express Box"
                        intResult = 88
                    Case "UPS Tube"
                        intResult = 88
                    Case "UPS Pallet "
                        intResult = 88

                    Case "USPS Express"
                        intResult = 90
                    Case "USPS First Class"
                        intResult = 90
                    Case "USPS Priority"
                        intResult = 90
                    Case "USPS Parcel Post"
                        intResult = 90
                    Case "USPS Bound Printed Matter"
                        intResult = 90
                    Case "USPS Media"
                        intResult = 90
                    Case "USPS Library"
                        intResult = 90
                    Case Else

                End Select
            Catch ex As Exception
                Throw ex
            End Try

            Return intResult
        End Function

        Public Function GetBizDocItems() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "Usp_GetBizDocItems", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function CreateBizDocTemplateByDefault() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@txtBizDocTemplate", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(1).Value = _strText

                arParms(2) = New Npgsql.NpgsqlParameter("@txtCSS", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _BizDocCSS

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@txtPackingSlipBizDocTemplate", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(4).Value = _strDescription

                SqlDAL.ExecuteNonQuery(connString, "USP_CreateBizDocTemplateByDefault", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOPPGetOppAddressDetails() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = OppBizDocId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur4", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur5", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur6", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPGetOppAddressDetails", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function CheckDuplicateBizDocs() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _BizDocId
                '@tintOppType
                arParms(2) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _OppType
                arParms(3) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _BizDocTemplateID
                arParms(4) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _OppId
                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput
                Dim ds As DataSet
                ds = SqlDAL.ExecuteDataset(connString, "USP_CheckDuplicateBizDocs", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' This function is called when user tries to close sales order and there are remaining qty which is not yet fulfilled 
        ''' in this case we are automatically creating fulfillment bizdoc with remaing qty of each item 
        ''' and this method marks all items as fulfilled/shipped and release qty from on allocation
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub AutoFulfillBizDoc()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = OppId

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = OppBizDocId

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityBizDocs_AutoFulFill", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
        ''' <summary>
        ''' Gets a BizDoc items with detail of kit child Items
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>User can add another kit with kit which do not have another kit as child means depth of 1 level only. so we have display child kit items also in bizdoc</remarks>
        Public Function GetBizDocItemsWithKitChilds() As DataSet
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OppId

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppBizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_OPPBizDocItems_GetWithKitChilds", arParms)
                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetUnPaidInvoices() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityBizDocs_GetUnPaidInvoices", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetShippingBizDocWithNoShippingLabelAndTracking() As DataTable
            Try
                Dim ds As DataSet
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@numBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = BizDocId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityBizDocs_GetByOppIDAndBizDocType", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Added by Neelam Kapila || 09/29/2017 - Added function to open BizDoc to see what design change looks Like
        Public Function GetOpportunityModifiedBizDocs() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocTempID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BizDocTemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim dsBizData As New DataSet
                dsBizData = SqlDAL.ExecuteDataset(connString, "USP_GetOpportunityModifiedBizDocs", arParms)
                If dsBizData.Tables.Count > 0 Then
                    Return dsBizData.Tables(0)
                Else
                    Return Nothing
                End If
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetVendorInvoices() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numRecordID", OppId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintMode", _tintMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityBizDocs_GetVendorInvoice", sqlParams.ToArray())
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CanReceiveVendorInvoice() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numVendorInvoiceBizDocID", OppBizDocId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_OpportunityBizDocs_CanReceiveVendorInvoice", sqlParams.ToArray()))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function CanBillAgainstVendorInvoice() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numVendorInvoiceBizDocID", OppBizDocId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_OpportunityBizDocs_CanBillAgainstVendorInvoice", sqlParams.ToArray()))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOppItemsForPickPackShip() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numBizDocID", _BizDocId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_GetOppItemsForPickPackShip", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub ChangePackingSlipQty()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppId", OppId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", OppBizDocId, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityBizDocItems_ChangePackingSlipQty", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetPickListWithNoFulfillment() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numOppId", OppId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityBizDocs_GetPickListWithNoFulfillment", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function CheckIfIemsPendingToAddByBizDocID() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numOppId", OppId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numBizDocID", BizDocId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_OpportunityBizDocs_CheckIfItemsPendingToAddByBizDocID", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub CreateInvoiceFromFulfillmentOrder(DomainID As Long, UserCntID As Long, lngOppId As Long, numFulfillmentBizDocID As Long, ClientTimeZoneOffset As Integer)
            Try
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0

                Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    Dim dtDetails, dtBillingTerms As DataTable

                    Dim objPageLayout As New BACRM.BusinessLogic.Contacts.CPageLayout
                    objPageLayout.OpportunityId = lngOppId
                    objPageLayout.DomainID = DomainID
                    dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                    lngDivId = CLng(dtDetails.Rows(0).Item("numDivisionID"))
                    lngCntID = CLng(dtDetails.Rows(0).Item("numContactID"))


                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.UserCntID = UserCntID
                    objOppBizDocs.DomainID = DomainID

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", DomainID, lngDivId) = 0 Then
                        Throw New Exception("Not able to create invoice. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                        Exit Sub
                    End If

                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.OppType = 1
                    objOppBizDocs.FromDate = DateTime.UtcNow
                    objOppBizDocs.ClientTimeZoneOffset = 0
                    objOppBizDocs.numSourceBizDocId = CInt(numFulfillmentBizDocID)
                    objOppBizDocs.FromOppBizDocsId = numFulfillmentBizDocID

                    'SETS BIZDOC ID
                    '--------------
                    Dim lintAuthorizativeBizDocsId As Long
                    lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                    objOppBizDocs.BizDocId = lintAuthorizativeBizDocsId
                    '--------------

                    'SETS BIZDOC TEMPLETE NEEDS TO BE USED
                    '-------------------------------------
                    objOppBizDocs.byteMode = 1
                    Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                    objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))
                    '-------------------------------------

                    Dim objCommon As New CCommon
                    objCommon.DomainID = DomainID
                    objCommon.Mode = 33
                    objCommon.Str = CStr(lintAuthorizativeBizDocsId) 'DEFUALT BIZ DOC ID FOR BILL
                    objOppBizDocs.SequenceId = CStr(objCommon.GetSingleFieldValue())

                    objCommon = New CCommon
                    objCommon.DomainID = DomainID
                    objCommon.Mode = 34
                    objCommon.Str = CStr(lngOppId)
                    objOppBizDocs.RefOrderNo = CStr(objCommon.GetSingleFieldValue())
                    objOppBizDocs.bitPartialShipment = True

                    OppBizDocID = objOppBizDocs.SaveBizDoc()
                    If OppBizDocID > 0 Then
                        'CREATE JOURNALS
                        '---------------
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting

                        dtOppBiDocItems = ds.Tables(0)

                        Dim objCalculateDealAmount As New CalculateDealAmount
                        objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, DomainID, dtOppBiDocItems)

                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                        Dim objJournalEntries As New JournalEntry
                        If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                            objJournalEntries.SaveJournalEntriesSales(lngOppId, DomainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")), 0)
                        Else
                            objJournalEntries.SaveJournalEntriesSalesNew(lngOppId, DomainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")), 0)
                        End If

                        '---------------
                        '------ SEND BIZDOC ALERTS - NOTIFY RECORD OWNERS, THEIR SUPERVISORS, AND YOUR TRADING PARTNERS WHEN A BIZDOC IS CREATED, MODIFIED, OR APPROVED.
                        Dim objAlert As New CAlerts
                        objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, lintAuthorizativeBizDocsId, DomainID, CAlerts.enmBizDoc.IsCreated)

                        Dim objAutomatonRule As New AutomatonRule
                        objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)

                        ''Added By Sachin Sadhu||Date:1stMay2014
                        ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                        ''          Using Change tracking
                        Dim objWfA As New Workflow.Workflow()
                        objWfA.DomainID = DomainID
                        objWfA.UserCntID = UserCntID
                        objWfA.RecordID = OppBizDocID
                        objWfA.SaveWFBizDocQueue()
                    End If

                    objTransaction.Complete()
                End Using
                'end of code
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function IsSalesClearingAccountEntryExists() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppId", OppId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_General_Journal_Details_CheckIfSalesClearingEntryExists", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetBizDocARAccountID() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", OppBizDocId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_OpportunityBizDocs_GetARAccountID", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub UpdateBizDocEditFields()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", OppBizDocId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numBizDocTempID", BizDocTemplateID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtFromDate", IIf(_FromDate = Nothing, DateTime.UtcNow, _FromDate), NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@numShipVia", _ShipCompany, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSequenceId", _SequenceId, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcTrackingNo", TrackingNo, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcRefOrderNo", _RefOrderNo, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numBizDocStatus", _BizDocStatus, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityBizDocs_UpdateBizDocEditFields", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace