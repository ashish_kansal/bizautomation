﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports System.Data.SqlClient

Namespace BACRM.BusinessLogic.Opportunities

    Public Class MassSalesFulfillment
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property ClientTimeZoneOffset As Integer
        Public Property ViewID As Short
        Public Property WarehouseID As Long
        Public Property IsGroupByOrder As Boolean
        Public Property IsRemoveBO As Boolean
        Public Property ShippingZone As Long
        Public Property IsIncludeSearch As Boolean
        Public Property SelectedOrderStaus As String
        Public Property FilterBy As Short
        Public Property SelectedFilterValue As String
        Public Property CustomSearchValue As String
        Public Property PageIndex As Integer
        Public Property TotalRecords As Integer
        Public Property SortColumn As String
        Public Property SortOrder As String
        Public Property BatchID As Long
        Public Property SelectedRecords As String
        Public Property PackingViewMode As Short
        Public Property OppID As Long
        Public Property OppItemID As Long
        Public Property OppBizDocID As Long
        Public Property ShipVia As Long
        Public Property PendingCloseFilter As Short
        Public Property PrintBizDocViewMode As Short

#End Region

#Region "Public Methods"

        Public Function GetOrderStatus(ByVal oppType As Short) As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintOppType", oppType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillment_GetOrderStatus", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetWarehouses() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillment_GetWarehouses", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetConfiguration() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillment_GetConfiguration", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetRecords() As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numViewID", ViewID, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseID", WarehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrder", IsGroupByOrder, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitRemoveBO", IsRemoveBO, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numShippingZone", ShippingZone, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitIncludeSearch", IsIncludeSearch, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@vcOrderStauts", SelectedOrderStaus, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@tintFilterBy", FilterBy, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcFilterValue", SelectedFilterValue, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcCustomSearchValue", CustomSearchValue, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numBatchID", BatchID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintPackingViewMode", PackingViewMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintPrintBizDocViewMode", PrintBizDocViewMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintPendingCloseFilter", PendingCloseFilter, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numPageIndex", PageIndex, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSortColumn", SortColumn, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcSortOrder", SortOrder, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numTotalRecords", 0, NpgsqlTypes.NpgsqlDbType.Bigint, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object = sqlParams.ToArray()

                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_MassSalesFulfillment_GetRecords", objParam, True)

                SortColumn = Convert.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(18).Value)
                SortOrder = Convert.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(19).Value)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetRecordsForPicking() As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrder", IsGroupByOrder, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numBatchID", BatchID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSelectedRecords", SelectedRecords, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_MassSalesFulfillment_GetRecordsForPicking", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function UpdateItemsPickedQty() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@vcSelectedRecords", SelectedRecords, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillment_UpdateItemsPickedQty", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetOrderDetailsForShippingRate() As DataSet
            Try
                Try
                    Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                    Dim getconnection As New GetConnection
                    Dim connString As String = getconnection.GetConnectionString

                    With sqlParams
                        .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                        .Add(SqlDAL.Add_Parameter("@numShipVia", ShipVia, NpgsqlTypes.NpgsqlDbType.Bigint))
                        .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                        .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                        .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    End With

                    Return SqlDAL.ExecuteDataset(connString, "USP_MassSalesFulfillment_GetOrderDetailsForShippingRate", sqlParams.ToArray())
                Catch ex As Exception
                    Throw
                End Try
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function UpdateShippingRate() As Boolean
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSelectedRecords", SelectedRecords, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_MassSalesFulfillment_UpdateShippingRate", sqlParams.ToArray()))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetOrderItems(ByVal mode As Short) As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintMode", mode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcSelectedRecords", SelectedRecords, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillment_GetOrderItems", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetRecordsForPacking() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", OppItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillment_GetRecordsForPacking", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetShippingBizDocForPacking() As DataSet
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocID", OppBizDocID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", OppItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_MassSalesFulfillment_GetShippingBizDocForPacking", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub UpdateOrderStauts(ByVal viewID As Short, ByVal packingMode As Short)
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintViewID", viewID, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintPackingMode", packingMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_MassSalesFulfillment_UpdateOrderStauts", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetShippingLabelForPrint() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocID", OppBizDocID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillment_GetShippingLabelForPrint", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
#End Region

    End Class

End Namespace

