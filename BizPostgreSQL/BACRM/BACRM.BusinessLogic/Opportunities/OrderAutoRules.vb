﻿Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Web.UI.WebControls
Imports Npgsql

Public Class OrderAutoRules
    Inherits BACRM.BusinessLogic.CBusinessBase

    Dim _BillToID As Integer
    Dim _ShipToID As Integer
    Dim _RuleID As Integer
    Dim _objDtRule As New DataTable
    Dim _ModeID As Integer
    Dim _AmountPaid As Double
    Dim _BizDocStatus As Integer
    Dim _BillToText As String
    Dim _ShipToText As String
    Dim _BizDocStatusText As String
    Dim _FullPaid As String
    Dim _Active As Integer
    Public Property Active() As Integer
        Get
            Return _Active
        End Get
        Set(ByVal value As Integer)
            _Active = value
        End Set
    End Property
    Public Property BillToText() As String
        Get
            Return _BillToText
        End Get
        Set(ByVal value As String)
            _BillToText = value
        End Set
    End Property
    Public Property ShipToText() As String
        Get
            Return _ShipToText
        End Get
        Set(ByVal value As String)
            _ShipToText = value
        End Set
    End Property

    Public Property BizDocStatusText() As String
        Get
            Return _BizDocStatusText
        End Get
        Set(ByVal value As String)
            _BizDocStatusText = value
        End Set
    End Property
    Public Property FullPaid() As String
        Get
            Return _FullPaid
        End Get
        Set(ByVal value As String)
            _FullPaid = value
        End Set
    End Property


    Public Property ModeID() As Integer
        Get
            Return _ModeID
        End Get
        Set(ByVal value As Integer)
            _ModeID = value
        End Set
    End Property
    Public Property DtRule() As DataTable
        Get
            Return _objDtRule
        End Get
        Set(ByVal value As DataTable)
            _objDtRule = value
        End Set
    End Property
    Public Property RuleID() As Integer
        Get
            Return _RuleID
        End Get
        Set(ByVal value As Integer)
            _RuleID = value
        End Set
    End Property
    'Public Property DomainId() As Integer
    '    Get
    '        Return DomainId
    '    End Get
    '    Set(ByVal value As Integer)
    '        DomainId = value
    '    End Set
    'End Property
    Public Property BillToID() As Integer
        Get
            Return _BillToID
        End Get
        Set(ByVal value As Integer)
            _BillToID = value
        End Set
    End Property
    Public Property ShipToID() As Integer
        Get
            Return _ShipToID
        End Get
        Set(ByVal value As Integer)
            _ShipToID = value
        End Set
    End Property
    Public Property AmountPaid() As Double
        Get
            Return _AmountPaid
        End Get
        Set(ByVal value As Double)
            _AmountPaid = value
        End Set
    End Property
    Public Property BizDocStatus() As Integer
        Get
            Return _BizDocStatus
        End Get
        Set(ByVal value As Integer)
            _BizDocStatus = value
        End Set
    End Property

    Public Function ValidationRules(ByVal objDt As DataTable, ByVal ItemTypeID As Integer, ByVal ItemClassID As Integer) As Boolean
        Try
            ValidationRules = True
            Dim objRow() As DataRow
            objRow = objDt.Select("numItemTypeID=" & ItemTypeID.ToString & " AND numItemClassID=" & ItemClassID.ToString)
            If objRow.Length > 0 Then ValidationRules = False

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Sub GetOrderRule()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}



            arparms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = _RuleID

            arparms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(1).Value = DomainID

            arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arparms(2).Value = Nothing
            arparms(2).Direction = ParameterDirection.InputOutput

            Dim ds As DataSet
            ds = SqlDAL.ExecuteDataset(connString, "USP_GetOrderRule", arparms)
            If ds.Tables(0).Rows.Count > 0 Then
                RuleID = CType(ds.Tables(0).Rows(0).Item("numRuleID"), Integer)
                BillToID = CType(ds.Tables(0).Rows(0).Item("numBillToID"), Integer)
                ShipToID = CType(ds.Tables(0).Rows(0).Item("numShipToID"), Integer)
                BillToText = CType(ds.Tables(0).Rows(0).Item("Bill To"), String)
                ShipToText = CType(ds.Tables(0).Rows(0).Item("Ship To"), String)
                FullPaid = CType(ds.Tables(0).Rows(0).Item("Amount Paid Full"), String)
                BizDocStatusText = CType(ds.Tables(0).Rows(0).Item("Biz Doc Status"), String)
                BizDocStatus = CType(ds.Tables(0).Rows(0).Item("numBizDocStatus"), Integer)
                Active = CType(ds.Tables(0).Rows(0).Item("btActive"), Integer)
            End If



        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Function GetOrderRuleDetails() As DataTable
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

            arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = DomainID

            arparms(1) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(1).Value = _RuleID

            arparms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arparms(2).Value = Nothing
            arparms(2).Direction = ParameterDirection.InputOutput

            Dim ds As DataSet
            ds = SqlDAL.ExecuteDataset(connString, "USP_GetOrderRuleDetails", arparms)
            Return ds.Tables(0)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function AddEditGrid(ByVal objDt As DataTable, ByVal intMode As Integer, ByVal ItemType As String, ByVal ItemClass As String, ByVal BillTo As String, ByVal ShipTo As String,
                                ByVal FullPay As String, ByVal BizDoc As String, ByVal ItemTypeID As Integer, ByVal ItemClassID As Integer, ByVal BillToID As Integer,
                                ByVal ShipToID As Integer, ByVal FullPaid As Integer, ByVal BizDocStatus As Integer, ByVal Active As Integer) As DataTable
        Try
            Dim objRow As DataRow

            Select Case intMode
                Case 1
                    objRow = objDt.NewRow
                    objRow("Item Type") = ItemType
                    objRow("Item Classification") = ItemClass
                    objRow("Bill To") = BillTo
                    objRow("Ship To") = ShipTo
                    objRow("Amount Paid Full") = FullPay
                    objRow("Biz Doc Status") = BizDoc
                    objRow("numItemTypeID") = ItemTypeID
                    objRow("numItemClassID") = ItemClassID
                    objRow("numBillToID") = BillToID
                    objRow("numShipToID") = ShipToID
                    objRow("btFullPaid") = FullPaid
                    objRow("numBizDocStatus") = BizDocStatus
                    objRow("numRuleDetailsID") = 0
                    objRow("btActive") = Active
                    objDt.Rows.Add(objRow)
                    objDt.AcceptChanges()
                    AddEditGrid = objDt
                Case 2
                Case 3
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function DeleteRuleDetailsExhit2(ByVal numRuleDetailsId As Integer) As Int32
        Try
            If numRuleDetailsId = 0 Then Return 1

            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            DeleteRuleDetailsExhit2 = 0

            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

            arparms(0) = New Npgsql.NpgsqlParameter("@numRuleDetailsId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = numRuleDetailsId

            arparms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arparms(1).Value = Nothing
            arparms(1).Direction = ParameterDirection.InputOutput

            Dim ds As DataSet
            ds = SqlDAL.ExecuteDataset(connString, "USP_DeleteOrderRuleDetail", arparms)
            If ds.Tables(0).Rows.Count > 0 Then Return 1
        Catch ex As Exception

        End Try
    End Function
    Public Function SaveRules() As Integer
        Try
            SaveRules = -1

            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

            arparms(0) = New Npgsql.NpgsqlParameter("@numMode", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = _ModeID

            arparms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(1).Value = DomainID

            arparms(2) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(2).Value = _RuleID

            arparms(3) = New Npgsql.NpgsqlParameter("@numBillToID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(3).Value = _BillToID

            arparms(4) = New Npgsql.NpgsqlParameter("@numShipToID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(4).Value = _ShipToID

            arparms(5) = New Npgsql.NpgsqlParameter("@btFullPaid", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(5).Value = _AmountPaid

            arparms(6) = New Npgsql.NpgsqlParameter("@numBizDocStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(6).Value = _BizDocStatus

            arparms(7) = New Npgsql.NpgsqlParameter("@btActive", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(7).Value = _Active

            arparms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arparms(8).Value = Nothing
            arparms(8).Direction = ParameterDirection.InputOutput

            Dim ds As DataSet
            ds = SqlDAL.ExecuteDataset(connString, "USP_ManageOrderAutoRule", arparms)
            If ds.Tables(0).Rows.Count > 0 Then
                RuleID = CType(ds.Tables(0).Rows(0).Item(0), Integer)

                If _ModeID <> 2 Then
                    If SaveRulesDtls() <> -1 Then
                        Return RuleID
                    Else
                        Return SaveRules
                    End If
                Else
                    Return RuleID
                End If
            Else
                Return SaveRules
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function SaveRulesDtls() As Integer
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            If _objDtRule.Rows.Count > 0 Then
                Dim objRow As DataRow
                For Each objRow In _objDtRule.Rows
                    SaveRulesDtls = -1

                    Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                    arparms(0) = New Npgsql.NpgsqlParameter("@numRuleID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(0).Value = _RuleID

                    arparms(1) = New Npgsql.NpgsqlParameter("@numItemTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(1).Value = objRow.Item("numItemTypeID")

                    arparms(2) = New Npgsql.NpgsqlParameter("@numItemClassID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(2).Value = objRow.Item("numItemClassID")

                    arparms(3) = New Npgsql.NpgsqlParameter("@numBillToID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(3).Value = objRow.Item("numBillToID")

                    arparms(4) = New Npgsql.NpgsqlParameter("@numShipToID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(4).Value = objRow.Item("numShipToID")

                    arparms(5) = New Npgsql.NpgsqlParameter("@btFullPaid", NpgsqlTypes.NpgsqlDbType.Bit)
                    arparms(5).Value = objRow.Item("btFullPaid")

                    arparms(6) = New Npgsql.NpgsqlParameter("@numBizDocStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arparms(6).Value = objRow.Item("numBizDocStatus")

                    arparms(7) = New Npgsql.NpgsqlParameter("@btActive", NpgsqlTypes.NpgsqlDbType.Bit)
                    arparms(7).Value = objRow.Item("btActive")

                    arparms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                    arparms(8).Value = Nothing
                    arparms(8).Direction = ParameterDirection.InputOutput

                    Dim ds As DataSet
                    ds = SqlDAL.ExecuteDataset(connString, "USP_ManageOrderAutoRuleDetails", arparms)
                    If ds.Tables(0).Rows.Count > 0 Then
                        SaveRulesDtls = CType(ds.Tables(0).Rows(0).Item(0), Integer)
                    Else
                        SaveRulesDtls = -1
                        Exit For
                    End If
                Next

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetBizDocAppRule(ByVal numBizDocTypeId As Int32) As DataSet
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

            arparms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = DomainID

            arparms(1) = New Npgsql.NpgsqlParameter("@numBizDocTypeId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(1).Value = numBizDocTypeId

            Dim ds As DataSet
            ds = SqlDAL.ExecuteDataset(connString, "USP_GetBizDocAppRuleList", arparms)

            Return ds
        Catch ex As Exception
            Throw ex
        End Try

    End Function
    Public Sub GenerateAutoPO(ByVal numOppBizDocId As Long)
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

            arparms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = numOppBizDocId

            Dim ds As DataSet
            ds = SqlDAL.ExecuteDataset(connString, "USP_OrderAutomationPO", arparms)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub GenerateAutoPO(ByVal numOppBizDocId As Long, Optional ByVal objTransaction As NpgsqlTransaction = Nothing)
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

            arparms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
            arparms(0).Value = numOppBizDocId

            Dim ds As DataSet
            If objTransaction IsNot Nothing Then
                ds = SqlDAL.ExecuteDataset(objTransaction, "USP_OrderAutomationPO", arparms)
            Else
                ds = SqlDAL.ExecuteDataset(connString, "USP_OrderAutomationPO", arparms)
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
