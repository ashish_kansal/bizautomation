﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.Opportunities

    Public Class ReturnHeader
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _ReturnHeaderID As Long
        Public Property ReturnHeaderID() As Long
            Get
                Return _ReturnHeaderID
            End Get
            Set(ByVal value As Long)
                _ReturnHeaderID = value
            End Set
        End Property

        Private _Rma As String
        Public Property Rma() As String
            Get
                Return _Rma
            End Get
            Set(ByVal value As String)
                _Rma = value
            End Set
        End Property

        Private _BizDocID As Long
        Public Property BizDocID() As Long
            Get
                Return _BizDocID
            End Get
            Set(ByVal value As Long)
                _BizDocID = value
            End Set
        End Property

        Private _BizDocName As String
        Public Property BizDocName() As String
            Get
                Return _BizDocName
            End Get
            Set(ByVal value As String)
                _BizDocName = value
            End Set
        End Property

        Private _DivisionId As Long
        Public Property DivisionId() As Long
            Get
                Return _DivisionId
            End Get
            Set(ByVal value As Long)
                _DivisionId = value
            End Set
        End Property

        Private _ContactId As Long
        Public Property ContactId() As Long
            Get
                Return _ContactId
            End Get
            Set(ByVal value As Long)
                _ContactId = value
            End Set
        End Property

        Private _OppId As Long
        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal value As Long)
                _OppId = value
            End Set
        End Property

        Private _ReturnType As Short
        Public Property ReturnType() As Short
            Get
                Return _ReturnType
            End Get
            Set(ByVal value As Short)
                _ReturnType = value
            End Set
        End Property

        Private _ReturnReason As Long
        Public Property ReturnReason() As Long
            Get
                Return _ReturnReason
            End Get
            Set(ByVal value As Long)
                _ReturnReason = value
            End Set
        End Property

        Private _ReturnStatus As Long
        Public Property ReturnStatus() As Long
            Get
                Return _ReturnStatus
            End Get
            Set(ByVal value As Long)
                _ReturnStatus = value
            End Set
        End Property

        Private _Amount As Decimal
        Public Property Amount() As Decimal
            Get
                Return _Amount
            End Get
            Set(ByVal value As Decimal)
                _Amount = value
            End Set
        End Property

        Private _AmountUsed As Decimal
        Public Property AmountUsed() As Decimal
            Get
                Return _AmountUsed
            End Get
            Set(ByVal value As Decimal)
                _AmountUsed = value
            End Set
        End Property

        Private _TotalTax As Decimal
        Public Property TotalTax() As Decimal
            Get
                Return _TotalTax
            End Get
            Set(ByVal value As Decimal)
                _TotalTax = value
            End Set
        End Property

        Private _TotalDiscount As Decimal
        Public Property TotalDiscount() As Decimal
            Get
                Return _TotalDiscount
            End Get
            Set(ByVal value As Decimal)
                _TotalDiscount = value
            End Set
        End Property

        Private _ReceiveType As Short
        Public Property ReceiveType() As Short
            Get
                Return _ReceiveType
            End Get
            Set(ByVal value As Short)
                _ReceiveType = value
            End Set
        End Property

        Private _Comments As String
        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal value As String)
                _Comments = value
            End Set
        End Property

        Private _CreatedBy As Long
        Public Property CreatedBy() As Long
            Get
                Return _CreatedBy
            End Get
            Set(ByVal value As Long)
                _CreatedBy = value
            End Set
        End Property

        Private _CreatedDate As DateTime
        Public Property CreatedDate() As DateTime
            Get
                Return _CreatedDate
            End Get
            Set(ByVal value As DateTime)
                _CreatedDate = value
            End Set
        End Property

        Private _ModifiedBy As Long
        Public Property ModifiedBy() As Long
            Get
                Return _ModifiedBy
            End Get
            Set(ByVal value As Long)
                _ModifiedBy = value
            End Set
        End Property

        Private _ModifiedDate As DateTime
        Public Property ModifiedDate() As DateTime
            Get
                Return _ModifiedDate
            End Get
            Set(ByVal value As DateTime)
                _ModifiedDate = value
            End Set
        End Property

        Private _strItems As String
        Public Property strItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal value As String)
                _strItems = value
            End Set
        End Property

        Private _sMode As Short
        Public Property sMode() As Short
            Get
                Return _sMode
            End Get
            Set(ByVal value As Short)
                _sMode = value
            End Set
        End Property

        Private _intMode As Integer
        Public Property Mode() As Integer
            Get
                Return _intMode
            End Get
            Set(ByVal value As Integer)
                _intMode = value
            End Set
        End Property

        Private _IsCreateRefundReceipt As Boolean
        Public Property IsCreateRefundReceipt() As Boolean
            Get
                Return _IsCreateRefundReceipt
            End Get
            Set(ByVal value As Boolean)
                _IsCreateRefundReceipt = value
            End Set
        End Property

        Private _CheckNumber As String
        Public Property CheckNumber() As String
            Get
                Return _CheckNumber
            End Get
            Set(ByVal value As String)
                _CheckNumber = value
            End Set
        End Property

        Private _AccountID As Long
        Public Property AccountID() As Long
            Get
                Return _AccountID
            End Get
            Set(ByVal value As Long)
                _AccountID = value
            End Set
        End Property

        Private _BillToAddressID As Long
        Public Property BillToAddressID() As Long
            Get
                Return _BillToAddressID
            End Get
            Set(ByVal value As Long)
                _BillToAddressID = value
            End Set
        End Property

        Private _ShipToAddressID As Long
        Public Property ShipToAddressID() As Long
            Get
                Return _ShipToAddressID
            End Get
            Set(ByVal value As Long)
                _ShipToAddressID = value
            End Set
        End Property

        Private _ReturnReasonName As String
        Public Property ReturnReasonName() As String
            Get
                Return _ReturnReasonName
            End Get
            Set(ByVal value As String)
                _ReturnReasonName = value
            End Set
        End Property

        Private _ReturnStatusName As String
        Public Property ReturnStatusName() As String
            Get
                Return _ReturnStatusName
            End Get
            Set(ByVal value As String)
                _ReturnStatusName = value
            End Set
        End Property

        Private _ItemCode As Long
        Public Property ItemCode() As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal value As Long)
                _ItemCode = value
            End Set
        End Property

        Private _ReturnItemID As Long
        Public Property ReturnItemID() As Long
            Get
                Return _ReturnItemID
            End Get
            Set(ByVal value As Long)
                _ReturnItemID = value
            End Set
        End Property

        Private _WareHouseItemID As Long
        Public Property WareHouseItemID() As Long
            Get
                Return _WareHouseItemID
            End Get
            Set(ByVal value As Long)
                _WareHouseItemID = value
            End Set
        End Property

        Private _DepositIDRef As Long
        Public Property DepositIDRef() As Long
            Get
                Return _DepositIDRef
            End Get
            Set(ByVal value As Long)
                _DepositIDRef = value
            End Set
        End Property

        Private _BillPaymentIDRef As Long
        Public Property BillPaymentIDRef() As Long
            Get
                Return _BillPaymentIDRef
            End Get
            Set(ByVal value As Long)
                _BillPaymentIDRef = value
            End Set
        End Property

        Private _lngParentID As Long
        Public Property lngParentID() As Long
            Get
                Return _lngParentID
            End Get
            Set(ByVal value As Long)
                _lngParentID = value
            End Set
        End Property

        Public Property ReferenceSalesOrder As Long
        Public Property OppBizDocID As Long

        Function GetReturnHeader() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintReceiveType", _ReceiveType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetHeaderReturnDetails", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageReturnHeader() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@vcRMA", _Rma, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@vcBizDocName", _BizDocName, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDivisionId", _DivisionId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numContactId", _ContactId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppId", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintReturnType", _ReturnType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numReturnReason", _ReturnReason, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReturnStatus", _ReturnStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@monAmount", _Amount, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@monTotalTax", _TotalTax, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@monTotalDiscount", _TotalDiscount, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@tintReceiveType", _ReceiveType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@vcComments", _Comments, NpgsqlTypes.NpgsqlDbType.Text, 16))

                    .Add(SqlDAL.Add_Parameter("@strItems", _strItems, NpgsqlTypes.NpgsqlDbType.Text))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _sMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numBillAddressId", _BillToAddressID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numShipAddressId", _ShipToAddressID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDepositIDRef", _DepositIDRef, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numBillPaymentIDRef", _BillPaymentIDRef, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numParentID", _lngParentID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReferenceSalesOrder", ReferenceSalesOrder, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", OppBizDocID, NpgsqlTypes.NpgsqlDbType.Bigint))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageReturnHeaderItems", objParam, True)
                _ReturnHeaderID = CCommon.ToLong(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

                Return _ReturnHeaderID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageReturnBizDocs() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintReceiveType", _ReceiveType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numReturnStatus", _ReturnStatus, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numAccountID", _AccountID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@vcCheckNumber", _CheckNumber, NpgsqlTypes.NpgsqlDbType.VarChar, 50))

                    .Add(SqlDAL.Add_Parameter("@IsCreateRefundReceipt", _IsCreateRefundReceipt, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@numItemCode", _ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt, 18))

                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageReturnBizDocs", objParam, True)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function DeleteReturnHeader() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numReturnStatus", _ReturnStatus, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteSalesReturnDetails", sqlParams.ToArray())

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetReturnDetails() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@tintReceiveType", _ReceiveType, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetHeaderReturnDetails", sqlParams.ToArray())

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        If Not IsDBNull(dr("vcRMA")) Then
                            Me.Rma = CType(dr("vcRMA"), String)
                        End If
                        If Not IsDBNull(dr("vcBizDocName")) Then
                            Me.BizDocName = CType(dr("vcBizDocName"), String)
                        End If
                        If Not IsDBNull(dr("numContactId")) Then
                            Me.ContactId = CType(dr("numContactId"), Long)
                        End If
                        If Not IsDBNull(dr("numDivisionId")) Then
                            Me.DivisionId = CType(dr("numDivisionId"), Long)
                        End If
                        If Not IsDBNull(dr("tintReturnType")) Then
                            Me.ReturnType = CType(dr("tintReturnType"), Short)
                        End If
                        If Not IsDBNull(dr("numReturnReason")) Then
                            Me.ReturnReason = CType(dr("numReturnReason"), Long)
                        End If
                        If Not IsDBNull(dr("numReturnStatus")) Then
                            Me.ReturnStatus = CType(dr("numReturnStatus"), Long)
                        End If

                        If Not IsDBNull(dr("monAmount")) Then
                            Me.Amount = ReturnMoney(dr("monAmount"))
                        End If

                        If Not IsDBNull(dr("monAmountUsed")) Then
                            Me.AmountUsed = ReturnMoney(dr("monAmountUsed"))
                        End If
                        If Not IsDBNull(dr("monTotalTax")) Then
                            Me.TotalTax = ReturnMoney(dr("monTotalTax"))
                        End If
                        If Not IsDBNull(dr("monTotalDiscount")) Then
                            Me.TotalDiscount = ReturnMoney(dr("monTotalDiscount"))
                        End If
                        If Not IsDBNull(dr("tintReceiveType")) Then
                            Me.ReceiveType = CType(dr("tintReceiveType"), Integer)
                        End If
                        If Not IsDBNull(dr("vcComments")) Then
                            Me.Comments = CType(dr("vcComments"), String)
                        End If
                        If Not IsDBNull(dr("numCreatedBy")) Then
                            Me.CreatedBy = CType(dr("numCreatedBy"), Long)
                        End If
                        If Not IsDBNull(dr("dtCreatedDate")) Then
                            Me.CreatedDate = CType(dr("dtCreatedDate"), Date)
                        End If
                        If Not IsDBNull(dr("numModifiedBy")) Then
                            Me.ModifiedBy = CType(dr("numModifiedBy"), Long)
                        End If
                        If Not IsDBNull(dr("dtModifiedDate")) Then
                            Me.ModifiedDate = CType(dr("dtModifiedDate"), Date)
                        End If

                        If Not IsDBNull(dr("ReturnReasonName")) Then
                            Me.ReturnReasonName = CType(dr("ReturnReasonName"), String)
                        End If

                        If Not IsDBNull(dr("ReturnStatusName")) Then
                            Me.ReturnStatusName = CType(dr("ReturnStatusName"), String)
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        '*******Code to call InsertUPdate Method from BLL
        'Dim objReturnHeader As New ReturnHeader
        '    objReturnHeader.ReturnHeaderID = ""
        '    objReturnHeader.RMA = ""
        '    objReturnHeader.BizDocName = ""
        '    objReturnHeader.DomainId = ""
        '    objReturnHeader.DivisionId = ""
        '    objReturnHeader.ContactId = ""
        '    objReturnHeader.OppId = ""
        '    objReturnHeader.ReturnType = ""
        '    objReturnHeader.ReturnReason = ""
        '    objReturnHeader.ReturnStatus = ""
        '    objReturnHeader.Amount = ""
        '    objReturnHeader.AmountUsed = ""
        '    objReturnHeader.TotalTax = ""
        '    objReturnHeader.TotalDiscount = ""
        '    objReturnHeader.ReceiveType = ""
        '    objReturnHeader.Comments = ""
        '    objReturnHeader.CreatedBy = ""
        '    objReturnHeader.dtCreatedDate = ""
        '    objReturnHeader.ModifiedBy = ""
        '    objReturnHeader.dtModifiedDate = ""
        '    objReturnHeader.ManageReturnHeader()

        Function GetRMASerializedIndItems() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnHeaderID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numReturnItemID", _ReturnItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetRMASerializedindItems", sqlParams.ToArray())
                Return ds

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ManageRMALotSerial() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numWareHouseItemID", _WareHouseItemID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@numReturnItemID", _ReturnItemID, NpgsqlTypes.NpgsqlDbType.BigInt, 9))

                    .Add(SqlDAL.Add_Parameter("@tintMode", _intMode, NpgsqlTypes.NpgsqlDbType.Smallint))

                    .Add(SqlDAL.Add_Parameter("@strItems", _strItems, NpgsqlTypes.NpgsqlDbType.Text))

                End With

                Dim objParam() As Object
                objParam = sqlParams.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_ManageRMALotSerial", objParam, True)

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function GetReturnItemForJournal() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numReturnHeaderID", _ReturnHeaderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_ReturnItems_GetForReturnJournal", sqlParams.ToArray())
                Return ds.Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class

End Namespace