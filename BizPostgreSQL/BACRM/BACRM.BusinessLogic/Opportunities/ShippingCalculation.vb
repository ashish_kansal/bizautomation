﻿Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic
Imports BACRM.BusinessLogic.Opportunities

Namespace BACRM.BusinessLogic.Opportunities

    Public Class ShippingCalculation

        Public Sub AutoCalculateOrderShippingRate()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New BACRMBUSSLOGIC.BussinessLogic.GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWareHouseID", 0, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtShipByDate", Nothing, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@bitFromService", True, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_OpporutnityMaster_GetForShippingRate", sqlParams.ToArray())

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    System.Threading.Tasks.Parallel.ForEach(dt.AsEnumerable(), Sub(dr) CalculateShippingRate(dr, True))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function MSCalculateOrderShippingRate(ByVal lngDomainID As Long, ByVal lngOppID As Long, ByVal lngWarehouseID As Long, ByVal shipByDate As Date?) As Tuple(Of Double, Integer, String, Boolean, String)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim connString As String = BACRMBUSSLOGIC.BussinessLogic.GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", lngDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", lngOppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWareHouseID", lngWarehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtShipByDate", shipByDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@bitFromService", False, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim dt As DataTable = SqlDAL.ExecuteDatable(connString, "USP_OpporutnityMaster_GetForShippingRate", sqlParams.ToArray())

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Return CalculateShippingRate(dt.Rows(0), False)
                Else
                    Return New Tuple(Of Double, Integer, String, Boolean, String)(0, 0, "", False, "Order not found.")
                End If
            Catch ex As Exception
                Return New Tuple(Of Double, Integer, String, Boolean, String)(0, 0, "", False, ex.Message)
            End Try
        End Function

        Private Function CalculateShippingRate(ByVal dr As DataRow, ByVal isFromService As Boolean) As Tuple(Of Double, Integer, String, Boolean, String)
            Dim lngDomainID As Long
            Dim lngOppID As Long

            Try
                lngDomainID = Convert.ToInt64(dr("numDomainId"))
                lngOppID = Convert.ToInt64(dr("numOppId"))
                Dim lngWareHouseID As Long = Convert.ToInt64(dr("numWareHouseID"))
                Dim dtShipByDate As Date? = If(dr("dtShipByDate") Is DBNull.Value, Nothing, Convert.ToDateTime(dr("dtShipByDate")))
                Dim expectedDate As Date? = If(dr("dtExpectedDate") Is DBNull.Value, Nothing, Convert.ToDateTime(dr("dtExpectedDate")))
                Dim numOrderShipVia As Long = Convert.ToInt64(dr("numShipVia"))
                Dim numOrderShipService As Long = Convert.ToInt64(dr("numShipService"))
                Dim marketplaceShippingCost As Double = Convert.ToDouble(dr("monMarketplaceShippingCost"))

                Dim isPreferredService As Boolean = False
                Dim tintType As Short = Convert.ToInt16(dr("tintType"))
                Dim vcPriorities As String = Convert.ToString(dr("vcPriorities"))
                Dim numShipVia As Integer = Convert.ToInt32(dr("numShipVia"))
                Dim numShipService As Integer = Convert.ToInt32(dr("numShipService"))
                Dim bitOverride As Boolean = Convert.ToBoolean(dr("bitOverride"))
                Dim fltWeight As Double = Convert.ToDouble(dr("fltWeight"))
                Dim numShipViaOverride As Long = Convert.ToInt64(dr("numShipViaOverride"))

                Dim dtShippingMethod As New DataTable()

                If tintType = 2 Then 'Use configured ship via & ship service in mass shipping configuration 
                    isPreferredService = True

                    Dim objRule As New ShioppingCart.ShippingRule()
                    objRule.DomainID = lngDomainID
                    objRule.ShippingCompanyID = numShipVia
                    objRule.ShippingMethod = numShipService
                    dtShippingMethod = objRule.EstimateShipping()
                Else
                    Dim objRule As New ShioppingCart.ShippingRule()
                    objRule.DomainID = lngDomainID
                    dtShippingMethod = objRule.EstimateShipping()
                End If

                Dim newColumn As New System.Data.DataColumn("IsShippingRuleValid", GetType(System.Boolean))
                newColumn.DefaultValue = True
                dtShippingMethod.Columns.Add(newColumn)

                dtShippingMethod.Columns.Add("TransitTime", GetType(System.Int32))
                dtShippingMethod.Columns.Add("ShipByDate")
                dtShippingMethod.Columns.Add("AnticipateDelivery")
                dtShippingMethod.Columns.Add("ShippingAmount", GetType(System.Decimal))


                If Not dtShippingMethod Is Nothing AndAlso dtShippingMethod.Rows.Count > 0 Then
                    Dim dsOrder As DataSet = GetOrderItemsForShippingRate(lngDomainID, lngOppID, lngWareHouseID, dtShipByDate)

                    If dsOrder Is Nothing Or dsOrder.Tables.Count = 0 Or dsOrder.Tables(0).Rows.Count = 0 Then
                        If isFromService Then
                            Dim objOMSRE As New OpportunityMasterShippingRateError
                            objOMSRE.DomainID = lngDomainID
                            objOMSRE.OppID = lngOppID
                            objOMSRE.ErrorMessage = "Ship from address not available."
                            objOMSRE.Save()

                            Return Nothing
                        Else
                            Return New Tuple(Of Double, Integer, String, Boolean, String)(0, 0, "", False, "Ship from address not available.")
                        End If
                    End If

                    If dsOrder Is Nothing Or dsOrder.Tables.Count < 2 AndAlso dsOrder.Tables(1).Rows.Count = 0 Then
                        If isFromService Then
                            Dim objOMSRE As New OpportunityMasterShippingRateError
                            objOMSRE.DomainID = lngDomainID
                            objOMSRE.OppID = lngOppID
                            objOMSRE.ErrorMessage = "Ship to address not available."
                            objOMSRE.Save()

                            Return Nothing
                        Else
                            Return New Tuple(Of Double, Integer, String, Boolean, String)(0, 0, "", False, "Ship to address not available.")
                        End If
                    End If

                    Dim totalWeight As Double = 0
                    If Not dsOrder Is Nothing AndAlso dsOrder.Tables.Count >= 2 AndAlso dsOrder.Tables(2).Rows.Count > 0 Then
                        For Each drBox As DataRow In dsOrder.Tables(2).Rows
                            totalWeight = totalWeight + Convert.ToDecimal(drBox("fltContainerWeight"))
                        Next
                    Else
                        If isFromService Then
                            Dim objOMSRE As New OpportunityMasterShippingRateError
                            objOMSRE.DomainID = lngDomainID
                            objOMSRE.OppID = lngOppID
                            objOMSRE.ErrorMessage = "Item(s) not found."
                            objOMSRE.Save()

                            Return Nothing
                        Else
                            Return New Tuple(Of Double, Integer, String, Boolean, String)(0, 0, "", False, "Item(s) not found.")
                        End If
                    End If

                    If totalWeight > 0 Then
                        If bitOverride AndAlso totalWeight > fltWeight Then
                            Dim objCommon As New CCommon
                            objCommon.DomainID = lngDomainID
                            objCommon.UpdateRecordID = lngOppID
                            objCommon.UpdateValueID = numShipViaOverride
                            objCommon.Mode = 49
                            objCommon.Comments = ""
                            objCommon.UpdateSingleFieldValue()
                        Else
                            For Each drShip As DataRow In dtShippingMethod.Rows
                                Dim sRateType As Short = 0
                                Dim ezRates As New nsoftware.InShip.Ezrates

                                Dim objItem As New Item.CItems
                                objItem.DomainID = lngDomainID

                                If Convert.ToInt32(drShip("numShippingCompanyID")) = 91 Then 'Fedex
                                    objItem.ShippingCMPID = 91
                                    Dim dsShippingConfiguration As DataSet = objItem.ShippingDtls()

                                    ezRates.Provider = nsoftware.InShip.EzratesProviders.pFedEx
                                    ezRates.Account.Server = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=9")(0)("vcShipFieldValue"))
                                    ezRates.Account.DeveloperKey = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=11")(0)("vcShipFieldValue"))
                                    ezRates.Account.Password = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=12")(0)("vcShipFieldValue"))
                                    ezRates.Account.AccountNumber = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=6")(0)("vcShipFieldValue"))
                                    ezRates.Account.MeterNumber = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=7")(0)("vcShipFieldValue"))
                                    sRateType = Convert.ToInt16(dsShippingConfiguration.Tables(0).Select("intShipFieldID=20")(0)("vcShipFieldValue"))
                                ElseIf Convert.ToInt32(drShip("numShippingCompanyID")) = 88 Then 'UPS
                                    objItem.ShippingCMPID = 88
                                    Dim dsShippingConfiguration As DataSet = objItem.ShippingDtls()

                                    ezRates.Provider = nsoftware.InShip.EzratesProviders.pUPS
                                    ezRates.Account.Server = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=5")(0)("vcShipFieldValue"))
                                    ezRates.Account.AccessKey = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=1")(0)("vcShipFieldValue"))
                                    ezRates.Account.UserId = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=2")(0)("vcShipFieldValue"))
                                    ezRates.Account.Password = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=3")(0)("vcShipFieldValue"))
                                    ezRates.Account.AccountNumber = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=4")(0)("vcShipFieldValue"))
                                Else 'USPS
                                    objItem.ShippingCMPID = 90
                                    Dim dsShippingConfiguration As DataSet = objItem.ShippingDtls()

                                    ezRates.Provider = nsoftware.InShip.EzratesProviders.pUSPS
                                    ezRates.Account.Server = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=17")(0)("vcShipFieldValue"))
                                    ezRates.Account.UserId = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=15")(0)("vcShipFieldValue"))
                                    ezRates.Account.Password = Convert.ToString(dsShippingConfiguration.Tables(0).Select("intShipFieldID=16")(0)("vcShipFieldValue"))
                                End If
                                ezRates.RequestedService = CType(Convert.ToInt32(drShip("intNsoftEnum")), nsoftware.InShip.ServiceTypes)

                                If Not dsOrder Is Nothing AndAlso dsOrder.Tables.Count >= 0 AndAlso dsOrder.Tables(0).Rows.Count > 0 Then
                                    ezRates.SenderAddress.Address1 = Convert.ToString(dsOrder.Tables(0).Rows(0)("vcStreet"))
                                    ezRates.SenderAddress.City = Convert.ToString(dsOrder.Tables(0).Rows(0)("vcCity"))
                                    ezRates.SenderAddress.ZipCode = Convert.ToString(dsOrder.Tables(0).Rows(0)("vcZipCode"))
                                    If objItem.ShippingCMPID = 91 Then
                                        ezRates.SenderAddress.State = Convert.ToString(dsOrder.Tables(0).Rows(0)("vcStateFedex"))
                                    ElseIf objItem.ShippingCMPID = 88 Then
                                        ezRates.SenderAddress.State = Convert.ToString(dsOrder.Tables(0).Rows(0)("vcStateUPS"))
                                    Else
                                        ezRates.SenderAddress.State = Convert.ToString(dsOrder.Tables(0).Rows(0)("vcStateUSPS"))
                                    End If

                                    If objItem.ShippingCMPID = 91 Then
                                        ezRates.SenderAddress.CountryCode = Convert.ToString(dsOrder.Tables(0).Rows(0)("vcCountryFedex"))
                                    ElseIf objItem.ShippingCMPID = 88 Then
                                        ezRates.SenderAddress.CountryCode = Convert.ToString(dsOrder.Tables(0).Rows(0)("vcCountryUPS"))
                                    Else
                                        ezRates.SenderAddress.CountryCode = Convert.ToString(dsOrder.Tables(0).Rows(0)("vcCountryUSPS"))
                                    End If
                                Else
                                    Throw New Exception("SHIP_FROM_ADDRESS_NOT_AVAILABLE")
                                End If

                                If Not dsOrder Is Nothing AndAlso dsOrder.Tables.Count >= 1 AndAlso dsOrder.Tables(1).Rows.Count > 0 Then
                                    ezRates.RecipientAddress.Address1 = Convert.ToString(dsOrder.Tables(1).Rows(0)("vcStreet"))
                                    ezRates.RecipientAddress.City = Convert.ToString(dsOrder.Tables(1).Rows(0)("vcCity"))
                                    ezRates.RecipientAddress.ZipCode = Convert.ToString(dsOrder.Tables(1).Rows(0)("vcZipCode"))

                                    If objItem.ShippingCMPID = 91 Then
                                        ezRates.RecipientAddress.State = Convert.ToString(dsOrder.Tables(1).Rows(0)("vcStateFedex"))
                                    ElseIf objItem.ShippingCMPID = 88 Then
                                        ezRates.RecipientAddress.State = Convert.ToString(dsOrder.Tables(1).Rows(0)("vcStateUPS"))
                                    Else
                                        ezRates.RecipientAddress.State = Convert.ToString(dsOrder.Tables(1).Rows(0)("vcStateUSPS"))
                                    End If

                                    If objItem.ShippingCMPID = 91 Then
                                        ezRates.RecipientAddress.CountryCode = Convert.ToString(dsOrder.Tables(1).Rows(0)("vcCountryFedex"))
                                    ElseIf objItem.ShippingCMPID = 88 Then
                                        ezRates.RecipientAddress.CountryCode = Convert.ToString(dsOrder.Tables(1).Rows(0)("vcCountryUPS"))
                                    Else
                                        ezRates.RecipientAddress.CountryCode = Convert.ToString(dsOrder.Tables(1).Rows(0)("vcCountryUSPS"))
                                    End If
                                Else
                                    Throw New Exception("SHIP_TO_FROM_ADDRESS_NOT_AVAILABLE")
                                End If

                                If Not dsOrder Is Nothing AndAlso dsOrder.Tables.Count >= 2 AndAlso dsOrder.Tables(2).Rows.Count > 0 Then
                                    For Each drPack As DataRow In dsOrder.Tables(2).Rows
                                        Dim objPackageDetail As New nsoftware.InShip.PackageDetail()
                                        objPackageDetail.PackagingType = nsoftware.InShip.TPackagingTypes.ptYourPackaging
                                        objPackageDetail.Length = Convert.ToInt32(drPack("fltContainerLength"))
                                        objPackageDetail.Width = Convert.ToInt32(drPack("fltContainerWidth"))
                                        objPackageDetail.Height = Convert.ToInt32(drPack("fltContainerHeight"))
                                        objPackageDetail.Girth = (2 * Convert.ToInt32(drPack("fltContainerLength"))) + (2 * Convert.ToInt32(drPack("fltContainerWidth")))
                                        If (ezRates.Provider = nsoftware.InShip.EzratesProviders.pUSPS) Then
                                            Dim intWeight As Integer

                                            If Integer.TryParse(Convert.ToString(Convert.ToDecimal(drPack("fltContainerWeight"))), intWeight) Then
                                                objPackageDetail.Weight = Convert.ToString(intWeight) + " lbs 0 oz"
                                            Else
                                                objPackageDetail.Weight = Convert.ToString(Decimal.Truncate(Convert.ToDecimal(drPack("fltContainerWeight")))) + " lbs " + Convert.ToInt64((Convert.ToDecimal(drPack("fltContainerWeight")) - Decimal.Truncate(Convert.ToDecimal(drPack("fltContainerWeight")))) * 16).ToString() + " oz"
                                            End If
                                        Else
                                            objPackageDetail.Weight = Convert.ToString(Convert.ToDecimal(drPack("fltContainerWeight")))
                                        End If

                                        ezRates.Packages.Add(objPackageDetail)
                                    Next

                                    ezRates.TotalWeight = Convert.ToString(totalWeight)
                                Else
                                    ezRates.Packages.Add(New nsoftware.InShip.PackageDetail())
                                    ezRates.Packages(0).PackagingType = nsoftware.InShip.TPackagingTypes.ptYourPackaging
                                    ezRates.Packages(0).Length = 0
                                    ezRates.Packages(0).Width = 0
                                    ezRates.Packages(0).Height = 0
                                    ezRates.Packages(0).Girth = 0
                                    If (ezRates.Provider = nsoftware.InShip.EzratesProviders.pUSPS) Then
                                        Dim intWeight As Integer

                                        If Integer.TryParse(Convert.ToString(totalWeight), intWeight) Then
                                            ezRates.Packages(0).Weight = Convert.ToString(intWeight) + " lbs 0 oz"
                                        Else
                                            ezRates.Packages(0).Weight = Convert.ToString(Decimal.Truncate(Convert.ToDecimal(totalWeight))) + " lbs " + Convert.ToInt64((Convert.ToDecimal(totalWeight) - Decimal.Truncate(Convert.ToDecimal(totalWeight))) * 16).ToString() + " oz"
                                        End If
                                    Else
                                        ezRates.Packages(0).Weight = Convert.ToString(totalWeight)
                                    End If

                                    ezRates.TotalWeight = "0.0"
                                End If

                                Try
                                    ezRates.GetRates()

                                    If ezRates.Config("Warning") = "" Then
                                        Dim rate As Double = 0
                                        Dim transitTime As Integer = 0

                                        If ezRates.Services.Count > 0 Then
                                            If (ezRates.Services(0).AccountNetCharge <> "") AndAlso sRateType = 1 Then 'negotiated rate
                                                rate = Convert.ToDouble(ezRates.Services(0).AccountNetCharge)
                                            Else
                                                rate = Convert.ToDouble(ezRates.Services(0).ListNetCharge)
                                            End If

                                            If Convert.ToDouble(drShip("fltMarkup")) > 0 Then
                                                If Convert.ToBoolean(drShip("bitMarkupType")) Then 'Percentage
                                                    rate = rate + ((rate * Convert.ToDouble(drShip("fltMarkup"))) / 100)
                                                Else
                                                    rate = rate + Convert.ToDouble(drShip("fltMarkup"))
                                                End If
                                            End If

                                            drShip("ShippingAmount") = rate

                                            If Convert.ToString(ezRates.Services(0).TransitTime) = "ONE_DAY" Then
                                                transitTime = 1
                                            ElseIf Convert.ToString(ezRates.Services(0).TransitTime) = "TWO_DAYS" Then
                                                transitTime = 2
                                            ElseIf Convert.ToString(ezRates.Services(0).TransitTime) = "THREE_DAYS" Then
                                                transitTime = 3
                                            ElseIf Convert.ToString(ezRates.Services(0).TransitTime) = "FOUR_DAYS" Then
                                                transitTime = 4
                                            ElseIf Convert.ToString(ezRates.Services(0).TransitTime) = "FIVE_DAYS" Then
                                                transitTime = 5
                                            ElseIf Convert.ToString(ezRates.Services(0).TransitTime) = "SIX_DAYS" Then
                                                transitTime = 6
                                            ElseIf Convert.ToString(ezRates.Services(0).TransitTime) = "SEVEN_DAYS" Then
                                                transitTime = 7
                                            ElseIf Convert.ToString(ezRates.Services(0).TransitTime) = "EIGHT_DAYS" Then
                                                transitTime = 8
                                            ElseIf Convert.ToString(ezRates.Services(0).TransitTime) = "NINE_DAYS" Then
                                                transitTime = 9
                                            Else
                                                transitTime = -1
                                            End If

                                            drShip("TransitTime") = transitTime
                                            drShip("ShipByDate") = If(dtShipByDate Is Nothing, DateTime.Now.Date, dtShipByDate)
                                            drShip("AnticipateDelivery") = Convert.ToDateTime(If(dtShipByDate Is Nothing, DateTime.Now.Date, dtShipByDate)).AddDays(If(transitTime <> -1, transitTime, 0))
                                        Else
                                            drShip("IsShippingRuleValid") = False
                                        End If
                                    Else
                                        drShip("IsShippingRuleValid") = False
                                    End If
                                Catch ex As Exception
                                    drShip("IsShippingRuleValid") = False
                                End Try
                            Next
                            '#End Region
                            If dtShippingMethod.Select("IsShippingRuleValid=true").Length > 0 Then
                                Dim isShippingRateFound As Boolean = False
                                Dim shippingRate As Decimal = 0
                                Dim transitTime As Integer = 0
                                Dim shipService As String = ""

                                dtShippingMethod = dtShippingMethod.[Select]("IsShippingRuleValid=true").CopyToDataTable()

                                If tintType = 3 Then 'Always use cheapest Shipping Rate      
                                    dtShippingMethod.DefaultView.Sort = "ShippingAmount ASC"
                                    dtShippingMethod = dtShippingMethod.DefaultView.ToTable()

                                    isShippingRateFound = True
                                    shippingRate = Convert.ToDecimal(dtShippingMethod.Rows(0)("ShippingAmount"))
                                    shipService = Convert.ToString(dtShippingMethod.Rows(0)("vcServiceName"))
                                    transitTime = Convert.ToInt32(dtShippingMethod.Rows(0)("transitTime"))
                                ElseIf tintType = 2 Then 'Only use this Ship-Via or Ship-Via & Service
                                    isShippingRateFound = True
                                    shippingRate = Convert.ToDecimal(dtShippingMethod.Rows(0)("ShippingAmount"))
                                    shipService = Convert.ToString(dtShippingMethod.Rows(0)("vcServiceName"))
                                    transitTime = Convert.ToInt32(dtShippingMethod.Rows(0)("transitTime"))
                                Else
                                    If Not String.IsNullOrEmpty(vcPriorities) Then
                                        Dim arrPriorities() As String = vcPriorities.Split(CChar(","))

                                        For Each objPriority As String In arrPriorities
                                            If objPriority = "1" AndAlso Not expectedDate Is Nothing AndAlso dtShippingMethod.Select("TransitTime > 0").Length > 0 Then
                                                Dim dtShippingMethodTemp As DataTable = dtShippingMethod.[Select]("TransitTime > 0").CopyToDataTable()
                                                dtShippingMethodTemp.DefaultView.Sort = "TransitTime ASC"
                                                dtShippingMethodTemp = dtShippingMethodTemp.DefaultView.ToTable()

                                                For Each drShip As DataRow In dtShippingMethodTemp.Rows
                                                    If Convert.ToDateTime(drShip("AnticipateDelivery")).Date <= expectedDate Then
                                                        isShippingRateFound = True
                                                        shippingRate = Convert.ToDecimal(drShip("ShippingAmount"))
                                                        shipService = Convert.ToString(drShip("vcServiceName"))
                                                        transitTime = Convert.ToInt32(dtShippingMethod.Rows(0)("transitTime"))
                                                        Exit For
                                                    End If
                                                Next

                                                If isShippingRateFound Then
                                                    Exit For
                                                End If
                                            ElseIf objPriority = "2" AndAlso numOrderShipService > 0 AndAlso dtShippingMethod.Select("intNsoftEnum = " & numOrderShipService).Length > 0 Then
                                                isShippingRateFound = True
                                                shippingRate = Convert.ToDecimal(dtShippingMethod.Select("intNsoftEnum = " & numOrderShipService)(0)("ShippingAmount"))
                                                shipService = Convert.ToString(dtShippingMethod.Select("intNsoftEnum = " & numOrderShipService)(0)("vcServiceName"))
                                                transitTime = Convert.ToInt32(dtShippingMethod.Select("intNsoftEnum = " & numOrderShipService)(0)("transitTime"))
                                            ElseIf objPriority = "3" AndAlso numOrderShipVia > 0 AndAlso dtShippingMethod.Select("numShippingCompanyID = " & numOrderShipVia).Length > 0 Then
                                                isShippingRateFound = True
                                                shippingRate = Convert.ToDecimal(dtShippingMethod.Select("numShippingCompanyID = " & numOrderShipVia)(0)("ShippingAmount"))
                                                shipService = Convert.ToString(dtShippingMethod.Select("numShippingCompanyID = " & numOrderShipVia)(0)("vcServiceName"))
                                                transitTime = Convert.ToInt32(dtShippingMethod.Select("numShippingCompanyID = " & numOrderShipVia)(0)("transitTime"))
                                            ElseIf objPriority = "4" Then
                                                dtShippingMethod.DefaultView.Sort = "ShippingAmount ASC"
                                                dtShippingMethod = dtShippingMethod.DefaultView.ToTable()

                                                isShippingRateFound = True
                                                shippingRate = Convert.ToDecimal(dtShippingMethod.Rows(0)("ShippingAmount"))
                                                shipService = Convert.ToString(dtShippingMethod.Rows(0)("vcServiceName"))
                                                transitTime = Convert.ToInt32(dtShippingMethod.Rows(0)("transitTime"))
                                            End If
                                        Next
                                    End If
                                End If

                                If isShippingRateFound Then
                                    If isFromService Then
                                        Dim objOpportunities As New MOpportunity
                                        objOpportunities.DomainID = lngDomainID
                                        objOpportunities.OpportunityId = lngOppID
                                        objOpportunities.ReleaseDate = dtShipByDate
                                        objOpportunities.ShipFromLocation = lngWareHouseID
                                        objOpportunities.UnitPrice = shippingRate
                                        objOpportunities.Desc = shipService
                                        objOpportunities.AddAutoCalculatedShippingRate()
                                    Else
                                        Return New Tuple(Of Double, Integer, String, Boolean, String)(shippingRate, transitTime, shipService, True, "")
                                    End If
                                Else
                                    If isFromService Then
                                        Dim objOMSRE As New OpportunityMasterShippingRateError
                                        objOMSRE.DomainID = lngDomainID
                                        objOMSRE.OppID = lngOppID
                                        objOMSRE.ErrorMessage = "Haven't found suitable shipping rates."
                                        objOMSRE.Save()

                                        Return Nothing
                                    Else
                                        Return New Tuple(Of Double, Integer, String, Boolean, String)(0, 0, "", False, "Haven't found suitable shipping rates.")
                                    End If
                                End If
                            Else
                                If isFromService Then
                                    Dim objOMSRE As New OpportunityMasterShippingRateError
                                    objOMSRE.DomainID = lngDomainID
                                    objOMSRE.OppID = lngOppID
                                    objOMSRE.ErrorMessage = "Not able to fetch shipping rates."
                                    objOMSRE.Save()

                                    Return Nothing
                                Else
                                    Return New Tuple(Of Double, Integer, String, Boolean, String)(0, 0, "", False, "Not able to fetch shipping rates.")
                                End If
                            End If
                        End If
                    Else
                        If isFromService Then
                            Dim objOMSRE As New OpportunityMasterShippingRateError
                            objOMSRE.DomainID = lngDomainID
                            objOMSRE.OppID = lngOppID
                            objOMSRE.ErrorMessage = "Shipment weight must be greater than zero."
                            objOMSRE.Save()

                            Return Nothing
                        Else
                            Return New Tuple(Of Double, Integer, String, Boolean, String)(0, 0, "", False, "Shipment weight must be greater than zero.")
                        End If
                    End If
                Else
                    If isFromService Then
                        Dim objOMSRE As New OpportunityMasterShippingRateError
                        objOMSRE.DomainID = lngDomainID
                        objOMSRE.OppID = lngOppID
                        objOMSRE.ErrorMessage = "Ship-via and services are not set up from ""Global Settings | Shipping""."
                        objOMSRE.Save()

                        Return Nothing
                    Else
                        Return New Tuple(Of Double, Integer, String, Boolean, String)(0, 0, "", False, "Ship-via and services are not set up from ""Global Settings | Shipping"".")
                    End If
                End If
            Catch ex As Exception
                If isFromService Then
                    Dim objOMSRE As New OpportunityMasterShippingRateError
                    objOMSRE.DomainID = lngDomainID
                    objOMSRE.OppID = lngOppID
                    objOMSRE.ErrorMessage = ex.Message
                    objOMSRE.Save()

                    Return Nothing
                Else
                    Return New Tuple(Of Double, Integer, String, Boolean, String)(0, 0, "", False, ex.Message)
                End If
            End Try
        End Function

        Private Function GetOrderItemsForShippingRate(ByVal lngDomainID As Long, ByVal lngOppID As Long, ByVal lngWareHouseID As Long, ByVal dtShipByDate As Date?) As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim connString As String = BACRMBUSSLOGIC.BussinessLogic.GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", lngDomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", lngOppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseID", lngWareHouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@dtShipByDate", dtShipByDate, NpgsqlTypes.NpgsqlDbType.Date))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_GetOrderItemsForAutoCalculatedShippingRate", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

End Namespace