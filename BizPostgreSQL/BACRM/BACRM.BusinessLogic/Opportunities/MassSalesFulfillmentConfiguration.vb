﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports System.Data.SqlClient

Namespace BACRM.BusinessLogic.Opportunities

    Public Class MassSalesFulfillmentConfiguration
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property IsGroupByOrderForPick As Boolean
        Public Property IsGroupByOrderForShip As Boolean
        Public Property IsGroupByOrderForInvoice As Boolean
        Public Property IsGroupByOrderForPay As Boolean
        Public Property IsGroupByOrderForClose As Boolean
        Public Property InvoicingType As Short
        Public Property ScanValue As Short
        Public Property PackingMode As Short
        Public Property PendingCloseFilter As Short
        Public Property IsGeneratePickListByOrder As Boolean
        Public Property OrderStatusPicked As Long
        Public Property OrderStatusPacked1 As Long
        Public Property OrderStatusPacked2 As Long
        Public Property OrderStatusPacked3 As Long
        Public Property OrderStatusPacked4 As Long
        Public Property OrderStatusInvoiced As Long
        Public Property OrderStatusPaid As Long
        Public Property OrderStatusClosed As Long
        Public Property IsPickWithoutInventoryCheck As Boolean
        Public Property IsEnablePickListMapping As Boolean
        Public Property IsEnableFulfillmentBizDocMapping As Boolean
        Public Property IsAutoWarehouseSelection As Boolean
        Public Property IsBOOrderStatus As Boolean
        Public Property PackShipButtons As Short
        Public Property IsAutoGeneratePackingSlip As Boolean
#End Region

#Region "Public Methods"

        Public Function GetByDomain() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassSalesFulfillmentConfiguration_Get", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub Save()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrderForPick", IsGroupByOrderForPick, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrderForShip", IsGroupByOrderForShip, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrderForInvoice", IsGroupByOrderForInvoice, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrderForPay", IsGroupByOrderForPay, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrderForClose", IsGroupByOrderForClose, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@tintInvoicingType", InvoicingType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintScanValue", ScanValue, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintPackingMode", PackingMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintPendingCloseFilter", PendingCloseFilter, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@bitGeneratePickListByOrder", IsGeneratePickListByOrder, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatusPicked", OrderStatusPicked, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatusPacked1", OrderStatusPacked1, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatusPacked2", OrderStatusPacked2, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatusPacked3", OrderStatusPacked3, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatusPacked4", OrderStatusPacked4, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatusInvoiced", OrderStatusInvoiced, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatusPaid", OrderStatusPaid, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOrderStatusClosed", OrderStatusClosed, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitPickWithoutInventoryCheck", IsPickWithoutInventoryCheck, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitEnablePickListMapping", IsEnablePickListMapping, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitEnableFulfillmentBizDocMapping", IsEnableFulfillmentBizDocMapping, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitAutoWarehouseSelection", IsAutoWarehouseSelection, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitBOOrderStatus", IsBOOrderStatus, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@tintPackShipButtons", PackShipButtons, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@bitAutoGeneratePackingSlip", IsAutoGeneratePackingSlip, NpgsqlTypes.NpgsqlDbType.Boolean))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_MassSalesFulfillmentConfiguration_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetColumnValue(ByVal vcDbColumnName As String) As Object
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcDbColumnName", vcDbColumnName, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_MassSalesFulfillmentConfiguration_GetColumnValue", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

#End Region

    End Class

End Namespace

