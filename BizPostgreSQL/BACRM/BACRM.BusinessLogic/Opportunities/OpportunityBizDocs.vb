﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient



Namespace BACRM.BusinessLogic.Opportunities
    Public Class OpportunityBizDocs
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property DivisionID As Long 'Required
        Public Property Relationship As Long
        Public Property Profile As Long
        Public Property CompanyName As String 'Required
        Public Property OppID As Long 'Required
        Public Property OppType As Short 'Required
        Public Property OppBizDocID As Long
        Public Property BizDocType As Long 'Required
        Public Property ShipVia As Long 'Required
        Public Property FromDate As Date?
        Public Property ClientMachineUTCTimeOffset As Integer 'Required
        Public Property BizDocItems As String
        Public Property IsDeferred As Short
        Public Property BizDocStatus As Long
        Public Property BizDocSequenceId As String
        Public Property Comments As String
        Public Property PONumber As String
        Public Property IsRentalBizDoc As Boolean
        Public Property BizDocTemplateID As Long
        Public Property TrackingNo As String
        Public Property TrackingURL As String
        Public Property RefOrderNo As String
        Public Property ExchangeRate As Decimal
        Public Property VendorInvoiceName As String
        Public Property IsEnableDeferredIncome As Boolean 'Session("IsEnableDeferredIncome")
        Public Property DeferredBizDocID As Long
        Public Property DiscountAcntType As Long
        Public Property IsAutolinkUnappliedPayment As Boolean 'Session("AutolinkUnappliedPayment")
        Public Property ARAccountID As Long
        Public Property SourceBizDocID As Integer

#End Region

#Region "Public Methods"

        Public Function CreateBizDoc(ByVal IsValidateSequenceID As Boolean) As Long
            Try
                Dim objOppBizDocs As New OppBizDocs

                If IsValidateSequenceID AndAlso BizDocType > 0 AndAlso CCommon.ToLong(BizDocSequenceId) > 0 Then
                    objOppBizDocs.DomainID = DomainID
                    objOppBizDocs.OppId = OppID
                    objOppBizDocs.OppBizDocId = OppBizDocID
                    objOppBizDocs.BizDocId = BizDocType
                    objOppBizDocs.SequenceId = BizDocSequenceId
                    Dim dtSequenceID As DataTable = objOppBizDocs.ValidateBizDocSequenceId()

                    If dtSequenceID.Rows.Count > 0 Then
                        Throw New Exception("You must specify a different BizDoc Sequence #. <b>" & BizDocSequenceId & "</b> has already been used.")
                    End If
                End If


                'Dim lintAuthorizativeBizDocsId As Long
                'Dim objOppBizDocs As New OppBizDocs
                'Dim lintOpportunityType As Integer
                'Dim objJournal As New JournalEntry

                If BizDocType > 0 Then
                    objOppBizDocs.DomainID = DomainID
                    objOppBizDocs.OppId = OppID
                    Dim lintAuthorizativeBizDocsId As Long = objOppBizDocs.GetAuthorizativeOpportuntiy()

                    If lintAuthorizativeBizDocsId = BizDocType Then 'save bizdoc only if selected company's AR and AP account is mapped
                        If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", DomainID, DivisionID) = 0 Then
                            Throw New Exception("Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""  for " & CompanyName)
                        End If
                    End If
                    If OppType = 2 Then
                        'Accounting validation->Do not allow to save PO untill Default COGs account is mapped
                        If ChartOfAccounting.GetDefaultAccount("CG", DomainID) = 0 Then
                            Throw New Exception("Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts""")
                        End If

                        If ChartOfAccounting.GetDefaultAccount("PC", DomainID) = 0 Then
                            Throw New Exception("Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts""")
                        End If

                        If ChartOfAccounting.GetDefaultAccount("PV", DomainID) = 0 Then
                            Throw New Exception("Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts""")
                        End If
                    End If

                    Dim isAddBizDoc As Boolean = If(OppBizDocID = 0, True, False)

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    objOppBizDocs.BizDocId = BizDocType
                    objOppBizDocs.UserCntID = UserCntID
                    objOppBizDocs.Relationship = Relationship
                    objOppBizDocs.Profile = Profile
                    objOppBizDocs.ARAccountID = ARAccountID
                    objOppBizDocs.OppType = OppType
                    objOppBizDocs.bitPartialShipment = True
                    objOppBizDocs.ShipCompany = ShipVia
                    If FromDate Is Nothing Then
                        objOppBizDocs.FromDate = DateTime.UtcNow
                    Else
                        objOppBizDocs.FromDate = FromDate
                    End If

                    If BizDocTemplateID > 0 Then
                        objOppBizDocs.BizDocTemplateID = BizDocTemplateID
                    Else
                        objOppBizDocs.byteMode = 1
                        Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                        objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))
                    End If

                    objOppBizDocs.numSourceBizDocId = SourceBizDocID
                    objOppBizDocs.BizDocStatus = BizDocStatus
                    objOppBizDocs.SequenceId = BizDocSequenceId
                    objOppBizDocs.vcComments = Comments
                    objOppBizDocs.vcPONo = PONumber
                    objOppBizDocs.strBizDocItems = BizDocItems
                    objOppBizDocs.Deferred = IsDeferred
                    objOppBizDocs.ClientTimeZoneOffset = ClientMachineUTCTimeOffset
                    objOppBizDocs.boolRentalBizDoc = IsRentalBizDoc
                    objOppBizDocs.vcURL = TrackingURL
                    objOppBizDocs.TrackingNo = TrackingNo
                    objOppBizDocs.RefOrderNo = RefOrderNo
                    objOppBizDocs.fltExchangeRateBizDoc = ExchangeRate
                    If BizDocType = 305 Then
                        objOppBizDocs.VendorInvoiceName = VendorInvoiceName
                    End If

                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        Try
                            OppBizDocID = objOppBizDocs.SaveBizDoc()
                        Catch ex As Exception
                            If ex.Message = "NOT_ALLOWED" Then
                                Throw New Exception("To split order items multiple bizdocs you must create all bizdocs with ""partial"" checked!")
                            ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                                Throw New Exception("Only a Sales Order can create a Fulfillment Order")
                            ElseIf ex.Message = "AlreadyInvoice" Then
                                Throw New Exception("You can’t create a Invoice against a Fulfillment Order that already contains a Invoice")
                            ElseIf ex.Message = "AlreadyPackingSlip" Then
                                Throw New Exception("You can’t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip")
                            ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                                Throw New Exception("You can’t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip")
                            ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                                Throw New Exception("Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section")
                            ElseIf ex.Message = "RECURRENCE_ALREADY_CREATED_FOR_ORDER" Then
                                Throw New Exception("You can not set recurrence on bizdoc because someone has already created recurrence for this order.")
                            ElseIf ex.Message = "RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE" Then
                                Throw New Exception("You can not set recurrence on bizdoc because total quantity of selected items are used in authorative bizdocs and no quantity is available now for recurring bizdoc.")
                            ElseIf ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                                Throw New Exception("You can't ship at this time because Work Order for this Sales Order is pending.")
                            Else
                                Throw
                            End If
                        End Try

                        If OppBizDocID = 0 Then
                            Throw New Exception("A BizDoc by the same name is already created. Please select another BizDoc !")
                        End If

                        Dim JournalId As Long
                        'chkFromDate.Checked then Deferred : Not Create Account Entries
                        If (lintAuthorizativeBizDocsId = BizDocType AndAlso IsDeferred = 0) Or BizDocType = 304 Then
                            '' Authorizative Accounting BizDocs

                            ''--------------------------------------------------------------------------''
                            Dim ds As New DataSet
                            objOppBizDocs.OppBizDocId = OppBizDocID
                            ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                            Dim dtOppBiDocItems As DataTable = ds.Tables(0)

                            Dim objCalculateDealAmount As New CalculateDealAmount
                            objCalculateDealAmount.CalculateDealAmount(OppID, OppBizDocID, OppType, DomainID, dtOppBiDocItems)


                            If isAddBizDoc = False Then
                                'Create new journal id
                                JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=If(Not FromDate Is Nothing, CDate(FromDate), DateTime.UtcNow), Description:=CCommon.ToString(ds.Tables(1).Rows(0).Item("vcBizDocID")))
                            Else
                                'Get existing journal id and delete old and insert new journal entries
                                JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs
                                'When From COA someone deleted journals then we need to create new journal header entry
                                If JournalId = 0 Then
                                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=If(Not FromDate Is Nothing, CDate(FromDate), DateTime.UtcNow), Description:=CCommon.ToString(ds.Tables(1).Rows(0).Item("vcBizDocID")))
                                Else
                                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, lngJournalId:=JournalId, Entry_Date:=If(Not FromDate Is Nothing, CDate(FromDate), DateTime.UtcNow), Description:=CCommon.ToString(ds.Tables(1).Rows(0).Item("vcBizDocID")))
                                End If
                            End If

                            Dim objJournalEntries As New JournalEntry
                            If OppType = 1 Then
                                If BizDocType = 304 AndAlso CCommon.ToBool(IsEnableDeferredIncome) Then 'Deferred Revenue
                                    objJournalEntries.SaveJournalEntriesDeferredIncome(OppID, DomainID, dtOppBiDocItems, JournalId, DivisionID, OppBizDocID, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), objCalculateDealAmount.GrandTotal)
                                Else
                                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                        objJournalEntries.SaveJournalEntriesSales(OppID, DomainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, DivisionID, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), DiscAcntType:=DiscountAcntType, lngShippingMethod:=ShipVia, bitInvoiceAgainstDeferredIncomeBizDoc:=DeferredBizDocID > 0)
                                    Else
                                        objJournalEntries.SaveJournalEntriesSalesNew(OppID, DomainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, DivisionID, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), DiscAcntType:=DiscountAcntType, lngShippingMethod:=ShipVia, bitInvoiceAgainstDeferredIncomeBizDoc:=DeferredBizDocID > 0)
                                    End If

                                    If isAddBizDoc AndAlso IsAutolinkUnappliedPayment Then
                                        Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                                        objSalesFulfillment.DomainID = DomainID
                                        objSalesFulfillment.UserCntID = UserCntID
                                        objSalesFulfillment.ClientTimeZoneOffset = ClientMachineUTCTimeOffset
                                        objSalesFulfillment.OppId = OppID
                                        objSalesFulfillment.OppBizDocId = OppBizDocID

                                        objSalesFulfillment.AutoLinkUnappliedPayment()
                                    End If
                                End If
                            Else
                                If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                    objJournalEntries.SaveJournalEntriesPurchaseVariance(OppID, DomainID, dtOppBiDocItems, JournalId, DivisionID, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc")), DiscAcntType:=DiscountAcntType, lngShippingMethod:=ShipVia, vcBaseCurrency:=CCommon.ToString(ds.Tables(1).Rows(0).Item("vcBaseCurrency")), vcForeignCurrency:=CCommon.ToString(ds.Tables(1).Rows(0).Item("vcForeignCurrency")))
                                Else
                                    objJournalEntries.SaveJournalEntriesPurchase(OppID, DomainID, dtOppBiDocItems, JournalId, DivisionID, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), DiscAcntType:=DiscountAcntType, lngShippingMethod:=ShipVia)
                                End If
                            End If


                            ''---------------------------------------------------------------------------------
                        ElseIf BizDocType = 296 Then 'Fulfillment BizDoc
                            'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                            Dim objOppBizDoc As New OppBizDocs
                            objOppBizDoc.DomainID = DomainID
                            objOppBizDoc.UserCntID = UserCntID
                            objOppBizDoc.OppId = OppID
                            objOppBizDoc.OppBizDocId = OppBizDocID
                            objOppBizDoc.AutoFulfillBizDoc()


                            'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                            '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                            'CREATE SALES CLEARING ENTERIES
                            'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                            '-------------------------------------------------
                            Dim ds As New DataSet
                            Dim dtOppBiDocItems As DataTable

                            objOppBizDocs.OppBizDocId = OppBizDocID
                            ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                            dtOppBiDocItems = ds.Tables(0)

                            Dim objCalculateDealAmount As New CalculateDealAmount
                            objCalculateDealAmount.CalculateDealAmount(OppID, OppBizDocID, 1, DomainID, dtOppBiDocItems)

                            'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                            JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                            Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                            If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                objJournalEntries.SaveJournalEnteriesSalesClearing(OppID, DomainID, dtOppBiDocItems, JournalId, DivisionID, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                            Else
                                objJournalEntries.SaveJournalEnteriesSalesClearingNew(OppID, DomainID, dtOppBiDocItems, JournalId, DivisionID, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                            End If
                            '---------------------------------------------------------------------------------
                        Else

                            'Distibute Total cost among bizdoc items and remove old cost when no cost specified
                            'For Auth sales bizdoc below code gets excuted after actual jouranl entries are created
                            Dim objOpp As New MOpportunity
                            objOpp.DomainID = DomainID
                            objOpp.CostCategoryID = 0
                            objOpp.OppBizDocID = OppBizDocID
                            objOpp.Mode = 1
                            objOpp.ManageEmbeddedCost()
                        End If

                        objTransactionScope.Complete()
                    End Using

                    'Added By Sachin Sadhu||Date:29thApril12014
                    'Purpose :To Added Opportunity data in work Flow queue based on created Rules
                    '          Using Change tracking
                    Dim objWfA As New Workflow.Workflow()
                    objWfA.DomainID = DomainID
                    objWfA.UserCntID = UserCntID
                    objWfA.RecordID = OppBizDocID
                    objWfA.SaveWFBizDocQueue()
                    'end of code
                Else
                    Throw New Exception("Bizdoc Type is required")
                End If

                If OppBizDocID > 0 AndAlso BizDocType = 296 Then
                    Try
                        objOppBizDocs.CreateInvoiceFromFulfillmentOrder(DomainID, UserCntID, OppID, OppBizDocID, ClientMachineUTCTimeOffset)
                    Catch ex As Exception
                        'DO NOT THROW ERROR
                    End Try
                End If

                Return OppBizDocID
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetBizDocDetailForBluepayPayment() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppBizDocsId", OppBizDocID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                Return SqlDAL.ExecuteScalar(connString, "USP_OpportunityBizDocs_GetBizDocDetailForBluepayPayment", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function
#End Region

    End Class
End Namespace
