'Created Anoop Jayaraj
Option Explicit On
Option Strict On

Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.Opportunities
    Public Class OppInvoice
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _OppBizDocId As Long
        Private _BizDocId As Long
        Private _OppId As Long
        Private _PurChaseOdrNo As String
        Private _Discount As Decimal
        Private _AmtPaid As Decimal
        Private _Terms As Long
        Private _Comments As String
        Private _ContactID As Long
        Private _PaymentMethod As Int32
        Private _DepositTo As Integer
        'Private DomainId As Integer
        Private _Reference As String
        Private _Memo As String
        Private _IntegratedToAcnt As Boolean
        Private _BizDocsPaymentDetId As Long
        Private _DeferredIncome As Boolean
        Private _DeferredIncomePeriod As Int16
        Private _DeferredIncomeStartDate As Date = New Date(1753, 1, 1)
        Private _ContractId As Long
        Private _bitDeferredIncomeSales As Boolean
        Private _bitDisable As Boolean
        'Private UserCntID As Long
        Private _DivisionID As Long
        Private _ExpenseAcount As Long
        Private _DueDate As Date
        Private _CurrencyID As Long

        Private _lngBillID As Long
        Public Property BillID() As Long
            Get
                Return _lngBillID
            End Get
            Set(ByVal value As Long)
                _lngBillID = value
            End Set
        End Property


        Private _lngAccountID As Long
        Public Property AccountID() As Long
            Get
                Return _lngAccountID
            End Get
            Set(ByVal value As Long)
                _lngAccountID = value
            End Set
        End Property

        Private _dblTotalBillAmount As Double
        Public Property TotalBillAmount() As Double
            Get
                Return _dblTotalBillAmount
            End Get
            Set(ByVal value As Double)
                _dblTotalBillAmount = value
            End Set
        End Property

        Private _blIsPaid As Boolean
        Public Property IsPaid() As Boolean
            Get
                Return _blIsPaid
            End Get
            Set(ByVal value As Boolean)
                _blIsPaid = value
            End Set
        End Property

        Private _strItems As String
        Public Property StrItems() As String
            Get
                Return _strItems
            End Get
            Set(ByVal value As String)
                _strItems = value
            End Set
        End Property

        Private _dblAmountDue As Double
        Public Property AmountDue() As Double
            Get
                Return _dblAmountDue
            End Get
            Set(ByVal value As Double)
                _dblAmountDue = value
            End Set
        End Property

        Private _strDescription As String
        Public Property Description() As String
            Get
                Return _strDescription
            End Get
            Set(ByVal value As String)
                _strDescription = value
            End Set
        End Property

        Private _dtBillDate As Date
        Public Property BillDate() As Date
            Get
                Return _dtBillDate
            End Get
            Set(ByVal value As Date)
                _dtBillDate = value
            End Set
        End Property


        Private _ProjectID As Long
        Public Property ProjectID() As Long
            Get
                Return _ProjectID
            End Get
            Set(ByVal value As Long)
                _ProjectID = value
            End Set
        End Property

        Private _LiabilityAccount As Long
        Public Property LiabilityAccount() As Long
            Get
                Return _LiabilityAccount
            End Get
            Set(ByVal value As Long)
                _LiabilityAccount = value
            End Set
        End Property


        Public Property CurrencyID() As Long
            Get
                Return _CurrencyID
            End Get
            Set(ByVal value As Long)
                _CurrencyID = value
            End Set
        End Property

        Private _CardTypeID As Long
        Public Property CardTypeID() As Long
            Get
                Return _CardTypeID
            End Get
            Set(ByVal value As Long)
                _CardTypeID = value
            End Set
        End Property

        Private _EmbeddedCostID As Long
        Public Property EmbeddedCostID() As Long
            Get
                Return _EmbeddedCostID
            End Get
            Set(ByVal value As Long)
                _EmbeddedCostID = value
            End Set
        End Property

        Public Property DueDate() As Date
            Get
                Return _DueDate
            End Get
            Set(ByVal value As Date)
                _DueDate = value
            End Set
        End Property

        Public Property ExpenseAcount() As Long
            Get
                Return _ExpenseAcount
            End Get
            Set(ByVal value As Long)
                _ExpenseAcount = value
            End Set
        End Property

        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal value As Long)
                _DivisionID = value
            End Set
        End Property

        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Long)
        '        UserCntID = value
        '    End Set
        'End Property
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Long)
                _ContactID = Value
            End Set
        End Property

        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal Value As String)
                _Comments = Value
            End Set
        End Property

        Public Property Terms() As Long
            Get
                Return _Terms
            End Get
            Set(ByVal Value As Long)
                _Terms = Value
            End Set
        End Property

        Public Property AmtPaid() As Decimal
            Get
                Return _AmtPaid
            End Get
            Set(ByVal Value As Decimal)
                _AmtPaid = Value
            End Set
        End Property



        Public Property Discount() As Decimal
            Get
                Return _Discount
            End Get
            Set(ByVal Value As Decimal)
                _Discount = Value
            End Set
        End Property

        Public Property PurChaseOdrNo() As String
            Get
                Return _PurChaseOdrNo
            End Get
            Set(ByVal Value As String)
                _PurChaseOdrNo = Value
            End Set
        End Property

        Public Property BizDocId() As Long
            Get
                Return _BizDocId
            End Get
            Set(ByVal Value As Long)
                _BizDocId = Value
            End Set
        End Property

        Public Property OppBizDocId() As Long
            Get
                Return _OppBizDocId
            End Get
            Set(ByVal Value As Long)
                _OppBizDocId = Value
            End Set
        End Property

        Public Property OppId() As Long
            Get
                Return _OppId
            End Get
            Set(ByVal Value As Long)
                _OppId = Value
            End Set
        End Property

        Public Property PaymentMethod() As Int32
            Get
                Return _PaymentMethod
            End Get
            Set(ByVal Value As Int32)
                _PaymentMethod = Value
            End Set
        End Property

        Public Property DepositTo() As Integer
            Get
                Return _DepositTo
            End Get
            Set(ByVal Value As Integer)
                _DepositTo = Value
            End Set
        End Property

        'Public Property DomainId() As Integer
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Integer)
        '        DomainId = value
        '    End Set
        'End Property

        Public Property Reference() As String
            Get
                Return _Reference
            End Get
            Set(ByVal value As String)
                _Reference = value
            End Set
        End Property

        Public Property Memo() As String
            Get
                Return _Memo
            End Get
            Set(ByVal value As String)
                _Memo = value
            End Set
        End Property

        Public Property IntegratedToAcnt() As Boolean
            Get
                Return _IntegratedToAcnt
            End Get
            Set(ByVal value As Boolean)
                _IntegratedToAcnt = value
            End Set
        End Property

        Public Property BizDocsPaymentDetId() As Long
            Get
                Return _BizDocsPaymentDetId
            End Get
            Set(ByVal value As Long)
                _BizDocsPaymentDetId = value
            End Set
        End Property

        Public Property DeferredIncome() As Boolean
            Get
                Return _DeferredIncome
            End Get
            Set(ByVal value As Boolean)
                _DeferredIncome = value
            End Set
        End Property

        Public Property DeferredIncomePeriod() As Int16
            Get
                Return _DeferredIncomePeriod
            End Get
            Set(ByVal value As Int16)
                _DeferredIncomePeriod = value
            End Set
        End Property

        Public Property DeferredIncomeStartDate() As Date
            Get
                Return _DeferredIncomeStartDate
            End Get
            Set(ByVal value As Date)
                _DeferredIncomeStartDate = value
            End Set
        End Property

        Public Property ContractId() As Long
            Get
                Return _ContractId
            End Get
            Set(ByVal value As Long)
                _ContractId = value
            End Set
        End Property

        Public Property bitDeferredIncomeSales() As Boolean
            Get
                Return _bitDeferredIncomeSales
            End Get
            Set(ByVal value As Boolean)
                _bitDeferredIncomeSales = value
            End Set
        End Property



        Public Property bitDisable() As Boolean
            Get
                Return _bitDisable
            End Get
            Set(ByVal value As Boolean)
                _bitDisable = value
            End Set
        End Property

        Private _CardHolder As String
        Public Property CardHolder() As String
            Get
                Return _CardHolder
            End Get
            Set(ByVal value As String)
                _CardHolder = value
            End Set
        End Property

        Private _CreditCardNumber As String
        Public Property CreditCardNumber() As String
            Get
                Return _CreditCardNumber
            End Get
            Set(ByVal value As String)
                _CreditCardNumber = value
            End Set
        End Property

        Private _CVV2 As String
        Public Property CVV2() As String
            Get
                Return _CVV2
            End Get
            Set(ByVal value As String)
                _CVV2 = value
            End Set
        End Property

        'Private _CardType As Short
        'Public Property CardType() As Short
        '    Get
        '        Return _CardType
        '    End Get
        '    Set(ByVal value As Short)
        '        _CardType = value
        '    End Set
        'End Property

        Private _ValidMonth As Short
        Public Property ValidMonth() As Short
            Get
                Return _ValidMonth
            End Get
            Set(ByVal value As Short)
                _ValidMonth = value
            End Set
        End Property

        Private _ValidYear As Int16
        Public Property ValidYear() As Int16
            Get
                Return _ValidYear
            End Get
            Set(ByVal value As Int16)
                _ValidYear = value
            End Set
        End Property

        Private _PaymentType As BillPaymentType
        Public Property PaymentType() As BillPaymentType
            Get
                Return _PaymentType
            End Get
            Set(ByVal value As BillPaymentType)
                _PaymentType = value
            End Set
        End Property

        Private _ClassID As Long
        Public Property ClassID() As Long
            Get
                Return _ClassID
            End Get
            Set(ByVal value As Long)
                _ClassID = value
            End Set
        End Property


        Private _IsCardAuthorized As Boolean
        Public Property IsCardAutorized() As Boolean
            Get
                Return _IsCardAuthorized
            End Get
            Set(ByVal value As Boolean)
                _IsCardAuthorized = value
            End Set
        End Property

        Private _IsAmountCaptured As Boolean
        Public Property IsAmountCaptured() As Boolean
            Get
                Return _IsAmountCaptured
            End Get
            Set(ByVal value As Boolean)
                _IsAmountCaptured = value
            End Set
        End Property


        Private _CommissionID As Long
        Public Property CommissionID() As Long
            Get
                Return _CommissionID
            End Get
            Set(ByVal value As Long)
                _CommissionID = value
            End Set
        End Property
        Private _bitflag As Boolean
        Public Property bitflag As Boolean
            Get
                Return _bitflag
            End Get
            Set(ByVal value As Boolean)
                _bitflag = value
            End Set
        End Property
        Private _numCCInfoID As Long
        Public Property numCCInfoID As Long
            Get
                Return _numCCInfoID
            End Get
            Set(ByVal value As Long)
                _numCCInfoID = value
            End Set
        End Property
        ''' <summary>
        '''   --Payment Type
        '''1- Purchase Order(While BizDoc) 
        '''2- Bill(while Entered from Create Bill)  
        '''4- Bill (when Liability selected)
        '''3-Sales Returns(wihle comes from Sales return Queue)
        '''5-Commission Expense
        ''' </summary>
        ''' <remarks></remarks>
        Enum BillPaymentType
            Bill = 2
            BillAgainstLiability = 4
            CommissionExpense = 5
            Payroll = 6
        End Enum
        Private _IsDefault As Boolean
        Public Property IsDefault As Boolean
            Get
                Return _IsDefault
            End Get
            Set(ByVal value As Boolean)
                _IsDefault = value
            End Set
        End Property

        Private _strSignatureFile As String
        Public Property strSignatureFile() As String
            Get
                Return _strSignatureFile
            End Get
            Set(ByVal value As String)
                _strSignatureFile = value
            End Set
        End Property

        Private _IsLandedCost As Boolean
        Public Property IsLandedCost() As Boolean
            Get
                Return _IsLandedCost
            End Get
            Set(ByVal value As Boolean)
                _IsLandedCost = value
            End Set
        End Property

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region





        Public Function UpdateViewedBy() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString



                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppBizDocId


                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID


                SqlDAL.ExecuteNonQuery(connString, "USP_OppUpDateBizDocs_UpdateViewed", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function UpdateAmountPaid() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(20) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocsId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppBizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@UserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@amountPaid", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = _AmtPaid

                arParms(3) = New Npgsql.NpgsqlParameter("@numPaymentMethod", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _PaymentMethod

                arParms(4) = New Npgsql.NpgsqlParameter("@numDepoistToChartAcntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _DepositTo

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@vcReference", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(6).Value = _Reference

                arParms(7) = New Npgsql.NpgsqlParameter("@vcMemo", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _Memo

                arParms(8) = New Npgsql.NpgsqlParameter("@bitIntegratedToAcnt", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = _IntegratedToAcnt

                arParms(9) = New Npgsql.NpgsqlParameter("@numBizDocsPaymentDetId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Direction = ParameterDirection.InputOutput
                arParms(9).Value = _BizDocsPaymentDetId

                arParms(10) = New Npgsql.NpgsqlParameter("@bitDeferredIncome", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = _DeferredIncome

                arParms(11) = New Npgsql.NpgsqlParameter("@sintDeferredIncomePeriod", NpgsqlTypes.NpgsqlDbType.SmallInt)
                arParms(11).Value = _DeferredIncomePeriod

                arParms(12) = New Npgsql.NpgsqlParameter("@dtDeferredIncomeStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(12).Value = _DeferredIncomeStartDate

                arParms(13) = New Npgsql.NpgsqlParameter("@numContractId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(13).Value = _ContractId

                arParms(14) = New Npgsql.NpgsqlParameter("@bitSalesDeferredIncome", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(14).Value = _bitDeferredIncomeSales

                arParms(15) = New Npgsql.NpgsqlParameter("@bitDisable", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = _bitDisable

                arParms(16) = New Npgsql.NpgsqlParameter("@numCardTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = _CardTypeID

                arParms(17) = New Npgsql.NpgsqlParameter("@bitCardAuthorized", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(17).Value = _IsCardAuthorized

                arParms(18) = New Npgsql.NpgsqlParameter("@bitAmountCaptured", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(18).Value = _IsAmountCaptured

                arParms(19) = New Npgsql.NpgsqlParameter("@vcSignatureFile", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(19).Value = _strSignatureFile

                arParms(20) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(20).Value = Nothing
                arParms(20).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_OppUpDateBizDocs_AmountPaid", objParam, True)

                _BizDocsPaymentDetId = Common.CCommon.ToLong(DirectCast(objParam, Npgsql.NpgsqlParameter())(9).Value)

                If _BizDocsPaymentDetId > 0 Then
                    Return True
                Else
                    Return False
                End If
                'Return CBool(SqlDAL.ExecuteScalar(connString, "USP_OppUpDateBizDocs_AmountPaid", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function AddCustomerCreditCardInfo() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCardHolder", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(1).Value = _CardHolder

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCreditCardNo", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(2).Value = _CreditCardNumber

                arParms(3) = New Npgsql.NpgsqlParameter("@vcCVV2", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(3).Value = _CVV2

                arParms(4) = New Npgsql.NpgsqlParameter("@numCardTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _CardTypeID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintValidMonth", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _ValidMonth

                arParms(6) = New Npgsql.NpgsqlParameter("@intValidYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _ValidYear

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@bitIsDefault", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = _IsDefault

                SqlDAL.ExecuteNonQuery(connString, "USP_AddCustomerCreditCardInfo", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub SaveCustomerCreditCardInfo()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numCCInfoID", numCCInfoID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numContactId", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcCardHolder", _CardHolder, NpgsqlTypes.NpgsqlDbType.VarChar, 500))
                    .Add(SqlDAL.Add_Parameter("@vcCreditCardNo", _CreditCardNumber, NpgsqlTypes.NpgsqlDbType.VarChar, 500))
                    .Add(SqlDAL.Add_Parameter("@vcCVV2", _CVV2, NpgsqlTypes.NpgsqlDbType.VarChar, 200))
                    .Add(SqlDAL.Add_Parameter("@numCardTypeID", _CardTypeID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintValidMonth", _ValidMonth, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@intValidYear", _ValidYear, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numUserCntId", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitIsDefault", _IsDefault, NpgsqlTypes.NpgsqlDbType.Bit))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_CustomerCreditCardInfo_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetCustomerCreditCardInfo() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppBizDocId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = UserCntID

                arParms(3) = New Npgsql.NpgsqlParameter("@bitFlag", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _bitflag

                arParms(4) = New Npgsql.NpgsqlParameter("@numCCInfoID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _numCCInfoID

                arParms(5) = New Npgsql.NpgsqlParameter("@bitIsDefault", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = _IsDefault

                arParms(6) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _DivisionID

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCustomerCreditCardInfo", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCustomerCCInfo(ByVal Optional numCCInfoID As Integer = 0, ByVal Optional numContactId As Integer = 0, ByVal Optional isdefault As Integer = 0) As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCCInfoID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numCCInfoID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numContactId

                arParms(2) = New Npgsql.NpgsqlParameter("@bitdefault", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = isdefault

                arParms(3) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DivisionID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetCustomerCCInfo", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteCustomerCCInfo(ByVal numCCInfoID As Integer, ByVal numContactId As Integer) As Integer
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@CCInfoID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = numCCInfoID

                arParms(1) = New Npgsql.NpgsqlParameter("@ContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = numContactId

                Return SqlDAL.ExecuteNonQuery(connString, "USP_Delete_CustomerCCInfo", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Sub ManageCustomerCreditCardInfo()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _ContactID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcCardHolder", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(1).Value = _CardHolder

                arParms(2) = New Npgsql.NpgsqlParameter("@vcCreditCardNo", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParms(2).Value = _CreditCardNumber

                arParms(3) = New Npgsql.NpgsqlParameter("@vcCVV2", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(3).Value = _CVV2

                arParms(4) = New Npgsql.NpgsqlParameter("@numCardTypeID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _CardTypeID

                arParms(5) = New Npgsql.NpgsqlParameter("@tintValidMonth", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(5).Value = _ValidMonth

                arParms(6) = New Npgsql.NpgsqlParameter("@intValidYear", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _ValidYear

                arParms(7) = New Npgsql.NpgsqlParameter("@numUserCntId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@numCCInfoID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = _numCCInfoID

                arParms(9) = New Npgsql.NpgsqlParameter("@bitIsdefault", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = _IsDefault

                SqlDAL.ExecuteNonQuery(connString, "USP_ManageCustomerCreditCardInfo", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub DeleteCustomerCreditCardInfo()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numCCInfoID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _numCCInfoID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteCustomerCreditCardInfo", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function AddBillDetails() As Double
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(21) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numPaymentMethod", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _PaymentMethod

                arParms(1) = New Npgsql.NpgsqlParameter("@monAmount", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(1).Value = _AmtPaid

                arParms(2) = New Npgsql.NpgsqlParameter("@vcReference", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(2).Value = _Reference

                arParms(3) = New Npgsql.NpgsqlParameter("@vcMemo", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = _Memo

                arParms(4) = New Npgsql.NpgsqlParameter("@numExpenseAccount", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _ExpenseAcount

                arParms(5) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _DivisionID

                arParms(6) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = DomainID

                arParms(7) = New Npgsql.NpgsqlParameter("@UserContactId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@dtDueDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(8).Value = _DueDate

                arParms(9) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(9).Value = _CurrencyID

                arParms(10) = New Npgsql.NpgsqlParameter("@numBizDocsPaymentDetId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(10).Direction = ParameterDirection.InputOutput
                arParms(10).Value = _BizDocsPaymentDetId

                arParms(11) = New Npgsql.NpgsqlParameter("@fltExchangeRate", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(11).Direction = ParameterDirection.InputOutput
                arParms(11).Value = 0

                'EmbeddedCostID is Optional parameter
                arParms(12) = New Npgsql.NpgsqlParameter("@numEmbeddedCostID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = _EmbeddedCostID

                arParms(13) = New Npgsql.NpgsqlParameter("@tintPaymentType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _PaymentType

                arParms(14) = New Npgsql.NpgsqlParameter("@numLiabilityAccount", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _LiabilityAccount

                arParms(15) = New Npgsql.NpgsqlParameter("@numCommissionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _CommissionID

                arParms(16) = New Npgsql.NpgsqlParameter("@numOppBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(16).Value = _OppBizDocId

                arParms(17) = New Npgsql.NpgsqlParameter("@numClassID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _ClassID

                arParms(18) = New Npgsql.NpgsqlParameter("@numProjectID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(18).Value = _ProjectID

                arParms(19) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(19).Value = _ContactID

                arParms(20) = New Npgsql.NpgsqlParameter("@numTermsID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(20).Value = _Terms

                arParms(21) = New Npgsql.NpgsqlParameter("@dtBillDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(21).Value = _dtBillDate

                Dim objParam() As Object
                objParam = arParms.ToArray()
                SqlDAL.ExecuteNonQuery(connString, "USP_AddMoneyOutBill", objParam, True)
                _BizDocsPaymentDetId = Convert.ToInt64(DirectCast(objParam, Npgsql.NpgsqlParameter())(10).Value)

                Return CDbl(DirectCast(objParam, Npgsql.NpgsqlParameter())(11).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportunityBizDocsDetails() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numBizDocsPaymentDetId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _BizDocsPaymentDetId

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOpportunityBizDocsDetails", arParms)
                Return ds.Tables(0)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function ManageBillHeader() As Long
            Dim lngResult As Long = 0
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams

                    .Add(SqlDAL.Add_Parameter("@numBillID", _lngBillID, NpgsqlTypes.NpgsqlDbType.BigInt, 18, ParameterDirection.InputOutput))

                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numAccountID", _lngAccountID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@dtBillDate", _dtBillDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@numTermsID", _Terms, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numDueAmount", _dblAmountDue, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@dtDueDate", _DueDate, NpgsqlTypes.NpgsqlDbType.Timestamp))

                    .Add(SqlDAL.Add_Parameter("@vcMemo", _Memo, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@vcReference", _Reference, NpgsqlTypes.NpgsqlDbType.VarChar, 1000))

                    .Add(SqlDAL.Add_Parameter("@numBillTotalAmount", _dblTotalBillAmount, NpgsqlTypes.NpgsqlDbType.Numeric))

                    .Add(SqlDAL.Add_Parameter("@bitIsPaid", _blIsPaid, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@strItems", _strItems, NpgsqlTypes.NpgsqlDbType.VarChar))

                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@numOppId", _OppId, NpgsqlTypes.NpgsqlDbType.BigInt))

                    .Add(SqlDAL.Add_Parameter("@bitLandedCost", _IsLandedCost, NpgsqlTypes.NpgsqlDbType.Bit))

                    .Add(SqlDAL.Add_Parameter("@numCurrencyID", _CurrencyID, NpgsqlTypes.NpgsqlDbType.BigInt))

                End With

                Dim objParam() As Object = sqlParams.ToArray()
                CCommon.ToLong(SqlDAL.ExecuteNonQuery(connString, "USP_ManageBillHeader", objParam, True))
                _lngBillID = CCommon.ToLong(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)
            Catch ex As Exception
                lngResult = 0
                Throw ex
            End Try

            Return _lngBillID
        End Function

        Public Function FetchBillDetails() As DataSet
            Dim dsResult As New DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numBillID", _lngBillID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With
                dsResult = SqlDAL.ExecuteDataset(connString, "USP_GetBillDetails", sqlParams.ToArray())
            Catch ex As Exception
                dsResult = Nothing
                Throw ex
            End Try
            Return dsResult
        End Function

    End Class
End Namespace