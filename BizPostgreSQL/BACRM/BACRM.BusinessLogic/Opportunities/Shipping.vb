﻿'Created Anoop Jayaraj
Option Explicit On
Option Strict On

Imports BACRMAPI.DataAccessLayer
Imports nsoftware.InShip
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports System.Text
Imports System.Web.UI.WebControls
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.IO
Imports System.Drawing.Imaging

Namespace BACRM.BusinessLogic.Opportunities
    Public Class Shipping
        Inherits BACRM.BusinessLogic.CBusinessBase

        Public commCount As Integer = -1

#Region "PROPERTIES"

        'Private DomainId As Long
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal value As Long)
        '        DomainId = value
        '    End Set
        'End Property
        Private _SenderState As String
        Public Property SenderState() As String
            Get
                Return _SenderState
            End Get
            Set(ByVal value As String)
                _SenderState = value
            End Set
        End Property
        Private _SenderZipCode As String
        Public Property SenderZipCode() As String
            Get
                Return _SenderZipCode
            End Get
            Set(ByVal value As String)
                _SenderZipCode = value
            End Set
        End Property
        Private _SenderCountryCode As String
        Public Property SenderCountryCode() As String
            Get
                Return _SenderCountryCode
            End Get
            Set(ByVal value As String)
                _SenderCountryCode = value
            End Set
        End Property
        Private _RecepientState As String
        Public Property RecepientState() As String
            Get
                Return _RecepientState
            End Get
            Set(ByVal value As String)
                _RecepientState = value
            End Set
        End Property
        Private _RecepientZipCode As String
        Public Property RecepientZipCode() As String
            Get
                Return _RecepientZipCode
            End Get
            Set(ByVal value As String)
                _RecepientZipCode = value
            End Set
        End Property
        Private _RecepientCountryCode As String
        Public Property RecepientCountryCode() As String
            Get
                Return _RecepientCountryCode
            End Get
            Set(ByVal value As String)
                _RecepientCountryCode = value
            End Set
        End Property
        Private _UseDimentions As Boolean
        Public Property UseDimentions() As Boolean
            Get
                Return _UseDimentions
            End Get
            Set(ByVal value As Boolean)
                _UseDimentions = value
            End Set
        End Property
        Private _ServiceType As Short
        Public Property ServiceType() As Short
            Get
                Return _ServiceType
            End Get
            Set(ByVal value As Short)
                _ServiceType = value
            End Set
        End Property
        Private _ServiceTypeText As String
        Public Property ServiceTypeText() As String
            Get
                Return _ServiceTypeText
            End Get
            Set(ByVal value As String)
                _ServiceTypeText = value
            End Set
        End Property
        Private _DropoffType As Short
        Public Property DropoffType() As Short
            Get
                Return _DropoffType
            End Get
            Set(ByVal value As Short)
                _DropoffType = value
            End Set
        End Property
        Private _PackagingType As Short
        Public Property PackagingType() As Short
            Get
                Return _PackagingType
            End Get
            Set(ByVal value As Short)
                _PackagingType = value
            End Set
        End Property
        Private _Width As Double
        Public Property Width() As Double
            Get
                Return _Width
            End Get
            Set(ByVal Value As Double)
                _Width = Value
            End Set
        End Property
        Private _Length As Double
        Public Property Length() As Double
            Get
                Return _Length
            End Get
            Set(ByVal Value As Double)
                _Length = Value
            End Set
        End Property
        Private _Height As Double
        Public Property Height() As Double
            Get
                Return _Height
            End Get
            Set(ByVal Value As Double)
                _Height = Value
            End Set
        End Property
        Private _WeightInLbs As Double
        Public Property WeightInLbs() As Double
            Get
                Return _WeightInLbs
            End Get
            Set(ByVal Value As Double)
                _WeightInLbs = Value
            End Set
        End Property
        Private _WeightInOunce As Double
        Public Property WeightInOunce() As Double
            Get
                Return _WeightInOunce
            End Get
            Set(ByVal value As Double)
                _WeightInOunce = value
            End Set
        End Property

        Private _GroupCount As Integer
        Public Property GroupCount() As Integer
            Get
                Return _GroupCount
            End Get
            Set(ByVal value As Integer)
                _GroupCount = value
            End Set
        End Property
        Private _ItemCode As Long
        Public Property ItemCode() As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal value As Long)
                _ItemCode = value
            End Set
        End Property
        Private _NoOfPackages As Integer
        Public Property NoOfPackages() As Integer
            Get
                Return _NoOfPackages
            End Get
            Set(ByVal value As Integer)
                _NoOfPackages = value
            End Set
        End Property
        Private _ErrorMsg As String
        Public Property ErrorMsg() As String
            Get
                Return _ErrorMsg
            End Get
            Set(ByVal value As String)
                _ErrorMsg = value
            End Set
        End Property
        Private _ShippingLabelFileName As String
        Public Property ShippingLabelFileName() As String
            Get
                Return _ShippingLabelFileName
            End Get
            Set(ByVal value As String)
                _ShippingLabelFileName = value
            End Set
        End Property
        Private _InvoiceNo As String
        Public Property InvoiceNo() As String
            Get
                Return _InvoiceNo
            End Get
            Set(ByVal value As String)
                _InvoiceNo = value
            End Set
        End Property
        Private _MasterTrackingNumber As String
        Public Property MasterTrackingNumber() As String
            Get
                Return _MasterTrackingNumber
            End Get
            Set(ByVal value As String)
                _MasterTrackingNumber = value
            End Set
        End Property
        Private _TrackingNumber As New System.Collections.ArrayList
        Public Property TrackingNumber() As System.Collections.ArrayList
            Get
                Return _TrackingNumber
            End Get
            Set(ByVal value As System.Collections.ArrayList)
                _TrackingNumber = value
            End Set
        End Property
        Private _ShippingReportItemId As Long
        Public Property ShippingReportItemId() As Long
            Get
                Return _ShippingReportItemId
            End Get
            Set(ByVal value As Long)
                _ShippingReportItemId = value
            End Set
        End Property
        Private _OpportunityId As Long
        Public Property OpportunityId() As Long
            Get
                Return _OpportunityId
            End Get
            Set(ByVal value As Long)
                _OpportunityId = value
            End Set
        End Property
        Private _OppBizDocId As Long
        Public Property OppBizDocId() As Long
            Get
                Return _OppBizDocId
            End Get
            Set(ByVal value As Long)
                _OppBizDocId = value
            End Set
        End Property
        'Private UserCntID As Long
        'Public Property UserCntID() As Long
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal value As Long)
        '        UserCntID = value
        '    End Set
        'End Property
        Private _ShippingReportId As Long
        Public Property ShippingReportId() As Long
            Get
                Return _ShippingReportId
            End Get
            Set(ByVal value As Long)
                _ShippingReportId = value
            End Set
        End Property
        Private _ID As Integer
        Public Property ID() As Integer
            Get
                Return _ID
            End Get
            Set(ByVal value As Integer)
                _ID = value
            End Set
        End Property
        Private _shippingCompanyID As Integer
        Private _Provider As Integer
        Public Property Provider() As Integer
            Get
                Return _Provider
            End Get
            Set(ByVal value As Integer)
                _shippingCompanyID = value
                If (value = 88) Then 'for UPS
                    _Provider = 1
                ElseIf (value = 90) Then 'for USPS
                    _Provider = 2
                ElseIf (value = 91) Then 'for FedEx
                    _Provider = 0
                Else
                    _Provider = 0
                End If
            End Set
        End Property

        Private _BoxID As Long
        Public Property BoxID() As Long
            Get
                Return _BoxID
            End Get
            Set(ByVal value As Long)
                _BoxID = value
            End Set
        End Property

        Private _OppBizDocItemID As Long
        Public Property OppBizDocItemID() As Long
            Get
                Return _OppBizDocItemID
            End Get
            Set(ByVal value As Long)
                _OppBizDocItemID = value
            End Set
        End Property
        Private _PayorCountryCode As String
        Public Property PayorCountryCode() As String
            Get
                Return _PayorCountryCode
            End Get
            Set(ByVal value As String)
                _PayorCountryCode = value
            End Set
        End Property
        Private _PayorZipCode As String
        Public Property PayorZipCode() As String
            Get
                Return _PayorZipCode
            End Get
            Set(ByVal value As String)
                _PayorZipCode = value
            End Set
        End Property
        Private _PayorType As Short
        Public Property PayorType() As Short
            Get
                Return _PayorType
            End Get
            Set(ByVal value As Short)
                _PayorType = value
            End Set
        End Property
        Private _PayorAccountNo As String
        Public Property PayorAccountNo() As String
            Get
                Return _PayorAccountNo
            End Get
            Set(ByVal value As String)
                _PayorAccountNo = value
            End Set
        End Property
        Private _RefereceNo As String
        ''' <summary>
        ''' Custom Reference Number to be printed on shipping label
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ReferenceNo() As String
            Get
                Return _RefereceNo
            End Get
            Set(ByVal value As String)
                _RefereceNo = value
            End Set
        End Property
        Private _SignatureType As Short
        Public Property SignatureType() As Short
            Get
                Return _SignatureType
            End Get
            Set(ByVal value As Short)
                _SignatureType = value
            End Set
        End Property

        Private _intShipperSpecialServices As Long
        Public Property ShipperSpecialServices() As Long
            Get
                Return _intShipperSpecialServices
            End Get
            Set(ByVal value As Long)
                _intShipperSpecialServices = value
            End Set
        End Property
        Private _dblTotalInsuredValue As Double
        Public Property TotalInsuredValue() As Double
            Get
                Return _dblTotalInsuredValue
            End Get
            Set(ByVal value As Double)
                _dblTotalInsuredValue = value
            End Set
        End Property
        Private _sConfigRateType As Short
        Public Property ConfigRateType() As Short
            Get
                Return _sConfigRateType
            End Get
            Set(ByVal value As Short)
                _sConfigRateType = value
            End Set
        End Property
        Private _CODAmount As String
        Public Property CODAmount() As String
            Get
                Return _CODAmount
            End Get
            Set(ByVal value As String)
                _CODAmount = value
            End Set
        End Property
        Private _strCODType As String
        Public Property CODType() As String
            Get
                Return _strCODType
            End Get
            Set(ByVal value As String)
                _strCODType = value
            End Set
        End Property
        Private _dblTotalCustomsValue As Double
        Public Property TotalCustomsValue() As Double
            Get
                Return _dblTotalCustomsValue
            End Get
            Set(ByVal value As Double)
                _dblTotalCustomsValue = value
            End Set
        End Property
        Private _IsGetRates As Boolean
        Public Property IsGetRates() As Boolean
            Get
                Return _IsGetRates
            End Get
            Set(ByVal value As Boolean)
                _IsGetRates = value
            End Set
        End Property

#End Region

#Region "GLOBAL VARIABLES "

        Dim PkgList As ArrayList
        Dim BoxList As ArrayList
        Dim ShipLabelImageList As ArrayList
        'Dim strTrackingNumbers As New StringBuilder

#End Region

#Region "PRIVATE/PUBLIC VARIABLES"

        Public Function GetRates(Optional ByVal IsInternational As Boolean = False) As DataTable
            Try
                If IsInternational = False Then
                    Dim Rates1 As New nsoftware.InShip.Ezrates
                    GetPrerequisite(Rates1)

                    Rates1.SenderAddress.State = SenderState
                    Rates1.SenderAddress.ZipCode = SenderZipCode
                    Rates1.SenderAddress.CountryCode = SenderCountryCode

                    Rates1.RecipientAddress.State = RecepientState
                    Rates1.RecipientAddress.ZipCode = RecepientZipCode
                    Rates1.RecipientAddress.CountryCode = RecepientCountryCode

                    Rates1.RequestedService = CType(_ServiceType, ServiceTypes)

                    If (Rates1.RequestedService = ServiceTypes.stFedExGroundHomeDelivery) Then
                        Rates1.RecipientAddress.AddressFlags = &H2
                    End If
                    'Rates1.DropoffType = CType(_DropoffType, RatesDropoffTypes)
                    If Rates1.Packages.Count > 0 Then
                        Rates1.Packages(0).PackagingType = TPackagingTypes.ptYourPackaging
                    End If

                    Rates1.TotalWeight = WeightInLbs.ToString()
                    'GetSpecialServices()'e.g.COD,Hold At Location
                    GetOptionalServices(Rates1)
                    Try
                        _ErrorMsg = ""
                        Rates1.GetRates()
                    Catch ex As InShipException
                        _ErrorMsg = "<b><font color='red'>" & ex.Code.ToString() & ", Error: " & ex.Message & "</font></b>"
                    End Try
                    Return ListRates(Rates1)

                Else
                    Dim Rates1 As New nsoftware.InShip.Fedexrates
                    GetPrerequisiteFedexRates(Rates1)

                    Rates1.SenderAddress.State = SenderState
                    Rates1.SenderAddress.ZipCode = SenderZipCode
                    Rates1.SenderAddress.CountryCode = SenderCountryCode

                    Rates1.RecipientAddress.State = RecepientState
                    Rates1.RecipientAddress.ZipCode = RecepientZipCode
                    Rates1.RecipientAddress.CountryCode = RecepientCountryCode

                    Rates1.RequestedService = CType(_ServiceType, ServiceTypes)

                    If (Rates1.RequestedService = ServiceTypes.stFedExGroundHomeDelivery) Then
                        Rates1.RecipientAddress.AddressFlags = &H2
                    End If
                    'Rates1.DropoffType = CType(_DropoffType, RatesDropoffTypes)
                    'Rates1.PackagingType = CType(_PackagingType, RatesPackagingTypes)

                    'Rates1.TotalWeight = Weight.ToString()
                    'GetSpecialServices()'e.g.COD,Hold At Location

                    GetOptionalServicesForFedexShipIntl(Rates1)
                    Try
                        _ErrorMsg = ""
                        Rates1.GetRates()
                    Catch ex As InShipException
                        _ErrorMsg = "<b><font color='red'>" & ex.Code.ToString() & ", Error: " & ex.Message & "</font></b>"
                    End Try
                    Return ListRates(Rates1)

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub GetPrerequisite(ByRef Rates1 As Ezrates)
            Try
                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                Dim dsShippingDtl As DataSet
                objItem.DomainID = DomainID
                objItem.ShippingCMPID = _shippingCompanyID
                dsShippingDtl = objItem.ShippingDtls()

                Rates1.Provider = CType(_Provider, EzratesProviders)
                For Each dr As DataRow In dsShippingDtl.Tables(0).Rows
                    Select Case dr("vcFieldName").ToString()

                        'Case "Rate Type"
                        '    If Rates1.Provider = EzratesProviders.pFedEx Then
                        '        Rates1.Config("RateType=" & dr("vcShipFieldValue").ToString())
                        '    End If

                        Case "Developer Key"
                            Rates1.Account.DeveloperKey = dr("vcShipFieldValue").ToString()

                        Case "Account Number"
                            Rates1.Account.AccountNumber = dr("vcShipFieldValue").ToString()
                        Case "Meter Number"
                            Rates1.Account.MeterNumber = dr("vcShipFieldValue").ToString()
                        Case "Weight Unit"
                            If dr("vcShipFieldValue").ToString().ToLower().Equals("lbs") Then
                                Rates1.Config("WeightUnit=LB")
                            Else
                                Rates1.Config("WeightUnit=KG")
                            End If

                            'Common fields
                        Case "Password"
                            Rates1.Account.Password = dr("vcShipFieldValue").ToString()
                        Case "Server URL"
                            Rates1.Account.Server = dr("vcShipFieldValue").ToString()

                            'Fields specific to UPS
                        Case "AccessKey"
                            Rates1.Account.AccessKey = dr("vcShipFieldValue").ToString()
                        Case "UserID"
                            Rates1.Account.UserId = dr("vcShipFieldValue").ToString()
                        Case "ShipperNo"
                            Rates1.Account.AccountNumber = dr("vcShipFieldValue").ToString()

                    End Select
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetPrerequisiteFedexRates(ByRef Rates1 As Fedexrates)
            Try
                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                Dim dsShippingDtl As DataSet
                objItem.DomainID = DomainID
                objItem.ShippingCMPID = _shippingCompanyID
                dsShippingDtl = objItem.ShippingDtls()

                For Each dr As DataRow In dsShippingDtl.Tables(0).Rows
                    Select Case dr("vcFieldName").ToString()

                        Case "Developer Key"
                            Rates1.FedExAccount.DeveloperKey = dr("vcShipFieldValue").ToString()

                        Case "Account Number"
                            Rates1.FedExAccount.AccountNumber = dr("vcShipFieldValue").ToString()
                        Case "Meter Number"
                            Rates1.FedExAccount.MeterNumber = dr("vcShipFieldValue").ToString()
                        Case "Weight Unit"
                            If dr("vcShipFieldValue").ToString().ToLower().Equals("lbs") Then
                                Rates1.Config("WeightUnit=LB")
                            Else
                                Rates1.Config("WeightUnit=KG")
                            End If
                        Case "Password"
                            Rates1.FedExAccount.Password = dr("vcShipFieldValue").ToString()
                        Case "Server URL"
                            Rates1.FedExAccount.Server = dr("vcShipFieldValue").ToString()
                        Case "ShipperNo"
                            Rates1.FedExAccount.AccountNumber = dr("vcShipFieldValue").ToString()

                    End Select
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetOptionalServices(ByRef Rates1 As Ezrates)
            Try
                'when requesting rates for a specific service (ServiceType set to a value other than 'UNSPECIFIED'), you will have to add as many packages as there are in the MPS specifying the package detail for each package included in the shipment. 
                If _ServiceType > 0 Then
                    For i As Integer = 0 To _NoOfPackages - 1
                        Rates1.Packages.Add(New PackageDetail())
                        If UseDimentions Then
                            Rates1.Packages(i).PackagingType = TPackagingTypes.ptYourPackaging '6
                            Rates1.Packages(i).Width = Convert.ToInt32(_Width)
                            Rates1.Packages(i).Height = Convert.ToInt32(_Height)
                            Rates1.Packages(i).Length = Convert.ToInt32(_Length)

                        ElseIf _PackagingType = 31 Then
                            Rates1.Packages(i).PackagingType = TPackagingTypes.ptYourPackaging

                        End If

                        If (Rates1.Provider = EzratesProviders.pUSPS) Then
                            Dim intWeight As Integer

                            If Integer.TryParse(CCommon.ToString(WeightInLbs), intWeight) Then
                                Rates1.Packages(i).Weight = Convert.ToString(intWeight) + " lbs 0 oz"
                            Else
                                Rates1.Packages(i).Weight = CCommon.ToString(Decimal.Truncate(Convert.ToDecimal(WeightInLbs))) + " lbs " + Convert.ToInt64((Convert.ToDecimal(WeightInLbs) - Decimal.Truncate(Convert.ToDecimal(WeightInLbs))) * 16).ToString() + " oz"
                            End If
                        Else
                            Rates1.Packages(i).Weight = WeightInLbs.ToString()
                        End If
                    Next
                Else
                    Rates1.Packages.Add(New PackageDetail())
                    If UseDimentions Then
                        'Rates1.Packages(0).PackagingType = TPackagingTypes.ptYourPackaging '6
                        Rates1.Packages(0).Width = Convert.ToInt32(_Width)
                        Rates1.Packages(0).Height = Convert.ToInt32(_Height)
                        Rates1.Packages(0).Length = Convert.ToInt32(_Length)
                    ElseIf _PackagingType = 31 Then
                        Rates1.Packages(0).PackagingType = TPackagingTypes.ptYourPackaging

                    End If
                    If (Rates1.Provider = EzratesProviders.pUSPS) Then
                        Dim intWeight As Integer

                        If Integer.TryParse(CCommon.ToString(WeightInLbs), intWeight) Then
                            Rates1.Packages(0).Weight = Convert.ToString(intWeight) + " lbs 0 oz"
                        Else
                            Rates1.Packages(0).Weight = CCommon.ToString(Decimal.Truncate(Convert.ToDecimal(WeightInLbs))) + " lbs " + Convert.ToInt64((Convert.ToDecimal(WeightInLbs) - Decimal.Truncate(Convert.ToDecimal(WeightInLbs))) * 16).ToString() + " oz"
                        End If
                    Else
                        Rates1.Packages(0).Weight = WeightInLbs.ToString()
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetOptionalServicesForFedexShipIntl(ByRef Rates1 As Fedexrates)
            Try
                'when requesting rates for a specific service (ServiceType set to a value other than 'UNSPECIFIED'), you will have to add as many packages as there are in the MPS specifying the package detail for each package included in the shipment. 
                If _ServiceType > 0 Then
                    For i As Integer = 0 To _NoOfPackages - 1
                        Rates1.Packages.Add(New PackageDetail())
                        If UseDimentions Then
                            Rates1.Packages(i).PackagingType = TPackagingTypes.ptYourPackaging '6
                            Rates1.Packages(i).Width = Convert.ToInt32(_Width)
                            Rates1.Packages(i).Height = Convert.ToInt32(_Height)
                            Rates1.Packages(i).Length = Convert.ToInt32(_Length)
                        End If

                        Rates1.Packages(i).Weight = WeightInLbs.ToString()
                    Next
                Else
                    Rates1.Packages.Add(New PackageDetail())
                    If UseDimentions Then
                        'Rates1.Packages(0).PackagingType = TPackagingTypes.ptYourPackaging '6
                        Rates1.Packages(0).Width = Convert.ToInt32(_Width)
                        Rates1.Packages(0).Height = Convert.ToInt32(_Height)
                        Rates1.Packages(0).Length = Convert.ToInt32(_Length)
                    End If
                    Rates1.Packages(0).Weight = WeightInLbs.ToString()

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function ListRates(ByRef Rates1 As Ezrates) As DataTable
            Try
                Dim dtRates As New DataTable()
                Dim boolHasWarning As Boolean = False
                If Rates1.Provider <> EzratesProviders.pUSPS Then
                    If Rates1.Config("Warning") = "" Then
                        boolHasWarning = False
                    Else
                        boolHasWarning = True
                    End If
                End If

                If String.IsNullOrEmpty(_ErrorMsg) Then
                    If (boolHasWarning = False Or Rates1.Services.Count > 0) Then

                        dtRates.Columns.Add("ID", GetType(Integer))
                        dtRates.Columns.Add("numBoxID", GetType(Integer))
                        dtRates.Columns.Add("numItemCode")
                        dtRates.Columns.Add("GroupName")
                        dtRates.Columns.Add("Date")
                        dtRates.Columns.Add("ServiceType")
                        dtRates.Columns.Add("tintServiceType")
                        dtRates.Columns.Add("Rate")
                        dtRates.Columns.Add("intTotalWeight")
                        dtRates.Columns.Add("intNoOfBox")
                        dtRates.Columns.Add("fltHeight")
                        dtRates.Columns.Add("fltWidth")
                        dtRates.Columns.Add("fltLength")
                        dtRates.Columns.Add("numOppBizDocItemID")
                        dtRates.Columns.Add("TransitTIme")

                        Dim objItem As New BACRM.BusinessLogic.Item.CItems
                        Dim dsShippingDtl As DataSet
                        Dim sRateType As Short
                        objItem.DomainID = DomainID
                        objItem.ShippingCMPID = _shippingCompanyID
                        dsShippingDtl = objItem.ShippingDtls()
                        For Each drshipping As DataRow In dsShippingDtl.Tables(0).Rows
                            Select Case drshipping("vcFieldName").ToString()

                                Case "Rate Type"
                                    sRateType = CCommon.ToShort(drshipping("vcShipFieldValue"))
                            End Select
                        Next

                        Dim i As Integer
                        For i = 0 To Rates1.Services.Count - 1
                            'Rates1.ServiceIndex = i
                            Dim dr As DataRow
                            dr = dtRates.NewRow()

                            If (Rates1.Provider = EzratesProviders.pUPS) Then
                                dr("Date") = DateAdd(DateInterval.Day, CCommon.ToInteger(Rates1.Services(i).TransitTime), Today).ToShortDateString()
                            Else
                                dr("Date") = Rates1.Services(i).DeliveryDay + " " + Rates1.Services(i).DeliveryDate
                            End If
                            If Rates1.Provider = EzratesProviders.pUSPS Then
                                dr("ServiceType") = System.Web.HttpContext.Current.Server.HtmlDecode(Rates1.Services(i).ServiceTypeDescription) 'TranslateServiceType(Rates1.Services(i).TypeReturned) 'Rates1.Services(i).TypeReturnedDescription 'ServiceTypeText 'CType(cmbServiceType.SelectedIndex, EzratesServiceTypes)
                            Else
                                dr("ServiceType") = Rates1.Services(i).ServiceTypeDescription 'TranslateServiceType(Rates1.Services(i).TypeReturned) 'Rates1.Services(i).TypeReturnedDescription 'ServiceTypeText 'CType(cmbServiceType.SelectedIndex, EzratesServiceTypes)
                            End If

                            dr("tintServiceType") = CInt(Rates1.Services(i).ServiceType)

                            If (Rates1.Services(i).AccountNetCharge <> "") AndAlso sRateType = 1 Then 'negotiated rate
                                dr("Rate") = CCommon.ToDecimal(Rates1.Services(i).AccountNetCharge)
                            Else
                                dr("Rate") = CCommon.ToDecimal(Rates1.Services(i).ListNetCharge)
                            End If
                            dr("TransitTIme") = Rates1.Services(i).TransitTime
                            dr("GroupName") = "G" + GroupCount.ToString()
                            dr("ID") = ID
                            dr("numBoxID") = _BoxID
                            ID = ID + 1
                            dr("numItemCode") = _ItemCode
                            dr("intTotalWeight") = WeightInLbs
                            dr("intNoOfBox") = _NoOfPackages
                            dr("fltHeight") = Height
                            dr("fltWidth") = Width
                            dr("fltLength") = Length
                            dr("numOppBizDocItemID") = _OppBizDocItemID
                            dtRates.Rows.Add(dr)
                        Next i
                    Else
                        _ErrorMsg = "<b><font color='red'>" + Rates1.Config("Warning") + "</font></b>"
                    End If
                End If

                Return dtRates
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function ListRates(ByRef Rates1 As Fedexrates) As DataTable
            Try
                Dim dtRates As New DataTable()
                Dim boolHasWarning As Boolean = False
                'If Rates1.Provider <> EzratesProviders.pUSPS Then
                If Rates1.Config("Warning") = "" Then
                    boolHasWarning = False
                Else
                    boolHasWarning = True
                End If
                'End If

                If String.IsNullOrEmpty(_ErrorMsg) Then
                    If (boolHasWarning = False Or Rates1.Services.Count > 0) Then

                        dtRates.Columns.Add("ID", GetType(Integer))
                        dtRates.Columns.Add("numBoxID", GetType(Integer))
                        dtRates.Columns.Add("numItemCode")
                        dtRates.Columns.Add("GroupName")
                        dtRates.Columns.Add("Date")
                        dtRates.Columns.Add("ServiceType")
                        dtRates.Columns.Add("tintServiceType")
                        dtRates.Columns.Add("Rate")
                        dtRates.Columns.Add("intTotalWeight")
                        dtRates.Columns.Add("intNoOfBox")
                        dtRates.Columns.Add("fltHeight")
                        dtRates.Columns.Add("fltWidth")
                        dtRates.Columns.Add("fltLength")
                        dtRates.Columns.Add("numOppBizDocItemID")
                        dtRates.Columns.Add("TransitTIme")

                        Dim objItem As New BACRM.BusinessLogic.Item.CItems
                        Dim dsShippingDtl As DataSet
                        Dim sRateType As Short
                        objItem.DomainID = DomainID
                        objItem.ShippingCMPID = _shippingCompanyID
                        dsShippingDtl = objItem.ShippingDtls()
                        For Each drshipping As DataRow In dsShippingDtl.Tables(0).Rows
                            Select Case drshipping("vcFieldName").ToString()
                                Case "Rate Type"
                                    sRateType = CCommon.ToShort(drshipping("vcShipFieldValue"))
                            End Select
                        Next

                        Dim i As Integer
                        For i = 0 To Rates1.Services.Count - 1
                            'Rates1.ServiceIndex = i
                            Dim dr As DataRow
                            dr = dtRates.NewRow()

                            dr("tintServiceType") = CInt(Rates1.Services(i).ServiceType)
                            If (Rates1.Services(i).AccountNetCharge <> "") AndAlso sRateType = 1 Then 'negotiated rate
                                dr("Rate") = CCommon.ToDecimal(Rates1.Services(i).AccountNetCharge)
                            Else
                                dr("Rate") = CCommon.ToDecimal(Rates1.Services(i).ListNetCharge)
                            End If
                            dr("TransitTIme") = Rates1.Services(i).TransitTime
                            dr("GroupName") = "G" + GroupCount.ToString()
                            dr("ID") = ID
                            dr("numBoxID") = _BoxID
                            ID = ID + 1
                            dr("numItemCode") = _ItemCode
                            dr("intTotalWeight") = WeightInLbs
                            dr("intNoOfBox") = _NoOfPackages
                            dr("fltHeight") = Height
                            dr("fltWidth") = Width
                            dr("fltLength") = Length
                            dr("numOppBizDocItemID") = _OppBizDocItemID
                            dtRates.Rows.Add(dr)
                        Next i
                    Else
                        _ErrorMsg = "<b><font color='red'>" + Rates1.Config("Warning") + "</font></b>"
                    End If
                End If

                Return dtRates
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub GetShippingLabel(Optional ByVal intMode As Integer = 0, _
                                    Optional ByVal blnIsDomestic As Boolean = True, _
                                    Optional ByVal intProvider As Integer = 91)
            Try
                Dim xmlFilePath As String = CCommon.GetDocumentPhysicalPath(DomainID) & "Shipping"
                Dim strFileName As String = ""
                If Not Directory.Exists(xmlFilePath) Then
                    Directory.CreateDirectory(xmlFilePath)
                End If

                If blnIsDomestic = False AndAlso intProvider = 0 Then
                    Dim FedexShipComp As New Fedexshipintl
                    GetPrerequisiteForLabelFedexShipIntl(FedexShipComp)
                    GetAddressFedex(FedexShipComp)
                    BuildPackagesFedexShipIntl(FedexShipComp)
                    AddFedexCommodity(FedexShipComp)

                    Try
                        If _intShipperSpecialServices > 0 Then
                            FedexShipComp.ShipmentSpecialServices = _intShipperSpecialServices
                        End If

                        strFileName = CCommon.ToString(_OpportunityId) & "_" & CCommon.ToString(_ShippingReportId) & ".xml"
                        FedexShipComp.Timeout = 0
                        FedexShipComp.Config("PackageCount=" & FedexShipComp.Packages.Count.ToString())
                        FedexShipComp.GetShipmentLabels()

                        System.IO.File.WriteAllText(xmlFilePath & "\ShippingRequest_" & strFileName, FedexShipComp.Config("FullRequest"))
                        System.IO.File.WriteAllText(xmlFilePath & "\ShippingResponse_" & strFileName, FedexShipComp.Config("FullResponse"))

                        If _intImageWidth > 0 AndAlso _intImageHeight > 0 Then
                            Dim imgShipImage As FileInfo
                            Dim imgShipImageFile As File
                            Dim strShippingImage As String = ""
                            For i As Integer = 0 To FedexShipComp.Packages.Count - 1
                                strShippingImage = ShipLabelImageList(i).ToString()
                                If FedexShipComp.Packages(i).ShippingLabelFile.Length > 0 Then
                                    strShippingImage = FedexShipComp.Packages(i).ShippingLabelFile
                                    imgShipImage = New FileInfo(strShippingImage)

                                    If imgShipImage.Extension = ".jpg" OrElse imgShipImage.Extension = ".jpeg" OrElse imgShipImage.Extension = ".png" OrElse imgShipImage.Extension = ".gif" Then
                                        Dim imgBytes As Byte() = File.ReadAllBytes(strShippingImage)
                                        ResizeImageFile(strShippingImage, imgBytes)
                                    End If
                                End If
                            Next

                        End If
                    Catch ex As InShipException
                        _ErrorMsg = "<b><font color='red'>" & "Error: " & ex.Message & "</font></b>"
                        System.IO.File.WriteAllText(xmlFilePath & "\ShippingRequestError_" & strFileName, FedexShipComp.Config("FullRequest"))
                        System.IO.File.WriteAllText(xmlFilePath & "\ShippingResponseError_" & strFileName, FedexShipComp.Config("FullResponse"))
                        Exit Sub
                    End Try

                    If intMode = 0 Then
                        _MasterTrackingNumber = FedexShipComp.MasterTrackingNumber
                        For i As Integer = 0 To FedexShipComp.Packages.Count - 1
                            _TrackingNumber.Add(FedexShipComp.Packages(i).TrackingNumber)
                        Next
                        SaveTrackingNumberFedexShipIntl(FedexShipComp)
                    Else
                        _TrackingNumber.Add(FedexShipComp.Packages(0).TrackingNumber)
                        SaveTrackingNumberFedexShipIntl(FedexShipComp)
                        'strTrackingNumbers.Append(ezship1.Packages(0).TrackingNumber & ",")

                        FedexShipComp = Nothing
                        PkgList = Nothing
                        BoxList = Nothing
                        ShipLabelImageList = Nothing
                    End If
                Else
                    Dim EzShipComp As New Ezship
                    GetPrerequisiteForLabelEzShip(EzShipComp)
                    GetConfig(EzShipComp)
                    GetAddressEzShip(EzShipComp)
                    BuildPackagesEzShip(EzShipComp)

                    If _intShipperSpecialServices > 0 Then
                        EzShipComp.ShipmentSpecialServices = _intShipperSpecialServices
                    End If

                    Try
                        strFileName = CCommon.ToString(_OpportunityId) & "_" & CCommon.ToString(_ShippingReportId) & ".xml"
                        EzShipComp.Timeout = 0
                        If EzShipComp.Provider = EzshipProviders.pFedEx Then
                            EzShipComp.Config("PackageCount=" & EzShipComp.Packages.Count.ToString())
                        End If
                        EzShipComp.GetShipmentLabels()

                        System.IO.File.WriteAllText(xmlFilePath & "\ShippingRequest_" & strFileName, EzShipComp.Config("FullRequest"))
                        System.IO.File.WriteAllText(xmlFilePath & "\ShippingResponse_" & strFileName, EzShipComp.Config("FullResponse"))

                        If EzShipComp.LabelImageType <> EzshipLabelImageTypes.itZPL AndAlso EzShipComp.LabelImageType <> EzshipLabelImageTypes.itZebra AndAlso _intImageWidth > 0 AndAlso _intImageHeight > 0 Then
                            Dim imgShipImage As FileInfo
                            Dim imgShipImageFile As File
                            Dim strShippingImage As String = ""
                            For i As Integer = 0 To EzShipComp.Packages.Count - 1
                                strShippingImage = ShipLabelImageList(i).ToString()
                                If EzShipComp.Packages(i).ShippingLabelFile.Length > 0 Then
                                    strShippingImage = EzShipComp.Packages(i).ShippingLabelFile
                                    imgShipImage = New FileInfo(strShippingImage)

                                    If imgShipImage.Extension = ".jpg" OrElse imgShipImage.Extension = ".jpeg" OrElse imgShipImage.Extension = ".png" OrElse imgShipImage.Extension = ".gif" Then
                                        Dim imgBytes As Byte() = File.ReadAllBytes(strShippingImage)
                                        ResizeImageFile(strShippingImage, imgBytes)
                                    End If
                                End If
                            Next

                        End If
                    Catch ex As InShipException
                        _ErrorMsg = "<b><font color='red'>" & "Error: " & ex.Message & "</font></b>"
                        System.IO.File.WriteAllText(xmlFilePath & "\ShippingRequestError_" & strFileName, EzShipComp.Config("FullRequest"))
                        System.IO.File.WriteAllText(xmlFilePath & "\ShippingResponseError_" & strFileName, EzShipComp.Config("FullResponse"))
                        Exit Sub
                    End Try

                    If intMode = 0 Then
                        _MasterTrackingNumber = EzShipComp.MasterTrackingNumber
                        For i As Integer = 0 To EzShipComp.Packages.Count - 1
                            _TrackingNumber.Add(EzShipComp.Packages(i).TrackingNumber)
                        Next
                        SaveTrackingNumberEZShip(EzShipComp)
                    Else
                        _TrackingNumber.Add(EzShipComp.Packages(0).TrackingNumber)
                        SaveTrackingNumberEZShip(EzShipComp)
                        'strTrackingNumbers.Append(ezship1.Packages(0).TrackingNumber & ",")

                        EzShipComp = Nothing
                        PkgList = Nothing
                        BoxList = Nothing
                        ShipLabelImageList = Nothing
                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetPrerequisiteForLabelEzShip(ByRef ezship1 As Ezship)
            Try
                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                Dim dsShippingDtl As DataSet
                objItem.DomainID = DomainID
                objItem.ShippingCMPID = _shippingCompanyID
                dsShippingDtl = objItem.ShippingDtls()

                ezship1.Provider = CType(_Provider, EzshipProviders)
                For Each dr As DataRow In dsShippingDtl.Tables(0).Rows
                    Select Case CCommon.ToString(dr("vcFieldName"))
                        Case "Developer Key"
                            ezship1.Account.DeveloperKey = dr("vcShipFieldValue").ToString()

                        Case "Account Number"
                            ezship1.Account.AccountNumber = dr("vcShipFieldValue").ToString()

                        Case "Meter Number"
                            ezship1.Account.MeterNumber = dr("vcShipFieldValue").ToString()

                        Case "Weight Unit"
                            If dr("vcShipFieldValue").ToString().ToLower().Equals("lbs") Then
                                ezship1.Config("WeightUnit=LB")
                            Else
                                ezship1.Config("WeightUnit=KG")
                            End If

                            'Common fields
                        Case "Password"
                            ezship1.Account.Password = dr("vcShipFieldValue").ToString()

                        Case "Server URL"
                            'Fields specific to UPS
                            If ezship1.Provider <> EzshipProviders.pUPS Then
                                ezship1.Account.Server = dr("vcShipFieldValue").ToString()
                            End If

                        Case "AccessKey"
                            ezship1.Account.AccessKey = dr("vcShipFieldValue").ToString()

                        Case "UserID"
                            ezship1.Account.UserId = dr("vcShipFieldValue").ToString()
                            If ezship1.Provider = EzshipProviders.pUSPS Then
                                ezship1.Account.AccountNumber = dr("vcShipFieldValue").ToString()
                            End If

                        Case "ShipperNo"
                            ezship1.Account.AccountNumber = dr("vcShipFieldValue").ToString()

                        Case "Label Server URL" 'For UPS:shipping label url and Get Rate URL are different
                            ezship1.Account.Server = dr("vcShipFieldValue").ToString()

                        Case "Is Live Mode?"
                            If ezship1.Provider = EzshipProviders.pUSPS Then
                                If CCommon.ToString(dr("vcShipFieldValue")).ToLower() = "yes" Or CCommon.ToString(dr("vcShipFieldValue")).ToLower() = "true" Or CCommon.ToString(dr("vcShipFieldValue")).ToLower() = "1" Then
                                    ezship1.Config("Certify=False")
                                Else
                                    ezship1.Config("Certify=True") 'test mode
                                End If
                            End If

                        Case "Is Endicia"
                            _IsGetRates = False

                        Case "Endicia UserID"
                            If _IsGetRates = False AndAlso ezship1.Provider = EzshipProviders.pUSPS Then
                                ezship1.Account.AccountNumber = dr("vcShipFieldValue").ToString()
                                ezship1.Config("TestMode=false")
                            End If

                        Case "Endicia Password"
                            If _IsGetRates = False AndAlso ezship1.Provider = EzshipProviders.pUSPS Then
                                ezship1.Account.Password = dr("vcShipFieldValue").ToString()
                            End If

                        Case "Endicia Label Server URL"
                            If _IsGetRates = False AndAlso ezship1.Provider = EzshipProviders.pUSPS Then
                                ezship1.Account.Server = dr("vcShipFieldValue").ToString()
                            End If
                        Case "Shipping Label Type"
                            If _shippingCompanyID = 91 AndAlso dr("vcShipFieldValue").ToString() = "1" Then
                                ezship1.LabelImageType = EzshipLabelImageTypes.itZebra
                                ezship1.Config("LabelOrientationType=0")
                                ezship1.Config("LabelStockType=2")

                                If Not PkgList Is Nothing Then
                                    For Each objPackageDetail As PackageDetail In PkgList
                                        objPackageDetail.ShippingLabelFile = objPackageDetail.ShippingLabelFile.Replace(".png", ".zpl")
                                    Next
                                End If
                            ElseIf _shippingCompanyID = 91 Then
                                ezship1.LabelImageType = EzshipLabelImageTypes.itPNG
                            ElseIf _shippingCompanyID = 88 AndAlso dr("vcShipFieldValue").ToString() = "1" Then
                                ezship1.LabelImageType = EzshipLabelImageTypes.itZPL
                                ezship1.Config("LabelSize=0")

                                If Not PkgList Is Nothing Then
                                    For Each objPackageDetail As PackageDetail In PkgList
                                        objPackageDetail.ShippingLabelFile = objPackageDetail.ShippingLabelFile.Replace(".gif", ".zpl")
                                    Next
                                End If
                            ElseIf _shippingCompanyID = 90 AndAlso dr("vcShipFieldValue").ToString() = "1" Then
                                ezship1.LabelImageType = EzshipLabelImageTypes.itZPL
                                ezship1.Config("LabelSize=0")

                                If Not PkgList Is Nothing Then
                                    For Each objPackageDetail As PackageDetail In PkgList
                                        objPackageDetail.ShippingLabelFile = objPackageDetail.ShippingLabelFile.Replace(".gif", ".zpl")
                                    Next
                                End If
                            Else
                                ezship1.LabelImageType = EzshipLabelImageTypes.itGIF
                            End If
                    End Select
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetPrerequisiteForLabelFedexShipIntl(ByRef FedexShip1 As Fedexshipintl)
            Try
                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                Dim dsShippingDtl As DataSet
                objItem.DomainID = DomainID
                objItem.ShippingCMPID = _shippingCompanyID
                dsShippingDtl = objItem.ShippingDtls()

                For Each dr As DataRow In dsShippingDtl.Tables(0).Rows
                    Select Case dr("vcFieldName").ToString()
                        Case "Developer Key"
                            FedexShip1.FedExAccount.DeveloperKey = dr("vcShipFieldValue").ToString()
                        Case "Account Number"
                            FedexShip1.FedExAccount.AccountNumber = dr("vcShipFieldValue").ToString()
                        Case "Meter Number"
                            FedexShip1.FedExAccount.MeterNumber = dr("vcShipFieldValue").ToString()
                        Case "Weight Unit"
                            If dr("vcShipFieldValue").ToString().ToLower().Equals("lbs") Then
                                FedexShip1.Config("WeightUnit=LB")
                            Else
                                FedexShip1.Config("WeightUnit=KG")
                            End If
                            'Common fields
                        Case "Password"
                            FedexShip1.FedExAccount.Password = dr("vcShipFieldValue").ToString()
                        Case "Server URL"
                            'Fields specific to UPS
                            FedexShip1.FedExAccount.Server = dr("vcShipFieldValue").ToString()
                        Case "Shipping Label Type"
                            If dr("vcShipFieldValue").ToString() = "1" Then
                                FedexShip1.LabelImageType = FedexshipintlLabelImageTypes.fitZebra
                                FedexShip1.Config("LabelOrientationType=0")
                                FedexShip1.Config("LabelStockType=2")
                            Else
                                FedexShip1.LabelImageType = FedexshipintlLabelImageTypes.fitPNG
                            End If
                    End Select
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetPrerequisiteForLabelUPSShipIntl(ByRef UpsShip1 As Upsshipintl)
            Try
                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                Dim dsShippingDtl As DataSet
                objItem.DomainID = DomainID
                objItem.ShippingCMPID = _shippingCompanyID
                dsShippingDtl = objItem.ShippingDtls()

                For Each dr As DataRow In dsShippingDtl.Tables(0).Rows
                    Select Case dr("vcFieldName").ToString()
                        Case "AccessKey"
                            UpsShip1.UPSAccount.AccessKey = dr("vcShipFieldValue").ToString()
                        Case "Password"
                            UpsShip1.UPSAccount.Password = dr("vcShipFieldValue").ToString()
                        Case "Account Number"
                            UpsShip1.UPSAccount.AccountNumber = dr("vcShipFieldValue").ToString()
                        Case "UserID"
                            UpsShip1.UPSAccount.UserId = dr("vcShipFieldValue").ToString()
                        Case "Weight Unit"
                            If dr("vcShipFieldValue").ToString().ToLower().Equals("lbs") Then
                                UpsShip1.Config("WeightUnit=LB")
                            Else
                                UpsShip1.Config("WeightUnit=KG")
                            End If
                        Case "Server URL"
                            'Fields specific to UPS
                            UpsShip1.UPSAccount.Server = dr("vcShipFieldValue").ToString()
                    End Select
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetPrerequisiteForLabelUSPSShipIntl(ByRef USPSShip1 As Uspsshipintl)
            Try
                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                Dim dsShippingDtl As DataSet
                objItem.DomainID = DomainID
                objItem.ShippingCMPID = _shippingCompanyID
                dsShippingDtl = objItem.ShippingDtls()

                For Each dr As DataRow In dsShippingDtl.Tables(0).Rows
                    Select Case dr("vcFieldName").ToString()
                        Case "UserID"
                            USPSShip1.USPSAccount.UserId = dr("vcShipFieldValue").ToString()
                        Case "Password"
                            USPSShip1.USPSAccount.Password = dr("vcShipFieldValue").ToString()
                        Case "Server URL"
                            'Fields specific to UPS
                            USPSShip1.USPSAccount.Server = dr("vcShipFieldValue").ToString()
                    End Select
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetAddressEzShip(ByRef ezship1 As Ezship)
            Try
                With ezship1
                    Dim dtTable As DataTable

                    'Fetch following fields from shippingreport table 
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = DomainID
                    objOppBizDoc.OppBizDocId = _OppBizDocId
                    objOppBizDoc.ShippingReportId = _ShippingReportId
                    dtTable = New DataTable
                    dtTable = objOppBizDoc.GetShippingReport().Tables(0)
                    If dtTable.Rows.Count > 0 Then

                        .SenderContact.FirstName = dtTable.Rows(0).Item("vcFromName").ToString()
                        .SenderContact.LastName = ""
                        .SenderContact.Company = dtTable.Rows(0).Item("vcFromCompany").ToString()
                        .SenderContact.Phone = dtTable.Rows(0).Item("vcFromPhone").ToString().Trim().Replace("-", "") 'The maximum length is 10 for U.S. and CA else 16

                        .SenderAddress.State = dtTable.Rows(0).Item("vcFromState").ToString()
                        .SenderAddress.ZipCode = dtTable.Rows(0).Item("vcFromZip").ToString()
                        .SenderAddress.CountryCode = dtTable.Rows(0).Item("vcFromCountry").ToString() ' this is optional in new FedExAPI V2
                        .SenderAddress.City = dtTable.Rows(0).Item("vcFromCity").ToString() '.SenderAddress.Residential = chbFromResidential.Checked
                        .SenderAddress.Address1 = dtTable.Rows(0).Item("vcFromAddressLine1").ToString()
                        .SenderAddress.Address2 = dtTable.Rows(0).Item("vcFromAddressLine2").ToString()

                        .RecipientContact.FirstName = dtTable.Rows(0).Item("vcToName").ToString()
                        .RecipientContact.LastName = "" 'dtTable.Rows(0).Item("Name").ToString()
                        .RecipientContact.Company = dtTable.Rows(0).Item("vcToCompany").ToString()
                        If ezship1.Provider = EzshipProviders.pUSPS Then  'Supply a 10-digit phone number without punctuation.
                            .RecipientContact.Phone = dtTable.Rows(0).Item("vcToPhone").ToString().Trim().Replace("-", "")
                        Else
                            .RecipientContact.Phone = dtTable.Rows(0).Item("vcToPhone").ToString().Trim().Replace("-", "")
                        End If
                        .RecipientAddress.State = dtTable.Rows(0).Item("vcToState").ToString()
                        .RecipientAddress.ZipCode = dtTable.Rows(0).Item("vcToZip").ToString()
                        .RecipientAddress.CountryCode = dtTable.Rows(0).Item("vcToCountry").ToString()
                        .RecipientAddress.City = dtTable.Rows(0).Item("vcToCity").ToString()
                        .RecipientAddress.Address1 = dtTable.Rows(0).Item("vcToAddressLine1").ToString()
                        .RecipientAddress.Address2 = dtTable.Rows(0).Item("vcToAddressLine2").ToString()

                        If (ezship1.ServiceType = ServiceTypes.stFedExGroundHomeDelivery) Then
                            ezship1.RecipientAddress.AddressFlags = &H2
                        End If

                        ezship1.Payor.PayorType = CType(Short.Parse(dtTable.Rows(0).Item("tintPayorType").ToString()), TPayorTypes) 'TPayorTypes.ptSender
                        ezship1.Payor.AccountNumber = CCommon.ToString(dtTable.Rows(0).Item("vcPayorAccountNo"))
                        ezship1.Payor.CountryCode = CCommon.ToString(dtTable.Rows(0).Item("vcPayorCountryCode"))
                        ezship1.Payor.ZipCode = CCommon.ToString(dtTable.Rows(0).Item("vcPayorZip"))
                    End If
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetAddressFedex(ByRef FedexShip1 As Fedexshipintl)
            Try
                With FedexShip1
                    Dim dtTable As DataTable

                    'Fetch following fields from shippingreport table 
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = DomainID
                    objOppBizDoc.OppBizDocId = _OppBizDocId
                    objOppBizDoc.ShippingReportId = _ShippingReportId
                    dtTable = New DataTable
                    dtTable = objOppBizDoc.GetShippingReport().Tables(0)
                    If dtTable.Rows.Count > 0 Then

                        .SenderContact.FirstName = dtTable.Rows(0).Item("vcFromName").ToString()
                        .SenderContact.LastName = ""
                        .SenderContact.Company = dtTable.Rows(0).Item("vcFromCompany").ToString()
                        .SenderContact.Phone = dtTable.Rows(0).Item("vcFromPhone").ToString() 'The maximum length is 10 for U.S. and CA else 16

                        .SenderAddress.State = dtTable.Rows(0).Item("vcFromState").ToString()
                        .SenderAddress.ZipCode = dtTable.Rows(0).Item("vcFromZip").ToString()
                        .SenderAddress.CountryCode = dtTable.Rows(0).Item("vcFromCountry").ToString() ' this is optional in new FedExAPI V2
                        .SenderAddress.City = dtTable.Rows(0).Item("vcFromCity").ToString() '.SenderAddress.Residential = chbFromResidential.Checked
                        .SenderAddress.Address1 = dtTable.Rows(0).Item("vcFromAddressLine1").ToString()
                        .SenderAddress.Address2 = dtTable.Rows(0).Item("vcFromAddressLine2").ToString()

                        .RecipientContact.FirstName = dtTable.Rows(0).Item("vcToName").ToString()
                        .RecipientContact.LastName = "" 'dtTable.Rows(0).Item("Name").ToString()
                        .RecipientContact.Company = dtTable.Rows(0).Item("vcToCompany").ToString()
                        .RecipientContact.Phone = dtTable.Rows(0).Item("vcToPhone").ToString()
                        .RecipientAddress.State = dtTable.Rows(0).Item("vcToState").ToString()
                        .RecipientAddress.ZipCode = dtTable.Rows(0).Item("vcToZip").ToString()
                        .RecipientAddress.CountryCode = dtTable.Rows(0).Item("vcToCountry").ToString()
                        .RecipientAddress.City = dtTable.Rows(0).Item("vcToCity").ToString()
                        .RecipientAddress.Address1 = dtTable.Rows(0).Item("vcToAddressLine1").ToString()
                        .RecipientAddress.Address2 = dtTable.Rows(0).Item("vcToAddressLine2").ToString()

                        If (FedexShip1.ServiceType = ServiceTypes.stFedExGroundHomeDelivery) Then
                            FedexShip1.RecipientAddress.AddressFlags = &H2
                        End If

                        FedexShip1.Payor.PayorType = CType(Short.Parse(dtTable.Rows(0).Item("tintPayorType").ToString()), TPayorTypes) 'TPayorTypes.ptSender
                        FedexShip1.Payor.AccountNumber = CCommon.ToString(dtTable.Rows(0).Item("vcPayorAccountNo"))
                        FedexShip1.Payor.CountryCode = CCommon.ToString(dtTable.Rows(0).Item("vcPayorCountryCode"))
                        FedexShip1.Payor.ZipCode = CCommon.ToString(dtTable.Rows(0).Item("vcPayorZip"))
                    End If
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetAddressUPS(ByRef UpsShip1 As Upsshipintl)
            Try
                With UpsShip1
                    Dim dtTable As DataTable

                    'Fetch following fields from shippingreport table 
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = DomainID
                    objOppBizDoc.OppBizDocId = _OppBizDocId
                    objOppBizDoc.ShippingReportId = _ShippingReportId
                    dtTable = New DataTable
                    dtTable = objOppBizDoc.GetShippingReport().Tables(0)
                    If dtTable.Rows.Count > 0 Then

                        .SenderContact.FirstName = dtTable.Rows(0).Item("vcFromName").ToString()
                        .SenderContact.LastName = ""
                        .SenderContact.Company = dtTable.Rows(0).Item("vcFromCompany").ToString()
                        .SenderContact.Phone = dtTable.Rows(0).Item("vcFromPhone").ToString() 'The maximum length is 10 for U.S. and CA else 16

                        .SenderAddress.State = dtTable.Rows(0).Item("vcFromState").ToString()
                        .SenderAddress.ZipCode = dtTable.Rows(0).Item("vcFromZip").ToString()
                        .SenderAddress.CountryCode = dtTable.Rows(0).Item("vcFromCountry").ToString() ' this is optional in new FedExAPI V2
                        .SenderAddress.City = dtTable.Rows(0).Item("vcFromCity").ToString() '.SenderAddress.Residential = chbFromResidential.Checked
                        .SenderAddress.Address1 = dtTable.Rows(0).Item("vcFromAddressLine1").ToString()
                        .SenderAddress.Address2 = dtTable.Rows(0).Item("vcFromAddressLine2").ToString()

                        .RecipientContact.FirstName = dtTable.Rows(0).Item("vcToName").ToString()
                        .RecipientContact.LastName = "" 'dtTable.Rows(0).Item("Name").ToString()
                        .RecipientContact.Company = dtTable.Rows(0).Item("vcToCompany").ToString()
                        .RecipientContact.Phone = dtTable.Rows(0).Item("vcToPhone").ToString()
                        .RecipientAddress.State = dtTable.Rows(0).Item("vcToState").ToString()
                        .RecipientAddress.ZipCode = dtTable.Rows(0).Item("vcToZip").ToString()
                        .RecipientAddress.CountryCode = dtTable.Rows(0).Item("vcToCountry").ToString()
                        .RecipientAddress.City = dtTable.Rows(0).Item("vcToCity").ToString()
                        .RecipientAddress.Address1 = dtTable.Rows(0).Item("vcToAddressLine1").ToString()
                        .RecipientAddress.Address2 = dtTable.Rows(0).Item("vcToAddressLine2").ToString()

                        UpsShip1.Payor.PayorType = CType(Short.Parse(dtTable.Rows(0).Item("tintPayorType").ToString()), TPayorTypes) 'TPayorTypes.ptSender
                        UpsShip1.Payor.AccountNumber = CCommon.ToString(dtTable.Rows(0).Item("vcPayorAccountNo"))
                        UpsShip1.Payor.CountryCode = CCommon.ToString(dtTable.Rows(0).Item("vcPayorCountryCode"))
                        UpsShip1.Payor.ZipCode = CCommon.ToString(dtTable.Rows(0).Item("vcPayorZip"))
                    End If
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetAddressUSPS(ByRef USPSShip1 As Uspsshipintl)
            Try
                With USPSShip1
                    Dim dtTable As DataTable

                    'Fetch following fields from shippingreport table 
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = DomainID
                    objOppBizDoc.OppBizDocId = _OppBizDocId
                    objOppBizDoc.ShippingReportId = _ShippingReportId
                    dtTable = New DataTable
                    dtTable = objOppBizDoc.GetShippingReport().Tables(0)
                    If dtTable.Rows.Count > 0 Then

                        .SenderContact.FirstName = dtTable.Rows(0).Item("vcFromName").ToString()
                        .SenderContact.LastName = ""
                        .SenderContact.Company = dtTable.Rows(0).Item("vcFromCompany").ToString()
                        .SenderContact.Phone = dtTable.Rows(0).Item("vcFromPhone").ToString() 'The maximum length is 10 for U.S. and CA else 16

                        .SenderAddress.State = dtTable.Rows(0).Item("vcFromState").ToString()
                        .SenderAddress.ZipCode = dtTable.Rows(0).Item("vcFromZip").ToString()
                        .SenderAddress.CountryCode = dtTable.Rows(0).Item("vcFromCountry").ToString() ' this is optional in new FedExAPI V2
                        .SenderAddress.City = dtTable.Rows(0).Item("vcFromCity").ToString() '.SenderAddress.Residential = chbFromResidential.Checked
                        .SenderAddress.Address1 = dtTable.Rows(0).Item("vcFromAddressLine1").ToString()
                        .SenderAddress.Address2 = dtTable.Rows(0).Item("vcFromAddressLine2").ToString()

                        .RecipientContact.FirstName = dtTable.Rows(0).Item("vcToName").ToString()
                        .RecipientContact.LastName = "" 'dtTable.Rows(0).Item("Name").ToString()
                        .RecipientContact.Company = dtTable.Rows(0).Item("vcToCompany").ToString()
                        .RecipientContact.Phone = dtTable.Rows(0).Item("vcToPhone").ToString()
                        .RecipientAddress.State = dtTable.Rows(0).Item("vcToState").ToString()
                        .RecipientAddress.ZipCode = dtTable.Rows(0).Item("vcToZip").ToString()
                        .RecipientAddress.CountryCode = dtTable.Rows(0).Item("vcToCountry").ToString()
                        .RecipientAddress.City = dtTable.Rows(0).Item("vcToCity").ToString()
                        .RecipientAddress.Address1 = dtTable.Rows(0).Item("vcToAddressLine1").ToString()
                        .RecipientAddress.Address2 = dtTable.Rows(0).Item("vcToAddressLine2").ToString()

                        'If (USPSShip1.ServiceType = ServiceTypes.stFedExGroundHomeDelivery) Then
                        '    USPSShip1.RecipientAddress.AddressFlags = &H2
                        'End If

                        'USPSShip1.Payor.PayorType = CType(Short.Parse(dtTable.Rows(0).Item("tintPayorType").ToString()), TPayorTypes) 'TPayorTypes.ptSender
                        'USPSShip1.Payor.AccountNumber = CCommon.ToString(dtTable.Rows(0).Item("vcPayorAccountNo"))
                        'USPSShip1.Payor.CountryCode = CCommon.ToString(dtTable.Rows(0).Item("vcPayorCountryCode"))
                        'USPSShip1.Payor.ZipCode = CCommon.ToString(dtTable.Rows(0).Item("vcPayorZip"))
                    End If
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetConfig(ByRef ezship1 As Ezship)
            Try
                ezship1.ServiceType = CType(_ServiceType, ServiceTypes)
                ezship1.ShipmentSpecialServices = _intShipperSpecialServices
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub AddPackage()
            Try
                Dim ShipLabelImage As String
                If ShipLabelImageList Is Nothing Then ShipLabelImageList = New ArrayList()
                If BoxList Is Nothing Then BoxList = New ArrayList()
                Dim pkg As New PackageDetail
                With pkg
                    If (_Provider = EzshipProviders.pFedEx) Then
                        'Shipping Integrator VERSION 3.0 CODE----------------------------------------------------------------------
                        '.Reference = "IN:" + _InvoiceNo + "; CR:" & IIf(ReferenceNo.Length > 0, ReferenceNo, " ").ToString() & ";"
                        '----------------------------------------------------------------------------------------------------------

                        'Shipping Integrator VERSION 4.0 CODE----------------------------------------------------------------------
                        If ReferenceNo IsNot Nothing AndAlso ReferenceNo.Length > 0 Then
                            .Reference = "IN:" + _InvoiceNo + "; CR:" & IIf(ReferenceNo.Length > 0, ReferenceNo, " ").ToString() & ";"
                        Else
                            .Reference = "IN:" + _InvoiceNo
                        End If
                        '---------------------------------------------------------------------------------------------------------

                        'Type value Meaning, the Reference should be set to: CN:123456; IN:123; SN:12;
                        'CR  Customer Reference - this gets printed on shipping label and invoice.  
                        'BL  Bill Of Lading - this gets printed on shipping label, Ground shipping reports, and in the Customer Reference element on the invoice.  
                        'IN  Invoice Number - this gets printed on shipping label and invoice.  
                        'PO  Purchase Order Number - this gets printed on shipping label and invoice.  
                        'DN  Department Number - this gets printed on shipping label and invoice.  
                        'SI  Shipment Integrity - this gets printed on invoice only.  
                        'SN  Store Number - this gets printed on shipping label and invoice. 

                        Select Case _strCODType
                            Case "Any Check" : pkg.CODType = TPCODTypes.codtpAny
                            Case "Cashier's Check Or Money Order" : pkg.CODType = TPCODTypes.codtpCompanyCheck
                        End Select
                        pkg.CODAmount = CCommon.ToString(_CODAmount)
                        pkg.SignatureType = CType(_SignatureType, TSignatureTypes)


                    ElseIf (_Provider = EzshipProviders.pUPS) Then

                        'Shipping Integrator VERSION 3.0 CODE----------------------------------------------------------------------
                        '.Reference = "IK:" + _InvoiceNo + "; TN:" & IIf(ReferenceNo.Length > 0, ReferenceNo, " ").ToString() & ";"
                        '----------------------------------------------------------------------------------------------------------

                        'Shipping Integrator VERSION 4.0 CODE----------------------------------------------------------------------
                        If ReferenceNo IsNot Nothing AndAlso ReferenceNo.Length > 0 Then
                            .Reference = "IK:" + _InvoiceNo + "; TN:" & IIf(ReferenceNo.Length > 0, ReferenceNo, " ").ToString() & ";"
                        Else
                            .Reference = "IK:" + _InvoiceNo
                        End If
                        '---------------------------------------------------------------------------------------------------------
                        'the Reference should be set to: 'AJ:123456; TN:123'.
                        'Code  Reference Number Type 
                        'AJ  Accounts Receivable Customer Account 
                        'AT  Appropriation Number 
                        'BM  Bill of Lading Number 
                        '9V  Collect on Delivery (COD) Number 
                        'ON  Dealer Order Number 
                        'DP  Department Number 
                        '3Q  Food and Drug Administration (FDA) Product Code 
                        'IK  Invoice Number 
                        'MK  Manifest Key Number 
                        'MJ  Model Number 
                        'PM  Part Number 
                        'PC  Production Code 
                        'PO  Purchase Order Number 
                        'RQ  Purchase Request Number 
                        'RZ  Return Authorization Number 
                        'SA  Salesperson Number 
                        'SE  Serial Number 
                        'ST  Store Number 
                        'TN  Transaction Reference Number 

                        Select Case _strCODType
                            Case "Any Check" : pkg.CODType = TPCODTypes.codtpAny
                            Case "Cashier's Check Or Money Order" : pkg.CODType = TPCODTypes.codtpCompanyCheck
                        End Select
                        pkg.CODAmount = CCommon.ToString(_CODAmount)

                    End If

                    .PackagingType = CType(_PackagingType, TPackagingTypes) 'Should be TPackagingTypes.ptYourPackaging
                    If _Provider = EzshipProviders.pUSPS Then
                        'Example: txtWeightLbs.Text + " lbs " + txtWeightOz.Text + " oz" 
                        'Note: If Oz is 0 it should be omitted from the string (just send lbs in that case)
                        'We are passing only lbs for time being
                        'If WeightInLbs > 0 Then
                        '    .Weight = WeightInLbs.ToString() + " lbs " + WeightInOunce.ToString() + " oz"
                        'Else
                        '    .Weight = WeightInLbs.ToString() '+ " lbs " + WeightInOunce.ToString() + " oz"
                        'End If
                        .Weight = FormatNumber(WeightInOunce, 1).ToString()
                    Else
                        .Weight = _WeightInLbs.ToString()
                    End If

                    If UseDimentions Then
                        .Width = CInt(_Width)
                        .Height = CInt(_Height)
                        .Length = CInt(_Length)
                        .PackagingType = TPackagingTypes.ptYourPackaging

                    End If


                    Dim objItem As New BACRM.BusinessLogic.Item.CItems
                    Dim dsShippingDtl As DataSet
                    objItem.DomainID = DomainID
                    objItem.ShippingCMPID = _shippingCompanyID
                    dsShippingDtl = objItem.ShippingDtls()

                    If Not dsShippingDtl Is Nothing AndAlso dsShippingDtl.Tables.Count > 0 AndAlso dsShippingDtl.Tables(0).Rows.Count > 0 Then
                        Dim arrRow As DataRow() = dsShippingDtl.Tables(0).Select("vcFieldName='Shipping Label Type'")

                        If Not arrRow Is Nothing AndAlso arrRow.Length > 0 Then
                            If CCommon.ToString(arrRow(0)("vcShipFieldValue")) = "1" Then
                                ShipLabelImage = System.Guid.NewGuid.ToString() & ".zpl"
                            Else
                                ShipLabelImage = IIf(_Provider = EzshipProviders.pFedEx, System.Guid.NewGuid.ToString() & ".png", System.Guid.NewGuid.ToString() & ".gif").ToString
                            End If
                        End If
                    Else
                        ShipLabelImage = IIf(_Provider = EzshipProviders.pFedEx, System.Guid.NewGuid.ToString() & ".png", System.Guid.NewGuid.ToString() & ".gif").ToString
                    End If

                    ShipLabelImageList.Add(ShipLabelImage)
                    .ShippingLabelFile = ShipLabelImage

                    BoxList.Add(_BoxID)
                End With

                If PkgList Is Nothing Then PkgList = New ArrayList()
                PkgList.Add(pkg)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BuildPackagesEzShip(ByRef ezship1 As Ezship)
            Try
                If Not PkgList Is Nothing Then
                    Dim enmPkg As IEnumerator = PkgList.GetEnumerator()
                    While enmPkg.MoveNext()
                        If ezship1.Provider = EzshipProviders.pUPS Then
                            ezship1.Config("CertifyDirectory=" & CCommon.GetDocumentPhysicalPath(DomainID))
                            If Not DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile.ToString().Contains(CCommon.GetDocumentPhysicalPath(DomainID)) Then
                                DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile = CCommon.GetDocumentPhysicalPath(DomainID) & DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile
                            End If
                            DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).SignatureType = CType(_SignatureType, TSignatureTypes)
                        ElseIf ezship1.Provider = EzshipProviders.pFedEx Then
                            If Not DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile.ToString().Contains(CCommon.GetDocumentPhysicalPath(DomainID)) Then
                                DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile = CCommon.GetDocumentPhysicalPath(DomainID) & DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile
                            End If
                            DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).SignatureType = CType(_SignatureType, TSignatureTypes)
                        ElseIf ezship1.Provider = EzshipProviders.pUSPS Then
                            If Not DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile.ToString().Contains(CCommon.GetDocumentPhysicalPath(DomainID)) Then
                                DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile = CCommon.GetDocumentPhysicalPath(DomainID) & DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile
                            End If
                            DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).SignatureType = CType(_SignatureType, TSignatureTypes)
                        End If

                        If CCommon.ToDouble(_dblTotalInsuredValue) > 0 Then
                            CType(enmPkg.Current, PackageDetail).InsuredValue = CCommon.ToString(_dblTotalInsuredValue)
                        End If

                        ezship1.Packages.Add(CType(enmPkg.Current, PackageDetail))
                    End While
                End If

                If (ezship1.Provider <> EzshipProviders.pFedEx And ezship1.Provider <> EzshipProviders.pUPS) Then
                    ezship1.Config("PostageProvider=1") 'Use Endicia instead of USPS directly.
                    ezship1.Config("CustomerId=2322") 'Mandatory for Endicia
                    ezship1.Config("TransactionId=ABCDEFGH1") 'Mandatory for Endicia
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BuildPackagesFedexShipIntl(ByRef FedexShipComp As Fedexshipintl)
            Try
                If Not PkgList Is Nothing Then
                    Dim enmPkg As IEnumerator = PkgList.GetEnumerator()
                    While enmPkg.MoveNext()
                        If Not DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile.ToString().Contains(CCommon.GetDocumentPhysicalPath(DomainID)) Then
                            DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile = CCommon.GetDocumentPhysicalPath(DomainID) & DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile
                        End If

                        FedexShipComp.Packages.Add(CType(enmPkg.Current, PackageDetail))
                    End While
                End If

                FedexShipComp.ServiceType = CType(_ServiceType, ServiceTypes)
                FedexShipComp.ShipmentSpecialServices = _intShipperSpecialServices
                FedexShipComp.DropoffType = FedexshipintlDropoffTypes.dtRegularPickup 'CType(_DropoffType, DomesticlabelsDropoffTypes)

                Select Case _PayorType
                    Case 0 : FedexShipComp.Payor.PayorType = TPayorTypes.ptSender
                    Case 1 : FedexShipComp.Payor.PayorType = TPayorTypes.ptRecipient
                    Case 2 : FedexShipComp.Payor.PayorType = TPayorTypes.ptThirdParty
                    Case 3 : FedexShipComp.Payor.PayorType = TPayorTypes.ptCollect
                End Select

                Select Case _PayorType
                    Case 0 : FedexShipComp.DutiesPayor.PayorType = TPayorTypes.ptSender
                    Case 1 : FedexShipComp.DutiesPayor.PayorType = TPayorTypes.ptRecipient
                    Case 2 : FedexShipComp.DutiesPayor.PayorType = TPayorTypes.ptThirdParty
                    Case 3 : FedexShipComp.DutiesPayor.PayorType = TPayorTypes.ptCollect
                End Select

                FedexShipComp.Payor.AccountNumber = _PayorAccountNo
                FedexShipComp.Payor.CountryCode = _PayorCountryCode

                FedexShipComp.DutiesPayor.AccountNumber = _PayorAccountNo
                FedexShipComp.DutiesPayor.CountryCode = _PayorCountryCode

                If CCommon.ToDouble(_dblTotalInsuredValue) > 0 Then
                    FedexShipComp.InsuredValue = CCommon.ToString(_dblTotalInsuredValue)
                End If

                FedexShipComp.TotalCustomsValue = CCommon.ToString(_dblTotalCustomsValue)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BuildPackagesUPSShipIntl(ByRef UpsShip1 As Upsshipintl)
            Try
                If Not PkgList Is Nothing Then
                    Dim enmPkg As IEnumerator = PkgList.GetEnumerator()
                    While enmPkg.MoveNext()
                        UpsShip1.Config("CertifyDirectory=" & CCommon.GetDocumentPhysicalPath(DomainID))
                        UpsShip1.Packages.Add(CType(enmPkg.Current, PackageDetail))
                    End While
                End If

                Select Case _PayorType
                    Case 0 : UpsShip1.Payor.PayorType = TPayorTypes.ptSender
                    Case 1 : UpsShip1.Payor.PayorType = TPayorTypes.ptRecipient
                    Case 2 : UpsShip1.Payor.PayorType = TPayorTypes.ptThirdParty
                    Case 3 : UpsShip1.Payor.PayorType = TPayorTypes.ptCollect

                    Case 0 : UpsShip1.DutiesPayor.PayorType = TPayorTypes.ptSender
                    Case 1 : UpsShip1.DutiesPayor.PayorType = TPayorTypes.ptRecipient
                    Case 2 : UpsShip1.DutiesPayor.PayorType = TPayorTypes.ptThirdParty
                    Case 3 : UpsShip1.DutiesPayor.PayorType = TPayorTypes.ptCollect
                End Select

                UpsShip1.Payor.AccountNumber = _PayorAccountNo
                UpsShip1.Payor.CountryCode = _PayorCountryCode
                UpsShip1.Payor.ZipCode = _PayorZipCode

                UpsShip1.DutiesPayor.AccountNumber = _PayorAccountNo
                UpsShip1.DutiesPayor.CountryCode = _PayorCountryCode
                UpsShip1.DutiesPayor.ZipCode = _PayorZipCode

                UpsShip1.ShipmentDescription = ""
                UpsShip1.TotalCustomsValue = CCommon.ToString(_dblTotalCustomsValue)
                UpsShip1.InternationalFormsFile = ""
                UpsShip1.LabelImageType = UpsshipintlLabelImageTypes.uitGIF

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BuildPackagesUSPSShipIntl(ByRef USPSShip1 As Uspsshipintl)
            Try
                If Not PkgList Is Nothing Then
                    Dim enmPkg As IEnumerator = PkgList.GetEnumerator()
                    While enmPkg.MoveNext()
                        If Not DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile.ToString().Contains(CCommon.GetDocumentPhysicalPath(DomainID)) Then
                            DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile = CCommon.GetDocumentPhysicalPath(DomainID) & DirectCast(enmPkg.Current, nsoftware.InShip.PackageDetail).ShippingLabelFile
                        End If

                        USPSShip1.Packages.Add(CType(enmPkg.Current, PackageDetail))
                    End While
                End If

                USPSShip1.ServiceType = CType(_ServiceType, ServiceTypes)
                USPSShip1.ShipmentSpecialServices = _intShipperSpecialServices
                'USPSShip1.DropoffType = FedexshipintlDropoffTypes.dtRegularPickup 'CType(_DropoffType, DomesticlabelsDropoffTypes)

                'USPSShip1.DutiesPayor.AccountNumber = _PayorAccountNo
                'USPSShip1.DutiesPayor.CountryCode = _PayorCountryCode

                'USPSShip1.InsuredValue = CCommon.ToString(_dblTotalInsuredValue)
                'USPSShip1.TotalCustomsValue = CCommon.ToString(_dblTotalCustomsValue)
                USPSShip1.LabelImageType = UspsshipintlLabelImageTypes.sitPNG

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub SaveTrackingNumberEZShip(ByRef ezship1 As Ezship)
            Try
                Dim ds As New DataSet
                Dim objOppBizDocs As New OppBizDocs
                Dim dtLabels As New DataTable
                dtLabels.Columns.Add("numBoxID")
                dtLabels.Columns.Add("vcShippingLabelImage")
                dtLabels.Columns.Add("vcTrackingNumber")
                dtLabels.Columns.Add("bitIsMasterTrackingNo")

                Dim dr As DataRow

                Dim strShippingImage As String
                'Create Shipping label for each box
                For i As Integer = 0 To ezship1.Packages.Count - 1
                    strShippingImage = ShipLabelImageList(i).ToString()
                    If (ezship1.Provider = EzshipProviders.pUPS) Then
                        If ezship1.Packages(i).ShippingLabelFile.Length > 0 Then
                            If ezship1.LabelImageType = EzshipLabelImageTypes.itZPL Then
                                Dim filePath As String = ezship1.Packages(i).ShippingLabelFile
                                Dim newExtension As String = ".zpl"
                                ezship1.Packages(i).ShippingLabelFile = IO.Path.ChangeExtension(filePath, newExtension)
                                IO.File.Move(filePath, IO.Path.ChangeExtension(filePath, newExtension))

                                Dim text As String = File.ReadAllText(IO.Path.ChangeExtension(filePath, newExtension))
                                text = text.Replace("^POI^", "^PON^")
                                File.WriteAllText(IO.Path.ChangeExtension(filePath, newExtension), text)
                            End If

                            strShippingImage = ezship1.Packages(i).ShippingLabelFile.Substring(ezship1.Packages(i).ShippingLabelFile.LastIndexOf("\"), ezship1.Packages(i).ShippingLabelFile.Length - ezship1.Packages(i).ShippingLabelFile.LastIndexOf("\")).Trim(CChar("\"))
                        End If
                    ElseIf ezship1.LabelImageType = EzshipLabelImageTypes.itZebra Or ezship1.LabelImageType = EzshipLabelImageTypes.itZPL Then
                        Dim filePath As String = ezship1.Packages(i).ShippingLabelFile

                        Dim text As String = File.ReadAllText(filePath)
                        text = text.Replace("^POI^", "^PON^")
                        File.WriteAllText(filePath, text)
                    End If

                    dr = dtLabels.NewRow()
                    dr("numBoxID") = BoxList(i).ToString()
                    dr("vcShippingLabelImage") = strShippingImage
                    dr("vcTrackingNumber") = ezship1.Packages(i).TrackingNumber
                    dr("bitIsMasterTrackingNo") = IIf(i = 0, 1, 0) 'Master Tracking Number: The first tracking number in a multiple-piece shipment that associates all tracking numbers in the shipment.
                    dtLabels.Rows.Add(dr)
                Next
                ds.Tables.Add(dtLabels.Copy)

                objOppBizDocs.UserCntID = UserCntID
                objOppBizDocs.strText = ds.GetXml
                objOppBizDocs.ManageShippingLabel()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub SaveTrackingNumberFedexShipIntl(ByRef FedexShip1 As Fedexshipintl)
            Try
                Dim ds As New DataSet
                Dim objOppBizDocs As New OppBizDocs
                Dim dtLabels As New DataTable
                dtLabels.Columns.Add("numBoxID")
                dtLabels.Columns.Add("vcShippingLabelImage")
                dtLabels.Columns.Add("vcTrackingNumber")
                dtLabels.Columns.Add("bitIsMasterTrackingNo")

                Dim dr As DataRow

                Dim strShippingImage As String
                'Create Shipping label for each box
                For i As Integer = 0 To FedexShip1.Packages.Count - 1
                    strShippingImage = ShipLabelImageList(i).ToString()

                    dr = dtLabels.NewRow()
                    dr("numBoxID") = BoxList(i).ToString()
                    dr("vcShippingLabelImage") = strShippingImage
                    dr("vcTrackingNumber") = FedexShip1.Packages(i).TrackingNumber
                    dr("bitIsMasterTrackingNo") = IIf(i = 0, 1, 0) 'Master Tracking Number: The first tracking number in a multiple-piece shipment that associates all tracking numbers in the shipment.
                    dtLabels.Rows.Add(dr)
                Next
                ds.Tables.Add(dtLabels.Copy)

                objOppBizDocs.UserCntID = UserCntID
                objOppBizDocs.strText = ds.GetXml
                objOppBizDocs.ManageShippingLabel()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub SaveTrackingNumberUPSShipIntl(ByRef UpsShip1 As Upsshipintl)
            Try
                Dim ds As New DataSet
                Dim objOppBizDocs As New OppBizDocs
                Dim dtLabels As New DataTable
                dtLabels.Columns.Add("numBoxID")
                dtLabels.Columns.Add("vcShippingLabelImage")
                dtLabels.Columns.Add("vcTrackingNumber")
                dtLabels.Columns.Add("bitIsMasterTrackingNo")

                Dim dr As DataRow

                Dim strShippingImage As String
                'Create Shipping label for each box
                For i As Integer = 0 To UpsShip1.Packages.Count - 1
                    strShippingImage = ShipLabelImageList(i).ToString()
                    If UpsShip1.Packages(i).ShippingLabelFile.Length > 0 Then
                        strShippingImage = UpsShip1.Packages(i).ShippingLabelFile.Substring(UpsShip1.Packages(i).ShippingLabelFile.LastIndexOf("\"), UpsShip1.Packages(i).ShippingLabelFile.Length - UpsShip1.Packages(i).ShippingLabelFile.LastIndexOf("\")).Trim(CChar("\"))
                    End If

                    dr = dtLabels.NewRow()
                    dr("numBoxID") = BoxList(i).ToString()
                    dr("vcShippingLabelImage") = strShippingImage
                    dr("vcTrackingNumber") = UpsShip1.Packages(i).TrackingNumber
                    dr("bitIsMasterTrackingNo") = IIf(i = 0, 1, 0) 'Master Tracking Number: The first tracking number in a multiple-piece shipment that associates all tracking numbers in the shipment.
                    dtLabels.Rows.Add(dr)
                Next
                ds.Tables.Add(dtLabels.Copy)

                objOppBizDocs.UserCntID = UserCntID
                objOppBizDocs.strText = ds.GetXml
                objOppBizDocs.ManageShippingLabel()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub SaveTrackingNumberUSPSShipIntl(ByRef USPSShip1 As Uspsshipintl)
            Try
                Dim ds As New DataSet
                Dim objOppBizDocs As New OppBizDocs
                Dim dtLabels As New DataTable
                dtLabels.Columns.Add("numBoxID")
                dtLabels.Columns.Add("vcShippingLabelImage")
                dtLabels.Columns.Add("vcTrackingNumber")
                dtLabels.Columns.Add("bitIsMasterTrackingNo")

                Dim dr As DataRow

                Dim strShippingImage As String
                'Create Shipping label for each box
                For i As Integer = 0 To USPSShip1.Packages.Count - 1
                    strShippingImage = ShipLabelImageList(i).ToString()

                    dr = dtLabels.NewRow()
                    dr("numBoxID") = BoxList(i).ToString()
                    dr("vcShippingLabelImage") = strShippingImage
                    dr("vcTrackingNumber") = USPSShip1.Packages(i).TrackingNumber
                    dr("bitIsMasterTrackingNo") = IIf(i = 0, 1, 0) 'Master Tracking Number: The first tracking number in a multiple-piece shipment that associates all tracking numbers in the shipment.
                    dtLabels.Rows.Add(dr)
                Next
                ds.Tables.Add(dtLabels.Copy)

                objOppBizDocs.UserCntID = UserCntID
                objOppBizDocs.strText = ds.GetXml
                objOppBizDocs.ManageShippingLabel()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function TranslateServiceType(ByVal tintServiceType As Integer) As String
            Try
                Dim service As String = ""

                Select Case tintServiceType
                    Case 0 : service = "Unspecified"

                    Case 10 : service = "Priority Overnight"
                    Case 11 : service = "Standard Overnight"
                    Case 12 : service = "First Overnight"
                    Case 13 : service = "FedEx 2nd Day"
                    Case 14 : service = "FedEx Express Saver"
                    Case 15 : service = "FedEx Ground"
                    Case 16 : service = "FedEx Ground Home Delivery"
                    Case 17 : service = "FedEx 1Day Freight"
                    Case 18 : service = "FedEx 2Day Freight"
                    Case 19 : service = "FedEx 3Day Freight"
                    Case 20 : service = "International Priority"
                    Case 21 : service = "International Priority Distribution"
                    Case 22 : service = "International Economy"
                    Case 23 : service = "International Economy Distribution"
                    Case 24 : service = "International First"
                    Case 25 : service = "International Priority Freight"
                    Case 26 : service = "International Economy Freight"
                    Case 27 : service = "International Distribution Freight"
                    Case 28 : service = "Europe International Priority"

                    Case 70 : service = "USPS Express"
                    Case 71 : service = "USPS First Class"
                    Case 72 : service = "USPS Priority"
                    Case 73 : service = "USPS Parcel Post"
                    Case 74 : service = "USPS Bound Printed Matter"
                    Case 75 : service = "USPS Media"
                    Case 76 : service = "USPS Library"

                    Case 82 : service = "USPS Express Mail International"
                    Case 83 : service = "USPS FirstClass Mail International"
                    Case 84 : service = "USPS Priority Mail International"

                    Case 40 : service = "UPS Next Day Air"
                    Case 42 : service = "UPS 2nd Day Air"
                    Case 43 : service = "UPS Ground"
                    Case 48 : service = "UPS 3Day Select"
                    Case 49 : service = "UPS Next Day Air Saver"
                    Case 50 : service = "UPS Saver"
                    Case 51 : service = "UPS Next Day Air Early A.M."
                    Case 55 : service = "UPS 2nd Day Air AM"
                End Select
                Return service
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Sub GenerateShippingLabel(ByVal lngOppID As Long, _
                                         ByVal lngOppBizDocID As Long, _
                                         ByVal lngShippingReportId As Long, _
                                         ByVal lngDomainID As Long, _
                                         ByVal lngUserContactID As Long)
            Try

                Dim xmlFilePath As String = CCommon.GetDocumentPhysicalPath(lngDomainID) & "Shipping"
                Dim strFileName As String = ""
                If Not Directory.Exists(xmlFilePath) Then
                    Directory.CreateDirectory(xmlFilePath)
                End If

                Dim objOppBiz As New OppBizDocs
                Dim dsShip As DataSet

                objOppBiz.ShippingReportId = lngShippingReportId
                objOppBiz.OppBizDocId = lngOppBizDocID
                objOppBiz.DomainID = lngDomainID
                dsShip = objOppBiz.GetShippingReport()

                If dsShip IsNot Nothing AndAlso dsShip.Tables.Count > 0 AndAlso dsShip.Tables(0).Rows.Count > 0 Then

                    Dim objShipping As New Shipping
                    With objShipping
                        .DomainID = lngDomainID
                        .UserCntID = lngUserContactID
                        .ShippingReportId = lngShippingReportId
                        .OpportunityId = lngOppID
                        .OppBizDocId = lngOppBizDocID
                        .InvoiceNo = CCommon.ToString(lngOppBizDocID) ' Gives error when we pass vcBizDocId ->customerReference value length must not exceed 40 characters for customer reference in requestedPackage 1 
                        .CODAmount = CCommon.ToString(dsShip.Tables(0).Rows(0)("numCODAmount"))
                        .CODType = CCommon.ToString(dsShip.Tables(0).Rows(0)("vcCODType"))
                        .SignatureType = CCommon.ToShort(dsShip.Tables(0).Rows(0)("tintSignatureType"))
                        .TotalInsuredValue = CCommon.ToDouble(dsShip.Tables(0).Rows(0)("numTotalInsuredValue"))
                        .TotalCustomsValue = CCommon.ToDouble(dsShip.Tables(0).Rows(0)("numTotalCustomsValue"))
                        _shippingCompanyID = CCommon.ToInteger(dsShip.Tables(0).Rows(0)("numShippingCompany"))
                        _SignatureType = .SignatureType
                        'GET BOX DETAILS AS PER SHIPPING REPORT ID
                        Dim dtBox As DataTable = Nothing
                        If objOppBiz Is Nothing Then objOppBiz = New OppBizDocs
                        objOppBiz.ShippingReportId = lngShippingReportId
                        dtBox = objOppBiz.GetShippingBoxes().Tables(0)

                        '-----------------------------------------

                        For i As Integer = 0 To dtBox.Rows.Count - 1

                            .PackagingType = CCommon.ToShort(dtBox.Rows(i)("numPackageTypeID"))
                            .ServiceType = CCommon.ToShort(dtBox.Rows(i)("numServiceTypeID"))
                            .PayorType = CCommon.ToShort(dtBox.Rows(i)("tintPayorType"))
                            .PayorAccountNo = CCommon.ToString(dtBox.Rows(i)("vcPayorAccountNo"))
                            .PayorCountryCode = CCommon.ToString(dtBox.Rows(i)("numPayorCountry"))
                            .PayorZipCode = CCommon.ToString(dtBox.Rows(i)("vcPayorZip"))

                            ' Add condition by which we can specify whether we use dimensions or not
                            If .PackagingType <> 31 Then

                                If _shippingCompanyID = 91 Then
                                    .UseDimentions = True
                                Else
                                    .UseDimentions = False
                                End If

                            Else
                                .UseDimentions = True
                            End If
                            .Height = CCommon.ToDouble(dtBox.Rows(i)("fltHeight"))
                            .Length = CCommon.ToDouble(dtBox.Rows(i)("fltLength"))
                            .Width = CCommon.ToDouble(dtBox.Rows(i)("fltWidth"))
                            .WeightInLbs = CCommon.ToDouble(dtBox.Rows(i)("fltTotalWeight"))
                            .WeightInOunce = .WeightInLbs * 16 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltWeight"))
                            .BoxID = CCommon.ToLong(dtBox.Rows(i)("numBoxID"))
                            _ServiceType = .ServiceType
                            .Provider = CCommon.ToInteger(OppBizDocs.GetServiceTypes(TranslateServiceType(_ServiceType).Trim()))
                            _Provider = .Provider

                            If .WeightInLbs = 0 Then
                                _ErrorMsg = _ErrorMsg & " Please specify weight of box."
                                Continue For
                            End If
                            .ReferenceNo = "" 'CCommon.ToString(lngOppID)

                            Dim ShipLabelImage As String = ""
                            If ShipLabelImageList Is Nothing Then ShipLabelImageList = New ArrayList()
                            If BoxList Is Nothing Then BoxList = New ArrayList()
                            Dim pkg As New PackageDetail
                            With pkg
                                If (_Provider = EzshipProviders.pFedEx) Then
                                    If objShipping.ReferenceNo IsNot Nothing AndAlso objShipping.ReferenceNo.Length > 0 Then
                                        .Reference = "IN:" + objShipping.InvoiceNo + "; CR:" & IIf(objShipping.ReferenceNo.Length > 0, objShipping.ReferenceNo, " ").ToString() & ";"
                                    Else
                                        .Reference = "IN:" + objShipping.InvoiceNo
                                    End If

                                    Select Case _strCODType
                                        Case "Any Check" : .CODType = TPCODTypes.codtpAny
                                        Case "Cashier's Check Or Money Order" : .CODType = TPCODTypes.codtpCompanyCheck
                                    End Select
                                    .CODAmount = CCommon.ToString(_CODAmount)
                                    .SignatureType = CType(_SignatureType, TSignatureTypes)
                                ElseIf (_Provider = EzshipProviders.pUPS) Then
                                    If objShipping.ReferenceNo IsNot Nothing AndAlso objShipping.ReferenceNo.Length > 0 Then
                                        .Reference = "IK:" + objShipping.InvoiceNo + "; TN:" & IIf(objShipping.ReferenceNo.Length > 0, objShipping.ReferenceNo, " ").ToString() & ";"
                                    Else
                                        .Reference = "IK:" + objShipping.InvoiceNo
                                    End If

                                    Select Case _strCODType
                                        Case "Any Check" : .CODType = TPCODTypes.codtpAny
                                        Case "Cashier's Check Or Money Order" : .CODType = TPCODTypes.codtpCompanyCheck
                                    End Select
                                    .CODAmount = CCommon.ToString(_CODAmount)
                                End If

                                .PackagingType = CType(objShipping.PackagingType, TPackagingTypes) 'Should be TPackagingTypes.ptYourPackaging
                                If _Provider = EzshipProviders.pUSPS Then
                                    .Weight = FormatNumber(objShipping.WeightInOunce, 1).ToString()
                                Else
                                    .Weight = objShipping.WeightInLbs.ToString()
                                End If

                                'If objShipping.UseDimentions Then
                                .Width = CInt(objShipping.Width)
                                .Height = CInt(objShipping.Height)
                                .Length = CInt(objShipping.Length)
                                '.PackagingType = TPackagingTypes.ptYourPackaging

                                'End If

                                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                                objItem.DomainID = lngDomainID
                                objItem.ShippingCMPID = _shippingCompanyID
                                Dim dsShippingDtl As DataSet = objItem.ShippingDtls()

                                If Not dsShippingDtl Is Nothing AndAlso dsShippingDtl.Tables.Count > 0 AndAlso dsShippingDtl.Tables(0).Rows.Count > 0 Then
                                    Dim arrRow As DataRow() = dsShippingDtl.Tables(0).Select("vcFieldName='Shipping Label Type'")

                                    If Not arrRow Is Nothing AndAlso arrRow.Length > 0 Then
                                        If CCommon.ToString(arrRow(0)("vcShipFieldValue")) = "1" Then
                                            ShipLabelImage = System.Guid.NewGuid.ToString() & ".zpl"
                                        Else
                                            ShipLabelImage = IIf(_Provider = EzshipProviders.pFedEx, System.Guid.NewGuid.ToString() & ".png", System.Guid.NewGuid.ToString() & ".gif").ToString
                                        End If
                                    End If
                                Else
                                    ShipLabelImage = IIf(_Provider = EzshipProviders.pFedEx, System.Guid.NewGuid.ToString() & ".png", System.Guid.NewGuid.ToString() & ".gif").ToString
                                End If

                                ShipLabelImageList.Add(ShipLabelImage)
                                '.ShippingLabelFile = CCommon.GetDocumentPhysicalPath(DomainID) & ShipLabelImage
                                .ShippingLabelFile = ShipLabelImage

                                BoxList.Add(objShipping.BoxID)
                            End With

                            If PkgList Is Nothing Then PkgList = New ArrayList()
                            PkgList.Add(pkg)

                            Dim specialService As Long
                            If CCommon.ToBool(dsShip.Tables(0).Rows(0)("IsCOD")) Then
                                specialService = specialService Or &H4L '&H4L 'COD 
                            End If

                            If CCommon.ToBool(dsShip.Tables(0).Rows(0)("IsDryIce")) Then
                                specialService = specialService Or &H80L '&H80L        'Dry Ice
                            End If

                            If CCommon.ToBool(dsShip.Tables(0).Rows(0)("IsHomeDelivery")) Then
                                specialService = specialService Or &H20000L ' &H20000L     'Home Delivery Premium
                            End If

                            If CCommon.ToBool(dsShip.Tables(0).Rows(0)("IsInsideDelevery")) Then
                                specialService = specialService Or &H40000L '&H40000L     'Inside Delivery
                            End If

                            If CCommon.ToBool(dsShip.Tables(0).Rows(0)("IsInsidePickup")) Then
                                specialService = specialService Or &H80000L '&H80000L     'Inside Pickup
                            End If

                            If CCommon.ToBool(dsShip.Tables(0).Rows(0)("IsReturnShipment")) Then
                                specialService = specialService Or &H8000000L 'Return Shipment
                            End If

                            If CCommon.ToBool(dsShip.Tables(0).Rows(0)("IsSaturdayDelivery")) Then
                                specialService = specialService Or &H10000000L 'Saturday Delivery
                            End If

                            If CCommon.ToBool(dsShip.Tables(0).Rows(0)("IsSaturdayPickup")) Then
                                specialService = specialService Or &H20000000L 'Saturday Pickup
                            End If

                            objShipping.ShipperSpecialServices = specialService

                            _OppBizDocId = objShipping.OppBizDocId
                            _ShippingReportId = objShipping.ShippingReportId
                            _ServiceType = objShipping.ServiceType
                            DomainID = lngDomainID
                        Next

                        Dim ezship1 As New Ezship
                        GetPrerequisiteForLabelEzShip(ezship1)
                        GetConfig(ezship1)
                        GetAddressEzShip(ezship1)
                        BuildPackagesEzShip(ezship1)
                        Try
                            strFileName = CCommon.ToString(lngOppID) & "_" & CCommon.ToString(lngShippingReportId) & ".xml"

                            ezship1.Timeout = 0
                            If ezship1.Provider = EzshipProviders.pFedEx Then
                                ezship1.Config("PackageCount=" & ezship1.Packages.Count.ToString())
                            End If
                            ezship1.GetShipmentLabels()

                            Try
                                _MasterTrackingNumber = ezship1.MasterTrackingNumber
                                For intCnt As Integer = 0 To ezship1.Packages.Count - 1
                                    _TrackingNumber.Add(ezship1.Packages(intCnt).TrackingNumber)
                                Next
                                SaveTrackingNumberEZShip(ezship1)
                            Catch ex As Exception
                                'DO NOT RAISE ERROR
                            End Try
                           
                            Dim objOppBizDocs As New OppBizDocs
                            objOppBizDocs.ShippingReportId = lngShippingReportId
                            objOppBizDocs.OppBizDocId = lngOppBizDocID
                            objOppBizDocs.UpdateBizDocsShippingFields()

                            System.IO.File.WriteAllText(xmlFilePath & "\ShippingRequest_" & strFileName, ezship1.Config("FullRequest"))
                            System.IO.File.WriteAllText(xmlFilePath & "\ShippingResponse_" & strFileName, ezship1.Config("FullResponse"))

                        Catch ex As InShipException
                            _ErrorMsg = "<b><font color='red'>" & "Error: " & ex.Message & "</font></b>"

                            System.IO.File.WriteAllText(xmlFilePath & "\ShippingRequestError_" & strFileName, ezship1.Config("FullRequest"))
                            System.IO.File.WriteAllText(xmlFilePath & "\ShippingResponseError_" & strFileName, ezship1.Config("FullResponse"))
                        End Try
                    End With
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetCommodityUnit(ByVal strUnit As String) As String
            Dim unit As String = ""
            Select Case strUnit
                Case "Units" : unit = "UT"
                Case "Carat" : unit = "AR"
                Case "Centigram" : unit = "CG"
                Case "Centimeters" : unit = "CM"
                Case "Cubic centimeters" : unit = "CM3"
                Case "Cubic feet" : unit = "CFT"
                Case "Cubic meters" : unit = "M3"
                Case "Dozen" : unit = "DOZ"
                Case "Dozen pair" : unit = "DPR"
                Case "Each" : unit = "EA"
                Case "Gallon" : unit = "GAL "
                Case "Grams" : unit = "G"
                Case "Gram" : unit = "G"
                Case "Gross" : unit = "GR"
                Case "Hours" : unit = "HR"
                Case "Hour" : unit = "HR"
                Case "Minutes" : unit = "MIN"
                Case "Minute" : unit = "MIN"
                Case "Kits" : unit = "KT"
                Case "Kilograms" : unit = "KG"
                Case "Kilogram" : unit = "KG"
                Case "Linear foot" : unit = "LFT"
                Case "Linear meters" : unit = "LNM"
                Case "Linear yard" : unit = "LYD"
                Case "Liters" : unit = "LTR"
                Case "Liter" : unit = "LTR"
                Case "Meters" : unit = "M"
                Case "Meter" : unit = "M"
                Case "KiloMeter" : unit = "KM"
                Case "Millimeter" : unit = "MM"
                Case "Centimeter" : unit = "CM"
                Case "Milligram" : unit = "MG"
                Case "Milliliter" : unit = "ML"
                Case "Number" : unit = "NO"
                Case "Ounces" : unit = "OZ"
                Case "Pairs" : unit = "PRS"
                Case "Pieces" : unit = "PCS"
                Case "Pound" : unit = "LB"
                Case "Square centimeters" : unit = "CM2"
                Case "Square feet" : unit = "SFT"
                Case "Square inches" : unit = "SQI"
                Case "Square meters" : unit = "M2"
                Case "Square yards" : unit = "SYD"
                Case "Yard" : unit = "YD"
            End Select
            Return unit
        End Function

        Private _strCommoDescription As String
        Public Property CompoDescription() As String
            Get
                Return _strCommoDescription
            End Get
            Set(ByVal value As String)
                _strCommoDescription = value
            End Set
        End Property

        Private _Quantity As Integer
        Public Property Quantity() As Integer
            Get
                Return _Quantity
            End Get
            Set(ByVal value As Integer)
                _Quantity = value
            End Set
        End Property

        Private _strUnitName As String
        Public Property UnitName() As String
            Get
                Return _strUnitName
            End Get
            Set(ByVal value As String)
                _strUnitName = value
            End Set
        End Property

        Private _dblUnitPrice As Double
        Public Property UnitPrice() As Double
            Get
                Return _dblUnitPrice
            End Get
            Set(ByVal value As Double)
                _dblUnitPrice = value
            End Set
        End Property

        Private _intImageWidth As Integer
        Public Property ImageWidth() As Integer
            Get
                Return _intImageWidth
            End Get
            Set(ByVal value As Integer)
                _intImageWidth = value
            End Set
        End Property

        Private _intImageHeight As Integer
        Public Property ImageHeight() As Integer
            Get
                Return _intImageHeight
            End Get
            Set(ByVal value As Integer)
                _intImageHeight = value
            End Set
        End Property

        Public Sub AddFedexCommodity(ByRef FedexShip1 As Fedexshipintl)
            commCount = FedexShip1.Commodities.Count
            FedexShip1.Commodities.Add(New CommodityDetail())
            FedexShip1.Commodities(commCount).Description = _strCommoDescription
            FedexShip1.Commodities(commCount).Quantity = _Quantity
            FedexShip1.Commodities(commCount).QuantityUnit = GetCommodityUnit(_strUnitName)
            FedexShip1.Commodities(commCount).UnitPrice = CCommon.ToString(_dblUnitPrice)
            FedexShip1.Commodities(commCount).Weight = CCommon.ToString(_WeightInLbs)
        End Sub

        Public Sub AddUSPSCommodity(ByRef UspsShip1 As Uspsshipintl)
            commCount = UspsShip1.Commodities.Count
            UspsShip1.Commodities.Add(New CommodityDetail())
            UspsShip1.Commodities(commCount).Description = _strCommoDescription
            UspsShip1.Commodities(commCount).Quantity = _Quantity
            UspsShip1.Commodities(commCount).QuantityUnit = GetCommodityUnit(_strUnitName)
            UspsShip1.Commodities(commCount).UnitPrice = CCommon.ToString(_dblUnitPrice)
            UspsShip1.Commodities(commCount).Weight = CCommon.ToString(_WeightInLbs)
        End Sub

        Public Function ResizeImageFile(ByVal strShippingImage As String, imageFile As Byte()) As Byte()
            ' Set targetSize to 1024
            Using oldImage As System.Drawing.Image = System.Drawing.Image.FromStream(New MemoryStream(imageFile))
                Dim newSize As Size = CalculateDimensions(oldImage.Size, _intImageWidth, _intImageHeight)
                Using newImage As New Bitmap(newSize.Width, newSize.Height, PixelFormat.Format24bppRgb)
                    Using canvas As Graphics = Graphics.FromImage(newImage)
                        canvas.SmoothingMode = SmoothingMode.AntiAlias
                        canvas.InterpolationMode = InterpolationMode.HighQualityBicubic
                        canvas.PixelOffsetMode = PixelOffsetMode.HighQuality
                        canvas.DrawImage(oldImage, New Rectangle(New Point(0, 0), newSize))
                        Dim m As New MemoryStream()
                        newImage.Save(m, ImageFormat.Jpeg)
                        newImage.Save(strShippingImage)
                    End Using
                End Using
            End Using
        End Function

        Public Shared Function CalculateDimensions(oldSize As Size, ByVal intWidth As Integer, ByVal intHeight As Integer) As Size
            Dim newSize As New Size()
            If intWidth > 0 AndAlso intHeight > 0 Then
                newSize.Width = intWidth
                newSize.Height = intHeight
            Else
                newSize.Width = oldSize.Width
                newSize.Height = oldSize.Height
            End If

            'If oldSize.Height > oldSize.Width Then
            '    newSize.Width = CInt(oldSize.Width * (CSng(targetSize) / CSng(oldSize.Height)))
            '    newSize.Height = targetSize
            'Else
            '    newSize.Width = targetSize
            '    newSize.Height = CInt(oldSize.Height * (CSng(targetSize) / CSng(oldSize.Width)))
            'End If
            Return newSize
        End Function

        Public Function imageToByteArray(imageIn As System.Drawing.Image) As Byte()
            Dim ms As New MemoryStream()
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif)
            Return ms.ToArray()
        End Function

        Public Function byteArrayToImage(byteArrayIn As Byte()) As System.Drawing.Image
            Dim ms As New MemoryStream(byteArrayIn)
            Dim returnImage As System.Drawing.Image = System.Drawing.Image.FromStream(ms)
            Return returnImage
        End Function

#End Region

    End Class
End Namespace
