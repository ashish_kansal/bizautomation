﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common

Public Class ECommerceCCTransactionLog
    Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

    Public Property ID As Long
    Public Property OppID As Long
    Public Property Amount As Decimal
    Public Property IsNSoftwareCallStarted As Boolean
    Public Property IsNSoftwareCallCompleted As Boolean
    Public Property IsSuccess As Boolean
    Public Property TransactionHistoryID As Long
    Public Property Message As String
    Public Property ExceptionMessage As String
    Public Property StackStrace As String

#End Region

#Region "Public Methods"

    Public Function CallStarted() As Long
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim ds As DataSet
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@monAmount", Amount, NpgsqlTypes.NpgsqlDbType.Numeric))
                .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
            End With

            Return CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_ECommerceCreditCardTransactionLog_CallStarted", sqlParams.ToArray()))
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub CallCompleted()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numID", ID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@bitSuccess", IsSuccess, NpgsqlTypes.NpgsqlDbType.Bit))
                .Add(SqlDAL.Add_Parameter("@vcMessage", Message, NpgsqlTypes.NpgsqlDbType.VarChar))
                .Add(SqlDAL.Add_Parameter("@vcExceptionMessage", ExceptionMessage, NpgsqlTypes.NpgsqlDbType.VarChar))
                .Add(SqlDAL.Add_Parameter("@vcStackStrace", StackStrace, NpgsqlTypes.NpgsqlDbType.Varchar))
                .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
            End With

            SqlDAL.ExecuteNonQuery(connString, "USP_ECommerceCreditCardTransactionLog_CallCompleted", sqlParams.ToArray())
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub UpdateTransactionHistoryID()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numID", ID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numTransHistoryID", TransactionHistoryID, NpgsqlTypes.NpgsqlDbType.BigInt))
            End With

            SqlDAL.ExecuteNonQuery(connString, "USP_ECommerceCreditCardTransactionLog_UpdateTHID", sqlParams.ToArray())
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

End Class
