﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common

Public Class TransactionHistory
    Inherits BACRM.BusinessLogic.CBusinessBase

    Private _TransHistoryID As Long
    Public Property TransHistoryID() As Long
        Get
            Return _TransHistoryID
        End Get
        Set(ByVal value As Long)
            _TransHistoryID = value
        End Set
    End Property

    Private _DivisionID As Long
    Public Property DivisionID() As Long
        Get
            Return _DivisionID
        End Get
        Set(ByVal value As Long)
            _DivisionID = value
        End Set
    End Property


    Private _ContactID As Long
    Public Property ContactID() As Long
        Get
            Return _ContactID
        End Get
        Set(ByVal value As Long)
            _ContactID = value
        End Set
    End Property


    Private _OppID As Long
    Public Property OppID() As Long
        Get
            Return _OppID
        End Get
        Set(ByVal value As Long)
            _OppID = value
        End Set
    End Property


    Private _OppBizDocsID As Long
    Public Property OppBizDocsID() As Long
        Get
            Return _OppBizDocsID
        End Get
        Set(ByVal value As Long)
            _OppBizDocsID = value
        End Set
    End Property


    Private _TransactionID As String
    Public Property TransactionID() As String
        Get
            Return _TransactionID
        End Get
        Set(ByVal value As String)
            _TransactionID = value
        End Set
    End Property


    Private _TransactionStatus As Short
    Public Property TransactionStatus() As Short
        Get
            Return _TransactionStatus
        End Get
        Set(ByVal value As Short)
            _TransactionStatus = value
        End Set
    End Property


    Private _PGResponse As String
    Public Property PGResponse() As String
        Get
            Return _PGResponse
        End Get
        Set(ByVal value As String)
            _PGResponse = value
        End Set
    End Property


    Private _Type As Short
    Public Property Type() As Short
        Get
            Return _Type
        End Get
        Set(ByVal value As Short)
            _Type = value
        End Set
    End Property


    Private _AuthorizedAmt As Decimal
    Public Property AuthorizedAmt() As Decimal
        Get
            Return _AuthorizedAmt
        End Get
        Set(ByVal value As Decimal)
            _AuthorizedAmt = value
        End Set
    End Property


    Private _CapturedAmt As Decimal
    Public Property CapturedAmt() As Decimal
        Get
            Return _CapturedAmt
        End Get
        Set(ByVal value As Decimal)
            _CapturedAmt = value
        End Set
    End Property


    Private _RefundAmt As Decimal
    Public Property RefundAmt() As Decimal
        Get
            Return _RefundAmt
        End Get
        Set(ByVal value As Decimal)
            _RefundAmt = value
        End Set
    End Property


    Private _CardHolder As String
    Public Property CardHolder() As String
        Get
            Return _CardHolder
        End Get
        Set(ByVal value As String)
            _CardHolder = value
        End Set
    End Property


    Private _CreditCardNo As String
    Public Property CreditCardNo() As String
        Get
            Return _CreditCardNo
        End Get
        Set(ByVal value As String)
            _CreditCardNo = value
        End Set
    End Property


    Private _Cvv2 As String
    Public Property Cvv2() As String
        Get
            Return _Cvv2
        End Get
        Set(ByVal value As String)
            _Cvv2 = value
        End Set
    End Property


    Private _ValidMonth As Short
    Public Property ValidMonth() As Short
        Get
            Return _ValidMonth
        End Get
        Set(ByVal value As Short)
            _ValidMonth = value
        End Set
    End Property


    Private _ValidYear As Integer
    Public Property ValidYear() As Integer
        Get
            Return _ValidYear
        End Get
        Set(ByVal value As Integer)
            _ValidYear = value
        End Set
    End Property


    Private _CreatedBy As Long
    Public Property CreatedBy() As Long
        Get
            Return _CreatedBy
        End Get
        Set(ByVal value As Long)
            _CreatedBy = value
        End Set
    End Property


    Private _CreatedDate As DateTime
    Public Property CreatedDate() As DateTime
        Get
            Return _CreatedDate
        End Get
        Set(ByVal value As DateTime)
            _CreatedDate = value
        End Set
    End Property


    Private _ModifiedBy As Long
    Public Property ModifiedBy() As Long
        Get
            Return _ModifiedBy
        End Get
        Set(ByVal value As Long)
            _ModifiedBy = value
        End Set
    End Property


    Private _ModifiedDate As DateTime
    Public Property ModifiedDate() As DateTime
        Get
            Return _ModifiedDate
        End Get
        Set(ByVal value As DateTime)
            _ModifiedDate = value
        End Set
    End Property

    Private _SignatureFile As String
    Public Property SignatureFile() As String
        Get
            Return _SignatureFile
        End Get
        Set(ByVal value As String)
            _SignatureFile = value
        End Set
    End Property
    Private _CardType As Long
    Public Property CardType() As Long
        Get
            Return _CardType
        End Get
        Set(ByVal value As Long)
            _CardType = value
        End Set
    End Property

    Public Property ResponseCode As String
    Public Property PaymentGatewayID As Integer

    Function GetTransactionHistory() As DataTable
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim ds As DataSet
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams

                .Add(SqlDAL.Add_Parameter("@numTransHistoryID", _TransHistoryID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@numOppID", _OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
            End With

            ds = SqlDAL.ExecuteDataset(connString, "USP_GetTransactionHistory", sqlParams.ToArray())
            Return ds.Tables(0)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function GetCreditCardDetails() As DataTable
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim ds As DataSet
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams

                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@numContactId", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@vcCreditCardNo", _CreditCardNo, NpgsqlTypes.NpgsqlDbType.VarChar))
                .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
            End With

            ds = SqlDAL.ExecuteDataset(connString, "USP_GetTransactionHistoryByCreditCard", sqlParams.ToArray())
            Return ds.Tables(0)

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function ManageTransactionHistory() As Long
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim ds As DataSet
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams

                .Add(SqlDAL.Add_Parameter("@numTransHistoryID", _TransHistoryID, NpgsqlTypes.NpgsqlDbType.BigInt, 9, ParameterDirection.InputOutput))

                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@numContactID", _ContactID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@numOppID", _OppID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@numOppBizDocsID", _OppBizDocsID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@vcTransactionID", _TransactionID, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                .Add(SqlDAL.Add_Parameter("@tintTransactionStatus", _TransactionStatus, NpgsqlTypes.NpgsqlDbType.Smallint))

                .Add(SqlDAL.Add_Parameter("@vcPGResponse", _PGResponse, NpgsqlTypes.NpgsqlDbType.VarChar, 200))

                .Add(SqlDAL.Add_Parameter("@tintType", _Type, NpgsqlTypes.NpgsqlDbType.Smallint))

                .Add(SqlDAL.Add_Parameter("@monAuthorizedAmt", _AuthorizedAmt, NpgsqlTypes.NpgsqlDbType.Numeric))

                .Add(SqlDAL.Add_Parameter("@monCapturedAmt", _CapturedAmt, NpgsqlTypes.NpgsqlDbType.Numeric))

                .Add(SqlDAL.Add_Parameter("@monRefundAmt", _RefundAmt, NpgsqlTypes.NpgsqlDbType.Numeric))

                .Add(SqlDAL.Add_Parameter("@vcCardHolder", _CardHolder, NpgsqlTypes.NpgsqlDbType.VarChar, 500))

                .Add(SqlDAL.Add_Parameter("@vcCreditCardNo", _CreditCardNo, NpgsqlTypes.NpgsqlDbType.VarChar, 500))

                .Add(SqlDAL.Add_Parameter("@vcCVV2", _Cvv2, NpgsqlTypes.NpgsqlDbType.VarChar, 200))

                .Add(SqlDAL.Add_Parameter("@tintValidMonth", _ValidMonth, NpgsqlTypes.NpgsqlDbType.Smallint))

                .Add(SqlDAL.Add_Parameter("@intValidYear", _ValidYear, NpgsqlTypes.NpgsqlDbType.Integer))

                .Add(SqlDAL.Add_Parameter("@vcSignatureFile", _SignatureFile, NpgsqlTypes.NpgsqlDbType.VarChar, 100))

                .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@numCardType", CardType, NpgsqlTypes.NpgsqlDbType.BigInt))

                .Add(SqlDAL.Add_Parameter("@vcResponseCode", ResponseCode, NpgsqlTypes.NpgsqlDbType.VarChar))

                .Add(SqlDAL.Add_Parameter("@intPaymentGateWay", PaymentGatewayID, NpgsqlTypes.NpgsqlDbType.Integer))

            End With

            Dim objParam() As Object
            objParam = sqlParams.ToArray()
            SqlDAL.ExecuteNonQuery(connString, "USP_ManageTransactionHistory", objParam, True)
            _TransHistoryID = CCommon.ToLong(DirectCast(objParam, Npgsql.NpgsqlParameter())(0).Value)

            Return _TransHistoryID
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Gets previously authrozed amount with amount to capture >= amount parameter value
    ''' </summary>
    ''' <param name="amount">Amount to be charged from authorized amount</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetAuthrizedTrasaction(ByVal amount As Decimal, ByVal mode As Short) As DataTable
        Try
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim ds As DataSet
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@monAmount", amount, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@tintMode", mode, NpgsqlTypes.NpgsqlDbType.Smallint)) '1 = AUTHORIZED ONLY TRNSACTION WHICH MATCH AMOUNT VALUE, 2 = LAST VALID AUTHORIZED ONLY TRASACTION
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                ds = SqlDAL.ExecuteDataset(connString, "USP_TransactionHistory_GetAuthrizedTrasaction", sqlParams.ToArray())

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        Catch ex As Exception
            Throw
        End Try
    End Function

    Function GetOrderTransactionHistory()
        Try
            Dim getconnection As New GetConnection
            Dim connString As String = getconnection.GetConnectionString
            Dim ds As DataSet
            Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)

            With sqlParams
                .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                .Add(SqlDAL.Add_Parameter("@numOppID", _OppID, NpgsqlTypes.NpgsqlDbType.Bigint))
                .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
            End With

            ds = SqlDAL.ExecuteDataset(connString, "USP_TransactionHistory_GetByOrder", sqlParams.ToArray())
            Return ds.Tables(0)
        Catch ex As Exception
            Throw
        End Try
    End Function

    '*******Code to call InsertUPdate Method from BLL
    'Dim objTransactionHistory As New TransactionHistory
    '    objTransactionHistory.TransHistoryID = ""
    '    objTransactionHistory.DomainID = ""
    '    objTransactionHistory.DivisionID = ""
    '    objTransactionHistory.ContactID = ""
    '    objTransactionHistory.OppID = ""
    '    objTransactionHistory.OppBizDocsID = ""
    '    objTransactionHistory.TransactionID = ""
    '    objTransactionHistory.TransactionStatus = ""
    '    objTransactionHistory.PGResponse = ""
    '    objTransactionHistory.Type = ""
    '    objTransactionHistory.AuthorizedAmt = ""
    '    objTransactionHistory.CapturedAmt = ""
    '    objTransactionHistory.RefundAmt = ""
    '    objTransactionHistory.CardHolder = ""
    '    objTransactionHistory.CreditCardNo = ""
    '    objTransactionHistory.CVV2 = ""
    '    objTransactionHistory.ValidMonth = ""
    '    objTransactionHistory.ValidYear = ""
    '    objTransactionHistory.CreatedBy = ""
    '    objTransactionHistory.CreatedDate = ""
    '    objTransactionHistory.ModifiedBy = ""
    '    objTransactionHistory.ModifiedDate = ""
    '    objTransactionHistory.UserCntID = Session("UserContactID")
    '    objTransactionHistory.DomainID = Session("DomainID")
    '    objTransactionHistory.ManageTransactionHistory()

End Class
