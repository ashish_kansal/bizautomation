﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports System.Collections.Generic

Namespace BACRM.BusinessLogic.Opportunities

    Public Class SalesOrderLineItemsPOLinking
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property SalesOrderID As Long
        Public Property SalesOrderOppChildItemID As Long
        Public Property SalesOrderOppKitChildItemID As Long
        Public Property SalesOrderItemID As Long
        Public Property PurchaseOrderID As Long
        Public Property PurchaseOrderItemID As Long

#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSalesOrderID", SalesOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSalesOrderItemID", SalesOrderItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numSalesOrderOppChildItemID", SalesOrderOppChildItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numSalesOrderOppKitChildItemID", SalesOrderOppKitChildItemID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@numPurchaseOrderID", PurchaseOrderID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPurchaseOrderItemID", PurchaseOrderItemID, NpgsqlTypes.NpgsqlDbType.BigInt))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_SalesOrderLineItemsPOLinking_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class

End Namespace