﻿Imports BACRM.BusinessLogic.Common
Imports BACRMAPI.DataAccessLayer
Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Collections.Generic
Imports System.Data.SqlClient

Namespace BACRM.BusinessLogic.Opportunities
    Public Class MassPurchaseFulfillmentConfiguration
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property IsGroupByOrderForReceive As Boolean
        Public Property IsGroupByOrderForPutAway As Boolean
        Public Property IsGroupByOrderForBill As Boolean
        Public Property IsGroupByOrderForClose As Boolean
        Public Property IsShowOnlyFullyReceived As Boolean
        Public Property IsReceiveBillOnClose As Boolean
        Public Property BillType As Short
        Public Property ScanValue As Short

#End Region

#Region "Public Methods"

        Public Function GetByUser() As DataTable
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_MassPurchaseFulfillmentConfiguration_GetByUser", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Sub Save()
            Try
                Dim sqlParams As New List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrderForReceive", IsGroupByOrderForReceive, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrderForPutAway", IsGroupByOrderForPutAway, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrderForBill", IsGroupByOrderForBill, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitGroupByOrderForClose", IsGroupByOrderForClose, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitShowOnlyFullyReceived", IsShowOnlyFullyReceived, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitReceiveBillOnClose", IsReceiveBillOnClose, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@tintScanValue", ScanValue, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintBillType", BillType, NpgsqlTypes.NpgsqlDbType.Smallint))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_MassPurchaseFulfillmentConfiguration_Save", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class
End Namespace


