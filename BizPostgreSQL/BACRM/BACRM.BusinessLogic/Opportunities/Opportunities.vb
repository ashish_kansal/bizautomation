'**********************************************************************************
' <COpportunities.vb>
' 
' 	CHANGE CONTROL:
'	
'	AUTHOR: Goyal 	DATE:28-Feb-05 	VERSION	CHANGES	KEYSTRING:
'**********************************************************************************

Option Explicit On
Option Strict On

Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports Npgsql

Namespace BACRM.BusinessLogic.Opportunities

    '**********************************************************************************
    ' Module Name  : None
    ' Module Type  : CCommon
    ' 
    ' Description  : This is the business logic classe for Common Stuff
    '**********************************************************************************
    Public Class COpportunities
        Inherits BACRM.BusinessLogic.CBusinessBase


        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Goyal 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        ''Private DomainId As Long = 0
        Private _ContactType As Long = 0
        Private _ContactID As Integer = 0
        Private _FirstName As String = String.Empty
        Private _LastName As String = String.Empty
        Private _Name As String = String.Empty
        Private _CompanyName As String = String.Empty
        Private _CompanyID As Integer = 0
        Private _DivisionID As Integer = 0
        Private _UserRights As Integer = 0
        Private _ContactNO As String = String.Empty
        Private _ShowAll As Integer = 0
        Private _CreatedBy As Long = 0
        Private _AssignedTo As Long = 0
        Private _ModifiedBy As Long = 0
        Private _UserRightType As Short
        Private _SortOrder As Integer
        Private _SortChar As String = String.Empty
        Private _CustName As String = String.Empty
        Private _OpportID As Long = 0
        Private _ItemCode As Long = 0
        Private _TerritoryID As Long = 0
        Private _CompFilter As String = String.Empty
        Private _CompClosedDate As Long = 0
        Private _AcctClosedDate As Long = 0
        Private _Comments As String = String.Empty
        Private _PubFlag As Integer = 0
        Private _PSource As Integer = 0
        Private _OpportName As String = String.Empty
        Private _PEstCloseDate As Long = 0
        Private _PClosingPercent As Long = 0
        Private _PStage As Long = 0
        Private _PAmount As Integer = 0
        Private _PConclAnalysis As Long = 0
        Private _POpenDate As Long = 0
        Private _PRating As Integer = 0
        Private _PQBInvoiceNo As String = String.Empty
        Private _PClosingDate As Long = 0
        Private _UnitHour As Double = 0
        Private _Price As Double = 0
        Private _TotAmount As Double = 0
        Private _Message As String = String.Empty
        Private _NoofDays As Integer = 0
        Private _OppName As String = String.Empty
        Private _CampaignID As Long = 0
        Private _CRMTYPE As Integer = 0
        Private _DelFlag As Integer = 0
        Private _AssignTo As Long = 0
        Private _Alert As Integer = 0
        Private _StageDetail As String = String.Empty
        Private _StageCompleted As Integer = 0
        Private _Flag As String = String.Empty
        Private _StageDetailsId As Long = 0
        Private _SubStageDetailsId As Long = 0
        Private _StagePercentageId As Long = 0
        Private _StageNumber As Long = 0
        Private _SlpID As Integer = 0
        Private _OppType As Integer = 0
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer
        Private _SortCharacter As Char
        Private _columnSortOrder As String
        Private _columnName As String
        Private _OppStatus As Short
        Private _RepType As Short ''Individual or Team
        Private _startDate As Date
        Private _endDate As Date
        Private _SalesDealsUserRights As Short
        Private _PurchaseDealsUserRights As Short
        Private _strUserIDs As String
        Private _byteMode As Short
        Private _bitPartner As Boolean
        Private _TeamType As Short
        Private _OppItemCode As Long
        Private _strItemDtls As String
        Private _ClientTimeZoneOffset As Integer
        Private _Shipped As Short
        Private _intOrder As Short = 0
        Private _intType As Short
        Private _bitAll As Boolean
        Private _vcCoupon As String

        Public Property vcCoupon As String
            Get
                Return _vcCoupon
            End Get
            Set(value As String)
                _vcCoupon = value
            End Set
        End Property
        Public Property bitAll() As Boolean
            Get
                Return _bitAll
            End Get
            Set(ByVal Value As Boolean)
                _bitAll = Value
            End Set
        End Property
        Public Property intType() As Short
            Get
                Return _intType
            End Get
            Set(ByVal Value As Short)
                _intType = Value
            End Set
        End Property
        Public Property intOrder() As Short
            Get
                Return _intOrder
            End Get
            Set(ByVal Value As Short)
                _intOrder = Value
            End Set
        End Property

        Public Property Shipped() As Short
            Get
                Return _Shipped
            End Get
            Set(ByVal Value As Short)
                _Shipped = Value
            End Set
        End Property

        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property
        Public Property strItemDtls() As String
            Get
                Return _strItemDtls
            End Get
            Set(ByVal Value As String)
                _strItemDtls = Value
            End Set
        End Property

        Public Property OppItemCode() As Long
            Get
                Return _OppItemCode
            End Get
            Set(ByVal Value As Long)
                _OppItemCode = Value
            End Set
        End Property

        Public Property bitPartner() As Boolean
            Get
                Return _bitPartner
            End Get
            Set(ByVal Value As Boolean)
                _bitPartner = Value
            End Set
        End Property

        Public Property byteMode() As Short
            Get
                Return _byteMode
            End Get
            Set(ByVal Value As Short)
                _byteMode = Value
            End Set
        End Property

        Public Property strUserIDs() As String
            Get
                Return _strUserIDs
            End Get
            Set(ByVal Value As String)
                _strUserIDs = Value
            End Set
        End Property

        Public Property PurchaseDealsUserRights() As Short
            Get
                Return _PurchaseDealsUserRights
            End Get
            Set(ByVal Value As Short)
                _PurchaseDealsUserRights = Value
            End Set
        End Property

        Public Property SalesDealsUserRights() As Short
            Get
                Return _SalesDealsUserRights
            End Get
            Set(ByVal Value As Short)
                _SalesDealsUserRights = Value
            End Set
        End Property

        Public Property endDate() As Date
            Get
                Return _endDate
            End Get
            Set(ByVal Value As Date)
                _endDate = Value
            End Set
        End Property

        Public Property startDate() As Date
            Get
                Return _startDate
            End Get
            Set(ByVal Value As Date)
                _startDate = Value
            End Set
        End Property

        Public Property RepType() As Short
            Get
                Return _RepType
            End Get
            Set(ByVal Value As Short)
                _RepType = Value
            End Set
        End Property

        Public Property OppStatus() As Short
            Get
                Return _OppStatus
            End Get
            Set(ByVal Value As Short)
                _OppStatus = Value
            End Set
        End Property

        Public Property columnName() As String
            Get
                Return _columnName
            End Get
            Set(ByVal Value As String)
                _columnName = Value
            End Set
        End Property

        Public Property columnSortOrder() As String
            Get
                Return _columnSortOrder
            End Get
            Set(ByVal Value As String)
                _columnSortOrder = Value
            End Set
        End Property

        Public Property SortCharacter() As Char
            Get
                Return _SortCharacter
            End Get
            Set(ByVal Value As Char)
                _SortCharacter = Value
            End Set
        End Property

        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Public Property OppType() As Integer
            Get
                Return _OppType
            End Get
            Set(ByVal Value As Integer)
                _OppType = Value
            End Set
        End Property
        Public Property SlpID() As Integer
            Get
                Return _SlpID
            End Get
            Set(ByVal Value As Integer)
                _SlpID = Value
            End Set
        End Property
        Public Property StagePercentageId() As Long
            Get
                Return _StagePercentageId
            End Get
            Set(ByVal Value As Long)
                _StagePercentageId = Value
            End Set
        End Property
        Public Property StageNumber() As Long
            Get
                Return _StageNumber
            End Get
            Set(ByVal Value As Long)
                _StageNumber = Value
            End Set
        End Property

        Public Property SubStageDetailsId() As Long
            Get
                Return _SubStageDetailsId
            End Get
            Set(ByVal Value As Long)
                _SubStageDetailsId = Value
            End Set
        End Property
        Public Property StageDetailsId() As Long
            Get
                Return _StageDetailsId
            End Get
            Set(ByVal Value As Long)
                _StageDetailsId = Value
            End Set
        End Property


        Public Property AssignTo() As Long
            Get
                Return _AssignTo
            End Get
            Set(ByVal Value As Long)
                _AssignTo = Value
            End Set
        End Property

        Public Property Alert() As Integer
            Get
                Return _Alert
            End Get
            Set(ByVal Value As Integer)
                _Alert = Value
            End Set
        End Property

        Public Property Flag() As String
            Get
                Return _Flag
            End Get
            Set(ByVal Value As String)
                _Flag = Value
            End Set
        End Property
        Public Property StageCompleted() As Integer
            Get
                Return _StageCompleted
            End Get
            Set(ByVal Value As Integer)
                _StageCompleted = Value
            End Set
        End Property

        Public Property StageDetail() As String
            Get
                Return _StageDetail
            End Get
            Set(ByVal Value As String)
                _StageDetail = Value
            End Set
        End Property

        Public Property CompClosedDate() As Long
            Get
                Return _CompClosedDate
            End Get
            Set(ByVal Value As Long)
                _CompClosedDate = Value
            End Set
        End Property
        Public Property PubFlag() As Integer
            Get
                Return _PubFlag
            End Get
            Set(ByVal Value As Integer)
                _PubFlag = Value
            End Set
        End Property

        Public Property Comments() As String
            Get
                Return _Comments
            End Get
            Set(ByVal Value As String)
                _Comments = Value
            End Set
        End Property
        Public Property AcctClosedDate() As Long
            Get
                Return _AcctClosedDate
            End Get
            Set(ByVal Value As Long)
                _AcctClosedDate = Value
            End Set
        End Property
        Public Property PEstCloseDate() As Long
            Get
                Return _PEstCloseDate
            End Get
            Set(ByVal Value As Long)
                _PEstCloseDate = Value
            End Set
        End Property

        Public Property OpportName() As String
            Get
                Return _OpportName
            End Get
            Set(ByVal Value As String)
                _OpportName = Value
            End Set
        End Property
        Public Property PSource() As Integer
            Get
                Return _PSource
            End Get
            Set(ByVal Value As Integer)
                _PSource = Value
            End Set
        End Property


        Public Property PAmount() As Integer
            Get
                Return _PAmount
            End Get
            Set(ByVal Value As Integer)
                _PAmount = Value
            End Set
        End Property

        Public Property PStage() As Long
            Get
                Return _PStage
            End Get
            Set(ByVal Value As Long)
                _PStage = Value
            End Set
        End Property
        Public Property PClosingPercent() As Long
            Get
                Return _PClosingPercent
            End Get
            Set(ByVal Value As Long)
                _PClosingPercent = Value
            End Set
        End Property
        Public Property PRating() As Integer
            Get
                Return _PRating
            End Get
            Set(ByVal Value As Integer)
                _PRating = Value
            End Set
        End Property

        Public Property POpenDate() As Long
            Get
                Return _POpenDate
            End Get
            Set(ByVal Value As Long)
                _POpenDate = Value
            End Set
        End Property
        Public Property PConclAnalysis() As Long
            Get
                Return _PConclAnalysis
            End Get
            Set(ByVal Value As Long)
                _PConclAnalysis = Value
            End Set
        End Property

        Public Property PClosingDate() As Long
            Get
                Return _PClosingDate
            End Get
            Set(ByVal Value As Long)
                _PClosingDate = Value
            End Set
        End Property
        Public Property PQBInvoiceNo() As String
            Get
                Return _PQBInvoiceNo
            End Get
            Set(ByVal Value As String)
                _PQBInvoiceNo = Value
            End Set
        End Property

        Public Property Price() As Double
            Get
                Return _Price
            End Get
            Set(ByVal Value As Double)
                _Price = Value
            End Set
        End Property

        Public Property UnitHour() As Double
            Get
                Return _UnitHour
            End Get
            Set(ByVal Value As Double)
                _UnitHour = Value
            End Set
        End Property


        Public Property NoofDays() As Integer
            Get
                Return _NoofDays
            End Get
            Set(ByVal Value As Integer)
                _NoofDays = Value
            End Set
        End Property

        Public Property Message() As String
            Get
                Return _Message
            End Get
            Set(ByVal Value As String)
                _Message = Value
            End Set
        End Property

        Public Property TotAmount() As Double
            Get
                Return _TotAmount
            End Get
            Set(ByVal Value As Double)
                _TotAmount = Value
            End Set
        End Property
        Public Property OppName() As String
            Get
                Return _OppName
            End Get
            Set(ByVal Value As String)
                _OppName = Value
            End Set
        End Property

        Public Property CampaignID() As Long
            Get
                Return _CampaignID
            End Get
            Set(ByVal Value As Long)
                _CampaignID = Value
            End Set
        End Property

        Public Property CRMTYPE() As Integer
            Get
                Return _CRMTYPE
            End Get
            Set(ByVal Value As Integer)
                _CRMTYPE = Value
            End Set
        End Property


        Public Property CompFilter() As String
            Get
                Return _CompFilter
            End Get
            Set(ByVal Value As String)
                _CompFilter = Value
            End Set
        End Property
        Public Property TerritoryID() As Long
            Get
                Return _TerritoryID
            End Get
            Set(ByVal Value As Long)
                _TerritoryID = Value
            End Set
        End Property
        Public Property OpportID() As Long
            Get
                Return _OpportID
            End Get
            Set(ByVal Value As Long)
                _OpportID = Value
            End Set
        End Property


        Public Property CustName() As String
            Get
                Return _CustName
            End Get
            Set(ByVal Value As String)
                _CustName = Value
            End Set
        End Property
        Public Property UserRightType() As Short
            Get
                Return _UserRightType
            End Get
            Set(ByVal Value As Short)
                _UserRightType = Value
            End Set
        End Property
        Public Property SortOrder() As Integer
            Get
                Return _SortOrder
            End Get
            Set(ByVal Value As Integer)
                _SortOrder = Value
            End Set
        End Property
        Public Property SortChar() As String
            Get
                Return _SortChar
            End Get
            Set(ByVal Value As String)
                _SortChar = Value
            End Set
        End Property

        Public Property CreatedBy() As Long
            Get
                Return _CreatedBy
            End Get
            Set(ByVal Value As Long)
                _CreatedBy = Value
            End Set
        End Property
        Public Property ModifiedBy() As Long
            Get
                Return _ModifiedBy
            End Get
            Set(ByVal Value As Long)
                _ModifiedBy = Value
            End Set
        End Property
        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property
        Public Property ContactType() As Long
            Get
                Return _ContactType
            End Get
            Set(ByVal Value As Long)
                _ContactType = Value
            End Set
        End Property
        Public Property ContactID() As Integer
            Get
                Return _ContactID
            End Get
            Set(ByVal Value As Integer)
                _ContactID = Value
            End Set
        End Property
        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Integer)
        '        UserCntID = Value
        '    End Set
        'End Property
        Public Property FirstName() As String
            Get
                Return _FirstName
            End Get
            Set(ByVal Value As String)
                _FirstName = Value
            End Set
        End Property
        Public Property LastName() As String
            Get
                Return _LastName
            End Get
            Set(ByVal Value As String)
                _LastName = Value
            End Set
        End Property
        Public Property Name() As String
            Get
                Return _Name
            End Get
            Set(ByVal Value As String)
                _Name = Value
            End Set
        End Property
        Public Property CompanyName() As String
            Get
                Return _CompanyName
            End Get
            Set(ByVal Value As String)
                _CompanyName = Value
            End Set
        End Property
        Public Property CompanyID() As Integer
            Get
                Return _CompanyID
            End Get
            Set(ByVal Value As Integer)
                _CompanyID = Value
            End Set
        End Property
        Public Property DivisionID() As Integer
            Get
                Return _DivisionID
            End Get
            Set(ByVal Value As Integer)
                _DivisionID = Value
            End Set
        End Property
        Public Property UserRights() As Integer
            Get
                Return _UserRights
            End Get
            Set(ByVal Value As Integer)
                _UserRights = Value
            End Set
        End Property
        Public Property ContactNo() As String
            Get
                Return _ContactNO
            End Get
            Set(ByVal Value As String)
                _ContactNO = Value
            End Set
        End Property

        Public Property AssignedTo() As Long
            Get
                Return _AssignedTo
            End Get
            Set(ByVal Value As Long)
                _AssignedTo = Value
            End Set
        End Property
        Public Property ItemCode() As Long
            Get
                Return _ItemCode
            End Get
            Set(ByVal Value As Long)
                _ItemCode = Value
            End Set
        End Property


        Public Property TeamType() As Short
            Get
                Return _TeamType
            End Get
            Set(ByVal Value As Short)
                _TeamType = Value
            End Set
        End Property

        Private _FilterBy As Short
        Public Property FilterBy() As Short
            Get
                Return _FilterBy
            End Get
            Set(ByVal value As Short)
                _FilterBy = value
            End Set
        End Property

        Private _SalesTemplateID As Long
        Public Property SalesTemplateID() As Long
            Get
                Return _SalesTemplateID
            End Get
            Set(ByVal value As Long)
                _SalesTemplateID = value
            End Set
        End Property

        Private _TemplateName As String
        Public Property TemplateName() As String
            Get
                Return _TemplateName
            End Get
            Set(ByVal value As String)
                _TemplateName = value
            End Set
        End Property

        Private _TemplateType As Short
        Public Property TemplateType() As Short
            Get
                Return _TemplateType
            End Get
            Set(ByVal value As Short)
                _TemplateType = value
            End Set
        End Property

        Private _SalesTemplateItemID As Long
        Public Property SalesTemplateItemID() As Long
            Get
                Return _SalesTemplateItemID
            End Get
            Set(ByVal value As Long)
                _SalesTemplateItemID = value
            End Set
        End Property

        Private _OrderStatus As Long
        Public Property OrderStatus() As Long
            Get
                Return _OrderStatus
            End Get
            Set(ByVal value As Long)
                _OrderStatus = value
            End Set
        End Property

        Private _BizDocId As Long
        Public Property BizDocId() As Long
            Get
                Return _BizDocId
            End Get
            Set(ByVal value As Long)
                _BizDocId = value
            End Set
        End Property

        Private _WarehouseItmsID As Long
        Public Property WarehouseItmsID() As Long
            Get
                Return _WarehouseItmsID
            End Get
            Set(ByVal value As Long)
                _WarehouseItmsID = value
            End Set
        End Property

        Private _bitSerialized As Boolean
        Public Property bitSerialized() As Boolean
            Get
                Return _bitSerialized
            End Get
            Set(ByVal value As Boolean)
                _bitSerialized = value
            End Set
        End Property

        Private _bitLotNo As Boolean
        Public Property bitLotNo() As Boolean
            Get
                Return _bitLotNo
            End Get
            Set(ByVal value As Boolean)
                _bitLotNo = value
            End Set
        End Property
        Private _bitflag As Boolean
        Public Property bitflag As Boolean
            Get
                Return _bitflag
            End Get
            Set(ByVal value As Boolean)
                _bitflag = value
            End Set
        End Property

        Private _RegularSearchCriteria As String
        Public Property RegularSearchCriteria() As String
            Get
                Return _RegularSearchCriteria
            End Get
            Set(ByVal Value As String)
                _RegularSearchCriteria = Value
            End Set
        End Property

        Private _CustomSearchCriteria As String
        Public Property CustomSearchCriteria() As String
            Get
                Return _CustomSearchCriteria
            End Get
            Set(ByVal Value As String)
                _CustomSearchCriteria = Value
            End Set
        End Property

        Private _ViewID As Long
        Public Property ViewID() As Long
            Get
                Return _ViewID
            End Get
            Set(ByVal Value As Long)
                _ViewID = Value
            End Set
        End Property

        Private _strOrderIDs As String
        Public Property strOrderID() As String
            Get
                Return _strOrderIDs
            End Get
            Set(ByVal value As String)
                _strOrderIDs = value
            End Set
        End Property

        Private _dtItemReceivedDate As DateTime
        Public Property dtItemReceivedDate() As DateTime
            Get
                Return _dtItemReceivedDate
            End Get
            Set(ByVal value As DateTime)
                _dtItemReceivedDate = value
            End Set
        End Property

        Private _strBizDocsIds As String
        Public Property strBizDocsIds() As String
            Get
                Return _strBizDocsIds
            End Get
            Set(ByVal value As String)
                _strBizDocsIds = value
            End Set
        End Property

        Private _AppliesTo As Short
        Public Property AppliesTo() As Short
            Get
                Return _AppliesTo
            End Get
            Set(ByVal value As Short)
                _AppliesTo = value
            End Set
        End Property

        Private _numUOM As Long
        Public Property numUOM() As Long
            Get
                Return _numUOM
            End Get
            Set(ByVal value As Long)
                _numUOM = value
            End Set
        End Property

        Private _vcUOMName As String
        Public Property vcUOMName() As String
            Get
                Return _vcUOMName
            End Get
            Set(ByVal value As String)
                _vcUOMName = value
            End Set
        End Property

        Private _vcAttributes As String
        Public Property vcAttributes() As String
            Get
                Return _vcAttributes
            End Get
            Set(ByVal value As String)
                _vcAttributes = value
            End Set
        End Property

        Private _vcAttrValues As String
        Public Property vcAttrValues() As String
            Get
                Return _vcAttrValues
            End Get
            Set(ByVal value As String)
                _vcAttrValues = value
            End Set
        End Property

        Public Property SearchText As String
        Public Property PromotionID As Long
        Public Property vcPromotionDetail As String
        Public Property WarehouseID As Long
        Public Property DashboardReminderType As Short
        Public Property CurrencyID As Long
        Public Property SiteID As Long

        Public Function DelOpp() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OpportID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = UserCntID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteOppurtunity", arParms)

                Return True
            Catch ex As Exception

                Throw ex
            Finally

            End Try

        End Function

        Public Function DemoteToOpportunity() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _OpportID


                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_DemoteToOpportunity", arParms)

                Return True
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ListContact() As DataSet
            Dim Connection As New Npgsql.NpgSqlConnection(GetConnection.GetConnectionString)
            Dim arParams() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

            arParams(0) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParams(0).Value = _ContactID

            arParams(1) = New Npgsql.NpgsqlParameter("@numDivID", NpgsqlTypes.NpgsqlDbType.Bigint)
            arParams(1).Value = _DivisionID

            arParams(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
            arParams(2).Value = 1

            arParams(3) = New Npgsql.NpgsqlParameter("@bitShowAll", NpgsqlTypes.NpgsqlDbType.Boolean)
            arParams(3).Value = True

            arParams(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arParams(4).Value = Nothing
            arParams(4).Direction = ParameterDirection.InputOutput

            ListContact = SqlDAL.ExecuteDataset(Connection, "usp_GetContactInfo", arParams)

        End Function

        Public Function ListSource() As DataSet
            Dim Connection As New Npgsql.NpgsqlConnection(GetConnection.GetConnectionString)

            Dim arparms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

            arparms(0) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
            arparms(0).Value = Nothing
            arparms(0).Direction = ParameterDirection.InputOutput

            ListSource = SqlDAL.ExecuteDataset(Connection, "usp_getSalesProcess", arparms)

        End Function




        Public Function CreateStagesPercentage1() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString

            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@StagePercentageId", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _StagePercentageId

                arParms(1) = New Npgsql.NpgsqlParameter("@StageNumber", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _StageNumber

                arParms(2) = New Npgsql.NpgsqlParameter("@StageDetail", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(2).Value = _StageDetail

                arParms(3) = New Npgsql.NpgsqlParameter("@SlpID", NpgsqlTypes.NpgsqlDbType.Integer, 4)
                arParms(3).Value = _SlpID

                arParms(4) = New Npgsql.NpgsqlParameter("@DomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@CreatedBy", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(5).Value = _CreatedBy


                arParms(6) = New Npgsql.NpgsqlParameter("@ModifiedBy", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(6).Value = _ModifiedBy

                arParms(7) = New Npgsql.NpgsqlParameter("@Flag", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(7).Value = _Flag

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "usp_InsertStagePercentageDetails", arParms)
                Return True
            Catch ex As Exception
                Throw ex
                Return False
                Throw ex
            End Try
        End Function
        Public Function RemoveSubOpportunity() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim Delint As Integer
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@OpportID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OpportID

                Delint = CInt(SqlDAL.ExecuteScalar(connString, "usp_deleteOppurtunitySubStage", arParms))

                Return True
            Catch ex As Exception

                Throw ex
            Finally

            End Try

        End Function
        Public Function RemoveOpportunityStages() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim Delint As Integer
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@OpportID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OpportID

                Delint = CInt(SqlDAL.ExecuteScalar(connString, "usp_DeleteOpportunityStageDetails", arParms))

                Return True
            Catch ex As Exception

                Throw ex
            Finally

            End Try

        End Function
        Public Function RemoveOpportunityStagesComments() As Boolean
            Dim getconnection As New GetConnection
            Dim connString As String = GetConnection.GetConnectionString
            Dim Delint As Integer
            Try
                ' Set up parameters 
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(0) {}

                ' @ItemCount Input Parameter 
                arParms(0) = New Npgsql.NpgsqlParameter("@OpportID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OpportID

                Delint = CInt(SqlDAL.ExecuteScalar(connString, "usp_DeleteOpportunityStageComments", arParms))

                Return True
            Catch ex As Exception

                Throw ex
            Finally

            End Try

        End Function
        Public Function ReturnOpportStageComments() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim itemCount As Integer

                arParms(0) = New Npgsql.NpgsqlParameter("@OpportID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OpportID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet
                'Call ExecuteReader static method of SqlDal class that returns an Object. Then cast the return value to string.
                ' We pass in database connection string, command type, stored procedure name and productID Npgsql.NpgsqlParameter
                ds = SqlDAL.ExecuteDataset(connString, "usp_GetOpportunityStageComments", arParms)
                'total no of records 
                Return ds 'return dataset
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetOpportunityList() As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintUserRightType", _UserRightType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintSortOrder", _SortOrder, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@dtLastDate", _endDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@OppType", _OppType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@columnName", _columnName, NpgsqlTypes.NpgsqlDbType.VarChar, 50))
                    .Add(SqlDAL.Add_Parameter("@columnSortOrder", _columnSortOrder, NpgsqlTypes.NpgsqlDbType.VarChar, 10))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", _DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitPartner", _bitPartner, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@intType", _intType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcRegularSearchCriteria", _RegularSearchCriteria, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcCustomSearchCriteria", _CustomSearchCriteria, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@SearchText", SearchText, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@SortChar", _SortCharacter, NpgsqlTypes.NpgsqlDbType.Char))
                    .Add(SqlDAL.Add_Parameter("@tintDashboardReminderType", DashboardReminderType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@tintOppStatus", OppStatus, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "usp_GetOpportunityList1", sqlParams.ToArray())

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    _TotalRecords = CCommon.ToInteger(ds.Tables(0).Rows(0)("TotalRecords"))
                Else
                    _TotalRecords = 0
                End If

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPOFulfilmentItems() As DataSet
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(14) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(2).Value = _SortCharacter

                arParms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.InputOutput
                arParms(5).Value = _TotalRecords

                arParms(6) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(6).Value = _columnName

                arParms(7) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(7).Value = _columnSortOrder

                arParms(8) = New Npgsql.NpgsqlParameter("@numRecordID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(8).Value = OpportID

                arParms(9) = New Npgsql.NpgsqlParameter("@FilterBy", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = FilterBy

                arParms(10) = New Npgsql.NpgsqlParameter("@Filter", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(10).Value = _CompFilter

                arParms(11) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Value = ClientTimeZoneOffset

                arParms(12) = New Npgsql.NpgsqlParameter("@numVendorInvoiceBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(12).Value = BizDocId

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                arParms(14) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(14).Value = Nothing
                arParms(14).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetPOItemsForFulfillment", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportunityListPortal() As DataTable
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(18) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID


                arParms(2) = New Npgsql.NpgsqlParameter("@tintUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _UserRightType

                arParms(3) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _SortOrder

                arParms(4) = New Npgsql.NpgsqlParameter("@dtLastDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _endDate

                arParms(5) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(5).Value = _SortCharacter


                arParms(6) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(6).Value = IIf(_FirstName = "", "", Replace(_FirstName, "'", "''"))

                arParms(7) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = IIf(_LastName = "", "", Replace(_LastName, "'", "''"))

                arParms(8) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(8).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(9) = New Npgsql.NpgsqlParameter("@OppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(9).Value = _OppType

                arParms(10) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _CurrentPage

                arParms(11) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Value = _PageSize

                arParms(12) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(12).Direction = ParameterDirection.InputOutput
                arParms(12).Value = _TotalRecords

                arParms(13) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(13).Value = _columnName

                arParms(14) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(14).Value = _columnSortOrder

                arParms(15) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(15).Value = _DivisionID

                arParms(16) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(16).Value = _bitPartner

                arParms(17) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(17).Value = _ClientTimeZoneOffset

                arParms(18) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(18).Value = Nothing
                arParms(18).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "usp_GetOpportunityList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(12).Value)
                Dim dtTable As DataTable = ds.Tables(0)

                Return dtTable
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDealsList1() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(24) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintSalesUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _SalesDealsUserRights

                arParms(3) = New Npgsql.NpgsqlParameter("@tintPurchaseUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _PurchaseDealsUserRights

                arParms(4) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _SortOrder

                arParms(5) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _CurrentPage

                arParms(6) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _PageSize

                arParms(7) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Value = _TotalRecords

                arParms(8) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(8).Value = _columnName

                arParms(9) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(9).Value = _columnSortOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@numCompanyID", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(10).Value = _CompanyID

                arParms(11) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Boolean)
                arParms(11).Value = _bitPartner

                arParms(12) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(12).Value = _ClientTimeZoneOffset

                arParms(13) = New Npgsql.NpgsqlParameter("@tintShipped", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _Shipped

                arParms(14) = New Npgsql.NpgsqlParameter("@intOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(14).Value = _intOrder

                arParms(15) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(15).Value = _intType

                arParms(16) = New Npgsql.NpgsqlParameter("@tintFilterBy", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(16).Value = _FilterBy

                arParms(17) = New Npgsql.NpgsqlParameter("@numOrderStatus", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(17).Value = _OrderStatus

                arParms(18) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(18).Value = _RegularSearchCriteria

                arParms(19) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(19).Value = _CustomSearchCriteria

                arParms(20) = New Npgsql.NpgsqlParameter("@SearchText", NpgsqlTypes.NpgsqlDbType.Varchar)
                arParms(20).Value = SearchText

                arParms(21) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char)
                arParms(21).Value = _SortCharacter

                arParms(22) = New Npgsql.NpgsqlParameter("@tintDashboardReminderType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(22).Value = DashboardReminderType

                arParms(23) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(23).Value = Nothing
                arParms(23).Direction = ParameterDirection.InputOutput

                arParms(24) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(24).Value = Nothing
                arParms(24).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetDealsList1", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    _TotalRecords = CCommon.ToInteger(ds.Tables(0).Rows(0)("TotalRecords"))
                Else
                    _TotalRecords = 0
                End If

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetDealsListExport(ByVal Timeline As String) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numRecOwner", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(1).Value = _CreatedBy

                arParms(2) = New Npgsql.NpgsqlParameter("@numAssignTO", NpgsqlTypes.NpgsqlDbType.BigInt, 18)
                arParms(2).Value = _AssignedTo

                arParms(3) = New Npgsql.NpgsqlParameter("@vcTimeLine", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(3).Value = Timeline

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetDealsList1_export", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetWorkOrderList() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintUserRightType", UserRightType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@CurrentPage", _CurrentPage, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@PageSize", _PageSize, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", _ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@numWOStatus", _Shipped, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@vcRegularSearchCriteria", _RegularSearchCriteria, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@vcCustomSearchCriteria", _CustomSearchCriteria, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@SearchText", SearchText, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetWorkOrder2", sqlParams.ToArray())

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    _TotalRecords = CCommon.ToInteger(ds.Tables(0).Rows(0)("TotalRecords"))
                Else
                    _TotalRecords = 0
                End If

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SalesFulfillment() As DataTable
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(12) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _SortOrder

                arParms(3) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(3).Value = _SortCharacter

                arParms(4) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(4).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(5) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _CurrentPage

                arParms(6) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = _PageSize

                arParms(7) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(7).Direction = ParameterDirection.InputOutput
                arParms(7).Value = _TotalRecords

                arParms(8) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(8).Value = _columnName

                arParms(9) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(9).Value = _columnSortOrder

                arParms(10) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _ClientTimeZoneOffset

                arParms(11) = New Npgsql.NpgsqlParameter("@numOrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(11).Value = _OrderStatus

                arParms(12) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(12).Value = Nothing
                arParms(12).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetSalesFulfillment", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetDealsListPortal() As DataTable
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(19) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID


                arParms(2) = New Npgsql.NpgsqlParameter("@tintSalesUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _SalesDealsUserRights


                arParms(3) = New Npgsql.NpgsqlParameter("@tintPurchaseUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _PurchaseDealsUserRights

                arParms(4) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = _SortOrder

                arParms(5) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(5).Value = _SortCharacter


                arParms(6) = New Npgsql.NpgsqlParameter("@FirstName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(6).Value = IIf(_FirstName = "", "", Replace(_FirstName, "'", "''"))

                arParms(7) = New Npgsql.NpgsqlParameter("@LastName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(7).Value = IIf(_LastName = "", "", Replace(_LastName, "'", "''"))

                arParms(8) = New Npgsql.NpgsqlParameter("@CustName", NpgsqlTypes.NpgsqlDbType.VarChar, 100)
                arParms(8).Value = IIf(_CustName = "", "", Replace(_CustName, "'", "''"))

                arParms(9) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _CurrentPage

                arParms(10) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(10).Value = _PageSize

                arParms(11) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(11).Direction = ParameterDirection.InputOutput
                arParms(11).Value = _TotalRecords

                arParms(12) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(12).Value = _columnName

                arParms(13) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(13).Value = _columnSortOrder

                arParms(14) = New Npgsql.NpgsqlParameter("@numCompanyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _CompanyID

                arParms(15) = New Npgsql.NpgsqlParameter("@bitPartner", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(15).Value = _bitPartner

                arParms(16) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(16).Value = _ClientTimeZoneOffset

                arParms(17) = New Npgsql.NpgsqlParameter("@tintShipped", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(17).Value = _Shipped

                arParms(18) = New Npgsql.NpgsqlParameter("@intOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(18).Value = _intOrder

                arParms(19) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(19).Value = Nothing
                arParms(19).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetDealsList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(11).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Public Function GetOrderList() As DataTable
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(0).Value = _SortCharacter

                arParms(1) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(1).Value = _CurrentPage

                arParms(2) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = _PageSize

                arParms(3) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Direction = ParameterDirection.InputOutput
                arParms(3).Value = _TotalRecords

                arParms(4) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(4).Value = _columnName

                arParms(5) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(5).Value = _columnSortOrder

                arParms(6) = New Npgsql.NpgsqlParameter("@numCompanyID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = _CompanyID

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetOrderList", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(3).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetInvoiceList() As DataTable
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _OppType

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetInvoiceList", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function GetOrderListInEcommerce() As DataSet
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(9) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = _OppType

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@tintShipped", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = Shipped

                arParms(6) = New Npgsql.NpgsqlParameter("@tintOppStatus", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Value = OppStatus

                arParms(7) = New Npgsql.NpgsqlParameter("@UserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(7).Value = UserCntID

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                arParms(9) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(9).Value = Nothing
                arParms(9).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetOrderListInEcommerce", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOrderItemListInEcommerce() As DataSet
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@UserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetOrderItemListInEcommerce", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetCaseListInEcommerce() As DataSet
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(2).Value = CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@UserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@CaseStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = _OrderStatus

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetCaseLisInEcommerce", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetRMAListInEcommerce() As DataSet
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = CurrentPage

                arParms(3) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@UserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = UserCntID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetRMAListInEcommerce", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetPerAnalysysisOppList() As DataTable
            Try


                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(13) {}


                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@OppStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _OppStatus

                arParms(3) = New Npgsql.NpgsqlParameter("@Type", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _RepType

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _TotalRecords

                arParms(7) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _columnName

                arParms(8) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(8).Value = _columnSortOrder

                arParms(9) = New Npgsql.NpgsqlParameter("@numStartDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(9).Value = _startDate

                arParms(10) = New Npgsql.NpgsqlParameter("@numEndDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(10).Value = _endDate

                arParms(11) = New Npgsql.NpgsqlParameter("@strUserIDs", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(11).Value = _strUserIDs

                arParms(12) = New Npgsql.NpgsqlParameter("@TeamType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(12).Value = _TeamType

                arParms(13) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(13).Value = Nothing
                arParms(13).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetOpportunityListPerAnyls", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOpportuntityBestAccountDealClosedReport() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numStatus", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _OppStatus

                arParms(3) = New Npgsql.NpgsqlParameter("@dtDateFrom", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(3).Value = _startDate

                arParms(4) = New Npgsql.NpgsqlParameter("@dtDateTo", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _endDate

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet

                ds = SqlDAL.ExecuteDataset(connString, "USP_BestAccountClosedReport", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ListCustomerAuto() As DataSet
            Try
                Dim Connection As New Npgsql.NpgSqlConnection(GetConnection.GetConnectionString)
                Dim arParams() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParams(0) = New Npgsql.NpgsqlParameter("@vcCompanyname", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParams(0).Value = _CompFilter

                arParams(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParams(1).Value = Nothing
                arParams(1).Direction = ParameterDirection.InputOutput

                ListCustomerAuto = SqlDAL.ExecuteDataset(Connection, "usp_GetContactInfoAutoPost", arParams)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetReceivePaymentDetailsForOpportunity() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetReceivePaymentDetailsForOpportunity", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        '' Created on 25 Sep 2008 for fetch Address against Division ID from DivisionMaster Table.
        Public Function GetExistingAddress(ByVal iUserId As Integer, ByVal iDivisionId As Integer) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = iUserId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = iDivisionId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetExistingAddress", arParms)
                Return ds.Tables(0)
                'Return TryCast(arParms(3).Value.ToString(), String)
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        '' End of the Method

        '' Created on 28 Sep 2008 for fetch Address against Division ID from AdditionalContactsInformation Table.
        Public Function GetContactDetails(ByVal iUserId As Integer, ByVal iDivisionId As Integer) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = iDivisionId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetContactDetails", arParms)
                If Not IsNothing(ds) Then
                    Return ds.Tables(0)
                Else : Return New DataTable()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        '' End of the Method

        '' Created on 29 Sep 2008 for Save the existing address in DivisionMaster Table.
        Public Function SaveExistingAddress(ByVal iNewUserId As Integer, ByVal iOldUserId As Integer, ByVal iNewDivisionId As Integer, ByVal iOldDivisionId As Integer, ByVal sAddressType As String, ByVal sPrimaryContactCheck As String) As Boolean
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numNewDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOldDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numNewUserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = iNewUserId

                arParms(3) = New Npgsql.NpgsqlParameter("@numOldUserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = iOldUserId

                arParms(4) = New Npgsql.NpgsqlParameter("@numNewDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = iNewDivisionId

                arParms(5) = New Npgsql.NpgsqlParameter("@numOldDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = iOldDivisionId

                arParms(6) = New Npgsql.NpgsqlParameter("@vcAddressType", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = sAddressType

                arParms(7) = New Npgsql.NpgsqlParameter("@numPrimaryContactCheck", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = sPrimaryContactCheck

                SqlDAL.ExecuteNonQuery(connString, "USP_SaveExistingAddress", arParms)
                Return True

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        '' End of the Method

        '' Created on 30 Sep 2008 for fetch Address against Division ID from DivisionMaster Table.
        Public Function GetBizDocExistingAddress(ByVal iUserId As Integer, ByVal iDivisionId As Integer, ByVal sAddressType As String, ByVal sPrimaryContactCheck As String) As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = iUserId

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = iDivisionId

                arParms(3) = New Npgsql.NpgsqlParameter("@vcAddressType", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(3).Value = sAddressType

                arParms(4) = New Npgsql.NpgsqlParameter("@numPrimaryContactCheck", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(4).Value = sPrimaryContactCheck

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_BizDocExistingAddress", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        '' End of the Method

        Public Function GetAuthoritativeOpportunityCount() As Long
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetAuthoritativeOpportunityCount", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetFirstAuthoritativeBizDocId() As Long
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_GetFirstAuthoritativeBizDocID", arParms))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSerializedOppItems() As DataTable
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetOppSerializedItems", arParms).Tables(0)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSerializedIndOppItems_BizDoc() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OppItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _OppType

                arParms(3) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _BizDocId

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur4", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "usp_GetOppserializedinditems_BizDoc", arParms)

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function UpdatePOItemsFulfillment(ByVal objTransaction As NpgsqlTransaction) As String
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString


                arParms(0) = New Npgsql.NpgsqlParameter("@numQtyReceived", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(0).Value = _UnitHour

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OppItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@vcError", NpgsqlTypes.NpgsqlDbType.VarChar, 200)
                arParms(2).Direction = ParameterDirection.InputOutput
                arParms(2).Value = ""

                arParms(3) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = UserCntID

                arParms(4) = New Npgsql.NpgsqlParameter("@dtItemReceivedDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParms(4).Value = _dtItemReceivedDate

                arParms(5) = New Npgsql.NpgsqlParameter("@numSelectedWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = WarehouseItmsID

                arParms(6) = New Npgsql.NpgsqlParameter("@numVendorInvoiceBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = BizDocId

                arParms(7) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = byteMode

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()

                If Not objTransaction Is Nothing Then
                    SqlDAL.ExecuteDataset(objTransaction, "USP_UpdatePOItemsForFulfillment", objParam)
                Else
                    SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_UpdatePOItemsForFulfillment", objParam, True)
                End If

                Return CCommon.ToString(DirectCast(objParam, Npgsql.NpgsqlParameter())(2).Value)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetWarehouseSF() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetWarehouseForSF", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function



        Public Function ManageWarehouseSF() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@strWarehouse", NpgsqlTypes.NpgsqlDbType.VarChar, 4000)
                arParms(1).Value = _strItemDtls

                SqlDAL.ExecuteNonQuery(connString, "USP_ManagePickWareHouse", arParms)

                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetSFItemsforImport() As DataSet
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _BizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@vcBizDocsIds", NpgsqlTypes.NpgsqlDbType.VarChar, 8000)
                arParms(2).Value = _strBizDocsIds

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GEtSFItemsForImporting", arParms)

            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function
        Public Function GetSalesTemplate() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(7) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesTemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _OpportID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = _byteMode

                arParms(4) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _DivisionID

                arParms(5) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = ClientTimeZoneOffset

                arParms(6) = New Npgsql.NpgsqlParameter("@numSiteID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = SiteID

                arParms(7) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(7).Value = Nothing
                arParms(7).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetOpportunitySalesTemplate", arParms)
                If Not IsNothing(ds) Then
                    Return ds.Tables(0)
                Else : Return New DataTable()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Public Function ManageSalesTemplate() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Direction = ParameterDirection.InputOutput
                arParms(0).Value = _SalesTemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcTemplateName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(1).Value = _TemplateName

                arParms(2) = New Npgsql.NpgsqlParameter("@tintType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _TemplateType

                arParms(3) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = _OpportID

                arParms(4) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = _DivisionID

                arParms(5) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = DomainID

                arParms(6) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(6).Value = UserCntID

                arParms(7) = New Npgsql.NpgsqlParameter("@tintAppliesTo", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(7).Value = AppliesTo

                arParms(8) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(8).Value = Nothing
                arParms(8).Direction = ParameterDirection.InputOutput

                Me.SalesTemplateID = CCommon.ToLong(SqlDAL.ExecuteScalar(connString, "USP_ManageOpportunitySalesTemplate", arParms))

                If SalesTemplateID > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteSalesTemplate() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesTemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteOpportunitySalesTemplate", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageSalesTemplateItems() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesTemplateItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesTemplateItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSalesTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SalesTemplateID

                arParms(2) = New Npgsql.NpgsqlParameter("@strItems", NpgsqlTypes.NpgsqlDbType.Text)
                arParms(2).Value = _strItemDtls

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return CBool(SqlDAL.ExecuteNonQuery(connString, "USP_ManageSalesTemplateItems", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function ManageSalesSpecificGenericTemplateItems() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(8) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesTemplateItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesTemplateItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@numSalesTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _SalesTemplateID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DomainID

                arParms(3) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = ItemCode

                arParms(4) = New Npgsql.NpgsqlParameter("@numWarehouseItmsID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = WarehouseItmsID

                arParms(5) = New Npgsql.NpgsqlParameter("@numUOM", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(5).Value = numUOM

                arParms(6) = New Npgsql.NpgsqlParameter("@vcUOMName", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(6).Value = vcUOMName

                arParms(7) = New Npgsql.NpgsqlParameter("@Attributes", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(7).Value = vcAttributes

                arParms(8) = New Npgsql.NpgsqlParameter("@AttrValues", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(8).Value = vcAttrValues

                Return CBool(SqlDAL.ExecuteNonQuery(connString, "USP_ManageSalesSpecificGenericTemplateItems", arParms))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function DeleteSalesTemplateItem() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesTemplateItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesTemplateItemID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                SqlDAL.ExecuteNonQuery(connString, "USP_DeleteSalesTemplateItems", arParms)
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSalesTemplateItems() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesTemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = DivisionID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetSalesTemplateItems", arParms)
                If Not IsNothing(ds) Then
                    Return ds.Tables(0)
                Else : Return New DataTable()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSalesTemplateItemDetail() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = SalesTemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetSalesTemplateItemDtl", arParms)
                If Not IsNothing(ds) Then
                    Return ds.Tables(0)
                Else : Return New DataTable()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSalesTemplateItemsByItemId() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesTemplateID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesTemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetSalesTemplateItemsByItemId", arParms)
                If Not IsNothing(ds) Then
                    Return ds.Tables(0)
                Else : Return New DataTable()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSalesOrderTemplateItems() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numSalesOrderId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _SalesTemplateID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetSalesOrderItems", arParms)
                If Not IsNothing(ds) Then
                    Return ds.Tables(0)
                Else : Return New DataTable()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' <summary>
        ''' 'this function takes 4-5 minits to complete .. so not using existing dataaccess layer which times out
        ''' insted its being called directly from windows service. later on we can incorporate way in
        ''' </summary>
        ''' <remarks></remarks>
        <Obsolete()>
        Public Sub UpdateDealAmount()
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                SqlDAL.ExecuteNonQuery(connString, "USP_UpdateDealAmount", Nothing)


            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetTopOrders() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _DivisionID

                arParms(1) = New Npgsql.NpgsqlParameter("@numContactID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ContactID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _OppType

                arParms(3) = New Npgsql.NpgsqlParameter("@TopCount", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = _PageSize

                arParms(4) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(4).Value = DomainID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetTopOrders", arParms)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetAutoAssign_OppSerializedItem() As DataTable
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = _OpportID

                arParms(1) = New Npgsql.NpgsqlParameter("@numBizDocID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _BizDocId

                arParms(2) = New Npgsql.NpgsqlParameter("@numWarehouseItmsID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _WarehouseItmsID

                arParms(3) = New Npgsql.NpgsqlParameter("@bitSerialized", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = _bitSerialized

                arParms(4) = New Npgsql.NpgsqlParameter("@bitLotNo", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = _bitLotNo

                arParms(5) = New Npgsql.NpgsqlParameter("@numUnitHour", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = _UnitHour

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_GetAutoAssign_OppSerializedItem", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

        Public Function GetDealsListForPortal() As DataSet
            Try

                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(18) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = UserCntID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintSortOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _SortOrder

                arParms(3) = New Npgsql.NpgsqlParameter("@SortChar", NpgsqlTypes.NpgsqlDbType.Char, 1)
                arParms(3).Value = _SortCharacter

                arParms(4) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = _CurrentPage

                arParms(5) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Value = _PageSize

                arParms(6) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(6).Direction = ParameterDirection.InputOutput
                arParms(6).Value = _TotalRecords

                arParms(7) = New Npgsql.NpgsqlParameter("@columnName", NpgsqlTypes.NpgsqlDbType.VarChar, 50)
                arParms(7).Value = _columnName

                arParms(8) = New Npgsql.NpgsqlParameter("@columnSortOrder", NpgsqlTypes.NpgsqlDbType.VarChar, 10)
                arParms(8).Value = _columnSortOrder

                arParms(9) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(9).Value = _ClientTimeZoneOffset

                arParms(10) = New Npgsql.NpgsqlParameter("@tintShipped", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(10).Value = _Shipped

                arParms(11) = New Npgsql.NpgsqlParameter("@intOrder", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(11).Value = _intOrder

                arParms(12) = New Npgsql.NpgsqlParameter("@intType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(12).Value = _intType

                arParms(13) = New Npgsql.NpgsqlParameter("@tintFilterBy", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(13).Value = _FilterBy

                arParms(14) = New Npgsql.NpgsqlParameter("@numOrderStatus", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(14).Value = _OrderStatus

                arParms(15) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(15).Value = _RegularSearchCriteria

                arParms(16) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(16).Value = _CustomSearchCriteria

                arParms(17) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(17).Value = _DivisionID

                arParms(18) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(18).Value = Nothing
                arParms(18).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_GetDealsListForPortal", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(6).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetOppSerialLotNumber() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OpportID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(2).Value = _WarehouseItmsID

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetOppSerialLotNumber", arParms)

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub SaveOppSerialLotNumber(ByVal vcSelectedItems As String, ByVal tintItemType As Int16)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(0).Value = _OpportID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt, 9)
                arParms(1).Value = _OppItemCode

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSelectedItems", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = vcSelectedItems

                arParms(3) = New Npgsql.NpgsqlParameter("@tintItemType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(3).Value = tintItemType

                SqlDAL.ExecuteNonQuery(connString, "USP_OppWarehouseSerializedItem_Insert", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
        ''' <summary>
        ''' Returns data when there are more than 1 PO of same vendor are available 
        ''' without any authoritive bizdoc and no received qty for any order item
        ''' </summary>
        ''' <param name="level">0-Vendor, 1-Order of Vendor, 2- Item of Purchase Order</param>
        ''' <param name="vendorID">only passed when level 1 otherwise 0</param>
        ''' <param name="poID">only passed when level 2 otherwise 0</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function GetPOAvailableForMerge(ByVal level As Int16, ByVal vendorID As Long, ByVal poID As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numLevel", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = level

                arParms(2) = New Npgsql.NpgsqlParameter("@numVendorID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = vendorID

                arParms(3) = New Npgsql.NpgsqlParameter("@numPOID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = poID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityMaster_GetPOAvailableForMerge", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function
        ''' <summary>
        ''' Sets the bitStopMerge flag of selected purchase orders as true in OpportunityMaster table
        ''' so marked purchase orders will not available for merger
        ''' </summary>
        ''' <param name="vcSelectedPO">comma seperated string of purchase order ids(numOppId)</param>
        ''' <remarks></remarks>
        Public Sub MarkAsDoNotMerge(ByVal vcSelectedPO As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numLevel", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = vcSelectedPO

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_SetDoNotMerge", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
        ''' <summary>
        ''' Merge selected purchase orders of same vendor to one selected master purchase order
        ''' </summary>
        ''' <param name="strSelectedPOs">string of xml</param>
        ''' <remarks></remarks>
        Public Sub MergeVendorPurchaseOrder(ByVal strSelectedPOs As String)
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = UserCntID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcXML", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = strSelectedPOs

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_MergeVendorPO", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Sub
        ''' <summary>
        ''' Get Item List no yet invoiced
        ''' </summary>
        ''' <param name="oppid">Long Opp ID</param>
        ''' <remarks></remarks>
        Public Function ViewItemNotYetInvoiced(ByVal OppId As Long, ByVal DomainId As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numoppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "usp_getNotInvoicedItems", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        ''' <summary>
        ''' Get Approver List Approval Unit Item
        ''' </summary>
        ''' <param name="oppid">Long Opp ID</param>
        ''' <remarks></remarks>
        Public Function ViewApproverItemUnitPrice(ByVal OppId As Long, ByVal DomainId As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numoppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "usp_getApproverItemUnitPrice", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ViewApproverPromotionApproval(ByVal OppId As Long, ByVal DomainId As Long) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainId

                arParms(1) = New Npgsql.NpgsqlParameter("@numoppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OppId

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "usp_getApproverPromotionApproval", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        ''' <summary>
        ''' Returns list of items needs to be picked from different warehouse and location 
        ''' </summary>
        ''' <returns>DataSet</returns>
        ''' <remarks></remarks>
        Public Function GetPackingDetail() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcBizDocsIds", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(1).Value = _strBizDocsIds

                arParms(2) = New Npgsql.NpgsqlParameter("@tintMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = _byteMode

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDataset(connString, "USP_GetPackingDetail", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function
        ''' <summary>
        ''' Checks whether provided serial/lot# available in warehouse
        ''' </summary>
        ''' <param name="vcSerialLot">Serial/Lot#</param>
        ''' <returns>true or false based on verification</returns>
        ''' <remarks></remarks>
        Public Function VerifySerialLot(ByVal vcSerialLot As String) As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = OppItemCode

                arParms(1) = New Npgsql.NpgsqlParameter("@numWarehouseItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = WarehouseItmsID

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSerialLot", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = vcSerialLot

                arParms(3) = New Npgsql.NpgsqlParameter("@bitLotNo", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = bitLotNo

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_VerifySerialLot", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetOrderItemDetails(ByVal numUOMID As Long, ByVal vcSelectedKitChildItems As String, ByVal numShippingCountry As Long) As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintOppType", OppType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numItemCode", ItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUOMID", numUOMID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseItemID", WarehouseItmsID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQty", UnitHour, NpgsqlTypes.NpgsqlDbType.Double))
                    .Add(SqlDAL.Add_Parameter("@monPrice", Price, NpgsqlTypes.NpgsqlDbType.Double))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", OppItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSelectedKitChildItems", vcSelectedKitChildItems, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseID", WarehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcCoupon", vcCoupon, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numCurrencyID", CurrencyID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_GetOrderSelectedItemDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetItemPriceBasedOnPricingOption(ByVal tintPriceMethod As Short, ByVal listPrice As Decimal, ByVal vendorCost As Decimal, ByVal uomConversionFactor As Double) As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(10) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(1).Value = DivisionID

                arParms(2) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(2).Value = OppType

                arParms(3) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(3).Value = ItemCode

                arParms(4) = New Npgsql.NpgsqlParameter("@tintPriceMethod", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(4).Value = tintPriceMethod

                arParms(5) = New Npgsql.NpgsqlParameter("@monListPrice", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(5).Value = listPrice

                arParms(6) = New Npgsql.NpgsqlParameter("@monVendorCost", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(6).Value = vendorCost

                arParms(7) = New Npgsql.NpgsqlParameter("@numQty", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(7).Value = UnitHour

                arParms(8) = New Npgsql.NpgsqlParameter("@fltUOMConversionFactor", NpgsqlTypes.NpgsqlDbType.Double)
                arParms(8).Value = uomConversionFactor

                arParms(9) = New Npgsql.NpgsqlParameter("@numCurrencyID", NpgsqlTypes.NpgsqlDbType.Bigint)
                arParms(9).Value = CurrencyID

                arParms(10) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(10).Value = Nothing
                arParms(10).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_GetItemPriceBasedOnMethod", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetItemPriceHistory() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(5) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = OppType

                arParms(2) = New Npgsql.NpgsqlParameter("@numItemCode", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = ItemCode

                arParms(3) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = ClientTimeZoneOffset

                arParms(4) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = DivisionID

                arParms(5) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(5).Value = Nothing
                arParms(5).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_Item_GetPriceHistory", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ValidateContainerQty() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OpportID

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_OpportunityMaster_ValidateContainerQty", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ValidateAllQtyAddedToBizDocType() As Boolean
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(3) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@numOppId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = OpportID

                arParms(2) = New Npgsql.NpgsqlParameter("@numBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = BizDocId

                arParms(3) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(3).Value = Nothing
                arParms(3).Direction = ParameterDirection.InputOutput

                Return CCommon.ToBool(SqlDAL.ExecuteScalar(connString, "USP_OpportunityMaster_QtyAddedToBizDocType", arParms))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetPendingFulfillmentBizDocs() As DataTable
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}

                arParms(0) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = OpportID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityBizDocs_GetPendingFulfillmentBizDocs", arParms)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub CreateFulfillmentBizDoc(DomainID As Long, UserCntID As Long, lngOppId As Long)
            Try
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0

                Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    Dim dtDetails, dtBillingTerms As DataTable

                    Dim objPageLayout As New BACRM.BusinessLogic.Contacts.CPageLayout
                    objPageLayout.OpportunityId = lngOppId
                    objPageLayout.DomainID = DomainID
                    dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                    lngDivId = CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID"))
                    lngCntID = CCommon.ToLong(dtDetails.Rows(0).Item("numContactID"))


                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.UserCntID = UserCntID
                    objOppBizDocs.DomainID = DomainID

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", DomainID, lngDivId) = 0 Then
                        Throw New Exception("Not able to fulfillment bizdoc. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                        Exit Sub
                    End If

                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.OppType = 1
                    objOppBizDocs.FromDate = DateTime.UtcNow
                    objOppBizDocs.ClientTimeZoneOffset = 0


                    'SETS BIZDOC ID
                    '--------------
                    objOppBizDocs.BizDocId = 296 'Fulfillment Bizdoc
                    '--------------

                    'SETS BIZDOC TEMPLETE NEEDS TO BE USED
                    '-------------------------------------
                    objOppBizDocs.byteMode = 1
                    Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                    objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))
                    '-------------------------------------

                    Dim objCommon As New CCommon
                    objCommon.DomainID = DomainID
                    objCommon.Mode = 33
                    objCommon.Str = CCommon.ToString(objOppBizDocs.BizDocId) 'DEFUALT BIZ DOC ID
                    objOppBizDocs.SequenceId = CCommon.ToString(objCommon.GetSingleFieldValue())

                    objCommon = New CCommon
                    objCommon.DomainID = DomainID
                    objCommon.Mode = 34
                    objCommon.Str = CCommon.ToString(lngOppId)
                    objOppBizDocs.RefOrderNo = CCommon.ToString(objCommon.GetSingleFieldValue())
                    objOppBizDocs.bitPartialShipment = True

                    OppBizDocID = objOppBizDocs.SaveBizDoc()

                    If OppBizDocID = 0 Then
                        Throw New Exception("Not able to create fulfillment bizdoc. A BizDoc by the same name is already created. Please select another BizDoc !")
                        Exit Sub
                    End If

                    'MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = DomainID
                    objOppBizDoc.UserCntID = UserCntID
                    objOppBizDoc.OppId = lngOppId
                    objOppBizDoc.OppBizDocId = OppBizDocID
                    objOppBizDoc.AutoFulfillBizDoc()


                    'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                    '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                    'CREATE SALES CLEARING ENTERIES
                    'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                    '-------------------------------------------------
                    Dim ds As New DataSet
                    Dim dtOppBiDocItems As DataTable

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                    dtOppBiDocItems = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, DomainID, dtOppBiDocItems)

                    JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs

                    'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                    Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                        objJournalEntries.SaveJournalEnteriesSalesClearing(lngOppId, DomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                    Else
                        objJournalEntries.SaveJournalEnteriesSalesClearingNew(lngOppId, DomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                    End If

                    '---------------------------------------------------------------------------------

                    objTransaction.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow.Workflow()
                objWfA.DomainID = DomainID
                objWfA.UserCntID = UserCntID
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code

                If OppBizDocID > 0 Then
                    Try
                        Dim objOppBizDoc As New OppBizDocs
                        objOppBizDoc.CreateInvoiceFromFulfillmentOrder(DomainID, UserCntID, lngOppId, OppBizDocID, 0)
                    Catch ex As Exception
                        'DO NOT THROW ERROR
                    End Try
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub CreateFulfillmentBizDocWithItems(DomainID As Long, UserCntID As Long, lngOppId As Long, vcItems As String, parentBizDocID As Integer)
            Try
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0

                Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    Dim dtDetails, dtBillingTerms As DataTable

                    Dim objPageLayout As New BACRM.BusinessLogic.Contacts.CPageLayout
                    objPageLayout.OpportunityId = lngOppId
                    objPageLayout.DomainID = DomainID
                    dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                    lngDivId = CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID"))
                    lngCntID = CCommon.ToLong(dtDetails.Rows(0).Item("numContactID"))


                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.UserCntID = UserCntID
                    objOppBizDocs.DomainID = DomainID

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", DomainID, lngDivId) = 0 Then
                        Throw New Exception("Not able to fulfillment bizdoc. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                        Exit Sub
                    End If

                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.OppType = 1
                    objOppBizDocs.FromDate = DateTime.UtcNow
                    objOppBizDocs.ClientTimeZoneOffset = 0
                    objOppBizDocs.numSourceBizDocId = parentBizDocID


                    'SETS BIZDOC ID
                    '--------------
                    objOppBizDocs.BizDocId = 296 'Fulfillment Bizdoc
                    '--------------

                    'SETS BIZDOC TEMPLETE NEEDS TO BE USED
                    '-------------------------------------
                    objOppBizDocs.byteMode = 1
                    Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                    objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))
                    '-------------------------------------

                    Dim objCommon As New CCommon
                    objCommon.DomainID = DomainID
                    objCommon.Mode = 33
                    objCommon.Str = CCommon.ToString(objOppBizDocs.BizDocId) 'DEFUALT BIZ DOC ID
                    objOppBizDocs.SequenceId = CCommon.ToString(objCommon.GetSingleFieldValue())

                    objCommon = New CCommon
                    objCommon.DomainID = DomainID
                    objCommon.Mode = 34
                    objCommon.Str = CCommon.ToString(lngOppId)
                    objOppBizDocs.RefOrderNo = CCommon.ToString(objCommon.GetSingleFieldValue())
                    objOppBizDocs.bitPartialShipment = True
                    objOppBizDocs.strBizDocItems = vcItems
                    OppBizDocID = objOppBizDocs.SaveBizDoc()

                    If OppBizDocID = 0 Then
                        Throw New Exception("Not able to create fulfillment bizdoc. A BizDoc by the same name is already created. Please select another BizDoc !")
                        Exit Sub
                    End If

                    'MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = DomainID
                    objOppBizDoc.UserCntID = UserCntID
                    objOppBizDoc.OppId = lngOppId
                    objOppBizDoc.OppBizDocId = OppBizDocID
                    objOppBizDoc.AutoFulfillBizDoc()


                    'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                    '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                    'CREATE SALES CLEARING ENTERIES
                    'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                    '-------------------------------------------------
                    Dim ds As New DataSet
                    Dim dtOppBiDocItems As DataTable

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                    dtOppBiDocItems = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, DomainID, dtOppBiDocItems)

                    JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs

                    'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                    Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                        objJournalEntries.SaveJournalEnteriesSalesClearing(lngOppId, DomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                    Else
                        objJournalEntries.SaveJournalEnteriesSalesClearingNew(lngOppId, DomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                    End If

                    '---------------------------------------------------------------------------------

                    If parentBizDocID > 0 Then
                        'IF fulfillment order qty is less than pack qty than change packing slip quantity to match fulfillment order qty
                        objOppBizDocs.UserCntID = UserCntID
                        objOppBizDocs.DomainID = DomainID
                        objOppBizDocs.OppId = lngOppId
                        objOppBizDocs.OppBizDocId = parentBizDocID
                        objOppBizDocs.ChangePackingSlipQty()
                    End If

                    objTransaction.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow.Workflow()
                objWfA.DomainID = DomainID
                objWfA.UserCntID = UserCntID
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code

                If OppBizDocID > 0 Then
                    Try
                        Dim objOppBizDoc As New OppBizDocs
                        objOppBizDoc.CreateInvoiceFromFulfillmentOrder(DomainID, UserCntID, lngOppId, OppBizDocID, 0)
                    Catch ex As Exception
                        'DO NOT THROW ERROR
                    End Try
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub AutoFulfillOrder(ByVal domainID As Long, ByVal userCntID As Long, ByVal lngOppId As Long, ByVal OppBizDocID As Long)
            Try
                Using objTransaction As New Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    Dim objPageLayout As New BACRM.BusinessLogic.Contacts.CPageLayout
                    objPageLayout.OpportunityId = lngOppId
                    objPageLayout.DomainID = domainID
                    Dim dtDetails As DataTable = objPageLayout.OpportunityDetails.Tables(0)
                    Dim lngDivId As Long = CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID"))

                    'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = domainID
                    objOppBizDoc.UserCntID = userCntID
                    objOppBizDoc.OppId = lngOppId
                    objOppBizDoc.OppBizDocId = OppBizDocID
                    objOppBizDoc.AutoFulfillBizDoc()


                    'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                    '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                    'CREATE SALES CLEARING ENTERIES
                    'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                    '-------------------------------------------------
                    Dim ds As New DataSet
                    Dim dtOppBiDocItems As DataTable

                    objOppBizDoc.OppBizDocId = OppBizDocID
                    ds = objOppBizDoc.GetOppInItemsForAuthorizativeAccounting
                    dtOppBiDocItems = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, domainID, dtOppBiDocItems)

                    Dim JournalId As Long = objOppBizDoc.GetJournalIdForAuthorizativeBizDocs

                    'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                    JournalId = objOppBizDoc.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                    Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                    If objOppBizDoc.IsSalesClearingAccountEntryExists() Then
                        objJournalEntries.SaveJournalEnteriesSalesClearing(lngOppId, domainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                    Else
                        objJournalEntries.SaveJournalEnteriesSalesClearingNew(lngOppId, domainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                    End If
                    '---------------------------------------------------------------------------------

                    objTransaction.Complete()
                End Using
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub CreateOppBizDoc(ByVal numDomainID As Long, ByVal numUserCntID As Long, ByVal lngDivId As Long, ByVal lngOppId As Long, ByVal lngBizDocTemplateID As Long)
            Try
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.OppType = 1
                objOppBizDocs.BizDocTemplateID = lngBizDocTemplateID
                objOppBizDocs.UserCntID = numUserCntID
                objOppBizDocs.DomainID = numDomainID
                objOppBizDocs.vcPONo = "" 'txtPO.Text
                objOppBizDocs.vcComments = "" 'txtComments.Text


                Dim objAdmin As New BACRM.BusinessLogic.Admin.CAdmin
                Dim dtBillingTerms As DataTable
                objAdmin.DivisionID = lngDivId
                dtBillingTerms = objAdmin.GetBillingTerms()

                Dim lngBizDoc As Long = objOppBizDocs.GetAuthorizativeOpportuntiy()

                If lngBizDoc = 0 Then Exit Sub

                objOppBizDocs.BizDocId = lngBizDoc

                Dim objCommon As New CCommon
                objCommon.DomainID = numDomainID
                objCommon.Mode = 33
                objCommon.Str = CStr(lngBizDoc)
                objOppBizDocs.SequenceId = CStr(objCommon.GetSingleFieldValue())

                objOppBizDocs.bitPartialShipment = True
                Dim OppBizDocID As Long

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    OppBizDocID = objOppBizDocs.SaveBizDoc()
                    CreateJournalEntry(objOppBizDocs, OppBizDocID, lngBizDoc, numDomainID, numUserCntID, lngDivId, lngOppId)

                    objTransactionScope.Complete()
                End Using
                ''Added By Sachin Sadhu||Date:1stMay12014
                ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                objWfA.DomainID = numDomainID
                objWfA.UserCntID = numUserCntID
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub CreateJournalEntry(ByVal objOppBizDocs As OppBizDocs, ByVal OppBizDocID As Long, ByVal lngBizDoc As Long, ByVal numDOmainID As Long, ByVal numUserCntID As Long, ByVal lngDivID As Long, ByVal lngOppId As Long)
            Try
                'Create Journals only for authoritative bizdocs
                '-------------------------------------------------
                Dim lintAuthorizativeBizDocsId As Long
                objOppBizDocs.DomainID = numDOmainID
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.UserCntID = numUserCntID
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                If lintAuthorizativeBizDocsId = lngBizDoc Then
                    Dim ds As New DataSet
                    objOppBizDocs.OppBizDocId = OppBizDocID
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                    Dim dtOppBiDocItems As DataTable = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount

                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, numDOmainID, dtOppBiDocItems)

                    ''---------------------------------------------------------------------------------
                    Dim objJEHeader As New BACRM.BusinessLogic.Accounting.JournalEntryHeader
                    With objJEHeader
                        .JournalId = 0
                        .RecurringId = 0
                        .EntryDate = CDate(Date.UtcNow.Date & " 12:00:00")
                        .Description = ""
                        .Amount = objCalculateDealAmount.GrandTotal
                        .CheckId = 0
                        .CashCreditCardId = 0
                        .ChartAcntId = 0
                        .OppId = lngOppId
                        .OppBizDocsId = OppBizDocID
                        .DepositId = 0
                        .BizDocsPaymentDetId = 0
                        .IsOpeningBalance = False
                        .LastRecurringDate = Date.Now
                        .NoTransactions = 0
                        .CategoryHDRID = 0
                        .ReturnID = 0
                        .CheckHeaderID = 0
                        .BillID = 0
                        .BillPaymentID = 0
                        .UserCntID = numUserCntID
                        .DomainID = numDOmainID
                    End With
                    Dim JournalId As Long = objJEHeader.Save()

                    Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry

                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                        objJournalEntries.SaveJournalEntriesSales(lngOppId, numDOmainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivID, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), 0)
                    Else
                        objJournalEntries.SaveJournalEntriesSalesNew(lngOppId, numDOmainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivID, CCommon.ToLong(ds.Tables(1).Rows(0).Item("numCurrencyID")), CCommon.ToDouble(ds.Tables(1).Rows(0).Item("fltExchangeRate")), 0)
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetSODropshipItems() As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@v_SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@v_SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@v_SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_GetSODropshipItems", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetSOBackOrderItems() As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_GetSOBackOrderItems", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetWorkOrderBackOrderItemDetail() As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItems", strItemDtls, NpgsqlTypes.NpgsqlDbType.Varchar))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_WorkOrder_GetBackOrderItemsDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetPurchaseOrderForFulfillment() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(6) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@tintSearchType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(1).Value = byteMode

                arParms(2) = New Npgsql.NpgsqlParameter("@vcSearchText", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(2).Value = SearchText

                arParms(3) = New Npgsql.NpgsqlParameter("@intOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(3).Value = CurrentPage

                arParms(4) = New Npgsql.NpgsqlParameter("@intPageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(4).Value = PageSize

                arParms(5) = New Npgsql.NpgsqlParameter("@intTotalRecords", NpgsqlTypes.NpgsqlDbType.Integer)
                arParms(5).Direction = ParameterDirection.Output

                arParms(6) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(6).Value = Nothing
                arParms(6).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParms.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "USP_OpportunityMaster_GetPurchaseOrderForFulfillment", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub UnreceiveItems(ByVal qtyToUnreceive As Double)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", OppItemCode, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@tintMode", byteMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@numUnitHourToUnreceive", qtyToUnreceive, NpgsqlTypes.NpgsqlDbType.Numeric))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityItems_UnReceiveQty", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetPromotiondRuleForOrder(ByVal monTotal As Double, Optional ByVal siteID As Long = 0) As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSubTotal", monTotal, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", siteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numPromotionID", PromotionID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetPromotiondRuleForOrder", sqlParams.ToArray())
                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function CheckCodeForPromotion(ByVal numProID As Long, ByVal txtCouponCode As String) As Long
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numProdId", numProID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@txtCouponCode", txtCouponCode, NpgsqlTypes.NpgsqlDbType.VarChar, 100))
                    .Add(SqlDAL.Add_Parameter("@numDivisionId", DivisionID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return CLng(SqlDAL.ExecuteScalar(connString, "USP_CheckCodeForPromotion", sqlParams.ToArray()))

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetShippingRuleDetails(ZipCode As String, Stateid As Long, numCountryId As Long, Optional siteID As Long = 0) As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWareHouseId", CCommon.ToString(WarehouseID), NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@ZipCode", ZipCode, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@StateId", Stateid, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numDivisionID", DivisionID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numSiteID", siteID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numCountryId", numCountryId, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With


                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetShippingRuleDetails", sqlParams.ToArray())

                Return ds
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetShippingExceptionsForOrder(vcItemCodes As String, ShippingAmt As Double, bitFreeShipping As Boolean) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcItemCodes", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParms(1).Value = vcItemCodes

                arParms(2) = New Npgsql.NpgsqlParameter("@ShippingAmt", NpgsqlTypes.NpgsqlDbType.Numeric)
                arParms(2).Value = ShippingAmt

                arParms(3) = New Npgsql.NpgsqlParameter("@bitFreeShipping", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = bitFreeShipping

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetShippingExceptionsForOrder", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetStateIDForZip(vcStateName As String) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(2) {}
                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@vcState", NpgsqlTypes.NpgsqlDbType.VarChar, 300)
                arParms(1).Value = vcStateName

                arParms(2) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(2).Value = Nothing
                arParms(2).Direction = ParameterDirection.InputOutput

                Dim ds As DataSet = SqlDAL.ExecuteDataset(connString, "USP_GetStateForChangeZip", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetOrderDetailForBizDoc() As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityMaster_GetOrderDetailForBizDoc", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function GetShippingDetail(ByVal ShippingReportID As Long) As DataTable
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainId", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numShippingReportID", ShippingReportID, NpgsqlTypes.NpgsqlDbType.Bigint))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDatable(connString, "USP_OpportunityMaster_GetShippingDetail", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub PutAway(ByVal vcWarehouses As String)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OpportID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppItemID", OppItemCode, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numQtyReceived", UnitHour, NpgsqlTypes.NpgsqlDbType.Numeric))
                    .Add(SqlDAL.Add_Parameter("@dtItemReceivedDate", dtItemReceivedDate, NpgsqlTypes.NpgsqlDbType.Timestamp))
                    .Add(SqlDAL.Add_Parameter("@vcWarehouses", vcWarehouses, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@numVendorInvoiceBizDocID", BizDocId, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@tintMode", byteMode, NpgsqlTypes.NpgsqlDbType.Smallint))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_PutAway", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function GetPOForCostOptimizationAndMerge(ByVal isOnlyMergable As Boolean, ByVal isExcludePOSendToVendor As Boolean, ByVal isDisplayOnlyCostSaving As Boolean, ByVal costType As Short) As DataSet
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numWarehouseID", WarehouseID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@bitOnlyMergable", isOnlyMergable, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitExcludePOSendToVendor", isExcludePOSendToVendor, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@bitDisplayOnlyCostSaving", isDisplayOnlyCostSaving, NpgsqlTypes.NpgsqlDbType.Bit))
                    .Add(SqlDAL.Add_Parameter("@tintCostType", costType, NpgsqlTypes.NpgsqlDbType.Smallint))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur2", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                    .Add(SqlDAL.Add_Parameter("@SWV_RefCur3", Nothing, NpgsqlTypes.NpgsqlDbType.Refcursor, paramDirectionName:=ParameterDirection.InputOutput))
                End With

                Return SqlDAL.ExecuteDataset(connString, "USP_OpportunityMaster_GetPOForMerge", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub MergePurchaseOrders(ByVal masterPO As Long, ByVal itemsToMerge As String)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numMasterPOID", masterPO, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcItemsToMerge", itemsToMerge, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_MergePurchaseOrders", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub UpdatePurchaseOrderCost(ByVal selectedRecords As String)
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numUserCntID", UserCntID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcSelectedRecords", selectedRecords, NpgsqlTypes.NpgsqlDbType.VarChar))
                    .Add(SqlDAL.Add_Parameter("@ClientTimeZoneOffset", ClientTimeZoneOffset, NpgsqlTypes.NpgsqlDbType.Integer))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMaster_UpdatePurchaseOrderCost", sqlParams.ToArray())
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace