﻿'Created Chintan Prajapati
Option Explicit On
Option Strict On

Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.BusinessLogic.Opportunities
    Public Class OpenBizDocs
        Inherits BACRM.BusinessLogic.CBusinessBase

        '#Region "Constructor"
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Dim _ToDate As Date
        Public Property ToDate() As Date
            Get
                Return _ToDate
            End Get
            Set(ByVal Value As Date)
                _ToDate = Value
            End Set
        End Property

        Dim _FromDate As Date
        Public Property FromDate() As Date
            Get
                Return _FromDate
            End Get
            Set(ByVal Value As Date)
                _FromDate = Value
            End Set
        End Property

        ''Private DomainId As Long = 0
        Private _OppType As Integer
        Private _BizDocStatus As Integer
        Private _SortCol As String
        Private _SortDirection As String
        Private _CurrentPage As Integer
        Private _PageSize As Integer
        Private _TotalRecords As Integer

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property OppType() As Integer
            Get
                Return _OppType
            End Get
            Set(ByVal Value As Integer)
                _OppType = Value
            End Set
        End Property

        'Public Property UserCntID() As Integer
        '    Get
        '        Return UserCntID
        '    End Get
        '    Set(ByVal Value As Integer)
        '        UserCntID = Value
        '    End Set
        'End Property


        Public Property SortCol() As String
            Get
                Return _SortCol
            End Get
            Set(ByVal value As String)
                _SortCol = value
            End Set
        End Property


        Public Property SortDirection() As String
            Get
                Return _SortDirection
            End Get
            Set(ByVal value As String)
                _SortDirection = value
            End Set
        End Property


        Public Property BizDocStatus() As Integer
            Get
                Return _BizDocStatus
            End Get
            Set(ByVal value As Integer)
                _BizDocStatus = value
            End Set
        End Property
        Public Property TotalRecords() As Integer
            Get
                Return _TotalRecords
            End Get
            Set(ByVal Value As Integer)
                _TotalRecords = Value
            End Set
        End Property

        Public Property PageSize() As Integer
            Get
                Return _PageSize
            End Get
            Set(ByVal Value As Integer)
                _PageSize = Value
            End Set
        End Property

        Public Property CurrentPage() As Integer
            Get
                Return _CurrentPage
            End Get
            Set(ByVal Value As Integer)
                _CurrentPage = Value
            End Set
        End Property

        Private _DivisionID As Long
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal value As Long)
                _DivisionID = value
            End Set
        End Property

        Private _OppBizDocIds As String
        Public Property OppBizDocIds() As String
            Get
                Return _OppBizDocIds
            End Get
            Set(ByVal value As String)
                _OppBizDocIds = value
            End Set
        End Property

        Private _SalesUserRightType As Short
        Public Property SalesUserRightType() As Short
            Get
                Return _SalesUserRightType
            End Get
            Set(ByVal value As Short)
                _SalesUserRightType = value
            End Set
        End Property

        Private _FilterType As Short
        Public Property FilterType() As Short
            Get
                Return _FilterType
            End Get
            Set(ByVal value As Short)
                _FilterType = value
            End Set
        End Property
        Private _DepositID As Long
        Public Property DepositID() As Long
            Get
                Return _DepositID
            End Get
            Set(ByVal value As Long)
                _DepositID = value
            End Set
        End Property

        Private _BizDocID As Long
        Public Property BizDocID() As Long
            Get
                Return _BizDocID
            End Get
            Set(ByVal value As Long)
                _BizDocID = value
            End Set
        End Property

        Private _strBizDocStatus As String
        Public Property strBizDocStatus() As String
            Get
                Return _strBizDocStatus
            End Get
            Set(ByVal value As String)
                _strBizDocStatus = value
            End Set
        End Property

        Private _strBizDocType As String
        Public Property strBizDocType() As String
            Get
                Return _strBizDocType
            End Get
            Set(ByVal value As String)
                _strBizDocType = value
            End Set
        End Property

        Private _strOrderStatus As String
        Public Property strOrderStatus() As String
            Get
                Return _strOrderStatus
            End Get
            Set(ByVal value As String)
                _strOrderStatus = value
            End Set
        End Property

        Private _strOrderSource As String
        Public Property strOrderSource() As String
            Get
                Return _strOrderSource
            End Get
            Set(ByVal value As String)
                _strOrderSource = value
            End Set
        End Property

        Private _ClientTimeZoneOffset As Integer
        Public Property ClientTimeZoneOffset() As Integer
            Get
                Return _ClientTimeZoneOffset
            End Get
            Set(ByVal Value As Integer)
                _ClientTimeZoneOffset = Value
            End Set
        End Property

        Private _bitFulFilled As Boolean
        Public Property bitFulfilled() As Boolean
            Get
                Return _bitFulFilled
            End Get
            Set(ByVal Value As Boolean)
                _bitFulFilled = Value
            End Set
        End Property

        Public Property OppID As Long
        Public Property vcShippingService As String
        Public Property numShippingZone As Long
        Public Property RegularSearchCriteria As String
        Public Property CustomSearchCriteria As String

        Public Function ListBizDocs() As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParams() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(17) {}

                arParams(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(0).Value = DomainID

                'arParams(1) = New Npgsql.NpgsqlParameter("@intBizDocStatus", NpgsqlTypes.NpgsqlDbType.Integer)
                'If (_BizDocStatus = Nothing) Then
                '    arParams(1).Value = DBNull.Value
                'Else
                '    arParams(1).Value = _BizDocStatus
                'End If
                arParams(1) = New Npgsql.NpgsqlParameter("@vcBizDocStatus", NpgsqlTypes.NpgsqlDbType.VarChar)
                If (_strBizDocStatus = Nothing) Then
                    arParams(1).Value = DBNull.Value
                Else
                    arParams(1).Value = _strBizDocStatus
                End If

                arParams(2) = New Npgsql.NpgsqlParameter("@tintOppType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParams(2).Value = _OppType

                arParams(3) = New Npgsql.NpgsqlParameter("@SortCol", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParams(3).Value = _SortCol

                arParams(4) = New Npgsql.NpgsqlParameter("@SortDirection", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParams(4).Value = _SortDirection

                arParams(5) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParams(5).Value = _CurrentPage

                arParams(6) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParams(6).Value = _PageSize

                arParams(7) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParams(7).Direction = ParameterDirection.InputOutput
                arParams(7).Value = _TotalRecords

                arParams(8) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(8).Value = _DivisionID

                arParams(9) = New Npgsql.NpgsqlParameter("@numBizDocId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(9).Value = IIf(_BizDocID = Nothing, 0, _BizDocID)

                arParams(10) = New Npgsql.NpgsqlParameter("@tintSalesUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParams(10).Value = _SalesUserRightType

                arParams(11) = New Npgsql.NpgsqlParameter("@tintPurchaseUserRightType", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParams(11).Value = 0

                arParams(12) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(12).Value = UserCntID

                arParams(13) = New Npgsql.NpgsqlParameter("@tintFilter", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(13).Value = _FilterType

                arParams(14) = New Npgsql.NpgsqlParameter("@dtFromDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParams(14).Value = _FromDate

                arParams(15) = New Npgsql.NpgsqlParameter("@dtToDate", NpgsqlTypes.NpgsqlDbType.Timestamp)
                arParams(15).Value = _ToDate

                arParams(16) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParams(16).Value = _ClientTimeZoneOffset

                arParams(17) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParams(17).Value = Nothing
                arParams(17).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParams.ToArray()
                ListBizDocs = SqlDAL.ExecuteDatasetWithOutPut(connString, "usp_GetOpenBizDocs", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(7).Value)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function GetSalesFulfillmentOrder(ByVal boolIncludeHistory As Boolean) As DataSet
            Try
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString
                Dim arParams() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(21) {}

                arParams(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(0).Value = DomainID

                arParams(1) = New Npgsql.NpgsqlParameter("@SortCol", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParams(1).Value = _SortCol

                arParams(2) = New Npgsql.NpgsqlParameter("@SortDirection", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParams(2).Value = _SortDirection

                arParams(3) = New Npgsql.NpgsqlParameter("@CurrentPage", NpgsqlTypes.NpgsqlDbType.Integer)
                arParams(3).Value = _CurrentPage

                arParams(4) = New Npgsql.NpgsqlParameter("@PageSize", NpgsqlTypes.NpgsqlDbType.Integer)
                arParams(4).Value = _PageSize

                arParams(5) = New Npgsql.NpgsqlParameter("@TotRecs", NpgsqlTypes.NpgsqlDbType.Integer)
                arParams(5).Direction = ParameterDirection.InputOutput
                arParams(5).Value = _TotalRecords

                arParams(6) = New Npgsql.NpgsqlParameter("@numUserCntID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(6).Value = UserCntID

                arParams(7) = New Npgsql.NpgsqlParameter("@vcBizDocStatus", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParams(7).Value = _strBizDocStatus

                arParams(8) = New Npgsql.NpgsqlParameter("@vcBizDocType", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParams(8).Value = _strBizDocType

                arParams(9) = New Npgsql.NpgsqlParameter("@numDivisionID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(9).Value = _DivisionID

                arParams(10) = New Npgsql.NpgsqlParameter("@ClientTimeZoneOffset", NpgsqlTypes.NpgsqlDbType.Integer)
                arParams(10).Value = _ClientTimeZoneOffset

                arParams(11) = New Npgsql.NpgsqlParameter("@bitIncludeHistory", NpgsqlTypes.NpgsqlDbType.Bit)
                arParams(11).Value = boolIncludeHistory

                arParams(12) = New Npgsql.NpgsqlParameter("@vcOrderStatus", NpgsqlTypes.NpgsqlDbType.VarChar, 500)
                arParams(12).Value = _strOrderStatus

                arParams(13) = New Npgsql.NpgsqlParameter("@vcOrderSource", NpgsqlTypes.NpgsqlDbType.VarChar, 1000)
                arParams(13).Value = _strOrderSource

                arParams(14) = New Npgsql.NpgsqlParameter("@numOppID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(14).Value = OppID

                arParams(15) = New Npgsql.NpgsqlParameter("@vcShippingService", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParams(15).Value = vcShippingService

                arParams(16) = New Npgsql.NpgsqlParameter("@numShippingZone", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParams(16).Value = numShippingZone

                arParams(17) = New Npgsql.NpgsqlParameter("@vcRegularSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParams(17).Value = _RegularSearchCriteria

                arParams(18) = New Npgsql.NpgsqlParameter("@vcCustomSearchCriteria", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParams(18).Value = _CustomSearchCriteria

                arParams(19) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParams(19).Value = Nothing
                arParams(19).Direction = ParameterDirection.InputOutput

                arParams(20) = New Npgsql.NpgsqlParameter("@SWV_RefCur2", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParams(20).Value = Nothing
                arParams(20).Direction = ParameterDirection.InputOutput

                arParams(21) = New Npgsql.NpgsqlParameter("@SWV_RefCur3", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParams(21).Value = Nothing
                arParams(21).Direction = ParameterDirection.InputOutput

                Dim objParam() As Object = arParams.ToArray()
                Dim ds As DataSet = SqlDAL.ExecuteDatasetWithOutPut(connString, "usp_GetSalesFulfillmentOrder", objParam, True)
                _TotalRecords = CInt(DirectCast(objParam, Npgsql.NpgsqlParameter())(5).Value)

                Return ds
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class
End Namespace
