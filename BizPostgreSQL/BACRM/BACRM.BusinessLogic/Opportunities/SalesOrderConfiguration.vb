﻿Imports BACRMBUSSLOGIC.BussinessLogic
Imports System.Data.SqlClient
Imports BACRMAPI.DataAccessLayer
Imports BACRM.BusinessLogic.Common
Namespace BACRM.BusinessLogic.Opportunities

    Public Class SalesOrderConfiguration
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Member Variables"
        Private _numSalesConfigurationID As Integer = 0
        Private _bitAutoFocusCustomer As Boolean
        Private _bitAutoFocusItem As Boolean
        Private _bitDisplayRootLocation As Boolean
        Private _bitDisplayCurrency As Boolean
        Private _bitDisplayAssignTo As Boolean
        Private _bitDisplayTemplate As Boolean
        Private _bitAutoAssignOrder As Boolean
        Private _bitDisplayLocation As Boolean
        Private _bitDisplayFinancialStamp As Boolean
        Private _bitDisplayItemDetails As Boolean
        Private _bitDisplayUnitCost As Boolean
        Private _bitDisplayProfitTotal As Boolean
        Private _bitDisplayShippingRates As Boolean
        Private _bitDisplayPaymentMethods As Boolean
        Private _bitCreateOpenBizDoc As Boolean
        Private _numListItemID As Integer
        Private _vcPaymentMethodIDs As String
        Private _bitDisplayCouponDiscount As Boolean
        Private _bitDisplayShippingCharges As Boolean
        Private _bitDisplayDiscount As Boolean
        Private _bitDisplaySaveNew As Boolean
        Private _bitDisplayAddToCart As Boolean
        Private _bitDisplayCreateInvoice As Boolean
        Private _bitDisplayComments As Boolean
        Private _bitDisplayExpectedDate As Boolean
        Private _bitDisplayReleaseDate As Boolean
        Private _bitDisplayCustomerPartEntry As Boolean
        Private _bitDisplayItemGridOrderAZ As Boolean
        Private _bitDisplayItemGridItemID As Boolean
        Private _bitDisplayItemGridSKU As Boolean
        Private _bitDisplayItemGridUnitListPrice As Boolean
        Private _bitDisplayItemGridUnitSalePrice As Boolean
        Private _bitDisplayItemGridDiscount As Boolean
        Private _bitDisplayItemGridItemRelaseDate As Boolean
        Private _bitDisplayItemGridLocation As Boolean
        Private _bitDisplayItemGridShipTo As Boolean
        Private _bitDisplayItemGridOnHandAllocation As Boolean
        Private _bitDisplayItemGridDescription As Boolean
        Private _bitDisplayItemGridNotes As Boolean
        Private _bitDisplayItemGridAttributes As Boolean
        Private _bitDisplayItemGridInclusionDetail As Boolean
        Private _bitDisplayItemGridItemClassification As Boolean


#End Region

#Region "Public Properties"
        Public Property SalesConfigurationID() As Integer
            Get
                Return _numSalesConfigurationID
            End Get
            Set(ByVal Value As Integer)
                _numSalesConfigurationID = Value
            End Set
        End Property
        Public Property bitAutoFocusCustomer() As Boolean
            Get
                Return _bitAutoFocusCustomer
            End Get
            Set(ByVal Value As Boolean)
                _bitAutoFocusCustomer = Value
            End Set
        End Property
        Public Property bitAutoFocusItem() As Boolean
            Get
                Return _bitAutoFocusItem
            End Get
            Set(ByVal Value As Boolean)
                _bitAutoFocusItem = Value
            End Set
        End Property
        Public Property bitDisplayRootLocation() As Boolean
            Get
                Return _bitDisplayRootLocation
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayRootLocation = Value
            End Set
        End Property
        Public Property bitDisplayCurrency() As Boolean
            Get
                Return _bitDisplayCurrency
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayCurrency = Value
            End Set
        End Property
        Public Property bitDisplayAssignTo() As Boolean
            Get
                Return _bitDisplayAssignTo
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayAssignTo = Value
            End Set
        End Property
        Public Property bitDisplayTemplate() As Boolean
            Get
                Return _bitDisplayTemplate
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayTemplate = Value
            End Set
        End Property
        Public Property bitAutoAssignOrder() As Boolean
            Get
                Return _bitAutoAssignOrder
            End Get
            Set(ByVal Value As Boolean)
                _bitAutoAssignOrder = Value
            End Set
        End Property
        Public Property bitDisplayLocation() As Boolean
            Get
                Return _bitDisplayLocation
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayLocation = Value
            End Set
        End Property
        Public Property bitDisplayFinancialStamp() As Boolean
            Get
                Return _bitDisplayFinancialStamp
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayFinancialStamp = Value
            End Set
        End Property
        Public Property bitDisplayItemDetails() As Boolean
            Get
                Return _bitDisplayItemDetails
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemDetails = Value
            End Set
        End Property
        Public Property bitDisplayUnitCost() As Boolean
            Get
                Return _bitDisplayUnitCost
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayUnitCost = Value
            End Set
        End Property
        Public Property bitDisplayProfitTotal() As Boolean
            Get
                Return _bitDisplayProfitTotal
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayProfitTotal = Value
            End Set
        End Property
        Public Property bitDisplayShippingRates() As Boolean
            Get
                Return _bitDisplayShippingRates
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayShippingRates = Value
            End Set
        End Property
        Public Property bitDisplayPaymentMethods() As Boolean
            Get
                Return _bitDisplayPaymentMethods
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayPaymentMethods = Value
            End Set
        End Property
        Public Property bitCreateOpenBizDoc() As Boolean
            Get
                Return _bitCreateOpenBizDoc
            End Get
            Set(ByVal Value As Boolean)
                _bitCreateOpenBizDoc = Value
            End Set
        End Property
        Public Property numListItemID() As Integer
            Get
                Return _numListItemID
            End Get
            Set(ByVal Value As Integer)
                _numListItemID = Value
            End Set
        End Property
        Public Property vcPaymentMethodIDs() As String
            Get
                Return _vcPaymentMethodIDs
            End Get
            Set(ByVal Value As String)
                _vcPaymentMethodIDs = Value
            End Set
        End Property
        Public Property bitDisplayCouponDiscount() As Boolean
            Get
                Return _bitDisplayCouponDiscount
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayCouponDiscount = Value
            End Set
        End Property
        Public Property bitDisplayShippingCharges() As Boolean
            Get
                Return _bitDisplayShippingCharges
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayShippingCharges = Value
            End Set
        End Property
        Public Property bitDisplayDiscount() As Boolean
            Get
                Return _bitDisplayDiscount
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayDiscount = Value
            End Set
        End Property
        Public Property bitDisplaySaveNew() As Boolean
            Get
                Return _bitDisplaySaveNew
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplaySaveNew = Value
            End Set
        End Property
        Public Property bitDisplayAddToCart() As Boolean
            Get
                Return _bitDisplayAddToCart
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayAddToCart = Value
            End Set
        End Property
        Public Property bitDisplayCreateInvoice() As Boolean
            Get
                Return _bitDisplayCreateInvoice
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayCreateInvoice = Value
            End Set
        End Property
        Public Property bitDisplayShipVia As Boolean
        Public Property bitDisplayComments() As Boolean
            Get
                Return _bitDisplayComments
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayComments = Value
            End Set
        End Property
        Public Property bitDisplayReleaseDate() As Boolean
            Get
                Return _bitDisplayReleaseDate
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayReleaseDate = Value
            End Set
        End Property

        Public Property bitDisplayExpectedDate() As Boolean
            Get
                Return _bitDisplayExpectedDate
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayExpectedDate = Value
            End Set
        End Property

        Public Property bitDisplayCustomerPartEntry() As Boolean
            Get
                Return _bitDisplayCustomerPartEntry
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayCustomerPartEntry = Value
            End Set
        End Property
        Public Property bitDisplayItemGridOrderAZ() As Boolean
            Get
                Return _bitDisplayItemGridOrderAZ
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridOrderAZ = Value
            End Set
        End Property
        Public Property bitDisplayItemGridItemID() As Boolean
            Get
                Return _bitDisplayItemGridItemID
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridItemID = Value
            End Set
        End Property
        Public Property bitDisplayItemGridSKU() As Boolean
            Get
                Return _bitDisplayItemGridSKU
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridSKU = Value
            End Set
        End Property
        Public Property bitDisplayItemGridUnitListPrice() As Boolean
            Get
                Return _bitDisplayItemGridUnitListPrice
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridUnitListPrice = Value
            End Set
        End Property
        Public Property bitDisplayItemGridUnitSalePrice() As Boolean
            Get
                Return _bitDisplayItemGridUnitSalePrice
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridUnitSalePrice = Value
            End Set
        End Property
        Public Property bitDisplayItemGridDiscount() As Boolean
            Get
                Return _bitDisplayItemGridDiscount
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridDiscount = Value
            End Set
        End Property
        Public Property bitDisplayItemGridItemRelaseDate() As Boolean
            Get
                Return _bitDisplayItemGridItemRelaseDate
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridItemRelaseDate = Value
            End Set
        End Property
        Public Property bitDisplayItemGridLocation() As Boolean
            Get
                Return _bitDisplayItemGridLocation
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridLocation = Value
            End Set
        End Property
        Public Property bitDisplayItemGridShipTo() As Boolean
            Get
                Return _bitDisplayItemGridShipTo
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridShipTo = Value
            End Set
        End Property
        Public Property bitDisplayItemGridOnHandAllocation() As Boolean
            Get
                Return _bitDisplayItemGridOnHandAllocation
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridOnHandAllocation = Value
            End Set
        End Property
        Public Property bitDisplayItemGridDescription() As Boolean
            Get
                Return _bitDisplayItemGridDescription
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridDescription = Value
            End Set
        End Property
        Public Property bitDisplayItemGridNotes() As Boolean
            Get
                Return _bitDisplayItemGridNotes
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridNotes = Value
            End Set
        End Property
        Public Property bitDisplayItemGridAttributes() As Boolean
            Get
                Return _bitDisplayItemGridAttributes
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridAttributes = Value
            End Set
        End Property
        Public Property bitDisplayItemGridInclusionDetail() As Boolean
            Get
                Return _bitDisplayItemGridInclusionDetail
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridInclusionDetail = Value
            End Set
        End Property
        Public Property bitDisplayItemGridItemClassification() As Boolean
            Get
                Return _bitDisplayItemGridItemClassification
            End Get
            Set(ByVal Value As Boolean)
                _bitDisplayItemGridItemClassification = Value
            End Set
        End Property

        Public Property bitDisplayItemGridItemPromotion As Boolean
        Public Property bitDisplayApplyPromotionCode As Boolean
        Public Property bitDisplayPayButton As Boolean
        Public Property bitDisplayPOSPay As Boolean
        Public Property vcPOHiddenColumns As String
#End Region

#Region "Public Methods"
        Public Sub SaveSalesOrderConfiguration()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(49) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString

                If _numSalesConfigurationID = 0 Then
                    arParms(0) = New Npgsql.NpgsqlParameter("@numSOCID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(0).Value = DBNull.Value
                Else
                    arParms(0) = New Npgsql.NpgsqlParameter("@numSOCID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(0).Value = SalesConfigurationID
                End If

                arParms(1) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = DomainID

                arParms(2) = New Npgsql.NpgsqlParameter("@bitAutoFocusCustomer", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(2).Value = bitAutoFocusCustomer

                arParms(3) = New Npgsql.NpgsqlParameter("@bitAutoFocusItem", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(3).Value = bitAutoFocusItem

                arParms(4) = New Npgsql.NpgsqlParameter("@bitDisplayRootLocation", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(4).Value = bitDisplayRootLocation

                arParms(5) = New Npgsql.NpgsqlParameter("@bitAutoAssignOrder", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(5).Value = bitAutoAssignOrder

                arParms(6) = New Npgsql.NpgsqlParameter("@bitDisplayLocation", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(6).Value = bitDisplayLocation

                arParms(7) = New Npgsql.NpgsqlParameter("@bitDisplayFinancialStamp", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(7).Value = bitDisplayFinancialStamp

                arParms(8) = New Npgsql.NpgsqlParameter("@bitDisplayItemDetails", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(8).Value = bitDisplayItemDetails

                arParms(9) = New Npgsql.NpgsqlParameter("@bitDisplayUnitCost", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(9).Value = bitDisplayUnitCost

                arParms(10) = New Npgsql.NpgsqlParameter("@bitDisplayProfitTotal", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(10).Value = bitDisplayProfitTotal

                arParms(11) = New Npgsql.NpgsqlParameter("@bitDisplayShippingRates", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(11).Value = bitDisplayShippingRates

                arParms(12) = New Npgsql.NpgsqlParameter("@bitDisplayPaymentMethods", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(12).Value = bitDisplayPaymentMethods

                arParms(13) = New Npgsql.NpgsqlParameter("@bitCreateOpenBizDoc", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(13).Value = bitCreateOpenBizDoc

                If numListItemID = -1 Then
                    arParms(14) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(14).Value = DBNull.Value
                Else
                    arParms(14) = New Npgsql.NpgsqlParameter("@numListItemID", NpgsqlTypes.NpgsqlDbType.BigInt)
                    arParms(14).Value = numListItemID
                End If

                If String.IsNullOrEmpty(vcPaymentMethodIDs) Then
                    arParms(15) = New Npgsql.NpgsqlParameter("@vcPaymentMethodIDs", NpgsqlTypes.NpgsqlDbType.VarChar)
                    arParms(15).Value = DBNull.Value
                Else
                    arParms(15) = New Npgsql.NpgsqlParameter("@vcPaymentMethodIDs", NpgsqlTypes.NpgsqlDbType.VarChar)
                    arParms(15).Value = vcPaymentMethodIDs
                End If

                arParms(16) = New Npgsql.NpgsqlParameter("@bitDisplayCurrency", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(16).Value = bitDisplayCurrency

                arParms(17) = New Npgsql.NpgsqlParameter("@bitDisplayAssignTo", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(17).Value = bitDisplayAssignTo

                arParms(18) = New Npgsql.NpgsqlParameter("@bitDisplayTemplate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(18).Value = bitDisplayTemplate

                arParms(19) = New Npgsql.NpgsqlParameter("@bitDisplayCouponDiscount", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(19).Value = bitDisplayCouponDiscount

                arParms(20) = New Npgsql.NpgsqlParameter("@bitDisplayShippingCharges", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(20).Value = bitDisplayShippingCharges

                arParms(21) = New Npgsql.NpgsqlParameter("@bitDisplayDiscount", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(21).Value = bitDisplayDiscount

                arParms(22) = New Npgsql.NpgsqlParameter("@bitDisplaySaveNew", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(22).Value = bitDisplaySaveNew

                arParms(23) = New Npgsql.NpgsqlParameter("@bitDisplayAddToCart", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(23).Value = bitDisplayAddToCart

                arParms(24) = New Npgsql.NpgsqlParameter("@bitDisplayCreateInvoice", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(24).Value = bitDisplayCreateInvoice

                arParms(25) = New Npgsql.NpgsqlParameter("@bitDisplayShipVia", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(25).Value = bitDisplayShipVia

                arParms(26) = New Npgsql.NpgsqlParameter("@bitDisplayComments", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(26).Value = bitDisplayComments

                arParms(27) = New Npgsql.NpgsqlParameter("@bitDisplayReleaseDate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(27).Value = bitDisplayReleaseDate

                arParms(28) = New Npgsql.NpgsqlParameter("@bitDisplayCustomerPart#Entry", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(28).Value = bitDisplayCustomerPartEntry

                arParms(29) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridOrderAZ", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(29).Value = bitDisplayItemGridOrderAZ

                arParms(30) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridItemID", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(30).Value = bitDisplayItemGridItemID

                arParms(31) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridSKU", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(31).Value = bitDisplayItemGridSKU

                arParms(32) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridUnitListPrice", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(32).Value = bitDisplayItemGridUnitListPrice

                arParms(33) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridUnitSalePrice", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(33).Value = bitDisplayItemGridUnitSalePrice

                arParms(34) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridDiscount", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(34).Value = bitDisplayItemGridDiscount

                arParms(35) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridItemRelaseDate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(35).Value = bitDisplayItemGridItemRelaseDate

                arParms(36) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridLocation", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(36).Value = bitDisplayItemGridLocation

                arParms(37) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridShipTo", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(37).Value = bitDisplayItemGridShipTo

                arParms(38) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridOnHandAllocation", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(38).Value = bitDisplayItemGridOnHandAllocation

                arParms(39) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridDescription", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(39).Value = bitDisplayItemGridDescription

                arParms(40) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridNotes", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(40).Value = bitDisplayItemGridNotes

                arParms(41) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridAttributes", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(41).Value = bitDisplayItemGridAttributes

                arParms(42) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridInclusionDetail", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(42).Value = bitDisplayItemGridInclusionDetail

                arParms(43) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridItemClassification", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(43).Value = bitDisplayItemGridItemClassification

                arParms(44) = New Npgsql.NpgsqlParameter("@bitDisplayExpectedDate", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(44).Value = bitDisplayExpectedDate

                arParms(45) = New Npgsql.NpgsqlParameter("@bitDisplayItemGridItemPromotion", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(45).Value = bitDisplayItemGridItemPromotion

                arParms(46) = New Npgsql.NpgsqlParameter("@bitDisplayApplyPromotionCode", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(46).Value = bitDisplayApplyPromotionCode

                arParms(47) = New Npgsql.NpgsqlParameter("@bitDisplayPayButton", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(47).Value = bitDisplayPayButton

                arParms(48) = New Npgsql.NpgsqlParameter("@bitDisplayPOSPay", NpgsqlTypes.NpgsqlDbType.Bit)
                arParms(48).Value = bitDisplayPOSPay

                arParms(49) = New Npgsql.NpgsqlParameter("@vcPOHiddenColumns", NpgsqlTypes.NpgsqlDbType.VarChar)
                arParms(49).Value = vcPOHiddenColumns

                SqlDAL.ExecuteNonQuery(connString, "USP_SalesOrderConfiguration_Save", arParms)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function GetSalesOrderConfiguration()
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(1) {}
                Dim getconnection As New GetConnection
                Dim connString As String = GetConnection.GetConnectionString
                Dim ds As DataSet

                arParms(0) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(0).Value = DomainID

                arParms(1) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(1).Value = Nothing
                arParms(1).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_SalesOrderConfiguration_Get", arParms)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim dataRow As DataRow
                    dataRow = ds.Tables(0).Rows(0)

                    SalesConfigurationID = CCommon.ToInteger(dataRow("numSOCID"))
                    DomainID = CCommon.ToInteger(dataRow("numDomainID"))
                    bitAutoFocusCustomer = CCommon.ToBool(dataRow("bitAutoFocusCustomer"))
                    bitAutoFocusItem = CCommon.ToBool(dataRow("bitAutoFocusItem"))
                    bitDisplayRootLocation = CCommon.ToBool(dataRow("bitDisplayRootLocation"))
                    bitDisplayCurrency = CCommon.ToBool(dataRow("bitDisplayCurrency"))
                    bitDisplayAssignTo = CCommon.ToBool(dataRow("bitDisplayAssignTo"))
                    bitDisplayTemplate = CCommon.ToBool(dataRow("bitDisplayTemplate"))
                    bitAutoAssignOrder = CCommon.ToBool(dataRow("bitAutoAssignOrder"))
                    bitDisplayLocation = CCommon.ToBool(dataRow("bitDisplayLocation"))
                    bitDisplayFinancialStamp = CCommon.ToBool(dataRow("bitDisplayFinancialStamp"))
                    bitDisplayItemDetails = CCommon.ToBool(dataRow("bitDisplayItemDetails"))
                    bitDisplayUnitCost = CCommon.ToBool(dataRow("bitDisplayUnitCost"))
                    bitDisplayProfitTotal = CCommon.ToBool(dataRow("bitDisplayProfitTotal"))
                    bitDisplayShippingRates = CCommon.ToBool(dataRow("bitDisplayShippingRates"))
                    bitDisplayPaymentMethods = CCommon.ToBool(dataRow("bitDisplayPaymentMethods"))
                    bitCreateOpenBizDoc = CCommon.ToBool(dataRow("bitCreateOpenBizDoc"))
                    numListItemID = CCommon.ToInteger(dataRow("numListItemID"))
                    vcPaymentMethodIDs = CCommon.ToString(dataRow("vcPaymentMethodIDs"))
                    bitDisplayCouponDiscount = CCommon.ToBool(dataRow("bitDisplayCouponDiscount"))
                    bitDisplayShippingCharges = CCommon.ToBool(dataRow("bitDisplayShippingCharges"))
                    bitDisplayDiscount = CCommon.ToBool(dataRow("bitDisplayDiscount"))
                    bitDisplaySaveNew = CCommon.ToBool(dataRow("bitDisplaySaveNew"))
                    bitDisplayAddToCart = CCommon.ToBool(dataRow("bitDisplayAddToCart"))
                    bitDisplayCreateInvoice = CCommon.ToBool(dataRow("bitDisplayCreateInvoice"))
                    bitDisplayShipVia = CCommon.ToBool(dataRow("bitDisplayShipVia"))
                    bitDisplayComments = CCommon.ToBool(dataRow("bitDisplayComments"))
                    bitDisplayReleaseDate = CCommon.ToBool(dataRow("bitDisplayReleaseDate"))
                    bitDisplayCustomerPartEntry = CCommon.ToBool(dataRow("bitDisplayCustomerPart#Entry"))
                    bitDisplayItemGridOrderAZ = CCommon.ToBool(dataRow("bitDisplayItemGridOrderAZ"))
                    bitDisplayItemGridItemID = CCommon.ToBool(dataRow("bitDisplayItemGridItemID"))
                    bitDisplayItemGridSKU = CCommon.ToBool(dataRow("bitDisplayItemGridSKU"))
                    bitDisplayItemGridUnitListPrice = CCommon.ToBool(dataRow("bitDisplayItemGridUnitListPrice"))
                    bitDisplayItemGridUnitSalePrice = CCommon.ToBool(dataRow("bitDisplayItemGridUnitSalePrice"))
                    bitDisplayItemGridDiscount = CCommon.ToBool(dataRow("bitDisplayItemGridDiscount"))
                    bitDisplayItemGridItemRelaseDate = CCommon.ToBool(dataRow("bitDisplayItemGridItemRelaseDate"))
                    bitDisplayItemGridLocation = CCommon.ToBool(dataRow("bitDisplayItemGridLocation"))
                    bitDisplayItemGridShipTo = CCommon.ToBool(dataRow("bitDisplayItemGridShipTo"))
                    bitDisplayItemGridOnHandAllocation = CCommon.ToBool(dataRow("bitDisplayItemGridOnHandAllocation"))
                    bitDisplayItemGridDescription = CCommon.ToBool(dataRow("bitDisplayItemGridDescription"))
                    bitDisplayItemGridNotes = CCommon.ToBool(dataRow("bitDisplayItemGridNotes"))
                    bitDisplayItemGridAttributes = CCommon.ToBool(dataRow("bitDisplayItemGridAttributes"))
                    bitDisplayItemGridInclusionDetail = CCommon.ToBool(dataRow("bitDisplayItemGridInclusionDetail"))
                    bitDisplayItemGridItemClassification = CCommon.ToBool(dataRow("bitDisplayItemGridItemClassification"))
                    bitDisplayExpectedDate = CCommon.ToBool(dataRow("bitDisplayExpectedDate"))
                    bitDisplayItemGridItemPromotion = CCommon.ToBool(dataRow("bitDisplayItemGridItemPromotion"))
                    bitDisplayApplyPromotionCode = CCommon.ToBool(dataRow("bitDisplayApplyPromotionCode"))
                    bitDisplayPayButton = CCommon.ToBool(dataRow("bitDisplayPayButton"))
                    bitDisplayPOSPay = CCommon.ToBool(dataRow("bitDisplayPOSPay"))
                    vcPOHiddenColumns = CCommon.ToString(dataRow("vcPOHiddenColumns"))
                Else
                    SalesConfigurationID = -1
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function
#End Region

    End Class

End Namespace

