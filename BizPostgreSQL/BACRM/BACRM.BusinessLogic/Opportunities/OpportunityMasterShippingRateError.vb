﻿Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common

Namespace BACRM.BusinessLogic.Opportunities

    Public Class OpportunityMasterShippingRateError
        Inherits BACRM.BusinessLogic.CBusinessBase

#Region "Public Properties"

        Public Property OppID As Long
        Public Property ErrorMessage As String

#End Region

#Region "Public Methods"

        Public Sub Save()
            Try
                Dim sqlParams As New System.Collections.Generic.List(Of Npgsql.NpgsqlParameter)
                Dim getconnection As New BACRMBUSSLOGIC.BussinessLogic.GetConnection
                Dim connString As String = getconnection.GetConnectionString

                With sqlParams
                    .Add(SqlDAL.Add_Parameter("@numDomainID", DomainID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@numOppID", OppID, NpgsqlTypes.NpgsqlDbType.BigInt))
                    .Add(SqlDAL.Add_Parameter("@vcErrorMessage", ErrorMessage, NpgsqlTypes.NpgsqlDbType.VarChar))
                End With

                SqlDAL.ExecuteNonQuery(connString, "USP_OpportunityMasterShippingRateError_Save", sqlParams.ToArray())
            Catch ex As Exception
                'DO NOT THROW
            End Try
        End Sub

#End Region

    End Class

End Namespace

