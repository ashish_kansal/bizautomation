'Created Anoop Jayaraj
Option Explicit On
Option Strict On

Imports BACRMAPI.DataAccessLayer
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Namespace BACRM.BusinessLogic.Opportunities
    Public Class SubStages
        Inherits BACRM.BusinessLogic.CBusinessBase

        Private _ProcessID As Long
        Private _strSubStages As String
        Private _SubStageID As Long
        Private _SubStageName As String
        'Private DomainId As Long

        'Public Property DomainId() As Long
        '    Get
        '        Return DomainId
        '    End Get
        '    Set(ByVal Value As Long)
        '        DomainId = Value
        '    End Set
        'End Property

        Public Property SubStageName() As String
            Get
                Return _SubStageName
            End Get
            Set(ByVal Value As String)
                _SubStageName = Value
            End Set
        End Property

        Public Property SubStageID() As Long
            Get
                Return _SubStageID
            End Get
            Set(ByVal Value As Long)
                _SubStageID = Value
            End Set
        End Property

        Public Property ProcessID() As Long
            Get
                Return _ProcessID
            End Get
            Set(ByVal Value As Long)
                _ProcessID = Value
            End Set
        End Property

        Public Property strSubStages() As String
            Get
                Return _strSubStages
            End Get
            Set(ByVal Value As String)
                _strSubStages = Value
            End Set
        End Property

        '#Region "Constructor"
        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32               
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-March-05
        '        '**********************************************************************************
        '        Public Sub New(ByVal intUserId As Integer)
        '            'Constructor
        '            MyBase.New(intUserId)
        '        End Sub

        '        '**********************************************************************************
        '        ' Name         : New
        '        ' Type         : Sub
        '        ' Scope        : Public
        '        ' Returns      : N/A
        '        ' Parameters   : ByVal intUserId As Int32              
        '        ' Description  : The constructor                
        '        ' Notes        : N/A                
        '        ' Created By   : Anoop Jayaraj 	DATE:28-Feb-05
        '        '**********************************************************************************
        '        Public Sub New()
        '            'Constructor
        '            MyBase.New()
        '        End Sub
        '#End Region

        Public Function GetSubStageBySalesProId() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 0

                arParms(1) = New Npgsql.NpgsqlParameter("@numProcessId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProcessID

                arParms(2) = New Npgsql.NpgsqlParameter("@numSubStageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _SubStageID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_AdminProcess", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function DeleteSubStage() As Boolean
            Try
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New getconnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 1

                arParms(1) = New Npgsql.NpgsqlParameter("@numProcessId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProcessID

                arParms(2) = New Npgsql.NpgsqlParameter("@numSubStageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _SubStageID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                SqlDAL.ExecuteNonQuery(connString, "USP_AdminProcess", arParms)
                Return True
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function


        Public Function GetSubStages() As DataTable
            Try
                Dim ds As DataSet
                Dim arParms() As Npgsql.NpgsqlParameter = New Npgsql.NpgsqlParameter(4) {}
                Dim getconnection As New GetConnection
                Dim connString As String = getconnection.GetConnectionString

                arParms(0) = New Npgsql.NpgsqlParameter("@byteMode", NpgsqlTypes.NpgsqlDbType.Smallint)
                arParms(0).Value = 2

                arParms(1) = New Npgsql.NpgsqlParameter("@numProcessId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(1).Value = _ProcessID

                arParms(2) = New Npgsql.NpgsqlParameter("@numSubStageId", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(2).Value = _SubStageID

                arParms(3) = New Npgsql.NpgsqlParameter("@numDomainID", NpgsqlTypes.NpgsqlDbType.BigInt)
                arParms(3).Value = DomainID

                arParms(4) = New Npgsql.NpgsqlParameter("@SWV_RefCur", NpgsqlTypes.NpgsqlDbType.Refcursor)
                arParms(4).Value = Nothing
                arParms(4).Direction = ParameterDirection.InputOutput

                ds = SqlDAL.ExecuteDataset(connString, "USP_AdminProcess", arParms)

                Return ds.Tables(0)
            Catch ex As Exception
                ' Log exception details
                Throw ex
            End Try
        End Function

    End Class
End Namespace
