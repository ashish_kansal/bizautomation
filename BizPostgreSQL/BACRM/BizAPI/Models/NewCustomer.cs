﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class NewCustomer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Organization { get; set; }
        public string Email { get; set; }
        public string Ext { get; set; }
        public string Phone { get; set; }
        public Address BillingAddress { get; set; }
        public bool IsShippingAddressDifferent { get; set; }
        public Address ShippingAddress { get; set; }
        public Relationship Relationship { get; set; }
    }
}