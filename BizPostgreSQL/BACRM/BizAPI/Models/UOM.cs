﻿using BACRM.BusinessLogic.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class UOM
    {
        #region Public Properties

        public long ID { get; set; }
        public string Name { get; set; }

        #endregion

        #region Public Methods

        public IEnumerable<UOM> GetUnitOfMeasures(long domainID)
        {
            try
            {
                return GetUOM(domainID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Private Methods

        private IEnumerable<UOM> GetUOM(long domainID)
        {
            List<UOM> listUOM = new List<UOM>();

            CCommon common = new CCommon();
            common.DomainID = domainID;
            common.UOMAll = true;
            DataTable dtUnit = common.GetItemUOM();

            if (dtUnit != null && dtUnit.Rows.Count > 0)
            {
                listUOM = dtUnit.AsEnumerable()
                                    .Select(row => new UOM
                                    {
                                        ID = CCommon.ToInteger(row["numUOMId"]),
                                        Name = CCommon.ToString(row["vcUnitName"]),
                                    }).ToList();
            }

            return listUOM.AsEnumerable();
        }

        #endregion
    }
}