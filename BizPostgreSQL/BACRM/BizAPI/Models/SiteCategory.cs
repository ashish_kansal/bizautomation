﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class SiteCategory
    {
        #region Public Properties
        
        public long CategoryID { get; set; }
        public long ParentCategoryID { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public int Level { get; set; }
        public int DisplayOrder { get; set; }

        #endregion
        


    }
}