﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class SalesOrderItem
    {
        public int ItemCode { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public Warehouse Warehouse { get; set; }
        public bool IsDropShip { get; set; }
        public long VendorID { get; set; }
        public bool IsWorkOrder { get; set; }
    }
}