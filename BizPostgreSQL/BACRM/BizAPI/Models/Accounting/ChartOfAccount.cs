﻿using System;

namespace BizAPI.Models.Accounting
{
    public class ChartOfAccount
    {
        public int AccountID { get; set; }
        public string AccountName { get; set; }
        public string AccountType { get; set; }
    }
}