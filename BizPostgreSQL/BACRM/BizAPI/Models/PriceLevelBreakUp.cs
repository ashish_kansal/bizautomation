﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class PriceLevelBreakUp
    {
        public int FromQuantity { get; set; }
        public int ToQuantity { get; set; }
        public Double Amount { get; set; }
    }
}