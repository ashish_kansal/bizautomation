﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class SalesOrderDetail
    {
        public long ID { get; set; }
        public string SalesOrderName { get; set; }
        public string Campaign { get; set; }
        public Customer Customer { get; set; }
        public Contact Contact { get; set; }
        public string Source { get; set; }
        public string BillingAddress { get; set; }
        public string ShippingAddress { get; set; }
        public string Status { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public bool IsOrderClosed { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public string AssignedTo { get; set; }
        public string Currency { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal ExchangeRate { get; set; }
        public IQueryable<SalesOrderDetailItem> SalesOrderItems { get; set; }
    }
}