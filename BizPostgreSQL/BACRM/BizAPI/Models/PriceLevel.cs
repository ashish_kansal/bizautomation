﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class PriceLevel
    {
        public string PriceLevelType { get; set; }
        public string AmountType { get; set; }
        public IQueryable<PriceLevelBreakUp> PriceLevelBreakUp { get; set; }
    }
}