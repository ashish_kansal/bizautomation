﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models.Items
{
    public class ItemList
    {
        public IQueryable<Item> Items { get; set; }
        public int TotalRecords { get; set; }
    }
}