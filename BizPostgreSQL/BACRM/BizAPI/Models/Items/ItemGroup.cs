﻿using System;

namespace BizAPI.Models.Items
{
    public class ItemGroup
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
}