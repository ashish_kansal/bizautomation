﻿using System;
using System.Linq;


namespace BizAPI.Models.Items
{
    public class Item
    {
        public int ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string ItemImageURL { get; set; }
        public string ItemImageThumbURL { get; set; }

        public char ItemTypeID { get; set; }
        public string ItemType { get; set; }

        public string SKU { get; set; }
        public string ModelID { get; set; }
        public string BarCodeID { get; set; }
        public string Manufacturer { get; set; }
        public string UnitofMeasure { get; set; }
        
        public Double ListPrice { get; set; }
        public Decimal AverageCost { get; set; }
        public Decimal CampaignLabourCost { get; set; }

        public int ItemGroupID { get; set; }
        public int ItemClassID { get; set; }
        public int ItemShippingClassID { get; set; }
        public int ItemClassificationID { get; set; }

        public Double Width { get; set; }
        public Double Weight { get; set; }
        public Double Height { get; set; }
        public Double Length { get; set; }

        public int BaseUnitID { get; set; }
        public int SaleUnitID { get; set; }
        public int PurchaseUnitID { get; set; }

        public bool IsKit { get; set; }
        public bool IsLot { get; set; }
        public bool IsAssembly { get; set; }
        public bool IsSerialized { get; set; }
        public bool IsTaxable { get; set; }
        public bool AllowBackOrder { get; set; }
        public bool IsFreeShipping { get; set; }

        public int COGSChartAccountID { get; set; }
        public int AssetChartAccountID { get; set; }
        public int IncomeChartAccountID { get; set; }

        public long VendorID { get; set; }
        public Double OnHand { get; set; }
        public Double OnOrder { get; set; }
        public Double OnReOrder { get; set; }
        public Double OnAllocation { get; set; }
        public Double OnBackOrder { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

        public IQueryable<Warehouse> Warehouses { get; set; }
        public PriceLevel PriceLevel { get; set; }

        public IQueryable<SubItem> SubItems { get; set; }
        public IQueryable<CustomField> CustomFields { get; set; }
        public string Categories { get; set; }
    }
}