﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models.Items
{
    public class SubItem
    {
        public int ItemCode { get; set; }
        public int Quantity { get; set; }
        public int WarehouseID { get; set; }
        public string WarehouseName { get; set; }
        public string UnitOfMeasure { get; set; }
    }
}