﻿using System;

namespace BizAPI.Models.Items
{
    public class ItemClass
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
}