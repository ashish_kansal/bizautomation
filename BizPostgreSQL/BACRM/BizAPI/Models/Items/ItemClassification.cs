﻿using System;

namespace BizAPI.Models.Items
{
    public class ItemClassification
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
}