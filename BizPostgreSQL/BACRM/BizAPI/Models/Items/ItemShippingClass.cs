﻿using System;

namespace BizAPI.Models.Items
{
    public class ItemShippingClass
    {
        public long ID { get; set; }
        public string Name { get; set; }
    }
}