﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models.Items
{
    public class CustomField
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string ValueID { get; set; }
        public string Value { get; set; }
    }
}