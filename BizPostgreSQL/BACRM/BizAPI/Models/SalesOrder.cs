﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class SalesOrder
    {
        public Customer Customer { get; set; }
        public Contact Contact { get; set; }
        public bool IsNewCustomer { get; set; }
        public NewCustomer NewCustomer { get; set; }
        public List<SalesOrderItem> Items { get; set; }
        public long TimeZoneOffset { get; set; }
        public string TimeStamp { get; set; }
    }
}