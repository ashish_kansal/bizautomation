﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class Warehouse
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public Double ListPrice { get; set; }
        public Double OnHand { get; set; }
        public Double OnOrder { get; set; }
        public Double OnReOrder { get; set; }
        public Double OnBackOrder { get; set; }
        public Double OnAllocation { get; set; }
    }
}