﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class BizAPIBusinessException
    {
        public string ErrorCode { get; set; }
        public string Error { get; set; }

        public BizAPIBusinessException(string errorCode, string errorDescription)
        {
            ErrorCode = errorCode;
            Error = errorDescription;
        }
    }
}