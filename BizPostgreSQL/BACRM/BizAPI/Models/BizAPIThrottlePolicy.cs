﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class BizAPIThrottlePolicy
    {
        public int ID { get; set; }
        public string RateLimitKey { get; set; }
        public bool IsIPAddress { get; set; }
        public long PerSecond { get; set; }
        public long PerMinute { get; set; }
        public long PerHour { get; set; }
        public long PerDay { get; set; }
        public long PerWeek { get; set; }
    }
}