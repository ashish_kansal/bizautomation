﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public IQueryable<Contact> Contacts { get; set; }
    }
}