﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class SalesOrderDetailList
    {
        public IQueryable<SalesOrderDetail> SalesOrders { get; set; }
        public int TotalRecords { get; set; }
    }
}