﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class SalesOrderDetailItem
    {
        public int ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public string ModelID { get; set; }
        public string Manufacturer { get; set; }
        public string UPC { get; set; }
        public string SKU { get; set; }
        public string Description { get; set; }
        public string Warehouse { get; set; }
        public int Quantity { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}