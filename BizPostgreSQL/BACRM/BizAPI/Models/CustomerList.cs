﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BizAPI.Models
{
    public class CustomerList
    {
        public IQueryable<Customer> Customers { get; set; }
        public int TotalRecords { get; set; }
    }
}