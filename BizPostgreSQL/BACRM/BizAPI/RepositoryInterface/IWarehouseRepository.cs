﻿using BizAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizAPI.RepositoryInterface
{
    public interface IWarehouseRepository
    {
        IQueryable<Warehouse> Get(int itemCode);
    }
}
