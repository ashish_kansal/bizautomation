﻿using BizAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizAPI.RepositoryInterface
{
    public interface IBizAPIRepository
    {
        int Log(Exception ex);
        DataSet GetUser(string publicKey);
        int ValidateSalesOrder(long domainID, SalesOrder salesOrder);
        List<BizAPIThrottlePolicy> Get(string bizAPIAccessKey, string ipAddress);
        SalesOrderDetailList Get(DateTime fromDate, DateTime? toDate, int currentPage, int pageSize);
        CustomerList GetCustomer(string searchText, int pageIndex, int pageSize);
    }
}
