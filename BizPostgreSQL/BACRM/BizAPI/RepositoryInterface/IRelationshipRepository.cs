﻿using BizAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizAPI.RepositoryInterface
{
    public interface IRelationshipRepository
    {
        IQueryable<Relationship> GetAll(long domainID);
    }
}
