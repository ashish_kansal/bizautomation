﻿using BizAPI.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizAPI.RepositoryInterface
{
    public interface IItemClassRepository
    {
        IQueryable<ItemClass> GetAll(long domainID);
    }
}
