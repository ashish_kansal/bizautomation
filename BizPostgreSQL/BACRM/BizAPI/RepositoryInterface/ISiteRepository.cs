﻿using BizAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizAPI.RepositoryInterface
{
    public interface ISiteRepository
    {
        IQueryable<SiteCategory> GetCategories(long domainID, long siteID); 
    }
}
