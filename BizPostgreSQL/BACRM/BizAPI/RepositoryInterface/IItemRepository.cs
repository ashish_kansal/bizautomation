﻿using BizAPI.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizAPI.RepositoryInterface
{
    public interface IItemRepository
    {
        ItemList GetAll(long domainID, int pageIndex, int pageSize, DateTime? createdAfter, DateTime? modifiedAfter);
        ItemList Search(long domainID, string searchText, int pageIndex, int pageSize, DateTime? createdAfter, DateTime? modifiedAfter);
    }
}
