﻿using BizAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizAPI.RepositoryInterface
{
    public interface ISalesOrderRepository
    {
        BizAPIBusinessException Add(long domainID, SalesOrder salesOrder);
    }
}
