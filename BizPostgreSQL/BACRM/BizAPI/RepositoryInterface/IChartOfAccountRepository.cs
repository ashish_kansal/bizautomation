﻿using BizAPI.Models.Accounting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BizAPI.RepositoryInterface
{
    public interface IChartOfAccountRepository
    {
        IQueryable<ChartOfAccount> GetCOGSChartAccount(long domainID);
        IQueryable<ChartOfAccount> GetAssetChartAccount(long domainID);
        IQueryable<ChartOfAccount> GetIncomeChartAccount(long domainID);
    }
}
