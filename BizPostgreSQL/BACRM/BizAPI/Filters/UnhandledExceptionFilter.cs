﻿using BACRM.BusinessLogic.Common;
using BizAPI.Content;
using BizAPI.Models;
using BizAPI.Repository;
using BizAPI.RepositoryInterface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace BizAPI.Filters
{
    public class UnhandledExceptionFilter : ExceptionFilterAttribute
    {
        #region Member Variables

        private IBizAPIRepository _iBizAPIRepository;

        #endregion

        public override void OnException(HttpActionExecutedContext context)
        {
            try
            {
                //Logs exception details to database
                _iBizAPIRepository = new BizAPIRepository();
                int errorID = _iBizAPIRepository.Log(context.Exception);

                //Sends exception email
                //TODO: UnComment following lines when deploying 
                //Email objEmail = new Email();
                //objEmail.SendSystemEmail("This is test Email Please ignore it. Unhandled exception occured in Biz API", "Please find error details with ID: " + errorID.ToString() + " in database table BizAPIErrorLog to perform appropriate action to resolve issue.", ConfigurationManager.AppSettings["SendExceptionMailCC"], "noreply@bizautomation.com", ConfigurationManager.AppSettings["SendExceptionMailTo"], "BizAPI", "");
            }
            catch
            {
                throw;
                //Do not throw error 
            }
            BizAPIBusinessException exception = new BizAPIBusinessException("Unknown Error", "An error occurred, please try again or contact the administrator.");
            context.Response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, JsonConvert.SerializeObject(exception));
        }
    }
}