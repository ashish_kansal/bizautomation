using BizAPI.Repository;
using BizAPI.RepositoryInterface;
using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;
using WebApiThrottle;

namespace BizAPI
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();


            container.RegisterType<IUOMRepository, UOMRepository>();
            container.RegisterType<IItemRepository, ItemRepository>();
            container.RegisterType<ISiteRepository, SiteRepository>();
            container.RegisterType<IStateRepository, StateRepository>();
            container.RegisterType<IBizAPIRepository, BizAPIRepository>();
            container.RegisterType<ICountryRepository, CountryRepository>();
            container.RegisterType<IContactRepository, ContactRepository>();
            container.RegisterType<IWarehouseRepository, WarehouseRepository>();
            container.RegisterType<IItemClassRepository, ItemClassRepository>();
            container.RegisterType<IItemGroupRepository, ItemGroupRepository>();
            container.RegisterType<ISalesOrderRepository, SalesOrderRepository>();
            container.RegisterType<IRelationshipRepository, RelationshipRepository>();
            container.RegisterType<IChartOfAccountRepository, ChartOfAccountRepository>();
            container.RegisterType<IItemShippingClassRepository, ItemShippingClassRepository>();
            container.RegisterType<IItemClassificationRepository, ItemClassificationRepository>();
            

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}