﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class RelationshipRepository : IRelationshipRepository
    {
        #region Public Methods
        
        public IQueryable<Relationship> GetAll(long domainID)
        {
            return GetRelationshipList(domainID);
        }

        #endregion

        #region Private Methods

        public IQueryable<Relationship> GetRelationshipList(long domainID)
        {
            List<Relationship> listRelationship = new List<Relationship>();

            CCommon objCommon = new CCommon();
            DataTable dtRelationship = objCommon.GetMasterListItems(5, domainID);

            if (dtRelationship != null && dtRelationship.Rows.Count > 0)
            {
                listRelationship = dtRelationship.AsEnumerable().Select(row => new Relationship
                                  {
                                      ID = CCommon.ToInteger(row["numListItemID"]),
                                      Name = CCommon.ToString(row["vcData"]),
                                  }).ToList();
            }

            return listRelationship.AsQueryable();
        }

        #endregion
    }
}