﻿using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Opportunities;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class ContactRepository : IContactRepository
    {
        #region Public Methods

        public IQueryable<Contact> Get(int customerID)
        {
            try
            {
                return GetContact(customerID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private IQueryable<Contact> GetContact(int customerID)
        {
            List<Contact> listContact = new List<Contact>();

            COpportunities objOpp = new COpportunities();
            objOpp.DivisionID = customerID;
            DataSet dsContact = objOpp.ListContact();

            if(dsContact != null && dsContact.Tables.Count > 0 && dsContact.Tables[0].Rows.Count > 0)
            {
                listContact = dsContact.Tables[0].AsEnumerable().Select(row => new Contact
                                  {
                                      ID = row["numContactId"] == DBNull.Value ? 0 : CCommon.ToInteger(row["numContactId"]),
                                      Name = row["Name"] == DBNull.Value ? string.Empty : CCommon.ToString(row["Name"]),
                                      FirstName = row["vcFirstName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcFirstName"]),
                                      LastName = row["vcLastName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcLastName"]),
                                      PhoneExt = row["numPhoneExtension"] == DBNull.Value ? string.Empty : CCommon.ToString(row["numPhoneExtension"]),
                                      Phone = row["numPhone"] == DBNull.Value ? string.Empty : CCommon.ToString(row["numPhone"]),
                                      Email = row["vcEmail"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcEmail"]),
                                      Fax = row["vcFax"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcFax"]),
                                      IsPrimaryContact = row["bitPrimaryContact"] == DBNull.Value ? false : CCommon.ToBool(CCommon.ToInteger(row["bitPrimaryContact"])),
                                      ContactType = row["vcContactTypeName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcContactTypeName"]),
                                      Department = row["vcDepartmentName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcDepartmentName"]),
                                      Position = row["vcPositionName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcPositionName"])
                                  }).ToList();
            }

            return listContact.AsQueryable();
        }

        #endregion
    }
}