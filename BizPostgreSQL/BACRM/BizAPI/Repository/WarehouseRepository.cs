﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class WarehouseRepository : IWarehouseRepository
    {
        #region Public Methods

        public IQueryable<Warehouse> Get(int itemCode)
        {
            try
            {
                return GetItemWarehouse(itemCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private IQueryable<Warehouse> GetItemWarehouse(int itemCode)
        {
            List<Warehouse> listWarehouse = new List<Warehouse>();
            CCommon objCommon = new CCommon();
            DataTable dtWarehouse = objCommon.GetWarehouseAttrBasedItem(itemCode);

            if (dtWarehouse != null && dtWarehouse.Rows.Count > 0)
            {
                listWarehouse = dtWarehouse.AsEnumerable().Select(row => new Warehouse
                {
                    ID = CCommon.ToLong(row["numWareHouseItemId"]),
                    Name = CCommon.ToString(row["vcWareHouse"]),
                    ListPrice = CCommon.ToDouble(row["monWListPrice"]),
                    OnHand = CCommon.ToDouble(row["numOnHand"]),
                    OnOrder = CCommon.ToDouble(row["numOnOrder"]),
                    OnReOrder = CCommon.ToDouble(row["numReorder"]),
                    OnBackOrder = CCommon.ToDouble(row["numBackOrder"]),
                    OnAllocation = CCommon.ToDouble(row["numAllocation"])
                }).ToList();
            }

            return listWarehouse.AsQueryable();
        }

        #endregion
    }
}