﻿using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Common;
using BizAPI.Models.Accounting;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class ChartOfAccountRepository : IChartOfAccountRepository
    {
        #region Public Methods

        public IQueryable<ChartOfAccount> GetCOGSChartAccount(long domainID)
        {
            try
            {
                return GetChartOfAccountByCode((int)domainID, "0106");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IQueryable<ChartOfAccount> GetAssetChartAccount(long domainID)
        {
            try
            {
                return GetChartOfAccountByCode((int)domainID, "0101");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public IQueryable<ChartOfAccount> GetIncomeChartAccount(long domainID)
        {
            try
            {
                return GetChartOfAccountByCode((int)domainID, "0103");
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private IQueryable<ChartOfAccount> GetChartOfAccountByCode(int domainD, string accountCode)
        {
            List<ChartOfAccount> listChartOfAccount = new List<ChartOfAccount>();

            ChartOfAccounting chartOfAccounting = new ChartOfAccounting();
            chartOfAccounting.DomainID = domainD;
            chartOfAccounting.AccountCode = accountCode;
            DataTable dt = chartOfAccounting.GetParentCategory();

            if (dt != null && dt.Rows.Count > 0)
            {
                listChartOfAccount = dt.AsEnumerable()
                                    .Select(row => new ChartOfAccount
                                    {
                                        AccountID = CCommon.ToInteger(row["numAccountID"]),
                                        AccountName = CCommon.ToString(row["vcAccountName"]),
                                        AccountType = CCommon.ToString(row["vcAccountType"])
                                    }).ToList();
            }

            return listChartOfAccount.AsQueryable();
        }

        #endregion
    }
}