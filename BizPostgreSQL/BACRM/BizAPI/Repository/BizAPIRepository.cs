﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using WebApiThrottle;

namespace BizAPI.Repository
{
    public class BizAPIRepository : IBizAPIRepository
    {
        #region Public Methods

        public int Log(Exception ex)
        {
            int errorID = 0;


            using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
            {
                Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = "usp_bizapierrorlog_insert";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.Add(new Npgsql.NpgsqlParameter() {ParameterName = "v_type", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = ex.GetType().Name});
                cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_source", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = ex.Source });
                cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_message", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, Value = ex.Message });
                cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_stackstrace", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, Value = ex.StackTrace });
                cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_domainid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, Value = HttpContext.Current.Request.Headers["DomainID"] });

                connection.Open();
                errorID = CCommon.ToInteger(cmd.ExecuteScalar());
                connection.Close();
            }


            return errorID;
        }

        public void SaveThrottleLog(ThrottleLogEntry entry)
        {
            try
            {
                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_bizapithrottlelog_save";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcip", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = entry.ClientIp });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcbizapiaccesskey", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = entry.ClientKey });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcendpoint", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, Value = entry.Endpoint });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_dtlogdate", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp, Value = entry.LogDate });

                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetUser(string publicKey)
        {
            DataSet dsUser = null;

            try
            {
                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_bizapi_getsecretkey";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_bizapipublickey", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = publicKey });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() {ParameterName = "swv_refcur", Direction = ParameterDirection.InputOutput, NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, Value = DBNull.Value});

                    Int32 i = 0;
                    dsUser = new DataSet();
                    Npgsql.NpgsqlDataAdapter sda = new Npgsql.NpgsqlDataAdapter();
                    connection.Open();
                    using (Npgsql.NpgsqlTransaction objTransaction = connection.BeginTransaction())
                    {
                        cmd.ExecuteNonQuery();

                        foreach (Npgsql.NpgsqlParameter parm in cmd.Parameters)
                        {
                            if (parm.NpgsqlDbType == NpgsqlTypes.NpgsqlDbType.Refcursor)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(parm.Value)))
                                {
                                    string parm_val = string.Format("FETCH ALL IN \"{0}\"", parm.Value.ToString());
                                    sda = new Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection);
                                    dsUser.Tables.Add(parm.Value.ToString());
                                    sda.Fill(dsUser.Tables[i]);
                                    i += 1;
                                }
                            }
                        }

                        objTransaction.Commit();
                    }
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return dsUser;
        }

        public int ValidateSalesOrder(long domainID, SalesOrder salesOrder)
        {
            int errorCode = 0;

            try
            {
                Int32 i = 0;
                DataSet dsUser = new DataSet();
                Npgsql.NpgsqlDataAdapter sda = new Npgsql.NpgsqlDataAdapter();

                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_bizapi_validatesalesorder";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numdomainid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, Value = domainID });
                    if (!salesOrder.IsNewCustomer)
                    {
                        cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numcustomerid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, Value = salesOrder.Customer.ID });
                        cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numcontactid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, Value = salesOrder.Contact.ID });
                    }
                    else
                    {
                        cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numcustomerid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, Value = DBNull.Value });
                        cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numcontactid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, Value = DBNull.Value });                       
                    }
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcitems", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Text, Value = string.Join(", ", from item in salesOrder.Items select item.ItemCode) });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "swv_refcur", Direction = ParameterDirection.InputOutput, NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, Value = DBNull.Value });

                    connection.Open();
                    using (Npgsql.NpgsqlTransaction objTransaction = connection.BeginTransaction())
                    {
                        cmd.ExecuteNonQuery();

                        foreach (Npgsql.NpgsqlParameter parm in cmd.Parameters)
                        {
                            if (parm.NpgsqlDbType == NpgsqlTypes.NpgsqlDbType.Refcursor)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(parm.Value)))
                                {
                                    string parm_val = string.Format("FETCH ALL IN \"{0}\"", parm.Value.ToString());
                                    sda = new Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection);
                                    dsUser.Tables.Add(parm.Value.ToString());
                                    sda.Fill(dsUser.Tables[i]);
                                    i += 1;
                                }
                            }
                        }

                        objTransaction.Commit();
                    }
                    connection.Close();
                }

                if (dsUser != null && dsUser.Tables.Count > 0 && dsUser.Tables[0].Rows.Count > 0)
                {
                    errorCode = Convert.ToInt32(dsUser.Tables[0].Rows[0][0]);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return errorCode;
        }

        public List<BizAPIThrottlePolicy> Get(string bizAPIAccessKey, string ipAddress)
        {
            List<BizAPIThrottlePolicy> listThrottlePolicy = null;

            try
            {
                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_bizapithrottlepolicy_get";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_bizapipublickey", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = bizAPIAccessKey });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_ipaddress", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = ipAddress });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "swv_refcur", Direction = ParameterDirection.InputOutput, NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, Value = DBNull.Value });

                    Int32 i = 0;
                    DataSet ds = new DataSet();
                    Npgsql.NpgsqlDataAdapter sda = new Npgsql.NpgsqlDataAdapter();
                    connection.Open();
                    using (Npgsql.NpgsqlTransaction objTransaction = connection.BeginTransaction())
                    {
                        cmd.ExecuteNonQuery();

                        foreach (Npgsql.NpgsqlParameter parm in cmd.Parameters)
                        {
                            if (parm.NpgsqlDbType == NpgsqlTypes.NpgsqlDbType.Refcursor)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(parm.Value)))
                                {
                                    string parm_val = string.Format("FETCH ALL IN \"{0}\"", parm.Value.ToString());
                                    sda = new Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection);
                                    ds.Tables.Add(parm.Value.ToString());
                                    sda.Fill(ds.Tables[i]);
                                    i += 1;
                                }
                            }
                        }

                        objTransaction.Commit();
                    }
                    connection.Close();

                    GetBizAPIThrottlePolicyList(ds, ref listThrottlePolicy);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return listThrottlePolicy;
        }

        public CustomerList GetCustomer(string searchText, int pageIndex, int pageSize)
        {
            int totalRecords = 0;

            List<Customer> listCustomer = new List<Customer>();
            try
            {
                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_customer_get_bizapi";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numdomainid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, Value = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]) });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_searchtext", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = searchText });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numpageindex", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = pageIndex });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numpagesize", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = pageSize });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_totalrecords", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = 0 });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "swv_refcur", Direction = ParameterDirection.InputOutput, NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, Value = DBNull.Value });

                    Int32 i = 0;
                    DataSet ds = new DataSet();
                    Npgsql.NpgsqlDataAdapter sda = new Npgsql.NpgsqlDataAdapter();
                    connection.Open();
                    using (Npgsql.NpgsqlTransaction objTransaction = connection.BeginTransaction()) {
                        cmd.ExecuteNonQuery();

                        foreach (Npgsql.NpgsqlParameter parm in cmd.Parameters)
                        {
                            if (parm.NpgsqlDbType == NpgsqlTypes.NpgsqlDbType.Refcursor)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(parm.Value)))
                                {
                                    string parm_val = string.Format("FETCH ALL IN \"{0}\"", parm.Value.ToString());
                                    sda = new Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection);
                                    ds.Tables.Add(parm.Value.ToString());
                                    sda.Fill(ds.Tables[i]);
                                    i += 1;
                                }
                            }
                        }

                        objTransaction.Commit();
                    }
                    connection.Close();

                    totalRecords = CCommon.ToInteger(cmd.Parameters["v_totalrecords"].Value);
                    GerCustomerList(ds, ref listCustomer);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return new CustomerList() { Customers = listCustomer.AsQueryable(), TotalRecords = totalRecords }; 
        }

        public SalesOrderDetailList Get(DateTime fromDate, DateTime? toDate, int currentPage, int pageSize)
        {
            int totalRecords = 0;
            DataSet ds = null;
            int i = 0;
            List<SalesOrderDetail> listSalesOrderDetail = new List<SalesOrderDetail>();
            try
            {
                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_bizapi_getsalesorderdetail";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numdomainid", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, Value = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]) });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_dtfromdate", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp, Value = fromDate });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_dttodate", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp, Value = toDate });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numpageindex", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = currentPage });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_numpagesize", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value = pageSize });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_totalrecords", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Integer, Value =0 });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "swv_refcur", Direction = ParameterDirection.InputOutput, NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, Value = DBNull.Value });

                    connection.Open();
                    using (Npgsql.NpgsqlTransaction objTransaction = connection.BeginTransaction())
                    {
                        cmd.ExecuteNonQuery();
                        foreach (Npgsql.NpgsqlParameter parm in cmd.Parameters)
                        {
                            if (parm.NpgsqlDbType == NpgsqlTypes.NpgsqlDbType.Refcursor)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(parm.Value)))
                                {
                                    string parm_val = string.Format("FETCH ALL IN \"{0}\"", parm.Value.ToString());
                                    Npgsql.NpgsqlDataAdapter dataAdatpter = new Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection);
                                    ds.Tables.Add(parm.Value.ToString());
                                    dataAdatpter.Fill(ds.Tables[i]);
                                    i += 1;
                                }
                            }
                        }

                        objTransaction.Commit();
                    }
                    connection.Close();

                    totalRecords = CCommon.ToInteger(cmd.Parameters["v_totalrecords"].Value);
                    GetSalesOrderDetailList(ds, ref listSalesOrderDetail);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return new SalesOrderDetailList() { SalesOrders = listSalesOrderDetail.AsQueryable(), TotalRecords = totalRecords }; 
        }

        #endregion

        #region Private Methods

        private void GetBizAPIThrottlePolicyList(DataSet ds, ref List<BizAPIThrottlePolicy> listBizAPIThrottlePolicy)
        {
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                listBizAPIThrottlePolicy = ds.Tables[0].AsEnumerable().Select(row => new BizAPIThrottlePolicy
                {
                    ID = CCommon.ToInteger(row["ID"]),
                    RateLimitKey = CCommon.ToString(row["RateLimitKey"]),
                    IsIPAddress = CCommon.ToBool(row["bitIsIPAddress"]),
                    PerSecond = CCommon.ToLong(row["numPerSecond"]),
                    PerMinute = CCommon.ToLong(row["numPerMinute"]),
                    PerHour = CCommon.ToLong(row["numPerHour"]),
                    PerDay = CCommon.ToLong(row["numPerDay"]),
                    PerWeek = CCommon.ToLong(row["numPerWeek"])
                }).ToList();
            }
        }

        private void GetSalesOrderDetailList(DataSet ds, ref List<SalesOrderDetail> listSalesOrderDetail)
        {
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                listSalesOrderDetail = ds.Tables[0].AsEnumerable().Select(row => new SalesOrderDetail
                {
                    ID = row["numOppId"] == DBNull.Value ? 0 : CCommon.ToLong(row["numOppId"]),
                    SalesOrderName = row["vcPOppName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcPOppName"]),
                    Campaign = row["vcCampaignName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcCampaignName"]),
                    Customer = row["numCompanyId"] == DBNull.Value ? null : new Customer() { ID = CCommon.ToInteger(row["numCompanyId"]), Name = CCommon.ToString(row["vcCompanyName"]) },
                    Contact = row["numContactId"] == DBNull.Value ? null : new Contact() { ID = CCommon.ToInteger(row["numContactId"]), Name = CCommon.ToString(row["numContactIdName"]) },
                    Source = row["tintSourceName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["tintSourceName"]),
                    BillingAddress = row["BillingAddress"] == DBNull.Value ? string.Empty : CCommon.ToString(row["BillingAddress"]),
                    ShippingAddress = row["ShippingAddress"] == DBNull.Value ? string.Empty : CCommon.ToString(row["ShippingAddress"]),
                    Status = row["numStatusName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["numStatusName"]),
                    TotalAmount = row["CalAmount"] == DBNull.Value ? 0 : CCommon.ToDecimal(row["CalAmount"]),
                    PaidAmount = row["monPAmount"] == DBNull.Value ? 0 : CCommon.ToDecimal(row["monPAmount"]),
                    IsOrderClosed = row["tintshipped"] == DBNull.Value ? false : CCommon.ToBool(CCommon.ToInteger(row["tintshipped"])),
                    CreatedDate = row["bintCreatedDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["bintCreatedDate"]),
                    ClosedDate = row["bintClosedDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["bintClosedDate"]),
                    AssignedTo = row["numAssignedToName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["numAssignedToName"]),
                    Currency = row["chrCurrency"] == DBNull.Value ? string.Empty : CCommon.ToString(row["chrCurrency"]),
                    CurrencySymbol = row["varCurrSymbol"] == DBNull.Value ? string.Empty : CCommon.ToString(row["varCurrSymbol"]),
                    ExchangeRate = row["fltExchangeRate"] == DBNull.Value ? 0 : CCommon.ToDecimal(row["fltExchangeRate"]),
                    SalesOrderItems = GetItems(CCommon.ToString(row["Items"]))
                }).ToList();
            }
        }

        private void GerCustomerList(DataSet ds, ref List<Customer> listCustomer)
        {
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                listCustomer = ds.Tables[0].AsEnumerable().Select(row => new Customer
                {
                    ID = row["numDivisionID"] == DBNull.Value ? 0 : CCommon.ToInteger(row["numDivisionID"]),
                    Name = row["vcCompanyName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcCompanyName"]),
                    Contacts = GetContacts(row["Contacts"] == DBNull.Value ? string.Empty : CCommon.ToString(row["Contacts"]))
                }).ToList();
            }
        }

        private IQueryable<Contact> GetContacts(string Contacts)
        {
            List<Contact> listContacts = new List<Contact>();

            if (!string.IsNullOrEmpty(Contacts))
            {
                XDocument xdoc = XDocument.Parse("<Contacts>" + Contacts + "</Contacts>", LoadOptions.None);

                listContacts = (from xml in xdoc.Elements().Descendants()
                                      select new Contact
                                      {
                                          ID = xml.Attribute("numContactId") == null ? 0 : CCommon.ToInteger(xml.Attribute("numContactId").Value),
                                          Name = xml.Attribute("Name") == null ? string.Empty : CCommon.ToString(xml.Attribute("Name").Value),
                                          FirstName = xml.Attribute("vcFirstName") == null ? string.Empty : CCommon.ToString(xml.Attribute("vcFirstName").Value),
                                          LastName = xml.Attribute("vcLastName") == null ? string.Empty : CCommon.ToString(xml.Attribute("vcLastName").Value),
                                          PhoneExt = xml.Attribute("numPhoneExtension") == null ? string.Empty : CCommon.ToString(xml.Attribute("numPhoneExtension").Value),
                                          Phone = xml.Attribute("numPhone") == null ? string.Empty : CCommon.ToString(xml.Attribute("numPhone").Value),
                                          Email = xml.Attribute("vcEmail") == null ? string.Empty : CCommon.ToString(xml.Attribute("vcEmail").Value),
                                          Fax = xml.Attribute("vcFax") == null ? string.Empty : CCommon.ToString(xml.Attribute("vcFax").Value),
                                          IsPrimaryContact = xml.Attribute("bitPrimaryContact") == null ? false : CCommon.ToBool(CCommon.ToInteger(xml.Attribute("bitPrimaryContact").Value)),
                                          ContactType = xml.Attribute("numContactType") == null ? string.Empty : CCommon.ToString(xml.Attribute("numContactType").Value),
                                          Department = xml.Attribute("vcDepartment") == null ? string.Empty : CCommon.ToString(xml.Attribute("vcDepartment").Value),
                                          Position = xml.Attribute("vcPosition") == null ? string.Empty : CCommon.ToString(xml.Attribute("vcPosition").Value)
                                      }).ToList();
            }

            return listContacts.AsQueryable();
        }

        private IQueryable<SalesOrderDetailItem> GetItems(string Items)
        {
            List<SalesOrderDetailItem> listSalesOrderItem = new List<SalesOrderDetailItem>();

            if (!string.IsNullOrEmpty(Items))
            {
                XDocument xdoc = XDocument.Parse("<Items>" + Items + "</Items>", LoadOptions.None);

                listSalesOrderItem = (from xml in xdoc.Elements().Descendants()
                                      select new SalesOrderDetailItem
                                      {
                                          ItemCode = xml.Attribute("ItemCode") == null ? 0 : CCommon.ToInteger(xml.Attribute("ItemCode").Value),
                                          ItemName = xml.Attribute("ItemName") == null ? string.Empty : CCommon.ToString(xml.Attribute("ItemName").Value),
                                          ItemType = xml.Attribute("ItemType") == null ? string.Empty : CCommon.ToString(xml.Attribute("ItemType").Value),
                                          ModelID = xml.Attribute("ModelID") == null ? string.Empty : CCommon.ToString(xml.Attribute("ModelID").Value),
                                          Manufacturer = xml.Attribute("Manufacturer") == null ? string.Empty : CCommon.ToString(xml.Attribute("Manufacturer").Value),
                                          UPC = xml.Attribute("UPC") == null ? string.Empty : CCommon.ToString(xml.Attribute("UPC").Value),
                                          SKU = xml.Attribute("SKU") == null ? string.Empty : CCommon.ToString(xml.Attribute("SKU").Value),
                                          Description = xml.Attribute("Description") == null ? string.Empty : CCommon.ToString(xml.Attribute("Description").Value),
                                          Warehouse = xml.Attribute("Warehouse") == null ? string.Empty : CCommon.ToString(xml.Attribute("Warehouse").Value),
                                          Quantity = xml.Attribute("Quantity") == null ? 0 : Convert.ToInt32(double.Parse(xml.Attribute("Quantity").Value)),
                                          UnitPrice = xml.Attribute("UnitPrice") == null ? 0 : CCommon.ToDecimal(xml.Attribute("UnitPrice").Value),
                                          TotalPrice = xml.Attribute("TotalAmount") == null ? 0 : CCommon.ToDecimal(xml.Attribute("TotalAmount").Value)
                                      }).ToList();
            }

            return listSalesOrderItem.AsQueryable();
        }

        #endregion
    }
}