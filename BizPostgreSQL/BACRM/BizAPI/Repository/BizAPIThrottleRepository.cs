﻿using BACRM.BusinessLogic.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApiThrottle;

namespace BizAPI.Repository
{
    public class BizAPIThrottleRepository : IThrottleRepository
    {
        [Serializable]
        internal struct ThrottleCounterWrapper
        {
            public DateTime Timestamp { get; set; }
            public long TotalRequests { get; set; }
            public TimeSpan ExpirationTime { get; set; }
        }

        public bool Any(string id)
        {
            int count = 0;
            bool isExist = false;

            try
            {
                Int32 i = 0;
                DataSet dsUser = new DataSet();
                Npgsql.NpgsqlDataAdapter sda = new Npgsql.NpgsqlDataAdapter();

                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_bizapithrottlecounter_check";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = id });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "swv_refcur", Direction = ParameterDirection.InputOutput, NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, Value = DBNull.Value });

                    connection.Open();
                    using (Npgsql.NpgsqlTransaction objTransaction = connection.BeginTransaction())
                    {
                        cmd.ExecuteNonQuery();

                        foreach (Npgsql.NpgsqlParameter parm in cmd.Parameters)
                        {
                            if (parm.NpgsqlDbType == NpgsqlTypes.NpgsqlDbType.Refcursor)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(parm.Value)))
                                {
                                    string parm_val = string.Format("FETCH ALL IN \"{0}\"", parm.Value.ToString());
                                    sda = new Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection);
                                    dsUser.Tables.Add(parm.Value.ToString());
                                    sda.Fill(dsUser.Tables[i]);
                                    i += 1;
                                }
                            }
                        }

                        objTransaction.Commit();
                    }

                    connection.Close();
                }

                if (dsUser != null && dsUser.Tables.Count > 0 && dsUser.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(dsUser.Tables[0].Rows[0][0]);
                }

                isExist = count > 0 ? true : false;
            }
            catch (Exception)
            {
                throw;
            }

            return isExist;
        }

        /// <summary>
        /// Insert or update
        /// </summary>
        public ThrottleCounter? FirstOrDefault(string id)
        {
            var entry = new ThrottleCounterWrapper();

            if (Get(id, ref entry))
            {
                //remove expired entry
                if (entry.Timestamp + entry.ExpirationTime < DateTime.UtcNow)
                {
                    try
                    {
                        using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                        {
                            Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                            cmd.Connection = connection;
                            cmd.CommandText = "usp_bizapithrottlecounter_delete";
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = id });

                            connection.Open();
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                    return null;
                }
            }

            return new ThrottleCounter
            {
                Timestamp = entry.Timestamp,
                TotalRequests = entry.TotalRequests
            };
        }

        /// <summary>
        /// Insert or update
        /// </summary>
        public void Save(string id, ThrottleCounter throttleCounter, TimeSpan expirationTime)
        {
            try
            {
                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    var array = id.Split(':');
                    var apiKey = array[0];
                    var vcID = array[1];

                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_bizapithrottlecounter_save";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = vcID });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_timespan", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Timestamp, Value = throttleCounter.Timestamp });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_totalrequest", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Numeric, Value = throttleCounter.TotalRequests });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_expirationtime", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Bigint, Value = expirationTime.Ticks });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_vcbizapiacesskey", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = apiKey });

                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Remove(string id)
        {
            try
            {
                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_bizapithrottlecounter_delete";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = id });

                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Clear()
        {
            try
            {
                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_bizapithrottlecounter_delete";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = "DeleteAll" });

                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool Get(string id, ref ThrottleCounterWrapper throttleCounterWrapper)
        {
            bool isExist = false;

            try
            {
                Int32 i = 0;
                DataSet ds = new DataSet();
                Npgsql.NpgsqlDataAdapter sda = new Npgsql.NpgsqlDataAdapter();

                using (Npgsql.NpgsqlConnection connection = new Npgsql.NpgsqlConnection(ConfigurationManager.AppSettings["ConnectionString"]))
                {
                    Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
                    cmd.Connection = connection;
                    cmd.CommandText = "usp_bizapithrottlecounter_get";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "v_id", NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Varchar, Value = id });
                    cmd.Parameters.Add(new Npgsql.NpgsqlParameter() { ParameterName = "swv_refcur", Direction = ParameterDirection.InputOutput, NpgsqlDbType = NpgsqlTypes.NpgsqlDbType.Refcursor, Value = DBNull.Value });

                    connection.Open();
                    using (Npgsql.NpgsqlTransaction objTransaction = connection.BeginTransaction())
                    {
                        cmd.ExecuteNonQuery();

                        foreach (Npgsql.NpgsqlParameter parm in cmd.Parameters)
                        {
                            if (parm.NpgsqlDbType == NpgsqlTypes.NpgsqlDbType.Refcursor)
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(parm.Value)))
                                {
                                    string parm_val = string.Format("FETCH ALL IN \"{0}\"", parm.Value.ToString());
                                    sda = new Npgsql.NpgsqlDataAdapter(parm_val.Trim().ToString(), connection);
                                    ds.Tables.Add(parm.Value.ToString());
                                    sda.Fill(ds.Tables[i]);
                                    i += 1;
                                }
                            }
                        }

                        objTransaction.Commit();
                    }
                    connection.Close();
                }

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    isExist = true;

                    throttleCounterWrapper = ds.Tables[0].AsEnumerable().Take(1).Select(row => new ThrottleCounterWrapper
                    {
                        Timestamp = Convert.ToDateTime(row["dtTimeSpan"]),
                        TotalRequests = CCommon.ToLong(row["numTotalRequest"]),
                        ExpirationTime = TimeSpan.FromTicks(CCommon.ToLong(row["bintExpirationTime"]))
                    }).First();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return isExist;
        }
    }
}