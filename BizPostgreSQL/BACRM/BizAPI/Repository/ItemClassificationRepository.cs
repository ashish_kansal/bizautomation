﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models.Items;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class ItemClassificationRepository : IItemClassificationRepository
    {
        #region Public Methods

        public IQueryable<ItemClassification> GetAll(long domainID)
        {
            try
            {
                return GetItemClassificationList(domainID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private IQueryable<ItemClassification> GetItemClassificationList(long domainID)
        {
            List<ItemClassification> listItemClassification = new List<ItemClassification>();

            CCommon objCommon = new CCommon();
            DataTable dtItemClassification = objCommon.GetMasterListItems(36, domainID);

            if (dtItemClassification != null && dtItemClassification.Rows.Count > 0)
            {
                listItemClassification = dtItemClassification.AsEnumerable()
                                  .Select(row => new ItemClassification
                                  {
                                      ID = CCommon.ToInteger(row["numListItemID"]),
                                      Name = CCommon.ToString(row["vcData"]),
                                  }).ToList();
            }

            return listItemClassification.AsQueryable();
        }

        #endregion

    }
}