﻿using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class StateRepository : IStateRepository
    {
        #region Public Methods

        public IQueryable<State> GetAll(int countryID, long domainID)
        {
            return GetStateList(countryID, domainID);
        }

        #endregion

        #region Private Methods

        private IQueryable<State> GetStateList(int countryID, long domainID)
        {
            List<State> listState = new List<State>();

            UserAccess objUserAccess = new UserAccess();
            objUserAccess.DomainID = domainID;
            objUserAccess.Country = countryID;
            DataTable dtState = objUserAccess.SelState();

            if (dtState != null && dtState.Rows.Count > 0)
            {
                listState = dtState.AsEnumerable().Select(row => new State
                {
                    ID = CCommon.ToInteger(row["numStateID"]),
                    Name = CCommon.ToString(row["vcState"]),
                }).ToList();
            }

            return listState.AsQueryable();
        }

        #endregion
    }
}