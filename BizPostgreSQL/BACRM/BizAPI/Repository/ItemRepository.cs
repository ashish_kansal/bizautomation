﻿using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using BizAPI.Models.Items;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace BizAPI.Repository
{
    public class ItemRepository : IItemRepository
    {
        #region Member Variables

        private string path = ConfigurationManager.AppSettings["PortalVirtualDirectoryName"];

        #endregion

        #region Public Methods

        public ItemList GetAll(long domainID, int pageIndex, int pageSize, DateTime? createdAfter, DateTime? modifiedAfter)
        {
            try
            {
                return GetItemList(domainID, pageIndex, pageSize, createdAfter, modifiedAfter);
            }
            catch
            {
                throw;
            }
        }

        public ItemList Search(long domainID, string searchText, int pageIndex, int pageSize, DateTime? createdAfter, DateTime? modifiedAfter)
        {
            try
            {
                return SearchItem(domainID, searchText, pageIndex, pageSize, createdAfter, modifiedAfter);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private ItemList GetItemList(long domainID, int pageIndex, int pageSize, DateTime? createdAfter, DateTime? modifiedAfter)
        {
            try
            {
                int totalRecords = 0;
                List<Item> listItems = new List<Item>();

                WarehouseRepository warehouseRepository = new WarehouseRepository();
                PriceLevelRepository priceLevelRepository = new PriceLevelRepository();

                CItems objItem = new CItems();
                objItem.DomainID = domainID;
                objItem.CurrentPage = CCommon.ToInteger(pageIndex);
                objItem.PageSize = CCommon.ToInteger(pageSize);
                objItem.CreatedAfter = createdAfter;
                objItem.ModifiedAfter = modifiedAfter;
                DataSet ds = objItem.GetItemsBizAPI(ref totalRecords);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    listItems = ds.Tables[0].AsEnumerable().Select(row => new Item
                    {
                        ItemCode = CCommon.ToInteger(row["numItemCode"]),
                        ItemName = CCommon.ToString(row["vcItemName"]),
                        ItemImageURL = string.IsNullOrEmpty(CCommon.ToString(row["vcPathForImage"])) ? "" : path + domainID + "/" + CCommon.ToString(row["vcPathForImage"]),
                        ItemImageThumbURL = string.IsNullOrEmpty(CCommon.ToString(row["vcPathForTImage"])) ? "" : path + domainID + "/" + CCommon.ToString(row["vcPathForTImage"]),
                        ItemDescription = CCommon.ToString(row["txtItemDesc"]),
                        ItemTypeID = Convert.ToChar(row["charItemType"]),
                        ItemType = CCommon.ToString(row["ItemType"]),
                        SKU = CCommon.ToString(row["vcSKU"]),
                        ModelID = CCommon.ToString(row["vcModelID"]),
                        BarCodeID = CCommon.ToString(row["numBarCodeId"]),
                        Manufacturer = CCommon.ToString(row["vcManufacturer"]),
                        UnitofMeasure = CCommon.ToString(row["vcUnitofMeasure"]),
                        ListPrice = CCommon.ToDouble(row["monListPrice"]),
                        AverageCost = CCommon.ToDecimal(row["monAverageCost"]),
                        CampaignLabourCost = CCommon.ToDecimal(row["monCampaignLabourCost"]),
                        ItemGroupID = CCommon.ToInteger(row["numItemGroup"]),
                        ItemClassID = CCommon.ToInteger(row["numItemClass"]),
                        ItemShippingClassID = CCommon.ToInteger(row["numShipClass"]),
                        ItemClassificationID = CCommon.ToInteger(row["numItemClassification"]),
                        Width = CCommon.ToDouble(row["fltWidth"]),
                        Weight = CCommon.ToDouble(row["fltWeight"]),
                        Height = CCommon.ToDouble(row["fltHeight"]),
                        Length = CCommon.ToDouble(row["fltLength"]),
                        BaseUnitID = CCommon.ToInteger(row["numBaseUnit"]),
                        SaleUnitID = CCommon.ToInteger(row["numSaleUnit"]),
                        PurchaseUnitID = CCommon.ToInteger(row["numPurchaseUnit"]),
                        IsKit = CCommon.ToBool(CCommon.ToInteger(row["bitKitParent"])),
                        IsLot = CCommon.ToBool(CCommon.ToInteger(row["bitLotNo"])),
                        IsAssembly = CCommon.ToBool(CCommon.ToInteger(row["bitAssembly"])),
                        IsSerialized = CCommon.ToBool(CCommon.ToInteger(row["bitSerialized"])),
                        IsTaxable = CCommon.ToBool(CCommon.ToInteger(row["bitTaxable"])),
                        AllowBackOrder = CCommon.ToBool(CCommon.ToInteger(row["bitAllowBackOrder"])),
                        IsFreeShipping = CCommon.ToBool(CCommon.ToInteger(row["bitFreeShipping"])),
                        COGSChartAccountID = CCommon.ToInteger(row["numCOGsChartAcntId"]),
                        AssetChartAccountID = CCommon.ToInteger(row["numAssetChartAcntId"]),
                        IncomeChartAccountID = CCommon.ToInteger(row["numIncomeChartAcntId"]),
                        VendorID = CCommon.ToLong(row["numVendorID"]),
                        OnHand = CCommon.ToDouble(row["numOnHand"]),
                        OnOrder = CCommon.ToDouble(row["numOnOrder"]),
                        OnReOrder = CCommon.ToDouble(row["numReorder"]),
                        OnBackOrder = CCommon.ToDouble(row["numBackOrder"]),
                        OnAllocation = CCommon.ToDouble(row["numAllocation"]),
                        Warehouses = warehouseRepository.Get(CCommon.ToInteger(row["numItemCode"])),
                        PriceLevel = priceLevelRepository.Get(CCommon.ToInteger(row["numItemCode"])),
                        SubItems = GetSubItems(CCommon.ToInteger(row["numItemCode"]), ds.Tables.Count >= 2 ? ds.Tables[1] : null),
                        CustomFields = GetCustomFields(CCommon.ToInteger(row["numItemCode"]), ds.Tables.Count >= 3 ? ds.Tables[2] : null),
                        CreatedDate = row["bintCreatedDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["bintCreatedDate"]),
                        ModifiedDate = row["bintModifiedDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["bintModifiedDate"]),
                        Categories = CCommon.ToString(row["vcCategories"])
                    }).ToList();
                }

                return new ItemList() { Items = listItems.AsQueryable(), TotalRecords = totalRecords };
            }
            catch (Exception ex)
            {
                ExceptionModule.ExceptionPublish(ex, 0, 0, null);
                throw;
            }
            
        }

        private ItemList SearchItem(long domainID, string searchText, int pageIndex, int pageSize, DateTime? createdAfter, DateTime? modifiedAfter)
        {
            int totalRecords = 0;
            List<Item> listItems = new List<Item>();
            WarehouseRepository warehouseRepository = new WarehouseRepository();
            PriceLevelRepository priceLevelRepository = new PriceLevelRepository();

            CItems objItem = new CItems();
            objItem.DomainID = domainID;
            objItem.CurrentPage = CCommon.ToInteger(pageIndex);
            objItem.PageSize = CCommon.ToInteger(pageSize);
            objItem.SearchText = searchText;
            objItem.CreatedAfter = createdAfter;
            objItem.ModifiedAfter = modifiedAfter;
            DataSet ds = objItem.SearchItemsBizAPI(ref totalRecords);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                listItems = ds.Tables[0].AsEnumerable().Select(row => new Item
                {
                    ItemCode = CCommon.ToInteger(row["numItemCode"]),
                    ItemName = CCommon.ToString(row["vcItemName"]),
                    ItemImageURL = string.IsNullOrEmpty(CCommon.ToString(row["vcPathForImage"])) ? "" : path + domainID + "/" + CCommon.ToString(row["vcPathForImage"]),
                    ItemImageThumbURL = string.IsNullOrEmpty(CCommon.ToString(row["vcPathForTImage"])) ? "" : path + domainID + "/" + CCommon.ToString(row["vcPathForTImage"]),
                    ItemDescription = CCommon.ToString(row["txtItemDesc"]),
                    ItemTypeID = Convert.ToChar(row["charItemType"]),
                    ItemType = CCommon.ToString(row["ItemType"]),
                    SKU = CCommon.ToString(row["vcSKU"]),
                    ModelID = CCommon.ToString(row["vcModelID"]),
                    BarCodeID = CCommon.ToString(row["numBarCodeId"]),
                    Manufacturer = CCommon.ToString(row["vcManufacturer"]),
                    UnitofMeasure = CCommon.ToString(row["vcUnitofMeasure"]),
                    ListPrice = CCommon.ToDouble(row["monListPrice"]),
                    AverageCost = CCommon.ToDecimal(row["monAverageCost"]),
                    CampaignLabourCost = CCommon.ToDecimal(row["monCampaignLabourCost"]),
                    ItemGroupID = CCommon.ToInteger(row["numItemGroup"]),
                    ItemClassID = CCommon.ToInteger(row["numItemClass"]),
                    ItemShippingClassID = CCommon.ToInteger(row["numShipClass"]),
                    ItemClassificationID = CCommon.ToInteger(row["numItemClassification"]),
                    Width = CCommon.ToDouble(row["fltWidth"]),
                    Weight = CCommon.ToDouble(row["fltWeight"]),
                    Height = CCommon.ToDouble(row["fltHeight"]),
                    Length = CCommon.ToDouble(row["fltLength"]),
                    BaseUnitID = CCommon.ToInteger(row["numBaseUnit"]),
                    SaleUnitID = CCommon.ToInteger(row["numSaleUnit"]),
                    PurchaseUnitID = CCommon.ToInteger(row["numPurchaseUnit"]),
                    IsKit = CCommon.ToBool(CCommon.ToInteger(row["bitKitParent"])),
                    IsLot = CCommon.ToBool(CCommon.ToInteger(row["bitLotNo"])),
                    IsAssembly = CCommon.ToBool(CCommon.ToInteger(row["bitAssembly"])),
                    IsSerialized = CCommon.ToBool(CCommon.ToInteger(row["bitSerialized"])),
                    IsTaxable = CCommon.ToBool(CCommon.ToInteger(row["bitTaxable"])),
                    AllowBackOrder = CCommon.ToBool(CCommon.ToInteger(row["bitAllowBackOrder"])),
                    IsFreeShipping = CCommon.ToBool(CCommon.ToInteger(row["bitFreeShipping"])),
                    COGSChartAccountID = CCommon.ToInteger(row["numCOGsChartAcntId"]),
                    AssetChartAccountID = CCommon.ToInteger(row["numAssetChartAcntId"]),
                    IncomeChartAccountID = CCommon.ToInteger(row["numIncomeChartAcntId"]),
                    VendorID = CCommon.ToLong(row["numVendorID"]),
                    OnHand = CCommon.ToDouble(row["numOnHand"]),
                    OnOrder = CCommon.ToDouble(row["numOnOrder"]),
                    OnReOrder = CCommon.ToDouble(row["numReorder"]),
                    OnBackOrder = CCommon.ToDouble(row["numBackOrder"]),
                    OnAllocation = CCommon.ToDouble(row["numAllocation"]),
                    Warehouses = warehouseRepository.Get(CCommon.ToInteger(row["numItemCode"])),
                    PriceLevel = priceLevelRepository.Get(CCommon.ToInteger(row["numItemCode"])),
                    SubItems = GetSubItems(CCommon.ToInteger(row["numItemCode"]), ds.Tables.Count >= 2 ? ds.Tables[1] : null),
                    CustomFields = GetCustomFields(CCommon.ToInteger(row["numItemCode"]), ds.Tables.Count >= 3 ? ds.Tables[2] : null),
                    CreatedDate = row["bintCreatedDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["bintCreatedDate"]),
                    ModifiedDate = row["bintModifiedDate"] == DBNull.Value ? (DateTime?)null : Convert.ToDateTime(row["bintModifiedDate"]),
                    Categories = CCommon.ToString(row["vcCategories"])
                }).ToList();
            }

            return new ItemList() { Items = listItems.AsQueryable(), TotalRecords = totalRecords };
        }

        private IQueryable<SubItem> GetSubItems(long itemCode, DataTable dtSubItems)
        {
            List<SubItem> listSubItems = new List<SubItem>();

            if (dtSubItems != null && dtSubItems.Rows.Count > 0)
            {
                DataRow[] arrFoundRows = dtSubItems.Select("numItemKitID=" + itemCode);

                if (arrFoundRows != null && arrFoundRows.Count() > 0)
                {
                    foreach (DataRow row in arrFoundRows)
                    {
                        SubItem objSubItem = new SubItem();
                        objSubItem.ItemCode = row["numChildItemID"] == DBNull.Value ? 0 : CCommon.ToInteger(row["numChildItemID"]);
                        objSubItem.Quantity = row["numQtyItemsReq"] == DBNull.Value ? 0 : CCommon.ToInteger(row["numQtyItemsReq"]);
                        objSubItem.WarehouseID = row["numWareHouseItemID"] == DBNull.Value ? 0 : CCommon.ToInteger(row["numWareHouseItemID"]);
                        objSubItem.WarehouseName = row["vcWareHouse"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcWareHouse"]);
                        objSubItem.UnitOfMeasure = row["vcUnitName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcUnitName"]);

                        listSubItems.Add(objSubItem);
                    }
                }
            }

            return listSubItems.AsQueryable();
        }

        private IQueryable<CustomField> GetCustomFields(long itemCode, DataTable dtCustomFields)
        {
            List<CustomField> listCustomFields = new List<CustomField>();

            if (dtCustomFields != null && dtCustomFields.Rows.Count > 0)
            {
                DataRow[] arrFoundRows = dtCustomFields.Select("numItemCode=" + itemCode);

                if (arrFoundRows != null && arrFoundRows.Count() > 0)
                {
                    foreach (DataRow row in arrFoundRows)
                    {
                        CustomField objCustomField = new CustomField();
                        objCustomField.Id = row["Id"] == DBNull.Value ? 0 : CCommon.ToInteger(row["Id"]);
                        objCustomField.Name = row["Name"] == DBNull.Value ? "" : CCommon.ToString(row["Name"]);
                        objCustomField.Type = row["Type"] == DBNull.Value ? "" : CCommon.ToString(row["Type"]);
                        objCustomField.ValueID = row["ValueId"] == DBNull.Value ? "" : CCommon.ToString(row["ValueId"]);
                        objCustomField.Value = row["Value"] == DBNull.Value ? string.Empty : CCommon.ToString(row["Value"]);

                        listCustomFields.Add(objCustomField);
                    }
                }
            }

            return listCustomFields.AsQueryable();
        }

        #endregion
    }
}