﻿using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using BizAPI.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class PriceLevelRepository
    {
        #region Public Methods

        public PriceLevel Get(int itemCode)
        {
            try
            {
                return GetPriceLevel(itemCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private PriceLevel GetPriceLevel(int itemCode)
        {
            PriceLevel priceLevel = new PriceLevel();

            PriceBookRule objPriceBookRule = new PriceBookRule();
            objPriceBookRule.RuleID = 0;
            objPriceBookRule.ItemID = itemCode;
            objPriceBookRule.CurrencyID = 0;
            DataTable dtPriceLevel = objPriceBookRule.GetPriceTable();

            if (dtPriceLevel != null && dtPriceLevel.Rows.Count > 0)
            {
                int ruleType = CCommon.ToInteger(dtPriceLevel.Rows[0]["tintRuleType"]);
                int amountType = CCommon.ToInteger(dtPriceLevel.Rows[0]["tintDiscountType"]);

                PriceLevelBreakUp priceLevelBreakUp;
                List<PriceLevelBreakUp> listPriceLevelBreakUp = new List<PriceLevelBreakUp>();

                switch (ruleType)
                {
                    case 1:
                        priceLevel.PriceLevelType = "Deduct from List price";
                        break;
                    case 2:
                        priceLevel.PriceLevelType = "Add to Primary Vendor Cost";
                        break;
                    case 3:
                        priceLevel.PriceLevelType = "Named Price";
                        break;
                }

                switch (amountType)
                {
                    case 1:
                        priceLevel.AmountType = "Percentage";
                        break;
                    case 2:
                        priceLevel.AmountType = "Flat Amount";
                        break;
                }

                foreach (DataRow dr in dtPriceLevel.Rows)
                {
                    priceLevelBreakUp = new PriceLevelBreakUp();
                    priceLevelBreakUp.FromQuantity = CCommon.ToInteger(dr["intFromQty"]);
                    priceLevelBreakUp.ToQuantity = CCommon.ToInteger(dr["intToQty"]);
                    priceLevelBreakUp.Amount = CCommon.ToDouble(dr["decDiscount"]);
                    listPriceLevelBreakUp.Add(priceLevelBreakUp);
                }

                priceLevel.PriceLevelBreakUp = listPriceLevelBreakUp.AsQueryable();
            }

            return priceLevel;
        }

        #endregion
    }
}