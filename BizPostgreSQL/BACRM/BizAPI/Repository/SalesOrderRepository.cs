﻿using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Alerts;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Opportunities;
using BizAPI.Content;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web;

namespace BizAPI.Repository
{
    public class SalesOrderRepository : ISalesOrderRepository
    {
        #region Public Methods

        public BizAPIBusinessException Add(long domainID, SalesOrder salesOrder)
        {
            return CreateSalesOrder(domainID, salesOrder);
        }

        #endregion

        #region Private Methods

        private BizAPIBusinessException CreateSalesOrder(long domainID, SalesOrder salesOrder)
        {
            //using (TransactionScope t1 = new TransactionScope())
            //{
                long lngDivId = 0;
                long lngCntID = 0;
                long userContactID = CCommon.ToLong(HttpContext.Current.Request.Headers["UserContactID"]);
                int priceBookDiscount = CCommon.ToInteger(HttpContext.Current.Request.Headers["PriceBookDiscount"]);
                CCommon objCommon = new CCommon();

                if (salesOrder.IsNewCustomer)
                {
                    InsertProspect(domainID, salesOrder.NewCustomer, ref lngDivId, ref lngCntID);
                }
                else
                {
                    lngDivId = salesOrder.Customer.ID;
                    lngCntID = salesOrder.Contact.ID;
                }

                DataSet dsTemp = CreateDataSet(domainID);
                DataTable dtitem = new DataTable();
                dtitem = dsTemp.Tables[2];
                dtitem.Rows.Clear();

                List<CItems> listOrderItems = new List<CItems>();
                foreach (SalesOrderItem orderItem in salesOrder.Items)
                {
                    CItems objItem = new CItems();
                    objCommon.DomainID = domainID;
                    objCommon.DivisionID = lngDivId;
                    objItem.byteMode = CCommon.ToShort(1);
                    objItem.ItemCode = orderItem.ItemCode;
                    objItem.VendorID = orderItem.VendorID;
                    bool isValidItem = objItem.GetItemDetails();

                    if (!objItem.ValidateItemAccount(CCommon.ToLong(orderItem.ItemCode), domainID, 1))
                    {
                        return new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.ItemWarehouseDoesNotExists, BizAPIBusinessExceptionMessage.ItemWarehouseDoesNotExists);
                    }

                    if (objItem.ItemType == "P")
                    {
                        DataTable dtWareHouse = objCommon.GetWarehouseAttrBasedItem(objItem.ItemCode);
                        if (dtWareHouse == null || dtWareHouse.Rows.Count == 0)
                        {
                            return new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.ItemDefaultWarehouseDoesNotExists, BizAPIBusinessExceptionMessage.ItemDefaultWarehouseDoesNotExists);
                        }

                        if (orderItem.Warehouse != null && orderItem.Warehouse.ID > 0)
                        {
                            DataRow[] result = dtWareHouse.Select("numWareHouseItemId = " + orderItem.Warehouse.ID);

                            if (result.Count() == 0)
                            {
                                return new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.ItemWarehouseDoesNotExists, string.Format(BizAPIBusinessExceptionMessage.ItemWarehouseDoesNotExists, objItem.ItemCode, objItem.ItemName));
                            }

                            objItem.WareHouseItemID = orderItem.Warehouse.ID;
                        }
                        else
                        {
                            objItem.WareHouseItemID = CCommon.ToLong(dtWareHouse.Rows[0]["numWareHouseItemId"]);
                        }
                    }


                    if (objItem.ItemType == "P" && IsDuplicate(dsTemp, objItem.ItemCode.ToString(), objItem.WareHouseItemID.ToString(), orderItem.IsDropShip))
                    {

                    }
                    else
                    {
                        DataSet ds = null;
                        DataTable dt = null;

                        CItems objItems = new CItems();
                        objItems.ItemCode = objItem.ItemCode;
                        objItems.ItemGroupID = 0;
                        objItems.DomainID = domainID;
                        ds = objItems.GetItemGroups();
                        dt = ds.Tables[0];
                        dt.TableName = "ChildItems";

                        if (dt.Rows.Count > 0)
                        {
                            int i = 0;
                            DataRow dr2 = null;

                            foreach (DataRow drow in dt.Rows)
                            {
                                dr2 = dtitem.NewRow();
                                dr2["numOppChildItemID"] = Convert.ToInt32((dtitem.Compute("MAX(numOppChildItemID)", "") == DBNull.Value ? 0 : dtitem.Compute("MAX(numOppChildItemID)", ""))) + 1;
                                dr2["numoppitemtCode"] = 0;
                                dr2["numItemCode"] = drow["numItemCode"];
                                dr2["numQtyItemsReq"] = drow["numQtyItemsReq"];
                                dr2["vcItemName"] = drow["vcItemName"];
                                dr2["monListPrice"] = drow["monListPrice"];
                                dr2["UnitPrice"] = drow["UnitPrice"].ToString().Split('/')[0];
                                dr2["charItemType"] = drow["charItemType"];
                                dr2["txtItemDesc"] = drow["txtItemDesc"];
                                dr2["Op_Flag"] = 1;
                                dr2["ItemType"] = drow["ItemType"];
                                dr2["numWarehouseItmsID"] = drow["numWarehouseItmsID"];
                                dr2["vcWareHouse"] = drow["vcWareHouse"];

                                dtitem.Rows.Add(dr2);

                                i += 1;
                            }
                        }


                        AddItemToSession(domainID, ref dsTemp, objItem, priceBookDiscount, orderItem.IsDropShip, orderItem.IsWorkOrder, orderItem.Quantity, orderItem.UnitPrice);
                    }
                }

                Opportunity(domainID, lngDivId, lngCntID, userContactID, ref dsTemp, salesOrder);
            //    t1.Complete();
            //}

            return null;
        }

        private DataSet CreateDataSet(long domainID)
        {
            try
            {
                DataSet dsTemp = new DataSet();
                DataTable dtItem = new DataTable();
                DataTable dtSerItem = new DataTable();
                DataTable dtChildItems = new DataTable();
                dtItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"));
                dtItem.Columns.Add("numItemCode");
                dtItem.Columns.Add("numUnitHour");

                dtItem.Columns.Add("monPrice");
                dtItem.Columns.Add("numUOM");
                dtItem.Columns.Add("vcUOMName");
                dtItem.Columns.Add("UOMConversionFactor");

                dtItem.Columns.Add("monTotAmount", typeof(decimal));
                dtItem.Columns.Add("numSourceID");
                dtItem.Columns.Add("vcItemDesc");
                dtItem.Columns.Add("vcModelID");
                dtItem.Columns.Add("numWarehouseID");
                dtItem.Columns.Add("vcItemName");
                dtItem.Columns.Add("Warehouse");
                dtItem.Columns.Add("numWarehouseItmsID");
                dtItem.Columns.Add("ItemType");
                dtItem.Columns.Add("Attributes");
                dtItem.Columns.Add("Op_Flag");
                dtItem.Columns.Add("bitWorkOrder");
                dtItem.Columns.Add("vcInstruction");
                dtItem.Columns.Add("bintCompliationDate");
                dtItem.Columns.Add("numWOAssignedTo");

                dtItem.Columns.Add("DropShip", typeof(bool));
                dtItem.Columns.Add("bitDiscountType", typeof(bool));
                dtItem.Columns.Add("fltDiscount", typeof(decimal));
                dtItem.Columns.Add("monTotAmtBefDiscount", typeof(decimal));

                dtItem.Columns.Add("bitTaxable0");
                dtItem.Columns.Add("Tax0", typeof(decimal));

                dtItem.Columns.Add("numVendorWareHouse");
                dtItem.Columns.Add("numShipmentMethod");
                dtItem.Columns.Add("numSOVendorId");

                dtItem.Columns.Add("numProjectID");
                dtItem.Columns.Add("numProjectStageID");
                dtItem.Columns.Add("charItemType");
                dtItem.Columns.Add("numToWarehouseItemID");

                dtItem.Columns.Add("bitIsAuthBizDoc", typeof(bool));
                dtItem.Columns.Add("numUnitHourReceived");
                dtItem.Columns.Add("numQtyShipped");
                dtItem.Columns.Add("vcBaseUOMName");
                dtItem.Columns.Add("numVendorID", System.Type.GetType("System.Int32"));
                dtItem.Columns.Add("fltItemWeight");
                dtItem.Columns.Add("IsFreeShipping");
                dtItem.Columns.Add("fltHeight");
                dtItem.Columns.Add("fltWidth");
                dtItem.Columns.Add("fltLength");
                dtItem.Columns.Add("vcSKU");

                DataTable dtTaxTypes = null;
                TaxDetails ObjTaxItems = new TaxDetails();
                ObjTaxItems.DomainID = domainID;
                dtTaxTypes = ObjTaxItems.GetTaxItems();

                foreach (DataRow drTax in dtTaxTypes.Rows)
                {
                    dtItem.Columns.Add("Tax" + drTax["numTaxItemID"], typeof(decimal));
                    dtItem.Columns.Add("bitTaxable" + drTax["numTaxItemID"]);
                }

                dtSerItem.Columns.Add("numWarehouseItmsDTLID");
                dtSerItem.Columns.Add("numWItmsID");
                dtSerItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"));
                dtSerItem.Columns.Add("vcSerialNo");
                dtSerItem.Columns.Add("Comments");
                dtSerItem.Columns.Add("Attributes");

                dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"));
                dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"));
                dtChildItems.Columns.Add("numItemCode");
                dtChildItems.Columns.Add("numQtyItemsReq");
                dtChildItems.Columns.Add("vcItemName");
                dtChildItems.Columns.Add("monListPrice");
                dtChildItems.Columns.Add("UnitPrice");
                dtChildItems.Columns.Add("charItemType");
                dtChildItems.Columns.Add("txtItemDesc");
                dtChildItems.Columns.Add("numWarehouseItmsID");
                dtChildItems.Columns.Add("Op_Flag");
                dtChildItems.Columns.Add("ItemType");
                dtChildItems.Columns.Add("Attr");
                dtChildItems.Columns.Add("vcWareHouse");

                dtItem.TableName = "Item";
                dtSerItem.TableName = "SerialNo";
                dtChildItems.TableName = "ChildItems";

                dsTemp.Tables.Add(dtItem);
                dsTemp.Tables.Add(dtSerItem);
                dsTemp.Tables.Add(dtChildItems);
                dtItem.PrimaryKey = new DataColumn[] { dsTemp.Tables[0].Columns["numoppitemtCode"] };
                dtChildItems.PrimaryKey = new DataColumn[] { dsTemp.Tables[2].Columns["numOppChildItemID"] };

                return dsTemp;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void Opportunity(long domainID, long lngDivId, long lngCntID, long userContactID, ref DataSet dsTemp, SalesOrder salesOrder)
        {
            try
            {
                string[] arrOutPut = null;
                MOpportunity objOpportunity = new MOpportunity();
                objOpportunity.OpportunityId = 0;
                objOpportunity.ContactID = lngCntID;
                objOpportunity.DivisionID = lngDivId;

                if (!salesOrder.IsNewCustomer)
                {
                    objOpportunity.OpportunityName = salesOrder.Customer.Name + "-SO-" + DateTime.Now.ToString("MMMM");
                }
                else
                {
                    if (!string.IsNullOrEmpty(salesOrder.NewCustomer.Organization.Trim()))
                    {
                        objOpportunity.OpportunityName = salesOrder.NewCustomer.Organization + "-SO-" + DateTime.Now.ToString("MMMM");
                    }
                    else
                    {
                        objOpportunity.OpportunityName = salesOrder.NewCustomer.LastName + "," + salesOrder.NewCustomer.FirstName + "-SO-" + DateTime.Now.ToString("MMMM");
                    }
                }


                objOpportunity.UserCntID = userContactID;
                objOpportunity.EstimatedCloseDate = DateTime.UtcNow.AddMinutes(CCommon.ToDouble(salesOrder.TimeZoneOffset));
                objOpportunity.PublicFlag = 0;
                objOpportunity.DomainID = domainID;

                objOpportunity.CouponCode = "";
                objOpportunity.Discount = 0;
                objOpportunity.boolDiscType = false;
                //End If

                if (CCommon.ToBool(HttpContext.Current.Request.Headers["MultiCurrency"]) == true)
                {
                    //TODO: implement multi curreny
                    //objOpportunity.CurrencyID = radcmbCurrency.SelectedValue;
                }
                else
                {
                    objOpportunity.CurrencyID = CCommon.ToLong(HttpContext.Current.Request.Headers["BaseCurrencyID"]);
                }

                objOpportunity.OppType = 1;


                objOpportunity.DealStatus = CCommon.ToLong(MOpportunity.EnmDealStatus.DealWon);
                objOpportunity.SourceType = 1;


                dsTemp.Tables[2].Rows.Clear();
                dsTemp.AcceptChanges();
                decimal TotalAmount = 0;

                if (dsTemp.Tables[0].Rows.Count > 0)
                {
                    TotalAmount = Convert.ToDecimal(dsTemp.Tables[0].Compute("Sum(monTotAmount)", ""));
                    objOpportunity.strItems = "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsTemp.GetXml();
                }


                CAdmin objAdmin = new CAdmin();
                DataTable dtBillingTerms = null;
                objAdmin.DivisionID = lngDivId;
                dtBillingTerms = objAdmin.GetBillingTerms();

                objOpportunity.boolBillingTerms = (CCommon.ToInteger(dtBillingTerms.Rows[0]["tintBillingTerms"]) == 1 ? true : false);
                objOpportunity.BillingDays = CCommon.ToInteger(dtBillingTerms.Rows[0]["numBillingDays"]);
                objOpportunity.boolInterestType = (CCommon.ToInteger(dtBillingTerms.Rows[0]["tintInterestType"]) == 1 ? true : false);
                objOpportunity.Interest = CCommon.ToDecimal(dtBillingTerms.Rows[0]["fltInterest"]);

                MOpportunity objOpp = new MOpportunity();
                objOpp.DivisionID = lngDivId;
                objOpp.DomainID = objOpportunity.DomainID;
                objOpportunity.CampaignID = objOpp.GetCampaignForOrganization();


                arrOutPut = objOpportunity.Save();
                long lngOppId = CCommon.ToLong(arrOutPut[0]);

                ///AddingBizDocs
                OppBizDocs objOppBizDocs = new OppBizDocs();

                int OppBizDocID = CreateOppBizDoc(domainID, objOppBizDocs, lngOppId, userContactID, lngDivId, salesOrder.TimeZoneOffset);
                CreateDropShipPO(domainID, lngOppId, userContactID, ref dsTemp, salesOrder.TimeZoneOffset);
                OrderAutoRules objRule = new OrderAutoRules();
                objRule.GenerateAutoPO(OppBizDocID);
                AddToRecentlyViewed(lngOppId, userContactID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void AddItemToSession(long domainID, ref DataSet dsTemp, CItems objItem, int priceBookDiscount, bool isDropShip, bool isWorkOrder, int quantity, decimal unitPrice)
        {
            try
            {
                short tempByteMode = 0;
                tempByteMode = objItem.byteMode;
                CCommon objCommon = new CCommon();
                objCommon.DomainID = domainID;

                string uomName = null;
                long vendorId = 0;

                DataSet ds = null;
                DataTable dtItemPricemgmt = null;

                //Start - Get Unit of Measure of item

                objItem.NoofUnits = 1;


                //Start - Get Vendor Id
                objItem.byteMode = 2;
                ds = objItem.GetPriceManagementList1();
                dtItemPricemgmt = ds.Tables[0];

                if (dtItemPricemgmt.Rows.Count == 0)
                {
                    dtItemPricemgmt = ds.Tables[2];
                }

                dtItemPricemgmt.Merge(ds.Tables[1]);

                if ((dtItemPricemgmt != null) && dtItemPricemgmt.Rows.Count > 0)
                {
                    vendorId = CCommon.ToLong(dtItemPricemgmt.Rows[0]["numDivisionID"]);
                }
                objItem.byteMode = tempByteMode;
                //End - Get Vendor Id


                //Start get item UOM
                DataTable dtUOM = null;
                objCommon.ItemCode = objItem.ItemCode;
                objCommon.UOMAll = true;
                dtUOM = objCommon.GetItemUOM();

                if ((dtUOM != null) && dtUOM.Rows.Count > 0)
                {
                    DataRow[] rows = dtUOM.Select("numUOMId = " + CCommon.ToString(objItem.SaleUnit));
                    if (rows.Count() > 0)
                    {
                        uomName = CCommon.ToString(rows[0]["vcUnitName"]);
                    }
                }
                //End get item UOM

                //Start - Get ItemUOMConversion
                DataTable dtUnit = null;
                objCommon.ItemCode = objItem.ItemCode;
                objCommon.UnitId = 0;
                dtUnit = objCommon.GetItemUOMConversion();
                //End - Get ItemUOMConversion

                //Start - Get Tax Detail
                string strApplicable = null;
                DataTable dtItemTax = null;


                dtItemTax = objItem.ItemTax();

                foreach (DataRow dr in dtItemTax.Rows)
                {
                    strApplicable = strApplicable + dr["bitApplicable"] + ",";
                }

                strApplicable = strApplicable + objItem.Taxable;


                AddEditItemtoDataTable(ref objCommon, ref dsTemp, objItem, true, Convert.ToChar(objItem.ItemType), "", isDropShip, Convert.ToString(objItem.KitParent), objItem.ItemCode, quantity, unitPrice,
                objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, "", 0, objItem.ModelID, CCommon.ToLong(objItem.SaleUnit), uomName, 1,
                false, 0, strApplicable, "", bitWorkOrder: isWorkOrder, vcBaseUOMName: CCommon.ToString(dtUnit.Rows[0]["vcBaseUOMName"]),
                numPrimaryVendorID: CCommon.ToLong(objItem.VendorID),
                numSOVendorId: vendorId);

                objCommon = null;

            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool IsDuplicate(DataSet dsTemp, string ItemCode, string WareHouseItemID, bool DropShip)
        {
            try
            {
                if ((dsTemp.Tables[0].Select(" numItemCode='" + ItemCode + "'" + (!string.IsNullOrEmpty(WareHouseItemID) ? " and numWarehouseItmsID = '" + WareHouseItemID + "'" : "") + " and DropShip ='" + DropShip + "'").Length > 0))
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void InsertProspect(long domainID, NewCustomer newCustomer, ref long lngDivId, ref long lngCntID)
        {
            try
            {
                CLeads objLeads = new CLeads();

                if (!string.IsNullOrEmpty(newCustomer.Organization.Trim()))
                {
                    objLeads.CompanyName = newCustomer.Organization;
                }
                else
                {
                    objLeads.CompanyName = newCustomer.LastName + "," + newCustomer.FirstName;
                }

                objLeads.CustName = newCustomer.Organization.Trim();
                objLeads.CRMType = 1;
                objLeads.DivisionName = "-";
                objLeads.LeadBoxFlg = 1;
                objLeads.UserCntID = Convert.ToInt64(HttpContext.Current.Request.Headers["UserContactId"]);
                objLeads.Country = Convert.ToInt64(HttpContext.Current.Request.Headers["DefCountry"]);
                objLeads.SCountry = Convert.ToInt64(HttpContext.Current.Request.Headers["DefCountry"]);
                objLeads.FirstName = newCustomer.FirstName;
                objLeads.LastName = newCustomer.LastName;
                objLeads.ContactPhone = newCustomer.Phone;
                objLeads.PhoneExt = newCustomer.Ext;
                objLeads.Email = newCustomer.Email;
                objLeads.DomainID = domainID;
                objLeads.ContactType = 0;
                objLeads.PrimaryContact = true;

                objLeads.UpdateDefaultTax = false;

                if (newCustomer.Relationship != null && newCustomer.Relationship.ID > 0)
                {
                    objLeads.CompanyType = newCustomer.Relationship.ID;
                }
                objLeads.NoTax = false;

                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();
                lngDivId = objLeads.CreateRecordDivisionsInfo();
                objLeads.DivisionID = lngDivId;
                lngCntID = objLeads.CreateRecordAddContactInfo();

                if (newCustomer.BillingAddress != null || newCustomer.ShippingAddress != null)
                {
                    CContacts objContacts = new CContacts();
                    objContacts.BillStreet = newCustomer.BillingAddress.Street.Trim();
                    objContacts.BillCity = newCustomer.BillingAddress.City.Trim();
                    if (newCustomer.BillingAddress.Country != null && newCustomer.BillingAddress.Country.ID > 0)
                    {
                        objContacts.BillCountry = newCustomer.BillingAddress.Country.ID;

                        if (!newCustomer.IsShippingAddressDifferent)
                        {
                            objContacts.ShipCountry = newCustomer.BillingAddress.Country.ID;
                        }
                    }
                    if (newCustomer.BillingAddress.State != null && newCustomer.BillingAddress.State.ID > 0)
                    {
                        objContacts.BillState = newCustomer.BillingAddress.State.ID;
                        if (!newCustomer.IsShippingAddressDifferent)
                        {
                            objContacts.ShipState = newCustomer.BillingAddress.State.ID;
                        }
                    }

                    objContacts.BillPostal = newCustomer.BillingAddress.PostalCode.Trim();
                    objContacts.BillingAddress = (newCustomer.IsShippingAddressDifferent == true ? CCommon.ToShort(0) : CCommon.ToShort(1));

                    objContacts.ShipStreet = (newCustomer.IsShippingAddressDifferent == true ? newCustomer.ShippingAddress.Street.Trim() : newCustomer.BillingAddress.Street.Trim());
                    objContacts.ShipCity = (newCustomer.IsShippingAddressDifferent == true ? newCustomer.ShippingAddress.City.Trim() : newCustomer.BillingAddress.City.Trim());
                    if (newCustomer.IsShippingAddressDifferent && newCustomer.ShippingAddress.Country != null && newCustomer.ShippingAddress.Country.ID > 0)
                    {
                        objContacts.ShipCountry = newCustomer.ShippingAddress.Country.ID;
                    }

                    if (newCustomer.IsShippingAddressDifferent && newCustomer.ShippingAddress.State != null && newCustomer.ShippingAddress.State.ID > 0)
                    {
                        objContacts.ShipState = newCustomer.ShippingAddress.State.ID;
                    }
                    objContacts.ShipPostal = (newCustomer.IsShippingAddressDifferent == true ? newCustomer.ShippingAddress.PostalCode.Trim() : newCustomer.BillingAddress.PostalCode.Trim());


                    objContacts.DivisionID = lngDivId;
                    objContacts.UpdateCompanyAddress();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private int CreateOppBizDoc(long domainID, OppBizDocs objOppBizDocs, long lngOppId, long userContactID, long lngDivId, long timeZoneOffset)
        {
            try
            {
                objOppBizDocs.OppId = lngOppId;
                objOppBizDocs.OppType = 1;
                objOppBizDocs.UserCntID = userContactID;
                objOppBizDocs.DomainID = domainID;
                objOppBizDocs.vcPONo = "";
                objOppBizDocs.vcComments = "";

                long lngBizDoc = 0;
                lngBizDoc = objOppBizDocs.GetAuthorizativeOpportuntiy();

                if (lngBizDoc == 0)
                {
                    return 0;
                }

                objOppBizDocs.BizDocId = lngBizDoc;

                CCommon objCommon = new CCommon();
                objCommon.DomainID = domainID;
                objCommon.Mode = 33;
                objCommon.Str = lngBizDoc.ToString();
                objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue().ToString();

                objOppBizDocs.bitPartialShipment = true;
                int OppBizDocID = objOppBizDocs.SaveBizDoc();

                CreateJournalEntry(objOppBizDocs, OppBizDocID, lngBizDoc, domainID, lngOppId, userContactID, lngDivId, timeZoneOffset);

                return OppBizDocID;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CreateJournalEntry(OppBizDocs objOppBizDocs, long OppBizDocID, long lngBizDoc, long domainID, long lngOppId, long userContactID, long lngDivId, long timeZoneOffset)
        {
            try
            {
                //Create Journals only for authoritative bizdocs
                //-------------------------------------------------
                long lintAuthorizativeBizDocsId = 0;
                objOppBizDocs.DomainID = domainID;
                objOppBizDocs.OppId = lngOppId;
                objOppBizDocs.UserCntID = userContactID;
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy();

                if (lintAuthorizativeBizDocsId == lngBizDoc)
                {
                    DataSet ds = new DataSet();
                    objOppBizDocs.OppBizDocId = OppBizDocID;
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting();
                    DataTable dtOppBiDocItems = ds.Tables[0];

                    CalculateDealAmount objCalculateDealAmount = new CalculateDealAmount();

                    objCalculateDealAmount.CalculateDealAmountBizAPI(lngOppId, OppBizDocID, 1, domainID, dtOppBiDocItems, CCommon.ToInteger(timeZoneOffset));

                    //'---------------------------------------------------------------------------------
                    long JournalId = SaveDataToHeader(objCalculateDealAmount.GrandTotal, lngOppId, OppBizDocID, userContactID, domainID);
                    JournalEntry objJournalEntries = new JournalEntry();


                    objJournalEntries.SaveJournalEntriesSalesBizAPI(lngOppId, domainID, dtOppBiDocItems, JournalId, OppBizDocID,
                                                             objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount,
                                                             objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId,
                                                             CCommon.ToLong(ds.Tables[1].Rows[0]["numCurrencyID"]),
                                                             CCommon.ToLong(ds.Tables[1].Rows[0]["fltExchangeRate"]),
                                                             userContactID,
                                                             CCommon.ToLong(HttpContext.Current.Request.Headers["BaseCurrencyID"]),
                                                             0);


                }
                //------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
                CAlerts objAlert = new CAlerts();
                objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, lngBizDoc, domainID, CAlerts.enmBizDoc.IsCreated);

                AutomatonRule objAutomatonRule = new AutomatonRule();
                objAutomatonRule.ExecuteAutomationRuleBizAPI(49, OppBizDocID, 1, domainID, userContactID, CCommon.ToInteger(timeZoneOffset), CCommon.ToString(HttpContext.Current.Request.Headers["ContactName"]), CCommon.ToString(HttpContext.Current.Request.Headers["UserEmail"]));
            }
            catch (Exception)
            {
                throw;
            }
        }

        private long SaveDataToHeader(decimal GrandTotal, long lngOppId, long OppBizDocID, long userContactID, long domainID)
        {
            try
            {
                long lngJournalId = 0;

                JournalEntryHeader objJEHeader = new JournalEntryHeader();
                objJEHeader.JournalId = 0;
                objJEHeader.RecurringId = 0;
                objJEHeader.EntryDate = new DateTime(System.DateTime.UtcNow.Date.Year, System.DateTime.UtcNow.Date.Month, System.DateTime.UtcNow.Date.Day, 12, 0, 0);
                objJEHeader.Description = "";
                objJEHeader.Amount = GrandTotal;
                objJEHeader.CheckId = 0;
                objJEHeader.CashCreditCardId = 0;
                objJEHeader.ChartAcntId = 0;
                objJEHeader.OppId = lngOppId;
                objJEHeader.OppBizDocsId = OppBizDocID;
                objJEHeader.DepositId = 0;
                objJEHeader.BizDocsPaymentDetId = 0;
                objJEHeader.IsOpeningBalance = false;
                objJEHeader.LastRecurringDate = System.DateTime.Now;
                objJEHeader.NoTransactions = 0;
                objJEHeader.CategoryHDRID = 0;
                objJEHeader.ReturnID = 0;
                objJEHeader.CheckHeaderID = 0;
                objJEHeader.BillID = 0;
                objJEHeader.BillPaymentID = 0;
                objJEHeader.UserCntID = userContactID;
                objJEHeader.DomainID = domainID;
                lngJournalId = objJEHeader.Save();
                return lngJournalId;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AddToRecentlyViewed(long RecordID, long userContactID)
        {
            try
            {
                CCommon objCommon = new CCommon();
                objCommon.charModule = 'O';
                objCommon.RecordId = RecordID;
                objCommon.UserCntID = userContactID;
                objCommon.AddVisiteddetails();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CreateDropShipPO(long domainID, long lngOppId, long userContactID, ref DataSet dsTemp, long timeZoneOffset)
        {
            try
            {
                if (dsTemp.Tables[0].Rows.Count > 0)
                {
                    DataRow[] drDSRows = null;
                    drDSRows = dsTemp.Tables[0].Select("DropShip=1");

                    if (drDSRows.Length > 0)
                    {
                        DataTable dtDSRows = null;
                        dtDSRows = drDSRows.CopyToDataTable();

                        if (dtDSRows.Rows.Count > 0)
                        {
                            CCommon objCommon = new CCommon();

                            DataView dv = new DataView(dtDSRows);
                            DataTable dtPrimaryVendor = dv.ToTable(true, "numVendorID");

                            MOpportunity objOpportunity = new MOpportunity();
                            DataRow[] drDSItems = null;

                            foreach (DataRow drVendor in dtPrimaryVendor.Rows)
                            {
                                //Get Company Values
                                objCommon.UserCntID = userContactID;
                                objCommon.DivisionID = CCommon.ToLong(drVendor["numVendorID"]);
                                objCommon.charModule = 'D';
                                objCommon.GetCompanySpecificValues1();

                                objOpportunity = new MOpportunity();
                                objOpportunity.OpportunityId = 0;
                                objOpportunity.OppType = 2;
                                //Purchase Order

                                objOpportunity.OpportunityName = objCommon.CompanyName + "-PO-" + DateTime.Now.ToString("MMMM");
                                objOpportunity.ContactID = objCommon.ContactID;
                                objOpportunity.DivisionID = objCommon.DivisionID;
                                objOpportunity.UserCntID = userContactID;
                                objOpportunity.EstimatedCloseDate = DateTime.UtcNow.AddMinutes(CCommon.ToDouble(timeZoneOffset));
                                objOpportunity.DomainID = domainID;
                                objOpportunity.Active = true;
                                objOpportunity.DealStatus = CCommon.ToLong(MOpportunity.EnmDealStatus.DealWon);
                                objOpportunity.SourceType = 1;

                                CAdmin objAdmin = new CAdmin();
                                DataTable dtBillingTerms = null;
                                objAdmin.DivisionID = objCommon.DivisionID;
                                dtBillingTerms = objAdmin.GetBillingTerms();

                                objOpportunity.boolBillingTerms = (CCommon.ToInteger(dtBillingTerms.Rows[0]["tintBillingTerms"]) == 1 ? true : false);
                                objOpportunity.BillingDays = CCommon.ToInteger(dtBillingTerms.Rows[0]["numBillingDays"]);
                                objOpportunity.boolInterestType = (CCommon.ToInteger(dtBillingTerms.Rows[0]["tintInterestType"]) == 1 ? true : false);
                                objOpportunity.Interest = CCommon.ToDecimal(dtBillingTerms.Rows[0]["fltInterest"]);

                                objOpportunity.OpportunityId = CCommon.ToLong(objOpportunity.Save()[0]);

                                drDSItems = dtDSRows.Select("numVendorID='" + drVendor["numVendorID"].ToString() + "'", null);
                                if (drDSItems.Length > 0)
                                {
                                    foreach (DataRow drItem in drDSItems)
                                    {
                                        objOpportunity.ItemCode = CCommon.ToLong(drItem["numItemCode"]);
                                        objOpportunity.Units = CCommon.ToDecimal(drItem["numUnitHour"]);

                                        DataSet ds = null;
                                        DataTable dtItemPricemgmt = null;

                                        objCommon = new CCommon();
                                        objCommon.DomainID = domainID;

                                        decimal decVendorCost = 0;

                                        CItems objItems = new CItems();
                                        objItems.ItemCode = CCommon.ToInteger(drItem["numItemCode"]);
                                        objItems.NoofUnits = CCommon.ToLong(drItem["numUnitHour"]);
                                        objItems.OppId = 0;
                                        objItems.DomainID = domainID;
                                        objItems.Amount = 0;
                                        objItems.WareHouseItemID = CCommon.ToLong(drItem["numWarehouseItmsID"]);
                                        objItems.DivisionID = CCommon.ToLong(drVendor["numVendorID"]);
                                        //Set Vendor
                                        objItems.byteMode = 2;
                                        //Set Purchase
                                        ds = objItems.GetPriceManagementList1();
                                        dtItemPricemgmt = ds.Tables[0];

                                        if (dtItemPricemgmt.Rows.Count == 0)
                                        {
                                            dtItemPricemgmt = ds.Tables[2];
                                        }

                                        dtItemPricemgmt.Merge(ds.Tables[1]);

                                        decVendorCost = CCommon.ToDecimal(dtItemPricemgmt.Rows[0]["ListPrice"]);

                                        objOpportunity.UnitPrice = decVendorCost;
                                        objOpportunity.Desc = CCommon.ToString(drItem["vcItemDesc"]);
                                        objOpportunity.ItemName = CCommon.ToString(drItem["vcItemName"]);
                                        objOpportunity.ModelName = CCommon.ToString(drItem["vcModelID"]);
                                        objOpportunity.ItemType = CCommon.ToString(drItem["charItemType"]);
                                        objOpportunity.Dropship = true;
                                        objOpportunity.WarehouseItemID = CCommon.ToLong(drItem["numWarehouseItmsID"]);
                                        objOpportunity.numUOM = CCommon.ToString(drItem["numUOM"]);

                                        objOpportunity.ProjectID = CCommon.ToLong(drItem["numProjectID"]);


                                        objOpportunity.UpdateOpportunityItems();
                                    }
                                }

                                BACRM.BusinessLogic.Reports.CustomReports objCustomReport = new BACRM.BusinessLogic.Reports.CustomReports();
                                objCustomReport.DynamicQuery = " PERFORM USP_UpdatingInventoryonCloseDeal(" + objOpportunity.OpportunityId + ",0," + userContactID + ");";
                                objCustomReport.ExecuteDynamicSql();

                                objOpportunity.BillToType = 0;
                                objOpportunity.ShipToType = 1;
                                objOpportunity.ParentOppID = lngOppId;
                                objOpportunity.ParentBizDocID = 0;
                                objOpportunity.UpdateOpportunityLinkingDtls();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void AddEditItemtoDataTable(ref CCommon objCommon, ref DataSet dsTemp, CItems objItem, bool AddMode, char ItemTypeValue, string ItemTypeText,
                                               bool DropShip, string IsKit, long ItemCode, decimal Units, decimal Price, string Desc, long WareHouseItemID, string ItemName,
                                               string Warehouse, long OppItemID, string ModelId, long UOM, string vcUOMName, decimal UOMConversionFactor,
                                               bool IsDiscountInPer, decimal Discount, string Taxable, string Tax, bool bitWorkOrder = false, string vcInstruction = "",
                                               string bintCompliationDate = "", long numWOAssignedTo = 0, long numVendorWareHouse = 0, long numShipmentMethod = 0,
                                               long numSOVendorId = 0, long numProjectID = 0, long numProjectStageID = 0, bool bDirectAdd = false, long ToWarehouseItemID = 0,
                                               bool IsPOS = false, string vcBaseUOMName = "", long numPrimaryVendorID = 0, double fltItemWeight = 0, bool IsFreeShipping = false,
                                               double fltHeight = 0, double fltWidth = 0, double fltLength = 0, string strSKU = "")
        {

            try
            {
                DataTable dtItem = null;
                DataTable dtSerItem = null;
                DataTable dtChildItems = null;

                dtItem = dsTemp.Tables[0];
                dtSerItem = dsTemp.Tables[1];
                dtChildItems = dsTemp.Tables[2];

                if (!dtItem.Columns.Contains("fltItemWeight"))
                    dtItem.Columns.Add("fltItemWeight");

                if (!dtItem.Columns.Contains("IsFreeShipping"))
                    dtItem.Columns.Add("IsFreeShipping");
                if (!dtItem.Columns.Contains("fltHeight"))
                    dtItem.Columns.Add("fltHeight");
                if (!dtItem.Columns.Contains("fltWidth"))
                    dtItem.Columns.Add("fltWidth");
                if (!dtItem.Columns.Contains("fltLength"))
                    dtItem.Columns.Add("fltLength");


                DataRow dr = null;

                long lngOppItemId = 0;

                if (AddMode == true)
                {
                    if ((dsTemp.Tables[0].Select("numItemCode='" + ItemCode + "'" + (WareHouseItemID > 0 ? " and numWarehouseItmsID = '" + WareHouseItemID + "'" : "") + " and DropShip ='" + DropShip + "'").Length > 0))
                    {

                    }

                    dr = dtItem.NewRow();
                    dr["Op_Flag"] = 1;
                    dr["numoppitemtCode"] = Convert.ToInt32((dtItem.Compute("MAX(numoppitemtCode)", "") == DBNull.Value ? 0 : dtItem.Compute("MAX(numoppitemtCode)", ""))) + 1;
                    dr["numItemCode"] = ItemCode;
                    dr["vcModelID"] = ModelId;

                    dr["ItemType"] = ItemTypeText;
                    dr["vcItemName"] = ItemName;

                    if (numProjectID > 0 && numProjectStageID > 0)
                    {
                        dr["numProjectID"] = numProjectID;
                        dr["numProjectStageID"] = numProjectStageID;
                    }
                    else
                    {
                        dr["numProjectID"] = 0;
                        dr["numProjectStageID"] = 0;
                    }

                    dr["charItemType"] = ItemTypeValue;
                    dr["bitIsAuthBizDoc"] = false;
                    dr["numUnitHourReceived"] = 0;
                    dr["numQtyShipped"] = 0;

                }
                else if (IsPOS == true)
                {
                    dr = dtItem.Rows.Find(OppItemID);
                    lngOppItemId = CCommon.ToLong(dr["numoppitemtCode"]);
                }
                else
                {
                    dr = dtItem.Rows.Find(OppItemID);
                    lngOppItemId = CCommon.ToLong(dr["numoppitemtCode"]);
                }


                //Strat - Changed By Sandeep
                //Reason - Following code is removed from above if condition because it is also required when item is updated.
                dr["bitWorkOrder"] = bitWorkOrder;

                if (bitWorkOrder == true)
                {
                    dr["vcInstruction"] = vcInstruction;
                    dr["bintCompliationDate"] = bintCompliationDate;
                    dr["numWOAssignedTo"] = numWOAssignedTo;
                }
                //End - Changed By Sandeep

                dr["vcItemDesc"] = Desc.Replace("'", "''");
                dr["numUnitHour"] = Units;
                dr["monPrice"] = Price;
                dr["numUOM"] = UOM;
                dr["vcUOMName"] = vcUOMName;
                dr["UOMConversionFactor"] = UOMConversionFactor;
                dr["monTotAmtBefDiscount"] = decimal.Round((Units * UOMConversionFactor) * Price, 2).ToString("f2");
                dr["vcBaseUOMName"] = vcBaseUOMName;

                dr["bitDiscountType"] = (IsDiscountInPer == true ? 0 : 1);
                dr["fltDiscount"] = Discount;

                if (IsDiscountInPer == true)
                {
                    dr["monTotAmount"] = CCommon.ToDecimal(dr["monTotAmtBefDiscount"]) - CCommon.ToDecimal(dr["monTotAmtBefDiscount"]) * CCommon.ToDecimal(dr["fltDiscount"]) / 100;
                }
                else
                {
                    dr["monTotAmount"] = CCommon.ToDecimal(dr["monTotAmtBefDiscount"]) - CCommon.ToDecimal(dr["fltDiscount"]);
                }

                dr["numVendorWareHouse"] = numVendorWareHouse;
                dr["numShipmentMethod"] = numShipmentMethod;
                dr["numSOVendorId"] = numSOVendorId;

                dr["fltItemWeight"] = fltItemWeight;
                dr["IsFreeShipping"] = IsFreeShipping;
                dr["fltHeight"] = fltHeight;
                dr["fltWidth"] = fltWidth;
                dr["fltLength"] = fltLength;
                dr["vcSKU"] = strSKU;

                if (ItemTypeValue == 'P')
                {
                    if (DropShip == false)
                    {
                        CItems objItems = new CItems();
                        objItems.WareHouseItemID = WareHouseItemID;
                        dr["numWarehouseID"] = objItems.GetWarehouseID();
                        dr["Warehouse"] = Warehouse;
                        dr["numWarehouseItmsID"] = WareHouseItemID;
                        dr["Attributes"] = objCommon.GetAttributesForWarehouseItem(CCommon.ToLong(dr["numWarehouseItmsID"]), false);
                        dr["DropShip"] = false;
                        dr["numToWarehouseItemID"] = ToWarehouseItemID;
                        dr["numVendorID"] = 0;
                    }
                    else
                    {
                        dr["numWarehouseID"] = DBNull.Value;
                        dr["numWarehouseItmsID"] = DBNull.Value;
                        dr["Warehouse"] = "";
                        dr["Attributes"] = "";
                        dr["DropShip"] = true;
                        dr["numVendorID"] = numPrimaryVendorID;
                    }
                }
                else
                {
                    dr["numWarehouseID"] = DBNull.Value;
                    dr["numWarehouseItmsID"] = DBNull.Value;
                }

                if (AddMode == true)
                {
                    dtItem.Rows.Add(dr);
                }

                if (bDirectAdd == false)
                {
                    if (dtChildItems.Rows.Count > 0)
                    {
                        for (Int32 i = 0; i <= dtChildItems.Rows.Count - 1; i++)
                        {
                            if ((dsTemp.Tables[0].Select("numItemCode='" + dtChildItems.Rows[i]["numItemCode"] + "'").Length == 0))
                            {
                                dr = dtItem.NewRow();

                                dr["Op_Flag"] = 1;
                                dr["numoppitemtCode"] = Convert.ToInt32((dtItem.Compute("MAX(numoppitemtCode)", "") == DBNull.Value ? 0 : dtItem.Compute("MAX(numoppitemtCode)", ""))) + 1;
                                //dtItem.Rows.Count + 1
                                dr["numItemCode"] = dtChildItems.Rows[i]["numItemCode"];

                                if (dtChildItems.Rows[i]["numQtyItemsReq"] == DBNull.Value)
                                {
                                    dr["numUnitHour"] = 0;
                                }
                                else if (dtChildItems.Rows[i]["numQtyItemsReq"].ToString().Length == 0)
                                {
                                    dr["numUnitHour"] = 0;
                                }
                                else
                                {
                                    dr["numUnitHour"] = Math.Abs(CCommon.ToDecimal(dtChildItems.Rows[i]["numQtyItemsReq"].ToString()));
                                }

                                dr["monPrice"] = CCommon.ToDecimal(dtChildItems.Rows[i]["monListPrice"].ToString());

                                dr["bitDiscountType"] = false;
                                dr["fltDiscount"] = 0;
                                dr["monTotAmtBefDiscount"] = (dr["numUnitHour"] == DBNull.Value ? 0 : CCommon.ToDecimal(dr["numUnitHour"])) * (dr["monPrice"] == DBNull.Value ? 0 : CCommon.ToDecimal(dr["monPrice"]));

                                dr["monTotAmount"] = CCommon.ToDecimal(dr["monTotAmtBefDiscount"]) - CCommon.ToDecimal(dr["fltDiscount"]);
                                dr["Tax0"] = 0;
                                dr["bitTaxable0"] = false;

                                dr["vcItemDesc"] = dtChildItems.Rows[i]["txtItemDesc"];

                                if (CCommon.ToInteger(dtChildItems.Rows[i]["numWarehouseItmsID"]) != 0)
                                {
                                    dr["numWarehouseItmsID"] = dtChildItems.Rows[i]["numWarehouseItmsID"];
                                    dr["Warehouse"] = dtChildItems.Rows[i]["vcWareHouse"];
                                }
                                else
                                {
                                    dr["numWarehouseItmsID"] = DBNull.Value;
                                    dr["Warehouse"] = DBNull.Value;
                                }

                                dr["DropShip"] = false;
                                dr["bitIsAuthBizDoc"] = false;
                                dr["numUnitHourReceived"] = 0;
                                dr["numQtyShipped"] = 0;

                                dr["vcItemName"] = dtChildItems.Rows[i]["vcItemName"];
                                dr["ItemType"] = dtChildItems.Rows[i]["charItemType"];

                                dr["UOMConversionFactor"] = 1;
                                dr["numUOM"] = 0;
                                dr["vcUOMName"] = "";
                                dr["bitWorkOrder"] = false;
                                dr["vcBaseUOMName"] = "";

                                dr["numVendorWareHouse"] = "0";
                                dr["numShipmentMethod"] = "0";
                                dr["numSOVendorId"] = "0";

                                if (numProjectID > 0 && numProjectStageID > 0)
                                {
                                    dr["numProjectID"] = numProjectID;
                                    dr["numProjectStageID"] = numProjectStageID;
                                }
                                else
                                {
                                    dr["numProjectID"] = 0;
                                    dr["numProjectStageID"] = 0;
                                }
                                dr["charItemType"] = dtChildItems.Rows[i]["charItemType"];

                                dtItem.Rows.Add(dr);
                            }
                        }
                    }
                }

                dr.AcceptChanges();
                dsTemp.AcceptChanges();


                if (dtSerItem.ParentRelations.Count == 0)
                    dsTemp.Relations.Add("Item", dsTemp.Tables[0].Columns["numoppitemtCode"], dsTemp.Tables[1].Columns["numoppitemtCode"]);

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}