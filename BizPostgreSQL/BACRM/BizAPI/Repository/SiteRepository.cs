﻿using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.ShioppingCart;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class SiteRepository : ISiteRepository
    {
        #region Public Methods

        public IQueryable<SiteCategory> GetCategories(long domainID, long siteID)
        {
            try
            {
                return GetSiteCategories(domainID, siteID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private IQueryable<SiteCategory> GetSiteCategories(long domainID, long siteID)
        {
            List<SiteCategory> listSiteCategories = new List<SiteCategory>();

            Sites objSite = new Sites();
            objSite.DomainID = domainID;
            objSite.SiteID = siteID;
            DataTable dtSiteCategories = objSite.GetSiteCategories();

            if (dtSiteCategories != null && dtSiteCategories.Rows.Count > 0)
            {
                listSiteCategories = dtSiteCategories.AsEnumerable().Select(row => new SiteCategory
                {
                    CategoryID = row["numCategoryID"] == DBNull.Value ? 0 : CCommon.ToLong(row["numCategoryID"]),
                    ParentCategoryID = row["numParentCategoryID"] == DBNull.Value ? 0 : CCommon.ToLong(row["numParentCategoryID"]),
                    CategoryName = row["vcCategoryName"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcCategoryName"]),
                    Description = row["vcDescription"] == DBNull.Value ? string.Empty : CCommon.ToString(row["vcDescription"]),
                    Level = row["tintLevel"] == DBNull.Value ? 0 : CCommon.ToInteger(row["tintLevel"]),
                    DisplayOrder = row["intDisplayOrder"] == DBNull.Value ? 0 : CCommon.ToInteger(row["intDisplayOrder"])
                }).ToList();
            }

            return listSiteCategories.AsQueryable();
        }

        #endregion
    }
}