﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class CountryRepository : ICountryRepository
    {
        #region Public Methods

        public IQueryable<Country> GetAll(long domainID)
        {
            return GetCountryList(domainID);
        }

        #endregion

        #region Private Methods

        private IQueryable<Country> GetCountryList(long domainID)
        {
            List<Country> listCountry = new List<Country>();

            CCommon objCommon = new CCommon();
            DataTable dtCountry = objCommon.GetMasterListItems(40, domainID);

            if (dtCountry != null && dtCountry.Rows.Count > 0)
            {
                listCountry = dtCountry.AsEnumerable().Select(row => new Country
                {
                    ID = CCommon.ToInteger(row["numListItemID"]),
                    Name = CCommon.ToString(row["vcData"]),
                }).ToList();
            }

            return listCountry.AsQueryable();
        }

        #endregion
    }
}