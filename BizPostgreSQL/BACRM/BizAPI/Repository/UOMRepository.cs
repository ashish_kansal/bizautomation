﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class UOMRepository : IUOMRepository
    {
        #region Public Methods

        public IQueryable<UOM> GetAll(long domainID)
        {
            try
            {
                return GetUOM(domainID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private IQueryable<UOM> GetUOM(long domainID)
        {
            List<UOM> listUOM = new List<UOM>();

            CCommon common = new CCommon();
            common.DomainID = domainID;
            common.UOMAll = true;
            DataTable dtUnit = common.GetItemUOM();

            if (dtUnit != null && dtUnit.Rows.Count > 0)
            {
                listUOM = dtUnit.AsEnumerable()
                                    .Select(row => new UOM
                                    {
                                        ID = CCommon.ToInteger(row["numUOMId"]),
                                        Name = CCommon.ToString(row["vcUnitName"]),
                                    }).ToList();
            }

            return listUOM.AsQueryable();
        }

        #endregion
    }
}