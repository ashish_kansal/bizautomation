﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models.Items;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class ItemClassRepository : IItemClassRepository
    {
        #region Public Methods

        public IQueryable<ItemClass> GetAll(long domainID)
        {
            try
            {
                return GetItemClassList(domainID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private IQueryable<ItemClass> GetItemClassList(long domainID)
        {
            List<ItemClass> listItemClass = new List<ItemClass>();

            CCommon objCommon = new CCommon();
            DataTable dtItemClass = objCommon.GetMasterListItems(381, domainID);

            if (dtItemClass != null && dtItemClass.Rows.Count > 0)
            {
                listItemClass = dtItemClass.AsEnumerable()
                                  .Select(row => new ItemClass
                                  {
                                      ID = CCommon.ToInteger(row["numListItemID"]),
                                      Name = CCommon.ToString(row["vcData"]),
                                  }).ToList();
            }

            return listItemClass.AsQueryable();
        }

        #endregion
    }
}