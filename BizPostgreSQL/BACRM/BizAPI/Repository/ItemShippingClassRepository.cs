﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models.Items;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class ItemShippingClassRepository : IItemShippingClassRepository
    {
        #region Public Methods

        public IQueryable<ItemShippingClass> GetAll(long domainID)
        {
            try
            {
                return GetItemShippingClassList(domainID);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private IQueryable<ItemShippingClass> GetItemShippingClassList(long domainID)
        {
            List<ItemShippingClass> listItemShippingClass = new List<ItemShippingClass>();

            CCommon objCommon = new CCommon();
            DataTable dtItemShippingClass = objCommon.GetMasterListItems(461, domainID);

            if (dtItemShippingClass != null && dtItemShippingClass.Rows.Count > 0)
            {
                listItemShippingClass = dtItemShippingClass.AsEnumerable()
                                  .Select(row => new ItemShippingClass
                                  {
                                      ID = CCommon.ToInteger(row["numListItemID"]),
                                      Name = CCommon.ToString(row["vcData"]),
                                  }).ToList();
            }

            return listItemShippingClass.AsQueryable();
        }

        #endregion
    }
}