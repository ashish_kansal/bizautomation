﻿using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Item;
using BizAPI.Models.Items;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BizAPI.Repository
{
    public class ItemGroupRepository : IItemGroupRepository
    {
        #region Public Methods

        public IQueryable<ItemGroup> GetAll(long domainID)
        {
            try
            {
                return GetItemGroupList(domainID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Private Methods

        private IQueryable<ItemGroup> GetItemGroupList(long domainID)
        {
            List<ItemGroup> listItemGroup = new List<ItemGroup>();
            CItems objItems = new CItems();
            objItems.DomainID = domainID;
            DataTable dtItemGroup = objItems.GetItemGroups().Tables[0];

            if (dtItemGroup != null && dtItemGroup.Rows.Count > 0)
            {
                listItemGroup = dtItemGroup.AsEnumerable()
                                  .Select(row => new ItemGroup
                                  {
                                      ID = CCommon.ToInteger(row["numItemGroupID"]),
                                      Name = CCommon.ToString(row["vcItemGroup"]),
                                  }).ToList();
            }

            return listItemGroup.AsQueryable();
        }

        #endregion
    }
}