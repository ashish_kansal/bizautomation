﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiThrottle;

namespace BizAPI.Repository
{
    public class BizAPIThrottleLogger : IThrottleLogger
    {
        #region Member Variables

        private BizAPIRepository _bizAPIRepository;

        #endregion

        #region Constructor

        public BizAPIThrottleLogger()
        {
            _bizAPIRepository = new BizAPIRepository();
        }

        #endregion

        #region Public Methods

        public void Log(ThrottleLogEntry entry)
        {
            _bizAPIRepository.SaveThrottleLog(entry);
        }

        #endregion
    }
}