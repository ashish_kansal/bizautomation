﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models.Items;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("ItemGroup")]
    public class ItemGroupController : ApiController
    {
        private readonly IItemGroupRepository _iItemGroupRepository;

        public ItemGroupController(IItemGroupRepository iItemGroupRepository)
        {
            _iItemGroupRepository = iItemGroupRepository;
        }

        /// <summary>
        /// Gets list of item groups
        /// </summary>
        /// <returns>returns list of item groups as IQueryable<T></returns>
        [HttpGet]
        [Route("GetAll")]
        public IQueryable<ItemGroup> GetAll(string timeStamp)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iItemGroupRepository.GetAll(domainID);
        }
    }
}
