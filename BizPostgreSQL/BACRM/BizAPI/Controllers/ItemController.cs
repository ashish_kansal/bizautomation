﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models.Items;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("Item")]
    public class ItemController : ApiController
    {
        private readonly IItemRepository _iItemRepository;

        public ItemController(IItemRepository iItemRepository)
        {
            _iItemRepository = iItemRepository;
        }

        /// <summary>
        /// Gets list of item based
        /// </summary>
        /// <param name="timeStamp">datetime in utc string</param>
        /// <param name="pageSize">size of page</param>
        /// <param name="currentPage">current page number</param>
        /// <param name="pageSize">size of page</param>
        /// <param name="createdAfter">to get records created after particular date</param>
        /// <param name="modifiedAfter">to get records created after particular date</param>
        /// <returns>returns list of item as ItemList</returns>
        [HttpGet]
        [Route("GetAll")]
        public ItemList GetAll(string timeStamp, int currentPage = 0, int pageSize = 0, DateTime? createdAfter = null, DateTime? modifiedAfter = null)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iItemRepository.GetAll(domainID, currentPage, pageSize, createdAfter, modifiedAfter);
        }

        /// <summary>
        /// Gets list of item based on search text
        /// </summary>
        /// <param name="searchText">text to be search</param>
        /// <param name="currentPage">current page number</param>
        /// <param name="pageSize">size of page</param>
        /// <param name="createdAfter">to get records created after particular date</param>
        /// <param name="modifiedAfter">to get records created after particular date</param>
        /// <returns>returns list of item as ItemList</returns>
        [HttpGet]
        [Route("Search")]
        public ItemList Search(string timeStamp, string searchText, int currentPage = 0, int pageSize = 0, DateTime? createdAfter = null, DateTime? modifiedAfter = null)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iItemRepository.Search(domainID, searchText, currentPage, pageSize, createdAfter, modifiedAfter);
        }
    }
}