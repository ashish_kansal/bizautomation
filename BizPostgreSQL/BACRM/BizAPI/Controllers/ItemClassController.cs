﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models.Items;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("ItemClass")]
    public class ItemClassController : ApiController
    {
        private readonly IItemClassRepository _iItemClassRepository;

        public ItemClassController(IItemClassRepository iItemClassRepository)
        {
            _iItemClassRepository = iItemClassRepository;
        }

        /// <summary>
        /// Gets list of item class
        /// </summary>
        /// <returns>returns list of item class as IQueryable<T></returns>
        [HttpGet]
        [Route("GetAll")]
        public IQueryable<ItemClass> GetAll(string timeStamp)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iItemClassRepository.GetAll(domainID);
        }
    }
}
