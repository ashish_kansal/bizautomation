﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("UnitOfMeasure")]
    public class UnitOfMeasureController : ApiController
    {
        private readonly IUOMRepository _iUOMRepository;

        public UnitOfMeasureController(IUOMRepository iUOMRepository)
        {
            _iUOMRepository = iUOMRepository;
        }

        [System.Web.Http.HttpGet]
        [Route("GetAll")]
        public IEnumerable<UOM> GetAll(string timeStamp)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iUOMRepository.GetAll(domainID);
        }
    }
}
