﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("Country")]
    public class CountryController : ApiController
    {
        private readonly ICountryRepository _iCountryRepository;

        public CountryController(ICountryRepository iCountryRepository)
        {
            _iCountryRepository = iCountryRepository;
        }

        /// <summary>
        /// Gets list of country
        /// </summary>
        /// <returns>Returns list of country as IQueryable<Country></returns>
        [HttpGet]
        [Route("GetAll")]
        public IQueryable<Country> GetAll(string timeStamp)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iCountryRepository.GetAll(domainID);
        }
    }
}
