﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("Site")]
    public class SiteController : ApiController
    {
        private readonly ISiteRepository _iSiteRepository;

        public SiteController(ISiteRepository iSiteRepository)
        {
            _iSiteRepository = iSiteRepository;
        }

        /// <summary>
        /// Gets list of site categories
        /// </summary>
        /// <param name="siteID">Id of site whose categories needs to be retrive</param>
        /// <returns>Returns list of SiteCategory as IQueryable<Contact></returns>
        [HttpGet]
        [Route("GetCategories")]
        public IQueryable<SiteCategory> GetCategories(long siteID)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iSiteRepository.GetCategories(domainID, siteID);
        }
    }
}
