﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models.Items;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("ItemClassification")]
    public class ItemClassificationController : ApiController
    {
        private readonly IItemClassificationRepository _iItemClassificationRepository;

        public ItemClassificationController(IItemClassificationRepository iItemClassificationRepository)
        {
            _iItemClassificationRepository = iItemClassificationRepository;
        }

        /// <summary>
        /// Gets list of item classificatios
        /// </summary>
        /// <returns>returns list of item classificatios as IQueryable<T></returns>
        [HttpGet]
        [Route("GetAll")]
        public IQueryable<ItemClassification> GetAll(string timeStamp)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iItemClassificationRepository.GetAll(domainID);
        }
    }
}
