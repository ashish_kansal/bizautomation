﻿using BACRM.BusinessLogic.Common;
using BizAPI.Content;
using BizAPI.Filters;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BizAPI.Controllers
{
    [Authorize]
    [RoutePrefix("SalesOrder")]
    public class SalesOrderController : ApiController
    {
        private readonly IBizAPIRepository _iBizAPIRepository;
        private readonly ISalesOrderRepository _iSalesOrderRepository;

        public SalesOrderController(ISalesOrderRepository iSalesOrderRepository, IBizAPIRepository iBizAPIRepository)
        {
            _iBizAPIRepository = iBizAPIRepository;
            _iSalesOrderRepository = iSalesOrderRepository;
        }

        /// <summary>
        /// Creates sales order
        /// </summary>
        /// <param name="salesOrder">Sales Order object</param>
        [HttpPost]
        [Route("Create")]
        public HttpResponseMessage Create([FromBody]SalesOrder salesOrder)
        {
            BizAPIBusinessException exception;
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);

            if (salesOrder == null)
            {
                exception = new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.IncorrectSalesOrderObject, BizAPIBusinessExceptionMessage.IncorrectSalesOrderObject);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(exception));
            }
            else if (!salesOrder.IsNewCustomer)
            {
                if (salesOrder.Customer == null)
                {
                    exception = new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.CustomerIsRequired,BizAPIBusinessExceptionMessage.CustomerIsRequired);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(exception));
                }

                if (salesOrder.Customer.ID == 0 || string.IsNullOrEmpty(salesOrder.Customer.Name))
                {
                    exception = new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.CustomerIDAndNameRequired,BizAPIBusinessExceptionMessage.CustomerIDAndNameRequired);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(exception));
                }
            }
            else
            {
                if (salesOrder.NewCustomer == null)
                {
                    exception = new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.NewCustomerIsRequired, BizAPIBusinessExceptionMessage.CustomerIDAndNameRequired);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(exception));
                }
                else if (string.IsNullOrEmpty(salesOrder.NewCustomer.FirstName) || string.IsNullOrEmpty(salesOrder.NewCustomer.LastName) || salesOrder.NewCustomer.Relationship == null || salesOrder.NewCustomer.Relationship.ID == 0)
                {
                    exception = new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.NewCustomerFirstNameLastNameRelationshipRequired, BizAPIBusinessExceptionMessage.NewCustomerFirstNameLastNameRelationshipRequired);
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(exception));
                }
            }

            if (salesOrder.Items.Count == 0)
            {
                exception = new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.ItemsAreRequired, BizAPIBusinessExceptionMessage.ItemsAreRequired);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(exception));
            }

            int errorCode = _iBizAPIRepository.ValidateSalesOrder(domainID, salesOrder);

            if (errorCode != 0)
            {
                switch (errorCode)
                {
                    case 60000:
                        exception = new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.CustomerDoesNotExists, BizAPIBusinessExceptionMessage.CustomerDoesNotExists);
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(exception));
                    case 60001:
                        exception = new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.ContactDoesNotExists, BizAPIBusinessExceptionMessage.ContactDoesNotExists);
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(exception));
                    case 60002:
                        exception = new BizAPIBusinessException(BizAPIBusinessExceptionErrorCode.ItemsDoesNotExists, BizAPIBusinessExceptionMessage.ItemsDoesNotExists);
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(exception));
                }
            }



            exception = _iSalesOrderRepository.Add(domainID, salesOrder);
            if (exception != null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, JsonConvert.SerializeObject(exception));
            }

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, "value");
            return response;
        }

        /// <summary>
        /// Gets sales order created between given from and to date
        /// </summary>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns>returns list of sales orders as IQueryable<Item></returns>
        [HttpGet]
        [Route("Get")]
        public SalesOrderDetailList Get(string timeStamp, DateTime fromDate, DateTime? toDate = null, int currentPage = 0, int pageSize = 0)
        {
            return _iBizAPIRepository.Get(fromDate, toDate, currentPage, pageSize);
        }
    }
}
