﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models.Items;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("ItemShippingClass")]
    public class ItemShippingClassController : ApiController
    {
        private readonly IItemShippingClassRepository _iItemShippingClassRepository;

        public ItemShippingClassController(IItemShippingClassRepository iItemShippingClassRepository)
        {
            _iItemShippingClassRepository = iItemShippingClassRepository;
        }

        /// <summary>
        /// Gets list of item shipping class
        /// </summary>
        /// <returns>returns list of item shipping class as IQueryable<T></returns>
        [HttpGet]
        [Route("GetAll")]
        public IQueryable<ItemShippingClass> GetAll(string timeStamp)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iItemShippingClassRepository.GetAll(domainID);
        }
    }
}
