﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("Customer")]
    public class CustomerController : ApiController
    {
        private readonly IBizAPIRepository _iBizAPIRepository;

        public CustomerController(IBizAPIRepository iBizAPIRepository)
        {
            _iBizAPIRepository = iBizAPIRepository;
        }

        /// <summary>
        /// Gets all customer
        /// </summary>
        /// <param name="timeStamp">datetime in utc</param>
        /// <param name="currentPage">current page index optional</param>
        /// <param name="pageSize">pagesize optional</param>
        /// <returns>Returns list of customer as CustomerList</returns>
        [HttpGet]
        [Route("GetAll")]
        public CustomerList GetAll(string timeStamp, int currentPage = 0, int pageSize = 0)
        {
            return _iBizAPIRepository.GetCustomer("", currentPage, pageSize);
        }

        /// <summary>
        /// Gets all customer based on searchtext
        /// </summary>
        /// <param name="timeStamp">datetime in utc</param>
        /// <param name="currentPage">current page index optional</param>
        /// <param name="pageSize">pagesize optional</param>
        /// <param name="searchText">text to be search in name</param>
        /// <returns>Returns list of customer as CustomerList</returns>
        [HttpGet]
        [Route("Get")]
        public CustomerList Get(string timeStamp, string searchText, int currentPage = 0, int pageSize = 0)
        {
            return _iBizAPIRepository.GetCustomer(searchText, currentPage, pageSize);
        }
    }
}
