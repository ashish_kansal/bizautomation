﻿using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("Warehouse")]
    public class WarehouseController : ApiController
    {
        private readonly IWarehouseRepository _iWarehouseRepository;

        public WarehouseController(IWarehouseRepository iWarehouseRepository)
        {
            _iWarehouseRepository = iWarehouseRepository;
        }

        /// <summary>
        /// Gets list of warehouse of item
        /// </summary>
        /// <param name="itemCode">item code for which warehouse detail need to be fetched</param>
        /// <returns>Returns list of warehouse of item as IQueryable<Warehouse></returns>
        [HttpGet]
        [Route("Get")]
        public IQueryable<Warehouse> Get(string timeStamp, int itemCode)
        {
            return _iWarehouseRepository.Get(itemCode);
        }
    }
}
