﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("Relationship")]
    public class RelationshipController : ApiController
    {
        private readonly IRelationshipRepository _iRelationshipRepository;

        public RelationshipController(IRelationshipRepository iRelationshipRepository)
        {
            _iRelationshipRepository = iRelationshipRepository;
        }

        /// <summary>
        /// Gets list of relationships
        /// </summary>
        /// <returns>Returns list of relationships as IQueryable<Relationship></returns>
        [HttpGet]
        [Route("GetAll")]
        public IQueryable<Relationship> GetAll(string timeStamp)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iRelationshipRepository.GetAll(domainID);
        }
    }
}
