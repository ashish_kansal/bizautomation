﻿using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("Contact")]
    public class ContactController : ApiController
    {
        private readonly IContactRepository _iContactRepository;

        public ContactController(IContactRepository iContactRepository)
        {
            _iContactRepository = iContactRepository;
        }

        /// <summary>
        /// Gets list of contact of customer
        /// </summary>
        /// <param name="customerID">Id of customer whose contact needs to be retrive</param>
        /// <returns>Returns list of contact as IQueryable<Contact></returns>
        [HttpGet]
        [Route("Get")]
        public IQueryable<Contact> Get(string timeStamp, int customerID)
        {
            return _iContactRepository.Get(customerID);
        }
    }
}
