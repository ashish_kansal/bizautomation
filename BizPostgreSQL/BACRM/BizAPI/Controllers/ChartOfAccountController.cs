﻿using BACRM.BusinessLogic.Common;
using BizAPI.Filters;
using BizAPI.Models.Accounting;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("ChartOfAccount")]
    public class ChartOfAccountController : ApiController
    {
        private readonly IChartOfAccountRepository _iChartOfAccountRepository;

        public ChartOfAccountController(IChartOfAccountRepository iChartOfAccountRepository)
        {
            _iChartOfAccountRepository = iChartOfAccountRepository;
        }

        /// <summary>
        /// Gets list of COGS Account
        /// </summary>
        /// <returns>returns list of COGS Account as IQueryable<T></returns>
        [System.Web.Http.HttpGet]
        [Route("GetCOGSAccount")]
        public IQueryable<ChartOfAccount> GetCOGSAccount(string timeStamp)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iChartOfAccountRepository.GetCOGSChartAccount(domainID);
        }

        /// <summary>
        /// Gets list of Asset Account
        /// </summary>
        /// <returns>returns list of Asset Account as IQueryable<T></returns>
        [System.Web.Http.HttpGet]
        [Route("GetAssetAccount")]
        public IQueryable<ChartOfAccount> GetAssetAccount(string timeStamp)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iChartOfAccountRepository.GetAssetChartAccount(domainID);
        }

        /// <summary>
        /// Gets list of Income Account
        /// </summary>
        /// <returns>returns list of Income Account as IQueryable<T></returns>
        [System.Web.Http.HttpGet]
        [Route("GetIncomeAccount")]
        public IQueryable<ChartOfAccount> GetIncomeAccount(string timeStamp)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iChartOfAccountRepository.GetIncomeChartAccount(domainID);
        }
    }
}
