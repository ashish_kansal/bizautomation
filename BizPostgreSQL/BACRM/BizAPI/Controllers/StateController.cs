﻿using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace BizAPI.Controllers
{
    [System.Web.Http.Authorize]
    [RoutePrefix("State")]
    public class StateController : ApiController
    {
        private readonly IStateRepository _iStateRepository;

        public StateController(IStateRepository iStateRepository)
        {
            _iStateRepository = iStateRepository;
        }

        /// <summary>
        /// Gets list of state of country
        /// </summary>
        /// <param name="countryID">country id</param>
        /// <returns>Returns list of state of coutry as IQueryable<State></returns>
        [HttpGet]
        [Route("Get")]
        public IQueryable<State> Get(string timeStamp, int countryID)
        {
            long domainID = CCommon.ToLong(HttpContext.Current.Request.Headers["DomainID"]);
            return _iStateRepository.GetAll(countryID, domainID);
        }
    }
}
