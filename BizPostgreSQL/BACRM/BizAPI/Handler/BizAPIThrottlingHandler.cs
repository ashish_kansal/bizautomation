﻿using BizAPI.Models;
using BizAPI.Repository;
using BizAPI.RepositoryInterface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using WebApiThrottle;

namespace BizAPI.Handler
{
    public class BizAPIThrottlingHandler : DelegatingHandler
    {
        #region Member Variables

        private IBizAPIRepository _iBizAPIRepository;
        private static readonly object _processLocker = new object();

        #endregion

        #region Public Properties

        /// <summary>
        /// Creates a new instance of the <see cref="ThrottlingHandler"/> class.
        /// By default, the <see cref="QuotaExceededResponseCode"/> property 
        /// is set to 429 (Too Many Requests).
        /// </summary>
        public BizAPIThrottlingHandler()
        {
            QuotaExceededResponseCode = (HttpStatusCode)429;
            Repository = new BizAPIThrottleRepository();
        }

        /// <summary>
        /// Throttling rate limits policy
        /// </summary>
        public ThrottlePolicy Policy { get; set; }

        /// <summary>
        /// Throttle metrics storage
        /// </summary>
        public IThrottleRepository Repository { get; set; }

        /// <summary>
        /// Log traffic and blocked requests
        /// </summary>
        public IThrottleLogger Logger { get; set; }

        /// <summary>
        /// If none specifed the default will be: 
        /// API calls quota exceeded! maximum admitted {0} per {1}
        /// </summary>
        public string QuotaExceededMessage { get; set; }

        /// <summary>
        /// Gets or sets the value to return as the HTTP status 
        /// code when a request is rejected because of the
        /// throttling policy. The default value is 429 (Too Many Requests).
        /// </summary>
        public HttpStatusCode QuotaExceededResponseCode { get; set; }

        #endregion

        #region Protected Methods

        protected IPAddress GetClientIp(HttpRequestMessage request)
        {
            IPAddress ipAddress;

            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                var ok = IPAddress.TryParse(((HttpContextBase)request.Properties["MS_HttpContext"]).Request.UserHostAddress, out ipAddress);

                if (ok)
                {
                    return ipAddress;
                }
            }

            if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            {
                var ok = IPAddress.TryParse(((RemoteEndpointMessageProperty)request.Properties[RemoteEndpointMessageProperty.Name]).Address, out ipAddress);

                if (ok)
                {
                    return ipAddress;
                }
            }

            if (request.Properties.ContainsKey("MS_OwinContext"))
            {
                var ok = IPAddress.TryParse(((Microsoft.Owin.OwinContext)request.Properties["MS_OwinContext"]).Request.RemoteIpAddress, out ipAddress);

                if (ok)
                {
                    return ipAddress;
                }
            }


            return null;
        }
        protected virtual RequestIdentity SetIndentity(HttpRequestMessage request)
        {
            var entry = new RequestIdentity();


            if (request.Headers.Authorization != null)
            {
                var header = request.Headers.Authorization.Parameter;
                var apiKey = header.Split('#')[0];
                var signature = header.Split('#')[1];

                entry.ClientKey = apiKey;
                entry.ClientIp = GetClientIp(request).ToString();
                entry.Endpoint = request.RequestUri.AbsolutePath;
            }

            return entry;
        }
        protected virtual string ComputeThrottleKey(RequestIdentity requestIdentity, RateLimitPeriod period)
        {
            var keyValues = new List<string>()
                {
                    "throttle"
                };

            if (Policy.IpThrottling)
                keyValues.Add(requestIdentity.ClientIp);

            if (Policy.ClientThrottling)
                keyValues.Add(requestIdentity.ClientKey);

            if (Policy.EndpointThrottling)
                keyValues.Add(requestIdentity.Endpoint);

            keyValues.Add(period.ToString());

            var id = string.Join("_", keyValues);
            var idBytes = Encoding.UTF8.GetBytes(id);
            var hashBytes = new System.Security.Cryptography.SHA1Managed().ComputeHash(idBytes);
            var hex = BitConverter.ToString(hashBytes).Replace("-", "");
            return hex;
        }
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Method == HttpMethod.Options)
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Headers.Add("Access-Control-Allow-Headers", "x-requested-with,authorization,content-type");
                response.Headers.Add("Access-Control-Allow-Origin", "*");

                var tcs = new TaskCompletionSource<HttpResponseMessage>();
                tcs.SetResult(response);
                return tcs.Task;
            }

            if (request.Headers.Authorization != null)
            {
                var header = request.Headers.Authorization.Parameter;
                var apiKey = header.Split('#')[0];

                _iBizAPIRepository = new BizAPIRepository();
                List<BizAPIThrottlePolicy> listBizAPIThrottlePolicy = _iBizAPIRepository.Get(apiKey, GetClientIp(request).ToString());

                if (listBizAPIThrottlePolicy == null || listBizAPIThrottlePolicy.Count() == 0)
                {
                    var response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
                    response.Headers.Add("Access-Control-Allow-Headers", "x-requested-with,authorization,content-type");
                    response.Headers.Add("Access-Control-Allow-Origin", "*");

                    var tcs = new TaskCompletionSource<HttpResponseMessage>();
                    tcs.SetResult(response);
                    return tcs.Task;
                }
                else
                {
                    Policy = new ThrottlePolicy(2, 5, 10, 15, 20);
                    Policy.IpThrottling = true;
                    Policy.ClientThrottling = true;
                    Logger = new BizAPIThrottleLogger();
                    Dictionary<string, RateLimits> ipRules = new Dictionary<string, RateLimits>();
                    Dictionary<string, RateLimits> clientRules = new Dictionary<string, RateLimits>();

                    if (listBizAPIThrottlePolicy.Where(x => x.IsIPAddress).Count() > 0)
                    {
                        foreach (var key in listBizAPIThrottlePolicy.Where(x => x.IsIPAddress))
                        {
                            ipRules.Add(key.RateLimitKey, new RateLimits() { PerSecond = key.PerSecond, PerMinute = key.PerMinute, PerHour = key.PerHour, PerDay = key.PerDay, PerWeek = key.PerWeek });
                        }
                    }
                    Policy.IpRules = ipRules;

                    if (listBizAPIThrottlePolicy.Where(x => !x.IsIPAddress).Count() > 0)
                    {
                        foreach (var key in listBizAPIThrottlePolicy.Where(x => !x.IsIPAddress))
                        {
                            clientRules.Add(key.RateLimitKey, new RateLimits() { PerSecond = key.PerSecond, PerMinute = key.PerMinute, PerHour = key.PerHour, PerDay = key.PerDay, PerWeek = key.PerWeek });
                        }
                    }
                    Policy.ClientRules = clientRules;

                    if (!Policy.IpThrottling && !Policy.ClientThrottling && !Policy.EndpointThrottling)
                        return base.SendAsync(request, cancellationToken);

                    var identity = SetIndentity(request);
                    if (IsWhitelisted(identity))
                        return base.SendAsync(request, cancellationToken);

                    TimeSpan timeSpan = TimeSpan.FromSeconds(1);

                    var rates = Policy.Rates.AsEnumerable();
                    if (Policy.StackBlockedRequests)
                    {
                        //all requests including the rejected ones will stack in this order: day, hour, min, sec
                        //if a client hits the hour limit then the minutes and seconds counters will expire and will eventually get erased from cache
                        rates = Policy.Rates.Reverse();
                    }

                    //apply policy
                    //the IP rules are applied last and will overwrite any client rule you might defined
                    foreach (var rate in rates)
                    {
                        var rateLimitPeriod = rate.Key;
                        var rateLimit = rate.Value;

                        switch (rateLimitPeriod)
                        {
                            case RateLimitPeriod.Second:
                                timeSpan = TimeSpan.FromSeconds(1);
                                break;
                            case RateLimitPeriod.Minute:
                                timeSpan = TimeSpan.FromMinutes(1);
                                break;
                            case RateLimitPeriod.Hour:
                                timeSpan = TimeSpan.FromHours(1);
                                break;
                            case RateLimitPeriod.Day:
                                timeSpan = TimeSpan.FromDays(1);
                                break;
                            case RateLimitPeriod.Week:
                                timeSpan = TimeSpan.FromDays(7);
                                break;
                        }

                        //apply custom rate limit for clients that will override endpoint limits
                        if (Policy.ClientRules != null && Policy.ClientRules.Keys.Contains(identity.ClientKey))
                        {
                            var limit = Policy.ClientRules[identity.ClientKey].GetLimit(rateLimitPeriod);
                            if (limit > 0) rateLimit = limit;
                        }

                        //enforce ip rate limit as is most specific 
                        string ipRule = null;
                        if (Policy.IpRules != null && ContainsIp(Policy.IpRules.Keys.ToList(), identity.ClientIp, out ipRule))
                        {
                            var limit = Policy.IpRules[ipRule].GetLimit(rateLimitPeriod);
                            if (limit > 0) rateLimit = limit;
                        }

                        //increment counter
                        string requestId;
                        var throttleCounter = ProcessRequest(identity, timeSpan, rateLimitPeriod, rateLimit, apiKey, out requestId);

                        if (throttleCounter.Timestamp + timeSpan < DateTime.UtcNow)
                            continue;

                        //apply endpoint rate limits
                        if (Policy.EndpointRules != null)
                        {
                            var rules = Policy.EndpointRules.Where(x => identity.Endpoint.Contains(x.Key.ToLowerInvariant())).ToList();
                            if (rules.Any())
                            {
                                //get the lower limit from all applying rules
                                var customRate = (from r in rules let rateValue = r.Value.GetLimit(rateLimitPeriod) select rateValue).Min();

                                if (customRate > 0)
                                {
                                    rateLimit = customRate;
                                }
                            }
                        }


                        //check if limit is reached
                        if (rateLimit > 0 && throttleCounter.TotalRequests > rateLimit)
                        {
                            string message;
                            if (!string.IsNullOrEmpty(QuotaExceededMessage))
                                message = QuotaExceededMessage;
                            else
                                message = "BizAPI calls quota exceeded! maximum admitted {0} per {1}.";

                            //break execution
                            return QuotaExceededResponse(request,
                                string.Format(message, rateLimit, rateLimitPeriod),
                                QuotaExceededResponseCode,
                                RetryAfterFrom(throttleCounter.Timestamp, rateLimitPeriod));
                        }
                    }

                    //log request
                    if (Logger != null) 
                        Logger.Log(ComputeLogEntry(identity,request));
                }
            }

            return base.SendAsync(request, cancellationToken).ContinueWith(task =>
            {
                var response = task.Result;
                if (!response.Headers.Contains("Access-Control-Allow-Headers") && !response.Headers.Contains("Access-Control-Allow-Origin"))
                {
                    response.Headers.Add("Access-Control-Allow-Headers", "x-requested-with,authorization,content-type");
                    response.Headers.Add("Access-Control-Allow-Origin", "*");
                }

                return response;
            });
        }

        #endregion

        #region Private Methods

        private bool IsWhitelisted(RequestIdentity requestIdentity)
        {
            if (Policy.IpThrottling)
                if (Policy.IpWhitelist != null && ContainsIp(Policy.IpWhitelist, requestIdentity.ClientIp))
                    return true;

            if (Policy.ClientThrottling)
                if (Policy.ClientWhitelist != null && Policy.ClientWhitelist.Contains(requestIdentity.ClientKey))
                    return true;

            if (Policy.EndpointThrottling)
                if (Policy.EndpointWhitelist != null && Policy.EndpointWhitelist.Any(x => requestIdentity.Endpoint.Contains(x.ToLowerInvariant())))
                    return true;

            return false;
        }

        private bool ContainsIp(List<string> ipRules, string clientIp)
        {
            var ip = IPAddress.Parse(clientIp);
            if (ipRules != null && ipRules.Any())
            {
                foreach (var rule in ipRules)
                {
                    var range = new IPAddressRange(rule);
                    if (range.Contains(ip)) return true;
                }
            }

            return false;
        }

        private string RetryAfterFrom(DateTime timestamp, RateLimitPeriod period)
        {
            var secondsPast = Convert.ToInt32((DateTime.UtcNow - timestamp).TotalSeconds);
            var retryAfter = 1;
            switch (period)
            {
                case RateLimitPeriod.Minute:
                    retryAfter = 60;
                    break;
                case RateLimitPeriod.Hour:
                    retryAfter = 60 * 60;
                    break;
                case RateLimitPeriod.Day:
                    retryAfter = 60 * 60 * 24;
                    break;
                case RateLimitPeriod.Week:
                    retryAfter = 60 * 60 * 24 * 7;
                    break;
            }
            retryAfter = retryAfter > 1 ? retryAfter - secondsPast : 1;
            return retryAfter.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }

        private bool ContainsIp(List<string> ipRules, string clientIp, out string rule)
        {
            rule = null;
            var ip = IPAddress.Parse(clientIp);
            if (ipRules != null && ipRules.Any())
            {
                foreach (var r in ipRules)
                {
                    var range = new IPAddressRange(r);
                    if (range.Contains(ip))
                    {
                        rule = r;
                        return true;
                    }
                }
            }

            return false;
        }

        private ThrottleCounter ProcessRequest(RequestIdentity requestIdentity, TimeSpan timeSpan, RateLimitPeriod period, long rateLimit, string bizAPIAccessKey, out string id)
        {
            long totalRequests = 0;

            var throttleCounter = new ThrottleCounter()
            {
                Timestamp = DateTime.UtcNow,
                TotalRequests = 1
            };

            id = ComputeThrottleKey(requestIdentity, period);

            //serial reads and writes
            lock (_processLocker)
            {
                var entry = Repository.FirstOrDefault(id);
                if (entry.HasValue)
                {
                    //entry has not expired
                    if (entry.Value.Timestamp + timeSpan >= DateTime.UtcNow)
                    {
                        //increment request count
                        totalRequests = entry.Value.TotalRequests + 1;

                        //deep copy
                        throttleCounter = new ThrottleCounter
                        {
                            Timestamp = entry.Value.Timestamp,
                            TotalRequests = totalRequests
                        };

                    }
                }

                //Do not update entry if limit is reached
                if (rateLimit > 0 && totalRequests <= rateLimit)
                {
                    //stores: id (string) - timestamp (datetime) - total (long)
                    Repository.Save(bizAPIAccessKey+ ":" + id, throttleCounter, timeSpan);
                }
            }

            return throttleCounter;
        }

        private Task<HttpResponseMessage> QuotaExceededResponse(HttpRequestMessage request, string message, HttpStatusCode responseCode, string retryAfter)
        {
            BizAPIBusinessException exception = new BizAPIBusinessException("429", message);
            var response = request.CreateResponse(responseCode, JsonConvert.SerializeObject(exception));
            response.Headers.Add("Access-Control-Allow-Headers", "x-requested-with,authorization,content-type");
            response.Headers.Add("Access-Control-Allow-Origin", "*");
            response.Headers.Add("Retry-After", new string[] { retryAfter });
            return Task.FromResult<HttpResponseMessage>(response);
        }

        private ThrottleLogEntry ComputeLogEntry(RequestIdentity identity, HttpRequestMessage request)
        {
            return new ThrottleLogEntry
                    {
                        ClientIp = identity.ClientIp,
                        ClientKey = identity.ClientKey,
                        Endpoint = identity.Endpoint,
                        LogDate = DateTime.UtcNow
                    };
        }

        #endregion
    }
}