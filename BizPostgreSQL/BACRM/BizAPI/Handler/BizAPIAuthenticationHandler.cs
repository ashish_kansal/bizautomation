﻿using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Common;
using BizAPI.Models;
using BizAPI.Repository;
using BizAPI.RepositoryInterface;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace BizAPI.Handler
{
    public class BizAPIAuthenticationHandler : DelegatingHandler
    {
        #region Member Variables

        private IBizAPIRepository _iBizAPIRepository;

        #endregion

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Method == HttpMethod.Options)
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Headers.Add("Access-Control-Allow-Headers", "x-requested-with,authorization,content-type");
                response.Headers.Add("Access-Control-Allow-Origin", "*");

                var tcs = new TaskCompletionSource<HttpResponseMessage>();
                tcs.SetResult(response);
                return tcs.Task;
            }

            if (request.Headers.Authorization != null)
            {
                string timeStamp = string.Empty;

                if (request.Method == HttpMethod.Post)
                {
                    var result = request.Content.ReadAsStringAsync().Result;
                    var salesOrder = JsonConvert.DeserializeObject<SalesOrder>(result);
                    timeStamp = salesOrder.TimeStamp;
                }
                else
                {
                    IEnumerable<KeyValuePair<string, string>> listQueryString = request.GetQueryNameValuePairs();
                    if (listQueryString.Where(x => x.Key == "timeStamp").Count() > 0)
                    {
                        timeStamp = listQueryString.Where(x => x.Key == "timeStamp").First().Value;
                    }
                }

                if (!string.IsNullOrEmpty(timeStamp) && IsValidTimeStamp(timeStamp))
                {
                    var header = request.Headers.Authorization.Parameter;
                    var apiKey = header.Split('#')[0];
                    var signature = header.Split('#')[1];

                    _iBizAPIRepository = new BizAPIRepository();
                    DataSet dsUserDetails = _iBizAPIRepository.GetUser(apiKey);

                    if (dsUserDetails != null && dsUserDetails.Tables.Count > 0 && dsUserDetails.Tables[0].Rows.Count > 0)
                    {
                        DataRow dr = dsUserDetails.Tables[0].Rows[0];
                        string email = CCommon.ToString(dr["vcEmailID"]);
                        string secretKey = CCommon.ToString(dr["vcBizAPISecretKey"]);


                        ASCIIEncoding encoding = new ASCIIEncoding();
                        byte[] keyByte = encoding.GetBytes(secretKey);
                        byte[] messageBytes = encoding.GetBytes(email + timeStamp);
                        HMACMD5 hmacmd5 = new HMACMD5(keyByte);
                        byte[] hashmessage = hmacmd5.ComputeHash(messageBytes);
                        var signatureToCompare = Convert.ToBase64String(hashmessage);

                        if (signature == signatureToCompare)
                        {


                            DataTable dtTable = dsUserDetails.Tables[0];

                            var currentPrincipal = new GenericPrincipal(new GenericIdentity(CCommon.ToString(dtTable.Rows[0]["numUserID"])), null);
                            Thread.CurrentPrincipal = currentPrincipal;
                            HttpContext.Current.User = currentPrincipal;

                            if (HttpContext.Current.Request.Headers["DomainID"] == null)
                            {
                                HttpContext.Current.Request.Headers.Add("DomainID", CCommon.ToString(dtTable.Rows[0]["numDomainID"]));
                            }
                            else
                            {
                                HttpContext.Current.Request.Headers["DomainID"] = CCommon.ToString(dtTable.Rows[0]["numDomainID"]);
                            }

                            if (HttpContext.Current.Request.Headers["UserContactID"] == null)
                            {
                                HttpContext.Current.Request.Headers.Add("UserContactID", CCommon.ToString(dtTable.Rows[0]["numUserDetailId"]));
                            }
                            else
                            {
                                HttpContext.Current.Request.Headers["UserContactID"] = CCommon.ToString(dtTable.Rows[0]["numUserDetailId"]);
                            }

                            if (HttpContext.Current.Request.Headers["DefCountry"] == null)
                            {
                                HttpContext.Current.Request.Headers.Add("DefCountry", CCommon.ToString(dtTable.Rows[0]["numDefCountry"]));
                            }
                            else
                            {
                                HttpContext.Current.Request.Headers["DefCountry"] = CCommon.ToString(dtTable.Rows[0]["numDefCountry"]);
                            }

                            if (HttpContext.Current.Request.Headers["PriceBookDiscount"] == null)
                            {
                                HttpContext.Current.Request.Headers.Add("PriceBookDiscount", CCommon.ToString(dtTable.Rows[0]["tintPriceBookDiscount"]));
                            }
                            else
                            {
                                HttpContext.Current.Request.Headers["PriceBookDiscount"] = CCommon.ToString(dtTable.Rows[0]["tintPriceBookDiscount"]);
                            }

                            if (HttpContext.Current.Request.Headers["MultiCurrency"] == null)
                            {
                                HttpContext.Current.Request.Headers.Add("MultiCurrency", CCommon.ToString(dtTable.Rows[0]["bitMultiCurrency"]));
                            }
                            else
                            {
                                HttpContext.Current.Request.Headers["MultiCurrency"] = CCommon.ToString(dtTable.Rows[0]["bitMultiCurrency"]);
                            }

                            if (HttpContext.Current.Request.Headers["BaseCurrencyID"] == null)
                            {
                                HttpContext.Current.Request.Headers.Add("BaseCurrencyID", CCommon.ToString(dtTable.Rows[0]["numCurrencyID"]));
                            }
                            else
                            {
                                HttpContext.Current.Request.Headers["BaseCurrencyID"] = CCommon.ToString(dtTable.Rows[0]["numCurrencyID"]);
                            }

                            if (HttpContext.Current.Request.Headers["ContactName"] == null)
                            {
                                HttpContext.Current.Request.Headers.Add("ContactName", CCommon.ToString(dtTable.Rows[0]["ContactName"]));
                            }
                            else
                            {
                                HttpContext.Current.Request.Headers["ContactName"] = CCommon.ToString(dtTable.Rows[0]["ContactName"]);
                            }

                            if (HttpContext.Current.Request.Headers["UserEmail"] == null)
                            {
                                HttpContext.Current.Request.Headers.Add("UserEmail", CCommon.ToString(dtTable.Rows[0]["vcEmailID"]));
                            }
                            else
                            {
                                HttpContext.Current.Request.Headers["UserEmail"] = CCommon.ToString(dtTable.Rows[0]["vcEmailID"]);
                            }
                        }
                    }
                }
            }

            return base.SendAsync(request, cancellationToken).ContinueWith(task =>
            {
                var response = task.Result;
                if (!response.Headers.Contains("Access-Control-Allow-Headers") && !response.Headers.Contains("Access-Control-Allow-Origin"))
                {
                    response.Headers.Add("Access-Control-Allow-Headers", "x-requested-with,authorization,content-type");
                    response.Headers.Add("Access-Control-Allow-Origin", "*");
                }

                return response;
            });
        }

        private static bool IsValidTimeStamp(string timestampString)
        {
            DateTime timestamp;

            bool isDateTime = DateTime.TryParse(timestampString, out timestamp);

            if (!isDateTime)
                return false;

            var now = DateTime.UtcNow;

            // TimeStamp should not be in 5 minutes behind or greater then 5 minute to avoid replay attack
            if (timestamp.ToUniversalTime() < now.AddMinutes(-CCommon.ToInteger(ConfigurationManager.AppSettings["RquestTimeFrame"])) || timestamp.ToUniversalTime() > now.AddMinutes(CCommon.ToInteger(ConfigurationManager.AppSettings["RquestTimeFrame"])))
                return false;

            return true;
        }
    }
}