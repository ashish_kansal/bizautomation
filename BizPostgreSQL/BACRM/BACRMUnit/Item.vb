﻿'Imports System.Text
'Imports BACRM.BusinessLogic.Opportunities
'Imports BACRM.BusinessLogic.Item
'Imports BACRM.BusinessLogic.Common
'Imports BACRM.BusinessLogic.Leads
'Imports BACRM.BusinessLogic.Accounting
'Imports BACRM.BusinessLogic.Contacts
'Imports BACRM.BusinessLogic.Admin
'Imports BACRM.BusinessLogic.Prospects
'Imports BACRM.BusinessLogic.Projects
'Imports BACRM.BusinessLogic.Alerts
'Imports BACRM.BusinessLogic.TimeAndExpense
'Imports BACRM.BusinessLogic.Documents

'<TestClass()>
'Public Class Item
'    Dim UserCntId As Integer = 1
'    Dim DomainID As Integer = 1

'    Dim COGSChartAcntId As Integer = 1223
'    Dim AssetChartAcntId As Integer = 1874
'    Dim IncomeChartAcntId As Integer = 1219

'    <TestMethod()>
'    Public Sub ItemTest()

'        ''Inventory : P    Non-Inventory: N   Service: S

'        'Inventory Item
'        ManageItems(197617, "P", False, False, False, False)

'        'Kit
'        ManageItems(197617, "P", True, False, False, False)

'        'Assembly
'        ManageItems(197617, "P", True, True, False, False)

'        'Serialized
'        ManageItems(197617, "P", False, False, True, False)

'        'LotNo
'        ManageItems(197617, "P", False, False, False, True)

'        'Non-Inventory Item
'        ManageItems(197617, "N", False, False, False, False)

'        'Service
'        ManageItems(197617, "S", False, False, False, False)
'    End Sub

'    Public Sub ManageItems(ByVal lngItemCode As Integer, ByVal ItemType As String, ByVal KitParent As Boolean, ByVal Assembly As Boolean, ByVal bitSerialized As Boolean, ByVal bitLotNo As Boolean)
'        Dim gotTheRightException As Boolean = False

'        Try
'            Dim objItems As New CItems
'            Dim ds As New DataSet
'            Dim dr As DataRow
'            Dim randObj As New Random

'            With objItems
'                .ItemCode = lngItemCode
'                .ItemName = "Unit Test_" & randObj.Next
'                .ItemDesc = "Unit Test Testing"
'                .ItemType = ItemType
'                .ListPrice = 0
'                .ItemClassification = 0
'                .ModelID = ""
'                .KitParent = KitParent
'                .bitSerialized = bitSerialized
'                .bitLotNo = bitLotNo

'                .Manufacturer = ""
'                .BarCodeID = CCommon.ToInteger(0)

'                .SKU = ""
'                .ItemGroupID = 0
'                .AverageCost = 0
'                .LabourCost = 0

'                .BaseUnit = 0
'                .PurchaseUnit = 0
'                .SaleUnit = 0

'                If lngItemCode > 0 Or (ItemType = "P" And lngItemCode = 0) Then

'                    objItems.ItemCode = lngItemCode
'                    ds = objItems.GetItemWareHouses()

'                    Dim dtInventory As DataTable = ds.Tables(0)

'                    'Op_Flag=1 : Add ,Op_Flag=2 : Update ,Op_Flag=3 : Delete

'                    ''Insert
'                    'dr = dtInventory.NewRow
'                    'dr("numWareHouseID") = "58"
'                    'dr("OnHand") = "20"
'                    'dr("Reorder") = "2"
'                    'dr("Location") = "0"
'                    'dr("Price") = "50"
'                    'dr("SKU") = "0"
'                    'dr("BarCode") = "0"
'                    'dr("Op_Flag") = "1"
'                    'dtInventory.Rows.Add(dr)

'                    ''Delete
'                    'For Each dr In ds.Tables(0).Rows
'                    '    dr("Op_Flag") = "3" 
'                    'Next

'                    ''Update 
'                    For Each dr In ds.Tables(0).Rows
'                        dr("Op_Flag") = "2"
'                        dr("OnHand") = "25"
'                    Next

'                    ds.Tables(0).TableName = "WareHouse"
'                    ds.AcceptChanges()

'                    If lngItemCode > 0 And (bitSerialized Or bitLotNo) Then
'                        Dim dtSerial As DataTable = ds.Tables(0)

'                        ' ''Add
'                        'dr = dtSerial.NewRow
'                        'dr("numWareHouseItemID") = ""
'                        'dr("vcSerialNo") = IIf(bitSerialized, "Serial ", "Lot ") & randObj.Next
'                        'dr("Comments") = ""

'                        'If bitLotNo Then
'                        '    dr("numQty") = "3"
'                        '    dr("OldQty") = "0"
'                        'Else
'                        '    dr("numQty") = "1"
'                        '    dr("OldQty") = "0"
'                        'End If
'                        'dr("Op_Flag") = "1"
'                        'dtSerial.Rows.Add(dr)

'                        ' ''Delete
'                        'For Each dr In ds.Tables(0).Rows
'                        '    If dr("Op_Flag") = "1" Then
'                        '        dr("Op_Flag") = "4"
'                        '    Else
'                        '        dr("Op_Flag") = "3"
'                        '        dr("numQty") = "0"
'                        '    End If
'                        'Next

'                        ' ''Update 
'                        'For Each dr In ds.Tables(0).Rows
'                        '    dr("Op_Flag") = "2"

'                        '    dr("vcSerialNo") = IIf(bitSerialized, "Edit Serial ", "Edit Lot ") & randObj.Next

'                        '    dr("numQty") = IIf(bitLotNo, "10", "1")
'                        'Next

'                        ds.AcceptChanges()
'                        ds.Tables(1).TableName = "SerializedItems"
'                    End If

'                    .strFieldList = ds.GetXml
'                End If

'                .UserCntId = UserCntId
'                .DomainID = DomainID

'                If .ItemType = "S" Then
'                    .COGSChartAcntId = 0
'                    .AssetChartAcntId = 0
'                Else
'                    .COGSChartAcntId = COGSChartAcntId
'                    .AssetChartAcntId = AssetChartAcntId
'                End If

'                .IncomeChartAcntId = IncomeChartAcntId
'                .Weight = 0
'                .Height = 0
'                .Length = 0
'                .Width = 0
'                .FreeShipping = False
'                .AllowBackOrder = True

'                If KitParent = True Then
'                    If lngItemCode > 0 Then
'                        Dim dtChildItem As New DataTable
'                        dtChildItem.Columns.Add("ItemKitID")
'                        dtChildItem.Columns.Add("ChildItemID")
'                        dtChildItem.Columns.Add("QtyItemsReq")
'                        dtChildItem.Columns.Add("numWarehouseItmsID")

'                        dtChildItem.Columns.Add("numUOMId")

'                        dr = dtChildItem.NewRow
'                        dr("ItemKitID") = lngItemCode
'                        dr("ChildItemID") = "2"
'                        dr("numUOMId") = "0"
'                        dr("QtyItemsReq") = "2"
'                        dr("numWarehouseItmsID") = "154273"
'                        dtChildItem.Rows.Add(dr)

'                        dr = dtChildItem.NewRow
'                        dr("ItemKitID") = lngItemCode
'                        dr("ChildItemID") = "4651"
'                        dr("QtyItemsReq") = "1"

'                        dtChildItem.Rows.Add(dr)

'                        dtChildItem.TableName = "Table1"
'                        ds.Tables.Add(dtChildItem)
'                        .strChildItems = ds.GetXml
'                        ds.Tables.Remove(dtChildItem)
'                    End If
'                    .ShowDeptItem = False
'                    .ShowDeptItemDesc = False
'                    .CalPriceBasedOnIndItems = True
'                    .Assembly = Assembly
'                End If
'            End With

'            objItems.ManageItemsAndKits()

'            gotTheRightException = IIf(objItems.ItemCode > 0, True, False)

'            'Throw New Exception("Error")
'        Catch ex As Exception
'            Dim s As String = ex.Message
'            'gotTheRightException = s.Contains("Error1")
'        End Try

'        Assert.IsTrue(gotTheRightException)
'    End Sub
'End Class
