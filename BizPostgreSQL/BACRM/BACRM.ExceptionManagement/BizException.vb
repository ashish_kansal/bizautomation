'**********************************************************************************
' BiZException.vb
' CHANGE CONTROL:
'	
'	AUTHOR 			DATE				VERSION	CHANGES	KEYSTRING:
'	Goyal	8 Mar 2005		
'**********************************************************************************

Imports System.IO
Imports System.Runtime.Serialization
Imports System.Data.SQLClient
Imports System.Configuration
Imports System.Globalization
Imports System.Text
Imports BACRM.ExceptionManagement.Interfaces


Namespace BACRM.ExceptionManagement

    '**********************************************************************************
    ' Module Name  : Framework
    ' Module Type  : Structure MessaqgeRow
    ' Description  : This structure will be used to send the Message across application boundaries
    ' Notes		   :
    '**********************************************************************************
    Public Structure SMessageRow

        Private m_strErrorCode As String
        Private m_strMessage As String
        Private m_strInnerException As String

#Region "Public Properties"

        ' Error Code
        Public Property ErrorCode() As String
            Get
                Return m_strErrorCode
            End Get
            Set(ByVal Value As String)
                m_strErrorCode = Value
            End Set
        End Property

        ' Error Message
        Public Property Message() As String
            Get
                Return m_strMessage
            End Get
            Set(ByVal Value As String)
                m_strMessage = Value
            End Set
        End Property

        ' Inner Error/Exception
        Public Property InnerError() As String
            Get
                Return m_strInnerException
            End Get
            Set(ByVal Value As String)
                m_strInnerException = Value
            End Set
        End Property

#End Region

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal i As Integer
        '**********************************************************************************
        Public Sub New(ByVal i As Integer)
            m_strErrorCode = ""
            m_strMessage = ""
            m_strInnerException = ""
        End Sub


       

       

        '**********************************************************************************
        ' Name         : NotEqualOp
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : Boolean	
        ' Parameters   : ByVal objFirst As SMessageRow
        '                ByVal objSecond As SMessageRow       
        '**********************************************************************************
        Public Overloads Shared Function NotEqualOp(ByVal objFirst As SMessageRow, ByVal objSecond As SMessageRow) As Boolean
            Return Not objFirst.Equals(objSecond)
        End Function    'NotEqualOp

    End Structure

    '**********************************************************************************
    ' Module Name  : Framework
    ' Module Type  : CBaseException
    ' Description  : Base Exception Class
    ' Notes        :
    'CBaseException class is derived from ApplicationException and also implements IExceptionPublisher.
    'ApplicationException is thrown by a user program, not by the common language runtime. 
    'If you are designing an application that needs to create its own exceptions, derive 
    'from the ApplicationException class. 

    'ApplicationException extends Exception, but does not add new functionality. 
    'This exception is provided as means to differentiate between exceptions defined by 
    'applications versus exceptions defined by the system.

    'ApplicationException does not provide information as to the cause of the exception. 
    'In most scenarios, instances of this class should not be thrown. In cases where this 
    'class is instantiated, a human-readable message describing the error should be passed 
    'to the constructor.
    '**********************************************************************************
    <Serializable()> Public Class BizBaseException
        Inherits ApplicationException
        Implements IExceptionPublisher

        Private m_msgRow As New SMessageRow(0)     '// 0 is dummy
        Private strLineNo As String = ""
        Private strProcedureName As String = ""
        Private Const _errorKey As String = "ErrorMessages"
        Private Const _connectionKey As String = "ConnectionString"


        ' Message Row
        Public ReadOnly Property MsgRow() As SMessageRow
            Get
                Return m_msgRow
            End Get
        End Property

#Region "Constructors"



        '**********************************************************************************
        ' Name         : New	
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : N/A
        ' Description  : Constructor       
        '**********************************************************************************
        Public Sub New()
            MyBase.New()
        End Sub    'New

        '**********************************************************************************
        ' Name         : New	
        ' Type         : Sub
        ' Scope        : Protected
        ' Returns      : N/A
        ' Parameters   : ByVal info As SerializationInfo
        '                ByVal context As StreamingContext       
        '**********************************************************************************
        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
        End Sub

        '**********************************************************************************
        ' Name         : New	
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String)
            MyBase.New(msgCode)
            BuildExceptionObject(msgCode, Nothing, Nothing, Nothing)

        End Sub


        '**********************************************************************************
        ' Name         : New	
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal innerException As Exception
        '                ByVal lineNo As Integer       
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal innerException As Exception, ByVal lineNo As Integer, ByVal procedureName As String)
            MyBase.New(msgCode, innerException)

            strLineNo = lineNo.ToString(CultureInfo.CurrentCulture)
            strProcedureName = procedureName

            BuildExceptionObject(msgCode, Nothing, innerException, Nothing)
        End Sub    'New

        '**********************************************************************************
        ' Name         : New	
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal innerException As Exception       
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal innerException As Exception)
            MyBase.New(msgCode, innerException)
            BuildExceptionObject(msgCode, Nothing, innerException, Nothing)
        End Sub    'New


        '**********************************************************************************
        ' Name         : New	
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal message As String
        '                ByVal innerException As Exception        
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal message As String, ByVal innerException As Exception)
            MyBase.New(msgCode, innerException)
            BuildExceptionObject(msgCode, message, innerException, Nothing)
        End Sub    'New


        '**********************************************************************************
        ' Name         : New	
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal aList As ArrayList
        '                ByVal innerException As Exception       
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal aList As ArrayList, ByVal innerException As Exception)
            MyBase.New(msgCode, innerException)
            BuildExceptionObject(msgCode, Nothing, innerException, aList)
        End Sub    'New

#End Region

        '**********************************************************************************
        ' Name         : BuildExceptionObject
        ' Type         : Sub
        ' Scope        : Private
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal message As String
        '                ByVal innerException As Exception
        '                ByVal aList As ArrayList
        '**********************************************************************************
        Private Sub BuildExceptionObject(ByVal msgCode As String, ByVal message As String, ByVal innerException As Exception, ByVal aList As ArrayList)
            If Not (innerException Is Nothing) Then
                m_msgRow.InnerError = innerException.ToString()
            End If
            m_msgRow.ErrorCode = msgCode
            If (m_msgRow.ErrorCode <> "") Then
                Try
                    FetchErrorMessageFromDB(msgCode)
                Catch ex As Exception

                End Try
            End If
        End Sub


        '/ <summary>
        '/ This function will perform the construction activities for the
        '/ exception object
        '/ </summary>
        '**********************************************************************************
        ' Name         : SetErrorMessage
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal strMessage As String
        '                ByVal strMsgCode As String
        '**********************************************************************************
        Public Overridable Sub SetErrorMessage(ByVal strMessage As String, ByVal strMsgCode As String)

            If strMsgCode Is Nothing Then
                m_msgRow.ErrorCode = ""
            Else
                m_msgRow.ErrorCode = strMsgCode
            End If

            If strMessage Is Nothing Then
                m_msgRow.Message = ""
            Else
                m_msgRow.Message = strMessage
            End If
        End Sub    'SetErrorMessage

        '**********************************************************************************
        ' Name         : AppendTextToError	
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal strText As String
        '                ByVal bAtEnd As Boolean
        '**********************************************************************************
        Public Overridable Sub AppendTextToError(ByVal strText As String, ByVal bAtEnd As Boolean)
            If strText Is Nothing Then
                Return
            End If

            If bAtEnd Then
                m_msgRow.Message += " " + strText
            Else
                m_msgRow.Message = strText + " " + m_msgRow.Message
            End If
        End Sub    'AppendTextToError

        '**********************************************************************************
        ' Name         : ShowErrorMessage
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : String	
        ' Parameters   : ByVal bFormat As Boolean

        '**********************************************************************************
        Public Overridable Function ShowErrorMessage(ByVal bFormat As Boolean) As String
            If Not bFormat Then
                Return m_msgRow.Message
            Else
                Return "Error Code: " + m_msgRow.ErrorCode + " Message: " + m_msgRow.Message
            End If
        End Function    'ShowErrorMessage

        '**********************************************************************************
        ' Name         : ShowStackTrace
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : String	
        ' Parameters   : N/A
        ' Description  : This function will return the stack trace of the inner exception

        '**********************************************************************************
        Public Overridable Function ShowStackTrace() As String
            Dim CLex As String = m_msgRow.InnerError
            Dim j As Integer = CLex.IndexOf(ControlChars.Lf)

            CLex = CLex.Replace(ControlChars.Lf, " ")
            CLex = CLex.Replace(ControlChars.Cr, " ")

            m_msgRow.InnerError = CLex

            Return CLex
        End Function    'ShowStackTrace

        '**********************************************************************************
        ' Name         : FetchErrorCode
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : String	
        ' Parameters   : N/A
        ' Description  : This function returns the error code

        '**********************************************************************************
        Public Overridable Function FetchErrorCode() As String
            Return m_msgRow.ErrorCode
        End Function    'FetchErrorCode

        '**********************************************************************************
        ' Name         : FetchInnerException
        ' Type         : Function
        ' Scope        : Public
        ' Returns      : String	
        ' Parameters   : N/A
        ' Description  : Function returns inner exception object in the form of string
        '**********************************************************************************
        Public Overridable Function FetchInnerException() As String
            Return m_msgRow.InnerError
        End Function    'GetInnerException

        ' Inner Exception from message row
        Public ReadOnly Property GetInnerException() As String
            Get
                Return m_msgRow.InnerError
            End Get
        End Property

        '**********************************************************************************
        ' Name      : FetchErrorMessageFromDB
        ' Type      : Sub
        ' Scope     : Public
        ' Returns   : 
        ' Parameters: ByVal strErrorCode As String
        ' Description: Gets the Error Message for given error code 
        ' Notes     : N/A
        '**********************************************************************************
        Private Overloads Sub FetchErrorMessageFromDB(ByVal strErrorCode As String)
            Dim strErrorPath As String

            Try
                Dim dsErrorMessage As New DataSet
                Dim drErrorRow As DataRow

                strErrorPath = ConfigurationManager.AppSettings("ErrorMessageXMLPath")
                dsErrorMessage.ReadXml(strErrorPath)
                Dim pk(0) As DataColumn

                'Add the Primary key to the dataset
                pk(0) = dsErrorMessage.Tables(0).Columns("Code")
                dsErrorMessage.Tables(0).PrimaryKey = pk

                drErrorRow = dsErrorMessage.Tables(0).Rows.Find(strErrorCode)

                If Not (drErrorRow Is Nothing) Then
                    'Set the Error Message
                    m_msgRow.Message = CStr(drErrorRow.Item("Message"))
                Else
                    m_msgRow.Message = "No message found for the code"
                End If
            Catch e As Exception
                m_msgRow.ErrorCode = "BIZ002"
                m_msgRow.Message = "Error while getting info related to another error."
                m_msgRow.InnerError = e.Message
            End Try
        End Sub












        '**********************************************************************************
        ' Name      : Publish
        ' Type      : Sub
        ' Scope     : Public Overridable
        ' Returns   : N/A
        ' Parameters: N/A
        ' Description: Persists or publishes the exception as per defined manner in a text file.
        '               If file does not exist, create the same before publishing exception.
        ' Notes     : Implements IExceptionPublisher.Publish
        '**********************************************************************************
        Public Overridable Sub Publish() Implements IExceptionPublisher.Publish
            Try
                Dim objWriter As StreamWriter
                Dim strLogFilePath As String = ConfigurationManager.AppSettings("LogFilePath")

                If Not File.Exists(strLogFilePath) Then
                    objWriter = File.CreateText(strLogFilePath)
                    objWriter.Write(ControlChars.Cr + ControlChars.Lf)
                    objWriter.Write("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString())
                    objWriter.WriteLine("  :{0}", "NEW LOG FILE CREATED")
                    objWriter.Flush()
                    objWriter.Close()
                End If

                objWriter = File.AppendText(strLogFilePath)

                objWriter.Write(ControlChars.Cr + ControlChars.Lf)
                objWriter.Write("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString())
                objWriter.WriteLine("  :{0}", m_msgRow.Message)

                objWriter.Flush()
                objWriter.Close()
            Catch systemEx As Exception
                Dim i As Integer = 10    'Just a dummy statement.
                Dim strErr As String = systemEx.Message    'Just a dummy statement.
            End Try    'do nothing and let exception not kill your application.
        End Sub   'Publish

#Region "Properties"

        ' Error Code from message row
        Public ReadOnly Property ErrorCode() As String
            Get
                Return m_msgRow.ErrorCode
            End Get
        End Property

        ' Error Message from message row
        Public ReadOnly Property ErrorMessage() As String
            Get
                Return m_msgRow.Message
            End Get
        End Property

        ' Inner Error/Exception from message row
        Public ReadOnly Property InnerError() As String
            Get
                Return m_msgRow.InnerError
            End Get
        End Property

#End Region

    End Class




    '**********************************************************************************
    ' Module Name  : Framework
    ' Module Type  : CLogicalException
    ' Description  : Logical Exception Class
    ' Notes        :
    ' CLogicalException Class. Use this for exception specific to business activites.
    ' It is derived from BizBaseException applications exception hierarchy.
    '**********************************************************************************
    <Serializable()> Public Class BizLogicalException : Inherits BizBaseException

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : N/A
        ' Description  : Constructor
        '**********************************************************************************
        Public Sub New()
            MyBase.New()
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Protected
        ' Returns      : N/A
        ' Parameters   : ByVal info As SerializationInfo
        '                ByVal context As StreamingContext
        '               
        ' Description  : Constructor        
        '**********************************************************************************
        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '               
        ' Description  : Constructor
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String)
            MyBase.New(msgCode)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal innerException As Exception
        '                ByVal lineNo As Integer
        '                ByVal procedureName As String
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal innerException As Exception, ByVal lineNo As Integer, ByVal procedureName As String)
            MyBase.New(msgCode, innerException, lineNo, procedureName)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal innerException As Exception
        '               
        ' Description  : Constructor       
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal innerException As Exception)
            MyBase.New(msgCode, innerException)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal message As String
        '                ByVal innerException As Exception
        '               
        ' Description  : Constructor        
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal message As String, ByVal innerException As Exception)
            MyBase.New(msgCode, innerException)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal aList As ArrayList
        '                ByVal innerException As Exception        
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal aList As ArrayList, ByVal innerException As Exception)
            MyBase.New(msgCode, innerException)
        End Sub

    End Class

    '**********************************************************************************
    ' Module Name  : Framework
    ' Module Type  : CDatabaseException
    ' Description  : Database Exception Class
    ' Notes        :
    ' CDatabaseException Class. Use this for exception specific to database activites.
    ' It is derived from BizBaseException applications exception hierarchy.
    '**********************************************************************************
    <Serializable()> Public Class BizDatabaseException : Inherits BizBaseException

        Private m_strParameters As String
        Private m_arrSqlParameters() As SqlParameter

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : N/A
        ' Description  : Constructor
        '**********************************************************************************
        Public Sub New()
            MyBase.New()
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Protected
        ' Returns      : N/A
        ' Parameters   : ByVal info As SerializationInfo
        '                ByVal context As StreamingContext
        '               
        ' Description  : Constructor
        
        '**********************************************************************************
        Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
            MyBase.New(info, context)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '               
        ' Description  : Constructor
        
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String)
            MyBase.New(msgCode)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal innerException As Exception
        '                ByVal lineNo As Integer
        '                ByVal procedureName As String
        '               
        ' Description  : Constructor
        
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal innerException As Exception, ByVal lineNo As Integer, ByVal procedureName As String)
            MyBase.New(msgCode, innerException, lineNo, procedureName)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal innerException As Exception
        '               
        ' Description  : Constructor
       '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal innerException As Exception)
            MyBase.New(msgCode, innerException)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal message As String
        '                ByVal innerException As Exception
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal message As String, ByVal innerException As Exception)
            MyBase.New(msgCode, innerException)
        End Sub

        '**********************************************************************************
        ' Name         : New
        ' Type         : Sub
        ' Scope        : Public
        ' Returns      : N/A
        ' Parameters   : ByVal msgCode As String
        '                ByVal aList As ArrayList
        '                ByVal innerException As Exception
        '**********************************************************************************
        Public Sub New(ByVal msgCode As String, ByVal aList As ArrayList, ByVal innerException As Exception)
            MyBase.New(msgCode, innerException)
        End Sub

        ' Query Parameters
        Public ReadOnly Property QueryParameters() As String
            Get
                Return m_strParameters
            End Get
        End Property

        ' SQL Parameters
        Public ReadOnly Property SqlParameters() As SqlParameter()
            Get
                Return m_arrSqlParameters
            End Get
        End Property

       
    End Class

End Namespace