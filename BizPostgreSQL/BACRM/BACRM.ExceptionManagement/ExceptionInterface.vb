'**********************************************************************************
' <ExceptionInterface.vb>
' 
' 	CHANGE CONTROL:
'	AUTHOR: Goel   	DATE:Mar 08, 2005		VERSION	CHANGES	KEYSTRING:
'**********************************************************************************

Namespace BACRM.ExceptionManagement.Interfaces

    '**********************************************************************************
    ' Module Name  : Framework
    ' Module Type  : Interface IExceptionPublisher
    ' Description  : Interfaces defined for Exception component
    ' Notes		   :
    '**********************************************************************************
    Public Interface IExceptionPublisher
        Sub Publish()

    End Interface

End Namespace