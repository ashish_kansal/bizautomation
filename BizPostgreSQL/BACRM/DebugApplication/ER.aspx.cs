﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;

using BACRM.BusinessLogic;
using BACRMBUSSLOGIC.BussinessLogic;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Contract;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.WebAPI;

using DebugApplication.AMWS_Communicator;
//using AmazonSource;

using eBay.Service.Core.Soap;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DebugApplication
{
    public partial class Ebay : System.Web.UI.Page
    {
        private string AmazonOrderFilePath = ConfigurationManager.AppSettings["AmazonOrderFilePath"];
        private string AmazonClosedOrdersPath = ConfigurationManager.AppSettings["AmazonClosedOrders"];

        private string EBayOrderFilePath = ConfigurationManager.AppSettings["EBayOrderFilePath"];
        private string EBayClosedOrders = ConfigurationManager.AppSettings["EBayClosedOrders"];

        private string GoogleOrderFilePath = ConfigurationManager.AppSettings["GoogleOrderFilePath"];
        private string GoogleClosedOrders = ConfigurationManager.AppSettings["GoogleClosedOrders"];

        private string MagentoOrderFilePath = ConfigurationManager.AppSettings["MagentoOrderFilePath"];
        private string MagentoClosedOrders = ConfigurationManager.AppSettings["MagentoClosedOrders"];

        private string AmazonMWSAccessKey = ConfigurationManager.AppSettings["AmazonMWSAccessKey"];
        private string AmazonMWSSecretKey = ConfigurationManager.AppSettings["AmazonMWSSecretKey"];

        eBayClient objeBayClient = null;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Imports Complete Product Listing from various Marketplaces and Adds to BizDatabase
        /// </summary>
        public void GetProductListingsCommand()
        {
            WebAPI objWebAPI = new WebAPI();
            long DomainID = 0;
            int WebApiId = 0;
            int OrderStatus = 0;
            int WareHouseID = 0;
            long BizDocId = 0;
            long RecordOwner = 0;
            long AssignTo = 0;
            long RelationshipId = 0;
            long ProfileId = 0;
            long UserContactID = 0;

            string Source = "";
            string ReportType = "", ReportOptions = "";

            try
            {
                objWebAPI.Mode = 2;
                DataTable dtWebAPI = objWebAPI.GetWebApi();
                foreach (DataRow dr in dtWebAPI.Rows)
                {
                    try
                    {
                        if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                        {
                            DomainID = CCommon.ToLong(dr["numDomainId"]);
                            WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                            Source = CCommon.ToString(dr["vcProviderName"]);
                            if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                            {
                                //objAmazonClient = new AmazonClient();
                                //ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                //objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                //objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                //objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
                                //objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);
                                //objSvcConfigInfo.FeedsServiceURL = objAmazonClient.GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                //objSvcConfigInfo.OrdersServiceURL = objAmazonClient.GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
                                //objSvcConfigInfo.ProductServiceURL = objAmazonClient.GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));

                                //OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                //WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                //BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                //RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                //AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                //RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                //ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                //UserContactID = CCommon.ToLong(dr["vcFifteenthFldValue"]);
                                //objSvcConfigInfo.ServiceURL_Type = 2;
                                //string ListingRequest = CCommon.ToString((dr["vcFourteenthFldValue"]));
                                //if (!string.IsNullOrEmpty(ListingRequest))
                                //{
                                //    string[] Requesttype = ListingRequest.Split('|');
                                //    if (!string.IsNullOrEmpty(Requesttype[0]) & Requesttype[0] == "1") //Request for Product Listing
                                //    {
                                //        if (!string.IsNullOrEmpty(Requesttype[1]))
                                //        {
                                //            ReportType = "ActiveListing";
                                //           ReportOptions = "";

                                //            objAmazonClient.RequestReport(objSvcConfigInfo, DomainID, WebApiId, ReportType, ReportOptions);

                                //            objWebAPI.DomainID = DomainID;
                                //            objWebAPI.UserContactID = UserContactID;
                                //            objWebAPI.WebApiId = WebApiId;
                                //            objWebAPI.FlagItemImport = "0|1";// +ListingReqType;
                                //            objWebAPI.ManageWebApiItemImport();
                                //        }
                                //    }
                                //}
                            }
                            else if (WebApiId == (int)WebAPIList.EBayUS) //E-Bay US)
                            {
                                objeBayClient = new eBayClient();
                                string eBaySite = CCommon.ToString(dr["vcFirstFldValue"]);
                                string Token = CCommon.ToString(dr["vcEighthFldValue"]);
                                RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                UserContactID = CCommon.ToLong(dr["vcFifteenthFldValue"]);

                                string ListingRequest = CCommon.ToString((dr["vcFourteenthFldValue"]));
                                if (!string.IsNullOrEmpty(ListingRequest))
                                {
                                    string[] Requesttype = ListingRequest.Split('|');
                                    if (!string.IsNullOrEmpty(Requesttype[0]) & Requesttype[0] == "1")
                                    {
                                        if (!string.IsNullOrEmpty(Requesttype[1]))
                                        {
                                            int ListingReqType = CCommon.ToInteger(Requesttype[1]);
                                            objeBayClient.GetEbayProductListings(DomainID, WebApiId, RecordOwner, UserContactID, WareHouseID, Token, eBaySite, ListingReqType);
                                            objWebAPI.DomainID = DomainID;
                                            objWebAPI.UserContactID = UserContactID;
                                            objWebAPI.WebApiId = WebApiId;
                                            objWebAPI.FlagItemImport = "0|0";
                                            objWebAPI.ManageWebApiItemImport();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (FaultException<error> ex)
                    {
                        error er = ex.Detail;
                        GeneralFunctions.WriteMessage(DomainID, "ERR", er.msg);
                        GeneralFunctions.WriteMessage(DomainID, "ERR", er.request);
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainID, "ERR", ex.Message);
                        GeneralFunctions.WriteMessage(DomainID, "ERR", ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                GeneralFunctions.WriteMessage(DomainID, "ERR", ex.Message);
                GeneralFunctions.WriteMessage(DomainID, "ERR", ex.StackTrace);
            }
        }

        protected void btnGetEbayItems_Click(object sender, System.EventArgs e)
        {
            GetProductListingsCommand();
        }

        #region Helper Functions

        /// <summary>
        /// Enum for List of WebAPIs supported by BizAutomation
        /// </summary>
        public enum WebAPIList
        {
            //Mapping the WebApiId in WebApiDetail Table
            AmazonUS = 2,
            EBayUS = 5,
            GoogleUS = 7,
            Magento = 8
        }

        /// <summary>
        /// Deletes Log Files older than the number of days specified
        /// And Deletes Log Files thats been in Trash Folder for morethan 30 Days.
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="DaysOld">DaysOld</param>
        private void DeleteLogFiles(long DomainId, int DaysOld)
        {
            try
            {
                CreateLogFiles objLogFiles = new CreateLogFiles();
                objLogFiles.DeleteLogFiles(DomainId, DaysOld);
                objLogFiles = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// GetRoutingRulesUserCntID
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="dbFieldName">dbFieldName</param>
        /// <param name="FieldValue">FieldValue</param>
        /// <returns></returns>
        private long GetRoutingRulesUserCntID(long DomainId, string dbFieldName, string FieldValue)
        {
            AutoRoutingRules objAutoRoutRles = new AutoRoutingRules();
            DataTable dtAutoRoutingRules = new DataTable();
            DataRow drAutoRow = null;
            DataSet ds = new DataSet();
            try
            {
                objAutoRoutRles.DomainID = DomainId;
                if (dtAutoRoutingRules.Columns.Count == 0)
                {
                    dtAutoRoutingRules.TableName = "Table";
                    dtAutoRoutingRules.Columns.Add("vcDbColumnName");
                    dtAutoRoutingRules.Columns.Add("vcDbColumnText");
                }
                dtAutoRoutingRules.Rows.Clear();
                drAutoRow = dtAutoRoutingRules.NewRow();
                drAutoRow["vcDbColumnName"] = dbFieldName;
                drAutoRow["vcDbColumnText"] = FieldValue;
                dtAutoRoutingRules.Rows.Add(drAutoRow);
                ds.Tables.Add(dtAutoRoutingRules);
                objAutoRoutRles.strValues = ds.GetXml();
                ds.Tables.Remove(ds.Tables[0]);
                return objAutoRoutRles.GetRecordOwner();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the Top WebApiOrderId for the given DomainId and WebApiId
        /// </summary>
        /// <param name="DomainID">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <returns>WebApiOrderId</returns>
        /// 

        //private string GetTopAmazonOrderID(long DomainID, int WebApiId)
        //{
        //    string AmazonOrderId = "";
        //    WebAPI objWebAPI = new WebAPI();
        //    objWebAPI.WebApiId = WebApiId;
        //    objWebAPI.DomainID = DomainID;
        //    AmazonOrderId = objWebAPI.GetTopWebApiOrderID();
        //    return AmazonOrderId;
        //}

        /// <summary>
        /// Gets the Top WebApiOrderReportId for the given DomainId and WebApiId
        /// </summary>
        /// <param name="DomainID">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <returns>WebApiOrderReportId</returns>
        private DataTable GetTopOrderReportID(long DomainID, int WebApiId)
        {
            DataTable dtTopReport;
            string OrderReportID = "";
            WebAPI objWebAPI = new WebAPI();
            objWebAPI.WebApiId = WebApiId;
            objWebAPI.DomainID = DomainID;
            objWebAPI.Mode = 0;
            dtTopReport = objWebAPI.GetTopAPIOrderReport();
            return dtTopReport;
        }

        private DataTable GetTopImportOrderRequest(long DomainID, int WebApiId)
        {
            DataTable dtTopOrderImportRequest;
            WebAPI objWebAPI = new WebAPI();
            objWebAPI.WebApiId = WebApiId;
            objWebAPI.DomainID = DomainID;
            objWebAPI.Mode = 1;
            dtTopOrderImportRequest = objWebAPI.GetTopAPIOrderReport();
            return dtTopOrderImportRequest;
        }


        #endregion Helper Functions

        #region API Settings Validation

        private void ValidateAPISettings()
        {

            long DomainID = 0;
            int WebApiId = 0;
            DataTable dtItems;
            string Source = "", ErrorMsg = "";

            WebAPI objWebAPI = new WebAPI();
            try
            {
                objWebAPI.Mode = 2;
                DataTable dtWebAPI = objWebAPI.GetWebApi();
                foreach (DataRow dr in dtWebAPI.Rows)
                {
                    string MarketplaceName = CCommon.ToString(dr["vcProviderName"]);
                    string LogMessage = "";
                    bool IsValid = true;
                    try
                    {
                        if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                        {
                            DomainID = CCommon.ToLong(dr["numDomainId"]);
                            WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                            objWebAPI.DomainID = DomainID;
                            objWebAPI.WebApiId = WebApiId;
                            objWebAPI.DateCreated = "";
                            objWebAPI.DateModified = "";
                            dtItems = objWebAPI.GetItemsCOAApi();

                            if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US
                            {
                                ////LogMessage = "Amazon Marketplace";
                                //string MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
                                //string MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
                                //string SiteCode = CCommon.ToString(dr["vcFifthFldValue"]);
                                //string MerchantToken = CCommon.ToString(dr["vcEighthFldValue"]);
                                //int AdminID = CCommon.ToInteger(dr["numAdminID"]);

                                //if (string.IsNullOrEmpty(MerchantID))
                                //{
                                //    IsValid = false;
                                //    LogMessage += " Merchant ID is empty.<br/>";
                                //}
                                //if (string.IsNullOrEmpty(MarketPlaceID))
                                //{
                                //    IsValid = false;
                                //    LogMessage += " MarketPlace ID is empty.<br/>";
                                //}
                                //if (string.IsNullOrEmpty(SiteCode))
                                //{
                                //    IsValid = false;
                                //    LogMessage += " Site Code is empty.<br/>";
                                //}
                                //if (string.IsNullOrEmpty(MerchantToken))
                                //{
                                //    IsValid = false;
                                //    LogMessage += " Merchant Token is empty.<br/>";
                                //}
                                //if (dtItems.Rows.Count > 0)
                                //{
                                //    string ItemTypeName = "";
                                //    string ItemCode = "";
                                //    string ItemName = "";
                                //    string ItemType = "";

                                //    foreach (DataRow drItem in dtItems.Rows)
                                //    {
                                //        ItemCode = CCommon.ToString(drItem["numItemCode"]);
                                //        ItemName = CCommon.ToString(drItem["vcItemName"]);
                                //        ItemType = CCommon.ToString(drItem["charItemType"]);
                                //        if (ItemType == "P")
                                //            ItemTypeName = "Inventory Item";

                                //        if (ItemType == "N")
                                //            ItemTypeName = "Non-Inventory Item";

                                //        if (ItemType == "S")
                                //            ItemTypeName = "Service Item";
                                //        LogMessage += "Chart Of Account mapping error for Item Type : " + ItemTypeName
                                //            + ", Item Code : " + ItemCode + ", Item Name : " + ItemName + " ";
                                //    }
                                //    IsValid = false;
                                //}

                                //BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                //string Message = "";
                                //bool SettingsCheck = false;
                                //SettingsCheck = objBizCommonFunctions.ValidateApiSetting(dr, out Message);
                                //if (!SettingsCheck)
                                //{
                                //    IsValid = false;
                                //    LogMessage += Message;
                                //}
                                //if (!IsValid)
                                //{
                                //    Message = "Your Amazon Online Marketplace Integration is Disabled on " + DateTime.Now.ToString("ddMMMyyy HH:MM") +
                                //         Environment.NewLine + "Due to.. " + Environment.NewLine;
                                //    Message += LogMessage;
                                //    GeneralFunctions.WriteMessage(DomainID, "ERR", Message);
                                //    CCommon objCommon = new CCommon();
                                //    objCommon.Mode = 40;
                                //    objCommon.DomainID = DomainID;
                                //    objCommon.UpdateRecordID = WebApiId;
                                //    objCommon.UpdateValueID = 0;
                                //    objCommon.UpdateSingleFieldValue();
                                //    BizCommonFunctions.SendMailMarketplaceDisabled(DomainID, AdminID, MarketplaceName, LogMessage);
                                //}
                            }
                            else if (WebApiId == (int)WebAPIList.EBayUS) //E-Bay US
                            {
                                string SiteCode = CCommon.ToString(dr["vcFirstFldValue"]);
                                string PayPalAddress = CCommon.ToString(dr["vcSecondFldValue"]);
                                string MarketplaceCode = CCommon.ToString(dr["vcFifthFldValue"]);
                                string MerchantToken = CCommon.ToString(dr["vcEighthFldValue"]);
                                int AdminID = CCommon.ToInteger(dr["numAdminID"]);

                                if (string.IsNullOrEmpty(SiteCode))
                                {
                                    IsValid = false;
                                    LogMessage += " E-Bay Site Code is empty.<br/>";
                                }
                                if (string.IsNullOrEmpty(PayPalAddress))
                                {
                                    IsValid = false;
                                    LogMessage += " E-BayPaypal Address is empty.<br/>";
                                }
                                if (string.IsNullOrEmpty(MarketplaceCode))
                                {
                                    IsValid = false;
                                    LogMessage += " E-Bay Marketplace Code is empty.<br/>";
                                }
                                if (string.IsNullOrEmpty(MerchantToken))
                                {
                                    IsValid = false;
                                    LogMessage += " E-Bay Merchant Token is empty.<br/>";
                                }

                                BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                string Message = "";
                                bool SettingsCheck = false;
                                SettingsCheck = objBizCommonFunctions.ValidateApiSetting(dr, out Message);
                                if (!SettingsCheck)
                                {
                                    IsValid = false;
                                    LogMessage += Message;
                                }
                                if (!IsValid)
                                {
                                    Message = "Your e-Bay Online Marketplace Integration is Disabled on " + DateTime.Now.ToString("ddMMMyyy HH:MM") +
                                         Environment.NewLine + "Due to.. " + Environment.NewLine;
                                    Message += LogMessage;
                                    GeneralFunctions.WriteMessage(DomainID, "ERR", Message);
                                    CCommon objCommon = new CCommon();
                                    objCommon.Mode = 40;
                                    objCommon.DomainID = DomainID;
                                    objCommon.UpdateRecordID = WebApiId;
                                    objCommon.UpdateValueID = 0;
                                    objCommon.UpdateSingleFieldValue();
                                    BizCommonFunctions.SendMailMarketplaceDisabled(DomainID, AdminID, MarketplaceName, LogMessage);
                                }
                            }
                            else if (WebApiId == (int)WebAPIList.GoogleUS) //Google US
                            {
                                string SiteCode = CCommon.ToString(dr["vcFirstFldValue"]);
                                string MerchantAccountId = CCommon.ToString(dr["vcSecondFldValue"]);
                                string LoginEmail = CCommon.ToString(dr["vcThirdFldValue"]);
                                string Password = CCommon.ToString(dr["vcFourthFldValue"]);
                                string CheckOutMerchantId = CCommon.ToString(dr["vcFifthFldValue"]);
                                string CheckOutMerchantKey = CCommon.ToString(dr["vcsixthFldValue"]);
                                int AdminID = CCommon.ToInteger(dr["numAdminID"]);

                                if (string.IsNullOrEmpty(SiteCode))
                                {
                                    IsValid = false;
                                    LogMessage += " Google Site Code is empty.<br/>";
                                }
                                if (string.IsNullOrEmpty(MerchantAccountId))
                                {
                                    IsValid = false;
                                    LogMessage += " Google Merchant Center Account Id is empty.<br/>";
                                }
                                if (string.IsNullOrEmpty(LoginEmail))
                                {
                                    IsValid = false;
                                    LogMessage += " Google Merchant Center login email is empty.<br/>";
                                }
                                if (string.IsNullOrEmpty(Password))
                                {
                                    IsValid = false;
                                    LogMessage += " Google Merchant Center Password is empty.<br/>";
                                }
                                if (string.IsNullOrEmpty(CheckOutMerchantId))
                                {
                                    IsValid = false;
                                    LogMessage += " Google Checkout Merchant Id is empty.<br/>";
                                }
                                if (string.IsNullOrEmpty(CheckOutMerchantKey))
                                {
                                    IsValid = false;
                                    LogMessage += " Google Checkout Merchant Key is empty.<br/>";
                                }

                                BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                string Message = "";
                                bool SettingsCheck = false;
                                SettingsCheck = objBizCommonFunctions.ValidateApiSetting(dr, out Message);
                                if (!SettingsCheck)
                                {
                                    IsValid = false;
                                    LogMessage += Message;
                                }
                                if (!IsValid)
                                {
                                    Message = "Your Google Online Marketplace Integration is Disabled on " + DateTime.Now.ToString("ddMMMyyy HH:MM") +
                                         Environment.NewLine + "Due to.. " + Environment.NewLine;
                                    Message += LogMessage;
                                    GeneralFunctions.WriteMessage(DomainID, "ERR", Message);
                                    CCommon objCommon = new CCommon();
                                    objCommon.Mode = 40;
                                    objCommon.DomainID = DomainID;
                                    objCommon.UpdateRecordID = WebApiId;
                                    objCommon.UpdateValueID = 0;
                                    objCommon.UpdateSingleFieldValue();
                                    BizCommonFunctions.SendMailMarketplaceDisabled(DomainID, AdminID, MarketplaceName, LogMessage);
                                }
                            }
                            else if (WebApiId == (int)WebAPIList.Magento) //Magento
                            {
                                string DomainURL = CCommon.ToString(dr["vcFirstFldValue"]);
                                string ApiUserId = CCommon.ToString(dr["vcSecondFldValue"]);
                                string ApiUserKey = CCommon.ToString(dr["vcThirdFldValue"]);
                                int AdminID = CCommon.ToInteger(dr["numAdminID"]);

                                if (string.IsNullOrEmpty(DomainURL))
                                {
                                    IsValid = false;
                                    LogMessage += " Magento Domain URL is empty.<br/>";
                                }
                                if (string.IsNullOrEmpty(ApiUserId))
                                {
                                    IsValid = false;
                                    LogMessage += " Magento API User Name is empty.<br/>";
                                }
                                if (string.IsNullOrEmpty(ApiUserKey))
                                {
                                    IsValid = false;
                                    LogMessage += " Magento API User Key is empty.<br/>";
                                }

                                BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                string Message = "";
                                bool SettingsCheck = false;
                                SettingsCheck = objBizCommonFunctions.ValidateApiSetting(dr, out Message);
                                if (!SettingsCheck)
                                {
                                    IsValid = false;
                                    LogMessage += Message;
                                }
                                if (!IsValid)
                                {
                                    Message = "Your Magento Online Marketplace Integration is Disabled on " + DateTime.Now.ToString("ddMMMyyy HH:MM") +
                                        Environment.NewLine + "Due to.. " + Environment.NewLine;
                                    Message += LogMessage;
                                    GeneralFunctions.WriteMessage(DomainID, "ERR", Message);
                                    CCommon objCommon = new CCommon();
                                    objCommon.Mode = 40;
                                    objCommon.DomainID = DomainID;
                                    objCommon.UpdateRecordID = WebApiId;
                                    objCommon.UpdateValueID = 0;
                                    objCommon.UpdateSingleFieldValue();
                                    BizCommonFunctions.SendMailMarketplaceDisabled(DomainID, AdminID, MarketplaceName, LogMessage);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorMsg = "Error while Validating Marketplace Settings for " + MarketplaceName + ".,";
                        GeneralFunctions.WriteMessage(DomainID, "ERR", ErrorMsg + " -" + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                GeneralFunctions.WriteMessage(DomainID, "ERR", ErrorMsg + " -" + ex.Message);
            }
        }

        #endregion API Settings Validation


    }
}