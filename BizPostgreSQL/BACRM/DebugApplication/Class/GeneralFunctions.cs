﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Data;

using BACRM.BusinessLogic.Common;

namespace DebugApplication
{
    class GeneralFunctions
    {

        /// <summary>
        /// Writes Meessage to Log File
        /// </summary>
        /// <param name="DomainId">Domain ID</param>
        /// <param name="Type">Message Type : "Error, Log"</param>
        /// <param name="Message">Message</param>
        public static void WriteMessage(long DomainId, string Type, string Message)
        {
            try
            {
                CreateLogFiles objLogFiles = new CreateLogFiles();
                objLogFiles.WriteLog(DomainId, Type, Message);
                objLogFiles = null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Converts a given delimited file into a dataset. 
        /// Assumes that the first line    
        /// of the text file contains the column names.
        /// </summary>
        /// <param name="File">The name of the file to open</param>    
        /// <param name="TableName">The name of the 
        /// Table to be made within the DataSet returned</param>
        /// <param name="delimiter">The string to delimit by</param>
        /// <returns></returns>  
        public static DataSet GetDataSetFromDelimitedFlatFile(string File, string delimiter)
        {
            //The DataSet to Return
            DataSet result = new DataSet();
            string TableName = "Table1";
            //Open the file in a stream reader.
            StreamReader s = new StreamReader(File);

            //Split the first line into the columns       
            string[] columns = s.ReadLine().Split(delimiter.ToCharArray());

            //Add the new DataTable to the RecordSet
            result.Tables.Add(TableName);

            //Cycle the colums, adding those that don't exist yet 
            //and sequencing the one that do.
            foreach (string col in columns)
            {
                bool added = false;
                string next = "";
                int i = 0;
                while (!added)
                {
                    //Build the column name and remove any unwanted characters.
                    string columnname = col + next;
                    columnname = columnname.Replace("#", "");
                    columnname = columnname.Replace("'", "");
                    columnname = columnname.Replace("&", "");

                    //See if the column already exists
                    if (!result.Tables[TableName].Columns.Contains(columnname))
                    {
                        //if it doesn't then we add it here and mark it as added
                        result.Tables[TableName].Columns.Add(columnname);
                        added = true;
                    }
                    else
                    {
                        //if it did exist then we increment the sequencer and try again.
                        i++;
                        next = "_" + i.ToString();
                    }
                }
            }

            //Read the rest of the data in the file.        
            string AllData = s.ReadToEnd();

            //Split off each row at the Carriage Return/Line Feed
            //Default line ending in most windows exports.  
            //You may have to edit this to match your particular file.
            //This will work for Excel, Access, etc. default exports.
            string[] rows = AllData.Split("\r\n".ToCharArray());

            //Now add each row to the DataSet        
            foreach (string r in rows)
            {
                //Split the row at the delimiter.
                string[] items = r.Split(delimiter.ToCharArray());

                //Add the item
                result.Tables[TableName].Rows.Add(items);
            }

            //Return the imported data.        
            return result;
        }

        /// <summary>
        /// Creates Directory for each Domain (if Not Exists)
        /// </summary>
        /// <param name="_Path">Directory Path</param>
        /// <param name="_DomainId">Domain Id</param>
        /// <returns>Specific Path for the Domain</returns>
        public static string GetPath(string _Path, long _DomainId)
        {
            try
            {
                // string DirectoryPath = _Path + _DomainId.ToString("00000") + "\\";
                string DirectoryPath = CCommon.GetDocumentPhysicalPath(_DomainId) + _Path + "\\";
                if (!Directory.Exists(DirectoryPath))
                {
                    Directory.CreateDirectory(DirectoryPath);//Creates Directory 
                }
                return DirectoryPath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Serialization/Deserialization

        /// <summary>
        /// Serialize an object into an XML string
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="obj">Object</param>
        /// <returns>xml string</returns>
        public static string SerializeToXml<T>(T obj)
        {
            try
            {
                System.IO.StringWriter Output = new System.IO.StringWriter(new System.Text.StringBuilder());
                XmlSerializer ser = new XmlSerializer(typeof(T));
                ser.Serialize(Output, obj);
                return Output.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Xml Serializer Serialize the Object to MemormStream by XMLTextWriter
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="obj">Object</param>
        /// <returns>Memory Stream</returns>
        public static MemoryStream SerializeToStream<T>(T obj)
        {
            try
            {
                MemoryStream memorystream = new MemoryStream();
                XmlWriter writer = new XmlTextWriter(memorystream, Encoding.Unicode);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                // Serialize using the XmlTextWriter.
                serializer.Serialize(writer, obj);
                memorystream.Position = 0;
                return memorystream;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Serialize an object to FileStream and write to file
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="obj">Object</param>
        /// <param name="filename">File full Path</param>
        public static void SerializeObjectToFile<T>(T obj, string filename)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                // Create an XmlTextWriter using a FileStream.
                Stream fs = new FileStream(filename, FileMode.Create);
                XmlWriter writer = new XmlTextWriter(fs, Encoding.Unicode);
                //string ns = "http://www.w3.org/2001/XMLSchema-instance";
                //writer.WriteAttributeString("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
                //writer.WriteAttributeString("xsi", "noNamespaceSchemaLocation", ns, "mySchema.xsd");

                // Serialize using the XmlTextWriter.
                serializer.Serialize(writer, obj);
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Serialize an object to FileStream and write to file
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="obj">Object</param>
        /// <param name="filename">File full Path</param>
        public static void SerializeObjectToUTF8_File<T>(T obj, string filename)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                // Create an XmlTextWriter using a FileStream.
                Stream fs = new FileStream(filename, FileMode.Create);
                XmlWriter writer = new XmlTextWriter(fs, Encoding.UTF8);
                //string ns = "http://www.w3.org/2001/XMLSchema-instance";
                //writer.WriteAttributeString("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
                //writer.WriteAttributeString("xsi", "noNamespaceSchemaLocation", ns, "mySchema.xsd");

                // Serialize using the XmlTextWriter.
                serializer.Serialize(writer, obj);
                writer.Close();
                writer = null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// DeSerialize a Xml file to the Object Type specified
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="obj">Object</param>
        /// <param name="filename">File full Path</param>
        public static void DeSerializeFileToObject<T>(out T obj, string filename)
        {
            try
            {
                string XMLFeed, strFileContent = "";
                MemoryStream ms = new MemoryStream();
                //XmlSerializer serializer = new XmlSerializer(typeof(T));
                //// Create an XmlTextWriter using a FileStream.
                //Stream fs = File.Open(filename, FileMode.Open, FileAccess.Read);
                //XmlReader Reader = new XmlTextReader(fs);
                //fs.Flush();
                //fs.Close();
                //// DeSerialize using the XmlTextReader.
                //obj = (T)serializer.Deserialize(Reader);

                XmlSerializer serializer = new XmlSerializer(typeof(T));
                StreamReader sr = new StreamReader(filename, System.Text.Encoding.Default);
                strFileContent = sr.ReadToEnd();

                //Replace '&' with 'and' from the XMl String
                XMLFeed = strFileContent.Replace("&", "and");

                ms = StringToStream(XMLFeed);
                StreamReader SrMemoryStream = new StreamReader(ms);
                //XmlReader Reader = new XmlTextReader(ms);
                sr.Close();
                sr.Dispose();
                obj = (T)serializer.Deserialize(SrMemoryStream);
                ms.Flush();
                ms.Close();
                SrMemoryStream.Close();
                SrMemoryStream.Dispose();
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Deserialize to Object XmlSerialization
        /// </summary>
        /// <typeparam name="T">Object Type</typeparam>
        /// <param name="obj">Object of Type T</param>
        /// <param name="xml">xml String</param>
        public static void DeserializeObject<T>(out T obj, string xml)
        {
            using (TextReader reader = new StringReader(xml))
            {
                XmlSerializer xs = new XmlSerializer(typeof(T));
              obj =  (T)xs.Deserialize(reader);
            }
        }
     
        /// <summary>
        /// Converts String to Memory Stream(ASCIIEncoding)
        /// </summary>
        /// <param name="str"> input string</param>
        /// <returns>Memory Stream</returns>
        private static MemoryStream StringToStream(string str)
        {
            try
            {
                //byte[] byteArray = Encoding.UTF8.GetBytes(str);
                //MemoryStream stream = new MemoryStream(byteArray);
                //return stream;

                //MemoryStream memoryStream = new MemoryStream();
                //TextWriter tw = new StreamWriter(memoryStream);
                //tw.WriteLine("str");
                //return memoryStream;

                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] data = encoding.GetBytes(str);
                MemoryStream memoryStream = new MemoryStream(data);
                return memoryStream;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
       
        /// <summary>
        /// Serialize Data Contract of Object Type T
        /// </summary>
        /// <typeparam name="T">Object Type</typeparam>
        /// <param name="obj">Object of Type T</param>
        /// <returns></returns>
        public static MemoryStream DataContractSerialize<T>(T obj)
        {
            MemoryStream memorystream = new MemoryStream();
            XmlWriter writer = new XmlTextWriter(memorystream, Encoding.Unicode);
            DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            serializer.WriteObject(writer, obj);
            memorystream.Position = 0;
            return memorystream;
        }

       /// <summary>
        /// DeSerialize Data Contract of Object Type T
       /// </summary>
        /// <typeparam name="T">Object Type</typeparam>
        /// <param name="obj">return Object of Type T</param>
       /// <param name="stream">Memory Stream</param>
        public static void DataContractDeSerialize<T>(out T obj, MemoryStream stream)
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            stream.Position = 0;
            XmlReader Reader = new XmlTextReader(stream);

            // DeSerialize using the XmlTextReader.
            obj = (T)serializer.ReadObject(Reader);

        }

        #endregion Serialization/Deserialization

    }
}
