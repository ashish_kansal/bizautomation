﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Configuration;
using System.Data;
using DebugApplication;
using BACRM.BusinessLogic.Common;

class CreateLogFiles
{
    #region Members

    DataTable dtLogDetails = new DataTable();
    private string LogFilePath = ConfigurationManager.AppSettings["LogFilePath"];
    private int DeleteTrash = CCommon.ToInteger(ConfigurationManager.AppSettings["DeleteTrash"]);

    #endregion Members

    #region Methods

    /// <summary>
    /// Adds entries in Error Or Log file respectively
    /// </summary>
    /// <param name="DomainID">Domain Id</param>
    /// <param name="MessageType">Message Type ERR--> Error File, LOG--> Log File</param>
    /// <param name="Message">Log Message</param>
    public void WriteLog(long DomainId, string MessageType, string Message)
    {
        string FileName = "";
        string TimeStamp = "";
        int TriesCount = 0;
        try
        {
            string LogPath = GeneralFunctions.GetPath(LogFilePath, DomainId);//Gets Log File Folder for particular Domain
            TimeStamp = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + " ---> ";

            // Creates a new seperate file entries everyday for Error and Log respectively
            if (MessageType == "ERR")
            {
                FileName = LogPath + "ErrorLog_" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            }
            else if (MessageType == "LOG")
            {
                FileName = LogPath + "Log_" + DateTime.Now.ToString("ddMMyyyy") + ".txt";
            }

            //Writes to the Message to File with TimeStamp
            while (true)
            {
                try
                {
                    TriesCount = TriesCount + 1;
                    StreamWriter sw = new StreamWriter(FileName, true);
                    sw.WriteLine(TimeStamp + Message + Environment.NewLine);
                    sw.Flush();
                    sw.Close();
                    return;
                }
                catch (Exception)
                {
                    if (TriesCount > 10)
                    {
                        //For Rare case not to miss Log entries
                        //Creates New File if Threads Colid more than 10 times while writing Logs to the File

                        string NewFileName = System.IO.Path.GetFileNameWithoutExtension(FileName) + "_1.txt";
                        StreamWriter sw = new StreamWriter(NewFileName, true);
                        sw.WriteLine(TimeStamp + Message + Environment.NewLine);
                        sw.Flush();
                        sw.Close();
                        return;
                    }
                    System.Threading.Thread.Sleep(500);
                }
            }
        }
        catch (Exception)
        {
        }
    }

    /// <summary>
    /// Gets the List Of Files in the Log Directory
    /// </summary>
    /// <param name="DomainId">Domain Id</param>
    /// <returns>Log & Error Files Details List as DataTable </returns>
    public DataTable GetLogFiles(long DomainId)
    {
        string LogPath = GeneralFunctions.GetPath(LogFilePath, DomainId);
        DataRow dr;
        if (dtLogDetails.Columns.Count == 0)
            DefineLogDetails();//Define Structure for Log file Details

        int i = 0;
        if (Directory.Exists(LogPath))
        {
            string[] fileEntries = Directory.GetFiles(LogPath, "*", SearchOption.AllDirectories);
            //DirectoryInfo di = new DirectoryInfo(LogPath);
            //FileSystemInfo[] files = di.GetFileSystemInfos();
            //var orderedFiles = Directory.GetFiles(LogPath, "*", SearchOption.AllDirectories).OrderBy(f => new FileInfo(f).Length); 
            foreach (string filePath in fileEntries)
            {
                FileInfo fileInfo = new FileInfo(filePath);
                if (dtLogDetails.Columns.Count == 0)
                    DefineLogDetails();

                dr = dtLogDetails.NewRow();
                dr["S.No"] = ++i;
                dr["Name"] = fileInfo.Name;
                dr["FullName"] = fileInfo.FullName;
                dr["Size"] = fileInfo.Length + " Bytes";
                dr["CreateTime"] = fileInfo.CreationTime;
                dr["LastWriteTime"] = fileInfo.LastWriteTime;
                dr["LastAccessTime"] = fileInfo.LastAccessTime;
                dr["FileExtention"] = fileInfo.Extension;
                dtLogDetails.Rows.Add(dr);
            }

        }
        DataRow[] Drs;
        DataTable DtSorted;
        DtSorted = dtLogDetails.Copy();
        DtSorted.DefaultView.Sort = "CreateTime";
        Drs = DtSorted.Select();
        dtLogDetails.Rows.Clear();
        foreach (DataRow Dr3 in Drs)
        {
            dtLogDetails.ImportRow(Dr3);
        }

        //dtLogDetails.DefaultView.Sort = "CreateTime";
        return dtLogDetails;
    }

    /// <summary>
    /// Deletes the Log Files for the Domain Id and Clear Trash Files Older than 10 Days 
    /// </summary>
    /// <param name="DomainId">Domain ID</param>
    /// <param name="DaysOld">File's No of Days Old</param>
    public void DeleteLogFiles(long DomainId, int DaysOld)
    {
        string LogPath = GeneralFunctions.GetPath(LogFilePath, DomainId);
        if (Directory.Exists(LogPath))
        {
            string TrashPath = GeneralFunctions.GetPath(LogFilePath + "_Trash\\", DomainId);
            string[] fileEntries = Directory.GetFiles(LogPath, "*", SearchOption.AllDirectories);
            if (fileEntries.Length > 0)
            {
                foreach (string filePath in fileEntries)
                {
                    FileInfo fileInfo = new FileInfo(filePath);
                    string destPath = TrashPath + Path.GetFileName(filePath);
                    TimeSpan ts = DateTime.Now.Subtract(fileInfo.CreationTime);
                    if (ts.Days >= DaysOld)
                    {
                        // Have to create New file rather than Copying File to Trash 
                        // to calculate the no. of Days the file been moved to Trash folder
                        File.Copy(filePath, destPath);
                        File.Delete(filePath);
                    }
                    if (fileInfo.CreationTime.Date > DateTime.Now)
                    {

                    }
                }
            }
            string[] TrashfileEntries = Directory.GetFiles(TrashPath, "*", SearchOption.AllDirectories);
            if (TrashfileEntries.Length > 0)
            {
                foreach (string Trashfile in TrashfileEntries)
                {
                    FileInfo fileInfo = new FileInfo(Trashfile);
                    string destPath = TrashPath + Path.GetFileName(Trashfile);
                    TimeSpan ts = DateTime.Now.Subtract(fileInfo.CreationTime);
                    if (ts.Days >= 30)
                    {
                        File.Delete(Trashfile);
                    }
                    if (fileInfo.CreationTime.Date > DateTime.Now)
                    {

                    }
                }
            }
        }
    }

    /// <summary>
    /// Define Structure for Log file Details
    /// </summary>
    public void DefineLogDetails()
    {
        if (dtLogDetails.Columns.Count > 0)
            dtLogDetails.Columns.Clear();
        dtLogDetails.Columns.Add("S.No");
        dtLogDetails.Columns.Add("Name");
        dtLogDetails.Columns.Add("FullName");
        dtLogDetails.Columns.Add("Size");
        dtLogDetails.Columns.Add("CreateTime");
        dtLogDetails.Columns.Add("LastWriteTime");
        dtLogDetails.Columns.Add("LastAccessTime");
        dtLogDetails.Columns.Add("FileExtention");
    }

    #endregion Methods
}

