﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;

using BACRM.BusinessLogic;
using BACRMBUSSLOGIC.BussinessLogic;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Contract;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.WebAPI;
using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Alerts;

using System.ServiceModel;
using DebugApplication.AMWS_Communicator;
using System.Data.SqlClient;
//using AmazonSource;

namespace DebugApplication
{
    class AmazonClient
    {
        #region Members

        DataSet ds = new DataSet();
        DataSet dsItems = new DataSet();
        DataTable dtItems = new DataTable();

        DataSet dsFBAOrderItems = new DataSet();
        DataTable dtFBAOrderItems = new DataTable();

        DataSet dsOrderItems = new DataSet();
        DataTable dtOrderItems = new DataTable();

        DataTable dtCompanyTaxTypes = new DataTable();
        DataRow drtax = null;

        long lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId;
        OppBizDocs objOppBizDocs = new OppBizDocs();

        private string FeedFilePath = ConfigurationManager.AppSettings["FeedFilePath"];
        private string FeedSubmitedFilePath = ConfigurationManager.AppSettings["FeedSubmitedFilePath"];
        private string FeedResultFilePath = ConfigurationManager.AppSettings["FeedResultFilePath"];
        private string OrderReportPath = ConfigurationManager.AppSettings["AmazonOrderFilePath"];
        private string ProductImagePath = ConfigurationManager.AppSettings["AmazonProductImagePath"];
        //private string ProcessedOrderReportPath = ConfigurationManager.AppSettings["AmazonClosedOrders"];
        //private static string LogMessage = "";

        private string AmazonMWSAccessKey = ConfigurationManager.AppSettings["AmazonMWSAccessKey"];
        private string AmazonMWSSecretKey = ConfigurationManager.AppSettings["AmazonMWSSecretKey"];

        string strdetails = "";
        string[] arrOutPut;
        string[] arrBillingIDs;

        #endregion Members

        #region Feed Generation

        /// <summary>
        /// Creates Product feed files 
        /// Item Detail feed, Item Inventory feed, Item price feed, Item Image feed.
        /// </summary>
        /// <param name="DomainId">Domain ID</param>
        /// <param name="dtNewItems">List of Items to Add to Amazon</param>
        /// <param name="MerchantToken">Merchant Token</param>
        public void NewProductFeed(long DomainId, DataTable dtNewItems, string MerchantToken)
        {
            string LogMessage = "", ErrorMessage = "";

            AmazonEnvelope objProductEnvelope = new AmazonEnvelope();
            List<AmazonEnvelopeMessage> lstProductMessages = new List<AmazonEnvelopeMessage>();

            AmazonEnvelope objInventoryEnvelope = new AmazonEnvelope();
            List<AmazonEnvelopeMessage> lstInventoryMessages = new List<AmazonEnvelopeMessage>();

            AmazonEnvelope objPriceEnvelope = new AmazonEnvelope();
            List<AmazonEnvelopeMessage> lstPriceMessages = new List<AmazonEnvelopeMessage>();

            AmazonEnvelope objProductImageEnvelope = new AmazonEnvelope();
            List<AmazonEnvelopeMessage> lstProductImageMessages = new List<AmazonEnvelopeMessage>();

            BACRM.BusinessLogic.Item.CItems objItems = new BACRM.BusinessLogic.Item.CItems();
            DataTable dtItemDetails = new DataTable();
            int MessageId = 1;
            int PImageMessageId = 1;
            try
            {
                foreach (DataRow dr in dtNewItems.Rows)
                {
                    try
                    {
                        if (CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numItemCode"]).Length > 0 & CCommon.ToString(dr["vcAPIItemId"]).Length <= 1)
                        {
                            objItems.ItemCode = CCommon.ToInteger(dr["numItemCode"]);
                            string ListPrice = CCommon.ToString(dr["monListPrice"]);
                            TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);

                            objItems.ClientZoneOffsetTime = CCommon.ToInteger(diff1.TotalMinutes);
                            dtItemDetails = objItems.ItemDetails();

                            if (CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]).Length > 0 & CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Length > 0 & CCommon.ToString(dtItemDetails.Rows[0]["numBarCodeId"]).Length > 0)
                            {
                                string DimensionUOM = "", WeightUOM = "";
                                string Weight = "", Length = "", Width = "", Height = "";
                                WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                                string WebApi_ItemDetails = ""; // CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";

                                if (CCommon.ToLong(dtItemDetails.Rows[0]["numItemGroup"]) > 0 && CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]).Length > 0)
                                {
                                    //WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + "_" + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]) + ".xml";
                                    WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + "_" + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]) + ".xml";
                                }
                                else
                                {
                                    WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";
                                }

                                if (File.Exists(WebApi_ItemDetails))
                                {
                                    StreamReader objStreamReader = new StreamReader(WebApi_ItemDetails);
                                    XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                    objStreamReader.Close();
                                }

                                string[] SearchTerms;
                                if (objWebAPIItemDetail.SearchTerms != null && objWebAPIItemDetail.SearchTerms != "")
                                {
                                    SearchTerms = objWebAPIItemDetail.SearchTerms.Split(',');
                                }
                                else
                                {
                                    SearchTerms = new string[] { "-" };
                                }
                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.DimensionUOM))
                                    DimensionUOM = objWebAPIItemDetail.DimensionUOM;

                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.WeightUOM))
                                    WeightUOM = objWebAPIItemDetail.WeightUOM;

                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.Length))
                                    Length = objWebAPIItemDetail.Length;

                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.Weight))
                                    Weight = objWebAPIItemDetail.Weight;

                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.Width))
                                    Width = objWebAPIItemDetail.Width;

                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.Height))
                                    Height = objWebAPIItemDetail.Height;

                                AmazonEnvelopeMessage objProductMessage = new AmazonEnvelopeMessage();
                                objProductMessage.MessageID = CCommon.ToString(MessageId);
                                objProductMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                                objProductMessage.OperationTypeSpecified = true;

                                AmazonEnvelopeMessage objInventoryMessage = new AmazonEnvelopeMessage();
                                objInventoryMessage.MessageID = CCommon.ToString(MessageId);
                                objInventoryMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                                objInventoryMessage.OperationTypeSpecified = true;

                                AmazonEnvelopeMessage objPriceMessage = new AmazonEnvelopeMessage();
                                objPriceMessage.MessageID = CCommon.ToString(MessageId);
                                objPriceMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                                objPriceMessage.OperationTypeSpecified = true;

                                Product objProduct = new Product();
                                objProduct.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);

                                //Standard Product Specifications
                                StandardProductID objStaProID = new StandardProductID();
                                objStaProID.Type = StandardProductIDType.UPC;
                                objStaProID.Value = CCommon.ToString(dtItemDetails.Rows[0]["numBarCodeId"]);
                                objProduct.StandardProductID = objStaProID;

                                //Product Description Specifications
                                ProductDescriptionData objDescription = new ProductDescriptionData();
                                objDescription.AutographedSpecified = false;
                                //objDescription.Brand = "Sony Ericsson";
                                //objDescription.BulletPoint = new string[3];
                                //objDescription.BulletPoint[0] = "Quad Band 850/900/1800/1900, HSDPA 900 / 2100";
                                //objDescription.BulletPoint[1] = "3.15 MP Camera, 2048x1536 pixels";
                                //objDescription.BulletPoint[2] = "Walkman player, Stereo FM radio with RDS, TrackID music recognition";
                                //objDescription.DeliveryChannel = new DeliveryChannel[2];
                                //objDescription.DeliveryChannel[0] = DeliveryChannel.in_store;
                                //objDescription.DeliveryChannel[1] = DeliveryChannel.direct_ship;

                                objDescription.Description = CCommon.ToString(dtItemDetails.Rows[0]["vcExtendedDescToAPI"]);
                                //objDescription.Description = CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]);
                                //objDescription.Designer = "Sony Ericsson";
                                objDescription.IsDiscontinuedByManufacturerSpecified = false;
                                objDescription.IsGiftMessageAvailableSpecified = false;
                                objDescription.IsGiftWrapAvailableSpecified = false;

                                //Item dimensioin specifications
                                LengthDimension length = new LengthDimension();
                                length.unitOfMeasure = GetLengthUOM((DimensionUOM));
                                length.Value = CCommon.ToDecimal(Length);

                                LengthDimension height = new LengthDimension();
                                height.unitOfMeasure = GetLengthUOM(DimensionUOM);
                                height.Value = CCommon.ToDecimal(Height);

                                LengthDimension width = new LengthDimension();
                                width.unitOfMeasure = GetLengthUOM(DimensionUOM);
                                width.Value = CCommon.ToDecimal(Width);

                                WeightDimension weight = new WeightDimension();
                                weight.unitOfMeasure = GetWeightUOM(WeightUOM);
                                weight.Value = CCommon.ToDecimal(Weight);

                                Dimensions objDimensions = new Dimensions();
                                objDimensions.Length = length;
                                objDimensions.Height = height;
                                objDimensions.Width = width;
                                objDimensions.Weight = weight;
                                objDescription.ItemDimensions = objDimensions;

                                // Package Dimension specifications
                                LengthDimension PLength = new LengthDimension();
                                PLength.unitOfMeasure = GetLengthUOM(DimensionUOM);
                                PLength.Value = CCommon.ToDecimal(dtItemDetails.Rows[0]["fltLength"]);

                                LengthDimension Pheight = new LengthDimension();
                                Pheight.unitOfMeasure = GetLengthUOM(DimensionUOM);
                                Pheight.Value = CCommon.ToDecimal(dtItemDetails.Rows[0]["fltHeight"]);

                                LengthDimension Pwidth = new LengthDimension();
                                Pwidth.unitOfMeasure = GetLengthUOM(DimensionUOM);
                                Pwidth.Value = CCommon.ToDecimal(dtItemDetails.Rows[0]["fltWidth"]);

                                SpatialDimensions objSDimentions = new SpatialDimensions();
                                objSDimentions.Length = PLength;
                                objSDimentions.Height = Pheight;
                                objSDimentions.Width = Pwidth;
                                objDescription.PackageDimensions = objSDimentions;

                                // Package Weight specifications
                                PositiveNonZeroWeightDimension Pweight = new PositiveNonZeroWeightDimension();
                                Pweight.unitOfMeasure = GetWeightUOM(WeightUOM);
                                Pweight.Value = CCommon.ToDecimal(dtItemDetails.Rows[0]["fltWeight"]);
                                objDescription.PackageWeight = Pweight;

                                objDescription.Prop65Specified = false;
                                //objDescription.ItemType = "";
                                //objDescription.LegalDisclaimer = "Sony Ericsson";
                                objDescription.Manufacturer = CCommon.ToString(dtItemDetails.Rows[0]["vcManufacturer"]);
                                //objDescription.MaxAggregateShipQuantity = "";
                                //objDescription.MaxOrderQuantity = "10";
                                //objDescription.Memorabilia = true;
                                //objDescription.MemorabiliaSpecified = true;
                                //objDescription.MerchantCatalogNumber = "123456789";
                                //objDescription.MfrPartNumber = "23456";

                                CurrencyAmount objCurrencyAmount = new CurrencyAmount();
                                objCurrencyAmount.currency = BaseCurrencyCode.USD;
                                objCurrencyAmount.Value = CCommon.ToDecimal(ListPrice);
                                objDescription.MSRP = objCurrencyAmount;

                                //objDescription.OtherItemAttributes = new string[3];
                                //objDescription.OtherItemAttributes[0] = "Android OS, v2.1 (Eclair), 600 MHz ARM 11 processor";
                                //objDescription.OtherItemAttributes[1] = "Android OS, v2.3 (gingerbread), 600 MHz ARM 11 processor";
                                //objDescription.OtherItemAttributes[2] = "Not Working (in Dead)";

                                objDescription.SerialNumberRequired = false;
                                objDescription.SerialNumberRequiredSpecified = true;

                                //PositiveNonZeroWeightDimension objPNWDim = new PositiveNonZeroWeightDimension();
                                //objPNWDim.unitOfMeasure = WeightUnitOfMeasure.KG;
                                //objPNWDim.Value = CCommon.ToDecimal(1000);
                                //objDescription.ShippingWeight = objPNWDim;
                                // objDescription.shi
                                //objDescription.SubjectContent = new string[2];
                                //objDescription.SubjectContent[0] = "Sony Ericsson Xperia W8 E16i";
                                //objDescription.SubjectContent[1] = "Design accents of an XPERIA X8";

                                objDescription.SearchTerms = SearchTerms;
                                objDescription.TargetAudience = new string[] { "Any One" };
                                objDescription.Title = CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);

                                objDescription.TSDAgeWarningSpecified = false;
                                objDescription.TSDLanguage = new ProductDescriptionDataTSDLanguage[] { ProductDescriptionDataTSDLanguage.English };
                                objDescription.TSDWarning = new ProductDescriptionDataTSDWarning[] { ProductDescriptionDataTSDWarning.no_warning_applicable };
                                // objDescription.UsedFor = new string[] { "Entire Business Management" };

                                objProduct.DiscontinueDateSpecified = false;

                                ProductDiscoveryData objPDisData = new ProductDiscoveryData();
                                objPDisData.BrowseExclusion = false;
                                objPDisData.BrowseExclusionSpecified = false;
                                objPDisData.RecommendationExclusionSpecified = false;
                                objPDisData.Priority = "10";

                                objProduct.DiscoveryData = objPDisData;

                                //objProduct.ExternalProductUrl = "http://google.com/";
                                objProduct.ItemPackageQuantity = "1";
                                objProduct.LaunchDate = DateTime.Now.AddHours(12);
                                objProduct.LaunchDateSpecified = true;
                                objProduct.NumberOfItems = "1";

                                //objProduct.OffAmazonChannel = ProductOffAmazonChannel.advertise;
                                objProduct.OffAmazonChannelSpecified = false;

                                objProduct.OnAmazonChannel = ProductOnAmazonChannel.sell;
                                objProduct.OnAmazonChannelSpecified = true;

                                //objProduct.ProductTaxCode = "123";
                                objProduct.ReleaseDate = DateTime.Now.AddHours(12);
                                objProduct.ReleaseDateSpecified = true;

                                ConditionInfo objCInfo = new ConditionInfo();
                                objCInfo.ConditionType = ConditionType.New;
                                //objCInfo.ConditionNote = "Not working (In Dead)";
                                objProduct.Condition = objCInfo;

                                objProduct.DescriptionData = objDescription;
                                objProductMessage.Item = objProduct;
                                lstProductMessages.Add(objProductMessage);

                                LogMessage = "Feed content created for Merchant : " + MerchantToken + "\n  FeedType : PRODUCT \n" + ", for Product(SKU : " + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]) + ")";
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From NewProductFeed : " + LogMessage);

                                Inventory objInventory = new Inventory();
                                objInventory.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);
                                objInventory.Quantity = CCommon.ToString(dr["QtyOnHand"]);
                                objInventory.FulfillmentLatency = "1";
                                objInventory.RestockDateSpecified = false;
                                objInventory.SwitchFulfillmentToSpecified = false;
                                objInventoryMessage.Item = objInventory;
                                lstInventoryMessages.Add(objInventoryMessage);

                                LogMessage = "INVENTORY Feed content created for Merchant : " + MerchantToken + "\n for Product(SKU : "
                                    + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]) + " Quantity : " + CCommon.ToString(dr["QtyOnHand"]) + ")";
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From NewProductFeed : " + LogMessage);

                                Price objPrice = new Price();
                                objPrice.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);
                                OverrideCurrencyAmount objOvrCurrencyAmount = new OverrideCurrencyAmount();
                                objOvrCurrencyAmount.currency = BaseCurrencyCodeWithDefault.USD;
                                objOvrCurrencyAmount.Value = CCommon.ToDecimal(ListPrice);
                                objPrice.StandardPrice = objOvrCurrencyAmount;
                                objPriceMessage.Item = objPrice;
                                lstPriceMessages.Add(objPriceMessage);

                                LogMessage = "PRICE Feed content created for Merchant : " + MerchantToken + ", for Product(SKU : "
                                    + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]) + " New Price : " + ListPrice + ")";
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From NewProductFeed : " + LogMessage);
                                if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["xmlItemImages"])))
                                {

                                    XmlDocument xmlDocItemImages = new XmlDocument();
                                    xmlDocItemImages.LoadXml(CCommon.ToString(dtItemDetails.Rows[0]["xmlItemImages"]));
                                    XmlNodeList ItemImages = xmlDocItemImages.SelectNodes("Images/ItemImages");
                                    string ImagePath = "";
                                    int i = 0;

                                    foreach (XmlNode Image in ItemImages)
                                    {
                                        try
                                        {
                                            AmazonEnvelopeMessage objProductImageMessage = new AmazonEnvelopeMessage();
                                            objProductImageMessage.MessageID = CCommon.ToString((PImageMessageId + 1));
                                            objProductImageMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                                            objProductImageMessage.OperationTypeSpecified = true;

                                            ImagePath = ProductImagePath + "//" + DomainId + "//" + CCommon.ToString(Image.Attributes["vcPathForImage"].Value);
                                            ProductImage objProductImage = new ProductImage();
                                            objProductImage.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);

                                            if (CCommon.ToString(Image.Attributes["bitDefault"].Value) == "1")
                                            {
                                                objProductImage.ImageType = ProductImageImageType.Main;
                                            }
                                            else
                                            {
                                                switch (i)
                                                {
                                                    case 1:
                                                        objProductImage.ImageType = ProductImageImageType.PT1;
                                                        break;

                                                    case 2:
                                                        objProductImage.ImageType = ProductImageImageType.PT2;
                                                        break;

                                                    case 3:
                                                        objProductImage.ImageType = ProductImageImageType.PT3;
                                                        break;

                                                    case 4:
                                                        objProductImage.ImageType = ProductImageImageType.PT4;
                                                        break;

                                                    case 5:
                                                        objProductImage.ImageType = ProductImageImageType.PT5;
                                                        break;

                                                    case 6:
                                                        objProductImage.ImageType = ProductImageImageType.PT6;
                                                        break;

                                                    case 7:
                                                        objProductImage.ImageType = ProductImageImageType.PT7;
                                                        break;

                                                    case 8:
                                                        objProductImage.ImageType = ProductImageImageType.PT8;
                                                        break;

                                                    case 9:
                                                        objProductImage.ImageType = ProductImageImageType.Search;
                                                        break;

                                                    case 10:
                                                        objProductImage.ImageType = ProductImageImageType.Swatch;
                                                        break;
                                                }
                                            }
                                            objProductImage.ImageLocation = ImagePath;
                                            objProductImageMessage.Item = objProductImage;
                                            LogMessage = "Image Feed content created for Merchant : " + MerchantToken + ", for Product(SKU : " + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"])
                                                + " , and Image Location : " + ImagePath + " ) ";
                                            GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From NewProductFeed : " + LogMessage);
                                            lstProductImageMessages.Add(objProductImageMessage);
                                            i++;
                                            PImageMessageId++;
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorMessage = "Error generating Product Image feed file for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item Name : "
                                                + CCommon.ToString(dr["vcItemName"]) + ". " + Environment.NewLine + ex.Message;
                                            GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From NewProductFeed : " + ErrorMessage);
                                            GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From NewProductFeed : " + ex.StackTrace);
                                        }
                                    }
                                }
                                MessageId++;
                            }
                        }
                        else
                        {
                            //ErrorMessage = "Error Creating feed file for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item Name : "
                            //+ CCommon.ToString(dr["vcItemName"]) + ". Either one must not have an empty Value";
                            //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrorMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorMessage = "Error generating feed file for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item Name : "
                            + CCommon.ToString(dr["vcItemName"]) + ". " + Environment.NewLine + ex.Message;
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From NewProductFeed : " + ErrorMessage);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From NewProductFeed : " + ex.StackTrace);
                    }
                }

                Header objHeader = new Header();
                objHeader.DocumentVersion = "1.0";
                objHeader.MerchantIdentifier = MerchantToken;
                string FileName = "";
                string path = "";

                if (lstProductMessages.Count > 0)
                {
                    objProductEnvelope.Header = objHeader;
                    //objProductEnvelope.MarketplaceName = "";
                    objProductEnvelope.MessageType = AmazonEnvelopeMessageType.Product;
                    objProductEnvelope.PurgeAndReplace = false;
                    objProductEnvelope.PurgeAndReplaceSpecified = true;
                    objProductEnvelope.Message = lstProductMessages.ToArray();
                    FileName = GenerateFileName(objProductEnvelope.Header.MerchantIdentifier, "PRODUCT");
                    path = GeneralFunctions.GetPath(FeedFilePath, DomainId) + FileName + ".xml";
                    GeneralFunctions.SerializeObjectToFile<AmazonEnvelope>(objProductEnvelope, path);
                }
                if (lstInventoryMessages.Count > 0)
                {
                    objInventoryEnvelope.Header = objHeader;
                    //objInventoryEnvelope.MarketplaceName = "";
                    objInventoryEnvelope.MessageType = AmazonEnvelopeMessageType.Inventory;
                    objInventoryEnvelope.PurgeAndReplace = false;
                    objInventoryEnvelope.PurgeAndReplaceSpecified = true;
                    objInventoryEnvelope.Message = lstInventoryMessages.ToArray();
                    FileName = GenerateFileName(objInventoryEnvelope.Header.MerchantIdentifier, "INVENTORY");
                    path = GeneralFunctions.GetPath(FeedFilePath, DomainId) + FileName + ".xml";
                    GeneralFunctions.SerializeObjectToFile<AmazonEnvelope>(objInventoryEnvelope, path);
                }

                if (lstPriceMessages.Count > 0)
                {
                    objPriceEnvelope.Header = objHeader;
                    //objInventoryEnvelope.MarketplaceName = "";
                    objPriceEnvelope.MessageType = AmazonEnvelopeMessageType.Price;
                    objPriceEnvelope.PurgeAndReplace = false;
                    objPriceEnvelope.PurgeAndReplaceSpecified = true;
                    objPriceEnvelope.Message = lstPriceMessages.ToArray();
                    FileName = GenerateFileName(objPriceEnvelope.Header.MerchantIdentifier, "PRICING");
                    path = GeneralFunctions.GetPath(FeedFilePath, DomainId) + FileName + ".xml";
                    GeneralFunctions.SerializeObjectToFile<AmazonEnvelope>(objPriceEnvelope, path);
                }

                if (lstProductImageMessages.Count > 0)
                {
                    objProductImageEnvelope.Header = objHeader;
                    objProductImageEnvelope.MessageType = AmazonEnvelopeMessageType.ProductImage;
                    objProductImageEnvelope.PurgeAndReplaceSpecified = false;
                    objProductImageEnvelope.Message = lstProductImageMessages.ToArray();
                    FileName = GenerateFileName(objProductImageEnvelope.Header.MerchantIdentifier, "IMAGE");
                    path = GeneralFunctions.GetPath(FeedFilePath, DomainId) + FileName + ".xml";
                    GeneralFunctions.SerializeObjectToFile<AmazonEnvelope>(objProductImageEnvelope, path);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = "Error generating Amazon Product feed file . " + Environment.NewLine + ex.Message;
                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From NewProductFeed : " + ErrorMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From NewProductFeed : " + ex.StackTrace);
            }
        }

        public void UpdateItemFeed(long DomainId, DataTable dtNewItems, string MerchantToken)
        {
            string LogMessage = "", ErrorMessage = "";

            AmazonEnvelope objProductEnvelope = new AmazonEnvelope();
            List<AmazonEnvelopeMessage> lstProductMessages = new List<AmazonEnvelopeMessage>();

            AmazonEnvelope objInventoryEnvelope = new AmazonEnvelope();
            List<AmazonEnvelopeMessage> lstInventoryMessages = new List<AmazonEnvelopeMessage>();

            AmazonEnvelope objPriceEnvelope = new AmazonEnvelope();
            List<AmazonEnvelopeMessage> lstPriceMessages = new List<AmazonEnvelopeMessage>();

            AmazonEnvelope objProductImageEnvelope = new AmazonEnvelope();
            List<AmazonEnvelopeMessage> lstProductImageMessages = new List<AmazonEnvelopeMessage>();

            BACRM.BusinessLogic.Item.CItems objItems = new BACRM.BusinessLogic.Item.CItems();
            DataTable dtItemDetails = new DataTable();
            int MessageId = 1;
            int PImageMessageId = 1;
            try
            {
                foreach (DataRow dr in dtNewItems.Rows)
                {
                    try
                    {
                        if (CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numItemCode"]).Length > 0 & CCommon.ToString(dr["vcAPIItemId"]).Length > 1)
                        {
                            objItems.ItemCode = CCommon.ToInteger(dr["numItemCode"]);
                            string ListPrice = CCommon.ToString(dr["monListPrice"]);
                            TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);

                            objItems.ClientZoneOffsetTime = CCommon.ToInteger(diff1.TotalMinutes);
                            dtItemDetails = objItems.ItemDetails();

                            if (CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]).Length > 0 & CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Length > 0)
                            {
                                string DimensionUOM = "", WeightUOM = "";
                                string Weight = "", Length = "", Width = "", Height = "";
                                WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                                string WebApi_ItemDetails = ""; //CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";

                                if (CCommon.ToLong(dtItemDetails.Rows[0]["numItemGroup"]) > 0 && CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]).Length > 0)
                                {
                                    WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + "_" + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]) + ".xml";
                                }
                                else
                                {
                                    WebApi_ItemDetails = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(dr["numItemCode"]) + ".xml";
                                }

                                if (File.Exists(WebApi_ItemDetails))
                                {
                                    StreamReader objStreamReader = new StreamReader(WebApi_ItemDetails);
                                    XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                    objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                    objStreamReader.Close();
                                }

                                string[] SearchTerms;
                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.DimensionUOM))
                                    DimensionUOM = objWebAPIItemDetail.DimensionUOM;

                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.WeightUOM))
                                    WeightUOM = objWebAPIItemDetail.WeightUOM;

                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.Length))
                                    Length = objWebAPIItemDetail.Length;

                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.Weight))
                                    Weight = objWebAPIItemDetail.Weight;

                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.Width))
                                    Width = objWebAPIItemDetail.Width;

                                if (!string.IsNullOrEmpty(objWebAPIItemDetail.Height))
                                    Height = objWebAPIItemDetail.Height;

                                AmazonEnvelopeMessage objProductMessage = new AmazonEnvelopeMessage();
                                objProductMessage.MessageID = CCommon.ToString(MessageId);
                                objProductMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                                objProductMessage.OperationTypeSpecified = true;

                                AmazonEnvelopeMessage objInventoryMessage = new AmazonEnvelopeMessage();
                                objInventoryMessage.MessageID = CCommon.ToString(MessageId);
                                objInventoryMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                                objInventoryMessage.OperationTypeSpecified = true;

                                AmazonEnvelopeMessage objPriceMessage = new AmazonEnvelopeMessage();
                                objPriceMessage.MessageID = CCommon.ToString(MessageId);
                                objPriceMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                                objPriceMessage.OperationTypeSpecified = true;

                                Product objProduct = new Product();
                                objProduct.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);

                                //Standard Product Specifications
                                StandardProductID objStaProID = new StandardProductID();
                                objStaProID.Type = StandardProductIDType.ASIN;
                                objStaProID.Value = CCommon.ToString(dr["vcAPIItemId"]);
                                objProduct.StandardProductID = objStaProID;

                                //Product Description Specifications
                                ProductDescriptionData objDescription = new ProductDescriptionData();
                                objDescription.AutographedSpecified = false;
                                //objDescription.Brand = "Sony Ericsson";
                                //objDescription.BulletPoint = new string[3];
                                //objDescription.BulletPoint[0] = "Quad Band 850/900/1800/1900, HSDPA 900 / 2100";
                                //objDescription.BulletPoint[1] = "3.15 MP Camera, 2048x1536 pixels";
                                //objDescription.BulletPoint[2] = "Walkman player, Stereo FM radio with RDS, TrackID music recognition";
                                //objDescription.DeliveryChannel = new DeliveryChannel[2];
                                //objDescription.DeliveryChannel[0] = DeliveryChannel.in_store;
                                //objDescription.DeliveryChannel[1] = DeliveryChannel.direct_ship;

                                objDescription.Description = CCommon.ToString(dtItemDetails.Rows[0]["vcExtendedDescToAPI"]);
                                //objDescription.Description = CCommon.ToString(dtItemDetails.Rows[0]["txtItemDesc"]);
                                //objDescription.Designer = "Sony Ericsson";
                                objDescription.IsDiscontinuedByManufacturerSpecified = false;
                                objDescription.IsGiftMessageAvailableSpecified = false;
                                objDescription.IsGiftWrapAvailableSpecified = false;

                                //Item dimensioin specifications
                                LengthDimension length = new LengthDimension();
                                length.unitOfMeasure = GetLengthUOM((DimensionUOM));
                                length.Value = CCommon.ToDecimal(Length);

                                LengthDimension height = new LengthDimension();
                                height.unitOfMeasure = GetLengthUOM(DimensionUOM);
                                height.Value = CCommon.ToDecimal(Height);

                                LengthDimension width = new LengthDimension();
                                width.unitOfMeasure = GetLengthUOM(DimensionUOM);
                                width.Value = CCommon.ToDecimal(Width);

                                WeightDimension weight = new WeightDimension();
                                weight.unitOfMeasure = GetWeightUOM(WeightUOM);
                                weight.Value = CCommon.ToDecimal(Weight);

                                Dimensions objDimensions = new Dimensions();
                                objDimensions.Length = length;
                                objDimensions.Height = height;
                                objDimensions.Width = width;
                                objDimensions.Weight = weight;
                                objDescription.ItemDimensions = objDimensions;

                                // Package Dimension specifications
                                LengthDimension PLength = new LengthDimension();
                                PLength.unitOfMeasure = GetLengthUOM(DimensionUOM);
                                PLength.Value = CCommon.ToDecimal(dtItemDetails.Rows[0]["fltLength"]);

                                LengthDimension Pheight = new LengthDimension();
                                Pheight.unitOfMeasure = GetLengthUOM(DimensionUOM);
                                Pheight.Value = CCommon.ToDecimal(dtItemDetails.Rows[0]["fltHeight"]);

                                LengthDimension Pwidth = new LengthDimension();
                                Pwidth.unitOfMeasure = GetLengthUOM(DimensionUOM);
                                Pwidth.Value = CCommon.ToDecimal(dtItemDetails.Rows[0]["fltWidth"]);

                                SpatialDimensions objSDimentions = new SpatialDimensions();
                                objSDimentions.Length = PLength;
                                objSDimentions.Height = Pheight;
                                objSDimentions.Width = Pwidth;
                                objDescription.PackageDimensions = objSDimentions;

                                // Package Weight specifications
                                PositiveNonZeroWeightDimension Pweight = new PositiveNonZeroWeightDimension();
                                Pweight.unitOfMeasure = GetWeightUOM(WeightUOM);
                                Pweight.Value = CCommon.ToDecimal(dtItemDetails.Rows[0]["fltWeight"]);
                                objDescription.PackageWeight = Pweight;

                                objDescription.Prop65Specified = false;
                                //objDescription.ItemType = "";
                                //objDescription.LegalDisclaimer = "Sony Ericsson";
                                objDescription.Manufacturer = CCommon.ToString(dtItemDetails.Rows[0]["vcManufacturer"]);
                                //objDescription.MaxAggregateShipQuantity = "";
                                //objDescription.MaxOrderQuantity = "10";
                                //objDescription.Memorabilia = true;
                                //objDescription.MemorabiliaSpecified = true;
                                //objDescription.MerchantCatalogNumber = "123456789";
                                //objDescription.MfrPartNumber = "23456";

                                CurrencyAmount objCurrencyAmount = new CurrencyAmount();
                                objCurrencyAmount.currency = BaseCurrencyCode.USD;
                                objCurrencyAmount.Value = CCommon.ToDecimal(ListPrice);
                                objDescription.MSRP = objCurrencyAmount;

                                //objDescription.OtherItemAttributes = new string[3];
                                //objDescription.OtherItemAttributes[0] = "Android OS, v2.1 (Eclair), 600 MHz ARM 11 processor";
                                //objDescription.OtherItemAttributes[1] = "Android OS, v2.3 (gingerbread), 600 MHz ARM 11 processor";
                                //objDescription.OtherItemAttributes[2] = "Not Working (in Dead)";

                                objDescription.SerialNumberRequired = false;
                                objDescription.SerialNumberRequiredSpecified = false;
                                objDescription.TSDAgeWarningSpecified = false;
                                objProduct.DiscontinueDateSpecified = false;

                                //PositiveNonZeroWeightDimension objPNWDim = new PositiveNonZeroWeightDimension();
                                //objPNWDim.unitOfMeasure = WeightUnitOfMeasure.KG;
                                //objPNWDim.Value = CCommon.ToDecimal(1000);
                                //objDescription.ShippingWeight = objPNWDim;
                                // objDescription.shi
                                //objDescription.SubjectContent = new string[2];
                                //objDescription.SubjectContent[0] = "Sony Ericsson Xperia W8 E16i";
                                //objDescription.SubjectContent[1] = "Design accents of an XPERIA X8";
                                objDescription.Title = CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]);


                                ProductDiscoveryData objPDisData = new ProductDiscoveryData();
                                objPDisData.BrowseExclusion = false;
                                objPDisData.BrowseExclusionSpecified = false;
                                objPDisData.RecommendationExclusionSpecified = false;

                                objProduct.DiscoveryData = objPDisData;

                                objProduct.LaunchDateSpecified = false;
                                objProduct.OffAmazonChannelSpecified = false;
                                objProduct.OnAmazonChannel = ProductOnAmazonChannel.sell;
                                objProduct.OnAmazonChannelSpecified = false;
                                objProduct.ReleaseDateSpecified = false;

                                //ConditionInfo objCInfo = new ConditionInfo();
                                //objCInfo.ConditionType = ConditionType.New;
                                ////objCInfo.ConditionNote = "Not working (In Dead)";
                                //objProduct.Condition = objCInfo;

                                objProduct.DescriptionData = objDescription;
                                objProductMessage.Item = objProduct;
                                lstProductMessages.Add(objProductMessage);

                                LogMessage = "Feed content created for Merchant : " + MerchantToken + "\n  FeedType : PRODUCT \n" + ", for Product(SKU : " + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]) + ")";
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From UpdateItemFeed : " + LogMessage);

                                Inventory objInventory = new Inventory();
                                objInventory.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);
                                objInventory.Quantity = CCommon.ToString(dr["QtyOnHand"]);
                                objInventory.FulfillmentLatency = "1";
                                objInventory.RestockDateSpecified = false;
                                objInventory.SwitchFulfillmentToSpecified = false;
                                objInventoryMessage.Item = objInventory;
                                lstInventoryMessages.Add(objInventoryMessage);

                                LogMessage = "INVENTORY Feed content created for Merchant : " + MerchantToken + "\n for Product(SKU : "
                                    + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]) + " Quantity : " + CCommon.ToString(dr["QtyOnHand"]) + ")";
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From UpdateItemFeed : " + LogMessage);

                                Price objPrice = new Price();
                                objPrice.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);
                                OverrideCurrencyAmount objOvrCurrencyAmount = new OverrideCurrencyAmount();
                                objOvrCurrencyAmount.currency = BaseCurrencyCodeWithDefault.USD;
                                objOvrCurrencyAmount.Value = CCommon.ToDecimal(ListPrice);
                                objPrice.StandardPrice = objOvrCurrencyAmount;
                                objPriceMessage.Item = objPrice;
                                lstPriceMessages.Add(objPriceMessage);

                                LogMessage = "PRICE Feed content created for Merchant : " + MerchantToken + ", for Product(SKU : "
                                    + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]) + " New Price : " + ListPrice + ")";
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From UpdateItemFeed : " + LogMessage);

                                if (!string.IsNullOrEmpty(CCommon.ToString(dtItemDetails.Rows[0]["xmlItemImages"])))
                                {


                                    XmlDocument xmlDocItemImages = new XmlDocument();
                                    xmlDocItemImages.LoadXml(CCommon.ToString(dtItemDetails.Rows[0]["xmlItemImages"]));
                                    XmlNodeList ItemImages = xmlDocItemImages.SelectNodes("Images/ItemImages");
                                    string ImagePath = "";
                                    int i = 0;

                                    foreach (XmlNode Image in ItemImages)
                                    {
                                        try
                                        {
                                            AmazonEnvelopeMessage objProductImageMessage = new AmazonEnvelopeMessage();
                                            objProductImageMessage.MessageID = CCommon.ToString((PImageMessageId + 1));
                                            objProductImageMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                                            objProductImageMessage.OperationTypeSpecified = true;

                                            ImagePath = ProductImagePath + "//" + DomainId + "//" + CCommon.ToString(Image.Attributes["vcPathForImage"].Value);
                                            ProductImage objProductImage = new ProductImage();
                                            objProductImage.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);

                                            if (CCommon.ToString(Image.Attributes["bitDefault"].Value) == "1")
                                            {
                                                objProductImage.ImageType = ProductImageImageType.Main;
                                            }
                                            else
                                            {
                                                switch (i)
                                                {
                                                    case 1:
                                                        objProductImage.ImageType = ProductImageImageType.PT1;
                                                        break;

                                                    case 2:
                                                        objProductImage.ImageType = ProductImageImageType.PT2;
                                                        break;

                                                    case 3:
                                                        objProductImage.ImageType = ProductImageImageType.PT3;
                                                        break;

                                                    case 4:
                                                        objProductImage.ImageType = ProductImageImageType.PT4;
                                                        break;

                                                    case 5:
                                                        objProductImage.ImageType = ProductImageImageType.PT5;
                                                        break;

                                                    case 6:
                                                        objProductImage.ImageType = ProductImageImageType.PT6;
                                                        break;

                                                    case 7:
                                                        objProductImage.ImageType = ProductImageImageType.PT7;
                                                        break;

                                                    case 8:
                                                        objProductImage.ImageType = ProductImageImageType.PT8;
                                                        break;

                                                    case 9:
                                                        objProductImage.ImageType = ProductImageImageType.Search;
                                                        break;

                                                    case 10:
                                                        objProductImage.ImageType = ProductImageImageType.Swatch;
                                                        break;
                                                }
                                            }
                                            objProductImage.ImageLocation = ImagePath;
                                            objProductImageMessage.Item = objProductImage;
                                            LogMessage = "Image Feed content created for Merchant : " + MerchantToken + ", for Product(SKU : " + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"])
                                                + " , and Image Location : " + ImagePath + " ) ";
                                            GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From UpdateItemFeed : " + LogMessage);
                                            lstProductImageMessages.Add(objProductImageMessage);
                                            i++;
                                            PImageMessageId++;
                                        }
                                        catch (Exception ex)
                                        {
                                            ErrorMessage = "Error generating Product Image feed file for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item Name : "
                                                + CCommon.ToString(dr["vcItemName"]) + ". " + Environment.NewLine + ex.Message;
                                            GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From UpdateItemFeed : " + ErrorMessage);
                                            GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From UpdateItemFeed : " + ex.StackTrace);
                                        }
                                    }
                                }
                                MessageId++;
                            }
                        }
                        else
                        {
                            ErrorMessage = "Error Creating feed file for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item Name : "
                            + CCommon.ToString(dr["vcItemName"]) + ". Either one must not have an empty Value";
                            GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From UpdateItemFeed : " + ErrorMessage);
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorMessage = "Error generating feed file for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item Name : "
                            + CCommon.ToString(dr["vcItemName"]) + ". " + Environment.NewLine + ex.Message;
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From UpdateItemFeed : " + ErrorMessage);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From UpdateItemFeed : " + ex.StackTrace);
                    }
                }

                Header objHeader = new Header();
                objHeader.DocumentVersion = "1.0";
                objHeader.MerchantIdentifier = MerchantToken;
                string FileName = "";
                string path = "";

                if (lstProductMessages.Count > 0)
                {
                    objProductEnvelope.Header = objHeader;
                    //objProductEnvelope.MarketplaceName = "";
                    objProductEnvelope.MessageType = AmazonEnvelopeMessageType.Product;
                    objProductEnvelope.PurgeAndReplace = false;
                    objProductEnvelope.PurgeAndReplaceSpecified = true;
                    objProductEnvelope.Message = lstProductMessages.ToArray();
                    FileName = GenerateFileName(objProductEnvelope.Header.MerchantIdentifier, "PRODUCT");
                    path = GeneralFunctions.GetPath(FeedFilePath, DomainId) + FileName + ".xml";
                    GeneralFunctions.SerializeObjectToFile<AmazonEnvelope>(objProductEnvelope, path);
                }
                if (lstInventoryMessages.Count > 0)
                {
                    objInventoryEnvelope.Header = objHeader;
                    //objInventoryEnvelope.MarketplaceName = "";
                    objInventoryEnvelope.MessageType = AmazonEnvelopeMessageType.Inventory;
                    objInventoryEnvelope.PurgeAndReplace = false;
                    objInventoryEnvelope.PurgeAndReplaceSpecified = true;
                    objInventoryEnvelope.Message = lstInventoryMessages.ToArray();
                    FileName = GenerateFileName(objInventoryEnvelope.Header.MerchantIdentifier, "INVENTORY");
                    path = GeneralFunctions.GetPath(FeedFilePath, DomainId) + FileName + ".xml";
                    GeneralFunctions.SerializeObjectToFile<AmazonEnvelope>(objInventoryEnvelope, path);
                }

                if (lstPriceMessages.Count > 0)
                {
                    objPriceEnvelope.Header = objHeader;
                    //objInventoryEnvelope.MarketplaceName = "";
                    objPriceEnvelope.MessageType = AmazonEnvelopeMessageType.Price;
                    objPriceEnvelope.PurgeAndReplace = false;
                    objPriceEnvelope.PurgeAndReplaceSpecified = true;
                    objPriceEnvelope.Message = lstPriceMessages.ToArray();
                    FileName = GenerateFileName(objPriceEnvelope.Header.MerchantIdentifier, "PRICING");
                    path = GeneralFunctions.GetPath(FeedFilePath, DomainId) + FileName + ".xml";
                    GeneralFunctions.SerializeObjectToFile<AmazonEnvelope>(objPriceEnvelope, path);
                }

                if (lstProductImageMessages.Count > 0)
                {
                    objProductImageEnvelope.Header = objHeader;
                    objProductImageEnvelope.MessageType = AmazonEnvelopeMessageType.ProductImage;
                    objProductImageEnvelope.PurgeAndReplaceSpecified = false;
                    objProductImageEnvelope.Message = lstProductImageMessages.ToArray();
                    FileName = GenerateFileName(objProductImageEnvelope.Header.MerchantIdentifier, "IMAGE");
                    path = GeneralFunctions.GetPath(FeedFilePath, DomainId) + FileName + ".xml";
                    GeneralFunctions.SerializeObjectToFile<AmazonEnvelope>(objProductImageEnvelope, path);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = "Error generating Amazon Product feed file . " + Environment.NewLine + ex.Message;
                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From UpdateItemFeed : " + ErrorMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From UpdateItemFeed : " + ex.StackTrace);
            }
        }

        /// <summary>
        /// Creates order fullfilment feed with order Item tracking number
        /// </summary>
        /// <param name="DomainId">Domain ID</param>
        /// <param name="AmazonOrderId">Amazon Order Id</param>
        /// <param name="AmazonOrderItemId">Amazon Order Item Id</param>
        /// <param name="TrackingNo">Tracking Number</param>
        /// <param name="ShippingMethod">Shipping Method</param>
        /// <param name="MerTrackingId">Merchant Tracking Id(BizDoc ID)</param>
        /// <param name="MerchantToken">Merchant Token</param>
        public void CreateOrderFulfillmentFeed(long DomainId, string AmazonOrderId, string AmazonOrderItemId, string ShippingCarrier, string TrackingNo,
            string ShippingMethod, long MerTrackingId, string MerchantToken)
        {
            try
            {
                AmazonEnvelope objAmaEnvelope = new AmazonEnvelope();
                List<OrderFulfillmentItem> obj1 = new List<OrderFulfillmentItem>();
                OrderFulfillmentItem obj = new OrderFulfillmentItem();
                obj1.Add(obj);
                obj.ItemElementName = ItemChoiceType4.AmazonOrderItemCode;
                obj.Item = AmazonOrderItemId;

                OrderFulfillment objOrderFulfillment = new OrderFulfillment();
                objOrderFulfillment.FulfillmentDate = DateTime.Now;
                objOrderFulfillment.MerchantFulfillmentID = CCommon.ToString(MerTrackingId);
                objOrderFulfillment.Item1 = obj1.ToArray();
                objOrderFulfillment.ItemElementName = ItemChoiceType3.AmazonOrderID;
                objOrderFulfillment.Item = AmazonOrderId;

                OrderFulfillmentFulfillmentData objFulfillmentData = new OrderFulfillmentFulfillmentData();
                objFulfillmentData.Item = GetShippingCarrierCode(ShippingCarrier);
                objFulfillmentData.ShipperTrackingNumber = TrackingNo;
                //objFulfillmentData.ShippingMethod = ShippingMethod;
                objOrderFulfillment.FulfillmentData = objFulfillmentData;

                Header objHeader = new Header();
                objHeader.DocumentVersion = "1.0";
                //objHeader.MerchantIdentifier = "M_JOSEPHXAVI_1080921";
                objHeader.MerchantIdentifier = MerchantToken;

                objAmaEnvelope.Header = objHeader;
                objAmaEnvelope.MessageType = AmazonEnvelopeMessageType.OrderFulfillment;
                objAmaEnvelope.PurgeAndReplace = false;
                objAmaEnvelope.PurgeAndReplaceSpecified = true;

                AmazonEnvelopeMessage objMessage = new AmazonEnvelopeMessage();
                objMessage.MessageID = "1";
                objMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                objMessage.OperationTypeSpecified = true;
                objMessage.Item = objOrderFulfillment;

                objAmaEnvelope.Message = new AmazonEnvelopeMessage[] { objMessage };

                string FileName = GenerateFileName(objAmaEnvelope.Header.MerchantIdentifier, "FULFILLMENT");
                string path = GeneralFunctions.GetPath(FeedFilePath, DomainId) + FileName + ".xml";
                GeneralFunctions.SerializeObjectToUTF8_File<AmazonEnvelope>(objAmaEnvelope, path);
                //LogMessage = "Feed content created for Merchant : " + objAmaEnvelope.Header.MerchantIdentifier + "\n  FeedType : INVENTORY \n" + "File Location : " + path;

                //GeneralFunctions.WriteMessage("LOG", LogMessage);

            }
            catch (Exception ex)
            {
                throw ex;
                // GeneralFunctions.WriteMessage("ERR", Ex.Message);
            }

        }

        public void OrderAcknowledgementFeed(long DomainId, string AmazonOrderId, string AmazonOrderItemId, string MerchantToken, string OrderStatus)
        {
            OrderAcknowledgement objOrderAcknowledgement = new OrderAcknowledgement();

            objOrderAcknowledgement.StatusCode = GetAmazonOrderStatus(OrderStatus);

            OrderAcknowledgementItem objOrderAcknowledgementItem = new OrderAcknowledgementItem();
            objOrderAcknowledgementItem.AmazonOrderItemCode = "";
            objOrderAcknowledgementItem.AmazonOrderItemCode = "";
            objOrderAcknowledgementItem.CancelReasonSpecified = true;
            objOrderAcknowledgementItem.CancelReason = OrderAcknowledgementItemCancelReason.CustomerReturn;

            // objOrderAcknowledgement.Item = objOrderAcknowledgementItem;
        }

        private OrderAcknowledgementStatusCode GetAmazonOrderStatus(string OrderStatus)
        {
            OrderAcknowledgementStatusCode StatusCode = new OrderAcknowledgementStatusCode();
            try
            {
                switch (OrderStatus.ToUpper())
                {
                    case "SUCCESS":
                        StatusCode = OrderAcknowledgementStatusCode.Success;
                        break;

                    case "FAILURE":
                        StatusCode = OrderAcknowledgementStatusCode.Failure;
                        break;

                    default:
                        StatusCode = OrderAcknowledgementStatusCode.Success;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return StatusCode;
        }
        #endregion  Feed Generation

        #region Inventory Management

        /// <summary>
        /// Creates Inventory Feed file
        /// </summary>
        /// <param name="DomainId">Domain ID</param>
        /// <param name="dtNewItems">Item List</param>
        /// <param name="MerchantToken">Merchant Token</param>
        public void UpdateInventoryFeed(long DomainId, DataTable dtNewItems, string MerchantToken)
        {
            string LogMessage = "", ErrorMessage = "";

            AmazonEnvelope objInventoryEnvelope = new AmazonEnvelope();
            List<AmazonEnvelopeMessage> lstInventoryMessages = new List<AmazonEnvelopeMessage>();
            BACRM.BusinessLogic.Item.CItems objItems = new BACRM.BusinessLogic.Item.CItems();
            DataTable dtItemDetails = new DataTable();
            int MessageId = 1;
            try
            {
                foreach (DataRow dr in dtNewItems.Rows)
                {
                    try
                    {
                        if (CCommon.ToString(dr["vcItemName"]).Length > 0 & CCommon.ToString(dr["numItemCode"]).Length > 0)
                        {
                            objItems.ItemCode = CCommon.ToInteger(dr["numItemCode"]);
                            string ListPrice = CCommon.ToString(dr["monListPrice"]);
                            TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);

                            objItems.ClientZoneOffsetTime = CCommon.ToInteger(diff1.TotalMinutes);
                            dtItemDetails = objItems.ItemDetails();

                            //Check if matrix item ? yes then check matching SKU from Warehouse
                            if (CCommon.ToLong(dtItemDetails.Rows[0]["numItemGroup"]) > 0)
                            {
                                objItems.byteMode = 1;
                                DataSet dsItemWarehouse = objItems.GetItemWareHouses();
                                DataTable dtItemWarehouse = dsItemWarehouse.Tables[0];
                                for (int intCnt = 0; intCnt <= dtItemWarehouse.Rows.Count - 1; intCnt++)
                                {
                                    if (CCommon.ToString(dtItemWarehouse.Rows[intCnt]["SKU"]).Length > 0 && CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Length > 0) // & CCommon.ToString(dtItemWarehouse.Rows[intCnt]["BarCode"]).Length > 0)
                                    {
                                        if (CCommon.ToString(dtItemWarehouse.Rows[intCnt]["SKU"]).Length > 0 && CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Length > 0 && CCommon.ToString(dtItemWarehouse.Rows[intCnt]["BarCode"]).Length > 0)
                                        {
                                            AmazonEnvelopeMessage objInventoryMessage = new AmazonEnvelopeMessage();
                                            objInventoryMessage.MessageID = CCommon.ToString(MessageId);
                                            objInventoryMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                                            objInventoryMessage.OperationTypeSpecified = true;

                                            Inventory objInventory = new Inventory();
                                            objInventory.SKU = CCommon.ToString(dtItemWarehouse.Rows[intCnt]["SKU"]);
                                            objInventory.Quantity = CCommon.ToString(dtItemWarehouse.Rows[intCnt]["SalesOnHand"]);
                                            objInventory.FulfillmentLatency = "1";
                                            objInventory.RestockDateSpecified = false;
                                            objInventory.SwitchFulfillmentToSpecified = false;
                                            objInventoryMessage.Item = objInventory;
                                            lstInventoryMessages.Add(objInventoryMessage);

                                            LogMessage = "INVENTORY Feed content created for Merchant : " + MerchantToken + "\n for Product(SKU : "
                                                + CCommon.ToString(dtItemWarehouse.Rows[intCnt]["SKU"]) + " Quantity : " + CCommon.ToString(dtItemWarehouse.Rows[intCnt]["SalesOnHand"]) + ")";
                                            GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From UpdateInvetoryFeed : " + LogMessage);
                                        }
                                    }
                                    else
                                    {

                                    }
                                }
                            }
                            else
                            {
                                if (CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]).Length > 0 & CCommon.ToString(dtItemDetails.Rows[0]["vcItemName"]).Length > 0 & CCommon.ToString(dtItemDetails.Rows[0]["numBarCodeId"]).Length > 0)
                                {
                                    AmazonEnvelopeMessage objInventoryMessage = new AmazonEnvelopeMessage();
                                    objInventoryMessage.MessageID = CCommon.ToString(MessageId);
                                    objInventoryMessage.OperationType = AmazonEnvelopeMessageOperationType.Update;
                                    objInventoryMessage.OperationTypeSpecified = true;

                                    Inventory objInventory = new Inventory();
                                    objInventory.SKU = CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]);
                                    objInventory.Quantity = CCommon.ToString(dr["QtyOnHand"]);
                                    objInventory.FulfillmentLatency = "1";
                                    objInventory.RestockDateSpecified = false;
                                    objInventory.SwitchFulfillmentToSpecified = false;
                                    objInventoryMessage.Item = objInventory;
                                    lstInventoryMessages.Add(objInventoryMessage);

                                    LogMessage = "INVENTORY Feed content created for Merchant : " + MerchantToken + "\n for Product(SKU : "
                                        + CCommon.ToString(dtItemDetails.Rows[0]["vcSKU"]) + " Quantity : " + CCommon.ToString(dr["QtyOnHand"]) + ")";
                                    GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From UpdateInvetoryFeed : " + LogMessage);
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorMessage = "Error generating Inventory feed file for Item Code : " + CCommon.ToString(dr["numItemCode"]) + ", Item Name : "
                            + CCommon.ToString(dr["vcItemName"]) + ". " + Environment.NewLine + ex.Message;
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From UpdateInvetoryFeed : " + ErrorMessage);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From UpdateInvetoryFeed : " + ex.StackTrace);
                    }
                    MessageId++;
                }

                Header objHeader = new Header();
                objHeader.DocumentVersion = "1.0";
                objHeader.MerchantIdentifier = MerchantToken;
                objInventoryEnvelope.Header = objHeader;
                //objInventoryEnvelope.MarketplaceName = "";
                objInventoryEnvelope.MessageType = AmazonEnvelopeMessageType.Inventory;
                objInventoryEnvelope.PurgeAndReplace = false;
                objInventoryEnvelope.PurgeAndReplaceSpecified = true;
                objInventoryEnvelope.Message = lstInventoryMessages.ToArray();
                string FileName = GenerateFileName(objInventoryEnvelope.Header.MerchantIdentifier, "INVENTORY");
                string path = GeneralFunctions.GetPath(FeedFilePath, DomainId) + FileName + ".xml";
                GeneralFunctions.SerializeObjectToFile<AmazonEnvelope>(objInventoryEnvelope, path);
            }
            catch (Exception ex)
            {
                ErrorMessage = "Error generating Amazon Inventory feed file . " + Environment.NewLine + ex.Message;
                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From UpdateInvetoryFeed : " + ErrorMessage);
                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From UpdateInvetoryFeed : " + ex.StackTrace);
            }
        }

        #endregion Inventory Management

        #region Feed Processing

        /// <summary>
        /// Reads the Feed Files for the Merchant and forward Feed request to Amazon Communicator Service 
        /// </summary>
        /// <param name="objSvcConfigInfo">Service Configuration Information</param>
        /// <param name="MerchantToken">Merchant Token to process Only Feed files of the Merchant</param>
        /// <param name="DomainId">Domain Id</param>
        public void ProcessFeedRequests(ServiceConfigInfo objSvcConfigInfo, string MerchantToken, long DomainId)
        {
            string LogMessage = "", FeedSubmissionId = "";
            try
            {
                string DestFileName = "";
                string FileName = "";
                string destFile = "";
                string sourceDir = GeneralFunctions.GetPath(FeedFilePath, DomainId);
                string destDir = GeneralFunctions.GetPath(FeedSubmitedFilePath, DomainId);
                string FeedType = "";
                int ProcessCount = 0;
                if (Directory.Exists(sourceDir))
                {
                    string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                    List<string> MerchantFiles = new List<string>();
                    List<string> ReOrderFeedFiles = new List<string>();
                    if (fileEntries.Length > 0)
                    {
                        foreach (string filepath in fileEntries)
                        {
                            try
                            {
                                FileName = Path.GetFileNameWithoutExtension(filepath);
                                if (FileName.Substring(0, 5) == MerchantToken.Substring(0, 5))
                                {
                                    MerchantFiles.Add(filepath);//Gets Feed files of the Merchant
                                }
                            }
                            catch (Exception Ex)
                            {
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessFeedRequests : " + Ex.Message);
                            }
                        }

                        //Reorder the Feed files
                        //Gets Product Feed Files First in the Order
                        foreach (string filepath in MerchantFiles)
                        {
                            try
                            {
                                FileName = Path.GetFileNameWithoutExtension(filepath);
                                if (FileName.Substring(6, 3) == "PRO")
                                {
                                    ReOrderFeedFiles.Add(filepath);
                                }
                            }
                            catch (Exception Ex)
                            {
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessFeedRequests : " + Ex.Message);
                            }
                        }
                        //Gets Inventory Feed Files next in the Order
                        foreach (string filepath in MerchantFiles)
                        {
                            try
                            {
                                FileName = Path.GetFileNameWithoutExtension(filepath);
                                if (FileName.Substring(6, 3) == "INV")
                                {
                                    ReOrderFeedFiles.Add(filepath);
                                }
                            }
                            catch (Exception Ex)
                            {
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessFeedRequests : " + Ex.Message);
                            }
                        }
                        //Gets Pricing Feed Files next in the Order
                        foreach (string filepath in MerchantFiles)
                        {
                            try
                            {
                                FileName = Path.GetFileNameWithoutExtension(filepath);
                                if (FileName.Substring(6, 3) == "PRI")
                                {
                                    ReOrderFeedFiles.Add(filepath);
                                }
                            }
                            catch (Exception Ex)
                            {
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessFeedRequests : " + Ex.Message);
                            }
                        }

                        //Gets Image Feed Files next in the Order
                        foreach (string filepath in MerchantFiles)
                        {
                            try
                            {
                                FileName = Path.GetFileNameWithoutExtension(filepath);
                                if (FileName.Substring(6, 3) == "IMA")
                                {
                                    ReOrderFeedFiles.Add(filepath);
                                }
                            }
                            catch (Exception Ex)
                            {
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessFeedRequests : " + Ex.Message);
                            }
                        }

                        //Gets Order Fulfilment Feed Files next in the Order
                        foreach (string filepath in MerchantFiles)
                        {
                            try
                            {
                                FileName = Path.GetFileNameWithoutExtension(filepath);
                                if (FileName.Substring(6, 3) == "FUL")
                                {
                                    ReOrderFeedFiles.Add(filepath);
                                }
                            }
                            catch (Exception Ex)
                            {
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessFeedRequests : " + Ex.Message);
                            }
                        }

                        //Process the Reordered Feed Files
                        foreach (string file in ReOrderFeedFiles)//Process Feed Requests
                        {
                            ProcessCount += 1;
                            if (ProcessCount >= 5)
                                return;
                            try
                            {
                                DestFileName = Path.GetFileNameWithoutExtension(file);
                                FeedType = GetFeedTypeFromFileName(DestFileName);
                                if (FeedType != "")
                                {
                                    MemoryStream ms = new MemoryStream();
                                    AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                                    // XmlReader Reader = new XmlTextReader(filePath, ms);
                                    Stream fs = File.Open(file, FileMode.Open, FileAccess.Read);
                                    fs.CopyTo(ms);
                                    fs.Close();
                                    ms.Position = 0;
                                    FeedSubmissionId = objClient.SubmitFeed(objSvcConfigInfo, FeedType, ms);
                                    if (FeedSubmissionId != "")
                                    {
                                        LogMessage = "FeedType : " + FeedType + " STATUS : SUBMITTED " + " ID : " + FeedSubmissionId;
                                        GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From ProcessFeedRequests : " + LogMessage);
                                    }
                                    objClient.Close();
                                    if (!System.IO.Directory.Exists(destDir))
                                        System.IO.Directory.CreateDirectory(destDir);

                                    DestFileName = FeedSubmissionId + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml";
                                    destFile = System.IO.Path.Combine(destDir, DestFileName);
                                    System.IO.File.Copy(file, destFile, true);
                                    File.Delete(file);
                                }
                            }
                            catch (FaultException<error> ex)
                            {
                                string ErrorMsg = "Error in submitting  Amazon feed file" + Path.GetFileNameWithoutExtension(file);
                                File.Copy(file, GeneralFunctions.GetPath("Error_AmazonFeedSubmit", DomainId) + Path.GetFileNameWithoutExtension(file) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                File.Delete(file);
                                error er = ex.Detail;
                                string ErrMessage = "Error received from Amazon Service : " + er.msg;
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessFeedRequests : " + ErrorMsg + " -" + ErrMessage);
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessFeedRequests : " + er.request);
                            }
                            catch (Exception Ex)
                            {
                                string ErrorMsg = "Error in submitting  Amazon feed file" + Path.GetFileNameWithoutExtension(file);
                                File.Copy(file, GeneralFunctions.GetPath("Error_AmazonFeedSubmit", DomainId) + Path.GetFileNameWithoutExtension(file) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml");
                                File.Delete(file);
                                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessFeedRequests : " + Ex.Message);
                            }
                        }
                    }
                }

            }
            catch (Exception Ex)
            {
                GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessFeedRequests : " + Ex.Message);
            }
        }

        /// <summary>
        /// Gets the Feed Submission Result
        /// (Not Used as of Now)
        /// </summary>
        /// <param name="objSvcConfigInfo">Service Configuration Information</param>
        /// <param name="FeedSubmissionID">Feed Submission ID</param>
        public void GetFeedSubmissionResult(ServiceConfigInfo objSvcConfigInfo, long DomainId, string FeedSubmissionID)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                ms = objClient.FeedSubmissionResult(objSvcConfigInfo, FeedSubmissionID);
                XmlWriter writer = new XmlTextWriter(ms, Encoding.Unicode);
                objClient.Close();
                string FileName = GeneralFunctions.GetPath(FeedResultFilePath, DomainId) + FeedSubmissionID + ".xml";
                using (FileStream fs = File.Open(FileName, FileMode.Create, FileAccess.Write))
                {
                    ms.WriteTo(fs);
                    fs.Flush();
                    fs.Close();
                    ms.Dispose();
                }
                AmazonEnvelope objAmaEnv = new AmazonEnvelope();
                GeneralFunctions.DeSerializeFileToObject<AmazonEnvelope>(out objAmaEnv, FileName);
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Amazon Service : " + er.msg;
                throw new Exception("Amazon:- From GetFeedSubmissionResult : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion Feed Processing

        #region Products Operation

        /// <summary>
        /// Gets the Amazon's ASIN List for the Merchant SKU List
        /// </summary>
        /// <param name="objSvcConfigInfo">Service Configuration Information</param>
        /// <param name="SKU">List of Seller's Stock Keeping Unique Identifier for the Item</param>
        /// <returns type="Dictionary"(KeyValue Collection)>SKU_ASIN</returns>
        public Dictionary<string, string> GetASIN_From_SKU(ServiceConfigInfo objSvcConfigInfo, List<string> SKU)
        {
            Dictionary<string, string> SKU_ASIN = new Dictionary<string, string>();
            try
            {
                AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                SKU_ASIN = objClient.GetASIN_From_SKU(objSvcConfigInfo, SKU.ToArray());
                objClient.Close();
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Amazon Service : " + er.msg;
                throw new Exception("Amazon:- From GetASIN_From_SKU : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return SKU_ASIN;
        }

        /// <summary>
        /// Gets Complete Item Details from Amazon MWS for Seller SKU 
        /// and Creates/Maps a Item Classification ID for List of Items
        /// Add Items to Biz and Map ItemApi Table with WebApi Item Id for each item
        /// Creates/Updates WebApi Item Detail (.xml) file with Amazon specific Details
        /// </summary>
        /// <param name="DomainId">Domain ID</param>
        /// <param name="WebApiId">WebAPI ID</param>
        /// <param name="RecordOwner">Default Record Owner</param>
        /// <param name="dtProductListings">List of Items with Basic Item Details</param>
        public void ProcessProductListingReport(long DomainId, int WebApiId, long RecordOwner, long UserContactID, DataTable dtProductListings, int WareHouseID)
        {
            string LogMessage = "";
            AmazonProduct objAmazonProduct = new AmazonProduct();
            DataTable dtProductDetails = new DataTable();
            try
            {
                BizCommonFunctions.CreateProductDetailsTable(dtProductDetails); //Get Item Detail Structure as for Biz
                foreach (DataRow drItem in dtProductListings.Rows)
                {
                    try
                    {
                        //string SellerSKU = CCommon.ToString(drItem["seller-sku"]);
                        string SellerSKU = CCommon.ToString(drItem["sku"]);
                        if (!string.IsNullOrEmpty(SellerSKU))
                        {
                            DataRow dr = dtProductDetails.NewRow();
                            dr = GetItemDetailsForSKU(DomainId, WebApiId, RecordOwner, SellerSKU, dr, ref dtProductListings); //Call to Get Item Detail from Amazon MWS
                            //System.Threading.Thread.Sleep(500);  //To avoid Request Throttling exception in Amazon MWS (if needed)
                            dr["vcItemName"] = drItem["item-name"];
                            dr["monListPrice"] = drItem["price"];
                            dr["txtItemHtmlDesc"] = drItem["item-description"];
                            dr["numQuantity"] = drItem["quantity"];
                            dtProductDetails.Rows.Add(dr);
                        }
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessProductListingReport : " + ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessProductListingReport : " + ex.StackTrace);
                    }
                }
                DataTable dtItemDetails = new DataTable();
                DataRow drMailItemDetail;
                BizCommonFunctions.CreateMailMessageItemDetail(dtItemDetails);
                int ItemCount = 0;

                string ItemClassificationName = "Amazon_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                long ItemClassificationID = 0;
                ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);// Call to get Item Classification ID(New/Existing)

                foreach (DataRow drItem in dtProductDetails.Rows)
                {
                    try
                    {
                        long ItemCode = BizCommonFunctions.AddNewItemToBiz(drItem, DomainId, WebApiId, RecordOwner, WareHouseID, ItemClassificationID);//Call to Add Item in Biz and Get new Biz ItemCode
                        ItemCount += 1;
                        drMailItemDetail = dtItemDetails.NewRow();
                        drMailItemDetail["SNo"] = CCommon.ToString(ItemCount);
                        drMailItemDetail["BizItemCode"] = CCommon.ToString(ItemCode);
                        drMailItemDetail["ItemName"] = drItem["vcItemName"];
                        drMailItemDetail["SKU"] = drItem["vcSKU"];
                        dtItemDetails.Rows.Add(drMailItemDetail);

                        LogMessage = "Added/Updated New Item in BizAutomation, Item Code : " + ItemCode + ", imported from Amazon Online Marketplace ";
                        GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From ProcessProductListingReport : " + LogMessage);

                        VariationCollection vCollection = new VariationCollection();
                        vCollection = objAmazonProduct.ItemVariations;

                        if (vCollection != null)
                        {

                            long ItemGroupID = 0;
                            DataTable dtCustomAttributes = new DataTable();
                            dtCustomAttributes.Columns.Add("ASIN");
                            dtCustomAttributes.Columns.Add("ItemName");
                            dtCustomAttributes.Columns.Add("WListPrice");
                            dtCustomAttributes.Columns.Add("OnHand");
                            dtCustomAttributes.Columns.Add("ReOrder");
                            dtCustomAttributes.Columns.Add("WSKU");
                            dtCustomAttributes.Columns.Add("WBarCode");
                            dtCustomAttributes.Columns.Add("strXML");
                            dtCustomAttributes.AcceptChanges();

                            DataSet dsCustom = new DataSet();
                            DataTable dtCFields = new DataTable();
                            DataTable dtCFValues = new DataTable();

                            WebAPI objWebAPI = new WebAPI();
                            dsCustom = objWebAPI.GetCflList(DomainId, 9);

                            dtCFields = dsCustom.Tables[0];
                            dtCFValues = dsCustom.Tables[1];

                            string strItemGroupName = "";
                            if (dtCFields != null && dtCFields.Rows.Count > 0)
                            {
                                foreach (Variations objVarie in vCollection.VariationList)
                                {
                                    long ListID = 0;
                                    long CustomFieldID = 0;

                                    //if (dtCFields.Select("Fld_label = '" + objVarie.Name.ToString().ToLower() + "'").Length == 0)
                                    if (dtCFields.Select(String.Format("fld_label = '{0}'", objVarie.Name.ToString().ToLower().Replace("'", "''"))).Length == 0)
                                    {
                                        try
                                        {
                                            CustomFields objCusField = new CustomFields();
                                            objCusField.locId = 9;
                                            objCusField.TabId = 0;
                                            objCusField.UserId = UserContactID;
                                            objCusField.FieldLabel = objVarie.Name.ToString();
                                            objCusField.FieldType = "SelectBox";
                                            objCusField.CusFldId = ListID;
                                            objCusField.DomainID = DomainId;
                                            objCusField.URL = "";
                                            objCusField.vcToolTip = objVarie.Name.ToString();
                                            objCusField.CflSave();
                                            CustomFieldID = objCusField.CusFldId;
                                            ListID = objCusField.ListId;

                                            if (strItemGroupName.Contains(CCommon.ToString(objVarie.Name)) == false)
                                            {
                                                strItemGroupName = strItemGroupName + "_" + CCommon.ToString(objVarie.Name);
                                            }

                                            if (CustomFieldID > 0 && ListID > 0)
                                            {
                                                DataRow drFields = dtCFields.NewRow();
                                                drFields["fld_id"] = CustomFieldID;
                                                drFields["Fld_label"] = objVarie.Name.ToString();
                                                drFields["TabName"] = "";
                                                drFields["loc_name"] = "";
                                                drFields["bitIsRequired"] = false;
                                                drFields["bitIsEmail"] = false;
                                                drFields["bitIsAlphaNumeric"] = false;
                                                drFields["bitIsNumeric"] = false;
                                                drFields["bitIsLengthValidation"] = false;
                                                drFields["ListID"] = ListID;
                                                dtCFields.Rows.Add(drFields);
                                                dtCFields.AcceptChanges();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LogMessage = "Error occurred while adding Item Attribute named : " + objVarie.Name.ToString() + ". Details of error :" + ex.Message.ToString();
                                            GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From ProcessOpenProductListingReport : " + LogMessage);
                                        }
                                    }
                                    else
                                    {
                                        ListID = CCommon.ToLong(dtCFields.Select(String.Format("fld_label = '{0}'", objVarie.Name.ToString().ToLower().Replace("'", "''")))[0]["ListID"]);
                                        if (strItemGroupName.Contains(CCommon.ToString(objVarie.Name)) == false)
                                        {
                                            strItemGroupName = strItemGroupName + "_" + CCommon.ToString(objVarie.Name);
                                        }
                                    }

                                    //if (dtCFValues.Select("ListID = " + ListID + " AND vcData = '" + objVarie.Value.ToString().ToLower() + "'").Length == 0)
                                    if (ListID > 0 && dtCFValues.Select(String.Format("vcData = '{0}' AND ListID = " + ListID, objVarie.Value.ToString().ToLower().Replace("'", "''"))).Length == 0)
                                    {
                                        try
                                        {
                                            CCommon objCommon = new CCommon();
                                            objCommon.DomainID = DomainId;
                                            objCommon.ListItemName = objVarie.Value.ToString();
                                            objCommon.ListID = ListID;
                                            objCommon.UserCntID = UserContactID;
                                            objCommon.ManageItemList();

                                            if (ListID > 0)
                                            {
                                                DataRow drFieldValues = dtCFValues.NewRow();
                                                drFieldValues["numListItemID"] = objCommon.ListItemID;
                                                drFieldValues["vcData"] = objVarie.Value.ToString();
                                                drFieldValues["constFlag"] = 0;
                                                drFieldValues["bitDelete"] = 0;
                                                drFieldValues["intSortOrder"] = false;
                                                drFieldValues["ListID"] = ListID;
                                                dtCFValues.Rows.Add(drFieldValues);
                                                dtCFValues.AcceptChanges();
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LogMessage = "Error occurred while adding Item Attribute value : " + objVarie.Value.ToString() + ". Details of error :" + ex.Message.ToString();
                                            GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From ProcessOpenProductListingReport : " + LogMessage);
                                        }
                                    }
                                }

                                foreach (VariationDetails vd in vCollection.VariationDetail)
                                {
                                    string[] strVariations = vd.VariationValue.Split(',');

                                    DataTable dtCA = new DataTable();
                                    dtCA.Columns.Add("Fld_ID");
                                    dtCA.Columns.Add("Fld_Value");
                                    dtCA.TableName = "CusFlds";
                                    dtCA.AcceptChanges();

                                    DataRow drAttr = dtCustomAttributes.NewRow();
                                    drAttr["ASIN"] = objAmazonProduct.ASIN;
                                    drAttr["ItemName"] = objAmazonProduct.Title;
                                    drAttr["WListPrice"] = vd.ListPrice;
                                    drAttr["OnHand"] = vd.Quantity;
                                    drAttr["ReOrder"] = vd.ReOrder;
                                    drAttr["WSKU"] = vd.SKU;
                                    drAttr["WBarCode"] = "";

                                    foreach (string strItem in strVariations)
                                    {
                                        string strKeyName = strItem.Split('~')[0];
                                        string strKeyValue = strItem.Split('~')[1];
                                        DataRow[] drCField = dtCFields.Select(String.Format("fld_label = '{0}'", strKeyName.ToString().Trim().ToLower().Replace("'", "''")));

                                        if (drCField.Length > 0)
                                        {
                                            long lngCFieldID = CCommon.ToLong(drCField[0]["fld_id"]);
                                            long lngListID = CCommon.ToLong(drCField[0]["ListID"]);
                                            if (lngCFieldID > 0)
                                            {
                                                DataRow[] drCValue = dtCFValues.Select("ListID = " + lngListID + " AND " + String.Format("vcData = '{0}'", strKeyValue.ToString().Trim().ToLower().Replace("'", "''")));
                                                if (drCValue.Length > 0)
                                                {
                                                    DataRow drCA = dtCA.NewRow();
                                                    drCA["Fld_ID"] = lngCFieldID;
                                                    drCA["Fld_Value"] = drCValue[0]["numListItemID"];
                                                    dtCA.Rows.Add(drCA);
                                                    dtCA.AcceptChanges();
                                                }
                                            }
                                        }
                                    }

                                    DataSet dsTemp = new DataSet();
                                    dsTemp.Tables.Add(dtCA);

                                    drAttr["strXML"] = dsTemp.GetXml();
                                    dtCustomAttributes.Rows.Add(drAttr);
                                    dtCustomAttributes.AcceptChanges();
                                }
                            }

                            //Create/ Update ItemGroup in Biz if not exist for new / old Item attributes
                            DataTable dtItemGroup = new DataTable();
                            dtItemGroup.TableName = "Table";
                            dtItemGroup.Columns.Add("numOppAccAttrID");

                            DataSet dsItemGroup = new DataSet();

                            BACRM.BusinessLogic.Item.CItems objItem = new BACRM.BusinessLogic.Item.CItems();
                            objItem.ItemGroupID = 0;
                            objItem.DomainID = DomainId;
                            dsItemGroup = objItem.GetItemGroups();

                            if (dsItemGroup != null && dsItemGroup.Tables.Count > 0 && dsItemGroup.Tables[0].Rows.Count > 0)
                            {
                                //string strItemGroupName = "";
                                foreach (DataRow drC in dtCFields.Rows)
                                {
                                    DataRow drIG = dtItemGroup.NewRow();
                                    drIG["numOppAccAttrID"] = drC["fld_id"];
                                    dtItemGroup.Rows.Add(drIG);
                                    dtItemGroup.AcceptChanges();
                                }

                                strItemGroupName = strItemGroupName.Trim('_');
                                if (dsItemGroup.Tables[0].Select(String.Format("vcItemGroup = '{0}'", strItemGroupName.ToString().Trim().ToLower().Replace("'", "''"))).Length == 0)
                                {
                                    DataSet dsTempIG = new DataSet();
                                    dsTempIG.Tables.Add(dtItemGroup);

                                    objItem.ItemGroupID = 0;
                                    objItem.ItemGroupName = strItemGroupName;
                                    objItem.str = dsTempIG.GetXml();
                                    objItem.DomainID = DomainId;
                                    ItemGroupID = objItem.ManageItemGroups();
                                }
                                else
                                {
                                    ItemGroupID = CCommon.ToLong(dsItemGroup.Tables[0].Select(String.Format("vcItemGroup = '{0}'", strItemGroupName.ToString().Trim().ToLower().Replace("'", "''")))[0]["numItemgroupID"]);
                                }

                                strItemGroupName = "";
                            }
                        }

                        WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                        string FilePath = "";// CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml";

                        BACRM.BusinessLogic.Item.CItems objItems = new BACRM.BusinessLogic.Item.CItems();
                        objItems.ItemCode = CCommon.ToInteger(ItemCode);
                        TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);
                        objItems.ClientZoneOffsetTime = CCommon.ToInteger(diff1.TotalMinutes);
                        DataTable dtCheckItemDetails = objItems.ItemDetails();

                        if (dtCheckItemDetails.Rows.Count > 0)
                        {
                            if (CCommon.ToLong(dtCheckItemDetails.Rows[0]["numItemGroup"]) > 0 && CCommon.ToString(drItem["vcSKU"]).Length > 0)
                            {
                                FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + "_" + CCommon.ToString(drItem["vcSKU"]) + ".xml";
                            }
                            else
                            {
                                FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml";
                            }
                        }
                        else
                        {
                            FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml";
                        }

                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                            XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                            objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                            objStreamReader.Close();
                        }
                        objWebAPIItemDetail.DimensionUOM = BizCommonFunctions.GetLengthUOMCode(CCommon.ToString(drItem["DimensionUOM"]));
                        objWebAPIItemDetail.WeightUOM = BizCommonFunctions.GetWeightUOMCode(CCommon.ToString(drItem["WeightUOM"]));
                        objWebAPIItemDetail.Length = CCommon.ToString(drItem["fltLength"]);
                        objWebAPIItemDetail.Height = CCommon.ToString(drItem["fltHeight"]);
                        objWebAPIItemDetail.Width = CCommon.ToString(drItem["fltWidth"]);
                        objWebAPIItemDetail.Weight = CCommon.ToString(drItem["fltWeight"]);
                        objWebAPIItemDetail.WarrantyDescription = CCommon.ToString(drItem["Warranty"]);

                        StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml");
                        if (dtCheckItemDetails.Rows.Count > 0)
                        {
                            if (CCommon.ToLong(dtCheckItemDetails.Rows[0]["numItemGroup"]) > 0 && CCommon.ToString(drItem["vcSKU"]).Length > 0)
                            {
                                objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + "_" + CCommon.ToString(drItem["vcSKU"]) + ".xml");
                            }
                            else
                            {
                                objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml");
                            }
                        }
                        else
                        {
                            objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml");
                        }

                        XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                        Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                        objStreamWriter.Close();

                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessProductListingReport : " + ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessProductListingReport : " + ex.StackTrace);
                    }
                }

                BizCommonFunctions.SendMailNewItemList(DomainId, WebApiId, RecordOwner, UserContactID, ItemCount, dtItemDetails, ItemClassificationName, "Amazon");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ProcessOpenProductListingReport(long DomainId, int WebApiId, long RecordOwner, long UserContactID, ref DataTable dtProductListings, int WareHouseID)
        {
            string LogMessage = "";
            AmazonProduct objAmazonProduct = new AmazonProduct();
            DataTable dtProductDetails = new DataTable();
            try
            {
                BizCommonFunctions.CreateProductDetailsTable(dtProductDetails); //Get Item Detail Structure as for Biz

                DataTable dtCopy = new DataTable();
                DataRow[] drCopy;
                short sIsChild = 0;

                foreach (DataRow drItem in dtProductListings.Rows)
                {
                    try
                    {
                        string SellerSKU = CCommon.ToString(drItem["SellerSKU"]);

                        dtCopy = dtProductListings.Copy();
                        drCopy = dtCopy.Select("asin1='" + drItem["asin1"] + "'");
                        if (drCopy != null && drCopy.Length > 0)
                        {
                            sIsChild = CCommon.ToShort(drCopy[0]["IsChild"]);
                        }

                        if (CCommon.ToShort(drItem["IsChild"]) == sIsChild)
                        {
                            if (!string.IsNullOrEmpty(SellerSKU))
                            {
                                DataRow dr = dtProductDetails.NewRow();
                                dr = GetItemDetailsForSKU(DomainId, WebApiId, RecordOwner, SellerSKU, dr, ref dtProductListings); //Call to Get Item Detail from Amazon MWS
                                //System.Threading.Thread.Sleep(500);  //To avoid Request Throttling exception in Amazon MWS (if needed)
                                dr["txtItemHtmlDesc"] = CCommon.ToString(drItem["item-description"]);
                                dr["numQuantity"] = CCommon.ToInteger(drItem["quantity"]);
                                dtProductDetails.Rows.Add(dr);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessOpenProductListingReport : " + ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessOpenProductListingReport : " + ex.StackTrace);
                    }

                    if (sIsChild == 0)
                    {
                        dtCopy = dtProductListings.Copy();
                    }
                }

                DataTable dtItemDetails = new DataTable();
                DataRow drMailItemDetail;
                BizCommonFunctions.CreateMailMessageItemDetail(dtItemDetails);
                int ItemCount = 0;

                string ItemClassificationName = "Amazon_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                long ItemClassificationID = 0;
                ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);// Call to get Item Classification ID(New/Existing)

                foreach (DataRow drItem in dtProductDetails.Rows)
                {
                    try
                    {
                        long ItemCode = BizCommonFunctions.AddNewItemToBiz(drItem, DomainId, WebApiId, RecordOwner, WareHouseID, ItemClassificationID);//Call to Add Item in Biz and Get new Biz ItemCode
                        ItemCount += 1;
                        drMailItemDetail = dtItemDetails.NewRow();
                        drMailItemDetail["SNo"] = CCommon.ToString(ItemCount);
                        drMailItemDetail["BizItemCode"] = CCommon.ToString(ItemCode);
                        drMailItemDetail["ItemName"] = drItem["vcItemName"];
                        drMailItemDetail["SKU"] = drItem["vcSKU"];
                        dtItemDetails.Rows.Add(drMailItemDetail);

                        LogMessage = "Added/Updated New Item in BizAutomation, Item Code : " + ItemCode + ", imported from Amazon Online Marketplace ";
                        GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From ProcessOpenProductListingReport : " + LogMessage);

                        WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                        string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml";
                        if (CCommon.ToLong(drItem["numItemGroup"]) > 0 && CCommon.ToString(drItem["vcSKU"]).Length > 0)
                        {
                            FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + "_" + CCommon.ToString(drItem["vcSKU"]) + ".xml";
                        }
                        else
                        {
                            FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml";
                        }

                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                            XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                            objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                            objStreamReader.Close();
                        }
                        objWebAPIItemDetail.DimensionUOM = BizCommonFunctions.GetLengthUOMCode(CCommon.ToString(drItem["DimensionUOM"]));
                        objWebAPIItemDetail.WeightUOM = BizCommonFunctions.GetWeightUOMCode(CCommon.ToString(drItem["WeightUOM"]));
                        objWebAPIItemDetail.Length = CCommon.ToString(drItem["fltLength"]);
                        objWebAPIItemDetail.Height = CCommon.ToString(drItem["fltHeight"]);
                        objWebAPIItemDetail.Width = CCommon.ToString(drItem["fltWidth"]);
                        objWebAPIItemDetail.Weight = CCommon.ToString(drItem["fltWeight"]);
                        objWebAPIItemDetail.WarrantyDescription = CCommon.ToString(drItem["Warranty"]);

                        StreamWriter objStreamWriter;// = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml");
                        if (CCommon.ToLong(dtItemDetails.Rows[0]["numItemGroup"]) > 0 && CCommon.ToString(drItem["vcSKU"]).Length > 0)
                        {
                            objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + "_" + CCommon.ToString(drItem["vcSKU"]) + ".xml");
                        }
                        else
                        {
                            objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(ItemCode) + ".xml");
                        }

                        XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                        Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                        objStreamWriter.Close();

                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessOpenProductListingReport : " + ex.Message);
                        GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From ProcessOpenProductListingReport : " + ex.StackTrace);
                    }
                }

                BizCommonFunctions.SendMailNewItemList(DomainId, WebApiId, RecordOwner, UserContactID, ItemCount, dtItemDetails, ItemClassificationName, "Amazon");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets Item Details from Marketplace for given Seller SKU and fills ItemDetails Datarow as needed in Biz to add new Item
        /// </summary>
        /// <param name="DomainId">Domain ID</param>
        /// <param name="WebApiId">WebApi ID</param>
        /// <param name="RecordOwner">Default Record Owner</param>
        /// <param name="SellerSKU">Seller's SKU</param>
        /// <param name="dr">Datarow with Structure of Item Details required as for Biz</param>
        /// <param name="dtProductListings">Datatable with Item Details required as for Biz</param>
        /// <returns>DataRow of Item Detail as required in Biz to Add Item</returns>
        public DataRow GetItemDetailsForSKU(long DomainId, int WebApiId, long RecordOwner, string SellerSKU, DataRow dr, ref DataTable dtProductListings)
        {
            AmazonProductList objAmazonProductList = new AmazonProductList();
            //AmazonProduct objAmazonProduct = new AmazonProduct();
            try
            {
                UserAccess objUserAccess = new UserAccess();
                objUserAccess.DomainID = DomainId;
                DataTable dtDomainDetails = objUserAccess.GetDomainDetails();
                int DefaultIncomeAccID = 0, DefaultCOGSAccID = 0, DefaultAssetAccID = 0;

                if (dtDomainDetails.Rows.Count > 0)
                {
                    DefaultIncomeAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numIncomeAccID"]);
                    DefaultCOGSAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numCOGSAccID"]);
                    DefaultAssetAccID = CCommon.ToInteger(dtDomainDetails.Rows[0]["numAssetAccID"]);
                    if (DefaultIncomeAccID == 0 || DefaultCOGSAccID == 0 || DefaultAssetAccID == 0)
                    {
                        string ExcepitonMessage = "Unable to add Online Marketplace Items into BizAutomation. Either default Income Account, COGS Account or Assert Account not set.,";
                        throw new Exception("Amazon:- From GetItemDetailsForSKU : " + ExcepitonMessage);
                    }
                    WebAPI objWebAPi = new WebAPI();
                    objWebAPi.DomainID = DomainId;
                    objWebAPi.WebApiId = WebApiId;
                    objWebAPi.Mode = 3;
                    DataTable dtWebAPI = objWebAPi.GetWebApi();

                    DataTable dtCFields = new DataTable();
                    DataTable dtCFValues = new DataTable();
                    DataSet dsCustFld = null;
                    CustomFields objCusField = new CustomFields();
                    objCusField.DomainID = DomainId;
                    objCusField.locId = 9;
                    dsCustFld = objCusField.GetCustomFieldListAndData();
                    if (dsCustFld != null && dsCustFld.Tables.Count > 0)
                    {
                        dtCFields = dsCustFld.Tables[0];
                        dtCFValues = dsCustFld.Tables[1];
                    }

                    foreach (DataRow drWebApi in dtWebAPI.Rows)
                    {
                        if (CCommon.ToBool(drWebApi["bitEnableAPI"]) == true)
                        {
                            if (WebApiId == CCommon.ToInteger(drWebApi["WebApiId"]))//Amazon US
                            {
                                try
                                {
                                    ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
                                    objSvcConfigInfo.MerchantID = CCommon.ToString(drWebApi["vcFirstFldValue"]);
                                    objSvcConfigInfo.MarketPlaceID = CCommon.ToString(drWebApi["vcSecondFldValue"]);
                                    objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(drWebApi["vcThirdFldValue"]);
                                    objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(drWebApi["vcFourthFldValue"]);
                                    objSvcConfigInfo.FeedsServiceURL = GetFeedsServiceURL(CCommon.ToString(drWebApi["vcFifthFldValue"]));
                                    objSvcConfigInfo.OrdersServiceURL = GetOrdersServiceURL(CCommon.ToString(drWebApi["vcFifthFldValue"]));
                                    objSvcConfigInfo.ProductServiceURL = GetProductsServiceURL(CCommon.ToString(drWebApi["vcFifthFldValue"]));
                                    objSvcConfigInfo.ServiceURL_Type = 3;
                                    AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                                    objAmazonProductList = objClient.GetProductDetailsForSellerSKU(objSvcConfigInfo, SellerSKU, ref dtProductListings, dtCFields, dtCFValues);

                                    long ItemGroupID = 0;
                                    DataTable dtCustomAttributes = new DataTable();
                                    dtCustomAttributes.Columns.Add("AmazonItemID");
                                    dtCustomAttributes.Columns.Add("ItemName");
                                    dtCustomAttributes.Columns.Add("WListPrice");
                                    dtCustomAttributes.Columns.Add("OnHand");
                                    dtCustomAttributes.Columns.Add("ReOrder");
                                    dtCustomAttributes.Columns.Add("WSKU");
                                    dtCustomAttributes.Columns.Add("WBarCode");
                                    dtCustomAttributes.Columns.Add("strXML");
                                    dtCustomAttributes.AcceptChanges();

                                    VariationCollection vCollection = new VariationCollection();
                                    foreach (AmazonProduct objAmazonProduct in objAmazonProductList.LstAmazonProduct)
                                    {
                                        vCollection = objAmazonProduct.ItemVariations;

                                        if (vCollection != null)
                                        {
                                            WebAPI objWebAPI = new WebAPI();
                                            
                                            string strItemGroupName = "";
                                            if (dtCFields != null && dtCFields.Rows.Count > 0)
                                            {
                                                foreach (Variations objVarie in vCollection.VariationList)
                                                {
                                                    long ListID = 0;
                                                    long CustomFieldID = 0;

                                                    //if (dtCFields.Select("Fld_label = '" + objVarie.Name.ToString().ToLower() + "'").Length == 0)
                                                    if (dtCFields.Select(String.Format("fld_label = '{0}'", objVarie.Name.ToString().ToLower().Replace("'", "''"))).Length == 0)
                                                    {
                                                        try
                                                        {
                                                            if (objCusField == null)
                                                            {
                                                                objCusField = new CustomFields();
                                                            }
                                                            objCusField.locId = 9;
                                                            objCusField.TabId = 0;
                                                            objCusField.UserId = RecordOwner;
                                                            objCusField.FieldLabel = objVarie.Name.ToString();
                                                            objCusField.FieldType = "SelectBox";
                                                            objCusField.CusFldId = ListID;
                                                            objCusField.DomainID = DomainId;
                                                            objCusField.URL = "";
                                                            objCusField.vcToolTip = objVarie.Name.ToString();
                                                            objCusField.CflSave();
                                                            CustomFieldID = objCusField.CusFldId;
                                                            ListID = objCusField.ListId;

                                                            if (strItemGroupName.Contains(CCommon.ToString(objVarie.Name)) == false)
                                                            {
                                                                strItemGroupName = strItemGroupName + "_" + CCommon.ToString(objVarie.Name);
                                                            }

                                                            if (CustomFieldID > 0 && ListID > 0)
                                                            {
                                                                DataRow drFields = dtCFields.NewRow();
                                                                drFields["fld_id"] = CustomFieldID;
                                                                drFields["Fld_label"] = objVarie.Name.ToString();
                                                                drFields["TabName"] = "";
                                                                drFields["loc_name"] = "";
                                                                drFields["bitIsRequired"] = false;
                                                                drFields["bitIsEmail"] = false;
                                                                drFields["bitIsAlphaNumeric"] = false;
                                                                drFields["bitIsNumeric"] = false;
                                                                drFields["bitIsLengthValidation"] = false;
                                                                drFields["ListID"] = ListID;
                                                                dtCFields.Rows.Add(drFields);
                                                                dtCFields.AcceptChanges();
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            //LogMessage = "Error occurred while adding Item Attribute named : " + objVarie.Name.ToString() + ". Details of error :" + ex.Message.ToString();
                                                            //GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From ProcessOpenProductListingReport : " + LogMessage);
                                                            string ErrMessage = "Error occurred while adding Item Attribute named : " + objVarie.Name.ToString() + ". Details of error :" + ex.Message.ToString();
                                                            throw new Exception("Amazon:- From ProcessOpenProductListingReport : " + ErrMessage);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        ListID = CCommon.ToLong(dtCFields.Select(String.Format("fld_label = '{0}'", objVarie.Name.ToString().ToLower().Replace("'", "''")))[0]["ListID"]);
                                                        if (strItemGroupName.Contains(CCommon.ToString(objVarie.Name)) == false)
                                                        {
                                                            strItemGroupName = strItemGroupName + "_" + CCommon.ToString(objVarie.Name);
                                                        }
                                                    }

                                                    //if (dtCFValues.Select("ListID = " + ListID + " AND vcData = '" + objVarie.Value.ToString().ToLower() + "'").Length == 0)
                                                    if (ListID > 0 && dtCFValues.Select(String.Format("vcData = '{0}' AND ListID = " + ListID, objVarie.Value.ToString().ToLower().Replace("'", "''"))).Length == 0)
                                                    {
                                                        try
                                                        {
                                                            CCommon objCommon = new CCommon();
                                                            objCommon.DomainID = DomainId;
                                                            objCommon.ListItemName = objVarie.Value.ToString();
                                                            objCommon.ListID = ListID;
                                                            objCommon.UserCntID = RecordOwner;
                                                            objCommon.ManageItemList();

                                                            if (ListID > 0)
                                                            {
                                                                DataRow drFieldValues = dtCFValues.NewRow();
                                                                drFieldValues["numListItemID"] = objCommon.ListItemID;
                                                                drFieldValues["vcData"] = objVarie.Value.ToString();
                                                                drFieldValues["constFlag"] = 0;
                                                                drFieldValues["bitDelete"] = 0;
                                                                drFieldValues["intSortOrder"] = false;
                                                                drFieldValues["ListID"] = ListID;
                                                                dtCFValues.Rows.Add(drFieldValues);
                                                                dtCFValues.AcceptChanges();
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            //LogMessage = "Error occurred while adding Item Attribute value : " + objVarie.Value.ToString() + ". Details of error :" + ex.Message.ToString();
                                                            //GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From ProcessOpenProductListingReport : " + LogMessage);

                                                            string ErrMessage = "Error occurred while adding Item Attribute value : " + objVarie.Value.ToString() + ". Details of error :" + ex.Message.ToString();
                                                            throw new Exception("Amazon:- From ProcessOpenProductListingReport : " + ErrMessage);
                                                        }
                                                    }
                                                }

                                                foreach (VariationDetails vd in vCollection.VariationDetail)
                                                {
                                                    string[] strVariations = vd.VariationValue.Split(',');

                                                    DataTable dtCA = new DataTable();
                                                    dtCA.Columns.Add("Fld_ID");
                                                    dtCA.Columns.Add("Fld_Value");
                                                    dtCA.TableName = "CusFlds";
                                                    dtCA.AcceptChanges();

                                                    DataRow drAttr = dtCustomAttributes.NewRow();
                                                    drAttr["AmazonItemID"] = objAmazonProduct.ASIN;
                                                    drAttr["ItemName"] = objAmazonProduct.Title;
                                                    drAttr["WListPrice"] = vd.ListPrice;
                                                    drAttr["OnHand"] = vd.Quantity;
                                                    drAttr["ReOrder"] = vd.ReOrder;
                                                    drAttr["WSKU"] = vd.SKU;
                                                    drAttr["WBarCode"] = "";

                                                    foreach (string strItem in strVariations)
                                                    {
                                                        string strKeyName = strItem.Split('~')[0];
                                                        string strKeyValue = strItem.Split('~')[1];
                                                        DataRow[] drCField = dtCFields.Select(String.Format("fld_label = '{0}'", strKeyName.ToString().Trim().ToLower().Replace("'", "''")));

                                                        if (drCField.Length > 0)
                                                        {
                                                            long lngCFieldID = CCommon.ToLong(drCField[0]["fld_id"]);
                                                            long lngListID = CCommon.ToLong(drCField[0]["ListID"]);
                                                            if (lngCFieldID > 0)
                                                            {
                                                                DataRow[] drCValue = dtCFValues.Select("ListID = " + lngListID + " AND " + String.Format("vcData = '{0}'", strKeyValue.ToString().Trim().ToLower().Replace("'", "''")));
                                                                if (drCValue.Length > 0)
                                                                {
                                                                    DataRow drCA = dtCA.NewRow();
                                                                    drCA["Fld_ID"] = lngCFieldID;
                                                                    drCA["Fld_Value"] = drCValue[0]["numListItemID"];
                                                                    dtCA.Rows.Add(drCA);
                                                                    dtCA.AcceptChanges();
                                                                }
                                                            }
                                                        }
                                                    }

                                                    DataSet dsTemp = new DataSet();
                                                    dsTemp.Tables.Add(dtCA);

                                                    drAttr["strXML"] = dsTemp.GetXml();
                                                    dtCustomAttributes.Rows.Add(drAttr);
                                                    dtCustomAttributes.AcceptChanges();
                                                }
                                            }

                                            //Create/ Update ItemGroup in Biz if not exist for new / old Item attributes
                                            DataTable dtItemGroup = new DataTable();
                                            dtItemGroup.TableName = "Table";
                                            dtItemGroup.Columns.Add("numOppAccAttrID");

                                            DataSet dsItemGroup = new DataSet();

                                            BACRM.BusinessLogic.Item.CItems objItem = new BACRM.BusinessLogic.Item.CItems();
                                            objItem.ItemGroupID = 0;
                                            objItem.DomainID = DomainId;
                                            dsItemGroup = objItem.GetItemGroups();

                                            if (dsItemGroup != null && dsItemGroup.Tables.Count > 0 && dsItemGroup.Tables[0].Rows.Count > 0)
                                            {
                                                //string strItemGroupName = "";
                                                foreach (DataRow drC in dtCFields.Rows)
                                                {
                                                    DataRow drIG = dtItemGroup.NewRow();
                                                    drIG["numOppAccAttrID"] = drC["fld_id"];
                                                    dtItemGroup.Rows.Add(drIG);
                                                    dtItemGroup.AcceptChanges();
                                                }

                                                strItemGroupName = strItemGroupName.Trim('_');
                                                if (dsItemGroup.Tables[0].Select(String.Format("vcItemGroup = '{0}'", strItemGroupName.ToString().Trim().ToLower().Replace("'", "''"))).Length == 0)
                                                {
                                                    DataSet dsTempIG = new DataSet();
                                                    dsTempIG.Tables.Add(dtItemGroup);

                                                    objItem.ItemGroupID = 0;
                                                    objItem.ItemGroupName = strItemGroupName;
                                                    objItem.str = dsTempIG.GetXml();
                                                    objItem.DomainID = DomainId;
                                                    ItemGroupID = objItem.ManageItemGroups();
                                                }
                                                else
                                                {
                                                    ItemGroupID = CCommon.ToLong(dsItemGroup.Tables[0].Select(String.Format("vcItemGroup = '{0}'", strItemGroupName.ToString().Trim().ToLower().Replace("'", "''")))[0]["numItemgroupID"]);
                                                }

                                                strItemGroupName = "";
                                            }
                                        }

                                        dr["numItemCode"] = 0;
                                        dr["vcItemName"] = objAmazonProduct.Title;
                                        dr["txtItemDesc"] = "";
                                        dr["charItemType"] = "P";
                                        dr["monListPrice"] = CCommon.ToDouble(objAmazonProduct.Price);
                                        dr["txtItemHtmlDesc"] = objAmazonProduct.Description;
                                        dr["bitTaxable"] = 0;
                                        dr["vcSKU"] = objAmazonProduct.SellerSKU;
                                        dr["numQuantity"] = CCommon.ToInteger(objAmazonProduct.Quantity);
                                        dr["bitKitParent"] = 0;
                                        dr["numDomainID"] = DomainId;
                                        dr["numUserCntID"] = RecordOwner;
                                        dr["numVendorID"] = 0;
                                        dr["bitSerialized"] = 0;
                                        dr["strFieldList"] = 0;
                                        dr["vcModelID"] = objAmazonProduct.ModelNumber;
                                        dr["numItemGroup"] = ItemGroupID;
                                        dr["numCOGSChartAcntId"] = DefaultCOGSAccID;
                                        dr["numAssetChartAcntId"] = DefaultAssetAccID;
                                        dr["numIncomeChartAcntId"] = DefaultIncomeAccID;
                                        dr["monAverageCost"] = 0;
                                        dr["monTotAmtBefDiscount"] = 0;
                                        dr["monLabourCost"] = 0;
                                        dr["fltWeight"] = objAmazonProduct.Weight;
                                        dr["fltHeight"] = objAmazonProduct.Height;
                                        dr["fltLength"] = objAmazonProduct.Length;
                                        dr["fltWidth"] = objAmazonProduct.Width;
                                        dr["fltShippingWeight"] = objAmazonProduct.ShippingWeight;
                                        dr["fltShippingHeight"] = objAmazonProduct.ShippingHeight;
                                        dr["fltShippingLength"] = objAmazonProduct.ShippingLength;
                                        dr["fltShippingWidth"] = objAmazonProduct.ShippingWidth;
                                        dr["DimensionUOM"] = objAmazonProduct.LengthUOM;
                                        dr["WeightUOM"] = objAmazonProduct.WeightUOM;
                                        dr["intWebApiId"] = WebApiId;
                                        dr["vcApiItemId"] = objAmazonProduct.ASIN;
                                        dr["numBarCodeId"] = 0;
                                        dr["vcManufacturer"] = objAmazonProduct.Manufacturer;
                                        dr["vcExportToAPI"] = WebApiId;
                                        dr["Warranty"] = objAmazonProduct.Warranty;
                                    }
                                }
                                catch (FaultException<error> ex)
                                {
                                    error er = ex.Detail;
                                    string ErrMessage = "Error received from Amazon Service : " + er.msg;
                                    throw new Exception("Amazon:- From GetItemDetailsForSKU : " + ErrMessage);
                                }
                                catch (Exception ex)
                                {
                                    throw ex;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dr;
        }

        #endregion Products Operation

        #region Orders

        /// <summary>
        /// Gets the Order ID List from Amazon and Saves to Biz Database
        /// </summary>
        /// <param name="objSvcConfigInfo">Service Configuration Information</param>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">WebApi Id</param>
        public void GetOrdersIDListToBiz(ServiceConfigInfo objSvcConfigInfo, DateTime CreatedAfter, DateTime CreatedBefore, long DomainId, int WebApiId)
        {
            string LogMessage = "";
            COrdersList OrderList = new COrdersList();
            WebAPI objWebAPI = new WebAPI();
            try
            {
                AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                OrderList = objClient.GetOrdersList(objSvcConfigInfo, CreatedAfter, CreatedBefore);
                objClient.Close();
                UserAccess objUserAccess = new UserAccess();
                string strOrderIds = "";
                if (OrderList.Orders.Length > 0)
                {
                    LogMessage = "Amazon Order Ids ( ";

                    foreach (COrders order in OrderList.Orders)
                    {
                        try
                        {
                            if (order.AmazonOrderId != null && !string.IsNullOrEmpty(order.AmazonOrderId))
                            {
                                strOrderIds += order.AmazonOrderId + ", ";
                                objWebAPI.DomainID = DomainId;
                                objWebAPI.WebApiId = WebApiId;
                                objWebAPI.WebApiOrdDetailId = 0;
                                objWebAPI.API_OrderId = order.AmazonOrderId;
                                objWebAPI.API_OrderStatus = 0;
                                objWebAPI.ManageAPIOrderDetails();

                                //objWebAPI.OppId = 0;
                                //objWebAPI.vcAPIOppId = order.AmazonOrderId;
                                //objWebAPI.UserCntId = CCommon.ToLong(dtUserAccess.Rows[0]["numUserCntId"]); ;
                                //objWebAPI.AddAPIopportunity();
                            }
                        }
                        catch (SqlException Ex)
                        {
                            if (Ex.Number == 2627)
                            {
                                LogMessage = objWebAPI.API_OrderId + " is added already to BizDatabase for DomainID : " + DomainId + " and WebApiID : " + WebApiId;
                                GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From GetOrdersIDListToBiz : " + LogMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogMessage = "Conflict occurred while adding Amazon FBA Order ID " + objWebAPI.API_OrderId + " to BizAutomation for DomainId : " + DomainId + ".Message: " + ex.Message;
                            GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From GetOrdersIDListToBiz : " + LogMessage);
                        }
                    }
                    LogMessage += strOrderIds + ") added in BizAutomation.";
                    GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From GetOrdersIDListToBiz : " + LogMessage);

                    LogMessage = OrderList.Orders.Length + " new Order ID is added to BizDatabase for DomainID : " + DomainId + " and WebApiID : " + WebApiId;
                    GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From GetOrdersIDListToBiz : " + LogMessage);
                }
                //else
                //{
                //    LogMessage = "No new Order Details found for DomainID : " + DomainId + " and WebApiID : " + WebApiId;
                //    GeneralFunctions.WriteMessage(DomainId, "LOG", LogMessage);
                //}
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Amazon Service : " + er.msg;
                throw new Exception("Amazon:- From GetOrdersIDListToBiz : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets FBA Order Details with Order Items from Amazon MWS
        /// </summary>
        /// <param name="objSvcConfigInfo"></param>
        /// <param name="DomainID"></param>
        /// <param name="WebApiId"></param>
        /// <param name="AmazonOrderId"></param>
        public void GetFBAOrderReportByOrderID(ServiceConfigInfo objSvcConfigInfo, long DomainID, int WebApiId, string AmazonOrderId)
        {
            COrdersList OrderList = new COrdersList();
            try
            {
                string LogMessage = "";
                string OrderFilePath = GeneralFunctions.GetPath("AmazonFBAOrderFilePath", DomainID) + AmazonOrderId + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml";
                List<string> lstAmazonOrder = new List<string>();
                lstAmazonOrder.Add(AmazonOrderId);
                AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                OrderList = objClient.GetOrderDetails(objSvcConfigInfo, lstAmazonOrder.ToArray());
                GeneralFunctions.SerializeObjectToFile<COrdersList>(OrderList, OrderFilePath);
                objClient.Close();
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Amazon Service : " + er.msg;
                throw new Exception("Amazon:- From GetFBAOrderReportByOrderID : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void ReadAmazonFBAOrders(COrdersList objOrdersList, long DomainID, int WebApiId, string Source,
            int numWareHouseID, long numRecordOwner, long numAssignTo, long numRelationshipId, long numProfileId,
            long numBizDocId, long BizDocStatusId, int numOrderStatus, int ExpenseAccountId, long DiscountItemMapping, long ShippingServiceItemID, long SalesTaxItemMappingID, string ShipToPhoneNo)
        {
            string LogMessage = "";
            decimal SalesTaxRate = 0;
            CContacts objContacts = null;
            CLeads objLeads = null;
            ImportWizard objImpWzd = new ImportWizard();
            MOpportunity objOpportunity = new MOpportunity();
            CCommon objCommon = new CCommon();
            string ItemMessage = "";
            decimal ShipCost = 0;
            decimal OrderTotal = 0;
            long CurrencyID = 0;
            decimal dTotDiscount = 0;
            decimal SalesTaxAmount = 0;
            WebAPI objWebApi = new WebAPI();

            if (objOrdersList.Orders != null)
            {
                COrders[] objOrders = objOrdersList.Orders;
                foreach (COrders Order in objOrders)
                {
                    if (!string.IsNullOrEmpty(Order.AmazonOrderId))
                    {
                        objWebApi.DomainID = DomainID;
                        objWebApi.WebApiId = WebApiId;
                        objWebApi.vcAPIOppId = Order.AmazonOrderId;
                        if (objWebApi.CheckDuplicateAPIOpportunity())
                        {


                            int a = -1;
                            string AddressStreet = "";//To concatenate Address Lines
                            objContacts = new CContacts();
                            objLeads = new CLeads();
                            objLeads.DomainID = DomainID;
                            if (!string.IsNullOrEmpty(Order.BuyerEmail)) //If contains Email Address
                            {
                                objLeads.Email = Order.BuyerEmail;

                                //Check for Details for the Given DomainID and EmailID
                                objLeads.GetConIDCompIDDivIDFromEmail();
                            }
                            else
                            {
                                objLeads.Email = "";
                            }
                            if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                            {
                                //If Email already registered
                                lngCntID = objLeads.ContactID;
                                lngDivId = objLeads.DivisionID;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(Order.BuyerName))
                                {
                                    objLeads.CompanyName = Order.BuyerName;
                                    objLeads.CustName = Order.BuyerName;
                                }
                                else if (Order.ShippingAddress != null)
                                {
                                    CAddress shippingAddress = Order.ShippingAddress;

                                    if (!string.IsNullOrEmpty(shippingAddress.Name))
                                    {
                                        objLeads.CompanyName = shippingAddress.Name;
                                        objLeads.CustName = shippingAddress.Name;
                                    }
                                }
                                if (Order.ShippingAddress != null)
                                {
                                    CAddress shippingAddress = Order.ShippingAddress;

                                    if (!string.IsNullOrEmpty(shippingAddress.Name))
                                    {
                                        objLeads.CompanyName = shippingAddress.Name;
                                        objLeads.CustName = shippingAddress.Name;
                                    }
                                    if (!string.IsNullOrEmpty(shippingAddress.County))
                                    {
                                        //objLeads.Country = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                        //objLeads.SCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                    }
                                    if (!string.IsNullOrEmpty(shippingAddress.CountryCode))
                                    {
                                        objCommon.DomainID = DomainID;
                                        objCommon.Mode = 22;
                                        objCommon.Str = shippingAddress.CountryCode;
                                        objLeads.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                        objLeads.SCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    }
                                    if (!string.IsNullOrEmpty(shippingAddress.Name))
                                    {
                                        a = shippingAddress.Name.IndexOf('.');
                                        if (a == -1)
                                            a = shippingAddress.Name.IndexOf(' ');
                                        if (a == -1)
                                        {
                                            objLeads.FirstName = shippingAddress.Name;
                                            objLeads.LastName = shippingAddress.Name;
                                        }
                                        else
                                        {
                                            objLeads.FirstName = shippingAddress.Name.Substring(0, a);
                                            objLeads.LastName = shippingAddress.Name.Substring(a + 1, shippingAddress.Name.Length - (a + 1));
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(shippingAddress.Phone))
                                    {
                                        objLeads.ContactPhone = shippingAddress.Phone;
                                        objLeads.PhoneExt = "";
                                    }
                                    else
                                    {
                                        objLeads.ContactPhone = ShipToPhoneNo;
                                        objLeads.PhoneExt = "";
                                    }
                                }
                                objLeads.UserCntID = numRecordOwner;
                                objLeads.ContactType = 70;
                                objLeads.PrimaryContact = true;
                                objLeads.UpdateDefaultTax = false;
                                objLeads.NoTax = false;
                                objLeads.CRMType = 1;
                                objLeads.DivisionName = "-";

                                objLeads.CompanyType = numRelationshipId;
                                objLeads.Profile = numProfileId;

                                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();//Creates Company Record
                                LogMessage = "New Company details added CompanyID : " + objLeads.CompanyID + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ReadAmazonFBAOrders : " + LogMessage);

                                lngDivId = objLeads.CreateRecordDivisionsInfo();
                                LogMessage = "New Divisions Informations added. Division Id : " + lngDivId + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ReadAmazonFBAOrders : " + LogMessage);

                                objLeads.ContactID = 0;
                                objLeads.DivisionID = lngDivId;
                                lngCntID = objLeads.CreateRecordAddContactInfo();//Creates Contact Info
                                LogMessage = "New Contact Informations added. Contact Id : " + lngCntID + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ReadAmazonFBAOrders : " + LogMessage);


                                if (Order.ShippingAddress != null)
                                {
                                    CAddress shippingAddress = Order.ShippingAddress;

                                    objContacts.FirstName = objLeads.FirstName;
                                    objContacts.LastName = objLeads.LastName;
                                    objContacts.ContactPhone = objLeads.ContactPhone;
                                    objContacts.Email = objLeads.Email;

                                    if (!string.IsNullOrEmpty(shippingAddress.AddressLine1))
                                    {
                                        AddressStreet = shippingAddress.AddressLine1;
                                    }
                                    if (!string.IsNullOrEmpty(shippingAddress.AddressLine2))
                                    {
                                        AddressStreet = AddressStreet + " " + shippingAddress.AddressLine2;
                                    }
                                    if (!string.IsNullOrEmpty(shippingAddress.AddressLine3))
                                    {
                                        AddressStreet = AddressStreet + " " + shippingAddress.AddressLine3;
                                    }

                                    objContacts.BillStreet = AddressStreet;
                                    objContacts.ShipStreet = AddressStreet;

                                    if (!string.IsNullOrEmpty(shippingAddress.City))
                                    {
                                        objContacts.BillCity = shippingAddress.City;
                                        objContacts.ShipCity = shippingAddress.City;
                                    }
                                    if (!string.IsNullOrEmpty(shippingAddress.StateOrRegion))
                                    {
                                        objCommon.DomainID = DomainID;
                                        objCommon.Mode = 21;
                                        objCommon.Str = shippingAddress.StateOrRegion;
                                        objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                        objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    }

                                    if (!string.IsNullOrEmpty(shippingAddress.CountryCode))
                                    {
                                        objCommon.DomainID = DomainID;
                                        objCommon.Mode = 22;
                                        objCommon.Str = shippingAddress.CountryCode;
                                        objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                        objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    }
                                    if (!string.IsNullOrEmpty(shippingAddress.PostalCode))
                                    {
                                        objContacts.BillPostal = shippingAddress.PostalCode;
                                        objContacts.ShipPostal = shippingAddress.PostalCode;
                                    }

                                    objContacts.BillingAddress = 0;
                                    objContacts.DivisionID = lngDivId;
                                    objContacts.ContactID = lngCntID;
                                    objContacts.RecordID = lngDivId;
                                    objContacts.DomainID = DomainID;
                                    objContacts.IsPrimaryAddress = CCommon.ToBool(1);
                                    objContacts.UpdateCompanyAddress();
                                }
                            }
                            AMWS_Communicator.CItems[] objAmazonOrderItems = Order.ItemsList;

                            objOpportunity.OppRefOrderNo = Order.AmazonOrderId;
                            objOpportunity.MarketplaceOrderID = Order.AmazonOrderId;
                            BizCommonFunctions.CreateItemTable(dtFBAOrderItems);
                            ItemMessage = AddFBAOrderItemDataset(DomainID, WebApiId, numRecordOwner, numWareHouseID, DiscountItemMapping, objAmazonOrderItems, "", ShippingServiceItemID, SalesTaxItemMappingID, out CurrencyID, out OrderTotal, out ShipCost, out dTotDiscount, out SalesTaxRate, out SalesTaxAmount);

                            objOpportunity.CurrencyID = CurrencyID;
                            objOpportunity.Amount = OrderTotal;
                            //objContacts.BillingAddress = 0;
                            //objContacts.DivisionID = lngDivId;
                            //objContacts.UpdateCompanyAddress();//Updates Company Details
                            // SaveTaxTypes();
                            BizCommonFunctions.SaveTaxTypes(ds, lngDivId);
                            arrBillingIDs = new string[4];
                            arrBillingIDs[0] = CCommon.ToString(objLeads.CompanyID);
                            arrBillingIDs[1] = CCommon.ToString(lngDivId);
                            arrBillingIDs[2] = CCommon.ToString(lngCntID);
                            arrBillingIDs[3] = objLeads.CompanyName;
                            if (lngCntID == 0)
                                return;
                            // }
                            objOpportunity.OpportunityId = 0;
                            DateTime OrderDate = Convert.ToDateTime(Order.PurchaseDate);
                            objOpportunity.MarketplaceOrderDate = OrderDate;
                            objOpportunity.MarketplaceOrderReportId = Order.AmazonOrderId;
                            objOpportunity.ContactID = lngCntID;
                            objOpportunity.DivisionID = lngDivId;
                            objOpportunity.UserCntID = numRecordOwner;
                            objOpportunity.AssignedTo = numAssignTo;

                            objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
                            objOpportunity.EstimatedCloseDate = DateTime.Now;
                            objOpportunity.PublicFlag = 0;
                            objOpportunity.DomainID = DomainID;
                            objOpportunity.OppType = 1;
                            objOpportunity.OrderStatus = numOrderStatus;

                            dsFBAOrderItems.Tables.Clear();
                            dsFBAOrderItems.Tables.Add(dtFBAOrderItems.Copy());
                            if (dsFBAOrderItems.Tables[0].Rows.Count != 0)
                                objOpportunity.strItems = ((dtFBAOrderItems.Rows.Count > 0) ? ("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsFBAOrderItems.GetXml()) : "");
                            else
                                objOpportunity.strItems = "";
                            objOpportunity.OppComments = ItemMessage;
                            objOpportunity.Source = WebApiId;
                            objOpportunity.SourceType = 3;
                            objOpportunity.TaxOperator = 2; //Remove by default setting of adding sales tax while it comes through marketplace itself
                            objOpportunity.DealStatus = (long)MOpportunity.EnmDealStatus.DealWon;
                            arrOutPut = objOpportunity.Save();
                            lngOppId = CCommon.ToLong(arrOutPut[0]);

                            //Save Source 
                            objOpportunity.vcSource = Source;
                            objOpportunity.WebApiId = WebApiId;
                            objOpportunity.OpportunityId = lngOppId;
                            objOpportunity.ManageOppLinking();

                            LogMessage = "Order Details updated for API OrderID : " + Order.AmazonOrderId + " Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ReadAmazonFBAOrders : " + LogMessage);

                            //if (SalesTaxRate > 0)
                            //{
                            //    CCommon objCommon1 = new CCommon();
                            //    objCommon1.Mode = 35;
                            //    objCommon1.UpdateRecordID = lngOppId;
                            //    objCommon1.Comments = CCommon.ToString(Math.Round(SalesTaxRate, 4));
                            //    objCommon1.DomainID = DomainID;
                            //    objCommon1.UpdateSingleFieldValue();
                            //    LogMessage = "Tax Percentage updated to " + CCommon.ToString(Math.Round(SalesTaxRate, 4)) + " for API OrderID : " + Order.AmazonOrderId + " Biz Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                            //    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                            //}

                            objWebApi.WebApiId = WebApiId;
                            objWebApi.DomainID = DomainID;
                            objWebApi.UserCntID = numRecordOwner;
                            objWebApi.OppId = lngOppId;
                            objWebApi.vcAPIOppId = Order.AmazonOrderId;
                            objWebApi.AddAPIopportunity();

                            objWebApi.API_OrderId = Order.AmazonOrderId;
                            objWebApi.API_OrderStatus = 1;
                            objWebApi.WebApiOrdDetailId = 1;
                            objWebApi.ManageAPIOrderDetails();
                            decimal OrderAmount = OrderTotal;// +ShipCost - dTotDiscount + SalesTaxAmount;

                            //string Reference = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM"); 
                            string Reference = arrOutPut[1];
                            if (ItemMessage == "")
                            {
                                bool IsAuthoritative = false;
                                BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                BizCommonFunctions.CreateOppBizDoc(objOppBizDocs, numRecordOwner, DomainID, lngOppId, objOpportunity.OppRefOrderNo, numBizDocId, BizDocStatusId, lngDivId, OrderDate, ShipCost, dTotDiscount, out OppBizDocID, out IsAuthoritative, ExpenseAccountId, SalesTaxRate, OrderAmount, Reference);
                                UpdateFBAOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, OppBizDocID, objOpportunity.OppRefOrderNo, Order.ItemsList);
                            }
                            else
                            {
                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ReadAmazonFBAOrders : " + ItemMessage);
                            }
                        }
                        else
                        {
                            string ErrMessage = "Order details for Amazon Order Id : " + Order.AmazonOrderId + " is already exists in BizAutomation";
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ReadAmazonFBAOrders : " + ErrMessage);
                        }
                    }
                }
            }
        }

        public string AddFBAOrderItemDataset(long DomainId, int WebApiId, long RecordOwner, int WareHouseId, long DiscountItemMapping, AMWS_Communicator.CItems[] objAmazonOrderItems,
            string ShippingServiceSelected, long ShippingServiceItemID, long SalesTaxItemMappingID, out long CurrencyID, out decimal OrderTotal,
            out decimal ShipCost, out decimal dTotDiscount, out decimal SalesTaxRate, out decimal SalesTaxAmount)
        {
            dtFBAOrderItems.Clear();
            dsFBAOrderItems.Clear();
            CCommon objCommon = new CCommon();
            string Message = "";
            DataRow drItem;
            int ItemCount = 0;
            string ItemType = "";
            string[] itemCodeType;
            string itemCode = "";
            WebAPI objWebApi = new WebAPI();
            objWebApi.DomainID = DomainId;
            objWebApi.WebApiId = WebApiId;
            dTotDiscount = 0;
            decimal decDiscount = 0;
            string DiscountDesc = "";
            decimal Tax0 = 0;
            OrderTotal = 0;
            ShipCost = 0;
            CurrencyID = 0;
            SalesTaxRate = 0;
            SalesTaxAmount = 0;
            DataTable dtDiscountItems = new DataTable();
            dtDiscountItems.Columns.Clear();
            dtDiscountItems.Columns.Add("DiscountName");
            dtDiscountItems.Columns.Add("DiscountAmount");
            dtDiscountItems.Columns.Add("TaxAmount");
            DataRow drDiscountItem;
            decimal SaleAmount1 = 0;

            foreach (AMWS_Communicator.CItems orderItem in objAmazonOrderItems)
            {
                ItemCount += 1;
                try
                {
                    if (!string.IsNullOrEmpty(orderItem.SellerSKU))
                    {
                        //objCommon.DomainID = DomainId;
                        //objCommon.Mode = 20;
                        //objCommon.Str = orderItem.SellerSKU;
                        //itemCode = CCommon.ToString(objCommon.GetSingleFieldValue());

                        objCommon.DomainID = DomainId;
                        objCommon.Mode = 38;
                        objCommon.Str = orderItem.SellerSKU;
                        itemCodeType = CCommon.ToString(objCommon.GetSingleFieldValue()).Split('~');
                        if (itemCodeType.Length > 1)
                        {
                            if (!string.IsNullOrEmpty(itemCodeType[0]))
                            {
                                itemCode = itemCodeType[0];
                            }
                            if (!string.IsNullOrEmpty(itemCodeType[1]))
                            {
                                ItemType = BizCommonFunctions.GetItemTypeNameByCharType(itemCodeType[1]);
                            }
                        }
                        if (itemCode == "")
                        {
                            DataTable dtProductDetails = new DataTable();
                            BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                            DataRow dr = dtProductDetails.NewRow();
                            DataTable dtCopy = new DataTable();
                            dr = GetItemDetailsForSKU(DomainId, WebApiId, RecordOwner, orderItem.SellerSKU, dr, ref dtCopy);
                            dr["numQuantity"] = 1;
                            string ItemClassificationName = "Amazon_OrderItem_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                            long ItemClassificationID = 0;
                            ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);
                            itemCode = CCommon.ToString(BizCommonFunctions.AddNonInventoryItemToBiz(dr, DomainId, WebApiId, RecordOwner, WareHouseId, ItemClassificationID));
                            ItemType = BizCommonFunctions.GetItemTypeNameByCharType("N");
                            WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                            string FilePath = ""; // CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";

                            BACRM.BusinessLogic.Item.CItems objItems = new BACRM.BusinessLogic.Item.CItems();
                            objItems.ItemCode = CCommon.ToInteger(itemCode);
                            TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);
                            objItems.ClientZoneOffsetTime = CCommon.ToInteger(diff1.TotalMinutes);
                            DataTable dtItemDetails = objItems.ItemDetails();
                            if (CCommon.ToLong(dtItemDetails.Rows[0]["numItemGroup"]) > 0 && CCommon.ToString(orderItem.SellerSKU).Length > 0)
                            {
                                FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + "_" + CCommon.ToString(orderItem.SellerSKU) + ".xml";
                            }
                            else
                            {
                                FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";
                            }

                            if (System.IO.File.Exists(FilePath))
                            {
                                System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                                XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                objStreamReader.Close();
                            }
                            objWebAPIItemDetail.DimensionUOM = BizCommonFunctions.GetLengthUOMCode(CCommon.ToString(dr["DimensionUOM"]));
                            objWebAPIItemDetail.WeightUOM = BizCommonFunctions.GetWeightUOMCode(CCommon.ToString(dr["WeightUOM"]));
                            objWebAPIItemDetail.Length = CCommon.ToString(dr["fltLength"]);
                            objWebAPIItemDetail.Height = CCommon.ToString(dr["fltHeight"]);
                            objWebAPIItemDetail.Width = CCommon.ToString(dr["fltWidth"]);
                            objWebAPIItemDetail.Weight = CCommon.ToString(dr["fltWeight"]);
                            objWebAPIItemDetail.WarrantyDescription = CCommon.ToString(dr["Warranty"]);

                            StreamWriter objStreamWriter; //= new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");
                            if (CCommon.ToLong(dtItemDetails.Rows[0]["numItemGroup"]) > 0 && CCommon.ToString(orderItem.SellerSKU).Length > 0)
                            {
                                objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + "_" + CCommon.ToString(orderItem.SellerSKU) + ".xml");
                            }
                            else
                            {
                                objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");
                            }

                            XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                            Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                            objStreamWriter.Close();

                        }
                        if (itemCode != "")
                        {
                            decDiscount = 0;
                            DiscountDesc = "";
                            drItem = dtFBAOrderItems.NewRow();
                            drItem["numoppitemtCode"] = ItemCount;
                            drItem["numItemCode"] = itemCode;
                            if (CCommon.ToInteger(orderItem.QuantityOrdered) != 0)
                            {
                                drItem["numUnitHour"] = orderItem.QuantityOrdered;
                                if (orderItem.ItemPrice != null)
                                {
                                    CMoney ItemPrice = new CMoney();
                                    ItemPrice = orderItem.ItemPrice;
                                    drItem["monPrice"] = CCommon.ToDecimal(ItemPrice.Amount) / CCommon.ToInteger(orderItem.QuantityOrdered);
                                    //SaleAmount1 = CCommon.ToDecimal(Component.Amount.Value) / CCommon.ToInteger(orderItem.Quantity); ;
                                    OrderTotal += CCommon.ToDecimal(ItemPrice.Amount);
                                    objCommon.DomainID = DomainId;
                                    objCommon.Mode = 15;
                                    objCommon.Str = ItemPrice.CurrencyCode;
                                    CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                                    //drItem["monTotAmount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                    //drItem["monTotAmtBefDiscount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                    drItem["monTotAmount"] = CCommon.ToDecimal(ItemPrice.Amount);

                                }
                                if (orderItem.ShippingPrice != null)
                                {
                                    if (CCommon.ToDecimal(orderItem.ShippingPrice.Amount) > 0)
                                    {
                                        ShipCost += CCommon.ToDecimal(orderItem.ShippingPrice.Amount);
                                    }
                                }
                                if (orderItem.ShippingDiscount != null)
                                {
                                    if (CCommon.ToDecimal(orderItem.ShippingDiscount.Amount) != 0)
                                    {
                                        decDiscount += decimal.Negate(CCommon.ToDecimal(orderItem.ShippingDiscount.Amount));
                                        DiscountDesc += " Shipping Discount : " + CCommon.ToString(decimal.Negate(CCommon.ToDecimal(orderItem.ShippingDiscount.Amount))) + Environment.NewLine;

                                        drDiscountItem = dtDiscountItems.NewRow();
                                        drDiscountItem["DiscountName"] = "Shipping Discount : for Item-SKU - " + orderItem.SellerSKU;
                                        drDiscountItem["DiscountAmount"] = CCommon.ToString(decimal.Negate(CCommon.ToDecimal(orderItem.ShippingDiscount.Amount)));
                                        drDiscountItem["TaxAmount"] = 0;
                                        dtDiscountItems.Rows.Add(drDiscountItem);

                                    }
                                }
                                if (orderItem.PromotionDiscount != null)
                                {
                                    CMoney PromotionalDiscount = new CMoney();
                                    PromotionalDiscount = orderItem.PromotionDiscount;

                                    if (CCommon.ToDecimal(PromotionalDiscount.Amount) != 0)
                                    {

                                        DiscountDesc += " Promotion Discount : " + CCommon.ToString(decimal.Negate(CCommon.ToDecimal(PromotionalDiscount.Amount))) + Environment.NewLine;

                                        drDiscountItem = dtDiscountItems.NewRow();
                                        drDiscountItem["DiscountName"] = "Promotion discount for Item-SKU - " + orderItem.SellerSKU;
                                        drDiscountItem["DiscountAmount"] = CCommon.ToString(decimal.Negate(CCommon.ToDecimal(PromotionalDiscount.Amount)));
                                        drDiscountItem["TaxAmount"] = 0;
                                        dtDiscountItems.Rows.Add(drDiscountItem);
                                        decDiscount += decimal.Negate(CCommon.ToDecimal(PromotionalDiscount.Amount));
                                        DiscountDesc += "Promotion discount for Item-SKU - " + orderItem.SellerSKU + " : " + CCommon.ToString(decimal.Negate(CCommon.ToDecimal(PromotionalDiscount.Amount))) + "; ";

                                    }
                                }
                                if (orderItem.ItemTax != null)
                                {
                                    if (CCommon.ToDecimal(orderItem.ItemTax.Amount) > 0)
                                    {
                                        //Tax0 += CCommon.ToDecimal(orderItem.ItemTax.Amount);
                                        SalesTaxAmount += CCommon.ToDecimal(orderItem.ItemTax.Amount);
                                    }
                                }

                                drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]);
                            }
                            drItem["numUOM"] = 0; // get it from item table, in procedure
                            drItem["vcUOMName"] = "";// get it from item table, in procedure
                            drItem["UOMConversionFactor"] = 1.0000;
                            drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                            drItem["vcModelID"] = "";// get it from item table, in procedure
                            drItem["numWarehouseID"] = WareHouseId;
                            if (orderItem.Title != null && orderItem.Title != "")
                            {
                                drItem["vcItemName"] = orderItem.Title;
                            }
                            drItem["Warehouse"] = "";
                            drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                            drItem["ItemType"] = ItemType;// get it from item table, in procedure
                            drItem["Attributes"] = "";
                            drItem["Op_Flag"] = 1;
                            drItem["bitWorkOrder"] = false;
                            drItem["DropShip"] = true;
                            //drItem["fltDiscount"] = decimal.Negate(decDiscount);
                            drItem["fltDiscount"] = 0;
                            dTotDiscount += decimal.Negate(decDiscount);
                            //if (fltDiscount > 0)
                            drItem["bitDiscountType"] = 1; //Flat discount
                            //else
                            //    drItem["bitDiscountType"] = false; // Percentage

                            //if (orderItem.ItemTax != null)
                            //{
                            //    CMoney ItemTax = orderItem.ItemTax;
                            //    Tax0 = CCommon.ToDecimal(ItemTax.Amount);
                            //}

                            drItem["Tax0"] = Tax0;
                            if (Tax0 > 0)
                                drItem["bitTaxable0"] = true;
                            else
                                drItem["bitTaxable0"] = false;

                            //drItem["Tax0"] = 0;
                            drItem["numVendorWareHouse"] = 0;
                            drItem["numShipmentMethod"] = 0;
                            drItem["numSOVendorId"] = 0;
                            drItem["numProjectID"] = 0;
                            drItem["numProjectStageID"] = 0;
                            drItem["charItemType"] = ""; // should be taken from procedure
                            drItem["numToWarehouseItemID"] = 0;
                            //drItem["Tax41"] = 0;
                            //drItem["bitTaxable41"] = false;
                            //drItem["Tax42"] = 0;
                            //drItem["bitTaxable42"] = false;
                            drItem["Weight"] = ""; // should take from procedure 
                            drItem["WebApiId"] = WebApiId;
                            drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                            //drItem["vcSKU"] = orderItem.SKU;
                            dtFBAOrderItems.Rows.Add(drItem);
                        }
                        else
                        {
                            Message += "Item " + orderItem.Title + " (SKU: " + orderItem.SellerSKU + ") not found in Amazon-Biz linking database or unable to get Item detail inorder Add in BizDatabase" + Environment.NewLine;
                        }
                    }

                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From AddFBAOrderItemDataset : " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From AddFBAOrderItemDataset : " + ex.StackTrace);
                    throw ex;
                }
            }
            if (ShipCost > 0)
            {
                OrderTotal += ShipCost;
                drItem = dtFBAOrderItems.NewRow();
                drItem = BizCommonFunctions.GetShippingCostItem(drItem, ShipCost, ShippingServiceItemID, DomainId, WebApiId);
                dtFBAOrderItems.Rows.Add(drItem);
            }
            if (SalesTaxAmount > 0)
            {
                OrderTotal += SalesTaxAmount;
                drItem = dtFBAOrderItems.NewRow();
                drItem = BizCommonFunctions.GetSalesTaxItem(drItem, SalesTaxAmount, SalesTaxItemMappingID, DomainId, WebApiId);
                dtFBAOrderItems.Rows.Add(drItem);
            }
            if (dTotDiscount > 0)
            {
                OrderTotal -= dTotDiscount;
            }
            if (dtDiscountItems.Rows.Count > 0)
            {
                foreach (DataRow drDiscount in dtDiscountItems.Rows)
                {
                    drItem = dtFBAOrderItems.NewRow();
                    string DiscountItemName = CCommon.ToString(drDiscount["DiscountName"]);
                    decimal dDiscountItemValue = CCommon.ToDecimal(drDiscount["DiscountAmount"]);
                    decimal dTaxAmount = CCommon.ToDecimal(drDiscount["TaxAmount"]);
                    drItem = BizCommonFunctions.GetDiscountOrderItem(drItem, dDiscountItemValue, DiscountItemMapping, DomainId, WebApiId, DiscountItemName, dTaxAmount);
                    dtFBAOrderItems.Rows.Add(drItem);
                }
            }
            return Message;
        }

        public void NewReadAmazonOrders(OrderReport objOrdReport, long DomainID, int WebApiId, string Source,
          int numWareHouseID, long numRecordOwner, long numAssignTo, long numRelationshipId, long numProfileId, long numBizDocId, long BizDocStatusId, int numOrderStatus, int ExpenseAccountId, long DiscountItemMapping, long ShippingServiceItemID, long SalesTaxItemMappingID, string MarketplaceOrderReportId, string ShipToPhoneNo)
        {
            string LogMessage = "";
            decimal SalesTaxRate = 0;
            CContacts objContacts = null;
            CLeads objLeads = null;
            ImportWizard objImpWzd = new ImportWizard();

            CCommon objCommon = new CCommon();

            //foreach (AmazonEnvelopeMessage Message in objAmazonEnvelope.Message)
            //{
            MOpportunity objOpportunity = new MOpportunity();
            OrderReportBillingData billData = new OrderReportBillingData();
            OrderReportFulfillmentData OrderFulFillData = new OrderReportFulfillmentData();
            OrderReportCustomerOrderInfo[] CustomerOrderInfo;
            OrderReportItem[] OrderItem = null;
            // OrderReport objOrdReport = new OrderReport();
            WebAPI objWebApi = new WebAPI();

            string ItemMessage = "";
            decimal ShipCost = 0;
            decimal OrderTotal = 0;
            long CurrencyID = 0;
            decimal dTotDiscount = 0;
            decimal SalesTaxAmount = 0;
            try
            {
                // objOrdReport = (OrderReport)Message.Item;

                if (objOrdReport.BillingData != null)
                    billData = objOrdReport.BillingData;

                if (objOrdReport.FulfillmentData != null)
                    OrderFulFillData = objOrdReport.FulfillmentData;

                if (objOrdReport.CustomerOrderInfo != null)
                    CustomerOrderInfo = objOrdReport.CustomerOrderInfo;

                if (objOrdReport.Item != null)
                    OrderItem = objOrdReport.Item;

                if (!string.IsNullOrEmpty(objOrdReport.AmazonOrderID))
                {
                    objWebApi.DomainID = DomainID;
                    objWebApi.WebApiId = WebApiId;
                    objWebApi.vcAPIOppId = objOrdReport.AmazonOrderID;
                    if (objWebApi.CheckDuplicateAPIOpportunity())
                    {


                        int a = -1;
                        string AddressStreet = "";//To concatenate Address Lines
                        objContacts = new CContacts();
                        objLeads = new CLeads();
                        objLeads.DomainID = DomainID;

                        if (billData.BuyerEmailAddress != null && billData.BuyerEmailAddress != "")//If contains Email Address
                        {
                            objLeads.Email = billData.BuyerEmailAddress;
                            objLeads.GetConIDCompIDDivIDFromEmail(); //Check for Details for the Given DomainID and EmailID
                        }
                        else
                        {
                            objLeads.Email = "";
                        }
                        if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                        {
                            //If Email already registered
                            lngCntID = objLeads.ContactID;
                            lngDivId = objLeads.DivisionID;
                        }
                        else
                        {
                            if (billData.BuyerName != null && billData.BuyerName != "")
                            {
                                objLeads.CompanyName = billData.BuyerName;
                                objLeads.CustName = billData.BuyerName;
                            }
                            else if (OrderFulFillData.Address != null)
                            {
                                AddressType shippingAddress = OrderFulFillData.Address;

                                if (shippingAddress.Name != null && shippingAddress.Name != "")
                                {
                                    objLeads.CompanyName = shippingAddress.Name;
                                    objLeads.CustName = shippingAddress.Name;
                                }
                            }

                            if (OrderFulFillData.Address != null)
                            {
                                AddressType shippingAddress = OrderFulFillData.Address;
                                if (shippingAddress.County != null && shippingAddress.County != "")
                                {
                                    //objLeads.Country = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                    //objLeads.SCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                }
                                if (shippingAddress.CountryCode != null && shippingAddress.CountryCode != "")
                                {
                                    objCommon.DomainID = DomainID;
                                    objCommon.Mode = 22;
                                    objCommon.Str = shippingAddress.CountryCode;
                                    objLeads.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    objLeads.SCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                }

                                if (shippingAddress.Name != null && shippingAddress.Name != "")
                                {
                                    a = shippingAddress.Name.IndexOf('.');
                                    if (a == -1)
                                        a = shippingAddress.Name.IndexOf(' ');
                                    if (a == -1)
                                    {
                                        objLeads.FirstName = shippingAddress.Name;
                                        objLeads.LastName = shippingAddress.Name;
                                    }
                                    else
                                    {
                                        objLeads.FirstName = shippingAddress.Name.Substring(0, a);
                                        objLeads.LastName = shippingAddress.Name.Substring(a + 1, shippingAddress.Name.Length - (a + 1));
                                    }
                                }
                                if (shippingAddress.PhoneNumber != null)
                                {
                                    PhoneNumberType[] objPhoneType;
                                    objPhoneType = shippingAddress.PhoneNumber;
                                    //objLeads.ContactPhone = CCommon.ToString(shippingAddress.PhoneNumber);
                                    if (shippingAddress.PhoneNumber.Length > 0)
                                    {
                                        objLeads.ContactPhone = CCommon.ToString(shippingAddress.PhoneNumber[0].Value);
                                    }
                                    else
                                    {
                                        objLeads.ContactPhone = ShipToPhoneNo;
                                    }
                                    objLeads.PhoneExt = "";
                                }
                                else
                                {
                                    objLeads.ContactPhone = ShipToPhoneNo;
                                    objLeads.PhoneExt = "";
                                }
                            }

                            objLeads.UserCntID = numRecordOwner;
                            objLeads.ContactType = 70;
                            objLeads.PrimaryContact = true;
                            objLeads.UpdateDefaultTax = false;
                            objLeads.NoTax = false;
                            objLeads.CRMType = 1;
                            objLeads.DivisionName = "-";

                            objLeads.CompanyType = numRelationshipId;
                            objLeads.Profile = numProfileId;

                            objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();//Creates Company Record
                            LogMessage = "New Company details added CompanyID : " + objLeads.CompanyID + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From NewReadAmazonOrders : " + LogMessage);

                            lngDivId = objLeads.CreateRecordDivisionsInfo();
                            LogMessage = "New Divisions Informations added. Division Id : " + lngDivId + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From NewReadAmazonOrders : " + LogMessage);

                            objLeads.ContactID = 0;
                            objLeads.DivisionID = lngDivId;
                            lngCntID = objLeads.CreateRecordAddContactInfo();//Creates Contact Info
                            LogMessage = "New Contact Informations added. Contact Id : " + lngCntID + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From NewReadAmazonOrders : " + LogMessage);

                            if (OrderFulFillData.Address != null)
                            {
                                objContacts.FirstName = objLeads.FirstName;
                                objContacts.LastName = objLeads.LastName;
                                objContacts.ContactPhone = objLeads.ContactPhone;
                                objContacts.Email = objLeads.Email;

                                AddressType shippingAddress = OrderFulFillData.Address;

                                if (shippingAddress.AddressFieldOne != null && shippingAddress.AddressFieldOne != "")
                                    AddressStreet = shippingAddress.AddressFieldOne;

                                if (shippingAddress.AddressFieldTwo != null && shippingAddress.AddressFieldTwo != "")
                                    AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldTwo;

                                if (shippingAddress.AddressFieldThree != null && shippingAddress.AddressFieldThree != "")
                                    AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldThree;

                                if (shippingAddress.City != null && shippingAddress.City != "")
                                    AddressStreet = AddressStreet + " " + shippingAddress.City;

                                objContacts.BillStreet = AddressStreet;
                                objContacts.ShipStreet = AddressStreet;

                                if (shippingAddress.City != null && shippingAddress.City != "")
                                {
                                    objContacts.BillCity = shippingAddress.City;
                                    objContacts.ShipCity = shippingAddress.City;
                                }

                                if (shippingAddress.StateOrRegion != null && shippingAddress.StateOrRegion != "")
                                {
                                    objCommon.DomainID = DomainID;
                                    objCommon.Mode = 21;
                                    objCommon.Str = shippingAddress.StateOrRegion;
                                    objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                }

                                if (shippingAddress.CountryCode != null && shippingAddress.CountryCode != "")
                                {
                                    objCommon.DomainID = DomainID;
                                    objCommon.Mode = 22;
                                    objCommon.Str = shippingAddress.CountryCode;
                                    objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                }

                                if (shippingAddress.PostalCode != null && shippingAddress.PostalCode != "")
                                {
                                    objContacts.BillPostal = shippingAddress.PostalCode;
                                    objContacts.ShipPostal = shippingAddress.PostalCode;
                                }

                                objContacts.BillingAddress = 0;
                                objContacts.DivisionID = lngDivId;
                                objContacts.ContactID = lngCntID;
                                objContacts.RecordID = lngDivId;
                                objContacts.DomainID = DomainID;
                                objContacts.IsPrimaryAddress = CCommon.ToBool(1);
                                objContacts.UpdateCompanyAddress();
                            }
                        }

                        objOpportunity.OppRefOrderNo = objOrdReport.AmazonOrderID;
                        objOpportunity.MarketplaceOrderID = objOrdReport.AmazonOrderID;
                        objOpportunity.MarketplaceOrderReportId = MarketplaceOrderReportId;
                        BizCommonFunctions.CreateItemTable(dtItems);
                        ItemMessage = AddOrderItemDataset(DomainID, WebApiId, numRecordOwner, numWareHouseID, DiscountItemMapping, OrderItem, CCommon.ToString(objOrdReport.FulfillmentData.FulfillmentServiceLevel), ShippingServiceItemID, SalesTaxItemMappingID, out CurrencyID, out OrderTotal, out ShipCost, out dTotDiscount, out SalesTaxRate, out SalesTaxAmount);

                        objOpportunity.CurrencyID = CurrencyID;
                        objOpportunity.Amount = OrderTotal;
                        objContacts.BillingAddress = 0;
                        objContacts.DivisionID = lngDivId;
                        //objContacts.UpdateCompanyAddress();//Updates Company Details
                        // SaveTaxTypes();
                        BizCommonFunctions.SaveTaxTypes(ds, lngDivId);
                        arrBillingIDs = new string[4];
                        arrBillingIDs[0] = CCommon.ToString(objLeads.CompanyID);
                        arrBillingIDs[1] = CCommon.ToString(lngDivId);
                        arrBillingIDs[2] = CCommon.ToString(lngCntID);
                        arrBillingIDs[3] = objLeads.CompanyName;
                        if (lngCntID == 0)
                            return;

                        //Create Opportunity Details
                        DateTime OrderDate = objOrdReport.OrderDate;
                        objOpportunity.MarketplaceOrderDate = OrderDate;
                        objOpportunity.OpportunityId = 0;
                        objOpportunity.ContactID = lngCntID;
                        objOpportunity.DivisionID = lngDivId;
                        objOpportunity.UserCntID = numRecordOwner;
                        objOpportunity.AssignedTo = numAssignTo;

                        objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
                        objOpportunity.EstimatedCloseDate = DateTime.Now;
                        objOpportunity.PublicFlag = 0;
                        objOpportunity.DomainID = DomainID;
                        objOpportunity.OppType = 1;
                        objOpportunity.OrderStatus = numOrderStatus;

                        dsItems.Tables.Clear();
                        dsItems.Tables.Add(dtItems.Copy());
                        if (dsItems.Tables[0].Rows.Count != 0)
                            objOpportunity.strItems = ((dtItems.Rows.Count > 0) ? ("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsItems.GetXml()) : "");
                        else
                            objOpportunity.strItems = "";
                        objOpportunity.OppComments = ItemMessage;
                        objOpportunity.Source = WebApiId;
                        objOpportunity.SourceType = 3;
                        objOpportunity.TaxOperator = 2; //Remove by default setting of adding sales tax while it comes through marketplace itself
                        objOpportunity.DealStatus = (long)MOpportunity.EnmDealStatus.DealWon;
                        arrOutPut = objOpportunity.Save();
                        lngOppId = CCommon.ToLong(arrOutPut[0]);

                        //Save Source 
                        objOpportunity.vcSource = Source;
                        objOpportunity.WebApiId = WebApiId;
                        objOpportunity.OpportunityId = lngOppId;
                        objOpportunity.ManageOppLinking();

                        objOpportunity.CompanyID = lngDivId;

                        if (OrderFulFillData.Address != null)
                        {
                            AddressType shippingAddress = OrderFulFillData.Address;
                            if (!string.IsNullOrEmpty(shippingAddress.Name))
                            {
                                objOpportunity.BillCompanyName = shippingAddress.Name;
                            }

                            if (shippingAddress.AddressFieldOne != null && shippingAddress.AddressFieldOne != "")
                            {
                                AddressStreet = shippingAddress.AddressFieldOne;
                            }
                            if (shippingAddress.AddressFieldTwo != null && shippingAddress.AddressFieldTwo != "")
                            {
                                AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldTwo;
                            }
                            if (shippingAddress.AddressFieldThree != null && shippingAddress.AddressFieldThree != "")
                            {
                                AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldThree;
                            }
                            if (shippingAddress.City != null && shippingAddress.City != "")
                            {
                                AddressStreet = AddressStreet + " " + shippingAddress.City;
                            }

                            objContacts.BillStreet = AddressStreet;
                            objContacts.ShipStreet = AddressStreet;
                            objOpportunity.BillStreet = AddressStreet;

                            if (shippingAddress.City != null && shippingAddress.City != "")
                            {
                                objContacts.BillCity = shippingAddress.City;
                                objContacts.ShipCity = shippingAddress.City;
                                objOpportunity.BillCity = shippingAddress.City;
                            }
                            if (shippingAddress.StateOrRegion != null && shippingAddress.StateOrRegion != "")
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 21;
                                objCommon.Str = shippingAddress.StateOrRegion;
                                objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }
                            if (shippingAddress.County != null && shippingAddress.County != "")
                            {
                                //objContacts.BillCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                //objContacts.ShipCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                            }
                            if (shippingAddress.CountryCode != null && shippingAddress.CountryCode != "")
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 22;
                                objCommon.Str = shippingAddress.CountryCode;
                                objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }
                            if (shippingAddress.PostalCode != null && shippingAddress.PostalCode != "")
                            {
                                objContacts.BillPostal = shippingAddress.PostalCode;
                                objContacts.ShipPostal = shippingAddress.PostalCode;
                                objOpportunity.BillPostal = shippingAddress.PostalCode;
                            }
                            objOpportunity.Mode = 0;
                            objOpportunity.UpdateOpportunityAddress();
                            objOpportunity.Mode = 1;
                            objOpportunity.UpdateOpportunityAddress();
                        }

                        LogMessage = "Order Details updated for API OrderID : " + objOrdReport.AmazonOrderID + " Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From NewReadAmazonOrders : " + LogMessage);

                        //if (SalesTaxRate > 0)
                        //{
                        //    CCommon objCommon1 = new CCommon();
                        //    objCommon1.Mode = 35;
                        //    objCommon1.UpdateRecordID = lngOppId;
                        //    objCommon1.Comments = CCommon.ToString(Math.Round(SalesTaxRate, 4));
                        //    objCommon1.DomainID = DomainID;
                        //    objCommon1.UpdateSingleFieldValue();
                        //    LogMessage = "Tax Percentage updated to " + CCommon.ToString(Math.Round(SalesTaxRate, 4)) + " for API OrderID : " + objOrdReport.AmazonOrderID + " Biz Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        //    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                        //}

                        objWebApi.WebApiId = WebApiId;
                        objWebApi.DomainID = DomainID;
                        objWebApi.UserCntID = numRecordOwner;
                        objWebApi.OppId = lngOppId;
                        objWebApi.vcAPIOppId = objOrdReport.AmazonOrderID;
                        objWebApi.AddAPIopportunity();

                        objWebApi.API_OrderId = objOrdReport.AmazonOrderID;
                        objWebApi.API_OrderStatus = 1;
                        objWebApi.WebApiOrdDetailId = 1;
                        objWebApi.ManageAPIOrderDetails();

                        //Insert mapping between Biz OrderItems and Amazon OrderItems 
                        UpdateApiOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, objOpportunity.OppRefOrderNo, OrderItem);
                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From NewReadAmazonOrders : " + "OppID:" + lngOppId + ",AmazonOrderID:" + objOrdReport.AmazonOrderID + ", Insert mapping between Biz OrderItems and Amazon OrderItems,Message:" + ItemMessage + ", OrderTotal:" + OrderTotal);


                        decimal OrderAmount = OrderTotal; // + ShipCost - dTotDiscount + SalesTaxAmount;

                        //string Reference = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM"); 
                        string Reference = arrOutPut[1];
                        if (ItemMessage == "")
                        {
                            if (OrderAmount >= 0)
                            {
                                bool IsAuthoritative = false;
                                BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                BizCommonFunctions.CreateOppBizDoc(objOppBizDocs, numRecordOwner, DomainID, lngOppId, objOpportunity.OppRefOrderNo, numBizDocId, BizDocStatusId, lngDivId, OrderDate, ShipCost, dTotDiscount, out OppBizDocID, out IsAuthoritative, ExpenseAccountId, SalesTaxRate, OrderAmount, Reference);

                            }
                            else
                            {
                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From NewReadAmazonOrders : " + "Order Total Amount (cannot be a negative amount. Conflict Occoured while processing Amazon Order Id : " + objOrdReport.AmazonOrderID);
                            }
                        }
                        else
                        {
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From NewReadAmazonOrders : " + ItemMessage);
                        }
                        //}
                        //else
                        //{
                        //    string ErrMessage = "Order report doesnot have sufficient Buyer details to process the Amazon Order : " + objOrdReport.AmazonOrderID;
                        //    GeneralFunctions.WriteMessage(DomainID, "LOG", ErrMessage);
                        //}
                    }
                    else
                    {
                        string ErrMessage = "Order details for Amazon Order Id : " + objOrdReport.AmazonOrderID + " is already exists in BizAutomation";
                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From NewReadAmazonOrders : " + ErrMessage);
                    }
                }
                else
                {
                    string ErrMessage = "Order report doesnot have sufficient Order Details. Conflict Occoured while processing Amazon Order Id : " + objOrdReport.AmazonOrderID;
                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From NewReadAmazonOrders : " + ErrMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Process Orders in Amazon Envelope and Creates Sales Order in BizDatabase
        /// Adds new Item into BizDatabase if the Order Item is Not available in BizDatabase
        /// Makes Deposit Entry
        /// Makes General Journal Header and Journal Detail entry
        /// </summary>
        /// <param name="objAmazonEnvelope">AmazonEnvelope Object</param>
        /// <param name="DomainID">Biz Domain ID for the Merchant</param>
        /// <param name="WebApiId">Web API ID</param>
        /// <param name="Source">Order's Source</param>
        /// <param name="numWareHouseID">Default Warehouse Id for the E-Bay Order Items</param>
        /// <param name="numRecordOwner">Orders Reocrd Owner's Id</param>
        /// <param name="numAssignTo">Assinged to User Id</param>
        /// <param name="numRelationshipId">E-Bay Orders Customer's default relationship Id</param>
        /// <param name="numProfileId">Default Profile Id</param>
        /// <param name="numBizDocId">Default Biz Document Id for E-Bay Orders</param>
        /// <param name="numOrderStatus">Default Order Status</param>
        /// <param name="ExpenseAccountId">Default Expense Account Id</param> 
        public void ProcessAmazonOrders(AmazonEnvelope objAmazonEnvelope, long DomainID, int WebApiId, string Source,
            int numWareHouseID, long numRecordOwner, long numAssignTo, long numRelationshipId, long numProfileId, long numBizDocId, long BizDocStatusId, int numOrderStatus, int ExpenseAccountId, long DiscountItemMapping, long ShippingServiceItemID, long SalesTaxItemMappingID, string ShipToPhoneNo)
        {
            string LogMessage = "";
            decimal SalesTaxRate = 0;
            CContacts objContacts = null;
            CLeads objLeads = null;
            ImportWizard objImpWzd = new ImportWizard();

            CCommon objCommon = new CCommon();

            foreach (AmazonEnvelopeMessage Message in objAmazonEnvelope.Message)
            {
                MOpportunity objOpportunity = new MOpportunity();
                OrderReportBillingData billData = new OrderReportBillingData();
                OrderReportFulfillmentData OrderFulFillData = new OrderReportFulfillmentData();
                OrderReportCustomerOrderInfo[] CustomerOrderInfo;
                OrderReportItem[] OrderItem = null;
                OrderReport objOrdReport = new OrderReport();
                WebAPI objWebApi = new WebAPI();

                string ItemMessage = "";
                decimal ShipCost = 0;
                decimal OrderTotal = 0;
                long CurrencyID = 0;
                decimal dTotDiscount = 0;
                decimal SalesTaxAmount = 0;
                try
                {
                    objOrdReport = (OrderReport)Message.Item;

                    if (objOrdReport.BillingData != null)
                        billData = objOrdReport.BillingData;

                    if (objOrdReport.FulfillmentData != null)
                        OrderFulFillData = objOrdReport.FulfillmentData;

                    if (objOrdReport.CustomerOrderInfo != null)
                        CustomerOrderInfo = objOrdReport.CustomerOrderInfo;

                    if (objOrdReport.Item != null)
                        OrderItem = objOrdReport.Item;

                    if (!string.IsNullOrEmpty(objOrdReport.AmazonOrderID))
                    {
                        objWebApi.DomainID = DomainID;
                        objWebApi.WebApiId = WebApiId;
                        objWebApi.vcAPIOppId = objOrdReport.AmazonOrderID;
                        if (objWebApi.CheckDuplicateAPIOpportunity())
                        {

                            int a = -1;
                            string AddressStreet = "";//To concatenate Address Lines
                            objContacts = new CContacts();
                            objLeads = new CLeads();
                            objLeads.DomainID = DomainID;

                            if (billData.BuyerEmailAddress != null && billData.BuyerEmailAddress != "")//If contains Email Address
                            {
                                objLeads.Email = billData.BuyerEmailAddress;
                                objLeads.GetConIDCompIDDivIDFromEmail(); //Check for Details for the Given DomainID and EmailID
                            }
                            else
                            {
                                objLeads.Email = "";
                            }
                            if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                            {
                                //If Email already registered
                                lngCntID = objLeads.ContactID;
                                lngDivId = objLeads.DivisionID;
                            }
                            else
                            {
                                if (billData.BuyerName != null && billData.BuyerName != "")
                                {
                                    objLeads.CompanyName = billData.BuyerName;
                                    objLeads.CustName = billData.BuyerName;
                                }
                                else if (OrderFulFillData.Address != null)
                                {
                                    AddressType shippingAddress = OrderFulFillData.Address;

                                    if (shippingAddress.Name != null && shippingAddress.Name != "")
                                    {
                                        objLeads.CompanyName = shippingAddress.Name;
                                        objLeads.CustName = shippingAddress.Name;
                                    }
                                }

                                if (OrderFulFillData.Address != null)
                                {
                                    AddressType shippingAddress = OrderFulFillData.Address;
                                    if (shippingAddress.County != null && shippingAddress.County != "")
                                    {
                                        //objLeads.Country = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                        //objLeads.SCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                    }
                                    if (shippingAddress.CountryCode != null && shippingAddress.CountryCode != "")
                                    {
                                        objCommon.DomainID = DomainID;
                                        objCommon.Mode = 22;
                                        objCommon.Str = shippingAddress.CountryCode;
                                        objLeads.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                        objLeads.SCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    }

                                    if (shippingAddress.Name != null && shippingAddress.Name != "")
                                    {
                                        a = shippingAddress.Name.IndexOf('.');
                                        if (a == -1)
                                            a = shippingAddress.Name.IndexOf(' ');
                                        if (a == -1)
                                        {
                                            objLeads.FirstName = shippingAddress.Name;
                                            objLeads.LastName = shippingAddress.Name;
                                        }
                                        else
                                        {
                                            objLeads.FirstName = shippingAddress.Name.Substring(0, a);
                                            objLeads.LastName = shippingAddress.Name.Substring(a + 1, shippingAddress.Name.Length - (a + 1));
                                        }
                                    }
                                    if (shippingAddress.PhoneNumber != null)
                                    {
                                        PhoneNumberType[] objPhoneType;
                                        objPhoneType = shippingAddress.PhoneNumber;
                                        //objLeads.ContactPhone = CCommon.ToString(shippingAddress.PhoneNumber);
                                        if (shippingAddress.PhoneNumber.Length > 0)
                                        {
                                            objLeads.ContactPhone = CCommon.ToString(shippingAddress.PhoneNumber[0].Value);
                                        }
                                        else
                                        {
                                            objLeads.ContactPhone = ShipToPhoneNo;
                                        }
                                        objLeads.PhoneExt = "";
                                    }
                                    else
                                    {
                                        objLeads.ContactPhone = ShipToPhoneNo;
                                        objLeads.PhoneExt = "";
                                    }
                                }

                                objLeads.UserCntID = numRecordOwner;
                                objLeads.ContactType = 70;
                                objLeads.PrimaryContact = true;
                                objLeads.UpdateDefaultTax = false;
                                objLeads.NoTax = false;
                                objLeads.CRMType = 1;
                                objLeads.DivisionName = "-";

                                objLeads.CompanyType = numRelationshipId;
                                objLeads.Profile = numProfileId;

                                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();//Creates Company Record
                                LogMessage = "New Company details added CompanyID : " + objLeads.CompanyID + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ProcessAmazonOrders : " + LogMessage);

                                lngDivId = objLeads.CreateRecordDivisionsInfo();
                                LogMessage = "New Divisions Informations added. Division Id : " + lngDivId + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ProcessAmazonOrders : " + LogMessage);

                                objLeads.ContactID = 0;
                                objLeads.DivisionID = lngDivId;
                                lngCntID = objLeads.CreateRecordAddContactInfo();//Creates Contact Info
                                LogMessage = "New Contact Informations added. Contact Id : " + lngCntID + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ProcessAmazonOrders : " + LogMessage);

                                if (OrderFulFillData.Address != null)
                                {
                                    objContacts.FirstName = objLeads.FirstName;
                                    objContacts.LastName = objLeads.LastName;
                                    objContacts.ContactPhone = objLeads.ContactPhone;
                                    objContacts.Email = objLeads.Email;

                                    AddressType shippingAddress = OrderFulFillData.Address;

                                    if (shippingAddress.AddressFieldOne != null && shippingAddress.AddressFieldOne != "")
                                        AddressStreet = shippingAddress.AddressFieldOne;

                                    if (shippingAddress.AddressFieldTwo != null && shippingAddress.AddressFieldTwo != "")
                                        AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldTwo;

                                    if (shippingAddress.AddressFieldThree != null && shippingAddress.AddressFieldThree != "")
                                        AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldThree;

                                    if (shippingAddress.City != null && shippingAddress.City != "")
                                        AddressStreet = AddressStreet + " " + shippingAddress.City;

                                    objContacts.BillStreet = AddressStreet;
                                    objContacts.ShipStreet = AddressStreet;

                                    if (shippingAddress.City != null && shippingAddress.City != "")
                                    {
                                        objContacts.BillCity = shippingAddress.City;
                                        objContacts.ShipCity = shippingAddress.City;
                                    }

                                    if (shippingAddress.StateOrRegion != null && shippingAddress.StateOrRegion != "")
                                    {
                                        objCommon.DomainID = DomainID;
                                        objCommon.Mode = 21;
                                        objCommon.Str = shippingAddress.StateOrRegion;
                                        objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                        objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    }

                                    if (shippingAddress.CountryCode != null && shippingAddress.CountryCode != "")
                                    {
                                        objCommon.DomainID = DomainID;
                                        objCommon.Mode = 22;
                                        objCommon.Str = shippingAddress.CountryCode;
                                        objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                        objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    }

                                    if (shippingAddress.PostalCode != null && shippingAddress.PostalCode != "")
                                    {
                                        objContacts.BillPostal = shippingAddress.PostalCode;
                                        objContacts.ShipPostal = shippingAddress.PostalCode;
                                    }

                                    objContacts.BillingAddress = 0;
                                    objContacts.DivisionID = lngDivId;
                                    objContacts.ContactID = lngCntID;
                                    objContacts.RecordID = lngDivId;
                                    objContacts.DomainID = DomainID;
                                    objContacts.IsPrimaryAddress = CCommon.ToBool(1);
                                    objContacts.UpdateCompanyAddress();
                                }
                            }

                            objOpportunity.OppRefOrderNo = objOrdReport.AmazonOrderID;
                            objOpportunity.MarketplaceOrderID = objOrdReport.AmazonOrderID;
                            BizCommonFunctions.CreateItemTable(dtItems);
                            ItemMessage = AddOrderItemDataset(DomainID, WebApiId, numRecordOwner, numWareHouseID, DiscountItemMapping, OrderItem, CCommon.ToString(objOrdReport.FulfillmentData.FulfillmentServiceLevel), ShippingServiceItemID, SalesTaxItemMappingID, out CurrencyID, out OrderTotal, out ShipCost, out dTotDiscount, out SalesTaxRate, out SalesTaxAmount);

                            objOpportunity.CurrencyID = CurrencyID;
                            objOpportunity.Amount = OrderTotal;
                            objContacts.BillingAddress = 0;
                            objContacts.DivisionID = lngDivId;
                            //objContacts.UpdateCompanyAddress();//Updates Company Details
                            // SaveTaxTypes();
                            BizCommonFunctions.SaveTaxTypes(ds, lngDivId);
                            arrBillingIDs = new string[4];
                            arrBillingIDs[0] = CCommon.ToString(objLeads.CompanyID);
                            arrBillingIDs[1] = CCommon.ToString(lngDivId);
                            arrBillingIDs[2] = CCommon.ToString(lngCntID);
                            arrBillingIDs[3] = objLeads.CompanyName;
                            if (lngCntID == 0)
                                return;

                            //Create Opportunity Details
                            DateTime OrderDate = objOrdReport.OrderDate;
                            objOpportunity.MarketplaceOrderDate = OrderDate;
                            objOpportunity.OpportunityId = 0;
                            objOpportunity.ContactID = lngCntID;
                            objOpportunity.DivisionID = lngDivId;
                            objOpportunity.UserCntID = numRecordOwner;
                            objOpportunity.AssignedTo = numAssignTo;

                            objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
                            objOpportunity.EstimatedCloseDate = DateTime.Now;
                            objOpportunity.PublicFlag = 0;
                            objOpportunity.DomainID = DomainID;
                            objOpportunity.OppType = 1;
                            objOpportunity.OrderStatus = numOrderStatus;

                            dsItems.Tables.Clear();
                            dsItems.Tables.Add(dtItems.Copy());
                            if (dsItems.Tables[0].Rows.Count != 0)
                                objOpportunity.strItems = ((dtItems.Rows.Count > 0) ? ("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsItems.GetXml()) : "");
                            else
                                objOpportunity.strItems = "";
                            objOpportunity.OppComments = ItemMessage;
                            objOpportunity.Source = WebApiId;
                            objOpportunity.SourceType = 3;
                            objOpportunity.TaxOperator = 2; //Remove by default setting of adding sales tax while it comes through marketplace itself
                            objOpportunity.DealStatus = (long)MOpportunity.EnmDealStatus.DealWon;
                            arrOutPut = objOpportunity.Save();
                            lngOppId = CCommon.ToLong(arrOutPut[0]);

                            //Save Source 
                            objOpportunity.vcSource = Source;
                            objOpportunity.WebApiId = WebApiId;
                            objOpportunity.OpportunityId = lngOppId;
                            objOpportunity.ManageOppLinking();

                            objOpportunity.CompanyID = lngDivId;

                            if (OrderFulFillData.Address != null)
                            {
                                AddressType shippingAddress = OrderFulFillData.Address;
                                if (!string.IsNullOrEmpty(shippingAddress.Name))
                                {
                                    objOpportunity.BillCompanyName = shippingAddress.Name;
                                }

                                if (shippingAddress.AddressFieldOne != null && shippingAddress.AddressFieldOne != "")
                                {
                                    AddressStreet = shippingAddress.AddressFieldOne;
                                }
                                if (shippingAddress.AddressFieldTwo != null && shippingAddress.AddressFieldTwo != "")
                                {
                                    AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldTwo;
                                }
                                if (shippingAddress.AddressFieldThree != null && shippingAddress.AddressFieldThree != "")
                                {
                                    AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldThree;
                                }
                                if (shippingAddress.City != null && shippingAddress.City != "")
                                {
                                    AddressStreet = AddressStreet + " " + shippingAddress.City;
                                }

                                objContacts.BillStreet = AddressStreet;
                                objContacts.ShipStreet = AddressStreet;
                                objOpportunity.BillStreet = AddressStreet;

                                if (shippingAddress.City != null && shippingAddress.City != "")
                                {
                                    objContacts.BillCity = shippingAddress.City;
                                    objContacts.ShipCity = shippingAddress.City;
                                    objOpportunity.BillCity = shippingAddress.City;
                                }
                                if (shippingAddress.StateOrRegion != null && shippingAddress.StateOrRegion != "")
                                {
                                    objCommon.DomainID = DomainID;
                                    objCommon.Mode = 21;
                                    objCommon.Str = shippingAddress.StateOrRegion;
                                    objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                }
                                if (shippingAddress.County != null && shippingAddress.County != "")
                                {
                                    //objContacts.BillCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                    //objContacts.ShipCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                }
                                if (shippingAddress.CountryCode != null && shippingAddress.CountryCode != "")
                                {
                                    objCommon.DomainID = DomainID;
                                    objCommon.Mode = 22;
                                    objCommon.Str = shippingAddress.CountryCode;
                                    objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                    objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                }
                                if (shippingAddress.PostalCode != null && shippingAddress.PostalCode != "")
                                {
                                    objContacts.BillPostal = shippingAddress.PostalCode;
                                    objContacts.ShipPostal = shippingAddress.PostalCode;
                                    objOpportunity.BillPostal = shippingAddress.PostalCode;
                                }
                                objOpportunity.Mode = 0;
                                objOpportunity.UpdateOpportunityAddress();
                                objOpportunity.Mode = 1;
                                objOpportunity.UpdateOpportunityAddress();
                            }

                            LogMessage = "Order Details updated for API OrderID : " + objOrdReport.AmazonOrderID + " Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ProcessAmazonOrders : " + LogMessage);

                            //if (SalesTaxRate > 0)
                            //{
                            //    CCommon objCommon1 = new CCommon();
                            //    objCommon1.Mode = 35;
                            //    objCommon1.UpdateRecordID = lngOppId;
                            //    objCommon1.Comments = CCommon.ToString(Math.Round(SalesTaxRate, 4));
                            //    objCommon1.DomainID = DomainID;
                            //    objCommon1.UpdateSingleFieldValue();
                            //    LogMessage = "Tax Percentage updated to " + CCommon.ToString(Math.Round(SalesTaxRate, 4)) + " for API OrderID : " + objOrdReport.AmazonOrderID + " Biz Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                            //    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                            //}

                            objWebApi.WebApiId = WebApiId;
                            objWebApi.DomainID = DomainID;
                            objWebApi.UserCntID = numRecordOwner;
                            objWebApi.OppId = lngOppId;
                            objWebApi.vcAPIOppId = objOrdReport.AmazonOrderID;
                            objWebApi.AddAPIopportunity();

                            objWebApi.API_OrderId = objOrdReport.AmazonOrderID;
                            objWebApi.API_OrderStatus = 1;
                            objWebApi.WebApiOrdDetailId = 1;
                            objWebApi.ManageAPIOrderDetails();

                            //Insert mapping between Biz OrderItems and Amazon OrderItems 
                            UpdateApiOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, objOpportunity.OppRefOrderNo, OrderItem);
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ProcessAmazonOrders : " + "OppID:" + lngOppId + ",AmazonOrderID:" + objOrdReport.AmazonOrderID + ", Insert mapping between Biz OrderItems and Amazon OrderItems,Message:" + ItemMessage + ", OrderTotal:" + OrderTotal);


                            decimal OrderAmount = OrderTotal; // + ShipCost - dTotDiscount + SalesTaxAmount;

                            //string Reference = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM"); 
                            string Reference = arrOutPut[1];
                            if (ItemMessage == "")
                            {
                                if (OrderAmount >= 0)
                                {
                                    bool IsAuthoritative = false;
                                    BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                                    BizCommonFunctions.CreateOppBizDoc(objOppBizDocs, numRecordOwner, DomainID, lngOppId, objOpportunity.OppRefOrderNo, numBizDocId, BizDocStatusId, lngDivId, OrderDate, ShipCost, dTotDiscount, out OppBizDocID, out IsAuthoritative, ExpenseAccountId, SalesTaxRate, OrderAmount, Reference);

                                }
                                else
                                {
                                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ProcessAmazonOrders : " + "Order Total Amount (cannot be a negative amount. Conflict Occoured while processing Amazon Order Id : " + objOrdReport.AmazonOrderID);
                                }
                            }
                            else
                            {
                                GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ProcessAmazonOrders : " + ItemMessage);
                            }
                            //}
                            //else
                            //{
                            //    string ErrMessage = "Order report doesnot have sufficient Buyer details to process the Amazon Order : " + objOrdReport.AmazonOrderID;
                            //    GeneralFunctions.WriteMessage(DomainID, "LOG", ErrMessage);
                            //}
                        }
                        else
                        {
                            string ErrMessage = "Order details for Amazon Order Id : " + objOrdReport.AmazonOrderID + " is already exists in BizAutomation";
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ProcessAmazonOrders : " + ErrMessage);
                        }

                    }
                    else
                    {
                        string ErrMessage = "Order report doesnot have sufficient Order Details. Conflict Occoured while processing Amazon Order Id : " + objOrdReport.AmazonOrderID;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", "Amazon:- From ProcessAmazonOrders : " + ErrMessage);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void RemoveNonTaxItems(DataTable dtOppItems)
        {
            try
            {
                foreach (DataRow dr in dtOppItems.Rows)
                {
                    if (CCommon.ToBool(dr["bitTaxable0"]) == false)
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Gets the Order Items List and creates a New structure of Order Item details inorder to add in BizDatabase
        ///  Adds new Item into BizDatabase if the Order Item is Not available in BizDatabase
        /// </summary>
        /// <param name="DomainId">Domain ID</param>
        /// <param name="WebApiId">WebApi ID</param>
        /// <param name="RecordOwner">Record Owner</param>
        /// <param name="WareHouseId">Ware House ID</param>
        /// <param name="objList">Order Items detail List</param>
        /// <param name="ShippingServiceSelected">Shipping Service Selected</param>
        /// <param type="output Parameter" name="CurrencyID">Currency ID</param>
        /// <param type="output Parameter" name="OrderTotal">Order Total</param>
        /// <param type="output Parameter" name="ShipCost">Shipping Cost</param>
        /// <param type="output Parameter" name="dTotDiscount">Total Discount</param>
        /// <returns>Error Message if Any(while processing Order line Items) </returns>
        private string AddOrderItemDataset(long DomainId, int WebApiId, long RecordOwner, int WareHouseId, long DiscountItemMapping, OrderReportItem[] objList, string ShippingServiceSelected,
            long ShippingServiceItemID, long SalesTaxItemMappingID, out long CurrencyID, out decimal OrderTotal,
            out decimal ShipCost, out decimal dTotDiscount, out decimal SalesTaxRate, out decimal SalesTaxAmount)
        {
            dtItems.Clear();
            dsItems.Clear();
            CCommon objCommon = new CCommon();
            string Message = "";
            DataRow drItem;
            int ItemCount = 0;
            string ItemType = "";
            string[] itemCodeType;
            string itemCode = "";
            WebAPI objWebApi = new WebAPI();
            objWebApi.DomainID = DomainId;
            objWebApi.WebApiId = WebApiId;
            dTotDiscount = 0;
            decimal decDiscount = 0;
            string DiscountDesc = "";
            decimal Tax0 = 0;
            OrderTotal = 0;
            ShipCost = 0;
            CurrencyID = 0;
            SalesTaxRate = 0;
            SalesTaxAmount = 0;
            DataTable dtDiscountItems = new DataTable();
            dtDiscountItems.Columns.Clear();
            dtDiscountItems.Columns.Add("DiscountName");
            dtDiscountItems.Columns.Add("DiscountAmount");
            dtDiscountItems.Columns.Add("TaxAmount");
            DataRow drDiscountItem;
            decimal SaleAmount1 = 0;

            foreach (OrderReportItem orderItem in objList)
            {
                BuyerPriceComponent[] ItemPriceComponents;
                AmazonFeesFee[] AmazonFees;
                ItemCount += 1;
                try
                {
                    if (orderItem.SKU != "" & orderItem.SKU != null)
                    {
                        //objCommon.DomainID = DomainId;
                        //objCommon.Mode = 20;
                        //objCommon.Str = orderItem.SKU;
                        //itemCode = CCommon.ToString(objCommon.GetSingleFieldValue());

                        objCommon.DomainID = DomainId;
                        objCommon.Mode = 38;
                        objCommon.Str = orderItem.SKU;
                        itemCodeType = CCommon.ToString(objCommon.GetSingleFieldValue()).Split('~');
                        if (itemCodeType.Length > 1)
                        {
                            if (!string.IsNullOrEmpty(itemCodeType[0]))
                            {
                                itemCode = itemCodeType[0];
                            }
                            if (!string.IsNullOrEmpty(itemCodeType[1]))
                            {
                                ItemType = BizCommonFunctions.GetItemTypeNameByCharType(itemCodeType[1]);
                            }
                        }

                        if (itemCode == "")
                        {
                            DataTable dtProductDetails = new DataTable();
                            BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                            DataRow dr = dtProductDetails.NewRow();

                            DataTable dtCopy = new DataTable();
                            dr = GetItemDetailsForSKU(DomainId, WebApiId, RecordOwner, orderItem.SKU, dr, ref dtCopy);
                            string ItemClassificationName = "Amazon_OrderItem_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                            long ItemClassificationID = 0;
                            ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);
                            itemCode = CCommon.ToString(BizCommonFunctions.AddNewItemToBiz(dr, DomainId, WebApiId, RecordOwner, WareHouseId, ItemClassificationID));
                            ItemType = BizCommonFunctions.GetItemTypeNameByCharType("P");
                            WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();

                            string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";

                            BACRM.BusinessLogic.Item.CItems objItems = new BACRM.BusinessLogic.Item.CItems();
                            objItems.ItemCode = CCommon.ToInteger(itemCode);
                            TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);
                            objItems.ClientZoneOffsetTime = CCommon.ToInteger(diff1.TotalMinutes);
                            DataTable dtItemDetails = objItems.ItemDetails();
                            if (CCommon.ToLong(dtItemDetails.Rows[0]["numItemGroup"]) > 0 && CCommon.ToString(orderItem.SKU).Length > 0)
                            {
                                FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + "_" + CCommon.ToString(orderItem.SKU) + ".xml";
                            }
                            else
                            {
                                FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";
                            }

                            if (System.IO.File.Exists(FilePath))
                            {
                                System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                                XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                objStreamReader.Close();
                            }
                            objWebAPIItemDetail.DimensionUOM = BizCommonFunctions.GetLengthUOMCode(CCommon.ToString(dr["DimensionUOM"]));
                            objWebAPIItemDetail.WeightUOM = BizCommonFunctions.GetWeightUOMCode(CCommon.ToString(dr["WeightUOM"]));
                            objWebAPIItemDetail.Length = CCommon.ToString(dr["fltLength"]);
                            objWebAPIItemDetail.Height = CCommon.ToString(dr["fltHeight"]);
                            objWebAPIItemDetail.Width = CCommon.ToString(dr["fltWidth"]);
                            objWebAPIItemDetail.Weight = CCommon.ToString(dr["fltWeight"]);
                            objWebAPIItemDetail.WarrantyDescription = CCommon.ToString(dr["Warranty"]);

                            StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");

                            if (CCommon.ToLong(dtItemDetails.Rows[0]["numItemGroup"]) > 0 && CCommon.ToString(orderItem.SKU).Length > 0)
                            {
                                objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + "_" + CCommon.ToString(orderItem.SKU) + ".xml");
                            }
                            else
                            {
                                objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");
                            }

                            XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                            Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                            objStreamWriter.Close();

                        }
                        if (itemCode != "")
                        {
                            decDiscount = 0;
                            DiscountDesc = "";
                            drItem = dtItems.NewRow();
                            drItem["numoppitemtCode"] = ItemCount;
                            drItem["numItemCode"] = itemCode;

                            if (CCommon.ToInteger(orderItem.Quantity) != 0)
                            {
                                drItem["numUnitHour"] = orderItem.Quantity;
                                if (orderItem.ItemPrice != null)
                                {
                                    ItemPriceComponents = orderItem.ItemPrice;

                                    foreach (BuyerPriceComponent Component in ItemPriceComponents)
                                    {
                                        if (Component.Type == BuyerPriceComponentType.Principal)
                                        {
                                            drItem["monPrice"] = CCommon.ToDecimal(Component.Amount.Value) / CCommon.ToInteger(orderItem.Quantity);
                                            //SaleAmount1 = CCommon.ToDecimal(Component.Amount.Value) / CCommon.ToInteger(orderItem.Quantity); ;
                                            OrderTotal += CCommon.ToDecimal(Component.Amount.Value);
                                            objCommon.DomainID = DomainId;
                                            objCommon.Mode = 15;
                                            objCommon.Str = CCommon.ToString(Component.Amount.currency);
                                            CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                                            //drItem["monTotAmount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                            //drItem["monTotAmtBefDiscount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                            drItem["monTotAmount"] = Component.Amount.Value;
                                        }
                                        //if (Component.Type == BuyerPriceComponentType.COD)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.CODFee)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.CODTax)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.ExportCharge)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.GiftWrap)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.GiftWrapTax)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.Goodwill)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.Other)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.RestockingFee)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.RestockingFeeTax)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.ReturnShipping)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        if (Component.Type == BuyerPriceComponentType.Shipping)
                                            ShipCost += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.ShippingTax)
                                        //    Tax0 += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.Surcharge)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        if (Component.Type == BuyerPriceComponentType.Tax)
                                        {
                                            Tax0 += CCommon.ToDecimal(Component.Amount.Value);
                                            if (orderItem.ItemTaxData != null)
                                            {
                                                if (orderItem.ItemTaxData.TaxableAmounts != null)
                                                {
                                                    decimal TaxableAmt = CCommon.ToDecimal(orderItem.ItemTaxData.TaxableAmounts.State.Value);
                                                    if (CCommon.ToDecimal(TaxableAmt) > 0)
                                                    {
                                                        SalesTaxRate = (CCommon.ToDecimal(Component.Amount.Value) * 100) / TaxableAmt;
                                                        SalesTaxAmount += CCommon.ToDecimal(Component.Amount.Value);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (orderItem.ItemFees != null)
                                {
                                    AmazonFees = orderItem.ItemFees;
                                    foreach (AmazonFeesFee FeeComponent in AmazonFees)
                                    {
                                        if (FeeComponent.Type == "Commission")
                                        {
                                            decDiscount += CCommon.ToDecimal(FeeComponent.Amount.Value);
                                            DiscountDesc += FeeComponent.Type + " fee : " + CCommon.ToString(FeeComponent.Amount.Value) + Environment.NewLine;
                                            if (CCommon.ToDecimal(FeeComponent.Amount.Value) != 0)
                                            {
                                                drDiscountItem = dtDiscountItems.NewRow();
                                                drDiscountItem["DiscountName"] = FeeComponent.Type + " fee for Item-SKU - " + orderItem.SKU;
                                                drDiscountItem["DiscountAmount"] = CCommon.ToString(FeeComponent.Amount.Value);
                                                drDiscountItem["TaxAmount"] = 0;
                                                dtDiscountItems.Rows.Add(drDiscountItem);
                                            }
                                        }
                                        if (FeeComponent.Type == "Variableclosingfee")
                                        {
                                            decDiscount += CCommon.ToDecimal(FeeComponent.Amount.Value);
                                            DiscountDesc += FeeComponent.Type + " fee : " + CCommon.ToString(FeeComponent.Amount.Value) + "; " + Environment.NewLine;
                                            if (CCommon.ToDecimal(FeeComponent.Amount.Value) != 0)
                                            {
                                                drDiscountItem = dtDiscountItems.NewRow();
                                                drDiscountItem["DiscountName"] = FeeComponent.Type + " fee for Item-SKU - " + orderItem.SKU;
                                                drDiscountItem["DiscountAmount"] = CCommon.ToString(FeeComponent.Amount.Value);
                                                drDiscountItem["TaxAmount"] = 0;
                                                // drDiscountItem["DiscountItemSKU"] = "";
                                                dtDiscountItems.Rows.Add(drDiscountItem);
                                            }
                                        }
                                    }

                                }
                                if (orderItem.Promotion != null)
                                {
                                    foreach (TaxablePromotionType promo in orderItem.Promotion)
                                    {
                                        if (promo.Component != null)
                                        {
                                            foreach (TaxablePromotionTypeComponent promoComponent in promo.Component)
                                            {
                                                PromotionApplicationType CompType = promoComponent.Type;
                                                string ComponentName = CCommon.ToString(CompType);
                                                if (promoComponent.Amount != null)
                                                {
                                                    CurrencyAmount promoAmount = new CurrencyAmount();
                                                    promoAmount = promoComponent.Amount;
                                                    if (CCommon.ToDecimal(promoAmount.Value) != 0)
                                                    {
                                                        decimal dTaxDiscount = SalesTaxRate * CCommon.ToDecimal(promoAmount.Value) / 100;
                                                        drDiscountItem = dtDiscountItems.NewRow();
                                                        drDiscountItem["DiscountName"] = ComponentName + " Promotion discount for Item-SKU - " + orderItem.SKU;
                                                        drDiscountItem["DiscountAmount"] = CCommon.ToString(CCommon.ToDecimal(promoAmount.Value));
                                                        drDiscountItem["TaxAmount"] = dTaxDiscount;
                                                        dtDiscountItems.Rows.Add(drDiscountItem);
                                                        decDiscount += promoAmount.Value;
                                                        DiscountDesc += "Promotion discount for Item-SKU - " + orderItem.SKU + " : " + CCommon.ToString(promoAmount.Value) + "; ";

                                                    }
                                                }
                                            }
                                        }
                                        // TaxablePromotionTypeComponent promoComponent =  promo.Component[0];
                                    }
                                }
                                //drItem["monPrice"] = SaleAmount1 - decimal.Negate(decDiscount);
                                //drItem["monTotAmount"] = SaleAmount1;
                                drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]);
                            }

                            drItem["numUOM"] = 0; // get it from item table, in procedure
                            drItem["vcUOMName"] = "";// get it from item table, in procedure
                            drItem["UOMConversionFactor"] = 1.0000;
                            drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                            drItem["vcModelID"] = "";// get it from item table, in procedure
                            drItem["numWarehouseID"] = WareHouseId;
                            if (orderItem.Title != null && orderItem.Title != "")
                            {
                                drItem["vcItemName"] = orderItem.Title;
                            }
                            drItem["Warehouse"] = "";
                            drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                            drItem["ItemType"] = ItemType;// get it from item table, in procedure
                            drItem["Attributes"] = "";
                            drItem["Op_Flag"] = 1;
                            drItem["bitWorkOrder"] = false;
                            drItem["DropShip"] = false;
                            //drItem["fltDiscount"] = decimal.Negate(decDiscount);
                            drItem["fltDiscount"] = 0;
                            dTotDiscount += decimal.Negate(decDiscount);
                            //if (fltDiscount > 0)
                            drItem["bitDiscountType"] = 1; //Flat discount
                            //else
                            //    drItem["bitDiscountType"] = false; // Percentage

                            //if (orderItem.ItemTax != null)
                            //{
                            //    CMoney ItemTax = orderItem.ItemTax;
                            //    Tax0 = CCommon.ToDecimal(ItemTax.Amount);
                            //}
                            Tax0 = 0;
                            drItem["Tax0"] = Tax0;
                            if (Tax0 > 0)
                                drItem["bitTaxable0"] = true;
                            else
                                drItem["bitTaxable0"] = false;

                            //drItem["Tax0"] = 0;
                            drItem["numVendorWareHouse"] = 0;
                            drItem["numShipmentMethod"] = 0;
                            drItem["numSOVendorId"] = 0;
                            drItem["numProjectID"] = 0;
                            drItem["numProjectStageID"] = 0;
                            drItem["charItemType"] = ""; // should be taken from procedure
                            drItem["numToWarehouseItemID"] = 0;
                            //drItem["Tax41"] = 0;
                            //drItem["bitTaxable41"] = false;
                            //drItem["Tax42"] = 0;
                            //drItem["bitTaxable42"] = false;
                            drItem["Weight"] = ""; // should take from procedure 
                            drItem["WebApiId"] = WebApiId;
                            drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                            drItem["vcSKU"] = orderItem.SKU;
                            dtItems.Rows.Add(drItem);
                        }
                        else
                        {
                            Message += "Item " + orderItem.Title + " (SKU: " + orderItem.SKU + ") not found in Amazon-Biz linking database or unable to get Item detail inorder Add in BizDatabase" + Environment.NewLine;
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From AddOrderItemDataset : " + ex.Message);
                    GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From AddOrderItemDataset : " + ex.StackTrace);
                    throw ex;
                }
            }
            if (SalesTaxAmount > 0)
            {
                OrderTotal += SalesTaxAmount;
                drItem = dtItems.NewRow();
                drItem = BizCommonFunctions.GetSalesTaxItem(drItem, SalesTaxAmount, SalesTaxItemMappingID, DomainId, WebApiId);
                dtItems.Rows.Add(drItem);
            }
            if (ShipCost > 0)
            {
                OrderTotal += ShipCost;
                drItem = dtItems.NewRow();
                drItem = BizCommonFunctions.GetShippingCostItem(drItem, ShipCost, ShippingServiceItemID, DomainId, WebApiId);
                dtItems.Rows.Add(drItem);
            }
            if (dTotDiscount > 0)
            {
                OrderTotal -= dTotDiscount;
            }

            if (dtDiscountItems.Rows.Count > 0)
            {
                foreach (DataRow drDiscount in dtDiscountItems.Rows)
                {
                    drItem = dtItems.NewRow();
                    string DiscountItemName = CCommon.ToString(drDiscount["DiscountName"]);
                    decimal dDiscountItemValue = CCommon.ToDecimal(drDiscount["DiscountAmount"]);
                    decimal dTaxAmount = CCommon.ToDecimal(drDiscount["TaxAmount"]);
                    drItem = BizCommonFunctions.GetDiscountOrderItem(drItem, dDiscountItemValue, DiscountItemMapping, DomainId, WebApiId, DiscountItemName, dTaxAmount);
                    dtItems.Rows.Add(drItem);
                }
            }


            return Message;
        }

        #endregion Orders

        #region Report Operations

        /// <summary>
        /// Schedule Report (Hard Coded for Generate Report on 15 Minutes Interval)
        /// Not Used as of Now (since Scheduling Report is One Time Operation)
        /// Even it can be Scheduled from Amazon MWS Scratchpad https://mws.amazonservices.com/scratchpad/index.html
        /// </summary>
        /// <param name="objSvcConfigInfo">Service Configuration Information</param>
        /// <param name="strReportTypeId">Report Type Identifier</param>
        /// <param name="strScheduleInterval">Time Interval to Generate </param>
        /// <param name="DomainId">Domain ID </param>
        /// <param name="WebApiId">WebAPI ID</param>
        public void ManageReportSchedule(ServiceConfigInfo objSvcConfigInfo, string strReportTypeId, string strScheduleInterval, long DomainId, int WebApiId)
        {
            try
            {
                AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                string ReportType = GetReportType(strReportTypeId);
                string ScheduleInterval = strScheduleInterval;
                MemoryStream ms = new MemoryStream();
                objClient.ManagedReportSchedule(objSvcConfigInfo, ReportType, ScheduleInterval);
                objClient.Close();
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Amazon Service : " + er.msg;
                throw new Exception("Amazon:- From ManageReportSchedule : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets Report IDs List To Biz
        /// </summary>
        /// <param name="objSvcConfigInfo">Service Configuration Information</param>
        /// <param name="DomainId">Biz Domain ID for the Merchant</param>
        /// <param name="WebApiId">Web API ID</param>
        /// <param name="FromDate">FromDate</param>
        /// <param name="ToDate">ToDate</param>
        /// <param name="ReportTypeID">ReportTypeID</param>
        public void GetReportIDListToBiz(ServiceConfigInfo objSvcConfigInfo, long DomainId, int WebApiId, DateTime FromDate, DateTime ToDate, string ReportTypeID = "", long UserContactID = 0)
        {
            WebAPI objWebAPI = new WebAPI();
            string LogMessage = "";
            try
            {
                string ReportType = GetReportType(ReportTypeID);
                string[] arrReportIds;
                AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                arrReportIds = objClient.GetReportList(objSvcConfigInfo, FromDate, ToDate, ReportType);
                objClient.Close();

                if (arrReportIds.Length > 0)
                {
                    objWebAPI.DomainID = DomainId;
                    objWebAPI.WebApiId = WebApiId;
                    objWebAPI.WebApiOrdDetailId = 0;
                    string strReportIds = "";
                    LogMessage = "Amazon Order Report Ids ( ";
                    foreach (string ReportId in arrReportIds)
                    {
                        strReportIds += ReportId + ", ";
                        //Add Report IDs to DataBase
                        try
                        {
                            if (ReportTypeID == "ScheduledXMLOrderReport")
                            {
                                objWebAPI.WebApiReportTypeId = 1;
                            }
                            else if (ReportTypeID == "ActiveListing")
                            {

                                objWebAPI.WebApiReportTypeId = 2;
                            }
                            else if (ReportTypeID == "OpenListing1")
                            {
                                objWebAPI.WebApiReportTypeId = 3;
                            }
                            objWebAPI.API_OrderReportId = ReportId;
                            objWebAPI.API_ReportStatus = 0;
                            objWebAPI.RStatus = 0;
                            objWebAPI.ManageWebAPIOrderReports();
                        }
                        catch (Exception ex)
                        {
                            string ErrMessage = "Error occoured while adding Amazon Report Id : " + ReportId + " in BizAutomation.";
                            GeneralFunctions.WriteMessage(DomainId, "ERR", "Amazon:- From GetReportIDListToBiz : " + ErrMessage + " - " + ex.Message);
                        }
                    }
                    if (ReportTypeID == "ActiveListing" || ReportTypeID == "OpenListing1")
                    {
                        objWebAPI.UserContactID = UserContactID;
                        objWebAPI.FlagItemImport = "0|0";
                        objWebAPI.ManageWebApiItemImport();
                        LogMessage = "Amazon Product Listing Report Ids ( ";
                    }
                    LogMessage += strReportIds + ") added in BizAutomation.";
                    GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From GetReportIDListToBiz : " + LogMessage);
                }
                //Update WebAPIDetail vcNinthFldValue to Current UTC Time
                //objWebAPI.UpdateAPISettingsDate(1);
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Amazon Service : " + er.msg;
                throw new Exception("Amazon:- From GetReportIDListToBiz : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        /// <summary>
        /// Gets Acknowledged Report IDs List To Biz
        /// </summary>
        /// <param name="objSvcConfigInfo"></param>
        /// <param name="DomainId">Biz Domain ID for the Merchant</param>
        /// <param name="WebApiId">Web API ID</param>
        /// <param name="FromDate">FromDate</param>
        /// <param name="ToDate">ToDate</param>
        /// <param name="ReportTypeID">ReportTypeID</param>
        public void GetAcknowledgedReportListToBiz(ServiceConfigInfo objSvcConfigInfo, long DomainId, int WebApiId, DateTime FromDate, DateTime ToDate, string ReportTypeID = "")
        {
            WebAPI objWebAPI = new WebAPI();
            try
            {
                string ReportType = GetReportType(ReportTypeID);
                string[] arrReportIds;
                AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                arrReportIds = objClient.GetAcknowledgedReportList(objSvcConfigInfo, FromDate, ToDate, ReportType);
                objClient.Close();

                if (arrReportIds.Length > 0)
                {
                    foreach (string ReportId in arrReportIds)
                    {
                        //Add Report IDs to DataBase
                        objWebAPI.DomainID = DomainId;
                        objWebAPI.WebApiId = WebApiId;
                        objWebAPI.WebApiOrdDetailId = 0;
                        if (ReportTypeID == "ScheduledXMLOrderReport")
                        {
                            objWebAPI.WebApiReportTypeId = 1;
                        }
                        else if (ReportTypeID == "ActiveListing")
                        {
                            objWebAPI.WebApiReportTypeId = 2;
                        }
                        else if (ReportTypeID == "OpenListing1")
                        {
                            objWebAPI.WebApiReportTypeId = 3;
                        }
                        objWebAPI.API_OrderReportId = ReportId;
                        objWebAPI.API_ReportStatus = 0;
                        objWebAPI.RStatus = 0;
                        objWebAPI.ManageWebAPIOrderReports();
                    }
                }
                //Update WebAPIDetail vcNinthFldValue to Current UTC Time
                objWebAPI.UpdateAPISettingsDate(4);
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Amazon Service : " + er.msg;
                throw new Exception("Amazon:- From GetAcknowledgedReportListToBiz : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        /// <summary>
        /// Get the Report from Amazon For the specified Report Id
        /// </summary>
        /// <param name="objSvcConfigInfo">Amazon Service Configuration Information</param>
        /// <param name="ReportID">ReportID</param>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        public void GetReportByID(ServiceConfigInfo objSvcConfigInfo, string ReportID, int ReportTypeId, long DomainId, int WebApiId)
        {
            try
            {
                // string ReportType = "OrderReport";
                AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                MemoryStream ms = new MemoryStream();
                string FileName = "";
                ms = objClient.GetReportFromReportID(objSvcConfigInfo, ReportID);

                if (ReportTypeId == 1)
                {
                    FileName = GeneralFunctions.GetPath(OrderReportPath, DomainId) + ReportID + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".xml";
                }
                else if (ReportTypeId == 2)
                {
                    FileName = GeneralFunctions.GetPath("ProductListing", DomainId) + 1 + "_" + ReportID + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".txt";
                }
                else if (ReportTypeId == 3)
                {
                    FileName = GeneralFunctions.GetPath("ProductListing", DomainId) + 2 + "_" + ReportID + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".txt";
                }
                using (FileStream fs = File.Open(FileName, FileMode.Create, FileAccess.Write))
                {
                    ms.WriteTo(fs);
                    fs.Flush();
                    fs.Close();
                    ms.Dispose();
                }
                //List<string> lstReportIDs = new List<string>();
                //lstReportIDs.Add(ReportID);
                //objClient.UpdateReportAcknowledgement(objSvcConfigInfo, lstReportIDs.ToArray());
                objClient.Close();
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Amazon Service : " + er.msg;
                throw new Exception("Amazon:- From GetReportByID : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        /// <summary>
        /// Request for a Report
        /// </summary>
        /// <param name="objSvcConfigInfo"></param>
        /// <param name="DomainId"></param>
        /// <param name="WebApiId"></param>
        /// <param name="ReportType"></param>
        /// <param name="ReportOptions"></param>
        public void RequestReport(ServiceConfigInfo objSvcConfigInfo, long DomainId, int WebApiId, string ReportType, string ReportOptions = "")
        {
            try
            {
                string ReportRequestId = "";
                AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
                string strReportType = GetReportType(ReportType);
                ReportRequestId = objClient.RequestReport(objSvcConfigInfo, strReportType, ReportOptions);
                GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From RequestReport : " + "Request for Product Listing Report is send to Amazon MWS, Report Request Id : " + ReportRequestId);
                objClient.Close();
            }
            catch (FaultException<error> ex)
            {
                error er = ex.Detail;
                string ErrMessage = "Error received from Amazon Service : " + er.msg;
                throw new Exception("Amazon:- From RequestReport : " + ErrMessage);
                //GeneralFunctions.WriteMessage(DomainId, "ERR", ErrMessage);
                // GeneralFunctions.WriteMessage(DomainId, "ERR", er.request);
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        #endregion Report Operations

        #region Helper Functions

        /// <summary>
        /// Gets FeedType From FileName
        /// </summary>
        /// <param name="FileName">FileName</param>
        /// <returns>FeedType as string</returns>
        private static string GetFeedTypeFromFileName(string FileName)
        {
            string feedType = "";
            try
            {
                string feedForm = FileName.Substring(6, 3);
                switch (feedForm.ToUpper())
                {
                    case "PRO":
                        feedType = "_POST_PRODUCT_DATA_";
                        break;

                    case "INV":
                        feedType = "_POST_INVENTORY_AVAILABILITY_DATA_";
                        break;

                    case "PRI":
                        feedType = "_POST_PRODUCT_PRICING_DATA_";
                        break;

                    case "IMA":
                        feedType = "_POST_PRODUCT_IMAGE_DATA_";
                        break;

                    case "REL":
                        feedType = "_POST_PRODUCT_RELATIONSHIP_DATA_";
                        break;

                    case "OVE":
                        feedType = "_POST_PRODUCT_RELATIONSHIP_DATA_";
                        break;

                    case "FUL":
                        feedType = "_POST_ORDER_FULFILLMENT_DATA_";
                        break;

                    default:
                        feedType = "";
                        break;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return feedType;
        }

        /// <summary>
        /// Gets CurrencyCode With Default
        /// </summary>
        /// <param name="CurrCode" type="string">CurrencyCode</param>
        /// <returns type="BaseCurrencyCodeWithDefault">BaseCurrencyCodeWithDefault</returns>
        private BaseCurrencyCodeWithDefault GetCurrencyCodeWithDefault(string CurrCode)
        {
            BaseCurrencyCodeWithDefault CurrencyCode = new BaseCurrencyCodeWithDefault();
            try
            {
                switch (CurrCode)
                {
                    case "CAD":
                        CurrencyCode = BaseCurrencyCodeWithDefault.CAD;
                        break;

                    case "DEFAULT":
                        CurrencyCode = BaseCurrencyCodeWithDefault.DEFAULT;
                        break;

                    case "EUR":
                        CurrencyCode = BaseCurrencyCodeWithDefault.EUR;
                        break;

                    case "GBP":
                        CurrencyCode = BaseCurrencyCodeWithDefault.GBP;
                        break;

                    case "JPY":
                        CurrencyCode = BaseCurrencyCodeWithDefault.JPY;
                        break;

                    case "USD":
                        CurrencyCode = BaseCurrencyCodeWithDefault.USD;
                        break;

                    default:
                        CurrencyCode = BaseCurrencyCodeWithDefault.USD;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return CurrencyCode;
        }

        /// <summary>
        /// Gets Amazon WeightUnitOfMeasure value from its String Value
        /// </summary>
        /// <param name="strWeightUOM">strWeightUOM</param>
        /// <returns>WeightUnitOfMeasure</returns>
        private WeightUnitOfMeasure GetWeightUOM(string strWeightUOM)
        {
            WeightUnitOfMeasure WeightUOM = new WeightUnitOfMeasure();
            try
            {
                switch (strWeightUOM.ToUpper())
                {
                    case "GR":
                        WeightUOM = WeightUnitOfMeasure.GR;
                        break;

                    case "KG":
                        WeightUOM = WeightUnitOfMeasure.KG;
                        break;

                    case "LB":
                        WeightUOM = WeightUnitOfMeasure.LB;
                        break;

                    case "MG":
                        WeightUOM = WeightUnitOfMeasure.MG;
                        break;

                    case "OZ":
                        WeightUOM = WeightUnitOfMeasure.OZ;
                        break;

                    default:
                        WeightUOM = WeightUnitOfMeasure.OZ;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return WeightUOM;
        }


        /// <summary>
        /// Gets Amazon LengthUnitOfMeasure value from its String Value
        /// </summary>
        /// <param name="strLengthUOM">strLengthUOM</param>
        /// <returns>LengthUnitOfMeasure</returns>
        private LengthUnitOfMeasure GetLengthUOM(string strLengthUOM)
        {
            LengthUnitOfMeasure LengthUOM = new LengthUnitOfMeasure();
            try
            {
                switch (strLengthUOM.ToUpper())
                {
                    case "centimeters":
                        LengthUOM = LengthUnitOfMeasure.centimeters;
                        break;

                    case "CM":
                        LengthUOM = LengthUnitOfMeasure.CM;
                        break;

                    case "decimeters":
                        LengthUOM = LengthUnitOfMeasure.decimeters;
                        break;

                    case "feet":
                        LengthUOM = LengthUnitOfMeasure.feet;
                        break;

                    case "FT":
                        LengthUOM = LengthUnitOfMeasure.FT;
                        break;

                    case "IN":
                        LengthUOM = LengthUnitOfMeasure.IN;
                        break;

                    case "inches":
                        LengthUOM = LengthUnitOfMeasure.inches;
                        break;

                    case "M":
                        LengthUOM = LengthUnitOfMeasure.M;
                        break;

                    case "meters":
                        LengthUOM = LengthUnitOfMeasure.meters;
                        break;

                    case "micrometers":
                        LengthUOM = LengthUnitOfMeasure.micrometers;
                        break;

                    case "millimeters":
                        LengthUOM = LengthUnitOfMeasure.millimeters;
                        break;

                    case "MM":
                        LengthUOM = LengthUnitOfMeasure.MM;
                        break;

                    case "nanometers":
                        LengthUOM = LengthUnitOfMeasure.nanometers;
                        break;

                    case "picometers":
                        LengthUOM = LengthUnitOfMeasure.picometers;
                        break;

                    default:
                        LengthUOM = LengthUnitOfMeasure.IN;
                        break;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return LengthUOM;
        }

        /// <summary>
        /// Get Amazon Base Currency Code
        /// </summary>
        /// <param name="CurrCode" type="string">CurrencyCode</param>
        /// <returns type="BaseCurrencyCode">BaseCurrencyCode</returns>
        private BaseCurrencyCode GetCurrencyCode(string CurrCode)
        {
            BaseCurrencyCode CurrencyCode = new BaseCurrencyCode();
            try
            {
                switch (CurrCode)
                {
                    case "CAD":
                        CurrencyCode = BaseCurrencyCode.CAD;
                        break;

                    case "CNY":
                        CurrencyCode = BaseCurrencyCode.CNY;
                        break;

                    case "EUR":
                        CurrencyCode = BaseCurrencyCode.EUR;
                        break;

                    case "GBP":
                        CurrencyCode = BaseCurrencyCode.GBP;
                        break;

                    case "JPY":
                        CurrencyCode = BaseCurrencyCode.JPY;
                        break;

                    case "USD":
                        CurrencyCode = BaseCurrencyCode.USD;
                        break;

                    default:
                        CurrencyCode = BaseCurrencyCode.USD;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return CurrencyCode;
        }

        /// <summary>
        /// Gets Standard ProductID Type
        /// </summary>
        /// <param name="Type" type="string">ProductID Type</param>
        /// <returns type="StandardProductIDType">ProductID Type</returns>
        private StandardProductIDType GetProductIdType(string Type)
        {
            StandardProductIDType PIdType = new StandardProductIDType();
            try
            {
                switch (Type)
                {
                    case "ASIN":
                        PIdType = StandardProductIDType.ASIN;
                        break;

                    case "EAN":
                        PIdType = StandardProductIDType.EAN;
                        break;

                    case "GTIN":
                        PIdType = StandardProductIDType.GTIN;
                        break;

                    case "ISBN":
                        PIdType = StandardProductIDType.ISBN;
                        break;

                    case "UPC":
                        PIdType = StandardProductIDType.UPC;
                        break;

                    default:
                        PIdType = StandardProductIDType.UPC;
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return PIdType;
        }

        /// <summary>
        /// Generates a Unique New File Name from Merchant ID and Feed Type
        /// </summary>
        /// <param name="MerchantID">Merchant ID</param>
        /// <param name="FeedType">Feed Type</param>
        /// <returns>Unique New File Name</returns>
        private string GenerateFileName(string MerchantID, string FeedType)
        {
            try
            {
                string filename = "";
                filename = MerchantID.Substring(0, 5) + "_" + FeedType.Substring(0, 3) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff");
                return filename;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Get Report Type
        /// </summary>
        /// <param name="Type">Report Type representation</param>
        /// <returns>Amazon Report Type</returns>
        private string GetReportType(string Type)
        {
            string strReportType = "";

            switch (Type)
            {
                case "OpenListing1":
                    strReportType = "_GET_FLAT_FILE_OPEN_LISTINGS_DATA_";
                    break;

                case "OpenListing2":
                    strReportType = "_GET_MERCHANT_LISTINGS_DATA_BACK_COMPAT_";
                    break;

                case "ActiveListing":
                    strReportType = "_GET_MERCHANT_LISTINGS_DATA_";
                    break;

                case "OpenListingsLite":
                    strReportType = "_GET_MERCHANT_LISTINGS_DATA_LITE_";
                    break;

                case "OpenListingsLiter":
                    strReportType = "_GET_MERCHANT_LISTINGS_DATA_LITER_";
                    break;

                case "CanceledListings":
                    strReportType = "_GET_MERCHANT_CANCELLED_LISTINGS_DATA_";
                    break;

                case "QualityListing":
                    strReportType = "_GET_MERCHANT_LISTINGS_DEFECT_DATA_";
                    break;

                case "UnshippedOrders":
                    strReportType = "_GET_FLAT_FILE_ACTIONABLE_ORDER_DATA_";
                    break;

                case "ScheduledXMLOrderReport":
                    strReportType = "_GET_ORDERS_DATA_";
                    break;

                case "FlatFileOrder":
                    strReportType = "_GET_FLAT_FILE_ORDER_REPORT_DATA_";
                    break;

                case "RequestedorScheduledFlatFileOrder":
                    strReportType = "_GET_FLAT_FILE_ORDERS_DATA_";
                    break;

                case "ConvergedFlatFileOrder":
                    strReportType = "_GET_CONVERGED_FLAT_FILE_ORDER_REPORT_DATA_";
                    break;

                case "FFOrdersByLastUpdate":
                    strReportType = "_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_LAST_UPDATE_";
                    break;

                case "FFOrdersByOrderDate":
                    strReportType = "_GET_FLAT_FILE_ALL_ORDERS_DATA_BY_ORDER_DATE_";
                    break;

                case "XMLOrdersByLastUpdate":
                    strReportType = "_GET_XML_ALL_ORDERS_DATA_BY_LAST_UPDATE_";
                    break;

                case "XMLOrdersByOrderDate":
                    strReportType = "_GET_XML_ALL_ORDERS_DATA_BY_ORDER_DATE_";
                    break;

                default:
                    strReportType = "";
                    break;

            }
            return strReportType;
        }

        /// <summary>
        /// Update Api Oppurtunities Item Details
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="UserCntId">UserCntId</param>
        /// <param name="OppId">Opp Id</param>
        /// <param name="OppBizDocId">Opp Biz Doc Id</param>
        /// <param name="AmazonOrderId">Amazon Order Id</param>
        /// <param name="objOrderItems">Order Items Collection</param>
        private void UpdateApiOppItemDetails(long DomainId, int WebApiId, long UserCntId, long OppId, string AmazonOrderId, OrderReportItem[] objOrderItems)
        {
            DataTable dtOppItems;
            MOpportunity objOpportunity = new MOpportunity();
            objOpportunity.Mode = 5;
            objOpportunity.DomainID = DomainId;
            objOpportunity.OpportunityId = OppId;

            WebAPI objWebApi = new WebAPI();
            try
            {
                dtOppItems = objOpportunity.GetOrderItems().Tables[0];

                foreach (DataRow dr in dtOppItems.Rows)
                {
                    foreach (OrderReportItem orderItem in objOrderItems)
                    {
                        if (orderItem.SKU == CCommon.ToString(dr["vcSKU"]))
                        {
                            GeneralFunctions.WriteMessage(DomainId, "LOG", "Amazon:- From UpdateApiOppItemDetails : " + "orderItem.SKU==CCommon.ToString(dr[vcSKU]):" + orderItem.SKU + "==" + CCommon.ToString(dr["vcSKU"]));
                            objWebApi.DomainID = DomainId;
                            objWebApi.WebApiId = WebApiId;
                            objWebApi.OppId = OppId;
                            objWebApi.OppItemId = CCommon.ToLong(dr["numOppItemtCode"]);
                            objWebApi.vcAPIOppId = AmazonOrderId;
                            objWebApi.vcApiOppItemId = orderItem.AmazonOrderItemCode;
                            objWebApi.RStatus = 0;
                            objWebApi.RecordOwner = UserCntId;
                            objWebApi.ManageApiOrderItems();
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Update Api Oppurtunities Item Details
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <param name="UserCntId">UserCntId</param>
        /// <param name="OppId">Opp Id</param>
        /// <param name="OppBizDocId">Opp Biz Doc Id</param>
        /// <param name="AmazonOrderId">Amazon Order Id</param>
        /// <param name="objOrderItems">Order Items Collection</param>
        private void UpdateFBAOppItemDetails(long DomainId, int WebApiId, long UserCntId, long OppId, long OppBizDocId, string AmazonOrderId, AMWS_Communicator.CItems[] objOrderItems)
        {
            DataTable dtOppItems;
            MOpportunity objOpportunity = new MOpportunity();
            objOpportunity.Mode = 5;
            objOpportunity.DomainID = DomainId;
            objOpportunity.OpportunityId = OppId;

            WebAPI objWebApi = new WebAPI();
            try
            {
                dtOppItems = objOpportunity.GetOrderItems().Tables[0];

                foreach (DataRow dr in dtOppItems.Rows)
                {
                    foreach (AMWS_Communicator.CItems orderItem in objOrderItems)
                    {
                        if (orderItem.SellerSKU == CCommon.ToString(dr["vcSKU"]))
                        {
                            objWebApi.DomainID = DomainId;
                            objWebApi.WebApiId = WebApiId;
                            objWebApi.OppId = OppId;
                            objWebApi.OppItemId = CCommon.ToLong(dr["numOppItemtCode"]);
                            objWebApi.vcAPIOppId = AmazonOrderId;
                            objWebApi.vcApiOppItemId = orderItem.OrderItemId;
                            objWebApi.RStatus = 1;
                            objWebApi.RecordOwner = UserCntId;
                            objWebApi.ManageApiOrderItems();
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Gets the Amazon MWS Feed Service URL for Country Code
        /// </summary>
        /// <param name="CountryCode">Country Code</param>
        /// <returns>Feed Service URL</returns>
        public string GetFeedsServiceURL(string CountryCode)
        {
            string ServiceURL = "";

            switch (CountryCode.ToUpper())
            {
                case "US": // United States
                    ServiceURL = "https://mws.amazonservices.com";
                    break;

                case "UK":// United Kingdom:
                    ServiceURL = "https://mws.amazonservices.co.uk";
                    break;

                case "JP":// Japan
                    ServiceURL = "https://mws.amazonservices.jp";
                    break;

                case "IT": //Italy
                    ServiceURL = "https://mws.amazonservices.it";
                    break;

                //case "IN":
                //    ServiceURL = "https://mws.amazonservices.in";
                //    break;

                case "FR": // France:
                    ServiceURL = "https://mws.amazonservices.fr";
                    break;

                //case "ES":
                //    ServiceURL = "https://mws.amazonservices.es";
                //    break;

                case "DE":  // Germany
                    ServiceURL = "https://mws.amazonservices.de";
                    break;

                case "CN": //China
                    ServiceURL = "https://mws.amazonservices.com.cn";
                    break;

                case "CA":  // Canada
                    ServiceURL = "https://mws.amazonservices.ca";
                    break;


                default: // United States
                    ServiceURL = "https://mws.amazonservices.com";
                    break;

            }

            return ServiceURL;
        }

        public CarrierCode GetShippingCarrierCode(string BizCarrierCode)
        {
            CarrierCode AmazonCarrierCode;

            switch (BizCarrierCode.ToUpper())
            {
                case "UPS":
                    AmazonCarrierCode = CarrierCode.UPS;
                    break;

                case "USPS":
                    AmazonCarrierCode = CarrierCode.USPS;
                    break;

                case "FEDEX":
                    AmazonCarrierCode = CarrierCode.FedEx;
                    break;

                case "DHL":
                    AmazonCarrierCode = CarrierCode.DHL;
                    break;

                case "BLUEPACKAGE":
                    AmazonCarrierCode = CarrierCode.BluePackage;
                    break;

                case "DPD":
                    AmazonCarrierCode = CarrierCode.DPD;
                    break;

                case "FASTWAY":
                    AmazonCarrierCode = CarrierCode.Fastway;
                    break;

                case "FEDEXSMARTPOST":
                    AmazonCarrierCode = CarrierCode.FedExSmartPost;
                    break;

                case "GLS":
                    AmazonCarrierCode = CarrierCode.GLS;
                    break;

                case "GO":
                    AmazonCarrierCode = CarrierCode.GO;
                    break;

                case "OSM":
                    AmazonCarrierCode = CarrierCode.OSM;
                    break;

                case "SDA":
                    AmazonCarrierCode = CarrierCode.SDA;
                    break;

                case "SMARTMAIL":
                    AmazonCarrierCode = CarrierCode.Smartmail;
                    break;

                case "STREAMLITE":
                    AmazonCarrierCode = CarrierCode.Streamlite;
                    break;

                case "TARGET":
                    AmazonCarrierCode = CarrierCode.Target;
                    break;

                case "TNT":
                    AmazonCarrierCode = CarrierCode.TNT;
                    break;

                case "UPSMAILINNOVATIONS":
                    AmazonCarrierCode = CarrierCode.UPSMailInnovations;
                    break;

                case "YAMATOTRANSPORT":
                    AmazonCarrierCode = CarrierCode.YamatoTransport;
                    break;

                default:
                    AmazonCarrierCode = CarrierCode.FedEx;
                    break;
            }
            return AmazonCarrierCode;
        }
        /// <summary>
        /// Gets the Amazon MWS Order Service URL for Country Code
        /// </summary>
        /// <param name="CountryCode">Country Code</param>
        /// <returns>Order Service URL</returns>
        public string GetOrdersServiceURL(string CountryCode)
        {
            string ServiceURL = "";

            switch (CountryCode.ToUpper())
            {
                case "US": // United States
                    ServiceURL = "https://mws.amazonservices.com/Orders/2013-09-01";
                    break;

                case "UK":// United Kingdom:
                    ServiceURL = "https://mws.amazonservices.co.uk/Orders/2013-09-01";
                    break;

                case "JP":// Japan
                    ServiceURL = "https://mws.amazonservices.jp/Orders/2013-09-01";
                    break;

                case "IT": //Italy
                    ServiceURL = "https://mws.amazonservices.it/Orders/2013-09-01";
                    break;

                //case "IN":
                //    ServiceURL = "https://mws.amazonservices.in/Orders/2013-09-01";
                //    break;

                case "FR": // France:
                    ServiceURL = "https://mws.amazonservices.fr/Orders/2013-09-01";
                    break;

                //case "ES":
                //    ServiceURL = "https://mws.amazonservices.es/Orders/2013-09-01";
                //    break;

                case "DE":  // Germany
                    ServiceURL = "https://mws.amazonservices.de/Orders/2013-09-01";
                    break;

                case "CN": //China
                    ServiceURL = "https://mws.amazonservices.com.cn/Orders/2013-09-01";
                    break;

                case "CA":  // Canada
                    ServiceURL = "https://mws.amazonservices.ca/Orders/2013-09-01";
                    break;


                default: // United States
                    ServiceURL = "https://mws.amazonservices.com/Orders/2013-09-01";
                    break;

            }

            return ServiceURL;
        }

        /// <summary>
        /// Gets the Amazon MWS Products Service URL for Country Code
        /// </summary>
        /// <param name="CountryCode">Country Code</param>
        /// <returns>Products Service URL</returns>
        public string GetProductsServiceURL(string CountryCode)
        {
            string ServiceURL = "";

            switch (CountryCode.ToUpper())
            {
                case "US": // United States
                    ServiceURL = "https://mws.amazonservices.com/Products/2011-10-01";
                    break;

                case "UK":// United Kingdom:
                    ServiceURL = "https://mws.amazonservices.co.uk/Products/2011-10-01";
                    break;

                case "JP":// Japan
                    ServiceURL = "https://mws.amazonservices.jp/Products/2011-10-01";
                    break;

                case "IT": //Italy
                    ServiceURL = "https://mws.amazonservices.it/Products/2011-10-01";
                    break;

                //case "IN":
                //    ServiceURL = "https://mws.amazonservices.in/Products/2011-10-01";
                //    break;

                case "FR": // France:
                    ServiceURL = "https://mws.amazonservices.fr/Products/2011-10-01";
                    break;

                //case "ES":
                //    ServiceURL = "https://mws.amazonservices.es/Products/2011-10-01";
                //    break;

                case "DE":  // Germany
                    ServiceURL = "https://mws.amazonservices.de/Products/2011-10-01";
                    break;

                case "CN": //China
                    ServiceURL = "https://mws.amazonservices.com.cn/Products/2011-10-01";
                    break;

                case "CA":  // Canada
                    ServiceURL = "https://mws.amazonservices.ca/Products/2011-10-01";
                    break;


                default: // United States
                    ServiceURL = "https://mws.amazonservices.com/Products/2011-10-01";
                    break;

            }
            return ServiceURL;
        }

        #endregion Helper Functions

        #region Remove after Testing

        //public long AddNewItemToBiz(DataRow drItem, long DomainId, int WebApiId, long RecordOwner, long ItemClassificationID)
        //{
        //    long ItemCode = 0;
        //    try
        //    {
        //        BACRM.BusinessLogic.Item.CItems objItem = new BACRM.BusinessLogic.Item.CItems();
        //        objItem.ItemCode = 0;
        //        objItem.ItemName = CCommon.ToString(drItem["vcItemName"]);
        //        objItem.ItemDesc = CCommon.ToString(drItem["txtItemDesc"]);
        //        objItem.ItemType = "P";
        //        objItem.ListPrice = CCommon.ToDouble(drItem["monListPrice"]);
        //        objItem.ItemClassification = ItemClassificationID;
        //        objItem.Taxable = false;
        //        objItem.SKU = CCommon.ToString(drItem["vcSKU"]);
        //        objItem.KitParent = false;
        //        objItem.DomainID = DomainId;
        //        objItem.UserCntID = RecordOwner;
        //        objItem.bitSerialized = false;
        //        objItem.VendorID = 0;
        //        objItem.ModelID = CCommon.ToString(drItem["vcModelID"]);
        //        objItem.ItemGroupID = 0;
        //        objItem.WebApiId = WebApiId;
        //        objItem.COGSChartAcntId = CCommon.ToInteger(drItem["numCOGSChartAcntId"]);
        //        objItem.AssetChartAcntId = CCommon.ToInteger(drItem["numAssetChartAcntId"]);
        //        objItem.IncomeChartAcntId = CCommon.ToInteger(drItem["numIncomeChartAcntId"]);
        //        objItem.ApiItemID = CCommon.ToString(drItem["vcApiItemId"]);
        //        objItem.Quantity = CCommon.ToLong(drItem["numQuantity"]);
        //        objItem.Height = CCommon.ToDouble(drItem["fltShippingHeight"]);
        //        objItem.Length = CCommon.ToDouble(drItem["fltShippingLength"]);
        //        objItem.Width = CCommon.ToDouble(drItem["fltShippingWidth"]);
        //        objItem.Weight = CCommon.ToDouble(drItem["fltShippingWeight"]);
        //        objItem.ListOfAPI = CCommon.ToString(drItem["vcExportToAPI"]);
        //        objItem.ManageItemsAndKits();
        //        objItem.str = CCommon.ToString(drItem["txtItemHtmlDesc"]);
        //        objItem.byteMode = 1;
        //        objItem.ManageItemExtendedDesc();
        //        ItemCode = objItem.ItemCode;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return ItemCode;
        //}


        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="DomainId"></param>
        ///// <param name="WebApiId"></param>
        ///// <param name="SellerSKU"></param>
        //public AmazonProduct GetProductDetailsForSKU(long DomainId, int WebApiId, string SellerSKU)
        //{
        //    AmazonProduct objAmazonProduct = new AmazonProduct();

        //    WebAPI objWebAPi = new WebAPI();
        //    objWebAPi.DomainID = DomainId;
        //    objWebAPi.WebApiId = WebApiId;
        //    objWebAPi.Mode = 3;
        //    DataTable dtWebAPI = objWebAPi.GetWebApi();
        //    foreach (DataRow dr in dtWebAPI.Rows)
        //    {
        //        if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
        //        {
        //            if (WebApiId == CCommon.ToInteger(dr["WebApiId"]))//Amazon US
        //            {
        //                ServiceConfigInfo objSvcConfigInfo = new ServiceConfigInfo();
        //                objSvcConfigInfo.MerchantID = CCommon.ToString(dr["vcFirstFldValue"]);
        //                objSvcConfigInfo.MarketPlaceID = CCommon.ToString(dr["vcSecondFldValue"]);
        //                objSvcConfigInfo.AccessKeyID = AmazonMWSAccessKey; //CCommon.ToString(dr["vcThirdFldValue"]);
        //                objSvcConfigInfo.SecretKeyValue = AmazonMWSSecretKey; //CCommon.ToString(dr["vcFourthFldValue"]);
        //                objSvcConfigInfo.FeedsServiceURL = GetFeedsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
        //                objSvcConfigInfo.OrdersServiceURL = GetOrdersServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
        //                objSvcConfigInfo.ProductServiceURL = GetProductsServiceURL(CCommon.ToString(dr["vcFifthFldValue"]));
        //                objSvcConfigInfo.ServiceURL_Type = 3;
        //                AmazonCommunicatorClient objClient = new AmazonCommunicatorClient();
        //                objAmazonProduct = objClient.GetProductDetailsForSellerSKU(objSvcConfigInfo, SellerSKU);

        //                //objClient.Get(objSvcConfigInfo, SKU.ToArray());
        //            }
        //        }
        //    }
        //    return objAmazonProduct;
        //}

        #endregion  Remove after Testing

        #region Test Bulk Order Import

        public void TestBulkOrderImport(OrderReport objOrdReport, long DomainID, int WebApiId, string Source,
       int numWareHouseID, long numRecordOwner, long numAssignTo, long numRelationshipId, long numProfileId, long numBizDocId, long BizDocStatusId, int numOrderStatus, int ExpenseAccountId, long DiscountItemMapping, long ShippingServiceItemID, long SalesTaxItemMappingID, string MarketplaceOrderReportId, string ShipToPhoneNo)
        {
            string LogMessage = "";
            decimal SalesTaxRate = 0;
            CContacts objContacts = null;
            CLeads objLeads = null;
            ImportWizard objImpWzd = new ImportWizard();

            CCommon objCommon = new CCommon();
            //foreach (AmazonEnvelopeMessage Message in objAmazonEnvelope.Message)
            //{
            MOpportunity objOpportunity = new MOpportunity();
            OrderReportBillingData billData = new OrderReportBillingData();
            OrderReportFulfillmentData OrderFulFillData = new OrderReportFulfillmentData();
            OrderReportCustomerOrderInfo[] CustomerOrderInfo;
            OrderReportItem[] OrderItem = null;
            // OrderReport objOrdReport = new OrderReport();
            WebAPI objWebApi = new WebAPI();

            string ItemMessage = "";
            decimal ShipCost = 0;
            decimal OrderTotal = 0;
            long CurrencyID = 0;
            decimal dTotDiscount = 0;
            decimal SalesTaxAmount = 0;
            try
            {
                // objOrdReport = (OrderReport)Message.Item;

                if (objOrdReport.BillingData != null)
                    billData = objOrdReport.BillingData;

                if (objOrdReport.FulfillmentData != null)
                    OrderFulFillData = objOrdReport.FulfillmentData;

                if (objOrdReport.CustomerOrderInfo != null)
                    CustomerOrderInfo = objOrdReport.CustomerOrderInfo;

                if (objOrdReport.Item != null)
                    OrderItem = objOrdReport.Item;

                if (!string.IsNullOrEmpty(objOrdReport.AmazonOrderID))
                {
                    int a = -1;
                    string AddressStreet = "";//To concatenate Address Lines
                    objContacts = new CContacts();
                    objLeads = new CLeads();
                    objLeads.DomainID = DomainID;

                    if (billData.BuyerEmailAddress != null && billData.BuyerEmailAddress != "")//If contains Email Address
                    {
                        objLeads.Email = billData.BuyerEmailAddress;
                        objLeads.GetConIDCompIDDivIDFromEmail(); //Check for Details for the Given DomainID and EmailID
                    }
                    else
                    {
                        objLeads.Email = "";
                    }
                    if (objLeads.ContactID > 0 && objLeads.DivisionID > 0)
                    {
                        //If Email already registered
                        lngCntID = objLeads.ContactID;
                        lngDivId = objLeads.DivisionID;
                    }
                    else
                    {
                        if (billData.BuyerName != null && billData.BuyerName != "")
                        {
                            objLeads.CompanyName = billData.BuyerName;
                            objLeads.CustName = billData.BuyerName;
                        }
                        else if (OrderFulFillData.Address != null)
                        {
                            AddressType shippingAddress = OrderFulFillData.Address;

                            if (shippingAddress.Name != null && shippingAddress.Name != "")
                            {
                                objLeads.CompanyName = shippingAddress.Name;
                                objLeads.CustName = shippingAddress.Name;
                            }
                        }

                        if (OrderFulFillData.Address != null)
                        {
                            AddressType shippingAddress = OrderFulFillData.Address;
                            if (shippingAddress.County != null && shippingAddress.County != "")
                            {
                                //objLeads.Country = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                                //objLeads.SCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                            }
                            if (shippingAddress.CountryCode != null && shippingAddress.CountryCode != "")
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 22;
                                objCommon.Str = shippingAddress.CountryCode;
                                objLeads.Country = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objLeads.SCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (shippingAddress.Name != null && shippingAddress.Name != "")
                            {
                                a = shippingAddress.Name.IndexOf('.');
                                if (a == -1)
                                    a = shippingAddress.Name.IndexOf(' ');
                                if (a == -1)
                                {
                                    objLeads.FirstName = shippingAddress.Name;
                                    objLeads.LastName = shippingAddress.Name;
                                }
                                else
                                {
                                    objLeads.FirstName = shippingAddress.Name.Substring(0, a);
                                    objLeads.LastName = shippingAddress.Name.Substring(a + 1, shippingAddress.Name.Length - (a + 1));
                                }
                            }
                            if (shippingAddress.PhoneNumber != null)
                            {
                                PhoneNumberType[] objPhoneType;
                                objPhoneType = shippingAddress.PhoneNumber;
                                //objLeads.ContactPhone = CCommon.ToString(shippingAddress.PhoneNumber);
                                if (shippingAddress.PhoneNumber.Length > 0)
                                {
                                    objLeads.ContactPhone = CCommon.ToString(shippingAddress.PhoneNumber[0].Value);
                                }
                                else
                                {
                                    objLeads.ContactPhone = ShipToPhoneNo;
                                }
                                objLeads.PhoneExt = "";
                            }
                            else
                            {
                                objLeads.ContactPhone = ShipToPhoneNo;
                                objLeads.PhoneExt = "";
                            }
                        }

                        objLeads.UserCntID = numRecordOwner;
                        objLeads.ContactType = 70;
                        objLeads.PrimaryContact = true;
                        objLeads.UpdateDefaultTax = false;
                        objLeads.NoTax = false;
                        objLeads.CRMType = 1;
                        objLeads.DivisionName = "-";

                        objLeads.CompanyType = numRelationshipId;
                        objLeads.Profile = numProfileId;

                        objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();//Creates Company Record
                        LogMessage = "New Company details added CompanyID : " + objLeads.CompanyID + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        lngDivId = objLeads.CreateRecordDivisionsInfo();
                        LogMessage = "New Divisions Informations added. Division Id : " + lngDivId + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        objLeads.ContactID = 0;
                        objLeads.DivisionID = lngDivId;
                        lngCntID = objLeads.CreateRecordAddContactInfo();//Creates Contact Info
                        LogMessage = "New Contact Informations added. Contact Id : " + lngCntID + " for CompanyID : " + objLeads.CompanyID + ", DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                        GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                        if (OrderFulFillData.Address != null)
                        {
                            objContacts.FirstName = objLeads.FirstName;
                            objContacts.LastName = objLeads.LastName;
                            objContacts.ContactPhone = objLeads.ContactPhone;
                            objContacts.Email = objLeads.Email;

                            AddressType shippingAddress = OrderFulFillData.Address;

                            if (shippingAddress.AddressFieldOne != null && shippingAddress.AddressFieldOne != "")
                                AddressStreet = shippingAddress.AddressFieldOne;

                            if (shippingAddress.AddressFieldTwo != null && shippingAddress.AddressFieldTwo != "")
                                AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldTwo;

                            if (shippingAddress.AddressFieldThree != null && shippingAddress.AddressFieldThree != "")
                                AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldThree;

                            if (shippingAddress.City != null && shippingAddress.City != "")
                                AddressStreet = AddressStreet + " " + shippingAddress.City;

                            objContacts.BillStreet = AddressStreet;
                            objContacts.ShipStreet = AddressStreet;

                            if (shippingAddress.City != null && shippingAddress.City != "")
                            {
                                objContacts.BillCity = shippingAddress.City;
                                objContacts.ShipCity = shippingAddress.City;
                            }

                            if (shippingAddress.StateOrRegion != null && shippingAddress.StateOrRegion != "")
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 21;
                                objCommon.Str = shippingAddress.StateOrRegion;
                                objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (shippingAddress.CountryCode != null && shippingAddress.CountryCode != "")
                            {
                                objCommon.DomainID = DomainID;
                                objCommon.Mode = 22;
                                objCommon.Str = shippingAddress.CountryCode;
                                objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                                objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            }

                            if (shippingAddress.PostalCode != null && shippingAddress.PostalCode != "")
                            {
                                objContacts.BillPostal = shippingAddress.PostalCode;
                                objContacts.ShipPostal = shippingAddress.PostalCode;
                            }

                            objContacts.BillingAddress = 0;
                            objContacts.DivisionID = lngDivId;
                            objContacts.ContactID = lngCntID;
                            objContacts.RecordID = lngDivId;
                            objContacts.DomainID = DomainID;
                            objContacts.IsPrimaryAddress = CCommon.ToBool(1);
                            objContacts.UpdateCompanyAddress();
                        }
                    }

                    objOpportunity.OppRefOrderNo = objOrdReport.AmazonOrderID;
                    objOpportunity.MarketplaceOrderID = objOrdReport.AmazonOrderID;
                    objOpportunity.MarketplaceOrderReportId = MarketplaceOrderReportId;
                    BizCommonFunctions.CreateItemTable(dtOrderItems);
                    ItemMessage = TestBulkOrder_AddOrderItemDataset(DomainID, WebApiId, numRecordOwner, numWareHouseID, DiscountItemMapping, OrderItem, CCommon.ToString(objOrdReport.FulfillmentData.FulfillmentServiceLevel), ShippingServiceItemID, SalesTaxItemMappingID, out CurrencyID, out OrderTotal, out ShipCost, out dTotDiscount, out SalesTaxRate, out SalesTaxAmount);

                    objOpportunity.CurrencyID = CurrencyID;
                    objOpportunity.Amount = OrderTotal;
                    objContacts.BillingAddress = 0;
                    objContacts.DivisionID = lngDivId;
                    //objContacts.UpdateCompanyAddress();//Updates Company Details
                    // SaveTaxTypes();
                    BizCommonFunctions.SaveTaxTypes(ds, lngDivId);
                    arrBillingIDs = new string[4];
                    arrBillingIDs[0] = CCommon.ToString(objLeads.CompanyID);
                    arrBillingIDs[1] = CCommon.ToString(lngDivId);
                    arrBillingIDs[2] = CCommon.ToString(lngCntID);
                    arrBillingIDs[3] = objLeads.CompanyName;
                    if (lngCntID == 0)
                        return;

                    //Create Opportunity Details
                    DateTime OrderDate = objOrdReport.OrderDate;
                    objOpportunity.MarketplaceOrderDate = OrderDate;
                    objOpportunity.OpportunityId = 0;
                    objOpportunity.ContactID = lngCntID;
                    objOpportunity.DivisionID = lngDivId;
                    objOpportunity.UserCntID = numRecordOwner;
                    objOpportunity.AssignedTo = numAssignTo;

                    objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
                    objOpportunity.EstimatedCloseDate = DateTime.Now;
                    objOpportunity.PublicFlag = 0;
                    objOpportunity.DomainID = DomainID;
                    objOpportunity.OppType = 1;
                    objOpportunity.OrderStatus = numOrderStatus;

                    dsOrderItems.Tables.Clear();
                    dsOrderItems.Tables.Add(dtOrderItems.Copy());
                    if (dsOrderItems.Tables[0].Rows.Count != 0)
                        objOpportunity.strItems = ((dtOrderItems.Rows.Count > 0) ? ("<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsOrderItems.GetXml()) : "");
                    else
                        objOpportunity.strItems = "";
                    objOpportunity.OppComments = ItemMessage;
                    objOpportunity.Source = WebApiId;
                    objOpportunity.SourceType = 3;
                    objOpportunity.TaxOperator = 2; //Remove by default setting of adding sales tax while it comes through marketplace itself
                    objOpportunity.DealStatus = (long)MOpportunity.EnmDealStatus.DealWon;
                    arrOutPut = objOpportunity.Save();
                    lngOppId = CCommon.ToLong(arrOutPut[0]);

                    //Save Source 
                    objOpportunity.vcSource = Source;
                    objOpportunity.WebApiId = WebApiId;
                    objOpportunity.OpportunityId = lngOppId;
                    objOpportunity.ManageOppLinking();

                    objOpportunity.CompanyID = lngDivId;

                    if (OrderFulFillData.Address != null)
                    {
                        AddressType shippingAddress = OrderFulFillData.Address;
                        if (!string.IsNullOrEmpty(shippingAddress.Name))
                        {
                            objOpportunity.BillCompanyName = shippingAddress.Name;
                        }

                        if (shippingAddress.AddressFieldOne != null && shippingAddress.AddressFieldOne != "")
                        {
                            AddressStreet = shippingAddress.AddressFieldOne;
                        }
                        if (shippingAddress.AddressFieldTwo != null && shippingAddress.AddressFieldTwo != "")
                        {
                            AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldTwo;
                        }
                        if (shippingAddress.AddressFieldThree != null && shippingAddress.AddressFieldThree != "")
                        {
                            AddressStreet = AddressStreet + " " + shippingAddress.AddressFieldThree;
                        }
                        if (shippingAddress.City != null && shippingAddress.City != "")
                        {
                            AddressStreet = AddressStreet + " " + shippingAddress.City;
                        }

                        objContacts.BillStreet = AddressStreet;
                        objContacts.ShipStreet = AddressStreet;
                        objOpportunity.BillStreet = AddressStreet;

                        if (shippingAddress.City != null && shippingAddress.City != "")
                        {
                            objContacts.BillCity = shippingAddress.City;
                            objContacts.ShipCity = shippingAddress.City;
                            objOpportunity.BillCity = shippingAddress.City;
                        }
                        if (shippingAddress.StateOrRegion != null && shippingAddress.StateOrRegion != "")
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 21;
                            objCommon.Str = shippingAddress.StateOrRegion;
                            objContacts.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.ShipState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillState = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (shippingAddress.County != null && shippingAddress.County != "")
                        {
                            //objContacts.BillCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                            //objContacts.ShipCountry = objImpWzd.GetStateAndCountry(18, shippingAddress.County, DomainID, 40);
                        }
                        if (shippingAddress.CountryCode != null && shippingAddress.CountryCode != "")
                        {
                            objCommon.DomainID = DomainID;
                            objCommon.Mode = 22;
                            objCommon.Str = shippingAddress.CountryCode;
                            objContacts.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objContacts.ShipCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                            objOpportunity.BillCountry = CCommon.ToInteger(objCommon.GetSingleFieldValue());
                        }
                        if (shippingAddress.PostalCode != null && shippingAddress.PostalCode != "")
                        {
                            objContacts.BillPostal = shippingAddress.PostalCode;
                            objContacts.ShipPostal = shippingAddress.PostalCode;
                            objOpportunity.BillPostal = shippingAddress.PostalCode;
                        }
                        objOpportunity.Mode = 0;
                        objOpportunity.UpdateOpportunityAddress();
                        objOpportunity.Mode = 1;
                        objOpportunity.UpdateOpportunityAddress();
                    }

                    LogMessage = "Order Details updated for API OrderID : " + objOrdReport.AmazonOrderID + " Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);

                    //if (SalesTaxRate > 0)
                    //{
                    //    CCommon objCommon1 = new CCommon();
                    //    objCommon1.Mode = 35;
                    //    objCommon1.UpdateRecordID = lngOppId;
                    //    objCommon1.Comments = CCommon.ToString(Math.Round(SalesTaxRate, 4));
                    //    objCommon1.DomainID = DomainID;
                    //    objCommon1.UpdateSingleFieldValue();
                    //    LogMessage = "Tax Percentage updated to " + CCommon.ToString(Math.Round(SalesTaxRate, 4)) + " for API OrderID : " + objOrdReport.AmazonOrderID + " Biz Opportunity Id : " + lngOppId + " for DomainID : " + DomainID + " and WebApiID : " + WebApiId;
                    //    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                    //}

                    objWebApi.WebApiId = WebApiId;
                    objWebApi.DomainID = DomainID;
                    objWebApi.UserCntID = numRecordOwner;
                    objWebApi.OppId = lngOppId;
                    objWebApi.vcAPIOppId = objOrdReport.AmazonOrderID;
                    objWebApi.AddAPIopportunity();

                    //insert amazon orderid into mapping table
                    objWebApi.API_OrderId = objOrdReport.AmazonOrderID;
                    objWebApi.API_OrderStatus = 1;
                    objWebApi.WebApiOrdDetailId = 1;
                    objWebApi.ManageAPIOrderDetails();



                    //Insert mapping between Biz OrderItems and Amazon OrderItems 
                    UpdateApiOppItemDetails(DomainID, WebApiId, numRecordOwner, lngOppId, objOpportunity.OppRefOrderNo, OrderItem);
                    GeneralFunctions.WriteMessage(DomainID, "LOG", "OppID:" + lngOppId + ",AmazonOrderID:" + objOrdReport.AmazonOrderID + ", Insert mapping between Biz OrderItems and Amazon OrderItems,Message:" + ItemMessage + ", OrderTotal:" + OrderTotal);


                    decimal OrderAmount = OrderTotal; // + ShipCost - dTotDiscount + SalesTaxAmount;

                    //string Reference = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM"); 
                    string Reference = arrOutPut[1];
                    if (ItemMessage == "")
                    {
                        if (OrderAmount >= 0)
                        {
                            bool IsAuthoritative = false;
                            BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                            BizCommonFunctions.CreateOppBizDoc(objOppBizDocs, numRecordOwner, DomainID, lngOppId, objOpportunity.OppRefOrderNo, numBizDocId, BizDocStatusId, lngDivId, OrderDate, ShipCost, dTotDiscount, out OppBizDocID, out IsAuthoritative, ExpenseAccountId, SalesTaxRate, OrderAmount, Reference);

                        }
                        else
                        {
                            GeneralFunctions.WriteMessage(DomainID, "LOG", "Order Total Amount (cannot be a negative amount. Conflict Occoured while processing Amazon Order Id : " + objOrdReport.AmazonOrderID);
                        }
                    }
                    else
                    {
                        GeneralFunctions.WriteMessage(DomainID, "LOG", ItemMessage);
                    }
                    //}
                    //else
                    //{
                    //    string ErrMessage = "Order report doesnot have sufficient Buyer details to process the Amazon Order : " + objOrdReport.AmazonOrderID;
                    //    GeneralFunctions.WriteMessage(DomainID, "LOG", ErrMessage);
                    //}
                }
                else
                {
                    string ErrMessage = "Order details for Amazon Order Id : " + objOrdReport.AmazonOrderID + " is already exists in BizAutomation";
                    GeneralFunctions.WriteMessage(DomainID, "LOG", ErrMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string TestBulkOrder_AddOrderItemDataset(long DomainId, int WebApiId, long RecordOwner, int WareHouseId, long DiscountItemMapping, OrderReportItem[] objList, string ShippingServiceSelected,
         long ShippingServiceItemID, long SalesTaxItemMappingID, out long CurrencyID, out decimal OrderTotal,
         out decimal ShipCost, out decimal dTotDiscount, out decimal SalesTaxRate, out decimal SalesTaxAmount)
        {
            dtOrderItems.Clear();
            dsOrderItems.Clear();
            CCommon objCommon = new CCommon();
            string Message = "";
            DataRow drItem;
            int ItemCount = 0;
            string ItemType = "";
            string[] itemCodeType;
            string itemCode = "";
            WebAPI objWebApi = new WebAPI();
            objWebApi.DomainID = DomainId;
            objWebApi.WebApiId = WebApiId;
            dTotDiscount = 0;
            decimal decDiscount = 0;
            string DiscountDesc = "";
            decimal Tax0 = 0;
            OrderTotal = 0;
            ShipCost = 0;
            CurrencyID = 0;
            SalesTaxRate = 0;
            SalesTaxAmount = 0;
            DataTable dtDiscountItems = new DataTable();
            dtDiscountItems.Columns.Clear();
            dtDiscountItems.Columns.Add("DiscountName");
            dtDiscountItems.Columns.Add("DiscountAmount");
            dtDiscountItems.Columns.Add("TaxAmount");
            DataRow drDiscountItem;
            decimal SaleAmount1 = 0;

            foreach (OrderReportItem orderItem in objList)
            {
                BuyerPriceComponent[] ItemPriceComponents;
                AmazonFeesFee[] AmazonFees;
                ItemCount += 1;
                try
                {
                    if (orderItem.SKU != "" & orderItem.SKU != null)
                    {
                        //objCommon.DomainID = DomainId;
                        //objCommon.Mode = 20;
                        //objCommon.Str = orderItem.SKU;
                        //itemCode = CCommon.ToString(objCommon.GetSingleFieldValue());

                        objCommon.DomainID = DomainId;
                        objCommon.Mode = 38;
                        objCommon.Str = orderItem.SKU;
                        itemCodeType = CCommon.ToString(objCommon.GetSingleFieldValue()).Split('~');
                        if (itemCodeType.Length > 1)
                        {
                            if (!string.IsNullOrEmpty(itemCodeType[0]))
                            {
                                itemCode = itemCodeType[0];
                            }
                            if (!string.IsNullOrEmpty(itemCodeType[1]))
                            {
                                ItemType = BizCommonFunctions.GetItemTypeNameByCharType(itemCodeType[1]);
                            }
                        }

                        if (itemCode == "")
                        {
                            DataTable dtProductDetails = new DataTable();
                            BizCommonFunctions.CreateProductDetailsTable(dtProductDetails);
                            DataRow dr = dtProductDetails.NewRow();

                            DataTable dtCopy = new DataTable();
                            dr = GetItemDetailsForSKU(DomainId, WebApiId, RecordOwner, orderItem.SKU, dr, ref dtCopy);
                            string ItemClassificationName = "Amazon_OrderItem_Import_" + DateTime.Now.ToString("ddMMMyyyy");
                            long ItemClassificationID = 0;
                            ItemClassificationID = BizCommonFunctions.GetItemClassificationID(DomainId, RecordOwner, ItemClassificationName);
                            itemCode = CCommon.ToString(BizCommonFunctions.AddNewItemToBiz(dr, DomainId, WebApiId, RecordOwner, WareHouseId, ItemClassificationID));
                            ItemType = BizCommonFunctions.GetItemTypeNameByCharType("P");
                            WebAPIItemDetail objWebAPIItemDetail = new WebAPIItemDetail();
                            string FilePath = CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml";
                            if (System.IO.File.Exists(FilePath))
                            {
                                System.IO.StreamReader objStreamReader = new System.IO.StreamReader(FilePath);
                                XmlSerializer x = new XmlSerializer(objWebAPIItemDetail.GetType());
                                objWebAPIItemDetail = (WebAPIItemDetail)x.Deserialize(objStreamReader);
                                objStreamReader.Close();
                            }
                            objWebAPIItemDetail.DimensionUOM = BizCommonFunctions.GetLengthUOMCode(CCommon.ToString(dr["DimensionUOM"]));
                            objWebAPIItemDetail.WeightUOM = BizCommonFunctions.GetWeightUOMCode(CCommon.ToString(dr["WeightUOM"]));
                            objWebAPIItemDetail.Length = CCommon.ToString(dr["fltLength"]);
                            objWebAPIItemDetail.Height = CCommon.ToString(dr["fltHeight"]);
                            objWebAPIItemDetail.Width = CCommon.ToString(dr["fltWidth"]);
                            objWebAPIItemDetail.Weight = CCommon.ToString(dr["fltWeight"]);
                            objWebAPIItemDetail.WarrantyDescription = CCommon.ToString(dr["Warranty"]);

                            StreamWriter objStreamWriter = new StreamWriter(CCommon.GetDocumentPhysicalPath(DomainId) + "WebAPI_Item_" + CCommon.ToString(DomainId) + "_" + CCommon.ToString(itemCode) + ".xml");
                            XmlSerializer Serializer = new XmlSerializer(objWebAPIItemDetail.GetType());
                            Serializer.Serialize(objStreamWriter, objWebAPIItemDetail);
                            objStreamWriter.Close();

                        }
                        if (itemCode != "")
                        {
                            decDiscount = 0;
                            DiscountDesc = "";
                            drItem = dtOrderItems.NewRow();
                            drItem["numoppitemtCode"] = ItemCount;
                            drItem["numItemCode"] = itemCode;

                            if (CCommon.ToInteger(orderItem.Quantity) != 0)
                            {
                                drItem["numUnitHour"] = orderItem.Quantity;
                                if (orderItem.ItemPrice != null)
                                {
                                    ItemPriceComponents = orderItem.ItemPrice;

                                    foreach (BuyerPriceComponent Component in ItemPriceComponents)
                                    {
                                        if (Component.Type == BuyerPriceComponentType.Principal)
                                        {
                                            drItem["monPrice"] = CCommon.ToDecimal(Component.Amount.Value) / CCommon.ToInteger(orderItem.Quantity);
                                            //SaleAmount1 = CCommon.ToDecimal(Component.Amount.Value) / CCommon.ToInteger(orderItem.Quantity); ;
                                            OrderTotal += CCommon.ToDecimal(Component.Amount.Value);
                                            objCommon.DomainID = DomainId;
                                            objCommon.Mode = 15;
                                            objCommon.Str = CCommon.ToString(Component.Amount.currency);
                                            CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                                            //drItem["monTotAmount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                            //drItem["monTotAmtBefDiscount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                                            drItem["monTotAmount"] = Component.Amount.Value;
                                        }
                                        //if (Component.Type == BuyerPriceComponentType.COD)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.CODFee)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.CODTax)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.ExportCharge)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.GiftWrap)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.GiftWrapTax)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.Goodwill)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.Other)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.RestockingFee)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.RestockingFeeTax)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.ReturnShipping)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        if (Component.Type == BuyerPriceComponentType.Shipping)
                                            ShipCost += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.ShippingTax)
                                        //    Tax0 += CCommon.ToDecimal(Component.Amount.Value);

                                        //if (Component.Type == BuyerPriceComponentType.Surcharge)
                                        //    fltDiscount += CCommon.ToDecimal(Component.Amount.Value);

                                        if (Component.Type == BuyerPriceComponentType.Tax)
                                        {
                                            Tax0 += CCommon.ToDecimal(Component.Amount.Value);
                                            if (orderItem.ItemTaxData != null)
                                            {
                                                if (orderItem.ItemTaxData.TaxableAmounts != null)
                                                {
                                                    decimal TaxableAmt = CCommon.ToDecimal(orderItem.ItemTaxData.TaxableAmounts.State.Value);
                                                    if (CCommon.ToDecimal(TaxableAmt) > 0)
                                                    {
                                                        SalesTaxRate = (CCommon.ToDecimal(Component.Amount.Value) * 100) / TaxableAmt;
                                                        SalesTaxAmount += CCommon.ToDecimal(Component.Amount.Value);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (orderItem.ItemFees != null)
                                {
                                    AmazonFees = orderItem.ItemFees;
                                    foreach (AmazonFeesFee FeeComponent in AmazonFees)
                                    {
                                        if (FeeComponent.Type == "Commission")
                                        {
                                            decDiscount += CCommon.ToDecimal(FeeComponent.Amount.Value);
                                            DiscountDesc += FeeComponent.Type + " fee : " + CCommon.ToString(FeeComponent.Amount.Value) + Environment.NewLine;
                                            if (CCommon.ToDecimal(FeeComponent.Amount.Value) != 0)
                                            {
                                                drDiscountItem = dtDiscountItems.NewRow();
                                                drDiscountItem["DiscountName"] = FeeComponent.Type + " fee for Item-SKU - " + orderItem.SKU;
                                                drDiscountItem["DiscountAmount"] = CCommon.ToString(FeeComponent.Amount.Value);
                                                drDiscountItem["TaxAmount"] = 0;
                                                dtDiscountItems.Rows.Add(drDiscountItem);
                                            }
                                        }
                                        if (FeeComponent.Type == "Variableclosingfee")
                                        {
                                            decDiscount += CCommon.ToDecimal(FeeComponent.Amount.Value);
                                            DiscountDesc += FeeComponent.Type + " fee : " + CCommon.ToString(FeeComponent.Amount.Value) + "; " + Environment.NewLine;
                                            if (CCommon.ToDecimal(FeeComponent.Amount.Value) != 0)
                                            {
                                                drDiscountItem = dtDiscountItems.NewRow();
                                                drDiscountItem["DiscountName"] = FeeComponent.Type + " fee for Item-SKU - " + orderItem.SKU;
                                                drDiscountItem["DiscountAmount"] = CCommon.ToString(FeeComponent.Amount.Value);
                                                drDiscountItem["TaxAmount"] = 0;
                                                // drDiscountItem["DiscountItemSKU"] = "";
                                                dtDiscountItems.Rows.Add(drDiscountItem);
                                            }
                                        }
                                    }

                                }
                                if (orderItem.Promotion != null)
                                {
                                    foreach (TaxablePromotionType promo in orderItem.Promotion)
                                    {
                                        if (promo.Component != null)
                                        {
                                            foreach (TaxablePromotionTypeComponent promoComponent in promo.Component)
                                            {
                                                PromotionApplicationType CompType = promoComponent.Type;
                                                string ComponentName = CCommon.ToString(CompType);
                                                if (promoComponent.Amount != null)
                                                {
                                                    CurrencyAmount promoAmount = new CurrencyAmount();
                                                    promoAmount = promoComponent.Amount;
                                                    if (CCommon.ToDecimal(promoAmount.Value) != 0)
                                                    {
                                                        decimal dTaxDiscount = SalesTaxRate * CCommon.ToDecimal(promoAmount.Value) / 100;
                                                        drDiscountItem = dtDiscountItems.NewRow();
                                                        drDiscountItem["DiscountName"] = ComponentName + " Promotion discount for Item-SKU - " + orderItem.SKU;
                                                        drDiscountItem["DiscountAmount"] = CCommon.ToString(CCommon.ToDecimal(promoAmount.Value));
                                                        drDiscountItem["TaxAmount"] = dTaxDiscount;
                                                        dtDiscountItems.Rows.Add(drDiscountItem);
                                                        decDiscount += promoAmount.Value;
                                                        DiscountDesc += "Promotion discount for Item-SKU - " + orderItem.SKU + " : " + CCommon.ToString(promoAmount.Value) + "; ";

                                                    }
                                                }
                                            }
                                        }
                                        // TaxablePromotionTypeComponent promoComponent =  promo.Component[0];
                                    }
                                }
                                //drItem["monPrice"] = SaleAmount1 - decimal.Negate(decDiscount);
                                //drItem["monTotAmount"] = SaleAmount1;
                                drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]);
                            }

                            drItem["numUOM"] = 0; // get it from item table, in procedure
                            drItem["vcUOMName"] = "";// get it from item table, in procedure
                            drItem["UOMConversionFactor"] = 1.0000;
                            drItem["vcItemDesc"] = DiscountDesc;// get it from item table, in procedure
                            drItem["vcModelID"] = "";// get it from item table, in procedure
                            drItem["numWarehouseID"] = WareHouseId;
                            if (orderItem.Title != null && orderItem.Title != "")
                            {
                                drItem["vcItemName"] = orderItem.Title;
                            }
                            drItem["Warehouse"] = "";
                            drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                            drItem["ItemType"] = ItemType;// get it from item table, in procedure
                            drItem["Attributes"] = "";
                            drItem["Op_Flag"] = 1;
                            drItem["bitWorkOrder"] = false;
                            drItem["DropShip"] = false;
                            //drItem["fltDiscount"] = decimal.Negate(decDiscount);
                            drItem["fltDiscount"] = 0;
                            dTotDiscount += decimal.Negate(decDiscount);
                            //if (fltDiscount > 0)
                            drItem["bitDiscountType"] = 1; //Flat discount
                            //else
                            //    drItem["bitDiscountType"] = false; // Percentage

                            //if (orderItem.ItemTax != null)
                            //{
                            //    CMoney ItemTax = orderItem.ItemTax;
                            //    Tax0 = CCommon.ToDecimal(ItemTax.Amount);
                            //}
                            Tax0 = 0;
                            drItem["Tax0"] = Tax0;
                            if (Tax0 > 0)
                                drItem["bitTaxable0"] = true;
                            else
                                drItem["bitTaxable0"] = false;

                            //drItem["Tax0"] = 0;
                            drItem["numVendorWareHouse"] = 0;
                            drItem["numShipmentMethod"] = 0;
                            drItem["numSOVendorId"] = 0;
                            drItem["numProjectID"] = 0;
                            drItem["numProjectStageID"] = 0;
                            drItem["charItemType"] = ""; // should be taken from procedure
                            drItem["numToWarehouseItemID"] = 0;
                            //drItem["Tax41"] = 0;
                            //drItem["bitTaxable41"] = false;
                            //drItem["Tax42"] = 0;
                            //drItem["bitTaxable42"] = false;
                            drItem["Weight"] = ""; // should take from procedure 
                            drItem["WebApiId"] = WebApiId;
                            drItem["vcSourceShipMethod"] = ShippingServiceSelected;
                            drItem["vcSKU"] = orderItem.SKU;
                            dtOrderItems.Rows.Add(drItem);
                        }
                        else
                        {
                            Message += "Item " + orderItem.Title + " (SKU: " + orderItem.SKU + ") not found in Amazon-Biz linking database or unable to get Item detail inorder Add in BizDatabase" + Environment.NewLine;
                        }
                    }
                }
                catch (Exception ex)
                {
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.Message);
                    GeneralFunctions.WriteMessage(DomainId, "ERR", ex.StackTrace);
                    throw ex;
                }
            }
            if (SalesTaxAmount > 0)
            {
                OrderTotal += SalesTaxAmount;
                drItem = dtOrderItems.NewRow();
                drItem = BizCommonFunctions.GetSalesTaxItem(drItem, SalesTaxAmount, SalesTaxItemMappingID, DomainId, WebApiId);
                dtOrderItems.Rows.Add(drItem);
            }
            if (ShipCost > 0)
            {
                OrderTotal += ShipCost;
                drItem = dtOrderItems.NewRow();
                drItem = BizCommonFunctions.GetShippingCostItem(drItem, ShipCost, ShippingServiceItemID, DomainId, WebApiId);
                dtOrderItems.Rows.Add(drItem);
            }
            if (dTotDiscount > 0)
            {
                OrderTotal -= dTotDiscount;
            }

            if (dtDiscountItems.Rows.Count > 0)
            {
                foreach (DataRow drDiscount in dtDiscountItems.Rows)
                {
                    drItem = dtOrderItems.NewRow();
                    string DiscountItemName = CCommon.ToString(drDiscount["DiscountName"]);
                    decimal dDiscountItemValue = CCommon.ToDecimal(drDiscount["DiscountAmount"]);
                    decimal dTaxAmount = CCommon.ToDecimal(drDiscount["TaxAmount"]);
                    drItem = BizCommonFunctions.GetDiscountOrderItem(drItem, dDiscountItemValue, DiscountItemMapping, DomainId, WebApiId, DiscountItemName, dTaxAmount);
                    dtOrderItems.Rows.Add(drItem);
                }
            }
            return Message;
        }

        #endregion Test Bulk Order Import
    }
}
