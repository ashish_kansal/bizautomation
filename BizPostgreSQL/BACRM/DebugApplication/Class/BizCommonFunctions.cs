﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Accounting;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Contacts;

using System.Data;
using System.Xml.Serialization;
using System.IO;
using BACRM.BusinessLogic.WebAPI;

namespace DebugApplication
{
    class BizCommonFunctions
    {
        static DataTable dtCompanyTaxTypes = new DataTable();
        static DataRow drtax = null;
        static string strdetails = "";
        static long JournalId;

        /// <summary>
        /// GetRoutingRulesUserCntID
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="dbFieldName">dbFieldName</param>
        /// <param name="FieldValue">FieldValue</param>
        /// <returns></returns>
        public static long GetRoutingRulesUserCntID(long DomainId, string dbFieldName, string FieldValue)
        {
            AutoRoutingRules objAutoRoutRles = new AutoRoutingRules();
            DataTable dtAutoRoutingRules = new DataTable();
            DataRow drAutoRow = null;
            DataSet ds = new DataSet();
            try
            {
                objAutoRoutRles.DomainID = DomainId;
                if (dtAutoRoutingRules.Columns.Count == 0)
                {
                    dtAutoRoutingRules.TableName = "Table";
                    dtAutoRoutingRules.Columns.Add("vcDbColumnName");
                    dtAutoRoutingRules.Columns.Add("vcDbColumnText");
                }
                dtAutoRoutingRules.Rows.Clear();
                drAutoRow = dtAutoRoutingRules.NewRow();
                drAutoRow["vcDbColumnName"] = dbFieldName;
                drAutoRow["vcDbColumnText"] = FieldValue;
                dtAutoRoutingRules.Rows.Add(drAutoRow);
                ds.Tables.Add(dtAutoRoutingRules);
                objAutoRoutRles.strValues = ds.GetXml();
                ds.Tables.Remove(ds.Tables[0]);
                return objAutoRoutRles.GetRecordOwner();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// SaveDataToHeader
        /// </summary>
        /// <param name="GrandTotal"></param>
        /// <param name="_DomainID">Merchant's Biz Domain Id</param>
        /// <param name="_UserCntID">Default Record Owner's Id</param>
        /// <param name="OppBizDocId">Opportunity BizDocument Id</param>
        /// <param name="_OppId">Opportunity Id</param>
        /// <param name="lngJournalId">Journal Id</param>
        /// <returns></returns>
        public static long SaveDataToHeader(decimal GrandTotal, long _DomainID, long _UserCntID, long OppBizDocId, long _OppId, DateTime OrderDate, long lngDepositeID)
        {
            try
            {
                JournalEntryHeader objJEHeader = new JournalEntryHeader();
                {
                    objJEHeader.JournalId = 0;
                    objJEHeader.RecurringId = 0;
                    objJEHeader.EntryDate = Convert.ToDateTime((OrderDate == null ? System.DateTime.UtcNow : OrderDate));
                    objJEHeader.Description = "";
                    objJEHeader.Amount = GrandTotal;
                    objJEHeader.CheckId = 0;
                    objJEHeader.CashCreditCardId = 0;
                    objJEHeader.ChartAcntId = 0;
                    objJEHeader.OppId = _OppId;
                    objJEHeader.OppBizDocsId = OppBizDocId;
                    objJEHeader.DepositId = lngDepositeID;
                    objJEHeader.BizDocsPaymentDetId = 0;
                    objJEHeader.IsOpeningBalance = false;
                    objJEHeader.LastRecurringDate = System.DateTime.Now;
                    objJEHeader.NoTransactions = 0;
                    objJEHeader.CategoryHDRID = 0;
                    objJEHeader.ReturnID = 0;
                    objJEHeader.CheckHeaderID = 0;
                    objJEHeader.BillID = 0;
                    objJEHeader.BillPaymentID = 0;
                    objJEHeader.UserCntID = _UserCntID;
                    objJEHeader.DomainID = _DomainID;
                }
                long lngJournalId = objJEHeader.Save();
                return lngJournalId;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Creates the Item table structure
        /// </summary>
        public static DataTable CreateItemTable(DataTable dtItems)
        {
            try
            {
                if (dtItems.Columns.Count == 0)
                {
                    dtItems.Columns.Add("numoppitemtCode");
                    dtItems.Columns.Add("numItemCode");
                    dtItems.Columns.Add("numUnitHour");
                    dtItems.Columns.Add("monPrice");
                    dtItems.Columns.Add("numUOM");
                    dtItems.Columns.Add("vcUOMName");
                    dtItems.Columns.Add("UOMConversionFactor");
                    dtItems.Columns.Add("monTotAmount", typeof(double));
                    dtItems.Columns.Add("vcItemDesc");
                    dtItems.Columns.Add("vcModelID");
                    dtItems.Columns.Add("numWarehouseID");
                    dtItems.Columns.Add("vcItemName");
                    dtItems.Columns.Add("Warehouse");
                    dtItems.Columns.Add("numWarehouseItmsID");
                    dtItems.Columns.Add("ItemType");
                    dtItems.Columns.Add("Attributes");
                    dtItems.Columns.Add("Op_Flag");
                    dtItems.Columns.Add("bitWorkOrder", typeof(bool));
                    dtItems.Columns.Add("DropShip", typeof(bool));
                    dtItems.Columns.Add("bitDiscountType", typeof(bool));
                    dtItems.Columns.Add("fltDiscount");
                    dtItems.Columns.Add("monTotAmtBefDiscount");
                    dtItems.Columns.Add("bitTaxable0", typeof(bool));
                    dtItems.Columns.Add("Tax0");
                    dtItems.Columns.Add("numVendorWareHouse");
                    dtItems.Columns.Add("numShipmentMethod");
                    dtItems.Columns.Add("numSOVendorId");
                    dtItems.Columns.Add("numProjectID");
                    dtItems.Columns.Add("numProjectStageID");
                    dtItems.Columns.Add("charItemType");
                    dtItems.Columns.Add("bitIsAuthBizDoc");
                    dtItems.Columns.Add("numToWarehouseItemID");
                    dtItems.Columns.Add("Tax41");
                    dtItems.Columns.Add("bitTaxable41", typeof(bool));
                    dtItems.Columns.Add("Tax42");
                    dtItems.Columns.Add("bitTaxable42", typeof(bool));

                    dtItems.Columns.Add("Weight");
                    dtItems.Columns.Add("WebApiId", typeof(int));
                    dtItems.Columns.Add("vcSourceShipMethod");
                    dtItems.Columns.Add("vcSKU");
                    dtItems.TableName = "Item";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtItems;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void SaveTaxTypes(DataSet ds, long lngDivId)
        {
            CProspects objProspects = new CProspects();
            try
            {
                if (dtCompanyTaxTypes.Columns.Count == 0)
                {
                    dtCompanyTaxTypes.Columns.Add("numTaxItemID");
                    dtCompanyTaxTypes.Columns.Add("bitApplicable");
                }
                drtax = dtCompanyTaxTypes.NewRow();
                drtax["numTaxItemID"] = 0;
                drtax["bitApplicable"] = 1;
                dtCompanyTaxTypes.Rows.Add(drtax);
                dtCompanyTaxTypes.TableName = "Table";
                ds.Tables.Add(dtCompanyTaxTypes);
                strdetails = ds.GetXml();
                ds.Tables.Remove(ds.Tables[0]);
                dtCompanyTaxTypes.Rows.Clear();
                objProspects.DivisionID = lngDivId;
                objProspects.strCompanyTaxTypes = strdetails;
                objProspects.ManageCompanyTaxTypes();
                ds.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// Creates BizDocument for the Amazon Order
        /// </summary>
        /// <param name="objOppBizDocs">objOppBizDocs</param>
        /// <param name="_UserCntID">Default Record Owner's Id</param>
        /// <param name="_DomainID">Merchant's Biz Domain Id</param>
        /// <param name="OppId">Opportunity Id</param>
        /// <param name="BizDocId">Default BizDocument Id</param>
        /// <param name="ShipCost">Shipping Cost</param>
        /// <param name="Discount">Total Discount</param>
        /// <param type="Out Parameter" name="OppBizDocID">Opportunity BizDocument Id</param>
        /// <param name="ExpenseAccountId">Default Expense Account Id</param>
        public static void CreateOppBizDoc(OppBizDocs objOppBizDocs, long UserCntID, long DomainID, long OppId, string RefOrderNo, long BizDocId, long BizDocStatusId,
           long lngDivId, DateTime OrderDate, decimal ShipCost, decimal Discount, out long OppBizDocID, out bool IsAuthoritative, int ExpenseAccountId, decimal SalesTaxRate, decimal OrderAmount, string Reference)
        {
            DataSet ds = new DataSet();
            DataTable dtOppBiDocItems;
            JournalEntry objJournalEntries = new JournalEntry();
            int i = 0;
            long lngBizDoc = 0;
            OppBizDocID = 0;
            long lintAuthorizativeBizDocsId = 0;
            IsAuthoritative = false;
            try
            {

                if (BizDocId > 0)
                {
                    objOppBizDocs.OppId = OppId;
                    objOppBizDocs.DomainID = DomainID;

                    objOppBizDocs.bitPartialShipment = true;
                    objOppBizDocs.byteMode = 3;
                    objOppBizDocs.SalesTaxRate = 0;
                    objOppBizDocs.OppType = 1;
                    objOppBizDocs.UserCntID = UserCntID;
                    objOppBizDocs.vcPONo = "";
                    objOppBizDocs.vcComments = "";

                    if (objOppBizDocs.ValidateCustomerAR_APAccounts("AR", DomainID, lngDivId) == 0)
                    {

                    }

                    objOppBizDocs.BizDocStatus = BizDocStatusId;
                    objOppBizDocs.FromDate = OrderDate;
                    TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);
                    objOppBizDocs.ClientTimeZoneOffset = CCommon.ToInteger(diff1.TotalMinutes);

                    //objOppBizDocs.ShipCost = ShipCost;// (Information.IsNumeric(OrderNode("shipping").InnerXml) ? OrderNode("shipping").InnerXml : 0);
                    //objOppBizDocs.Discount = Discount;
                    //objOppBizDocs.boolDiscType = true; //Flat Amount

                    lngBizDoc = BizDocId;//Pass from Configuration   //objOppBizDocs.GetAuthorizativeOpportuntiy();
                    if (lngBizDoc == 0)
                        return;
                    objOppBizDocs.BizDocId = lngBizDoc;
                    objOppBizDocs.OppBizDocId = 0;

                    CCommon objCommon = new CCommon();
                    objCommon.DomainID = DomainID;
                    objCommon.Mode = 33;
                    objCommon.Str = CCommon.ToString(BizDocId);
                    objOppBizDocs.SequenceId = CCommon.ToString(objCommon.GetSingleFieldValue());

                    // objCommon = new CCommon();
                    //objCommon.DomainID = DomainID;
                    //objCommon.Mode = 34;
                    //objCommon.Str = CCommon.ToString(OppId);
                    //objOppBizDocs.RefOrderNo = RefOrderNo;

                    OppBizDocID = objOppBizDocs.SaveBizDoc();

                    if (objOppBizDocs.BizDocStatus > 0)
                    {
                        objOppBizDocs.tintMode = 0;
                        objOppBizDocs.OpportunityBizDocStatusChange();
                    }


                    lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy();

                    if (lintAuthorizativeBizDocsId == BizDocId)
                    {
                        IsAuthoritative = true;

                        objOppBizDocs.OppBizDocId = OppBizDocID;
                        ds.Tables.Clear();
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting();
                        dtOppBiDocItems = ds.Tables[0];

                        CalculateDealAmount objCalculateDealAmount = new CalculateDealAmount();
                        objCalculateDealAmount.CalculateDealAmount(OppId, OppBizDocID, 1, DomainID, dtOppBiDocItems);
                        JournalId = SaveDataToHeader(objCalculateDealAmount.GrandTotal, DomainID, UserCntID, OppBizDocID, OppId, OrderDate, 0);

                        objJournalEntries.SaveJournalEntriesSales(OppId, DomainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, objCalculateDealAmount.DivisionID,
                            CCommon.ToLong(ds.Tables[1].Rows[0]["numCurrencyID"]), CCommon.ToDouble(ds.Tables[1].Rows[0]["fltExchangeRate"]), DiscAcntType: ExpenseAccountId);

                        ds.Tables.Clear();
                    }
                }

                if (OrderAmount > 0)
                    MakeDepositEntry(DomainID, UserCntID, OppId, IsAuthoritative, OppBizDocID, lngDivId, OrderAmount, OrderDate, Reference);
                else
                    GeneralFunctions.WriteMessage(DomainID, "LOG", "Order Amount " + OrderAmount + " for Opportunity ID " + OppId + " is not valid to Deposit entry");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Makes Deposit entry for the Order Amount
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="UserContactID">UserContactID</param>
        /// <param name="OppID">OppID</param>
        /// <param name="DivisionId">DivisionId</param>
        /// <param name="Amount">Amount</param>
        /// <param name="OrderDate">OrderDate</param>
        /// <param name="Reference">Reference</param>
        public static void MakeDepositEntry(long DomainId, long UserContactID, long OppID, bool IsAuthoritative, long OppBizDocID, long DivisionId, decimal Amount, DateTime OrderDate, string Reference, int PaymentMethod = 4)
        {
            try
            {
                long UndepositedFundAccountID = ChartOfAccounting.GetDefaultAccount("UF", DomainId);// Undeposited Fund
                long lngDepositeID, lngJournalID;
                string Description = "";
                string strItems = "";
                if (IsAuthoritative)
                {
                    DataSet ds = new DataSet();
                    DataTable dt = new DataTable();
                    DataTable dtDeposit = new DataTable();
                    CCommon.AddColumnsToDataTable(ref dtDeposit, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid");
                    DataRow dr = dtDeposit.NewRow();
                    dr["numDepositeDetailID"] = 0;
                    dr["numOppBizDocsID"] = OppBizDocID;
                    dr["numOppID"] = OppID;
                    dr["monAmountPaid"] = Amount;
                    dtDeposit.Rows.Add(dr);
                    ds.Tables.Add(dtDeposit.Copy());
                    ds.Tables[0].TableName = "Item";
                    strItems = ds.GetXml();
                }
                else
                {
                    strItems = "";
                }

                MakeDeposit objMakeDeposit = new MakeDeposit();

                objMakeDeposit.DomainID = DomainId;
                objMakeDeposit.UserCntID = UserContactID;
                objMakeDeposit.DivisionId = CCommon.ToInteger(DivisionId);
                objMakeDeposit.Entry_Date = OrderDate;
                objMakeDeposit.Reference = Reference;
                objMakeDeposit.Memo = Reference;
                objMakeDeposit.PaymentMethod = PaymentMethod;
                objMakeDeposit.DepositeToType = 2;
                objMakeDeposit.numAmount = Amount;

                objMakeDeposit.AccountId = CCommon.ToInteger(UndepositedFundAccountID);

                objMakeDeposit.RecurringId = 0;
                objMakeDeposit.DepositId = 0;
                objMakeDeposit.UserCntID = UserContactID;
                objMakeDeposit.DomainID = DomainId;
                objMakeDeposit.StrItems = strItems;
                objMakeDeposit.Mode = 0;
                objMakeDeposit.DepositePage = 2;
                //.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                //   .ExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                lngDepositeID = objMakeDeposit.SaveDataToMakeDepositDetails();
                lngJournalID = SaveDataToHeader(Amount, DomainId, UserContactID, 0, 0, OrderDate, lngDepositeID);
                SaveDataToGeneralJournalDetailsForCashAndChecks(DomainId, lngDepositeID, lngJournalID, Amount, DivisionId, UndepositedFundAccountID, PaymentMethod);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        ///// <summary>
        ///// Saves Data Header
        ///// </summary>
        ///// <param name="DomainID">DomainID</param>
        ///// <param name="UserContactID">UserContactID</param>
        ///// <param name="OrderDate">OrderDate</param>
        ///// <param name="Description">Description</param>
        ///// <param name="Amount">Amount</param>
        ///// <param name="OppID">OppID</param>
        ///// <param name="UndepositedFundAccountID">UndepositedFundAccountID</param>
        ///// <param name="lngDepositeID">lngDepositeID</param>
        ///// <returns></returns>
        //private long SaveDataToHeader(long DomainID, long UserContactID, DateTime OrderDate, string Description, decimal Amount, long OppID, long UndepositedFundAccountID, long lngDepositeID)
        //{
        //    try
        //    {
        //        JournalEntryHeader objJEHeader = new JournalEntryHeader();
        //        long lngJournalID = 0;
        //        {
        //            objJEHeader.JournalId = 0;
        //            objJEHeader.RecurringId = 0;
        //            objJEHeader.EntryDate = OrderDate;
        //            objJEHeader.Description = Description;
        //            objJEHeader.Amount = Amount;
        //            objJEHeader.CheckId = 0;
        //            objJEHeader.CashCreditCardId = 0;
        //            objJEHeader.ChartAcntId = 0;
        //            objJEHeader.OppId = OppID;
        //            objJEHeader.OppBizDocsId = 0;
        //            objJEHeader.DepositId = lngDepositeID;
        //            objJEHeader.BizDocsPaymentDetId = 0;
        //            objJEHeader.IsOpeningBalance = CCommon.ToBool(0);
        //            objJEHeader.LastRecurringDate = System.DateTime.Now;
        //            objJEHeader.NoTransactions = 0;
        //            objJEHeader.CategoryHDRID = 0;
        //            objJEHeader.ReturnID = 0;
        //            objJEHeader.CheckHeaderID = 0;
        //            objJEHeader.BillID = 0;
        //            objJEHeader.BillPaymentID = 0;
        //            objJEHeader.UserCntID = UserContactID;
        //            objJEHeader.DomainID = DomainID;
        //        }
        //        lngJournalID = objJEHeader.Save();
        //        return lngJournalID;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// Save Data To GeneralJournalDetails For Cash And Checks
        /// </summary>
        /// <param name="DomainID">DomainID</param>
        /// <param name="lngDepositeID">lngDepositeID</param>
        /// <param name="lngJournalID">lngJournalID</param>
        /// <param name="p_Amount">p_Amount</param>
        /// <param name="DivisionId">DivisionId</param>
        /// <param name="UndepositedFundAccountID">Undeposited FundAccount ID</param>
        private static void SaveDataToGeneralJournalDetailsForCashAndChecks(long DomainID, long lngDepositeID, long lngJournalID, decimal p_Amount, long DivisionId, long UndepositedFundAccountID, int PaymentMethod)
        {
            try
            {
                JournalEntryCollection objJEList = new JournalEntryCollection();
                JournalEntryNew objJE = new JournalEntryNew();
                //Debit : Undeposit Fund (Amount)
                objJE = new JournalEntryNew();

                objJE.TransactionId = 0;
                objJE.DebitAmt = p_Amount;
                objJE.CreditAmt = 0;
                objJE.ChartAcntId = UndepositedFundAccountID;
                objJE.Description = "Amount Paid (" + p_Amount.ToString() + ")  To Undeposited Fund";
                objJE.CustomerId = DivisionId;
                objJE.MainDeposit = CCommon.ToBool(1);
                objJE.MainCheck = CCommon.ToBool(0);
                objJE.MainCashCredit = CCommon.ToBool(0);
                objJE.OppitemtCode = 0;
                objJE.BizDocItems = "";
                objJE.Reference = "";
                objJE.PaymentMethod = PaymentMethod;
                objJE.Reconcile = false;
                objJE.CurrencyID = 0;
                objJE.FltExchangeRate = 0;
                objJE.TaxItemID = 0;
                objJE.BizDocsPaymentDetailsId = 0;
                objJE.ContactID = 0;
                objJE.ItemID = 0;
                objJE.ProjectID = 0;
                objJE.ClassID = 0;
                objJE.CommissionID = 0;
                objJE.ReconcileID = 0;
                objJE.Cleared = CCommon.ToBool(0);
                objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositHeader);
                objJE.ReferenceID = lngDepositeID;

                objJEList.Add(objJE);

                //For Each dr As DataRow In dtItems.Rows
                //Credit: Customer A/R With (Amount)
                OppBizDocs objOppBizDocs = new OppBizDocs();

                long lngCustomerARAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", DomainID, DivisionId);

                objJE = new JournalEntryNew();

                objJE.TransactionId = 0;
                objJE.DebitAmt = 0;
                objJE.CreditAmt = p_Amount;
                objJE.ChartAcntId = lngCustomerARAccount;
                objJE.Description = "Credit Customer's AR account";
                objJE.CustomerId = DivisionId;
                objJE.MainDeposit = CCommon.ToBool(0);
                objJE.MainCheck = CCommon.ToBool(0);
                objJE.MainCashCredit = CCommon.ToBool(0);
                objJE.OppitemtCode = 0;
                objJE.BizDocItems = "";
                objJE.Reference = "";
                objJE.PaymentMethod = PaymentMethod;
                objJE.Reconcile = false;
                objJE.CurrencyID = 0;
                objJE.FltExchangeRate = 0;
                objJE.TaxItemID = 0;
                objJE.BizDocsPaymentDetailsId = 0;
                objJE.ContactID = 0;
                objJE.ItemID = 0;
                objJE.ProjectID = 0;
                objJE.ClassID = 0;
                objJE.CommissionID = 0;
                objJE.ReconcileID = 0;
                objJE.Cleared = CCommon.ToBool(0);
                objJE.ReferenceType = CCommon.ToShort(enmReferenceType.DepositDetail);
                objJE.ReferenceID = 0;

                objJEList.Add(objJE);

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, DomainID);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets Item Classification ID from Item Classification Name
        /// Creates New Item Classification for given Name if Not Exists
        /// </summary>
        /// <param name="DomainId"></param>
        /// <param name="RecordOwner"></param>
        /// <param name="ItemClassificationName"></param>
        /// <returns></returns>
        public static long GetItemClassificationID(long DomainId, long RecordOwner, string ItemClassificationName)
        {
            long ItemClassificationID = 0;
            CCommon objCommon = new CCommon();
            DataTable dtItemclasifications = objCommon.GetMasterListItems(36, DomainId);
            foreach (DataRow dr1 in dtItemclasifications.Rows)
            {
                if (CCommon.ToString(dr1["vcData"]) == ItemClassificationName)
                {
                    ItemClassificationID = CCommon.ToLong(dr1["numListItemID"]);
                }
            }
            if (ItemClassificationID == 0)
            {
                objCommon.ListID = 36;
                objCommon.ListItemID = 0;
                objCommon.ListItemName = ItemClassificationName;
                objCommon.DomainID = DomainId;
                objCommon.UserCntID = RecordOwner;
                objCommon.ManageItemList();
                DataTable dt = objCommon.GetMasterListItems(36, DomainId);
                foreach (DataRow dr2 in dt.Rows)
                {
                    if (CCommon.ToString(dr2["vcData"]) == ItemClassificationName)
                    {
                        ItemClassificationID = CCommon.ToLong(dr2["numListItemID"]);
                    }
                }
            }
            return ItemClassificationID;
        }

        /// <summary>
        /// Adds new Item to Biz Database.  
        /// </summary>
        /// <param name="drItem">Item Detail</param>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">WebApi Id</param>
        /// <param name="RecordOwner">Record Owner</param>
        /// <param name="WareHouseID">WareHouse ID</param>
        /// <param name="ItemClassificationID">Item Classification ID</param>
        /// <returns></returns>
        public static long AddNewItemToBiz(DataRow drItem, long DomainId, int WebApiId, long RecordOwner, int WareHouseID, long ItemClassificationID, long ItemGroupID = 0, string ItemType = "P")
        {
            long ItemCode = 0;
            try
            {
                BACRM.BusinessLogic.Item.CItems objItem = new BACRM.BusinessLogic.Item.CItems();
                objItem.ItemCode = 0;
                objItem.ItemName = CCommon.ToString(drItem["vcItemName"]);
                objItem.ItemDesc = CCommon.ToString(drItem["txtItemDesc"]);
                objItem.ItemType = ItemType;
                objItem.ListPrice = CCommon.ToDouble(drItem["monListPrice"]);
                objItem.ItemClassification = ItemClassificationID;
                objItem.Taxable = false;
                objItem.SKU = CCommon.ToString(drItem["vcSKU"]);
                objItem.KitParent = false;
                objItem.DomainID = DomainId;
                objItem.UserCntID = RecordOwner;
                objItem.bitSerialized = false;
                objItem.VendorID = 0;
                objItem.ModelID = CCommon.ToString(drItem["vcModelID"]);
                objItem.ItemGroupID = ItemGroupID;
                objItem.WebApiId = WebApiId;
                objItem.COGSChartAcntId = CCommon.ToInteger(drItem["numCOGSChartAcntId"]);
                objItem.AssetChartAcntId = CCommon.ToInteger(drItem["numAssetChartAcntId"]);
                objItem.IncomeChartAcntId = CCommon.ToInteger(drItem["numIncomeChartAcntId"]);
                objItem.ApiItemID = CCommon.ToString(drItem["vcApiItemId"]);
                objItem.Quantity = CCommon.ToLong(drItem["numQuantity"]);
                objItem.Height = CCommon.ToDouble(drItem["fltShippingHeight"]);
                objItem.Length = CCommon.ToDouble(drItem["fltShippingLength"]);
                objItem.Width = CCommon.ToDouble(drItem["fltShippingWidth"]);
                objItem.Weight = CCommon.ToDouble(drItem["fltShippingWeight"]);
                objItem.ListOfAPI = CCommon.ToString(drItem["vcExportToAPI"]);
                objItem.bitArchieve = CCommon.ToBool(CCommon.ToInteger(drItem["IsArchieve"]));
                objItem.ProcedureCallFlag = 1;
                objItem.ManageItemsAndKits();
                objItem.str = CCommon.ToString(drItem["txtItemHtmlDesc"]);
                objItem.byteMode = 1;
                objItem.ManageItemExtendedDesc();
                ItemCode = objItem.ItemCode;
                CCommon objCommon = new CCommon();
                if (ItemGroupID == 0)
                {
                    objCommon.Mode = 39;
                    objCommon.DomainID = DomainId;
                    objCommon.Str = CCommon.ToString(ItemCode) + "~" + CCommon.ToString(drItem["vcSKU"]);
                    objCommon.UserCntID = RecordOwner;
                    long WareHouseItemID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                    objItem.OnHand = CCommon.ToLong(drItem["numQuantity"]);
                    objItem.WListPrice = CCommon.ToDouble(drItem["monListPrice"]);
                    objItem.WarehouseID = WareHouseID;
                    objItem.WSKU = CCommon.ToString(drItem["vcSKU"]);
                    objItem.WareHouseItemID = WareHouseItemID;

                    objItem.byteMode = 1;
                    objItem.AddUpdateWareHouseForItems();
                    if (CCommon.ToInteger(drItem["numQuantity"]) > 0)
                    {
                        objItem.MakeItemQtyAdjustmentJournal(ItemCode, objItem.ItemName, CCommon.ToInteger(drItem["numQuantity"]), CCommon.ToDecimal(objItem.ListPrice), CCommon.ToLong(objItem.AssetChartAcntId), RecordOwner, DomainId);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ItemCode;
        }

        /// <summary>
        /// Adds Non Inventory Item to Biz Database.  
        /// </summary>
        /// <param name="drItem">Item Detail</param>
        /// <param name="DomainId">Domain Id</param>
        /// <param name="WebApiId">WebApi Id</param>
        /// <param name="RecordOwner">Record Owner</param>
        /// <param name="WareHouseID">WareHouse ID</param>
        /// <param name="ItemClassificationID">Item Classification ID</param>
        /// <returns></returns>
        public static long AddNonInventoryItemToBiz(DataRow drItem, long DomainId, int WebApiId, long RecordOwner, int WareHouseID, long ItemClassificationID)
        {
            long ItemCode = 0;
            try
            {
                BACRM.BusinessLogic.Item.CItems objItem = new BACRM.BusinessLogic.Item.CItems();
                objItem.ItemCode = 0;
                objItem.ItemName = CCommon.ToString(drItem["vcItemName"]);
                objItem.ItemDesc = CCommon.ToString(drItem["txtItemDesc"]);
                objItem.ItemType = "N";
                objItem.ListPrice = CCommon.ToDouble(drItem["monListPrice"]);
                objItem.ItemClassification = ItemClassificationID;
                objItem.Taxable = false;
                objItem.SKU = CCommon.ToString(drItem["vcSKU"]);
                objItem.KitParent = false;
                objItem.DomainID = DomainId;
                objItem.UserCntID = RecordOwner;
                objItem.bitSerialized = false;
                objItem.VendorID = 0;
                objItem.ModelID = CCommon.ToString(drItem["vcModelID"]);
                objItem.ItemGroupID = 0;
                objItem.WebApiId = WebApiId;

                if (objItem.ItemType == "N")
                {
                    objItem.AssetChartAcntId = 0;
                }
                else
                {
                    objItem.AssetChartAcntId = CCommon.ToInteger(drItem["numAssetChartAcntId"]);
                }
                objItem.COGSChartAcntId = CCommon.ToInteger(drItem["numCOGSChartAcntId"]);
                objItem.IncomeChartAcntId = CCommon.ToInteger(drItem["numIncomeChartAcntId"]);
                objItem.ApiItemID = CCommon.ToString(drItem["vcApiItemId"]);
                objItem.Quantity = 1;
                objItem.Height = CCommon.ToDouble(drItem["fltShippingHeight"]);
                objItem.Length = CCommon.ToDouble(drItem["fltShippingLength"]);
                objItem.Width = CCommon.ToDouble(drItem["fltShippingWidth"]);
                objItem.Weight = CCommon.ToDouble(drItem["fltShippingWeight"]);
                objItem.ListOfAPI = CCommon.ToString(drItem["vcExportToAPI"]);
                objItem.ProcedureCallFlag = 1;
                objItem.ManageItemsAndKits();
                objItem.str = CCommon.ToString(drItem["txtItemHtmlDesc"]);
                objItem.byteMode = 1;
                objItem.ManageItemExtendedDesc();
                ItemCode = objItem.ItemCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ItemCode;
        }

        /// <summary>
        /// Get Weight UOM Code
        /// </summary>
        /// <param name="strWeightUOM"></param>
        /// <returns></returns>
        public static string GetWeightUOMCode(string strWeightUOM)
        {
            string WeightUOMCode = "";
            try
            {
                switch (strWeightUOM.ToUpper())
                {
                    case "GRAMS":
                        WeightUOMCode = "GR";
                        break;

                    case "KILOGRAMS":
                        WeightUOMCode = "KG";
                        break;

                    case "POUNDS":
                        WeightUOMCode = "LB";
                        break;

                    case "MILLIGRAMS":
                        WeightUOMCode = "MG";
                        break;

                    case "OUNCES":
                        WeightUOMCode = "OZ";
                        break;

                    default:
                        WeightUOMCode = "LB";
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return WeightUOMCode;
        }

        /// <summary>
        /// Get Length UOM Code
        /// </summary>
        /// <param name="strLengthUOM"></param>
        /// <returns></returns>
        public static string GetLengthUOMCode(string strLengthUOM)
        {
            string LengthUOMCode = "";
            try
            {
                switch (strLengthUOM.ToUpper())
                {
                    case "CENTIMETERS":
                        LengthUOMCode = "CM";
                        break;

                    case "DECIMETERS":
                        LengthUOMCode = "DM";
                        break;

                    case "FEET":
                        LengthUOMCode = "FT";
                        break;

                    case "INCHES":
                        LengthUOMCode = "IN";
                        break;

                    case "METERS":
                        LengthUOMCode = "M";
                        break;

                    case "MILLIMETERS":
                        LengthUOMCode = "MM";
                        break;

                    case "NANOMETERS":
                        LengthUOMCode = "NM";
                        break;

                    case "PICOMETERS":
                        LengthUOMCode = "PM";
                        break;

                    default:
                        LengthUOMCode = "IN";
                        break;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return LengthUOMCode;
        }

        /// <summary>
        /// Creates the Product Detail table structure
        /// </summary>
        /// <param name="dtProductDetails">DataTable  to Create Structure and Return</param>
        /// <returns>DataTable</returns>
        public static DataTable CreateProductDetailsTable(DataTable dtProductDetails)
        {
            try
            {
                if (dtProductDetails.Columns.Count == 0)
                {
                    dtProductDetails.Columns.Add("numItemCode");
                    dtProductDetails.Columns.Add("vcItemName");
                    dtProductDetails.Columns.Add("txtItemDesc");
                    dtProductDetails.Columns.Add("charItemType");
                    dtProductDetails.Columns.Add("monListPrice", typeof(double));
                    dtProductDetails.Columns.Add("txtItemHtmlDesc");
                    dtProductDetails.Columns.Add("bitTaxable", typeof(bool));
                    dtProductDetails.Columns.Add("vcSKU");
                    dtProductDetails.Columns.Add("bitKitParent", typeof(bool));
                    dtProductDetails.Columns.Add("numDomainID");
                    dtProductDetails.Columns.Add("numUserCntID");
                    dtProductDetails.Columns.Add("numVendorID");
                    dtProductDetails.Columns.Add("bitSerialized", typeof(bool));
                    dtProductDetails.Columns.Add("strFieldList");
                    dtProductDetails.Columns.Add("vcModelID");
                    dtProductDetails.Columns.Add("numItemGroup");
                    dtProductDetails.Columns.Add("numCOGSChartAcntId");
                    dtProductDetails.Columns.Add("numAssetChartAcntId");
                    dtProductDetails.Columns.Add("numIncomeChartAcntId");
                    dtProductDetails.Columns.Add("monAverageCost", typeof(double));
                    dtProductDetails.Columns.Add("monTotAmtBefDiscount", typeof(double));
                    dtProductDetails.Columns.Add("monLabourCost", typeof(double));
                    dtProductDetails.Columns.Add("fltWeight");
                    dtProductDetails.Columns.Add("fltHeight");
                    dtProductDetails.Columns.Add("fltLength");
                    dtProductDetails.Columns.Add("fltWidth");
                    dtProductDetails.Columns.Add("fltShippingWeight");
                    dtProductDetails.Columns.Add("fltShippingHeight");
                    dtProductDetails.Columns.Add("fltShippingLength");
                    dtProductDetails.Columns.Add("fltShippingWidth");
                    dtProductDetails.Columns.Add("WeightUOM");
                    dtProductDetails.Columns.Add("DimensionUOM");
                    dtProductDetails.Columns.Add("bitFreeshipping", typeof(bool));
                    dtProductDetails.Columns.Add("bitAllowBackOrder", typeof(bool));
                    dtProductDetails.Columns.Add("UnitofMeasure");
                    dtProductDetails.Columns.Add("bitShowDeptItem", typeof(bool));
                    dtProductDetails.Columns.Add("bitShowDeptItemDesc", typeof(bool));
                    dtProductDetails.Columns.Add("bitAssembly", typeof(bool));
                    dtProductDetails.Columns.Add("bitCalAmtBasedonDepItems", typeof(bool));
                    dtProductDetails.Columns.Add("intWebApiId", typeof(int));
                    dtProductDetails.Columns.Add("vcApiItemId");
                    dtProductDetails.Columns.Add("numBarCodeId");
                    dtProductDetails.Columns.Add("numQuantity");
                    dtProductDetails.Columns.Add("vcManufacturer");
                    dtProductDetails.Columns.Add("numBaseUnit");
                    dtProductDetails.Columns.Add("numPurchaseUnit");
                    dtProductDetails.Columns.Add("numSaleUnit");
                    dtProductDetails.Columns.Add("bitLotNo", typeof(bool));
                    dtProductDetails.Columns.Add("IsArchieve");
                    dtProductDetails.Columns.Add("numItemClass");
                    dtProductDetails.Columns.Add("tintStandardProductIDType");
                    dtProductDetails.Columns.Add("vcExportToAPI");
                    dtProductDetails.Columns.Add("Warranty");
                    dtProductDetails.Columns.Add("numEBayCategoryID");
                    dtProductDetails.Columns.Add("EbayListingType");
                    dtProductDetails.Columns.Add("EBayListingDuration");
                    dtProductDetails.Columns.Add("EbayStandardShippingCost");
                    dtProductDetails.Columns.Add("EbayExpressShippingCost");
                    dtProductDetails.Columns.Add("MagentoAttributeSetID");
                    dtProductDetails.Columns.Add("MagentoProductType");
                    dtProductDetails.Columns.Add("GoogleProductCategory");
                    dtProductDetails.Columns.Add("GoogleProductType");
                    dtProductDetails.Columns.Add("GoogleProductURL");
                    dtProductDetails.Columns.Add("ProductImageLink");
                    dtProductDetails.Columns.Add("MSRPrice");
                    //dtProductDetails.Columns.Add("EbayItemSpecifics");
                    //dtProductDetails.Columns.Add("EbayItemVariations");
                    dtProductDetails.TableName = "Item";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dtProductDetails;
        }

        /// <summary>
        /// Create Mail Message Item Detail Structure
        /// </summary>
        /// <param name="dtProductDetails"></param>
        /// <returns></returns>
        public static DataTable CreateMailMessageItemDetail(DataTable dtProductDetails)
        {
            try
            {
                if (dtProductDetails.Columns.Count == 0)
                {
                    dtProductDetails.Columns.Add("SNo");
                    dtProductDetails.Columns.Add("BizItemCode");
                    dtProductDetails.Columns.Add("ItemName");
                    dtProductDetails.Columns.Add("SKU");
                }
            }
            catch (Exception ex)
            {

            }
            return dtProductDetails;
        }

        /// <summary>
        /// Mail to Customer with Imported Item Details and Item Clasification
        /// </summary>
        /// <param name="DomainId"></param>
        /// <param name="WebApiId"></param>
        /// <param name="RecordOwner"></param>
        /// <param name="ItemCount"></param>
        /// <param name="dtItemDetails"></param>
        /// <param name="ItemClassificationName"></param>
        /// <param name="Marketplace"></param>
        public static void SendMailNewItemList(long DomainId, int WebApiId, long RecordOwner, long UserContactID, int ItemCount, DataTable dtItemDetails, string ItemClassificationName, string Marketplace)
        {
            StringBuilder MailMessage = new StringBuilder();
            try
            {
                string tab = "\t";
                string Message = "";
                string Subject = "New Items are imported to BizAutomation from " + Marketplace;
                MailMessage.Append("Dear customer, <br/> ");
                MailMessage.Append("<br />");
                Message = "New Items are imported to BizAutomation from your <b>" + Marketplace + "</b> seller account on " + DateTime.Now.ToString("ddMMMyyy HH:MM");
                Message += "<br />You can find the imported items in BizAutomation under Biz Item Classification <b>" + ItemClassificationName + "</b><br />";
                Message += "<br /><b>Imported Item List </b><br /><br />";
                MailMessage.Append(Message);
                MailMessage.AppendLine(tab + tab + "<table border=\"1\">");
                // headers.
                MailMessage.Append(tab + tab + tab + "<tr bgcolor=\"#E6E4E6\">");

                foreach (DataColumn dc in dtItemDetails.Columns)
                {
                    MailMessage.AppendFormat("<td>{0}</td>", dc.ColumnName);
                }
                MailMessage.AppendLine("</tr>");
                foreach (DataRow dr in dtItemDetails.Rows)
                {
                    MailMessage.Append(tab + tab + tab + "<tr bgcolor=\"#F7FDFF\">");
                    foreach (DataColumn dc in dtItemDetails.Columns)
                    {
                        string cellValue = dr[dc] != null ? dr[dc].ToString() : "";
                        MailMessage.AppendFormat("<td>{0}</td>", cellValue);
                    }
                    MailMessage.AppendLine("</tr>");
                }

                MailMessage.AppendLine(tab + tab + "</table>");
                MailMessage.Append("<br />");
                MailMessage.Append(" <br /> <font size=\"2.5\" color=\"#8A0A0A\" >* Item Details for SKU which is already exists in BizAutomation will be Updated</font> <br /><br />");
                MailMessage.Append("this is automated mail from BizAutomation.<br />");
                MailMessage.Append("please do not respond to this email.<br />");
                MailMessage.Append("<br />");
                MailMessage.Append("BizAutomation.com<br />");
                MailMessage.Append("One system for your <strong><em><span style='color: #00b050;'>ENTIRE</span></em></strong>");
                MailMessage.Append(" business<br />");
                MailMessage.Append("U.S.(888)-407-4781<br />");
                MailMessage.Append("Outside U.S. (408)-786-5116<br />");
                MailMessage.Append("<br />");
                MailMessage.Append("<br />");

                int intUserContactId = CCommon.ToInteger(UserContactID);
                CContacts objContact = new CContacts();
                string ToEmailId = objContact.GetEmailId(intUserContactId);
                //ToEmailId = "joseph@bizautomation.com";
                Email objEmail = new Email();
                objEmail.SendSystemEmail(Subject, MailMessage.ToString(), "", "noreply@bizautomation.com", ToEmailId, "BizAutomation", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Creates a Order Item for Discount amount
        /// </summary>
        /// <param name="drItem">Order Item Datarow Structure</param>
        /// <param name="dTotDiscount">Total Discount</param>
        /// <param name="DiscountItemId">Item Id to map discount Amount</param>
        /// <param name="DomainID">DomainID</param>
        /// <param name="WebApiId">WebApiId</param>
        public static DataRow GetDiscountOrderItem(DataRow drItem, decimal dTotDiscount, long DiscountItemId, long DomainID, long WebApiId, string DiscountItemName = "", decimal TaxAmount = 0)
        {
            CCommon objCommon = new CCommon();
            string Message = "";
            decimal decDiscount = 0;

            try
            {
                drItem["numoppitemtCode"] = 0;
                drItem["numItemCode"] = DiscountItemId;
                drItem["numUnitHour"] = 1;
                drItem["monPrice"] = dTotDiscount;
                drItem["monTotAmount"] = dTotDiscount;
                //OrderTotal += CCommon.ToDecimal(Component.Amount.Value);
                // objCommon.DomainID = DomainID;
                //  objCommon.Mode = 15;
                //objCommon.Str = CCommon.ToString(Component.Amount.currency);
                //  CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                //drItem["monTotAmount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                //drItem["monTotAmtBefDiscount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                //  drItem["monTotAmount"] = Component.Amount.Value;
                drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]) - decDiscount;
                drItem["numUOM"] = 0; // get it from item table, in procedure
                drItem["vcUOMName"] = "";// get it from item table, in procedure
                drItem["UOMConversionFactor"] = 1.0000;
                drItem["vcItemDesc"] = "";// get it from item table, in procedure
                drItem["vcModelID"] = "";// get it from item table, in procedure
                //drItem["numWarehouseID"] = WareHouseId;
                drItem["vcItemName"] = DiscountItemName;
                drItem["Warehouse"] = "";
                drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                drItem["ItemType"] = "Service";// get it from item table, in procedure
                drItem["Attributes"] = "";
                drItem["Op_Flag"] = 1;
                drItem["bitWorkOrder"] = false;
                drItem["DropShip"] = false;
                drItem["fltDiscount"] = decimal.Negate(decDiscount);
                drItem["bitDiscountType"] = 1; //Flat discount
                drItem["Tax0"] = TaxAmount;
                if (TaxAmount != 0)
                    drItem["bitTaxable0"] = true;
                else
                    drItem["bitTaxable0"] = false;

                drItem["numVendorWareHouse"] = 0;
                drItem["numShipmentMethod"] = 0;
                drItem["numSOVendorId"] = 0;
                drItem["numProjectID"] = 0;
                drItem["numProjectStageID"] = 0;
                drItem["charItemType"] = ""; // should be taken from procedure
                drItem["numToWarehouseItemID"] = 0;
                //drItem["Tax41"] = 0;
                //drItem["bitTaxable41"] = false;
                //drItem["Tax42"] = 0;
                //drItem["bitTaxable42"] = false;
                drItem["Weight"] = ""; // should take from procedure 
                drItem["WebApiId"] = WebApiId;
                drItem["vcSourceShipMethod"] = "";
                // dtItems.Rows.Add(drItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return drItem;
        }

        /// <summary>
        /// Creates a Order Item for Shipping Cost
        /// </summary>
        /// <param name="drItem">Order Item Datarow Structure</param>
        /// <param name="ShippingCost">Shipping Cost</param>
        /// <param name="DiscountItemId">Item Id to map Shipping Cost Amount</param>
        /// <param name="DomainID">Domain ID</param>
        /// <param name="WebApiId">WebApi Id</param>
        public static DataRow GetShippingCostItem(DataRow drItem, decimal ShippingCost, long ShippingServiceItemID, long DomainID, long WebApiId)
        {
            CCommon objCommon = new CCommon();
            string Message = "";
            decimal decDiscount = 0;

            try
            {
                drItem["numoppitemtCode"] = 0;
                drItem["numItemCode"] = ShippingServiceItemID;
                drItem["numUnitHour"] = 1;
                drItem["monPrice"] = ShippingCost;
                drItem["monTotAmount"] = ShippingCost;
                //OrderTotal += CCommon.ToDecimal(Component.Amount.Value);
                // objCommon.DomainID = DomainID;
                //  objCommon.Mode = 15;
                //objCommon.Str = CCommon.ToString(Component.Amount.currency);
                //  CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                //drItem["monTotAmount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                //drItem["monTotAmtBefDiscount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                //  drItem["monTotAmount"] = Component.Amount.Value;
                drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]);
                drItem["numUOM"] = 0; // get it from item table, in procedure
                drItem["vcUOMName"] = "";// get it from item table, in procedure
                drItem["UOMConversionFactor"] = 1.0000;
                drItem["vcItemDesc"] = "";// get it from item table, in procedure
                drItem["vcModelID"] = "";// get it from item table, in procedure
                //drItem["numWarehouseID"] = WareHouseId;
                if (ShippingCost > 0)
                    drItem["vcItemName"] = "Shipping Cost";
                else
                    drItem["vcItemName"] = "Free Shipping";
                drItem["Warehouse"] = "";
                drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                drItem["ItemType"] = "Service";// get it from item table, in procedure
                drItem["Attributes"] = "";
                drItem["Op_Flag"] = 1;
                drItem["bitWorkOrder"] = false;
                drItem["DropShip"] = false;
                drItem["fltDiscount"] = 0;
                drItem["bitDiscountType"] = 1; //Flat discount
                drItem["Tax0"] = 0;
                drItem["bitTaxable0"] = false;
                drItem["numVendorWareHouse"] = 0;
                drItem["numShipmentMethod"] = 0;
                drItem["numSOVendorId"] = 0;
                drItem["numProjectID"] = 0;
                drItem["numProjectStageID"] = 0;
                drItem["charItemType"] = ""; // should be taken from procedure
                drItem["numToWarehouseItemID"] = 0;
                //drItem["Tax41"] = 0;
                //drItem["bitTaxable41"] = false;
                //drItem["Tax42"] = 0;
                //drItem["bitTaxable42"] = false;
                drItem["Weight"] = ""; // should take from procedure 
                drItem["WebApiId"] = WebApiId;
                drItem["vcSourceShipMethod"] = "";
                //dtItems.Rows.Add(drItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return drItem;
        }


        public static DataRow GetSalesTaxItem(DataRow drItem, decimal SalesTaxAmount, long SalesTaxItemMappingID, long DomainID, long WebApiId)
        {
            CCommon objCommon = new CCommon();
            string Message = "";
            decimal decDiscount = 0;

            try
            {
                drItem["numoppitemtCode"] = 0;
                drItem["numItemCode"] = SalesTaxItemMappingID;
                drItem["numUnitHour"] = 1;
                drItem["monPrice"] = SalesTaxAmount;
                drItem["monTotAmount"] = SalesTaxAmount;
                //OrderTotal += CCommon.ToDecimal(Component.Amount.Value);
                // objCommon.DomainID = DomainID;
                //  objCommon.Mode = 15;
                //objCommon.Str = CCommon.ToString(Component.Amount.currency);
                //  CurrencyID = CCommon.ToLong(objCommon.GetSingleFieldValue());
                //drItem["monTotAmount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                //drItem["monTotAmtBefDiscount"] = CCommon.ToString(CCommon.ToInteger(orderItem.Quantity) * CCommon.ToDecimal(Component.Amount.Value));
                //  drItem["monTotAmount"] = Component.Amount.Value;
                drItem["monTotAmtBefDiscount"] = CCommon.ToDecimal(drItem["monTotAmount"]);
                drItem["numUOM"] = 0; // get it from item table, in procedure
                drItem["vcUOMName"] = "";// get it from item table, in procedure
                drItem["UOMConversionFactor"] = 1.0000;
                drItem["vcItemDesc"] = "";// get it from item table, in procedure
                drItem["vcModelID"] = "";// get it from item table, in procedure
                //drItem["numWarehouseID"] = WareHouseId;
                drItem["vcItemName"] = "Sales Tax";
                drItem["Warehouse"] = "";
                drItem["numWarehouseItmsID"] = 0;//will be given from procedure
                drItem["ItemType"] = "Service";// get it from item table, in procedure
                drItem["Attributes"] = "";
                drItem["Op_Flag"] = 1;
                drItem["bitWorkOrder"] = false;
                drItem["DropShip"] = false;
                drItem["fltDiscount"] = 0;
                drItem["bitDiscountType"] = 1; //Flat discount
                drItem["Tax0"] = 0;
                drItem["bitTaxable0"] = false;
                drItem["numVendorWareHouse"] = 0;
                drItem["numShipmentMethod"] = 0;
                drItem["numSOVendorId"] = 0;
                drItem["numProjectID"] = 0;
                drItem["numProjectStageID"] = 0;
                drItem["charItemType"] = ""; // should be taken from procedure
                drItem["numToWarehouseItemID"] = 0;
                //drItem["Tax41"] = 0;
                //drItem["bitTaxable41"] = false;
                //drItem["Tax42"] = 0;
                //drItem["bitTaxable42"] = false;
                drItem["Weight"] = ""; // should take from procedure 
                drItem["WebApiId"] = WebApiId;
                drItem["vcSourceShipMethod"] = "";
                //dtItems.Rows.Add(drItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return drItem;
        }


        public bool CheckMasterlistItem(long DomainID, long ListID, long ListItemID)
        {
            bool IsExists = false;
            try
            {
                CCommon objCommon = new CCommon();
                DataTable dt = objCommon.CheckMasterlistItem(DomainID, ListID, ListItemID);
                if (dt.Rows.Count > 0)
                {
                    IsExists = CCommon.ToBool(CCommon.ToInteger(dt.Rows[0]["IsExists"]));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return IsExists;
        }

        public bool CheckContact(long DomainID, long numContactId)
        {
            bool IsExists = false;
            try
            {
                CCommon objCommon = new CCommon();
                DataTable dt = objCommon.ConEmpList(DomainID, CCommon.ToBool(0), 0);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (CCommon.ToLong(dr["numContactID"]) == numContactId)
                        {
                            IsExists = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return IsExists;
        }

        public bool CheckMappedItem(int ItemCode)
        {
            bool IsExists = false;
            try
            {
                BACRM.BusinessLogic.Item.CItems objItems = new BACRM.BusinessLogic.Item.CItems();
                objItems.ItemCode = ItemCode;
                TimeSpan diff1 = DateTime.Now.Subtract(DateTime.UtcNow);
                objItems.ClientZoneOffsetTime = CCommon.ToInteger(diff1.TotalMinutes);
                DataTable dt = objItems.ItemDetails();

                if (dt.Rows.Count > 0)
                {
                    IsExists = true;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return IsExists;
        }

        public bool ValidateApiSetting(DataRow dr, out string Message)
        {
            Message = "";
            bool IsValid = true;
            long DomainID = 0;
            int WebApiId = 0;
            int OrderStatus = 0;
            int ShippedOrderStatus = 0;
            int WareHouseID = 0;
            int ExpenseAccountId = 0;
            long BizDocId = 0;
            long BizDocStatusId = 0;
            long FBABizDocId = 0;
            long FBABizDocStatusId = 0;
            long RecordOwner = 0;
            long AssignTo = 0;
            long RelationshipId = 0;
            long ProfileId = 0;
            int DiscountItemMapping = 0;
            int SalesTaxItemMappingID = 0;
            int ShippingServiceItemID = 0;
            try
            {

                DomainID = CCommon.ToLong(dr["numDomainId"]);
                WebApiId = CCommon.ToInteger(dr["WebApiId"]);

                OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                ShippedOrderStatus = CCommon.ToInteger(dr["numUnShipmentOrderStatus"]);
                WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                BizDocStatusId = CCommon.ToLong(dr["numBizDocStatusId"]);
                FBABizDocId = CCommon.ToLong(dr["numFBABizDocId"]);
                FBABizDocStatusId = CCommon.ToLong(dr["numFBABizDocStatusId"]);
                RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                ProfileId = CCommon.ToLong(dr["numProfileId"]);
                // ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                DiscountItemMapping = CCommon.ToInteger(dr["numDiscountItemMapping"]);
                ShippingServiceItemID = CCommon.ToInteger(dr["numShippingServiceItemID"]);
                SalesTaxItemMappingID = CCommon.ToInteger(dr["numSalesTaxItemMapping"]);
                int AdminID = CCommon.ToInteger(dr["numAdminID"]);
                if (!ValidateShippingFromAddress(DomainID, AdminID))
                {
                    IsValid = false;
                    Message += " Shipping address is missing for the Organization. Please provide Shipping Address in Organization Details..<br/>";
                }
                if (DiscountItemMapping > 0)
                {
                    if (!CheckMappedItem(DiscountItemMapping))
                    {
                        IsValid = false;
                        Message += " Mapped Discount Service Item does not exists.<br/>";
                    }
                }
                else
                {
                    IsValid = false;
                    Message += " Discount Item is not mapped.<br/>";
                }

                if (ShippingServiceItemID > 0)
                {
                    if (!CheckMappedItem(ShippingServiceItemID))
                    {
                        IsValid = false;
                        Message += " Mapped Shipping Service Item not exists.<br/>";
                    }
                    if (DiscountItemMapping > 0)
                    {
                        if (DiscountItemMapping == ShippingServiceItemID)
                        {
                            IsValid = false;
                            Message += " You cannot map same service item for Shipping Service Item and Discount Service Item.<br/>";
                        }
                    }
                    if (SalesTaxItemMappingID > 0)
                    {
                        if (SalesTaxItemMappingID == ShippingServiceItemID)
                        {
                            IsValid = false;
                            Message += " You cannot map same service item for Shipping Service Item and Sales Tax Service Item.<br/>";
                        }
                    }
                }
                else
                {
                    IsValid = false;
                    Message += " Shipping Service Item  is not mapped.<br/>";
                }

                if (SalesTaxItemMappingID > 0)
                {
                    if (!CheckMappedItem(SalesTaxItemMappingID))
                    {
                        IsValid = false;
                        Message += " Mapped Sales Tax Service Item not exists.<br/>";
                    }
                }
                else
                {
                    IsValid = false;
                    Message += " Shipping Sales Tax Item is not mapped.<br/>";
                }

                if (DiscountItemMapping == ShippingServiceItemID)
                {
                    IsValid = false;
                    Message += " You cannot map same Service Item for Shipping Service item abd Sales Tax Service Item.<br/>";
                }
                if (OrderStatus > 0)
                {
                    if (!CheckMasterlistItem(DomainID, 176, OrderStatus))
                    {
                        IsValid = false;
                        Message += " Mapped Order status does not exists, Please map Order status.<br/>";
                    }
                }
                else
                {
                    IsValid = false;
                    Message += " Order status is not mapped.<br/>";
                }

                if (WebApiId == 2)//Amazon US
                {
                    if (FBABizDocId > 0)
                    {
                        if (!CheckMasterlistItem(DomainID, 27, FBABizDocId))
                        {
                            IsValid = false;
                            Message += " Mapped FBA BizDoc Id does not exists, Please map FBA BizDoc Id.<br/>";
                        }
                        if (BizDocId == FBABizDocId)
                        {
                            IsValid = false;
                            Message += " You cannot map same BizDoc Type for FBM Orders and FBA Orders.<br/>";
                        }
                    }
                    else
                    {
                        IsValid = false;
                        Message += " FBA BizDoc Id is not mapped.<br/>";
                    }

                    if (FBABizDocStatusId > 0)
                    {
                        if (!CheckMasterlistItem(DomainID, 11, FBABizDocStatusId))
                        {
                            IsValid = false;
                            Message += " Mapped FBA BizDoc status does not exists, Please map FBA BizDoc status.<br/>";
                        }
                        if (BizDocId == FBABizDocId)
                        {
                            IsValid = false;
                            Message += " You cannot map same BizDoc Status for FBM Orders and FBA Orders.<br/>";
                        }
                    }
                    else
                    {
                        IsValid = false;
                        Message += " FBA BizDoc status is not mapped.<br/>";
                    }

                    if (ShippedOrderStatus > 0)
                    {
                        if (!CheckMasterlistItem(DomainID, 176, ShippedOrderStatus))
                        {
                            IsValid = false;
                            Message += " Mapped FBA Order status does not exists, Please map FBA Order status.<br/>";
                        }
                    }
                    else
                    {
                        IsValid = false;
                        Message += " FBA Order status is not mapped.<br/>";
                    }
                }

                if (BizDocId > 0)
                {
                    if (!CheckMasterlistItem(DomainID, 27, BizDocId))
                    {
                        IsValid = false;
                        Message += " Mapped BizDoc Type does not exists.<br/>";
                    }
                }
                else
                {
                    IsValid = false;
                    Message += " Default BizDoc Type is not mapped.<br/>";
                }

                if (BizDocStatusId > 0)
                {
                    if (!CheckMasterlistItem(DomainID, 11, BizDocStatusId))
                    {
                        IsValid = false;
                        Message += " Mapped BizDoc Status does not exists.<br/>";
                    }
                }
                else
                {
                    IsValid = false;
                    Message += " Default BizDoc Status is not mapped.<br/>";
                }
                if (RecordOwner > 0)
                {
                    if (!CheckContact(DomainID, RecordOwner))
                    {
                        IsValid = false;
                        Message += " Mapped Record Owner does not exists.<br/>";
                    }
                }
                else
                {
                    IsValid = false;
                    Message += " Default Record Owner is not mapped.<br/>";
                }

                if (AssignTo > 0)
                {
                    if (!CheckContact(DomainID, AssignTo))
                    {
                        IsValid = false;
                        Message += " Mapped Assigned to record Id does not exists.<br/>";
                    }
                }
                else
                {
                    IsValid = false;
                    Message += " Default Record Owner is not mapped.<br/>";
                }
                if (RelationshipId > 0)
                {
                    if (!CheckMasterlistItem(DomainID, 5, RelationshipId))
                    {
                        IsValid = false;
                        Message += " Mapped Relationship does not exists.<br/>";
                    }
                }
                else
                {
                    IsValid = false;
                    Message += " Default Relationship is not mapped.<br/>";
                }
                if (ProfileId != 0)
                {
                    if (!CheckMasterlistItem(DomainID, 21, ProfileId))
                    {
                        IsValid = false;
                        Message += " Mapped Profile Id does not exists.<br/>";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return IsValid;
        }

        /// <summary>
        /// Mail to Customer with Imported Item Details and Item Clasification
        /// </summary>
        /// <param name="DomainId"></param>
        /// <param name="WebApiId"></param>
        /// <param name="RecordOwner"></param>
        /// <param name="ItemCount"></param>
        /// <param name="dtItemDetails"></param>
        /// <param name="ItemClassificationName"></param>
        /// <param name="Marketplace"></param>
        public static void SendMailMarketplaceDisabled(long DomainId, int AdminID, string Marketplace, string ErrorMessage)
        {
            StringBuilder MailMessage = new StringBuilder();
            try
            {
                string ToEmailId = "";
                string tab = "\t";
                string Message = "";
                string Subject = "Your  " + Marketplace + " Online Marketplace Integration is Disabled";
                MailMessage.Append("Dear customer, <br/> ");
                MailMessage.Append("<br />");
                Message = "Your  " + Marketplace + " Online Marketplace Integration is Disabled on " + DateTime.Now.ToString("ddMMMyyy HH:MM");
                Message += "<br />Due to <br />";
                Message += "<br />" + ErrorMessage + " <br /><br />";
                MailMessage.Append(Message);
                MailMessage.Append("this is automated mail from BizAutomation.<br />");
                MailMessage.Append("please do not respond to this email.<br />");
                MailMessage.Append("<br />");
                MailMessage.Append("BizAutomation.com<br />");
                MailMessage.Append("One system for your <strong><em><span style='color: #00b050;'>ENTIRE</span></em></strong>");
                MailMessage.Append(" business<br />");
                MailMessage.Append("U.S.(888)-407-4781<br />");
                MailMessage.Append("Outside U.S. (408)-786-5116<br />");
                MailMessage.Append("<br />");
                MailMessage.Append("<br />");

                CContacts objContact = new CContacts();
                ToEmailId = objContact.GetEmailId(AdminID);
                // ToEmailId = "joseph@bizautomation.com";
                Email objEmail = new Email();
                //  DataTable dt = new DataTable();
                //objEmail.SendEmail(Subject, MailMessage.ToString(), "", "joseph@bizautomation.com", "jxavier.mca@gmail.com",dt,"","",0,false);
                // objEmail.SendSystemEmail(Subject, MailMessage.ToString(), "", "joseph@bizautomation.com", ToEmailId, "BizAutomation", "");
                objEmail.SendSystemEmail(Subject, MailMessage.ToString(), "", "noreply@bizautomation.com", ToEmailId, "BizAutomation", "");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lngOppID"></param>
        /// <param name="DomainID"></param>
        /// <param name="WebApiId"></param>
        /// <param name="RecOwner"></param>
        /// <returns></returns>
        public string DeleteOpportunity(long lngOppID, long DomainID, int WebApiId, long RecOwner)
        {
            string Message = "";
            COpportunities objOpport = new COpportunities();
            try
            {
                long lintCount = 0;
                objOpport.OpportID = lngOppID;
                objOpport.DomainID = DomainID;
                objOpport.UserCntID = RecOwner;
                lintCount = objOpport.GetAuthoritativeOpportunityCount();
                //if (lintCount == 0)
                //{
                //    CAdmin objAdmin = new CAdmin();
                //    objAdmin.DomainID = DomainID;
                //    objAdmin.ModeType = 0;
                //    objAdmin.ProjectID = lngOppID;
                //    objAdmin.RemoveStagePercentageDetails();
                //    objOpport.DelOpp();
                //}
                //else 

                if (lintCount > 0)
                {
                    JournalEntry objJournalEntry = new JournalEntry();
                    objJournalEntry.DomainID = DomainID;
                    objJournalEntry.OppId = lngOppID;
                    objJournalEntry.OppBIzDocID = 0;
                    objJournalEntry.BillID = 0;
                    objJournalEntry.ReturnID = 0;
                    objJournalEntry.ClientTimeZoneOffset = 0;
                    DataSet ds = objJournalEntry.GetOppBizDocsReceivePayment();

                    DataTable dtReceivePayment = ds.Tables[0];
                    DataTable dt2 = ds.Tables[1];
                    foreach (DataRow drReceivePayment in dtReceivePayment.Rows)
                    {
                        objJournalEntry.DomainID = DomainID;
                        objJournalEntry.JournalId = 0;
                        objJournalEntry.BillPaymentID = 0;
                        objJournalEntry.DepositId = CCommon.ToLong(drReceivePayment["numReferenceID"]);
                        objJournalEntry.CheckHeaderID = 0;
                        objJournalEntry.BillID = 0;
                        objJournalEntry.CategoryHDRID = 0;
                        objJournalEntry.ReturnID = 0;
                        objJournalEntry.UserCntID = RecOwner;
                        objJournalEntry.DeleteJournalEntryDetails();
                    }
                    DataTable dtTable = default(DataTable);
                    OppBizDocs objBizDocs = new OppBizDocs();
                    objBizDocs.OppId = lngOppID;
                    objBizDocs.ClientTimeZoneOffset = 0; ;
                    DataSet dsBizDocs = objBizDocs.GetBizDocsByOOpId();

                    if (dsBizDocs != null && dsBizDocs.Tables.Count > 1)
                    {
                        dtTable = dsBizDocs.Tables[1];
                    }

                    if (dtTable != null && dtTable.Rows.Count > 0)
                    {
                        foreach (DataRow drBizDocs in dtTable.Rows)
                        {
                            DeleteOppBizDocs(lngOppID, CCommon.ToLong(drBizDocs["numOppBizDocsId"]), DomainID, RecOwner);

                            //objBizDocs.OppBizDocId = CCommon.ToLong(drBizDocs["numOppBizDocsId"]);
                            //objBizDocs.DeleteComissionJournal();
                            ////AutomatonRule objAutomatonRule = new AutomatonRule();

                            ////  objAutomatonRule.ExecuteAutomationRule(49, objOppBizDocs.OppBizDocId, 4);

                            //objBizDocs.DeleteBizDoc();
                        }
                        //IsOrderClosed = CCommon.ToBool(dtTable.Rows[0]("tintShipped"));
                    }
                }
                CAdmin objAdmin = new CAdmin();
                objAdmin.DomainID = DomainID;
                objAdmin.ModeType = 0;
                objAdmin.ProjectID = lngOppID;
                objAdmin.RemoveStagePercentageDetails();
                objOpport.DelOpp();
            }
            catch (Exception ex)
            {
                if (ex.Message == "DEPENDANT")
                {
                    Message = "Can not remove record, Your option is to remove Time and Expense associated with all stages from \"milestone & stages\" sub-tab and try again.";
                }
                else if (ex.Message == "CASE DEPENDANT")
                {
                    Message = "Dependent case exists. Cannot be Deleted.";
                }
                else if (ex.Message == "CreditBalance DEPENDANT")
                {
                    Message = "Credit balance of current order is being used. Your option is used to credit balance of Organization from Accunting sub tab.";
                }
                else if (ex.Message == "OppItems DEPENDANT")
                {
                    Message = "Items alreay used. Cannot be Deleted.";
                }
                else if (ex.Message == "FY_CLOSED")
                {
                    Message = "This transaction can not be posted,Because transactions date belongs to closed financial year.";
                }
                else
                {
                    throw ex;
                }
            }

            return Message;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="OppId"></param>
        /// <param name="OppBizId"></param>
        /// <param name="DomainID"></param>
        /// <param name="RecordOwner"></param>
        public void DeleteOppBizDocs(long OppId, long OppBizId, long DomainID, long RecordOwner)
        {
            OppBizDocs objBizDocs = new OppBizDocs();
            OppBizDocs objOppBizDocs = new OppBizDocs();
            JournalEntry objJournalEntry = new JournalEntry();

            long lintAuthorizativeBizDocsId = 0;
            long lintJournalId = 0;

            try
            {
                objBizDocs.OppBizDocId = OppBizId;
                objBizDocs.OppId = OppId;
                objBizDocs.DomainID = DomainID;

                objOppBizDocs.OppId = OppId;
                objOppBizDocs.UserCntID = RecordOwner;
                objOppBizDocs.DomainID = DomainID;
                long lintCount = objBizDocs.GetOpportunityBizDocsCount();
                objOppBizDocs.OppType = 1;
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy();
                objOppBizDocs.OppBizDocId = OppBizId;
                lintJournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs();
                if (lintAuthorizativeBizDocsId == OppBizId)
                {
                    if (lintCount == 0)
                    {
                        //'To Delete Journal Entry
                        if (lintJournalId != 0)
                        {
                            objJournalEntry.JournalId = CCommon.ToInteger(lintJournalId);
                            objJournalEntry.DomainID = DomainID;
                            objJournalEntry.DeleteJournalEntryDetails();
                        }
                        objBizDocs.DeleteComissionJournal();

                        AutomatonRule objAutomatonRule = new AutomatonRule();
                        objAutomatonRule.ExecuteAutomationRule(49, objOppBizDocs.OppBizDocId, 4);

                        objBizDocs.DeleteBizDoc();

                    }
                    else
                    {
                    }
                }
                else
                {
                    if (lintJournalId != 0)
                    {
                        objJournalEntry.JournalId = CCommon.ToInteger(lintJournalId);
                        objJournalEntry.DomainID = DomainID;
                        objJournalEntry.DeleteJournalEntryDetails();
                    }
                    objBizDocs.DeleteComissionJournal();
                    objBizDocs.DeleteBizDoc();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DomainID"></param>
        /// <param name="WebApiId"></param>
        /// <param name="Source"></param>
        /// <param name="RecordOwner"></param>
        /// <param name="drImportOrderReq"></param>
        /// <returns></returns>
        public bool RemoveApiOrdersForReImport(long DomainID, int WebApiId, string Source, long RecordOwner, DataRow drImportOrderReq)
        {
            bool CanReImport = false;
            string OrderId = "";
            int RequestType = 0;
            DateTime FromDate, ToDate;
            long RequestId = 0;
            WebAPI objWebAPI = new WebAPI();
            string LogMessage = "";
            try
            {

                OrderId = CCommon.ToString(drImportOrderReq["vcApiOrderId"]);
                RequestId = CCommon.ToLong(drImportOrderReq["RequestId"]);
                RequestType = CCommon.ToInteger(drImportOrderReq["tintRequestType"]);
                FromDate = Convert.ToDateTime(drImportOrderReq["dtFromDate"]);
                ToDate = Convert.ToDateTime(drImportOrderReq["dtToDate"]);

                if (RequestType == 1)
                {
                    objWebAPI.Mode = 1;
                }
                else if (RequestType == 2)
                {
                    objWebAPI.Mode = 0;
                }

                objWebAPI.vcAPIOppId = OrderId;
                objWebAPI.DomainID = DomainID;
                objWebAPI.WebApiId = WebApiId;
                objWebAPI.OrdersFromDate = FromDate;
                objWebAPI.OrdersToDate = ToDate;

                DataTable dtApiOpportunities = objWebAPI.GetApiOpportunities();
                BizCommonFunctions objBizCommonFunctions = new BizCommonFunctions();
                string OppDeleteMessage = "";
                if (dtApiOpportunities.Rows.Count > 0)
                {
                    foreach (DataRow drApiOpp in dtApiOpportunities.Rows)
                    {
                        try
                        {
                            long OppId = CCommon.ToLong(drApiOpp["numOppId"]);
                            string MarketplaceOrderId = CCommon.ToString(drApiOpp["vcMarketplaceOrderId"]);
                            OppDeleteMessage = DeleteOpportunity(OppId, DomainID, WebApiId, RecordOwner);
                            if (OppDeleteMessage == "")
                            {
                                LogMessage = "API Order Detail for Biz Opportunity ID " + CCommon.ToString(OppId) + " and  " + Source + " Order Id " + MarketplaceOrderId + " is deleted from BizAutomation as for Re-Import Order request";
                                GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    CanReImport = true;
                }
                else
                {
                    LogMessage = "No Orders found to Remove from BizAutomation to process Re-Import Order Request for " + Source + ".";
                    GeneralFunctions.WriteMessage(DomainID, "LOG", LogMessage);
                    CanReImport = true;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return CanReImport;
        }

        public static string GetItemTypeNameByCharType(string CharItemType)
        {
            string ItemTypeName = "";
            try
            {
                switch (CharItemType)
                {
                    case "P":
                        ItemTypeName = "Inventory Item";
                        break;

                    case "S":
                        ItemTypeName = "Service";
                        break;

                    case "N":
                        ItemTypeName = "Non-Inventory Item";
                        break;

                    default:
                        ItemTypeName = "Inventory Item";
                        break;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return ItemTypeName;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DomainID"></param>
        /// <param name="AdminId"></param>
        /// <returns></returns>
        private bool ValidateShippingFromAddress(long DomainID, long AdminId)
        {
            try
            {
                bool IsValid = true;
                string strMessage = "";
                DataTable dtShipFrom = default(DataTable);
                CContacts objContacts = new CContacts();
                objContacts.ContactID = AdminId;
                objContacts.DomainID = DomainID;
                dtShipFrom = objContacts.GetBillOrgorContAdd();

                if (dtShipFrom.Rows.Count > 0)
                {
                    if (CCommon.ToString(dtShipFrom.Rows[0]["vcShipCountry"]).Trim().Length < 2)
                    {
                        IsValid = false;
                    }
                    if (CCommon.ToString(dtShipFrom.Rows[0]["vcShipState"]).Trim().Length < 2)
                    {
                        IsValid = false;
                    }
                    if (CCommon.ToString(dtShipFrom.Rows[0]["vcFirstname"]).Trim().Length < 2)
                    {
                        IsValid = false;
                    }
                    if (CCommon.ToString(dtShipFrom.Rows[0]["vcLastname"]).Trim().Length < 2)
                    {
                        IsValid = false;
                    }
                    if (CCommon.ToString(dtShipFrom.Rows[0]["vcCompanyName"]).Trim().Length < 2)
                    {
                        IsValid = false;
                    }
                    if (CCommon.ToString(dtShipFrom.Rows[0]["vcPhone"]).Trim().Length < 4)
                    {
                        IsValid = false;
                    }
                    if (CCommon.ToString(dtShipFrom.Rows[0]["vcShipPostCode"]).Trim().Length < 2)
                    {
                        IsValid = false;
                    }
                    if (CCommon.ToString(dtShipFrom.Rows[0]["vcShipCity"]).Trim().Length < 2)
                    {
                        IsValid = false;
                    }
                    if (CCommon.ToString(dtShipFrom.Rows[0]["vcShipStreet"]).Trim().Length < 2)
                    {
                        IsValid = false;
                    }
                }
                else
                {
                    IsValid = false;
                }
                return IsValid;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
