﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;

using BACRM.BusinessLogic;
using BACRMBUSSLOGIC.BussinessLogic;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.Item;
using BACRM.BusinessLogic.Opportunities;
using BACRM.BusinessLogic.Leads;
using BACRM.BusinessLogic.Contacts;
using BACRM.BusinessLogic.Prospects;
using BACRM.BusinessLogic.Contract;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.WebAPI;

using DebugApplication.AMWS_Communicator;
//using AmazonSource;

using eBay.Service.Core.Soap;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DebugApplication
{
    public partial class Amazon : System.Web.UI.Page
    {
        private string AmazonOrderFilePath = ConfigurationManager.AppSettings["AmazonOrderFilePath"];
        private string AmazonClosedOrdersPath = ConfigurationManager.AppSettings["AmazonClosedOrders"];

        private string EBayOrderFilePath = ConfigurationManager.AppSettings["EBayOrderFilePath"];
        private string EBayClosedOrders = ConfigurationManager.AppSettings["EBayClosedOrders"];

        private string GoogleOrderFilePath = ConfigurationManager.AppSettings["GoogleOrderFilePath"];
        private string GoogleClosedOrders = ConfigurationManager.AppSettings["GoogleClosedOrders"];

        private string MagentoOrderFilePath = ConfigurationManager.AppSettings["MagentoOrderFilePath"];
        private string MagentoClosedOrders = ConfigurationManager.AppSettings["MagentoClosedOrders"];

        private string AmazonMWSAccessKey = ConfigurationManager.AppSettings["AmazonMWSAccessKey"];
        private string AmazonMWSSecretKey = ConfigurationManager.AppSettings["AmazonMWSSecretKey"];

        eBayClient objeBayClient = null;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGetAmazonItems_Click(object sender, System.EventArgs e)
        {
            ReadAmazonProductListingReport();
        }

        /// <summary>
        /// (This Method is Applicable only for Amazon MWS as of Now)
        /// Reads the Product Listing Report File from Specific folder and creates Items in BizDatabase 
        /// and Maps Item with WebApis by ItemAPI Table entry in BizDatabase
        /// </summary>

        //DataTable dtProductListingsMain;
        private void ReadAmazonProductListingReport()
        {
            AmazonClient objAmazonClient = null;
            WebAPI objWebAPI = new WebAPI();

            int WebApiId = 0;
            int OrderStatus = 0;
            int WareHouseID = 0;
            int ExpenseAccountId = 0;
            long DomainID = 0;
            long BizDocId = 0;
            long RecordOwner = 0;
            long AssignTo = 0;
            long RelationshipId = 0;
            long ProfileId = 0;
            long DiscountItemMapping = 0;
            long UserContactID = 0;
            string Source = "", ErrorMsg = "";

            try
            {
                objWebAPI.Mode = 2;
                DataTable dtWebAPI = objWebAPI.GetWebApi();
                foreach (DataRow dr in dtWebAPI.Rows)
                {
                    try
                    {
                        if (CCommon.ToBool(dr["bitEnableAPI"]) == true)
                        {
                            DomainID = CCommon.ToLong(dr["numDomainId"]);
                            WebApiId = CCommon.ToInteger(dr["WebApiId"]);
                            if (WebApiId == (int)WebAPIList.AmazonUS) //Amazon US)
                            {
                                objAmazonClient = new AmazonClient();
                                string sourceDir = GeneralFunctions.GetPath("ProductListing", DomainID);
                                if (Directory.Exists(sourceDir))
                                {
                                    string[] fileEntries = Directory.GetFiles(sourceDir, "*", SearchOption.AllDirectories);//reads all feed files
                                    if (fileEntries.Length > 0)
                                    {
                                        Source = CCommon.ToString(dr["vcProviderName"]);
                                        OrderStatus = CCommon.ToInteger(dr["numOrderStatus"]);
                                        WareHouseID = CCommon.ToInteger(dr["numWareHouseID"]);
                                        BizDocId = CCommon.ToLong(dr["numBizDocId"]);
                                        RecordOwner = CCommon.ToLong(dr["numRecordOwner"]);
                                        AssignTo = CCommon.ToLong(dr["numAssignTo"]);
                                        RelationshipId = CCommon.ToLong(dr["numRelationshipId"]);
                                        ProfileId = CCommon.ToLong(dr["numProfileId"]);
                                        // ExpenseAccountId = CCommon.ToInteger(dr["numExpenseAccountId"]);
                                        ExpenseAccountId = 0;
                                        DiscountItemMapping = CCommon.ToLong(dr["numDiscountItemMapping"]);
                                        UserContactID = CCommon.ToLong(dr["vcFifteenthFldValue"]);
                                        foreach (string filepath in fileEntries)
                                        {
                                            try
                                            {
                                                DataTable dtProductListings;
                                                DataSet ds = GeneralFunctions.GetDataSetFromDelimitedFlatFile(filepath, "\t");
                                                dtProductListings = ds.Tables[0];

                                                DataView dvProdList = dtProductListings.DefaultView;
                                                dvProdList.RowFilter = "[item-name] <> ''";
                                                dtProductListings = dvProdList.ToTable();

                                                //dtProductListingsMain = dtProductListings.Copy();
                                                if (dtProductListings.Rows.Count > 0)
                                                {
                                                    dtProductListings.Columns.Add("IsChild",typeof(short)); // 0  for Parent, 1 for Child record

                                                    if (CCommon.ToString(Path.GetFileNameWithoutExtension(filepath)).Substring(0, 1) == "1")
                                                        dtProductListings.Columns["seller-sku"].ColumnName = "SellerSKU";
                                                    else if (CCommon.ToString(Path.GetFileNameWithoutExtension(filepath)).Substring(0, 1) == "2")
                                                        dtProductListings.Columns["sku"].ColumnName = "SellerSKU";
                                                    else
                                                        throw new Exception("Service1 : From ReadAmazonProductListingReport: " + "Product Listing type conflicts, please check File Name " + Path.GetFileNameWithoutExtension(filepath) + ". 1-> Active Listing, 2-> Complete Listing");
                                                    //objAmazonClient.ProcessProductListingReport(DomainID, WebApiId, RecordOwner,UserContactID, dtProductListings, WareHouseID);
                                                    objAmazonClient.ProcessOpenProductListingReport(DomainID, WebApiId, RecordOwner, UserContactID, ref dtProductListings, WareHouseID);
                                                    File.Copy(filepath, GeneralFunctions.GetPath("ClosedAmazonProductListingReport", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".txt");
                                                    File.Delete(filepath);
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                ErrorMsg = "Service1 : From ReadAmazonProductListingReport: " + "Error in reading Amazon Product listing report" + Path.GetFileNameWithoutExtension(filepath);
                                                File.Copy(filepath, GeneralFunctions.GetPath("ReadingErrorAmazonProdListingReport", DomainID) + Path.GetFileNameWithoutExtension(filepath) + "_" + DateTime.Now.ToString("yyyyMMddHHmmfff") + ".txt");
                                                File.Delete(filepath);
                                                GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ErrorMsg + " -" + ex.Message);
                                                GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ex.StackTrace);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ex.Message);
                        GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ex.Message);
                GeneralFunctions.WriteMessage(DomainID, "ERR", "Service1 : From ReadAmazonProductListingReport: " + ex.StackTrace);
            }
        }

        #region Helper Functions

        /// <summary>
        /// Enum for List of WebAPIs supported by BizAutomation
        /// </summary>
        public enum WebAPIList
        {
            //Mapping the WebApiId in WebApiDetail Table
            AmazonUS = 2,
            EBayUS = 5,
            GoogleUS = 7,
            Magento = 8
        }

        /// <summary>
        /// Deletes Log Files older than the number of days specified
        /// And Deletes Log Files thats been in Trash Folder for morethan 30 Days.
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="DaysOld">DaysOld</param>
        private void DeleteLogFiles(long DomainId, int DaysOld)
        {
            try
            {
                CreateLogFiles objLogFiles = new CreateLogFiles();
                objLogFiles.DeleteLogFiles(DomainId, DaysOld);
                objLogFiles = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// GetRoutingRulesUserCntID
        /// </summary>
        /// <param name="DomainId">DomainId</param>
        /// <param name="dbFieldName">dbFieldName</param>
        /// <param name="FieldValue">FieldValue</param>
        /// <returns></returns>
        private long GetRoutingRulesUserCntID(long DomainId, string dbFieldName, string FieldValue)
        {
            AutoRoutingRules objAutoRoutRles = new AutoRoutingRules();
            DataTable dtAutoRoutingRules = new DataTable();
            DataRow drAutoRow = null;
            DataSet ds = new DataSet();
            try
            {
                objAutoRoutRles.DomainID = DomainId;
                if (dtAutoRoutingRules.Columns.Count == 0)
                {
                    dtAutoRoutingRules.TableName = "Table";
                    dtAutoRoutingRules.Columns.Add("vcDbColumnName");
                    dtAutoRoutingRules.Columns.Add("vcDbColumnText");
                }
                dtAutoRoutingRules.Rows.Clear();
                drAutoRow = dtAutoRoutingRules.NewRow();
                drAutoRow["vcDbColumnName"] = dbFieldName;
                drAutoRow["vcDbColumnText"] = FieldValue;
                dtAutoRoutingRules.Rows.Add(drAutoRow);
                ds.Tables.Add(dtAutoRoutingRules);
                objAutoRoutRles.strValues = ds.GetXml();
                ds.Tables.Remove(ds.Tables[0]);
                return objAutoRoutRles.GetRecordOwner();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Gets the Top WebApiOrderId for the given DomainId and WebApiId
        /// </summary>
        /// <param name="DomainID">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <returns>WebApiOrderId</returns>
        private string GetTopAmazonOrderID(long DomainID, int WebApiId)
        {
            string AmazonOrderId = "";
            WebAPI objWebAPI = new WebAPI();
            objWebAPI.WebApiId = WebApiId;
            objWebAPI.DomainID = DomainID;
            AmazonOrderId = objWebAPI.GetTopWebApiOrderID();
            return AmazonOrderId;
        }

        /// <summary>
        /// Gets the Top WebApiOrderReportId for the given DomainId and WebApiId
        /// </summary>
        /// <param name="DomainID">DomainId</param>
        /// <param name="WebApiId">WebApiId</param>
        /// <returns>WebApiOrderReportId</returns>
        private DataTable GetTopOrderReportID(long DomainID, int WebApiId)
        {
            DataTable dtTopReport;
            string OrderReportID = "";
            WebAPI objWebAPI = new WebAPI();
            objWebAPI.WebApiId = WebApiId;
            objWebAPI.DomainID = DomainID;
            objWebAPI.Mode = 0;
            dtTopReport = objWebAPI.GetTopAPIOrderReport();
            return dtTopReport;
        }

        private DataTable GetTopImportOrderRequest(long DomainID, int WebApiId)
        {
            DataTable dtTopOrderImportRequest;
            WebAPI objWebAPI = new WebAPI();
            objWebAPI.WebApiId = WebApiId;
            objWebAPI.DomainID = DomainID;
            objWebAPI.Mode = 1;
            dtTopOrderImportRequest = objWebAPI.GetTopAPIOrderReport();
            return dtTopOrderImportRequest;
        }


        #endregion Helper Functions

    }
}