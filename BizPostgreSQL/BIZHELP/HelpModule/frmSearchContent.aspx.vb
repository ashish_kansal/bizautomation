﻿Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Help
Imports BACRM.BusinessLogic.Common

Partial Public Class frmSearchContent
    Inherits System.Web.UI.Page
    Public objHelp As New Help
    Dim lngHelpID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If CCommon.ToLong(Session("AccessID")) = 0 Then
            Dim Loginurl As String = ConfigurationManager.AppSettings("LoginURL")
            Loginurl = Loginurl + "?From=Help"
            ClientScript.RegisterStartupScript(Me.GetType(), "login", "top.location.href='" & Loginurl & "';", True)
            Exit Sub
        End If

        Dim blnQuery As Boolean = False

        lngHelpID = CCommon.ToLong(Request("h"))


        If Not IsPostBack Then
            LoadTree()

            If (Not Request.QueryString("pageurl") Is Nothing) Then
                ViewState("PostBackCounter") = 2
                blnQuery = True
                Dim objSearch As New DataTable
                objSearch = objHelp.GethelpDataByPageURl(CCommon.ToString(Request("pageurl")).Trim)

            End If
        End If

    End Sub


    Private Sub LoadTree()
        Dim parentNode As RadTreeNode
        Dim currentNode As RadTreeNode
        Try
            Dim dt As New DataTable
            dt = objHelp.GetHelpCategory("", 1)

            Dim strParent As String
            Dim objRow As DataRow

            strParent = ""
            For Each objRow In dt.Rows
                If strParent <> objRow.Item("vcCatecoryName") Then
                    parentNode = New RadTreeNode
                    currentNode = New RadTreeNode
                    parentNode.Text = objRow.Item("vcCatecoryName")
                    parentNode.Value = 0
                    parentNode.NavigateUrl = "#"
                    radCategories.Nodes.Add(parentNode)
                    'radCategories.Nodes(radCategories.Nodes.Count - 1).Expanded = True
                    currentNode = radCategories.Nodes(radCategories.Nodes.Count - 1)

                    parentNode = New RadTreeNode
                    parentNode.Text = objRow.Item("helpheader")
                    parentNode.Value = objRow.Item("numHelpId")
                    parentNode.NavigateUrl = "../HelpModule/loadhelp.aspx?h=" & objRow.Item("numHelpId")
                    parentNode.Target = "ContentFrame"
                    'parentNode.Expanded = True
                    currentNode.Nodes.Add(parentNode)
                    'currentNode.Expanded = True
                    strParent = objRow.Item("vcCatecoryName")
                Else
                    parentNode = New RadTreeNode
                    currentNode = New RadTreeNode
                    currentNode = radCategories.Nodes(radCategories.Nodes.Count - 1)
                    'radCategories.Nodes(radCategories.Nodes.Count - 1).Expanded = True
                    parentNode = New RadTreeNode
                    parentNode.Text = objRow.Item("helpheader")
                    parentNode.Value = objRow.Item("numHelpId")
                    parentNode.NavigateUrl = "../HelpModule/loadhelp.aspx?h=" & objRow.Item("numHelpId")
                    parentNode.Target = "ContentFrame"
                    'parentNode.Expanded = True
                    currentNode.Nodes.Add(parentNode)
                    'currentNode.Expanded = True
                End If


            Next

        Catch ex As Exception
            Throw ex
        End Try


    End Sub
    'Private Sub SearchWord(ByVal strSearch As String)
    '    Try
    '        Dim objDT As New DataTable


    '        Dim strParent As String

    '        strParent = ""

    '        objDT = objHelp.GetHelpCategory(strSearch, 2)
    '        If objDT.Rows.Count = 0 Then
    '            litHelp.Text = Server.HtmlDecode("<p class=MsoNormal><b><span style=""font-family: Arial; color: blue;""> No Results Found For Your Search Criteria </span></b></p>")
    '            Exit Sub
    '        End If

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub ContentFill(ByVal intHelpId As Integer)
    '    Dim objDt As New DataTable
    '    objDt = objHelp.GetHelpCategory("", 3, intHelpId)
    '    litHelp.Text = vbNullString
    '    If objDt.Rows.Count = 0 Then Exit Sub
    '    litHelp.Text = Server.HtmlDecode(objDt.Rows(0).Item("helpDescription"))
    '    'TextArea1.InnerHtml = "<html><body>" & objDt.Rows(0).Item("helpDescription") & "</body></html>"
    'End Sub




    'Private Sub radCategories_NodeClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadTreeNodeEventArgs) Handles radCategories.NodeClick
    '    ViewState("PostBackCounter") = 2

    '    If CCommon.ToLong(e.Node.Value) > 0 Then
    '        ContentFill(e.Node.Value)
    '    End If
    'End Sub
End Class