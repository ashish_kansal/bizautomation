﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSearchContent.aspx.vb"
    Inherits="BIZHELP.frmSearchContent" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function treeExpandAllNodes() {
            var treeView = $find("radCategories");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].expand();
                }
            }
        }

        function treeCollapseAllNodes() {
            var treeView = $find("radCategories");
            var nodes = treeView.get_allNodes();

            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].get_nodes() != null) {
                    nodes[i].collapse();
                }
            }
        }
        function FindNodeAndExpand(NodeValue) {
            var tree = $find("radCategories");
            var node1 = tree.findNodeByValue(NodeValue);
            node1.select();
            node1.get_parent().expand();
        }
        
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <table width="100%" border="0">
            <tr>
                <td align="left" valign="top" width="250px">
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" class="RadTreeView_Default">
                                <a href="javascript: treeExpandAllNodes();" style="color: Gray;">Expand All </a>
                                &nbsp; <a href="javascript: treeCollapseAllNodes();" style="color: Gray;">Collapse All
                                </a>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td>
                                <telerik:RadTreeView ID="radCategories" runat="server" AllowNodeEditing="false" Skin="Default">
                                </telerik:RadTreeView>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="left" valign="top">
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
