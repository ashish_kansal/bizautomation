﻿Imports BACRM.BusinessLogic.Help
Imports BACRM.BusinessLogic.Common
Public Class LoadHelp
    Inherits System.Web.UI.Page
    Public objHelp As New Help
    Dim lngHelpID As Long
    Dim strSearchQuery As String
    Dim strSearchbyPageName As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If CCommon.ToLong(Session("AccessID")) = 0 Then
            Dim Loginurl As String = ConfigurationManager.AppSettings("LoginURL")
            Loginurl = Loginurl + "?From=Help"
            ClientScript.RegisterStartupScript(Me.GetType(), "login", "top.location.href='" & Loginurl & "';", True)
            Exit Sub
        End If

        Dim blnQuery As Boolean = False
        lngHelpID = CCommon.ToLong(Request("h"))
        strSearchQuery = CCommon.ToString(Request("st")).Trim
        strSearchbyPageName = CCommon.ToString(Request("pagename")).Trim
        repSearchResult.Visible = False
        If Not IsPostBack Then
            If lngHelpID > 0 Then
                ContentFill(lngHelpID)
            End If
            If strSearchQuery.Length > 0 Then
                SearchWord(strSearchQuery)
            End If
            If strSearchbyPageName.Length > 0 Then
                SearchByPageName()
            End If
        End If

    End Sub
    Private Sub SearchWord(ByVal strSearch As String)
        Try
            Dim objDT As New DataTable
            Dim strArr() As String = strSearch.Split(New Char() {" "}, StringSplitOptions.RemoveEmptyEntries)

            strSearch = ""
            For i As Int16 = 0 To strArr.Length - 1
                strSearch = strSearch & """" & strArr(i) & "*"" OR "
            Next
            strSearch = strSearch.TrimEnd(New Char() {"O", "R", " "})
            objDT = objHelp.GetHelpCategory(strSearch, 2)
            If objDT.Rows.Count = 0 Then
                NoResultFound()
                Exit Sub
            Else
                repSearchResult.DataSource = objDT
                repSearchResult.DataBind()
                repSearchResult.Visible = True
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ContentFill(ByVal intHelpId As Integer)
        Dim objDt As New DataTable
        objDt = objHelp.GetHelpCategory("", 3, intHelpId)
        litHelp.Text = vbNullString
        If objDt.Rows.Count = 0 Then Exit Sub
        litHelp.Text = Server.HtmlDecode(objDt.Rows(0).Item("helpDescription"))
        'TextArea1.InnerHtml = "<html><body>" & objDt.Rows(0).Item("helpDescription") & "</body></html>"
    End Sub

    Private Sub SearchByPageName()
        Dim objDt As New DataTable
        objDt = objHelp.GetHelpCategory(strSearchbyPageName, 4)
        litHelp.Text = vbNullString
        If objDt.Rows.Count = 0 Then
            NoResultFound()
            Exit Sub
        Else
            litHelp.Text = Server.HtmlDecode(objDt.Rows(0).Item("helpDescription"))
            If Not ClientScript.IsStartupScriptRegistered("expandtreenode") Then Page.ClientScript.RegisterStartupScript(Me.GetType, "expandtreenode", "top.frames['mainframe'].frames['TreeFrame'].window.FindNodeAndExpand('" + objDt.Rows(0)("numHelpID").ToString + "')", True)
        End If
    End Sub
    Private Sub NoResultFound()
        litHelp.Text = Server.HtmlDecode("<p class=MsoNormal><b><span style=""font-family: Arial; color: blue;""> No Results Found For Your Search Criteria </span></b></p>")
    End Sub


End Class