﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="LoadHelp.aspx.vb" Inherits="BIZHELP.LoadHelp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .item
        {
            background-color: #EEE;
            font: 12px/18px "Segoe UI" ,Arial,sans-serif;
            color: Black;
            border-top: solid 1px lightBlue;
            margin-left:15px;
        }

    </style>
<script type="text/javascript" src="../Js/flowplayer-3.2.6.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:Literal ID="litHelp" runat="server"></asp:Literal>
    <asp:Repeater runat="server" ID="repSearchResult">
    <HeaderTemplate>
    Search Results..
    </HeaderTemplate>
        <ItemTemplate>
            <div class="item">
                <a href="LoadHelp.aspx?h=<%# Eval("numHelpID") %>">
                    <%# Eval("helpHeader")%></a>
                <br />
                <%# Eval("helpshortdesc")%>
            </div>
        </ItemTemplate>
        <AlternatingItemTemplate>
            <div class="item">
                <a href="LoadHelp.aspx?h=<%# Eval("numHelpID") %>">
                    <%# Eval("helpHeader")%></a>
                <br />
                <%# Eval("helpshortdesc")%>
            </div>
        </AlternatingItemTemplate>
    </asp:Repeater>
    
    <script>
        flowplayer("player", "../Player/flowplayer-3.2.7.swf");
		</script>
    </form>
</body>
</html>
