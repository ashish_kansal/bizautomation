﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Header.aspx.vb" Inherits="BIZHELP.Header" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function Search() {
            parent.frames['mainframe'].frames['ContentFrame'].location.href = 'LoadHelp.aspx?st=' + trim(document.getElementById('txtSearch').value);
        }
        function trim(str, chars) {
            return ltrim(rtrim(str, chars), chars);
        }

        function ltrim(str, chars) {
            chars = chars || "\\s";
            return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
        }

        function rtrim(str, chars) {
            chars = chars || "\\s";
            return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
        }
    </script>
    <style type="text/css">
        .search-box input
        {
            border: none;
          
        }
        #btnSearch
        {
            background: url('../Images/btnSearch.gif') no-repeat scroll 0px 1px transparent;
            cursor: pointer;
        }
        #txtSearch
        {
            width:300px;
            
        }
        
        .search-box
        {
            display: inline;
            border: 2px solid green;
            width:300px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td colspan="2" align="left">
                <a href="http://www.BizAutomation.com" target="_parent">
                    <img src="../Images/New LogoBiz1.JPG" border="0" alt="BizAutomation.com" /></a>
            </td>
            <td align="right" valign="bottom">
                <div class='search-box'>
                    <asp:TextBox runat="server" ID="txtSearch" />
                    <asp:Button Text="" ToolTip="Search" runat="server" ID="btnSearch" OnClientClick="Search();" />
                </div>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
