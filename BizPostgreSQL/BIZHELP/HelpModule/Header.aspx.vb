﻿Imports BACRM.BusinessLogic.Common

Public Class Header
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If CCommon.ToLong(Session("AccessID")) = 0 Then
            Dim Loginurl As String = ConfigurationManager.AppSettings("LoginURL")
            Loginurl = Loginurl + "?From=Help"
            ClientScript.RegisterStartupScript(Me.GetType(), "login", "top.location.href='" & Loginurl & "';", True)
            Exit Sub
        End If
        txtSearch.Attributes.Add("onkeydown", "if(event.which || event.keyCode){if ((event.which == 13) || (event.keyCode == 13)) {document.getElementById('" + btnSearch.ClientID + "').click();return false;}} else {return true}; ")
    End Sub

End Class