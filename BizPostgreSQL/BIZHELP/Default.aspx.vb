﻿Imports BACRM.BusinessLogic.Help
Imports BACRM.BusinessLogic.Common
Public Class _Default
    Inherits System.Web.UI.Page
    Public objHelp As New Help
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim objCommon As New CCommon()
            Try
                If CCommon.ToString(Request("a")).Length > 1 Then
                    objHelp.numAccessId = CCommon.ToLong(objCommon.Decrypt(CCommon.ToString(Request("a"))))
                ElseIf CCommon.ToLong(Session("AccessID")) > 0 Then
                    objHelp.numAccessId = CCommon.ToLong(Session("AccessID"))
                Else
                    objHelp.numAccessId = CCommon.ToLong(objCommon.Decrypt(CCommon.ToString(Request("a"))))
                End If
            Catch ex As Exception
                objHelp.numAccessId = 0
            End Try

            Dim blnSuccess As Boolean = objHelp.GetHelpLogin()
            Dim Loginurl As String = ConfigurationManager.AppSettings("LoginURL")
            If blnSuccess = False Then
                Response.Redirect(Loginurl + "?From=Help&pageurl=" & CCommon.ToString(Request("pageurl")), False)
            Else
                Session("AccessID") = objHelp.numAccessId
                If CCommon.ToString(Request("a")).Length > 1 Then
                    Response.Redirect(Request.Url.ToString().Split("?")(0) & "?pageurl=" & Request("pageurl"), False)
                End If
            End If

        End If

    End Sub

End Class