﻿Imports BACRM.BusinessLogic.Notification
Imports System.Configuration
Imports System.IO
Imports BACRM.BusinessLogic.Common
Imports System.Net
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Admin
Imports System.Xml
Imports BACRM.BusinessLogic.Accounting

Public Class CularisClient

    Private _EnableAPI As Boolean
    Public Property EnableAPI() As Boolean
        Get
            Return _EnableAPI
        End Get
        Set(ByVal value As Boolean)
            _EnableAPI = value
        End Set
    End Property

    Private _APIURL As String
    Public Property APIURL() As String
        Get
            Return _APIURL
        End Get
        Set(ByVal value As String)
            _APIURL = value
        End Set
    End Property

    Private _UserKey As String
    Public Property UserKey() As String
        Get
            Return _UserKey
        End Get
        Set(ByVal value As String)
            _UserKey = value
        End Set
    End Property

    Private _SectionKey As String
    Public Property SectionKey() As String
        Get
            Return _SectionKey
        End Get
        Set(ByVal value As String)
            _SectionKey = value
        End Set
    End Property

    Private _Token As String
    Public Property Token() As String
        Get
            Return _Token
        End Get
        Set(ByVal value As String)
            _Token = value
        End Set
    End Property

    Private _DomainID As Long
    Public Property DomainID() As Long
        Get
            Return _DomainID
        End Get
        Set(ByVal Value As Long)
            _DomainID = Value
        End Set
    End Property

    Private _WareHouseID As Long
    Public Property WareHouseID() As Long
        Get
            Return _WareHouseID
        End Get
        Set(ByVal value As Long)
            _WareHouseID = value
        End Set
    End Property

    Private _UserCntID As Long
    Public Property UserCntID() As Long
        Get
            Return _UserCntID
        End Get
        Set(ByVal value As Long)
            _UserCntID = value
        End Set
    End Property

    Private _COGSChartAcntId As Long
    Public Property COGSChartAcntId() As Long
        Get
            Return _COGSChartAcntId
        End Get
        Set(ByVal value As Long)
            _COGSChartAcntId = value
        End Set
    End Property

    Private _AssetChartAcntId As Long
    Public Property AssetChartAcntId() As Long
        Get
            Return _AssetChartAcntId
        End Get
        Set(ByVal value As Long)
            _AssetChartAcntId = value
        End Set
    End Property

    Private _IncomeChartAcntId As Long
    Public Property IncomeChartAcntId() As Long
        Get
            Return _IncomeChartAcntId
        End Get
        Set(ByVal value As Long)
            _IncomeChartAcntId = value
        End Set
    End Property

    Private _DateCreated As String
    Public Property DateCreated() As String
        Get
            Return _DateCreated
        End Get
        Set(ByVal value As String)
            _DateCreated = value
        End Set
    End Property

    Private _DateModified As String
    Public Property DateModified() As String
        Get
            Return _DateModified
        End Get
        Set(ByVal value As String)
            _DateModified = value
        End Set
    End Property

    Private _WebApiId As Integer
    Public Property WebApiId() As Integer
        Get
            Return _WebApiId
        End Get
        Set(ByVal value As Integer)
            _WebApiId = value
        End Set
    End Property

    Private _order_id As Long
    Public Property order_id() As Long
        Get
            Return _order_id
        End Get
        Set(ByVal value As Long)
            _order_id = value
        End Set
    End Property

    Private _sku As String
    Public Property sku() As String
        Get
            Return _sku
        End Get
        Set(ByVal value As String)
            _sku = value
        End Set
    End Property

    Private _ProductName As String
    Public Property ProductName() As String
        Get
            Return _ProductName
        End Get
        Set(ByVal value As String)
            _ProductName = value
        End Set
    End Property

    Private _Price As Double
    Public Property Price() As Double
        Get
            Return _Price
        End Get
        Set(ByVal value As Double)
            _Price = value
        End Set
    End Property

    Private _Weight As Double
    Public Property Weight() As Double
        Get
            Return _Weight
        End Get
        Set(ByVal value As Double)
            _Weight = value
        End Set
    End Property

    Private _QtyOnHand As Long
    Public Property QtyOnHand() As Long
        Get
            Return _QtyOnHand
        End Get
        Set(ByVal value As Long)
            _QtyOnHand = value
        End Set
    End Property

    Private _ProductKey As String
    Public Property ProductKey() As String
        Get
            Return _ProductKey
        End Get
        Set(ByVal value As String)
            _ProductKey = value
        End Set
    End Property

    Private _Product_Id As String
    Public Property Product_Id() As String
        Get
            Return _Product_Id
        End Get
        Set(ByVal value As String)
            _Product_Id = value
        End Set
    End Property

    Public Function Login(ByVal APIURL As String) As String
        Try
            strAPIURL = "http://www." + APIURL + ".com/api/rest/?method=auth.login&user=xGrlz5gLzN8M&section=B0U3sxywE4oH"
            response = WebAPI.GetResponse(strAPIURL)
            If (response.IndexOf("<token>") > 0) Then
                Dim start As Integer = response.IndexOf("<token>", StringComparison.OrdinalIgnoreCase)
                Return response.Substring(start + 7, 12)
            End If
            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' http://docs.com/api-access/orders/ordersget_orders/
    ''' </summary>
    ''' <remarks></remarks>
    Public Function GetOrders() As String
        Try
            strAPIURL = "http://www." + _APIURL + ".com/api/rest/?method=orders.get_orders&token=" & _Token & "&section=" & _SectionKey & IIf(_DateCreated.Length > 0, "&date=" + _DateCreated, "") & IIf(_order_id > 0, "&order_id=" & _order_id, "")
            response = WebAPI.GetResponse(strAPIURL)
            Return response
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function GetProducts() As String
        Try
            strAPIURL = "http://www." + _APIURL + ".com/api/rest/?method=products.get_products&token=" & _Token & "&section=" & _SectionKey & IIf(_DateCreated.Length > 0, "&date_created=" + _DateCreated, "") & IIf(_DateModified.Length > 0, "&date_modified=" + _DateModified, "")
            response = WebAPI.GetResponse(strAPIURL)
            Return response
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function AddProduct() As Long
        Try
            strAPIURL = "http://www." + _APIURL + ".com/api/rest/?method=products.add_product&token=" & _Token & "&section=" & _SectionKey & "&sku=" & WebAPI.UrlEncode(_sku) & "&name=" & WebAPI.UrlEncode(_ProductName) & "&price=" & _Price & "&lbs=" & _Weight & "&key=" & _ProductKey & "&inventory=" & _QtyOnHand & "&status=active"
            response = WebAPI.GetResponse(strAPIURL)
            If response.Contains("<success>1</success>") Then
                Dim intStart As Integer = response.IndexOf("<product_id>", StringComparison.OrdinalIgnoreCase)
                Dim intEnd As Integer = response.IndexOf("</product_id>", StringComparison.OrdinalIgnoreCase)
                If intStart > 0 And intEnd > 0 Then
                    Return response.Substring(intStart + 12, intEnd - intStart - 12)
                End If
            End If
            Return 0
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function UpdateProduct() As Long
        Try
            'strAPIURL = "http://www." + _APIURL + ".com/api/rest/?method=products.update_product&token=" & _Token & "&section=" & _SectionKey & "&product=" & _Product_Id & "&sku=" & _sku & "&name=" & _ProductName & "&price=" & _Price & "&lbs=" & _Weight & "&key=" & _ProductKey & "&inventory=" & _QtyOnHand & "&status=active"
            'Auto Update is enabled
            strAPIURL = "http://www." + _APIURL + ".com/api/rest/?method=products.add_product&auto_update=1&token=" & _Token & "&section=" & _SectionKey & "&sku=" & WebAPI.UrlEncode(_sku) & "&name=" & WebAPI.UrlEncode(_ProductName) & "&price=" & _Price & "&lbs=" & _Weight & "&key=" & _ProductKey & "&inventory=" & _QtyOnHand  ' "&status=active" Removed when requested by A&E Cage Co.
            response = WebAPI.GetResponse(strAPIURL)
            If response.Contains("<success>1</success>") Then
                Dim intStart As Integer = response.IndexOf("<product_id>", StringComparison.OrdinalIgnoreCase)
                Dim intEnd As Integer = response.IndexOf("</product_id>", StringComparison.OrdinalIgnoreCase)
                If intStart > 0 And intEnd > 0 Then
                    Return response.Substring(intStart + 12, intEnd - intStart - 12)
                End If
            End If
            Return 0
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Public Function UpdateProductInventory() As Boolean
        Try
            strAPIURL = "http://www." + _APIURL + ".com/api/rest/?method=products.update_product_inventory&token=" & _Token & "&section=" & _SectionKey & "&product=" & _Product_Id & "&inventory=" & _QtyOnHand & "&inventory_change=add"
            response = WebAPI.GetResponse(strAPIURL)
            Return response.Contains("<success>1</success>")
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Dim OrderDoc As New XmlDocument
    Dim OrderList As XmlNodeList
    Dim strOppId As String

    Public Sub SaveOrders(ByVal strOrders As String)
        Try
            Dim blSucessFlag As Boolean = False
            OrderDoc.LoadXml(strOrders)
            OrderList = OrderDoc.GetElementsByTagName("order")
            Dim i As Integer = OrderList.Count - 1
            While i <> -1
                Try ' On Error skip current order and Continue processing next order
                    If OrderList(i)("status").InnerXml.ToLower = "new" Then
                        strOppId = AddOpportunity(OrderList(i))
                        WriteMessage("OppID=" + strOppId)
                        If CInt(strOppId) > 0 Then blSucessFlag = True
                    End If
                Catch ex As Exception
                    WriteMessage("Order No " & OrderList(i).Attributes("id").InnerXml() & " Throwed Error:" & ex.Message)
                End Try
                i = i - 1
            End While

            If blSucessFlag = True Then
                objWebAPI.UpdateAPISettingsDate(1)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Dim lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId As Long
    Dim objItem As New CItems
    Dim objContacts As New CContacts
    Dim dtItems As New DataTable
    Dim objLeads As New CLeads
    Dim objWebAPI As New WebAPI
    Dim BillingDoc As New XmlDocument
    Dim objOpportunity As New MOpportunity
    Dim objImpWzd As New ImportWizard
    Dim ShippingDoc As New XmlDocument
    Dim ShippingList As XmlNodeList
    Dim BillingList As XmlNodeList
    Dim ShippingNode As XmlNode
    Dim BillingNode As XmlNode
    Dim TotalAmount As Decimal = 0
    Dim dsItems As New DataSet
    Dim dtMilestone As DataTable
    Dim dsNew As New DataSet
    Dim objOppBizDocs As New OppBizDocs
    Dim arrOutPut() As String
    Dim arrBillingIDs(3) As String
    Public Function AddOpportunity(ByVal OrderXML As XmlNode) As String
        Try
            objWebAPI.WebApiId = _WebApiId
            objWebAPI.DomainId = _DomainID
            objWebAPI.vcAPIOppId = OrderXML.Attributes("id").InnerXml
            If (objWebAPI.CheckDuplicateAPIOpportunity() = True) Then
                CreateItemDataSet(OrderXML, dtItems)
                'Start Insert Prospect
                BillingDoc.LoadXml(OrderXML.OuterXml)
                BillingList = BillingDoc.GetElementsByTagName("billing")
                BillingNode = BillingList(0)
                ShippingDoc.LoadXml(OrderXML.OuterXml)
                ShippingList = ShippingDoc.GetElementsByTagName("shipping")
                ShippingNode = ShippingList(1)

                If BillingList.Count > 0 And ShippingList.Count > 0 Then
                    'Check Existing Contact with same email ID
                    If (BillingNode("first").InnerXml & BillingNode("last").InnerXml).Length > 0 Then
                        objLeads.Email = BillingNode("email").InnerXml
                        objLeads.DomainID = _DomainID
                        objLeads.GetConIDCompIDDivIDFromEmail()
                        If objLeads.ContactID > 0 And objLeads.DivisionID > 0 Then
                            lngCntID = objLeads.ContactID
                            lngDivId = objLeads.DivisionID
                        Else
                            With objLeads
                                .CompanyName = BillingNode("company").InnerXml
                                .CustName = BillingNode("first").InnerXml + " " + BillingNode("last").InnerXml
                                .CRMType = 1
                                .DivisionName = "-"
                                .LeadBoxFlg = 1
                                .UserCntID = GetRoutingRulesUserCntID("vcCompanyName", BillingNode("company").InnerXml)
                                .Country = objImpWzd.GetStateAndCountry(0, BillingNode("country").InnerXml, _DomainID)
                                .SCountry = objImpWzd.GetStateAndCountry(0, ShippingNode("country").InnerXml, _DomainID) 'shipping country 'Session("DefCountry")
                                .FirstName = BillingNode("first").InnerXml
                                .LastName = BillingNode("last").InnerXml
                                .ContactPhone = BillingNode("phone").InnerXml
                                .PhoneExt = ""
                                .Email = BillingNode("email").InnerXml
                                .DomainID = _DomainID
                                .ContactType = 70
                                .UpdateDefaultTax = False
                                .NoTax = False
                            End With
                            objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                            lngDivId = objLeads.CreateRecordDivisionsInfo
                            objLeads.ContactID = 0
                            objLeads.DivisionID = lngDivId
                            lngCntID = objLeads.CreateRecordAddContactInfo()

                        End If
                        objContacts.BillStreet = BillingNode("address_1").InnerXml + " " + BillingNode("address_2").InnerXml
                        objContacts.BillCity = BillingNode("city").InnerXml
                        objContacts.BillState = objImpWzd.GetStateAndCountry(1, BillingNode("region").InnerXml, _DomainID)
                        objContacts.BillCountry = objImpWzd.GetStateAndCountry(0, BillingNode("country").InnerXml, _DomainID)
                        objContacts.BillPostal = BillingNode("postal_code").InnerXml
                        objContacts.BillingAddress = 0 'shipping address is not same as billing 


                        objContacts.ShipStreet = ShippingNode("address_1").InnerXml + " " + ShippingNode("address_2").InnerXml
                        objContacts.ShipState = objImpWzd.GetStateAndCountry(1, ShippingNode("region").InnerXml, _DomainID)
                        objContacts.ShipPostal = ShippingNode("postal_code").InnerXml
                        objContacts.ShipCountry = objImpWzd.GetStateAndCountry(0, ShippingNode("country").InnerXml, _DomainID)
                        objContacts.ShipCity = ShippingNode("city").InnerXml

                        objContacts.DivisionID = lngDivId
                        objContacts.UpdateCompanyAddress()
                        SaveTaxTypes()
                        arrBillingIDs(0) = objLeads.CompanyID
                        arrBillingIDs(1) = lngDivId
                        arrBillingIDs(2) = lngCntID
                        arrBillingIDs(3) = objLeads.CompanyName
                        If lngCntID = 0 Then
                            Exit Function
                        End If
                        objLeads.CompanyID = 0
                        lngDivId = 0
                        lngCntID = 0
                        objLeads.CompanyName = ""


                        ''--------------------------------------------------------------------------------------------------------------------------------------------------------------------
                        ''Create whole new company outof shipping Conatct Whether email is provided or not.
                        '' -Reason to add this clause is to faciliate dropship kind of order
                        '' Shipping Fname and Lname are mendatory
                        'If (ShippingNode("first").InnerXml & ShippingNode("last").InnerXml).Length > 0 Then
                        '    objLeads = New CLeads()
                        '    If ShippingNode("email").InnerXml.ToLower = BillingNode("email").InnerXml.ToLower Then
                        '        objLeads.Email = ""
                        '    Else
                        '        objLeads.Email = ShippingNode("email").InnerXml
                        '    End If
                        '    objLeads.DomainID = _DomainID
                        '    'objLeads.GetConIDCompIDDivIDFromEmail()
                        '    'If objLeads.ContactID > 0 And objLeads.DivisionID > 0 Then
                        '    '    lngCntID = objLeads.ContactID
                        '    '    lngDivId = objLeads.DivisionID
                        '    'Else
                        '    With objLeads
                        '        .CompanyName = ShippingNode("company").InnerXml
                        '        .CustName = ShippingNode("first").InnerXml + " " + ShippingNode("last").InnerXml
                        '        .CRMType = 1
                        '        .DivisionName = "-"
                        '        .LeadBoxFlg = 1
                        '        .UserCntID = GetRoutingRulesUserCntID("vcCompanyName", ShippingNode("company").InnerXml)
                        '        .Country = objImpWzd.GetStateAndCountry(0, ShippingNode("country").InnerXml, _DomainID)
                        '        .SCountry = objImpWzd.GetStateAndCountry(0, ShippingNode("country").InnerXml, _DomainID) 'shipping country 'Session("DefCountry")
                        '        .FirstName = ShippingNode("first").InnerXml
                        '        .LastName = ShippingNode("last").InnerXml
                        '        .Phone = ShippingNode("phone").InnerXml
                        '        .PhoneExt = ""
                        '        .ContactType = 70 '4410 ' For Production its 14427
                        '        .UpdateDefaultTax = False
                        '        .NoTax = False
                        '    End With
                        '    objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                        '    lngDivId = objLeads.CreateRecordDivisionsInfo
                        '    objLeads.ContactID = 0
                        '    objLeads.DivisionID = lngDivId
                        '    lngCntID = objLeads.CreateRecordAddContactInfo()

                        '    'End If
                        '    objContacts.BillStreet = ShippingNode("address_1").InnerXml + " " + ShippingNode("address_2").InnerXml
                        '    objContacts.BillCity = ShippingNode("city").InnerXml
                        '    objContacts.BillState = objImpWzd.GetStateAndCountry(1, ShippingNode("region").InnerXml, _DomainID)
                        '    objContacts.BillCountry = objImpWzd.GetStateAndCountry(0, ShippingNode("country").InnerXml, _DomainID)
                        '    objContacts.BillPostal = ShippingNode("postal_code").InnerXml
                        '    objContacts.BillingAddress = 0 'shipping address is not same as billing 


                        '    objContacts.ShipStreet = ShippingNode("address_1").InnerXml + " " + ShippingNode("address_2").InnerXml
                        '    objContacts.ShipState = objImpWzd.GetStateAndCountry(1, ShippingNode("region").InnerXml, _DomainID)
                        '    objContacts.ShipPostal = ShippingNode("postal_code").InnerXml
                        '    objContacts.ShipCountry = objImpWzd.GetStateAndCountry(0, ShippingNode("country").InnerXml, _DomainID)
                        '    objContacts.ShipCity = ShippingNode("city").InnerXml

                        '    objContacts.DivisionID = lngDivId
                        '    objContacts.UpdateCompanyAddress()
                        '    SaveTaxTypes()
                        'End If


                    End If
                    'End Insert Prospect

                    objLeads.CompanyID = arrBillingIDs(0)
                    lngDivId = arrBillingIDs(1)
                    lngCntID = arrBillingIDs(2)
                    objLeads.CompanyName = arrBillingIDs(3)

                    objOpportunity.OpportunityId = 0
                    objOpportunity.ContactID = lngCntID
                    objOpportunity.DivisionID = lngDivId
                    objOpportunity.UserCntID = GetRoutingRulesUserCntID("vcBilState", objContacts.BillState)
                    objOpportunity.OpportunityName = objLeads.CompanyName & "-SO-" & Format(Now(), "MMMM")
                    objOpportunity.EstimatedCloseDate = Date.UtcNow 'DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    objOpportunity.PublicFlag = 0
                    objOpportunity.DomainID = _DomainID
                    objOpportunity.OppType = 1
                    dsItems.Tables.Clear()
                    dsItems.Tables.Add(dtItems.Copy())
                    If dsItems.Tables(0).Rows.Count > 0 Then
                        TotalAmount = CType(dsItems.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)
                        objOpportunity.strItems = IIf(dtItems.Rows.Count > 0, "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsItems.GetXml, "")
                    End If
                    objOpportunity.vcSource = "http://www." + _APIURL + ".com"
                    arrOutPut = objOpportunity.SaveFromAPI()
                    lngOppId = arrOutPut(0)

                    objOpportunity.OpportunityId = lngOppId
                    dtMilestone = objOpportunity.BusinessProcessByOpID
                    dtMilestone.Rows(1).Item("bitStageCompleted") = 1
                    dtMilestone.Rows(1).Item("bintStageComDate") = Date.UtcNow
                    objOpportunity.bytemode = 0
                    dtMilestone.TableName = "Table"
                    dsNew.Tables.Add(dtMilestone.Copy)
                    objOpportunity.strMilestone = dsNew.GetXml
                    dsNew.Tables.Remove(dsNew.Tables(0))
                    objOpportunity.SaveMilestone()

                    'AddingBizDocs
                    _UserCntID = GetRoutingRulesUserCntID("vcBilState", objContacts.BillState)
                    'WriteMessage("create bizdsoc")
                    'WriteMessage(OrderXML.Attributes("id").InnerXml.ToString())
                    CreateOppBizDoc(objOppBizDocs, OrderXML)
                    'WriteMessage("create bizdsoc done")

                    'Map new OppId to Cularis Order ID
                    objWebAPI.WebApiId = _WebApiId
                    objWebAPI.DomainId = _DomainID
                    objWebAPI.UserCntId = _UserCntID
                    objWebAPI.OppId = lngOppId
                    objWebAPI.vcAPIOppId = OrderXML.Attributes("id").InnerXml
                    objWebAPI.AddAPIopportunity()

                    'Following change is done for problem of shipping CompanyName in case of dropship
                    'if bitDropShip = true then do following 

                    objOpportunity.OpportunityId = lngOppId
                    objOpportunity.bytemode = 1 'Will update shipping Address in OppAddress and tintShipToType=2 in OppMaster
                    'objOpportunity.CompanyID = objLeads.CompanyID 'Don't pass 
                    objOpportunity.BillCompanyName = ShippingNode("first").InnerXml + " " + ShippingNode("last").InnerXml
                    objOpportunity.BillStreet = objContacts.ShipStreet
                    objOpportunity.BillCity = objContacts.ShipCity
                    objOpportunity.BillPostal = objContacts.ShipPostal
                    If objContacts.ShipState > 0 Then objOpportunity.BillState = objContacts.ShipState
                    objOpportunity.BillCountry = objContacts.ShipCountry
                    objOpportunity.UpdateOpportunityAddress()

                End If
            End If
            Return lngOppId.ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Dim ItemDoc As New XmlDocument
    Dim ItemList As XmlNodeList
    Dim drItem As DataRow
    Dim ItemNode As XmlNode
    Private Sub CreateItemDataSet(ByVal OrderXML As XmlNode, ByVal dtItems As DataTable)
        Try
            If dtItems.Columns.Count = 0 Then
                dtItems.Columns.Add("numoppitemtCode")
                dtItems.Columns.Add("numItemCode")
                dtItems.Columns.Add("numUnitHour")
                dtItems.Columns.Add("monPrice")
                dtItems.Columns.Add("monTotAmount", GetType(Double))
                dtItems.Columns.Add("vcItemDesc")
                dtItems.Columns.Add("numWarehouseItmsID")
                dtItems.Columns.Add("vcItemName")
                dtItems.Columns.Add("ItemType")
                dtItems.Columns.Add("Weight")
                dtItems.Columns.Add("Op_Flag")
                dtItems.Columns.Add("DropShip", GetType(Boolean))
                dtItems.Columns.Add("WebApiId", GetType(Integer))
                dtItems.TableName = "Item"
            End If
            ItemDoc.LoadXml(OrderXML.OuterXml)
            ItemList = ItemDoc.GetElementsByTagName("item")
            dtItems.Rows.Clear()
            For i As Integer = 0 To ItemList.Count - 1
                ItemNode = ItemList(i)
                If IsNumeric(ItemNode("key").InnerXml) Then
                    drItem = dtItems.NewRow
                    drItem("numoppitemtCode") = (i + 1).ToString()
                    drItem("numItemCode") = ItemNode("key").InnerXml
                    drItem("numUnitHour") = ItemNode("quantity").InnerXml
                    drItem("monPrice") = ItemNode("price").InnerXml
                    drItem("monTotAmount") = (ItemNode("price").InnerXml * ItemNode("quantity").InnerXml)
                    drItem("vcItemDesc") = "" 'Passing blank as Cularis contains HTML Tables and which creates problem while showing it in products and services 'ItemNode("description").InnerXml 
                    drItem("numWarehouseItmsID") = 0 'will be given from procedure
                    drItem("vcItemName") = ItemNode("name").InnerXml
                    drItem("ItemType") = "P"
                    drItem("Weight") = ItemNode("lbs").InnerXml
                    drItem("Op_Flag") = 1
                    drItem("DropShip") = False
                    drItem("WebApiId") = 1 'WebAPIID = 1 = Cularis -> Added to identify Actual ItemCode from Supplied ItemID
                    dtItems.Rows.Add(drItem)
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim objAutoRoutRles As New AutoRoutingRules
    Dim dtAutoRoutingRules As New DataTable
    Dim drAutoRow As DataRow
    Dim ds As New DataSet
    Private Function GetRoutingRulesUserCntID(ByVal dbFieldName As String, ByVal FieldValue As String) As Long
        Try
            objAutoRoutRles.DomainID = _DomainID
            If dtAutoRoutingRules.Columns.Count = 0 Then
                dtAutoRoutingRules.TableName = "Table"
                dtAutoRoutingRules.Columns.Add("vcDbColumnName")
                dtAutoRoutingRules.Columns.Add("vcDbColumnText")
            End If

            dtAutoRoutingRules.Rows.Clear()
            drAutoRow = dtAutoRoutingRules.NewRow
            drAutoRow("vcDbColumnName") = dbFieldName
            drAutoRow("vcDbColumnText") = FieldValue
            dtAutoRoutingRules.Rows.Add(drAutoRow)

            ds.Tables.Add(dtAutoRoutingRules)
            objAutoRoutRles.strValues = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))

            Return objAutoRoutRles.GetRecordOwner()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Dim dtOppBiDocItems As DataTable
    Dim dtOppBiDocDtl As DataTable
    Dim objJournalEntries As New JournalEntry
    Dim i As Integer
    Dim lngBizDoc As Long
    Private Sub CreateOppBizDoc(ByVal objOppBizDocs As OppBizDocs, ByRef OrderNode As XmlNode)
        Try
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.OppType = 1
            objOppBizDocs.UserCntID = _UserCntID
            objOppBizDocs.DomainID = _DomainID
            objOppBizDocs.vcPONo = ""
            objOppBizDocs.vcComments = ""
            objOppBizDocs.ShipCost = IIf(IsNumeric(OrderNode("shipping").InnerXml), OrderNode("shipping").InnerXml, 0)


            lngBizDoc = objOppBizDocs.GetAuthorizativeOpportuntiy()
            If lngBizDoc = 0 Then Exit Sub
            objOppBizDocs.BizDocId = lngBizDoc
            objOppBizDocs.OppBizDocId = 0
            'objOppBizDocs.bitPartialShipment = True 'added on carl's request 
            OppBizDocID = objOppBizDocs.SaveBizDoc()

            objOppBizDocs.OppBizDocId = OppBizDocID
            ds.Tables.Clear()
            ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting()
            dtOppBiDocItems = ds.Tables(0)


            Dim objCalculateDealAmount As New CalculateDealAmount
            objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, _DomainID, dtOppBiDocItems)



            JournalId = SaveDataToHeader(objCalculateDealAmount.GrandTotal)

            objJournalEntries.SaveJournalEntriesSales(lngOppId, _DomainID, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, objCalculateDealAmount.DivisionID, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
            ds.Tables.Clear()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim objProspects As New CProspects
    Dim dtCompanyTaxTypes As New DataTable
    Dim strdetails As String
    Dim drtax As DataRow
    Sub SaveTaxTypes()
        Try
            If dtCompanyTaxTypes.Columns.Count = 0 Then
                dtCompanyTaxTypes.Columns.Add("numTaxItemID")
                dtCompanyTaxTypes.Columns.Add("bitApplicable")
            End If

            drtax = dtCompanyTaxTypes.NewRow
            drtax("numTaxItemID") = 0
            drtax("bitApplicable") = 1
            dtCompanyTaxTypes.Rows.Add(drtax)

            dtCompanyTaxTypes.TableName = "Table"
            ds.Tables.Add(dtCompanyTaxTypes)
            strdetails = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))
            dtCompanyTaxTypes.Rows.Clear()

            objProspects.DivisionID = lngDivId
            objProspects.strCompanyTaxTypes = strdetails
            objProspects.ManageCompanyTaxTypes()
            ds.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim ProductDoc As New XmlDocument
    Dim ProductNode As XmlNode
    Dim ProductList As XmlNodeList
    Dim strAPIURL As String
    Dim response As String
    Dim blflag As Boolean = False
    Public Sub AddLatestProducts()
        Try
            'Get all Latest product from Inventory which were Added/modified recently 
            objWebAPI.DomainId = _DomainID
            objWebAPI.WebApiId = _WebApiId
            objWebAPI.DateCreated = _DateCreated
            objWebAPI.DateModified = _DateModified
            dtItems = objWebAPI.GetItemsApi()
            WriteMessage("No of Products Received:" & dtItems.Rows.Count)
            For Each dr As DataRow In dtItems.Rows
                If dr("vcModelID").ToString().Trim().Length > 0 And dr("vcItemName").ToString().Trim().Length > 0 Then
                    sku = dr("vcModelID").ToString()
                    ProductName = dr("vcItemName").ToString()
                    Price = dr("monListPrice")
                    Weight = dr("intWeight")
                    ProductKey = dr("numItemCode")
                    QtyOnHand = dr("QtyOnHand")
                    If CInt(dr("vcAPIItemID")) > 0 Then
                        'Update Product to cularis
                        Product_Id = UpdateProduct() 'auto update true
                    Else
                        'Add Product
                        Product_Id = AddProduct()
                    End If
                    If _Product_Id > 0 Then
                        WriteMessage(Product_Id.ToString())
                        'update linking ID 
                        'UpdateProductInventory()
                        objWebAPI.ItemCode = dr("numItemCode")
                        objWebAPI.Product_Id = _Product_Id
                        objWebAPI.UserCntId = GetRoutingRulesUserCntID("", "")
                        objWebAPI.AddItemAPILinking()
                        blflag = True
                    End If
                End If
            Next
            If blflag Then
                objWebAPI.UpdateAPISettingsDate(0)
            End If
            dtItems.Rows.Clear()
            dtItems.Columns.Clear()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub SaveLatestProducts()
        Try
            response = GetProducts()
            ProductDoc.LoadXml(response)
            ProductList = ProductDoc.GetElementsByTagName("product")
            WriteMessage("No of Products Received:" + ProductList.Count.ToString())

            For i As Integer = 0 To ProductList.Count - 1
                ProductNode = ProductList(i)
                objItem.ApiName = "cularis"
                objItem.WebApiId = _WebApiId
                objItem.ApiItemID = ProductList(i).Attributes("id").InnerXml
                objItem.ItemCode = 0
                objItem.ItemName = ProductNode("name").InnerXml
                objItem.ItemDesc = ProductNode("description").InnerText
                objItem.ItemType = "P"
                'pass “P” if Inventory
                'pass “N” if Non-Inventory
                'pass “S” if Service

                objItem.ListPrice = ProductNode("price").InnerXml
                objItem.ItemClassification = 0
                objItem.Taxable = False
                objItem.SKU = ProductNode("sku").InnerXml
                objItem.KitParent = False
                objItem.DateEntered = ProductNode("date").InnerXml
                objItem.DomainID = _DomainID
                objItem.UserCntId = GetRoutingRulesUserCntID("", "")
                objItem.bitSerialized = False
                objItem.COGSChartAcntId = _COGSChartAcntId
                objItem.AssetChartAcntId = _AssetChartAcntId
                objItem.IncomeChartAcntId = _IncomeChartAcntId
                'objItem.Weight = ""
                'objItem.Length = ""
                'objItem.Height = ""
                objItem.ManageItemsAndKits()
                If objItem.ItemCode > 0 And _WareHouseID > 0 Then
                    objItem.WarehouseID = _WareHouseID
                    objItem.WareHouseItemID = objItem.AddWareHouseForitems()
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Dim lntJournalId As Integer

    Dim lobjAuthoritativeBizDocs As New AuthoritativeBizDocs

    Private Function SaveDataToHeader(ByVal GrandTotal As Decimal) As Integer
        Try
            With lobjAuthoritativeBizDocs
                .Amount = GrandTotal
                .DomainId = _DomainID
                .JournalId = 0
                .UserCntID = _UserCntID
                .OppBIzDocID = OppBizDocID
                .OppId = lngOppId
                lntJournalId = .SaveDataToJournalEntryHeaderForAuthoritativeBizDocs
                Return lntJournalId
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Shared Sub WriteMessage(ByVal Message As String)
        Try
            WebAPI.WriteMessage(Message, System.Configuration.ConfigurationManager.AppSettings("LogFilePath"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'direct URL for test 
    'http://www.aecageco.com/api/rest/?method=products.add_product&token=g251rF9usSNM&section=B0U3sxywE4oH&sku=&
    'http://www.aecageco.com/api/rest/?method=products.get_product&token=g251rF9usSNM&section=B0U3sxywE4oH&product=1845
    'http://www.aecageco.com/api/rest/?method=products.get_product_by_key&token=g251rF9usSNM&section=B0U3sxywE4oH&key=1745
    'http://www.aecageco.com/api/rest/?method=orders.get_orders&token=g251rF9usSNM&section=B0U3sxywE4oH&order_id=23
End Class
