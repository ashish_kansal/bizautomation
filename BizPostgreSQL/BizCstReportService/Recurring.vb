﻿Imports BACRM.BusinessLogic.Notification
Imports System.Configuration
Imports BACRM.BusinessLogic
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities
Public Class Recurring
    Dim objNotification As New Notification
    Dim OppType As Long
    Dim JournalId As Integer
    Dim lngDomainId, lngOppId, lngOppBizDocId, lngUserCntID, lngDivisionID As Long
    Dim objJournalEntries As New JournalEntry
    Dim ds As New DataSet
    Dim dtRecurring, dtOppBiDocDtl, dtOppBiDocItems As DataTable
    Dim objOppBizDocs As New Opportunities.OppBizDocs

    Public Sub CreateRecurringOpportunity()
        Try
            objNotification.CreateRecurringForOpportunity()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub CreateRecurringBizDocs()
        Try
            'get all recurring biz docs for today.-- [USP_CreateRecurringOpportunityBizDocs]
            ds = objNotification.GetRecurringOpportunityBizDocs()
            If ds.Tables(0).Rows.Count > 0 Then
                dtRecurring = ds.Tables(0)
                For Each dr As DataRow In dtRecurring.Rows
                    lngOppId = dr("numOppId")
                    lngUserCntID = dr("numRecordOwner")
                    lngDomainId = dr("numDomainId")
                    lngDivisionID = dr("numDivisionID")
                    objNotification.OppId = lngOppId
                    objNotification.CreateRecurringOpportunityBizDocs() 'execute procedure USP_RecurringForBizDocs with perticular OppId 
                    lngOppBizDocId = objNotification.OppBizDocId
                    If lngOppBizDocId > 0 Then 'If successfull then Returns lngOppBizDocId 
                        ' Create accounting entries for BizDoc 
                        objOppBizDocs.OppBizDocId = lngOppBizDocId
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                        dtOppBiDocItems = ds.Tables(0)
                        objOppBizDocs.OppId = lngOppId
                        objOppBizDocs.DomainID = lngDomainId
                        OppType = objOppBizDocs.GetOpportunityType()


                        Dim objCalculateDealAmount As New CalculateDealAmount
                        objCalculateDealAmount.CalculateDealAmount(lngOppId, lngOppBizDocId, OppType, lngDomainId, dtOppBiDocItems)

                        
                        ''---------------------------------------------------------------------------------
                        JournalId = SaveDataToHeader(objCalculateDealAmount.GrandTotal)
                        If OppType = 1 Then
                            objJournalEntries.SaveJournalEntriesSales(lngOppId, lngDomainId, dtOppBiDocItems, JournalId, lngOppBizDocId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivisionID, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                        Else
                            objJournalEntries.SaveJournalEntriesPurchase(lngOppId, lngDomainId, dtOppBiDocItems, JournalId, lngDivisionID, lngOppBizDocId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function SaveDataToHeader(ByVal GrandTotal As Decimal) As Integer
        Try
            Dim lntJournalId As Integer
            Dim lobjAuthoritativeBizDocs As New AuthoritativeBizDocs
            With lobjAuthoritativeBizDocs
                .Amount = Replace(GrandTotal, ",", "")
                .DomainId = lngDomainId
                .JournalId = 0
                .UserCntID = lngUserCntID
                .OppBIzDocID = lngOppBizDocId
                .OppId = lngOppId
                lntJournalId = .SaveDataToJournalEntryHeaderForAuthoritativeBizDocs
                Return lntJournalId
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
