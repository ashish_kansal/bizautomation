﻿Imports BACRM.BusinessLogic.Notification
Imports MailBee.SmtpMail
Imports System.Configuration
Imports System.IO
Imports MailBee
Imports MailBee.Security
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities

Public Class ScheduledAlert
    Dim ObjNotification As Notification
    Private m As Smtp = Nothing

    Public Sub StartCheck()
        Try
            SendCustRptScheduelerMails() 'Creates Recurring of Sales Order
            SendAlertMails() 'Sends Alerts
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Action Items Scheduling
    Public Function ActItemsMail() As Boolean
        Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
        Try
            If ObjNotification Is Nothing Then
                ObjNotification = New Notification
            End If
            Dim objEmail As New Email
            Dim dataTable As DataTable
            Dim smtpserver As SmtpServer
            ObjNotification.interval = ConfigurationManager.AppSettings("Timer2")
            dataTable = ObjNotification.getActItemsMail
            Dim strSentComm As String = ""
            For Each Erow As DataRow In dataTable.Rows
                m = New Smtp
                smtpserver = New SmtpServer

                'smtpserver.Name = ConfigurationManager.AppSettings("SMTPServer")
                objEmail.GetSMTPDetails(smtpserver, Long.Parse(Erow.Item("numDomainID").ToString()))
                m.SmtpServers.Add(smtpserver)
                m.ThrowExceptions = False
                If File.Exists("c:\aspNetEmail.log") = True Then
                    m.Log.Filename = "C:\aspNetEmail.log"
                End If
                'Get From Email Details based on DomainID
                objEmail.GetFromEmailDetails(m, Long.Parse(Erow.Item("numDomainID").ToString()))
                'm.From.DisplayName = "Administrator"
                'm.From.AsString = "Administrator@bizautomation.com"
                m.To.AsString = IIf(IsDBNull(Erow.Item("vcEmail")), "", Erow.Item("vcEmail"))
                m.Subject = IIf(IsDBNull(Erow.Item("vcSubject")), "", Erow.Item("vcSubject"))
                m.BodyHtmlText = IIf(IsDBNull(Erow.Item("vcBody")), "", Erow.Item("vcBody"))
                m.Send()
                If strSentComm = "" Then
                    strSentComm = Erow.Item("numCommId")
                Else
                    strSentComm = strSentComm & "," & Erow.Item("numCommId")
                End If

            Next
            If strSentComm <> "" Then

            End If
            ObjNotification.StrCommIds = strSentComm
            ObjNotification.UpdateActItems()

            'getMileStonesAlerts
            dataTable = ObjNotification.getMileStonesAlerts

            For Each Erow As DataRow In dataTable.Rows
                m = New Smtp
                smtpserver = New SmtpServer
                objEmail.GetSMTPDetails(smtpserver, Long.Parse(Erow.Item("numDomainID").ToString()))
                m.SmtpServers.Add(smtpserver)
                If File.Exists("c:\aspNetEmail.log") = True Then
                    m.Log.Filename = "C:\aspNetEmail.log"
                End If
                m.ThrowExceptions = False
                m.From.AsString = IIf(IsDBNull(Erow.Item("vcFrom")), "", Erow.Item("vcFrom"))
                m.To.AsString = IIf(IsDBNull(Erow.Item("vcTo")), "", Erow.Item("vcTo"))
                m.Cc.AsString = IIf(IsDBNull(Erow.Item("vcCC")), "", Erow.Item("vcCC"))
                m.Subject = IIf(IsDBNull(Erow.Item("vcSubject")), "", Erow.Item("vcSubject"))
                m.BodyHtmlText = IIf(IsDBNull(Erow.Item("vcBody")), "", Erow.Item("vcBody"))
                m.Send()
            Next
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Custom Report Scheduler (table:[CustRptScheduler])
    Private Function SendCustRptScheduelerMails() As Boolean
        Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
        Try
            If ObjNotification Is Nothing Then
                ObjNotification = New Notification
            End If
            Dim objEmail As New Email

            WriteMessage("Inside function SendCustRptScheduelerMails")
            Dim dataTable As DataTable
            dataTable = ObjNotification.getSchedules()
            WriteMessage("getSchedules() is Done ")

            Dim emailTable As New DataTable()
            Dim numScheduleid As Long
            Dim numReportid As Long
            Dim FromName As String
            Dim FromEmail As String
            Dim ReportName As String
            Dim ToEmail As String
            Dim filename As String
            Dim objpdfclass As New pdfclass
            Dim smtpserver As SmtpServer
            For Each row As DataRow In dataTable.Rows
                numScheduleid = row.Item("ScheduleId")
                numReportid = row.Item("ReportId")
                FromName = row.Item("ContactName")
                FromEmail = row.Item("ContactEmail")
                ReportName = row.Item("ReportName")
                ToEmail = row.Item("ToEmail")
                filename = objpdfclass.Convert(ConfigurationManager.AppSettings("PortalUrl") & "/reports/frmCustomReport.aspx?reportId=" & numReportid)

                m = New Smtp
                smtpserver = New SmtpServer
                'Take server from domain details
                'smtpserver.Name = ConfigurationManager.AppSettings("SMTPServer")

                objEmail.GetSMTPDetails(smtpserver, Long.Parse(row.Item("DomainID").ToString()))
                m.SmtpServers.Add(smtpserver)
                If File.Exists("c:\aspNetEmail.log") = True Then
                    m.Log.Filename = "C:\aspNetEmail.log"
                End If
                m.ThrowExceptions = False
                m.From.DisplayName = FromName '"Administrator"
                m.From.AsString = FromEmail '"Administrator@bizautomation.com"

                m.To.AsString = ToEmail
                '  Dim a As New aspNetEmail.Attachment(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & filename)
                m.AddAttachment(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & filename)
                m.Subject = ReportName & "Generated On" & Format(Now, "MM/dd/yyyy")
                'm.BodyFormat = MailFormat.Html
                m.BodyHtmlText = "Report From Bizautomation Scheduler on " & Format(Now, "MM/dd/yyyy")
                m.Send()

                If File.Exists(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & filename) = True Then
                    File.Delete(ConfigurationManager.AppSettings("PortalLocation") & "\Documents\Docs\" & filename)
                End If
                ObjNotification.numScheduleid = numScheduleid
                ObjNotification.UpdateScheduledOccurance()
            Next
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Private Sub SendAlertMails()
        Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
        Try
            If ObjNotification Is Nothing Then
                ObjNotification = New Notification
            End If
            Dim sb As New System.Text.StringBuilder
            Dim dataTable As DataTable
            dataTable = ObjNotification.getEmailAlerts()
            Dim objCommon As New CCommon
            WriteMessage("Total No of alerts to send: " + dataTable.Rows.Count.ToString())
            For Each Erow As DataRow In dataTable.Rows
                m = New Smtp
                Dim smtpserver As SmtpServer = New SmtpServer
                If Erow.Item("bitPSMTPServer") = True Then
                    smtpserver.AuthMethods = AuthenticationMethods.Auto
                    smtpserver.AuthOptions = AuthenticationOptions.PreferSimpleMethods
                    smtpserver.AccountName = Erow.Item("vcPSMTPUserName")
                    Dim strPassword As String
                    strPassword = objCommon.Decrypt(Erow.Item("vcPSmtpPassword"))
                    smtpserver.Password = strPassword
                End If
                If Erow.Item("numPSMTPPort") <> 0 Then
                    smtpserver.Port = Erow.Item("numPSMTPPort")
                End If
                If Erow.Item("bitPSMTPSSL") = True Then
                    smtpserver.SslMode = SslStartupMode.UseStartTls
                End If
                'smtpsmtpserver.Name = ConfigurationManager.AppSettings("SMTPServer")
                m.SmtpServers.Add(smtpserver)

                If File.Exists("c:\aspNetEmail.log") = True Then
                    m.Log.Filename = "C:\aspNetEmail.log"
                End If
                m.ThrowExceptions = False
                m.From.DisplayName = "Administrator"
                m.From.AsString = "Administrator@bizautomation.com"
                'm.FromName = "Administrator"
                m.To.AsString = IIf(IsDBNull(Erow.Item("vcTo")), "", Erow.Item("vcTo")) '"chitnanprajapati@gmail.com"
                m.Cc.AsString = IIf(IsDBNull(Erow.Item("vcCC")), "", Erow.Item("vcCC"))
                m.Subject = IIf(IsDBNull(Erow.Item("vcSubject")), "", Erow.Item("vcSubject"))
                m.BodyHtmlText = IIf(IsDBNull(Erow.Item("vcBody")), "", Erow.Item("vcBody"))
                ' Attempt to connect.
                m.Connect()

                m.Send()
                m.Disconnect()


                '' Display the host name of the server the connection was established with.
                'sb.Append("Connected to " + m.SmtpServers(m.GetCurrentSmtpServerIndex()).Name)
                '' Make sure all the recipients are ok.
                'If m.TestSend(SendFailureThreshold.AllRecipientsFailed) <> TestSendResult.OK Then
                '    sb.Append("No recipients can receive the message.recipients:" + m.To.AsString)
                '    ObjNotification.DeliveryStatus = DeliveryStatus.Failed
                'Else
                '    ' Show refused recipients if they are exists
                '    If m.GetRefusedRecipients().Count > 0 Then
                '        sb.Append("The following recipients failed: " & m.GetRefusedRecipients().ToString())
                '        ObjNotification.DeliveryStatus = DeliveryStatus.Failed
                '    Else
                '        sb.Append("All recipients are ok. Will send the message now.")

                '        ' Send e-mail. If it cannot be delivered, bounce will
                '        ' arrive to bounce@domain3.com, not to joe@domain1.com

                '        m.Send()
                '        ObjNotification.DeliveryStatus = DeliveryStatus.Sent
                '        'm.Send("bounce@domain.com", CType(Nothing, String))
                '        sb.Append("Sent to: " + m.GetAcceptedRecipients().ToString())
                '    End If
                'End If
                '' Disconnect from the server
                'm.Disconnect()

                ''update status of mail 
                'If Not IsDBNull(Erow.Item("numConECampDTLID")) Then
                '    ObjNotification.numConECampDTLID = Erow.Item("numConECampDTLID")
                '    ObjNotification.EmailLog = sb.ToString()
                '    ObjNotification.UpdateECampaignAlertStatus()
                'End If
                sb.Remove(0, sb.Length)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub SendEcampaignMails()
        Smtp.LicenseKey = ConfigurationManager.AppSettings("SMTPLicense")
        Dim pickupFolderPath As String = ConfigurationManager.AppSettings("MailQueuePath")
        Dim dataTable As DataTable
        Dim i As Integer = 1
        Dim filename As String = String.Empty
        Dim objCommon As New CCommon
        'Dim smtpserver As SmtpServer
        If ObjNotification Is Nothing Then
            ObjNotification = New Notification
        End If

        Try
            dataTable = ObjNotification.getECampaignMails()
            WriteMessage("Total No of Mails Fetched : " + dataTable.Rows.Count.ToString())
            For Each Erow As DataRow In dataTable.Rows
                m = New Smtp

                If File.Exists("c:\aspNetEmail.log") = True Then
                    m.Log.Filename = "C:\aspNetEmail.log"
                    m.Log.Enabled = True
                End If
                m.ThrowExceptions = False
                m.From.DisplayName = Erow.Item("vcFrom") '"Administrator"
                m.From.AsString = Erow.Item("vcFrom") '"Administrator@bizautomation.com"
                'm.FromName = "Administrator"
                m.To.AsString = IIf(IsDBNull(Erow.Item("vcTo")), "", Erow.Item("vcTo")) '"chitnanprajapati@gmail.com"
                m.Cc.AsString = IIf(IsDBNull(Erow.Item("vcCC")), "", Erow.Item("vcCC"))
                m.Subject = IIf(IsDBNull(Erow.Item("vcSubject")), "", Erow.Item("vcSubject"))
                m.BodyHtmlText = IIf(IsDBNull(Erow.Item("vcBody")), "", Erow.Item("vcBody"))
                filename = m.SubmitToPickupFolder(pickupFolderPath)
                WriteMessage(i.ToString & ").ECampaign Email Saved into: " & pickupFolderPath & "\" & filename)
                i = i + 1

                ''update status of mail 
                If Not IsDBNull(Erow.Item("numConECampDTLID")) Then
                    ObjNotification.numConECampDTLID = Erow.Item("numConECampDTLID")
                    ObjNotification.EmailLog = "ECampaign Email Saved into: " & pickupFolderPath & "\" & filename
                    ObjNotification.DeliveryStatus = DeliveryStatus.Sent
                    ObjNotification.UpdateECampaignAlertStatus()
                End If

            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub CreateECampaignActionItems()
        If ObjNotification Is Nothing Then
            ObjNotification = New Notification
        End If
        Dim dt As DataTable
        Dim objActionItem As New ActionItem
        Dim dtTemplate As DataTable
        Dim lngCommId As Long
        Try
            dt = ObjNotification.getECampaignActionItems()
            WriteMessage("Create Action Items from Drip Campaign- Fetched records :" & dt.Rows.Count.ToString())
            For Each dr As DataRow In dt.Rows
                objActionItem.RowID = dr("numActionItemTemplate")
                dtTemplate = objActionItem.LoadThisActionItemTemplateData()
                If dtTemplate.Rows.Count > 0 Then
                    With objActionItem
                        .CommID = 0
                        .Task = GetTask(dtTemplate.Rows(0)("Type"))
                        .ContactID = dr("numContactID")
                        .DivisionID = dr("numDivisionId")
                        .Details = ""
                        .AssignedTo = dr("numRecOwner")
                        .UserCntID = dr("numRecOwner")
                        .DomainID = dr("numDomainID")
                        If .Task = 973 Then
                            .BitClosed = 1
                        Else : .BitClosed = 0
                        End If
                        .CalendarName = ""
                        If .Task = 973 Or .Task = 974 Then
                            .StartTime = dr("intStartDate")
                            .EndTime = DateAdd(DateInterval.Day, 1, .StartTime)
                        Else
                            .StartTime = dr("intStartDate")
                            If Not IsDBNull(dtTemplate.Rows(0)("DueDays")) Then
                                .EndTime = DateAdd(DateInterval.Day, dtTemplate.Rows(0)("DueDays"), .StartTime)
                            End If
                        End If
                        .Activity = dtTemplate.Rows(0)("Activity")
                        .Status = dtTemplate.Rows(0)("Priority")
                        .Snooze = 0
                        .SnoozeStatus = 0
                        .Remainder = 5
                        .RemainderStatus = 1
                        .ClientTimeZoneOffset = 0
                        .bitOutlook = 0
                        .SendEmailTemplate = dtTemplate.Rows(0)("bitSendEmailTemp")
                        .EmailTemplate = dtTemplate.Rows(0)("numEmailTemplate")
                        .Hours = dtTemplate.Rows(0)("tintHours")
                        .Alert = dtTemplate.Rows(0)("bitAlert")
                        '.ActivityId = lngActivityId
                        '.CaseID = GetQueryStringVal(Request.QueryString("enc"), "CaseID")
                        '.CaseTimeId = CInt(txtCaseTimeID.Text)
                        '.CaseExpId = CInt(txtCaseExpenseID.Text)

                    End With

                    lngCommId = objActionItem.SaveCommunicationinfo()
                    WriteMessage("Saved New Action Item : CommunicationID=" & lngCommId)
                End If

            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function GetTask(ByVal TaskName As String) As Long
        Select Case TaskName.ToLower()
            Case "communication"
                Return 971
            Case "task"
                Return 972
            Case "notes"
                Return 973
            Case "follow-up anytime"
                Return 974
            Case "sales call"
                Return 975
            Case Else
                Return 972
        End Select
    End Function
    Public Sub GetNewMails()
        Try
            Dim objEmail As New Email
            Dim objUserAccess As New UserAccess
            Dim dtUsers As DataTable
            Dim strMessage As String

            dtUsers = objUserAccess.GetImapEnabledUsers()
            For i As Integer = 0 To dtUsers.Rows.Count - 1
                Try
                    strMessage = objEmail.GetNewEmails(dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString())
                Catch ex As Exception
                    WriteMessage("UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Error Message:" & ex.Message + " " + ex.StackTrace.ToString())
                End Try
                WriteMessage("UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Message:" & strMessage)
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'will fetch last 20 mails and verify against database
    Public Sub SyncDeletedMails(ByVal NoOfMail As Integer)
        Try
            Dim objEmail As New Email
            Dim objUserAccess As New UserAccess
            Dim dtUsers As DataTable
            Dim strMessage As String

            dtUsers = objUserAccess.GetImapEnabledUsers()
            For i As Integer = 0 To dtUsers.Rows.Count - 1
                Try
                    strMessage = objEmail.SyncMailbox(NoOfMail, dtUsers.Rows(i)("numUserCntId").ToString(), dtUsers.Rows(i)("numDomainId").ToString())
                Catch ex As Exception
                    WriteMessage("UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Error Message:" & ex.Message + " " + ex.StackTrace.ToString())
                End Try
                WriteMessage("UserCntID:" & dtUsers.Rows(i)("numUserCntId").ToString() & " DomainID:" & dtUsers.Rows(i)("numDomainId").ToString() & ",Message:" & strMessage)
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Below method will keep DealAmount in OpportunityBizDocs Table Updated Daily basis 
    'this implementation is done in order to gain performance for A/R aging Summery report Which is 20 sec as on 31/08/2009
    Public Sub UpdateDealAmount()
        Try
            Dim objOpp As New COpportunities
            objOpp.UpdateDealAmount()
            WriteMessage("******** Deal Amount Has been Updated for the day*********")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Enum DeliveryStatus
        Failed
        Sent
    End Enum
    Private Shared Sub WriteMessage(ByVal Message As String)
        Try
            WebAPI.WriteMessage(Message, System.Configuration.ConfigurationManager.AppSettings("LogFilePath"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
