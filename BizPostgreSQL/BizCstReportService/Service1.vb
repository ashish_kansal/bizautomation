Imports System.Data.SqlClient
Imports System.Configuration
Imports System
Imports System.Xml
Imports System.IO
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Common

Public Class Service1
    Private WithEvents Timer1 As System.Timers.Timer
    Private WithEvents Timer2 As System.Timers.Timer
    Private WithEvents CularisWakeupTimer As System.Timers.Timer
    Private WithEvents ECampaignWakeupTime As System.Timers.Timer
    Private WithEvents GetNewMailsWakeupTime As System.Timers.Timer
    Private WithEvents SynchronizeDeletedMailWakeupTime As System.Timers.Timer
    Private WithEvents UpdateDealAmountWakeupTimer As System.Timers.Timer
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Try
            InitializeTimer()
            Timer1.Enabled = True
            Timer2.Enabled = True
            CularisWakeupTimer.Enabled = True
            ECampaignWakeupTime.Enabled = True
            GetNewMailsWakeupTime.Enabled = True
            SynchronizeDeletedMailWakeupTime.Enabled = True
            UpdateDealAmountWakeupTimer.Enabled = True
            WriteMessage("BizService Started Sucessfully,Timer1=" + Timer1.Interval.ToString() + ", Timer2=" + Timer2.Interval.ToString())
            Dim objSendMail As New Email()
            objSendMail.SendSystemEmail("BizService has started at " & DateTime.Now.ToString, DateTime.Now.ToString, "", "noreply@bizautomation.com", ConfigurationManager.AppSettings("NotifyWhenServiceGoesDown"), "", "")
        Catch ex As Exception
            'Throw ex
            WriteMessage(ex.Message)
        End Try
    End Sub

    Private Sub InitializeTimer()
        If Timer1 Is Nothing Then
            Timer1 = New System.Timers.Timer
            Timer1.AutoReset = True

            Timer1.Interval = ReadAppSettingInterval(600000, 1)
        End If
        If Timer2 Is Nothing Then
            Timer2 = New System.Timers.Timer
            Timer2.AutoReset = True
            Timer2.Interval = ReadAppSettingInterval(600000, 2)
        End If
        If CularisWakeupTimer Is Nothing Then
            CularisWakeupTimer = New System.Timers.Timer
            CularisWakeupTimer.AutoReset = True
            CularisWakeupTimer.Interval = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings("CularisWakeupTime") * 60000)
        End If
        If ECampaignWakeupTime Is Nothing Then
            ECampaignWakeupTime = New System.Timers.Timer
            ECampaignWakeupTime.AutoReset = True
            ECampaignWakeupTime.Interval = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings("ECampaignWakeupTime") * 60000)
        End If
        If GetNewMailsWakeupTime Is Nothing Then
            GetNewMailsWakeupTime = New System.Timers.Timer
            GetNewMailsWakeupTime.AutoReset = True
            GetNewMailsWakeupTime.Interval = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings("GetNewMailsWakeupTime") * 60000)
        End If
        If SynchronizeDeletedMailWakeupTime Is Nothing Then
            SynchronizeDeletedMailWakeupTime = New System.Timers.Timer
            SynchronizeDeletedMailWakeupTime.AutoReset = True
            SynchronizeDeletedMailWakeupTime.Interval = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings("SynchronizeDeletedMailWakeupTime") * 60000)
        End If
        If UpdateDealAmountWakeupTimer Is Nothing Then
            UpdateDealAmountWakeupTimer = New System.Timers.Timer
            UpdateDealAmountWakeupTimer.AutoReset = True
            UpdateDealAmountWakeupTimer.Interval = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings("UpdateDealAmountWakeupTime") * 60000)
        End If
    End Sub
    Protected Function ReadAppSettingInterval(ByVal interval As Double, ByVal intbyte As Integer) As Double
        '   Dim interval As Double = 600000, tempInterval 'initialize to ten minutes.


        Dim intervalMin As String = String.Empty
        If intbyte = 1 Then
            intervalMin = System.Configuration.ConfigurationManager.AppSettings("Timer1")
        ElseIf intbyte = 2 Then
            intervalMin = System.Configuration.ConfigurationManager.AppSettings("Timer2")
        End If

        If IsNumeric(intervalMin) Then
            interval = Convert.ToDouble(Convert.ToInt32(intervalMin) * 60000)
        End If

        Return interval
    End Function

    Protected Overrides Sub OnStop()
        Timer1.Enabled = False
        Timer2.Enabled = False
        CularisWakeupTimer.Enabled = False
        ECampaignWakeupTime.Enabled = False
        GetNewMailsWakeupTime.Enabled = False
        SynchronizeDeletedMailWakeupTime.Enabled = False
        UpdateDealAmountWakeupTimer.Enabled = False
        'Send Notification mail 
        Dim objSendMail As New Email()
        objSendMail.SendSystemEmail("BizService went down at " & DateTime.Now.ToString, DateTime.Now.ToString, "", "noreply@bizautomation.com", ConfigurationManager.AppSettings("NotifyWhenServiceGoesDown"), "", "")
    End Sub
    Protected Overrides Sub OnContinue()
        Timer1.Enabled = True
        Timer2.Enabled = True
        CularisWakeupTimer.Enabled = True
        ECampaignWakeupTime.Enabled = True
        GetNewMailsWakeupTime.Enabled = True
        SynchronizeDeletedMailWakeupTime.Enabled = True
        UpdateDealAmountWakeupTimer.Enabled = True
    End Sub
    Protected Overrides Sub OnPause()
        Timer1.Enabled = False
        Timer2.Enabled = False
        CularisWakeupTimer.Enabled = False
        ECampaignWakeupTime.Enabled = False
        GetNewMailsWakeupTime.Enabled = False
        SynchronizeDeletedMailWakeupTime.Enabled = False
        UpdateDealAmountWakeupTimer.Enabled = False
    End Sub

    Private Sub timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer1.Elapsed
        Try
            'Dim i As Integer = 0
            'Dim j As Int32 = 10 / i

            Dim objAlert As New ScheduledAlert
            objAlert.StartCheck()
            Dim objRecurring As New Recurring
            objRecurring.CreateRecurringOpportunity()
            objRecurring.CreateRecurringBizDocs()
            WriteMessage("StartCheck Done @ " + DateTime.Now)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub timer1_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles Timer2.Elapsed
        Try
            Dim objAlert As New ScheduledAlert
            Dim result As Boolean
            result = objAlert.ActItemsMail()
            WriteMessage("ActItemsMail Done @ " + DateTime.Now)
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub


    Dim strToken As String = String.Empty
    Dim strOrders As String = String.Empty

    Private Sub CularisWakeupTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles CularisWakeupTimer.Elapsed
        Try
            Dim objAPI As New WebAPI
            Dim dtAPI As DataTable = objAPI.GetWebApi()
            Dim client As CularisClient

            For Each dr As DataRow In dtAPI.Rows
                If dr("bitEnableAPI") = True Then
                    strToken = dr("vcFourthFldValue").ToString()
                    If strToken.Length > 0 Then
                        client = New CularisClient
                        With client
                            .APIURL = dr("vcFirstFldValue").ToString()
                            .Token = strToken
                            .SectionKey = dr("vcThirdFldValue").ToString()
                            .DomainID = dr("numDomainId").ToString()
                            .WebApiId = dr("WebApiId").ToString()
                            If dr("vcTenthFldValue").ToString().Length > 0 Then
                                .DateCreated = CDate(dr("vcTenthFldValue"))
                            Else
                                .DateCreated = "" 'get all products for first time 
                            End If
                            .DateModified = ""
                            .AddLatestProducts() 'Add Newly Created Products
                            .DateCreated = ""
                            If dr("vcTenthFldValue").ToString().Length > 0 Then
                                .DateModified = CDate(dr("vcTenthFldValue"))
                            Else
                                .DateModified = ""
                            End If
                            .AddLatestProducts() ' add modified products 

                            ''Get Orders and Create Sales Order in Biz
                            .order_id = dr("LastInsertedOrderID").ToString()
                            strOrders = .GetOrders()
                            'Dim oFile As System.IO.File
                            'Dim oRead As System.IO.StreamReader
                            'oRead = oFile.OpenText("D:\Documents and Settings\Chintan\Desktop\A.xml")
                            'strOrders = oRead.ReadToEnd()
                            .SaveOrders(strOrders)
                        End With
                    End If
                End If
            Next
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub
    Private Shared Sub WriteMessage(ByVal Message As String)
        Try
            WebAPI.WriteMessage(Message, System.Configuration.ConfigurationManager.AppSettings("LogFilePath"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '-----Old Method used to fetch data from Cularis and insert into BizAutomation-----------
    'Private Sub CularisWakeupTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles CularisWakeupTimer.Elapsed
    '    Try
    '        Dim objAPI As New WebAPI
    '        Dim dtAPI As DataTable = objAPI.GetWebApi()
    '        Dim client As CularisClient

    '        For Each dr As DataRow In dtAPI.Rows
    '            strToken = dr("vcFourthFldValue").ToString()
    '            If strToken.Length > 0 Then
    '                client = New CularisClient
    '                'get Newly Created Products
    '                With client
    '                    .APIURL = dr("vcFirstFldValue").ToString()
    '                    .Token = dr("vcFourthFldValue").ToString()
    '                    .SectionKey = dr("vcThirdFldValue").ToString()
    '                    If dr("vcTenthFldValue").ToString().Length > 0 Then
    '                        .DateCreated = CDate(dr("vcTenthFldValue")).ToString("yyyyMMddHHmmss")
    '                    Else
    '                        .DateCreated = "" 'get all products for first time -DateTime.UtcNow.AddDays(-3).ToString("yyyyMMddHHmmss")
    '                    End If
    '                    .DateModified = ""
    '                    .DomainID = dr("numDomainId").ToString()
    '                    .WareHouseID = dr("numDefaultWareHouseID").ToString()
    '                    .COGSChartAcntId = dr("numDefaultCOGSChartAcntId").ToString()
    '                    .AssetChartAcntId = dr("numDefaultAssetChartAcntId").ToString()
    '                    .IncomeChartAcntId = dr("numDefaultIncomeChartAcntId").ToString()
    '                    .WebApiId = dr("WebApiId").ToString()
    '                    '.SaveLatestProducts()
    '                    'get Modified Products
    '                    .DateCreated = ""
    '                    If dr("vcTenthFldValue").ToString().Length > 0 Then
    '                        .DateModified = CDate(dr("vcTenthFldValue")).ToString("yyyyMMddHHmmss")
    '                    Else
    '                        .DateModified = DateTime.UtcNow.AddDays(-3).ToString("yyyyMMddHHmmss")
    '                    End If
    '                    .SaveLatestProducts()

    '                    'Get Orders and Create Sales Order in Biz
    '                    .APIURL = dr("vcFirstFldValue").ToString()
    '                    .Token = dr("vcFourthFldValue").ToString()
    '                    .SectionKey = dr("vcThirdFldValue").ToString()
    '                    'If dr("vcNinthFldValue").ToString().Length > 0 Then
    '                    '    .DateCreated = CDate(dr("vcNinthFldValue")).ToString("yyyyMMddHHmmss")
    '                    'Else
    '                    '    .DateCreated = "" 'Get All Orders first time 'DateTime.UtcNow.AddDays(-3).ToString("yyyyMMddHHmmss")
    '                    'End If
    '                    .order_id = dr("LastInsertedOrderID").ToString()
    '                    strOrders = .GetOrders()
    '                    'WriteMessage(strOrders)
    '                    'Dim tr As TextReader = New StreamReader("D:/Documents and Settings/Chintan/Desktop/api.xml")
    '                    'strOrders += tr.ReadToEnd()
    '                    'tr.Close()
    '                    .DomainID = dr("numDomainId").ToString()
    '                    .APIURL = dr("vcFirstFldValue").ToString()
    '                    .SaveOrders(strOrders)
    '                    'WriteMessage(strOrders)
    '                End With
    '            End If
    '        Next
    '    Catch ex As Exception
    '        WriteMessage(ex.Message)
    '        WriteMessage(ex.StackTrace.ToString())
    '    End Try
    'End Sub

    Private Sub ECampaignWakeupTime_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles ECampaignWakeupTime.Elapsed
        Try
            Dim objAlert As New ScheduledAlert
            WriteMessage("Sending ECampaign Mails to Queue")
            objAlert.SendEcampaignMails()
            objAlert.CreateECampaignActionItems()
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub GetNewMailsWakeupTime_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles GetNewMailsWakeupTime.Elapsed
        Try
            Dim objAlert As New ScheduledAlert
            WriteMessage("Fetching IMAP Enabled User Details")
            objAlert.GetNewMails()
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub SynchronizeDeletedMailWakeupTime_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles SynchronizeDeletedMailWakeupTime.Elapsed
        Try
            Dim objAlert As New ScheduledAlert
            WriteMessage("Started Synchronizing Inbox with Email Account")
            objAlert.SyncDeletedMails(System.Configuration.ConfigurationManager.AppSettings("SynchronizeLastDeletedMailCount"))
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub

    Private Sub UpdateDealAmountWakeupTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles UpdateDealAmountWakeupTimer.Elapsed
        Try
            Dim objAlert As New ScheduledAlert
            objAlert.UpdateDealAmount()
        Catch ex As Exception
            WriteMessage(ex.Message)
            WriteMessage(ex.StackTrace.ToString())
        End Try
    End Sub
End Class
