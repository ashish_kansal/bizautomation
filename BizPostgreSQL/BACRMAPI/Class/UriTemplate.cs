﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BACRMAPI
{
    public class UriTemplate
    {
        public const string AuthenticateUser = "Login?user={UserName}&pass={Password}";
        public const string AuthenticateUserUsingToken = "TokenLogin?Token={Token}";
        public const string PortalLogin = "PortalLogin?user={UserName}&pass={Password}&DomainID={DomainID}";

        public const string GetItem = "GetItem?ItemCode={ItemCode}&Token={Token}";
        public const string GetItems = "GetItems?Token={Token}&ItemCode={ItemCode}";
        public const string DeleteItem = "DeleteItem?ItemCode={ItemCode}&Token={Token}";
        public const string GetInventoryStatus = "InventoryStatus?Token={Token}&ItemCode={ItemCode}&WarehouseID={WarehouseID}";
        public const string GetItemPriceRec = "ItemPriceRecommendation?Token={Token}&ItemCode={ItemCode}&NoOfunits={NoOfunits}";
        public const string AddItem = "AddItem?Token={Token}&ItemName={ItemName}&ItemDesc={ItemDesc}&ItemType={ItemType}&ModelID={ModelID}&SKU={SKU}&ListPrice={ListPrice}&Weight={Weight}&Height={Height}&Length={Length}&Width={Width}&WareHouseID={WareHouseID}&NoOfUnit={NoOfUnit}&ItemCode={ItemCode}";

        public const string AddOrder = "AddOrder?Token={Token}&CompanyName={CompanyName}&FirstName={FirstName}&LastName={LastName}&Email={Email}&Phone={Phone}&BillStreet={BillStreet}&BillCity={BillCity}&BillState={BillState}&BillCountry={BillCountry}&BillPostalCode={BillPostalCode}&ShipStreet={ShipStreet}&ShipCity={ShipCity}&ShipState={ShipState}&ShipCountry={ShipCountry}&ShipPostalCode={ShipPostalCode}&OppSource={OppSource}&ShipCost={ShipCost}&Items={Items}";
        public const string AddRelationship = "AddRelationship?Token={Token}&CompanyName={CompanyName}&FirstName={FirstName}&LastName={LastName}&GroupID={GroupID}&CRMType={CRMType}&Email={Email}&WebSite={WebSite}&Comments={Comments}";



    }
}
