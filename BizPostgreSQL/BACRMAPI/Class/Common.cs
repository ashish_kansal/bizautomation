﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Web;
using System.ServiceModel;
using BACRM.BusinessLogic.WebAPI;
namespace BACRMAPI
{
    public static class Common
    {
        //public static BizLogin ValidateToken1(string Token)
        //{
        //    BizLogin objLogin = new BizLogin();
        //    objLogin = objLogin.IsAuthenticated(Token);
        //    objLogin.IsValid = true;
        //    if (!objLogin.IsValid)
        //    {
        //        error e = new error();
        //        e.msg = "Token is not valid";
        //        FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
        //        throw fe;
        //    }
        //    return objLogin;
        //}
        public static void ValidateRequest(string Token,ref long DomainID,ref long UserCntId)
        {
            if (string.IsNullOrEmpty(Token))
            {
                error e = new error();
                e.msg = "Token is Empty";
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            WebService objWS = new WebService();
            objWS.Token = Token;
            if (!objWS.ValidateToken())
            {
                error e = new error();
                e.msg = "Token is not valid";
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            else
            {
                DomainID = objWS.DomainID;
                UserCntId = objWS.UserCntID;
            }
        }
    }
}
