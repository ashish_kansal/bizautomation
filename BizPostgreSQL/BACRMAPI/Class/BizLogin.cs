﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
//using Microsoft.ServiceModel.Web;
using System.Net;
using BACRM.BusinessLogic.Common;
using BACRM.BusinessLogic.Admin;
using System.Data;
using System.Web.SessionState;
using System.Configuration;
namespace BACRMAPI
{
    [DataContract]
    public class BizLogin
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public bool IsValid { get; set; }
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public string ReturnURL { get; set; }
        [DataMember]
        public string CurrentDate { get; set; }
        [DataMember]
        public string DomainID { get; set; }
        public void IsAuthenticated()
        {
            CCommon objCommon = new CCommon();
            DataSet dsUserDetails = new DataSet();
            string plainText = string.Empty;
            string cipherText = string.Empty;
            plainText = Password;
            cipherText = objCommon.Encrypt(plainText);

            dsUserDetails = objCommon.CheckUserAtLogin(UserName.Trim(), cipherText,"N");
            DataTable dtTable = default(DataTable);
            dtTable = dsUserDetails.Tables[0];
            if (dtTable.Rows.Count != 1)
            {
                IsValid = false;
            }
            else
            {
                IsValid = true;
                CurrentDate = DateTime.Now.ToString();
                Token = HttpUtility.UrlEncode(objCommon.Encrypt(UserName + "&*%" + Password + "&*%" + CurrentDate));
                ReturnURL = ConfigurationManager.AppSettings.Get("SiteURL") + "/BACRMUI/Login.aspx?Token=" + Token;
                DomainID = dtTable.Rows[0]["numDomainID"].ToString();
            }
        }
        public BizLogin IsAuthenticated(string Token)
        {
            CCommon objCommon = new CCommon();
            String strToken = objCommon.Decrypt(HttpUtility.UrlDecode(Token));
            String strCurrentDate, strUserName, strPassword;
            string[] strArray = strToken.Split(new string[] { "&*%" }, StringSplitOptions.RemoveEmptyEntries);

            strUserName = strArray[0];
            strPassword = strArray[1];
            strCurrentDate = strArray[2];
            TimeSpan objTimeSpan = DateTime.Now - DateTime.Parse(strCurrentDate);
            if (objTimeSpan.Minutes > 20 | objTimeSpan.Minutes < 0)
            {
                //throw new WebProtocolException(HttpStatusCode.BadRequest);
            }

            BizLogin objLogin = new BizLogin();
            objLogin.UserName = strUserName;
            objLogin.Password = strPassword;
            objLogin.IsAuthenticated();
            return objLogin;
        }


        public void LogInToPortal()
        {
            UserAccess objUser = new UserAccess();
            
            CCommon objCommon = new CCommon();
            DataSet dsUserDetails = new DataSet();
            string plainText = string.Empty;
            string cipherText = string.Empty;
            plainText = Password;
            cipherText = objCommon.Encrypt(plainText);

            dsUserDetails = objCommon.CheckUserAtLogin(UserName.Trim(), cipherText, "N");
            DataTable dtTable = default(DataTable);
            dtTable = dsUserDetails.Tables[0];
            if (dtTable.Rows.Count != 1)
            {
                IsValid = false;
            }
            else
            {
                IsValid = true;
                CurrentDate = DateTime.Now.ToString();
                Token = HttpUtility.UrlEncode(objCommon.Encrypt(UserName + "&*%" + Password + "&*%" + CurrentDate));
                ReturnURL = "http://localhost/BACRMUI/Login.aspx?Token=" + Token;
            }
        }


    }
    //public enum FaultCode
    //{

    //    [EnumMember]

    //    ERROR,

    //    [EnumMember]

    //    INCORRECT_PARAMETER

    //}

    [DataContract]
    public class error
    {

        //[DataMember]
        //public FaultCode errorcode;
        [DataMember]
        public string msg;
        [DataMember]
        public string request;

    }
}
