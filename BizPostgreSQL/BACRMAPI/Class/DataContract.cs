﻿using System.Runtime.Serialization;
[DataContract(Name="Item")]
public class ItemData
{

    [DataMember(Order=1)]
    public string ItemCode;
    [DataMember(Order = 2)]
    public string ItemName;
    [DataMember(Order = 3)]
    public string ModelID;
    [DataMember(Order = 4)]
    public string SKU;
    [DataMember(Order = 5)]
    public string ItemType;
    [DataMember(Order = 5)]
    public string ItemDesc;
    [DataMember(Order = 7)]
    public string ListPrice;
    [DataMember(Order = 8)]
    public string NoOfUnit;
    [DataMember(Order = 9)]
    public string Weight;
    [DataMember(Order = 10)]
    public string Width;
    [DataMember(Order = 11)]
    public string Length;
    [DataMember(Order = 12)]
    public string Height;
    [DataMember(Order = 13)]
    public string UnitOfMeasure;
  

}