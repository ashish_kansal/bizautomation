﻿/// Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
//using Microsoft.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Linq;
using System.Net;
using BACRM.BusinessLogic.Common;
using System.Data;
using System.Web.SessionState;
using System.Web;
using BACRM.BusinessLogic.Leads;
using Microsoft.ServiceModel.Web;
// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "BACRMAPI")]

namespace BACRMAPI
{
    // TODO: Modify the service behavior settings (instancing, concurrency etc) based on the service's requirements.
    // TODO: NOTE: Please set IncludeExceptionDetailInFaults to false in production environments
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceContract]
    public class Relationship
    {

        //GroupID
        //5 =  My Leads
        //2 = Public Leads
        //1 = Web Leads

        //For Lead CRMType = 0
        //For Prospect or Franchises CRMType = 1
        //For Account CRMType = 2

        //http://localhost:1910/Relationship.svc/AddRelationship?Token=99bfccbc-fd9e-49cc-89e8-83214d6e93a6&CompanyName=Jindal&FirstName=laxmi&LastName=Mittal&GroupID=5&CRMType=0&Email=test@t.com&WebSite=www.t.com&Comments=this is test
        [WebHelp(Comment = "Adds the Lead/Account/Prospect/Franchises")]
        [WebGet(UriTemplate = UriTemplate.AddRelationship)]
        [FaultContract(typeof(error))]
        [OperationContract]
        string AddRelationship(string Token, string CompanyName, string FirstName, string LastName, long GroupID, int CRMType, string Email, string WebSite,string Comments)
        {
            try
            {
                Common.ValidateRequest(Token, ref DomainId, ref UserCntId);
                if (string.IsNullOrEmpty(FirstName))
                {
                    error e = new error();
                    e.msg = "FirstName is empty";
                    FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                    throw fe;
                }
                if (string.IsNullOrEmpty(LastName))
                {
                    error e = new error();
                    e.msg = "FirstName is empty";
                    FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                    throw fe;
                }
                if (DomainId == 0)
                {
                    error e = new error();
                    e.msg = "DomainID is empty";
                    FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                    throw fe;
                }
                if (UserCntId == 0)
                {
                    error e = new error();
                    e.msg = "UserCntID is empty";
                    FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                    throw fe;
                }


                CLeads objLead = new CLeads();
                Boolean flag = true;
                long lngDivisionId, lngCntId = 0;
                objLead.Email = Email;
                objLead.WebSite = WebSite;
                DataSet ds = objLead.CheckEmailWeb();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    flag = false;
                    error e = new error();
                    e.msg = "A contact with this email already exists";
                    FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                    throw fe;
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    flag = false;
                    error e = new error();
                    e.msg = "An organization with this domain name already exists";
                    FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                    throw fe;
                }
                if (flag)
                {

                    objLead.CompanyName = CompanyName;
                    objLead.FirstName = FirstName;
                    objLead.LastName = FirstName;
                    objLead.GroupID = GroupID;
                    objLead.DomainID = DomainId;
                    objLead.UserCntID = UserCntId;
                    objLead.CRMType = CRMType;
                    objLead.ContactType = 70;
                    objLead.PrimaryContact = true;

                    objLead.Comments = Comments;

                    //Create Company
                    objLead.CompanyID = objLead.CreateRecordCompanyInfo();
                    //Create Division
                    if (objLead.DivisionID == 0)
                    {
                        lngDivisionId = objLead.CreateRecordDivisionsInfo();
                        objLead.DivisionID = lngDivisionId;
                    }
                    //Create Contact
                    lngCntId = objLead.CreateRecordAddContactInfo();
                    objLead.ContactID = lngCntId;
                }
                return lngCntId.ToString();
            }
            catch (Exception ex)
            {
                error e = new error();
                e.msg = ex.Message;
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        public long DomainId;
        public long UserCntId;
    }
}
