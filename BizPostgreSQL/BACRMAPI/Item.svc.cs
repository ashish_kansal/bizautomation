﻿//using System;
//using System.Collections.Generic;
//using System.Runtime.Serialization;
//using System.ServiceModel;
//using System.Text;
//using BACRM.BusinessLogic.Item;
//using System.ServiceModel.Activation;
//using System.Data;
//using System.ServiceModel.Web;
//using Microsoft.ServiceModel.Web;
//using BACRM.BusinessLogic.Opportunities;
//using BACRM.BusinessLogic.WebAPI;
//using System.Xml;
//using BACRM.BusinessLogic.Admin;
//using BACRM.BusinessLogic.Leads;
//using BACRM.BusinessLogic.Contacts;
//[assembly: ContractNamespace("", ClrNamespace = "BACRMAPI")]
//namespace BACRMAPI
//{
//    // NOTE: if you change the class name "Item" here, you must also update the reference to "Item" in Web.config.
//    // TODO: Modify the service behavior settings (instancing, concurrency etc) based on the service's requirements.
//    // TODO: NOTE: Please set IncludeExceptionDetailInFaults to false in production environments
//    [ServiceBehavior(IncludeExceptionDetailInFaults = true, InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
//    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
//    [ServiceContract]
//    public class Item
//    {

//        [WebHelp(Comment = "Adds new Item to Inventory,and returns Created Item ID")]

//        //http://localhost:1910/Item.svc/AddItem?Token=99bfccbc-fd9e-49cc-89e8-83214d6e93a6&ItemName=Cintan&ItemDesc=&ItemType=P&ModelID=C123&SKU=SKU123&ListPrice=123&Weight=2.1&Height=5&Length=4&Width=3&WareHouseID=&NoOfUnit=10&ItemCode=
//        [WebGet(UriTemplate = UriTemplate.AddItem)]
//        [FaultContract(typeof(error))]
//        [OperationContract]
//        public string AddItem(string Token, string ItemName, string ItemDesc, string ItemType, string ModelID, string SKU, string ListPrice, string Weight, string Height, string Length, string Width, string WareHouseID, string NoOfUnit, int ItemCode)
//        {
//            CItems objItem = new CItems();
//            if (string.IsNullOrEmpty(Token) || string.IsNullOrEmpty(ItemName))
//            {
//                error e = new error();
//                e.msg = "Parameter Missing";
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }
//            Common.ValidateRequest(Token, ref DomainId, ref UserCntId);
//            UserAccess objUserAccess = new UserAccess();
//            objUserAccess.DomainID = DomainId;
//            DataTable dtTable;
//            dtTable = objUserAccess.GetECommerceDetails();
//            if (dtTable.Rows.Count == 1)
//            {
//                objItem.COGSChartAcntId = int.Parse(dtTable.Rows[0]["numDefaultCOGSChartAcntId"].ToString());
//                objItem.AssetChartAcntId = int.Parse(dtTable.Rows[0]["numDefaultAssetChartAcntId"].ToString());
//                objItem.IncomeChartAcntId = int.Parse(dtTable.Rows[0]["numDefaultIncomeChartAcntId"].ToString());
//                objItem.WarehouseID = long.Parse(dtTable.Rows[0]["numDefaultWareHouseID"].ToString());
//                long lngWareHouseId = 0;
//                if (long.TryParse(WareHouseID, out lngWareHouseId)) // override Default warehouse if WarehouseID supplied
//                {
//                    objItem.WarehouseID = lngWareHouseId;
//                }
//            }
//            if (objItem.ItemType == "S")
//            {
//                objItem.COGSChartAcntId = 0;
//                objItem.AssetChartAcntId = 0;
//            }

//            objItem.ItemCode = ItemCode;
//            objItem.ItemName = ItemName;
//            objItem.ItemDesc = ItemDesc;
//            //pass “P” if Inventory
//            //pass “N” if Non-Inventory
//            //pass “S” if Service
//            if ((ItemType == "P") || (ItemType == "N") || (ItemType == "S"))
//            {
//                objItem.ItemType = ItemType;
//            }
//            else
//            {
//                error e = new error();
//                e.msg = "Invalid ItemType";
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }

//            objItem.ListPrice = double.Parse(ListPrice);
//            objItem.ItemClassification = 0;
//            objItem.Taxable = false;
//            objItem.ModelID = ModelID;
//            objItem.SKU = SKU;
//            objItem.KitParent = false;
//            objItem.DateEntered = DateTime.UtcNow;
//            objItem.DomainID = DomainId;
//            objItem.UserCntID = UserCntId;
//            objItem.bitSerialized = false;
//            objItem.Weight = double.Parse(Weight);
//            objItem.Width = double.Parse(Width);
//            objItem.Length = double.Parse(Length);
//            objItem.Height = double.Parse(Height);
//            String strError = objItem.ManageItemsAndKits();
//            if (strError.Length > 2)
//            {
//                error e = new error();
//                e.msg = strError;
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }
//            if (objItem.ItemCode > 0 && objItem.WarehouseID > 0)
//            {
//                objItem.WareHouseItemID = objItem.AddWareHouseForitems();
//                objItem.byteMode = 0;
//                long lngNoOfUnit = 0;
//                if (long.TryParse(NoOfUnit, out lngNoOfUnit))
//                {
//                    objItem.NoofUnits = lngNoOfUnit;
//                    objItem.UpdateInventory();
//                }
//            }
//            return objItem.ItemCode.ToString();
//        }

//        //http://localhost:1910/Item.svc/GetItem?ItemCode=872&Token=99bfccbc-fd9e-49cc-89e8-83214d6e93a6
//        [WebHelp(Comment = "Returns a Item when ItemCode is found")]
//        [WebGet(UriTemplate = UriTemplate.GetItem)]
//        [FaultContract(typeof(error))]
//        [OperationContract]
//        public ItemData GetItem(int ItemCode, string Token)
//        {
//            if (ItemCode < 1 || string.IsNullOrEmpty(Token))
//            {
//                error e = new error();
//                e.msg = "Parameter Missing";
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }
//            Common.ValidateRequest(Token, ref DomainId, ref UserCntId);
//            CItems objItem = new CItems();
//            DataSet dsItems = new DataSet();
//            objItem.DomainID = DomainId;
//            objItem.ItemCode = ItemCode;
//            objItem.mode = false;
//            dsItems = objItem.GetItemsForAPI();

//            if (dsItems.Tables[0].Rows.Count > 0)
//            {
//                ItemData objItem1 = new ItemData();

//                DataRow dr = dsItems.Tables[0].NewRow();
//                dr = dsItems.Tables[0].Rows[0];

//                objItem1.ItemCode = dr["numItemCode"].ToString().ToString();
//                objItem1.ItemName = dr["vcItemName"].ToString();
//                objItem1.ItemDesc = dr["txtItemDesc"].ToString();
//                objItem1.ItemType = dr["charItemType"].ToString();
//                objItem1.ListPrice = dr["monListPrice"].ToString();
//                objItem1.ModelID = dr["vcModelID"].ToString();
//                objItem1.SKU = dr["vcSKU"].ToString();
//                objItem1.NoOfUnit = dr["QtyOnHand"].ToString();
//                objItem1.UnitOfMeasure = dr["vcUnitofMeasure"].ToString();
//                objItem1.Weight = dr["fltWeight"].ToString();
//                objItem1.Height = dr["fltHeight"].ToString();
//                objItem1.Length = dr["fltLength"].ToString();
//                objItem1.Width = dr["fltWidth"].ToString();

//                return objItem1;
//            }
//            else
//            {
//                error e = new error();
//                e.msg = "ItemCode Does not exist";
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }
//        }

//        //http://localhost:1910/Item.svc/GetItems?Token=99bfccbc-fd9e-49cc-89e8-83214d6e93a6&ItemCode=1000
//        [WebHelp(Comment = "Get All Items greater than ItemCode Supplied,Returns Data as a string")]
//        [WebGet(UriTemplate = UriTemplate.GetItems)]
//        [FaultContract(typeof(error))]
//        [OperationContract]
//        public ItemData[] GetItems(string Token, int ItemCode)
//        {
//            if (string.IsNullOrEmpty(Token))
//            {
//                error e = new error();
//                e.msg = "Parameter Missing";
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }
//            Common.ValidateRequest(Token, ref DomainId, ref UserCntId);
//            CItems objItem = new CItems();
//            DataSet dsItems = new DataSet();

//            objItem.DomainID = DomainId;
//            objItem.ItemCode = ItemCode;
//            objItem.mode = true;
//            dsItems = objItem.GetItemsForAPI();
//            System.Collections.Generic.List<ItemData> objItemList = new System.Collections.Generic.List<ItemData>();


//            foreach (DataRow dr in dsItems.Tables[0].Rows)
//            {
//                ItemData objItem1 = new ItemData();

//                objItem1.ItemCode = dr["numItemCode"].ToString().ToString();
//                objItem1.ItemName = dr["vcItemName"].ToString();
//                objItem1.ItemDesc = dr["txtItemDesc"].ToString();
//                objItem1.ItemType = dr["charItemType"].ToString();
//                objItem1.ListPrice = dr["monListPrice"].ToString();
//                objItem1.ModelID = dr["vcModelID"].ToString();
//                objItem1.SKU = dr["vcSKU"].ToString();
//                objItem1.NoOfUnit = dr["QtyOnHand"].ToString();
//                objItem1.UnitOfMeasure = dr["vcUnitofMeasure"].ToString();
//                objItem1.Weight = dr["fltWeight"].ToString();
//                objItem1.Height = dr["fltHeight"].ToString();
//                objItem1.Length = dr["fltLength"].ToString();
//                objItem1.Width = dr["fltWidth"].ToString();

//                objItemList.Add(objItem1);
//            }
//            return objItemList.ToArray();
//        }

//        //http://localhost:1910/Item.svc/DeleteItem?ItemCode=1016&Token=99bfccbc-fd9e-49cc-89e8-83214d6e93a6
//        [WebHelp(Comment = "Delete item from inventory")]
//        [WebGet(UriTemplate = UriTemplate.DeleteItem)]
//        [FaultContract(typeof(error))]
//        [OperationContract]
//        public bool DeleteItem(int ItemCode, string Token)
//        {
//            if (ItemCode < 1 || string.IsNullOrEmpty(Token))
//            {
//                error e = new error();
//                e.msg = "Parameter Missing";
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }
//            Common.ValidateRequest(Token, ref DomainId, ref UserCntId);
//            CItems objItem = new CItems();
//            objItem.ItemCode = ItemCode;
//            objItem.DomainID = DomainId;
//            if (objItem.DeleteItems())
//            {
//                return true;
//            }
//            else
//            {
//                error e = new error();
//                e.msg = "Dependent Records Exists. Cannot Be deleted";
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }
//        }
//        //http://localhost:1910/Item.svc/InventoryStatus?Token=99bfccbc-fd9e-49cc-89e8-83214d6e93a6&ItemCode=99&WarehouseID=
//        [WebHelp(Comment = "Get Item Status from Inventory")]
//        [WebGet(UriTemplate = UriTemplate.GetInventoryStatus)]
//        [FaultContract(typeof(error))]
//        [OperationContract]
//        public string GetInventoryStatus(string Token, int ItemCode, string WarehouseID)
//        {
//            if (ItemCode < 1 || string.IsNullOrEmpty(Token))
//            {
//                error e = new error();
//                e.msg = "Parameter Missing";
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }
//            Common.ValidateRequest(Token, ref DomainId, ref UserCntId);
//            CItems objItem = new CItems();
//            objItem.ItemCode = ItemCode;
//            objItem.WarehouseID = WarehouseID == "" ? 0 : long.Parse(WarehouseID);
//            objItem.DomainID = DomainId;
//            return objItem.GetInventoryItemStatus();
//        }

//        //http://localhost:1910/Item.svc/ItemPriceRecommendation?Token=99bfccbc-fd9e-49cc-89e8-83214d6e93a6&ItemCode=99&NoOfunits=20
//        [WebHelp(Comment = "Get Item recommended price based on old orders,returns amount")]
//        [WebGet(UriTemplate = UriTemplate.GetItemPriceRec)]
//        [FaultContract(typeof(error))]
//        [OperationContract]
//        public string GetItemPriceRec(string Token, int ItemCode, int NoOfunits)
//        {
//            if (ItemCode < 1 || string.IsNullOrEmpty(Token) || NoOfunits < 1)
//            {
//                error e = new error();
//                e.msg = "Parameter Missing";
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }
//            Common.ValidateRequest(Token, ref DomainId, ref UserCntId);
//            string Amount = "0.0";
//            DataTable dtItemPricemgmt = default(DataTable);

//            CItems objItem = new CItems();
//            objItem.ItemCode = ItemCode;
//            objItem.NoofUnits = NoOfunits;
//            objItem.byteMode = 1;
//            if (objItem.byteMode == 1)
//            {
//                DataSet ds = default(DataSet);
//                ds = objItem.GetPriceManagementList1();
//                dtItemPricemgmt = ds.Tables[0];
//                if (dtItemPricemgmt.Rows.Count == 0)
//                {
//                    dtItemPricemgmt = ds.Tables[2];
//                    dtItemPricemgmt.Merge(ds.Tables[1]);
//                }
//                else
//                {
//                    dtItemPricemgmt.Merge(ds.Tables[1]);
//                }
//            }
//            else
//            {
//                dtItemPricemgmt = objItem.GetPriceManagementList1().Tables[0];
//            }
//            if (dtItemPricemgmt.Rows.Count > 0)
//            {
//                Amount = string.Format("{0:0.00}", decimal.Parse(dtItemPricemgmt.Rows[0]["ListPrice"].ToString()));
//            }
//            return Amount;
//        }

//        long lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId;

//        //http://localhost:1910/Item.svc/AddOrder?Token=99bfccbc-fd9e-49cc-89e8-83214d6e93a6&CompanyName=ZTE&FirstName=Aashish&LastName=Vadher&Email=ashish.zte@gmail.com&Phone=9758658965&BillStreet=7th cross fifth main,jayanagar&BillCity=Bangalore&BillState=karnataka&BillCountry=India&BillPostalCode=380061&ShipStreet=13,keval Kunj society,K.k.nagar cross road ghhatlodia&ShipCity=Ahmedabad&ShipState=Gujarat&ShipCountry=India&ShipPostalCode=506641&OppSource=www.storrz.com&ShipCost=10&Items=18:2:25.50,19:3:100.21,25:1:80,
//        [WebGet(UriTemplate = UriTemplate.AddOrder)]
//        [FaultContract(typeof(error))]
//        [OperationContract]
//        public string AddOrder(string Token, string CompanyName, string FirstName, string LastName, string Email, string Phone, string BillStreet, string BillCity, string BillState, string BillCountry, string BillPostalCode, string ShipStreet, string ShipCity, string ShipState, string ShipCountry, string ShipPostalCode, string OppSource, double ShipCost, string Items)
//        {
//            if (string.IsNullOrEmpty(Token) || string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName))
//            {
//                error e = new error();
//                e.msg = "Parameter Missing";
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }

//            Common.ValidateRequest(Token, ref DomainId, ref UserCntId);
//            CLeads objLeads = new CLeads();
//            ImportWizard objImpWzd = new ImportWizard();
//            CContacts objContacts = new CContacts();
//            DataSet dsItems = new DataSet();
//            DataTable dtMilestone;
//            DataSet dsNew = new DataSet();
//            OppBizDocs objOppBizDocs = new OppBizDocs();
//            String[] arrOutPut;
//            DataTable dtItems = new DataTable();


//            CreateItemDataSet(ref dtItems, Items);

//            //Start Insert Prospect
//            //Check Existing Contact with same email ID
//            if ((FirstName + LastName).Length > 0)
//            {
//                objLeads.Email = Email;
//                objLeads.DomainID = DomainId;
//                objLeads.GetConIDCompIDDivIDFromEmail();
//                if (objLeads.ContactID > 0 & objLeads.DivisionID > 0)
//                {
//                    lngCntID = objLeads.ContactID;
//                    lngDivId = objLeads.DivisionID;
//                }
//                else
//                {
//                    objLeads.CompanyName = CompanyName;
//                    objLeads.CustName = FirstName + " " + LastName;
//                    objLeads.CRMType = 1;
//                    objLeads.DivisionName = "-";
//                    objLeads.LeadBoxFlg = 1;
//                    objLeads.UserCntID = UserCntId;
//                    objLeads.FirstName = FirstName;
//                    objLeads.LastName = LastName;
//                    objLeads.ContactPhone = Phone;
//                    objLeads.PhoneExt = "";
//                    objLeads.Email = Email;
//                    objLeads.DomainID = DomainId;
//                    objLeads.ContactType = 70;
//                    objLeads.PrimaryContact = true;
//                    objLeads.UpdateDefaultTax = false;
//                    objLeads.NoTax = false;

//                    objLeads.CompanyID = objLeads.CreateRecordCompanyInfo();
//                    lngDivId = objLeads.CreateRecordDivisionsInfo();
//                    objLeads.ContactID = 0;
//                    objLeads.DivisionID = lngDivId;

//                    lngCntID = objLeads.CreateRecordAddContactInfo();
//                }
//                if (lngCntID == 0)
//                {
//                    return "";
//                }

//                SaveTaxTypes();

//                objContacts.BillStreet = BillStreet;
//                objContacts.BillCity = BillCity;
//                objContacts.BillState = objImpWzd.GetStateAndCountry(1, BillState, DomainId);
//                objContacts.BillCountry = objImpWzd.GetStateAndCountry(0, BillCountry, DomainId);
//                objContacts.BillPostal = BillPostalCode;
//                objContacts.BillingAddress = 0;
//                //shipping address is not same as billing 

//                objContacts.ShipStreet = ShipStreet;
//                objContacts.ShipState = objImpWzd.GetStateAndCountry(1, ShipState, DomainId);
//                objContacts.ShipPostal = ShipPostalCode;
//                objContacts.ShipCountry = objImpWzd.GetStateAndCountry(0, ShipCountry, DomainId);
//                objContacts.ShipCity = ShipCity;

//                objContacts.DivisionID = lngDivId;
//                objContacts.UpdateCompanyAddress();
//            }
//            //End Insert Prospect
//            MOpportunity objOpportunity = new MOpportunity();

//            objOpportunity.OpportunityId = 0;
//            objOpportunity.ContactID = lngCntID;
//            objOpportunity.DivisionID = lngDivId;

//            objOpportunity.UserCntID = UserCntId;
//            objOpportunity.OpportunityName = objLeads.CompanyName + "-SO-" + DateTime.Now.ToString("MMMM");
//            objOpportunity.EstimatedCloseDate = System.DateTime.UtcNow;
//            //DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
//            objOpportunity.PublicFlag = 0;
//            objOpportunity.DomainID = DomainId;
//            objOpportunity.OppType = 1;
//            dsItems.Tables.Clear();
//            dsItems.Tables.Add(dtItems.Copy());
//            if (dsItems.Tables[0].Rows.Count > 0)
//            {
//                //TotalAmount = (decimal)dsItems.Tables[0].Compute("Sum(monTotAmount)", "");
//                objOpportunity.strItems = (dtItems.Rows.Count > 0 ? "<?xml version=\"1.0\" encoding=\"iso-8859-1\" ?>" + dsItems.GetXml() : "");
//            }
//            objOpportunity.vcSource = OppSource;
//            arrOutPut = objOpportunity.SaveFromAPI();
//            lngOppId = long.Parse(arrOutPut[0]);

//            objOpportunity.OpportunityId = lngOppId;
//            dtMilestone = objOpportunity.BusinessProcessByOpID();
//            dtMilestone.Rows[1]["bitStageCompleted"] = 1;
//            dtMilestone.Rows[1]["bintStageComDate"] = System.DateTime.UtcNow;
//            objOpportunity.Mode = 0;
//            dtMilestone.TableName = "Table";
//            dsNew.Tables.Add(dtMilestone.Copy());
//            objOpportunity.strMilestone = dsNew.GetXml();
//            dsNew.Tables.Remove(dsNew.Tables[0]);
//            objOpportunity.SaveMilestone();


//            CreateOppBizDoc(objOppBizDocs, ShipCost.ToString());

//            return lngOppId.ToString();
//        }

//        private void CreateOppBizDoc(OppBizDocs objOppBizDocs, string ShipCost)
//        {
//            DataSet ds = new DataSet();
//            DataTable dtOppBiDocItems;
//            BACRM.BusinessLogic.Accounting.JournalEntry objJournalEntries = new BACRM.BusinessLogic.Accounting.JournalEntry();
//            long lngBizDoc;

//            try
//            {
//                objOppBizDocs.OppId = lngOppId;
//                objOppBizDocs.OppType = 1;
//                objOppBizDocs.UserCntID = UserCntId;
//                objOppBizDocs.DomainID = DomainId;
//                objOppBizDocs.vcPONo = "";
//                objOppBizDocs.vcComments = "";
//                objOppBizDocs.ShipCost = decimal.Parse(ShipCost);


//                lngBizDoc = objOppBizDocs.GetAuthorizativeOpportuntiy();
//                if (lngBizDoc == 0) return;

//                objOppBizDocs.BizDocId = lngBizDoc;
//                objOppBizDocs.OppBizDocId = 0;
//                OppBizDocID = objOppBizDocs.SaveBizDoc();

//                objOppBizDocs.OppBizDocId = OppBizDocID;
//                ds.Tables.Clear();
//                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting();
//                dtOppBiDocItems = ds.Tables[0];


//                CalculateDealAmount objCalculateDealAmount = new CalculateDealAmount();
//                objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, DomainId, dtOppBiDocItems,false);


//                JournalId = SaveDataToHeader(objCalculateDealAmount.GrandTotal);

//                objJournalEntries.SaveJournalEntriesSales(lngOppId, DomainId, dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, objCalculateDealAmount.DivisionID, long.Parse(ds.Tables[1].Rows[0]["numCurrencyID"].ToString()), long.Parse(ds.Tables[1].Rows[0]["fltExchangeRate"].ToString()), 0, 0);
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        public void SaveTaxTypes()
//        {
//            BACRM.BusinessLogic.Prospects.CProspects objProspects = new BACRM.BusinessLogic.Prospects.CProspects();
//            DataTable dtCompanyTaxTypes = new DataTable();
//            string strdetails;
//            DataRow drtax;
//            DataSet ds = new DataSet();
//            try
//            {
//                if (dtCompanyTaxTypes.Columns.Count == 0)
//                {
//                    dtCompanyTaxTypes.Columns.Add("numTaxItemID");
//                    dtCompanyTaxTypes.Columns.Add("bitApplicable");
//                }

//                drtax = dtCompanyTaxTypes.NewRow();
//                drtax["numTaxItemID"] = 0;
//                drtax["bitApplicable"] = 1;
//                dtCompanyTaxTypes.Rows.Add(drtax);

//                dtCompanyTaxTypes.TableName = "Table";
//                ds.Tables.Add(dtCompanyTaxTypes);
//                strdetails = ds.GetXml();
//                ds.Tables.Remove(ds.Tables[0]);

//                objProspects.DivisionID = lngDivId;
//                objProspects.strCompanyTaxTypes = strdetails;
//                objProspects.ManageCompanyTaxTypes();
//                ds.Dispose();
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        private long SaveDataToHeader(decimal GrandTotal)
//        {
//            long lntJournalId;
//            BACRM.BusinessLogic.Accounting.JournalEntry lobjAuthoritativeBizDocs = new BACRM.BusinessLogic.Accounting.JournalEntry();
//            try
//            {
//                lobjAuthoritativeBizDocs.Entry_Date = DateTime.UtcNow;
//                lobjAuthoritativeBizDocs.Amount = GrandTotal;
//                lobjAuthoritativeBizDocs.DomainID = DomainId;
//                lobjAuthoritativeBizDocs.JournalId = 0;
//                lobjAuthoritativeBizDocs.UserCntID = UserCntId;
//                lobjAuthoritativeBizDocs.OppBIzDocID = OppBizDocID;
//                lobjAuthoritativeBizDocs.OppId = lngOppId;
//                lntJournalId = lobjAuthoritativeBizDocs.SaveDataToJournalEntryHeader();
//                return lntJournalId;
//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        private void CreateItemDataSet(ref DataTable dtItems, string strItems)
//        {
//            try
//            {
//                dtItems.Columns.Add("numoppitemtCode");
//                dtItems.Columns.Add("numItemCode");
//                dtItems.Columns.Add("numUnitHour");
//                dtItems.Columns.Add("monPrice");
//                dtItems.Columns.Add("monTotAmount", typeof(double));
//                dtItems.Columns.Add("vcItemDesc");
//                dtItems.Columns.Add("numWarehouseItmsID");
//                dtItems.Columns.Add("vcItemName");
//                dtItems.Columns.Add("ItemType");
//                dtItems.Columns.Add("Weight");
//                dtItems.Columns.Add("Op_Flag");
//                dtItems.Columns.Add("DropShip", typeof(bool));
//                dtItems.Columns.Add("WebApiId", typeof(int));
//                dtItems.TableName = "Item";

//                string ItemCode;
//                string Units;
//                string Price;
//                string[] separator = { "," };

//                string[] Items = WebAPI.UrlDecode(strItems).Split(separator, StringSplitOptions.RemoveEmptyEntries);

//                DataRow drItem;
//                dtItems.Rows.Clear();
//                for (int i = 0; i < Items.Length; i++)
//                {
//                    string[] Item = Items[i].ToString().Split(':');
//                    ItemCode = Item[0].ToString();
//                    Units = Item[1].ToString();
//                    Price = Item[2].ToString();

//                    drItem = dtItems.NewRow();

//                    drItem["numoppitemtCode"] = (i + 1).ToString();
//                    drItem["numItemCode"] = ItemCode;
//                    drItem["numUnitHour"] = Units;
//                    drItem["monPrice"] = Price;
//                    drItem["monTotAmount"] = double.Parse(Price) * int.Parse(Units);
//                    drItem["vcItemDesc"] = "";
//                    //Passing blank as Cularis contains HTML Tables and which creates problem while showing it in products and services 'ItemNode("description").InnerXml 
//                    drItem["numWarehouseItmsID"] = -1;//indicates to use Default WarehouseId from ECommerce Setting Page. 
//                    //will be given from procedure
//                    drItem["vcItemName"] = "";
//                    drItem["ItemType"] = "";
//                    drItem["Weight"] = "";
//                    drItem["Op_Flag"] = 1;
//                    drItem["DropShip"] = false;
//                    drItem["WebApiId"] = 0;
//                    dtItems.Rows.Add(drItem);
//                }
//            }
//            catch (Exception ex)
//            {
//                error e = new error();
//                e.msg = ex.Message;
//                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
//                throw fe;
//            }
//        }



//        public long DomainId;
//        public long UserCntId;
//    }
//}
