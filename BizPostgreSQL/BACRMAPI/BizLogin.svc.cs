﻿/// Copyright (c) Microsoft Corporation.  All rights reserved.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
//using Microsoft.ServiceModel.Web;
using System.ServiceModel.Activation;
using System.Linq;
using System.Net;
using BACRM.BusinessLogic.Common;
using System.Data;
using System.Web.SessionState;
using System.Web;
using BACRM.BusinessLogic.Admin;
using BACRM.BusinessLogic.WebAPI;
// The following line sets the default namespace for DataContract serialized typed to be ""
[assembly: ContractNamespace("", ClrNamespace = "BACRMAPI")]

namespace BACRMAPI
{
    // TODO: Modify the service behavior settings (instancing, concurrency etc) based on the service's requirements.
    // TODO: NOTE: Please set IncludeExceptionDetailInFaults to false in production environments
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Single)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceContract]
    public class BizLoginService
    {
        //http://localhost:1910/BizLogin.svc/Login?user=administrator@bizautodemo.com&pass=Test1213
        //[WebHelp(Comment = "Returns the Token if authenticated, which will be used for subsequent requests")]
        [WebGet(UriTemplate = UriTemplate.AuthenticateUser)]
        [OperationContract]
        string AuthenticateUser(string UserName, string Password)
        {
            if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password))
            {
                error e = new error();
                e.msg = "Username/Password can not be blank.";
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
            DataSet dsUserDetails = new DataSet();
            CCommon objCommon = new CCommon();
            dsUserDetails = objCommon.CheckUserAtLogin(UserName.Trim(), objCommon.Encrypt(Password.Trim()), "N");
            if (dsUserDetails.Tables[0].Rows.Count == 1)
            {
                // generate Token and Save
                WebService objWS = new WebService();
                objWS.Token = System.Guid.NewGuid().ToString();
                objWS.DomainID = long.Parse(dsUserDetails.Tables[0].Rows[0]["numDomainID"].ToString());
                //objWS.UserCntId = 0 
                if (objWS.ManageWebService())
                    return objWS.Token;
                else
                    return "";
            }
            else
            {
                error e = new error();
                e.msg = "Invalid Username/Password.";
                FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
                throw fe;
            }
        }

        ////[WebHelp(Comment = "Authentication using Token")]
        //[WebGet(UriTemplate = UriTemplate.AuthenticateUserUsingToken)]
        //[OperationContract]
        //BizLogin AuthenticateUserUsingToken(string Token)
        //{
        //    if (string.IsNullOrEmpty(Token))
        //    {
        //        error e = new error();
        //        e.msg = "Token can not be blank.";
        //        FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
        //        throw fe;
        //    }
        //    BizLogin objLogin = new BizLogin();
        //    objLogin = objLogin.IsAuthenticated(Token);
        //    return objLogin;
        //}
        //[WebHelp(Comment = "Returns the Token if authenticated, which will be used for subsequent requests")]
        //    [WebGet(UriTemplate = UriTemplate.PortalLogin)]
        //    [OperationContract]
        //    bool PortalLogin(string UserName, string Password, long DomainID)
        //    {
        //        if (string.IsNullOrEmpty(UserName) || string.IsNullOrEmpty(Password) || DomainID == 0)
        //        {
        //            error e = new error();
        //            e.msg = "Username/Password can not be blank.";
        //            FaultException<error> fe = new FaultException<error>(e, new FaultReason(e.msg), new FaultCode(e.msg), e.msg);
        //            throw fe;
        //        }
        //        UserAccess objUser = new UserAccess();
        //        DataTable dtUser = new DataTable();
        //        objUser.Email = UserName;
        //        objUser.Password = Password;
        //        objUser.DomainID = DomainID;
        //        dtUser = objUser.ExtranetLogin();
        //        if (dtUser.Rows[0][0].ToString() == "1")
        //            return true;
        //        else
        //            return false;
        //    }
    }
}
