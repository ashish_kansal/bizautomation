SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter TRIGGER [dbo].[UpdateCurrentBalanceAfterDelete]
   ON  [dbo].[General_Journal_Details]
   AFTER DELETE
AS 
BEGIN

DECLARE @OnumChartAccountId numeric(18,0)
DECLARE @ODebitAmount DECIMAL(20,5)
DECLARE @OCreditAmount DECIMAL(20,5)
DECLARE @numDomainId NUMERIC(18,0)

SET @ODebitAmount=0;
SET @OCreditAmount=0;


--SET @OnumChartAccountId= (SELECT numChartAcntId from Deleted)
--SET @ODebitAmount= isnull((SELECT numDebitAmt from Deleted),0);
--SET @OCreditAmount= isnull((SELECT numCreditAmt from Deleted),0);


declare BizDoc_Cursor CURSOR FOR
SELECT numChartAcntId,numDebitAmt,numCreditAmt,numDomainId from Deleted

OPEN BizDoc_Cursor

FETCH   NEXT FROM BizDoc_Cursor 
into @OnumChartAccountId,@ODebitAmount,@OCreditAmount,@numDomainId;
WHILE @@FETCH_STATUS = 0
   BEGIN

		UPDATE 	Chart_of_Accounts SET numOpeningBal = isnull(numOpeningBal,0) - @ODebitAmount + @OCreditAmount
		where numAccountId=@OnumChartAccountId
		AND numdomainID = @numDomainId;

		FETCH  NEXT  FROM BizDoc_Cursor 
		into @OnumChartAccountId,@ODebitAmount,@OCreditAmount,@numDomainId;
   END;

CLOSE BizDoc_Cursor;
END 
SET NOCOUNT OFF;
