GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Item_IUD]'))
	DROP TRIGGER [dbo].[Item_IUD]
GO

CREATE TRIGGER dbo.Item_IUD
ON dbo.Item
AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @action as char(1) = 'I';

	DECLARE @bitElasticSearch BIT

	SET @bitElasticSearch = ISNULL((SELECT TOP 1 ECD.bitElasticSearch FROM 
	(SELECT * FROM INSERTED UNION SELECT * FROM DELETED) T
	INNER JOIN eCommerceDTL ECD ON (ECD.numDomainId = T.numDomainID AND ECD.bitElasticSearch = 1)),0)

	IF EXISTS(SELECT * FROM DELETED)
    BEGIN
        SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
    END
    ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
	BEGIN
		RETURN; -- Nothing updated or inserted.
    END

	IF @action = 'I'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Item',numItemCode,'Insert',numDomainID FROM INSERTED

		IF(@bitElasticSearch = 1)
		BEGIN

		END
		
	END
	ELSE IF @action = 'U'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Item',numItemCode,'Update',numDomainID FROM INSERTED

		INSERT INTO ElasticSearchBizCart
		(
			vcModule
			,vcValue
			,vcAction
			,numDomainID
		) 
		SELECT 
			'Item'
			,i.numItemCode
			,'Update'
			,i.numDomainID
		FROM 
			INSERTED I
	END
	ELSE IF @action = 'D'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Item',numItemCode,'Delete',numDomainID FROM DELETED

		INSERT INTO ElasticSearchBizCart
		(
			vcModule
			,vcValue
			,vcAction
			,numDomainID
		) 
		SELECT 
			'Item'
			,i.numItemCode
			,'Delete'
			,i.numDomainID
		FROM 
			INSERTED I
	END
END