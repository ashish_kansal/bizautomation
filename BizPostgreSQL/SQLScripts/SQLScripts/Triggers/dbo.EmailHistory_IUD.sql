GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[EmailHistory_IUD]'))
	DROP TRIGGER [dbo].[EmailHistory_IUD]
GO

CREATE TRIGGER dbo.EmailHistory_IUD
ON dbo.EmailHistory
AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @action as char(1) = 'I';

	IF EXISTS(SELECT * FROM DELETED)
    BEGIN
        SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
    END
    ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
	BEGIN
		RETURN; -- Nothing updated or inserted.
    END

	IF @action = 'I'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Email',numEmailHstrID,'Insert',numDomainID FROM INSERTED
	END
	ELSE IF @action = 'D'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Email',numEmailHstrID,'Delete',numDomainID FROM DELETED
	END
END