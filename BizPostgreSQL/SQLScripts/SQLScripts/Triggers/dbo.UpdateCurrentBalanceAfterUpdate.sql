SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TRIGGER [dbo].[UpdateCurrentBalanceAfterUpdate]
   ON  [dbo].[General_Journal_Details]
   AFTER UPDATE
AS 

DECLARE @numChartAccountId numeric(18,0)
DECLARE @DebitAmount DECIMAL(20,5)
DECLARE @CreditAmount DECIMAL(20,5)

DECLARE @OnumChartAccountId numeric(18,0)
DECLARE @ODebitAmount DECIMAL(20,5)
DECLARE @OCreditAmount DECIMAL(20,5)
DECLARE @numDomainId NUMERIC(18,0)


SET @DebitAmount= 0;
SET @CreditAmount= 0;

SET @ODebitAmount=0;
SET @OCreditAmount=0;

--SET @numChartAccountId= isnull((SELECT numChartAcntId from Inserted),0);
--SET @DebitAmount= isnull((SELECT numDebitAmt from Inserted),0);
--SET @CreditAmount= isnull((SELECT numCreditAmt from Inserted),0);
--
--SET @OnumChartAccountId= (SELECT numChartAcntId from Deleted)
--SET @ODebitAmount= isnull((SELECT numDebitAmt from Deleted),0);
--SET @OCreditAmount= isnull((SELECT numCreditAmt from Deleted),0);


BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



declare BizDoc_CursorD CURSOR FOR
SELECT numChartAcntId,numDebitAmt,numCreditAmt,numDomainId from Deleted

OPEN BizDoc_CursorD

FETCH   NEXT FROM BizDoc_CursorD 
into @OnumChartAccountId,@ODebitAmount,@OCreditAmount,@numDomainId;
WHILE @@FETCH_STATUS = 0
   BEGIN

		UPDATE 	Chart_of_Accounts SET numOpeningBal = isnull(numOpeningBal,0) - @ODebitAmount + @OCreditAmount
		where numAccountId=@OnumChartAccountId
		AND numDomainID = @numDomainId;

		FETCH  NEXT  FROM BizDoc_CursorD 
		into @OnumChartAccountId,@ODebitAmount,@OCreditAmount,@numDomainId;
   END;

CLOSE BizDoc_CursorD;


	
declare BizDoc_Cursor CURSOR FOR
SELECT numChartAcntId,numDebitAmt,numCreditAmt,numDomainID from Inserted

OPEN BizDoc_Cursor

FETCH   NEXT FROM BizDoc_Cursor 
into @numChartAccountId,@DebitAmount,@CreditAmount,@numDomainId;
WHILE @@FETCH_STATUS = 0
   BEGIN
		UPDATE 	Chart_of_Accounts SET numOpeningBal = isnull(numOpeningBal,0) + @DebitAmount - @CreditAmount
		where numAccountId=@numChartAccountId
		AND numDomainID = @numDomainId;

      FETCH  NEXT  FROM BizDoc_Cursor 
		into @numChartAccountId,@DebitAmount,@CreditAmount,@numDomainId;
   END;

CLOSE BizDoc_Cursor;


END


SET NOCOUNT OFF;




