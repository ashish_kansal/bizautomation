GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[StagePercentageDetailsTaskTimeLog_Update]'))
	DROP TRIGGER [dbo].[StagePercentageDetailsTaskTimeLog_Update]
GO

CREATE TRIGGER dbo.StagePercentageDetailsTaskTimeLog_Update ON dbo.StagePercentageDetailsTaskTimeLog AFTER UPDATE
AS 
BEGIN
	IF EXISTS (SELECT 
					CL.numContractId  
				FROM 
					ContractsLog CL 
				INNER JOIN
					Contracts C
				ON
					CL.numContractId = C.numContractId
				INNER JOIN	
					INSERTED
				ON
					C.numDomainID = inserted.numDomainID
					AND CL.numReferenceId = inserted.numTaskID
					AND vcDetails='1')
	BEGIN
		DECLARE @numDomainID NUMERIC(18,0)
		DECLARE @numContractsLogId NUMERIC(18,0)
		DECLARE @numContactID NUMERIC(18,0)
		DECLARE @numUsedTime INT
		DECLARE @numTaskID NUMERIC(18,0)

		SELECT 
			@numDomainID=inserted.numDomainID
			,@numContractsLogId=numContractsLogId
			,@numContactID = CL.numContractId
			,@numUsedTime = numUsedTime
			,@numTaskID = inserted.numTaskID
		FROM
			ContractsLog CL 
		INNER JOIN
			Contracts C
		ON
			CL.numContractId = C.numContractId
		INNER JOIN	
			INSERTED
		ON
			C.numDomainID = inserted.numDomainID
			AND CL.numReferenceId = inserted.numTaskID
			AND vcDetails='1'

		
		IF ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=@numTaskID ORDER BY ID DESC),0) = 4
		BEGIN
			UPDATE Contracts SET timeUsed=ISNULL(timeUsed,0)-ISNULL(@numUsedTime,0),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContactID

			-- GET UPDATED TIME
			SET @numUsedTime = CAST(dbo.GetTimeSpendOnTaskByProject(@numDomainID,@numTaskID,2) AS INT)
			
			UPDATE ContractsLog SET numUsedTime=(@numUsedTime * 60) WHERE numContractsLogId=@numContractsLogId	
		END
		ELSE
		BEGIN
			UPDATE Contracts SET timeUsed=ISNULL(timeUsed,0)-ISNULL(@numUsedTime,0),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContactID
			DELETE FROM ContractsLog WHERE numContractsLogId=@numContractsLogId
		END
	END
END
GO