GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AfterDeleteOpportunityBizDocs]'))
	DROP TRIGGER [dbo].[AfterDeleteOpportunityBizDocs]
GO

CREATE TRIGGER [dbo].[AfterDeleteOpportunityBizDocs]
ON [dbo].[OpportunityBizDocs]
AFTER DELETE
AS
BEGIN
	INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Delete',OpportunityMaster.numDomainID FROM DELETED INNER JOIN OpportunityMaster ON DELETED.numOppId = OpportunityMaster.numOppId
END