SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter TRIGGER [dbo].[UpdateCurrentBalanceAfterInsert]
   ON  [dbo].[General_Journal_Details]
   AFTER INSERT
AS 

DECLARE @numChartAccountId numeric(18,0)
DECLARE @DebitAmount DECIMAL(20,5)
DECLARE @CreditAmount DECIMAL(20,5)
DECLARE @numDomainId NUMERIC(18,0)

SET @DebitAmount= 0;
SET @CreditAmount= 0;


BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--SET @numChartAccountId= isnull((SELECT numChartAcntId from Inserted),0);
--SET @DebitAmount= isnull((SELECT numDebitAmt from Inserted),0);
--SET @CreditAmount= isnull((SELECT numCreditAmt from Inserted),0);

DECLARE @bitOpeningBalance BIT ;
SELECT TOP 1 @bitOpeningBalance = ISNULL(bitOpeningBalance,0) FROM dbo.General_Journal_Header WHERE numJournal_Id IN (SELECT numJournalId FROM Inserted)

IF @bitOpeningBalance =0
BEGIN
	declare BizDoc_Cursor CURSOR FOR
	SELECT numChartAcntId,numDebitAmt,numCreditAmt,numDomainId from Inserted

	OPEN BizDoc_Cursor

	FETCH   NEXT FROM BizDoc_Cursor 
	into @numChartAccountId,@DebitAmount,@CreditAmount,@numDomainId;
	WHILE @@FETCH_STATUS = 0
	   BEGIN
			UPDATE 	Chart_of_Accounts SET numOpeningBal = isnull(numOpeningBal,0) + @DebitAmount - @CreditAmount
			where numAccountId=@numChartAccountId
			AND numDomainId = @numDomainId;

		  FETCH  NEXT  FROM BizDoc_Cursor 
			into @numChartAccountId,@DebitAmount,@CreditAmount,@numDomainId;
	   END;

	CLOSE BizDoc_Cursor;
	
END

END




