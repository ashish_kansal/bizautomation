GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[StagePercentageDetailsTaskTimeLog_Delete]'))
	DROP TRIGGER [dbo].[StagePercentageDetailsTaskTimeLog_Delete]
GO

CREATE TRIGGER dbo.StagePercentageDetailsTaskTimeLog_Delete ON dbo.StagePercentageDetailsTaskTimeLog AFTER DELETE
AS 
BEGIN
	IF EXISTS (SELECT 
					CL.numContractId  
				FROM 
					ContractsLog CL 
				INNER JOIN
					Contracts C
				ON
					CL.numContractId = C.numContractId
				INNER JOIN	
					deleted
				ON
					C.numDomainID = deleted.numDomainID
					AND CL.numReferenceId = deleted.numTaskID
					AND vcDetails='1')
	BEGIN
		DECLARE @numContractsLogId NUMERIC(18,0)
		DECLARE @numContactID NUMERIC(18,0)
		DECLARE @numUsedTime INT

		SELECT 
			@numContractsLogId=numContractsLogId
			,@numContactID = CL.numContractId
			,@numUsedTime = numUsedTime
		FROM
			ContractsLog CL 
		INNER JOIN
			Contracts C
		ON
			CL.numContractId = C.numContractId
		INNER JOIN	
			deleted
		ON
			C.numDomainId = deleted.numDomainID
			AND CL.numReferenceId = deleted.numTaskID
			AND vcDetails='1'

		UPDATE Contracts SET timeUsed=ISNULL(timeUsed,0)-ISNULL(@numUsedTime,0),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContactID
		DELETE FROM ContractsLog WHERE numContractsLogId=@numContractsLogId
	END
END
GO