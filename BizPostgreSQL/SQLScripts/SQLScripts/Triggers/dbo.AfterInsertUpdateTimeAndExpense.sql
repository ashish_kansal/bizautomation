-- SELECT * FROM [OpportunityMaster] where monDealAmount is not null
--SELECT dbo.[GetDealAmount](1704,GETUTCDATE(),0)
Create TRIGGER [dbo].[AfterInsertUpdateTimeAndExpense]
ON [dbo].TimeAndExpense
after INSERT,UPDATE
AS
  DECLARE  @numOppId NUMERIC(9)
  
  SELECT 	@numOppId = numOppId
  FROM   inserted
  
  UPDATE [OpportunityMaster]
  SET    monDealAmount = dbo.[GetDealAmount](@numOppId,GETUTCDATE(),0)
  WHERE  [numOppId] = @numOppId
  