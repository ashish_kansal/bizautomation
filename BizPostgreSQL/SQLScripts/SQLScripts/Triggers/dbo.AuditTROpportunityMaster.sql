SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Alter trigger AuditTROpportunityMaster on OpportunityMaster for insert, update, delete
as

declare @bit int ,
@field int ,
@maxfield int ,
@char int ,
@fieldname varchar(128) ,
@TableName varchar(128) ,
@PKCols varchar(1000) ,
@sql varchar(2000),
@Type char(1) ,
@PKSelect varchar(1000)


select @TableName = 'OpportunityMaster'

-- Action
if exists (select * from inserted)
if exists (select * from deleted)
select @Type = 'U'
else
select @Type = 'I'
else
select @Type = 'D'

-- get list of columns
select numOppId,tintOppType,numContactId,numDivisionId,bintAccountClosingDate,bitPublicFlag,tintSource,vcPOppName,intPEstimatedCloseDate,numPClosingPercent,numCampainID,monPAmount,lngPConclAnalysis,tintActive,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,numRecOwner,tintOppStatus,tintshipped,bitOrder,numSalesOrPurType,numAssignedTo,numAssignedBy,numUserClosed,tintBillToType,tintShipToType,numCurrencyID,fltExchangeRate,monDealAmount,numStatus,numBusinessProcessID,fltDiscountTotal,bitDiscountTypeTotal,bitStockTransfer,bintOppToOrder,tintSourceType,tintTaxOperator,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,fltDiscount,bitDiscountType,numDiscountAcntType,numOppBizDocTempID,monShipCost,vcWebApiOrderNo,vcOppRefOrderNo,vcCouponCode,dtItemReceivedDate,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,bitUseShippersAccountNo   into #ins from inserted
select numOppId,tintOppType,numContactId,numDivisionId,bintAccountClosingDate,bitPublicFlag,tintSource,vcPOppName,intPEstimatedCloseDate,numPClosingPercent,numCampainID,monPAmount,lngPConclAnalysis,tintActive,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,numRecOwner,tintOppStatus,tintshipped,bitOrder,numSalesOrPurType,numAssignedTo,numAssignedBy,numUserClosed,tintBillToType,tintShipToType,numCurrencyID,fltExchangeRate,monDealAmount,numStatus,numBusinessProcessID,fltDiscountTotal,bitDiscountTypeTotal,bitStockTransfer,bintOppToOrder,tintSourceType,tintTaxOperator,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,fltDiscount,bitDiscountType,numDiscountAcntType,numOppBizDocTempID,monShipCost,vcWebApiOrderNo,vcOppRefOrderNo,vcCouponCode,dtItemReceivedDate,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,numPercentageComplete,numAccountClass,bitUseShippersAccountNo   into #del from deleted

-- Get primary key columns for full outer join
select @PKCols = COALESCE(@PKCols + ' and', ' on') + ' i.' + c.COLUMN_NAME + ' = d.' + c.COLUMN_NAME,
@PKSelect = COALESCE(@PKSelect+'+','') + '''<' + COLUMN_NAME + '=''+convert(varchar(100),coalesce(i.' + COLUMN_NAME +',d.' + COLUMN_NAME + '))+''>'''
from INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,
INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
where pk.TABLE_NAME = @TableName
and CONSTRAINT_TYPE = 'PRIMARY KEY'
and c.TABLE_NAME = pk.TABLE_NAME
and c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

--PRINT @PKSelect

if @PKCols is null
begin
	raiserror('no PK on table %s', 16, -1, @TableName)
	return
END

DECLARE @Valid AS INT;
--PRINT COLUMNS_UPDATED()
 
  DECLARE @numRecordID NUMERIC(9),@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9)
  SELECT @numRecordID = numOppId,@numDomainID=numDomainID,@numUserCntID=numModifiedBy FROM inserted

  DECLARE @AuditGUID uniqueidentifier
  SET @AuditGUID = NEWID()

  select @field = 0, @maxfield = max(ORDINAL_POSITION) from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = @TableName 
  DECLARE @Columns_Updated VARCHAR(1000)

	while @field < @maxfield
	BEGIN
		SET @Valid=0
		SET @fieldname=''

		SELECT TOP 1 @fieldname = COLUMN_NAME,@Valid=COLUMNPROPERTY(OBJECT_ID(TABLE_SCHEMA + '.' + TABLE_NAME), COLUMN_NAME, 'ColumnID'),
		@field = ORDINAL_POSITION
		from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = @TableName and ORDINAL_POSITION > @field 
		AND DATA_TYPE NOT IN('image','text','ntext') ORDER BY ORDINAL_POSITION

		select @bit = (@Valid - 1 )% 8 + 1
		select @bit = power(2,@bit - 1)
		select @char = ((@Valid - 1) / 8) + 1

		IF substring(COLUMNS_UPDATED(),@char, 1) & @bit > 0 or @Type in ('I','D')
		BEGIN

		PRINT @field
		PRINT @fieldname

			IF LEN(@fieldname)>0
			BEGIN
				--SET @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + @fieldname
				
				select @sql = 'insert Audit (Type, TableName, PK, FieldName, OldValue, NewValue, UpdateDate,  numRecordID, numDomainID, AuditGUID)'
				select @sql = @sql + ' select ''' + @Type + ''''
				select @sql = @sql + ',''' + @TableName + ''''
				select @sql = @sql + ',' + @PKSelect
				select @sql = @sql + ',''' + @fieldname + ''''
				select @sql = @sql + ',convert(varchar(1000),d.' + @fieldname + ')'
				select @sql = @sql + ',convert(varchar(1000),i.' + @fieldname + ')'
				select @sql = @sql + ',''' + CAST(GETUTCDATE() AS VARCHAR(50)) + ''''
				select @sql = @sql + ','+ Cast(@numRecordID AS VARCHAR(18))+ ','+ CAST(@numDomainID AS VARCHAR(18))+ ','''+ CAST(@AuditGUID AS VARCHAR(50))+ ''''
				select @sql = @sql + ' from #ins i full outer join #del d'
				select @sql = @sql + @PKCols
				select @sql = @sql + ' where i.' + @fieldname + ' <> d.' + @fieldname
				select @sql = @sql + ' or (i.' + @fieldname + ' is null and d.' + @fieldname + ' is not null)'
				select @sql = @sql + ' or (i.' + @fieldname + ' is not null and d.' + @fieldname + ' is null)'
				
				PRINT @sql
				exec (@sql)
			END
		END 
	END 
	
	--PRINT @Columns_Updated
	
	SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM Audit WHERE AuditGUID=@AuditGUID AND @Type='U'
	SET @Columns_Updated=ISNULL(@Columns_Updated,'')
	
	DECLARE @tintWFTriggerOn AS TINYINT
	SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END 
	
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 70, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated
	
go