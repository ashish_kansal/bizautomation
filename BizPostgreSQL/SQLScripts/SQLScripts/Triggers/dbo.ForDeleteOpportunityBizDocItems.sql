GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TR'AND NAME ='ForDeleteOpportunityBizDocItems')
DROP trigger ForDeleteOpportunityBizDocItems
GO
Create TRIGGER dbo.ForDeleteOpportunityBizDocItems ON dbo.OpportunityBizDocItems
    FOR DELETE
AS
    DECLARE @numItemCode NUMERIC(9)
    DECLARE @numOppItemID NUMERIC(9)
    DECLARE @numOppBizDocItemID NUMERIC(9)
    DECLARE @numMaxOppBizDocItemID NUMERIC(9)

--Because now we allow Create Sales Order, then Ship, Then Invoice Bug#1390  
--    SELECT  @numMaxOppBizDocItemID = MAX(numOppBizDocItemID)
--    FROM    deleted
--
--    SELECT  TOP 1 @numItemCode = numItemCode,
--            @numOppItemID = numOppItemID,
--            @numOppBizDocItemID = numOppBizDocItemID
--    FROM    deleted
--    ORDER BY numOppBizDocItemID asc
--	PRINT @numMaxOppBizDocItemID
--  WHILE @numOppBizDocItemID <= @numMaxOppBizDocItemID
--  BEGIN
--  	
--  			    PRINT @numOppBizDocItemID
--  			    
--		  /*Update Qty Shipped for inventory item*/
--
--
--			IF ( SELECT COUNT(*)
--				 FROM   Item AS I
--				 WHERE  I.charItemType = 'P'
--						AND I.[numItemCode] = @numItemCode
--			   ) > 0 
--				BEGIN
--		--            PRINT @numWareHouseItemID
--		            	
--					/*Reverting back Shipped Qty to allocation*/
--					DECLARE @numQtyShipped NUMERIC(9)
--		--            DECLARE @numQtyReceived NUMERIC(9)
--					SELECT  @numQtyShipped = ISNULL([numQtyShipped], 0)
--		--					@numQtyReceived = ISNULL([numUnitHourReceived], 0)
--					FROM    OpportunityItems
--					WHERE   [numoppitemtCode] = @numOppItemID
--						
--					IF @numQtyShipped > 0 
--						BEGIN
--							UPDATE  WareHouseItems
--							SET     numAllocation = numAllocation + @numQtyShipped
--							WHERE   numWareHouseItemID IN (
--									SELECT TOP 1
--											[numWarehouseItmsID]
--									FROM    [OpportunityItems]
--									WHERE   [numoppitemtCode] = @numOppItemID )
--		                
--		                
--							UPDATE  [OpportunityItems]
--							SET     [numQtyShipped] = 0
--							WHERE   [numoppitemtCode] = @numOppItemID
--							
--						END
--						/*this case needs to be written , right now no idea how where to move qty*/
--		--             IF @numQtyReceived > 0 
--		--                BEGIN
--		--                
--		--                    UPDATE  WareHouseItems
--		--                    SET     [numOnHand] =  + @numQtyReceived
--		--                    WHERE   numWareHouseItemID IN (
--		--                            SELECT TOP 1
--		--                                    [numWarehouseItmsID]
--		--                            FROM    [OpportunityItems]
--		--                            WHERE   [numoppitemtCode] = @numOppItemID )
--		--                
--		--                
--		--                    UPDATE  [OpportunityItems]
--		--                    SET     [numUnitHourReceived] = 0
--		--                    WHERE   [numoppitemtCode] = @numOppItemID
--							
--		--                END
--		            
--			
--				END
--  	
--  	
--  	
--  	
--  	SELECT  TOP 1 @numItemCode = numItemCode,
--            @numOppItemID = numOppItemID,
--            @numOppBizDocItemID = numOppBizDocItemID
--    FROM    deleted
--    WHERE numOppBizDocItemID >@numOppBizDocItemID
--    ORDER BY numOppBizDocItemID ASC 
--    IF @@ROWCOUNT = 0 
--		SET @numOppBizDocItemID = @numMaxOppBizDocItemID +1
--    
--  END
  

  
