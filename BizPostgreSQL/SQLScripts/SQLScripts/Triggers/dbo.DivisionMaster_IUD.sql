GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[DivisionMaster_IUD]'))
	DROP TRIGGER [dbo].[DivisionMaster_IUD]
GO

CREATE TRIGGER dbo.DivisionMaster_IUD
ON dbo.DivisionMaster
AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @action as char(1) = 'I';

	IF EXISTS(SELECT * FROM DELETED)
    BEGIN
        SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
    END
    ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
	BEGIN
		RETURN; -- Nothing updated or inserted.
    END

	IF @action = 'I'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',numCompanyID,'Insert',INSERTED.numDomainID FROM INSERTED
	END
	ELSE IF @action = 'U'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',numCompanyID,'Update',INSERTED.numDomainID FROM INSERTED

		IF UPDATE (numTerID) 
		BEGIN
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Contact',numContactId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN AdditionalContactsInformation ON INSERTED.numDivisionID=AdditionalContactsInformation.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN OpportunityMaster ON INSERTED.numDivisionID=OpportunityMaster.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'ActionItem',numCommId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN Communication ON INSERTED.numDivisionID=Communication.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN OpportunityMaster ON INSERTED.numDivisionID=OpportunityMaster.numDivisionId INNER JOIN OpportunityBizDocs ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Case',numCaseId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN Cases ON INSERTED.numDivisionID=Cases.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Project',numProId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN ProjectsMaster ON INSERTED.numDivisionID=ProjectsMaster.numDivisionId
		END
	END
	ELSE IF @action = 'D'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',numDivisionID,'Delete',DELETED.numDomainID FROM DELETED
	END
END