GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AfterDeleteOpportunityMaster]'))
	DROP TRIGGER [dbo].[AfterDeleteOpportunityMaster]
GO

CREATE TRIGGER [dbo].[AfterDeleteOpportunityMaster] ON [dbo].[OpportunityMaster] FOR DELETE AS 
BEGIN
	INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Delete',numDomainID FROM DELETED
	
	DECLARE @numOppId NUMERIC(9)
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
  
	SELECT
		@numDomainID = numDomainID
		,@numDivisionID = numDivisionId
		,@numOppId = numOppId 
	FROM 
		deleted

	IF (SELECT COUNT(*) FROM OrganizationRatingRule WHERE numDomainID = @numDomainID) > 0
	BEGIN
		DECLARE @tintPerformance AS NUMERIC(18,0)
		SET @tintPerformance = (SELECT DISTINCT tintPerformance FROM OrganizationRatingRule WHERE numDomainID = @numDomainID)

		EXEC USP_CompanyInfo_UpdateRating @numDomainID,@numDivisionID,@tintPerformance
	END
END