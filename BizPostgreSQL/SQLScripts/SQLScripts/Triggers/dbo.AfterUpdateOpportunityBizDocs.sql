GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[AfterUpdateOpportunityBizDocs]'))
	DROP TRIGGER [dbo].[AfterUpdateOpportunityBizDocs]
GO

CREATE TRIGGER [dbo].[AfterUpdateOpportunityBizDocs]
ON [dbo].[OpportunityBizDocs]
After UPDATE 
AS
  DECLARE  @numOppBizDocsId NUMERIC(9)
  DECLARE  @numOppId NUMERIC(9)
	DECLARE @numBizDocId NUMERIC(9)
  DECLARE @numBizDocStatus NUMERIC(9)
  DECLARE @numDomainID NUMERIC(9)
  DECLARE @vcBizDocID VARCHAR(250)

  INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',OpportunityMaster.numDomainID FROM INSERTED INNER JOIN OpportunityMaster ON INSERTED.numOppId = OpportunityMaster.numOppId

--IF UPDATE(fltDiscount) OR update(bitDiscountType) OR update(monAmountPaid) OR update(bitPartialFulfilment) OR update(monShipCost) OR update(numShipVia) OR update(bitBillingTerms) OR update(intBillingDays) OR update(bitInterestType) OR update(fltInterest)   ---(COLUMNS_UPDATED() & 1332740320) > 0
  IF update(monAmountPaid) OR update(bitPartialFulfilment) OR update(monShipCost) OR update(numShipVia)    ---(COLUMNS_UPDATED() & 1332740320) > 0
    BEGIN
      SELECT @numOppBizDocsId = numOppBizDocsId,
             @numOppId = numOppId
      FROM   deleted
      UPDATE [OpportunityBizDocs]
      SET    monDealAmount = dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId)
      WHERE  [numOppBizDocsId] = @numOppBizDocsId
--      PRINT @numOppBizDocsId
    END

SET NOCOUNT ON;

if UPDATE(numBizDocStatus)
BEGIN
	declare BizActionCursor CURSOR FOR
	select numBizDocId,numBizDocStatus,numOppId,numOppBizDocsId,vcBizDocID from Inserted

OPEN BizActionCursor



FETCH   NEXT FROM BizActionCursor 
into @numBizDocId,@numBizDocStatus,@numOppId,@numOppBizDocsId,@vcBizDocID;
WHILE @@FETCH_STATUS = 0
   BEGIN
	
	select @numDomainID=numDomainID from OpportunityMaster where numOppId=@numOppId;

	insert into Communication

	select 972,numEmployeeId,ACI.numDivisionid,'BizDoc No: ' + @vcBizDocID + ' Status Into: ' + dbo.GetListIemName(@numBizDocStatus)  ,0,0,0,numActionTypeId,numEmployeeId,
		0,0,0,numEmployeeId,getutcdate(),numEmployeeId,getutcdate(),@numDomainID,0,Null,0,getutcdate(),getutcdate(),numEmployeeId,1,0,0,0,0,0,0,numActionTypeId,0,0,null,null,null from BizDocStatusApprove BS 
		inner join AdditionalContactsInformation ACI 
		on ACI.numContactId=BS.numEmployeeID and 
		BS.numBizDocTypeId=@numBizDocId and 
		BS.numBizDocStatusID=@numBizDocStatus and 
		ACI.numDomainID=BS.numDomainID and 
		BS.numDomainID=@numDomainID

	FETCH  NEXT  FROM BizActionCursor 
		into @numBizDocId,@numBizDocStatus,@numOppId,@numOppBizDocsId,@vcBizDocID;
END;

	CLOSE BizActionCursor;
END
    
