GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[ProjectsMaster_IUD]'))
	DROP TRIGGER [dbo].[ProjectsMaster_IUD]
GO

CREATE TRIGGER dbo.ProjectsMaster_IUD
ON [dbo].[ProjectsMaster]
AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @action as char(1) = 'I';

	IF EXISTS(SELECT * FROM DELETED)
    BEGIN
        SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
    END
    ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
	BEGIN
		RETURN; -- Nothing updated or inserted.
    END

	IF @action = 'I'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Project',numProId,'Insert',INSERTED.numDomainID FROM INSERTED
	END
	ELSE IF @action = 'U'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Project',numProId,'Update',INSERTED.numDomainID FROM INSERTED
	END
	ELSE IF @action = 'D'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Project',numProId,'Delete',DELETED.numDomainID FROM DELETED
	END
END