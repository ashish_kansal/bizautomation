GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[CompanyInfo_IUD]'))
	DROP TRIGGER [dbo].[CompanyInfo_IUD]
GO

CREATE TRIGGER dbo.CompanyInfo_IUD
ON dbo.CompanyInfo
AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

    DECLARE @action as char(1) = 'I';

	IF EXISTS(SELECT * FROM DELETED)
    BEGIN
        SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
    END
    ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
	BEGIN
		RETURN; -- Nothing updated or inserted.
    END

	IF @action = 'I'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',INSERTED.numCompanyID,'Insert',INSERTED.numDomainID FROM INSERTED 
	END
	ELSE IF @action = 'U'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',INSERTED.numCompanyID,'Update',INSERTED.numDomainID FROM INSERTED

		IF UPDATE(vcCompanyName)
		BEGIN
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Contact',numContactId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN AdditionalContactsInformation ON DivisionMaster.numDivisionID=AdditionalContactsInformation.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN OpportunityMaster ON DivisionMaster.numDivisionID=OpportunityMaster.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'ActionItem',numCommId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN Communication ON DivisionMaster.numDivisionID=Communication.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN OpportunityMaster ON DivisionMaster.numDivisionID=OpportunityMaster.numDivisionId INNER JOIN OpportunityBizDocs ON OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Case',numCaseId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN Cases ON DivisionMaster.numDivisionID=Cases.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Project',numProId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN ProjectsMaster ON DivisionMaster.numDivisionID=ProjectsMaster.numDivisionId
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Return',numReturnHeaderID,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN DivisionMaster ON INSERTED.numCompanyId = DivisionMaster.numCompanyID INNER JOIN ReturnHeader ON DivisionMaster.numDivisionID=ReturnHeader.numDivisionId
		END
	END
	ELSE IF @action = 'D'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Organization',numDivisionID,'Delete',DELETED.numDomainID FROM DELETED INNER JOIN DivisionMaster ON DELETED.numCompanyId = DivisionMaster.numCompanyID
	END
END