/****** Object:  Trigger [dbo].[ES_Item_All_DML]    Script Date: 10/04/2020 10:04:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[ES_Item_All_DML]
ON [dbo].[Item]
AFTER INSERT, UPDATE, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

	DECLARE @bitElasticSearch BIT

	SET @bitElasticSearch = ISNULL((SELECT TOP 1 ECD.bitElasticSearch FROM 
	(SELECT * FROM INSERTED UNION SELECT * FROM DELETED) T
	INNER JOIN eCommerceDTL ECD ON (ECD.numDomainId = T.numDomainID AND ECD.bitElasticSearch = 1)),0)

	IF(@bitElasticSearch = 1)
	BEGIN
		DECLARE @action as char(1) = 'I';

		IF EXISTS(SELECT * FROM DELETED)
		BEGIN
			SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
		END
		ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
		BEGIN
			RETURN; -- Nothing updated or inserted.
		END

		IF @action = 'I'
		BEGIN
			--INSERT INTO ElasticSearchBizCart(vcModule,numRecordID, vcAction, numDomainID)--, numSiteID) 
			--SELECT 'Item',i.numItemCode,'Insert',i.numDomainID--,S.numSiteID 
			--FROM Sites S 
			--INNER JOIN CategoryProfileSites CPS ON S.numSiteID=CPS.numSiteID
			--INNER JOIN CategoryProfile CP ON CP.ID = CPS.numCategoryProfileID
			--INNER JOIN eCommerceDTL ECD ON ECD.numSiteId = s.numSiteID and ECD.bitElasticSearch = 1
			--INNER JOIN INSERTED i ON i.numDomainID = s.numDomainID

			INSERT INTO ElasticSearchBizCart(vcModule,vcValue, vcAction, numDomainID) 
			SELECT 'Item',i.numItemCode,'Insert',i.numDomainID 
			FROM INSERTED i
		END
		ELSE 
		BEGIN
			--SELECT ES.numElasticSearchBizCartID,ES.numSiteID 
			--INTO #TempInsertRecordIds 
			--FROM ElasticSearchBizCart ES 
			--INNER JOIN deleted i ON (i.numItemCode = ES.numRecordID AND i.numDomainID = ES.numDomainID) 
			--WHERE vcModule = 'Item'

			--UPDATE ElasticSearchBizCart SET vcAction = IIF(@action = 'U','Update','Delete') 
			--WHERE numElasticSearchBizCartID IN (SELECT numElasticSearchBizCartID FROM #TempInsertRecordIds)

			--INSERT INTO ElasticSearchBizCart(vcModule,numRecordID, vcAction, numDomainID, numSiteID) 
			--SELECT 'Item',i.numItemCode,IIF(@action = 'U','Update','Delete'),i.numDomainID,S.numSiteID FROM Sites S 
			--INNER JOIN CategoryProfileSites CPS ON S.numSiteID=CPS.numSiteID
			--INNER JOIN CategoryProfile CP ON CP.ID = CPS.numCategoryProfileID
			--INNER JOIN eCommerceDTL ECD ON ECD.numSiteId = s.numSiteID and ECD.bitElasticSearch = 1
			--INNER JOIN DELETED i ON i.numDomainID = s.numDomainID
			--WHERE S.numSiteID NOT IN (SELECT numSiteId FROM #TempInsertRecordIds)

			SELECT ES.numElasticSearchBizCartID,ES.vcValue
			INTO #TempInsertRecordIds 
			FROM ElasticSearchBizCart ES 
			INNER JOIN deleted i ON (i.numItemCode = ES.vcValue AND i.numDomainID = ES.numDomainID) 
			WHERE vcModule = 'Item'

			UPDATE ElasticSearchBizCart 
			SET vcAction = IIF(@action = 'U','Update','Delete') 
			WHERE numElasticSearchBizCartID IN (SELECT numElasticSearchBizCartID FROM #TempInsertRecordIds)

			INSERT INTO ElasticSearchBizCart(vcModule,vcValue, vcAction, numDomainID) 
			SELECT 'Item',i.numItemCode,IIF(@action = 'U','Update','Delete'),i.numDomainID
			FROM DELETED i
			WHERE i.numItemCode NOT IN (SELECT vcValue FROM #TempInsertRecordIds)
		END

	END
END
GO

ALTER TABLE [dbo].[Item] ENABLE TRIGGER [ES_Item_All_DML]
GO


