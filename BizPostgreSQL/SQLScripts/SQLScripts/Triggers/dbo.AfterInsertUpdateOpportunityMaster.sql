GO
Alter TRIGGER [dbo].[AfterInsertUpdateOpportunityMaster]
ON [dbo].[OpportunityMaster]
AFTER INSERT,UPDATE
AS
BEGIN
	DECLARE @action as char(1) = 'I';

	IF EXISTS(SELECT * FROM DELETED)
    BEGIN
        SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
    END
    ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
	BEGIN
		SET @action = ''
    END


	DECLARE @numOppId NUMERIC(9)
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @numCreatedBy NUMERIC(18,0)
	DECLARE @numModifiedBy NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numStatus NUMERIC(18,0)
  
	SELECT
		@numDomainID = numDomainID
		,@numDivisionID = numDivisionId
		,@numOppId = numOppId 
		,@numCreatedBy = numCreatedBy
		,@numModifiedBy = numModifiedBy
		,@numStatus = numStatus
	FROM 
		inserted

	IF @action = 'I'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Insert',numDomainID FROM INSERTED
		EXEC USP_EDIQueue_Insert @numDomainID,@numCreatedBy,@numOppId,@numStatus
	END
	ELSE IF @action = 'U'
	BEGIN
		INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'Opp/Order',numOppId,'Update',numDomainID FROM INSERTED

		IF UPDATE(numAssignedTo) OR UPDATE(numRecOwner) OR UPDATE(tintOppType) OR UPDATE(tintOppStatus)
		BEGIN
			INSERT INTO ElasticSearchModifiedRecords (vcModule,numRecordID, vcAction, numDomainID) SELECT 'BizDoc',numOppBizDocsId,'Update',INSERTED.numDomainID FROM INSERTED INNER JOIN OpportunityBizDocs ON INSERTED.numOppId = OpportunityBizDocs.numOppId
		END

		IF UPDATE(numStatus)
		BEGIN
			EXEC USP_EDIQueue_Insert @numDomainID,@numModifiedBy,@numOppId,@numStatus
		END
	END
  
	UPDATE 
		[OpportunityMaster]
	SET 
		monDealAmount = dbo.[GetDealAmount](@numOppId,GETUTCDATE(),0)
	WHERE 
		[numOppId] = @numOppId

	IF (SELECT COUNT(*) FROM OrganizationRatingRule WHERE numDomainID = @numDomainID) > 0
	BEGIN
		DECLARE @tintPerformance AS NUMERIC(18,0)
		SET @tintPerformance = (SELECT DISTINCT tintPerformance FROM OrganizationRatingRule WHERE numDomainID = @numDomainID)

		EXEC USP_CompanyInfo_UpdateRating @numDomainID,@numDivisionID,@tintPerformance
	END
END  