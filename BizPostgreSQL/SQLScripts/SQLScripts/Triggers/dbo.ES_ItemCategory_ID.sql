GO
IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[ES_ItemCategory_ID]'))
	DROP TRIGGER [dbo].[ES_ItemCategory_ID]
GO
CREATE TRIGGER [dbo].[ES_ItemCategory_ID]
ON [dbo].[ItemCategory]
AFTER INSERT, DELETE
AS 
BEGIN
    SET NOCOUNT ON;

	DECLARE @bitElasticSearch BIT

	SET @bitElasticSearch = ISNULL((SELECT TOP 1 ECD.bitElasticSearch FROM 
	(SELECT * FROM INSERTED UNION SELECT * FROM DELETED) T
	INNER JOIN Item i ON (i.numItemCode = T.numItemID)
	INNER JOIN eCommerceDTL ECD ON (ECD.numDomainId = i.numDomainID AND ECD.bitElasticSearch = 1)),0)

	IF(@bitElasticSearch = 1)
	BEGIN
		DECLARE @action as char(1) = 'I';

		IF EXISTS(SELECT * FROM DELETED)
		BEGIN
			SET @action = CASE WHEN EXISTS(SELECT * FROM INSERTED) THEN 'U' ELSE 'D' END
		END
		ELSE IF NOT EXISTS(SELECT * FROM INSERTED) 
		BEGIN
			RETURN; -- Nothing updated or inserted.
		END

		IF @action = 'I'
		BEGIN
			INSERT INTO ElasticSearchBizCart(vcModule,vcValue, vcAction, numDomainID)--, numSiteID) 
			SELECT 'Item',Item.numItemCode,'Insert',Item.numDomainID--,S.numSiteID 
			FROM Item  
			INNER JOIN INSERTED i ON (i.numItemID = Item.numItemCode)
		END
		ELSE 
		BEGIN
			IF(NOT EXISTS(SELECT 1 FROM ElasticSearchBizCart WHERE vcModule = 'Item' AND vcValue IN (SELECT numItemID FROM deleted)))
			BEGIN	
				INSERT INTO ElasticSearchBizCart(vcModule,vcValue, vcAction, numDomainID) 
				SELECT 'Item',Item.numItemCode,'Update',Item.numDomainID
				FROM Item  
				INNER JOIN DELETED i ON (i.numItemID = Item.numItemCode)

			END
		END
	END
END
GO

ALTER TABLE [dbo].[ItemCategory] ENABLE TRIGGER [ES_ItemCategory_ID]
GO


