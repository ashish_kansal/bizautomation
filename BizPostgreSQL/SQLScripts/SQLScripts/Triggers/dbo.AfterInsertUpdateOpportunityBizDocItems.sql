/****** Object:  Trigger [dbo].[AfterInsertUpdateOpportunityBizDocItems]    Script Date: 09/01/2009 00:58:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- UPDATE [OpportunityBizDocs] SET monAmountPaid=2 WHERE [numOppBizDocsId] =2132
-- SELECT * FROM [OpportunityBizDocs] where monDealAmount is not null

--SELECT dbo.[GetDealAmount](1694,GETUTCDATE(),2144)
ALTER TRIGGER [AfterInsertUpdateOpportunityBizDocItems] ON [dbo].[OpportunityBizDocItems]
    AFTER INSERT, UPDATE
AS
    DECLARE @numOppBizDocsId NUMERIC(9)
    DECLARE @numOppId NUMERIC(9)
    DECLARE @numOppItemID NUMERIC(9)
    DECLARE @QtyShipped AS NUMERIC(9) 
  
    SELECT  @numOppBizDocsId = numOppBizDocId,
            @numOppItemID = numOppItemID,
            @QtyShipped = numUnitHour
    FROM    inserted
    SELECT  @numOppId = numOppId
    FROM    [OpportunityBizDocs]
    WHERE   numOppBizDocsId = @numOppBizDocsId
  
    UPDATE  [OpportunityBizDocs]
    SET     monDealAmount = dbo.[GetDealAmount](@numOppId, GETUTCDATE(),
                                                @numOppBizDocsId)
    WHERE   [numOppBizDocsId] = @numOppBizDocsId
 
  
  /*Update Qty Shipped for inventory item*/
  /*Commented Reason: because it needs to be updated manually from sales fulfillment page*/
--
--    IF ( SELECT I.charItemType
--         FROM   OpportunityItems AS OI
--                LEFT OUTER JOIN Item AS I ON OI.numItemCode = I.numItemCode
--         WHERE  numoppitemtCode = @numOppItemID
--       ) = 'P' 
--        BEGIN
--            UPDATE  [OpportunityItems]
--            SET     [numQtyShipped] = @QtyShipped
--            WHERE   [numoppitemtCode] = @numOppItemID	
--        END


  
  

