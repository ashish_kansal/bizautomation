/****** Object:  UserDefinedFunction [dbo].[fn_GetBudgetComments]    Script Date: 07/26/2008 18:12:28 ******/

GO

GO
--Created By Siva 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getbudgetcomments')
DROP FUNCTION fn_getbudgetcomments
GO
CREATE FUNCTION [dbo].[fn_GetBudgetComments]
(@numBudgetId numeric(9),@numChartAcntId numeric(9)=0)
Returns varchar(50)
As
Begin
Declare @vcComments as varchar(50)      
    
    Select @vcComments=isnull(vcComments,'') From OperationBudgetDetails Where numBudgetId=@numBudgetId And numChartAcntId=@numChartAcntId
----     
   
      
 Return @vcComments  
End
GO
