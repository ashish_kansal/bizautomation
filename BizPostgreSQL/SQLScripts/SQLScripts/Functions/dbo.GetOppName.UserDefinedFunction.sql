/****** Object:  UserDefinedFunction [dbo].[GetOppName]    Script Date: 07/26/2008 18:13:06 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getoppname')
DROP FUNCTION getoppname
GO
CREATE FUNCTION [dbo].[GetOppName](@numOppid numeric(9))
	RETURNS varchar (100) 
AS
BEGIN
declare @OppName as varchar (100)
select @OppName=vcPOppName from OpportunityMaster where numOppId=@numOppid
	RETURN @OppName
END
GO
