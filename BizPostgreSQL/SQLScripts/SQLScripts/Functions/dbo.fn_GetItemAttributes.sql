GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemAttributes')
DROP FUNCTION fn_GetItemAttributes
GO
CREATE FUNCTION [dbo].[fn_GetItemAttributes] 
(
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@bitEcommerce BIT
)
RETURNS VARCHAR(500)
AS
BEGIN
	DECLARE @strAttribute varchar(500) = ''
	DECLARE @numCusFldID NUMERIC(18,0) = 0
	DECLARE @numCusFldValue NUMERIC(18,0) = 0

	SELECT TOP 1
		@numCusFldID=fld_id
		,@numCusFldValue=fld_value 
	FROM 
		ItemAttributes 
	WHERE 
		numDomainID = @numDomainID
		AND numItemCode=@numItemCode 
		AND fld_value != 0
	ORDER BY 
		fld_id

	while @numCusFldID>0
	begin
		IF ISNUMERIC(@numCusFldValue)=1
		begin
			SELECT 
				@strAttribute = CONCAT(@strAttribute,(CASE WHEN LEN(@strAttribute) > 0 THEN (CASE WHEN ISNULL(@bitEcommerce,0) = 1 THEN '&nbsp;&nbsp;&nbsp;&nbsp;' ELSE ',' END) ELSE '' END),(CASE WHEN ISNULL(@bitEcommerce,0) = 1 THEN CONCAT('<b><i>(',Fld_label,')</i></b>&nbsp;') ELSE Fld_label END)) 
			FROM 
				CFW_Fld_Master 
			WHERE 
				Fld_id=@numCusFldID 
				AND Grp_id = 9
                
			IF LEN(@strAttribute) > 0
			BEGIN
				IF ISNULL(@bitEcommerce,0) = 1
				BEGIN
					SELECT 
						@strAttribute=CONCAT(@strAttribute,vcData) 
					FROM 
						ListDetails 
					WHERE 
						numListItemID=@numCusFldValue
				END
				ELSE
				BEGIN
					SELECT @strAttribute=@strAttribute+':'+ vcData FROM ListDetails WHERE numListItemID=@numCusFldValue
				END
			END
		END
		ELSE
		BEGIN
			SELECT 
				@strAttribute = CONCAT(@strAttribute,(CASE WHEN LEN(@strAttribute) > 0 THEN (CASE WHEN ISNULL(@bitEcommerce,0) = 1 THEN '&nbsp;&nbsp;&nbsp;&nbsp;' ELSE ',' END) ELSE '' END),(CASE WHEN ISNULL(@bitEcommerce,0) = 1 THEN CONCAT('<b><i>(',Fld_label,')</i></b>&nbsp;') ELSE Fld_label END)) 
			FROM 
				CFW_Fld_Master 
			WHERE 
				Fld_id=@numCusFldID 
				AND Grp_id = 9

			IF LEN(@strAttribute) > 0
			BEGIN
				IF ISNULL(@bitEcommerce,0) = 1
				BEGIN
					SELECT 
						@strAttribute=CONCAT(@strAttribute,'-') 
					FROM 
						ListDetails 
					WHERE 
						numListItemID=@numCusFldValue
				END
				ELSE
				BEGIN
					SELECT @strAttribute=@strAttribute+':-'
				END
			END
		end


		SELECT TOP 1
			@numCusFldID=fld_id
			,@numCusFldValue=fld_value 
		FROM 
			ItemAttributes 
		WHERE 
			numDomainID = @numDomainID
			AND numItemCode=@numItemCode 
			AND fld_value != 0
			AND fld_id > @numCusFldID
		ORDER BY 
			fld_id

		IF @@rowcount=0 SET @numCusFldID=0
	END

	RETURN @strAttribute
END
GO
