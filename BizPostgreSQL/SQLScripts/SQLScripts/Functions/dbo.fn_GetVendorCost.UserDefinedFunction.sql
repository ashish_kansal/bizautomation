GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getvendorcost')
DROP FUNCTION fn_getvendorcost
GO
CREATE FUNCTION fn_GetVendorCost
(@numItemCode numeric(18))
returns DECIMAL(20,5)
as
begin
declare @Cost DECIMAL(20,5)
set @Cost=0

select @Cost=isnull(monCost,0) from Vendor V inner join Item I on v.numVendorID=I.numVendorID and 
V.numItemCode=I.numItemCode 
where v.numItemCode=@numItemCode

return @Cost;

end