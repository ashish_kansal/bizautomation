--SELECT  dbo.GetAccountedEmbeddedCost(11960)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAccountedEmbeddedCost')
DROP FUNCTION GetAccountedEmbeddedCost
GO
CREATE FUNCTION dbo.GetAccountedEmbeddedCost
   (
      @numOppBizDocID AS NUMERIC,
      @tintMode AS tinyint
    )
RETURNS DECIMAL(20,5)
BEGIN
	DECLARE @numJournal_Id NUMERIC
	DECLARE @monCost DECIMAL(20,5)
	
	IF @tintMode = 1
	BEGIN
		--SELECT @monCost = ISNULL(SUM([monCost]),0) FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocID
		SELECT @monCost = ISNULL(SUM([monEmbeddedCost]),0) FROM [OpportunityBizDocItems] WHERE [numOppBizDocID]=@numOppBizDocID AND [bitEmbeddedCost]=1
	END
	ELSE IF @tintMode =2
	BEGIN
		SELECT @numJournal_Id = [numJournal_Id] FROM [General_Journal_Header] WHERE [numOppBizDocsId] = @numOppBizDocID
		SELECT @monCost = ISNULL(SUM([numCreditAmt]),0) FROM [General_Journal_Details] WHERE [numJournalId] = @numJournal_Id AND [chBizDocItems]='EC'
	END
	
	
	RETURN @monCost
END 
