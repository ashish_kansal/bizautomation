GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'fn'
                    AND NAME = 'GetNthRecurringDate' ) 
    DROP FUNCTION GetNthRecurringDate
GO
/*
Returns Nth occurance date for given recurring template
@startdate = date from which recurring is going to start 
@intRecurrenceNumber = no of recurrance from start date
*/
-- SELECT [dbo].[GetNthRecurringDate](GETDATE(),2,10)
CREATE FUNCTION [dbo].[GetNthRecurringDate] ( @StartDate DATETIME,
                                              @intRecurrenceNumber INTEGER,
                                              @numRecurringID NUMERIC )
RETURNS DATETIME
AS 
BEGIN
    DECLARE @dtRetValue DATETIME 
 	
    DECLARE @chrIntervalType AS CHAR(1)                              
    DECLARE @tintIntervalDays AS TINYINT                              
    DECLARE @tintMonthlyType AS TINYINT                              
    DECLARE @tintFirstDet AS TINYINT                              
    DECLARE @tintWeekDays AS TINYINT                              
    DECLARE @tintMonths AS TINYINT                              
    DECLARE @dtStartDate AS DATETIME                              
    DECLARE @dtEndDate AS DATETIME          
    DECLARE @numNoTransactionsRecurring AS NUMERIC(9)                              
    DECLARE @bitEndType AS BIT             
    DECLARE @bitEndTransactionType AS BIT     
 	Declare @Day as tinyint                          
	Declare @Month as  tinyint
	Declare @Year as integer


    SELECT  @chrIntervalType = chrIntervalType,
            @tintIntervalDays = tintIntervalDays,
            @tintMonthlyType = tintMonthlyType,
            @tintFirstDet = tintFirstDet,
            @tintWeekDays = tintWeekDays,
            @tintMonths = tintMonths,
            @dtStartDate = dtStartDate,
            @dtEndDate = dtEndDate,
            @bitEndType = bitEndType,
            @bitEndTransactionType = bitEndTransactionType
    FROM    dbo.RecurringTemplate
    WHERE   numRecurringId = @numRecurringID
 	
 	IF @chrIntervalType ='D'
 	BEGIN
	 	SET @dtRetValue = DATEADD(DAY,@tintIntervalDays*@intRecurrenceNumber,@StartDate)
	 	
	END
 	IF @chrIntervalType ='M'
 	BEGIN
 		    SET @dtRetValue =  DATEADD(month,@tintIntervalDays*@intRecurrenceNumber,@StartDate)
 			Set @Day = DATEPART(Day, @dtRetValue)
			Set @Month = DATEPART(Month, @dtRetValue)
			Set @Year= DATEPART(Year, @dtRetValue)
			
 		IF @tintWeekDays>0 
 		BEGIN
 		--Every Nth Weekday of month
--			SET @dtRetValue = Dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)
----			IF @tintFirstDet = 5  
----			SET @tintFirstDet = -1 -- a positive number means from the beginning of month and a negative number means from the end of month
			
			SET @dtRetValue =dbo.fn_GetNthWeekdayOfMonth(@dtRetValue,@tintWeekDays,CASE @tintFirstDet WHEN 5 THEN -1 ELSE @tintFirstDet END )
			
--			SET @dtRetValue =[dbo].[fn_RecurringTemplate](@tintFirstDet,@tintWeekDays,@dtRetValue);
		END
		ELSE
		BEGIN
			--Every Nth Day of Month
	 		
			Set @Day = DATEPART(Day, @dtRetValue)
			Set @Month = DATEPART(Month, @dtRetValue)
			Set @Year= DATEPART(Year, @dtRetValue)
	        
			SET @dtRetValue = Dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)
		END
 	END 
 	
 	IF @chrIntervalType ='Y'
 	BEGIN
 		SET @dtStartDate = DATEADD(YEAR,@intRecurrenceNumber,@dtStartDate)
 		Set @Year= DATEPART(Year, @dtStartDate)
        Set @dtRetValue=dateadd(month,((@Year-1900)*12)+@tintMonths-1,@tintFirstDet-1)
	END
 	
 	
    RETURN @dtRetValue
END