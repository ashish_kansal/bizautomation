/****** Object:  UserDefinedFunction [dbo].[GetProjectStage]    Script Date: 07/26/2008 18:13:09 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getprojectstage')
DROP FUNCTION getprojectstage
GO
CREATE FUNCTION [dbo].[GetProjectStage](@numProid as numeric(9),@numUsercntId as numeric(9),@numDomainId as numeric(9))
returns varchar(4000)
as 
begin
	declare @stage as varchar(4000)
	set @stage = ''
	select @stage=
	case when @stage='' then vcStageDetail else 
 @stage+'<br>'+vcStageDetail
 end
	from ProjectsStageDetails PSd
	join  ProjectsMaster pro    on pro.numProid=PSd.numProid 
	where PSd.numProid = @numProid and  bitStageCompleted=0 
	and PSd.numAssignTo=@numUsercntId and pro.numDomainId =@numDomainId
return @stage
end
GO
