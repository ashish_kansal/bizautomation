/****** Object:  UserDefinedFunction [dbo].[GetItemIncomeAccount]    Script Date: 07/26/2008 18:13:04 ******/

GO

GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getitemincomeaccount')
DROP FUNCTION getitemincomeaccount
GO
CREATE FUNCTION [dbo].[GetItemIncomeAccount](@numItemCode numeric(9))  
 RETURNS numeric(9) 
AS  
BEGIN  
declare @numItemIncomeAccount as  numeric(9)
  
select @numItemIncomeAccount=isnull(numIncomeChartAcntId,'0') from Item where numItemCode=@numItemCode     
  
  
return isnull(@numItemIncomeAccount,'0')  
END
GO
