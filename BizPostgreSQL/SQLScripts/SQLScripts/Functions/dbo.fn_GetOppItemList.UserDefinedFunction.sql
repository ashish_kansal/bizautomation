/****** Object:  UserDefinedFunction [dbo].[fn_GetOppItemList]    Script Date: 07/26/2008 18:12:41 ******/

GO

GO
--Created By: Debasish Nag      
--Created On: 24th June 2006
--This function returns the Items associated with an oppotunity
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getoppitemlist')
DROP FUNCTION fn_getoppitemlist
GO
CREATE FUNCTION [dbo].[fn_GetOppItemList](@numOppID numeric)  
RETURNS Nvarchar(400)   
AS  
BEGIN  
 DECLARE @vcItemIds as NVarchar(400)  
 SELECT @vcItemIds = COALESCE(@vcItemIds + ',', '') + Cast(numItemCode as Nvarchar)
 FROM OpportunityItems WHERE numOppId = @numOppID

 RETURN @vcItemIds 
END
GO
