/****** Object:  UserDefinedFunction [dbo].[GetOppAmount]    Script Date: 07/26/2008 18:13:05 ******/

GO

GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getoppamount')
DROP FUNCTION getoppamount
GO
CREATE FUNCTION [dbo].[GetOppAmount](@numUserCntID numeric(9),@numDomainID numeric(9),@dtDateFrom datetime,@dtDateTo datetime)  
 RETURNS decimal   
AS  
BEGIN  
declare @OppAmount as decimal  
  
select @OppAmount=sum(OM.monPAmount)  from OpportunityMaster OM 
where OM.tintOpptype=1 and tintOppStatus=1 and OM.numdomainid=@numDomainID 
and  OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo  and OM.numRecOwner=@numUserCntID
  
 RETURN @OppAmount  
END
GO
