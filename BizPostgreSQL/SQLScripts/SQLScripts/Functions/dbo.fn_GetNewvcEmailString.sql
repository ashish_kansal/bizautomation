
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetNewvcEmailString]    Script Date: 11/02/2011 12:21:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------Function For Returning new string ----------------------
------Created By : Pinkal Patel Date : 06-Oct-2011
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetNewvcEmailString')
DROP FUNCTION fn_GetNewvcEmailString
GO
CREATE FUNCTION [dbo].[fn_GetNewvcEmailString](
@vcEmailId VARCHAR(2000),
@numDomianID NUMERIC(18,0) = NULL
) RETURNS VARCHAR(2000)
As
BEGIN
DECLARE @vcNewEmailId AS VARCHAR(2000)
DECLARE @Email AS VARCHAR(200)
DECLARE @EmailName AS VARCHAR(200)
DECLARE @EmailAdd AS VARCHAR(200)
DECLARE @StartPos AS INTEGER
DECLARE @EndPos AS INTEGER
DECLARE @numEmailid AS NUMERIC(9)
---DECLARE @numDomainId AS NUMERIC(18, 0)

SET @vcNewEmailId =''
	IF @vcEmailId <> '' 
    BEGIN
        WHILE CHARINDEX('#^#', @vcEmailId) > 0	
            BEGIN
                SET @Email = LTRIM(RTRIM(SUBSTRING(@vcEmailId, 1,
                                                   CHARINDEX('#^#', @vcEmailId,
                                                             0)+2)))
                IF CHARINDEX('$^$', @Email) > 0 
                    BEGIN
                        SET @EmailName = LTRIM(RTRIM(SUBSTRING(@Email, 0, CHARINDEX('$^$', @Email))))
                        SET @EmailAdd = LTRIM(RTRIM(SUBSTRING(@Email, LEN(@EmailName) + 1, CHARINDEX('$^$', @Email) + LEN(@Email) - 1)))
                        SET @EmailAdd = REPLACE(@Emailadd, '$^$', '')
                        SET @EmailAdd = REPLACE(@Emailadd, '#^#', '')                
                    END
                   SET @vcNewEmailId =@vcNewEmailId + (SELECT TOP  1 CAST(ISNULL(numEmailid,0) AS VARCHAR) FROM dbo.EmailMaster WHERE dbo.EmailMaster.vcEmailID=@EmailAdd AND numDomainId=@numDomianID) + '$^$' + @EmailName + '$^$' + @EmailAdd   + '#^#'
				--PRINT @Email
				--SET @vcNewEmailId = @vcNewEmailId + ','
                SET @vcEmailId = LTRIM(RTRIM(SUBSTRING(@vcEmailId,
                                                       CHARINDEX('#^#', @vcEmailId)
                                                       + 3, LEN(@vcEmailId))))
	
		       END
            
			END
			IF LEN(@vcNewEmailId) > 0
			BEGIN
				 SET @vcNewEmailId = LEFT(@vcNewEmailId,LEN(@vcNewEmailId)-3)
			END                
                 RETURN @vcNewEmailId
                 
	END