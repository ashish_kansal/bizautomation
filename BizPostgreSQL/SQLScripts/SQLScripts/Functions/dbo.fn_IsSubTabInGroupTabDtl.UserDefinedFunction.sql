
GO
/****** Object:  UserDefinedFunction [dbo].[fn_IsSubTabInGroupTabDtl]    Script Date: 08/04/2009 23:42:22 ******/

GO

GO
-- =============================================
-- Author:		Mohan Joshi
-- Create date: 
-- Description:	
-- =============================================
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_issubtabingrouptabdtl')
DROP FUNCTION fn_issubtabingrouptabdtl
GO
CREATE FUNCTION [dbo].[fn_IsSubTabInGroupTabDtl] 
(
	-- Add the parameters for the function here
	@numModuleId numeric(9),
	@numTabId numeric(9)
)
RETURNS bit
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result bit

	-- Add the T-SQL statements to compute the return value here
--    set @Result=0
--	if ( not exists(select numTabId from GroupTabDetails where numModuleId=@numModuleId)) or exists(select numTabId from GroupTabDetails where numModuleId=@numModuleId and numTabId=@numTabId) 
--     begin
--      set @Result=1
--     end
    -- Return the result of the function
	RETURN @Result

END
