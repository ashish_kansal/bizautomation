/****** Object:  UserDefinedFunction [dbo].[fn_GetManagerEmail]    Script Date: 07/26/2008 18:12:39 ******/

GO

GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getmanageremail')
DROP FUNCTION fn_getmanageremail
GO
CREATE FUNCTION [dbo].[fn_GetManagerEmail](@byteMode as tinyint,
@numUserID as numeric(9)=0,
@numContactID as numeric(9)=0)
returns varchar(100)
as
begin

declare @vcEmail as varchar(100)
set @vcEmail=''
if @byteMode=0 --- If Userid Is Available
begin
	select @vcEmail=isnull(vcEmail,'') from AdditionalContactsInformation where
	numContactID=(select numManagerID from AdditionalContactsInformation Addc
	join UserMaster U
	on U.numUserDetailId=Addc.numContactId
	where numUserID=@numUserID)
end
if @byteMode=1  --- If Conatct ID is Available
begin
	 select @vcEmail=isnull(vcEmail,'') from AdditionalContactsInformation where
	numContactID=(select numManagerID from AdditionalContactsInformation where numContactID=@numContactID)

end

return @vcEmail

end
GO
