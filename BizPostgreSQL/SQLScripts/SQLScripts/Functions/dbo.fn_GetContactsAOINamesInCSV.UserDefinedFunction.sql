/****** Object:  UserDefinedFunction [dbo].[fn_GetContactsAOINamesInCSV]    Script Date: 07/26/2008 18:12:32 ******/

GO
SET QUOTED_IDENTIFIER OFF
GO
--Created By: Debasish Nag    
--Created On: 1th Sept 2005    
--This function returns the comma separated list of AOI Names is used in Adv Search
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcontactsaoinamesincsv')
DROP FUNCTION fn_getcontactsaoinamesincsv
GO
CREATE FUNCTION [dbo].[fn_GetContactsAOINamesInCSV](@numContactID numeric)
	RETURNS varchar(250) 
AS
BEGIN
	DECLARE @AOINames varchar(250)
	SELECT 
		@AOINames = COALESCE(@AOINames + ',', '') + AOIM.vcAOIName
	FROM 
		AOIContactLink AOIC
	INNER JOIN
		AOIMaster AOIM
	ON
		AOIC.numCOntactID=@numContactID
		AND AOIC.numAOIID = AOIM.numAOIId
	WHERE 
		AOIC.tintSelected = 1
	RETURN @AOINames
END
GO
