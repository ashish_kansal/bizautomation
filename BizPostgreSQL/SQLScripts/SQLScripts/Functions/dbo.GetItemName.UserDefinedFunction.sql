/****** Object:  UserDefinedFunction [dbo].[GetItemName]    Script Date: 07/26/2008 18:13:04 ******/

GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getitemname')
DROP FUNCTION getitemname
GO
CREATE FUNCTION [dbo].[GetItemName](@numItemCode numeric(9))
	RETURNS varchar (100) 
AS
BEGIN
declare @vcItemName as varchar(100)

select @vcItemName=isnull(vcItemName,'-') from Item where numItemCode=@numItemCode   


return isnull(@vcItemName,'-')
END
GO
