/****** Object:  UserDefinedFunction [dbo].[fn_GetAcntTypeDescription]    Script Date: 07/26/2008 18:12:27 ******/

GO

GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getacnttypedescription')
DROP FUNCTION fn_getacnttypedescription
GO
CREATE FUNCTION [dbo].[fn_GetAcntTypeDescription](@numId varchar(100),@CharType as char(3))    
returns Varchar(5000)    
Begin    
Declare @numJournalId as numeric(9)    
Declare @numTransactionId as numeric(9)    
Declare @varAcntType as varchar(5000)    
Declare @AcntTypeId as varchar(40)    
Declare @AcntTypeDescription as varchar(500)    
Declare @lenAcntType as int    
Set @varAcntType=''    
Set @AcntTypeDescription=''    
if @CharType='C'    
 Select @numJournalId=numJournal_Id from General_Journal_Header Where numCheckId=@numId    
Else if @CharType='CC'    
 Select @numJournalId=numJournal_Id from General_Journal_Header Where numCashCreditCardId=@numId    
Else if  @CharType='JD'    
 Select @numJournalId=numJournal_Id from General_Journal_Header Where numJournal_Id=@numId    
Else if @CharType='DD'  
  Select @numJournalId=numJournal_Id from General_Journal_Header Where numDepositId=@numId    
Else if @CharType='Opp' 
  Select @numJournalId=numJournal_Id from General_Journal_Header Where numOppId=@numId  
Select @numTransactionId=min(numTransactionId) From General_Journal_Details Where numJournalId=@numJournalId     
While @numTransactionId>0    
Begin    
   Select @AcntTypeId=numChartAcntId From General_Journal_Details Where numTransactionId=@numTransactionId    
   Select @AcntTypeDescription=[vcAccountName] From Chart_of_Accounts Where numAccountId=@AcntTypeId    
   Set @varAcntType=@varAcntType + @AcntTypeDescription + ','    
   Select @numTransactionId=min(numTransactionId)  From General_Journal_Details Where numTransactionId>@numTransactionId And numJournalId=@numJournalId    
   if @@rowcount=0     
   set @numTransactionId=0       
End    
set @lenAcntType = len(@varAcntType)        
if @lenAcntType >0     
Begin    
set @varAcntType = substring(@varAcntType,1,@lenAcntType-1)        
End    
    
return @varAcntType    
End
GO
