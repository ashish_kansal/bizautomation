
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetAssemblyPossibleWOQty')
DROP FUNCTION fn_GetAssemblyPossibleWOQty
GO
CREATE FUNCTION [dbo].[fn_GetAssemblyPossibleWOQty]
(
	@numItemCode as numeric(18,0)
	,@numWarehouseID NUMERIC(18,0)
)     
RETURNS INT 
AS      
BEGIN      
	DECLARE @numCount AS INT = 0;

	WITH CTE (numParentItemCode,numItemCode,vcItemName,numRequired,bitAssembly) AS
	(
		SELECT 
			CAST(0 AS NUMERIC(18,0)),
			ItemDetails.numChildItemID,
			Item.vcItemName,
			CAST(ItemDetails.numQtyItemsReq AS FLOAT)
			,ISNULL(Item.bitAssembly,0)
		FROM 
			ItemDetails 
		INNER JOIN
			Item
		ON
			ItemDetails.numChildItemID = Item.numItemCode
		WHERE 
			ItemDetails.numItemKitID = @numItemCode
			AND Item.charItemType = 'P'
		UNION ALL
		SELECT 
			CTE.numItemCode,
			ItemDetails.numChildItemID,
			Item.vcItemName,
			CAST((ItemDetails.numQtyItemsReq * CTE.numRequired) AS FLOAT)
			,ISNULL(Item.bitAssembly,0)
		FROM 
			CTE
		INNER JOIN
			ItemDetails 
		ON
			CTE.numItemCode = ItemDetails.numItemKitID
		INNER JOIN
			Item
		ON
			ItemDetails.numChildItemID = Item.numItemCode
			AND Item.charItemType = 'P'
	)



	SELECT 
		@numCount = MIN(CAST((ISNULL(numTotalOnHand,0)/(CASE WHEN ISNULL(numRequired,0) = 0 THEN 1 ELSE ISNULL(numRequired,0) END)) AS FLOAT)) 
	FROM 
		CTE c
	OUTER APPLY
	(
		SELECT 
			SUM(numOnHand) AS numTotalOnHand
		FROM
			WareHouseItems
		INNER JOIN 
			Warehouses W 
		ON 
			W.numWareHouseID = WareHouseItems.numWareHouseID
		WHERE
			numItemID = c.numItemCode
			AND WareHouseItems.numWareHouseID = @numWarehouseID
	) WI
	WHERE 
		numItemCode NOT IN (SELECT numParentItemCode FROM CTE)

	RETURN ISNULL(@numCount,0)
END
