GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetKitWeightBasedOnItemSelection')
DROP FUNCTION GetKitWeightBasedOnItemSelection
GO
CREATE FUNCTION [dbo].[GetKitWeightBasedOnItemSelection]
(
	@numItemCode NUMERIC(18,0),
	@vcSelectedKitChildItems VARCHAR(MAX)
)    
RETURNS FLOAT  
AS    
BEGIN   
	DECLARE @TempExistingItems TABLE
	(
		vcItem VARCHAR(100)
	)

	INSERT INTO @TempExistingItems
	(
		vcItem
	)
	SELECT 
		OutParam 
	FROM 
		SplitString(@vcSelectedKitChildItems,',')

	DECLARE @fltWeight AS FLOAT


	IF (SELECT COUNT(*) FROM @TempExistingItems) > 0
	BEGIN
		SET @fltWeight = ISNULL((SELECT
									SUM(numQty * ISNULL(I.fltWeight,0))
								FROM
								(
									SELECT 
										Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2)) As numItemCode
										,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3)) As numQty
									FROM  
									(
										SELECT 
											vcItem 
										FROM 
											@TempExistingItems
									) As [x] 
								) T2
								INNER JOIN
									Item I 
								ON
									T2.numItemCode = I.numItemCode),0)
	END
	ELSE
	BEGIN
		SET @fltWeight = ISNULL((SELECT
									SUM(ISNULL(ID.numQtyItemsReq,0) * dbo.fn_UOMConversion(numUOMID,I.numItemCode,I.numDomainID,I.numPurchaseUnit) * ISNULL(I.fltWeight,0))
								FROM
									ItemDetails ID
								INNER JOIN
									Item I 
								ON
									ID.numChildItemID = I.numItemCode
								WHERE
									numItemKitID=@numItemCode),0)
	END
	
	RETURN @fltWeight
END
GO