GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderPlannedStartDate')
DROP FUNCTION GetWorkOrderPlannedStartDate
GO
CREATE FUNCTION [dbo].[GetWorkOrderPlannedStartDate]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
)    
RETURNS DATETIME
AS
BEGIN
	DECLARE @dtCreatedDate DATETIME
	DECLARE @dtMaxItemExpectedDate DATETIME
	DECLARE @dtPlannedStartDate DATETIME
	DECLARE @numWarehouseID NUMERIC(18,0)

	SELECT
		@dtCreatedDate = bintCreatedDate
		,@dtPlannedStartDate = ISNULL(dtmStartDate,bintCreatedDate)
		,@numWarehouseID=ISNULL(WareHouseItems.numWareHouseID,0)
	FROM
		WorkOrder
	LEFT JOIN
		WareHouseItems
	ON
		WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	WHERE
		WorkOrder.numDomainID=@numDomainID
		AND WorkOrder.numWOId = @numWOID

	SET @dtMaxItemExpectedDate = (SELECT 
										MAX(dtItemExpectedDate)
									FROM 
									(
										SELECT 
											(CASE 
												WHEN EXISTS (SELECT WOInner.numWOId FROM WorkOrder WOInner WHERE WOInner.numDomainID=@numDomainID AND WOInner.numItemCode = Item.numItemCode AND WOInner.numParentWOID = @numWOID)
												THEN [dbo].[GetWorkOrderPlannedStartDate](@numDomainID,(SELECT WOInner.numWOId FROM WorkOrder WOInner WHERE WOInner.numDomainID=@numDomainID AND WOInner.numItemCode = Item.numItemCode AND WOInner.numParentWOID = @numWOID))
												ELSE DATEADD(DAY,ISNULL(dbo.GetVendorPreferredMethodLeadTime(Item.numVendorID,@numWarehouseID),0),@dtCreatedDate)
											END) dtItemExpectedDate
										FROM
											WorkOrderDetails
										INNER JOIN
											Item
										ON
											WorkOrderDetails.numChildItemID = Item.numItemCode
										INNER JOIN
											Vendor
										ON
											Item.numVendorID = Vendor.numVendorID
											AND Item.numItemCode = Vendor.numItemCode
										INNER JOIN
											WareHouseItems 
										ON
											WorkOrderDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
										WHERE
											numWOId = @numWOID
											AND (EXISTS (SELECT WOInner.numWOId FROM WorkOrder WOInner WHERE WOInner.numDomainID=@numDomainID AND WOInner.numItemCode = Item.numItemCode AND WOInner.numParentWOID = @numWOID) OR (WorkOrderDetails.numQtyItemsReq > ISNULL(WareHouseItems.numAllocation,0)))
									)TEMP)

	IF @dtMaxItemExpectedDate IS NOT NULL AND @dtMaxItemExpectedDate > @dtPlannedStartDate
	BEGIN
		SET @dtPlannedStartDate = @dtMaxItemExpectedDate
	END

	RETURN @dtPlannedStartDate
END
GO