/****** Object:  UserDefinedFunction [dbo].[GetDateRange]    Script Date: 07/26/2008 18:13:00 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getdaterange')
DROP FUNCTION getdaterange
GO
CREATE FUNCTION [dbo].[GetDateRange] (  
  
@ViewRange   
  
as varchar(50),  
@Period as char(1)  
)   
  
RETURNS datetime  
AS  
BEGIN  
  
  
DECLARE @PeriodDate datetime


declare @getDate DateTime
set @getDate=dateadd(d,datediff(w,0,GetutcDate()),1)
DECLARE @TodayDayOfWeek INT

--get number of a current day (1-Monday, 2-Tuesday... 7-Sunday)
SET @TodayDayOfWeek = datepart(dw, @getDate) 
   
  
IF  
  
@Period = 'S'   
  
SELECT @PeriodDate=CASE @ViewRange   
   
  
   
WHEN 'TW' THEN DATEADD(dd, -@TodayDayOfWeek, @getDate)    
  
WHEN 'LW' THEN DATEADD(dd, -(@TodayDayOfWeek+7), @getDate)   
  
WHEN 'TM' THEN DATEADD(m,DATEDIFF(m,0,GETUTCDATE()),0)   
  
WHEN 'LM' THEN DATEADD(m,DATEDIFF(m,0,GETUTCDATE())-1,0)   
  
WHEN 'TQ' THEN DATEADD(q,DATEDIFF(q,0,GETUTCDATE()),0)   
  
WHEN 'LQ' THEN DATEADD(q,DATEDIFF(q,0,GETUTCDATE())-1,0)   
  
--WHEN 'TY' THEN DATEADD(y,DATEDIFF(y,0,GETUTCDATE()),0)   
WHEN 'TY' THEN dateadd(day,0,'1/1/'+convert(varchar(4),year(getutcdate())))
  
--WHEN 'LY' THEN DATEADD(y,DATEDIFF(y,0,GETUTCDATE())-1,0)  
WHEN 'LY' THEN dateadd(day,0,'1/1/'+convert(varchar(4),year(getutcdate())-1)) 
  
END   
ELSE  
  
  
SELECT @PeriodDate=CASE @ViewRange   
  
WHEN 'TW' THEN DATEADD(dd, -@TodayDayOfWeek+7, @getDate)    
  
WHEN 'LW' THEN DATEADD(dd, -@TodayDayOfWeek, @getDate)    
  
WHEN 'TM' THEN DATEADD(m,DATEDIFF(m,0,GETUTCDATE())+1,0)   
  
WHEN 'LM' THEN DATEADD(m,DATEDIFF(m,0,GETUTCDATE()),0)   
  
WHEN 'TQ' THEN DATEADD(q,DATEDIFF(q,0,GETUTCDATE())+1,0)   
  
WHEN 'LQ' THEN DATEADD(q,DATEDIFF(q,0,GETUTCDATE()),0)   
  
--WHEN 'TY' THEN DATEADD(y,DATEDIFF(y,0,GETUTCDATE())+1,0)   
--  
--WHEN 'LY' THEN DATEADD(y,DATEDIFF(y,0,GETUTCDATE()),0)   

WHEN 'TY' THEN dateadd(day,0,'12/31/'+convert(varchar(4),year(getutcdate())))
  

WHEN 'LY' THEN dateadd(day,0,'12/31/'+convert(varchar(4),year(getutcdate())-1)) 
  
END   
  
-- Return the result of the function   
  
RETURN @PeriodDate   
END
GO
