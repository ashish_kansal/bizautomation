GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetUnitPriceAfterPriceLevelApplication')
DROP FUNCTION GetUnitPriceAfterPriceLevelApplication
GO
CREATE FUNCTION [dbo].[GetUnitPriceAfterPriceLevelApplication]
(
	@numDomainID numeric(18,0), 
	@numItemCode NUMERIC(18,0), 
	@numQuantity FLOAT, 
	@numWarehouseItemID NUMERIC(18,0), 
	@isPriceRule BIT, 
	@numPriceRuleID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
)  
RETURNS FLOAT  
AS  
BEGIN  

DECLARE @finalUnitPrice FLOAT = 0
DECLARE @tintRuleType INT
DECLARE @tintDiscountType INT
DECLARE @decDiscount FLOAT
DECLARE @ItemPrice FLOAT

SET @tintRuleType = 0
SET @tintDiscountType = 0
SET @decDiscount  = 0
SET @ItemPrice = 0

DECLARE @TEMPPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
)

DECLARE @monCalculatePrice AS DECIMAL(20,5)
DECLARE @tintPriceLevel INT = 0
SELECT @tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0
BEGIN
	INSERT INTO @TEMPPrice
	(
		bitSuccess
		,monPrice
	)
	SELECT
		bitSuccess
		,monPrice
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,(SELECT ISNULL(tintKitAssemblyPriceBasedOn,1) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode),0,0,'',0,1)

	IF (SELECT bitSuccess FROM @TEMPPrice) = 1
	BEGIN
		SET @monCalculatePrice = (SELECT monPrice FROM @TEMPPrice)
	END
END

IF @isPriceRule = 1
BEGIN
	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numPriceRuleID = @numPriceRuleID 
		AND ISNULL(numCurrencyID,0) = 0
		AND (@numQuantity BETWEEN intFromQty AND intToQty)
END
ELSE
BEGIN
	IF ISNULL(@tintPriceLevel,0) > 0
	BEGIN
		SELECT
			@tintRuleType = tintRuleType,
			@tintDiscountType = tintDiscountType,
			@decDiscount = decDiscount
		FROM
		(
			SELECT 
				ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
				tintRuleType,
				tintDiscountType,
				decDiscount
			FROM 
				PricingTable 
			WHERE 
				PricingTable.numItemCode = @numItemCode
				AND ISNULL(numCurrencyID,0) = 0
		) TEMP
		WHERE
			Id = @tintPriceLevel
	END
	ELSE
	BEGIN
		SELECT TOP 1
			@tintRuleType = tintRuleType,
			@tintDiscountType = tintDiscountType,
			@decDiscount = decDiscount
		FROM 
			PricingTable 
		WHERE 
			numItemCode = @numItemCode 
			AND ISNULL(numCurrencyID,0) = 0
			AND ((@numQuantity BETWEEN intFromQty AND intToQty) OR tintRuleType = 3)
	END
END

	

IF @tintRuleType > 0 AND @tintDiscountType > 0
BEGIN
	IF @tintRuleType = 1 -- Deduct from List price
	BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
				SELECT @ItemPrice = @monCalculatePrice
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
				SELECT @finalUnitPrice = @ItemPrice - @decDiscount
		END
	END
	ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
	BEGIN
		If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
			SELECT @ItemPrice = @monCalculatePrice
		ELSE
			SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
			SELECT @finalUnitPrice = @ItemPrice + @decDiscount
		END
	END
	ELSE IF @tintRuleType = 3 -- Named Price
	BEGIN
		SELECT @finalUnitPrice = @decDiscount
	END
END

return @finalUnitPrice
END
GO
