/****** Object:  UserDefinedFunction [dbo].[fn_getProPreviousStage]    Script Date: 07/26/2008 18:12:44 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getpropreviousstage')
DROP FUNCTION fn_getpropreviousstage
GO
CREATE FUNCTION [dbo].[fn_getProPreviousStage](@numProId as numeric(9),@numCurrentStageId as numeric(9))
returns numeric(9)
as
begin

	declare @numProStageId as numeric(9)
	declare @numPrevProStageId as numeric(9)
	set @numPrevProStageId =0
	set @numProStageId =0

	select top 1 @numProStageId=numProStageId from ProjectsStageDetails 
	where numProId = @numProId and numstagepercentage not in (0,100)

	order by numProStageId,numstagepercentage

	while @numProStageId > 0
	begin 

		if @numProStageId <> @numCurrentStageId	
			set @numPrevProStageId = @numProStageId
		if @numProStageId = @numCurrentStageId
			set @numProStageId = 0

		if @numProStageId <> 0
		begin
			select top 1 @numProStageId=numProStageId from ProjectsStageDetails 
			where numProId = @numProId and numstagepercentage not in (0,100) and  numProStageId>@numProStageId
			order by numProStageId,numstagepercentage
			 if @@rowcount=0 set @numProStageId=0 
		end

	end

	return  @numPrevProStageId

end
GO
