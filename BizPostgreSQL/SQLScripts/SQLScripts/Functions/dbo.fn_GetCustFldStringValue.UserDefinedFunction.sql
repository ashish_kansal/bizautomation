GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetCustFldStringValue')
DROP FUNCTION fn_GetCustFldStringValue
GO
CREATE FUNCTION [dbo].[fn_GetCustFldStringValue]
(
	@numFldId NUMERIC(18,0),
	@numRecordID NUMERIC(18,0),
	@vcFldValue VARCHAR(MAX)
)  
RETURNS VARCHAR(500)   
AS  
BEGIN  
	DECLARE @vcValue as  varchar(500)  

	DECLARE @fld_type AS VARCHAR(20)
	SELECT @fld_type = Fld_type FROM dbo.CFW_Fld_Master WHERE Fld_id = @numFldId

	IF @fld_type = 'SelectBox' AND ISNUMERIC(@vcFldValue) = 1
	BEGIN
		SET @vcValue = (SELECT vcData FROM dbo.ListDetails WHERE numListItemID = @vcFldValue)
	END
	ELSE IF @fld_type = 'CheckBoxList'
	BEGIN
		SELECT 
			@vcValue = STUFF((SELECT CONCAT(',', vcData) 
		FROM 
			ListDetails 
		WHERE 
			numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(@vcFldValue,',')) FOR XML PATH('')), 1, 1, '')
	END
	ELSE IF @fld_type = 'CheckBox'
	BEGIN
		SET @vcValue = (CASE WHEN ISNULL(@vcFldValue,0)=1 THEN 'Yes' ELSE 'No' END)
	END
	ELSE
	BEGIN
		SET @vcValue = @vcFldValue
	END

	IF @vcValue IS NULL 
		SET @vcValue=''  

	RETURN ISNULL(@vcValue ,'')
END
GO