/****** Object:  UserDefinedFunction [dbo].[fn_getTemplateName]    Script Date: 07/26/2008 18:12:46 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_gettemplatename')
DROP FUNCTION fn_gettemplatename
GO
CREATE FUNCTION [dbo].[fn_getTemplateName] (@numGenericDocID as numeric,@numDomainId as numeric)
returns varchar(300)
as 
begin
	declare @vcDocName as varchar(300)
	set @vcDocName=''

	select @vcDocName=isnull(vcDocName,'') from GenericDocuments      
	where    numGenericDocID=  @numGenericDocID And numDomainId = @numDomainID  

	return @vcDocName
end
GO
