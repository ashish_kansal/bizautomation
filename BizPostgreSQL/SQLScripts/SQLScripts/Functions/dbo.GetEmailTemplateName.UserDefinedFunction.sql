
GO
/****** Object:  UserDefinedFunction [dbo].[GetEmailTemplateName]    Script Date: 02/18/2009 15:19:09 ******/

GO

GO
-- =============================================
-- Author:		Pratik Vasani
-- Create date: 13/Dec/2008
-- Description:	This function gets the email template name for the specified email template  ID
-- =============================================
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getemailtemplatename')
DROP FUNCTION getemailtemplatename
GO
CREATE FUNCTION [dbo].[GetEmailTemplateName] 
(
	@numConEmailCampID numeric(9) -- Pass PK([numConEmailCampID]) of ConECampaign
	
	--@ECampDetailId numeric(9,0)
)
RETURNS varchar(100)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result varchar(100)

	-- Add the T-SQL statements to compute the return value here


SELECT @Result= [vcDocName]
FROM   [GenericDocuments]
WHERE  GenericDocuments.numDocCategory = 369
       AND [numGenericDocID] IN (SELECT TOP 1 [numEmailTemplate]
                                 FROM   [ECampaignDTLs]
                                 WHERE  [numECampDTLId] IN (SELECT   TOP 1 [numECampDTLID]
                                                            FROM     [ConECampaignDTL]
                                                            WHERE    [bitSend] = 1
                                                                     AND [numConECampID] = @numConEmailCampID
                                                            ORDER BY [numConECampDTLID] DESC))


--SELECT    @Result= GenericDocuments.vcDocName
--FROM         GenericDocuments INNER JOIN
--                      ECampaignDTLs ON GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
--WHERE    (GenericDocuments.numDocCategory = 369)  
--and  ECampaignDTLs.numECampDTLID = @ECampDetailId

--(SELECT TOP 1 [numConECampDTLID] FROM [ConECampaignDTL] WHERE  [numConECampID] =[ConECampaign].[numConEmailCampID] AND ConECampaignDTL.bitSend = 1 ORDER BY [numConECampDTLID] DESC)

	-- Return the result of the function
	RETURN @Result

END
