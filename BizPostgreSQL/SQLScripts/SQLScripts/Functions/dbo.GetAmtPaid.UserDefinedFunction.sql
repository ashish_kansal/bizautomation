/****** Object:  UserDefinedFunction [dbo].[GetAmtPaid]    Script Date: 07/26/2008 18:12:55 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAmtPaid')
DROP FUNCTION GetAmtPaid
GO
CREATE FUNCTION [dbo].[GetAmtPaid](@numOppid numeric(9))
	RETURNS DECIMAL(20,5)
AS
BEGIN
declare @AmtPaid as DECIMAL(20,5)

select top 1 @AmtPaid=tintOppStatus from OpportunityMaster where numOppid=@numOppid

	RETURN @AmtPaid
END
GO
