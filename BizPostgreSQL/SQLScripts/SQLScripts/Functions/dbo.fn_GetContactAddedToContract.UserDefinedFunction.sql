GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcontactaddedtocontract')
DROP FUNCTION fn_getcontactaddedtocontract
GO
CREATE FUNCTION [dbo].[fn_GetContactAddedToContract]
    (
      @numDomainId numeric,
      @numConractID numeric
    )
RETURNS VARCHAR(2000)
AS BEGIN

    DECLARE @ContactList VARCHAR(2000)
    SELECT  @ContactList = COALESCE(@ContactList + ', ', '')
            + CAST(numContactID AS VARCHAR(5))
    FROM    [ContractsContact]
	WHERE [numDomainId]= @numDomainId AND [numContractId]=@numConractID
    
    RETURN ISNULL(@ContactList,'')
   END 