GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='GetWOStatus')
DROP FUNCTION GetWOStatus
GO
CREATE FUNCTION [dbo].[GetWOStatus] 
(
	@numDomainID NUMERIC(18,0)
	,@vcWorkOrderIds VARCHAR(1000)
)
RETURNS @TEMPWorkOrder TABLE
(
	ID INT IDENTITY(1,1)
	,numWOID NUMERIC(18,0)
	,numItemCode NUMERIC(18,0)
	,numQtyToBuild FLOAT
	,numWarehouseID NUMERIC(18,0)
	,numWorkOrderStatus TINYINT
	,vcWorkOrderStatus VARCHAR(300)
)

AS
BEGIN
	INSERT INTO @TEMPWorkOrder
	(
		numWOID
		,numItemCode
		,numQtyToBuild
		,numWarehouseID 
		,numWorkOrderStatus
		,vcWorkOrderStatus
	)
	SELECT
		WorkOrder.numWOId
		,WorkOrder.numItemCode
		,ISNULL(WorkOrder.numQtyItemsReq,0)
		,WareHouseItems.numWareHouseID
		,(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 0 ELSE NULL END)
		,(CASE WHEN WorkOrder.numWOStatus = 23184 THEN '<span style="color:#00b050">Build Completed</span>' ELSE NULL END)
	FROM
		WorkOrder
	INNER JOIN
		WareHouseItems
	ON
		WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		OpportunityItems
	ON
		WorkOrder.numOppItemID = OpportunityItems.numoppitemtCode
	WHERE
		WorkOrder.numDomainID = @numDomainID
	ORDER BY
		ISNULL(WorkOrder.dtmEndDate,OpportunityItems.ItemReleaseDate) ASC
		,WorkOrder.dtmStartDate ASC
		,WorkOrder.bintCreatedDate ASC
		,ISNULL(WorkOrder.numParentWOID,0) DESC

	DECLARE @WorkOrderItems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)		
		,numAvailable FLOAT
	)

	INSERT INTO @WorkOrderItems
	(
		numItemCode
		,numWarehouseID
		,numAvailable
	)
	SELECT
		TEMP.numItemCode
		,WI.numWareHouseID
		,SUM(WI.numOnHand) + SUM(WI.numAllocation)
	FROM
	(
		SELECT
			I.numItemCode
			,TWO.numWareHouseID
		FROM
			@TEMPWorkOrder TWO
		INNER JOIN
			WorkOrderDetails WOD
		ON
			TWO.numWOID = WOD.numWOId
		INNER JOIN
			Item I
		ON
			WOD.numChildItemID = I.numItemCode
			AND ISNULL(WOD.numQtyItemsReq_Orig,0) > 0
		GROUP BY
			I.numItemCode
			,TWO.numWareHouseID
	) TEMP
	INNER JOIN
		WareHouseItems WI
	ON
		TEMP.numItemCode = WI.numItemID
		AND TEMP.numWarehouseID = WI.numWareHouseID
	GROUP BY
		TEMP.numItemCode
		,WI.numWareHouseID
		

	DECLARE @i INT = 1
	DECLARE @iCount INT
	SELECT @iCount = COUNT(*) FROM @TEMPWorkOrder

	DECLARE @numWOID NUMERIC(18,0)
	DECLARE @numQtyToBuild FLOAT
	DECLARE @numQtyCanBeBuild FLOAT
	DECLARE @numQtyBuildable FLOAT
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @vcWorkOrderStatus VARCHAR(100)

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numWOID=numWOID
			,@numQtyToBuild=numQtyToBuild
			,@numWarehouseID=numWarehouseID
			,@vcWorkOrderStatus=vcWorkOrderStatus
		FROM 
			@TEMPWorkOrder 
		WHERE 
			ID = @i

		IF @vcWorkOrderStatus IS NULL
		BEGIN
			SET @numQtyCanBeBuild = ISNULL((SELECT 
											MIN(numQtyCanBeBuild)
										FROM
										(
											SELECT
												I.numItemCode
												,WOD.numQtyItemsReq
												,WOD.numQtyItemsReq_Orig
												,WOI.numAvailable
												,(CASE 
													WHEN ISNULL(WOI.numAvailable,0) >= (ISNULL(WOD.numQtyItemsReq,0) - ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId=WOD.numWODetailId),0))
													THEN @numQtyToBuild - FLOOR(ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId=WOD.numWODetailId),0)/WOD.numQtyItemsReq_Orig)
													WHEN ISNULL(WOI.numAvailable,0) > 0 
													THEN FLOOR(ISNULL(WOI.numAvailable,0)/WOD.numQtyItemsReq_Orig)
													ELSE 0
												END) numQtyCanBeBuild
											FROM
												WorkOrderDetails WOD
											INNER JOIN
												Item I
											ON
												WOD.numChildItemID = I.numItemCode
											INNER JOIN
												@WorkOrderItems WOI
											ON
												I.numItemCode = WOI.numItemCode
												AND WOI.numWarehouseID = @numWarehouseID
											WHERE
												numWOId = @numWOID
												AND ISNULL(WOD.numWareHouseItemId,0) > 0
												AND ISNULL(WOD.numQtyItemsReq_Orig,0) > 0
										) TEMP1),0)

			SET @numQtyBuildable = ISNULL((SELECT 
												MIN(numQtyBuildable)
											FROM
											(
												SELECT
													FLOOR(SUM(WOPI.numPickedQty)/WOD.numQtyItemsReq_Orig) numQtyBuildable
												FROM
													WorkOrderDetails WOD
												INNER JOIN
													WorkOrderPickedItems WOPI
												ON
													WOD.numWODetailId = WOPI.numWODetailId
												WHERE
													numWOId = @numWOID
													AND ISNULL(WOD.numWareHouseItemId,0) > 0
													AND ISNULL(WOD.numQtyItemsReq_Orig,0) > 0
												GROUP BY
													WOD.numWODetailId
													,WOD.numQtyItemsReq_Orig
											) TEMP1),0)

			UPDATE
				@TEMPWorkOrder
			SET 
				numWorkOrderStatus = (CASE 
										WHEN @numQtyBuildable = @numQtyToBuild
										THEN 3
										WHEN @numQtyCanBeBuild >= @numQtyToBuild 
										THEN 1 
										WHEN @numQtyCanBeBuild > 0
										THEN 2
										ELSE 0
									END)
				,vcWorkOrderStatus = (CASE 
										WHEN @numQtyBuildable = @numQtyToBuild
										THEN '<span style="color:#00b050">Fully Buildable</span>'
										WHEN @numQtyCanBeBuild >= @numQtyToBuild AND @numQtyBuildable = 0
										THEN '<span style="color:#7030a0">Fully Pickable</span>' 
										ELSE
										CONCAT((CASE WHEN @numQtyCanBeBuild > 0 THEN CONCAT('<span style="color:#7030a0">Pickable(',@numQtyCanBeBuild,')</span>')  ELSE '' END)
										,(CASE WHEN @numQtyBuildable > 0 THEN CONCAT(' <span style="color:#00b050">Buildable(',@numQtyBuildable,')</span>') ELSE '' END)
										,(CASE WHEN (@numQtyToBuild - ISNULL((SELECT
																					MIN(numPickedQty)
																				FROM
																				(
																					SELECT 
																						WODInner.numWODetailId
																						,FLOOR(SUM(WOPI.numPickedQty)/WODInner.numQtyItemsReq_Orig) numPickedQty
																					FROM
																						WorkOrderDetails WODInner
																					LEFT JOIN
																						WorkOrderPickedItems WOPI
																					ON
																						WODInner.numWODetailId=WOPI.numWODetailId
																					WHERE
																						WODInner.numWOId=@numWOID
																						AND ISNULL(WODInner.numWareHouseItemId,0) > 0
																						AND ISNULL(WODInner.numQtyItemsReq_Orig,0) > 0
																					GROUP BY
																						WODInner.numWODetailId
																						,WODInner.numQtyItemsReq_Orig
																				) TEMP),0) -@numQtyCanBeBuild) > 0 THEN CONCAT(' <span style="color:#ff0000">BO(',@numQtyToBuild -@numQtyCanBeBuild,')</span>') ELSE '' END))
									END)
			WHERE
				numWOID = @numWOID


			UPDATE
				WOI
			SET
				WOI.numAvailable = ISNULL(WOI.numAvailable,0) - ISNULL(WOD.numQtyItemsReq,0)
			FROM
				WorkOrderDetails WOD
			INNER JOIN
				Item I
			ON
				WOD.numChildItemID = I.numItemCode
			LEFT JOIN
				@WorkOrderItems WOI
			ON
				I.numItemCode = WOI.numItemCode
				AND WOI.numWarehouseID = @numWarehouseID
				AND ISNULL(WOD.numQtyItemsReq_Orig,0) > 0
			WHERE
				numWOId = @numWOID

		END

		SET @i = @i + 1
	END

	RETURN
END
GO