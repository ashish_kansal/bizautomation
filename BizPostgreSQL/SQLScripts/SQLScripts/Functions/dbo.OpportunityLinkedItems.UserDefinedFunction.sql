/****** Object:  UserDefinedFunction [dbo].[OpportunityLinkedItems]    Script Date: 07/26/2008 18:13:14 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='opportunitylinkeditems')
DROP FUNCTION opportunitylinkeditems
GO
CREATE FUNCTION [dbo].[OpportunityLinkedItems](@numOppID as numeric(9))
returns numeric(9)
as
begin
	declare @RetVal as numeric(9)
	set @RetVal =0
	
	  select @RetVal=@RetVal+isnull(count(*),0) from
	  ProjectsOpportunities 
	  where numOppId=@numOppID 

	  select @RetVal=@RetVal+isnull(count(*),0) from
	  caseOpportunities 
	  where numOppId=@numOppID  
	return @RetVal

end
GO
