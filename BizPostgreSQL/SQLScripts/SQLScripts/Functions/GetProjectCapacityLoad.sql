/****** Object:  UserDefinedFunction [dbo].[Split]    Script Date: 07/26/2008 18:13:14 ******/
SET ANSI_NULLS OFF
GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetProjectCapacityLoad')
DROP FUNCTION GetProjectCapacityLoad
GO
CREATE FUNCTION [dbo].[GetProjectCapacityLoad]
(
	@numDomainID NUMERIC(18,0)
	,@numUsercntID NUMERIC(18,0)
	,@numTeamID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintDateRange TINYINT -- 1:Today, 2:Week, 3:Month
	,@dtFromDate DATETIME
	,@ClientTimeZoneOffset INT
)    
RETURNS INT   
AS
BEGIN
	SET @dtFromDate = DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,@dtFromDate)

	DECLARE @numDivisionID NUMERIC(18,0)
	SELECT @numDivisionID=numDivisionID FROM Domain WHERE numDomainId=@numDomainID
	
	DECLARE @numQtyToBuild FLOAT
	DECLARE @dtPlannedStartDate DATETIME
	DECLARE @dtActualStartDate DATETIME = NULL

	SELECT 
		@numQtyToBuild=1
		,@dtPlannedStartDate=dtmStartDate 
	FROM 
		ProjectsMaster WHERE numProId=@numProId


	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId=@numProId) AND tintAction=1)
	BEGIN
		SELECT @dtActualStartDate = MIN(dtActionTime) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId=@numProId) AND tintAction=1
	END

	DECLARE @TEMPTaskTimeSequence TABLE
	(
		numAssignedTo NUMERIC(18,0)
		,dtDate DATE
		,numTaskHours NUMERIC(18,0)
	)

	DECLARE @TempTaskAssignee TABLE
	(
		numAssignedTo NUMERIC(18,0)
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,numTaskAssignee NUMERIC(18,0)
		,intTaskType INT --1:Parallel, 2:Sequential
		,dtPlannedStartDate DATETIME
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numTaskTimeInMinutes
		,intTaskType
		,numTaskAssignee
	)
	SELECT 
		SPDT.numTaskID
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (@numQtyToBuild - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0))
		,ISNULL(SPDT.intTaskType,1)
		,SPDT.numAssignTo
	FROM 
		StagePercentageDetailsTask SPDT
	INNER JOIN
		ProjectsMaster pro
	ON
		SPDT.numProjectId = pro.numProId
	WHERE 
		SPDT.numDomainId=@numDomainID
		AND SPDT.numProjectId = @numProId
		AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 

	UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

	INSERT INTO @TempTaskAssignee
	(
		numAssignedTo
	)
	SELECT DISTINCT
		numTaskAssignee
	FROM
		@TempTasks

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numTempUserCntID NUMERIC(18,0)
	DECLARE @dtStartDate DATETIME
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @bitParallelStartSet BIT = 0

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTaskAssignee=numTaskAssignee
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay 
			,@vcWorkDays=vcWorkDays
			,@dtStartDate = (CASE WHEN @dtActualStartDate IS NULL THEN DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay) ELSE @dtActualStartDate END)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
		END
		ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
		END

		UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0 AND LEN(ISNULL(@vcWorkDays,'')) > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF DATEPART(WEEKDAY,@dtStartDate) IN (SELECT Id FROM dbo.SplitIDs(@vcWorkDays,',')) AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
					BEGIN
						SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()),112)) + @tmStartOfDay)))

						-- CHECK TIME LEFT FOR DAY BASED
						IF @numTimeLeftForDay >= @numProductiveTimeInMinutes
						BEGIN
							SET @numTimeLeftForDay = @numProductiveTimeInMinutes;
						END
					END
					ELSE
					BEGIN
						SET @numTimeLeftForDay = @numProductiveTimeInMinutes
					END

					IF @numTimeLeftForDay > 0
					BEGIN
						INSERT INTO @TEMPTaskTimeSequence
						(
							numAssignedTo
							,dtDate
							,numTaskHours
						)
						VALUES
						(
							@numTaskAssignee
							,@dtStartDate
							,(CASE WHEN @numTimeLeftForDay > @numTotalTaskInMinutes THEN @numTotalTaskInMinutes ELSE @numTimeLeftForDay END)
						)

						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
						END

						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)					
				END				
			END
		END	

		UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END


	DECLARE @TEMPEmployee TABLE
	(
		ID INT IDENTITY(1,1)
		,numContactID NUMERIC(18,0)
		,numProductiveMinutes NUMERIC(18,0)
		,tmStartOfDay TIME(7)
		,vcWorkDays VARCHAR(20)
		,numCapacityLoad NUMERIC(18,0)
	)

	INSERT INTO @TEMPEmployee
	(
		numContactID
		,numProductiveMinutes
		,tmStartOfDay
		,vcWorkDays
	)
	SELECT 
		AdditionalContactsInformation.numContactID
		,(ISNULL(WorkSchedule.numProductiveHours,0) * 60) + ISNULL(WorkSchedule.numProductiveMinutes,0)
		,WorkSchedule.tmStartOfDay
		,WorkSchedule.vcWorkDays
	FROM
		AdditionalContactsInformation
	LEFT JOIN
		WorkSchedule
	ON
		AdditionalContactsInformation.numContactId = WorkSchedule.numUserCntID
	WHERE
		AdditionalContactsInformation.numDomainID=@numDomainID
		AND AdditionalContactsInformation.numDivisionId = @numDivisionID
		AND ((@numUsercntID > 0 AND AdditionalContactsInformation.numContactId=@numUsercntID) OR (@numTeamID > 0 AND AdditionalContactsInformation.numTeam=@numTeamID))

	DECLARE @j INT 
	DECLARE @jCount INT
	DECLARE @numTimeAndExpenseMinues NUMERIC(18,0)
	DECLARE @numAssemblyBuildMinues NUMERIC(18,0)

	SET @j = 1
	SELECT @jCount = COUNT(*) FROM @TEMPEmployee
	DECLARE @numContactID NUMERIC(18,0)

	WHILE @j <= @jCount
	BEGIN
		SELECT 
			@numContactID=numContactID
			,@numProductiveTimeInMinutes = numProductiveMinutes
		FROM 
			@TEMPEmployee 
		WHERE 
			ID=@j

		SET @numTimeAndExpenseMinues = ISNULL((SELECT 
														SUM(DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate))
													FROM 
														TimeAndExpense 
													WHERE 
														numDomainID=@numDomainID 
														AND numUserCntID = @numContactID
														AND numCategory=1 
														AND numType IN (1,2)
														AND 1 = (CASE 
																	WHEN @tintDateRange = 1 -- Day
																	THEN (CASE WHEN CAST(dtFromDate AS DATE) = CAST(@dtFromDate AS DATE) THEN 1 ELSE 0 END)
																	WHEN @tintDateRange = 2 -- Week
																	THEN (CASE WHEN CAST(dtFromDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(DAY,7,@dtFromDate) AS DATE) THEN 1 ELSE 0 END) --DATEADD(DAY,2 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE() AS DATE))
																	WHEN @tintDateRange = 3 -- Month
																	THEN (CASE WHEN CAST(dtFromDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(MILLISECOND,-3,DATEADD(MONTH,1,@dtFromDate)) AS DATE) THEN 1 ELSE 0 END) --CAST(DATEADD(DAY, 1-DAY(GETUTCDATE()), GETUTCDATE()) AS DATE)
																	ELSE 0
																END)),0)

		SET @numAssemblyBuildMinues = ISNULL((SELECT 
												SUM(numTaskHours)
											FROM
												@TEMPTaskTimeSequence 
											WHERE
												numAssignedTo = @numContactID
												AND 1 = (CASE 
															WHEN @tintDateRange = 1 -- Day
															THEN (CASE WHEN CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE) THEN 1 ELSE 0 END)
															WHEN @tintDateRange = 2 -- Week
															THEN (CASE WHEN CAST(dtDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(DAY,7,@dtFromDate) AS DATE) THEN 1 ELSE 0 END)
															WHEN @tintDateRange = 3 -- Month
															THEN (CASE WHEN CAST(dtDate AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(DATEADD(MILLISECOND,-3,DATEADD(MONTH,1,@dtFromDate)) AS DATE) THEN 1 ELSE 0 END)
															ELSE 0
														END)),0)


		SET @numProductiveTimeInMinutes = @numProductiveTimeInMinutes * (CASE WHEN @tintDateRange=1 THEN 1 WHEN @tintDateRange=2 THEN 7 WHEN @tintDateRange=3 THEN 30 END) 
		
		UPDATE @TEMPEmployee SET numCapacityLoad = (CASE WHEN @numProductiveTimeInMinutes > 0 THEN (((@numAssemblyBuildMinues + @numTimeAndExpenseMinues) * 100) / @numProductiveTimeInMinutes) ELSE 0 END) WHERE ID = @j


		SET @j = @j + 1
	END

	RETURN ISNULL((SELECT AVG(numCapacityLoad) FROM @TEMPEmployee),0)
END
