GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetShippingReportAddress')
DROP FUNCTION fn_GetShippingReportAddress
GO
CREATE FUNCTION [dbo].[fn_GetShippingReportAddress]
(
	@tintType INT -- 1: FROM Addres, 2: TO Address
	,@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
) 
RETURNS @TEMP TABLE
(
	vcName VARCHAR(100)
	,vcCompanyName VARCHAR(100)
	,vcPhone VARCHAR(50)
	,vcStreet VARCHAR(300)
	,vcCity VARCHAR(50)
	,vcState NUMERIC(18,0)
	,vcZipCode VARCHAR(50)
	,vcCountry NUMERIC(18,0)
	,bitResidential BIT
)
AS
BEGIN
	IF @tintType = 1
	BEGIN
		INSERT INTO 
			@TEMP
		SELECT TOP 1
			CONCAT(ISNULL(A.vcFirstName,''),' ',ISNULL(A.vcLastName,''))
			,ISNULL(C.vcCompanyName,'')
			,ISNULL(D.[vcComPhone],'')
			,ISNULL(AD2.vcstreet,'')
			,ISNULL(AD2.vcCity,'')  
			,ISNULL(AD2.numState,0)
			,ISNULL(AD2.vcPostalCode,'')
			,ISNULL(AD2.numCountry,0)
			,ISNULL(AD2.bitResidential,0)
		FROM 
			DivisionMaster D
		INNER JOIN
			CompanyInfo C
		ON
			D.numCompanyID=C.numCompanyId
		LEFT JOIN
			AdditionalContactsInformation A        
		ON 
			A.numDivisionId = D.numDivisionId
			AND ISNULL(A.bitPrimaryContact,0)=1
		LEFT JOIN 
			dbo.AddressDetails AD2 
		ON 
			AD2.numDomainID=D.numDomainID 
			AND AD2.numRecordID= D.numDivisionID 
			AND AD2.tintAddressOf=2
			AND AD2.tintAddressType=2 
			AND AD2.bitIsPrimary=1
		WHERE
			C.numDomainID=@numDomainID
			AND C.numCompanyType=93
	END
	ELSE IF @tintType = 2
	BEGIN
		DECLARE @tintShipToType AS TINYINT
		DECLARE @numDivisionID AS NUMERIC(18,0)
		DECLARE @numContactID AS NUMERIC(18,0)
		DECLARE @vcName AS VARCHAR(100)
		DECLARE @vcPhone VARCHAR(50)
	
		SELECT  
			@tintShipToType = tintShipToType
			,@numDivisionID = numDivisionID
			,@numContactID = numContactID
        FROM 
			OpportunityMaster
        WHERE 
			numOppId = @numOppID


        SELECT  
			@vcName = ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
			,@vcPhone = (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
        FROM 
			AdditionalContactsInformation
        WHERE 
			numContactID = @numContactID

        IF (@tintShipToType IS NULL OR @tintShipToType = 1) 
		BEGIN
			INSERT INTO 
				@TEMP
			SELECT
				(CASE 
					WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
					THEN vcAltContact
					WHEN ISNULL(numContact,0) > 0
					THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
					ELSE @vcName
				END)
				,dbo.fn_GetComapnyName(@numDivisionID)
				,(CASE 
					WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
					THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
					WHEN ISNULL(numContact,0) > 0
					THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
					ELSE @vcPhone
				END)
				,ISNULL(AD.VcStreet,'')
                ,ISNULL(AD.VcCity,'')
                ,ISNULL(AD.numState,0)
                ,ISNULL(AD.vcPostalCode,'')
                ,ISNULL(AD.numCountry,0)
				,ISNULL(AD.bitResidential,0)
			FROM
				dbo.AddressDetails AD
			LEFT JOIN
				AdditionalContactsInformation ACI
			ON
				AD.numContact = ACI.numContactId
            WHERE   
				AD.numDomainID=@numDomainID
				AND AD.numRecordID = @numDivisionID
                AND AD.tintAddressOf = 2
                AND AD.tintAddressType = 2
                AND AD.bitIsPrimary = 1
		END
        ELSE IF @tintShipToType = 0 
		BEGIN
			INSERT INTO
				@TEMP
			SELECT
				(CASE 
					WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
					THEN vcAltContact
					WHEN ISNULL(numContact,0) > 0
					THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
					ELSE @vcName
				END)
				,dbo.fn_GetComapnyName(@numDivisionID)
				,(CASE 
					WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
					THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
					WHEN ISNULL(numContact,0) > 0
					THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
					ELSE @vcPhone
				END)
				,ISNULL(AD1.VcStreet,'')
                ,ISNULL(AD1.VcCity,'')
                ,ISNULL(AD1.numState,0)
                ,ISNULL(AD1.vcPostalCode,'')
                ,ISNULL(AD1.numCountry,0)
				,ISNULL(AD1.bitResidential,0)
			FROM 
				CompanyInfo Com1
            INNER JOIN 
				DivisionMaster div1 
			ON 
				com1.numCompanyID = div1.numCompanyID
            INNER JOIN 
				Domain D1 
			ON 
				D1.numDivisionID = div1.numDivisionID
            INNER JOIN 
				dbo.AddressDetails AD1 
			LEFT JOIN
				AdditionalContactsInformation ACI
			ON
				AD1.numContact = ACI.numContactId
			ON 
				AD1.numDomainID = div1.numDomainID
				AND AD1.numRecordID = div1.numDivisionID
				AND tintAddressOf = 2
				AND tintAddressType = 2
				AND bitIsPrimary = 1
            WHERE 
				D1.numDomainID = @numDomainID
		END              
        ELSE IF @tintShipToType = 2 OR @tintShipToType = 3
		BEGIN
			INSERT INTO
				@TEMP
			SELECT  
				(CASE 
					WHEN ISNULL(bitAltShippingContact,0)=1 AND LEN(ISNULL(vcAltShippingContact,'')) > 0
					THEN vcAltShippingContact
					WHEN ISNULL(numShippingContact,0) > 0
					THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
					ELSE @vcName
				END)
				,dbo.GetCompanyNameFromContactID(@numContactID, @numDomainID)
				,(CASE 
					WHEN ISNULL(bitAltShippingContact,0)=1 AND LEN(ISNULL(vcAltShippingContact,'')) > 0
					THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
					WHEN ISNULL(numShippingContact,0) > 0
					THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
					ELSE @vcPhone
				END)
				,ISNULL(vcShipStreet,'')
                ,ISNULL(vcShipCity,'')
                ,ISNULL(numShipState,0)
                ,ISNULL(vcShipPostCode,'')
                ,ISNULL(numShipCountry,0)
				,0
			FROM 
				OpportunityAddress
			LEFT JOIN
				AdditionalContactsInformation ACI
			ON
				OpportunityAddress.numShippingContact = ACI.numContactId
			WHERE
				numOppID = @numOppID
		END           
	END

	RETURN
END