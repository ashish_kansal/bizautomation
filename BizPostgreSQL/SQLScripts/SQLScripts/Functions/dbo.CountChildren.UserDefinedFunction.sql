/****** Object:  UserDefinedFunction [dbo].[CountChildren]    Script Date: 07/26/2008 18:12:26 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='countchildren')
DROP FUNCTION countchildren
GO
CREATE FUNCTION [dbo].[CountChildren]
(@id int, @cChildren int) 
RETURNS bigint 
AS
BEGIN

--IF EXISTS (SELECT     
--    Sites.SiteCatID
--FROM         
--    dbo.Categories 
--INNER JOIN
--    dbo.Sites 
--ON 
--    dbo.Categories.CategoryID = dbo.Sites.SiteCatID
--WHERE 
--    dbo.Categories.ParentID = @id OR dbo.Sites.SiteCatID = @id)
--BEGIN 
--   SET @cChildren = @cChildren + (
--     SELECT 
--        Count(SiteCatID) 
--        FROM 
--            Sites 
--        WHERE 
--            SiteCatID = @id AND SiteActive = 1)
--  SELECT 
--              @cChildren = dbo.CountChildren(CategoryID, @cChildren) 
--            FROM 
--              Categories 
--            WHERE 
--              ParentID = @id 
--END 
  RETURN @cChildren 
END
GO
