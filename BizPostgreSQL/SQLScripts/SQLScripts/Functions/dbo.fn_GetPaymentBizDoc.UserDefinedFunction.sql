/****** Object:  UserDefinedFunction [dbo].[fn_GetPaymentBizDoc]    Script Date: 10/05/2009 14:28:37 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getpaymentbizdoc')
DROP FUNCTION fn_getpaymentbizdoc
GO
CREATE FUNCTION [dbo].[fn_GetPaymentBizDoc]
(@numBizDocsPaymentDetId numeric(8),
 @numDomainId int)
Returns varchar(2000)
AS 
begin

declare @vcDescription varchar(2000);
declare @Amount varchar(50);
declare @numCheckNo varchar(50);
declare @vcReference varchar(50);
declare @vcMemo varchar(50);

declare BizDoc_Cursor CURSOR FOR
	
SELECT cast(SUM(monAmount) as varchar(50)) as Amount,
	isnull( CAST(numCheckNo AS VARCHAR(50)),'No-Chek') as numCheckNo,
	isnull(vcReference,'No-Ref') as vcReference,
	isnull(vcMemo,'No-Memo' ) as vcMemo FROM VIEW_BIZDOCPAYMENT
	WHERE numBizDocsPaymentDetId=@numBizDocsPaymentDetId AND 
	numDomainId=@numDomainId
	GROUP BY numCheckNo,vcReference,vcMemo

set @vcDescription=''

OPEN BizDoc_Cursor

FETCH NEXT FROM BizDoc_Cursor into @Amount,@numCheckNo,@vcReference,@vcMemo
WHILE @@FETCH_STATUS = 0
   BEGIN
		set @vcDescription= @vcDescription + ',' + 'Ref: ' + @vcReference +
		' Memo: ' + @vcMemo + 
		' Chek No: ' + @numCheckNo + 
		' Amount: ' + @Amount

      FETCH NEXT FROM BizDoc_Cursor into @Amount,@numCheckNo,@vcReference,@vcMemo		
   END;

CLOSE BizDoc_Cursor;
DEALLOCATE BizDoc_Cursor;

RETURN substring(@vcDescription,2,len(@vcDescription))

end