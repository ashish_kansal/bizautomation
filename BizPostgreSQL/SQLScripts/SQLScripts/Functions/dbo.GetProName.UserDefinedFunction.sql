/****** Object:  UserDefinedFunction [dbo].[Getgetgetgetproname]    Script Date: 07/26/2008 18:13:10 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='Getproname')
DROP FUNCTION Getproname
GO
CREATE FUNCTION [dbo].[Getproname](@numProid numeric(9))
	RETURNS varchar (100) 
AS
BEGIN
declare @Getproname as varchar (100)
select @Getproname=vcProjectName from ProjectsMaster where numOppId=@numProid
	RETURN @Getproname
END
GO
