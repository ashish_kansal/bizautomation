/****** Object:  UserDefinedFunction [dbo].[GetDaydetails]    Script Date: 07/26/2008 18:13:00 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getdaydetails')
DROP FUNCTION getdaydetails
GO
CREATE FUNCTION [dbo].[GetDaydetails](@numUserCntID as numeric(9),@strDate varchar(11),@numCategoryType as numeric(9),@ClientTimeZoneOffset as int)              
returns varchar(1000)              
as              
begin              
            
declare @Time as float   set @time =0          
declare @Expense as float    set @Expense =0          
declare @Leave as varchar(10)              
declare @rtDtl as varchar(30)       
--declare @DateOffset as datetime      
--set @DateOffset = dateadd(minute,-@ClientTimeZoneOffset,convert(datetime,@strDate))      
--set @strDate = convert(varchar(11),@DateOffset)      
if @numCategoryType<>0              
begin             
 select @Time=@Time+isnull(sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60),0)               
 from TimeAndExpense               
 where convert(varchar(10),dateadd(minute,-@ClientTimeZoneOffset,dtFromDate),101)=@strDate   
and numCategory=1 and numUserCntID=@numUserCntID and numType=@numCategoryType             
            
             
end            
else            
begin              
  select @Time=@Time+isnull(sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60),0)               
  from TimeAndExpense               
  where convert(varchar(10),dateadd(minute,-@ClientTimeZoneOffset,dtFromDate),101)=@strDate   
and numCategory=1 and numUserCntID=@numUserCntID               
           
end            
              
if @numCategoryType<>0             
begin              
 select @Expense=@Expense+isnull(convert(varchar,round(sum(monAmount),2))    ,0)          
 from TimeAndExpense               
 where @strDate =   
 convert(varchar(10),dtFromDate,101)  
 and numCategory=2 and numUserCntID=@numUserCntID and numType=@numCategoryType              
        
            
end            
else              
begin            
 select @Expense=@Expense+isnull(round(sum(monAmount),2),0)              
 from TimeAndExpense    where @strDate =             
 convert(varchar(10),dtFromDate,101)  
and numCategory=2 and numUserCntID=@numUserCntID              
             
           
end            
              
if @numCategoryType<>0               
 select @Leave=(Case when ( dtFromDate=@strDate and bitFromFullDay=0 ) then  'HDL'               
 when ( dtFromDate=@strDate and bitFromFullDay=1 ) then  'FDL'               
 when ( dtToDate=@strDate and bitToFullDay=0 ) then  'HDL'               
 when ( dtToDate=@strDate and bitToFullDay=1 ) then  'FDL'               
 when ( (@strDate between dtFromDate and dtToDate) and dtFromDate<>@strDate and dtToDate<>@strDate) then  'FDL'              
 else '-' end)              
 from TimeAndExpense               
 where (@strDate between dtFromDate and dtToDate)  and numCategory=3 and numUserCntID=@numUserCntID and numType=@numCategoryType              
else              
 select @Leave=(Case when ( dtFromDate=@strDate and bitFromFullDay=0 ) then  'HDL'               
 when ( dtFromDate=@strDate and bitFromFullDay=1 ) then  'FDL'               
 when ( dtToDate=@strDate and bitToFullDay=0 ) then  'HDL'               
 when ( dtToDate=@strDate and bitToFullDay=1 ) then  'FDL'               
 when ( (@strDate between dtFromDate and dtToDate) and dtFromDate<>@strDate and dtToDate<>@strDate) then  'FDL'              
 else '-' end)              
 from TimeAndExpense               
 where (@strDate between dtFromDate and dtToDate)  and numCategory=3 and numUserCntID=@numUserCntID              
            
if ((@Time is null or @Time=0) and (@Expense is null or @Expense=0) and @Leave is null) set @rtDtl='-'              
else if (@Time is not null  or @Expense is not null or @Leave is not null)               
begin              
            
            
if @Time is not null  and  @Time <> 0            
 set @rtDtl=@Time            
else             
set @rtDtl=''            
             
if @Expense is not null  and  @Expense <> 0            
 begin              
  if @rtDtl='' set @rtDtl='E'              
        else set @rtDtl=@rtDtl+',E'              
 end              
              
if @Leave is not null               
 begin              
  if @rtDtl='' set @rtDtl='L'              
                else set @rtDtl=@rtDtl+',L'              
 end              
end              
            
if @rtDtl='' set @rtDtl='0'
if @rtDtl='-' set @rtDtl=' '              
return @rtDtl              
              
end
GO
