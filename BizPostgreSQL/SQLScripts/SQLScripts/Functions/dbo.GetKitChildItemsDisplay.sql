GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetKitChildItemsDisplay')
DROP FUNCTION GetKitChildItemsDisplay
GO
CREATE FUNCTION [dbo].[GetKitChildItemsDisplay]
(
	@numItemKitID NUMERIC(18,0)
	,@numChildKitItemID  NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
)    
RETURNS VARCHAR(MAX)
AS    
BEGIN   
	DECLARE @vcDisplayText VARCHAR(MAX)
	DECLARE @vcItemName VARCHAR(300)
	DECLARE @vcSKU VARCHAR(100)
	DECLARE @vcModelID VARCHAR(100)
	
	SELECT @numItemCode=numItemCode,@vcItemName=ISNULL(vcItemName,''),@vcSKU=ISNULL(vcSKU,''),@vcModelID=ISNULL(vcModelID,'') FROM Item WHERE numItemCode=@numItemCode

	DECLARE @tintView TINYINT
	DECLARE @vcFields VARCHAR(1000)
	SELECT @tintView=tintView,@vcFields=ISNULL(vcFields,'') FROM ItemDetails WHERE numItemKitID=@numItemKitID AND numChildItemID=@numChildKitItemID

	IF LEN(@vcFields) > 0
	BEGIN
		IF @tintView = 2
		BEGIN
			SET @vcDisplayText = '<ul class="list-unstyled ul-child-kit-item" style="margin-bottom:0px;padding:10px">'

			IF CHARINDEX(',0~1,',CONCAT(',',@vcFields,','),0) <> 0
			BEGIN
				SET @vcDisplayText = CONCAT(@vcDisplayText,'<li><img class="ul-child-kit-item-image" src="',(CASE 
																WHEN ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1),'') = ''
																THEN '../Images/DefaultProduct.png'
																ELSE CONCAT('##URL##/',(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1))
															END),'" alt="',@vcItemName,'"/></li><li>')
			END
		END
		ELSE
		BEGIN
			SET @vcDisplayText = '<ul class="list-unstyled ul-child-kit-item" style="margin-bottom:0px;padding:10px">'
		END

		IF CHARINDEX(',0~2,',CONCAT(',',@vcFields,','),0) <> 0 AND LEN(@vcItemName) > 0
		BEGIN
			SET @vcDisplayText = CONCAT(@vcDisplayText,'<li class=" ul-child-kit-item-name">',@vcItemName,'</li>')
		END

		IF CHARINDEX(',0~3,',CONCAT(',',@vcFields,','),0) <> 0 AND LEN(@vcSKU) > 0
		BEGIN
			SET @vcDisplayText = CONCAT(@vcDisplayText,'<li>',@vcSKU,'</li>')
		END

		IF CHARINDEX(',0~4',CONCAT(',',@vcFields,','),0) <> 0 AND LEN(@vcModelID) > 0
		BEGIN
			SET @vcDisplayText = CONCAT(@vcDisplayText,'<li>',@vcModelID,'</li>')
		END

		SELECT
			@vcDisplayText = CONCAT(@vcDisplayText,'<li>',dbo.GetCustFldValueItem(CAST(REPLACE(OutParam,'1~','') AS NUMERIC),@numItemCode),'</li>')
		FROM
			dbo.SplitString(@vcFields,',')
		WHERE
			CHARINDEX('1~',OutParam,0) <> 0

		IF CHARINDEX(',0~1,',CONCAT(',',@vcFields,','),0) <> 0 AND @tintView = 1
		BEGIN
			SET @vcDisplayText = CONCAT('<ul class="list-inline ul-child-kit-item"><li><img class="ul-child-kit-item-image" src="',(CASE 
																							WHEN ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1),'') = ''
																							THEN '../Images/DefaultProduct.png'
																							ELSE CONCAT('##URL##/',(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1))
																						END),'" alt="',@vcItemName,'"/></li><li>',@vcDisplayText,'</ul></li></ul>')
		END
		ELSE
		BEGIN
			SET @vcDisplayText = CONCAT(@vcDisplayText,'</ul>')
		END
	END
	ELSE
	BEGIN
		SET @vcDisplayText = CONCAT(@vcItemName,' (Item Code:',@numItemCode,(CASE WHEN LEN(@vcSKU) > 0 THEN CONCAT(', SKU:',@vcSKU) ELSE '' END),')')
	END

	RETURN @vcDisplayText
END
GO