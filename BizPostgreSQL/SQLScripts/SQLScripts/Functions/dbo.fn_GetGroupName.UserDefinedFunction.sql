/****** Object:  UserDefinedFunction [dbo].[fn_GetGroupName]    Script Date: 07/26/2008 18:12:37 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getgroupname')
DROP FUNCTION fn_getgroupname
GO
CREATE FUNCTION [dbo].[fn_GetGroupName](@numGrpID numeric)
Returns varchar(100) 
As
BEGIN
	DECLARE @RetName Varchar(20)
	set @RetName=''
	select @RetName=vcgrpName from Groups  where numGrpID=@numGrpID
	RETURN @RetName
END
GO
