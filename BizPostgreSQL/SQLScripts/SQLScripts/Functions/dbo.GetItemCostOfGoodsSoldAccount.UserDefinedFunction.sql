/****** Object:  UserDefinedFunction [dbo].[GetItemCostOfGoodsSoldAccount]    Script Date: 07/26/2008 18:13:04 ******/

GO

GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getitemcostofgoodssoldaccount')
DROP FUNCTION getitemcostofgoodssoldaccount
GO
CREATE FUNCTION [dbo].[GetItemCostOfGoodsSoldAccount](@numItemCode numeric(9))      
 RETURNS numeric(9)     
AS      
BEGIN      
declare @numCOGSChartAcntId as  numeric(9)    
      
select @numCOGSChartAcntId=isnull(numCOGsChartAcntId,'0') from Item where numItemCode=@numItemCode         
      
      
return isnull(@numCOGSChartAcntId,'0')      
END
GO
