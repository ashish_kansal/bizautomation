/****** Object:  UserDefinedFunction [dbo].[GetDaydetails]    Script Date: 07/26/2008 18:13:00 ******/

GO

GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'fn'
                    AND NAME = 'GetDayDetailsForDomain' ) 
    DROP FUNCTION GetDayDetailsForDomain
GO
CREATE FUNCTION [dbo].[GetDayDetailsForDomain]
    (
      @numDomainID AS NUMERIC(9),
      @strDate VARCHAR(11),
      @numCategoryType AS NUMERIC(9),
      @ClientTimeZoneOffset AS INT
    )
RETURNS VARCHAR(1000)
AS BEGIN              
            
    DECLARE @Time AS FLOAT
    SET @time = 0          
    DECLARE @Expense AS FLOAT
    SET @Expense = 0          
    DECLARE @Leave AS VARCHAR(10)              
    DECLARE @rtDtl AS VARCHAR(30)       

    IF @numCategoryType <> 0 
        BEGIN             
            SELECT  @Time = @Time + ISNULL(SUM(CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate))
                                               / 60), 0)
            FROM    TimeAndExpense
            WHERE   CONVERT(VARCHAR(10), DATEADD(minute,
                                                 -@ClientTimeZoneOffset,
                                                 dtFromDate), 101) = @strDate
                    AND numCategory = 1
                    AND numDomainID = @numDomainID
                    AND numType = @numCategoryType             
            
             
        END            
    ELSE 
        BEGIN              
            SELECT  @Time = @Time + ISNULL(SUM(CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate))
                                               / 60), 0)
            FROM    TimeAndExpense
            WHERE   CONVERT(VARCHAR(10), DATEADD(minute,
                                                 -@ClientTimeZoneOffset,
                                                 dtFromDate), 101) = @strDate
                    AND numCategory = 1
                    AND numDomainID = @numDomainID               
           
        END            
              
    IF @numCategoryType <> 0 
        BEGIN              
            SELECT  @Expense = @Expense
                    + ISNULL(CONVERT(VARCHAR, ROUND(SUM(monAmount), 2)), 0)
            FROM    TimeAndExpense
            WHERE   @strDate = CONVERT(VARCHAR(10), dtFromDate, 101)
                    AND numCategory = 2
                    AND numDomainID = @numDomainID
                    AND numType = @numCategoryType              
        
            
        END            
    ELSE 
        BEGIN            
            SELECT  @Expense = @Expense + ISNULL(ROUND(SUM(monAmount), 2), 0)
            FROM    TimeAndExpense
            WHERE   @strDate = CONVERT(VARCHAR(10), dtFromDate, 101)
                    AND numCategory = 2
                    AND numDomainID = @numDomainID              
             
           
        END            
              
    IF @numCategoryType <> 0 
        SELECT  @Leave = ( CASE WHEN ( dtFromDate = @strDate
                                       AND bitFromFullDay = 0
                                     ) THEN 'HDL'
                                WHEN ( dtFromDate = @strDate
                                       AND bitFromFullDay = 1
                                     ) THEN 'FDL'
                                WHEN ( dtToDate = @strDate
                                       AND bitToFullDay = 0
                                     ) THEN 'HDL'
                                WHEN ( dtToDate = @strDate
                                       AND bitToFullDay = 1
                                     ) THEN 'FDL'
                                WHEN ( ( @strDate BETWEEN dtFromDate AND dtToDate )
                                       AND dtFromDate <> @strDate
                                       AND dtToDate <> @strDate
                                     ) THEN 'FDL'
                                ELSE '-'
                           END )
        FROM    TimeAndExpense
        WHERE   ( @strDate BETWEEN dtFromDate AND dtToDate )
                AND numCategory = 3
                AND numDomainID = @numDomainID
                AND numType = @numCategoryType              
    ELSE 
        SELECT  @Leave = ( CASE WHEN ( dtFromDate = @strDate
                                       AND bitFromFullDay = 0
                                     ) THEN 'HDL'
                                WHEN ( dtFromDate = @strDate
                                       AND bitFromFullDay = 1
                                     ) THEN 'FDL'
                                WHEN ( dtToDate = @strDate
                                       AND bitToFullDay = 0
                                     ) THEN 'HDL'
                                WHEN ( dtToDate = @strDate
                                       AND bitToFullDay = 1
                                     ) THEN 'FDL'
                                WHEN ( ( @strDate BETWEEN dtFromDate AND dtToDate )
                                       AND dtFromDate <> @strDate
                                       AND dtToDate <> @strDate
                                     ) THEN 'FDL'
                                ELSE '-'
                           END )
        FROM    TimeAndExpense
        WHERE   ( @strDate BETWEEN dtFromDate AND dtToDate )
                AND numCategory = 3
                AND numDomainID = @numDomainID              
            
    IF ( ( @Time IS NULL
           OR @Time = 0
         )
         AND ( @Expense IS NULL
               OR @Expense = 0
             )
         AND @Leave IS NULL
       ) 
        SET @rtDtl = '-'              
    ELSE 
        IF ( @Time IS NOT NULL
             OR @Expense IS NOT NULL
             OR @Leave IS NOT NULL
           ) 
            BEGIN              
            
            
                IF @Time IS NOT NULL
                    AND @Time <> 0 
                    SET @rtDtl = @Time            
                ELSE 
                    SET @rtDtl = ''            
             
                IF @Expense IS NOT NULL
                    AND @Expense <> 0 
                    BEGIN              
                        IF @rtDtl = '' 
                            SET @rtDtl = 'E'              
                        ELSE 
                            SET @rtDtl = @rtDtl + ',E'              
                    END              
              
                IF @Leave IS NOT NULL 
                    BEGIN              
                        IF @rtDtl = '' 
                            SET @rtDtl = 'L'              
                        ELSE 
                            SET @rtDtl = @rtDtl + ',L'              
                    END              
            END              
            
    IF @rtDtl = '' SET @rtDtl = '0'              
    IF @rtDtl='-' SET @rtDtl=' '                  
    RETURN @rtDtl              
              
   END
GO
