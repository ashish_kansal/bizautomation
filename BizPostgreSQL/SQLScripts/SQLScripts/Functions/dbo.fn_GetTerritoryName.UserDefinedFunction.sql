GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getterritoryname')
DROP FUNCTION fn_getterritoryname
GO
CREATE FUNCTION [dbo].[fn_GetTerritoryName](@numTerID numeric)  
Returns varchar(100)   
As  
BEGIN  
 declare @TerrName as varchar(100)  
  
 Select @TerrName=isnull(vcData,'') from ListDetails where numListItemID=@numTerID  
  
return @TerrName  
END  