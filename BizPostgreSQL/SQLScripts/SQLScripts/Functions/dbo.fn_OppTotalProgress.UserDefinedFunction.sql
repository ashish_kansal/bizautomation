/****** Object:  UserDefinedFunction [dbo].[fn_OppTotalProgress]    Script Date: 07/26/2008 18:12:49 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_opptotalprogress')
DROP FUNCTION fn_opptotalprogress
GO
CREATE FUNCTION [dbo].[fn_OppTotalProgress] (@numOppId  as numeric)
returns varchar(10)
as 
begin

declare @Percentage as decimal(10,0)

select @Percentage=sum(tintPercentage) from OpportunityStageDetails 
where numOppId=@numOppId and bitStageCompleted=1

return convert(varchar(10),@Percentage)+'%'

End
GO
