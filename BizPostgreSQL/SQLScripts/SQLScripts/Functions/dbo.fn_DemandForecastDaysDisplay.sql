GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_DemandForecastDaysDisplay')
DROP FUNCTION fn_DemandForecastDaysDisplay
GO
CREATE FUNCTION [dbo].[fn_DemandForecastDaysDisplay]
(
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numItemCode NUMERIC(18,0)
	,@numLeadDays NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@numForecastDays NUMERIC(18,0)
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@tintDemandPlanBasedOn TINYINT
)    
RETURNS VARCHAR(MAX)
AS    
BEGIN   
	DECLARE @vcResult VARCHAR(1000) = ''


	DECLARE @fltDemandBasedOnReleaseDate FLOAT = 0
	DECLARE @fltDemandBasedOnHistoricalSales FLOAT = 0
	DECLARE @fltDemandBasedOnOpportunity FLOAT = 0
	DECLARE @dtFromDate DATETIME
	DECLARE @dtToDate DATETIME
	DECLARE @numAnalysisDays INT = 0

	SET @fltDemandBasedOnReleaseDate = ISNULL((SELECT
												SUM(numUnitHour - ISNULL(numQtyShipped,0))
											FROM
												OpportunityMaster OM
											INNER JOIN
												OpportunityItems OI
											ON
												OM.numOppId=OI.numOppId
											WHERE
												OM.numDomainId=@numDomainID
												AND OM.tintOppType=1
												AND OM.tintOppStatus=1
												AND ISNULL(OM.tintshipped,0)=0
												AND (ISNULL(OI.numUnitHour,0) - ISNULL(numQtyShipped,0)) > 0
												AND OI.numItemCode=@numItemCode
												AND OI.numWarehouseItmsID=@numWarehouseItemID
												AND ISNULL(bitDropship,0) = 0
												AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
												AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)
	

	-- KIT CHILD ITEMS
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0))
												FROM
													OpportunityMaster OM
												INNER JOIN
													OpportunityItems OI
												ON
													OM.numOppId = OI.numOppId
												INNER JOIN
													OpportunityKitItems OKI
												ON
													OI.numoppitemtCode=OKI.numOppItemID
												WHERE
													OM.numDomainId=@numDomainID
													AND OM.tintOppType=1
													AND OM.tintOppStatus=1
													AND ISNULL(OM.tintshipped,0)=0
													AND OKI.numChildItemID=@numItemCode
													AND OKI.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(OI.bitWorkOrder,0) = 0
													AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
													AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

	-- KIT CHILD ITEMS OF CHILD KITS
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0))
												FROM
													OpportunityMaster OM
												INNER JOIN
													OpportunityItems OI
												ON
													OM.numOppId=OI.numOppId
												INNER JOIN
													OpportunityKitChildItems OKCI
												ON
													OI.numoppitemtCode=OKCI.numOppItemID
												WHERE
													OM.numDomainId=@numDomainID
													AND OM.tintOppType=1
													AND OM.tintOppStatus=1
													AND ISNULL(OM.tintshipped,0)=0
													AND OKCI.numItemID=@numItemCode
													AND OKCI.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(OI.bitWorkOrder,0) = 0
													AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
													AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

	--ITEMS USED IN WORK ORDER
	SET @fltDemandBasedOnReleaseDate = ISNULL(@fltDemandBasedOnReleaseDate,0) 
										+ ISNULL((SELECT
													SUM(WOD.numQtyItemsReq)
												FROM
													WorkOrderDetails WOD
												INNER JOIN
													WorkOrder WO
												ON
													WOD.numWOId=WO.numWOId
												LEFT JOIN
													OpportunityItems OI
												ON
													WO.numOppItemID = OI.numoppitemtCode
												LEFT JOIN
													OpportunityMaster OM
												ON
													OI.numOppId=OM.numOppId
												WHERE
													WO.numDomainID=@numDomainID
													AND WO.numWOStatus <> 23184 -- NOT COMPLETED
													AND WOD.numChildItemID = @numItemCode
													AND WOD.numWareHouseItemId=@numWarehouseItemID
													AND ISNULL(WOD.numQtyItemsReq,0) > 0
													AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
													AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
													AND 1 = (CASE 
															WHEN OI.numoppitemtCode IS NOT NULL 
															THEN (CASE 
																	WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
																	THEN 1 
																	ELSE 0 
																END)
															ELSE 
																(CASE 
																WHEN WO.bintCompliationDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
																THEN 1 
																ELSE 0 
																END)
															END)),0)


	IF ISNULL(@bitShowHistoricSales,0) = 1
	BEGIN
		-- Get Days of Historical Analysis
		SELECT @numAnalysisDays=numDays FROM DemandForecastAnalysisPattern WHERE numDFAPID = @numHistoricalAnalysisPattern

		IF ISNULL(@bitBasedOnLastYear,0) = 1 --last week from last year
		BEGIN
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETUTCDATE()))
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETUTCDATE()))
		END
		ELSE
		BEGIN
			SET @dtToDate = DATEADD(d,-1,GETUTCDATE())
			SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETUTCDATE())
		END

		SET @fltDemandBasedOnHistoricalSales = ISNULL((SELECT
															SUM(numUnitHour)
														FROM
															OpportunityMaster OM
														INNER JOIN
															OpportunityItems OI	
														ON
															OM.numOppId=OI.numOppId
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND numItemCode=@numItemCode
															AND numWarehouseItmsID=@numWarehouseItemID
															AND ISNULL(OI.numUnitHour,0) > 0
															AND ISNULL(bitDropship,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)
		

		-- KIT CHILD ITEMS
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(OKI.numQtyItemsReq)
														FROM
															OpportunityMaster OM
														INNER JOIN
															OpportunityItems OI
														ON
															OM.numOppId=OI.numOppId
														INNER JOIN
															OpportunityKitItems OKI
														ON
															OI.numoppitemtCode=OKI.numOppItemID
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND OKI.numChildItemID=@numItemCode
															AND OKI.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(OKI.numQtyItemsReq,0) > 0
															AND ISNULL(OI.bitWorkOrder,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)
		

		-- KIT CHILD ITEMS OF CHILD KITS
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(OKCI.numQtyItemsReq)
														FROM
															OpportunityMaster OM
														INNER JOIN
															OpportunityItems OI
														ON
															OM.numOppId=OI.numOppId
														INNER JOIN
															OpportunityKitChildItems OKCI
														ON
															OI.numoppitemtCode=OKCI.numOppItemID
														WHERE
															OM.numDomainId=@numDomainID
															AND OM.tintOppType=1
															AND OM.tintOppStatus=1
															AND OKCI.numItemID=@numItemCode
															AND OKCI.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(OKCI.numQtyItemsReq,0) > 0
															AND ISNULL(OI.bitWorkOrder,0) = 0
															AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)

		--ITEMS USED IN WORK ORDER
		SET @fltDemandBasedOnHistoricalSales = ISNULL(@fltDemandBasedOnHistoricalSales,0) 
												+ ISNULL((SELECT
															SUM(WOD.numQtyItemsReq)
														FROM
															WorkOrderDetails WOD
														INNER JOIN
															WorkOrder WO
														ON
															WOD.numWOId=WO.numWOId
														LEFT JOIN
															OpportunityItems OI
														ON
															WO.numOppItemID = OI.numoppitemtCode
														LEFT JOIN
															OpportunityMaster OM
														ON
															OI.numOppId=OM.numOppId
														WHERE
															WO.numDomainID=@numDomainID
															AND WOD.numChildItemID = @numItemCode
															AND WOD.numWareHouseItemId=@numWarehouseItemID
															AND ISNULL(WOD.numQtyItemsReq,0) > 0
															AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)),0)

		SET @fltDemandBasedOnHistoricalSales = (@fltDemandBasedOnHistoricalSales/@numAnalysisDays) * @numForecastDays
	END

	IF ISNULL(@bitIncludeOpportunity,0) = 1
	BEGIN
		SET @fltDemandBasedOnOpportunity = ISNULL((SELECT
														SUM(OI.numUnitHour * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OM.numOppId=OI.numOppId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND numItemCode=@numItemCode
														AND numWarehouseItmsID=@numWarehouseItemID
														AND ISNULL(bitDropship,0) = 0
														AND (ISNULL(OI.numUnitHour,0) * (PP.intTotalProgress/100)) > 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >=  @numOpportunityPercentComplete
												),0)

		-- KIT CHILD ITEMS
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(OKI.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OM.numOppId=OI.numOppId
													INNER JOIN
														OpportunityKitItems OKI
													ON
														OI.numoppitemtCode=OKI.numOppItemID
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND OKI.numChildItemID=@numItemCode
														AND OKI.numWareHouseItemId=@numWarehouseItemID
														AND ISNULL(OI.bitWorkOrder,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND (ISNULL(OKI.numQtyItemsReq,0) * (PP.intTotalProgress/100)) > 0
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)
		

		-- KIT CHILD ITEMS OF CHILD KITS
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(OKCI.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OM.numOppId=OI.numOppId
													INNER JOIN
														OpportunityKitChildItems OKCI
													ON
														OI.numoppitemtCode=OKCI.numOppItemID
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.numDomainId=@numDomainID
														AND OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND OKCI.numItemID=@numItemCode
														AND OKCI.numWareHouseItemId=@numWarehouseItemID
														AND ISNULL(OI.bitWorkOrder,0) = 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
														AND (ISNULL(OKCI.numQtyItemsReq,0) * (PP.intTotalProgress/100)) > 0
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)

		--ITEMS USED IN WORK ORDER
		SET @fltDemandBasedOnOpportunity = ISNULL(@fltDemandBasedOnOpportunity,0) 
											+ ISNULL((SELECT
														SUM(WOD.numQtyItemsReq * (PP.intTotalProgress/100))
													FROM
														OpportunityMaster OM
													INNER JOIN
														OpportunityItems OI
													ON
														OI.numOppId=OM.numOppId
													INNER JOIN
														WorkOrder WO
													ON
														OI.numoppitemtCode = WO.numOppItemID
													INNER JOIN
														WorkOrderDetails WOD
													ON
														WO.numWOId=WOD.numWOId
													INNER JOIN
														ProjectProgress PP
													ON
														OM.numOppId = PP.numOppId
													WHERE
														OM.tintOppType=1
														AND OM.tintOppStatus=0
														AND WO.numDomainID=@numDomainID
														AND WO.numWOStatus <> 23184 -- NOT COMPLETED
														AND WOD.numChildItemID = @numItemCode
														AND WOD.numWareHouseItemId=@numWarehouseItemID
														AND (ISNULL(WOD.numQtyItemsReq,0) * (PP.intTotalProgress/100)) > 0
														AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
														AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
														AND PP.intTotalProgress >= @numOpportunityPercentComplete),0)
	END


	IF @tintDemandPlanBasedOn = 2
	BEGIN
		DECLARE @numBackOrder FLOAT
		DECLARE @numExpectedQty FLOAT

		SET @numBackOrder = ISNULL((SELECT numBackOrder FROM WareHouseItems	WHERE numWareHouseItemID=@numWarehouseItemID),0)
		SET @numExpectedQty = ISNULL((SELECT 
										SUM(OI.numUnitHour) 
									FROM 
										OpportunityMaster OM 
									INNER JOIN 
										OpportunityItems OI 
									ON 
										OM.numOppId=OI.numOppId 
									WHERE 
										OM.tintOppType=2 
										AND OM.tintOppStatus=1 
										AND numItemCode=@numItemCode
										AND numWarehouseItmsID=@numWarehouseItemID 
										AND OI.ItemRequiredDate IS NOT NULL 
										AND OI.ItemRequiredDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))),0)

		IF @numBackOrder > @numExpectedQty
		BEGIN
			SET @numBackOrder = @numBackOrder - @numExpectedQty
			SET @numExpectedQty = 0

			SET @vcResult = CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',@numForecastDays,'" onclick="return OpenDFRecords(1,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,0)"><span class="badge bg-red">',@numBackOrder * -1,'</span></a><input id="hRelease',@numForecastDays,'" type="hidden" value="',@fltDemandBasedOnReleaseDate,'"></input></li>',(CASE WHEN @bitShowHistoricSales=1 THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',',@numAnalysisDays,',',ISNULL(@bitBasedOnLastYear,0),',0)"><span class="badge bg-yellow">',@fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN @bitIncludeOpportunity=1 THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,',@numOpportunityPercentComplete,''')"><span class="badge bg-green">',@fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>')
		END
		ELSE
		BEGIN
			SET @numExpectedQty = @numExpectedQty - @numBackOrder
			SET @vcResult = CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',@numForecastDays,'" onclick="return OpenDFRecords(1,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,0)"><span class="badge bg-green">',@numExpectedQty,'</span></a><input id="hRelease',@numForecastDays,'" type="hidden" value="',@fltDemandBasedOnReleaseDate,'"></input></li>',(CASE WHEN @bitShowHistoricSales=1 THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',',@numAnalysisDays,',',ISNULL(@bitBasedOnLastYear,0),',0)"><span class="badge bg-yellow">',@fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN @bitIncludeOpportunity=1 THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,',@numOpportunityPercentComplete,''')"><span class="badge bg-green">',@fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>')
		END
	END
	ELSE
	BEGIN
		SET @vcResult = CONCAT('<ul class=''list-inline list-days''><li><a href="#" id="aRelease',@numForecastDays,'" onclick="return OpenDFRecords(1,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,0)"><span class="badge ',CASE WHEN @fltDemandBasedOnReleaseDate > 0 THEN 'bg-red'  ELSE 'bg-light-blue' END,'">',CASE WHEN @fltDemandBasedOnReleaseDate > 0 THEN @fltDemandBasedOnReleaseDate * -1 ELSE @fltDemandBasedOnReleaseDate END,'</span></a><input id="hRelease',@numForecastDays,'" type="hidden" value="',@fltDemandBasedOnReleaseDate,'"></input></li>',(CASE WHEN @bitShowHistoricSales=1 THEN CONCAT('<li><a href="#" style="color:#8faadc" onclick="return OpenDFRecords(2,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',',@numAnalysisDays,',',ISNULL(@bitBasedOnLastYear,0),',0)"><span class="badge bg-yellow">',@fltDemandBasedOnHistoricalSales,'</span></a></li>') ELSE '' END),(CASE WHEN @bitIncludeOpportunity=1 THEN CONCAT('<li><a href="#" style="color:#a9d18e" onclick="return OpenDFRecords(3,',@numForecastDays,',',@numItemCode,',',@numWarehouseItemID,',0,0,',@numOpportunityPercentComplete,''')"><span class="badge bg-green">',@fltDemandBasedOnOpportunity,'</span></a></li>') ELSE '' END),'</ul>')
	END

	RETURN @vcResult
END
GO