/****** Object:  UserDefinedFunction [dbo].[fn_GetSlpId]    Script Date: 07/26/2008 18:12:44 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getslpid')
DROP FUNCTION fn_getslpid
GO
CREATE FUNCTION [dbo].[fn_GetSlpId](  
@numOppId numeric)  
returns numeric  
Begin  
Declare @retval numeric  
select @retval = slp_id from stagePercentageDetails where numStageDetailsid  
 = (Select top 1 numStageDetailsid from opportunitystagedetails where  
 numOppid = @numOppID)  
Return @retval  
End
GO
