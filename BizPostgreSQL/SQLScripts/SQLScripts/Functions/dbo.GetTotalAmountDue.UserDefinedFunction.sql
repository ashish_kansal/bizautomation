
GO
/****** Object:  UserDefinedFunction [dbo].[GetTotalAmountDue]    Script Date: 04/18/2009 20:22:59 ******/

GO

GO
--Created By Siva                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='gettotalamountdue')
DROP FUNCTION gettotalamountdue
GO
CREATE FUNCTION [dbo].[GetTotalAmountDue](@numUserId numeric,@dtStartDate as datetime,@dtEndDate as datetime,@numUserCntId numeric,@numDomainId numeric,@ClientTimeZoneOffset int )                        
returns DECIMAL(20,5)                        
As                        
Begin                        
  Declare @fltRegularWork as float                        
  Declare @fltOverTimeWork as float                        
  Declare @fltLimDailHrs as float                        
  Declare @bitOverTime as bit                       
  Declare @bitOverTimeHrsDailyOrWeekly as bit                     
  Declare @monOverTimeRate as DECIMAL(20,5)                        
  Declare @monOverTimeAmt as DECIMAL(20,5)                        
  Declare @monCommissionAmt as DECIMAL(20,5)                        
  Declare @monTotalAmtDue as DECIMAL(20,5)                        
  Declare @monTotalExpenses as DECIMAL(20,5)                    
Declare @monHourlyRate as DECIMAL(20,5)                       
   set @monHourlyRate= 0                  
set @monTotalExpenses=0    
   Select @monHourlyRate=isnull(monHourlyRate,0), @fltLimDailHrs=isnull(numLimDailHrs,0),@bitOverTime=isnull(bitOverTime,0),@bitOverTimeHrsDailyOrWeekly=isnull(bitOverTimeHrsDailyOrWeekly,0),                  
@monOverTimeRate=isnull(monOverTimeRate,0) From UserMaster Where numUserId=@numUserId                        
----                   
---- Select  @fltRegularWork=Sum(Cast(datediff(minute,dtfromdate,dttodate) as float) *@monHourlyRate/Cast(60 as float))                   
----from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1  And numUserCntID=@numUserCntId And numDomainID=@numDomainId                        
 select @monTotalExpenses= isnull((Sum(Cast(monAmount as float))),0)     
 from timeandexpense where bitReimburse=1 and  numCategory=2          
 And numType in (1,2) And numUserCntID=@numUserCntId And numDomainID=@numDomainId            
  and   dtFromDate between   @dtStartDate and    @dtEndDate         
    
  
-- Set @monTotalExpenses = dbo.GetTotalExpense(@numUserCntId,@numDomainId,@dtStartDate,@dtEndDate)                  
  Set @fltRegularWork= dbo.fn_GetTotalHrs(@bitOverTime,@bitOverTimeHrsDailyOrWeekly,@fltLimDailHrs,@dtStartDate,@dtEndDate,@numUserCntId,@numDomainId,0,@ClientTimeZoneOffset)                
  --print @fltRegularWork                        
                          
  Set @fltOverTimeWork= Cast(dbo.GetOverHrsWorked(@bitOverTime,@bitOverTimeHrsDailyOrWeekly,@fltLimDailHrs,@dtStartDate,@dtEndDate,@numUserCntId,@numDomainId,@ClientTimeZoneOffset) as float)                         
  if @bitOverTime=1 and @monOverTimeRate <>0                        
     Set @monOverTimeAmt = @fltOverTimeWork*@monOverTimeRate                        
  Set @monCommissionAmt=dbo.[GetCommissionAmountOfDuration](@numUserId,@numUserCntId,@numDomainId,@dtStartDate,@dtEndDate,0)
     Set @monTotalAmtDue=(@fltRegularWork*@monHourlyRate)+isnull(@monOverTimeAmt,0)+isnull(@monCommissionAmt,0) + isnull(@monTotalExpenses,0)                      
  --print @fltOverTimeWork                        
  return @monTotalAmtDue                        
End
