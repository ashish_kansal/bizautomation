
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetEmailAlert]    Script Date: 11/02/2011 16:15:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetEmailAlert')
DROP FUNCTION fn_GetEmailAlert
GO
CREATE  FUNCTION [dbo].[fn_GetEmailAlert]
(
	@numDomainId NUMERIC,
	@numContactId NUMERIC,
	@numUserContactId NUMERIC 
)returns @tbl table (numAlert NUMERIC ,vcMsg VARCHAR(50))
AS
BEGIN 
DECLARE @bitOpenActionItem  BIT
DECLARE @vcOpenActionMsg VARCHAR(50)
DECLARE @bitOpencases BIT
DECLARE @vcOpenCasesMsg VARCHAR(50)
DECLARE @bitOpenProject BIT
DECLARE @vcOpenProjectMsg VARCHAR(50)
DECLARE @bitOpenSalesOpp BIT
DECLARE @vcOpenSalesOppMsg VARCHAR(50)
DECLARE @bitOpenPurchaseOpp BIT
DECLARE @vcOpenPurchaseOppMsg VARCHAR(50)
DECLARE @bitBalancedue BIT
DECLARE @vcBalancedueMsg VARCHAR(50)
DECLARE @monBalancedue DECIMAL(20,5) 
DECLARE @bitARAccount BIT
DECLARE @vcARAccountMsg VARCHAR(50)
DECLARE @bitAPAccount BIT
DECLARE @vcAPAccountMsg VARCHAR(50)
DECLARE @bitUnreadEMail BIT
DECLARE @intUnreadEmail INT
DECLARE @vcUnreadEmailMsg VARCHAR(50)
DECLARE @bitPurchasedPast BIT
DECLARE @monPurchasedPast DECIMAL(20,5) 
DECLARE @vcPurchasedPastMsg VARCHAR(50)
DECLARE @bitSoldPast BIT
DECLARE @monSoldPast DECIMAL(20,5) 
DECLARE @vcSoldPastMsg VARCHAR(50)
DECLARE @bitCustomField BIT
DECLARE @numCustomFieldId NUMERIC 
DECLARE @vcCustomFieldValue VARCHAR(50)
DECLARE @vcCustomfieldMsg VARCHAR(50)
DECLARE @numCount NUMERIC 
DECLARE @strMsg VARCHAR(50)
SET @numCount = 0
DECLARE @numDivisionID NUMERIC 
SELECT 
      @bitOpenActionItem = ISNULL(bitOpenActionItem,0)
      ,@vcOpenActionMsg =[vcOpenActionMsg]
      ,@bitOpencases = ISNULL(bitOpenCases,0)
      ,@vcOpenCasesMsg =[vcOpenCasesMsg]
      ,@bitOpenProject =ISNULL([bitOpenProject],0)
      ,@vcOpenProjectMsg = [vcOpenProjectMsg]
      ,@bitOpenSalesOpp = ISNULL([bitOpenSalesOpp],0)
      ,@vcOpenSalesOppMsg= [vcOpenSalesOppMsg]
      ,@bitOpenPurchaseOpp =ISNULL([bitOpenPurchaseOpp],0)
      ,@vcOpenPurchaseOppMsg =[vcOpenPurchaseOppMsg]
      ,@bitBalancedue = ISNULL([bitBalancedue],0)
      ,@monBalancedue=[monBalancedue]
      ,@vcBalancedueMsg =[vcBalancedueMsg]
      ,@bitARAccount =ISNULL([bitARAccount],0)
      ,@vcARAccountMsg = [vcARAccountMsg]
      ,@bitAPAccount = ISNULL([bitAPAccount],0)
      ,@vcAPAccountMsg = [vcAPAccountMsg]
      ,@bitUnreadEmail = ISNULL([bitUnreadEmail],0)
      ,@intUnreadEmail =[intUnreadEmail]
      ,@vcUnreadEmailMsg =[vcUnreadEmailMsg]
      ,@bitPurchasedPast = ISNULL([bitPurchasedPast],0)
      ,@monPurchasedPast =[monPurchasedPast]
      ,@vcPurchasedPastMsg = [vcPurchasedPastMsg]
      ,@bitSoldPast = ISNULL([bitSoldPast],0)
      ,@monSoldPast = [monSoldPast]
      ,@vcSoldPastMsg = [vcSoldPastMsg]
      ,@bitCustomField = ISNULL([bitCustomField],0)
      ,@numCustomFieldId = [numCustomFieldId]
      ,@vcCustomFieldValue = [vcCustomFieldValue]
      ,@vcCustomFieldMsg = [vcCustomFieldMsg]
      FROM AlertConfig  WHERE AlertConfig.numDomainId = @numDomainId AND AlertConfig.numContactId = @numUserContactId
      
      
      --TAKE divisionID
     
     DECLARE @OpenActionItemCount AS NUMERIC,@OpenCaseCount AS NUMERIC ,@OpenProjectCount AS NUMERIC ,@OpenSalesOppCount AS NUMERIC ,@OpenPurchaseOppCount AS NUMERIC ,
     @TotalBalanceDue AS NUMERIC ,@UnreadEmailCount AS NUMERIC ,@PastPurchaseOrderValue AS NUMERIC ,@PastSalesOrderValue AS NUMERIC 
      
     SET  @numDivisionID = (SELECT numDivisionId FROM dbo.AdditionalContactsInformation WHERE numContactId = @numContactId)
      
      SELECT @OpenCaseCount=CASE WHEN @bitOpencases=1 THEN SUM(OpenCaseCount) ELSE 0 END,
			 @OpenProjectCount=CASE WHEN @bitOpenProject=1 THEN SUM(OpenProjectCount) ELSE 0 END,
			 @OpenSalesOppCount=CASE WHEN @bitOpenSalesOpp=1 THEN SUM(OpenSalesOppCount) ELSE 0 END,
			 @OpenPurchaseOppCount=CASE WHEN @bitOpenPurchaseOpp=1 THEN SUM(OpenPurchaseOppCount) ELSE 0 END,
			 @TotalBalanceDue=CASE WHEN @bitBalancedue=1 THEN SUM(TotalBalanceDue) ELSE 0 END,
			 @UnreadEmailCount=CASE WHEN @bitUnreadEmail=1 THEN SUM(UnreadEmailCount) ELSE 0 END,
			 @PastPurchaseOrderValue=CASE WHEN @bitPurchasedPast=1 THEN SUM(PastPurchaseOrderValue) ELSE 0 END,
			 @PastSalesOrderValue=CASE WHEN @bitSoldPast=1 THEN SUM(PastSalesOrderValue) ELSE 0 END,
			 @OpenActionItemCount=SUM(CASE WHEN @bitOpenActionItem=1 THEN CASE WHEN v.numContactId=@numContactId THEN 1 ELSE 0 END ELSE 0 END)  
			FROM VIEW_Email_Alert_Config v WHERE v.numDomainid =@numDomainId AND v.numDivisionID = @numDivisionID
      
      IF @bitOpenActionItem = 1   
      BEGIN 
		if @OpenActionItemCount > 0
			BEGIN
				SET @numCount = @numCount + 1
				SET @strMsg = @vcOpenActionMsg
			END 
		END	
      IF @bitOpencases = 1 
      BEGIN 
		if @OpenCaseCount > 0	
			BEGIN	
				SET @numCount = @numCount + 1
				SET @strMsg = @vcOpenCasesMsg
			END
	  END 
     IF @bitOpenProject = 1 
	 BEGIN
		IF @OpenProjectCount > 0
			 BEGIN 
				SET @numCount = @numCount + 1
				SET @strMsg = @vcOpenProjectMsg
			 END 
	 END
     IF @bitOpenSalesOpp = 1 
	 BEGIN
		IF @OpenSalesOppCount > 0
			 BEGIN 
      			SET @numCount = @numCount + 1
				SET @strMsg = @vcOpenSalesOppMsg
			 END 
		END
      IF @bitOpenPurchaseOpp = 1 
	  BEGIN
		IF @OpenPurchaseOppCount > 0
			BEGIN 
      			SET @numCount = @numCount + 1
				SET @strMsg = @vcOpenPurchaseOppMsg
			END 
	  END
      IF @bitBalancedue = 1 
	  BEGIN	
		IF @TotalBalanceDue > @monBalancedue
			BEGIN 
      			SET @numCount = @numCount + 1
				SET @strMsg = @vcBalancedueMsg
			END 
	  END 
      IF @bitUnreadEmail = 1 
	  BEGIN
		IF @UnreadEmailCount >= @intUnreadEmail
			 BEGIN 
      			SET @numCount = @numCount + 1
				SET @strMsg = @vcUnreadEmailMsg
			 END 
	  END
      IF @bitPurchasedPast = 1 
	  BEGIN
		IF @PastPurchaseOrderValue >= @monPurchasedPast
			BEGIN 
      			SET @numCount = @numCount + 1
				SET @strMsg = @vcPurchasedPastMsg
			END 
		END
      IF @bitSoldPast = 1 
	  BEGIN
		IF @PastSalesOrderValue >= @monSoldPast
			BEGIN 
      			SET @numCount = @numCount + 1
				SET @strMsg = @vcSoldPastMsg
			END
	  END 
      IF @numCount > 1
      BEGIN
	  	SET @strMsg = 'Alert'
	  END
      INSERT INTO @tbl SELECT @numCount ,@strMsg 
      RETURN 
      END 
      
      ---SELECT * FROM VIEW_Email_Alert_Config