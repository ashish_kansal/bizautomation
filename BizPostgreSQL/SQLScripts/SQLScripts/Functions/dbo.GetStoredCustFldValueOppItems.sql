GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetStoredCustFldValueOppItems')
DROP FUNCTION GetStoredCustFldValueOppItems
GO
CREATE FUNCTION GetStoredCustFldValueOppItems
(
 @numFldId NUMERIC(18,0),
 @numRecordId NUMERIC(18,0),
 @numItemCode NUMERIC(18,0)
)
RETURNS VARCHAR(MAX) 
AS
BEGIN
	DECLARE @vcValue AS VARCHAR(MAX) = ''
       
	IF EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE Fld_ID = @numFldId  AND RecId = @numRecordId)
	BEGIN
		SELECT 
			@vcValue = ISNULL(Fld_Value,'') 
		FROM 
			CFW_Fld_Values_OppItems
		WHERE 
			Fld_ID = @numFldId 
			AND RecId = @numRecordId
	END
	ELSE
	BEGIN
		SELECT 
			@vcValue = ISNULL(Fld_Value,'') 
		FROM 
			CFW_Fld_Values_Item 
		WHERE 
			Fld_ID = @numFldId 
			AND RecId = @numItemCode
	END

	RETURN @vcValue
END