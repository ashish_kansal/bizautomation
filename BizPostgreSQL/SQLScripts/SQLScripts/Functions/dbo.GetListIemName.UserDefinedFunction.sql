/****** Object:  UserDefinedFunction [dbo].[GetListIemName]    Script Date: 07/26/2008 18:13:04 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getlistiemname')
DROP FUNCTION getlistiemname
GO
CREATE FUNCTION [dbo].[GetListIemName](@vcItemID numeric(9))
Returns varchar(100) 
As
BEGIN
	declare @listName as varchar(100)
    set @listName=''
    if (@vcItemID is not null and @vcItemID<>0)
    begin 
		Select @listName=isnull(vcdata,'-') from listdetails where numlistitemid=@vcItemID
	end
	else set @listName='-'


return @listName
END
GO
