GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCustFldValueOppItems')
DROP FUNCTION GetCustFldValueOppItems
GO
CREATE FUNCTION GetCustFldValueOppItems
(
 @numFldId NUMERIC(18,0),
 @numRecordId NUMERIC(18,0),
 @numItemCode NUMERIC(18,0)
)
RETURNS VARCHAR(MAX) 
AS
BEGIN
	DECLARE @vcValue AS VARCHAR(MAX) = ''
	DECLARE @fld_Type AS VARCHAR(50)
	DECLARE @numListID AS NUMERIC(18,0)

    SELECT 
		@numListID=numListID,
		@fld_Type=Fld_type 
	FROM 
		CFW_Fld_Master 
	WHERE 
		Fld_id=@numFldId
       
	IF EXISTS (SELECT Fld_Value FROM CFW_Fld_Values_OppItems WHERE Fld_ID = @numFldId  AND RecId = @numRecordId)
	BEGIN
		SELECT 
			@vcValue = ISNULL(Fld_Value,0) 
		FROM 
			CFW_Fld_Values_OppItems
		WHERE 
			Fld_ID = @numFldId 
			AND RecId = @numRecordId
	END
	ELSE
	BEGIN
		SELECT 
			@vcValue = ISNULL(Fld_Value,0) 
		FROM 
			CFW_Fld_Values_Item 
		WHERE 
			Fld_ID = @numFldId 
			AND RecId = @numItemCode
	END

	IF (@fld_Type='TextBox' OR @fld_Type='TextArea')
	BEGIN
		IF @vcValue='' SET @vcValue='-'
	END
	ELSE IF @fld_Type='SelectBox'
	BEGIN
		IF @vcValue='' 
			SET @vcValue='-'
		ELSE 
			SET @vcValue = dbo.GetListIemName(@vcValue)
	END 
	ELSE IF @fld_Type = 'CheckBox' 
	BEGIN
		SET @vcValue= (CASE WHEN @vcValue=0 THEN 'No' ELSE 'Yes' END)
	END
	ELSE IF @fld_type = 'CheckBoxList'
	BEGIN
		SELECT 
			@vcValue = STUFF((SELECT CONCAT(',', vcData) 
		FROM 
			ListDetails 
		WHERE 
			numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(@vcValue,',')) FOR XML PATH('')), 1, 1, '')
	END
	
	IF @fld_Type = 'DateField'
	BEGIN
		IF @vcValue = '0' OR @vcValue=''
			SET @vcValue = NULL
	END
	ELSE
	BEGIN
		IF @vcValue IS NULL 
			SET @vcValue=''
	END

	RETURN @vcValue
END