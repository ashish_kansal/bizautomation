
/****** Object:  UserDefinedFunction [dbo].[GetAccountTypeCode]    Script Date: 09/25/2009 16:11:52 ******/

GO

GO
-- select dbo.GetAccountTypeCode(72,124,1) 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAccountTypeCode')
DROP FUNCTION GetAccountTypeCode
GO
CREATE FUNCTION	[dbo].[GetAccountTypeCode]
(@numDomainID NUMERIC(9),@numParentID NUMERIC(9),@tintMode TINYINT=0)
returns varchar(50)
AS 
BEGIN
	DECLARE @numAccountTypeID NUMERIC(9)
	DECLARE @vcMaxAccountTypeCode VARCHAR(50)
	DECLARE @vcMaxCharOfAccountCode VARCHAR(50)
	DECLARE @vcNewAccountCode VARCHAR(50)
	DECLARE @vcParentAccountCode VARCHAR(50)

IF @tintMode = 0 OR @tintMode = 1 --0: Get Account code for Account Type, 1:Get new Account code for Chart of Account on change of parent account type 
	BEGIN
		SELECT @numAccountTypeID =[numAccountTypeID],@vcParentAccountCode=[vcAccountCode] FROM [AccountTypeDetail] WHERE [numDomainID]=@numDomainID AND numAccountTypeID =@numParentID
		
		IF (SELECT COUNT(*) FROM  [AccountTypeDetail] WHERE numDomainID=@numDomainID AND  [numParentID]=@numAccountTypeID) > 0 OR (SELECT COUNT(*) FROM [Chart_Of_Accounts] WHERE numDomainID=@numDomainID AND numParntAcntTypeID=@numParentID) > 0
		BEGIN
			SELECT  TOP 1 @vcMaxAccountTypeCode = @vcParentAccountCode + REPLACE( STR(isnull(CAST(SUBSTRING([vcAccountCode],LEN([vcAccountCode])-1,LEN([vcAccountCode])) AS INT),0) + 1 ,2) , ' ', '0')
			FROM [AccountTypeDetail] WHERE numDomainID=@numDomainID AND [numParentID]=@numAccountTypeID ORDER BY [numAccountTypeID] DESC

			SELECT  TOP 1 @vcMaxCharOfAccountCode = @vcParentAccountCode + REPLACE( STR(isnull(CAST(SUBSTRING([vcAccountCode],LEN([vcAccountCode])-1,LEN([vcAccountCode])) AS INT),0) + 1,2) , ' ', '0')
				FROM [Chart_Of_Accounts] WHERE numDomainID=@numDomainID AND numParntAcntTypeID=@numParentID AND isnull(bitIsSubAccount,0) = 0 ORDER BY [vcAccountCode] DESC

			SET @vcNewAccountCode = (CASE WHEN ISNULL(@vcMaxCharOfAccountCode,0) > ISNULL(@vcMaxAccountTypeCode,'0') THEN @vcMaxCharOfAccountCode ELSE  @vcMaxAccountTypeCode END)
		END
		ELSE
		BEGIN
			SELECT  @vcNewAccountCode = @vcParentAccountCode + '01'
		END
END 
IF @tintMode = 2 --select code from AccounttypeDetail table
BEGIN
		SELECT @vcNewAccountCode=[vcAccountCode] FROM [AccountTypeDetail]  WHERE numDomainID=@numDomainID AND [numAccountTypeID]=@numParentID
END 

IF @tintMode = 3 --Get new Account code for Chart of Account on change of IsSubAccount flag from 0 to 1 or 1 to 0
BEGIN
		SELECT @vcParentAccountCode=[vcAccountCode] FROM [Chart_Of_Accounts]
		WHERE [numDomainID]=@numDomainID AND numAccountId=@numParentID
				
			--RETURN  @vcParentAccountCode
		IF (SELECT COUNT(*) FROM  [Chart_Of_Accounts] WHERE numDomainID=@numDomainID AND numParentAccId=@numParentID) >0
			BEGIN
				
				SELECT  TOP 1 @vcNewAccountCode = @vcParentAccountCode + REPLACE( STR(isnull(CAST(SUBSTRING([vcAccountCode],LEN([vcAccountCode])-1,LEN([vcAccountCode])) AS INT),0) + 1 ,2) , ' ', '0')
				FROM [Chart_Of_Accounts] WHERE numDomainID=@numDomainID AND numParentAccId=@numParentID ORDER BY [vcAccountCode] DESC
			END
		ELSE
			BEGIN
				SELECT  @vcNewAccountCode = @vcParentAccountCode + '01' --REPLACE( STR(1 ,2) , ' ', '0')
					--			FROM [AccountTypeDetail] WHERE numAccountTypeID = @numParentID
		END
END 	

RETURN ISNULL(@vcNewAccountCode,'')
END