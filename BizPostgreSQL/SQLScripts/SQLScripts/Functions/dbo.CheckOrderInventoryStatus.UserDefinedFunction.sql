
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckOrderInventoryStatus')
DROP FUNCTION CheckOrderInventoryStatus
GO
CREATE FUNCTION [dbo].[CheckOrderInventoryStatus] 
 (
      @numOppID AS NUMERIC(18,0),
      @numDomainID AS NUMERIC(18,0),
	  @tintMode TINYINT
    )
RETURNS NVARCHAR(MAX)
AS BEGIN

	DECLARE @vcInventoryStatus AS NVARCHAR(MAX) = '';
	
	IF EXISTS(SELECT numOppId FROM dbo.OpportunityMaster WHERE numDomainId=@numDomainID 
			  AND numOppId=@numOppID AND ISNULL(tintshipped,0)=1) OR (SELECT 
																		COUNT(*) 
																	FROM 
																	(
																		SELECT
																			OI.numoppitemtCode,
																			ISNULL(OI.numUnitHour,0) AS OrderedQty,
																			ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																		FROM
																			OpportunityItems OI
																		INNER JOIN
																			Item I
																		ON
																			OI.numItemCode = I.numItemCode
																		OUTER APPLY
																		(
																			SELECT
																				SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																			FROM
																				OpportunityBizDocs
																			INNER JOIN
																				OpportunityBizDocItems 
																			ON
																				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																			WHERE
																				OpportunityBizDocs.numOppId = @numOppID
																				AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																				AND ISNULL(bitFulFilled,0) = 1
																		) AS TempFulFilled
																		WHERE
																			OI.numOppID = @numOppID
																			AND UPPER(I.charItemType) = 'P'
																	) X
																	WHERE
																		X.OrderedQty <> X.FulFilledQty) = 0
	BEGIN
		SET @vcInventoryStatus='<font color="#008000">Shipped</font>';
	END
	ELSE
	BEGIN
		DECLARE @TEMP TABLE
		(
			numOppItemID NUMERIC(18,0)
			,numQtyOrdered NUMERIC(18,0)
			,numQtyShipped NUMERIC(18,0)
			,numQtyBackOrder NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppItemID
			,numQtyOrdered
			,numQtyShipped
			,numQtyBackOrder
		)
		SELECT
			OI.numoppitemtCode
			,OI.numUnitHour
			,ISNULL(numQtyShipped,0)
			,CASE WHEN ISNULL(OI.bitDropShip,0) = 1 THEN 0 ELSE dbo.CheckOrderItemInventoryStatus(OI.numItemCode,OI.numUnitHour,OI.numoppitemtCode,ISNULL(OI.numWarehouseItmsID,0),I.bitKitParent) END
		FROM
			OpportunityItems OI
		INNER JOIN 
			Item I 
		ON 
			OI.numItemCode = I.numItemcode
		LEFT JOIN 
			WareHouseItems WI 
		ON 
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		OUTER APPLY
		(
			SELECT
				SUM(OpportunityBizDocItems.numUnitHour) AS numShippedQty
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE
				OpportunityBizDocs.numOppId = @numOppID
				AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND ISNULL(bitFulFilled,0) = 1
		) AS TempShipped
		WHERE
			numOppId=@numOppID
			AND I.charItemType='P'

		DECLARE @numTotalOrdered FLOAT
		DECLARE @numTotalShipped FLOAT
		DECLARE @numTotalBackOrder FLOAT
		
		SELECT @numTotalOrdered=SUM(numQtyOrdered) FROM @TEMP
		SELECT @numTotalShipped=SUM(numQtyShipped) FROM @TEMP
		SELECT @numTotalBackOrder=SUM(numQtyBackOrder) FROM @TEMP

		IF @numTotalBackOrder > 0
		BEGIN
			SET @vcInventoryStatus = CONCAT('<font color="#FF0000">BO (',@numTotalBackOrder,')</font>')
		END

		IF ((@numTotalOrdered - @numTotalShipped) -  @numTotalBackOrder) > 0
		BEGIN
			SET @vcInventoryStatus = CONCAT(@vcInventoryStatus,(CASE WHEN LEN(@vcInventoryStatus) > 0 THEN ' ' ELSE '' END),'<font color="#6f30a0">Shippable (',((@numTotalOrdered - @numTotalShipped) -  @numTotalBackOrder),')</font>')
		END

		IF @numTotalShipped > 0
		BEGIN
			SET @vcInventoryStatus = CONCAT(@vcInventoryStatus,(CASE WHEN LEN(@vcInventoryStatus) > 0 THEN ' ' ELSE '' END),'<font color="#00b050">Shipped (',@numTotalShipped,')</font>')
		END 
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			OpportunityItems OI
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OM.numOppId = OI.numOppId 
		WHERE 
			OI.numOppId = @numOppID
			AND OM.numDomainId = @numDomainId
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(OI.bitDropShip, 0) = 1
			AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = @numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)) > 0
	BEGIN
		IF @tintMode = 1
		BEGIN
			SET @vcInventoryStatus = CONCAT(@vcInventoryStatus,' <a href=''#'' onclick=''OpenCreateDopshipPOWindow(',@numOppID,')''><img src=''../images/Dropship.png'' style=''height:25px;'' /></a>')
		END
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			dbo.OpportunityItems OI
			INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
			INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
			INNER JOIN WarehouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
			LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
		WHERE   
			OI.numOppId = @numOppID
			AND OM.numDomainId = @numDomainId
			AND ISNULL((SELECT bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID),0) = 1
			AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(I.bitAssembly, 0) = 0
			AND ISNULL(I.bitKitParent, 0) = 0
			AND ISNULL(OI.bitDropship,0) = 0) > 0
	BEGIN
		IF @tintMode = 1
		BEGIN
			SET @vcInventoryStatus = CONCAT(@vcInventoryStatus,' <a href=''#'' onclick=''OpenCreateBackOrderPOWindow(',@numOppID,')''><img src=''../images/BackorderPO.png'' style=''height:25px;'' /></a>')
		END
	END

	IF LEN(ISNULL(@vcInventoryStatus,'')) = 0
	BEGIN
		SET @vcInventoryStatus='<font color="#000000">Not Applicable</font>';
	END
    
    RETURN ISNULL(@vcInventoryStatus,'')
END
