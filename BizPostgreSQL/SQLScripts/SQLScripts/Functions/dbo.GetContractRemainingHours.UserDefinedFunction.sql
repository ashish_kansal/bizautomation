/****** Object:  UserDefinedFunction [dbo].[GetContractRemainingHours]    Script Date: 07/26/2008 18:12:57 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcontractremaininghours')
DROP FUNCTION getcontractremaininghours
GO
CREATE FUNCTION [dbo].[GetContractRemainingHours](@numContractID numeric,@numDivisionId numeric,@numDomainId numeric)                  
 RETURNS decimal(10,2)                  
as                  
Begin                  
DECLARE @vcRetValue1 decimal (10,2)               
        
 select         
 @vcRetValue1= isnull(sum(convert(decimal(10,2),datediff(minute,dtFromDate,dtToDate))/60 ),0)  
        
 from timeandexpense where numType =1 and numCategory =1 
 and numContractid=@numContractId
 and  numDomainId=@numDomainId      
          
  
 RETURN isnull(@vcRetValue1,0)
end
GO
