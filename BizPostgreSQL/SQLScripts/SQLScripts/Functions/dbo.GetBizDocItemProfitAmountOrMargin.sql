GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetBizDocItemProfitAmountOrMargin')
DROP FUNCTION GetBizDocItemProfitAmountOrMargin
GO
CREATE FUNCTION [dbo].[GetBizDocItemProfitAmountOrMargin]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
	,@numOppBizDocItemID NUMERIC(18,0)
	,@tintMode TINYINT --1: Profit Amount, 2:Profit Margin
)    
RETURNS DECIMAL(20,5)    
AS    
BEGIN  
	DECLARE @Value AS DECIMAL(20,5)

	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	If @tintMode = 1
	BEGIN
		SELECT
			@Value = SUM(ISNULL(TEMP.monTotAmount,0) - (ISNULL(TEMP.numUnitHour,0) * (CASE 
																				WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																				THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																				ELSE
																					(CASE @ProfitCost 
																						WHEN 3 THEN ISNULL(TEMP.monCost,0) * dbo.fn_UOMConversion(TEMP.numBaseUnit,TEMP.numItemCode,@numDomainID,TEMP.numPurchaseUnit) 
																						ELSE ISNULL(TEMP.monAverageCost,0)
																					END)
																			END)))
		FROM
		(
			SELECT
				OM.numOppId
				,OI.numoppitemtCode
				,OI.vcAttrValues
				,OBDI.numUnitHour
				,OBDI.monTotAmount
				,I.numItemCode
				,I.numBaseUnit
				,I.numPurchaseUnit
				,V.monCost
				,(CASE 
					WHEN ISNULL(I.bitKitParent,0) = 1 
					THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
							ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
					ELSE ISNULL(OBDI.monAverageCost,0) 
				END) monAverageCost
			FROM
				OpportunityBizDocs OBD
			INNER JOIN
				OpportunityMaster OM
			ON
				OBD.numOppId = OM.numOppId
			INNER JOIN
				OpportunityBizDocItems OBDI
			ON
				OBD.numOppBizDocsId = OBDI.numOppBizDocID
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OBDI.numItemCode = I.numItemCode
			LEFT JOIN 
				Vendor V 
			ON 
				V.numVendorID=I.numVendorID 
				AND V.numItemCode=I.numItemCode
			WHERE
				OBD.numOppId = @numOppID
				AND OBD.numOppBizDocsId = @numOppBizDocID
				AND OBDI.numOppBizDocItemID = @numOppBizDocItemID
				AND I.numItemCode NOT IN (@numDiscountItemID)
		) TEMP
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = TEMP.numOppId
				AND SOLIPL.numSalesOrderItemID = TEMP.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = TEMP.numOppId
				AND OIInner.numItemCode = TEMP.numItemCode
				AND OIInner.vcAttrValues = TEMP.vcAttrValues
		) TEMPPO		
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			@Value = (SUM(Profit)/CASE WHEN SUM(monTotAmount) = 0 THEN 1.0 ELSE SUM(monTotAmount) END) * 100
		FROM
		(
			SELECT
				ISNULL(OBDI.monTotAmount,0) monTotAmount
				,ISNULL(OBDI.monTotAmount,0) - (ISNULL(OBDI.numUnitHour,0) * (CASE 
																		WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																		THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																		ELSE (CASE @ProfitCost 
																				WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																				ELSE (CASE 
																						WHEN ISNULL(I.bitKitParent,0) = 1 
																						THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
																								ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
																						ELSE ISNULL(OBDI.monAverageCost,0) 
																					END)
																				END)
																	END)) Profit
			FROM
				OpportunityBizDocs OBD
			INNER JOIN
				OpportunityMaster OM
			ON
				OBD.numOppId = OM.numOppId
			INNER JOIN
				OpportunityBizDocItems OBDI
			ON
				OBD.numOppBizDocsId = OBDI.numOppBizDocID
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OBDI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					SalesOrderLineItemsPOLinking SOLIPL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					SOLIPL.numPurchaseOrderID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
					AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
				WHERE
					SOLIPL.numSalesOrderID = OM.numOppId
					AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
			) TEMPMatchedPO
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					OpportunityLinking OL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					OL.numChildOppID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
				WHERE
					OL.numParentOppID = OM.numOppId
					AND OIInner.numItemCode = OI.numItemCode
					AND OIInner.vcAttrValues = OI.vcAttrValues
			) TEMPPO
			Left JOIN 
				Vendor V 
			ON 
				V.numVendorID=I.numVendorID 
				AND V.numItemCode=I.numItemCode
			WHERE
				OBD.numOppId = @numOppID
				AND OBD.numOppBizDocsId = @numOppBizDocID
				AND OBDI.numOppBizDocItemID = @numOppBizDocItemID
				AND I.numItemCode NOT IN (@numDiscountItemID)
		) TEMP
	END

	RETURN CAST(@Value AS DECIMAL(20,5))
END
GO