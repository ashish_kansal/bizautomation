GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemGroupsName')
DROP FUNCTION fn_GetItemGroupsName
GO
CREATE FUNCTION [dbo].[fn_GetItemGroupsName](@numItemGroupID numeric)
Returns varchar(100) 
As
BEGIN
	declare @vcItemGroup as varchar(100)

	Select @vcItemGroup=isnull(vcItemGroup,'') from ItemGroups where numItemGroupID=@numItemGroupID

return isnull(@vcItemGroup,'-')
END
GO
