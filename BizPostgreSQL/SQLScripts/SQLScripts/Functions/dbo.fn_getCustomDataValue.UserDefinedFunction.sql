/****** Object:  UserDefinedFunction [dbo].[fn_getCustomDataValue]    Script Date: 07/26/2008 18:12:35 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcustomdatavalue')
DROP FUNCTION fn_getcustomdatavalue
GO
CREATE FUNCTION [dbo].[fn_getCustomDataValue](@vcSqlValue  varchar(100),@intOptions  numeric)
Returns varchar(100) 
as
BEGIN
DECLARE @RetVal varchar(100)
	SET @RetVal=''
select @RetVal=vcOptionName from CustomReptOptValues where intoptions =@intOptions and vcSqlValue like @vcSqlValue
RETURN @RetVal
END
GO
