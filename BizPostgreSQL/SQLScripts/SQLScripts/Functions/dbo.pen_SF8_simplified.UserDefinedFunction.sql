/****** Object:  UserDefinedFunction [dbo].[pen_SF8_simplified]    Script Date: 07/26/2008 18:13:14 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='pen_sf8_simplified')
DROP FUNCTION pen_sf8_simplified
GO
CREATE FUNCTION [dbo].[pen_SF8_simplified] (@SF8PF int) RETURNS decimal(9,2) AS BEGIN declare
@return_value decimal (9,2) DECLARE @SF8PF_val1 decimal(4,2) DECLARE @SF8PF_val2
decimal(4,2) DECLARE @SF8PF_val3 decimal(4,2) DECLARE @SF8PF_val4 decimal(4,2)
DECLARE @SF8PF_val5 decimal(4,2)

Set @SF8PF_val1=21.46 Set @SF8PF_val2=30.31 Set @SF8PF_val3=40.07 Set
@SF8PF_val4=48.33 Set @SF8PF_val5=54.05

set @return_value = Case @SF8PF when 1 then @SF8PF_val1 when 2 then @SF8PF_val2 when
3 then @SF8PF_val3 when 4 then @SF8PF_val4 when 5 then @SF8PF_val5 End

RETURN @return_value

END
GO
