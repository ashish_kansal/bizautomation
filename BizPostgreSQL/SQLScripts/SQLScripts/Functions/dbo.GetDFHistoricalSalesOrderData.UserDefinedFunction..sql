GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetDFHistoricalSalesOrderData')
DROP FUNCTION GetDFHistoricalSalesOrderData
GO
CREATE FUNCTION [dbo].[GetDFHistoricalSalesOrderData] 
 (
	  @numDFID AS NUMERIC(18,0),
      @numDomainID AS NUMERIC(9),
	  @dtFromDate AS DATETIME,
	  @dtToDate AS DATETIME
)
RETURNS  @TEMP TABLE 
(
	numItemCode NUMERIC(18,0), 
	numWarehouseID NUMERIC(18,0),
	numWarehouseItemID NUMERIC(18,0),
	numQtySold INTEGER
)
AS BEGIN 
		DECLARE @bitWarehouseFilter BIT = 0
		DECLARE @bitItemClassificationFilter BIT = 0
		DECLARE @bitItemGroupFilter BIT = 0

		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END


		DECLARE @TEMPORDER TABLE
		(
			numOppID NUMERIC(18,0),
			numOppItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numUnitHour FLOAT,
			bitKitParent BIT,
			bitAssembly BIT
		)

		INSERT INTO	
			@TEMPORDER
		SELECT
			OpportunityMaster.numOppId,
			OpportunityItems.numoppitemtCode,	
			Item.numItemCode,
			WareHouseItems.numWareHouseID,
			(CASE
				WHEN ISNULL(Item.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)) > 0
				THEN
					(CASE 
						WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=Item.numItemCode AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WareHouseItems.numWareHouseItemID,0)) 
						THEN
							(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=Item.numItemCode AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WareHouseItems.numWareHouseItemID,0))
						ELSE
							WareHouseItems.numWareHouseItemID
					END)
				ELSE
					(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWareHouseID=WareHouseItems.numWareHouseID AND numWLocationID = -1)
			END),
			OpportunityItems.numUnitHour,
			Item.bitKitParent,
			Item.bitAssembly
		FROM
			OpportunityMaster
		INNER JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID AND
			CONVERT(DATE,OpportunityMaster.bintCreatedDate) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate) AND
			OpportunityMaster.tintOppType = 1 AND 
			OpportunityMaster.tintOppStatus = 1 
			AND ISNULL(OpportunityItems.bitDropship,0) = 0
			AND ISNULL(OpportunityItems.bitWorkOrder,0) = 0
			AND
			(
				(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
			)
			AND
			(
				(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
				Item.numItemClassification IN (SELECT numItemClassification FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
				Item.numItemGroup IN (SELECT numItemGroup FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		
		-- GET KIT ITEMS CHILD ITEMS
		;WITH CTE(numOppChildItemID,numOppID,numOppItemID,numItemCode,numItemGroup,bitKitParent,numWarehouseID,numWarehouseItemID,numQty) AS
		(
			SELECT 
				numOppChildItemID
				,OKI.numOppId
				,OKI.numOppItemID
				,numChildItemID
				,I.numItemGroup
				,I.bitKitParent
				,WI.numWarehouseID
				,WI.numWareHouseItemID
				,t1.numUnitHour * numQtyItemsReq_Orig
			FROM 
				@TEMPORDER AS t1
			INNER JOIN
				OpportunityKitItems OKI
			ON
				t1.numOppID = OKI.numOppId
				AND t1.numOppItemID = OKI.numOppItemID
			INNER JOIN
				Item I
			ON
				OKI.numChildItemID = I.numItemCode
			INNER JOIN
				WarehouseItems WI
			ON
				OKI.numWareHouseItemId = WI.numWarehouseItemID
				AND ISNULL(t1.bitKitParent,0) = 1 
				AND ISNULL(t1.bitAssembly,0) = 0 
			UNION ALL
			SELECT
				OKCI.numOppKitChildItemID
				,OKCI.numOppId
				,OKCI.numOppItemID
				,OKCI.numItemID
				,I.numItemGroup
				,I.bitKitParent
				,WI.numWarehouseID
				,WI.numWareHouseItemID
				,c.numQty * numQtyItemsReq_Orig
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN	
				CTE c
			ON
				OKCI.numOppChildItemID = c.numOppChildItemID
			INNER JOIN
				Item I
			ON
				OKCI.numItemID = I.numItemCode
			INNER JOIN
				WarehouseItems WI
			ON
				OKCI.numWareHouseItemId = WI.numWarehouseItemID
		)

		INSERT INTO
			@TEMPORDER
		SELECT
			numOppID,
			numOppItemID,
			numItemCode,
			numWarehouseID,
			(CASE
				WHEN ISNULL(c.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(numWareHouseItemID,0)) > 0
				THEN
					(CASE 
						WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0)) 
						THEN
							(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0))
						ELSE
							c.numWareHouseItemID
					END)
				ELSE
					(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=c.numItemCode AND numWareHouseID=c.numWareHouseID AND numWLocationID = -1)
			END),
			numQty,
			bitKitParent,
			0
		FROM
			CTE c
		WHERE
			ISNULL(bitKitParent,0) = 0

		INSERT INTO
			@TEMPORDER
		SELECT   
			0,
			0,
			I.numItemCode,
			WI.numWarehouseID,
			(CASE
				WHEN ISNULL(I.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WI.numWareHouseItemID,0)) > 0
				THEN
					(CASE 
						WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0)) 
						THEN
							(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0))
						ELSE
							WI.numWareHouseItemID
					END)
				ELSE
					(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID AND numWLocationID = -1)
			END),
			WOD.numQtyItemsReq,
			0,
			0
		FROM
			WorkOrder WO
		INNER JOIN 
			WorkOrderDetails WOD 
		ON 
			WO.numWOId = WOD.numWOId
		INNER JOIN
			Item I
		ON
			WOD.numChildItemID = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			WOD.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			WO.numDomainID=@numDomainID
			AND CONVERT(DATE,WO.bintCreatedDate) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)
			AND
			(
				(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
				WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
			)
			AND
			(
				(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
				I.numItemClassification IN (SELECT numItemClassification FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
				I.numItemGroup IN (SELECT numItemGroup FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		-- Get total quantity of inventoty or assembly item in Sales Order(tintOppType = 1 AND tintOppStatus = 1)
		INSERT INTO
			@TEMP
		SELECT
			t1.numItemCode, 
			t1.numWarehouseID,
			t1.numWarehouseItemID,
			SUM(t1.numUnitHour) AS numQtySold
		FROM
			@TEMPORDER AS t1
		WHERE
			ISNULL(t1.bitKitParent,0) = 0 
			AND ISNULL(t1.bitAssembly,0) = 0
			AND ISNULL(t1.numWarehouseItemID,0) > 0
		GROUP BY
			t1.numItemCode,
			t1.numWarehouseID,
			t1.numWarehouseItemID

		RETURN
END