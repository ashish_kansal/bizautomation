GO
--select dbo.GetReturnDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='GetReturnDealAmount')
DROP FUNCTION GetReturnDealAmount
GO
CREATE FUNCTION [dbo].[GetReturnDealAmount]
(
	@numReturnHeaderID NUMERIC(18,0),
	@tintMode TINYINT
)    
RETURNS @Result TABLE     
(    
	monAmount DECIMAL(20,5),
	monTotalTax DECIMAL(20,5),
	monTotalDiscount DECIMAL(20,5)  
)    
AS    
BEGIN   
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @monTotalDiscount DECIMAL(20,5),@monAmount DECIMAL(20,5),@monTotalTax DECIMAL(20,5),@tintReturnType TINYINT 
 
	SELECT 
		@numDomainID = numDomainID,
		@monTotalDiscount=ISNULL(monTotalDiscount,0),
		@monAmount=ISNULL(monAmount,0),
		@tintReturnType=tintReturnType
	FROM 
		dbo.ReturnHeader 
	WHERE 
		numReturnHeaderID=@numReturnHeaderID
 
	 IF @tintMode=5 OR @tintMode=6 OR @tintMode=7 OR @tintMode=8 OR (@tintReturnType=1 AND @tintMode=9)
	 BEGIN
 	
		SET @monTotalTax=0
 	
 		DECLARE @numTaxItemID AS NUMERIC(9)
		DECLARE @fltPercentage FLOAT = 0
		DECLARE @tintTaxType TINYINT = 1
		DECLARE @numUnitHour FLOAT = 0
		DECLARE @ItemAmount DECIMAL(20,5) = 0
		DECLARE @numTaxID AS NUMERIC(18,0)

		SELECT @monAmount=SUM(
								(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END)  
								* (CASE WHEN numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(20,5),ISNULL(monTotAmount,0) / numUnitHour) END)
							) 
		FROM 
			ReturnItems 
		WHERE 
			numReturnHeaderID=@numReturnHeaderID   


		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numTaxItemID NUMERIC(18,0),
			fltPercentage FLOAT,
			tintTaxType TINYINT,
			numTaxID NUMERIC(18,0)
		)
		 
		INSERT INTO @TEMP
		(
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		)
		SELECT
			numTaxItemID,
			fltPercentage,
			ISNULL(tintTaxType,1),
			ISNULL(numTaxID,0)
		FROM 
			OpportunityMasterTaxItems 
		WHERE 
			numReturnHeaderID=@numReturnHeaderID  
		ORDER BY 
			numTaxItemID 

		DECLARE @i AS INT = 1
		DECLARE @Count AS INT

		SELECT @Count=COUNT(*) FROM @TEMP
 
		WHILE @i <= @Count
		BEGIN
			SELECT 
				@numTaxItemID=numTaxItemID,
				@fltPercentage=fltPercentage,
				@tintTaxType=ISNULL(tintTaxType,1),
				@numTaxID = ISNULL(numTaxID,0)
			FROM 
				@TEMP 
			WHERE 
				ID = @i

			IF ISNULL(@fltPercentage,0)>0
			BEGIN  
				DECLARE @numReturnItemID AS NUMERIC
				SET @numReturnItemID=0
				SET @ItemAmount=0	
					
				SELECT TOP 1 
					@numReturnItemID=numReturnItemID,
					@numUnitHour = (CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(ReturnItems.numUOMId, 0)),
					@ItemAmount=(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) 
								* (CASE WHEN numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(monTotAmount,0) / numUnitHour) END)
				FROM 
					ReturnItems
				INNER JOIN
					Item I
				ON
					ReturnItems.numItemCode = I.numItemCode
				WHERE 
					numReturnHeaderID=@numReturnHeaderID   
				ORDER BY 
					numReturnItemID 
				     
				WHILE @numReturnItemID>0    
				BEGIN
					IF (SELECT COUNT(*) from OpportunityItemsTaxItems where numReturnItemID=@numReturnItemID and numTaxItemID=@numTaxItemID and ISNULL(numTaxID,0) = ISNULL(@numTaxID,0))>0
					BEGIN
						SET @monTotalTax= @monTotalTax + (CASE WHEN @tintTaxType = 2 THEN (ISNULL(@fltPercentage,0) * ISNULL(@numUnitHour,0)) ELSE @fltPercentage * @ItemAmount / 100 END)
					END    
				    
					SELECT TOP 1 
						@numReturnItemID=numReturnItemID,
						@numUnitHour = (CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(ReturnItems.numUOMId, 0)),
						@ItemAmount=(CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * (CASE WHEN numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(monTotAmount,0) / numUnitHour) END)
					FROM 
						ReturnItems 
					INNER JOIN
						Item I
					ON
						ReturnItems.numItemCode = I.numItemCode
					WHERE 
						numReturnHeaderID=@numReturnHeaderID  and numReturnItemID>@numReturnItemID   
					ORDER BY 
						numReturnItemID     
				    
					IF @@rowcount=0 SET @numReturnItemID=0    
				END 
			END 
	    
			SET @i = @i + 1
		END  
	 END
 
 
 INSERT INTO @result
 SELECT ISNULL(@monAmount,0),ISNULL(@monTotalTax,0),ISNULL(@monTotalDiscount,0)
 
 RETURN
END
GO