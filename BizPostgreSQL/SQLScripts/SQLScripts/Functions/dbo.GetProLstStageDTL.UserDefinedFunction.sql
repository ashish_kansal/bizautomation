/****** Object:  UserDefinedFunction [dbo].[GetProLstStageDTL]    Script Date: 07/26/2008 18:13:10 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getprolststagedtl')
DROP FUNCTION getprolststagedtl
GO
CREATE FUNCTION [dbo].[GetProLstStageDTL](@numProid numeric(9))
	RETURNS varchar (500) 
AS
BEGIN
declare @StageDetail as varchar(500)
declare @DueDate as varchar(100)
declare @ComDate as varchar(100)
declare @numProStageId as varchar(10)

set @numProStageId=(select top 1 numProStageId  from ProjectsStageDetails 
where numProID=@numProid and numStagePercentage!=100 and bitstagecompleted=1 
and bintStageComDate=(select top 1 max(bintStageComDate) from ProjectsStageDetails 
where numProID=@numProid and bitstagecompleted=1))
if @numProStageId !=''
begin

set @StageDetail=(select vcStageDetail from ProjectsStageDetails where numProStageId=@numProStageId)


end

return @StageDetail
END
GO
