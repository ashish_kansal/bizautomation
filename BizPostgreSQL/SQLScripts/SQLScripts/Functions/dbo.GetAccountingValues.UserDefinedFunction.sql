/****** Object:  UserDefinedFunction [dbo].[GetAccountingValues]    Script Date: 07/26/2008 18:12:54 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAccountingValues')
DROP FUNCTION GetAccountingValues
GO
CREATE FUNCTION [dbo].[GetAccountingValues](@type as smallint,@numDomainId as numeric(9),@startDate as datetime,@endDate as dateTime)  
returns decimal(10,2)  
as  
begin  
  
--declare @Income  as decimal(10,2)  
--declare @Expenses  as decimal(10,2)  
--declare @COGS  as decimal(10,2)  
--declare @OIncome  as decimal(10,2)  
--declare @OExpenses  as decimal(10,2)  
--Declare @GrossProfit as decimal(10,2)  
--Declare @NetOperatingIncome as decimal(10,2)  
--Declare @NetOtherIncome as decimal(10,2)  
--Declare @NetIncome as decimal(10,2)  
--set @Income = 0  
--set @Expenses = 0  
--set @COGS = 0  
--set @OIncome = 0  
--set @OExpenses = 0  
--set @GrossProfit = 0  
--  
--  
--  
----------Income  
--if @type = 1 or @type =6 or @type =7  
--begin  
-- Select @Income= isnull(sum(GJD.numDebitAmt)-sum(GJD.numCreditAmt),0) From General_Journal_Header GJH  
-- join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId  
-- Where  GJH.datEntry_Date >= @startDate and GJH.datEntry_Date <= @endDate  
-- and GJD.numChartAcntId in(Select numAccountId From Chart_of_Accounts where numAcntType=822 and numdomainId=@numDomainId)  
--   
--end  
--  
--  
-----Expenses  
--if @type = 2 or @type =7  
--begin  
-- Select @Expenses=isnull( sum(GJD.numDebitAmt)-sum(GJD.numCreditAmt),0) From General_Journal_Header GJH  
-- join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId  
-- Where  GJH.datEntry_Date >= @startDate and GJH.datEntry_Date <= @endDate  
-- and  GJD.numChartAcntId in(Select numAccountId From Chart_of_Accounts where numAcntType=824 and numdomainId=@numDomainId)  
--End  
--  
----COGS  
--if @type = 3 or @type =6 or @type =7  
--begin  
-- Select @COGS=isnull(sum(GJD.numDebitAmt)-sum(GJD.numCreditAmt),0) From General_Journal_Header GJH  
-- join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId  
-- Where   GJH.datEntry_Date >= @startDate and GJH.datEntry_Date <= @endDate  
-- and GJD.numChartAcntId in(Select numAccountId From Chart_of_Accounts where numAcntType=823 and numdomainId=@numDomainId)  
--End  
---- Other income  
--if @type = 4 or @type =7  
--begin  
--  
-- Select @OIncome=isnull(sum(GJD.numDebitAmt)-sum(GJD.numCreditAmt),0) From General_Journal_Header GJH  
-- join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId  
-- Where   GJH.datEntry_Date >= @startDate and GJH.datEntry_Date <= @endDate  
-- and GJD.numChartAcntId in(Select numAccountId From Chart_of_Accounts where numAcntType=825 and numdomainId=@numDomainId)  
--End  
---- Other Expenses  
--if @type = 5 or @type =7  
--begin  
-- Select @OExpenses=isnull(sum(GJD.numDebitAmt)-sum(GJD.numCreditAmt),0) From General_Journal_Header GJH  
-- join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId  
-- Where   GJH.datEntry_Date >= @startDate and GJH.datEntry_Date <= @endDate  
-- and GJD.numChartAcntId in(Select numAccountId From Chart_of_Accounts where numAcntType=826 and numdomainId=@numDomainId)  
--end   
--  
--Set @GrossProfit =@Income - @COGS  
-- Set @NetOperatingIncome=@GrossProfit-@Expenses  
-- Set @NetOtherIncome=@OIncome-@OExpenses  
--Set @NetIncome=@NetOperatingIncome+@NetOtherIncome  
--  
----Select * From ListDetails where numlistitemid>812  
--return   
--case   
-- when @type =1 then  
-- @Income  
-- when @type =2 then  
-- @Expenses  
-- when @type =3 then  
-- @COGS  
-- when @type =4 then  
-- @OIncome  
-- when @type =5 then  
-- @OExpenses  
-- when @type =6 then  
-- @GrossProfit  
-- when @type =7 then  
-- @NetIncome  
--end  
--  
RETURN 0
end
GO
