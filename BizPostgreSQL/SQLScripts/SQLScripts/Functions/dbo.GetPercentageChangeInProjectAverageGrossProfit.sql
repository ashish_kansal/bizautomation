GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPercentageChangeInProjectAverageGrossProfit')
DROP FUNCTION GetPercentageChangeInProjectAverageGrossProfit
GO
CREATE FUNCTION [dbo].[GetPercentageChangeInProjectAverageGrossProfit]
(
	 @numDomainID AS NUMERIC(18,0),
	 @dtStartDate DATE,
	 @dtEndDate DATE
)    
RETURNS NUMERIC(18,2)
AS    
BEGIN   
	DECLARE @ProjectGrossProfitChange NUMERIC(18,2) = 0.00
	DECLARE @ProjectGrossProfit1 INT
	DECLARE @ProjectGrossProfit2 INT

	SELECT
		@ProjectGrossProfit1 = AVG(GrossProfit)
	FROM
		(
			SELECT
				dbo.GetProjectGrossProfit(@numDomainID,numProId) AS GrossProfit
			FROM
				ProjectsMaster
			WHERE
				numDomainId=@numDomainID
				AND bintCreatedDate BETWEEN @dtStartDate AND @dtEndDate
		) TEMP

	SELECT
		@ProjectGrossProfit2 = AVG(GrossProfit)
	FROM
		(
			SELECT
				dbo.GetProjectGrossProfit(@numDomainID,numProId) AS GrossProfit
			FROM
				ProjectsMaster
			WHERE
				numDomainId=@numDomainID
				AND bintCreatedDate BETWEEN DATEADD(YEAR,-1,@dtStartDate) AND DATEADD(YEAR,-1,@dtEndDate)
		) TEMP


	SET @ProjectGrossProfitChange = 100.0 * (ISNULL(@ProjectGrossProfit1,0) - ISNULL(@ProjectGrossProfit2,0)) / (CASE WHEN ISNULL(@ProjectGrossProfit2,0) = 0 THEN 1 ELSE ISNULL(@ProjectGrossProfit2,0) END)


	RETURN ISNULL(@ProjectGrossProfitChange,0.00)
END
GO