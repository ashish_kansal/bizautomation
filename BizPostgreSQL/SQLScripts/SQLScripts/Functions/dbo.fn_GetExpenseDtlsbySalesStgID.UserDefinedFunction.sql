/****** Object:  UserDefinedFunction [dbo].[fn_GetExpenseDtlsbySalesStgID]    Script Date: 07/26/2008 18:12:37 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getexpensedtlsbysalesstgid')
DROP FUNCTION fn_getexpensedtlsbysalesstgid
GO
CREATE FUNCTION [dbo].[fn_GetExpenseDtlsbySalesStgID](@numOppID numeric, @numOppStageID numeric,@tintType tinyint)    
 RETURNS DECIMAL(20,5)     
AS    
--Thsi function will take the ID fo rthe Territory or the List Details and return the corresponding name.    
BEGIN    
 DECLARE @vcRetValue DECIMAL(20,5)    
     
    
-- select @vcRetValue=monAmount    
-- from  OpportunityExpense where numOppID=@numOppID and numOppStageID=@numOppStageID    
    
 select @vcRetValue=convert(varchar(100),case when numtype=1 then  monAmount else 0 end )      
from  timeandexpense
where numOppID=@numOppID and numStageID=@numOppStageID     
	and numCategory = 2 and tintTEType = 1 
   
 RETURN @vcRetValue     
END
GO
