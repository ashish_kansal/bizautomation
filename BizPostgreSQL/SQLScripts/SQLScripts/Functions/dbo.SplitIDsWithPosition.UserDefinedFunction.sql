GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='SplitIDsWithPosition')
DROP FUNCTION SplitIDsWithPosition
GO
--SELECT * FROM dbo.SplitIDsWithPosition('197605,197606,197595,197598', ',')
CREATE FUNCTION [dbo].[SplitIDsWithPosition]
(
	@IdList varchar(8000),
	@Delimiter char(1) = '/'
)
RETURNS 
@ParsedList table
(
	RowNumber int,
	Id int
)
AS
BEGIN
	DECLARE @Id varchar(10), @Pos INT, @rownumber INT 

	SET @IdList = LTRIM(RTRIM(@IdList))+ @Delimiter
	SET @Pos = CHARINDEX(@Delimiter, @IdList, 1)
	SET @rownumber = 0;
	IF REPLACE(@IdList, @Delimiter, '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @Id = LTRIM(RTRIM(LEFT(@IdList, @Pos - 1)))
			IF @Id <> ''
			BEGIN
				SET @rownumber = @rownumber +1;
				INSERT INTO @ParsedList (RowNumber,Id) 
				VALUES (@rownumber ,CAST(@Id AS int)) --Use Appropriate conversion
			END
			SET @IdList = RIGHT(@IdList, LEN(@IdList) - @Pos)
			SET @Pos = CHARINDEX(@Delimiter, @IdList, 1)

		END
	END	
	RETURN
END
GO
