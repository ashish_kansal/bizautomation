/****** Object:  UserDefinedFunction [dbo].[fn_GetTotalHrs]    Script Date: 07/26/2008 18:12:47 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_gettotalhrs')
DROP FUNCTION fn_gettotalhrs
GO
CREATE FUNCTION [dbo].[fn_GetTotalHrs](@bitOverTime bit,@bitOverTimeHrsDailyOrWeekly bit,@decOverTime decimal,@dtStartDate as datetime,@dtEndDate as datetime,@numUserCntID numeric,@numDomainId numeric,@bitTotal bit,@ClientTimeZoneOffset int)                                        
    
 RETURNS decimal(10,2)                                            
as                                            
Begin                       
  Declare @decTotalHrsWorked as decimal(10,2)            
     Declare @decTotalOverTimeHrsWorked as decimal(10,2)          
     Declare @decTotalHrs as decimal(10,2)          
     Set @decTotalHrsWorked=0          
  Set @decTotalOverTimeHrsWorked=0          
  Set @decTotalHrs=0          
   Select @decTotalHrsWorked=sum(x.Hrs) From (Select  isnull(Sum(Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)),0) as Hrs            
  from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1                 
  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) And             
  numUserCntID=@numUserCntID  And numDomainID=@numDomainId             
  ) x                                                
     Set @decTotalOverTimeHrsWorked = dbo.GetOverHrsWorked(@bitOverTime,@bitOverTimeHrsDailyOrWeekly,@decOverTime,@dtStartDate,@dtEndDate,@numUserCntID,@numDomainId,@ClientTimeZoneOffset)          
 if @bitTotal=1      
      Set @decTotalHrs=@decTotalHrsWorked          
 Else if @bitTotal=0      
   Set @decTotalHrs=@decTotalHrsWorked-@decTotalOverTimeHrsWorked      
       
  Return @decTotalHrs          
End
GO
