/****** Object:  UserDefinedFunction [dbo].[funDistance]    Script Date: 07/26/2008 18:12:53 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fundistance')
DROP FUNCTION fundistance
GO
CREATE FUNCTION [dbo].[funDistance] 
(
 @LAT1 float,
 @LON1 float,
 @LAT2 float,
 @LON2 float
)
RETURNS float
AS
BEGIN
 DECLARE @Distance float;
 
IF @LAT1=@LAT2 and @LON1=@LON2
	SET @Distance=0
ELSE
 SET @Distance = 3963.0 * acos(sin(@LAT1/57.2958) * sin(@LAT2/57.2958) + 
   cos(@LAT1/57.2958) * cos(@LAT2/57.2958) *  cos(@LON2/57.2958 - @LON1/57.2958))
 
 -- Return the result of the function
 RETURN @Distance
 
END
GO
