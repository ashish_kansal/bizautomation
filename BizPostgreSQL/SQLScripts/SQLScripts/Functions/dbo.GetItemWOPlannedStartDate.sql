GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetItemWOPlannedStartDate')
DROP FUNCTION GetItemWOPlannedStartDate
GO
CREATE FUNCTION [dbo].[GetItemWOPlannedStartDate]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@numQtyToBuild FLOAT
	,@dtPlannedStartDate DATETIME
)    
RETURNS DATETIME
AS
BEGIN
	DECLARE @dtVendorShipDate DATETIME 
	
	SET @dtVendorShipDate = (SELECT 
								MAX(dtVendorShipDate)
							FROM
							(
								SELECT 
									Item.numItemCode
									,(CASE 
										WHEN ISNULL(Item.bitAssembly,0) = 1
										THEN [dbo].[GetItemWOPlannedStartDate](@numDomainID
																			,Item.numItemCode
																			,@numWarehouseID
																			,(CASE 
																				WHEN ISNULL((SELECT TOP 1 ISNULL(numOnHand,0) - ISNULL(numBackOrder,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWarehouseID=@numWarehouseID AND ISNULL(numWLocationID,0) = 0 ORDER BY numWarehouseItemID),0) > 0
																				THEN CAST((@numQtyToBuild * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID,0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT) - ISNULL((SELECT TOP 1 ISNULL(numOnHand,0) - ISNULL(numBackOrder,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWarehouseID=@numWarehouseID AND ISNULL(numWLocationID,0) = 0 ORDER BY numWarehouseItemID),0)
																				ELSE CAST((@numQtyToBuild * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID,0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT)
																			END)
																			,@dtPlannedStartDate)    
										ELSE DATEADD(DAY,ISNULL(dbo.GetVendorPreferredMethodLeadTime(Item.numVendorID,@numWarehouseID),0),@dtPlannedStartDate)
									END) dtVendorShipDate
								FROM 
									ItemDetails
								INNER JOIN
									Item
								ON
									ItemDetails.numChildItemID = Item.numItemCode
								LEFT JOIN
									Vendor
								ON
									Item.numVendorID = Vendor.numVendorID
									AND Item.numItemCode = Vendor.numItemCode
								WHERE
									numItemKitID=@numItemCode
									AND Item.charItemType = 'P'
									AND 1= (CASE 
												WHEN Item.bitAssembly = 1 
												THEN (CASE WHEN ISNULL((SELECT TOP 1 ISNULL(numOnHand,0) - ISNULL(numBackOrder,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWarehouseID=@numWarehouseID AND ISNULL(numWLocationID,0) = 0 ORDER BY numWarehouseItemID),0) < CAST((@numQtyToBuild * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID,0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT) THEN 1 ELSE 0 END)
												ELSE (CASE WHEN ISNULL((SELECT SUM(numOnHand) - SUM(numBackOrder) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWarehouseID=@numWarehouseID),0) < CAST((@numQtyToBuild * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID,0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT) THEN 1 ELSE 0 END)
											END)
						) TEMP)

	RETURN (CASE WHEN @dtVendorShipDate IS NOT NULL THEN @dtVendorShipDate ELSE @dtPlannedStartDate END)
END