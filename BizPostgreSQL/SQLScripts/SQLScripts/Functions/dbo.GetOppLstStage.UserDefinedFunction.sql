/****** Object:  UserDefinedFunction [dbo].[GetOppLstStage]    Script Date: 07/26/2008 18:13:05 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getopplststage')
DROP FUNCTION getopplststage
GO
CREATE FUNCTION [dbo].[GetOppLstStage](@numOppid numeric(9))
	RETURNS varchar (500) 
AS
BEGIN
declare @StageDetail as varchar(500)
declare @ComDate as varchar(50)
declare @numOppStageId as varchar(10)

set @numOppStageId=(select top 1 numOppStageId  from OpportunityStageDetails 
where numoppId=@numOppid and numStagePercentage!=100 and bitstagecompleted=1 
and bintStageComDate=(select top 1 max(bintStageComDate) from OpportunityStageDetails 
where numoppId=@numOppid and bitstagecompleted=1))
if @numOppStageId !=''
begin

set @StageDetail=(select vcStageDetail from OpportunityStageDetails where numOppStageId=@numOppStageId)
set @ComDate= (select bintStageComDate from OpportunityStageDetails where numOppStageId=@numOppStageId)

set @StageDetail=@StageDetail  +' ,' + isnull(@ComDate,'-')

end

return @StageDetail
END
GO
