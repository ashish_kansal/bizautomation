/****** Object:  UserDefinedFunction [dbo].[fn_GeTimeDtlsbyProStgID]    Script Date: 07/26/2008 18:12:37 ******/

GO

GO
--tintType=1(Billable), 2 then non billable
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getimedtlsbyprostgid')
DROP FUNCTION fn_getimedtlsbyprostgid
GO
CREATE FUNCTION [dbo].[fn_GeTimeDtlsbyProStgID](@numProID numeric, @numProStageID numeric,@tintType tinyint)            
 RETURNS varchar(100)             
AS            
BEGIN            
 DECLARE @vcRetValue varchar (250)            
 
--Now show sumtoal of amount and toal hours for selected stage
SELECT @vcRetValue= CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 2), ( CONVERT(DECIMAL(10, 2), CONVERT(DECIMAL(10, 2),SUM( DATEDIFF(minute, dtFromDate, dtToDate)))
                                                        / 60) ))) + ' ( Amt '
        + CASE WHEN numType = 1 THEN CONVERT(VARCHAR(100), CAST( SUM(monamount * CAST(CAST(DATEDIFF(minute, dtFromDate, dtToDate) AS DECIMAL(10,2))/60 AS DECIMAL(10,2)) ) AS DECIMAL(10,2))  )
               ELSE '0.00'
          END + ')'             
from  timeandexpense    
where 
numProID=@numProID and numStageID=@numProStageID
 AND
 numCategory = 1 and tintTEType = 2
 AND numType= @tintType
GROUP BY dtFromDate,dtToDate,numType

            
 RETURN @vcRetValue             
END
GO
