/****** Object:  UserDefinedFunction [dbo].[fn_GetMarketBudgetMonthTotalAmt]    Script Date: 07/26/2008 18:12:40 ******/

GO

GO
--Created By Siva                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getmarketbudgetmonthtotalamt')
DROP FUNCTION fn_getmarketbudgetmonthtotalamt
GO
CREATE FUNCTION [dbo].[fn_GetMarketBudgetMonthTotalAmt]               
(@numMarketId numeric(9),@stDate datetime,@numListItemID numeric(9)=0,@numDomainId numeric(9))                  
Returns varchar(10)                
As                  
Begin                   
    Declare @monAmount as DECIMAL(20,5)              
    Declare @montotalAmt as DECIMAL(20,5)             
    Declare @numMonth as tinyint           
    Declare @montotAmt as varchar(10)                     
    set @numMonth =1            
    Set @montotalAmt=0   
    Set @monAmount=0        
 While @numMonth<=12          
Begin          
  Declare @dtFiscalStDate as datetime                            
  Declare @dtFiscalEndDate as datetime                       
  Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@stDate,@numDomainId),@numDomainId)                         
  Set @dtFiscalStDate=dateadd(month,@numMonth-1,@dtFiscalStDate)                      
  Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@stDate,@numDomainId),@numDomainId)))                      
  Select @monAmount= isnull(MBD.monAmount,0) From MarketBudgetMaster MBM
  inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId
  Where MBM.numDomainId=@numDomainId And MBD.numListItemID=@numListItemID And MBD.numMarketId=@numMarketId and  MBD.tintmonth=month(@dtFiscalStDate) And MBD.intyear=year(@dtFiscalStDate)          
  Set  @montotalAmt =@montotalAmt+@monAmount          
  Set @numMonth=@numMonth+1          
 End          
    if @montotalAmt=0.00 Set @montotAmt=''  Else Set @montotAmt=@montotalAmt        
                    
 Return @montotAmt                  
End
GO
