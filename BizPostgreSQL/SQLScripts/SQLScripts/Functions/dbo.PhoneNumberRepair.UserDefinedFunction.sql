/****** Object:  UserDefinedFunction [dbo].[PhoneNumberRepair]    Script Date: 07/26/2008 18:13:14 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='phonenumberrepair')
DROP FUNCTION phonenumberrepair
GO
CREATE FUNCTION [dbo].[PhoneNumberRepair](@FieldValue varchar(25))
Returns varchar(25) AS
Begin
DECLARE
@StrippedText varchar(25),
@LenFieldValue int,
@Looper int,
@CharCnt int,
@NoOfTimes int,
@CurrentCharacter varchar(1),
@AreaCode varchar(3),
@Prefix varchar(3),
@Suffix varchar(4)

Select @LenFieldValue = Len(@FieldValue)
Select @Looper = 1
Select @StrippedText = ''
Select @CharCnt = 0
Select @NoOfTimes = 0

While @Looper <= @LenFieldValue
        Begin
                Select @CurrentCharacter = Substring(@FieldValue, @Looper, 1)
                If IsNumeric(@CurrentCharacter) = 1 and not @CurrentCharacter = '-'
                        Begin
				If @StrippedText = ''
				Begin
                                	Select @StrippedText = @CurrentCharacter
				End
				Else
				Begin
					If @CharCnt >= 3 and @NoOfTimes < 2
					Begin
						Select @StrippedText = @StrippedText + '-' + @CurrentCharacter
						Select @charCnt = 0
						Select @NoOfTimes = @NoOfTimes + 1
					End
					Else
					Begin
                                		Select @StrippedText = @StrippedText + @CurrentCharacter
					End
				End
                        End
                Select @Looper = @Looper + 1
		Select @CharCnt = @CharCnt + 1
        End

Return @StrippedText
End
GO
