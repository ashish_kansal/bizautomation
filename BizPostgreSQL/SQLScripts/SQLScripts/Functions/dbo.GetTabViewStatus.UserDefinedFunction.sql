/****** Object:  UserDefinedFunction [dbo].[GetTabViewStatus]    Script Date: 07/26/2008 18:13:11 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='gettabviewstatus')
DROP FUNCTION gettabviewstatus
GO
CREATE FUNCTION [dbo].[GetTabViewStatus](@numTabID numeric(9),@numGroupID numeric(9),@numRelationShip numeric(9))
	RETURNS tinyint 
AS
BEGIN
declare @Allow as tinyint
declare @tintGroupType as tinyint
select @tintGroupType=tintGroupType from AuthenticationGroupMaster where numGroupID= @numGroupID
if @tintGroupType=2 select @Allow=bitallowed from GroupTabDetails where numGroupID=@numGroupID and numTabID=@numTabID and numRelationShip=@numRelationShip
else select @Allow=bitallowed from GroupTabDetails where numGroupID=@numGroupID and numTabID=@numTabID
	
if @Allow is null set @Allow=0

RETURN @Allow
END
GO
