/****** Object:  UserDefinedFunction [dbo].[fn_GetEmailIDs]    Script Date: 07/26/2008 18:12:36 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getemailids')
DROP FUNCTION fn_getemailids
GO
CREATE FUNCTION [dbo].[fn_GetEmailIDs] (@vcEmailIds varchar(250))
	RETURNS varchar (1000) 
AS
--This function will take the AOI IDs in comma separated string and return the AOI names with <BR> separating each AOI.
BEGIN
	DECLARE @intFirstComma integer
	DECLARE @intNextComma integer
	DECLARE @vcTempValue varchar(255)
	DECLARE @vcRetVal varchar (1000)
	DECLARE @vcTempVal numeric

	SET @vcRetVal=''

	IF @vcEmailIds=''
	BEGIN
		RETURN '&nbsp;'
	END
	SET @intFirstComma=CHARINDEX(';', @vcEmailIds, 0)
	SET @intNextComma=CHARINDEX(';', @vcEmailIds, @intFirstComma + 1)
	IF @intNextComma=0
	BEGIN
		SET @vcTempValue=SUBSTRING(@vcEmailIds,@intFirstComma+1,LEN(@vcEmailIds)-@intFirstComma)
		IF @vcTempValue<>''
		BEGIN
			SET @vcTempVal = 0
			SELECT @vcTempVal=numContactID FROM AdditionalContactsInformation WHERE vcEmail=@vcTempValue
			IF @vcTempVal > 0
			BEGIN
				SET @vcRetVal = @vcRetVal + ',' + CAST(@vcTempVal AS varchar)
			END
		END
		SET @intFirstComma=CHARINDEX(';', @vcEmailIds, @intNextComma)
	END
	WHILE @intFirstComma>0
	BEGIN
		IF @intNextComma=0
		BEGIN
			SET @vcTempValue=SUBSTRING(@vcEmailIds,@intFirstComma+1,LEN(@vcEmailIds)-@intFirstComma)
			IF @vcTempValue<>''
			BEGIN
				SET @vcTempVal = 0
				SELECT @vcTempVal=numContactID FROM AdditionalContactsInformation WHERE vcEmail=@vcTempValue
				IF @vcTempVal > 0
				BEGIN
					SET @vcRetVal = @vcRetVal + ',' + CAST(@vcTempVal AS varchar)
				END
			END
			BREAK
		END
		ELSE
		BEGIN
			SET @vcTempValue=SUBSTRING(@vcEmailIds,@intFirstComma+1,@intNextComma-@intFirstComma-1)
			IF @vcTempValue<>''
			BEGIN
				SET @vcTempVal = 0
				SELECT @vcTempVal=numContactID FROM AdditionalContactsInformation WHERE vcEmail=@vcTempValue
				IF @vcTempVal > 0
				BEGIN
					SET @vcRetVal = @vcRetVal + ',' + CAST(@vcTempVal As varchar)
				END
			END
		END

		SET @intFirstComma=CHARINDEX(';', @vcEmailIds, @intNextComma)
		SET @intNextComma=CHARINDEX(';', @vcEmailIds, @intFirstComma + 1)
	END

	RETURN @vcRetVal
END
GO
