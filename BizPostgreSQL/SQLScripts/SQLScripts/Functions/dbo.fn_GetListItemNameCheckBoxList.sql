
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetListItemNameCheckBoxList')
DROP FUNCTION fn_GetListItemNameCheckBoxList
GO
CREATE FUNCTION [dbo].[fn_GetListItemNameCheckBoxList]
(
	@vclistIDs NVARCHAR(1000)
)
RETURNS NVARCHAR(1000) 
As
BEGIN
	declare @listName as NVARCHAR(1000)

	SELECT 
		@listName = STUFF((SELECT CONCAT(',', vcData) 
	FROM 
		ListDetails 
	WHERE 
		numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(@vclistIDs,',')) FOR XML PATH('')), 1, 1, '')

	RETURN ISNULL(@listName,'-')
END
GO
