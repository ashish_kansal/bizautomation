/****** Object:  UserDefinedFunction [dbo].[fn_MonthName1]    Script Date: 07/26/2008 18:12:49 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_monthname1')
DROP FUNCTION fn_monthname1
GO
CREATE FUNCTION [dbo].[fn_MonthName1] (@intDate int) 
	returns varchar (20) 
BEGIN
	RETURN dbo.fn_MonthName(SUBSTRING(cast(@intDate as varchar),5,2)) + ',' + RIGHT(cast(@intDate as varchar),2) + ' ' + LEFT(cast(@intDate as varchar),4)
END
GO
