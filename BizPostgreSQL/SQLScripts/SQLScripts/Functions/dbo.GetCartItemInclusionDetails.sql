GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCartItemInclusionDetails')
DROP FUNCTION GetCartItemInclusionDetails
GO
CREATE FUNCTION [dbo].[GetCartItemInclusionDetails]
(
	@numCartID NUMERIC(18,0)
)    
RETURNS VARCHAR(MAX)    
AS    
BEGIN   
	DECLARE @vcInclusionDetails AS VARCHAR(MAX)

	DECLARE @TEMPTABLE TABLE
	(
		ID INT IDENTITY(1,1),
		numCartID NUMERIC(18,0),
		vcChildKitItemSelection VARCHAR(MAX),
		numItemCode NUMERIC(18,0),
		bitKitParent BIT,
		numParentItemCode NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(1000)
	);

	--FIRST LEVEL CHILD
	INSERT INTO @TEMPTABLE
	(
		numCartID,vcChildKitItemSelection,numItemCode,bitKitParent,numParentItemCode,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
	)
	SELECT 
		CI.numCartId,
		CI.vcChildKitItemSelection,
		ID.numChildItemID,
		ISNULL(I.bitKitParent,0),
		0,
		CAST((CI.numUnitHour * ID.numQtyItemsReq) AS FLOAT),
		ISNULL(ID.numUOMID,0),
		1,
		CASE 
			WHEN ISNULL(I.bitKitParent,0)=1
			THEN
				CONCAT('<li><b>',I.vcItemName, ':</b><br/> ') 
			ELSE 
				CONCAT('<li>',I.vcItemName, ' (', CAST((CI.numUnitHour * ID.numQtyItemsReq) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>')
		END
	FROM 
		CartItems CI
	INNER JOIN
		ItemDetails ID
	ON
		CI.numItemCode = ID.numItemKitID
	INNER JOIN
		Item I
	ON
		ID.numChildItemID = I.numItemCode
	LEFT JOIN
		UOM
	ON
		UOM.numUOMId = (CASE WHEN ISNULL(ID.numUOMID,0) > 0 THEN ID.numUOMID ELSE I.numBaseUnit END)
	WHERE
		CI.numCartId = @numCartID
	
	
	INSERT INTO @TEMPTABLE
	(
		numCartID,vcChildKitItemSelection,numItemCode,numParentItemCode,numTotalQty,numUOMID,tintLevel,vcInclusionDetail
	)
	SELECT 
		c.numCartId,
		c.vcChildKitItemSelection,
		ID.numChildItemID,
		c.numItemCode,
		CAST((c.numTotalQty * ID.numQtyItemsReq) AS FLOAT),
		ISNULL(ID.numUOMID,0),
		2,
		CONCAT('<li>',I.vcItemName, ' (', CAST((c.numTotalQty * ID.numQtyItemsReq) AS FLOAT),' ',ISNULL(UOM.vcUnitName,'Units'),')</li>')
	FROM 
		@TEMPTABLE c
	CROSS APPLY
	(
		SELECT
			SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
			,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
		FROM 
			dbo.Split(c.vcChildKitItemSelection,',') 
	) AS t2
	INNER JOIN
		ItemDetails ID
	ON
		ID.numItemKitID = t2.numKitItemID
		AND ID.numChildItemID = t2.numKitChildItemID
		AND c.numItemCode = t2.numKitItemID
	INNER JOIN
		Item I
	ON
		t2.numKitChildItemID = I.numItemCode
	LEFT JOIN
		UOM
	ON
		UOM.numUOMId = (CASE WHEN ISNULL(ID.numUOMID,0) > 0 THEN ID.numUOMID ELSE I.numBaseUnit END)
	WHERE
		LEN(ISNULL(c.vcChildKitItemSelection,'')) > 0

	DECLARE @TEMPTABLEFINAL TABLE
	(
		ID INT IDENTITY(1,1),
		numCartID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		bitKitParent BIT,
		numParentItemCode NUMERIC(18,0),
		numTotalQty NUMERIC(18,2),
		numUOMID NUMERIC(18,0),
		tintLevel TINYINT,
		vcInclusionDetail VARCHAR(MAX)
	);

	INSERT INTO @TEMPTABLEFINAL
	(
		numCartID,
		numItemCode,
		bitKitParent,
		numParentItemCode,
		numTotalQty,
		numUOMID,
		tintLevel,
		vcInclusionDetail
	)
	SELECT
		numCartID,
		numItemCode,
		bitKitParent,
		numParentItemCode,
		numTotalQty,
		numUOMID,
		tintLevel,
		vcInclusionDetail
	FROM 
		@TEMPTABLE 
	WHERE 
		tintLevel=1
		
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT

	SELECT @iCount = COUNT(*) FROM @TEMPTABLEFINAL WHERE tintLevel=1

	WHILE @i <= @iCount
	BEGIN
		IF (ISNULL((SELECT bitKitParent FROM @TEMPTABLEFINAL WHERE ID=@i),0) = 0 OR (ISNULL((SELECT bitKitParent FROM @TEMPTABLEFINAL WHERE ID=@i),0) = 1 AND (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numCartID=(SELECT numCartID FROM @TEMPTABLEFINAL WHERE ID=@i) AND numParentItemCode =(SELECT numItemCode FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0))
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,(SELECT vcInclusionDetail FROM @TEMPTABLEFINAL WHERE ID=@i))
		END

		IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE tintLevel=2 AND numCartID=(SELECT numCartID FROM @TEMPTABLEFINAL WHERE ID=@i) AND numParentItemCode =(SELECT numItemCode FROM @TEMPTABLEFINAL WHERE ID=@i)) > 0
		BEGIN
			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,'<ul class="list-unstyled">')

			SELECT @vcInclusionDetails = COALESCE(@vcInclusionDetails,'') + vcInclusionDetail FROM @TEMPTABLE WHERE tintLevel=2 AND numCartID=(SELECT numCartID FROM @TEMPTABLEFINAL WHERE ID=@i) AND numParentItemCode =(SELECT numItemCode FROM @TEMPTABLEFINAL WHERE ID=@i)

			SET @vcInclusionDetails = CONCAT(@vcInclusionDetails,'</ul></li>')
		END

		SET @i = @i + 1
	END

	SET @vcInclusionDetails = CONCAT('<ul style="text-align:left;margin:0px;padding:0px 0px 0px 14px;">',@vcInclusionDetails,'</ul>')
	
	RETURN @vcInclusionDetails
END
GO