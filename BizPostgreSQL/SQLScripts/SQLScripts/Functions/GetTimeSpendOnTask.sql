/****** Object:  UserDefinedFunction [dbo].[GetTimeSpendOnTask]    Script Date: 2/4/2020 9:09:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[GetTimeSpendOnTask]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)    
RETURNS VARCHAR(50)    
AS
BEGIN
	DECLARE @vcTotalTimeSpendOnTask VARCHAR(50) = ''
	DECLARE @numTotalMinutes INT = 0

	DECLARE @TEMTIMELong TABLE
	(
		ID INT IDENTITY(1,1)
		,tintAction TINYINT
		,dtActionTime DATETIME
	)

	INSERT INTO @TEMTIMELong
	(
		tintAction
		,dtActionTime
	)
	SELECT
		tintAction
		,dtActionTime
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime
		,tintAction

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @tintLastAction TINYINT
	DECLARE @dtLastActionTime DATETIME

	DECLARE @tintAction TINYINT
	DECLARE @dtActionTime DATETIME
	SELECT @iCount = COUNT(*) FROM @TEMTIMELong

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@tintAction=tintAction
			,@dtActionTime=dtActionTime
		FROM
			@TEMTIMELong
		WHERE
			ID = @i

		IF @i = 1 
		BEGIN
			IF @tintAction <> 1
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END
		ELSE
		BEGIN
			IF @tintLastAction = 1 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			--ELSE IF @tintLastAction = 2 AND @tintAction <> 3
			ELSE IF @tintLastAction = 2 AND @tintAction NOT IN(3,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 3 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 4
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				IF @tintAction IN (2,4)
				BEGIN
					SET @numTotalMinutes = @numTotalMinutes + DATEDIFF(MINUTE,@dtLastActionTime,@dtActionTime)
				END

				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END

		SET @i = @i + 1
	END

	IF @vcTotalTimeSpendOnTask <> 'Invalid time sequence'
	BEGIN
		RETURN FORMAT(@numTotalMinutes / 60,'00') + ':' + FORMAT(@numTotalMinutes % 60,'00')
	END
	
	RETURN @vcTotalTimeSpendOnTask
END
