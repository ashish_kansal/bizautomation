GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetProjectName')
DROP FUNCTION GetProjectName
GO
CREATE FUNCTION [dbo].[GetProjectName](@numProId numeric(9))
	RETURNS varchar (100) 
AS
BEGIN
declare @vcProjectName as varchar (100)
SET @vcProjectName=''
select @vcProjectName=vcProjectName from ProjectsMaster where numProId=@numProId
	RETURN @vcProjectName
END
GO
