/****** Object:  UserDefinedFunction [dbo].[GetIncidents]    Script Date: 07/26/2008 18:13:03 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getincidents')
DROP FUNCTION getincidents
GO
CREATE FUNCTION [dbo].[GetIncidents](@numcontractid numeric,@numDomainId numeric,@numdivisionid as numeric)          
RETURNS numeric          
as          
begin          
declare @retvalue1 as integer       
set @retvalue1 = 0       
    
--select @retvalue1=isnull(count(*),0) from TimeAndExpense      
--where tintTeType in( 2,3)    
--and numType = 1    
--and numcontractId = @numcontractid    
--and numdomainId = @numDomainId    

select @retvalue1 = isnull(count(*),0) from cases where numcontractId = @numcontractid and numdomainId = @numDomainId
select @retvalue1 =@retvalue1+ isnull(count(*),0) from projectsmaster where numcontractId = @numcontractid and numdomainId = @numDomainId
          
return @retvalue1         
          
end
GO
