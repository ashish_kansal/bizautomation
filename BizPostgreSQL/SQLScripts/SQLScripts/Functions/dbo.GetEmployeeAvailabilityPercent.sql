GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='GetEmployeeAvailabilityPercent')
DROP FUNCTION GetEmployeeAvailabilityPercent
GO
CREATE FUNCTION [dbo].[GetEmployeeAvailabilityPercent]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@ClientTimeZoneOffset INT
)    
RETURNS @EmployeeAvailability TABLE
(	
	numTotalAvailableMinutes NUMERIC(18,0)
	,numAvailabilityPercent NUMERIC(18,0)
	,numProductiveMinutes NUMERIC(18,0)
	,numProductivityPercent NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	
	SELECT 
		@numWorkScheduleID=WS.ID
		,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
		,@vcWorkDays=CONCAT(',',vcWorkDays,',')
	FROM 
		UserMaster 
	INNER JOIN
		WorkSchedule WS
	ON
		WS.numUserCntID = UserMaster.numUserDetailId
	WHERE 
		UserMaster.numDomainID = @numDomainID 
		AND UserMaster.numUserDetailID=@numUserCntID

	DECLARE @numTotalProductiveMinutes NUMERIC(18,0) = 0
	DECLARE @numTotalAbsentMinutes NUMERIC(18,0) = 0
	DECLARE @numActualProductiveMinutes NUMERIC(18,0) = ISNULL(dbo.GetEmployeeProductiveMinutes(@numDomainID,@numUserCntID,@dtFromDate,@dtToDate,@ClientTimeZoneOffset),0)

	WHILE @dtFromDate <= @dtToDate
	BEGIN
		IF EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE TimeAndExpense.numDomainID = @numDomainID AND TimeAndExpense.numUserCntID=@numUserCntID AND ISNULL(numCategory,0) = 1 AND numType = 4)
		BEGIN
			SET @numTotalAbsentMinutes = ISNULL(@numTotalAbsentMinutes,0) + @numProductiveTimeInMinutes
		END 
		ELSE IF CHARINDEX(CONCAT(',',DATEPART(WEEKDAY,@dtFromDate),','),@vcWorkDays) > 0 AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,@dtFromDate) BETWEEN dtDayOffFrom AND dtDayOffTo)
		BEGIN
			SET @numTotalProductiveMinutes = ISNULL(@numTotalProductiveMinutes,0) + @numProductiveTimeInMinutes
		END

		 SET @dtFromDate = DATEADD(DAY,1,@dtFromDate)
	END

	INSERT INTO @EmployeeAvailability
	(
		numTotalAvailableMinutes
		,numAvailabilityPercent
		,numProductiveMinutes
		,numProductivityPercent
	)
	VALUES
	(
		@numTotalProductiveMinutes - @numTotalAbsentMinutes
		,(CASE WHEN ISNULL(@numTotalProductiveMinutes,0) > 0 THEN (((@numTotalProductiveMinutes - @numTotalAbsentMinutes) * 100) / @numTotalProductiveMinutes) ELSE 0 END)
		,@numActualProductiveMinutes
		,(CASE WHEN ISNULL(@numTotalProductiveMinutes,0) > 0 THEN (@numActualProductiveMinutes * 100) / @numTotalProductiveMinutes ELSE 0 END)
	)

	RETURN 
END
GO