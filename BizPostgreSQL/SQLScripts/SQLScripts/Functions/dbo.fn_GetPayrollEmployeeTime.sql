GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetPayrollEmployeeTime')
DROP FUNCTION fn_GetPayrollEmployeeTime
GO
CREATE FUNCTION [dbo].[fn_GetPayrollEmployeeTime] 
(
    @numDomainID NUMERIC(18,0)
	,@numUserCntID INT
	,@dtFromDate DATE
	,@dtToDate DATE
	,@ClientTimeZoneOffset INT
)
RETURNS @EmployeeTime TABLE
(	
	numRecordID NUMERIC(18,0)
	,tintRecordType TINYINT
	,vcSource VARCHAR(200)
	,dtDate DATE
	,vcDate VARCHAR(50)
	,numType TINYINT
	,vcType VARCHAR(500)
	,vcHours VARCHAR(20)
	,numMinutes INT
	,vcDetails VARCHAR(MAX)
	,numApprovalComplete INT
)
AS 
BEGIN

	-- Time & Expense
	INSERT INTO @EmployeeTime
	(
		numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete
	)
	SELECT
		TimeAndExpense.numCategoryHDRID
		,1
		,'Time & Expense' AS vcSource
		,dtFromDate
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,dtFromDate),@numDomainID) AS vcDate
		,TimeAndExpense.numType
		,(CASE 
			WHEN TimeAndExpense.numType = 4 THEN 'Absence (Un-Paid)' 
			WHEN TimeAndExpense.numType = 3 THEN 'Absence (Paid)' 
			WHEN TimeAndExpense.numType = 2 Then 'Non-Billable Time' 
			WHEN TimeAndExpense.numType = 1 Then 'Billable' 
			ELSE '' 
		END) AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate),0),108) vcHours
		,DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate) numMinutes
		,ISNULL(TimeAndExpense.txtDesc,'') vcDetails
		,ISNULL(numApprovalComplete,0) numApprovalComplete
	FROM
		TimeAndExpense 
	LEFT JOIN
		OpportunityMaster
	ON
		TimeAndExpense.numOppID=OpportunityMaster.numOppID
	WHERE
		TimeAndExpense.numDomainID = @numDomainID
		AND TimeAndExpense.numUserCntID=@numUserCntID
		AND ISNULL(numCategory,0) = 1
		AND numType NOT IN (4) -- DO NOT INCLUDE UNPAID LEAVE
		AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,dtFromDate) AS Date) BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		dtFromDate

	-- Action Item
	INSERT INTO @EmployeeTime
	(
		numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete
	)
	SELECT
		Communication.numCommId
		,2
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Communication.numCommId,'&lngType=0">'
				,dbo.GetListIemName(bitTask),'</a>',(CASE WHEN DM.numDivisionID > 0 THEN CONCAT(' (<a target="_blank" href="',(CASE WHEN DM.tintCRMType = 2 THEN '../account/frmAccounts.aspx?DivID=' WHEN DM.tintCRMType = 1 THEN '../prospects/frmProspects.aspx?DivID=' ELSE '../Leads/frmLeads.aspx?DivID=' END),Dm.numDivisionID,'">',ISNULL(CI.vcCompanyName,'-'),'</a>)') ELSE '' END)
		) AS vcSource
		,dtStartTime
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,Communication.dtStartTime),@numDomainID) AS vcDate
		,0
		,dbo.GetListIemName(bitTask) AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,Communication.dtStartTime,Communication.dtEndTime),0),108) vcHours
		,DATEDIFF(MINUTE,Communication.dtStartTime,Communication.dtEndTime) numMinutes
		,ISNULL(Communication.textDetails,'') vcDetails
		,0
	FROM
		Communication 
	LEFT JOIN
		DivisionMaster DM
	ON
		Communication.numDivisionId = DM.numDivisionID
	LEFT JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		Communication.numDomainID = @numDomainID
		AND Communication.numAssign=@numUserCntID
		AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,Communication.dtStartTime) AS Date) BETWEEN @dtFromDate AND @dtToDate
		AND ISNULL(bitClosedFlag,0) = 1
	ORDER BY
		dtStartTime

	-- Calendar
	INSERT INTO @EmployeeTime
	(
		numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete
	)
	SELECT
		Activity.ActivityID
		,3
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Activity.ActivityID,'&lngType=1">','Action Item','</a>') AS vcSource
		,Activity.StartDateTimeUtc
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,Activity.StartDateTimeUtc),@numDomainID) AS vcDate
		,0
		,'Calendar' AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,Activity.StartDateTimeUtc,DATEADD(SECOND,Activity.Duration,Activity.StartDateTimeUtc)),0),108) vcHours
		,DATEDIFF(MINUTE,Activity.StartDateTimeUtc,DATEADD(SECOND,Activity.Duration,Activity.StartDateTimeUtc)) numMinutes
		,ISNULL(Activity.ActivityDescription,'') vcDetails
		,0
	FROM
		[Resource]
	INNER JOIN
		ActivityResource 
	ON
		[Resource].ResourceID = ActivityResource.ResourceID
	INNER JOIN
		Activity 
	ON
		ActivityResource.ActivityID=Activity.ActivityID
	WHERE
		[Resource].numUserCntId = @numUserCntID
		AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1, Activity.StartDateTimeUtc) AS Date) BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		Activity.StartDateTimeUtc

	-- Work Order Tasks
	INSERT INTO @EmployeeTime
	(
		numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
		,numApprovalComplete
	)
	SELECT 
		WO.numWOId
		,4
		,CONCAT('<a target="_blank" href="../Items/frmWorkOrder.aspx?WOID=',WO.numWOId,'">',ISNULL(WO.vcWorkOrderName,'-'),'</a>'
				,(CASE WHEN OM.numOppID > 0 THEN CONCAT(' (<a target="_blank" href="../opportunity/frmOpportunities.aspx?opId=',OM.numOppId,'">',ISNULL(OM.vcPOppName,'-'),'</a>)') ELSE '' END)
				,(CASE WHEN DM.numDivisionID > 0 THEN CONCAT(' (<a target="_blank" href="',(CASE WHEN DM.tintCRMType = 2 THEN '../account/frmAccounts.aspx?DivID=' WHEN DM.tintCRMType = 1 THEN '../prospects/frmProspects.aspx?DivID=' ELSE '../Leads/frmLeads.aspx?DivID=' END),Dm.numDivisionID,'">',ISNULL(CI.vcCompanyName,'-'),'</a>)') ELSE '' END)
			) vcWorkOrderName
		,SPDTTLStart.dtActionTime
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,SPDTTLStart.dtActionTime),@numDomainID) AS vcDate
		,0
		,'Work Order' AS vcType
		,CONCAT(FORMAT(ISNULL(DATEDIFF(MINUTE,SPDTTLStart.dtActionTime,TEMP.dtActionTime),0) / 60,'00'),':',FORMAT(ISNULL(DATEDIFF(MINUTE,SPDTTLStart.dtActionTime,TEMP.dtActionTime),0) % 60,'')) vcHours
		,ISNULL(DATEDIFF(MINUTE,SPDTTLStart.dtActionTime,TEMP.dtActionTime),0) numMinutes
		,ISNULL(SPDT.vcTaskName,'') vcDetails
		,0
	FROM 
		StagePercentageDetailsTask SPDT
	INNER JOIN
		WorkOrder WO
	ON
		SPDT.numWorkOrderId = WO.numWOId
	LEFT JOIN
		OpportunityMaster OM
	ON
		WO.numOppId = OM.numOppId
	LEFT JOIN
		DivisionMaster DM
	ON
		OM.numDivisionId = DM.numDivisionID
	LEFT JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	INNER JOIN
		StagePercentageDetailsTaskTimeLog  SPDTTLStart
	ON
		SPDT.numTaskId = SPDTTLStart.numTaskID
	CROSS APPLY
	(
		SELECT
			TEMP.dtActionTime
		FROM
		(
			SELECT TOP 1
				SPDTTLEnd.dtActionTime
			FROM
				StagePercentageDetailsTaskTimeLog  SPDTTLEnd
			WHERE
				SPDTTLEnd.numDomainID = @numDomainID
				AND SPDTTLEnd.numUserCntID = @numUserCntID
				AND SPDTTLStart.numTaskID = SPDTTLEnd.numTaskID
				AND SPDTTLEnd.tintAction IN (2,4)
				AND SPDTTLEnd.dtActionTime >= SPDTTLStart.dtActionTime
			ORDER BY	
				dtActionTime
		) TEMP
		WHERE
			DATEDIFF(HOUR,SPDTTLStart.dtActionTime,TEMP.dtActionTime) < 24
	) TEMP
	WHERE 
		SPDT.numDomainID = @numDomainID
		AND ISNULL(SPDT.numWorkOrderId,0) > 0
		AND SPDTTLStart.numDomainID = @numDomainID 
		AND SPDTTLStart.numUserCntID=@numUserCntID 
		AND SPDTTLStart.tintAction IN (1,3) 
		AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,SPDTTLStart.dtActionTime) AS Date) BETWEEN @dtFromDate AND @dtToDate

	RETURN 
END
GO