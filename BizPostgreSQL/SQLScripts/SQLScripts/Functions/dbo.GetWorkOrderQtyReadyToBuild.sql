GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderQtyReadyToBuild')
DROP FUNCTION GetWorkOrderQtyReadyToBuild
GO
CREATE FUNCTION [dbo].[GetWorkOrderQtyReadyToBuild] 
(
	@numWOID NUMERIC(18,0)
	,@numQty FLOAT
)
RETURNS FLOAT
AS 
BEGIN
	DECLARE @fltQtyReadyToBuild FLOAT = @numQty

	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numWOId = @numWOID AND numWOStatus <> 23184)
	BEGIN
		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numOppID NUMERIC(18,0)
			,numOppItemID NUMERIC(18,0)
			,numOppChildItemID NUMERIC(18,0)
			,numOppKitChildItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,vcItemName VARCHAR(MAX)
			,numQty FLOAT
			,numQtyReq_Orig FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus BIT
			,numQtyReadyToBuild FLOAT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numOppID,numOppItemID,numOppChildItemID,numOppKitChildItemID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus,numQtyReadyToBuild) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numOppID,
				numOppItemID,
				numOppChildItemID,
				numOppKitChildItemID,
				numItemCode,
				@numQty,
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus,
				(CASE WHEN numWOStatus = 23184 THEN CAST(numQtyItemsReq AS FLOAT) ELSE CAST(0 AS FLOAT) END)
			FROM
				WorkOrder
			WHERE
				numWOId = @numWOID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numOppID,
				WorkOrder.numOppItemID,
				WorkOrder.numOppChildItemID,
				WorkOrder.numOppKitChildItemID,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus,
				(CASE WHEN numWOStatus = 23184 THEN CAST(WorkOrder.numQtyItemsReq AS FLOAT) ELSE CAST(0 AS FLOAT) END)
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)

		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numOppID,
			numOppItemID,
			numOppChildItemID,
			numOppKitChildItemID,
			numItemCode,
			vcItemName,
			CTEWorkOrder.numQtyItemsReq,
			0,
			CTEWorkOrder.numWarehouseItemID,
			bitWorkOrder,
			tintBuildStatus,
			0
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		UPDATE
			T1
		SET
			numQtyReq_Orig = (SELECT WorkOrderDetails.numQtyItemsReq_Orig FROM WorkOrderDetails INNER JOIN WorkOrder ON WorkOrderDetails.numWOId=WorkOrder.numWOId WHERE WorkOrder.numWOId = T1.numParentWOID AND WorkOrderDetails.numChildItemID=T1.numItemCode AND WorkOrderDetails.numWareHouseItemId=T1.numWarehouseItemID AND WorkOrder.numOppId=T1.numOppID AND WorkOrder.numOppItemID=T1.numOppItemID AND WorkOrder.numOppChildItemID=T1.numOppChildItemID AND WorkOrder.numOppKitChildItemID=T1.numOppKitChildItemID)
		FROM
			@TEMP T1
		WHERE	
			numParentWOID > 0

		INSERT INTO 
			@TEMP
		SELECT
			t1.ItemLevel + 1,
			t1.numWOID,
			NULL,
			numOppID,
			numOppItemID,
			numOppChildItemID,
			numOppKitChildItemID,
			WorkOrderDetails.numChildItemID,
			Item.vcItemName,
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numQtyItemsReq_Orig,
			WorkOrderDetails.numWarehouseItemID,
			0 AS bitWorkOrder,
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN 2
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN 1=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
							END
						ELSE 1
					END)
			END),
			(CASE 
				WHEN t1.tintBuildStatus = 2 
				THEN WorkOrderDetails.numQtyItemsReq
				ELSE
					(CASE 
						WHEN Item.charItemType='P'
						THEN
							CASE 
								WHEN 1=2  
								THEN (CASE WHEN ISNULL(numQtyItemsReq,0) <= WareHouseItems.numOnHand THEN (WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig) ELSE CAST(ISNULL(numOnHand,0)/numQtyItemsReq_Orig AS INT) END)
								ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) <= WareHouseItems.numAllocation THEN (WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig) ELSE CAST(ISNULL(numAllocation,0)/numQtyItemsReq_Orig AS INT) END) 
							END
						ELSE (WorkOrderDetails.numQtyItemsReq/numQtyItemsReq_Orig)
					END)
			END)
		FROM
			WorkOrderDetails
		INNER JOIN
			@TEMP t1
		ON
			WorkOrderDetails.numWOId = t1.numWOID
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
			AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
		LEFT JOIN
			WareHouseItems
		ON
			WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempWOID NUMERIC(18,0)
		INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

		SELECT @iCount=COUNT(*) FROM @TEMPWO

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

			UPDATE
				T1
			SET
				numQtyReadyToBuild = (CASE WHEN ISNULL(numQty,0) <= (SELECT MIN(numQtyReadyToBuild) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID) THEN (numQty/numQtyReq_Orig) ELSE CAST(ISNULL((SELECT MIN(numQtyReadyToBuild) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID),0)/numQtyReq_Orig AS INT) END) 
			FROM
				@TEMP T1
			WHERE
				numWOID=@numTempWOID
				AND numParentWOID > 0

			SET @i = @i + 1
		END

		SET @fltQtyReadyToBuild = (SELECT MIN(numQtyReadyToBuild) FROM @TEMP WHERE numParentWOID=@numWOID)
	END

	RETURN @fltQtyReadyToBuild
END