/****** Object:  UserDefinedFunction [dbo].[GetCustFldValue]    Script Date: 07/26/2008 18:12:59 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcustfldvalue')
DROP FUNCTION getcustfldvalue
GO
CREATE FUNCTION [dbo].[GetCustFldValue](@numFldId numeric(9),@pageId as tinyint,@numRecordId as numeric(9))    
 RETURNS varchar (1000)     
AS    
BEGIN    

DECLARE @Fld_type AS VARCHAR(100)
DECLARE @vcURL AS VARCHAR(MAX)
SELECT @Fld_type = ISNULL(Fld_type,''),@vcURL = ISNULL(vcURL,'') FROM CFW_Fld_Master WHERE Fld_id = @numFldId
    
declare @vcValue as  varchar(1000)    
if @pageId=1    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=4    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values_Cont where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=3    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values_Case where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
    
if @pageId=5    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values_Item where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=2 or @pageId=6    
begin    
 select @vcValue=Fld_Value from CFW_Fld_Values_Opp where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=7 or @pageId=8    
begin    
 select @vcValue=Fld_Value from CFW_Fld_Values_Product where Fld_ID=@numFldId and RecId=@numRecordId    
    
end   
  
if @pageId=9  
begin  
 select @vcValue=Fld_Value from CFW_Fld_Values_Serialized_Items where Fld_ID=@numFldId and RecId=@numRecordId    
    
end  
if @pageId=11  
begin  
 select @vcValue=Fld_Value from CFW_Fld_Values_Pro where Fld_ID=@numFldId and RecId=@numRecordId    
    
end  
    
if @vcValue is null set @vcValue= (CASE WHEN @Fld_type = 'Link' THEN @vcURL WHEN @Fld_type='SelectBox' OR @Fld_type='CheckBox' THEN '0' ELSE '' END)
    
RETURN @vcValue    
    
END
GO
