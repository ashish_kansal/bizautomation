GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrganizationPriceLevelName')
DROP FUNCTION GetOrganizationPriceLevelName
GO
CREATE FUNCTION [dbo].[GetOrganizationPriceLevelName] 
( 
	@numDomainID NUMERIC(18,0)
	,@tintPriceLevel TINYINT 
)
RETURNS VARCHAR(300)
AS
BEGIN
	DECLARE @vcPriceLevelName VARCHAR(300) = ''

	SELECT 
		@vcPriceLevelName = ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',pnt.tintPriceLevel))
	FROM
		PricingNamesTable pnt
	WHERE
		pnt.numDomainID = @numDomainID
		AND pnt.tintPriceLevel = @tintPriceLevel

	RETURN ISNULL(@vcPriceLevelName,'')
END
GO