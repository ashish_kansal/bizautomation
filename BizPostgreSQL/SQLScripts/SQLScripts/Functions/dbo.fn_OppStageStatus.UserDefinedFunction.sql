/****** Object:  UserDefinedFunction [dbo].[fn_OppStageStatus]    Script Date: 07/26/2008 18:12:49 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
--Created By: Debasish Nag    
--Created On: 6th Jan 2006    
--This function returns Last Status Stage of the Opportunity
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_oppstagestatus')
DROP FUNCTION fn_oppstagestatus
GO
CREATE FUNCTION [dbo].[fn_OppStageStatus](@numOppID numeric, @vcReturnData NVarchar(20))
RETURNS Nvarchar(100) 
AS
BEGIN
	DECLARE @OppStatusReturnEntity as NVarchar(100)
        IF @numOppID > 0
        BEGIN
	 SELECT TOP 1 @OppStatusReturnEntity = numStage FROM dbo.OpportunityStageDetails WHERE numOppId = IsNull(@numOppID,0) AND bitStageCompleted = 1 ORDER BY numStagePercentage,numOppStageId DESC
         IF @vcReturnData = 'numStatusName'
	 SELECT @OppStatusReturnEntity = dbo.fn_AdvSearchColumnName(IsNull(@OppStatusReturnEntity,0),'L')
        END
        ELSE
        SELECT @OppStatusReturnEntity = ''

	RETURN @OppStatusReturnEntity
END
GO
