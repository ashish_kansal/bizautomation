/****** Object:  UserDefinedFunction [dbo].[fn_GetProcurementBudgetMonthTotalAmt]    Script Date: 07/26/2008 18:12:43 ******/

GO

GO
--Created By Siva                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getprocurementbudgetmonthtotalamt')
DROP FUNCTION fn_getprocurementbudgetmonthtotalamt
GO
CREATE FUNCTION [dbo].[fn_GetProcurementBudgetMonthTotalAmt]             
(@numProcurementId numeric(9),@stDate datetime,@numItemGroupID numeric(9)=0,@numDomainId numeric(9))                
Returns varchar(10)              
As                
Begin                 
    Declare @monAmount as DECIMAL(20,5)            
    Declare @montotalAmt as DECIMAL(20,5)           
    Declare @numMonth as tinyint         
    Declare @montotAmt as varchar(10)                   
    Set @numMonth =1          
    Set @montotalAmt=0       
 Set @monAmount=0  
 While @numMonth<=12        
Begin        
  Declare @dtFiscalStDate as datetime                          
  Declare @dtFiscalEndDate as datetime                     
  Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@stDate,@numDomainId),@numDomainId)                       
  Set @dtFiscalStDate=dateadd(month,@numMonth-1,@dtFiscalStDate)                    
  --Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@stDate,@numDomainId),@numDomainId)))                    
  Select @monAmount= isnull(PBD.monAmount,0) From ProcurementBudgetMaster PBM
  inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId
  Where PBM.numDomainId=@numDomainId And PBD.numItemGroupID=@numItemGroupID And PBD.numProcurementId=@numProcurementId and PBD.tintmonth=month(@dtFiscalStDate) And PBD.intyear=year(@dtFiscalStDate)        

  Set  @montotalAmt =@montotalAmt+@monAmount        
  Set @numMonth=@numMonth+1        
 End        
    if @montotalAmt=0.00 Set @montotAmt=''  Else Set @montotAmt=@montotalAmt      
                  
 Return @montotAmt                
End
GO
