GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_UOMConversion')
DROP FUNCTION fn_UOMConversion
GO
CREATE FUNCTION [dbo].[fn_UOMConversion]
(
	@numFromUnit NUMERIC,
	@numItemCode NUMERIC,
	@numDomainID NUMERIC,
	@numToUnit NUMERIC=null
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @bitEnableItemLevelUOM AS BIT = 0
	DECLARE @MultiplicationFactor FLOAT

	SELECT @bitEnableItemLevelUOM=ISNULL(bitEnableItemLevelUOM,0) FROM Domain WHERE numDomainID = @numDomainID
 
	IF ISNULL(@numToUnit,0) = 0
		SELECT @numToUnit=numBaseUnit FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

	IF EXISTS (SELECT @numToUnit WHERE @numToUnit IS null)
		RETURN 1
 
	IF(@numFromUnit=@numToUnit)
		RETURN 1

	IF @bitEnableItemLevelUOM = 0
	BEGIN
		;with recursiveTable(FROMUnit,LevelNum,numUOM1,decConv1,numUOM2,decConv2,multiplicationFactor) as
		(
			select  numUOM1,0 as LevelNum
					,ct.numUOM1,ct.decConv1,ct.numUOM2,ct.decConv2
					,CAST((ct.decConv2 / CAST(ct.decConv1 AS DECIMAL(18,4))) AS DECIMAL(18,4)) as multiplicationFactor
			from UOMConversion ct
			where   ct.numUOM1 = @numFromUnit AND ct.numDomainID=@numDomainID

			union all

			select  FROMUnit,LevelNum + 1
					,rt.numUOM2,rt.decConv2,ct.numUOM2,ct.decConv2
					,CAST((rt.multiplicationFactor *(ct.decConv2 / CAST(ct.decConv1 AS DECIMAL(18,4)))) AS DECIMAL(18,4))
			from UOMConversion ct
			inner join recursiveTable rt on rt.numUOM2 = ct.numUOM1 WHERE ct.numDomainID=@numDomainID
		)

		select top 1 @MultiplicationFactor=multiplicationFactor from recursiveTable where FROMUnit = @numFromUnit and numUOM2 = @numToUnit

		IF EXISTS (SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT null)
		BEGIN
			RETURN @MultiplicationFactor	
		END 
		ELSE
		BEGIN
			;with recursiveTable(FROMUnit,LevelNum,numUOM1,decConv1,numUOM2,decConv2,multiplicationFactor) as
			(
				select  numUOM2,0 as LevelNum
						,ct.numUOM1,ct.decConv1,ct.numUOM2,ct.decConv2
						,CONVERT(float,ct.decConv1 / ct.decConv2) as multiplicationFactor
				from UOMConversion ct
				where   ct.numUOM2 = @numFromUnit AND ct.numDomainID=@numDomainID

				union all

				select  FROMUnit,LevelNum + 1
						,ct.numUOM1,ct.decConv1,ct.numUOM2,ct.decConv2
						,CONVERT(float,rt.multiplicationFactor * CONVERT(float,(ct.decConv1 / ct.decConv2)))
				from UOMConversion ct
				inner join recursiveTable rt on rt.numUOM1 = ct.numUOM2 WHERE ct.numDomainID=@numDomainID
			)

			select top 1 @MultiplicationFactor=multiplicationFactor from recursiveTable where FROMUnit = @numFromUnit and numUOM1 = @numToUnit

			IF EXISTS(SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT null)
			BEGIN
				RETURN @MultiplicationFactor	
			END 
		END
	END
	ELSE
	BEGIN
		;WITH CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS
		(
			SELECT  
				IUOM.numSourceUOM,
				0 AS LevelNum,
				IUOM.numSourceUOM,
				IUOM.numTargetUOM,
				IUOM.numTargetUnit
			FROM 
				ItemUOMConversion IUOM
			WHERE   
				IUOM.numDomainID=@numDomainID
				AND IUOM.numItemCode = @numItemCode
				AND IUOM.numSourceUOM = @numFromUnit
			UNION ALL
			SELECT  
				cte.FROMUnit,
				LevelNum + 1,
				IUOM.numSourceUOM,
				IUOM.numTargetUOM,
				(IUOM.numTargetUnit * cte.numTargetUnit)
			FROM 
				ItemUOMConversion IUOM
			INNER JOIN 
				CTE cte 
			ON 
				IUOM.numSourceUOM = cte.numTargetUOM 
			WHERE 
				IUOM.numDomainID=@numDomainID
				AND IUOM.numItemCode = @numItemCode
		)

		SELECT TOP 1 @MultiplicationFactor=numTargetUnit FROM CTE WHERE FROMUnit = @numFromUnit AND numTargetUOM = @numToUnit

		IF EXISTS (SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT NULL)
		BEGIN
			RETURN @MultiplicationFactor	
		END
		ELSE
		BEGIN
			;WITH CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS
			(
				SELECT  
					IUOM.numTargetUOM,
					0 AS LevelNum,
					IUOM.numSourceUOM,
					IUOM.numTargetUOM,
					(1 / IUOM.numTargetUnit)
				FROM 
					ItemUOMConversion IUOM
				WHERE   
					IUOM.numDomainID=@numDomainID
					AND IUOM.numItemCode = @numItemCode
					AND IUOM.numTargetUOM = @numFromUnit
				UNION ALL
				SELECT  
					cte.FROMUnit,
					LevelNum + 1,
					IUOM.numSourceUOM,
					IUOM.numTargetUOM,
					((1 / IUOM.numTargetUnit) * cte.numTargetUnit)
				FROM 
					ItemUOMConversion IUOM
				INNER JOIN 
					CTE cte 
				ON 
					IUOM.numTargetUOM = cte.numSourceUOM 
				WHERE 
					IUOM.numDomainID=@numDomainID
					AND IUOM.numItemCode = @numItemCode
			)

			select top 1 @MultiplicationFactor=numTargetUnit from cte where FROMUnit = @numFromUnit and numSourceUOM = @numToUnit

			IF EXISTS(SELECT @MultiplicationFactor WHERE @MultiplicationFactor IS NOT null)
			BEGIN
				RETURN @MultiplicationFactor	
			END 
		END
	END

RETURN 1
END
GO
