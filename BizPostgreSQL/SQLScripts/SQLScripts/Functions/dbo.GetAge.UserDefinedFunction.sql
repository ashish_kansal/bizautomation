/****** Object:  UserDefinedFunction [dbo].[GetAge]    Script Date: 07/26/2008 18:12:54 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAge')
DROP FUNCTION GetAge
GO
CREATE FUNCTION [dbo].[GetAge] (@DOB datetime, @Today Datetime) RETURNS Int
AS 
Begin
Declare @Age As Int
Set @Age = Year(@Today) - Year(@DOB)
If Month(@Today) < Month(@DOB) 
Set @Age = @Age -1
If Month(@Today) = Month(@DOB) and Day(@Today) < Day(@DOB) 
Set @Age = @Age - 1
Return @AGE
End
GO
