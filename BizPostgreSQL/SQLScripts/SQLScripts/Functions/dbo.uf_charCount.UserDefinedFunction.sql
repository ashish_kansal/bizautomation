GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='uf_charcount')
DROP FUNCTION uf_charcount
GO
CREATE FUNCTION [dbo].[uf_charCount] (@string varchar(8000), @character char(1))
RETURNS SMALLINT  
AS
BEGIN 
	DECLARE @stringtrunc varchar(8000); 
	SET @stringtrunc = REPLACE(@string,@character,''); -- remove the specified character 
	RETURN (LEN(@string) - LEN(@stringtrunc)); -- return the difference in length, this is the char count
END