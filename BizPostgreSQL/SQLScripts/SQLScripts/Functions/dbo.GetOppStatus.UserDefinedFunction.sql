/****** Object:  UserDefinedFunction [dbo].[GetOppStatus]    Script Date: 07/26/2008 18:13:06 ******/

GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getoppstatus')
DROP FUNCTION getoppstatus
GO
CREATE FUNCTION [dbo].[GetOppStatus](@numOppid numeric(9))
	RETURNS varchar (100) 
AS
BEGIN
declare @status as tinyint
declare @Shipping as tinyint
declare @vcStatus as varchar(100)
select @status=tintOppStatus,@Shipping=isnull(tintShipped,0) from OpportunityMaster where numOppid=@numOppid


if @status =1 
begin

if @Shipping=0 set @vcStatus='Deal Won'

if @Shipping=1 set @vcStatus='Deal Won(Completed)'

end

else if @status=2 set @vcStatus='Deal Lost'

else set @vcStatus='Open'

	RETURN @vcStatus
END
GO
