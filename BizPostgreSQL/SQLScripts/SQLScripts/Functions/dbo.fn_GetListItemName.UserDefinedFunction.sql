/****** Object:  UserDefinedFunction [dbo].[fn_GetListItemName]    Script Date: 07/26/2008 18:12:38 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getlistitemname')
DROP FUNCTION fn_getlistitemname
GO
CREATE FUNCTION [dbo].[fn_GetListItemName](@numlistID numeric)
Returns varchar(100) 
As
BEGIN
	declare @listName as varchar(100)

	Select @listName=isnull(vcdata,'') from listdetails where numlistitemid=@numlistID

return isnull(@listName,'-')
END
GO
