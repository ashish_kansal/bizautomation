
GO
/****** Object:  UserDefinedFunction [dbo].[ActSchedule]    Script Date: 07/26/2008 18:12:25 ******/

GO

GO
-- created by anoop jayaraj  
--gives complete schedule of action item on a particularday for an user  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='actschedule')
DROP FUNCTION actschedule
GO
CREATE FUNCTION [dbo].[ActSchedule](@numUserCntID as numeric(9),@dtStartDate datetime,@tintTask int,@offSet int)  
returns varchar(1000)  
as  
begin  
declare @vcSchedule as varchar(1000)  
declare @numCommId as numeric(9)  
declare @dtEnd as datetime  
  
set @dtEnd=DateAdd(day, 1, @dtStartDate)  
  
set @numCommId=0  
set @vcSchedule=''  
if @tintTask>0  
begin  
 select top 1 @numCommId=numCommId,@vcSchedule=right(convert(varchar, DateAdd(minute,-@offSet, dtStartTime)), 8) +'-'+right(convert(varchar, DateAdd(minute, -@offSet, dtEndTime)), 8) from Communication   
 where numAssign=@numUserCntID and ((DateAdd(minute, -@offSet, dtStartTime) between @dtStartDate and @dtEnd) or (DateAdd(minute, -@offSet, dtEndTime) between @dtStartDate and @dtEnd)) and bitTask=@tintTask and bitClosedFlag=0  
 while @numCommId>0  
 begin  
  select top 1 @numCommId=numCommId,@vcSchedule=@vcSchedule+', '+right(convert(varchar, DateAdd(minute,-@offSet, dtStartTime)), 8) +'-'+right(convert(varchar, DateAdd(minute, -@offSet, dtEndTime)), 8) from Communication   
  where numAssign=@numUserCntID 
and ((DateAdd(minute, -@offSet, dtStartTime) between @dtStartDate and @dtEnd) or 
(DateAdd(minute, -@offSet, dtEndTime) between @dtStartDate and @dtEnd)) and bitTask=@tintTask and bitClosedFlag=0   
  and numCommId>@numCommId  
    
  if @@rowcount=0 set @numCommId=0  
 end  
end  
else  
begin  
 select top 1 @numCommId=numCommId,@vcSchedule=right(convert(varchar, DateAdd(minute,-@offSet, dtStartTime)), 8) +'-'+right(convert(varchar, DateAdd(minute, -@offSet, dtEndTime)), 8) from Communication   
 where numAssign=@numUserCntID and ((DateAdd(minute, -@offSet, dtStartTime) between @dtStartDate and @dtEnd) or (DateAdd(minute, -@offSet, dtEndTime) between @dtStartDate and @dtEnd)) and bitClosedFlag=0 and bitTask in(1,2)  
 while @numCommId>0  
 begin  
  select top 1 @numCommId=numCommId,@vcSchedule=@vcSchedule+', '+right(convert(varchar, DateAdd(minute,-@offSet, dtStartTime)), 8) +'-'+right(convert(varchar, DateAdd(minute, -@offSet, dtEndTime)), 8) from Communication   
  where numAssign=@numUserCntID and ((DateAdd(minute, -@offSet, dtStartTime) between @dtStartDate and @dtEnd) or (DateAdd(minute, -@offSet, dtEndTime) between @dtStartDate and @dtEnd))   
  and bitClosedFlag=0 and numCommId>@numCommId and bitTask in(1,2)  
  if @@rowcount=0 set @numCommId=0  
 end  
end  
  
  
  
return Replace(@vcSchedule, 'M', '')  
end
GO
