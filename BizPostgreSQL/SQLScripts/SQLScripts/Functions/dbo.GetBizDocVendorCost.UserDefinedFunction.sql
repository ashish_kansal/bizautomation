/*Will return sum total of Vendor cost*/
--SELECT * FROM OpportunityBizDocItems
--SELECT [dbo].[GetBizDocVendorCost](5424)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetBizDocVendorCost')
DROP FUNCTION GetBizDocVendorCost
GO
CREATE FUNCTION [dbo].[GetBizDocVendorCost]
    (
      @numOppBizDocID NUMERIC
    )
RETURNS DECIMAL(10, 2)
AS BEGIN

    DECLARE @VendorCost DECIMAL(10, 2)

    SELECT  @VendorCost = SUM(ISNULL(V.[monCost], 0) * ISNULL(BI.[numUnitHour], 0))
    FROM    [OpportunityBizDocItems] BI
            INNER JOIN Item I ON I.[numItemCode] = BI.[numItemCode]
            LEFT OUTER JOIN [Vendor] V ON V.[numVendorID] = I.[numVendorID]
                                          AND V.[numItemCode] = I.[numItemCode]
    WHERE   BI.[numOppBizDocID] = @numOppBizDocID

    RETURN @VendorCost
   END 