/****** Object:  UserDefinedFunction [dbo].[fn_GetBudgetMonthDet]    Script Date: 07/26/2008 18:12:29 ******/

GO

GO
--Created By Siva        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getbudgetmonthdet')
DROP FUNCTION fn_getbudgetmonthdet
GO
CREATE FUNCTION [dbo].[fn_GetBudgetMonthDet]        
(@numBudgetId numeric(9),@tintMonth tinyint,@numYear numeric(9),@numDomainId numeric(9),@numChartAcntId numeric(9)=0)        
Returns varchar(10)        
As        
Begin         
    Declare @monAmount as varchar(10)        
      
    Select @monAmount=OBD.monAmount From OperationBudgetMaster OBM
    inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId
    Where OBD.numBudgetId=@numBudgetId And OBD.numChartAcntId=@numChartAcntId and OBD.tintMonth=@tintMonth And OBD.intYear=@numYear
    And OBM.numDomainId=@numDomainId        
----       
---- Set @vcmonth = Case @tintMonth when 1 then 'Jan' when 2 then 'Feb' when 3 then 'Mar' when 4 then 'Apr' when 5 then 'May' when 6 then 'Jun' when 7 then 'Jul'        
---- when 8 then 'Aug' when 9 then 'Sep' when 10 then 'Oct' when 11 then 'Nov' when 12 then 'Dec' End      
     if @monAmount='0.00' Set @monAmount=''      
 Return @monAmount        
End
GO
