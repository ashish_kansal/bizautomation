GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderStatus')
DROP FUNCTION GetWorkOrderStatus
GO
CREATE FUNCTION [dbo].[GetWorkOrderStatus] 
(
	@numDomainID NUMERIC(18,0)
    ,@tintCommitAllocation TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
)
RETURNS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @vcWoStatus VARCHAR(500) = ''

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID) > 0
	BEGIN
		SET @vcWoStatus = CONCAT('<a href="#" onClick="return OpenWorkOrder(',(SELECT numWOId FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID AND ISNULL(numParentWOID,0)=0),');"><img src="../images/Icon/workorder.png" /></a> ')

		DECLARE @TEMP TABLE
		(
			ItemLevel INT
			,numParentWOID NUMERIC(18,0)
			,numWOID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numQty FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,bitWorkOrder BIT
			,tintBuildStatus TINYINT
		)

		DECLARE @TEMPWO TABLE
		(
			ID INT IDENTITY(1,1)
			,numWOID NUMERIC(18,0)
		)

		;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemKitID,numQtyItemsReq,numWarehouseItemID,bitWorkOrder,tintBuildStatus) AS
		(
			SELECT
				1,
				numParentWOID,
				numWOId,
				numItemCode,
				CAST(numQtyItemsReq AS FLOAT),
				numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM
				WorkOrder
			WHERE
				numDomainID=@numDomainID
				AND numOppId=@numOppID 
				AND numOppItemID=@numOppItemID
			UNION ALL
			SELECT 
				c.ItemLevel+2,
				c.numWOID,
				WorkOrder.numWOId,
				WorkOrder.numItemCode,
				CAST(WorkOrder.numQtyItemsReq AS FLOAT),
				WorkOrder.numWareHouseItemId,
				1 AS bitWorkOrder,
				(CASE WHEN numWOStatus = 23184 THEN 2 ELSE 0 END) AS tintBuildStatus
			FROM 
				WorkOrder
			INNER JOIN 
				CTEWorkOrder c 
			ON 
				WorkOrder.numParentWOID = c.numWOID
		)


		INSERT 
			@TEMP
		SELECT
			ItemLevel,
			numParentWOID,
			numWOID,
			numItemCode,
			CTEWorkOrder.numQtyItemsReq,
			CTEWorkOrder.numWarehouseItemID,
			bitWorkOrder,
			tintBuildStatus
		FROM 
			CTEWorkOrder
		INNER JOIN 
			Item
		ON
			CTEWorkOrder.numItemKitID = Item.numItemCode
		LEFT JOIN
			WareHouseItems
		ON
			CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID

		IF EXISTS (SELECT numWOID FROM @TEMP WHERE tintBuildStatus <> 2)
		BEGIN
			INSERT INTO 
				@TEMP
			SELECT
				t1.ItemLevel + 1,
				t1.numWOID,
				NULL,
				WorkOrderDetails.numChildItemID,
				WorkOrderDetails.numQtyItemsReq,
				WorkOrderDetails.numWarehouseItemID,
				0 AS bitWorkOrder,
				(CASE 
					WHEN t1.tintBuildStatus = 2 
					THEN 2
					ELSE
						(CASE 
							WHEN Item.charItemType='P'
							THEN
								CASE 
									WHEN @tintCommitAllocation=2  
									THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numOnHand THEN 0 ELSE 1 END)
									ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
								END
							ELSE 1
						END)
				END)
			FROM
				WorkOrderDetails
			INNER JOIN
				@TEMP t1
			ON
				WorkOrderDetails.numWOId = t1.numWOID
			INNER JOIN 
				Item
			ON
				WorkOrderDetails.numChildItemID = Item.numItemCode
				AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
			LEFT JOIN
				WareHouseItems
			ON
				WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID


			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT
			DECLARE @numTempWOID NUMERIC(18,0)
			INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

			SELECT @iCount=COUNT(*) FROM @TEMPWO

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

				UPDATE
					T1
				SET
					tintBuildStatus = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND tintBuildStatus=0) > 0 THEN 0 ELSE 1 END)
				FROM
					@TEMP T1
				WHERE
					numWOID=@numTempWOID
					AND T1.tintBuildStatus <> 2

				SET @i = @i + 1
			END

			IF (SELECT COUNT(*) FROM @TEMP WHERE bitWorkOrder=1 AND tintBuildStatus=2) = (SELECT COUNT(*) FROM @TEMP WHERE bitWorkOrder=1)
			BEGIN
				SET @vcWoStatus = CONCAT(@vcWoStatus,'Build completed')
			END
			ELSE IF (SELECT COUNT(*) FROM @TEMP WHERE tintBuildStatus=0) > 0
			BEGIN
				SET @vcWoStatus = CONCAT(@vcWoStatus,'Not ready to build')
			END
			ELSE
			BEGIN
				SET @vcWoStatus = CONCAT(@vcWoStatus,'Ready to build')
			END
		END
		ELSE
		BEGIN
			SET @vcWoStatus = CONCAT(@vcWoStatus,'Build completed')
		END
	END

	RETURN @vcWoStatus
END