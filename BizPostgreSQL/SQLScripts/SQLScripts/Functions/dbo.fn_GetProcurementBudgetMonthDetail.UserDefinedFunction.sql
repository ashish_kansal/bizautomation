/****** Object:  UserDefinedFunction [dbo].[fn_GetProcurementBudgetMonthDetail]    Script Date: 07/26/2008 18:12:43 ******/

GO

GO
--Created By Siva          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getprocurementbudgetmonthdetail')
DROP FUNCTION fn_getprocurementbudgetmonthdetail
GO
CREATE FUNCTION [dbo].[fn_GetProcurementBudgetMonthDetail]  
(@numProcurementId numeric(9),@tintMonth tinyint,@intYear numeric(9),@numDomainId numeric(9),@numItemGroupId numeric(9)=0)          
Returns varchar(10)          
As          
Begin           
    Declare @monAmount as varchar(10)          
    Select @monAmount=PBD.monAmount From ProcurementBudgetMaster PBM
    inner join  ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId
    Where PBM.numDomainId=@numDomainId And PBD.numProcurementId=@numProcurementId And PBD.numItemGroupId=@numItemGroupId 
    And PBD.tintMonth=@tintMonth And PBD.intYear=@intYear        
    if @monAmount='0.00' Set @monAmount=''        
 Return @monAmount          
End
GO
