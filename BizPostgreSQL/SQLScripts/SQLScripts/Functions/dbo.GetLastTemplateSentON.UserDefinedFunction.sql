
GO
/****** Object:  UserDefinedFunction [dbo].[GetLastTemplateSentON]    Script Date: 06/04/2009 15:01:53 ******/

GO

GO
--SELECT [dbo].[GetLastTemplateSentON](18,1)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getlasttemplatesenton')
DROP FUNCTION getlasttemplatesenton
GO
CREATE FUNCTION [dbo].[GetLastTemplateSentON]
               (@numConECampID NUMERIC(9),
                @numDomainId       NUMERIC(9))
RETURNS VARCHAR(50)
AS
  BEGIN
    DECLARE  @vcStartDate VARCHAR(50)
    SET @vcStartDate = ''
    SELECT TOP 1 @vcStartDate = [dbo].[FormatedDateFromDate](DTL.[bintSentON],@numDomainId)
    FROM   [ConECampaignDTL] DTL
    WHERE  [numConECampID] = @numConECampID
    ORDER BY DTL.[bintSentON] DESC
    
    RETURN @vcStartDate
  END
