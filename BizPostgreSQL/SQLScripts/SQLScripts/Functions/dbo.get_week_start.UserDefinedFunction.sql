/****** Object:  UserDefinedFunction [dbo].[get_week_start]    Script Date: 07/26/2008 18:12:54 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='get_week_start')
DROP FUNCTION get_week_start
GO
CREATE FUNCTION [dbo].[get_week_start] (@date datetime)
RETURNS datetime AS
BEGIN
   return dateadd(yyyy, datepart(yyyy,
      dateadd(weekday,1-datepart(weekday, @date),@date))-1900, 0)
    + dateadd(dy, datepart(dy,
      dateadd(weekday,1-datepart(weekday, @date),@date))-1,0)
END
GO
