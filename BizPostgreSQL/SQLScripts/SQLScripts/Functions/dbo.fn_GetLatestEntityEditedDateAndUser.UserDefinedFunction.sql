/****** Object:  UserDefinedFunction [dbo].[fn_GetLatestEntityEditedDateAndUser]    Script Date: 07/26/2008 18:12:38 ******/

GO

GO
--Created By: Debasish Nag                              
--Created On: 31st Dec 2005                              
--This function returns the Date/Time and information about the user who has last modified the record                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getlatestentityediteddateanduser')
DROP FUNCTION fn_getlatestentityediteddateanduser
GO
CREATE FUNCTION [dbo].[fn_GetLatestEntityEditedDateAndUser](@numDivisionId Numeric, @numContactId Numeric, @vcEntity Char(3))                          
RETURNS Nvarchar(100)                           
AS                          
BEGIN                         
 DECLARE @intCreationDate Bigint          
 DECLARE @intModificationDate Bigint          
 DECLARE @numCreatedBy Numeric          
 DECLARE @numModifiedBy Numeric          
          
 DECLARE @dtCreationDate DateTime                          
 DECLARE @vcCreatedBy NVarchar(100)                          
 DECLARE @dtModificationDate DateTime                          
 DECLARE @vcModifiedBy NVarchar(100)                          
 DECLARE @vcLatestEntityEditedDateAndUser NVarchar(100)            
 DECLARE @numLastContactId Numeric            
 IF @numContactId = 0                    
 BEGIN      
       IF @vcEntity = 'Org'            
            BEGIN            
                --Organization Created Date (Actually the Division Creation Date is considered here)                          
                SELECT @dtCreationDate = d.bintCreatedDate, @vcCreatedBy = dbo.fn_GetUserName(d.numCreatedBy),                
                @vcModifiedBy = d.numModifiedBy                
                FROM DivisionMaster d                          
                WHERE d.numDivisionId = @numDivisionId                          
                      
                IF @vcModifiedBy IS NOT NULL                
                   BEGIN                          
                      SELECT @dtModificationDate = d.bintModifiedDate, @vcModifiedBy = dbo.fn_GetUserName(d.numModifiedBy)                
                      FROM DivisionMaster d                
                      WHERE d.numDivisionId = @numDivisionId                          
                      
                      SET @vcLatestEntityEditedDateAndUser = Cast(@dtModificationDate As NVarchar(20)) + ' by ' + @vcModifiedBy                       
                   END                          
                ELSE                              
                   SET @vcLatestEntityEditedDateAndUser = Cast(@dtCreationDate As NVarchar(20)) + ' by ' + @vcCreatedBy                          
            END         
 END                    
 ELSE IF @numDivisionId = 0                  
 BEGIN                      
       IF @vcEntity = 'Cnt'                    
       BEGIN                          
            --Contact                         
            SELECT @dtCreationDate = bintCreatedDate, @vcCreatedBy = dbo.fn_GetUserName(numCreatedBy),                  
            @vcModifiedBy = numModifiedBy                  
            FROM AdditionalContactsInformation                          
            WHERE numContactId = @numContactId                         
                      
            IF @vcModifiedBy IS NOT NULL                          
            BEGIN                          
                SELECT @dtModificationDate = bintModifiedDate, @vcModifiedBy = dbo.fn_GetUserName(numModifiedBy)                          
                FROM AdditionalContactsInformation                      
                WHERE numContactId = @numContactId                  
                  
                SET @vcLatestEntityEditedDateAndUser = Cast(@dtModificationDate As NVarchar(20)) + ' by ' + @vcModifiedBy                        
            END                      
            ELSE                      
                SET @vcLatestEntityEditedDateAndUser = Cast(@dtCreationDate As NVarchar(20)) + ' by ' + @vcCreatedBy                    
       END                    
 END                      
 RETURN @vcLatestEntityEditedDateAndUser             
END
GO
