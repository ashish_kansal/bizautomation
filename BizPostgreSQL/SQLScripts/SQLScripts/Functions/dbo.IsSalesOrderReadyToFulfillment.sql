GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='IsSalesOrderReadyToFulfillment')
DROP FUNCTION IsSalesOrderReadyToFulfillment
GO
CREATE FUNCTION [dbo].[IsSalesOrderReadyToFulfillment] 
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintCommitAllocation TINYINT
    
)
RETURNS BIT
AS
BEGIN
	DECLARE @bitReadyToShip BIT = 1

	IF EXISTS(SELECT tintshipped FROM dbo.OpportunityMaster WHERE numDomainId=@numDomainID 
			  AND numOppId=@numOppID AND ISNULL(tintshipped,0)=1) OR (SELECT 
																		COUNT(*) 
																	FROM 
																	(
																		SELECT
																			OI.numoppitemtCode,
																			ISNULL(OI.numUnitHour,0) AS OrderedQty,
																			ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
																		FROM
																			OpportunityItems OI
																		INNER JOIN
																			Item I
																		ON
																			OI.numItemCode = I.numItemCode
																		OUTER APPLY
																		(
																			SELECT
																				SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
																			FROM
																				OpportunityBizDocs
																			INNER JOIN
																				OpportunityBizDocItems 
																			ON
																				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																			WHERE
																				OpportunityBizDocs.numOppId = @numOppID
																				AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
																				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
																				AND ISNULL(bitFulFilled,0) = 1
																		) AS TempFulFilled
																		WHERE
																			OI.numOppID = @numOppID
																			AND UPPER(I.charItemType) = 'P'
																			AND ISNULL(OI.bitDropShip,0) = 0
																	) X
																	WHERE
																		X.OrderedQty <> X.FulFilledQty) = 0
	BEGIN
		SET @bitReadyToShip=0
	END
	ELSE
	BEGIN
		DECLARE @TEMP TABLE
		(
			numOppID NUMERIC(18,0)
			,numOppItemID NUMERIC(18,0)
			,numOppChildItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehosueItemID NUMERIC(18,0)
			,numQtyToShip FLOAT
			,numOnHand FLOAT
			,numAllocation FLOAT
			,bitWorkOrder BIT
			,bitBuildCompleted BIT
		)

		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
			,numOppChildItemID
			,numItemCode
			,numWarehosueItemID
			,numQtyToShip
			,numOnHand
			,numAllocation
			,bitWorkOrder
			,bitBuildCompleted
		)
		SELECT
			OI.numOppId
			,OI.numoppitemtCode
			,0
			,OI.numItemCode
			,OI.numWarehouseItmsID
			,ISNULL(numUnitHour,0) - ISNULL(numQtyShipped,0)
			,ISNULL(WI.numOnHand,0)
			,ISNULL(WI.numAllocation,0)
			,ISNULL(OI.bitWorkOrder,0)
			,(CASE WHEN WO.numWOStatus=23184 THEN 1 ELSE 0 END)
		FROM
			OpportunityItems OI
		INNER JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID=WI.numWareHouseItemID
		LEFT JOIN
			WorkOrder WO
		ON
			OI.numOppId = WO.numOppId
			AND OI.numoppitemtCode = WO.numOppItemID
			AND ISNULL(numOppChildItemID,0) = 0
			AND ISNULL(numOppKitChildItemID,0) = 0
		WHERE
			OI.numOppId=@numOppID
			AND ISNULL(OI.bitDropShip,0) = 0
		
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
			,numOppChildItemID
			,numItemCode
			,numWarehosueItemID
			,numQtyToShip
			,numOnHand
			,numAllocation
			,bitWorkOrder
			,bitBuildCompleted
		)
		SELECT
			OKI.numOppId
			,OKI.numOppItemID
			,OKI.numOppChildItemID
			,OKI.numChildItemID
			,OKI.numWareHouseItemId
			,OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0))
			,ISNULL(WI.numOnHand,0)
			,ISNULL(WI.numAllocation,0)
			,(CASE WHEN ISNULL(WO.numWOId,0) > 0 THEN 1 ELSE 0 END) 
			,(CASE WHEN WO.numWOStatus=23184 THEN 1 ELSE 0 END)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numOppId = OKI.numOppId
			AND OI.numoppitemtCode = OKI.numOppItemID
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId=WI.numWareHouseItemID
		LEFT JOIN
			WorkOrder WO
		ON
			OKI.numOppId = WO.numOppId
			AND OKI.numOppItemID = WO.numOppItemID
			AND ISNULL(WO.numOppChildItemID,0) = OKI.numOppChildItemID
			AND ISNULL(WO.numOppKitChildItemID,0) = 0
		WHERE
			OI.numOppId=@numOppID
		
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
			,numOppChildItemID
			,numItemCode
			,numWarehosueItemID
			,numQtyToShip
			,numOnHand
			,numAllocation
			,bitWorkOrder
			,bitBuildCompleted
		)
		SELECT
			OKCI.numOppId
			,OKCI.numOppItemID
			,0
			,OKCI.numItemID
			,OKCI.numWareHouseItemId
			,OKCI.numQtyItemsReq_Orig * t1.numQtyToShip
			,ISNULL(WI.numOnHand,0)
			,ISNULL(WI.numAllocation,0)
			,(CASE WHEN ISNULL(WO.numWOId,0) > 0 THEN 1 ELSE 0 END) 
			,(CASE WHEN WO.numWOStatus=23184 THEN 1 ELSE 0 END)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			@TEMP t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId=WI.numWareHouseItemID
		LEFT JOIN
			WorkOrder WO
		ON
			OKCI.numOppId = WO.numOppId
			AND OKCI.numOppItemID = WO.numOppItemID
			AND ISNULL(WO.numOppChildItemID,0) = OKCI.numOppChildItemID
			AND ISNULL(WO.numOppKitChildItemID,0) = OKCI.numOppKitChildItemID
		WHERE
			OKCI.numOppId=@numOppID

		IF (SELECT COUNT(*) FROM @TEMP WHERE ISNULL(bitWorkOrder,0)=1 AND ISNULL(bitBuildCompleted,0)=0) > 0
		BEGIN
			SET @bitReadyToShip = 0
		END
		ELSE IF @tintCommitAllocation = 2 
		BEGIN
			IF (SELECT COUNT(*) FROM @TEMP WHERE numQtyToShip > numOnHand) > 0
			BEGIN
				SET @bitReadyToShip = 0
			END
		END
		ELSE IF (SELECT COUNT(*) FROM @TEMP WHERE numQtyToShip > numAllocation) > 0
		BEGIN
			SET @bitReadyToShip = 0
		END
	END

	RETURN @bitReadyToShip
END
GO




