GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTotalQtyByUOM')
DROP FUNCTION GetTotalQtyByUOM
GO
CREATE FUNCTION [dbo].[GetTotalQtyByUOM]
(
	@numOppBizDocID NUMERIC(18,0)
)
RETURNS VARCHAR(3000)
AS
BEGIN
	DECLARE @vcTotalQtybyUOM AS VARCHAR(3000) = ''

	
	SELECT
		@vcTotalQtybyUOM = COALESCE(@vcTotalQtybyUOM,'') + (CASE WHEN LEN(@vcTotalQtybyUOM) > 0 THEN ', ' ELSE '' END) + CONCAT(TEMP.vcUOMName,' - ', SUM(numQty))
	FROM
		(
			SELECT 
				OI.numUOMId,
				dbo.fn_GetUOMName(OI.numUOMId) vcUOMName,
				CAST(ISNULL((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode, I.numDomainId, ISNULL(OI.numUOMId, 0))* SUM(OBI.numUnitHour)),0) AS NUMERIC(18, 2)) AS numQty
			FROM
				OpportunityBizDocItems OBI
			INNER JOIN
				OpportunityItems OI
			ON
				OBI.numOppItemID = OI.numOppItemtCode
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				numOppBizDocID = @numOppBizDocID
				AND ISNULL(OI.numUOMId,0) > 0
			GROUP BY
				I.numItemCode,
				I.numDomainID,
				I.numBaseUnit,
				OI.numUOMId
		) AS TEMP
		GROUP BY
			TEMP.numUOMId,
			TEMP.vcUOMName
		ORDER BY
			TEMP.vcUOMName


	RETURN ISNULL(@vcTotalQtybyUOM,'')
END
GO
