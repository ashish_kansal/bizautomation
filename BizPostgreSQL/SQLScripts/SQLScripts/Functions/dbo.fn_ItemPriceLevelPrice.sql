GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_ItemPriceLevelPrice')
DROP FUNCTION fn_ItemPriceLevelPrice
GO
CREATE FUNCTION [dbo].[fn_ItemPriceLevelPrice] 
(
	@numItemCode NUMERIC(18,0)
	,@monListPrice DECIMAL(20,5)
	,@monVendorCost DECIMAL(20,5)
)
RETURNS @Result TABLE     
(    
	vcPriceLevelType VARCHAR(300),
	vcDiscountType VARCHAR(300),
	monPriceLevel1 VARCHAR(300),
	monPriceLevel2 VARCHAR(300),
	monPriceLevel3 VARCHAR(300),
	monPriceLevel4 VARCHAR(300),
	monPriceLevel5 VARCHAR(300),
	monPriceLevel6 VARCHAR(300),
	monPriceLevel7 VARCHAR(300),
	monPriceLevel8 VARCHAR(300),
	monPriceLevel9 VARCHAR(300),
	monPriceLevel10 VARCHAR(300),
	monPriceLevel11 VARCHAR(300),
	monPriceLevel12 VARCHAR(300),
	monPriceLevel13 VARCHAR(300),
	monPriceLevel14 VARCHAR(300),
	monPriceLevel15 VARCHAR(300),
	monPriceLevel16 VARCHAR(300),
	monPriceLevel17 VARCHAR(300),
	monPriceLevel18 VARCHAR(300),
	monPriceLevel19 VARCHAR(300),
	monPriceLevel20 VARCHAR(300)
)    
AS
BEGIN
	
	INSERT INTO @Result
	(
		monPriceLevel1,
		monPriceLevel2,
		monPriceLevel3,
		monPriceLevel4,
		monPriceLevel5,
		monPriceLevel6,
		monPriceLevel7,
		monPriceLevel8,
		monPriceLevel9,
		monPriceLevel10,
		monPriceLevel11,
		monPriceLevel12,
		monPriceLevel13,
		monPriceLevel14,
		monPriceLevel15,
		monPriceLevel16,
		monPriceLevel17,
		monPriceLevel18,
		monPriceLevel19,
		monPriceLevel20
	)
	SELECT 
		ISNULL(DestinationTable.monPriceLevel1,''),
		ISNULL(DestinationTable.monPriceLevel2,''),
		ISNULL(DestinationTable.monPriceLevel3,''),
		ISNULL(DestinationTable.monPriceLevel4,''),
		ISNULL(DestinationTable.monPriceLevel5,''),
		ISNULL(DestinationTable.monPriceLevel6,''),
		ISNULL(DestinationTable.monPriceLevel7,''),
		ISNULL(DestinationTable.monPriceLevel8,''),
		ISNULL(DestinationTable.monPriceLevel9,''),
		ISNULL(DestinationTable.monPriceLevel10,''),
		ISNULL(DestinationTable.monPriceLevel11,''),
		ISNULL(DestinationTable.monPriceLevel12,''),
		ISNULL(DestinationTable.monPriceLevel13,''),
		ISNULL(DestinationTable.monPriceLevel14,''),
		ISNULL(DestinationTable.monPriceLevel15,''),
		ISNULL(DestinationTable.monPriceLevel16,''),
		ISNULL(DestinationTable.monPriceLevel17,''),
		ISNULL(DestinationTable.monPriceLevel18,''),
		ISNULL(DestinationTable.monPriceLevel19,''),
		ISNULL(DestinationTable.monPriceLevel20,'')
	FROM
	(
		SELECT
			CONCAT('monPriceLevel',Id) Id
			,CASE 
				WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
				THEN CONCAT(CAST(ISNULL(@monListPrice,0) - (ISNULL(@monListPrice,0) * (decDiscount /100)) AS DECIMAL(20,5)),' (',intFromQty,'-',intToQty,')')
				WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
				THEN CONCAT(CAST(ISNULL(@monListPrice,0) - decDiscount AS DECIMAL(20,5)),' (',intFromQty,'-',intToQty,')')
				WHEN tintRuleType=1 AND tintDiscountType=3 --Deduct from List price & Named Price
				THEN CAST(decDiscount AS VARCHAR)
				WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN CONCAT(CAST(ISNULL(@monVendorCost,0) + (ISNULL(@monVendorCost,0) * (decDiscount /100)) AS DECIMAL(20,5)),' (',intFromQty,'-',intToQty,')')
				WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN CONCAT(CAST(ISNULL(@monVendorCost,0) + decDiscount AS DECIMAL(20,5)),' (',intFromQty,'-',intToQty,')')
				WHEN tintRuleType=2 AND tintDiscountType=3 -- Add to Primary Vendor Cost & Named Price
				THEN CAST(decDiscount AS VARCHAR)
				WHEN tintRuleType=3 --Named Price
				THEN CAST(decDiscount AS VARCHAR)
			END AS monPrice
		FROM
		(
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numPricingID) Id
				,numItemCode
				,[numPriceRuleID]
				,[intFromQty]
				,[intToQty]
				,[tintRuleType]
				,[tintDiscountType]
				,decDiscount
		
			FROM 
				PricingTable
			WHERE 
				PricingTable.numItemCode=@numItemCode
				AND ISNULL(numCurrencyID,0) = 0
				AND ISNULL(numPriceRuleID,0) = 0
		) TEMP
	) SourceTable
	PIVOT 
	(
	 MIN(monPrice)
	 FOR Id
	 IN ([monPriceLevel1],[monPriceLevel2],[monPriceLevel3],[monPriceLevel4],[monPriceLevel5],[monPriceLevel6],[monPriceLevel7],[monPriceLevel8],[monPriceLevel9],[monPriceLevel10],[monPriceLevel11],[monPriceLevel12],[monPriceLevel13],[monPriceLevel14],[monPriceLevel15],[monPriceLevel16],[monPriceLevel17],[monPriceLevel18],[monPriceLevel19],[monPriceLevel20])
	) AS DestinationTable

	UPDATE 
		@Result
	SET
		vcPriceLevelType=ISNULL((SELECT TOP 1 
									(CASE tintRuleType 
										WHEN 1
										THEN 'Deduct From List Price'
										WHEN 2 
										THEN 'Add to Primary Vendor Cost'
										WHEN 3 
										THEN 'Named Price'
										ELSE ''
									END) 
								FROM 
									PricingTable 
								WHERE 
									numItemCode=@numItemCode
									AND ISNULL(numCurrencyID,0) = 0
									AND ISNULL(numPriceRuleID,0) = 0),'')
		,vcDiscountType=ISNULL((SELECT TOP 1 
									(CASE 
										WHEN tintDiscountType=1 AND (tintRuleType=1 OR tintRuleType=2)
										THEN 'Percentage'
										WHEN tintDiscountType=2 AND (tintRuleType=1 OR tintRuleType=2)
										THEN 'Flat Amount'
										WHEN tintDiscountType=3 AND (tintRuleType=1 OR tintRuleType=2)
										THEN 'Named Price' 
										ELSE ''
									END)
								FROM 
									PricingTable 
								WHERE 
									numItemCode=@numItemCode
									AND ISNULL(numCurrencyID,0) = 0
									AND ISNULL(numPriceRuleID,0) = 0),'')
	RETURN
END