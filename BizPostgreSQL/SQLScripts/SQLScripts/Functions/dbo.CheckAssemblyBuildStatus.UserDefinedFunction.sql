GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckAssemblyBuildStatus')
DROP FUNCTION CheckAssemblyBuildStatus
GO
CREATE FUNCTION [dbo].[CheckAssemblyBuildStatus] 
(
	@numItemKitID NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@numQty NUMERIC(18,0)
	,@bitSalesOrder BIT
	,@tintCommitAllocation TINYINT
)
RETURNS BIT
AS
BEGIN
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID

    DECLARE @IsReadyToBuild BIT = 1

	DECLARE @Temp TABLE
	(
		numItemCode NUMERIC(18,0),
		numQtyRequired INT,
		numOnAllocation FLOAT,
		bitReadyToBuild BIT
	)

	INSERT INTO 
		@Temp
	SELECT
		numChildItemID,
		CAST(ISNULL(numQtyItemsReq * @numQty,0) AS INT),
		WareHouseItems.numAllocation,
		CASE 
			WHEN @bitSalesOrder=1 AND @tintCommitAllocation=2 
			THEN (CASE WHEN ISNULL(numQtyItemsReq * @numQty,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
			ELSE (CASE WHEN ISNULL(numQtyItemsReq * @numQty,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
		END AS bitReadyToBuild 
	FROM
		ItemDetails
	CROSS APPLY
	(
		SELECT TOP 1 
			*
		FROM
			WareHouseItems
		WHERE
			numItemID = ItemDetails.numChildItemID
			AND numWareHouseID = @numWarehouseID
		ORDER BY
			numWareHouseItemID
	) WareHouseItems
	WHERE
		numItemKitID = @numItemKitID

	IF (SELECT COUNT(*) FROM @Temp WHERE  bitReadyToBuild = 0) > 0
	BEGIN
		SET @IsReadyToBuild = 0
	END

    RETURN @IsReadyToBuild
END
GO
