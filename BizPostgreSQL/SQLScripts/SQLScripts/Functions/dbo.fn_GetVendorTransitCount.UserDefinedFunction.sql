GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetVendorTransitCount')
DROP FUNCTION fn_GetVendorTransitCount
GO
CREATE FUNCTION [dbo].[fn_GetVendorTransitCount]
(
	@numVendorID NUMERIC,
	@numAddressID NUMERIC,
	@numDomainID NUMERIC
)
RETURNS INT 
AS
BEGIN

	DECLARE @VendorTransitCount INT
 
	SELECT 
		@VendorTransitCount=COUNT(*) 
	FROM 
		OpportunityMaster Opp 
	INNER JOIN 
		OpportunityItems OppI 
	ON 
		opp.numOppId=OppI.numOppId
    WHERE 
		Opp.numDomainId=@numDomainID
		AND Opp.numDivisionId=@numVendorID
		AND Opp.tintOppType= 2 
		AND Opp.tintOppstatus=1 
		AND Opp.tintShipped=0 
		AND OppI.numVendorWareHouse=@numAddressID
		AND (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)

RETURN @VendorTransitCount

END
GO
