GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetProjectGrossProfit')
DROP FUNCTION GetProjectGrossProfit
GO
CREATE FUNCTION [dbo].[GetProjectGrossProfit]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
)    
RETURNS DECIMAL(20,5)    
AS    
BEGIN   
	DECLARE @TotalIncome DECIMAL(20,5) = 0
	DECLARE @TotalExpense DECIMAL(20,5) = 0

	SET @TotalIncome = @TotalIncome + ISNULL((SELECT 
												SUM(
												ISNULL(OPP.[monPrice], 0) * (CASE numType
																				WHEN 1 THEN ISNULL(OPP.numUnitHour, 0)
																				ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
																			END) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END))
											FROM    
												[TimeAndExpense] TE
											INNER JOIN 
												OpportunityMaster OM 
											ON 
												TE.numOppId = OM.numOppId
											INNER JOIN 
												OpportunityItems OPP 
											ON 
												OPP.numOppId = TE.numOppId
												AND OPP.numoppitemtCode = TE.numOppItemID 
												AND OPP.numProjectId=TE.numProId
											INNER JOIN 
												Item I 
											ON 
												OPP.[numItemCode] = I.[numItemCode]
											WHERE   
												[numProId] = @numProId
												AND TE.[numDomainID] = @numDomainID
												AND numType =1 --Billable 
												AND OM.[tintOppType] = 1),0)

	SET @TotalIncome = @TotalIncome + ISNULL((SELECT
													SUM(ISNULL(OPP.[monPrice], 0) * ISNULL(OPP.numUnitHour, 0) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END))
												FROM 
													OpportunityMaster OM 
												INNER JOIN 
													OpportunityItems OPP 
												ON 
													OPP.numOppId = OM.numOppId
												INNER JOIN 
													Item I 
												ON 
													OPP.[numItemCode] = I.[numItemCode]
												WHERE 
													OM.[numDomainID] = @numDomainID 
													AND OPP.numProjectID = @numProId
													AND OPP.numProjectStageID=0 
													AND OM.[tintOppType] = 1),0)

	SET @TotalIncome = @TotalIncome + ISNULL((SELECT 
												SUM(DED.monAmountPaid) 
											FROM  
												DepositMaster DEM 
											JOIN 
												DepositeDetails DED 
											ON 
												DEM.numDepositID=DED.numDepositID
											WHERE 
												DEM.tintDepositePage=1 
												AND DEM.numDomainId=@numDomainID  
												AND DED.numProjectID = @numProId),0)
				
	SET @TotalIncome = @TotalIncome + ISNULL((SELECT 
												SUM(GD.numCreditAmt)
											FROM 
												dbo.General_Journal_Header GH 
											JOIN 
												dbo.General_Journal_Details GD 
											ON 
												GH.numJournal_Id = GD.numJournalId
											JOIN 
												dbo.Chart_Of_Accounts COA 
											ON 
												COA.numAccountId = GD.numChartAcntId
											WHERE 
												ISNULL(GH.numCheckHeaderID,0)=0 
												AND ISNULL(GH.numBillPaymentID,0)=0 
												AND ISNULL(GH.numBillID,0)=0
												AND ISNULL(GH.numDepositId,0)=0 
												AND ISNULL(GH.numOppId,0)=0 
												AND ISNULL(GH.numOppBizDocsId,0)=0
												AND ISNULL(GH.numReturnId,0)=0
												AND GH.numDomainId=@numDomainID 
												AND GD.numProjectID = @numProId
												AND ISNULL(GD.numCreditAmt,0)>0 
												AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')),0)
		
	SET @TotalExpense = @TotalExpense + ISNULL((SELECT
													SUM((OPP.[monPrice] * OPP.[numUnitHour]))
												FROM 
													[OpportunityMaster] OM 
												JOIN 
													[OpportunityItems] OPP 
												ON 
													OM.[numOppId] = OPP.[numOppId]
												LEFT OUTER JOIN 
													[Item] I 
												ON 
													OPP.[numItemCode] = I.[numItemCode]
												LEFT OUTER JOIN 
													[Chart_Of_Accounts] COA 
												ON 
													I.[numCOGsChartAcntId] = COA.[numAccountId]
												WHERE 
													OM.numDomainId=@numDomainID  
													AND OPP.numProjectID = @numProId
													AND OM.[tintOppType] = 2),0)

	SET @TotalExpense = @TotalExpense + ISNULL((SELECT
													SUM(BD.monAmount)
												FROM 
													ProjectsOpportunities PO 
												JOIN 
													BillHeader BH 
												ON 
													BH.numBillID = PO.numBillID
												JOIN 
													BillDetails BD 
												ON 
													BH.numBillID=BD.numBillID 
													AND PO.numProID=BD.numProjectID
												WHERE 
													PO.numDomainId=@numDomainID  
													AND PO.numProID = @numProId
													AND ISNULL(PO.numBillId,0)>0),0)

	SET @TotalExpense = @TotalExpense + ISNULL((SELECT 
													SUM(BD.monAmount)
												FROM 
													BillHeader BH 
												JOIN 
													BillDetails BD 
												ON 
													BH.numBillID=BD.numBillID 
												WHERE 
													BH.numDomainId=@numDomainID  
													AND BD.numProjectID = @numProId),0)

	SET @TotalExpense = @TotalExpense + ISNULL((SELECT 
													SUM(ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,dttodate) AS FLOAT)/Cast(60 AS FLOAT),0) * ISNULL(UM.monHourlyRate,0))
												FROM 
													[TimeAndExpense] TE 
												JOIN 
													UserMaster UM 
												ON 
													UM.numUserDetailId=TE.numUserCntID
												WHERE 
													[numProId] = @numProId
													AND TE.[numDomainID] = @numDomainID
													AND numType in (1,2) --Non Billable & Billable
													AND TE.numUserCntID>0),0)

	SET @TotalExpense = @TotalExpense + ISNULL((SELECT
													SUM(CD.monAmount)
												FROM 
													CheckHeader CH 
												JOIN 
													CheckDetails CD 
												ON 
													CH.numCheckHeaderID=CD.numCheckHeaderID
												WHERE 
													CH.tintReferenceType=1 
													AND CH.numDomainId=@numDomainID  
													AND CD.numProjectID = @numProId),0)

	SET @TotalExpense = @TotalExpense + ISNULL((SELECT 
													SUM(GD.numDebitAmt) 
												FROM 
													dbo.General_Journal_Header GH 
												JOIN 
													dbo.General_Journal_Details GD 
												ON 
													GH.numJournal_Id = GD.numJournalId
												JOIN 
													dbo.Chart_Of_Accounts COA 
												ON 
													COA.numAccountId = GD.numChartAcntId
												WHERE 
													ISNULL(GH.numCheckHeaderID,0)=0 
													AND ISNULL(GH.numBillPaymentID,0)=0 
													AND ISNULL(GH.numBillID,0)=0
													AND ISNULL(GH.numDepositId,0)=0 
													AND ISNULL(GH.numOppId,0)=0 
													AND ISNULL(GH.numOppBizDocsId,0)=0
													AND ISNULL(GH.numReturnId,0)=0
													AND GH .numDomainId=@numDomainID 
													AND GD.numProjectID = @numProId
													AND ISNULL(GD.numDebitAmt,0) > 0
													AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')),0)

	RETURN ISNULL(@TotalIncome,0) - ISNULL(@TotalExpense,0) -- Set Accuracy of Two precision
END
GO