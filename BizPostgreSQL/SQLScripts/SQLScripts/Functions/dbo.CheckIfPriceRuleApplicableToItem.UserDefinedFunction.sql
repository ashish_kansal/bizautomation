GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckIfPriceRuleApplicableToItem')
DROP FUNCTION CheckIfPriceRuleApplicableToItem
GO
CREATE FUNCTION [dbo].[CheckIfPriceRuleApplicableToItem](@numRuleID NUMERIC(18,0), @numItemID NUMERIC(18,0), @numDivisionID NUMERIC(18,0))  
RETURNS BIT  
AS  
BEGIN  

DECLARE @isRuleApplicable BIT = 0;

DECLARE @isStep2RuleApplicable BIT = 0;
DECLARE @isStep3RuleApplicable BIT = 0;

DECLARE @tempStep2 INT;
DECLARE @tempStep3 INT;

SELECT @tempStep2=tintStep2, @tempStep3=tintStep3  FROM PriceBookRules WHERE numPricRuleID = @numRuleID

/* Check if rule is applied to given item */
IF @tempStep2 = 1 --Apply to items individually
BEGIN
	IF (SELECT COUNT(*) FROM PriceBookRuleItems WHERE numRuleID = @numRuleID AND tintType=1 AND numValue = @numItemID) > 0
		SET @isStep2RuleApplicable = 1
END
ELSE IF @tempStep2 = 2 --Apply to items with the selected item classifications
BEGIN
	IF (SELECT COUNT(*) FROM PriceBookRuleItems WHERE numRuleID = @numRuleID AND tintType=2 AND numValue = (SELECT numItemClassification FROM Item WHERE numItemCode = @numItemID)) > 0
		SET @isStep2RuleApplicable = 1
END
ELSE IF @tempStep2 = 3 -- All Items
BEGIN
	SET @isStep2RuleApplicable = 1
END

/* If step 2 rule is not applicable then there is no need to go for check in step 3 */
IF @isStep2RuleApplicable = 1
BEGIN
	IF @tempStep3 = 1 --Apply to customers individually
	BEGIN
		IF (SELECT COUNT(*) FROM PriceBookRuleDTL WHERE numRuleID = @numRuleID AND numValue = @numDivisionID) > 0
			SET @isStep3RuleApplicable = 1
	END
	ELSE IF @tempStep3 = 2 --Apply to customers with the following Relationships & Profiles
	BEGIN
		IF (
			SELECT 
				COUNT(*) 
			FROM 
				PriceBookRuleDTL 
			WHERE 
				numRuleID = @numRuleID AND 
				numValue = (
								SELECT 
									ISNULL(numCompanyType,0) 
								FROM 
									CompanyInfo 
								WHERE 
									numCompanyId = (SELECT numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionID)
							) AND 
				numProfile = (
								SELECT 
									ISNULL(vcProfile,0) 
								FROM 
									CompanyInfo 
								WHERE 
									numCompanyId = (SELECT numCompanyID FROM DivisionMaster WHERE numDivisionID = @numDivisionID))
			) > 0
			SET @isStep3RuleApplicable = 1
	END
	ELSE IF @tempStep3 = 3 --All Customers
	BEGIN
		SET @isStep3RuleApplicable = 1
	END
END

IF @isStep2RuleApplicable = 1 AND @isStep3RuleApplicable = 1
BEGIN
	SET @isRuleApplicable = 1
END  

return @isRuleApplicable
END
GO
