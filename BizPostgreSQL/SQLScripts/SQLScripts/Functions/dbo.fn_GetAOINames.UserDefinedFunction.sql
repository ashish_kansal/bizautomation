/****** Object:  UserDefinedFunction [dbo].[fn_GetAOINames]    Script Date: 07/26/2008 18:12:28 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getaoinames')
DROP FUNCTION fn_getaoinames
GO
CREATE FUNCTION [dbo].[fn_GetAOINames] (@vcAOIList varchar(250))
	RETURNS varchar (1000) 
AS
--This function will take the AOI IDs in comma separated string and return the AOI names with <BR> separating each AOI.
BEGIN
	DECLARE @intFirstComma integer
	DECLARE @intNextComma integer
	DECLARE @bintValue bigint
	DECLARE @vcRetVal varchar (1000)
	DECLARE @vcTempVal varchar (255)

	SET @vcRetVal=''

	IF @vcAOIList=''
	BEGIN
		RETURN '&nbsp;'
	END
	SET @intFirstComma=CHARINDEX(',', @vcAoiList, 0)
	SET @intNextComma=CHARINDEX(',', @vcAoiList, @intFirstComma + 1)
	IF @intNextComma=0
	BEGIN
		SET @bintValue=SUBSTRING(@vcAoiList,@intFirstComma+1,LEN(@vcAoiList)-@intFirstComma)
		IF CONVERT(numeric,@bintValue)>0
		BEGIN
			--Get the AOI Name.
			SELECT @vcTempVal=vcAOIName FROM AOIMaster WHERE numAOIID=@bintValue
			SET @vcRetVal = @vcTempVal + '<BR>'
		END

		SET @intFirstComma=CHARINDEX(',', @vcAoiList, @intNextComma)
	END
	WHILE @intFirstComma>0
	BEGIN
		IF @intNextComma=0
		BEGIN
			SET @bintValue=SUBSTRING(@vcAoiList,@intFirstComma+1,LEN(@vcAoiList)-@intFirstComma)
			IF CONVERT(numeric,@bintValue)>0
			BEGIN
				--Get the AOI Name.
				SELECT @vcTempVal=vcAOIName FROM AOIMaster WHERE numAOIID=@bintValue
				SET @vcRetVal = @vcRetVal + @vcTempVal + '<BR>'
			END
			BREAK
		END
		ELSE
		BEGIN
			SET @bintValue=SUBSTRING(@vcAoiList,@intFirstComma+1,@intNextComma-@intFirstComma-1)
			IF CONVERT(numeric,@bintValue)>0
			BEGIN
				--Get the AOI Name.
				SELECT @vcTempVal=vcAOIName FROM AOIMaster WHERE numAOIID=@bintValue
				SET @vcRetVal = @vcRetVal + @vcTempVal + '<BR>'
			END
		END

		SET @intFirstComma=CHARINDEX(',', @vcAoiList, @intNextComma)
		SET @intNextComma=CHARINDEX(',', @vcAoiList, @intFirstComma + 1)
	END

	RETURN @vcRetVal
END
GO
