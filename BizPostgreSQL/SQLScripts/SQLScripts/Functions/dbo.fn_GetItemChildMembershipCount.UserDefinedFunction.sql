
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemChildMembershipCount')
DROP FUNCTION fn_GetItemChildMembershipCount
GO
CREATE FUNCTION [dbo].[fn_GetItemChildMembershipCount]
(
	@numDomainID as numeric(18,0),
	@numItemCode as numeric(18,0)
)     
RETURNS INT 
AS      
BEGIN      
	DECLARE @numCount AS INT = 0

	SELECT
		@numCount = COUNT(vcItemName)
	FROM
	(
		SELECT 
			Item.vcItemName,
			CASE 
				WHEN ISNULL(Item.bitAssembly,0)=1 THEN 'Assembly' 
				WHEN ISNULL(Item.bitKitParent,0)=1 THEN 'Kit'
			END AS ParentItemType,
			ISNULL(ItemDetails.numQtyItemsReq,0) AS numQtyItemsReq
		FROM 
			ItemDetails 
		INNER JOIN
			Item
		ON
			ItemDetails.numItemKitID = Item.numItemCode
		WHERE 
			numChildItemID = @numItemCode
		UNION ALL
		SELECT
			ItemGroups.vcItemGroup AS vcItemName,
			'Item Group' AS ParentItemType,
			'' AS numQtyItemsReq
		FROM
			Item
		INNER JOIN
			ItemGroups
		ON
			Item.numItemGroup = ItemGroups.numItemGroupID
		WHERE
			Item.numDomainID = @numDomainID 
			AND numItemCode = @numItemCode
		UNION ALL
		SELECT 
			Item.vcItemName,
			'Related Items' AS ParentItemType,
			'' AS numQtyItemsReq
		FROM 
			SimilarItems 
		INNER JOIN
			Item
		ON
			SimilarItems.numParentItemCode = Item.numItemCode
		WHERE 
			SimilarItems.numDomainID = @numDomainID
			AND SimilarItems.numItemCode = @numItemCode
	) AS Temp

	RETURN @numCount
END
