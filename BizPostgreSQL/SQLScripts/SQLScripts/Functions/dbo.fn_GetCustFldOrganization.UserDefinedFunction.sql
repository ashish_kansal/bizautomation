
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetCustFldOrganization')
DROP FUNCTION fn_GetCustFldOrganization
GO
CREATE FUNCTION [dbo].[fn_GetCustFldOrganization](@numFldId numeric(18),@pageId as tinyint,@numRecordId as numeric(9))  
 RETURNS varchar (100)   
AS  
BEGIN  
	DECLARE @vcValue as  varchar(300)  
	DECLARE @vcFldValue AS VARCHAR(MAX)
	DECLARE @fld_type AS VARCHAR(20)

	SELECT @fld_type = Fld_type FROM dbo.CFW_Fld_Master WHERE Fld_id = @numFldId

	SELECT
		@vcFldValue = Fld_Value
	FROM 
		CFW_FLD_Values 
	WHERE 
		Fld_ID=@numFldId AND 
		RecId=@numRecordId  

	IF @fld_type = 'SelectBox' AND ISNUMERIC(@vcFldValue) = 1
	BEGIN
		SET @vcValue = (SELECT vcData FROM dbo.ListDetails WHERE numListItemID = @vcFldValue)
	END
	ELSE IF @fld_type = 'CheckBoxList'
	BEGIN
		SELECT 
			@vcValue = STUFF((SELECT CONCAT(',', vcData) 
		FROM 
			ListDetails 
		WHERE 
			numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(@vcFldValue,',')) FOR XML PATH('')), 1, 1, '')
	END
	ELSE IF @fld_type = 'CheckBox'
	BEGIN
		SET @vcValue = (CASE WHEN ISNULL(@vcFldValue,0)=1 THEN 'Yes' ELSE 'No' END)
	END
	ELSE
	BEGIN
		SET @vcValue = @vcFldValue
	END

	IF @vcValue IS NULL 
		SET @vcValue=''  

	RETURN @vcValue  
END
GO