--This function is specific to sales tax. generic function for calculating tax is fn_CalItemTaxAmt
--SELECT * FROM [OpportunityMaster] WHERE [numOppId]=12275
--SELECT * FROM [OpportunityBizDocs] WHERE [numOppBizDocsId]=12360
--SELECT * FROM [OpportunityAddress] WHERE [numOppID]=12340
-- select dbo.[fn_CalSalesTaxAmt](11946,110)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_calsalestaxamt')
DROP FUNCTION fn_calsalestaxamt
GO
CREATE FUNCTION [fn_CalSalesTaxAmt]
    (
      @numOppBizDocID AS NUMERIC,
      @numDomainID AS NUMERIC 
    )
RETURNS FLOAT
BEGIN
    DECLARE @numDivisionID AS NUMERIC(9)
    DECLARE @numOppID AS NUMERIC(9)
    DECLARE @numCountry AS NUMERIC(9)
    DECLARE @numState AS NUMERIC(9)
    DECLARE @TaxPercentage AS MONEY
    
SELECT  @numOppID = [numOppId]  FROM    [OpportunityBizDocs]    WHERE   [numOppBizDocsId] = @numOppBizDocID

SELECT TOP 1 @numState=numState,@numCountry=numCountry FROM  dbo.[fn_getTaxableStateCountry](@numDomainID,@numOppID)
--             RETURN @numState
--              RETURN @numCountry
    SET @TaxPercentage = 0              
    IF @numCountry > 0
        AND @numState > 0 
        BEGIN
            SELECT  @TaxPercentage = decTaxPercentage
            FROM    TaxDetails
            WHERE   numCountryID = @numCountry
                    AND numStateID = @numState
                    AND numDomainID = @numDomainID
					and numTaxItemID = 0 -- for sales tax
        END 
    IF ISNULL(@TaxPercentage,0) = 0 
    BEGIN
		SELECT  @TaxPercentage = decTaxPercentage
            FROM    TaxDetails
            WHERE   numCountryID = @numCountry
                    AND numStateID = 0
                    AND numDomainID = @numDomainID
					and numTaxItemID = 0 -- for sales tax
	END    
	
    RETURN ISNULL(@TaxPercentage,0)
END