/****** Object:  UserDefinedFunction [dbo].[fnGetTotalDealsWon]    Script Date: 07/26/2008 18:12:51 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fngettotaldealswon')
DROP FUNCTION fngettotaldealswon
GO
CREATE FUNCTION [dbo].[fnGetTotalDealsWon](
	@numDomainID NUMERIC(9),
	@intFromDate BIGINT,
	@intToDate BIGINT) RETURNS INTEGER
AS
BEGIN
	DECLARE @dcDealsWon INTEGER
	SELECT
	@dcDealsWon=COUNT(OM.numCreatedBy)
	 FROM OpportunityMaster OM
	--JOIN  UserMaster UM
	--ON UM.numUserID=OM.numCreatedBy
	--JOIN AdditionalContactsInformation AI
	--ON AI.numContactId = OM.numContactID
	WHERE OM.tintOppStatus=1
	AND OM.numDomainID=@numDomainID
	AND OM.bintAccountClosingDate BETWEEN @intFromDate AND @intToDate
	--GROUP BY UM.vcUserName,vcFirstName,vcLastName
	--ORDER BY UM.vcUserName

	RETURN @dcDealsWon
END
GO
