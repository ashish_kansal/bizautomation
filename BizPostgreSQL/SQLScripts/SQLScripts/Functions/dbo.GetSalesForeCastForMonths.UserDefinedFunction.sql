/****** Object:  UserDefinedFunction [dbo].[GetSalesForeCastForMonths]    Script Date: 07/26/2008 18:13:10 ******/

GO

GO
--Created By Siva                       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getsalesforecastformonths')
DROP FUNCTION getsalesforecastformonths
GO
CREATE FUNCTION [dbo].[GetSalesForeCastForMonths](@month int,@year int,@numDomainId numeric(9),@numUserCntID numeric(9),@sintForecast int)                
Returns DECIMAL(20,5)                
As                
Begin                
                
Declare @monOppAmt as DECIMAL(20,5)                
Declare @TotalPercentage as float                
Declare @TotalAmt as DECIMAL(20,5)                
Declare @UnclosedDealCount as int                
Declare @Percentage as float                
           
if @sintForecast=1           
Begin          
    select @TotalAmt= isnull(monForecastAmount,0)        
    from Forecast FC  Where tintForecastType=1 and tintmonth=@month and  sintYear=@year and numDomainID=@numDomainID           
End          
Else          
Begin               
---- Select  @monOppAmt=Sum(monPAmount*(Select tintPercentage from OpportunityMaster Opp                
---- inner join  OpportunityStageDetails OSD on Opp.numOppId=OSD.numOppId  Where Opp.tintOppType=1 And  OSD.bitstagecompleted=1 and month(intPEstimatedCloseDate)=@month and year(intPEstimatedCloseDate)=@year and  Opp.numRecOwner=@numUserCntID 
 --And Opp.tintoppStatus=1                           
---- And Opp.numDomainID=@numDomainId) From OpportunityMaster                 
----    Where tintOppType=1 And month(intPEstimatedCloseDate)=@month and year(intPEstimatedCloseDate)=@year and  numRecOwner=@numUserCntID And tintoppStatus=1 and tintshipped=0    
----    and numDomainID=@numDomainId          
----         
---- Select @TotalPercentage =sum(tintPercentage) from OpportunityMaster Opp                
---- inner join  OpportunityStageDetails OSD on Opp.numOppId=OSD.numOppId                
---- Where Opp.tintOppType=1 And  OSD.bitstagecompleted=1 and month(intPEstimatedCloseDate)=@month and year(intPEstimatedCloseDate)=@year and  Opp.numRecOwner=@numUserCntID  And Opp.tintoppStatus=1                           
---- And Opp.numDomainID=@numDomainId                
----                
----              
---- Select @UnclosedDealCount=Count(*) From OpportunityMaster where tintOppType=1 and month(intPEstimatedCloseDate)=@month and year(intPEstimatedCloseDate)=@year and  numRecOwner=@numUserCntID And tintoppStatus=0                           
----    and numDomainID=@numDomainId                  
----                
---- Set @Percentage=(Case When @TotalPercentage=0 then 1 Else @TotalPercentage End) /(Case When @UnclosedDealCount=0 then 1 Else @UnclosedDealCount End)                
---- ----Print @UnclosedDealCount                
---- ----Print @monOppAmt                
---- ----Print @Percentage                
 --Print @Percentage         
   
Select @monOppAmt=Sum(X.Amount)  From (Select (monPAmount*(Select sum(isnull(tintPercentage,0))  from OpportunityStageDetails OSD                
 Where OSD.numOppId=OpportunityMaster.numOppId And OSD.bitstagecompleted=1))/100 as Amount  From OpportunityMaster                 
    Where tintOppType=1 And month(intPEstimatedCloseDate)=@month and year(intPEstimatedCloseDate)=@year and  numRecOwner=@numUserCntID And tintoppStatus=1 and tintshipped=0    
    and numDomainID=@numDomainId)X    
         
 Set @TotalAmt=isnull(@monOppAmt,0)                
End            
             
--Print @TotalAmt                
Return @TotalAmt                
End
GO
