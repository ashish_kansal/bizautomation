/****** Object:  UserDefinedFunction [dbo].[OppClosedPipeline]    Script Date: 07/26/2008 18:13:14 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='oppclosedpipeline')
DROP FUNCTION oppclosedpipeline
GO
CREATE FUNCTION [dbo].[OppClosedPipeline](@OppStatus tinyint,@numDomainId numeric(9),@numCreatedby as numeric,@month as int,@year int)
RETURNS integer
as 
begin
declare @retVal numeric
set @retval =0 
select @retval=sum(monPamount) from opportunitymaster
 where tintOppType = 1 and tintoppstatus=@OppStatus and 
opportunitymaster.numdomainid= @numDomainId and numRecOwner=@numCreatedby
and month(bintCreatedDate) = @month and year (bintCreatedDate) = @year
return @retval 
end
GO
