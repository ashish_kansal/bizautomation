/****** Object:  UserDefinedFunction [dbo].[fn_ParentCategory]    Script Date: 07/26/2008 18:12:50 ******/

GO

GO
--Created by siva 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_parentcategory')
DROP FUNCTION fn_parentcategory
GO
CREATE FUNCTION [dbo].[fn_ParentCategory](@CategoryId varchar(100),@numDomainId as int)      
returns varchar(100)      
Begin      
      
declare @parentId varchar(100)      
declare @parentId1 varchar(100),@Parentlen int,@ParentCategories varchar(1000)      
set @parentId1=''      
select @parentId = isnull(numParntAcntTypeId,0) from chart_of_Accounts where numAccountId = @CategoryId And numDomainId=@numDomainId      
while @parentId>1      
begin      
set @parentId1=  @parentId1 + @parentId + ','    
select @parentId = isnull(numParntAcntTypeId,0) from chart_of_Accounts where numAccountId = @parentId    And numDomainId=@numDomainId    
end      
  
set @Parentlen = len(@parentId1)      
if @Parentlen >0   
Begin  
set @ParentCategories = substring(@parentId1,1,@Parentlen-1)      
End  
  
return @ParentCategories      
End
GO
