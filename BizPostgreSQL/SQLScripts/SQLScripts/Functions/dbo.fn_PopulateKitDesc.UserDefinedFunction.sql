/****** Object:  UserDefinedFunction [dbo].[fn_PopulateKitDesc]    Script Date: 07/26/2008 18:12:50 ******/

GO

GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_populatekitdesc')
DROP FUNCTION fn_populatekitdesc
GO
CREATE FUNCTION [dbo].[fn_PopulateKitDesc](@numOppId AS NUMERIC(9),@numOppItemID as numeric(9)) returns varchar(300)

as
begin
	declare @Desc as varchar(300)
	set @Desc=''
	declare @numChildItem as numeric(9)
	declare @Quantity as integer

--	select top 1 @numChildItem=numChildItemID,@Quantity=numQtyItemsReq
--	from dbo.ItemDetails where numItemKitID=@numOppItemCode ORDER BY numChildItemID ASC 
--
--	while @numChildItem>0
--	begin
--
--	select @Desc=@Desc+ ISNULL(vcItemName,'')+'('+convert(varchar(30),@Quantity*@Units)+' '+ isnull(vcUnitofMeasure,'Units') +'), '  from Item where numItemCode=@numChildItem
--
--
--	select top 1 @numChildItem=numChildItemID,@Quantity=numQtyItemsReq
--	from dbo.ItemDetails where numItemKitID=@numOppItemCode and numChildItemID>@numChildItem
--
--	if @@rowcount=0 set @numChildItem=0
--	end


  select @Desc=SUBSTRING(( SELECT ' ,' + ISNULL(I.vcItemName,'') +
  CASE WHEN charitemtype='P' THEN '(' + convert(varchar(30),CAST((OKI.numQtyItemsReq / OKI.numQtyItemsReq_Orig) * (OKI.numQtyItemsReq_Orig / dbo.fn_UOMConversion(OKI.numUOMId,0,I.numDomainId,I.numBaseUnit)) AS DECIMAL(10,2)))
				+' '+ isnull(UOM.vcUnitName,'Units') +')' ELSE '' END 
                                    FROM  OpportunityKitItems OKI JOIN Item I ON OKI.numChildItemID=I.numItemCode JOIN UOM ON UOM.numUOMId=OKI.numUOMId 
									WHERE OKI.numOppId=@numOppId AND OKI.numOppItemID=@numOppItemID ORDER BY numOppChildItemID ASC 
                                  FOR XML PATH('') ), 3, 200000)
	
  return @Desc
end
GO
