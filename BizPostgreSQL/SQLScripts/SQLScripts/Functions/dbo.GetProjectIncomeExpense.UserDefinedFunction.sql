GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='GetProjectIncomeExpense')
DROP FUNCTION GetProjectIncomeExpense
GO
CREATE FUNCTION [dbo].[GetProjectIncomeExpense]
(@numDomainID NUMERIC,@numProId NUMERIC)    
RETURNS @Result TABLE     
(    
 monIncome money,monExpense money,monBalance money  
)    

as    
begin   
 
 
DECLARE @monIncome MONEY,@monExpense MONEY
 SET @monIncome=0
 SET @monExpense=0
 
			--Sales Order
			SELECT @monIncome= @monIncome + ISNULL(SUM(ISNULL(OPP.[monPrice], 0) * CASE numType
                      WHEN 1 THEN ISNULL(OPP.numUnitHour, 0)
                      ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),
                                  0)
                    END),0)
            FROM    [TimeAndExpense] TE
                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
            WHERE   [numProId] = @numProId
                    AND TE.[numDomainID] = @numDomainID
                    AND numType =1 --Billable 
					AND OM.[tintOppType] = 1

			--Sales Order
			SELECT @monIncome= @monIncome + ISNULL(SUM(ISNULL(OPP.[monPrice], 0) * ISNULL(OPP.numUnitHour, 0)),0)
            FROM    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
            WHERE  OM.[numDomainID] = @numDomainID AND OPP.numProjectID = @numProId
                    AND OPP.numProjectStageID=0 AND OM.[tintOppType] = 1

			--Purchase Order
            select @monExpense= @monExpense + ISNULL(SUM(OPP.[monPrice] * OPP.[numUnitHour]),0)
			 from  [OpportunityMaster] OM 
						JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
                        LEFT OUTER JOIN [Item] I ON OPP.[numItemCode] = I.[numItemCode]
						LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
			 where  OM.numDomainId=@numDomainID  
					AND OPP.numProjectID = @numProId
					AND OM.[tintOppType] = 2	

			 --Bill
			select @monExpense= @monExpense + ISNULL(SUM(ISNULL(BD.monAmount,0)),0)
			 from  ProjectsOpportunities PO 
				   JOIN BillHeader BH ON BH.numBillID = PO.numBillID
				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID AND PO.numProID=BD.numProjectID
			 where  PO.numDomainId=@numDomainID  
					AND PO.numProID = @numProId
					and isnull(PO.numBillId,0)>0
			
			
			--Bill		
			select @monExpense= @monExpense + ISNULL(SUM(ISNULL(BD.monAmount,0)),0) 
			 from  BillHeader BH 
				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID 
			 where  BH.numDomainId=@numDomainID  
					AND BD.numProjectID = @numProId
				
				
			--Employee Expense							
			SELECT  @monExpense= @monExpense + ISNULL(SUM(isnull(Cast(datediff(minute,TE.dtfromdate,dttodate) as float)/Cast(60 as float),0) * isnull(UM.monHourlyRate,0)),0)
            FROM    [TimeAndExpense] TE join UserMaster UM on UM.numUserDetailId=TE.numUserCntID
            WHERE   [numProId] = @numProId
                    AND TE.[numDomainID] = @numDomainID
                    AND numType in (1,2) --Non Billable & Billable
					AND TE.numUserCntID>0
--					AND bitReimburse=1 and  numCategory=2  

			
			--Deposit
			select @monIncome= @monIncome + ISNULL(SUM(ISNULL(DED.monAmountPaid,0)),0)
			 from  DepositMaster DEM JOIN DepositeDetails DED ON DEM.numDepositID=DED.numDepositID
			 WHERE DEM.tintDepositePage=1 AND DEM.numDomainId=@numDomainID  
					AND DED.numProjectID = @numProId
			
			
			--Check					
			select @monExpense= @monExpense + ISNULL(SUM(ISNULL(CD.monAmount,0)),0)
			 from  CheckHeader CH JOIN CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
			 WHERE CH.tintReferenceType=1 AND CH.numDomainId=@numDomainID  
					AND CD.numProjectID = @numProId		
			
			
			--Journal
			select @monExpense= @monExpense + ISNULL(SUM(ISNULL(GD.numDebitAmt,0)),0)
			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
					AND ISNULL(GH.numReturnId,0)=0
					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
					AND ISNULL(GD.numDebitAmt,0)>0	AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')	
				
			
			 --Journal		
			 select @monIncome= @monIncome + ISNULL(SUM(ISNULL(GD.numCreditAmt,0)),0)
			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
					AND ISNULL(GH.numReturnId,0)=0
					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
					AND ISNULL(GD.numCreditAmt,0)>0  AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')
					
 
 INSERT INTO @result
 SELECT ISNULL(@monIncome,0),ISNULL(@monExpense,0),ISNULL(@monIncome,0) - ISNULL(@monExpense,0)
 
 RETURN
END
GO