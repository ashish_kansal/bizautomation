SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created By Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetItemAffectedList')
DROP FUNCTION GetItemAffectedList
GO
CREATE FUNCTION dbo.GetItemAffectedList
    (
      @numRuleID NUMERIC,
      @tinType TINYINT,
      @numDomainID NUMERIC(10,0) 
    )
RETURNS VARCHAR(MAX)
AS 
BEGIN
    DECLARE @ItemList VARCHAR(2000)
			IF @tinType = 1 
				BEGIN
					SELECT  @ItemList = COALESCE(@ItemList + ', ', '') + RTRIM(LTRIM(I.vcItemName))
					FROM    [dbo].[PromotionOfferItems] POI
							INNER JOIN Item I ON I.[numItemCode] = POI.[numValue] 
							LEFT OUTER JOIN [dbo].[ShippingLabelRuleMaster] SR ON SR.[numShippingRuleID] = POI.numProId AND I.numDomainID = SR.numDomainID
					WHERE   SR.intItemAffected = @tinType
							AND SR.[numShippingRuleID] = @numRuleID	
							AND SR.numDomainID = @numDomainID
							AND POI.tintType = 1
							AND POI.tintRecordType = 3
				END
            ELSE IF @tinType = 2
				BEGIN
					SELECT  @ItemList = COALESCE(@ItemList + ', ', '') + RTRIM(LTRIM(LD.vcData))
					FROM    [dbo].[PromotionOfferItems] POI
							INNER JOIN ListDetails LD ON LD.numListItemID = POI.[numValue] 
							LEFT OUTER JOIN [dbo].[ShippingLabelRuleMaster] SR ON SR.[numShippingRuleID] = POI.numProId AND LD.numDomainID = SR.numDomainID
					WHERE   SR.intItemAffected = @tinType
							AND SR.[numShippingRuleID] = @numRuleID	
							AND SR.numDomainID = @numDomainID
							AND POI.tintType = 3
							AND POI.tintRecordType = 3
				END
                  
    RETURN ISNULL(@ItemList, '')
    
END 

GO