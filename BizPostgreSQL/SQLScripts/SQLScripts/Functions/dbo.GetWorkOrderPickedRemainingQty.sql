GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderPickedRemainingQty')
DROP FUNCTION GetWorkOrderPickedRemainingQty
GO
CREATE FUNCTION [dbo].[GetWorkOrderPickedRemainingQty]
(
	@numWOID NUMERIC(18,0)
	,@numQtyItemsReq FLOAT
)    
RETURNS VARCHAR(50)
AS    
BEGIN   
	DECLARE @numPickedQty FLOAT = 0
	
	SET @numPickedQty = ISNULL((SELECT
									MIN(numPickedQty)
								FROM
								(
									SELECT 
										WorkOrderDetails.numWODetailId
										,FLOOR(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
									FROM
										WorkOrderDetails
									LEFT JOIN
										WorkOrderPickedItems
									ON
										WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
									WHERE
										WorkOrderDetails.numWOId=@numWOID
										AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
									GROUP BY
										WorkOrderDetails.numWODetailId
										,WorkOrderDetails.numQtyItemsReq_Orig
								) TEMP),0)

	RETURN CONCAT(@numPickedQty,' | ',(@numQtyItemsReq - @numPickedQty))
END
GO