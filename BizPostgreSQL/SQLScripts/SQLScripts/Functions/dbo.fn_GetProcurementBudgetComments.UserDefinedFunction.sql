/****** Object:  UserDefinedFunction [dbo].[fn_GetProcurementBudgetComments]    Script Date: 07/26/2008 18:12:42 ******/

GO

GO
--Created By Siva     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getprocurementbudgetcomments')
DROP FUNCTION fn_getprocurementbudgetcomments
GO
CREATE FUNCTION [dbo].[fn_GetProcurementBudgetComments]    
(@numProcurementId numeric(9),@numDomainId numeric(9),@numItemGroupId numeric(9)=0)    
Returns varchar(50)    
As    
Begin    
 Declare @vcComments as varchar(50)          
 Select @vcComments=isnull(PBD.vcComments,'') From ProcurementBudgetMaster PBM
  inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId 
  Where PBM.numDomainId=@numDomainId And PBD.numProcurementId=@numProcurementId And PBD.numItemGroupId=@numItemGroupId    
 Return @vcComments      
End
GO
