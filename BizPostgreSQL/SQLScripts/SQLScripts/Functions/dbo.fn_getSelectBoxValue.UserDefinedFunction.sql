/****** Object:  UserDefinedFunction [dbo].[fn_getSelectBoxValue]    Script Date: 07/26/2008 18:12:38 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getSelectBoxValue')
DROP FUNCTION fn_getSelectBoxValue
GO
CREATE FUNCTION [dbo].[fn_getSelectBoxValue](@vcListItemType AS VARCHAR(3),@numlistID VARCHAR(25),@vcDbColumnName VARCHAR(50))
Returns varchar(100) 
As
BEGIN
	declare @listName as varchar(100)

			 IF @vcListItemType='LI'                                                     
				BEGIN                                                     
					SELECT @listName=ISNULL(vcData,'') from ListDetails WHERE numListItemID=@numlistID
				end                                                    
			 else if @vcListItemType='S'                                                     
				 begin    
					select @listName=vcState  from State WHERE numStateID=@numlistID                                                
				end                                                    
			else if @vcListItemType='T'                                                     
				begin   
					select @listName=vcData from ListDetails WHERE numListItemID=@numlistID                                                
				end   
			else if @vcListItemType='C'                                                     
				begin     
					select @listName=vcCampaignName from CampaignMaster WHERE numCampaignId=@numlistID                                                
				end  
			else if @vcListItemType='AG'                                                     
				begin      
					select @listName=vcGrpName  from Groups WHERE numGrpId=@numlistID                                                
				end   
			else if @vcListItemType='DC'                                                     
				begin    
					select @listName=vcECampName  from ECampaign WHERE numECampaignID=@numlistID                                                
				end 
			else if   @vcListItemType='U' AND @vcDbColumnName='numContractId'                                                
				begin       
					SELECT @listName=vcContractName  FROM dbo.ContractManagement WHERE numContractId=@numlistID
				end                   
			else if   @vcListItemType='U' OR @vcDbColumnName='numManagerID'                                                
				begin       
					select @listName=dbo.fn_GetContactName(@numlistID) 
				end   
		    ELSE IF @vcDbColumnName='charSex'
				BEGIN
					select @listName=CASE WHEN @numlistID='M' then 'Male' when @numlistID='F' then 'Female' else  '-' end 
				 END
			ELSE IF @vcDbColumnName='tintOppStatus'
			  BEGIN
			  		SELECT @listName=CASE @numlistID WHEN 1 THEN 'Deal Won' WHEN 2 THEN 'Deal Lost' ELSE 'Open' END 
			  END
			Else                                                     
				BEGIN                                                     
					select @listName=vcData from ListDetails WHERE numListItemID=@numlistID
				end 	

return isnull(@listName,'-')
END
GO
