/****** Object:  UserDefinedFunction [dbo].[fn_GetOpportunitySourceValue]    Script Date: 07/26/2008 18:12:38 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetOpportunitySourceValue')
DROP FUNCTION fn_GetOpportunitySourceValue
GO
CREATE FUNCTION [dbo].[fn_GetOpportunitySourceValue](@tintSource AS NUMERIC(18),@tintSourceType TINYINT,@numDomainID NUMERIC(18))
Returns varchar(100) 
As
BEGIN

declare @listName as varchar(200)

IF @tintSourceType=1 --List Master
BEGIN
	IF @tintSource = 0
	BEGIN
		SELECT @listName = 'Internal Order'
	END
	ELSE
	BEGIN
		SELECT @listName=ISNULL(vcData,'') from ListDetails WHERE numListItemID=@tintSource
	END
END
ELSE IF @tintSourceType=2 --WebSite
BEGIN
	  SELECT @listName=ISNULL(vcSiteName,'')
  FROM   Sites WHERE  [numDomainID] = @numDomainID AND numSiteID=@tintSource
END
ELSE IF @tintSourceType=3 --WebAPI
BEGIN
	 SELECT @listName=ISNULL(vcProviderName,'') FROM dbo.WebAPI WHERE WebApiId=@tintSource
END
ELSE IF @tintSourceType=4 --Marketplaces (EchannelHub)
BEGIN
	 SELECT @listName=ISNULL(vcMarketplace,'') FROM dbo.eChannelHub WHERE ID=@tintSource
END
	
	RETURN @listName
END
GO
