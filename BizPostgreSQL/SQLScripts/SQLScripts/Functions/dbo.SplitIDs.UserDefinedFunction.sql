/****** Object:  UserDefinedFunction [dbo].[SplitIDs]    Script Date: 07/26/2008 18:13:15 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='splitids')
DROP FUNCTION splitids
GO
CREATE FUNCTION [dbo].[SplitIDs]
(
	@IdList varchar(8000),
	@Delimiter char(1) = '/'
)
RETURNS 
@ParsedList table
(
	Id int
)
AS
BEGIN
	DECLARE @Id varchar(10), @Pos int

	SET @IdList = LTRIM(RTRIM(@IdList))+ @Delimiter
	SET @Pos = CHARINDEX(@Delimiter, @IdList, 1)

	IF REPLACE(@IdList, @Delimiter, '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			IF ISNUMERIC(LTRIM(RTRIM(LEFT(@IdList, @Pos - 1)))) = 1
			BEGIN
				SET @Id = LTRIM(RTRIM(LEFT(@IdList, @Pos - 1)))
				IF @Id <> ''
				BEGIN
					INSERT INTO @ParsedList (Id) 
					VALUES (CAST(@Id AS int)) --Use Appropriate conversion
				END
			END

			SET @IdList = RIGHT(@IdList, LEN(@IdList) - @Pos)
			SET @Pos = CHARINDEX(@Delimiter, @IdList, 1)
		END
	END	
	RETURN
END
GO
