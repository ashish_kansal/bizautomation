GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetItemPriceLevel')
DROP FUNCTION fn_GetItemPriceLevel
GO
CREATE FUNCTION [dbo].[fn_GetItemPriceLevel]
(
    @numItemCode NUMERIC,
	@numWareHouseItemID NUMERIC(18,0)
)
RETURNS @PriceBookRules table
(
	intFromQty int,intToQty int,vcRuleType VARCHAR(50),vcDiscountType VARCHAR(50),
	decDiscount DECIMAL(18,2),monPriceLevelPrice DECIMAL(20,5),vcPriceLevelType VARCHAR(50)
)                               
AS 
BEGIN


	DECLARE @monListPrice AS DECIMAL(20,5) = 0
	DECLARE @monWListPrice AS DECIMAL(20,5) = 0
	DECLARE @monVendorCost AS DECIMAL(20,5) = 0
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numItemClassification NUMERIC(18,0)

	SELECT @numDomainID=numDomainID, @numItemClassification=numItemClassification, @monListPrice=monListPrice FROM Item WHERE numitemCode=@numItemCode

	IF ((@numWareHouseItemID>0) AND EXISTS(SELECT * FROM item WHERE numItemCode=@numItemCode and  charItemType='P'))      
	BEGIN      
		SELECT 
			@monWListPrice=isnull(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF ISNULL(@monWListPrice,0) > 0 
			SET @monListPrice = @monWListPrice
	END      



	SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)


    INSERT INTO @PriceBookRules
	SELECT 
		[intFromQty]
		,[intToQty]
		,CASE tintRuleType 
			WHEN 1 THEN 'Deduct from List price'
			WHEN 2 THEN 'Add to Primary Vendor Cost'
			WHEN 3 THEN 'Named Price' 
		END vcRuleType
		,CASE 
			WHEN (tintRuleType=1 OR tintRuleType=2) AND tintDiscountType=1 THEN 'Percentage'
			WHEN (tintRuleType=1 OR tintRuleType=2) AND tintDiscountType=2 THEN 'Flat'
			ELSE '' 
		END vcDiscountType
		,CASE WHEN tintRuleType=3 THEN 0 ELSE decDiscount END AS decDiscount
		,CASE WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
					THEN @monListPrice - (@monListPrice * ( decDiscount /100))
                 WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
					THEN @monListPrice - decDiscount
                 WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
					THEN @monVendorCost + (@monVendorCost * ( decDiscount /100))
                 WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
					THEN @monVendorCost + decDiscount
                 WHEN tintRuleType=3 --Named Price
					THEN decDiscount
                END AS monPriceLevelPrice,'Item Price Level'											
        FROM    [PricingTable]
        WHERE   ISNULL(numItemCode,0)=@numItemCode AND ISNULL(numCurrencyID,0) = 0
        ORDER BY [numPricingID] 


	declare @numRelationship as numeric(9);SET @numRelationship=0                
	declare @numProfile as numeric(9);SET @numProfile=0  
	declare @numDivisionID as numeric(9);SET @numDivisionID=0                
                

INSERT INTO @PriceBookRules
	SELECT PT.[intFromQty],PT.[intToQty],CASE PT.tintRuleType WHEN 1 THEN 'Deduct from List price'
												WHEN 2 THEN 'Add to Primary Vendor Cost'
												WHEN 3 THEN 'Named Price' END vcRuleType,
	CASE WHEN (PT.tintRuleType=1 OR PT.tintRuleType=2) AND PT.tintDiscountType=1 THEN 'Percentage'
		 WHEN (PT.tintRuleType=1 OR PT.tintRuleType=2) AND PT.tintDiscountType=2 THEN 'Flat'
		ELSE '' END vcDiscountType,CASE WHEN PT.tintRuleType=3 THEN 0 ELSE PT.decDiscount END AS decDiscount,
		CASE WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
					THEN @monListPrice - (@monListPrice * ( PT.decDiscount /100))
                 WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
					THEN @monListPrice - PT.decDiscount
                 WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
					THEN @monVendorCost + (@monVendorCost * ( PT.decDiscount /100))
                 WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
					THEN @monVendorCost + PT.decDiscount
                 WHEN PT.tintRuleType=3 --Named Price
					THEN PT.decDiscount
                END AS monPriceLevelPrice,'Price Rule'	
FROM   PriceBookRules P 
       LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
         LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
        JOIN [PricingTable] PT ON P.numPricRuleID = PT.numPriceRuleID AND ISNULL(PT.numCurrencyID,0) = 0
WHERE   P.numDomainID = @numDomainID AND P.tintRuleFor=1 AND P.tintPricingMethod = 1
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC

Return
   END

GO


