
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderShipReceiveStatusForIndividualItem')
DROP FUNCTION GetOrderShipReceiveStatusForIndividualItem
GO
CREATE FUNCTION [dbo].[GetOrderShipReceiveStatusForIndividualItem] 
(
      @numOppID AS NUMERIC(18,0),
	  @tintOppType AS NUMERIC(18,0),
	  @tintshipped AS TINYINT,
	  @numItemCode AS NUMERIC(18,0)
)
RETURNS VARCHAR(100)
AS 
BEGIN	
	DECLARE @vcShippedReceivedStatus VARCHAR(100) = ''

	DECLARE @TotalQty AS FLOAT  = 0

	IF @tintOppType = 1 --SALES ORDER
	BEGIN
		DECLARE @numQtyShipped AS FLOAT = 0

		SELECT @TotalQty = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID AND I.numItemCode=@numItemCode),0)

		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
		IF ISNULL(@tintshipped,0) = 1
		BEGIN
			SET @numQtyShipped = @TotalQty
		END
		ELSE
		BEGIN
			SELECT @numQtyShipped = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numQtyShipped) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID  AND I.numItemCode=@numItemCode),0)
		END

		SELECT @vcShippedReceivedStatus =(CASE WHEN  @TotalQty = @numQtyShipped THEN '<b><font color="green">' ELSE '<b><font color="red">' END) + CONCAT(CAST(@TotalQty AS FLOAT),' / ',CAST(@numQtyShipped AS FLOAT)) + '</font></b>'
	END
	ELSE IF @tintOppType = 2 --PURCHASE ORDER
	BEGIN
		DECLARE @numQtyReceived AS FLOAT = 0

		SELECT @TotalQty = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHour) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID  AND I.numItemCode=@numItemCode),0)
		
		-- IF ORDER IS CLOSED WITHOUT FULFILLMENT MARK ALL QTY IS FULFILLED
		IF ISNULL(@tintshipped,0) = 1
		BEGIN
			SET @numQtyReceived = @TotalQty
		END
		ELSE
		BEGIN
			SELECT @numQtyReceived = ISNULL((SELECT SUM(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,I.numDomainId, ISNULL(OI.numUOMId, 0)),1) * numUnitHourReceived) FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode = I.numItemCode  WHERE UPPER(I.charItemType) = 'P' AND ISNULL(bitDropship,0) = 0 AND numOppId = @numOppID AND I.numItemCode=@numItemCode),0)
		END

		SELECT @vcShippedReceivedStatus =(CASE WHEN  @TotalQty = @numQtyReceived THEN '<b><font color="green">' ELSE '<b><font color="red">' END) + CONCAT(CAST(@TotalQty AS FLOAT),' / ',CAST(@numQtyReceived AS FLOAT)) + '</font></b>'
	END

	RETURN @vcShippedReceivedStatus
END
GO