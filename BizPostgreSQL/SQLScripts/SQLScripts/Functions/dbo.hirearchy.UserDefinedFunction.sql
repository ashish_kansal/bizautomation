/****** Object:  UserDefinedFunction [dbo].[hirearchy]    Script Date: 07/26/2008 18:13:13 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='hirearchy')
DROP FUNCTION hirearchy
GO
CREATE FUNCTION [dbo].[hirearchy](@id as int) returns varchar(1000) 
as 
begin
declare @tree as varchar(1000)

set @tree = ''
---if (parent_cat_code <> 0 and  parent_cat_code is not  null )
select @tree = dbo.hirearchy(numAccountId)  + vcCatgyName from Chart_of_Accounts where numParntAcntTypeId= @id
return @tree 
end
GO
