/****** Object:  UserDefinedFunction [dbo].[fn_GetKitInventoryByWarehouse]    Script Date: 05/30/2014  ******/
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'fn'
                    AND NAME = 'fn_GetKitAvailabilityByWarehouse' ) 
    DROP FUNCTION fn_GetKitAvailabilityByWarehouse
GO
CREATE FUNCTION [dbo].[fn_GetKitAvailabilityByWarehouse]
(
    @numKitId NUMERIC(18,0),
    @numWarehouseID NUMERIC(18,0)
)
RETURNS FLOAT
AS BEGIN          
    DECLARE @fltAvailable AS FLOAT = 0;     
    
	WITH CTE(numItemKitID, numItemCode, numQtyItemsReq, numCalculatedQty) AS 
	( 
		SELECT   
			CONVERT(NUMERIC(18,0),0),
			numItemCode,
            DTL.numQtyItemsReq,
            CAST(DTL.numQtyItemsReq AS NUMERIC(9, 0)) AS numCalculatedQty
        FROM
			Item
        INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID = numItemCode
        WHERE 
			numItemKitID = @numKitId
        UNION ALL
        SELECT 
			Dtl.numItemKitID,
            i.numItemCode,
            DTL.numQtyItemsReq,
            CAST(( DTL.numQtyItemsReq * c.numCalculatedQty ) AS NUMERIC(9, 0)) AS numCalculatedQty
        FROM 
			Item i
        INNER JOIN 
			ItemDetails Dtl 
		ON 
			Dtl.numChildItemID = i.numItemCode
        INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode
        WHERE    
			Dtl.numChildItemID != @numKitId
    )
    
	SELECT  
		@fltAvailable = CAST(FLOOR(MIN(CASE 
									WHEN ISNULL(numOnHand,0) + ISNULL(numAllocation,0) = 0 THEN 0
                                    WHEN ISNULL(numOnHand,0) + ISNULL(numAllocation,0) >= numQtyItemsReq AND numQtyItemsReq > 0 THEN (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) / numQtyItemsReq
                                    ELSE 0
                                    END)
						) AS FLOAT)
    FROM    
		cte c
    OUTER APPLY
	(
		SELECT TOP 1
			WareHouseItems.*
		FROM
			WareHouseItems
		INNER JOIN 
			Warehouses W 
		ON 
			W.numWareHouseID = WareHouseItems.numWareHouseID
		WHERE
			numItemID = c.numItemCode
			AND WareHouseItems.numWareHouseID = @numWarehouseID
		ORDER BY
			numWareHouseItemID
	) WI
        
    RETURN @fltAvailable         
          
   END
GO
