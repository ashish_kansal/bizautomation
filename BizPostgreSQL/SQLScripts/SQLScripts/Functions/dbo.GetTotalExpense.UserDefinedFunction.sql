/****** Object:  UserDefinedFunction [dbo].[GetTotalExpense]    Script Date: 07/26/2008 18:13:13 ******/

GO

GO
--Created By Siva            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='gettotalexpense')
DROP FUNCTION gettotalexpense
GO
CREATE FUNCTION [dbo].[GetTotalExpense](@numUserCntID numeric,@numDomainId numeric,@dtStartDate datetime ,@dtEndDate datetime)                                  
 RETURNS decimal(10,2)                                  
as                                  
Begin              
           
           
 Declare @Expense as decimal              
    set @Expense=0          
      
Select  @Expense=@Expense+isnull((Sum(Cast(monAmount as float)))   ,0)    
from TimeAndExpense Where numCategory=2 And numType in (1,2) And numUserCntID=@numUserCntID And numDomainID=@numDomainId And
--and		 dtTCreatedOn between   @dtStartDate and    @dtEndDate              
(dtFromDate between @dtStartDate And @dtEndDate Or dtToDate between @dtStartDate And @dtEndDate)     
     
  Return @Expense               
                
End
GO
