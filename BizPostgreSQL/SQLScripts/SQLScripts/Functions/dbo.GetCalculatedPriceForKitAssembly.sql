GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCalculatedPriceForKitAssembly')
DROP FUNCTION GetCalculatedPriceForKitAssembly
GO
CREATE FUNCTION [dbo].[GetCalculatedPriceForKitAssembly]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
	,@numCurrencyID NUMERIC(18,0)
	,@fltExchangeRate FLOAT
)    
RETURNS DECIMAL(20,5)
AS    
BEGIN   
	DECLARE @monCalculatedPrice DECIMAL(20,5) = 0

	IF ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		SET @numWarehouseItemID = ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode),0)
	END

	SELECT
		@monCalculatedPrice = ISNULL(monPrice,0)
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice
		(
			@numDomainID
			,@numDivisionID
			,@numItemCode
			,@numQty
			,@numWarehouseItemID
			,@tintKitAssemblyPriceBasedOn
			,@numOppID
			,@numOppItemID
			,@vcSelectedKitChildItems
			,@numCurrencyID
			,@fltExchangeRate
		)    

	RETURN @monCalculatedPrice
END
GO