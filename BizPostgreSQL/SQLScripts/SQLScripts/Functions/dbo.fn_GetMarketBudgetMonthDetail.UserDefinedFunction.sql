/****** Object:  UserDefinedFunction [dbo].[fn_GetMarketBudgetMonthDetail]    Script Date: 07/26/2008 18:12:40 ******/

GO

GO
--Created By Siva                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getmarketbudgetmonthdetail')
DROP FUNCTION fn_getmarketbudgetmonthdetail
GO
CREATE FUNCTION [dbo].[fn_GetMarketBudgetMonthDetail]        
(@numMarketId numeric(9),@tintMonth tinyint,@intYear numeric(9),@numDomainId numeric(9),@numListItemID numeric(9)=0)                
Returns varchar(10)                
As                
Begin                 
    Declare @monAmount as varchar(10)                
    Select @monAmount=MBD.monAmount From MarketBudgetMaster MBM  
    inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId  
    Where MBM.numDomainId=@numDomainId And MBD.numMarketId=@numMarketId 
    And MBD.numListItemID=@numListItemID and MBD.tintMonth=@tintMonth And MBD.intYear=@intYear              
    if @monAmount='0.00' Set @monAmount=''              
 Return @monAmount                
End
GO
