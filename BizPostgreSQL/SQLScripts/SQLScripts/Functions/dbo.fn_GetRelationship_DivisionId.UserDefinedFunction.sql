/****** Object:  UserDefinedFunction [dbo].[fn_GetRelationship_DivisionId]    Script Date: 07/26/2008 18:12:44 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getrelationship_divisionid')
DROP FUNCTION fn_getrelationship_divisionid
GO
CREATE FUNCTION [dbo].[fn_GetRelationship_DivisionId](@ListID numeric,@numDomainID numeric)
	RETURNS varchar(100) 
AS
BEGIN
	DECLARE @vcRelationValue varchar (250),@vcDivisionId varchar(250)
	SET @vcRelationValue=''
        SET @vcDivisionId=''
	
        

	SELECT @vcRelationValue=vcData FROM ListDetails WHERE numListItemID=@ListID and (constFlag=1 or numDomainID=@numDomainID) 
       RETURN @vcRelationValue	
END
GO
