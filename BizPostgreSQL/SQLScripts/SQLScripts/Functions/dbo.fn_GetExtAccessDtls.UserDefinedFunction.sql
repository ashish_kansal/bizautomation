/****** Object:  UserDefinedFunction [dbo].[fn_GetExtAccessDtls]    Script Date: 07/26/2008 18:12:37 ******/

GO

GO
--select dbo.fn_GetCCEmail(12)
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getextaccessdtls')
DROP FUNCTION fn_getextaccessdtls
GO
CREATE FUNCTION [dbo].[fn_GetExtAccessDtls](@numExtranetID numeric)
returns varchar(10)
as
begin
declare @retData as varchar(10)
declare @numDivisionID numeric
declare @countContacts as numeric
declare @countExtContacts as numeric
select @numDivisionID=numDivisionID from ExtarnetAccounts where numExtranetID=@numExtranetID

select @countContacts=count(*) from AdditionalContactsInformation where numDivisionID=@numDivisionID and vcEmail<>''
select @countExtContacts=count(*) from ExtranetAccountsDtl where numExtranetID=@numExtranetID and tintAccessAllowed=1

if @countExtContacts=0
set @retData='No'
else if @countExtContacts=@countContacts
set @retData ='All Users'
else if @countExtContacts<@countContacts
set @retData ='Some Users'


return @retData
end
GO
