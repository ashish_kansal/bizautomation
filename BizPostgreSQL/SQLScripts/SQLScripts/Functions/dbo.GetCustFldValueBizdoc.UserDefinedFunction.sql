/****** Object:  UserDefinedFunction [dbo].[GetCustFldValueBizdoc]    Script Date: 07/26/2008 18:12:59 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcustfldvaluebizdoc')
DROP FUNCTION getcustfldvaluebizdoc
GO
CREATE FUNCTION [dbo].[GetCustFldValueBizdoc](@numFldId numeric(9),@pageId as tinyint,@numRecordId as numeric(9))
	RETURNS varchar (100) 
AS
BEGIN

declare @vcValue as  varchar(100)
declare @fld_Type as varchar(50)
declare @numListID as numeric(9)
set @vcValue=''
if @pageId=7 or @pageId=8
begin
       select @numListID=numListID ,@fld_Type=Fld_type from CFW_Fld_Master where Fld_id=@numFldId
       select @vcValue=isnull(Fld_Value,0) from CFW_Fld_Values_Product where Fld_ID=@numFldId and RecId=@numRecordId
       if (@fld_Type='TextBox' or @fld_Type='TextArea')
       begin
		if @vcValue='0' set @vcValue='-'
		if  @vcValue='' set @vcValue='-'
       end
       else if @fld_Type='SelectBox' set @vcValue=dbo.GetListIemName(@vcValue)
       else if @fld_Type='CheckBox' set @vcValue= case when @vcValue=0 then 'No' else 'Yes' end
end

if @vcValue is null set @vcValue=''

RETURN @vcValue

END
GO
