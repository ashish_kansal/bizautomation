/****** Object:  UserDefinedFunction [dbo].[fn_GetDepDtlsbyProStgID]    Script Date: 07/26/2008 18:12:35 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getdepdtlsbyprostgid')
DROP FUNCTION fn_getdepdtlsbyprostgid
GO
CREATE FUNCTION [dbo].[fn_GetDepDtlsbyProStgID](@numProID numeric, @numProStageID numeric,@tintType tinyint)
	RETURNS varchar(100) 
AS
--Thsi function will take the ID fo rthe Territory or the List Details and return the corresponding name.
BEGIN
	DECLARE @vcRetValue varchar (100)
	set @vcRetValue=''

	select @vcRetValue=@@rowcount
	from  ProjectsDependency where numProId=@numProID and numProStageID=@numProStageID
	
	if @@rowcount > 0 set @vcRetValue=1
	else set  @vcRetValue=0
	RETURN @vcRetValue	
END
GO
