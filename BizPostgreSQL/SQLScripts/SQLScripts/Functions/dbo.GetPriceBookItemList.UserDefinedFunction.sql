GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getpricebookitemlist')
DROP FUNCTION getpricebookitemlist
GO
CREATE FUNCTION dbo.GetPriceBookItemList
    (
      @numRuleID NUMERIC,
      @tintMode TINYINT 
    )
RETURNS VARCHAR(2000)
AS BEGIN
    DECLARE @ItemList VARCHAR(2000)

    IF @tintMode = 1 
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + vcItemName
            FROM    [PriceBookRuleItems] PD
                    INNER JOIN Item I ON I.[numItemCode] = PD.[numValue]
                    LEFT OUTER JOIN [PriceBookRules] PB ON PB.[numPricRuleID] = PD.[numRuleID]
            WHERE   PB.[tintStep2] = 1
                    AND PB.[numPricRuleID] = @numRuleID
        END
      
      IF @tintMode = 2
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + vcData
            FROM    [PriceBookRuleItems] PD
                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = PD.[numValue]
                    LEFT OUTER JOIN [PriceBookRules] PB ON PB.[numPricRuleID] = PD.[numRuleID]
            WHERE   PB.[tintStep2] = 2
                    AND PB.[numPricRuleID] = @numRuleID
        END
        IF @tintMode = 3
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + C.[vcCompanyName]
            FROM    [PriceBookRuleDTL] PD
                    INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = PD.[numValue]
                    LEFT OUTER JOIN [CompanyInfo] C ON C.[numCompanyId] = DM.[numCompanyID]
                    LEFT OUTER JOIN [PriceBookRules] PB ON PB.[numPricRuleID] = PD.[numRuleID]
            WHERE   PB.[tintStep3] = 1
                    AND PB.[numPricRuleID] = @numRuleID
        END
        
        IF @tintMode = 4
        BEGIN

             SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + (LI.vcData +'/' + LI1.vcData)
            FROM    [PriceBookRuleDTL] PD
                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = PD.[numValue]
                    Inner JOIN [ListDetails] LI1 ON LI1.[numListItemID] = PD.[numProfile]
                    LEFT OUTER JOIN [PriceBookRules] PB ON PB.[numPricRuleID] = PD.[numRuleID]
            WHERE   PB.[tintStep3] = 2
                    AND PB.[numPricRuleID] = @numRuleID
        END
        
        
    RETURN ISNULL(@ItemList, '')
   END 