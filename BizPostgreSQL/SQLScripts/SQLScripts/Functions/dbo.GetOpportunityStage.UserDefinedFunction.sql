/****** Object:  UserDefinedFunction [dbo].[GetOpportunityStage]    Script Date: 07/26/2008 18:13:06 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getopportunitystage')
DROP FUNCTION getopportunitystage
GO
CREATE FUNCTION [dbo].[GetOpportunityStage](@numOppId as numeric(9),@numUsercntId as numeric(9),@numDomainId as numeric(9))
returns varchar(4000)
as 
begin
	declare @stage as varchar(4000)
	set @stage = ''
	select @stage=
	case when @stage='' then vcStageDetail else  @stage+'<br>'+vcStageDetail end
	from OpportunityStageDetails OSd
	join  OpportunityMaster opp    on opp.numOppid=OSd.numoppid 
	where OSd.numoppid = @numOppId and  bitStageCompleted=0 
	and OSd.numAssignTo=@numUsercntId and opp.numDomainId =@numDomainId
--	print @stage
return @stage
end
GO
