/****** Object:  UserDefinedFunction [dbo].[fn_GetTotalRegularHrsWorked]    Script Date: 07/26/2008 18:12:47 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_gettotalregularhrsworked')
DROP FUNCTION fn_gettotalregularhrsworked
GO
CREATE FUNCTION [dbo].[fn_GetTotalRegularHrsWorked]    
(@dtStartDate datetime,@dtEndDate datetime,@numUserCntID numeric,@numDomainId numeric)                                    
 RETURNS decimal(10,2)      
as    
Begin    
  Declare @decTotalHrsWorked as decimal(10,2)      
 Select @decTotalHrsWorked=sum(x.Hrs) From (Select  isnull(Sum(Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)),0) as Hrs    
  from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1         
  And (dtFromDate between @dtStartDate And @dtEndDate Or dtToDate between @dtStartDate And @dtEndDate) And     
  numUserCntID=@numUserCntID  And numDomainID=@numDomainId     
  union all    
  Select isnull(sum(Cast(datediff(minute,dtStartDate,dtEndDate)as float)/Cast(60 as float)),0) as Hrs    
 From Opportunitytime Where (dtStartDate between @dtStartDate And @dtEndDate or dtEndDate between @dtStartDate and @dtEndDate)    
 And numPCreatedBy=@numUserCntID    
 union all    
  Select isnull(Sum(Cast(datediff(minute,dtStartTime,dtEndTime) as float)/Cast(60 as float)),0) as Hrs From casetime    
 Where (dtStartTime between @dtStartDate and @dtEndDate Or dtEndTime between @dtStartDate and @dtEndDate)    
 And numCreatedBy=@numUserCntID       
 union all    
  Select isnull(Sum(Cast(datediff(minute,dtStartTime,dtEndTime) as float)/Cast(60 as float)),0) as Hrs From projectstime    
 Where (dtStartTime between @dtStartDate and @dtEndDate Or dtEndTime between @dtStartDate and @dtEndDate)    
 And numPCreatedBy=@numUserCntID) x    
 Return @decTotalHrsWorked    
End
GO
