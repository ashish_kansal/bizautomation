GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'fn'
                    AND NAME = 'fn_getcontactaddedtoopportunity' )
    DROP FUNCTION fn_getcontactaddedtoopportunity
GO
CREATE FUNCTION [dbo].[fn_GetContactAddedToOpportunity] ( @numOppId NUMERIC )
RETURNS VARCHAR(2000)
AS
    BEGIN

        DECLARE @ContactList VARCHAR(2000)
        SELECT  @ContactList = COALESCE(@ContactList + ', ', '')
                + CAST(numContactID AS VARCHAR(20))
        FROM    [OpportunityContact]
        WHERE   [numOppId] = @numOppId
    
        RETURN ISNULL(@ContactList,'')
    END 