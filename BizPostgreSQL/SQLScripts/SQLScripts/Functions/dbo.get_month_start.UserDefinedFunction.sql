/****** Object:  UserDefinedFunction [dbo].[get_month_start]    Script Date: 07/26/2008 18:12:53 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='get_month_start')
DROP FUNCTION get_month_start
GO
CREATE FUNCTION [dbo].[get_month_start] (@date datetime)
RETURNS datetime AS
BEGIN
   RETURN dateadd(m,datediff(m,0, @date),0)
   END
GO
