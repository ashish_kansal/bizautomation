GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GenerateBizAPIPublicKey')
DROP FUNCTION GenerateBizAPIPublicKey
GO
CREATE FUNCTION [dbo].[GenerateBizAPIPublicKey]()  
returns varchar(10)  
as  
begin  

DECLARE @chars NCHAR(62);
DECLARE @PublicKey VARCHAR(10)
DECLARE @IsUnique BIT
SET @IsUnique = 0
SET @chars = N'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz'

WHILE (@IsUnique = 0)
BEGIN
    SET @PublicKey = SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
            + SUBSTRING(@chars, CAST(((SELECT rnd FROM [dbo].[View_Random]) * LEN(@chars)) AS INT) + 1, 1)
    IF (SELECT COUNT(*) FROM dbo.UserMaster WHERE vcBizAPIPublicKey = @PublicKey COLLATE Latin1_General_CS_AS) = 0
		SET @IsUnique = 1
    
END
  
return @PublicKey
end
GO
