/****** Object:  UserDefinedFunction [dbo].[funZips]    Script Date: 07/26/2008 18:12:53 ******/

GO

GO
-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <Create Date,,>    
-- Description: <Description,,>    
-- =============================================    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='funzips')
DROP FUNCTION funzips
GO
CREATE FUNCTION [dbo].[funZips]     (    
 -- Add the parameters for the function here    
 @Zip char(5),     
 @Radius int    
)    
RETURNS @Result TABLE     
(    
 -- Add the column definitions for the TABLE variable here    
 ZipCode char(5),Distance numeric(10,2)    
)    
AS    
BEGIN    
 DECLARE @LAT1 float    
 DECLARE @LON1 float    
     
    
 SELECT @LAT1 = Latitude, @LON1 = Longitude FROM ZipCodes WHERE ZipCode = @Zip    
     
--In ZipCodes table actually Latitude=Longitude ,Longitude=Latitude values
 -- Fill the table variable with the rows for your result set    
 INSERT INTO @Result    
  SELECT ZipCode,dbo.funDistance(@LON1 ,@LAT1, Longitude ,Latitude) as Distance FROM ZipCodes WHERE dbo.funDistance(@LON1 ,@LAT1, Longitude ,Latitude) < @Radius    
 RETURN     
END
GO
