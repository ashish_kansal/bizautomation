/****** Object:  UserDefinedFunction [dbo].[fn_GetComapnyName]    Script Date: 07/26/2008 18:12:30 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcomapnyname')
DROP FUNCTION fn_getcomapnyname
GO
CREATE FUNCTION [dbo].[fn_GetComapnyName](@numDivisionID Numeric)  
 RETURNS varchar(201) 
BEGIN  
 declare @numCompanyID as varchar(15)
 declare @vcDivision as varchar(100)
	select @numCompanyID=numCompanyID,
	@vcDivision=CASE WHEN LEN(isnull(vcDivisionName,''))>1 THEN ', '+ isnull(vcDivisionName,'') ELSE '' END   from divisionMaster where numDivisionID=@numDivisionID

	return (select vcCompanyName+  @vcDivision from companyinfo  where numCompanyID =@numCompanyID)
 
END
GO
