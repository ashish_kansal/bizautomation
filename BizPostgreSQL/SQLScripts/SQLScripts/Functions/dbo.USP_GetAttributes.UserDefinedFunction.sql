/****** Object:  UserDefinedFunction [dbo].[USP_GetAttributes]    Script Date: 07/26/2008 18:13:15 ******/

GO

GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='usp_getattributes')
DROP FUNCTION usp_getattributes
GO
CREATE FUNCTION [dbo].[USP_GetAttributes] (@numRecID numeric,@bitSerialized  bit)
returns varchar(100)
as
begin
declare @strAttribute varchar(100)
declare @numCusFldID numeric(9)
declare @numCusFldValue varchar(100)
set @numCusFldID=0
set @strAttribute=''
select top 1 @numCusFldID=fld_id,@numCusFldValue=REPLACE(fld_value,'$','') from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and bitSerialized=@bitSerialized order by fld_id
while @numCusFldID>0
begin
	if isnumeric(@numCusFldValue)=1
	begin
		--set @strAttribute=@strAttribute+@numCusFldValue
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID
		select @strAttribute=@strAttribute+':'+ vcData +',' from ListDetails where numListItemID=@numCusFldValue
	end
	else
	begin
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID
		select @strAttribute=@strAttribute+':-,' 
	end
	select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and bitSerialized=@bitSerialized and fld_id>@numCusFldID order by fld_id
	if @@rowcount=0 set @numCusFldID=0
	
end

If LEN(@strAttribute) > 0
BEGIN
	SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
END

RETURN @strAttribute
end
GO
