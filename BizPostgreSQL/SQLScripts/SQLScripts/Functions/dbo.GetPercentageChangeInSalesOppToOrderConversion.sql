GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPercentageChangeInSalesOppToOrderConversion')
DROP FUNCTION GetPercentageChangeInSalesOppToOrderConversion
GO
CREATE FUNCTION [dbo].[GetPercentageChangeInSalesOppToOrderConversion]
(
	 @numDomainID AS NUMERIC(18,0),
	 @dtStartDate DATE,
	 @dtEndDate DATE,
	 @tintCompareValueOf TINYINT --1: Number of Conversion, 2: Amount of Conversion
)    
RETURNS NUMERIC(18,2)
AS    
BEGIN   
	DECLARE @SalesOppToOrderConversionChange NUMERIC(18,2) = 0.00
	DECLARE @SalesOppToOrderConversion1 INT
	DECLARE @SalesOppToOrderConversion2 INT

	SELECT
		@SalesOppToOrderConversion1 = (CASE WHEN @tintCompareValueOf=1 THEN COUNT(numOppID) ELSE SUM(monDealAmount) END)
	FROM
		OpportunityMaster OM
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.bintCreatedDate BETWEEN  @dtStartDate AND @dtEndDate
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=1
		AND bintOppToOrder IS NOT NULL

	SELECT
		@SalesOppToOrderConversion2 = (CASE WHEN @tintCompareValueOf=1 THEN COUNT(numOppID) ELSE SUM(monDealAmount) END)
	FROM
		OpportunityMaster OM
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,@dtStartDate) AND DATEADD(YEAR,-1,@dtEndDate)
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=1
		AND bintOppToOrder IS NOT NULL


	SET @SalesOppToOrderConversionChange = 100.0 * (ISNULL(@SalesOppToOrderConversion1,0) - ISNULL(@SalesOppToOrderConversion2,0)) / (CASE WHEN ISNULL(@SalesOppToOrderConversion2,0) = 0 THEN 1 ELSE ISNULL(@SalesOppToOrderConversion2,0) END)


	RETURN ISNULL(@SalesOppToOrderConversionChange,0.00)
END
GO