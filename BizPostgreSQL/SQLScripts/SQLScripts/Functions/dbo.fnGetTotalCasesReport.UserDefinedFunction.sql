/****** Object:  UserDefinedFunction [dbo].[fnGetTotalCasesReport]    Script Date: 07/26/2008 18:12:51 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fngettotalcasesreport')
DROP FUNCTION fngettotalcasesreport
GO
CREATE FUNCTION [dbo].[fnGetTotalCasesReport](
        @numStatus tinyint,
	@numAssignedTo BIGINT,
	@intFromDate BIGINT,
	@intToDate BIGINT) RETURNS INTEGER
AS
BEGIN
	DECLARE @TotalCases INTEGER
	if @numStatus=0 
        Begin
	SELECT
	@TotalCases=COUNT(*)
	 FROM cases C 	
	WHERE C.numAssignedTo=@numAssignedTo 
	AND C.bintCreatedDate BETWEEN @intFromDate AND @intToDate
	End
	
        if @numStatus<>0
	Begin
	SELECT
	@TotalCases=COUNT(*)
	 FROM cases C 	
	WHERE C.numAssignedTo=@numAssignedTo AND C.numStatus=@numStatus 
	AND C.bintCreatedDate BETWEEN @intFromDate AND @intToDate
	End
	
	RETURN @TotalCases
END
GO
