/****** Object:  UserDefinedFunction [dbo].[fn_getValidEmailIds]    Script Date: 07/26/2008 18:12:48 ******/

GO

GO
--  SET @vcEmailList = 'ravid@kaba.com , milindv@karchitects.com , 1milindv@exch2kfebe.com , 1Administrator@exch2kfebe.com ,  , 1Maulik.D@exch2kfebe.com , 1NileshL@eazy2pay.com , '
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getvalidemailids')
DROP FUNCTION fn_getvalidemailids
GO
CREATE FUNCTION [dbo].[fn_getValidEmailIds](@vcEmailList VARCHAR(2000))
RETURNS INT 
BEGIN
	Declare @intCharIndex INT , @vcEmailSelect VARCHAR(100) , @intReturnValue INT
	SET @intReturnValue  = 0
	SET @vcEmailList = ',' + Replace(@vcEmailList,' ','')
	SELECT  TOP 1 @intCharIndex =CHARINDEX ( ','+vcEmail+','  , @vcEmailList) , @vcEmailSelect  = MAX(vcEmail)
		 	FROM AdditionalContactsInformation WHERE numContactType = 92 
			Group by vcEmail
			--SELECT @intCharIndex ,@vcEmailSelect
	IF @intCharIndex >=1 
		BEGIN
			SELECT TOP 1 @intCharIndex =CHARINDEX ( ','+ vcEmail+ ',' , Replace( @vcEmailList,@vcEmailSelect  ,'' ))
					FROM AdditionalContactsInformation  
					WHERE LEN(RTRIM(LTRIM(vcEmail))) >= 1
				IF @intCharIndex  >= 1 
				BEGIN
					SET @intReturnValue =  1
				END
				ELSE
				BEGIN
					SET @intReturnValue = 0
				END
		END
		ELSE
			BEGIN
			  	SET @intReturnValue = 0
			END
	RETURN @intReturnValue 
END
GO
