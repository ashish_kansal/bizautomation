

--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_CalBizDocTaxAmt')
DROP FUNCTION fn_CalBizDocTaxAmt
GO
CREATE FUNCTION [dbo].[fn_CalBizDocTaxAmt]
(
	@numDomainID as numeric(9),
	@numTaxItemID as numeric(9),
	@numOppID AS NUMERIC(9),
	@numOppItemID as numeric(9),
	@tintMode AS TINYINT,
	@monAmount AS DECIMAL(20,5),
	@numUnitHour FLOAT
) 
RETURNS DECIMAL(20,5)
AS
BEGIN
	DECLARE @monTaxAmount AS DECIMAL(20,5) = 0

	DECLARE @tintTaxOperator AS TINYINT;
	SET @tintTaxOperator=0
   
	IF @tintMode=1 --Order
	BEGIN
		SELECT @tintTaxOperator=ISNULL([tintTaxOperator],0) FROM [OpportunityMaster] WHERE numOppID=@numOppID and numDomainID=@numDomainID

		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
		IF @numTaxItemID = 0 and @tintTaxOperator = 1
		BEGIN
			SELECT
				@monTaxAmount = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(@numUnitHour,0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(@monAmount,0) / 100 END,0))
			FROM
				OpportunityMasterTaxItems OMTI
			WHERE
				OMTI.numOppId=@numOppID
				AND OMTI.numTaxItemID=0
		END	
		ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
		BEGIN
			SET @monTaxAmount = 0
		END
		ELSE IF (SELECT COUNT(*) FROM OpportunityItemsTaxItems WHERE numOppID=@numOppID AND numOppItemId=@numOppItemID AND numTaxItemID=@numTaxItemID)>0
		BEGIN
			SELECT
				@monTaxAmount = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(@numUnitHour,0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(@monAmount,0) / 100 END,0))
			FROM
				OpportunityItemsTaxItems OITI
			INNER JOIn
				OpportunityMasterTaxItems OMTI
			ON
				OITI.numTaxItemID = OMTI.numTaxItemID
				AND ISNULL(OITI.numTaxID,0) = ISNULL(OMTI.numTaxID,0)
			WHERE
				OMTI.numOppId=@numOppID
				AND OITI.numOppItemID=@numOppItemID
				AND OITI.numTaxItemID=@numTaxItemID
		END 
	END

	IF @tintMode=2 --Return RMA
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityItemsTaxItems WHERE numReturnItemID=@numOppItemID and numTaxItemID=@numTaxItemID)>0
		BEGIN
			SELECT
				@monTaxAmount = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(@numUnitHour,0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(@monAmount,0) / 100 END,0))
			FROM
				OpportunityItemsTaxItems OITI
			INNER JOIn
				OpportunityMasterTaxItems OMTI
			ON
				OITI.numTaxItemID = OMTI.numTaxItemID
				AND ISNULL(OITI.numTaxID,0) = ISNULL(OMTI.numTaxID,0)
			WHERE
				OITI.numReturnHeaderID=@numOppID 
				AND OITI.numTaxItemID=@numTaxItemID 
				AND OITI.numReturnItemID=@numOppItemID 
		END 
	END

	RETURN ISNULL(@monTaxAmount,0)
END
