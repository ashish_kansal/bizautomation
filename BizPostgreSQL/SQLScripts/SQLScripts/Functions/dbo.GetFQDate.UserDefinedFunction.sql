/****** Object:  UserDefinedFunction [dbo].[GetFQDate]    Script Date: 07/26/2008 18:13:03 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getfqdate')
DROP FUNCTION getfqdate
GO
CREATE FUNCTION [dbo].[GetFQDate](@date datetime,@quarter as tinyint,@Period as char(1)='S',@numDomainId numeric(4))  
returns datetime  
as   
begin  

  
declare @year as numeric(4)  
  
set @year =dbo.getfiscalyear(@date,@numDomainId) 
  
  
DECLARE @PeriodDate datetime     

IF    
    
@Period = 'S'
	SELECT @PeriodDate=CASE @quarter 
	   
		WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(@year,@numDomainId))   
		WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(@year,@numDomainId))   
		WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(@year,@numDomainId))  
		WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(@year,@numDomainId))  
	end
else
	SELECT @PeriodDate=CASE @quarter 
		WHEN 1 THEN dateadd(day,-1,dateadd(month,3,dbo.GetFiscalStartDate(@year,@numDomainId)))
		WHEN 2 THEN dateadd(day,-1,dateadd(month,6,dbo.GetFiscalStartDate(@year,@numDomainId))) 
		WHEN 3 THEN  dateadd(day,-1,dateadd(month,9,dbo.GetFiscalStartDate(@year,@numDomainId)))    
		WHEN 4 THEN  dateadd(day,-1,dateadd(month,12,dbo.GetFiscalStartDate(@year,@numDomainId)))
	End
	
  
 
return @PeriodDate  
end
GO
