/****** Object:  UserDefinedFunction [dbo].[GetSalesForeCastForMonthsForProjectedBankBalance]    Script Date: 07/26/2008 18:13:11 ******/

GO

GO
--Created By Siva                       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getsalesforecastformonthsforprojectedbankbalance')
DROP FUNCTION getsalesforecastformonthsforprojectedbankbalance
GO
CREATE FUNCTION [dbo].[GetSalesForeCastForMonthsForProjectedBankBalance](@intType int,@month int,@year int,@numDomainId numeric(9),@numUserCntID numeric(9),@sintForecast int)                
Returns DECIMAL(20,5)                
As                
Begin                
                
Declare @monOppAmt as DECIMAL(20,5)                
Declare @TotalPercentage as float                
Declare @TotalAmt as DECIMAL(20,5)                
Declare @UnclosedDealCount as int                
Declare @Percentage as float                
Declare @currentYear as int     
 Set @TotalAmt=0
    if @intType=0   
 Set @currentYear=year(getutcdate())  
Else If @intType=-1  
 Set @currentYear=year(getutcdate())-1  
Else If @intType=1  
 Set @currentYear=year(getutcdate())+1  
           
if @sintForecast=1           
Begin 
            
    select @TotalAmt= sum(isnull(monForecastAmount,0))
    from Forecast FC  Where tintForecastType=1     
    --(sintYear<@year or (tintmonth<=@Month and sintYear=@Year))     
   and ((tintmonth between month(getutcdate()) And (case when @currentYear=@year then @Month else 0 end) And  sintYear=@year)    
   or (tintmonth between 1 and (case when @currentYear< @year then  @Month else 0 end) and sintYear=@year) Or (tintmonth between month(getutcdate()) and (case when @currentYear< @year then  12 else 0 end)  And sintYear=@year-1))    
   and numDomainID=@numDomainID       
        
End          
Else          
Begin               
 Select  @monOppAmt=Sum(isnull(monPAmount,0)) From OpportunityMaster                 
 Where tintOppType=1 And tintOppStatus<>2 and  numRecOwner=@numUserCntID And tintoppStatus=0        
---- and (year(intPEstimatedCloseDate)<@year Or (month(intPEstimatedCloseDate)<=@Month And year(intPEstimatedCloseDate)=@Year))      
and ((month(intPEstimatedCloseDate) between month(getutcdate()) And (case when @currentYear=@year then @Month else 0 end) And Year(intPEstimatedCloseDate)=@year)    
or (month(intPEstimatedCloseDate) between 1 and (case when @currentYear< @year then  @Month else 0 end) and Year(intPEstimatedCloseDate)=@year) Or (month(intPEstimatedCloseDate) between month(getutcdate()) and (case when @currentYear< @year then  12   
else 0 end)  And Year(intPEstimatedCloseDate)=@year-1))    
and numDomainID=@numDomainId          
         
         
 Select @TotalPercentage =sum(isnull(tintPercentage,0)) from OpportunityMaster Opp                
 inner join  OpportunityStageDetails OSD on Opp.numOppId=OSD.numOppId                
 Where tintOppType=1 And  bitstagecompleted=1 and tintOppStatus<>2 and  Opp.numRecOwner=@numUserCntID  And Opp.tintoppStatus=0                           
---- and (year(intPEstimatedCloseDate)<@year Or (month(intPEstimatedCloseDate)<=@Month And year(intPEstimatedCloseDate)=@Year))      
and ((month(intPEstimatedCloseDate) between month(getutcdate()) And (case when @currentYear=@year then @Month else 0 end) And Year(intPEstimatedCloseDate)=@year)    
or (month(intPEstimatedCloseDate) between 1 and (case when @currentYear< @year then  @Month else 0 end) and Year(intPEstimatedCloseDate)=@year) Or (month(intPEstimatedCloseDate) between month(getutcdate()) and (case when @currentYear< @year then  12   
else 0 end)  And Year(intPEstimatedCloseDate)=@year-1))    
and Opp.numDomainID=@numDomainId                
                
                
 Select @UnclosedDealCount=Count(*) From OpportunityMaster where tintOppType=1 And tintOppStatus<>2 and  numRecOwner=@numUserCntID And tintoppStatus=0                           
-- and (year(intPEstimatedCloseDate)<@year Or (month(intPEstimatedCloseDate)<=@Month And year(intPEstimatedCloseDate)=@Year))     
and ((month(intPEstimatedCloseDate) between month(getutcdate()) And (case when @currentYear=@year then @Month else 0 end) And Year(intPEstimatedCloseDate)=@year)    
or (month(intPEstimatedCloseDate) between 1 and (case when @currentYear< @year then  @Month else 0 end) and Year(intPEstimatedCloseDate)=@year) Or (month(intPEstimatedCloseDate) between month(getutcdate()) and (case when @currentYear< @year then  12   
else 0 end)  And Year(intPEstimatedCloseDate)=@year-1))    
and numDomainID=@numDomainId                  
                
 Set @Percentage=(Case When @TotalPercentage=0 then 1 Else @TotalPercentage End) /(Case When @UnclosedDealCount=0 then 1 Else @UnclosedDealCount End)                
 ----Print @UnclosedDealCount                
 ----Print @monOppAmt                
 ----Print @Percentage                
 --Print @Percentage                
 Set @TotalAmt=isnull(@monOppAmt,0)*@Percentage/100                  
End            
             
--Print @TotalAmt                
Return @TotalAmt                
End
GO
