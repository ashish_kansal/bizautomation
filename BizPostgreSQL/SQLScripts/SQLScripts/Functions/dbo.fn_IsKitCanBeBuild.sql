GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_IsKitCanBeBuild')
DROP FUNCTION fn_IsKitCanBeBuild
GO
CREATE FUNCTION [dbo].[fn_IsKitCanBeBuild]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@numWarehouseID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
)
RETURNS BIT
AS    
BEGIN 
	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
		,bitFirst BIT
		,numOnHand FLOAT
	)
  
	DECLARE @TempExistingItems TABLE
	(
		vcItem VARCHAR(100)
	)

	INSERT INTO @TempExistingItems
	(
		vcItem
	)
	SELECT 
		OutParam 
	FROM 
		SplitString(@vcSelectedKitChildItems,',')

	DECLARE @TEMPSelectedKitChilds TABLE
	(
		ChildKitItemID NUMERIC(18,0),
		ChildKitWarehouseItemID NUMERIC(18,0),
		ChildKitChildItemID NUMERIC(18,0),
		ChildKitChildWarehouseItemID NUMERIC(18,0),
		ChildQty FLOAT
	)

	INSERT INTO @TEMPSelectedKitChilds
	(
		ChildKitItemID
		,ChildKitChildItemID
		,ChildQty
	)
	SELECT 
		Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 1))
		,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2))
		,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3))
	FROM  
	(
		SELECT 
			vcItem 
		FROM 
			@TempExistingItems
	) As [x]

	UPDATE 
		@TEMPSelectedKitChilds 
	SET 
		ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems INNER JOIN WarehouseLocation ON WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID WHERE numItemID=ChildKitItemID AND WareHouseItems.numWareHouseID=@numWarehouseID ORDER BY ISNULL(WarehouseLocation.numWLocationID,0) ASC)
		,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems INNER JOIN WarehouseLocation ON WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID WHERE numItemID=ChildKitChildItemID AND WareHouseItems.numWareHouseID=@numWarehouseID ORDER BY ISNULL(WarehouseLocation.numWLocationID,0) ASC)


	IF (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
	BEGIN
		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
			,numOnHand
		)
		SELECT
			I.numItemCode
			,ChildQty
			,I.numBaseUnit
			,0
			,ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID),0)
		FROM
			@TEMPSelectedKitChilds T1
		INNER JOIN
			Item I
		ON
			T1.ChildKitChildItemID = I.numItemCOde
		INNER JOIN
			WareHouseItems WI
		ON
			T1.ChildKitChildWarehouseItemID = WI.numWareHouseItemID
		WHERE
			ISNULL(ChildKitItemID,0)=0
	END
	ELSE
	BEGIN
		;WITH CTE (numItemCode, vcItemName, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
		(
			SELECT
				ID.numChildItemID,
				I.vcItemName,
				ISNULL(I.bitKitParent,0),
				ISNULL(I.bitCalAmtBasedonDepItems,0),
				CAST((@numQty * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
				ISNULL(numUOMId,0)
			FROM 
				[ItemDetails] ID
			INNER JOIN 
				[Item] I 
			ON 
				ID.[numChildItemID] = I.[numItemCode]
			WHERE   
				[numItemKitID] = @numItemCode
				AND ISNULL(I.charItemType,0) = 'P'
				AND 1 = (CASE 
							WHEN ISNULL(I.bitKitParent,0) = 1 AND LEN(ISNULL(@vcSelectedKitChildItems,'')) > 0 THEN 
								(CASE 
									WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) 
									THEN 1 
									ELSE 0 
								END) 
							ELSE (CASE 
									WHEN (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
									THEN	
										(CASE 
											WHEN numChildItemID IN (SELECT ChildKitChildItemID FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) 
											THEN 1 
											ELSE 0 
										END) 
									ELSE 1
								END)
						END)
			UNION ALL
			SELECT
				ID.numChildItemID,
				I.vcItemName,
				ISNULL(I.bitKitParent,0),
				ISNULL(I.bitCalAmtBasedonDepItems,0),
				CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
				ISNULL(ID.numUOMId,0)
			FROM 
				CTE As Temp1
			INNER JOIN
				[ItemDetails] ID
			ON
				ID.numItemKitID = Temp1.numItemCode
			INNER JOIN 
				[Item] I 
			ON 
				ID.[numChildItemID] = I.[numItemCode]
			WHERE
				Temp1.bitKitParent = 1
				AND ISNULL(I.charItemType,0) = 'P'
				AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND t2.ChildKitItemID=ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)
		)

		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
			,numOnHand
		)
		SELECT
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
			,ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE numItemID=numItemCode AND numWareHouseID=@numWarehouseID),0)
		FROM
			CTE
	END

	DECLARE @bitKitCandBeBuild BIT

	IF EXISTS (SELECT * FROM @TEMPitems WHERE ISNULL(bitKitParent,0) = 0 AND numQtyItemsReq > numOnHand)
	BEGIN
		SET @bitKitCandBeBuild = 0
	END
	ELSE 
	BEGIN
		SET @bitKitCandBeBuild = 1
	END

	RETURN @bitKitCandBeBuild
END