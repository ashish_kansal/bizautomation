/****** Object:  UserDefinedFunction [dbo].[fn_CheckContactAOI]    Script Date: 07/26/2008 18:12:26 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_checkcontactaoi')
DROP FUNCTION fn_checkcontactaoi
GO
CREATE FUNCTION [dbo].[fn_CheckContactAOI](@numCntID numeric, @vcAOIs varchar(550))
Returns Integer 
As
BEGIN
	DECLARE @RetVal numeric
	SET @RetVal=0
	DECLARE @strQuery varchar (1000)
	SET @strQuery = 'SELECT @RetVal=numContactID FROM AOIContactlink WHERE numAOIID IN (' + @vcAOIs + ') AND numContactID=@numCntID'
	EXEC @strQuery
	RETURN @RetVal
END
GO
