/****** Object:  UserDefinedFunction [dbo].[fn_GetBudgetMonthTotalAmt]    Script Date: 07/26/2008 18:12:29 ******/

GO

GO
--Created By Siva                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getbudgetmonthtotalamt')
DROP FUNCTION fn_getbudgetmonthtotalamt
GO
CREATE FUNCTION [dbo].[fn_GetBudgetMonthTotalAmt]               
(@numBudgetId numeric(9),@stDate datetime,@numChartAcntId numeric(9)=0,@numDomainId numeric(9))                  
Returns varchar(10)                
As                  
Begin                   
    Declare @monAmount as DECIMAL(20,5)              
    Declare @montotalAmt as DECIMAL(20,5)             
    Declare @numMonth as tinyint           
    Declare @montotAmt as varchar(10)                     
    set @numMonth =1            
      Set @montotalAmt=0         
   Set @montotAmt=0    
 Set @montotalAmt=0      
 While @numMonth<=12          
Begin          
  Set @monAmount=0    
  Declare @dtFiscalStDate as datetime                            
  --Declare @dtFiscalEndDate as datetime                       
  Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@stDate,@numDomainId),@numDomainId)                         
  Set @dtFiscalStDate=dateadd(month,@numMonth-1,@dtFiscalStDate)                      
  --Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@stDate,@numDomainId),@numDomainId)))                      
  Select @monAmount= isnull(OBD.monAmount,0) From OperationBudgetMaster OBM  
  inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId   
  Where OBM.numDomainId=@numDomainId And OBD.numChartAcntId=@numChartAcntId And OBD.numBudgetId=@numBudgetId   
  and  OBD.tintmonth=month(@dtFiscalStDate) And OBD.intyear=year(@dtFiscalStDate)          
  Set  @montotalAmt =@montotalAmt+@monAmount          
  Set @numMonth=@numMonth+1          
 End          
    if @montotalAmt=0.00 Set @montotAmt=''  Else Set @montotAmt=@montotalAmt        
                    
 Return @montotAmt                  
End
GO
