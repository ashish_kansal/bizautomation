/****** Object:  UserDefinedFunction [dbo].[GetCustFldItems]    Script Date: 07/26/2008 18:12:58 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcustflditems')
DROP FUNCTION getcustflditems
GO
CREATE FUNCTION [dbo].[GetCustFldItems]
(
	@numFldId NUMERIC(18,0)
	,@pageId AS TINYINT
	,@numRecordId AS NUMERIC(18,0)
	,@tintMode AS TINYINT  -- 0: Warehouse Level Attributes, 1: Item Level Attributes 
)  
RETURNS VARCHAR(300)   
AS  
BEGIN  
	DECLARE @vcValue AS VARCHAR(300)

	IF @pageId=9 AND ISNULL(@tintMode,0) = 0
	BEGIN
		SELECT 
			@vcValue=Fld_Value 
		FROM 
			CFW_Fld_Values_Serialized_Items 
		WHERE 
			Fld_ID=@numFldId 
			AND RecId=@numRecordId
	END 
	ELSE IF ISNULL(@tintMode,0) = 1
	BEGIN
		SELECT 
			@vcValue=Fld_Value 
		FROM 
			ItemAttributes 
		WHERE 
			Fld_ID=@numFldId 
			AND numItemCode=@numRecordId
	ENd

	IF @vcValue IS NULL 
		SET @vcValue='0'  
	
	RETURN @vcValue  
END
GO
