GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCommissionContactItemList')
DROP FUNCTION GetCommissionContactItemList
GO
CREATE FUNCTION dbo.GetCommissionContactItemList
    (
      @numRuleID NUMERIC,
      @tintMode TINYINT 
    )
RETURNS VARCHAR(MAX)
AS BEGIN
    DECLARE @ItemList VARCHAR(MAX)

	IF @tintMode = 0
        BEGIN
			DECLARE @tintAssignTo AS TINYINT

			SELECT @tintAssignTo=tintAssignTo FROM CommissionRules WHERE numComRuleID=@numRuleID

			IF @tintAssignTo = 3
			BEGIN
				SELECT 
					@ItemList = COALESCE(@ItemList + ', ', '') + CONCAT(D.vcPartnerCode,'-',C.vcCompanyName,' (Partner)')
				FROM 
					CommissionRuleContacts CC 
				INNER JOIN
					DivisionMaster D
				ON
					CC.numValue = D.numDivisionID
				INNER JOIN
					CompanyInfo C
				ON
					D.numCompanyID =C.numCompanyId
				WHERE 
					CC.numComRuleID=@numRuleID
			END
			ELSE
			BEGIN
				SELECT 
						@ItemList = COALESCE(@ItemList + ', ', '') + CASE CC.bitCommContact WHEN 1 THEN (SELECT vcCompanyName FROM DivisionMaster D join CompanyInfo C on C.numCompanyID=D.numCompanyID  where D.numDivisionID=CC.numValue) + '(Commission Contact)' ELSE dbo.fn_GetContactName(numValue) END
				 FROM    [CommissionRuleContacts] CC
						--INNER JOIN AdditionalContactsInformation A ON  CC.numValue=A.numContactID
				WHERE   CC.[numComRuleID] = @numRuleID
			END
        END
        
    IF @tintMode = 1 
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + vcItemName
            FROM    [CommissionRuleItems] CI
                    INNER JOIN Item I ON I.[numItemCode] = CI.[numValue]
            WHERE   CI.[numComRuleID] = @numRuleID
        END
      
      IF @tintMode = 2
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + vcData
            FROM    [CommissionRuleItems] CI
                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = CI.[numValue]
            WHERE   CI.[numComRuleID] = @numRuleID
        END
       IF @tintMode = 3
        BEGIN

            SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + C.[vcCompanyName]
            FROM    [CommissionRuleOrganization] PD
                    INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = PD.[numValue]
                    LEFT OUTER JOIN [CompanyInfo] C ON C.[numCompanyId] = DM.[numCompanyID]
                    LEFT OUTER JOIN [CommissionRules] PB ON PB.[numComRuleID] = PD.[numComRuleID]
            WHERE   PB.tintComOrgType = 1
                    AND PB.[numComRuleID] = @numRuleID
        END
        
        IF @tintMode = 4
        BEGIN

             SELECT TOP 10
                    @ItemList = COALESCE(@ItemList + ', ', '') + (LI.vcData +'/' + LI1.vcData)
            FROM    [CommissionRuleOrganization] PD
                    INNER JOIN [ListDetails] LI ON LI.[numListItemID] = PD.[numValue]
                    Inner JOIN [ListDetails] LI1 ON LI1.[numListItemID] = PD.[numProfile]
                    LEFT OUTER JOIN [CommissionRules] PB ON PB.[numComRuleID] = PD.[numComRuleID]
            WHERE   PB.tintComOrgType = 2
                    AND PB.[numComRuleID] = @numRuleID
        END
        
    RETURN ISNULL(@ItemList, '')
   END 