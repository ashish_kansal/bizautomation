
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckOrderInvoicingStatus')
DROP FUNCTION CheckOrderInvoicingStatus
GO
CREATE FUNCTION [dbo].[CheckOrderInvoicingStatus] 
 (
      @numOppID AS NUMERIC(9),
      @numDomainID AS NUMERIC(9)
)
RETURNS NVARCHAR(MAX)
AS BEGIN

	DECLARE @tintInvoicing AS NVARCHAR(MAX); SET @tintInvoicing='<font color="#000000">All</font>';

	/********************************************************Fully Invoiced - 1********************************************************/
	--EXEC USP_CheckOrderedAndInvoicedOrBilledQty 23749



	--IF EXISTS(select 1)
	--BEGIN
	--	IF(SELECT COUNT(*) FROM Opportunity WHERE Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
 --                     			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
 --                     			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
 --                     			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
 --                     			Where OM.numDomainId=' + CONVERT(VARCHAR(15),@numDomainId) + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
 --                     			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour]))

	--	SET @tintInvoicing='<font color="#008000">Fully Invoiced</font>';
	--END

	/********************************************************UN-INVOICED - 2********************************************************/
	IF (SELECT COUNT(*) 
		FROM OpportunityBizDocs 
		WHERE numOppId = @numOppID 
			AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = @numDomainID)) = 0
	BEGIN
		SET @tintInvoicing = '<font color="#800080">Un-Invoiced</font>';
	END

	/********************************************************PARTIALLY INVOICED (UNITS) - 3********************************************************/
	DECLARE @numUnits INT

	SELECT @numUnits = (SUM(ISNULL(OrderedQty,0)) - SUM(ISNULL(InvoicedQty,0)))
	FROM 
	(
		SELECT OI.numoppitemtCode, ISNULL(OI.numUnitHour,0) AS OrderedQty, ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
		FROM OpportunityItems OI
		INNER JOIN Item I ON OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
			FROM OpportunityBizDocs
				INNER JOIN OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			WHERE OpportunityBizDocs.numOppId = @numOppID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = @numDomainID)
		) AS TempInvoice
		WHERE
		OI.numOppID = @numOppID
	) X

	IF @numUnits > 0
	BEGIN
		SET @tintInvoicing = '<font color="#800080">Partially Invoiced(Units)</font>';
	END
	/********************************************************PARTIALLY INVOICED (%) - 4********************************************************/

	--SELECT monDealAmount,* FROM OpportunityMaster WHERE numOppId = 717
	--SELECT SUM(monDealAmount) FROM OpportunityBizDocs WHERE numOppId=717 AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=72)

	IF (SELECT SUM(ISNULL(monDealAmount,0)) 
	FROM OpportunityBizDocs 
	WHERE numOppId = @numOppID AND numBizDocId IN (SELECT numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId = @numDomainID))>0

	BEGIN
		SET @tintInvoicing = '<font color="#800080">Partially Invoiced(%)</font>';
	END
    
	/********************************************************Deferred Income Invoices - 5********************************************************/


    RETURN ISNULL(@tintInvoicing,'')
END
