GO
--select dbo.GetDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPaidAmount')
DROP FUNCTION GetPaidAmount
GO
CREATE FUNCTION [dbo].[GetPaidAmount]
(@numOppID numeric)    
returns DECIMAL(20,5)    
as    
begin   
DECLARE @TotalAmtPaid AS DECIMAL(20,5)
SET @TotalAmtPaid=(SELECT SUM(isnull(monAmountPaid ,0)) FROM OpportunityBizDocs WHERE numOppId=@numOppID)
return CAST(@TotalAmtPaid AS DECIMAL(20,5)) -- Set Accuracy of Two precision
END
GO