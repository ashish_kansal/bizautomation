/****** Object:  UserDefinedFunction [dbo].[fn_OrgAndContCustomFieldsColumns]    Script Date: 07/26/2008 18:12:50 ******/

GO

GO
--Created By: Debasish Nag            
--Created On: 11th Nov 2005            
--Purpose: To Get the Custom Fields for Organization and Contacts as a string of columns          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_organdcontcustomfieldscolumns')
DROP FUNCTION fn_organdcontcustomfieldscolumns
GO
CREATE FUNCTION [dbo].[fn_OrgAndContCustomFieldsColumns](@numLocationId tinyInt, @ReturnType tinyInt)           
 RETURNS NVarchar(4000)          
AS            
BEGIN            
 DECLARE @vcCustomOrgContactFieldList AS NVarchar(4000)            
 IF @numLocationId  = 1  --ORGANIZATION          
 BEGIN          
  IF @ReturnType = 0 --Column list along with their datatypes          
  BEGIN            
   SELECT @vcCustomOrgContactFieldList = Coalesce(@vcCustomOrgContactFieldList + ', ', '') +             
     'CFld' + Convert(NVarchar, cfm.fld_id) + ' NVarchar(50)'          
   FROM CFW_Fld_Master cfm            
   WHERE cfm.Grp_id = 1           
   GROUP BY cfm.Fld_id            
  END          
  IF @ReturnType = 1 --Column list          
  BEGIN            
   SELECT @vcCustomOrgContactFieldList = Coalesce(@vcCustomOrgContactFieldList + ', ', '') +             
     'CFld' + Convert(NVarchar, cfm.fld_id)          
   FROM CFW_Fld_Master cfm            
   WHERE cfm.Grp_id = 1           
   GROUP BY cfm.Fld_id            
  END          
  IF @ReturnType = 2 --Function list to populate the data          
  BEGIN            
   SELECT @vcCustomOrgContactFieldList = Coalesce(@vcCustomOrgContactFieldList + ', ', '') +             
     '(SELECT TOP 1 Case WHEN M.Fld_type = ''CheckBox'' THEN cfcov' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value WHEN M.Fld_type = ''SelectBox'' THEN dbo.fn_AdvSearchColumnName(cfcov' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value,''L'')   
     ELSE cfcov' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value END FROM CFW_Fld_Master M, CFW_Fld_Values cfcov' + Convert(NVarchar, cfm.fld_id) + ' WHERE M.Fld_ID = ' + Convert(NVarchar, cfm.fld_id) + ' AND recId = numDivisionId AND   
     cfcov' + Convert(NVarchar, cfm.fld_id) + '.Fld_Id = M.Fld_Id AND M.Grp_Id = 1) As CFld' + Convert(NVarchar, cfm.fld_id)          
   FROM CFW_Fld_Master cfm            
   WHERE cfm.Grp_id = 1          
   GROUP BY cfm.Fld_id            
  END          
 END          
 ELSE IF @numLocationId = 4 --CONTACTS          
 BEGIN          
  IF @ReturnType = 0 --Column list along with their datatypes          
  BEGIN            
   SELECT @vcCustomOrgContactFieldList = Coalesce(@vcCustomOrgContactFieldList + ', ', '') +             
     'CFld' + Convert(NVarchar, cfm.fld_id) + ' NVarchar(50)'          
   FROM CFW_Fld_Master cfm            
   WHERE cfm.Grp_id = 4          
   GROUP BY cfm.Fld_id            
  END          
  IF @ReturnType = 1 --Column list          
  BEGIN            
   SELECT @vcCustomOrgContactFieldList = Coalesce(@vcCustomOrgContactFieldList + ', ', '') +             
     'CFld' + Convert(NVarchar, cfm.fld_id)          
   FROM CFW_Fld_Master cfm            
   WHERE cfm.Grp_id = 4          
   GROUP BY cfm.Fld_id            
  END          
  IF @ReturnType = 2 --Function list to populate the data          
  BEGIN            
   SELECT @vcCustomOrgContactFieldList = Coalesce(@vcCustomOrgContactFieldList + ', ', '') +             
     '(SELECT TOP 1 Case WHEN M.Fld_type = ''CheckBox'' THEN cfcnv' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value WHEN M.Fld_type = ''SelectBox'' THEN dbo.fn_AdvSearchColumnName(cfcnv' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value,''L'')   
     ELSE cfcnv' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value END FROM CFW_Fld_Master M, CFW_Fld_Values_Cont cfcnv' + Convert(NVarchar, cfm.fld_id) + ' WHERE M.Fld_ID = ' + Convert(NVarchar, cfm.fld_id) + ' AND recId = numContactId   
    AND cfcnv' + Convert(NVarchar, cfm.fld_id) + '.Fld_Id = M.Fld_Id AND M.Grp_Id = 4) As CFld' + Convert(NVarchar, cfm.fld_id)          
   FROM CFW_Fld_Master cfm            
   WHERE cfm.Grp_id = 4          
   GROUP BY cfm.Fld_id            
  END          
 END          
 RETURN @vcCustomOrgContactFieldList          
END
GO
