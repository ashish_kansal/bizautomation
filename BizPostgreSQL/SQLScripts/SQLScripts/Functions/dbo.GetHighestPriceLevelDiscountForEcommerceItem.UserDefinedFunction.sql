GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'FN'
                    AND NAME = 'GetHighestPriceLevelDiscountForEcommerceItem' ) 
    DROP FUNCTION GetHighestPriceLevelDiscountForEcommerceItem
GO
CREATE FUNCTION GetHighestPriceLevelDiscountForEcommerceItem
--Below function applies Price Rule on given Price and Qty and returns discount
    (
      @numItemCode AS NUMERIC(9),
      @numDomainID AS NUMERIC(9) = 0,
      @numWareHouseItemID AS NUMERIC(9)
    )
RETURNS DECIMAL(20,5)
AS BEGIN
	DECLARE @monHighestDiscount AS DECIMAL(20,5);
	
    DECLARE @monListPrice AS DECIMAL(20,5) ;
        SET @monListPrice = 0
    DECLARE @monVendorCost AS DECIMAL(20,5) ;
        SET @monVendorCost = 0

    IF ( ( @numWareHouseItemID > 0 )
         AND EXISTS ( SELECT    *
                      FROM      item
                      WHERE     numItemCode = @numItemCode
                                AND bitSerialized = 0
                                AND charItemType = 'P' )
       ) 
        BEGIN      
            SELECT  @monListPrice = ISNULL(monWListPrice, 0)
            FROM    WareHouseItems
            WHERE   numWareHouseItemID = @numWareHouseItemID      
            IF @monListPrice = 0 
                SELECT  @monListPrice = monListPrice
                FROM    Item
                WHERE   numItemCode = @numItemCode       
        END      
    ELSE 
        BEGIN      
            SELECT  @monListPrice = monListPrice
            FROM    Item
            WHERE   numItemCode = @numItemCode      
        END 

    SELECT  @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)

    SELECT TOP 1 @monHighestDiscount = CASE WHEN tintRuleType = 1 AND tintDiscountType = 1 --Deduct from List price & Percentage
										 THEN decDiscount 
										 WHEN tintRuleType = 1 AND tintDiscountType = 2 --Deduct from List price & Flat discount
										 THEN (decDiscount * 100) / (@monListPrice - decDiscount)
										 WHEN tintRuleType = 2 AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
										 THEN decDiscount
										 WHEN tintRuleType = 2 AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
										 THEN (decDiscount * 100) / (@monVendorCost + decDiscount)
										 WHEN tintRuleType = 3 --Named Price 
										 THEN 0
							 END
    FROM    [PricingTable]
    WHERE   ISNULL(numItemCode, 0) = @numItemCode AND ISNULL(numCurrencyID,0) = 0
    ORDER BY [numPricingID] 
    
    RETURN ISNULL(@monHighestDiscount,0)
    
END
