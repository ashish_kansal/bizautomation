/****** Object:  UserDefinedFunction [dbo].[GetDomainNameFromURL]    Script Date: 08/14/2009 00:14:28 ******/

GO

GO
-- select dbo.GetDomainNameFromURL('http://www.localhost.net/BizCart/Pages/19/BizVsNetSuit.aspx')
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getdomainnamefromurl')
DROP FUNCTION getdomainnamefromurl
GO
CREATE FUNCTION [dbo].[GetDomainNameFromURL]
               (@vcURL VARCHAR(1000))
RETURNS VARCHAR(100)
AS
  BEGIN
    DECLARE  @DomainName VARCHAR(100)
   DECLARE @index INTEGER	
    SET @DomainName =REPLACE(@vcURL,'http://','')
    SET @DomainName =REPLACE(@DomainName,'https://','')
    SET @index = CHARINDEX('/',@DomainName)
    IF @index >0 
    SET @DomainName = SUBSTRING(@DomainName,0,@index)
	SET @DomainName = REPLACE(@DomainName,'www.','')
	--SET @index = ISNULL(CHARINDEX('.',@DomainName),0)
	SET @index = CHARINDEX('/',@DomainName)
	IF @index >0 
		SET @DomainName = SUBSTRING(@DomainName,0,@index)
	
    RETURN isnull(@DomainName,'')
  END
