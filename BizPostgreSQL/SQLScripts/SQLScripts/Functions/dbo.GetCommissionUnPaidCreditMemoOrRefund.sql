GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetCommissionUnPaidCreditMemoOrRefund')
DROP FUNCTION GetCommissionUnPaidCreditMemoOrRefund
GO
CREATE FUNCTION [dbo].[GetCommissionUnPaidCreditMemoOrRefund]
(
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT,
	 @dtStartDate DATE,
	 @dtEndDate DATE
)    
RETURNS @TempCommissionUnPaidCreditMemoOrRefund TABLE
(
	numUserCntID NUMERIC(18,0),
	numReturnHeaderID NUMERIC(18,0),
	tintReturnType TINYINT,
	numReturnItemID NUMERIC(18,0),
	monCommission DECIMAL(20,5),
	numComRuleID NUMERIC(18,0),
	tintComType TINYINT,
	tintComBasedOn TINYINT,
	decCommission FLOAT,
	tintAssignTo TINYINT
)    
AS    
BEGIN   
	DECLARE @monCommissionAmount AS DECIMAL(20,5)

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numPartner NUMERIC(18,0)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		DivisionMaster.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		(CASE WHEN ISNULL(OMSalesReturn.numAssignedTo,0) > 0 THEN OMSalesReturn.numAssignedTo ELSE OMCreditMemo.numAssignedTo END),
		(CASE WHEN ISNULL(OMSalesReturn.numRecOwner,0) > 0 THEN OMSalesReturn.numRecOwner ELSE OMCreditMemo.numRecOwner END),
		Item.numItemCode,
		Item.numItemClassification,
		ReturnHeader.numReturnHeaderID,
		ReturnHeader.tintReturnType,
		(CASE WHEN tintReturnType IN (1,2) THEN ReturnItems.numReturnItemID ELSE 0 END),
		(CASE WHEN tintReturnType IN (1,2) THEN ISNULL(ReturnItems.numUnitHour,0) ELSE 0 END),
		(CASE WHEN tintReturnType IN (1,2) THEN ISNULL(ReturnItems.monTotAmount,0) ELSE ReturnHeader.monAmount END),
		(CASE WHEN tintReturnType IN (1,2) THEN (ISNULL(ReturnItems.monVendorCost,ISNULL((SELECT Vendor.monCost FROM Item INNER JOIN Vendor ON Item.numVendorID=Vendor.numVendorID AND Vendor.numItemCode=Item.numItemCode WHERE Item.numItemCode=ReturnItems.numItemCode),0)) * ISNULL(ReturnItems.numUnitHour,0)) ELSE 0 END),
		(CASE WHEN tintReturnType IN (1,2) THEN (ISNULL(ReturnItems.monAverageCost,Item.monAverageCost) * ISNULL(ReturnItems.numUnitHour,0)) ELSE 0 END),
		(CASE WHEN ISNULL(OMSalesReturn.numPartner,0) > 0 THEN OMSalesReturn.numPartner ELSE OMCreditMemo.numPartner END)
	FROM 
		ReturnHeader
	INNER JOIN
		DivisionMaster 
	ON
		ReturnHeader.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		OpportunityMaster OMSalesReturn
	ON
		OMSalesReturn.numOppId = ReturnHeader.numOppId
	LEFT JOIN
		OpportunityMaster OMCreditMemo
	ON
		OMCreditMemo.numOppId = ReturnHeader.numReferenceSalesOrder
	LEFT JOIN
		ReturnItems 
	ON
		ReturnHeader.numReturnHeaderID=ReturnItems.numReturnHeaderID
	LEFT JOIN
		OpportunityItems
	ON
		ReturnItems.numOppItemID=OpportunityItems.numoppitemtCode
	LEFT JOIN
		Item
	ON
		ReturnItems.numItemCode = Item.numItemCode
	WHERE
		ReturnHeader.numDomainId = @numDomainID 
		AND ReturnHeader.numReturnStatus = 301
		AND (ISNULL(ReturnHeader.numOppId,0) > 0 OR ISNULL(ReturnHeader.numReferenceSalesOrder,0) > 0)
		AND (ReturnHeader.dtCreatedDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,ReturnHeader.dtCreatedDate) AS DATE) BETWEEN @dtStartDate AND @dtEndDate))
		AND (ReturnItems.numItemCode IS NULL OR ReturnItems.numItemCode NOT IN ( @numDiscountServiceItemID))  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN ReturnItems.numItemCode IS NOT NULL AND ReturnItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */

		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numReturnHeaderID NUMERIC(18,0),
			tintReturnType TINYINT,
			numReturnItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount DECIMAL(20,5),
			monVendorCost DECIMAL(20,5),
			monAvgCost DECIMAL(20,5)
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission DECIMAL(20,5)
			DECLARE @numTotalAmount AS DECIMAL(20,5)
			DECLARE @numTotalUnit AS FLOAT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numReturnHeaderID,
				tintReturnType,
				numReturnItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE 
					WHEN tintReturnType = 3 OR tintReturnType = 4 THEN 1
					ELSE 
						(CASE @tintComAppliesTo
							--SPECIFIC ITEMS
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
							-- ITEM WITH SPECIFIC CLASSIFICATIONS
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
							-- ALL ITEMS
							ELSE 1
						END)
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- Order Partner
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempCommissionUnPaidCreditMemoOrRefund 
					(
						numUserCntID,
						numReturnHeaderID,
						tintReturnType,
						numReturnItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission,
						tintAssignTo
					)
					SELECT
						numUserCntID,
						numReturnHeaderID,
						tintReturnType,
						numReturnItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE tintReturnType
										WHEN 3 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
										WHEN 4 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
										ELSE
											CASE @tintCommissionType
												--TOTAL AMOUNT PAID
												WHEN 1 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
												--ITEM GROSS PROFIT (VENDOR COST)
												WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / CAST(100 AS FLOAT))
												--ITEM GROSS PROFIT (AVERAGE COST)
												WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / CAST(100 AS FLOAT))
											END
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission,
						@tintAssignTo
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE tintReturnType
								WHEN 3 THEN 1
								WHEN 4 THEN 1
								ELSE
									(CASE @tintCommissionType 
										--TOTAL PAID INVOICE
										WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
										-- ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
										-- ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
										ELSE
											0 
									END)
							END)
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempCommissionUnPaidCreditMemoOrRefund 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numReturnHeaderID=T1.numReturnHeaderID 
									AND tintReturnType = T1.tintReturnType 
									AND numReturnItemID = T1.numReturnItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END


	RETURN
END
GO