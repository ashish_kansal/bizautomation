/****** Object:  UserDefinedFunction [dbo].[GetCustFldValueRtNull]    Script Date: 07/26/2008 18:12:59 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcustfldvaluertnull')
DROP FUNCTION getcustfldvaluertnull
GO
CREATE FUNCTION [dbo].[GetCustFldValueRtNull](@numFldId numeric(9),@pageId as tinyint,@numRecordId as numeric(9))    
 RETURNS varchar (100)     
AS    
BEGIN    
    
declare @vcValue as  varchar(100)    
if @pageId=1    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=4    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values_Cont where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=3    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values_Case where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
    
if @pageId=5    
begin    
 select @vcValue=Fld_Value from CFW_FLD_Values_Item where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=2 or @pageId=6    
begin    
 select @vcValue=Fld_Value from CFW_Fld_Values_Opp where Fld_ID=@numFldId and RecId=@numRecordId    
    
end    
    
if @pageId=7 or @pageId=8    
begin    
 select @vcValue=Fld_Value from CFW_Fld_Values_Product where Fld_ID=@numFldId and RecId=@numRecordId    
    
end   
  
if @pageId=9  
begin  
 select @vcValue=Fld_Value from CFW_Fld_Values_Serialized_Items where Fld_ID=@numFldId and RecId=@numRecordId    
    
end   

if @pageId=11  
begin  
 select @vcValue=Fld_Value from CFW_FLD_Values_Pro where Fld_ID=@numFldId and RecId=@numRecordId    
end   
    
if @vcValue ='0' set @vcValue=null   
    
RETURN @vcValue    
    
END
GO
