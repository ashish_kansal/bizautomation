/****** Object:  UserDefinedFunction [dbo].[GetTotalDealsWonForBestPartners]    Script Date: 07/26/2008 18:13:13 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='gettotaldealswonforbestpartners')
DROP FUNCTION gettotaldealswonforbestpartners
GO
CREATE FUNCTION [dbo].[GetTotalDealsWonForBestPartners](
	@numCompanyID NUMERIC(9)) RETURNS INTEGER
AS
BEGIN
	DECLARE @dcDealsWon INTEGER
	SELECT
	@dcDealsWon=COUNT(OM.numOppID)
		FROM         AdditionalContactsInformation AI
		INNER JOIN DivisionMaster DM
		ON AI.numDivisionId = DM.numDivisionID 
		INNER JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId 
		INNER JOIN OpportunityMaster OM
		ON DM.numDivisionID = OM.numDivisionId
		WHERE OM.tintOppStatus = 1
		AND CI.numCompanyID=@numCompanyID
		GROUP BY OM.numOppID



	RETURN @dcDealsWon
END
GO
