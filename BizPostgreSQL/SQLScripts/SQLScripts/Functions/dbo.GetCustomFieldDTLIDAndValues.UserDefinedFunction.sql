GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetCustomFieldDTLIDAndValues')
DROP FUNCTION GetCustomFieldDTLIDAndValues

GO
CREATE FUNCTION [dbo].[GetCustomFieldDTLIDAndValues]
(
	@PageID integer
	,@RecID numeric(9)
)
RETURNS @Results TABLE 
(
	FldDTLID numeric(9)
	,Fld_ID numeric(9)
	,Fld_Value varchar(1000)
) 
AS
BEGIN
	IF @pageId=1 OR @PageId= 12 OR @PageId= 13 OR @PageId= 14
	BEGIN 
		INSERT INTO @Results 
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)    
		SELECT 
			FldDTLID
			,Fld_ID
			,Fld_Value 
		FROM 
			CFW_FLD_Values 
		WHERE 
			RecId=@RecID
	END      
	ELSE IF @pageId=4      
	BEGIN
		INSERT INTO @Results 
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)      
		SELECT 
			FldDTLID
			,Fld_ID
			,Fld_Value 
		FROM 
			CFW_FLD_Values_Cont 
		WHERE 
			RecId=@RecID       
	END
	ELSE IF @pageId=3      
	BEGIN
		INSERT INTO @Results 
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)      
		SELECT 
			FldDTLID
			,Fld_ID
			,Fld_Value 
		FROM 
			CFW_FLD_Values_Case 
		WHERE 
			RecId=@RecID	  
	END 
	ELSE IF @pageId=5      
	BEGIN      
		INSERT INTO @Results 
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)      
		SELECT 
			FldDTLID
			,Fld_ID
			,Fld_Value 
		FROM
			CFW_FLD_Values_Item 
		WHERE 
			RecId=@RecID      
	END
	ELSE IF @pageId=2 or @pageId=6      
	BEGIN
		INSERT INTO @Results 
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)      
		SELECT 
			FldDTLID
			,Fld_ID
			,Fld_Value 
		FROM 
			CFW_Fld_Values_Opp 
		WHERE 
			RecId=@RecID
	END
	ELSE IF @pageId=7 or @pageId=8      
	BEGIN      
		INSERT INTO @Results 
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)      
		SELECT 
			FldDTLID
			,Fld_ID
			,Fld_Value  
		FROM 
			CFW_Fld_Values_Product 
		WHERE 
			RecId=@RecID      
	END
	ELSE IF @pageId=9    
	BEGIN 
		INSERT INTO @Results 
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)      
		SELECT 
			FldDTLID
			,Fld_ID
			,Fld_Value 
		FROM 
			CFW_Fld_Values_Serialized_Items 
		WHERE 
			RecId=@RecID
	END
	ELSE IF @pageId=11    
	BEGIN  
		INSERT INTO @Results 
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)      
		SELECT 
			FldDTLID
			,Fld_ID
			,Fld_Value 
		FROM 
			CFW_Fld_Values_Pro 
		WHERE 
			RecId=@RecID
	END
	ELSE IF @pageId=18
	BEGIN  
  
		INSERT INTO @Results 
		(
			FldDTLID
			,Fld_ID
			,Fld_Value
		)      
		SELECT 
			FldDTLID
			,Fld_ID
			,Fld_Value  
		FROM 
			CFW_Fld_Values_OppItems 
		WHERE 
			RecId=@RecID
	END    

	RETURN     
END
GO
