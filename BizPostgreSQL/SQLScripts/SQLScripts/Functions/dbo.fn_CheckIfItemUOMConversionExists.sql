GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_CheckIfItemUOMConversionExists')
DROP FUNCTION fn_CheckIfItemUOMConversionExists
GO
CREATE FUNCTION [dbo].[fn_CheckIfItemUOMConversionExists]
(
	@numDomainID NUMERIC,
	@numItemCode NUMERIC,
	@numSourceUnit NUMERIC
)
RETURNS BIT
AS
BEGIN
	DECLARE @bitExists BIT  = 0
	DECLARE @numBaseUnit AS NUMERIC(18,0) = 0

	SELECT @numBaseUnit=ISNULL(numBaseUnit,0) FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	
	IF @numSourceUnit=@numBaseUnit
		SET @bitExists = 1
	ELSE
	BEGIN
		DECLARE @intConversionsFount AS INT = 0

		;WITH CTE(FromUnit,LevelNum,numSourceUOM,numTargetUOM,numTargetUnit) AS
		(
			SELECT  
				IUOM.numSourceUOM,
				0 AS LevelNum,
				IUOM.numSourceUOM,
				IUOM.numTargetUOM,
				IUOM.numTargetUnit
			FROM 
				ItemUOMConversion IUOM
			WHERE   
				IUOM.numDomainID=@numDomainID
				AND IUOM.numItemCode = @numItemCode
				AND IUOM.numSourceUOM = @numSourceUnit
			UNION ALL
			SELECT  
				cte.FROMUnit,
				LevelNum + 1,
				IUOM.numSourceUOM,
				IUOM.numTargetUOM,
				(IUOM.numTargetUnit * cte.numTargetUnit)
			FROM 
				ItemUOMConversion IUOM
			INNER JOIN 
				CTE cte 
			ON 
				IUOM.numSourceUOM = cte.numTargetUOM 
			WHERE 
				IUOM.numDomainID=@numDomainID
				AND IUOM.numItemCode = @numItemCode
		)

		SELECT @intConversionsFount=COUNT(*) FROM CTE WHERE FROMUnit = @numSourceUnit AND numTargetUOM = @numBaseUnit

		IF ISNULL(@intConversionsFount,0) > 0
		BEGIN
			SET @bitExists = 1
		END
	END

	RETURN @bitExists
END
GO
