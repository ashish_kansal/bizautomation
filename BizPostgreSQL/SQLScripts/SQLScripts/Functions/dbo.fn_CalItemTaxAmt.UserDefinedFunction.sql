

--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_calitemtaxamt')
DROP FUNCTION fn_calitemtaxamt
GO
CREATE FUNCTION [dbo].[fn_CalItemTaxAmt]
(
	@numDomainID as numeric(9),
	@numDivisionID as numeric(9),
	@numItemCode as numeric(9),
	@numTaxItemID as numeric(9),
	@numOppId as numeric(9),
	@bitAlwaysTaxApply AS BIT,
	@numReturnHeaderID AS NUMERIC(9)
) 
RETURNS @TEMP TABLE
(
	decTaxValue FLOAT, 
	tintTaxType TINYINT,
	numTaxID NUMERIC(18,0)
)
AS
BEGIN

	SET @numOppId=NULLIF(@numOppId,0)
	SET @numReturnHeaderID=NULLIF(@numReturnHeaderID,0)

	DECLARE @numTaxID AS NUMERIC(18,0)
	declare @tintBaseTaxCalcOn as tinyint;
	declare @tintBaseTaxOnArea as tinyint;

	SELECT  @tintBaseTaxCalcOn = [tintBaseTax],@tintBaseTaxOnArea=tintBaseTaxOnArea FROM [Domain] WHERE [numDomainId] = @numDomainID

	declare @TaxPercentage as float
	DECLARE @tintTaxType AS TINYINT = 1 -- 1 = PERCENTAGE, 2 = FLAT AMOUNT
	declare @bitTaxApplicable as bit;set @bitTaxApplicable=0 ---Check Tax is applied for the Item

	IF @numTaxItemID = 0 AND @numItemCode <> 0
	BEGIN
		SELECT 
			@bitTaxApplicable=bitTaxable 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode
	END
	ELSE IF @numItemCode <> 0
	BEGIN
		SELECT 
			@bitTaxApplicable=bitApplicable,
			@numTaxID = numTaxID
		FROM 
			ItemTax 
		WHERE 
			numItemCode=@numItemCode 
			AND numTaxItemID=@numTaxItemID
	END
	ELSE IF @numItemCode=0 
	BEGIN
		SET @bitTaxApplicable=1
	END


	---Check Tax is applied for Company
	if @bitTaxApplicable=1 AND @bitAlwaysTaxApply=0
	BEGIN
		set @bitTaxApplicable=0

		if @numTaxItemID=0 
			select @bitTaxApplicable=(case when bitNoTax=1 then 0 else 1 end) from DivisionMaster where numDivisionID=@numDivisionID
		else
			select @bitTaxApplicable=bitApplicable from DivisionTaxTypes where numDivisionID=@numDivisionID and numTaxItemID=@numTaxItemID
	END


	if @bitTaxApplicable=1
	BEGIN

		declare @numCountry as numeric(9),@numState as numeric(9)
		declare @vcCity as varchar(100),@vcZipPostal as varchar(20)

		IF @numOppId>0
		BEGIN
		  DECLARE  @tintOppType  AS TINYINT,@tintBillType  AS TINYINT,@tintShipType  AS TINYINT, @bitDropShipAddress BIT
	 
			SELECT  @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType,@bitDropShipAddress = ISNULL(bitDropShipAddress,0)
			  FROM   OpportunityMaster WHERE  numOppId = @numOppId
	 
			IF @tintBaseTaxCalcOn = 1 --Billing Address
			BEGIN
				IF @tintBillType IS NULL or (@tintBillType = 1 AND @tintOppType = 1) or (@tintBillType = 1 AND @tintOppType = 2)
				BEGIN
					Select @numState = ISNULL(AD.numState, 0),
					@numCountry = ISNULL(AD.numCountry, 0),
					@vcCity=isnull(AD.VcCity,''),@vcZipPostal=isnull(AD.vcPostalCode,'')
					from AddressDetails AD where AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
				END
				ELSE IF @tintBillType = 0
				BEGIN
					Select @numState = ISNULL(AD1.numState, 0),
						@numCountry = ISNULL(AD1.numCountry, 0),
						@vcCity=isnull(AD1.VcCity,''),@vcZipPostal=isnull(AD1.vcPostalCode,'')
							FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
								JOIN Domain D1  ON D1.numDivisionID = div1.numDivisionID
								JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
								WHERE  D1.numDomainID = @numDomainID
				END
				 ELSE IF @tintBillType = 2 or @tintBillType = 3
				BEGIN
					Select @numState = ISNULL(numBillState, 0),
					@numCountry = ISNULL(numBillCountry, 0),
					@vcCity=isnull(VcBillCity,''),@vcZipPostal=isnull(vcBillPostCode,'')				
					FROM   OpportunityAddress WHERE  numOppID = @numOppId
				END
			
			END
			ELSE --Shipping Address(Default)
			BEGIN
				IF @bitDropShipAddress = 1
				BEGIN
					Select @numState = ISNULL(numShipState, 0),
						@numCountry = ISNULL(numShipCountry, 0),
						@vcCity=isnull(vcShipCity,''),@vcZipPostal=isnull(vcShipPostCode,'')
						FROM  OpportunityAddress WHERE  numOppID = @numOppId
				END
				ELSE IF @tintShipType IS NULL or (@tintShipType = 1 AND @tintOppType = 1) or (@tintShipType = 1 AND @tintOppType = 2) 
				BEGIN
						Select @numState = ISNULL(AD2.numState, 0),
						@numCountry = ISNULL(AD2.numCountry, 0),
						@vcCity=isnull(AD2.VcCity,''),@vcZipPostal=isnull(AD2.vcPostalCode,'')				
						from AddressDetails AD2 where AD2.numDomainID=@numDomainID AND AD2.numRecordID=@numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1
				END
				ELSE IF @tintShipType = 0 
				BEGIN
					Select @numState = ISNULL(AD1.numState, 0),
					@numCountry = ISNULL(AD1.numCountry, 0),
					@vcCity=isnull(AD1.VcCity,''),@vcZipPostal=isnull(AD1.vcPostalCode,'')	
					FROM companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
							JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
							JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
							WHERE  D1.numDomainID = @numDomainID
				END
				ELSE IF @tintShipType = 2 or @tintShipType = 3  
				BEGIN
					Select @numState = ISNULL(numShipState, 0),
						@numCountry = ISNULL(numShipCountry, 0),
						@vcCity=isnull(vcShipCity,''),@vcZipPostal=isnull(vcShipPostCode,'')
						FROM  OpportunityAddress WHERE  numOppID = @numOppId
				END
			END
		END
		ELSE IF @numReturnHeaderID>0
		BEGIN
			IF @tintBaseTaxCalcOn = 1 --Billing Address
			BEGIN
				Select @numState = ISNULL(numBillState, 0),
				@numCountry = ISNULL(numBillCountry, 0),
				@vcCity=isnull(VcBillCity,''),@vcZipPostal=isnull(vcBillPostCode,'')				
				FROM   OpportunityAddress WHERE  numReturnHeaderID = @numReturnHeaderID
			END
			ELSE --Shipping Address(Default)
			BEGIN
				Select @numState = ISNULL(numShipState, 0),
					@numCountry = ISNULL(numShipCountry, 0),
					@vcCity=isnull(vcShipCity,''),@vcZipPostal=isnull(vcShipPostCode,'')
					FROM  OpportunityAddress WHERE  numReturnHeaderID = @numReturnHeaderID
			END
		END
		ELSE
		BEGIN
		   IF @tintBaseTaxCalcOn = 2 --Shipping Address(Default)
				   SELECT   @numState = ISNULL(numState, 0),
							@numCountry = ISNULL(numCountry, 0),
							@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')
				   FROM     dbo.AddressDetails
				   WHERE    numDomainID = @numDomainID
							AND numRecordID = @numDivisionID
							AND tintAddressOf = 2
							AND tintAddressType = 2
							AND bitIsPrimary=1
			ELSE --Billing Address
					SELECT   @numState = ISNULL(numState, 0),
							@numCountry = ISNULL(numCountry, 0),
							@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')
				   FROM     dbo.AddressDetails
				   WHERE    numDomainID = @numDomainID
							AND numRecordID = @numDivisionID
							AND tintAddressOf = 2
							AND tintAddressType = 1
							AND bitIsPrimary=1
		END 



		If EXISTS (SELECT tintBaseTaxOnArea FROM TaxCountryConfi where numDomainID=@numDomainID and numCountry=@numCountry)
		BEGIN
			--TAKE FROM TAXCOUNTRYCONFI IF NOT THEN USE DEFAULT FROM DOMAIN
			SELECT @tintBaseTaxOnArea=tintBaseTaxOnArea FROM TaxCountryConfi WHERE numDomainID=@numDomainID AND numCountry=@numCountry 
		END
             
		SET @TaxPercentage=0 
             
		IF @numCountry >0 and @numState>0
		BEGIN
			IF @numState>0        
			BEGIN  
				IF EXISTS (SELECT 
								* 
							FROM 
								TaxDetails 
							WHERE 
								numCountryID=@numCountry 
								AND numStateID=@numState
								AND numDomainID= @numDomainID 
								AND numTaxItemID=@numTaxItemID
								AND 1=(Case 
										when @tintBaseTaxOnArea=0 then 1 --State
										when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
										when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
										else 0 end))            
				BEGIN
					IF @numTaxItemID = 1
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT
							decTaxPercentage
							,tintTaxType 
							,numTaxID
						FROM 
							TaxDetails 
						WHERE 
							numCountryID=@numCountry 
							AND numStateID=@numState
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID
							AND 1=(Case 
									when @tintBaseTaxOnArea=0 then 1 --State
									when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
									when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
									else 0 end)
					END
					ELSE
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT TOP 1 
							decTaxPercentage
							,tintTaxType
							,numTaxID
						FROM 
							TaxDetails 
						WHERE 
							numCountryID=@numCountry 
							AND numStateID=@numState
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID
							AND 1=(Case 
									when @tintBaseTaxOnArea=0 then 1 --State
									when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
									when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
									else 0 end)
					END        
				END
				ELSE IF EXISTS(SELECT * FROM TaxDetails where numCountryID=@numCountry and numStateID=@numState AND numDomainID= @numDomainID and numTaxItemID=@numTaxItemID AND isnull(vcCity,'')='' and isnull(vcZipPostal,'')='')            
				BEGIN
					IF @numTaxItemID = 1
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT 
							decTaxPercentage
							,tintTaxType
							,numTaxID 
						FROM 
							TaxDetails 
						WHERE
							numCountryID=@numCountry 
							AND numStateID=@numState
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID 
							AND ISNULL(vcCity,'')='' 
							AND ISNULL(vcZipPostal,'')=''      
					END
					ELSE
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT TOP 1 
							decTaxPercentage
							,tintTaxType
							,numTaxID 
						FROM 
							TaxDetails 
						WHERE
							numCountryID=@numCountry 
							AND numStateID=@numState
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID 
							AND ISNULL(vcCity,'')='' 
							AND ISNULL(vcZipPostal,'')=''      
					END
				END
				ELSE         
				BEGIN    
					IF @numTaxItemID = 1
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT 
							decTaxPercentage
							,tintTaxType
							,numTaxID
						FROM 
							TaxDetails 
						WHERE 
							numCountryID=@numCountry 
							AND numStateID=0 
							AND ISNULL(vcCity,'')='' 
							AND ISNULL(vcZipPostal,'')='' 
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID
					END
					ELSE
					BEGIN
						INSERT INTO @TEMP 
						(
							decTaxValue,
							tintTaxType,
							numTaxID
						)
						SELECT TOP 1 
							decTaxPercentage
							,tintTaxType
							,numTaxID 
						FROM 
							TaxDetails 
						WHERE 
							numCountryID=@numCountry 
							AND numStateID=0 
							AND ISNULL(vcCity,'')='' 
							AND ISNULL(vcZipPostal,'')='' 
							AND numDomainID= @numDomainID 
							AND numTaxItemID=@numTaxItemID                   
					END
				END
			END                  
		END
		ELSE IF @numCountry > 0
		BEGIN
			IF @numTaxItemID = 1
			BEGIN
				INSERT INTO @TEMP 
				(
					decTaxValue,
					tintTaxType,
					numTaxID
				)
				SELECT 
					decTaxPercentage
					,tintTaxType 
					,numTaxID
				FROM 
					TaxDetails 
				WHERE 
					numCountryID=@numCountry 
					AND numStateID=0 
					AND ISNULL(vcCity,'')='' 
					AND ISNULL(vcZipPostal,'')='' 
					AND numDomainID= @numDomainID 
					AND numTaxItemID=@numTaxItemID
			END
			ELSE
			BEGIN 
				INSERT INTO @TEMP 
				(
					decTaxValue,
					tintTaxType,
					numTaxID
				)
				SELECT TOP 1 
					decTaxPercentage
					,tintTaxType 
					,numTaxID
				FROM 
					TaxDetails 
				WHERE 
					numCountryID=@numCountry 
					AND numStateID=0 
					AND ISNULL(vcCity,'')='' 
					AND ISNULL(vcZipPostal,'')='' 
					AND numDomainID= @numDomainID 
					AND numTaxItemID=@numTaxItemID
			END
		END
	END

	RETURN
END

