GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GenerateSEOFriendlyURL')
DROP FUNCTION fn_GenerateSEOFriendlyURL
GO

CREATE Function [dbo].[fn_GenerateSEOFriendlyURL]
(
@CategoryName varchar(1000)
) Returns Varchar(50) 
As
Begin
DECLARE @CName as varchar(1000)
SET @CName =''
if @CategoryName <> ''
BEGIN

SET @CName = LOWER(@CategoryName)
SET @CName = LTRIM(@CName) 
SET @CName = RTRIM(@CName) 
SET @CName = dbo.fn_RegexReplace('/&/w+;','',@CName,1,1) 
SET @CName = dbo.fn_RegexReplace('[^a-z0-9\-\s()]','',@CName,1,1) 
SET @CName = dbo.fn_RegexReplace('[\s-]+','-',@CName,1,1) 
SET @CName = REPLACE(@CName,' ','-') 
SET @CName = dbo.fn_RegexReplace('-{2,}','-',@CName,1,1) 
SET @CName = RIGHT(@CName, len(@CName)+1 - patindex('%[^-]%', @CName))
SET @CName = LEFT(@CName, len(@CName)+1 - patindex('%[^-]%', @CName))
SET @CName = REPLACE(@CName,'-',' ') 

IF(LEN(@CName) > 80)
BEGIN
SET @CName = SUBSTRING(@CName,0,80) 
END

END

Return (@CName)
End
