/****** Object:  UserDefinedFunction [dbo].[fn_GetJournalCheq]    Script Date: 10/05/2009 14:29:11 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getjournalcheq')
DROP FUNCTION fn_getjournalcheq
GO
CREATE FUNCTION [dbo].[fn_GetJournalCheq]
(@numCheckId numeric(8),
 @numDomainId int)
Returns varchar(2000)
AS 
begin


declare @Amount varchar(50);
declare @numCheckNo varchar(50);
declare @vcMemo varchar(2000);
declare @vcDescription varchar(50);


declare BizDoc_Cursor CURSOR FOR
	
SELECT cast(SUM(monCheckAmt) as varchar(50)) as Amount,
	isnull( CAST(numCheckNo AS VARCHAR(50)),'No-Cheq') as numCheckNo,
	isnull(varMemo,'No-Memo' ) as vcMemo FROM CHECKDETAILS
	WHERE numCheckId=@numCheckId AND 
	numDomainId=@numDomainId
	GROUP BY numCheckNo,varMemo

set @vcDescription=''

OPEN BizDoc_Cursor

FETCH NEXT FROM BizDoc_Cursor into @Amount,@numCheckNo,@vcMemo
WHILE @@FETCH_STATUS = 0
   BEGIN
		set @vcDescription= @vcDescription + ',' +
		' Memo: ' + @vcMemo + 
		' Chek No: ' + @numCheckNo + 
		' Amount: ' + @Amount

      FETCH NEXT FROM BizDoc_Cursor into @Amount,@numCheckNo,@vcMemo		
   END;

CLOSE BizDoc_Cursor;
DEALLOCATE BizDoc_Cursor;

RETURN substring(@vcDescription,2,len(@vcDescription))

end