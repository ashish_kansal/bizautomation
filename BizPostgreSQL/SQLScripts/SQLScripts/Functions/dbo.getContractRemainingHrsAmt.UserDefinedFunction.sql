/****** Object:  UserDefinedFunction [dbo].[getContractRemainingHrsAmt]    Script Date: 07/26/2008 18:12:58 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcontractremaininghrsamt')
DROP FUNCTION getcontractremaininghrsamt
GO
CREATE FUNCTION [dbo].[getContractRemainingHrsAmt](@mode as bit,@numContractId as numeric)                  
RETURNS DECIMAL(10,2)                  
BEGIN                  
 ------@mode = 1 for Amount ,0 for Hours                  
	DECLARE @Hours decimal(10,2) = 0                 
	DECLARE @Amount  decimal(10,2) = 0                 
	DECLARE @ContractHours  decimal(10,2) = 0                
	DECLARE @ContractAmount  decimal(10,2)  = 0        
             
        
	SELECT 
		@ContractHours=isnull(numHours,0),
		@ContractAmount=isnull(numAmount,0)         
	FROM 
		contractmanagement 
	WHERE 
		numcontractid = @numContractId          

	SELECT 
		@ContractAmount=@ContractAmount+isnull(sum(monAmount),0) 
	FROM 
		OpportunityBizDocsDetails  
	WHERE 
		numContractId =  @numContractId 
        
	IF @mode = 0             
	BEGIN            
		SELECT                   
			@Hours= isnull(sum(convert(decimal(10,2),datediff(minute,dtFromDate,dtToDate))/60 ),0)                               
		FROM 
			timeandexpense 
		WHERE 
			numType =1 
			AND numCategory =1 
			AND numContractid=@numContractId      
       
		IF @ContractHours > @Hours                   
		BEGIN              
			SET @Hours= @ContractHours - @Hours            
		END                  
		ELSE                  
		BEGIN                  
			SET @Hours= 0                  
		END                 
	END         
           
	IF @mode = 1                
	BEGIN                
		SELECT 
			@Amount= ISNULL(SUM(ISNULL(monAmount,0)),0)
		FROM 
			timeandexpense 
		WHERE 
			numType = 1 
			AND numCategory = 2
			AND numContractid=@numContractId                 
  
		SELECT 
			@Amount = @Amount + ISNULL(sum(monDealAmount),0) 
		FROM 
			(
				SELECT 
					CASE 
					WHEN PO.numOppBizDocID >0 
						THEN dbo.[GetDealAmount](PO.numoppID,GETDATE(),PO.numOppBizDocID) 
					WHEN PO.numBillID >0 
						THEN ISNULL((SELECT ISNULL(monAmount,0) FROM [OpportunityBizDocsDetails] WHERE numBizDocsPaymentDetId = PO.numBillID),0)
					ELSE 0
					END AS monDealAmount 
				FROM 
					ProjectsOpportunities PO 
				WHERE 
					numProId in (select numProId from ProjectsMaster where numContractId=@numContractId)) AS X
        
		IF @ContractAmount > @Amount                   
		BEGIN               
			SET @Amount = @ContractAmount - @Amount   
		END                     
		ELSE            
		BEGIN            
			SET @Amount= 0            
		END                            
	END          
                        
	DECLARE @ret as decimal(10,2)                  
              
	IF @mode = 1                  
		SET @ret= @Amount                  
	ELSE                   
		SET @ret= @Hours                  
	
	RETURN @ret                  
END
GO
