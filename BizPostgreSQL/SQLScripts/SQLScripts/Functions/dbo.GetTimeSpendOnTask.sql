GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTimeSpendOnTask')
DROP FUNCTION GetTimeSpendOnTask
GO
CREATE FUNCTION [dbo].[GetTimeSpendOnTask]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintReturnType TINYINT
)    
RETURNS VARCHAR(100)    
AS
BEGIN
	DECLARE @vcTotalTimeSpendOnTask VARCHAR(50) = ''
	DECLARE @numTotalMinutes INT = 0

	DECLARE @TEMTIMELong TABLE
	(
		ID INT IDENTITY(1,1)
		,tintAction TINYINT
		,dtActionTime DATETIME
	)

	INSERT INTO @TEMTIMELong
	(
		tintAction
		,dtActionTime
	)
	SELECT
		tintAction
		,dtActionTime
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime
		,tintAction

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @tintLastAction TINYINT
	DECLARE @dtLastActionTime DATETIME

	DECLARE @tintAction TINYINT
	DECLARE @dtActionTime DATETIME
	SELECT @iCount = COUNT(*) FROM @TEMTIMELong

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@tintAction=tintAction
			,@dtActionTime=dtActionTime
		FROM
			@TEMTIMELong
		WHERE
			ID = @i

		IF @i = 1 
		BEGIN
			IF @tintAction <> 1
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END
		ELSE
		BEGIN
			IF @tintLastAction = 1 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			--ELSE IF @tintLastAction = 2 AND @tintAction <> 3
			ELSE IF @tintLastAction = 2 AND @tintAction NOT IN(3,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 3 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE IF @tintLastAction = 4
			BEGIN
				SET @vcTotalTimeSpendOnTask = 'Invalid time sequence'
				BREAK;
			END
			ELSE
			BEGIN
				IF @tintAction IN (2,4)
				BEGIN
					SET @numTotalMinutes = @numTotalMinutes + DATEDIFF(MINUTE,@dtLastActionTime,@dtActionTime)
				END

				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END

		SET @i = @i + 1
	END

	IF @vcTotalTimeSpendOnTask <> 'Invalid time sequence'
	BEGIN
		IF ISNULL(@tintReturnType,0) = 1
		BEGIN
			RETURN CONCAT('<ul class="list-inline"><li style="padding-right:0px">',(CASE WHEN (@numTotalMinutes / 60) > 0 THEN CONCAT('<label style="color:#3366ff">',(@numTotalMinutes / 60),(CASE WHEN (@numTotalMinutes / 60) > 1 THEN ' Hours' ELSE ' Hour' END),'</label>') ELSE '' END),'</li><li>',(CASE WHEN (@numTotalMinutes % 60) > 0 THEN CONCAT('<label>',@numTotalMinutes % 60,(CASE WHEN (@numTotalMinutes % 60) > 1 THEN ' Minutes' ELSE ' Minute' END),'</label>') ELSE '' END),'</li></ul>')
		END
		ELSE IF ISNULL(@tintReturnType,0) = 2
		BEGIN
			RETURN @numTotalMinutes
		END
		ELSE
		BEGIN
			RETURN FORMAT(@numTotalMinutes / 60,'00') + ':' + FORMAT(@numTotalMinutes % 60,'00')
		END
	END
	ELSE IF ISNULL(@tintReturnType,0) = 2
	BEGIN
		SET @vcTotalTimeSpendOnTask = '0'
	END
	
	RETURN @vcTotalTimeSpendOnTask
END
GO