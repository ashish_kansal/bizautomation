/****** Object:  UserDefinedFunction [dbo].[ufn_GetDaysInYear]    Script Date: 07/26/2008 18:13:15 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='ufn_getdaysinyear')
DROP FUNCTION ufn_getdaysinyear
GO
CREATE FUNCTION [dbo].[ufn_GetDaysInYear] ( @pDate    DATETIME )
RETURNS INT
AS
BEGIN

    DECLARE @IsLeapYear        BIT

    SET @IsLeapYear = 0
    IF (YEAR( @pDate ) % 4 = 0 AND YEAR( @pDate ) % 100 != 0) OR
        YEAR( @pDate ) % 400 = 0
        SET @IsLeapYear = 1

    RETURN 365 + @IsLeapYear

END
GO
