GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_SurveyMatrixAns')
DROP FUNCTION fn_SurveyMatrixAns
GO

CREATE FUNCTION [fn_SurveyMatrixAns]
    (
      @numSurId AS NUMERIC,
      @numQuestionID AS NUMERIC,
      @numRespondantID AS NUMERIC,
      @numAnsID AS numeric
    )
RETURNS VARCHAR(MAX)
BEGIN

    DECLARE @listStr VARCHAR(MAX)
 
    SELECT  
		@listStr = COALESCE(@listStr + ',', '') + sam.vcAnsLabel
    FROM 
		SurveyMatrixAnsMaster sam
	INNER JOIN
        SurveyResponse sr
	ON
		sr.numSurId = sam.numSurId
    WHERE   
        sr.numParentSurId = sam.numSurId
        AND sr.numSurId = @numSurId
        AND sr.numQuestionID = sam.numQID
        AND sr.numMatrixID = sam.numMatrixID
        AND sr.numRespondantID = @numRespondantID
        AND sr.numQuestionID = @numQuestionID
        AND sam.numQID = @numQuestionID
        AND sr.numAnsID =@numAnsID          
                       
    RETURN @listStr
END