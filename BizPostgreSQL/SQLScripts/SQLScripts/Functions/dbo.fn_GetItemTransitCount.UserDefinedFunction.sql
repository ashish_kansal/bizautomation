GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemTransitCount')
DROP FUNCTION fn_GetItemTransitCount
GO
CREATE FUNCTION [dbo].[fn_GetItemTransitCount]
(
	@numItemcode NUMERIC,
	@numDomainID NUMERIC
)
RETURNS INT 
AS
BEGIN

 DECLARE @ItemTransitCount INT
 
 select @ItemTransitCount=COUNT(*) 
     FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId 
		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
				 and Opp.numDomainId=@numDomainID AND OppI.numItemcode=@numItemcode
				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)

RETURN @ItemTransitCount

END
GO
