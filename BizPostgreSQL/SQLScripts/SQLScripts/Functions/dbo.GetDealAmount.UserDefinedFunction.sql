GO
--select dbo.GetDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getdealamount')
DROP FUNCTION getdealamount
GO
CREATE FUNCTION [dbo].[GetDealAmount]
(@numOppID numeric,@dt Datetime,@numOppBizDocsId numeric=0)    
returns DECIMAL(20,5)    
as    
begin   
  
  
declare @DivisionID as numeric(9)                         
                                  
declare @TaxPercentage as float                    
  
declare @shippingAmt as DECIMAL(20,5)    
declare @TotalTaxAmt as DECIMAL(20,5)     
declare @decDisc as float 
declare @bitDiscType as bit ;set @bitDiscType=0     
declare @decInterest as float    
    
set @decInterest=0    
set @decDisc=0    
declare @bitBillingTerms as bit     
declare @intBillingDays as integer    
declare @bitInterestType as bit    
declare @fltInterest as float    
declare @dtFromDate as varchar(20)    
declare @strDate as datetime    
declare @TotalAmt as DECIMAL(20,5)    
declare @TotalAmtAfterDisc as DECIMAL(20,5)    
declare @fltDiscount as float  
declare @tintOppStatus as tinyint  
declare @tintshipped as tinyint 
declare @dtshipped as datetime 
declare @numBillCountry as numeric(9)
declare @numBillState as numeric(9)
declare @numDomainID as numeric(9)
declare @tintOppType tinyint
declare @bitTaxApplicable BIT
set @TotalAmt=0


--declare @monCreditAmount as DECIMAL(20,5);set @monCreditAmount=0    
DECLARE @tintTaxOperator AS TINYINT;SET @tintTaxOperator=0

select    
@DivisionID=numDivisionID,@shippingAmt=0,    
@bitBillingTerms=0,    
@intBillingDays=0,@bitInterestType=0,    
@fltInterest=0,@fltDiscount=0,
@tintOppStatus=tintOppStatus ,@tintshipped=tintshipped,@numDomainID=numDomainID,@tintOppType=tintOpptype,
@tintTaxOperator=ISNULL([tintTaxOperator],0)
from OpportunityMaster where numOppId=@numOppId          
   

IF @numOppBizDocsId=0
BEGIN
	select @TotalAmt=isnull(sum(X.amount),0)    
			from (SELECT monTotAmount as Amount from OpportunityItems opp                   
					join item i on opp.numItemCode=i.numItemCode where numOppId=@numOppId  
			 union ALL --timeandexpense (expense)  
			  SELECT monAmount as amount from timeandexpense   
			   where (numOppId=@numOppId or numOppBizDocsId in (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId ) )  
			   and numCategory = 2 and numtype=1  
			)x  
END
ELSE
BEGIN
SELECT @bitBillingTerms=isnull(bitBillingTerms,0),    
	@intBillingDays=isnull(intBillingDays,0),@bitInterestType=isnull(bitInterestType,0),    
	@fltInterest=isnull(fltInterest,0),@fltDiscount=isnull(fltDiscount,0),
	@bitDiscType=isnull(bitDiscountType,0)
FROM dbo.OpportunityMaster WHERE numOppId=@numOppID

	select    
	@shippingAmt=isnull(monShipCost,0),    
	@dtshipped=dtShippedDate,
--	@monCreditAmount=isnull(monCreditAmount,0),
	@dtFromDate=dtFromDate
	from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId

select @TotalAmt=isnull(sum(X.amount),0)
from (SELECT OBD.monTotAmount as Amount from OpportunityItems opp                   
		join OpportunityBizDocItems OBD on OBD.numOppItemID=Opp.numoppitemtCode    
		where numOppId=@numOppId  and OBD.numOppBizDocID=@numOppBizDocsId
 
	union ALL  --timeandexpense (expense)  
	 select monAmount as amount from timeandexpense  where (numOppId=@numOppId or numOppBizDocsId =@numOppBizDocsId )  
		and numCategory = 2 and numtype=1  
)x  

END

	SET @TotalTaxAmt=isnull(dbo.[GetDealTaxAmount](@numOppId,@numOppBizDocsId,@numDOmainId),0)


set @TotalAmtAfterDisc= @TotalAmt 
if @tintOppStatus=1 
begin  
   
    if @tintshipped=1 set @dt=isnull(@dtshipped,@dt)
	if  @bitBillingTerms=1    
	begin    
		--set @strDate = DateAdd(Day, convert(int,ISNULL(dbo.fn_GetListItemName(isnull(@intBillingDays,0)),0)),@dtFromDate)    
		set @strDate = DateAdd(Day, convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(@intBillingDays,0)),0)),@dtFromDate)    
		--(SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	     
		if @bitInterestType=0     
		begin    
			if @strDate>@dt    
			begin    
				set @decDisc=0    
			end    
			else    
			begin    
				set @decDisc=@fltInterest    
	       
			end    
		end    
		ELSE    
		begin    
			if @strDate>@dt    
			begin    
				set @decInterest=0    
			end    
			else    
			begin    
				set @decInterest=@fltInterest    
			end    
		end    
	end    
	 
	if @decDisc>0  
	begin      
          if @bitDiscType=0 set @TotalAmtAfterDisc=@TotalAmt-((@TotalAmt*@fltDiscount/100)+((@TotalAmt*(100-@fltDiscount)/100)*@decDisc/100))+@shippingAmt+@TotalTaxAmt  
          else if @bitDiscType=1  set @TotalAmtAfterDisc=@TotalAmt-(@fltDiscount+(@TotalAmt-@fltDiscount)*@decDisc/100)+@shippingAmt+@TotalTaxAmt  
	end    
	else  
	begin 
		if @bitDiscType=0 	set @TotalAmtAfterDisc=@TotalAmt+(@decInterest*(@TotalAmt*(100-@fltDiscount)/100)/100)-(@TotalAmt*@fltDiscount/100)+@shippingAmt+@TotalTaxAmt    
		else if @bitDiscType=1 	set @TotalAmtAfterDisc=@TotalAmt+(@decInterest*(@TotalAmt-@fltDiscount)/100)-(@fltDiscount)+@shippingAmt+@TotalTaxAmt    
	 end  
end 
else
begin
	if @bitDiscType=0 	set @TotalAmtAfterDisc=@TotalAmt+@shippingAmt+@TotalTaxAmt -(@TotalAmt*@fltDiscount/100)
	else if @bitDiscType=1 	set @TotalAmtAfterDisc=@TotalAmt+@shippingAmt+@TotalTaxAmt -@fltDiscount
end

 --SET @TotalAmtAfterDisc = @TotalAmtAfterDisc - isnull(@monCreditAmount,0)
SET @TotalAmtAfterDisc = ROUND(@TotalAmtAfterDisc,2)

return CAST(@TotalAmtAfterDisc AS DECIMAL(20,5)) -- Set Accuracy of Two precision
END
GO