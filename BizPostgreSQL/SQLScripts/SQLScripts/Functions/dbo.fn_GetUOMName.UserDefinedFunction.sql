GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetUOMName')
DROP FUNCTION fn_GetUOMName
GO
CREATE FUNCTION [dbo].[fn_GetUOMName](@numUOMId numeric)
Returns varchar(100) 
As
BEGIN
	declare @vcUnitName as varchar(100)

	Select @vcUnitName=isnull(vcUnitName,'') from UOM where numUOMId=@numUOMId

return isnull(@vcUnitName,'-')
END
GO
