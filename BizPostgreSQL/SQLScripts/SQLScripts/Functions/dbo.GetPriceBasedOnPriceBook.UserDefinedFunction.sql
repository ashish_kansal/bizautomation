-- SELECT dbo.GetPriceBasedOnPriceBook(366,100,1)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getpricebasedonpricebook')
DROP FUNCTION getpricebasedonpricebook
GO
CREATE FUNCTION GetPriceBasedOnPriceBook
--Below function applies Price Rule on given Price an Qty and returns calculated value
(
    @numRuleID NUMERIC,
    @monPrice DECIMAL(20,5),
    @units FLOAT,
    @ItemCode numeric
)
RETURNS DECIMAL(20,5)
AS BEGIN
    DECLARE @PriceBookPrice DECIMAL(20,5) = 0
    DECLARE @tintRuleType TINYINT
    DECLARE @tintPricingMethod TINYINT
    DECLARE @tintDiscountType TINYINT
    DECLARE @decDiscount FLOAT
    DECLARE @intQntyItems INT
    DECLARE @decMaxDedPerAmt FLOAT

    SELECT  
		@tintRuleType = PB.[tintRuleType],
		@tintPricingMethod = PB.[tintPricingMethod],
		@tintDiscountType = PB.[tintDiscountType],
		@decDiscount = PB.[decDiscount],
		@intQntyItems = PB.[intQntyItems],
		@decMaxDedPerAmt = PB.[decMaxDedPerAmt]
	FROM    
		[PriceBookRules] PB
	LEFT OUTER JOIN 
		[PriceBookRuleDTL] PD 
	ON 
		PB.[numPricRuleID] = PD.[numRuleID]
	WHERE 
		[numPricRuleID] = @numRuleID
	

    
    IF ( @tintPricingMethod = 1 ) -- Price table
    BEGIN
		SET @intQntyItems = 1;
			   
		SELECT TOP 1  
			@tintRuleType = [tintRuleType],
			@tintDiscountType = [tintDiscountType],
			@decDiscount = [decDiscount]
		FROM 
			[PricingTable]
		WHERE 
			[numPriceRuleID] = @numRuleID
			AND ISNULL(numCurrencyID,0) = 0
			AND @units BETWEEN [intFromQty] AND [intToQty]

		IF (@@ROWCOUNT=0)						
			RETURN @monPrice;


		IF @tintRuleType = 2
		BEGIN
			SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)
		END
	
		-- SELECT dbo.GetPriceBasedOnPriceBook(366,500,20)
		IF ( @tintDiscountType = 1 ) -- Percentage 
        BEGIN
			SET @decDiscount = @monPrice * ( @decDiscount /100);

			IF ( @tintRuleType = 1 )
					SET @PriceBookPrice =  (@monPrice - @decDiscount)  ;
			IF ( @tintRuleType = 2 )
					SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
        END
               
		IF ( @tintDiscountType = 2 ) -- Flat discount 
		BEGIN
			IF ( @tintRuleType = 1 )
					SET @PriceBookPrice =  (@monPrice -   @decDiscount) ;
			IF ( @tintRuleType = 2 )
					SET @PriceBookPrice =  (@monPrice +  @decDiscount)  ;
		END

		IF ( @tintDiscountType = 2 ) -- Named Price
		BEGIN
			SET @PriceBookPrice =  (@monPrice -   @decDiscount) ;
		END
           
    END
        
    IF (@tintPricingMethod = 2) -- Pricing Formula 
    BEGIN

		IF @tintRuleType = 2
		BEGIN
			SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)
		END
		  
        IF ( @tintDiscountType = 1 ) -- Percentage 
        BEGIN
			SET @decDiscount = @decDiscount * (@units/@intQntyItems);
			SET @decDiscount = @decDiscount * @monPrice /100;
			SET @decMaxDedPerAmt = @decMaxDedPerAmt * @monPrice /100;
					
					
			IF (@decDiscount > @decMaxDedPerAmt)
				SET @decDiscount = @decMaxDedPerAmt;
					
					
			IF ( @tintRuleType = 1 )
					SET @PriceBookPrice = @monPrice - @decDiscount;
			IF ( @tintRuleType = 2 )
					SET @PriceBookPrice = @monPrice + @decDiscount;
        END
        IF ( @tintDiscountType = 2 ) -- Flat discount 
        BEGIN
			SET @decDiscount = @decDiscount * (@units/@intQntyItems);

			IF (@decDiscount > @decMaxDedPerAmt)
				SET @decDiscount = @decMaxDedPerAmt;
					
			IF ( @tintRuleType = 1 )
				SET @PriceBookPrice =  (@monPrice -   @decDiscount)  ;
			IF ( @tintRuleType = 2 )
				SET @PriceBookPrice = (@monPrice +  @decDiscount)  ;
        END
			
    END


    RETURN @PriceBookPrice ;

   END