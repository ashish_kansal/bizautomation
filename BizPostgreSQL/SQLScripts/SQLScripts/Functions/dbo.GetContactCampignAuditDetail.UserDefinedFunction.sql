GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetContactCampignAuditDetail')
DROP FUNCTION GetContactCampignAuditDetail
GO
CREATE FUNCTION [dbo].[GetContactCampignAuditDetail] (@numECampaignID NUMERIC(18,0), @numContactID NUMERIC(18,0))
RETURNS VARCHAR(500)
AS
BEGIN
	DECLARE @vcCampaignAudit AS VARCHAR(500) = ''
    DECLARE @CampaignName AS VARCHAR(200)
	DECLARE @NextDrip AS VARCHAR(200)
	DECLARE @CompletedStages AS VARCHAR(200)
    DECLARE @LastDrip AS VARCHAR(200)

	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @ConECampaignID AS VARCHAR(200)
	DECLARE @bitCompleted AS BIT = 0

	IF (SELECT COUNT(*) FROM ECampaign WHERE numECampaignID = @numECampaignID) > 0
	BEGIN
		SELECT @vcCampaignAudit = vcECampName, @numDomainID=numDomainID FROM ECampaign WHERE numECampaignID = @numECampaignID
		SELECT TOP 1 @ConECampaignID=numConEmailCampID  FROM ConECampaign WHERE numECampaignID = @numECampaignID AND numContactID = @numContactID ORDER BY numConEmailCampID DESC

		SELECT TOP 1 @NextDrip= CONCAT(MONTH(dtExecutionDate),'-',DAY(dtExecutionDate)) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 0 ORDER BY numConECampDTLID ASC
		SELECT TOP 1 @LastDrip= CONCAT(MONTH(bintSentON),'-',DAY(bintSentON)) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 1 ORDER BY numConECampDTLID DESC
		SELECT @CompletedStages=COUNT(*) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 1

		IF (SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 0) = 0
		BEGIN
			SET @bitCompleted = 1
		END

		SET @vcCampaignAudit = @vcCampaignAudit + ' ' + ISNULL(@NextDrip,'') + ' / Audit (' + ISNULL(@CompletedStages,0) + ') ' +  ISNULL(@LastDrip,'') + ' ' + (CASE WHEN @bitCompleted = 1 THEN '#Completed#' ELSE '#Running#' END)
	END

	RETURN @vcCampaignAudit
END
GO