/****** Object:  UserDefinedFunction [dbo].[GetUTCDateWithoutTime]    Script Date: 03/06/2009 00:28:19 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getutcdatewithouttime')
DROP FUNCTION getutcdatewithouttime
GO
CREATE FUNCTION [dbo].[GetUTCDateWithoutTime]()
RETURNS DATETIME	 
AS
BEGIN
DECLARE @Dt DATETIME
SELECT @Dt  =DATEADD(DAY, 0, DATEDIFF(DAY,0,GETUTCDATE()));

  RETURN @Dt 
END
