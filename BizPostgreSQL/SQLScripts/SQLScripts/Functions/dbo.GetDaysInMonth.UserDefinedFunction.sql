/****** Object:  UserDefinedFunction [dbo].[GetDaysInMonth]    Script Date: 07/26/2008 18:13:00 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getdaysinmonth')
DROP FUNCTION getdaysinmonth
GO
CREATE FUNCTION [dbo].[GetDaysInMonth] ( @date    DATETIME )
RETURNS INT
AS
BEGIN

    RETURN CASE WHEN MONTH(@date) IN (1, 3, 5, 7, 8, 10, 12) THEN 31
                WHEN MONTH(@date) IN (4, 6, 9, 11) THEN 30
                ELSE CASE WHEN (YEAR(@date) % 4    = 0 AND
                                YEAR(@date) % 100 != 0) OR
                               (YEAR(@date) % 400  = 0)
                          THEN 29
                          ELSE 28
                     END
           END

END
GO
