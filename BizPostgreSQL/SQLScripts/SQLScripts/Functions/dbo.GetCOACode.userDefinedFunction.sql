
GO
/****** Object:  UserDefinedFunction [dbo].[GetCOACode]    Script Date: 09/25/2009 16:09:01 ******/

GO

GO
-- select dbo.[GetCOACode](1,2) 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCOACode')
DROP FUNCTION GetCOACode
GO
CREATE FUNCTION	[dbo].[GetCOACode]
(@numDomainID NUMERIC(9),@numParentID NUMERIC(9))
returns varchar(50)
AS 
BEGIN
	DECLARE @numAccountTypeID NUMERIC(9)
	DECLARE @vcNewAccountCode VARCHAR(50)
	DECLARE @vcParentAccountCode VARCHAR(50)
	
	SELECT @numAccountTypeID =[numAccountTypeID],@vcParentAccountCode=[vcAccountCode] FROM [AccountTypeDetail]
	WHERE [numDomainID]=@numDomainID AND numAccountTypeID =@numParentID

RETURN ISNULL(@vcNewAccountCode,'')
END