GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetEmailStringAsJson')
DROP FUNCTION fn_GetEmailStringAsJson
GO
CREATE FUNCTION [dbo].[fn_GetEmailStringAsJson]
(
	@vcEmailId VARCHAR(4000)
) 
RETURNS VARCHAR(4000)
As
BEGIN
	DECLARE @vcNewEmailId AS VARCHAR(2000) = '['

	DECLARE @TEMPEmails TABLE
	(
		ID INT IDENTITY(1,1)
		,vcContactEmail VARCHAR(1000)
	)

	DECLARE @TEMPEMailPart TABLE
	(
		ID INT
		,vcPartValue VARCHAR(300)
	)

	INSERT INTO @TEMPEmails (vcContactEmail) SELECT Items FROM dbo.SplitByString(@vcEmailId,'#^#')

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @vcContactEmail VARCHAR(1000)
	DECLARE @vcEmailJson VARCHAR(1000)

	SELECT @iCount=COUNT(*) FROM @TEMPEmails

	WHILE @i <= @iCount
	BEGIN
		DELETE FROM @TEMPEMailPart
		SELECT @vcContactEmail = vcContactEmail FROM @TEMPEmails WHERE ID = @i

		INSERT INTO @TEMPEMailPart (ID,vcPartValue) SELECT ROW_NUMBER() OVER(ORDER BY (SELECT NULL)),Items FROM dbo.SplitByString(@vcContactEmail,'$^$')

		 SET @vcEmailJson = CONCAT('{',
										'"id":"',(CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMPEMailPart),0) > 0 THEN ISNULL((SELECT EM.numContactId FROM @TEMPEMailPart TEP INNER JOIN EmailMaster EM ON CAST(TEP.vcPartValue AS NUMERIC)=Em.numEmailId WHERE TEP.ID=1),0) ELSE '0' END), '",',
										'"first_name":"',(CASE 
															WHEN ISNULL((SELECT COUNT(*) FROM @TEMPEMailPart),0) > 0
															 THEN (CASE 
																		WHEN LTRIM(RTRIM(ISNULL((SELECT EM.vcName FROM @TEMPEMailPart TEP INNER JOIN EmailMaster EM ON CAST(TEP.vcPartValue AS NUMERIC)=Em.numEmailId WHERE TEP.ID=1),''))) = '' 
																		THEN ISNULL((SELECT vcPartValue FROM @TEMPEMailPart WHERE ID=2),'-')
																		ELSE ISNULL((SELECT EM.vcName FROM @TEMPEMailPart TEP INNER JOIN EmailMaster EM ON CAST(TEP.vcPartValue AS NUMERIC)=Em.numEmailId WHERE TEP.ID=1),'-')
																	END) 
															 ELSE '-' 
														END),'",',
										'"last_name":"","email":"' ,(CASE WHEN ISNULL((SELECT COUNT(*) FROM @TEMPEMailPart),0) > 2 THEN ISNULL((SELECT vcPartValue FROM @TEMPEMailPart WHERE ID=3),'-') ELSE '0' END),'","url":"#"}')

		SET @vcNewEmailId = CONCAT(@vcNewEmailId,(CASE WHEN @vcNewEmailId = '[' THEN '' ELSE ',' END),@vcEmailJson)

		SET @i = @i + 1
	END

	SET @vcNewEmailId = CONCAT(@vcNewEmailId,']')

	RETURN @vcNewEmailId
END