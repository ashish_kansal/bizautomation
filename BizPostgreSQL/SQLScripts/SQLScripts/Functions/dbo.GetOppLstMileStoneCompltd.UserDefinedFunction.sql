/****** Object:  UserDefinedFunction [dbo].[GetOppLstMileStoneCompltd]    Script Date: 07/26/2008 18:13:05 ******/

GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getopplstmilestonecompltd')
DROP FUNCTION getopplstmilestonecompltd
GO
CREATE FUNCTION [dbo].[GetOppLstMileStoneCompltd](@numoppid numeric(9))
	RETURNS varchar (100) 
AS
BEGIN
declare @numPercentage as varchar(100)
set @numPercentage=(select top 1 numStagePercentage  from OpportunityStageDetails where numoppId=@numoppid 
and numStagePercentage not  in (select numStagePercentage from OpportunityStageDetails where numoppId=@numoppid  and bitstagecompleted=0)and  numStagePercentage!=100 and bitstagecompleted=1 
order by numStagePercentage desc)
if @numPercentage !=''
begin
set @numPercentage= 'Milestone -' + @numPercentage + ' %'
end



return isnull(@numPercentage,'-')
END
GO
