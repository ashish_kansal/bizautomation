/****** Object:  UserDefinedFunction [dbo].[fn_GetModuleName]    Script Date: 07/28/2009 23:06:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		
-- Create date: 
-- Description:	
-- =============================================
Create FUNCTION [dbo].[fn_GetModuleName] 
(
	-- Add the parameters for the function here
	@numModuleId bigint
)
RETURNS varchar(100)
AS
BEGIN
	-- Declare the return variable here
	DECLARE  @ModuleName  varchar (100)

	-- Add the T-SQL statements to compute the return value here
	set @ModuleName=''
	SELECT  @ModuleName=vcModuleName from modulemaster where numModuleId=@numModuleId

	-- Return the result of the function
	RETURN @ModuleName

END
