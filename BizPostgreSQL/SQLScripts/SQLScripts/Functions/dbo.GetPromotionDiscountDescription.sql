GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPromotionDiscountDescription')
DROP FUNCTION GetPromotionDiscountDescription
GO
CREATE FUNCTION GetPromotionDiscountDescription
(
	@numPromotionID NUMERIC(18,0)
	,@vcCurrency VARCHAR(10)
)
RETURNS VARCHAR(1000) 
AS
BEGIN
	DECLARE @vcValue AS VARCHAR(1000) = ''

	SELECT
		@vcValue = (CASE 
						WHEN tintDiscountType = 1 
						THEN
							CONCAT('Get ',ISNULL(fltDiscountValue,0),'% off any of these item�s ',(CASE WHEN tintItemCalDiscount = 1 THEN 'sale price' ELSE 'list price' END))
						WHEN tintDiscountType = 2 
						THEN
							CONCAT('Get ',@vcCurrency,ISNULL(fltDiscountValue,0),' off any of these item�s ',(CASE WHEN tintItemCalDiscount = 1 THEN 'sale price' ELSE 'list price' END))
						WHEN tintDiscountType = 3 
						THEN
							CONCAT('Get any ',ISNULL(fltDiscountValue,0),' of these items for FREE !')
					END)
	FROM
		PromotionOffer
	WHERE
		numProId=@numPromotionID

	RETURN ISNULL(@vcValue,'')
END


										
									