/****** Object:  UserDefinedFunction [dbo].[GetCustFldItemsValue]    Script Date: 07/26/2008 18:12:58 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCustFldItemsValue')
DROP FUNCTION GetCustFldItemsValue
GO
CREATE FUNCTION [dbo].[GetCustFldItemsValue]
(
	@numFldId numeric(9)
	,@pageId as tinyint
	,@numRecordId as numeric(9)
	,@tintMode as TINYINT -- 0: Warehouse Level Attributes, 1: Item Level Attributes 
	,@fld_type as varchar(100)
)  
RETURNS VARCHAR(300)   
AS  
BEGIN  
	DECLARE @vcValue AS VARCHAR(100)  

	IF @pageId=9 AND ISNULL(@tintMode,0) = 0
	BEGIN
		SELECT 
			@vcValue= CASE 
						WHEN @fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
						WHEN @fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
						ELSE Fld_Value 
					 END 
		FROM 
			CFW_Fld_Values_Serialized_Items 
		WHERE 
			Fld_ID=@numFldId 
			AND RecId=@numRecordId  
	END 
	ELSE IF ISNULL(@tintMode,0) = 1
	BEGIN
		SELECT 
			@vcValue= CASE 
						WHEN @fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
						WHEN @fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
						ELSE CAST(Fld_Value AS VARCHAR)
					 END 
		FROM 
			ItemAttributes 
		WHERE 
			Fld_ID=@numFldId 
			AND numItemCode=@numRecordId
	END

	IF @vcValue IS NULL 
		SET @vcValue='0'  

	RETURN @vcValue  
END
GO
