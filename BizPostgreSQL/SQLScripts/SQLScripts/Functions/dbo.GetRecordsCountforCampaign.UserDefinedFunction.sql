
-- select dbo.[GetRecordsCountforCampaign](61,'2007-01-25 00:00:00:000','2010-02-06 00:00:00:000',1,0)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getrecordscountforcampaign')
DROP FUNCTION getrecordscountforcampaign
GO
CREATE FUNCTION [dbo].[GetRecordsCountforCampaign]
              (@numCampaignID NUMERIC(9),
               @dtStartDate DATETIME  = NULL,
               @dtEndDate   DATETIME  = NULL,
               @tintFlag    TINYINT,
               @tintCRMType TINYINT)
RETURNS DECIMAL(20,5)
AS
  BEGIN
    DECLARE  @RecordCount INTEGER;
    IF @tintFlag = 0 --Offline Campaign
      BEGIN
        SELECT @RecordCount = COUNT(* )
        FROM   [DivisionMaster] DM
        WHERE [numCampaignID] = @numCampaignID
               AND DM.[tintCRMType] = @tintCRMType
      
      END
    ELSE
      IF @tintFlag = 1 --Online Campaign
        BEGIN
          IF (@dtStartDate IS NOT NULL 
              AND @dtEndDate IS NOT NULL)
            BEGIN
              
              DECLARE @numDomainID NUMERIC(9)
              SELECT @numDomainID=[numDomainID] FROM   [CampaignMaster] CM WHERE  CM.[numCampaignID] = @numCampaignID
              
              SELECT @RecordCount = ISNULL(COUNT(DISTINCT X.numDivisionID),0) FROM 
              (	  SELECT 
						DM.[numDivisionID]
				  FROM  
						 [DivisionMaster] DM
					WHERE  
							 DM.[tintCRMType] = @tintCRMType
							 AND DM.numDomainID = @numDomainID
							 AND DM.numCampaignID=@numCampaignID
							 and DM.bintCreatedDate between  @dtStartDate And  @dtEndDate) X
            END
        END
    RETURN isnull(@RecordCount,0)
  END
