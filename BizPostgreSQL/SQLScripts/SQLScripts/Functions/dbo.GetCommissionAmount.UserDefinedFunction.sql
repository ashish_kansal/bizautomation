/****** Object:  UserDefinedFunction [dbo].[GetCommissionAmount]    Script Date: 07/26/2008 18:12:56 ******/

GO

GO
--Created By Siva                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCommissionAmount')
DROP FUNCTION GetCommissionAmount
GO
CREATE FUNCTION [dbo].[GetCommissionAmount](@numUserId numeric,@numUserCntID numeric,@numDomainId numeric)                                          
 RETURNS decimal(10,2)                                          
as                                          
Begin                   
 Declare @AssignedAmount as DECIMAL(20,5)                  
 Declare @OwnerAmount as DECIMAL(20,5)                  
 Declare @bitCommOwner as bit                  
 Declare @bitCommAssignee as bit                  
 Declare @decCommOwner as decimal(10,2)                  
 Declare @decCommAssignee as decimal(10,2)                  
 Declare @CommAssignedAmt as DECIMAL(20,5)                  
 Declare @CommOwnerAmt as DECIMAL(20,5)                  
 Declare @TotalCommAmt as DECIMAL(20,5)             
 Declare @bitRoleComm as bit            
 Declare @CommRoleAmt as DECIMAL(20,5)          
                  
                  
 Select @AssignedAmount=Sum(isnull(monPAmount,0)) From OpportunityMaster    
  Where tintOppType =1 And tintOppStatus=1 And numAssignedTo=@numUserCntID And numDomainId=@numDomainId                
 Select @OwnerAmount=Sum(isnull(monPAmount,0)) From OpportunityMaster     
 Where tintOppType =1 And tintOppStatus=1 And  numRecOwner=@numUserCntID And numDomainId=@numDomainId                       
     
 Select     
 @bitCommOwner=bitCommOwner,@bitCommAssignee=bitCommAssignee,    
 @decCommOwner=isnull(fltCommOwner,0),    
 @decCommAssignee=isnull(fltCommAssignee,0),@bitRoleComm=bitRoleComm     
 from UserMaster Where numUserId=@numUserId                  
    
    
    
 if @bitCommOwner=1 And @decCommOwner <>0                   
    Set @CommOwnerAmt=@OwnerAmount*@decCommOwner/100                  
                   
 if @bitCommAssignee=1 And @decCommAssignee <>0                   
    Set @CommAssignedAmt=@AssignedAmount*@decCommAssignee/100                  
 if @bitRoleComm=1           
     Begin          
       Select @CommRoleAmt=sum(isnull(monAmountPaid,0)* fltPercentage/100) from opportunitymaster Opp          
       join OpportunityContact OppCont on Opp.numOppId=OppCont.numOppId     
       left join OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId and oppBiz.bitAuthoritativeBizDocs = 1  
        join UserRoles UR on OppCont.numRole=UR.numRole          
		join usermaster Um on UR.numUserCntId = Um.numUserId and OppCont.numContactId=UM.numUserdetailID         
       Where tintOppType=1 And Opp.tintOppStatus=1 And Opp.numDomainId=@numDomainId And OppCont.numContactId=@numUserCntID          
      End                 
    Set @TotalCommAmt =isnull(@CommOwnerAmt,0)+isnull(@CommAssignedAmt,0)+isnull(@CommRoleAmt,0)            
    Return @TotalCommAmt                  
End
GO
