/****** Object:  UserDefinedFunction [dbo].[fn_RecurringTemplate]    Script Date: 07/26/2008 18:12:51 ******/

GO

GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_recurringtemplate')
DROP FUNCTION fn_recurringtemplate
GO
CREATE FUNCTION [dbo].[fn_RecurringTemplate](@tintFirstDet tinyint,@tintWeekDays tinyint,@DNextDate as datetime)  
Returns Datetime  
 As  
Begin  
DECLARE  @vcRetValue varchar(100)  
--To Get First Sunday of a Month ---  
If @tintFirstDet=1 And @tintWeekDays=1   
Set @vcRetValue = DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,1-datepart(day,@DNextDate),@DNextDate)), 6)   
  
--To Get Second Sunday of a Month ---  
If @tintFirstDet=2 And @tintWeekDays=1   
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,8-datepart(day,@DNextDate),@DNextDate)),6)   
  
--To Get Third Sunday of a Month--  
If @tintFirstDet=3 And @tintWeekDays=1   
Set @vcRetValue=DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,15-datepart(day,@DNextDate),@DNextDate)), 6)   
  
--To Get Fourth Sunday of a Month--  
If @tintFirstDet=4 And @tintWeekDays=1   
Set @vcRetValue=DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,22-datepart(day,@DNextDate),@DNextDate)), 6)  
  
--To Get Fifty Sunday of a Month  
If @tintFirstDet=5 And @tintWeekDays=1   
Set @vcRetValue=DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,29-datepart(day,@DNextDate),@DNextDate)), 6)    
  
  
  
----------------------------------------------------------------------------------------------  
  
  
--To Get First Monday of Month ---  
If @tintFirstDet=1 And @tintWeekDays=2  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,6-datepart(day,@DNextDate),@DNextDate)), 0)   
  
--Second  monday of month  
If @tintFirstDet=2 And @tintWeekDays=2   
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,13-datepart(day,@DNextDate),@DNextDate)), 0)   
  
  
--Third Monday of Month  
If @tintFirstDet=3 And @tintWeekDays=2   
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,20-datepart(day,@DNextDate),@DNextDate)), 0)   
  
--Fourth Monday of Month  
If @tintFirstDet=4 And @tintWeekDays=2   
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,27-datepart(day,@DNextDate),@DNextDate)), 0)  
  
  
--Fifth Month of month  
  
If @tintFirstDet=5 And @tintWeekDays=2   
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,34-datepart(day,@DNextDate),@DNextDate)), 0)    
  
------------------------------------------------------------------------------------------------  
  
  
  
--To Get First Tuesday of Month ---  
If @tintFirstDet=1 And @tintWeekDays=3  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,5-datepart(day,@DNextDate),@DNextDate)), 1)   
  
--Second  Tuesday of month  
If @tintFirstDet=2 And @tintWeekDays=3  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,12-datepart(day,@DNextDate),@DNextDate)), 1)   
  
  
--Third Tuesday of Month  
If @tintFirstDet=3 And @tintWeekDays=3  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,19-datepart(day,@DNextDate),@DNextDate)), 1)   
  
--Fourth Tuesday of Month  
If @tintFirstDet=4 And @tintWeekDays=3  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,26-datepart(day,@DNextDate),@DNextDate)), 1)  
  
  
--Fifth Tuesday of month  
If @tintFirstDet=5 And @tintWeekDays=3  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,33-datepart(day,@DNextDate),@DNextDate)), 1)    
  
  
---------------------------------------------------------------------------------------------------  
  
  
If @tintFirstDet=1 And @tintWeekDays=4  
--To Get First Wednesday of Month ---  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,5-datepart(day,@DNextDate),@DNextDate)), 2)   
  
  
--Second  Wednesday of month  
If @tintFirstDet=2 And @tintWeekDays=4  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,12-datepart(day,@DNextDate),@DNextDate)),2)   
  
  
--Third Wednesday of Month  
If @tintFirstDet=3 And @tintWeekDays=4  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,19-datepart(day,@DNextDate),@DNextDate)), 2)   
  
--Fourth Wednesday of Month  
If @tintFirstDet=4 And @tintWeekDays=4  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,26-datepart(day,@DNextDate),@DNextDate)), 2)  
  
  
--Fifth Wednesday of month  
  
If @tintFirstDet=5 And @tintWeekDays=4  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,33-datepart(day,@DNextDate),@DNextDate)), 2)    
  
----------------------------------------------------------------------------------------------------------  
  
  
  
  
--To Get First Thursday of Month ---  
  
If @tintFirstDet=1 And @tintWeekDays=5  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,4-datepart(day,@DNextDate),@DNextDate)), 3)   
  
--Second  Thursday of month  
  
If @tintFirstDet=2 And @tintWeekDays=5  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,11-datepart(day,@DNextDate),@DNextDate)),3)   
  
  
--Third Thursday of Month  
  
If @tintFirstDet=3 And @tintWeekDays=5  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,18-datepart(day,@DNextDate),@DNextDate)), 3)   
  
--Fourth Thursday of Month  
  
If @tintFirstDet=4 And @tintWeekDays=5  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,25-datepart(day,@DNextDate),@DNextDate)), 3)  
  
  
--Fifth Thursday of month  
  
  
If @tintFirstDet=5 And @tintWeekDays=5  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,32-datepart(day,@DNextDate),@DNextDate)), 3)    
  
  
----------------------------------------------------------------------------------------------------  
  
  
  
--To Get First Friday of Month ---  
If @tintFirstDet=1 And @tintWeekDays=6  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,2-datepart(day,@DNextDate),@DNextDate)), 4)   
  
--Second  Friday of month  
If @tintFirstDet=2 And @tintWeekDays=6  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,8-datepart(day,@DNextDate),@DNextDate)),4)   
  
  
--Third Friday of Month  
If @tintFirstDet=3 And @tintWeekDays=6  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,15-datepart(day,@DNextDate),@DNextDate)), 4)   
  
--Fourth Friday of Month  
If @tintFirstDet=4 And @tintWeekDays=6  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,21-datepart(day,@DNextDate),@DNextDate)), 4)  
  
  
--Fifth Friday of month  
If @tintFirstDet=5 And @tintWeekDays=6  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,28-datepart(day,@DNextDate),@DNextDate)), 4)    
  
  
  
  
  
--To Get First Saturday of Month ---  
If @tintFirstDet=1 And @tintWeekDays=7  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,1-datepart(day,@DNextDate),@DNextDate)), 5)   
  
--Second  Saturday of month  
If @tintFirstDet=2 And @tintWeekDays=7  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,8-datepart(day,@DNextDate),@DNextDate)),5)   
  
  
--Third Saturday of Month  
If @tintFirstDet=3 And @tintWeekDays=7  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,15-datepart(day,@DNextDate),@DNextDate)), 5)   
  
--Fourth Saturday of Month  
If @tintFirstDet=4 And @tintWeekDays=7  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,22-datepart(day,@DNextDate),@DNextDate)), 5)  
  
  
--Fifth Saturday of month  
If @tintFirstDet=5 And @tintWeekDays=7  
Set @vcRetValue= DATEADD(wk, DATEDIFF(wk,0,dateadd(dd,29-datepart(day,@DNextDate),@DNextDate)), 5)    
  
Return @vcRetValue  
  
End
GO
