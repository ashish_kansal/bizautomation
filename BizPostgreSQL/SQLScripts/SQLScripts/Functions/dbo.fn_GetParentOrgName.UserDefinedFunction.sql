/****** Object:  UserDefinedFunction [dbo].[fn_GetParentOrgName]    Script Date: 07/26/2008 18:12:41 ******/

GO

GO
--Created By: Debasish Nag      
--Created On: 11th Feb 2006      
--Purpose: To Get the Name of Parent of the Organization as per request  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getparentorgname')
DROP FUNCTION fn_getparentorgname
GO
CREATE FUNCTION [dbo].[fn_GetParentOrgName](@numDivisionId Numeric)     
 RETURNS NVarchar(100)    
AS      
BEGIN   
 DECLARE @vcCompanyName as NVarchar(100)  
 SELECT @vcCompanyName = vcCompanyName FROM  
 (  
            SELECT ci.vcCompanyName FROM CompanyAssociations ca, CompanyInfo ci, DivisionMaster dm  
            WHERE ca.numDivisionID=@numDivisionId AND        
            ca.numAssociateFromDivisionId = DM.numDivisionId AND  
            dm.numCompanyId = ci.numCompanyId AND  
            ca.bitChildOrg=1 AND 
            ca.bitDeleted  = 0
            UNION  
            SELECT ci.vcCompanyName FROM CompanyAssociations ca, CompanyInfo ci  
            WHERE ca.numAssociateFromDivisionId=@numDivisionId AND  
            ca.numCompanyId = ci.numCompanyId AND  
            ca.bitParentOrg=1 AND
            ca.bitDeleted  = 0   
 ) As tmpTable  
  
 RETURN @vcCompanyName    
END
GO
