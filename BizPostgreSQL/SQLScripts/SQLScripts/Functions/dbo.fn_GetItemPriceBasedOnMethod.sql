GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetItemPriceBasedOnMethod')
DROP FUNCTION fn_GetItemPriceBasedOnMethod
GO
CREATE FUNCTION [dbo].[fn_GetItemPriceBasedOnMethod] 
(
    @numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@bitCalculatedPrice BIT
	,@monCalculatedPrice DECIMAL(20,5)
	,@numWarehouseItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
	,@numCurrencyID NUMERIC(18,0)
)
RETURNS @ItemPrice TABLE
(
	monPrice DECIMAL(20,5)
	,tintDisountType TINYINT
	,decDiscount FLOAT
	,monFinalPrice DECIMAL(20,5) 
)
AS 
BEGIN	
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @monVendorCost DECIMAL(20,5)
	DECLARE @tintPriceLevel AS TINYINT
	DECLARE @tintRuleType AS TINYINT
	DECLARE @tintDisountType AS TINYINT
	DECLARE @decDiscount AS FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) = NULL
	DECLARE @monFinalPrice AS DECIMAL(20,5) = NULL
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification AS NUMERIC(18,0)
	DECLARE @tintPriceBookDiscount AS TINYINT
	DECLARE @tintPriceMethod TINYINT
	DECLARE @fltUOMConversionFactor INT = 1
	DECLARE @bitKitParent BIT
	DECLARE @bitAssembly BIT
	DECLARE @bitCalAmtBasedonDepItems BIT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	DECLARE @numBaseCurrency NUMERIC(18,0)
	DECLARE @fltExchangeRate FLOAT

	SELECT 
		@monListPrice = (CASE 
							WHEN ISNULL(@bitCalculatedPrice,0) = 1 
							THEN @monCalculatedPrice 
							ELSE (CASE 
									WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE ItemCurrencyPrice.numDomainID=@numDomainID AND ItemCurrencyPrice.numItemCode=@numItemCode AND ItemCurrencyPrice.numCurrencyID=ISNULL(@numCurrencyID,0))  
									THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE ItemCurrencyPrice.numDomainID=@numDomainID AND ItemCurrencyPrice.numItemCode=@numItemCode AND ItemCurrencyPrice.numCurrencyID=ISNULL(@numCurrencyID,0)),0)
									ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(Item.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(Item.monListPrice,0) END)
								END) 
						END)
		,@numItemClassification=numItemClassification
		,@monVendorCost=((CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monCost,0) END) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit))
		,@bitKitParent=ISNULL(bitKitParent,0)
		,@bitAssembly=ISNULL(bitAssembly,0)
		,@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0)
		,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
	FROM 
		Item 
	LEFT JOIN
		Vendor
	ON
		Item.numVendorID = Vendor.numVendorID
		AND Item.numItemCode = Vendor.numItemCode
	WHERE
		Item.numItemCode=@numItemCode

	SELECT 
		@numBaseCurrency=ISNULL(numCurrencyID,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID

	IF @numBaseCurrency = @numCurrencyID
	BEGIN
		SET @fltExchangeRate = 1
	END
	ELSE
	BEGIN
		SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),1)

		IF ISNULL(@fltExchangeRate,0) = 0
		BEGIN
			SET @fltExchangeRate = 1
		END
	END

	IF @bitCalculatedPrice = 0 AND (ISNULL(@bitKitParent,0)=1 OR ISNULL(@bitAssembly,0) = 1) AND ISNULL(@bitCalAmtBasedonDepItems,0) = 1
	BEGIN
		SELECT
			@monListPrice = monPrice
		FROM
			dbo.fn_GetKitAssemblyCalculatedPrice
			(
				@numDomainID
				,@numDivisionID
				,@numItemCode
				,@numQty
				,@numWarehouseItemID
				,@tintKitAssemblyPriceBasedOn
				,0
				,0
				,@vcSelectedKitChildItems
				,@numCurrencyID
				,@fltExchangeRate
			)    
	END
		

	SELECT 
		@tintPriceMethod=ISNULL(numDefaultSalesPricing,0)
		,@tintPriceBookDiscount=tintPriceBookDiscount 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID

	-- GET ORGANIZATION DETAL
	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile,
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID       

	IF @tintPriceMethod= 1 -- PRICE LEVEL
	BEGIN
		IF (SELECT 
				COUNT(*) 
			FROM 
				PricingTable 
			WHERE 
				numItemCode=@numItemCode 
				AND ISNULL(numPriceRuleID,0) = 0 
				AND 1 = (CASE 
							WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
							THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
							ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
						END)) > 0
		BEGIN
			SELECT
				@tintDisountType = 1,
				@decDiscount = 0,
				@tintRuleType = tintRuleType,
				@monPrice = (CASE tintRuleType
										WHEN 1 -- Deduct From List Price
										THEN
											CASE tintDiscountType 
												WHEN 1 -- PERCENT
												THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
												WHEN 2 -- FLAT AMOUNT
												THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
												WHEN 3 -- NAMED PRICE
												THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
											END
										WHEN 2 -- Add to primary vendor cost
										THEN
											CASE 
												WHEN ISNULL(@bitKitParent,0) = 1
												THEN
													@monListPrice
												ELSE
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)) * @fltUOMConversionFactor
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
													END
											END
										WHEN 3 -- Named price
										THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
										END
									) 
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
					*
				FROM
					PricingTable
				WHERE 
					PricingTable.numItemCode = @numItemCode
					AND ISNULL(PricingTable.numPriceRuleID,0) = 0
					AND 1 = (CASE 
								WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
								THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
								ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
							END)
			) TEMP
			WHERE
				numItemCode=@numItemCode 
				AND ISNULL(numPriceRuleID,0) = 0 
				AND 1 = (CASE 
							WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
							THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
							ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
						END)
				AND 1 = (CASE 
							WHEN ISNULL(@tintPriceLevel,0) > 0 
							THEN (CASE WHEN Id=@tintPriceLevel THEN 1 ELSE 0 END) 
							ELSE (CASE WHEN @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty THEN 1 ELSE 0 END)
						END)
		END

		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		SELECT 
			ISNULL(@monPrice,@monListPrice) AS monPrice,
			1 AS tintDisountType,
			0 AS decDiscount,
			ISNULL(@monPrice,@monListPrice)
	END
	ELSE IF @tintPriceMethod= 2 AND (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainID=@numDomainID AND tintRuleFor=1) > 0 -- PRICE RULE
	BEGIN
		DECLARE @numPriceRuleID NUMERIC(18,0)
		DECLARE @tintPriceRuleType AS TINYINT
		DECLARE @tintPricingMethod TINYINT
		DECLARE @tintPriceBookDiscountType TINYINT
		DECLARE @decPriceBookDiscount FLOAT
		DECLARE @intQntyItems INT
		DECLARE @decMaxDedPerAmt FLOAT

		SELECT 
			TOP 1
			@numPriceRuleID = numPricRuleID,
			@tintPricingMethod = tintPricingMethod,
			@tintPriceRuleType = p.tintRuleType,
			@tintPriceBookDiscountType = P.[tintDiscountType],
			@decPriceBookDiscount = P.[decDiscount],
			@intQntyItems = P.[intQntyItems],
			@decMaxDedPerAmt = P.[decMaxDedPerAmt]
		FROM   
			PriceBookRules P 
		LEFT JOIN 
			PriceBookRuleDTL PDTL 
		ON 
			P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN 
			PriceBookRuleItems PBI 
		ON 
			P.numPricRuleID = PBI.numRuleID
		LEFT JOIN 
			[PriceBookPriorities] PP 
		ON 
			PP.[Step2Value] = P.[tintStep2] 
			AND PP.[Step3Value] = P.[tintStep3]
		WHERE  
			P.numDomainID=@numDomainID 
			AND tintRuleFor=1
			AND (
					((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
					OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
					OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

					OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
					OR ((tintStep2 = 2 AND PBI.numValue = @numItemClassification) AND (tintStep3 = 3)) -- Priority 8
					OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
				)
		ORDER BY 
			PP.Priority ASC


		IF ISNULL(@numPriceRuleID,0) > 0
		BEGIN
			IF @tintPricingMethod = 1 -- BASED ON PRICE TABLE
			BEGIN
				SELECT
					@tintDisountType = tintDiscountType,
					@decDiscount = ISNULL(decDiscount,0),
					@tintRuleType = tintRuleType,
					@monPrice = (CASE tintRuleType WHEN 1 THEN @monListPrice WHEN 2 THEN @monVendorCost END) ,
					@monFinalPrice = (CASE tintRuleType
											WHEN 1 -- Deduct From List Price
											THEN
												CASE tintDiscountType 
													WHEN 1 -- PERCENT
													THEN (CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END)
													WHEN 2 -- FLAT AMOUNT
													THEN (CASE WHEN @monListPrice > 0 THEN (CASE WHEN (@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monListPrice * @numQty * @fltUOMConversionFactor) - (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor) ELSE 0 END) ELSE 0 END)
													WHEN 3 -- NAMED PRICE
													THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
												END
											WHEN 2 -- Add to primary vendor cost
											THEN
												CASE
												WHEN ISNULL(@bitKitParent,0) = 1
												THEN @monListPrice
												ELSE
													CASE tintDiscountType 
														WHEN 1  -- PERCENT
														THEN @monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100))
														WHEN 2 -- FLAT AMOUNT
														THEN (CASE WHEN (@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END) > 0 THEN ((@monVendorCost * @numQty * @fltUOMConversionFactor) + (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)) / (@numQty * @fltUOMConversionFactor)  ELSE 0 END)
														WHEN 3 -- NAMED PRICE
														THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
													END
												END
											WHEN 3 -- Named price
											THEN (CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END)
											END
										) 
				FROM
				(
					SELECT 
						ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
						*
					FROM
						PricingTable
					WHERE 
						PricingTable.numItemCode = @numItemCode
						AND ISNULL(PricingTable.numPriceRuleID,0) = 0
						AND 1 = (CASE 
									WHEN EXISTS (SELECT * FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0) = 0 AND numCurrencyID=@numCurrencyID)
									THEN (CASE WHEN ISNULL(numCurrencyID,0) = @numCurrencyID THEN 1 ELSE 0 END)
									ELSE (CASE WHEN ISNULL(numCurrencyID,0) = 0 THEN 1 ELSE 0 END)
								END)
				) TEMP
				WHERE
					((tintRuleType = 3 AND Id=@tintPriceLevel) OR (tintRuleType <> 3 AND @numQty * @fltUOMConversionFactor >= intFromQty AND @numQty * @fltUOMConversionFactor <= intToQty))
			END
			ELSE IF @tintPricingMethod = 2 -- BASED ON RULE
			BEGIN
				SET @monPrice = (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END)

				IF (@tintPriceBookDiscountType = 1 ) -- Percentage 
				BEGIN
					SET @tintDisountType = @tintPriceBookDiscountType
					SET @decPriceBookDiscount = @decPriceBookDiscount * (@numQty/@intQntyItems);
					SET @decDiscount = @decPriceBookDiscount * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * (CASE WHEN @tintPriceRuleType = 2 THEN @monVendorCost ELSE @monListPrice END) /100;
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
				
					IF ( @tintPriceRuleType = 1 )
							SET @monFinalPrice = (@monListPrice - @decDiscount);
					IF ( @tintPriceRuleType = 2 )
							SET @monFinalPrice = (@monVendorCost + @decDiscount);

					SET @monFinalPrice = (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST((ISNULL(@monFinalPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(@monFinalPrice,0) END)
				END
				IF ( @tintPriceBookDiscountType = 2 ) -- Flat discount 
				BEGIN
					SET @decDiscount = (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST((ISNULL(@decPriceBookDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(@decPriceBookDiscount,0) END) * ((@numQty * @fltUOMConversionFactor)/@intQntyItems);

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintPriceRuleType = 1 )
						SET @monFinalPrice =  ((@monListPrice * @numQty * @fltUOMConversionFactor) - @decDiscount) / (@numQty * @fltUOMConversionFactor);
						
					IF ( @tintPriceRuleType = 2 )
						SET @monFinalPrice = ((@monVendorCost * @numQty * @fltUOMConversionFactor) + @decDiscount) / (@numQty * @fltUOMConversionFactor);
				END
			END
		END
		
		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		SELECT 
			(CASE WHEN @monFinalPrice IS NULL THEN @monListPrice ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@monPrice,0) ELSE ISNULL(@monFinalPrice,0) END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN 1 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@tintPriceBookDiscountType,1) ELSE 1 END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN 0 ELSE (CASE WHEN @tintPriceBookDiscount = 1 THEN ISNULL(@decPriceBookDiscount,0) ELSE 0 END) END),
			(CASE WHEN @monFinalPrice IS NULL THEN @monListPrice ELSE ISNULL(@monFinalPrice,0) END)
	END
	ELSE IF @tintPriceMethod = 3 -- Last Price
	BEGIN
		SELECT 
			TOP 1 
			@monPrice = (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST(((ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate) AS INT) ELSE (ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) END) 
			,@tintDisountType = ISNULL(OpportunityItems.bitDiscountType,1)
			,@decDiscount = (CASE 
								WHEN ISNULL(OpportunityItems.bitDiscountType,1) = 1 
								THEN (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST(((ISNULL(OpportunityItems.fltDiscount,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate) AS INT) ELSE (ISNULL(OpportunityItems.fltDiscount,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) END) 
								ELSE ISNULL(OpportunityItems.fltDiscount,0) END) 
			,@monFinalPrice = (CASE 
								WHEN ISNULL(OpportunityItems.fltDiscount,0) > 0 AND ISNULL(numUnitHour,0) > 0 
								THEN (CASE 
										WHEN ISNULL(OpportunityItems.bitDiscountType,1)=1 
										THEN (CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST(((((ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0))/numUnitHour) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate) AS INT) ELSE (((ISNULL(monTotAmtBefDiscount,0) - ISNULL(monTotAmount,0))/numUnitHour) * ISNULL(OpportunityMaster.fltExchangeRate,1)) END)
										ELSE CAST((CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST(((ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate) AS INT) ELSE (ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) END)  - ((CASE WHEN @numBaseCurrency <> @numCurrencyID THEN CAST(((ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) / @fltExchangeRate) AS INT) ELSE (ISNULL(monPrice,0) * ISNULL(OpportunityMaster.fltExchangeRate,1)) END)  * (ISNULL(OpportunityItems.fltDiscount,0)/100)) AS DECIMAL(20,5)) END) 
								ELSE monPrice 
								END)
			,@tintRuleType = 3
		FROM 
			OpportunityItems 
		JOIN 
			OpportunityMaster 
		ON 
			OpportunityItems.numOppId = OpportunityMaster.numOppId 
		WHERE 
			OpportunityMaster.numDomainId= @numDomainID
			AND tintOppType = 1
			AND tintOppStatus = 1
			AND numItemCode=@numItemCode
		ORDER BY
			OpportunityMaster.numOppId DESC

		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		VALUES
		( 
			ISNULL(@monPrice,0),
			@tintDisountType,
			@decDiscount,
			@monFinalPrice
		)
	END
	ELSE
	BEGIN
		INSERT INTO @ItemPrice
		(
			monPrice
			,tintDisountType
			,decDiscount
			,monFinalPrice
		)
		VALUES
		( 
			ISNULL(@monListPrice,0),
			1,
			0,
			ISNULL(@monListPrice,0)
		)
	END

	RETURN
END
GO