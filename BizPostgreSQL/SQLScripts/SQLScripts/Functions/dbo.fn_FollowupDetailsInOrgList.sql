GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_FollowupDetailsInOrgList')
DROP FUNCTION fn_FollowupDetailsInOrgList
GO
CREATE FUNCTION [dbo].[fn_FollowupDetailsInOrgList](@numContactID NUMERIC, @numFollowup INT, @numDomainID NUMERIC) 

RETURNS VARCHAR(MAX)
AS
BEGIN

DECLARE @RetFollowup Varchar(MAX)
SET @RetFollowup=''

 IF(@numFollowup = 1)
  BEGIN
	SELECT @RetFollowup =
	CASE 
		WHEN (SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID=ConECampaign.numConEmailCampID ANd ISNULL(bitSend,0)=1) > 0 THEN
			(SELECT (CASE 
					 WHEN GenericDocuments.numGenericDocID IS NOT NULL
					 THEN CONCAT(GenericDocuments.vcDocName + ' ',
								 ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.bintSentON,@numDomainID),''),
								(CASE 
									WHEN ISNULL(ConECampaignDTL.bitSend,0)=1 AND ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=0 THEN ' <span style="color:black;font-weight:bold">Executed:</span><span style="color:red;">Failed</span>' 
									WHEN ISNULL(ConECampaignDTL.bitSend,0)=1 AND ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=1 THEN ' <span style="color:black;font-weight:bold">Executed:</span><span style="color:green;">Success</span>' 
									ELSE ' <span style="color:black;font-weight:bold">Executed:</span><span style="color:purple;">Pending</span>' 
								END),
								(CASE 
									WHEN (ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=1 AND ISNULL(ConECampaignDTL.bitEmailRead,0) = 1) THEN ' <span style="color:black;font-weight:bold">Read:</span><span style="color:green;">Yes</span>' 
									WHEN (ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=1 AND ISNULL(ConECampaignDTL.bitEmailRead,0) = 0) THEN ' <span style="color:black;font-weight:bold">Read:</span><span style="color:purple;">Pending</span>' 
									ELSE ' <span style="color:black;font-weight:bold">Read:</span><span style="color:purple;"></span>' 
								END))
					 WHEN tblActionItemData.RowID IS NOT NULL 
					 THEN CONCAT(tblActionItemData.Activity + ' ',
								ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.bintSentON,@numDomainID),''))--,
								--' <img alt="Action Items" height="16px" width="16px" title="Action Items" src="../images/MasterList-16.gif">')
					ELSE ''
					END)
			FROM
				ConECampaignDTL
			LEFT JOIN
				ECampaignDTLs
			ON
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN
				GenericDocuments 
			ON
				ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
			LEFT JOIN
				tblActionItemData
			ON
				ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
			WHERE
				ConECampaignDTL.numConECampDTLID = (SELECT MAX(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID=ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1)
				)

		ELSE ''
	END 
	FROM
		ConECampaign
	WHERE
		ConECampaign.numContactId=@numContactID
		AND ISNULL(bitEngaged,0)=1
	--SET @RetFollowup= vcLastFollowup
END

ELSE IF (@numFollowup = 2)
BEGIN
	SELECT @RetFollowup = 
		CASE 
			WHEN (SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID=ConECampaign.numConEmailCampID ANd ISNULL(bitSend,0)=0) > 0 THEN
				(SELECT (CASE 
						WHEN GenericDocuments.numGenericDocID IS NOT NULL THEN CONCAT(GenericDocuments.vcDocName, +' '+ ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.dtExecutionDate,@numDomainID),''))
						WHEN tblActionItemData.RowID IS NOT NULL THEN CONCAT(tblActionItemData.Activity,  +' '+ ISNULL(dbo.FormatedDateFromDate(ConECampaignDTL.dtExecutionDate,@numDomainID),''))
						ELSE ''
						END)
				FROM
					ConECampaignDTL
				LEFT JOIN
					ECampaignDTLs
				ON
					ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
				LEFT JOIN
					GenericDocuments 
				ON
					ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
				LEFT JOIN
					tblActionItemData
				ON
					ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
				WHERE
					ConECampaignDTL.numConECampDTLID = (SELECT MIN(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID=ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=0)
					)

			ELSE ''
		END 
	FROM
		ConECampaign
	WHERE
		ConECampaign.numContactId=@numContactID
		AND ISNULL(bitEngaged,0)=1
--SET @RetFollowup = vcNextFollowup
END

ELSE IF(@numFollowup = 3)
  BEGIN
	SELECT @RetFollowup =
	CASE 
		WHEN (SELECT COUNT(*) FROM ConECampaignDTL CEDInner WHERE CEDInner.numConECampID=ConECampaign.numConEmailCampID ANd ISNULL(bitSend,0)=1) > 0 THEN
			(SELECT (CASE 
					 WHEN GenericDocuments.numGenericDocID IS NOT NULL THEN
											(SELECT vcEmailLog FROM ConECampaignDTL WHERE ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1)
				ELSE ''
				END)
					
			FROM
				ConECampaignDTL
			LEFT JOIN
				ECampaignDTLs
			ON
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN
				GenericDocuments 
			ON
				ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
			LEFT JOIN
				tblActionItemData
			ON
				ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
			WHERE
				ConECampaignDTL.numConECampDTLID = (SELECT MAX(ConECampaignDTL.numConECampDTLID) FROM ConECampaignDTL WHERE ConECampaignDTL.numConECampID=ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1)
				
			)
		ELSE ''
	END 
	FROM
		ConECampaign
	WHERE
		ConECampaign.numContactId=@numContactID
		AND ISNULL(bitEngaged,0)=1   --- LastEmailStatus
END

RETURN @RetFollowup
END


GO


