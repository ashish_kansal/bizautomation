/****** Object:  UserDefinedFunction [dbo].[fn_PopulateKitDesc]    Script Date: 07/26/2008 18:12:50 ******/

GO

GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_populateassemblydesc')
DROP FUNCTION fn_populateassemblydesc
GO
CREATE FUNCTION [dbo].[fn_PopulateAssemblyDesc](@numOppItemCode as numeric(9),@Units as FLOAT) returns varchar(300)

as
begin
	declare @Desc as varchar(300)
	set @Desc=''
	declare @numChildItem as numeric(9)
	declare @Quantity as integer
	set @numChildItem=0

	select top 1 @numChildItem=numChildItemID,@Quantity=numQtyItemsReq 
	from ItemDetails ID
    join Item I
    on ID.numChildItemID=I.numItemCode
    Join OpportunityItems OI
    on OI.numItemCode=ID.numItemKitID
    where numoppitemtCode=@numOppItemCode order by numChildItemID

	while @numChildItem>0
	begin

	select @Desc=@Desc+ vcItemName+'('+convert(varchar(30),@Quantity*@Units)+' '+ isnull(vcUnitofMeasure,'Units') +'), '  from Item where numItemCode=@numChildItem


	select top 1 @numChildItem=numChildItemID,@Quantity=numQtyItemsReq 
	from ItemDetails ID
    join Item I
    on ID.numChildItemID=I.numItemCode
    Join OpportunityItems OI
    on OI.numItemCode=ID.numItemKitID
    where numoppitemtCode=@numOppItemCode and numChildItemID>@numChildItem order by numChildItemID

	if @@rowcount=0 set @numChildItem=0
	end

	return @Desc
end
GO
