/****** Object:  UserDefinedFunction [dbo].[fn_GetPercentageWon]    Script Date: 07/26/2008 18:12:42 ******/

GO

GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getpercentagewon')
DROP FUNCTION fn_getpercentagewon
GO
CREATE FUNCTION [dbo].[fn_GetPercentageWon](@numUserID numeric,@numDomainID numeric,@dtDateFrom datetime,@dtDateTo datetime)
returns varchar(10)
as
begin
declare @retPer as varchar(10)
declare @intOppWon as integer
declare @intOPP as integer

set @intOppWon=(select count(*) from OpportunityMaster where numrecowner=@numUserID and tintOppStatus=1 and bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo)
select @intOPP=(select count(*) from opportunitymaster where numrecowner=@numUserID and numdomainid=@numDomainID      
 and intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo)

if @intOPP=0 
begin
set @retPer=0
end
else
begin
set @retPer=round((@intOppWon/@intOPP)*100,2)
end
return @retPer
end
GO
