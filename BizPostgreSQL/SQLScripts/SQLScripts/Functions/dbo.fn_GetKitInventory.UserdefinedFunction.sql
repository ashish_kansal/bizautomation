GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetKitInventory')
DROP FUNCTION fn_GetKitInventory
GO
CREATE FUNCTION [dbo].[fn_GetKitInventory]
(
	@numKitId NUMERIC
	,@numWarehouseID NUMERIC(18,0)
)          
RETURNS FLOAT          
AS          
BEGIN          
	DECLARE @OnHand AS FLOAT
	SET @OnHand = 0       
    
	;WITH CTE(numItemKitID,numItemCode,numQtyItemsReq,numCalculatedQty) AS
	(
		SELECT 
			CONVERT(NUMERIC(18,0),0),
			numItemCode,
			DTL.numQtyItemsReq,
			CAST(DTL.numQtyItemsReq AS NUMERIC(9,0)) AS numCalculatedQty                                
		FROM 
			Item
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numKitId 
		UNION ALL
		SELECT 
			Dtl.numItemKitID,
			i.numItemCode,
			DTL.numQtyItemsReq,
			CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS NUMERIC(9,0)) AS numCalculatedQty
		FROM 
			Item i                               
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			Dtl.numChildItemID=i.numItemCode
		INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode 
		WHERE 
			Dtl.numChildItemID != @numKitId
	)

	SELECT
		@OnHand = CAST(MIN(FLOOR(numOnHand)) AS FLOAT) 
	FROM
	(
		SELECT 
			(CASE WHEN ISNULL(SUM(numOnHand),0)=0 THEN 0 WHEN ISNULL(SUM(numOnHand),0)>=numQtyItemsReq AND numQtyItemsReq>0 THEN ISNULL(SUM(numOnHand),0)/numQtyItemsReq ELSE 0 end) AS numOnHand
		FROM 
			cte c  
		LEFT JOIN
		(
			SELECT 
				numItemID
				,numWareHouseID
				,ISNULL(numOnHand,0) numOnHand
			FROM
				WareHouseItems
		) WI
		ON
			WI.numItemID = c.numItemCode
			AND WI.numWareHouseID = @numWarehouseID 
		GROUP BY
			c.numItemCode
			,c.numQtyItemsReq
	) TEMP

	RETURN @OnHand          
END
GO
