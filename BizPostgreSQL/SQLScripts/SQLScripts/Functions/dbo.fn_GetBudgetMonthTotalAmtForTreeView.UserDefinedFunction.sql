/****** Object:  UserDefinedFunction [dbo].[fn_GetBudgetMonthTotalAmtForTreeView]    Script Date: 07/26/2008 18:12:30 ******/

GO

GO
--Created By Siva                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getbudgetmonthtotalamtfortreeview')
DROP FUNCTION fn_getbudgetmonthtotalamtfortreeview
GO
CREATE FUNCTION [dbo].[fn_GetBudgetMonthTotalAmtForTreeView]                   
(@numBudgetId numeric(9),@Date as Datetime,@Id numeric(9)=0,@sintByte as tinyint,@numDomainId numeric(9))                      
Returns DECIMAL(20,5)                  
As                      
Begin                       
    Declare @monAmount as DECIMAL(20,5)                  
    Declare @montotalAmt as DECIMAL(20,5)                 
    Declare @numMonth as tinyint               
    Set @numMonth=month(getutcdate())               
    Set @montotalAmt=0      
    Set @monAmount=0         
    
 If @sintByte=0         
  Select @monAmount= sum(isnull(monAmount,0)) From OperationBudgetDetails Where  numChartAcntId=@Id And numBudgetId=@numBudgetId And (intyear=year(@Date) Or intyear=year(@Date)+1) 
   Else If @sintByte=1        
  Select @monAmount= sum(isnull(monAmount,0)) From MarketBudgetDetails Where  numListItemID=@Id And numMarketId=@numBudgetId  And (intyear=year(@Date) Or intyear=year(@Date)+1)             
   Else If  @sintByte=2        
  Select @monAmount= sum(isnull(monAmount,0)) From ProcurementBudgetDetails Where  numItemGroupId=@Id And numProcurementId=@numBudgetId  And (intyear=year(@Date) Or intyear=year(@Date)+1)              
    
                        
 Return @monAmount                      
End
GO
