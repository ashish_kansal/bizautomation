
--Created By Anoop Jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getexchangerate')
DROP FUNCTION getexchangerate
GO
CREATE FUNCTION [dbo].[GetExchangeRate](@numDomainID numeric,@numCurrencyID numeric)                                  
 RETURNS float                                
as                                  
Begin    

declare @numBaseCurrencyID as numeric
declare @fltExchangeRate as float

select @numBaseCurrencyID=numCurrencyID from Domain Where numDomainID=@numDomainID

if @numBaseCurrencyID=@numCurrencyID set @fltExchangeRate=1
else if exists(select * from Currency Where numCurrencyID=@numBaseCurrencyID and chrCurrency='USD')
select @fltExchangeRate=fltExchangeRate from Currency Where numCurrencyID=@numCurrencyID
else
select @fltExchangeRate=C1.fltExchangeRate/(case when C2.fltExchangeRate=0 then 1 else C2.fltExchangeRate end)  from Currency C1 
Join Currency C2
on C1.numDomainID=C2.numDomainID
where C1.numCurrencyID=@numCurrencyID and C2.numCurrencyID=@numBaseCurrencyID

return isnull(@fltExchangeRate,1)                             
                
End
GO
