GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_FindVendorCost')
DROP FUNCTION fn_FindVendorCost
GO
CREATE FUNCTION [dbo].[fn_FindVendorCost]
(
	@numItemCode NUMERIC,
	@numDivisionID as numeric(9)=0,                  
    @numDomainID as numeric(9)=0,            
	@units FLOAT
)
RETURNS DECIMAL(20,5) 
AS
BEGIN

 DECLARE @VendorCost DECIMAL(20,5) 
 
declare @numRelationship as numeric(9)                  
declare @numProfile as numeric(9)                  
declare @monListPrice DECIMAL(20,5)
DECLARE @bitCalAmtBasedonDepItems bit

/*Profile and relationship id */
select @numRelationship=numCompanyType,@numProfile=vcProfile from DivisionMaster D                  
join CompanyInfo C on C.numCompanyId=D.numCompanyID                  
where numDivisionID =@numDivisionID       

IF EXISTS(SELECT ISNULL([monCost],0) ListPrice FROM [Vendor] V INNER JOIN dbo.Item I 
		ON V.numItemCode = I.numItemCode
		WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID AND V.numVendorID=@numDivisionID)       
begin      
       SELECT @monListPrice=ISNULL([monCost],0) FROM [Vendor] V INNER JOIN dbo.Item I 
				ON V.numItemCode = I.numItemCode
				WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID 
				AND V.numVendorID=@numDivisionID
end      
else      
begin      
	 SELECT @monListPrice=ISNULL([monCost],0) FROM [Vendor] WHERE [numItemCode]=@numItemCode AND [numDomainID]=@numDomainID
end      


SELECT TOP 1 @VendorCost=dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice ,@units,I.numItemCode)
		FROM    Item I 
        LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2]
                                              AND PP.[Step3Value] = P.[tintStep3]
WHERE   numItemCode = @numItemCode AND tintRuleFor=2
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC

if @VendorCost is null
	select @VendorCost=convert(DECIMAL(20,5),@monListPrice)                       

if @VendorCost is null
		 select @VendorCost=(monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end))                      
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID 
 	


 RETURN @VendorCost

END
GO
