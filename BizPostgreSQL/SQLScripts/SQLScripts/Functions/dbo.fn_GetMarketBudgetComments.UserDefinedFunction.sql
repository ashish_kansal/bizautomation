/****** Object:  UserDefinedFunction [dbo].[fn_GetMarketBudgetComments]    Script Date: 07/26/2008 18:12:39 ******/

GO

GO
--Created By Siva       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getmarketbudgetcomments')
DROP FUNCTION fn_getmarketbudgetcomments
GO
CREATE FUNCTION [dbo].[fn_GetMarketBudgetComments]      
(@numMarketId numeric(9),@numDomainId numeric(9),@numListItemID numeric(9)=0)      
Returns varchar(50)      
As      
Begin      
 Declare @vcComments as varchar(50)            
 Select @vcComments=isnull(MBD.vcComments,'') From MarketBudgetMaster MBM
  inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId    
 Where MBM.numDomainId=@numDomainId And MBD.numMarketId=@numMarketId And MBD.numListItemID=@numListItemID     
 Return @vcComments        
End
GO
