/****** Object:  UserDefinedFunction [dbo].[GetCommisionDtls]    Script Date: 07/26/2008 18:12:56 ******/

GO

GO
--select dbo.GetCommisionDtls(1,'1/1/2000','1/1/2007')
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCommisionDtls')
DROP FUNCTION GetCommisionDtls
GO
CREATE FUNCTION [dbo].[GetCommisionDtls](@numUserID numeric(9),@dtFrom datetime,@dtTo datetime)
	RETURNS DECIMAL(20,5) 
AS
BEGIN

declare @bitMainComm as bit
declare @bitRoleComm as bit
declare @fltMainCommPer as bit
declare @TotMoney as DECIMAL(20,5)
select @bitMainComm=bitMainComm,@fltMainCommPer=fltMainCommPer,@bitRoleComm=bitRoleComm from UserMaster where numUserID=@numUserID

if @bitMainComm=1 
begin
	
        select @TotMoney=sum(monPAmount)*@fltMainCommPer from OpportunityMaster where tintOppType=1 and tintOppStatus=1 and numCreatedby=@numUserID
	and (bintAccountClosingDate between @dtFrom and @dtTo)
end

if @bitRoleComm=1 
begin
	set @TotMoney=@TotMoney+0
end

return isnull(@TotMoney,0)
END
GO
