GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getOPPState')
DROP FUNCTION fn_getOPPState
GO
CREATE FUNCTION [dbo].[fn_getOPPState] (@numOppId numeric,@numDomainID  NUMERIC,@tintMode AS TINYINT)
RETURNS VARCHAR(1000)
AS
BEGIN
	DECLARE @strState varchar(50)
	DECLARE  @tintOppType  AS TINYINT
	DECLARE  @tintBillType  AS TINYINT
	DECLARE  @tintShipType  AS TINYINT
	DECLARE @numBillToAddressID AS NUMERIC(18,0)
	DECLARE @numShipToAddressID AS NUMERIC(18,0)
 
	DECLARE @numParentOppID AS NUMERIC,@numDivisionID AS NUMERIC 
      
	SELECT  
		@tintOppType = tintOppType,
		@tintBillType = tintBillToType,
		@numBillToAddressID = numBillToAddressID,
		@tintShipType = tintShipToType,
		@numShipToAddressID = numShipToAddressID,
		@numDivisionID = numDivisionID
	FROM   
		OpportunityMaster 
	WHERE  
		numOppId = @numOppId

	-- When Creating PO from SO and Bill type is Customer selected 
	SELECT @numParentOppID=ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId
            
	IF @tintMode=1 --Billing Address
	BEGIN
		If ISNULL(@numBillToAddressID,0) > 0
		BEGIN
			SELECT  
				@strState = isnull(dbo.fn_GetState(AD.numState),'') 
			FROM 
				AddressDetails AD 
			WHERE
				AD.numDomainID=@numDomainID 
				AND AD.numAddressID = @numBillToAddressID
		END
		ELSE
		BEGIN
				IF @tintBillType IS NULL OR (@tintBillType = 1 AND @tintOppType = 1) --Primary Bill Address or When Sales order and bill to is set to customer	 
				BEGIN
					SELECT  
						@strState =  isnull(dbo.fn_GetState(AD.numState),'') 
									 
					FROM 
						AddressDetails AD 
					WHERE 
						AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
							AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1                    
				END
				ELSE IF @tintBillType = 1 AND @tintOppType = 2 -- When Create PO from SO and Bill to is set to Customer
				BEGIN
					SELECT @strState = dbo.fn_getOPPState(@numParentOppID,@numDomainID,@tintMode)
				END
				ELSE IF @tintBillType = 0
				BEGIN
					SELECT @strState = isnull(dbo.fn_GetState(AD.numState),'') 
										 FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
												JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
												JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
														WHERE  D1.numDomainID = @numDomainID
				END
				ELSE IF @tintBillType = 2 OR @tintBillType = 3
				BEGIN
					SELECT @strState = isnull(dbo.fn_GetState(numBillState),'') 
									FROM   OpportunityAddress WHERE  numOppID = @numOppId
				END
		END
	END
	ELSE IF @tintMode=2 --Shipping Address
	BEGIN
		If ISNULL(@numShipToAddressID,0) > 0
		BEGIN
			SELECT  
				@strState = isnull(dbo.fn_GetState(AD.numState),'')
			FROM 
				AddressDetails AD 
			WHERE
				AD.numDomainID=@numDomainID 
				AND AD.numAddressID = @numShipToAddressID
		END
		ELSE
		BEGIN
			IF @tintShipType IS NULL OR (@tintShipType = 1 AND @tintOppType = 1)
			BEGIN
					SELECT  @strState = isnull(dbo.fn_GetState(AD.numState),'') 
					FROM AddressDetails AD 
						WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
						AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
			END
			ELSE IF @tintShipType = 1 AND @tintOppType = 2 -- When Create PO from SO and Ship to is set to Customer 
			BEGIN
				SELECT @strState=dbo.fn_getOPPState(@numParentOppID,@numDomainID,@tintMode)
			END
			ELSE IF @tintShipType = 0
			BEGIN
				SELECT @strState = isnull(dbo.fn_GetState(AD.numState),'') 
									 FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
											JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
											JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
													WHERE  D1.numDomainID = @numDomainID
			END
			ELSE IF @tintShipType = 2 OR @tintShipType = 3
			BEGIN
				SELECT @strState = isnull(dbo.fn_GetState(numShipState),'')
								FROM   OpportunityAddress WHERE  numOppID = @numOppId
			END
		END
	END

return @strState
end
GO
