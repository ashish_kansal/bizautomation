/****** Object:  UserDefinedFunction [dbo].[fn_GetContactsAOIsCSV]    Script Date: 07/26/2008 18:12:32 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcontactsaoiscsv')
DROP FUNCTION fn_getcontactsaoiscsv
GO
CREATE FUNCTION [dbo].[fn_GetContactsAOIsCSV] (@numContactID numeric)
	RETURNS varchar(250) 
AS
BEGIN
	DECLARE @abc varchar(250)
	SET @abc=''
	SELECT @abc = @abc + ',' + ltrim(rtrim(str(numAOIID))) + ','
		FROM AOIContactLink 
		WHERE numCOntactID=@numContactID
	RETURN @abc
END
GO
