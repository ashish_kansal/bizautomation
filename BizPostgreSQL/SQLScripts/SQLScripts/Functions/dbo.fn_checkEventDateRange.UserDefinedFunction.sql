/****** Object:  UserDefinedFunction [dbo].[fn_checkEventDateRange]    Script Date: 07/26/2008 18:12:27 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_checkeventdaterange')
DROP FUNCTION fn_checkeventdaterange
GO
CREATE FUNCTION [dbo].[fn_checkEventDateRange](@bintDueDate datetime,@bintProcessCreateddate datetime,@startDate as integer
			,@startTime integer,@StartTimePeriod integer,@numOppId numeric(9),@numOppStageId numeric(9),@Type as char(1))
returns bit
as 
begin
	declare @inRange as bit
	declare @bintPrevClosedate as datetime
	set @inRange = 0

	if @startDate = 1 
		begin
			if  convert (datetime,convert(varchar(11),@bintDueDate) +'  '+ dbo.fn_getCustomDataValue(isnull(@startTime,0),17) + case when @StartTimePeriod = 0 then 'AM' else 'PM' end )
			between  dateadd(hour,-1,getdate()) and getdate() 
			set @inRange = 1
				
		end
	else if @startDate = 2
		begin
			if  convert (datetime,convert(varchar(11),@bintProcessCreateddate) +'  '+ dbo.fn_getCustomDataValue(isnull(@startTime,0),17) + case when @StartTimePeriod = 0 then 'AM' else 'PM' end )
			between  dateadd(hour,-1,getdate()) and getdate() 
			set @inRange = 1
		end
	else
		begin
			if @type = 'O'
				begin
					select @bintPrevClosedate = bintStagecomDate from  OpportunityStageDetails where 
					numStagePErcentage <> 0 and numOppId = @numOppId and bitStageCompleted =1   
					and numOppStageId = dbo.fn_getOppPreviousStage(@numOppId,@numOppStageId)
					set @bintPrevClosedate = dateadd(day,@startDate-3,@bintPrevClosedate) 
				end
			else
				begin
					select @bintPrevClosedate = bintStagecomDate from  ProjectsStageDetails where 
					numStagePErcentage <> 0 and numProId = @numOppId and bitStageCompleted =1   
					and numProStageId = dbo.fn_getProPreviousStage(@numOppId,@numOppStageId)
					set @bintPrevClosedate = dateadd(day,@startDate-3,@bintPrevClosedate)
				end
			if  convert (datetime,convert(varchar(11),@bintPrevClosedate) +'  '+ dbo.fn_getCustomDataValue(isnull(@startTime,0),17) + case when @StartTimePeriod = 0 then 'AM' else 'PM' end )
			between  dateadd(hour,-1,getdate()) and getdate() 
			set @inRange = 1

		end
	return  @inRange 
end
GO
