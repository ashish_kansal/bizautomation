/****** Object:  UserDefinedFunction [dbo].[GetItemInventoryAssetAccount]    Script Date: 07/26/2008 18:13:04 ******/

GO

GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getiteminventoryassetaccount')
DROP FUNCTION getiteminventoryassetaccount
GO
CREATE FUNCTION [dbo].[GetItemInventoryAssetAccount](@numItemCode numeric(9))    
 RETURNS numeric(9)   
AS    
BEGIN    
declare @numAssetChartAcntId as  numeric(9)  
    
select @numAssetChartAcntId=isnull(numAssetChartAcntId,'0') from Item where numItemCode=@numItemCode       
    
    
return isnull(@numAssetChartAcntId,'0')    
END
GO
