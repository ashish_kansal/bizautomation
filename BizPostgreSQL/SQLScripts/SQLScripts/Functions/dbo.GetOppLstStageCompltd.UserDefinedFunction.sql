/****** Object:  UserDefinedFunction [dbo].[GetOppLstStageCompltd]    Script Date: 07/26/2008 18:13:06 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getopplststagecompltd')
DROP FUNCTION getopplststagecompltd
GO
CREATE FUNCTION [dbo].[GetOppLstStageCompltd](@numOppid numeric(9))
	RETURNS varchar (500) 
AS
BEGIN
declare @StageDetail as varchar(500)
declare @DueDate as varchar(100)
declare @ComDate as varchar(100)
declare @numOppStageId as varchar(10)

set @numOppStageId=(select top 1 numOppStageId  from OpportunityStageDetails 
where numoppId=@numOppid and numStagePercentage!=100 and bitstagecompleted=1 
and bintStageComDate=(select top 1 max(bintStageComDate) from OpportunityStageDetails 
where numoppId=@numOppid and bitstagecompleted=1))
if @numOppStageId !=''
begin

set @StageDetail=(select vcStageDetail from OpportunityStageDetails where numOppStageId=@numOppStageId)


end

return @StageDetail
END
GO
