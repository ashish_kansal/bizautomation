/****** Object:  UserDefinedFunction [dbo].[fn_GetTotalHrsWorked]    Script Date: 07/26/2008 18:12:47 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_gettotalhrsworked')
DROP FUNCTION fn_gettotalhrsworked
GO
CREATE FUNCTION [dbo].[fn_GetTotalHrsWorked]        
(@dtStartDate datetime,@dtEndDate datetime,@numUserCntID numeric,@numDomainId numeric)                                        
 RETURNS decimal(10,2)          
as        
Begin        
  Declare @decTotalHrsWorked as decimal(10,2)          
 Select @decTotalHrsWorked=sum(x.Hrs) From (Select  isnull(Sum(Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)),0) as Hrs        
  from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1             
  And (dtFromDate between @dtStartDate And @dtEndDate Or dtToDate between @dtStartDate And @dtEndDate) And         
  numUserCntID=@numUserCntID  And numDomainID=@numDomainId         
  ) x        
 Return @decTotalHrsWorked        
End
GO
