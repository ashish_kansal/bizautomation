GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPercentageChangeInLeadToAccountConversion')
DROP FUNCTION GetPercentageChangeInLeadToAccountConversion
GO
CREATE FUNCTION [dbo].[GetPercentageChangeInLeadToAccountConversion]
(
	 @numDomainID AS NUMERIC(18,0),
	 @dtStartDate DATE,
	 @dtEndDate DATE,
	 @tintDateRange TINYINT --1:YTD TO SAME PERIOD LAST YEAR, 2:MTD TO SAME PERIOD LAST YEAR
)    
RETURNS NUMERIC(18,2)
AS    
BEGIN   
	DECLARE @LeadToAccountConversionChange NUMERIC(18,2) = 0.00
	DECLARE @TotalLeadsConvertedToAccount1 INT
	DECLARE @TotalLeadsConvertedToAccount2 INT


	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,FirstRecordID NUMERIC(18,0)
		,LastRecordID NUMERIC(18,0)
		,tintFirstCRMType TINYINT
		,tintLastCRMType TINYINT
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,FirstRecordID
		,LastRecordID
	)
	SELECT 
		DivisionMasterPromotionHistory.numDivisionID
		,MIN(ID)
		,MAX(ID)
	FROM 
		DivisionMasterPromotionHistory 
	INNER JOIN
		DivisionMaster DM
	ON
		DivisionMasterPromotionHistory.numDivisionID = DM.numDivisionID
	WHERE 
		DivisionMasterPromotionHistory.numDomainID=@numDomainID 
		AND DM.numDomainID = @numDomainID
		AND DM.bintCreatedDate BETWEEN @dtStartDate AND @dtEndDate
	GROUP BY
		DivisionMasterPromotionHistory.numDivisionID

	UPDATE
		T1
	SET
		tintFirstCRMType = DMPH1.tintPreviousCRMType
		,tintLastCRMType = DMPH2.tintNewCRMType
	FROM
		@TEMP T1
	INNER JOIN
		DivisionMasterPromotionHistory DMPH1
	ON
		T1.FirstRecordID = DMPH1.ID
	INNER JOIN
		DivisionMasterPromotionHistory DMPH2
	ON
		T1.LastRecordID = DMPH2.ID
	WHERE
		DMPH1.tintPreviousCRMType =0

	SELECT 
		@TotalLeadsConvertedToAccount1 = COUNT(*)
	FROM 
		@TEMP T1
	WHERE
		T1.tintFirstCRMType = 0
		AND T1.tintLastCRMType = 2
	
	
	DELETE FROM @TEMP

	INSERT INTO @TEMP
	(
		numDivisionID
		,FirstRecordID
		,LastRecordID
	)
	SELECT 
		DivisionMasterPromotionHistory.numDivisionID
		,MIN(ID)
		,MAX(ID)
	FROM 
		DivisionMasterPromotionHistory 
	INNER JOIN
		DivisionMaster DM
	ON
		DivisionMasterPromotionHistory.numDivisionID = DM.numDivisionID
	WHERE 
		DivisionMasterPromotionHistory.numDomainID=@numDomainID 
		AND DM.numDomainID = @numDomainID
		AND 1 = (CASE 
					WHEN @tintDateRange=1 
					THEN (CASE WHEN DM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,@dtStartDate) AND DATEADD(YEAR,-1,@dtEndDate) THEN 1 ELSE 0 END)
					WHEN @tintDateRange=2 
					THEN (CASE WHEN DM.bintCreatedDate BETWEEN DATEADD(MONTH,-1,@dtStartDate) AND DATEADD(MONTH,-1,@dtEndDate) THEN 1 ELSE 0 END)
					ELSE 0
				END)
	GROUP BY
		DivisionMasterPromotionHistory.numDivisionID

	UPDATE
		T1
	SET
		tintFirstCRMType = DMPH1.tintPreviousCRMType
		,tintLastCRMType = DMPH2.tintNewCRMType
	FROM
		@TEMP T1
	INNER JOIN
		DivisionMasterPromotionHistory DMPH1
	ON
		T1.FirstRecordID = DMPH1.ID
	INNER JOIN
		DivisionMasterPromotionHistory DMPH2
	ON
		T1.LastRecordID = DMPH2.ID
	WHERE
		DMPH1.tintPreviousCRMType =0

	SELECT 
		@TotalLeadsConvertedToAccount2 = COUNT(*)
	FROM 
		@TEMP T1
	WHERE
		T1.tintFirstCRMType = 0
		AND T1.tintLastCRMType = 2


	SET @LeadToAccountConversionChange = 100.0 * (ISNULL(@TotalLeadsConvertedToAccount1,0) - ISNULL(@TotalLeadsConvertedToAccount2,0)) / (CASE WHEN ISNULL(@TotalLeadsConvertedToAccount2,0) = 0 THEN 1 ELSE ISNULL(@TotalLeadsConvertedToAccount2,0) END)


	RETURN ISNULL(@LeadToAccountConversionChange,0.00)
END
GO