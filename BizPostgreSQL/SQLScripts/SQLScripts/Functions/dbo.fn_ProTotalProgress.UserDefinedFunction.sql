/****** Object:  UserDefinedFunction [dbo].[fn_ProTotalProgress]    Script Date: 07/26/2008 18:12:50 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_prototalprogress')
DROP FUNCTION fn_prototalprogress
GO
CREATE FUNCTION [dbo].[fn_ProTotalProgress] (@numProId  as numeric)  
returns varchar(10)  
as   
begin  
  
declare @Percentage as decimal(10,0)  
  
select @Percentage=sum(tintPercentage) from ProjectsStageDetails   
where numProId=@numProId and bitStageCompleted=1  
  
return convert(varchar(10),@Percentage)+'%'  
  
End
GO
