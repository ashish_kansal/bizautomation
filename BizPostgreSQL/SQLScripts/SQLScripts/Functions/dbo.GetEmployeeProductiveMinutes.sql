GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetEmployeeProductiveMinutes')
DROP FUNCTION GetEmployeeProductiveMinutes
GO
CREATE FUNCTION [dbo].[GetEmployeeProductiveMinutes]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@ClientTimeZoneOffset INT
)    
RETURNS INT  
AS
BEGIN
	DECLARE @numProductiveMinutes NUMERIC(18,0) = 0

	SELECT
		@numProductiveMinutes = SUM(DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate))
	FROM
		TimeAndExpense 
	LEFT JOIN
		OpportunityMaster
	ON
		TimeAndExpense.numOppID=OpportunityMaster.numOppID
	WHERE
		TimeAndExpense.numDomainID = @numDomainID
		AND TimeAndExpense.numUserCntID=@numUserCntID
		AND ISNULL(TimeAndExpense.numCategory,0) = 1
		AND numType NOT IN (4) -- DO NOT INCLUDE UNPAID LEAVE
		AND dtFromDate BETWEEN @dtFromDate AND @dtToDate

	SELECT
		@numProductiveMinutes = ISNULL(@numProductiveMinutes,0) + SUM(DATEDIFF(MINUTE,Communication.dtStartTime,Communication.dtEndTime))
	FROM
		Communication 
	LEFT JOIN
		DivisionMaster DM
	ON
		Communication.numDivisionId = DM.numDivisionID
	LEFT JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		Communication.numDomainID = @numDomainID
		AND Communication.numAssign=@numUserCntID
		AND Communication.dtStartTime BETWEEN @dtFromDate AND @dtToDate
		AND ISNULL(bitClosedFlag,0) = 1

	SELECT
		@numProductiveMinutes = ISNULL(@numProductiveMinutes,0) + SUM(DATEDIFF(MINUTE,Activity.StartDateTimeUtc,DATEADD(SECOND,Activity.Duration,Activity.StartDateTimeUtc)))
	FROM
		[Resource]
	INNER JOIN
		ActivityResource 
	ON
		[Resource].ResourceID = ActivityResource.ResourceID
	INNER JOIN
		Activity 
	ON
		ActivityResource.ActivityID=Activity.ActivityID
	WHERE
		[Resource].numUserCntId = @numUserCntID
		AND Activity.StartDateTimeUtc BETWEEN @dtFromDate AND @dtToDate

	SELECT 
		@numProductiveMinutes = ISNULL(@numProductiveMinutes,0) + dbo.GetTimeSpendOnTaskInMinutesDateRange(@numDomainID,@numUserCntID,SPDTTL.numTaskID,@dtFromDate,@dtToDate)
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numUserCntID = @numUserCntID
		AND SPDTTL.dtActionTime BETWEEN @dtFromDate AND @dtToDate
	GROUP BY
		SPDTTL.numTaskID

	RETURN @numProductiveMinutes
END
GO