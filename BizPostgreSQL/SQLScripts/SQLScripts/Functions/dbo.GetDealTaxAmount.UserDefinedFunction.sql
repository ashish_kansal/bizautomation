GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetDealTaxAmount')
DROP FUNCTION GetDealTaxAmount
GO
CREATE FUNCTION [dbo].[GetDealTaxAmount]
(
	@numOppID NUMERIC(18,0),
	@numOppBizDocsId NUMERIC(18,0) = 0,
	@numDomainId NUMERIC(18,0)
)    
RETURNS DECIMAL(20,5)
AS    
BEGIN   
	DECLARE @TotalTaxAmt AS DECIMAL(20,5) = 0		
	DECLARE @numTaxItemID AS NUMERIC(9)
		
	SELECT TOP 1 @numTaxItemID=numTaxItemID FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId ORDER BY numTaxItemID 
 
	WHILE @numTaxItemID>-1    
	BEGIN
		SELECT @TotalTaxAmt = @TotalTaxAmt + ISNULL(dbo.fn_CalOppItemTotalTaxAmt(@numDomainID,@numTaxItemID,@numOppID,@numOppBizDocsId),0)
    
		SELECT TOP 1 @numTaxItemID=numTaxItemID FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID>@numTaxItemID ORDER BY numTaxItemID
    
		IF @@rowcount=0 
			SET @numTaxItemID=-1    
	END   
	    
	RETURN CAST(ISNULL(@TotalTaxAmt,0) AS DECIMAL(20,5)) -- Set Accuracy of Two precision
END
GO