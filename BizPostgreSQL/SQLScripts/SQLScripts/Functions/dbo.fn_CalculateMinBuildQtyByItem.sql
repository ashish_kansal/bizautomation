GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_CalculateMinBuildQtyByItem')
DROP FUNCTION fn_CalculateMinBuildQtyByItem
GO
CREATE FUNCTION [dbo].[fn_CalculateMinBuildQtyByItem]
(
	@numKitId As NUMERIC=0,
	@numWareHouseID AS NUMERIC=0
)
RETURNS NUMERIC(18,0)
AS
BEGIN
	DECLARE @numMaxBuildQty AS NUMERIC(18,2)=0;

	DECLARE @tempTable TABLE
	(
		numItemKitID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUOMQuantity NUMERIC(18,0)
		,numBaseUnit NUMERIC(18,0)
		,numIDUOMId NUMERIC(18,0)
	)

	INSERT INTO @tempTable	
	SELECT 
		convert(NUMERIC(18,0),0)
		,numItemCode
		,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1))
		,ISNULL(numBaseUnit,0)
		,ISNULL(Dtl.numUOMId,ISNULL(numBaseUnit,0)) AS numIDUOMId
	FROM 
		Item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numKitId 

	INSERT INTO @tempTable
	SELECT 
		Dtl.numItemKitID
		,i.numItemCode
		,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))
		,ISNULL(i.numBaseUnit,0)
		,ISNULL(Dtl.numUOMId,ISNULL(i.numBaseUnit,0)) AS numIDUOMId
	FROM
		Item i                               
	INNER JOIN 
		ItemDetails Dtl
	ON 
		Dtl.numChildItemID=i.numItemCode
	INNER JOIN 
		@tempTable c 
	ON 
		Dtl.numItemKitID = c.numItemCode 
	WHERE 
		Dtl.numChildItemID!=@numKitId

	SELECT 
		@numMaxBuildQty = MIN(numOnHand/(CASE WHEN ISNULL(c.numUOMQuantity,0)=0 THEN 1 ELSE c.numUOMQuantity END))
	FROM 
		@tempTable c  
	LEFT JOIN 
		UOM 
	ON 
		UOM.numUOMId=c.numBaseUnit
	LEFT JOIN 
		UOM IDUOM 
	ON 
		IDUOM.numUOMId=c.numIDUOMId
	OUTER APPLY
	(
		SELECT TOP 1
			numWareHouseItemID
			,numOnHand
			,numOnOrder
			,numReorder
			,numAllocation
			,numBackOrder
			,numItemID
			,vcWareHouse
			,monWListPrice
		FROM
			WareHouseItems
		LEFT JOIN
			Warehouses W 
		ON
			W.numWareHouseID=WareHouseItems.numWareHouseID
		WHERE 
			WareHouseItems.numItemID = c.numItemCode 
			AND WareHouseItems.numWareHouseID=@numWarehouseID
		ORDER BY
			numWareHouseItemID
	) WI
	WHERE
		numItemKitID =0  AND WI.numWareHouseItemID > 0

	RETURN @numMaxBuildQty
END
GO