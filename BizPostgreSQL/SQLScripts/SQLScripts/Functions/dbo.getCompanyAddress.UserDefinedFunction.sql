/****** Object:  UserDefinedFunction [dbo].[getCompanyAddress]    Script Date: 07/26/2008 18:12:56 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getCompanyAddress')
DROP FUNCTION getCompanyAddress
GO
CREATE FUNCTION [dbo].[getCompanyAddress](@numDivisionId as numeric,@tintAddressType as tinyint,@numDomainID numeric)
returns varchar(1000)
as 
begin 
declare @street as varchar(100)
declare @City as varchar(100)
declare @State as varchar(100)
declare @PostCode as varchar(100)
declare @vcCountry as varchar(100)
declare @address1 as varchar(1000)
declare @add as varchar(100)
set @add =','
set @address1=''

SELECT
TOP 1 
@street =isnull(AD.vcStreet,'') ,
@City =isnull(AD.vcCity,''),
@State= isnull(dbo.fn_GetState(AD.numState),'') ,
@PostCode = isnull(AD.vcPostalCode,'') ,     
@vcCountry = isnull(dbo.fn_GetListName(AD.numCountry,0),'')   
                                                 
FROM                                                           
dbo.AddressDetails AD 
where AD.numRecordID=@numDivisionId AND tintAddressOf=2 AND tintAddressType=@tintAddressType 
AND AD.bitIsPrimary=1 AND numDomainID=@numDomainID

-- tintAddressOf => Contact->1,Organization->2,Opportunity->3
-- tintAddressType=> Billto Address-> 1, Shipto Address->2

set @address1 = @address1+@street
	if  len(@address1) = 0
	set @add=''
	else
	set @add=','

set @address1 = @address1 + case when @City <>'' then @add+@City else'' end
	if  len(@address1) = 0
	set @add=''
	else
	set @add=','

set @address1 = @address1 + case when @State <>'' then @add+@State else'' end
	if  len(@address1) = 0
	set @add=''
	else
	set @add=','

set @address1 = @address1 + case when @PostCode <>'' then @add+@PostCode else'' end
	if  len(@address1) = 0
	set @add=''
	else
	set @add=','

set @address1 = @address1 + case when @vcCountry <>'' then @add+@vcCountry else'' end
--print @address1
return @address1
end
GO
