-- will return state and country based on Billto/ShipTo set for domain and from relevant address
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_getTaxableStateCountry')
DROP FUNCTION fn_getTaxableStateCountry
GO
CREATE FUNCTION fn_getTaxableStateCountry
    (
      @numDomainID AS NUMERIC,
      @numOppID AS NUMERIC
    )
RETURNS @retInfo TABLE
    (
      numState NUMERIC,
      numCountry NUMERIC
    )
AS BEGIN 
    DECLARE @numDivisionID AS NUMERIC 
    DECLARE @tintBillToType AS TINYINT
    DECLARE @tintShipToType AS TINYINT
    DECLARE @tintBaseTax AS TINYINT
    DECLARE @numCountry AS NUMERIC(9)
    DECLARE @numState AS NUMERIC(9)

    SELECT  @tintBaseTax = [tintBaseTax] FROM    [Domain]    WHERE   [numDomainId] = @numDomainID
    SELECT  @numDivisionID = [numDivisionId], @tintBillToType = ISNULL([tintBillToType], 1), @tintShipToType = ISNULL([tintShipToType], 1) FROM    [OpportunityMaster]
    WHERE   [numOppId] = @numOppID
    
    IF @tintBaseTax = 2 --Shipping Address(Default)
        BEGIN
			-- Fore Help on Billto /Ship to type Flags See Bug ID 207
            IF @tintShipToType = 0 
                BEGIN
                    SELECT  @numState = ISNULL(AD1.numState, 0),
                            @numCountry = ISNULL(AD1.numCountry, 0)
                    FROM    companyinfo [Com1]
                            JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
                            JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
                            LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=div1.numDomainID 
							AND AD1.numRecordID=div1.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=2 AND AD1.bitIsPrimary=1
                    WHERE   D1.numDomainID = @numDomainID 
                END
            ELSE 
                IF @tintShipToType = 1 
                    BEGIN
						  SELECT   @numState = ISNULL(numState, 0),
									@numCountry = ISNULL(numCountry, 0)
						   FROM     dbo.AddressDetails
						   WHERE    numDomainID = @numDomainID
									AND numRecordID = @numDivisionID
									AND tintAddressOf = 2
									AND tintAddressType = 2
									AND bitIsPrimary=1
                    END
                ELSE 
                    IF @tintShipToType = 2 
                        BEGIN
                            SELECT  @numState = ISNULL([numShipState], 0),
                                    @numCountry = ISNULL([numShipCountry], 0)
                            FROM    [OpportunityAddress]
                            WHERE   [numOppID] = @numOppID
                        END
        END

    IF @tintBaseTax = 1 -- Billing Address
        BEGIN
			-- Fore Help on Billto /Ship to type Flags See Bug ID 207
            IF @tintBillToType = 0 
                BEGIN
                     SELECT  @numState = ISNULL(AD1.numState, 0),
                            @numCountry = ISNULL(AD1.numCountry, 0)
                    FROM    companyinfo [Com1]
                            JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
                            JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
                            LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=div1.numDomainID 
							AND AD1.numRecordID=div1.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
                    WHERE   D1.numDomainID = @numDomainID 
                END
            ELSE 
                IF @tintBillToType = 1 
                    BEGIN
                        SELECT   @numState = ISNULL(numState, 0),
									@numCountry = ISNULL(numCountry, 0)
						   FROM     dbo.AddressDetails
						   WHERE    numDomainID = @numDomainID
									AND numRecordID = @numDivisionID
									AND tintAddressOf = 2
									AND tintAddressType = 1
									AND bitIsPrimary=1
                    END
                ELSE 
                    IF @tintBillToType = 2 
                        BEGIN
                            SELECT  @numState = ISNULL([numBillState], 0),
                                    @numCountry = ISNULL([numBillCountry], 0)
                            FROM    [OpportunityAddress]
                            WHERE   [numOppID] = @numOppID
                        END
			
        END
	
    INSERT  INTO @retInfo
            SELECT  @numState,
                    @numCountry
    RETURN ;
   END