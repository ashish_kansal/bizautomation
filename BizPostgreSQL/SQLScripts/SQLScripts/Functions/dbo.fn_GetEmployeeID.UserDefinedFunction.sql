
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetEmployeeID]    Script Date: 02/19/2010 17:23:13 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getemployeeid')
DROP FUNCTION fn_getemployeeid
GO
CREATE FUNCTION [dbo].[fn_GetEmployeeID]
(@numBizDocAppID numeric(8),
 @numDomainId int)
Returns varchar(2000)
AS 
begin

declare @vcEmployee varchar(2000);
declare @vcUserName varchar(50);


declare Employee_Cursor CURSOR FOR
	
select cast (numEmployeeID as varchar(50) ) as vcUserName
 from AdditionalContactsInformation ACI
	INNER JOIN BizDocApprovalRuleEmployee BAR
	ON BAR.numEmployeeID = ACI.numContactID AND 
	BAR.numBizDocAppID=@numBizDocAppID

SET @vcEmployee=''

OPEN Employee_Cursor

FETCH NEXT FROM Employee_Cursor into @vcUserName
WHILE @@FETCH_STATUS = 0
   BEGIN
		set @vcEmployee= @vcEmployee + ',' + @vcUserName
		

      FETCH NEXT FROM Employee_Cursor into @vcUserName
   END;

CLOSE Employee_Cursor;
DEALLOCATE Employee_Cursor;

RETURN substring(@vcEmployee,2,len(@vcEmployee))

end