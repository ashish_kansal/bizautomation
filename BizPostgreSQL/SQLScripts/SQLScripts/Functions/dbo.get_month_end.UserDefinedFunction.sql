/****** Object:  UserDefinedFunction [dbo].[get_month_end]    Script Date: 07/26/2008 18:12:53 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='get_month_end')
DROP FUNCTION [get_month_end]
GO
CREATE FUNCTION [dbo].[get_month_end] (@date datetime)
RETURNS datetime AS
BEGIN
   RETURN dateadd(ms, -3, dateadd (m,datediff(m,0,
          dateadd(m,1,@date)),0))
END
GO
