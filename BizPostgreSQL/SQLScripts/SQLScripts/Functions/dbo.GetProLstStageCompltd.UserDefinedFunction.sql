/****** Object:  UserDefinedFunction [dbo].[GetProLstStageCompltd]    Script Date: 07/26/2008 18:13:09 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getprolststagecompltd')
DROP FUNCTION getprolststagecompltd
GO
CREATE FUNCTION [dbo].[GetProLstStageCompltd](@numProid numeric(9),@ClientTimeZoneOffset int)  
 RETURNS varchar (500)   
AS  
BEGIN  
declare @StageDetail as varchar(500)  
declare @DueDate as varchar(100)  
declare @ComDate as varchar(100)  
declare @numProStageId as varchar(10)  
  
set @numProStageId=(select top 1 numProStageId  from ProjectsStageDetails   
where numProId=@numProid and numStagePercentage!=100 and bitstagecompleted=1   
and bintStageComDate=(select top 1 max(bintStageComDate) from ProjectsStageDetails   
where numProId=@numProid and bitstagecompleted=1))  
  
if @numProStageId !=''  
begin  
  
set @StageDetail=(select vcStageDetail from ProjectsStageDetails where numProStageId=@numProStageId)  
set @DueDate=(select bintDueDate from ProjectsStageDetails where numProStageId=@numProStageId)  
set @ComDate= (select dateadd(minute,-@ClientTimeZoneOffset,bintStageComDate) from ProjectsStageDetails where numProStageId=@numProStageId)  
  
set @StageDetail=@StageDetail +' ,' + isnull(@ComDate,'-') + ' ,' + isnull(@DueDate,'-')   
  
end  
  
return @StageDetail  
END
GO
