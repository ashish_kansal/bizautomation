/****** Object:  UserDefinedFunction [dbo].[GetContractRemainingAmount]    Script Date: 07/26/2008 18:12:57 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcontractremainingamount')
DROP FUNCTION getcontractremainingamount
GO
CREATE FUNCTION [dbo].[GetContractRemainingAmount](@numContractId numeric,@numDivisionId numeric,@numDomainId numeric)                          
 RETURNS decimal(10,2)                          
as                          
Begin                          
  
DECLARE @vcRetValue1 decimal (10,2)                           
     
select @vcRetValue1 = isnull(sum(monamount),0)  
from  timeandexpense TM   
where       
TM.numContractId=@numContractId   
and numtype=1 and numCategory=2    
and  tM.numDomainId=@numDomainId            
  
RETURN isnull(@vcRetValue1,0)                     
  
end
GO
