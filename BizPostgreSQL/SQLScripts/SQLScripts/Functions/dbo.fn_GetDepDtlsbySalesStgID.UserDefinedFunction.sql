/****** Object:  UserDefinedFunction [dbo].[fn_GetDepDtlsbySalesStgID]    Script Date: 07/26/2008 18:12:36 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getdepdtlsbysalesstgid')
DROP FUNCTION fn_getdepdtlsbysalesstgid
GO
CREATE FUNCTION [dbo].[fn_GetDepDtlsbySalesStgID](@numOppID numeric, @numOppStageID numeric,@tintType tinyint)
	RETURNS varchar(100) 
AS

BEGIN
	DECLARE @vcRetValue varchar (100)
	set @vcRetValue=''

	select @vcRetValue=@@rowcount
	from  OpportunityDependency where numOpportunityId=@numOppID and numOppStageID=@numOppStageID
	
	if @@rowcount > 0 set @vcRetValue=1
	else set  @vcRetValue=0
	RETURN @vcRetValue	
END
GO
