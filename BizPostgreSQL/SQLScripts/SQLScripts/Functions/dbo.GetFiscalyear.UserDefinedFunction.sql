/****** Object:  UserDefinedFunction [dbo].[GetFiscalyear]    Script Date: 07/26/2008 18:13:02 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getfiscalyear')
DROP FUNCTION getfiscalyear
GO
CREATE FUNCTION [dbo].[GetFiscalyear](@date datetime,@numDomainId numeric(9))
returns numeric(4)
as
begin 
declare @startMonth as varchar(2)
select @startMonth=isnull(tintfiscalStartMonth,1) from  domain where numDomainId=@numDomainId


declare @GetFiscalYear as numeric(4)

  If @date < (convert(varchar(4),Year(@date))+'/'+ @startMonth +'/'+'01')
	begin
		set @GetFiscalYear =    Year(@date) - 0 - 1
	end
else
	begin
	 set @GetFiscalYear =    Year(@date) - 0
	end
--print @GetFiscalYear
return @GetFiscalYear
end
GO
