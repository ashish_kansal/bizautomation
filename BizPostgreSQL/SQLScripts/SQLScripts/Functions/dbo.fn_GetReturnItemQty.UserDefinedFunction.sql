
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetReturnItemQty]    Script Date: 01/22/2009 02:13:04 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getreturnitemqty')
DROP FUNCTION fn_getreturnitemqty
GO
CREATE FUNCTION [dbo].[fn_GetReturnItemQty]
(@numOppItemCode BIGINT) 
RETURNS bigint
AS
BEGIN
DECLARE	@Qty NUMERIC(9)
DECLARE	@Qty1 NUMERIC(9)
DECLARE @Status VARCHAR(20)

SELECT  @Status = dbo.[GetListIemName]([numReturnStatus]) ,@Qty = ISNULL([numQtyReturned],0),@Qty1 = ISNULL([numQtyToReturn],0) FROM [Returns] WHERE [numOppItemCode] = @numOppItemCode

IF @Status = 'Returned'
BEGIN
	SET @Qty = @Qty;
END
ELSE 
BEGIN
	SET @Qty = @Qty1;
END

  RETURN ISNULL(@Qty,0)
END
