/****** Object:  UserDefinedFunction [dbo].[fn_GetContactName]    Script Date: 07/26/2008 18:12:31 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcontactname')
DROP FUNCTION fn_getcontactname
GO
CREATE FUNCTION [dbo].[fn_GetContactName](@numContactID numeric)
Returns varchar(100) 
As
BEGIN
	DECLARE @RetName Varchar(100)
	set @RetName=''
	select @RetName=isnull(vcFirstname,'')+' '+isnull(vcLastName,'') from AdditionalContactsInformation  where numContactID=@numContactID
	RETURN isnull(@RetName,'-')
END
GO
