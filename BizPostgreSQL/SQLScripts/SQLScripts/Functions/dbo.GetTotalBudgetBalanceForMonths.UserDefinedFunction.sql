/****** Object:  UserDefinedFunction [dbo].[GetTotalBudgetBalanceForMonths]    Script Date: 07/26/2008 18:13:12 ******/

GO

GO
--Created By Siva          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='gettotalbudgetbalanceformonths')
DROP FUNCTION gettotalbudgetbalanceformonths
GO
CREATE FUNCTION [dbo].[GetTotalBudgetBalanceForMonths](@Month int,@Year int,@monAmount DECIMAL(20,5),@numDomainId numeric(9),@numUserCntID numeric(9),@sintForecast smallint)          
Returns DECIMAL(20,5)          
As          
Begin          
Declare @monOperBudgetAmt as DECIMAL(20,5)          
Declare @monProBudgetAmt as DECIMAL(20,5)          
Declare @monMarkBudgetAmt as DECIMAL(20,5)   
Declare @monTotalBudgetBalance as DECIMAL(20,5)         
----Set @monCashInflowAmt = dbo.GetCashInflowForMonths(@Month,@Year,@numDomainId)          
----Set @monSalesForeCastAmt=dbo.GetSalesForeCastForMonths(@Month,@Year,@numDomainId,@numUserCntID,@sintForecast)          

--For Operation Budget
	Select @monOperBudgetAmt=sum(isnull(monAmount,0))From OperationBudgetMaster OBM
	inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId
    where OBD.tintMonth=@month And OBD.intYear=@Year And OBM.numDomainId=@numDomainId
--For Procurement Budget
	Select @monProBudgetAmt=sum(isnull(monAmount,0))From ProcurementBudgetMaster PBM
	inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId
    where PBD.tintMonth=@month And PBD.intYear=@Year And PBM.numDomainId=@numDomainId
--For Market Budget     
   Select @monMarkBudgetAmt=sum(isnull(monAmount,0))From MarketBudgetMaster MBM
   inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId
   where tintMonth=@month And intYear=@Year  And MBM.numDomainId=@numDomainId
  
Set @monTotalBudgetBalance=isnull(@monOperBudgetAmt,0)+isnull(@monProBudgetAmt,0)+isnull(@monMarkBudgetAmt,0)          
          
        
return @monTotalBudgetBalance          
End
GO
