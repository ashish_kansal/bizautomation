/****** Object:  UserDefinedFunction [dbo].[fn_GetSubStgDtlsbyProStgID]    Script Date: 07/26/2008 18:12:45 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getsubstgdtlsbyprostgid')
DROP FUNCTION fn_getsubstgdtlsbyprostgid
GO
CREATE FUNCTION [dbo].[fn_GetSubStgDtlsbyProStgID](@numProID numeric, @numProStageID numeric,@tintType tinyint)
	RETURNS varchar(100) 
AS

BEGIN
	DECLARE @vcRetValue varchar (100)
	set @vcRetValue=''

	select @vcRetValue=@@rowcount
	from  ProjectsSubStageDetails 
	where numProId=@numProID and numStageDetailsId=@numProStageID
	
	if @@rowcount > 0 set @vcRetValue=1
	else set  @vcRetValue=0
	RETURN @vcRetValue	
END
GO
