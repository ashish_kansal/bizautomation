--Created By Chintan
/*Now there are 2 ways to calculate commisions
 1)Base commission % on total amounts paid (to invoices). (Default)
 2)Base commission % on gross profit.(New)
Which can be set from domain details

Gross Profit = (Amount Paid To Invoice - Total Vendor Cost)
Assumption: Vendor cost is specified for all items in invoice
*/
--SELECT GETDATE() FROM [UserMaster] WHERE [numDomainID]=72


--SELECT dbo.GetCommissionAmountOfDuration(82,17,72,'2010-03-01 19:55:59.437','2010-03-31 19:55:59.437')
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCommissionAmountOfDuration')
DROP FUNCTION GetCommissionAmountOfDuration
GO
CREATE FUNCTION [dbo].[GetCommissionAmountOfDuration]
    (
      @numUserId NUMERIC,
      @numUserCntID NUMERIC,
      @numDomainId NUMERIC,
      @dtStartDate AS DATETIME,
      @dtEndDate AS DATETIME,
      @tintMode AS TINYINT
    )
RETURNS DECIMAL(10, 2)
AS BEGIN

	DECLARE @TotalCommAmt DECIMAL(10, 2)
   
   SELECT @TotalCommAmt=isnull(sum(numComissionAmount),0) 
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and BC.numUserCntId=@numUserCntID
   where Opp.numDomainId=@numDomainId and
   --OB.dtCreatedDate between @dtStartDate and @dtEndDate AND
   BC.numUserCntId=@numUserCntID
   AND BC.bitCommisionPaid=0 --added by chintan BugID 982
   AND oppBiz.bitAuthoritativeBizDocs = 1 
      AND ((isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount AND @tintMode=0)
	   OR (isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount AND @tintMode=1))

   
-- SELECT @TotalCommAmt=isnull(sum(OppBizDocsPayDet.monAmount * OppBizDocsDet.fltExchangeRate),0)
-- FROM   opportunitybizdocsdetails oppbizdocsdet
--        INNER JOIN OpportunityBizDocsPaymentDetails OppBizDocsPayDet ON OppBizDocsDet.numBizDocsPaymentDetId = OppBizDocsPayDet.numBizDocsPaymentDetId
--        INNER JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsId= oppbizdocsdet.numBizDocsId
--    WHERE  OppBizDocsDet.numDomainId = @numDomainId
--           AND OppBizDocsDet.bitAuthoritativeBizDocs = 1
--           AND OppBizDocsPayDet.bitIntegrated = 0
--           AND OppBizDocsDet.numBizDocsId > 0
--           AND oppbizdocsdet.tintPaymentType = 5
--		   --AND oppbizdocsdet.dtCreationDate between @dtStartDate and @dtEndDate 
--	       and numContactID=@numUserCntID
    
    RETURN @TotalCommAmt
   END
