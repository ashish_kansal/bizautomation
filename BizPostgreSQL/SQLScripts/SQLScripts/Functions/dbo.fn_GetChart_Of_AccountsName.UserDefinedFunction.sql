GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetChart_Of_AccountsName')
DROP FUNCTION fn_GetChart_Of_AccountsName
GO
CREATE FUNCTION [dbo].[fn_GetChart_Of_AccountsName](@numAccountId numeric)
Returns varchar(100) 
As
BEGIN
	declare @vcAccountName as varchar(100)

	Select @vcAccountName=isnull(vcAccountName,'') from Chart_Of_Accounts where numAccountId=@numAccountId

return isnull(@vcAccountName,'-')
END
GO
