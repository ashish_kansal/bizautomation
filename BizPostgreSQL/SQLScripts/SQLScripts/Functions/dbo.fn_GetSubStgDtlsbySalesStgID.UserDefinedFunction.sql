/****** Object:  UserDefinedFunction [dbo].[fn_GetSubStgDtlsbySalesStgID]    Script Date: 07/26/2008 18:12:45 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getsubstgdtlsbysalesstgid')
DROP FUNCTION fn_getsubstgdtlsbysalesstgid
GO
CREATE FUNCTION [dbo].[fn_GetSubStgDtlsbySalesStgID](@numOppID numeric, @numOppStageID numeric,@tintType tinyint)
	RETURNS varchar(100) 
AS
--Thsi function will take the ID fo rthe Territory or the List Details and return the corresponding name.
BEGIN
	DECLARE @vcRetValue varchar (100)
	set @vcRetValue=''

	select @vcRetValue=@@rowcount
	from  OpportunitySubStageDetails 
	where numOppId=@numOppID and numStageDetailsId=@numOppStageID
	
	if @@rowcount > 0 set @vcRetValue=1
	else set  @vcRetValue=0
	RETURN @vcRetValue	
END
GO
