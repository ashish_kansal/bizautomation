/****** Object:  UserDefinedFunction [dbo].[GetScheduledEmails]    Script Date: 07/26/2008 18:13:11 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getscheduledemails')
DROP FUNCTION getscheduledemails
GO
CREATE FUNCTION [dbo].[GetScheduledEmails] (@numScheduleid as numeric(9))     
returns varchar(1000)
as
begin
	declare @vcToEmail as varchar(1000)
	declare @vcEmail as varchar(100)
	declare @numContactId as numeric
	
	select @numContactId=min(CRS.numContactId) from CustRptSchContacts CRS      
	join AdditionalContactsInformation AC on   CRS.numContactId =  AC.numContactId 
	and numScheduleId =@numScheduleid

	declare @i as integer
	set @i=0 
	Set @vcToEmail=''

	while @numContactId > 0
	begin
	 
		select  @vcEmail=vcEmail from AdditionalContactsInformation  Where     numContactId =@numContactId
		set @vcToEmail= case when @i=0 then @vcEmail else @vcToEmail +';'+ @vcEmail end

		 select @numContactId=min(CRS.numContactId) from CustRptSchContacts CRS      
		 join AdditionalContactsInformation AC on   CRS.numContactId =  AC.numContactId 
		 where  numScheduleId =@numScheduleid and  CRS.numContactId > @numContactId
		 set @i=@i+1 	
	End  

return  @vcToEmail
end


-- where      
-- numScheduleid =
GO
