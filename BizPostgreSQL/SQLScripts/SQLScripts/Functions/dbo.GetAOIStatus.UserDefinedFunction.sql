/****** Object:  UserDefinedFunction [dbo].[GetAOIStatus]    Script Date: 07/26/2008 18:12:55 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAOIStatus')
DROP FUNCTION GetAOIStatus
GO
CREATE FUNCTION [dbo].[GetAOIStatus](@numAOIID numeric(9),@numContactID numeric(9))
	RETURNS tinyint 
AS
BEGIN
declare @Status as tinyint

select @Status=tintSelected from AOIContactLink where @numAOIID=numAOIID and @numContactID=numContactID
return @Status
END
GO
