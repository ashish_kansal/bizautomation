GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAlertDetail')
DROP FUNCTION GetAlertDetail
GO
CREATE FUNCTION [dbo].[GetAlertDetail]
(
	@numEmailHstrID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numECampaignID NUMERIC(18,0),
	@numContactID NUMERIC(18,0),
	@bitOpenActionItem BIT,
	@bitOpencases BIT,
	@bitOpenProject BIT,
	@bitOpenSalesOpp BIT,
	@bitBalancedue BIT,
	@bitUnreadEmail BIT,
	@bitCampaign BIT,
	@TotalBalanceDue BIGINT,
	@OpenSalesOppCount BIGINT,
	@OpenCaseCount BIGINT,
	@OpenProjectCount BIGINT,
	@UnreadEmailCount BIGINT,
	@OpenActionItemCount BIGINT,
	@CampaignDTLCount BIGINT
)
Returns VARCHAR(MAX) 
As
BEGIN
	DECLARE @vcAlert AS VARCHAR(MAX) = ''
	
	--CHECK AR BALANCE
	IF @bitBalancedue = 1 AND  @TotalBalanceDue > 0 
    BEGIN 
		SET @vcAlert = '<a href="#" onclick="OpenAlertDetail(1,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/dollar.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
    END 

	--CHECK OPEN SALES ORDER COUNT
	IF @bitOpenSalesOpp = 1 AND @OpenSalesOppCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(2,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/icons/cart.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
    END 

	--CHECK OPEN CASES COUNT
	IF @bitOpencases = 1 AND @OpenCaseCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(3,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/icons/headphone_mic.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
    END 

	--CHECK OPEN PROJECT COUNT
	IF @bitOpenProject = 1 AND @OpenProjectCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(4,'+ CAST(@numDivisionID AS VARCHAR(18)) +',0)"><img alt="" src="../images/Compass-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'  
    END 

	--CHECK UNREAD MAIL COUNT
	--IF @bitUnreadEmail = 1 AND @UnreadEmailCount > 0 
 --   BEGIN 
 --      SET @vcAlert = @vcAlert + '<a title="Recent Correspondance" class="hyperlink" onclick="OpenCorresPondance('+ CAST(@numContactID AS VARCHAR(10)) + ');">(' + CAST(@UnreadEmailCount AS VARCHAR(10)) + ')</a>&nbsp;'
 --   END
	
	--CHECK ACTIVE CAMPAIGN COUNT
	IF @bitCampaign = 1 AND ISNULL(@numECampaignID,0) > 0
	BEGIN
		
		IF @CampaignDTLCount = 0
		BEGIN
			SET @vcAlert = @vcAlert + '<img alt="" src="../images/comflag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
		END
		ELSE
		BEGIN
			SET @vcAlert = @vcAlert + '<img alt="" src="../images/GreenFlag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
		END
	END 

	--CHECK COUNT OF ACTION ITEM
	IF @bitOpenActionItem = 1 AND @OpenActionItemCount > 0 
	BEGIN
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenAlertDetail(5,'+ CAST(@numDivisionID AS VARCHAR(18)) +',' + CAST(@numContactID AS VARCHAR(18)) + ')"><img alt="" src="../images/MasterList-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
	END
	IF(@numContactID>0)
	BEGIN
		SET @vcAlert = @vcAlert + '<a href="#" onclick="OpenWindow('+ CAST(@numDivisionID AS VARCHAR(18)) +',0,0)"><img alt="" src="../images/MasterList_Organization.png" style="height: 20px; vertical-align: middle; padding-bottom: 2px;" /></a>&nbsp;'
	END
	RETURN @vcAlert
END
GO