/****** Object:  UserDefinedFunction [dbo].[fn_GetContactAOIsCSV]    Script Date: 07/26/2008 18:12:30 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcontactaoiscsv')
DROP FUNCTION fn_getcontactaoiscsv
GO
CREATE FUNCTION [dbo].[fn_GetContactAOIsCSV] (@numContactID numeric)
	RETURNS varchar(250) 
AS
BEGIN
	DECLARE @abc varchar(250)
	SET @abc=''
	SELECT @abc = @abc + ',' + ltrim(rtrim(str(numAOIID))) + ','
		FROM AOIContactLink 
		WHERE numCOntactID=@numContactID
	RETURN @abc
END
GO
