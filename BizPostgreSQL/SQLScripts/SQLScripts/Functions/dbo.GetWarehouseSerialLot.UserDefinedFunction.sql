GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWarehouseSerialLot')
DROP FUNCTION GetWarehouseSerialLot
GO
CREATE FUNCTION [dbo].[GetWarehouseSerialLot]
(
	@numDomainID AS NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitLot BIT = 0
)
RETURNS VARCHAR(8000) 
AS
BEGIN
	DECLARE @SerialList VARCHAR(8000)

	IF @bitLot = 1
	BEGIN
		SELECT  
			@SerialList = COALESCE(CONCAT(@SerialList,', '), '') + CONCAT(vcSerialNo,'(', numQty ,')','(',dbo.FormatedDateFromDate(dExpirationDate,@numDomainID),')')
		FROM    
			WareHouseItmsDTL 
		WHERE   
			numWareHouseItemID = @numWarehouseItemID
			AND numQty > 0
		ORDER BY
			dExpirationDate ASC
	END
	ELSE
	BEGIN
		SELECT  
			@SerialList = COALESCE(CONCAT(@SerialList,', '), '') + vcSerialNo
		FROM    
			WareHouseItmsDTL 
		WHERE   
			numWareHouseItemID = @numWarehouseItemID
			AND numQty > 0
		ORDER BY
			numWareHouseItmsDTLID ASC
	END

	SET @SerialList = ISNULL(SUBSTRING(@SerialList,2,LEN(@SerialList)),0)
	
	RETURN @SerialList
END
GO