
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckOrderItemInventoryStatus')
DROP FUNCTION CheckOrderItemInventoryStatus
GO
CREATE FUNCTION [dbo].[CheckOrderItemInventoryStatus] 
(
	@numItemCode NUMERIC(18,0) = NULL,
	@numQuantity AS FLOAT = 0,
	@numOppItemCode AS NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitKitParent BIT
)
RETURNS FLOAT
AS
BEGIN
    DECLARE @bitBackOrder FLOAT = 0
	DECLARE @numAllocation FLOAT
	DECLARE @numQtyShipped FLOAT = 0
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numWarehouseID NUMERIC(18,0)

	SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseItemID=@numWarehouseItemID

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Item INNER JOIN Domain ON Item.numDomainID=Domain.numDomainId WHERE numItemCode=@numItemCode
	SELECT @numQtyShipped = ISNULL(numQtyShipped,0) FROM OpportunityItems WHERE numoppitemtCode = @numOppItemCode

	IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq FLOAT,
			numQtyItemReq_Orig FLOAT,
			numAllocation FLOAT,
			numOnHand FLOAT,
			numPrentItemBackOrder INT,
			numWOID NUMERIC(18,0)
		)

		INSERT INTO 
			@TEMPTABLE
		SELECT
			OKI.numChildItemID,
			0,
			OKI.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKI.numWareHouseItemId,
			OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
			OKI.numQtyItemsReq_Orig,
			ISNULL(WI.numAllocation,0) + ISNULL((SELECT 
														SUM(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0))
													FROM 
														WareHouseItems WIInner
													WHERE 
														WIInner.numItemID=WI.numItemID 
														AND WIInner.numWareHouseID = WI.numWareHouseID
														AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),0),
			ISNULL(WI.numOnHand,0) + ISNULL((SELECT 
												SUM(ISNULL(WIInner.numOnHand,0))
											FROM 
												WareHouseItems WIInner
											WHERE 
												WIInner.numItemID=WI.numItemID 
												AND WIInner.numWareHouseID = WI.numWareHouseID
												AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),0),
			0,
			ISNULL((SELECT numWOId FROM WorkOrder WHERE numOppId=OKi.numOppId AND numOppItemID=OKi.numOppItemID AND numOppChildItemID=OKI.numOppChildItemID AND ISNULL(numOppKitChildItemID,0) = 0 AND ISNULL(numParentWOID,0)=0),0)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OI.numoppitemtCode = @numOppItemCode  

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numAllocation,0) + ISNULL((SELECT 
														SUM(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0))
													FROM 
														WareHouseItems WIInner
													WHERE 
														WIInner.numItemID=WI.numItemID 
														AND WIInner.numWareHouseID = WI.numWareHouseID
														AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),0),
			ISNULL(WI.numOnHand,0) + ISNULL((SELECT 
												SUM(ISNULL(WIInner.numOnHand,0))
											FROM 
												WareHouseItems WIInner
											WHERE 
												WIInner.numItemID=WI.numItemID 
												AND WIInner.numWareHouseID = WI.numWareHouseID
												AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),0),
			0,
			ISNULL((SELECT numWOId FROM WorkOrder WHERE numOppId=OKCI.numOppId AND numOppItemID=OKCI.numOppItemID AND numOppChildItemID=OKCI.numOppChildItemID AND numOppKitChildItemID = OKCI.numOppKitChildItemID AND ISNULL(numParentWOID,0)=0),0)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		UPDATE 
			@TEMPTABLE 
		SET 
			numPrentItemBackOrder = (CASE
										WHEN ISNULL(numWOID,0) > 0
										THEN
											(CASE 
												WHEN dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq) >= numQtyItemsReq
												THEN 0 
												ELSE
													(CASE WHEN numQtyItemReq_Orig > 0 THEN (numQtyItemsReq/numQtyItemReq_Orig) - CAST(dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/numQtyItemReq_Orig AS INT) ELSE 0 END)
											END)
										ELSE
											(CASE 
												WHEN @tintCommitAllocation = 2 
												THEN
													(CASE 
														WHEN numOnHand >= numQtyItemsReq
														THEN 0 
														ELSE
															(CASE WHEN numQtyItemReq_Orig > 0 THEN (numQtyItemsReq/numQtyItemReq_Orig) - CAST(numOnHand/numQtyItemReq_Orig AS INT) ELSE 0 END)
													END)
												ELSE
													(CASE 
														WHEN numAllocation >= numQtyItemsReq
														THEN 0 
														ELSE
															(CASE WHEN numQtyItemReq_Orig > 0 THEN (numQtyItemsReq/numQtyItemReq_Orig) - CAST(numAllocation/numQtyItemReq_Orig AS INT) ELSE 0 END)
													END)
											END)
									END)
		WHERE
			bitKitParent = 0

		
		-- NOW USE SUB KIT BACK ORDER NUMBER TO GENERATE PARENT KIT BACK ORDER NUMBER
		UPDATE 
			t2
		SET 
			t2.numPrentItemBackOrder = (CASE WHEN t2.numQtyItemReq_Orig > 0 THEN CEILING((SELECT MAX(numPrentItemBackOrder) FROM @TEMPTABLE t1 WHERE t1.numParentItemID = t2.numItemCode) / t2.numQtyItemReq_Orig) ELSE 0 END)
		FROM
			@TEMPTABLE t2
		WHERE
			bitKitParent = 1

		SET @bitBackOrder = ISNULL((SELECT MAX(numPrentItemBackOrder) FROM @TEMPTABLE WHERE ISNULL(numParentItemID,0) = 0),0)
	END
	ELSE
	BEGIN
		SELECT
			@numAllocation = ISNULL(numAllocation,0)
		FROM
			WareHouseItems 
		WHERE
			numWareHouseItemID = @numWarehouseItemID

		SET @numAllocation = ISNULL(@numAllocation,0) + ISNULL((SELECT 
																SUM(ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0))
															FROM 
																WareHouseItems 
															WHERE 
																numItemID=@numItemCode 
																AND numWareHouseID = @numWarehouseID
																AND numWareHouseItemID <> @numWarehouseItemID),0)

		IF (@numQuantity - @numQtyShipped) > @numAllocation
            SET @bitBackOrder = (@numQuantity - @numQtyShipped) - @numAllocation
		ELSE
			SET @bitBackOrder = 0
	END

	RETURN @bitBackOrder

END
GO
