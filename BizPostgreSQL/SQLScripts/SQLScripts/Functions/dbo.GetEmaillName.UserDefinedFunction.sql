/****** Object:  UserDefinedFunction [dbo].[GetEmaillName]    Script Date: 07/26/2008 18:13:02 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getemaillname')
DROP FUNCTION getemaillname
GO
CREATE FUNCTION [dbo].[GetEmaillName](@numEmailHstrID as numeric,@tinttype as int)
returns varchar(200)
as 
begin
declare @Name as varchar(200)
 select top 1 @Name=isnull(EHS.vcName,'') from EmailHStrToBCCAndCC  EHS  
join EmailMaster EM on em.numEmailid=  EHS.numEmailId  
 where numEmailHstrID=@numEmailHstrID and tintType=@tinttype
return @Name
end
GO
