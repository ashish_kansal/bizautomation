/****** Object:  UserDefinedFunction [dbo].[GetCashInflowForMonthsForProjectedBankBalance]    Script Date: 07/26/2008 18:12:56 ******/

GO

GO
--Created By Siva            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCashInflowForMonthsForProjectedBankBalance')
DROP FUNCTION GetCashInflowForMonthsForProjectedBankBalance
GO
CREATE FUNCTION [dbo].[GetCashInflowForMonthsForProjectedBankBalance](@intType as int,@month as int,@year as int,@numDomainId as numeric(9))            
returns DECIMAL(20,5)            
As            
Begin            
--Declare @monDebitIncomeAmt as DECIMAL(20,5)            
--Declare @monCreditIncomeAmt as DECIMAL(20,5)            
--Declare @monDebitARAmt as DECIMAL(20,5)            
--Declare @monCreditARAmt as DECIMAL(20,5)            
--Declare @monTotalIncomeAmt as DECIMAL(20,5)            
--Declare @monTotalARAmt as DECIMAL(20,5)            
--Declare @monCashInflowAmt as DECIMAL(20,5)            
--Declare @currentYear as int   

--    if @intType=0 
--	Set @currentYear=year(getutcdate())
--Else If @intType=-1
--	Set @currentYear=year(getutcdate())-1
--Else If @intType=1
--	Set @currentYear=year(getutcdate())+1
--      
--        
--Select  @monDebitIncomeAmt=sum(isnull(GJD.numDebitAmt,0)), @monCreditIncomeAmt= sum(isnull(GJD.numCreditAmt,0)) From General_Journal_Header GJH            
--inner join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId            
--inner join Chart_of_Accounts CA on GJD.numChartAcntId = CA.numAccountId             
--Where GJH.numDomainId=@numDomainId And CA.numAcntType in (822,825)  
--and ((month(GJH.datEntry_Date) between month(getutcdate()) And (case when @currentYear=@year then @Month else 0 end) And Year(GJH.datEntry_Date)=@year)  
--or (month(GJH.datEntry_Date) between 1 and (case when @currentYear< @year then  @Month else 0 end) and Year(GJH.datEntry_Date)=@year) Or (month(GJH.datEntry_Date) between month(getutcdate()) and (case when @currentYear< @year then  12 else 0 end)  And 
--Year(GJH.datEntry_Date)=@year-1))  
--   
----And ((Year(GJH.datEntry_Date)<@Year And month(GJH.datEntry_Date)<=month(getutcdate())) Or ((month(GJH.datEntry_Date)<=month(getutcdate()) And Year(GJH.datEntry_Date)=@year)))        
--       
--                        
------Print '@numDebitIncomeAmt=='+convert(varchar(10),@monDebitIncomeAmt)            
------Print '@numCreditIncomeAmt=='+convert(varchar(10),@monCreditIncomeAmt)            
--            
--Select  @monDebitARAmt=sum(isnull(GJD.numDebitAmt,0)), @monCreditARAmt= sum(isnull(GJD.numCreditAmt,0)) From General_Journal_Header GJH            
--inner join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId            
--inner join Chart_of_Accounts CA on GJD.numChartAcntId = CA.numAccountId             
--Where GJH.numDomainId=@numDomainId And CA.numAcntType =814     
--and ((month(GJH.datEntry_Date) between month(getutcdate()) And (case when @currentYear=@year then @Month else 0 end) And Year(GJH.datEntry_Date)=@year)  
--or (month(GJH.datEntry_Date) between 1 and (case when @currentYear< @year then  @Month else 0 end) and Year(GJH.datEntry_Date)=@year) Or (month(GJH.datEntry_Date) between month(getutcdate()) and (case when @currentYear< @year then  12 else 0 end)  And 
--Year(GJH.datEntry_Date)=@year-1))  
--   
------ And ((Year(GJH.datEntry_Date)<@Year And month(GJH.datEntry_Date)<=month(getutcdate())) Or ((month(GJH.datEntry_Date)<=month(getutcdate()) And Year(GJH.datEntry_Date)=@year)))        
--           
--            
--Set @monTotalIncomeAmt=isnull(@monCreditIncomeAmt,0)-isnull(@monDebitIncomeAmt,0)            
--Set @monTotalARAmt=isnull(@monDebitARAmt,0)-isnull(@monCreditARAmt,0)            
----Print '@monTotalIncomeAmt=='+convert(varchar(10),@monTotalIncomeAmt)            
----Print '@monTotalARAmt=='+convert(varchar(10),@monTotalARAmt)            
--Set @monCashInflowAmt=isnull(@monTotalIncomeAmt,0)+isnull(@monTotalARAmt,0)          
----Print '@monCashInflowAmt=='+convert(varchar(10),@monCashInflowAmt)            
RETURN 0-- @monCashInflowAmt            
End
GO
