/****** Object:  UserDefinedFunction [dbo].[GetFiscalQuarter]    Script Date: 07/26/2008 18:13:02 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getfiscalquarter')
DROP FUNCTION getfiscalquarter
GO
CREATE FUNCTION [dbo].[GetFiscalQuarter](@date datetime,@numDomainId numeric(4))
returns integer
as 
begin

declare @year as numeric(4)

set @year =dbo.getfiscalyear(@date,@numDomainId)




--print @date
--print @year
Declare @SQ1 as datetime 
Declare @SQ2 as datetime 
Declare @SQ3 as datetime 
Declare @SQ4 as datetime 
Declare @EQ1 as datetime 
Declare @EQ2 as datetime 
Declare @EQ3 as datetime 
Declare @EQ4 as datetime 
Declare @Quat as numeric(2)
--print '----quarter1------------------'


set @SQ1 = dateadd(month,0,dbo.GetFiscalStartDate(@year,@numDomainId)) 
set @EQ1 = dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(@year,@numDomainId)))
--print @sq1
--print @eq1
--print '----quarter2------------------'
set @SQ2 = dateadd(month,3,dbo.GetFiscalStartDate(@year,@numDomainId)) 
set @EQ2 = dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(@year,@numDomainId)))
--print @sq2
--print @eq2
--print '----quarter3------------------'
set @SQ3 = dateadd(month,6,dbo.GetFiscalStartDate(@year,@numDomainId)) 
set @EQ3 =  dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(@year,@numDomainId)))
--print @sq3
--print @eq3
--print '----quarter4------------------'
set @SQ4 = dateadd(month,9,dbo.GetFiscalStartDate(@year,@numDomainId)) 
set @EQ4 =  dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(@year,@numDomainId)))
--print @sq4
--print @eq4
--print '------------------------------'

if (@sQ1 <= @date and @eq1>=@date )
begin 
--print '1'
set @Quat =1
end
else
if @sQ2 <= @date  and @eq2 >= @date
begin 
--print '2'
set @Quat =2
end
else
if @sQ3 <= @date and @eq3 >= @date
begin 
--print '3'
set @Quat =3
end
else
if @sQ4 <= @date and @eq4 >= @date
begin 
--print '4'
set @Quat =4
end
--print @Quat
return @Quat
end
GO
