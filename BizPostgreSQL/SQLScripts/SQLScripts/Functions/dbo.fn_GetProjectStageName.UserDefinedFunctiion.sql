GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetProjectStageName')
DROP FUNCTION fn_GetProjectStageName
GO
CREATE FUNCTION [dbo].[fn_GetProjectStageName](@numProId numeric,@numStageDetailsId numeric)
Returns varchar(100) 
As
BEGIN
	declare @vcStageName as varchar(100)

	Select @vcStageName=isnull(vcStageName,'') from StagePercentageDetails where numProjectID=@numProId 
    and numStageDetailsId=@numStageDetailsId

return isnull(@vcStageName,'-')
END
GO
