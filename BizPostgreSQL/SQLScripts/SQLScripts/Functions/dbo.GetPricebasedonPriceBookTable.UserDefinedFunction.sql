-- SELECT dbo.GetPriceBasedOnPriceBook(366,100,1)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='getpricebasedonpricebookTable')
DROP FUNCTION getpricebasedonpricebookTable
GO
CREATE FUNCTION getpricebasedonpricebookTable
--Below function applies Price Rule on given Price an Qty and returns calculated value
    (
      @numRuleID NUMERIC,
      @monPrice DECIMAL(20,5),
      @units INT,
      @ItemCode numeric
    )
RETURNS @PriceBookRules table
(
	PriceBookPrice DECIMAL(20,5),
    tintDiscountTypeOriginal TINYINT,
    decDiscountOriginal DECIMAL(9,2),
	tintRuleType TINYINT
)                               
AS 
BEGIN
	DECLARE @ListPrice DECIMAL(20,5)
    DECLARE @PriceBookPrice DECIMAL(20,5)
    DECLARE @tintRuleType TINYINT
    DECLARE @tintPricingMethod TINYINT
    DECLARE @tintDiscountType TINYINT
    DECLARE @decDiscount DECIMAL(9,2)
	DECLARE @decDiscountOriginal DECIMAL(9,2)
    DECLARE @intQntyItems INT
    DECLARE @decMaxDedPerAmt DECIMAL(9,2)
--    DECLARE @intOperator INT


	SET @ListPrice =  @monPrice   
    SET @PriceBookPrice = 0
    SELECT  @tintRuleType = PB.[tintRuleType],
			@tintPricingMethod = PB.[tintPricingMethod],
			@tintDiscountType = PB.[tintDiscountType],
			@decDiscount = PB.[decDiscount],
			@decDiscountOriginal=PB.[decDiscount],
			@intQntyItems = PB.[intQntyItems],
			@decMaxDedPerAmt = PB.[decMaxDedPerAmt]
	FROM    [PriceBookRules] PB
			LEFT OUTER JOIN [PriceBookRuleDTL] PD ON PB.[numPricRuleID] = PD.[numRuleID]
	WHERE   [numPricRuleID] = @numRuleID
	
    IF ( @tintPricingMethod = 1 ) -- Price table
        BEGIN
				SET @intQntyItems = 1;
				
				SELECT TOP 1 @tintRuleType = [tintRuleType],
						@tintDiscountType = [tintDiscountType],
						@decDiscount = [decDiscount],
						@decDiscountOriginal=[decDiscount]
				FROM     [PricingTable]
				WHERE    [numPriceRuleID] = @numRuleID
						AND ISNULL(numCurrencyID,0) = 0
						AND @units BETWEEN [intFromQty] AND [intToQty]

				IF @tintRuleType = 2
				BEGIN
					SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)

					IF (@@ROWCOUNT=0)	
					BEGIN					
						INSERT INTO @PriceBookRules(PriceBookPrice,tintDiscountTypeOriginal,decDiscountOriginal)
						   Values (@monPrice,0,0)
						RETURN
					END
				END
	
				
				-- SELECT dbo.GetPriceBasedOnPriceBook(366,500,20)
				 IF ( @tintDiscountType = 1 ) -- Percentage 
                BEGIN
					
					SET @decDiscount = @monPrice * ( @decDiscount /100);

					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice =  (@monPrice - @decDiscount)  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
                END
               -- SELECT dbo.GetPriceBasedOnPriceBook(366,500,21)
				IF ( @tintDiscountType = 2 ) -- Flat discount 
					BEGIN
							
						IF ( @tintRuleType = 1 )
								SET @PriceBookPrice =  (@monPrice -   @decDiscount) ;
						IF ( @tintRuleType = 2 )
								SET @PriceBookPrice =  (@monPrice +  @decDiscount)  ;
					END
           
        END
        
    IF ( @tintPricingMethod = 2 ) -- Pricing Formula 
        BEGIN
			
			IF @tintRuleType = 2
			BEGIN
				SELECT @monPrice = dbo.[fn_GetVendorCost](@ItemCode)

				IF (@@ROWCOUNT=0)	
				BEGIN					
					INSERT INTO @PriceBookRules(PriceBookPrice,tintDiscountTypeOriginal,decDiscountOriginal)
						Values (@monPrice,0,0)
					RETURN
				END
			END
		  
    -- SELECT dbo.GetPriceBasedOnPriceBook(368,500,10)

            IF ( @tintDiscountType = 1 ) -- Percentage 
                BEGIN
					SET @decDiscount = @decDiscount * (@units/@intQntyItems);
					SET @decDiscount = @decDiscount * @monPrice /100;
--					RETURN @decDiscount
--					RETURN @decMaxDedPerAmt
					SET @decMaxDedPerAmt = @decMaxDedPerAmt * @monPrice /100;
					
					
					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					
					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice = @monPrice - @decDiscount  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = @monPrice + @decDiscount   ;
					
                END
            IF ( @tintDiscountType = 2 ) -- Flat discount 
                BEGIN
					SET @decDiscount = @decDiscount * (@units/@intQntyItems);
--					RETURN @decDiscount
--					RETURN @decMaxDedPerAmt

					IF (@decDiscount > @decMaxDedPerAmt)
						SET @decDiscount = @decMaxDedPerAmt;
					
					IF ( @tintRuleType = 1 )
							SET @PriceBookPrice =  (@monPrice -   @decDiscount)  ;
					IF ( @tintRuleType = 2 )
							SET @PriceBookPrice = (@monPrice +  @decDiscount)  ;
                END
			
        END

		If @tintRuleType = 2
		BEGIN
			IF @tintDiscountType = 1 -- Percentage
			BEGIN
				If @PriceBookPrice > @ListPrice
					SET @decDiscountOriginal = 0
				ELSE
					If @ListPrice > 0
						SET @decDiscountOriginal = ((@ListPrice - @PriceBookPrice) * 100) / CAST(@ListPrice AS FLOAT)
					ELSE
						SET @decDiscountOriginal = 0
			END
			ELSE IF @tintDiscountType = 2 -- Flat Amount
			BEGIN
				If @PriceBookPrice > @ListPrice
					SET @decDiscountOriginal = 0
				ELSE
					SET @decDiscountOriginal = @ListPrice - @PriceBookPrice
			END
		END


    INSERT INTO @PriceBookRules(PriceBookPrice,tintDiscountTypeOriginal,decDiscountOriginal,tintRuleType)
					Values (@PriceBookPrice,@tintDiscountType,@decDiscountOriginal,@tintRuleType)
	Return

   END
