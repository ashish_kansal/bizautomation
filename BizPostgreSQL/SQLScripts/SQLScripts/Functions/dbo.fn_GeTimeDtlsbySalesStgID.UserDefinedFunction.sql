/****** Object:  UserDefinedFunction [dbo].[fn_GeTimeDtlsbySalesStgID]    Script Date: 07/26/2008 18:12:38 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getimedtlsbysalesstgid')
DROP FUNCTION fn_getimedtlsbysalesstgid
GO
CREATE FUNCTION [dbo].[fn_GeTimeDtlsbySalesStgID](@numOppID numeric, @numOppStageID numeric,@tintType tinyint)          
 RETURNS varchar(100)           
AS          
--Thsi function will take the ID fo rthe Territory or the List Details and return the corresponding name.          
BEGIN          
 DECLARE @vcRetValue varchar (250)          
           
          
      
select       
@vcRetValue=         
      
convert(varchar(100),convert(varchar(100),convert(decimal(10,1),(convert(decimal(10,1),datediff(minute,dtFromDate,dtToDate))/60))))     
+' ( R/H '+case when numType=1 then convert(varchar(100),monamount) else '0.00' end+')'         
from  timeandexpense    
where numOppID=@numOppID and numStageID=@numOppStageID         
 and numCategory = 1 and tintTEType = 1    
          
          
 RETURN @vcRetValue           
END
GO
