/****** Object:  UserDefinedFunction [dbo].[fn_GetContactNameFromUserID]    Script Date: 07/26/2008 18:12:32 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcontactnamefromuserid')
DROP FUNCTION fn_getcontactnamefromuserid
GO
CREATE FUNCTION [dbo].[fn_GetContactNameFromUserID](@numUserID Numeric)  
 RETURNS varchar(1000) 
BEGIN  
 DECLARE @RetVal varchar(1000)  
 select @RetVal=vcFirstName+' '+vcLastName from AdditionalContactsInformation
 join usermaster on numUserDetailId=numContactID
 where numuserid=@numuserID 
 RETURN isnull(@RetVal,'-')  
END
GO
