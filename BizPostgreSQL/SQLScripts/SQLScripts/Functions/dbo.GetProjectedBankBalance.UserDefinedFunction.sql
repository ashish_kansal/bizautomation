/****** Object:  UserDefinedFunction [dbo].[GetProjectedBankBalance]    Script Date: 07/26/2008 18:13:09 ******/

GO

GO
--Created By Siva                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getprojectedbankbalance')
DROP FUNCTION getprojectedbankbalance
GO
CREATE FUNCTION [dbo].[GetProjectedBankBalance](@intType int,@numBudgetId numeric(9),@Month int,@Year int,@numDomainId numeric(9),@numUserCntID numeric(9),@sintForecast smallint,@Id as numeric(9))                      
Returns DECIMAL(20,5)                      
As                      
Begin                      
--                      
--Declare @monCashInflowAmt as DECIMAL(20,5)                      
--Declare @monSalesForeCastAmt as DECIMAL(20,5)                      
--Declare @monDebitBankAmt as DECIMAL(20,5)                      
--Declare @monCreditBankAmt as DECIMAL(20,5)                      
--Declare @monCashOnHand as DECIMAL(20,5)                      
--Declare @monProjectedBankBalance as DECIMAL(20,5)             
--Declare @TotalAmount as DECIMAL(20,5)       
--Declare @currentYear as int       
--      
--         
--Declare @monOperBudgetAmt as DECIMAL(20,5)                
--Declare @monProBudgetAmt as DECIMAL(20,5)                
--Declare @monMarkBudgetAmt as DECIMAL(20,5)       
--Set @monOperBudgetAmt=0      
--Set @monProBudgetAmt=0      
--Set @monMarkBudgetAmt=0      
--Set @TotalAmount=0      
--                    
--Set @monCashInflowAmt = dbo.GetCashInflowForMonthsForProjectedBankBalance(@intType,@Month,@Year,@numDomainId)                      
--Set @monSalesForeCastAmt=dbo.GetSalesForeCastForMonthsForProjectedBankBalance(@intType,@Month,@Year,@numDomainId,@numUserCntID,@sintForecast)                      
--
--if @intType=0 
--	Set @currentYear=year(getutcdate())
--Else If @intType=-1
--	Set @currentYear=year(getutcdate())-1
--Else If @intType=1
--	Set @currentYear=year(getutcdate())+1
--
--
-- --For Operation Budget    
-- Select @monOperBudgetAmt=sum(isnull(monAmount,0))From OperationBudgetMaster OBM    
-- inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId    
--where  OBM.numDomainId=@numDomainId and ((OBD.tintMonth between month(getutcdate()) And (case when @currentYear=@year then @Month else 0 end) And OBD.intYear=@year)
--or (OBD.tintMonth between 1 and (case when @currentYear< @year then  @Month else 0 end) and OBD.intYear=@year) Or (OBD.tintMonth between month(getutcdate()) and (case when @currentYear< @year then  12 else 0 end)  And OBD.intYear=@year-1))
--
----For Procurement Budget    
-- Select @monProBudgetAmt=sum(isnull(monAmount,0))From ProcurementBudgetMaster PBM    
-- inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId    
-- where  PBM.numDomainId=@numDomainId and ( ( PBD.tintMonth between month(getutcdate()) And (case when @currentYear=@year then @Month else 0 end) And PBD.intYear=@year)
-- or (PBD.tintMonth between 1 and (case when @currentYear< @year then  @Month else 0 end) and PBD.intYear=@year) Or (PBD.tintMonth between month(getutcdate()) and (case when @currentYear< @year then  12 else 0 end)  And PBD.intYear=@year-1))
-- 
----For Market Budget         
--   Select @monMarkBudgetAmt=sum(isnull(monAmount,0))From MarketBudgetMaster MBM    
--   inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId    
--  where   MBM.numDomainId=@numDomainId and ((MBD.tintMonth between month(getutcdate()) And (case when @currentYear=@year then @Month else 0 end) And MBD.intYear=@year)
-- or (MBD.tintMonth between 1 and (case when @currentYear< @year then  @Month else 0 end) and MBD.intYear=@year) Or (MBD.tintMonth between month(getutcdate()) and (case when @currentYear< @year then  12 else 0 end)  And MBD.intYear=@year-1))
--
--  
--   Set @TotalAmount =isnull(@monOperBudgetAmt,0)+isnull(@monProBudgetAmt,0)+isnull(@monMarkBudgetAmt,0)      
--      
----Cash on Hand                       
-- Select @monDebitBankAmt=sum(isnull(GJD.numDebitAmt,0)), @monCreditBankAmt=sum(isnull(GJD.numCreditAmt,0)) From General_Journal_Header GJH                        
-- inner join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId                        
-- inner join Chart_of_Accounts CA on GJD.numChartAcntId = CA.numAccountId                         
-- Where GJH.numDomainId=@numDomainId And CA.numAcntType = 813 And datediff(day,getutcdate(),GJH.datEntry_Date)<=0 And Year(GJH.datEntry_Date)=Year(getutcdate())            
--          
--                
--Set @monCashOnHand = isnull(@monDebitBankAmt,0)-isnull(@monCreditBankAmt,0)                      
--Set @monProjectedBankBalance =isnull(@monCashOnHand,0) + isnull(@monCashInflowAmt,0) + isnull(@monSalesForeCastAmt,0) - isnull(@TotalAmount,0)                      
--                    
Return 0--@monProjectedBankBalance                      
                      
End
GO
