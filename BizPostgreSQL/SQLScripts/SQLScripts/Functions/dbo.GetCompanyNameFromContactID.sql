
GO
/****** Object:  UserDefinedFunction [dbo].[GetCompanyNameFromContactID]    Script Date: 02/28/2009 14:08:53 ******/

GO

GO
-- =============================================
-- Author:		Pratik Vasani
-- Create date: 13/DEC/2008
-- Description:	This function gets the Company Name from ContactID
-- =============================================
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcompanynamefromcontactid')
DROP FUNCTION getcompanynamefromcontactid
GO
CREATE FUNCTION [dbo].[GetCompanyNameFromContactID] 
(
	-- Add the parameters for the function here
	@numContactId numeric(9,0),
	@numDomainId numeric(9,0)
)
RETURNS VARCHAR(30)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result VARCHAR(30)

	-- Add the T-SQL statements to compute the return value here

SELECT    @Result= CompanyInfo.vcCompanyName
FROM         DivisionMaster INNER JOIN
                      CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId INNER JOIN
                      AdditionalContactsInformation ON DivisionMaster.numDivisionID = AdditionalContactsInformation.numDivisionId
WHERE     (AdditionalContactsInformation.numContactId = @numContactId) and AdditionalContactsInformation.numDomainID=@numDomainId
	-- Return the result of the function
	RETURN @Result

END
