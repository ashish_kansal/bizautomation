GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_StripHTML')
DROP FUNCTION fn_StripHTML
GO

CREATE FUNCTION fn_StripHTML
(@HTMLText VARCHAR(MAX))
RETURNS VARCHAR(MAX)
AS
BEGIN
DECLARE @Start INT
DECLARE @End INT
DECLARE @Length INT

SET @Start = CHARINDEX('<!--',@HTMLText)
SET @End = CHARINDEX('-->',@HTMLText,CHARINDEX('<!--',@HTMLText))
SET @Length = (@End - @Start) + 3
WHILE @Start > 0 AND @End > 0 AND @Length > 0
BEGIN
	SET @HTMLText = STUFF(@HTMLText,@Start,@Length,'')
	SET @Start = CHARINDEX('<!--',@HTMLText)
	SET @End = CHARINDEX('-->',@HTMLText,CHARINDEX('<!--',@HTMLText))
	SET @Length = (@End - @Start) + 3
END

SET @Start = CHARINDEX('<',@HTMLText)
SET @End = CHARINDEX('>',@HTMLText,CHARINDEX('<',@HTMLText))
SET @Length = (@End - @Start) + 1
WHILE @Start > 0 AND @End > 0 AND @Length > 0
BEGIN
	SET @HTMLText = STUFF(@HTMLText,@Start,@Length,'')
	SET @Start = CHARINDEX('<',@HTMLText)
	SET @End = CHARINDEX('>',@HTMLText,CHARINDEX('<',@HTMLText))
	SET @Length = (@End - @Start) + 1
END

RETURN LTRIM(RTRIM(REPLACE(@HTMLText,'&nbsp;','')))
END
GO