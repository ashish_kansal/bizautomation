

go
/* Note: applied in tintType = 4 and 8
Performace Report:The "Based On" drop-down shows 30, 60, 90, days etc... but when we display cost it's always for 1 month (assuming we're talking about a monthly campaign), 
we're not multiplying cost x 2 if 60 days is selected, etc.. (60 = x 2  90 x 3, 120 = x 4, 240 = x 8, 360 = x 12)  Idea - Why don't we just have a check box 
below the "Cost" field in campaign details called "Monthly Campaign", which if checked multiplies the cost value for the campaign performance when multiple months are slected as mentioned.
*/

--select dbo.GetIncomeforCampaign(1,1)              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getincomeforcampaign')
DROP FUNCTION getincomeforcampaign
GO
CREATE FUNCTION [dbo].[GetIncomeforCampaign](@numCampaignid numeric(9),@tintType TINYINT,@dtStartDate DATETIME  = NULL,@dtEndDate   DATETIME  = NULL)              
 RETURNS DECIMAL(20,5)
AS              
Begin
--declare @intLaunchDate as varchar(20)              
--declare @intEndDate as varchar(20)              
declare @amount as DECIMAL(20,5)              
declare @monCampaignCost as DECIMAL(20,5)             
Declare @oppTime as DECIMAL(20,5)            
Declare @ServiceCost as DECIMAL(20,5)             
Declare @TotalIncome as DECIMAL(20,5)            
--DECLARE @numDomainID AS NUMERIC
--DECLARE @Val INT
--SELECT @Val = ROUND(CONVERT(float,DATEDIFF(day,@dtStartDate,@dtEndDate))/30,0)--		SELECT ROUND(CONVERT(float,75)/30,0)
--IF ISNULL(@Val,0) = 0 
--	SET @Val = 1
            
select top 1 
--@intLaunchDate=intLaunchDate,              
--@intEndDate=intEndDate,              
--@monCampaignCost=monCampaignCost
@monCampaignCost = (SELECT SUM(monAmount) FROM dbo.BillDetails BD JOIN dbo.BillHeader BH ON BD.numBillID = BH.numBillID AND numCampaignId = CampaignMaster.numCampaignId GROUP BY numCampaignId)
--@numDomainID =numDomainID              
from campaignmaster              
where numCampaignid=@numCampaignid              
	

If @tintType=1 ---Total Income potential -> the sum total of all sales opportunities that come from the organization tied to the campaign.              
Begin              
  select @amount=sum(opp.monDealAmount) from OpportunityMaster  Opp              
  where 
--  opp.bintCreatedDate > @intLaunchDate and opp.bintCreatedDate < @intEndDate              
	opp.tintOppType = 1
	AND opp.tintOppStatus = 0
	AND opp.numCampainID=@numCampaignid
--  AND Opp.numDivisionID IN (
----  SELECT distinct DM.[numDivisionID] FROM [DivisionMaster] dm JOIN [OpportunityMaster] Opp ON dm.[numDivisionID] = Opp.[numDivisionId]
----  WHERE DM.tintCRMType= 2 AND  DM.[numCampaignID] = @numCampaignid
--  SELECT DISTINCT DM.[numDivisionID] FROM    [OpportunityMaster] Opp
--  JOIN [DivisionMaster] dm ON dm.[numDivisionID] = Opp.[numDivisionId]
--  LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
--  WHERE 1=1
--  --AND DM.tintCRMType=2 
--  AND (CAMP.numCampaignID = @numCampaignid OR DM.numCampaignID = @numCampaignid)
--  )
End              
If @tintType=2 ---Total Income 
Begin              
  select @amount= sum(opp.monDealAmount) from OpportunityMaster  Opp              
  where 
  opp.tintOppType = 1              
  and opp.tintOppStatus=1  
  AND opp.numCampainID=@numCampaignid
--  AND Opp.numDivisionID IN (            
--  SELECT DISTINCT DM.[numDivisionID] FROM    [OpportunityMaster] Opp
--  JOIN [DivisionMaster] dm ON dm.[numDivisionID] = Opp.[numDivisionId]
--  LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
--  WHERE 1=1
--  --AND DM.tintCRMType=2 
--  AND (CAMP.numCampaignID = @numCampaignid AND DM.numCampaignID = @numCampaignid))
  --AND (Opp.numCampainID = @numCampaignid)
  --Altered by chintan : Carl suggested to count amount based on Organizarion's linking to Campaign instead of Each sales order
End              
If @tintType=3 ---Cost to Mfg. Deal              
Begin
  Declare @NoofClosedDeals as integer              
  Declare @CostofGoodsSold as DECIMAL(20,5)            
  declare @CostofLabor as DECIMAL(20,5)            
  select @NoofClosedDeals=count(*) from OpportunityMaster  Opp              
  where 
   opp.tintOppType= 1              
  and opp.tintOppStatus=1   
  AND opp.numCampainID=@numCampaignid
--   AND Opp.numDivisionID IN (            
--  SELECT DISTINCT DM.[numDivisionID] FROM    [OpportunityMaster] Opp
--  JOIN [DivisionMaster] dm ON dm.[numDivisionID] = Opp.[numDivisionId]
--  LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
--  WHERE (CAMP.numCampaignID = @numCampaignid OR DM.numCampaignID = @numCampaignid)
--  )
              
  --Cost of Goods Sold            
  Select @CostofGoodsSold=Sum(oppIt.numUnitHour * (CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE i.monAverageCost END)) From             
  OpportunityItems oppIt              
  Join opportunityMaster Opp on oppIt.numOppId=Opp.numOppId            
  left join item i on oppIt.numItemCode=i.numItemCode               
  left join ListDetails L on i.numItemClassification=L.numListItemID            
  Where Opp.tintOppType= 1 And opp.tintOppStatus=1 
   And (i.charItemType='P' or i.charItemType='A') 
   AND opp.numCampainID=@numCampaignid
--AND Opp.numDivisionID IN (            
----  SELECT distinct DM.[numDivisionID] FROM [DivisionMaster] dm JOIN [OpportunityMaster] Opp ON dm.[numDivisionID] = Opp.[numDivisionId]
----  WHERE DM.[numCampaignID] = @numCampaignid
--  SELECT DISTINCT DM.[numDivisionID] FROM    [OpportunityMaster] Opp
--  JOIN [DivisionMaster] dm ON dm.[numDivisionID] = Opp.[numDivisionId]
--  LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
--  WHERE (CAMP.numCampaignID = @numCampaignid OR DM.numCampaignID = @numCampaignid)
--  ) 
             
  --Cost of Labor            
  select @oppTime=case when Sum((convert(decimal(10,2),cm.numamount - (dbo.GetContractRemainingAmount(ot.numcontractId,Opp.numDivisionId,Opp.numDomainId)-convert(decimal(10,2),datediff(minute,dtfromdate,dttodate))*monamount/60))))>=0              
   then 0 else Sum((convert(decimal(10,2),cm.numamount - (dbo.GetContractRemainingAmount(ot.numcontractId,Opp.numDivisionId,Opp.numDomainId)-convert(decimal(10,2),datediff(minute,dtfromdate,dttodate))*monamount/60))))*-1 end            
  from timeandexpense OT               
  join opportunitymaster Opp on OT.numoppid=Opp.numoppid               
  join contractmanagement cm on ot.numcontractid = cm.numcontractid             
  Where ot.numcategory =1 and ot.numtype =1 and Opp.tintOppType= 1 And Opp.tintOppStatus=1 
  AND opp.numCampainID=@numCampaignid
--AND Opp.numDivisionID IN (            
----  SELECT distinct DM.[numDivisionID] FROM [DivisionMaster] dm JOIN [OpportunityMaster] Opp ON dm.[numDivisionID] = Opp.[numDivisionId]
----  WHERE DM.[numCampaignID] = @numCampaignid
--  SELECT DISTINCT DM.[numDivisionID] FROM    [OpportunityMaster] Opp
--  JOIN [DivisionMaster] dm ON dm.[numDivisionID] = Opp.[numDivisionId]
--  LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
--  WHERE (CAMP.numCampaignID = @numCampaignid OR DM.numCampaignID = @numCampaignid)
--  )
            
  --For Service Item            
  Select @ServiceCost=sum(oppIt.monTotAmount*isnull(i.monCampaignLabourCost,0)/100) From             
  OpportunityItems oppIt              
  Join opportunityMaster Opp on oppIt.numOppId=Opp.numOppId            
  left join item i on oppIt.numItemCode=i.numItemCode               
  left join ListDetails L on i.numItemClassification=L.numListItemID            
  Where Opp.tintOppType= 1 And Opp.tintOppStatus=1 
  And i.charItemType='S' 
  AND opp.numCampainID=@numCampaignid
--AND Opp.numDivisionID IN (            
----  SELECT distinct DM.[numDivisionID] FROM [DivisionMaster] dm JOIN [OpportunityMaster] Opp ON dm.[numDivisionID] = Opp.[numDivisionId]
----  WHERE DM.[numCampaignID] = @numCampaignid
--  SELECT DISTINCT DM.[numDivisionID] FROM    [OpportunityMaster] Opp
--  JOIN [DivisionMaster] dm ON dm.[numDivisionID] = Opp.[numDivisionId]
--  LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
--  WHERE (CAMP.numCampaignID = @numCampaignid OR DM.numCampaignID = @numCampaignid)
--  )  
             
  --Set @CostofLabor=convert(DECIMAL(20,5),isnull(@oppTime,0))+convert(DECIMAL(20,5),isnull(@ServiceCost,0))            
             
  If @NoofClosedDeals>0              
  Begin              
   --Set @amount=(convert(DECIMAL(20,5),isnull(@monCampaignCost,0))+convert(DECIMAL(20,5),isnull(@CostofGoodsSold,0))+convert(DECIMAL(20,5),isnull(@CostofLabor,0)))/@NoofClosedDeals              
   Set @amount = (convert(DECIMAL(20,5),isnull(@monCampaignCost,0)))/@NoofClosedDeals              
  End              
  Else Set @amount=0              
               
End              
If @tintType=4 ---ROI              
Begin              
 --Cost of Goods Sold            
 --Select @CostofGoodsSold =ISNULL(dbo.GetIncomeforCampaign(@numCampaignid,5,@dtStartDate,@dtEndDate),0)
  --Cost of Labor            
 --select @CostofLabor=ISNULL(dbo.GetIncomeforCampaign(@numCampaignid,6,@dtStartDate,@dtEndDate),0)
 -- Total Income            
 Select @TotalIncome= ISNULL(dbo.GetIncomeforCampaign(@numCampaignid,2,@dtStartDate,@dtEndDate),0)
 --SET @amount=convert(DECIMAL(20,5),isnull(@CostofGoodsSold,0))+convert(DECIMAL(20,5),isnull(@CostofLabor,0))+convert(DECIMAL(20,5),isnull(@TotalIncome,0))-convert(DECIMAL(20,5),isnull(@monCampaignCost,0))
 SET @amount=convert(DECIMAL(20,5),isnull(@TotalIncome,0))-convert(DECIMAL(20,5),isnull(@monCampaignCost,0))
 
/*
	SET @amount=convert(DECIMAL(20,5),isnull(@CostofGoodsSold,0))+convert(DECIMAL(20,5),isnull(@CostofLabor,0))+convert(DECIMAL(20,5),isnull(@TotalIncome,0))-convert(DECIMAL(20,5),isnull(@monCampaignCost,0))
	SET @amount = @amount * @Val;
*/
End              
            
If @tintType=5 -- COGS       
Begin            
 Select @amount=Sum(oppIt.numUnitHour*(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE i.monAverageCost END)) From             
 OpportunityItems oppIt              
 Join opportunityMaster Opp on oppIt.numOppId=Opp.numOppId            
 left join item i on oppIt.numItemCode=i.numItemCode               
 left join ListDetails L on i.numItemClassification=L.numListItemID            
 Where Opp.tintOppType= 1 And Opp.tintOppStatus=1 
 And (i.charItemType='P' or i.charItemType='A') 
 AND opp.numCampainID=@numCampaignid
-- AND Opp.numDivisionID IN (            
----  SELECT distinct DM.[numDivisionID] FROM [DivisionMaster] dm JOIN [OpportunityMaster] Opp ON dm.[numDivisionID] = Opp.[numDivisionId]
----  WHERE DM.[numCampaignID] = @numCampaignid
--  SELECT DISTINCT DM.[numDivisionID] FROM    [OpportunityMaster] Opp
--  JOIN [DivisionMaster] dm ON dm.[numDivisionID] = Opp.[numDivisionId]
--  LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
--  WHERE (CAMP.numCampaignID = @numCampaignid OR DM.numCampaignID = @numCampaignid)
--  )     
End            
             
if @tintType=6 -- Cost of Labor            
Begin            
   Select @oppTime= case when Sum((convert(decimal(10,2),cm.numamount - (dbo.GetContractRemainingAmount(ot.numcontractId,Opp.numDivisionId,Opp.numDomainId)-convert(decimal(10,2),datediff(minute,dtfromdate,dttodate))*monamount/60))))>=0              
  then 0 else Sum((convert(decimal(10,2),cm.numamount - (dbo.GetContractRemainingAmount(ot.numcontractId,Opp.numDivisionId,Opp.numDomainId)-convert(decimal(10,2),datediff(minute,dtfromdate,dttodate))*monamount/60))))*-1 end            
 From timeandexpense OT              
 join opportunitymaster Opp on OT.numoppid=Opp.numoppid               
 join contractmanagement cm on ot.numcontractid = cm.numcontractid             
 Where ot.numCategory =1 and ot.numType =1 and Opp.tintOppType= 1 And Opp.tintOppStatus=1 
 AND opp.numCampainID=@numCampaignid
-- AND Opp.numDivisionID IN (            
----  SELECT distinct DM.[numDivisionID] FROM [DivisionMaster] dm JOIN [OpportunityMaster] Opp ON dm.[numDivisionID] = Opp.[numDivisionId]
----  WHERE DM.[numCampaignID] = @numCampaignid
--  SELECT DISTINCT DM.[numDivisionID] FROM    [OpportunityMaster] Opp
--  JOIN [DivisionMaster] dm ON dm.[numDivisionID] = Opp.[numDivisionId]
--  LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
--  WHERE (CAMP.numCampaignID = @numCampaignid OR DM.numCampaignID = @numCampaignid)
--  ) 
            
 --For Service Item            
 Select @ServiceCost=sum(oppIt.monTotAmount*isnull(monCampaignLabourCost,0)/100) From             
 OpportunityItems oppIt              
 Join opportunityMaster Opp on oppIt.numOppId=Opp.numOppId            
 left join item i on oppIt.numItemCode=i.numItemCode               
 left join ListDetails L on i.numItemClassification=L.numListItemID            
 Where Opp.tintOppType= 1 And Opp.tintOppStatus=1 
 And i.charItemType='S' 
 AND opp.numCampainID=@numCampaignid
-- AND Opp.numDivisionID IN (            
----  SELECT distinct DM.[numDivisionID] FROM [DivisionMaster] dm JOIN [OpportunityMaster] Opp ON dm.[numDivisionID] = Opp.[numDivisionId]
----  WHERE DM.[numCampaignID] = @numCampaignid
--  SELECT DISTINCT DM.[numDivisionID] FROM    [OpportunityMaster] Opp
--  JOIN [DivisionMaster] dm ON dm.[numDivisionID] = Opp.[numDivisionId]
--  LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
--  WHERE (CAMP.numCampaignID = @numCampaignid OR DM.numCampaignID = @numCampaignid)
--  )
             
 Set @amount=convert(DECIMAL(20,5),isnull(@oppTime,0))+convert(DECIMAL(20,5),isnull(@ServiceCost,0))            
End   


return round(isnull(@amount,0),2)              
            
End

