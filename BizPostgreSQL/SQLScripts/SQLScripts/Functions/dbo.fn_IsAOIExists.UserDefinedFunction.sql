/****** Object:  UserDefinedFunction [dbo].[fn_IsAOIExists]    Script Date: 07/26/2008 18:12:48 ******/

GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_isaoiexists')
DROP FUNCTION fn_isaoiexists
GO
CREATE FUNCTION [dbo].[fn_IsAOIExists] (@vcContactAOIList varchar(250), @vcAoiList varchar(250))
	RETURNS bit 
AS
BEGIN
	DECLARE @intFirstComma integer
	DECLARE @intNextComma integer
	DECLARE @bintValue bigint
	DECLARE @RetVal bit

	SET @RetVal=0

	IF @vcAOIList=''
	BEGIN
		RETURN 1
	END
	SET @intFirstComma=CHARINDEX(',', @vcAoiList, 0)
	SET @intNextComma=CHARINDEX(',', @vcAoiList, @intFirstComma + 1)
	IF @intNextComma=0
	BEGIN
		SET @bintValue=SUBSTRING(@vcAoiList,@intFirstComma+1,LEN(@vcAoiList)-@intFirstComma)
		IF @bintValue>0
		BEGIN
			IF CHARINDEX(',' + CAST(@bintValue AS varchar) + ',', @vcContactAOIList, 0)>0 
			BEGIN
				SET @RetVal=1
				RETURN @RetVal
			END
		END

		SET @intFirstComma=CHARINDEX(',', @vcAoiList, @intNextComma)
	END
	
	WHILE @intFirstComma>0
	BEGIN
		IF @intNextComma=0
		BEGIN
			SET @bintValue=SUBSTRING(@vcAoiList,@intFirstComma+1,LEN(@vcAoiList)-@intFirstComma)
			IF @bintValue>0
			BEGIN
				IF CHARINDEX(',' + CAST(@bintValue AS varchar) + ',', @vcContactAOIList, 0)>0 
				BEGIN
					SET @RetVal=1
					RETURN @RetVal
				END
			END
			BREAK
		END
		ELSE
		BEGIN
			SET @bintValue=SUBSTRING(@vcAoiList,@intFirstComma+1,@intNextComma-@intFirstComma-1)
			IF @bintValue>0
			BEGIN
				IF CHARINDEX(',' + CAST(@bintValue AS varchar) + ',', @vcContactAOIList, 0)>0 
				BEGIN
					SET @RetVal=1
					RETURN @RetVal
				END
			END
		END

		SET @intFirstComma=CHARINDEX(',', @vcAoiList, @intNextComma)
		SET @intNextComma=CHARINDEX(',', @vcAoiList, @intFirstComma + 1)
	END

	RETURN @RetVal
END
GO
