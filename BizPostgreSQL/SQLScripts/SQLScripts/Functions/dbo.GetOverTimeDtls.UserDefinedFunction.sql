/****** Object:  UserDefinedFunction [dbo].[GetOverTimeDtls]    Script Date: 07/26/2008 18:13:08 ******/

GO

GO
--select dbo.GetOverTimeDtls(1,'1/1/2000','1/1/2007')
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getovertimedtls')
DROP FUNCTION getovertimedtls
GO
CREATE FUNCTION [dbo].[GetOverTimeDtls](@numUserID numeric(9),@dtFrom datetime,@dtTo datetime)
	RETURNS float 
AS
BEGIN
declare @fltOvrHours as float
declare @fltHours as float
declare @fltDailyHours as float
declare @bitOverTime as bit
declare @dtFrom1 as datetime
set @fltOvrHours=0
select @bitOverTime=bitOverTime ,@fltHours=numLimDailHrs from UserMaster where numUserID=@numUserID
	if @bitOverTime=1
	begin
	declare @Check  tinyint
	set @Check=0
		while @Check>0 

		begin
			--set @dtFrom1=dateadd(day,1,@dtFrom)
			--select @fltDailyHours=convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60
			--from TimeAndExpense where numUserID=@numUserID and numCategory=1 and numType=1 
			--and (dtFromDate between @dtFrom and @dtFrom1)
                        --if @fltDailyHours>@fltHours set @fltOvrHours=@fltOvrHours+@fltDailyHours-@fltHours
			--if @dtFrom=@dtTo set @Check=0
			--set @dtFrom=@dtFrom1
			set @fltOvrHours=1
                       set @Check=1
		end
	end
	RETURN @fltOvrHours
END
GO
