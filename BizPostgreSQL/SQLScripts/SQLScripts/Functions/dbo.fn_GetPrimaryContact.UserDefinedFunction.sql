/****** Object:  UserDefinedFunction [dbo].[fn_GetPrimaryContact]    Script Date: 07/26/2008 18:12:42 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getprimarycontact')
DROP FUNCTION fn_getprimarycontact
GO
CREATE FUNCTION [dbo].[fn_GetPrimaryContact](@numDivisionID Numeric)  
 RETURNS varchar(15) 
BEGIN  
 declare @contactID as varchar(15)

  select @contactID=isnull(numContactID,0) from AdditionalContactsInformation 
where  (ISNULL(bitPrimaryContact,0)=1
		    OR numContacttype IS NULL  
		   OR numContacttype=92)
and  numDivisionID=@numDivisionID
 
return @contactID
END
GO
