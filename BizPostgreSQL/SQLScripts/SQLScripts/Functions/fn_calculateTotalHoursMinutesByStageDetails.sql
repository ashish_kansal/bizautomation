
GO
--created by Prasanta Pradhan
--select dbo.fn_calculateTotalHoursMinutesByStageDetails(62055,717)

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_calculateTotalHoursMinutesByStageDetails')
DROP FUNCTION fn_calculateTotalHoursMinutesByStageDetails
GO
CREATE FUNCTION [dbo].[fn_calculateTotalHoursMinutesByStageDetails]
(
@numStageDetailsId As NUMERIC=0,
@numOppId As NUMERIC(18,0)=0,
@numProjectId As NUMERIC(18,0)=0
)
Returns VARCHAR(500) 
as
begin
DECLARE @totalDays AS NUMERIC(18,2)=0
DECLARE @totalHours AS NUMERIC(18,2)=0
DECLARE @totalMinutes AS NUMERIC(18,2)=0

SELECT @totalDays=MAX([Days]) FROM 
(SELECT ISNULL(numAssignTo,0) AS numAssignTo,
FLOOR(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0))/480) AS [Days],
(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0)) % 480)/60 AS [Hours],
(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0)) % 60) AS [Minutes]
FROM StagePercentageDetailsTask WHERE numStageDetailsId=@numStageDetailsId AND bitSavedTask=1 AND (numOppId=@numOppId OR numProjectId=@numProjectId)
GROUP BY numAssignTo) AS T

SELECT @totalHours=FLOOR(SUM([Hours])),@totalMinutes=FLOOR(SUM([Minutes])) FROM 
(SELECT ISNULL(numAssignTo,0) AS numAssignTo,
FLOOR(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0))/480) AS [Days],
(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0)) % 480)/60 AS [Hours],
(SUM((ISNULL(numHours,0)*60)+ISNULL(numMinutes,0)) % 60) AS [Minutes]
FROM StagePercentageDetailsTask WHERE numStageDetailsId=@numStageDetailsId AND bitSavedTask=1 AND (numOppId=@numOppId OR numProjectId=@numProjectId)
GROUP BY numAssignTo) AS T

RETURN CAST(@totalDays AS varchar)+'_'+CAST(@totalHours AS varchar)+'_'+CAST(@totalMinutes AS varchar)
end
GO
