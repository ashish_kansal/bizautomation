GO
--select dbo.GetDealAmount(1133,getdate(),1610)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetVendorPreferredMethodLeadTime')
DROP FUNCTION GetVendorPreferredMethodLeadTime
GO
CREATE FUNCTION [dbo].[GetVendorPreferredMethodLeadTime]
(
	@numVendorID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
)
RETURNS INT    
AS    
BEGIN   
	DECLARE @LeadTime AS INT = NULL

	IF EXISTS (SELECT * FROM VendorShipmentMethod WHERE numVendorID=@numVendorID AND ISNULL(numWarehouseID,0)=@numWarehouseID) 
	BEGIN
		SET @LeadTime = ISNULL((SELECT TOP 1 numListValue FROM VendorShipmentMethod WHERE numVendorID=@numVendorID AND ISNULL(numWarehouseID,0)=@numWarehouseID ORDER BY bitPrimary DESC,bitPreferredMethod DESC),0)
	END
	ELSE
	BEGIN
		SET @LeadTime = ISNULL((SELECT TOP 1 numListValue FROM VendorShipmentMethod WHERE numVendorID=@numVendorID ORDER BY bitPrimary DESC,bitPreferredMethod DESC),0)
	END

	RETURN ISNULL(@LeadTime,0)
END
GO