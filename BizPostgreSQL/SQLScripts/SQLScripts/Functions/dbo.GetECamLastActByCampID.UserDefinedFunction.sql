/****** Object:  UserDefinedFunction [dbo].[GetECamLastActByCampID]    Script Date: 07/26/2008 18:13:01 ******/

GO

GO
--select dbo.GetECamLastAct(1)
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getecamlastactbycampid')
DROP FUNCTION getecamlastactbycampid
GO
CREATE FUNCTION [dbo].[GetECamLastActByCampID](@numConECampID numeric)
returns varchar(100)
as
begin
declare @LastActivity as varchar(100)
select top 1 @LastActivity=vcDocName +', '+ convert(varchar(20),bintSentON) from ConECampaign Con 
join ConECampaignDTL CDTL 
on CDTL.numConECampID=Con.numConEmailCampID
join ECampaignDTLs EDTL
on CDTL.numECampDTLID= EDTL.numECampDTLId
join GenericDocuments G
on numGenericDocID=numEmailTemplate
where Con.numConEmailCampID=@numConECampID and bitSend=1 order by numConECampDTLID desc

return isnull(@LastActivity,'-')

end
GO
