GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetProjectedFinish')
DROP FUNCTION GetProjectedFinish
GO
CREATE FUNCTION [dbo].[GetProjectedFinish]
(
	@numDomainID NUMERIC(18,0)
	,@tintRecordType TINYINT --1: Item, 2: Workorder
	,@numRecordID NUMERIC(18,0)
	,@numQtyToBuild FLOAT
	,@numBuildProcessID NUMERIC(18,0)
	,@dtProjectedStartDate DATETIME
	,@ClientTimeZoneOffset INT
	,@numWarehouseID NUMERIC(18,0)
)    
RETURNS DATETIME    
AS
BEGIN
	DECLARE @dtPlannedStartDate DATETIME
	DECLARE @dtProjectedFinishDate DATETIME

	DECLARE @TempTaskAssignee TABLE
	(
		numAssignedTo NUMERIC(18,0)
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,numTaskAssignee NUMERIC(18,0)
		,intTaskType INT --1:Parallel, 2:Sequential
		,dtPlannedStartDate DATETIME
		,dtFinishDate DATETIME
	)
	
	IF @tintRecordType = 2
	BEGIN
		
		SELECT 
			@numQtyToBuild=numQtyItemsReq
			,@dtPlannedStartDate=dbo.GetWorkOrderPlannedStartDate(@numDomainID,WorkOrder.numWOId)
			--,@dtPlannedStartDate=ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) 
		FROM 
			WorkOrder 
		WHERE
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numWOId=@numRecordID

		IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID IN (SELECT SPDT.numTaskID FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=@numRecordID) AND tintAction=1)
		BEGIN
			SELECT @dtPlannedStartDate = MIN(SPDTTL.dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID IN (SELECT SPDT.numTaskID FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=@numRecordID) AND tintAction=1
		END

		SET @dtProjectedFinishDate = @dtPlannedStartDate

		INSERT INTO @TempTasks
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT 
			SPDT.numTaskID
			,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * @numQtyToBuild
			,ISNULL(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			WorkOrder WO
		ON
			SPDT.numWorkOrderId = WO.numWOId
		WHERE 
			SPDT.numDomainId=@numDomainID
			AND SPDT.numWorkOrderId = @numRecordID
	END
	ELSE
	BEGIN
		SET @dtPlannedStartDate = dbo.GetItemWOPlannedStartDate(@numDomainID
										,@numRecordID
										,@numWarehouseID
										,@numQtyToBuild
										,@dtProjectedStartDate
									)     
		SET @dtProjectedFinishDate = @dtPlannedStartDate

		INSERT INTO @TempTasks
		(
			numTaskID
			,numTaskTimeInMinutes
			,intTaskType
			,numTaskAssignee
		)
		SELECT 
			SPDT.numTaskID
			,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0))  * @numQtyToBuild
			,ISNULL(SPDT.intTaskType,1)
			,SPDT.numAssignTo
		FROM 
			StagePercentageDetails SPD
		INNER JOIN
			StagePercentageDetailsTask SPDT
		ON
			SPD.numStageDetailsId=SPDT.numStageDetailsId
		WHERE 
			SPD.numDomainId=@numDomainID
			AND SPD.slp_id=@numBuildProcessID
			AND ISNULL(SPDT.numWorkOrderId,0) = 0
	END

	UPDATE @TempTasks SET dtPlannedStartDate=@dtPlannedStartDate

	INSERT INTO @TempTaskAssignee
	(
		numAssignedTo
	)
	SELECT DISTINCT
		numTaskAssignee
	FROM
		@TempTasks

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numTempUserCntID NUMERIC(18,0)
	DECLARE @dtStartDate DATETIME
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @bitParallelStartSet BIT = 0

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTaskAssignee=numTaskAssignee
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay
			,@vcWorkDays=CONCAT(',',vcWorkDays,',')
			,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtPlannedStartDate),112)) + tmStartOfDay)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
		END
		ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numAssignedTo = @numTaskAssignee
		END
		ELSE IF @dtStartDate IS NULL
		BEGIN
			SET @dtStartDate = @dtPlannedStartDate
		END

		UPDATE @TempTasks SET dtPlannedStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0 AND ISNULL(@vcWorkDays,'') <> ''
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF CHARINDEX(CONCAT(',',DATEPART(WEEKDAY,@dtStartDate),','),@vcWorkDays) > 0 AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					IF CAST(@dtStartDate AS DATE) = CAST(GETUTCDATE() AS DATE)
					BEGIN
						-- CHECK TIME LEFT FOR DAY BASED
						SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))
					END
					ELSE
					BEGIN
						SET @numTimeLeftForDay = @numProductiveTimeInMinutes
					END

					IF @numTimeLeftForDay > 0
					BEGIN
						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END

						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
				END				
			END
		END	

		UPDATE @TempTasks SET dtFinishDate=@dtStartDate WHERE ID=@i 
		UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END

	IF (SELECT COUNT(*) FROM @TempTasks) > 0 
	BEGIN
		SELECT @dtProjectedFinishDate=MAX(dtFinishDate) FROM @TempTasks
	END

	RETURN DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtProjectedFinishDate)
END 
GO