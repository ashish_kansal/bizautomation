GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_CalOppItemTotalTaxAmt')
DROP FUNCTION fn_CalOppItemTotalTaxAmt
GO
CREATE FUNCTION [dbo].[fn_CalOppItemTotalTaxAmt]
(
@numDomainID AS NUMERIC(9),    
@numTaxItemID AS NUMERIC(9),    
@numOppID AS NUMERIC(9), 
@numOppBizDocID AS NUMERIC(9)
) 
RETURNS DECIMAL(20,5)
AS
BEGIN
	DECLARE @numOppItemID AS NUMERIC(9) 
	DECLARE @numOppBizDocItemID AS NUMERIC(9) 
	DECLARE @numItemCode AS NUMERIC(9)  
	DECLARE @numUnitHour AS FLOAT
	DECLARE @ItemAmount AS DECIMAL(20,5)    
	DECLARE @TotalTaxAmt AS DECIMAL(20,5) = 0 
	DECLARE @tintTaxOperator AS TINYINT = 0
	
	SELECT 
		@tintTaxOperator= ISNULL([tintTaxOperator],0) 
	FROM 
		[OpportunityMaster] 
	WHERE 
		numOppID=@numOppID

	DECLARE @numShippingServiceItemID AS NUMERIC(18,0)
	SELECT @numShippingServiceItemID = numShippingServiceItemID FROM Domain WHERE numDomainID = @numDomainID

	IF @numOppBizDocID=0
	BEGIN
		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
		IF @numTaxItemID = 0 and @tintTaxOperator = 1
		BEGIN
			SELECT 
				@TotalTaxAmt = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(OI.numUOMId, 0)) * ISNULL(numUnitHour,0),0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(monTotAmount,0) / 100 END,0)) 
			FROM 
				OpportunityItems OI
			INNER JOIN
				OpportunityMasterTaxItems OMTI
			ON
				OMTI.numOppId=OI.numOppId
				AND OMTI.numTaxItemID = 0
			JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				OI.numOppID=@numOppID  
				AND ISNULL(OI.numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)
		END	
		ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
		BEGIN
			SET @TotalTaxAmt = 0
		END
		ELSE
		BEGIN
			SELECT 
				@TotalTaxAmt = SUM(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(OI.numUOMId, 0)) * ISNULL(numUnitHour,0),0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(monTotAmount,0) / 100 END) 
			FROM 
				OpportunityItems OI
			INNER JOIN
				OpportunityItemsTaxItems OITI
			ON
				OI.numoppitemtCode=OITI.numOppItemID
				AND OITI.numOppId=@numOppID
			INNER JOIN
				OpportunityMasterTaxItems OMTI
			ON
				OMTI.numOppId=@numOppID
				AND OITI.numTaxItemID = OMTI.numTaxItemID
				AND ISNULL(OITI.numTaxID,0)=ISNULL(OMTI.numTaxID,0)
			JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				OI.numOppID=@numOppID  
				AND ISNULL(OI.numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)
				AND OITI.numTaxItemID=@numTaxItemID
		END
	END
	ELSE --IF BIZDoc
	BEGIN
		--- Skip tax applicable checking if tintTaxOperator =1 (Forcibly apply sales tax )
		IF @numTaxItemID = 0 and @tintTaxOperator = 1
		BEGIN
			SELECT 
				@TotalTaxAmt = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(OI.numUOMId, 0)) * ISNULL(OpportunityBizDocItems.numUnitHour,0),0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(OpportunityBizDocItems.monTotAmount,0) / 100 END,0)) 
			FROM 
				OpportunityBizDocItems
			JOIN
				OpportunityItems OI
			ON
				OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				OpportunityMasterTaxItems OMTI
			ON
				OMTI.numOppId=OI.numOppId
				AND OMTI.numTaxItemID = 0
			JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				numOppBizDocID=@numOppBizDocID  
				AND ISNULL(OpportunityBizDocItems.numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)  
				AND OI.numOppID=@numOppID  
		END	
		ELSE IF @numTaxItemID = 0 and @tintTaxOperator = 2
		BEGIN
			SET @TotalTaxAmt = 0
		END
		ELSE
		BEGIN
			SELECT 
				@TotalTaxAmt = SUM(ISNULL(CASE WHEN OMTI.tintTaxType = 2 THEN ISNULL(OMTI.fltPercentage,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(OI.numUOMId, 0)) * ISNULL(OpportunityBizDocItems.numUnitHour,0),0) ELSE  ISNULL(OMTI.fltPercentage,0) * ISNULL(OpportunityBizDocItems.monTotAmount,0) / 100 END,0)) 
			FROM 
				OpportunityBizDocItems
			JOIN
				OpportunityItems OI
			ON
				OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				OpportunityItemsTaxItems OITI
			ON
				OI.numoppitemtCode=OITI.numOppItemID
				AND OITI.numOppId=@numOppID
			INNER JOIN
				OpportunityMasterTaxItems OMTI
			ON
				OMTI.numOppId=@numOppID
				AND OITI.numTaxItemID = OMTI.numTaxItemID
				AND ISNULL(OITI.numTaxID,0)=ISNULL(OMTI.numTaxID,0)
			JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				numOppBizDocID=@numOppBizDocID  
				AND ISNULL(OpportunityBizDocItems.numItemCode,0) <> ISNULL(@numShippingServiceItemID,0)  
				AND OI.numOppID=@numOppID  
				AND OITI.numTaxItemID=@numTaxItemID
		END
	END

	RETURN ISNULL(@TotalTaxAmt,0)
end

