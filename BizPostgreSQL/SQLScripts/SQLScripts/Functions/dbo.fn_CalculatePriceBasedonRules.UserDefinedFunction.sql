/****** Object:  UserDefinedFunction [dbo].[fn_CalculatePriceBasedonRules]    Script Date: 07/26/2008 18:12:26 ******/

GO

GO
--created by anoop jayaraj
--select dbo.fn_CalculatePriceBasedonRules(3,3,50)

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_calculatepricebasedonrules')
DROP FUNCTION fn_calculatepricebasedonrules
GO
CREATE FUNCTION [dbo].[fn_CalculatePriceBasedonRules]
(@numRuleID as numeric(9)=0,
@intQuantity as integer=0,
@monListPrice DECIMAL(20,5)) returns DECIMAL(20,5)
as
begin
--	declare @monDiscprice as DECIMAL(20,5)
--	declare @tintRuleType as tinyint
--	declare @fltDedPercentage as float
--	declare @decflatAmount as decimal
--	declare @bitVolDisc as bit
--	declare @intQntyItems as integer
--	declare @decMaxDedPerAmt as decimal
--	declare @bitTillDate as bit
--	declare @dtTillDate as datetime
--	declare @factor as integer
--	--set @monListPrice=0
--
--	select @tintRuleType=tintRuleType,@fltDedPercentage=fltDedPercentage,@decflatAmount=decflatAmount,@bitVolDisc=bitVolDisc,
--	@intQntyItems=intQntyItems,@decMaxDedPerAmt=decMaxDedPerAmt,@bitTillDate=bitTillDate,@dtTillDate=dtTillDate from PriceBookRules
--	where numPricRuleID=@numRuleID
--
--
--	--if @decflatAmount>@monListPrice*@intQuantity
--	--set @decflatAmount=0
--
--	set @monListPrice=@monListPrice*@intQuantity
--    set @monDiscprice=@monListPrice
--
--	--if @tintRuleType=0 set @monDiscprice=@monDiscprice-(@monDiscprice*@fltDedPercentage)/100
--	--else if @tintRuleType=1 set @monDiscprice=@monDiscprice-@decflatAmount
--
--
--
--	if (@bitTillDate=1 and getutcdate()<=dateadd(day,1,@dtTillDate))
--	begin
--			if @bitVolDisc=1
--			begin
--				if @intQntyItems<>0
--      				begin
--						set @factor=@intQuantity/@intQntyItems
--						if @tintRuleType=0
--						begin
--							if @decMaxDedPerAmt >= @factor*@fltDedPercentage set @monDiscprice=@monListPrice-(@monListPrice*@factor*@fltDedPercentage/100)
--							else  set @monDiscprice=@monListPrice-@monListPrice*@decMaxDedPerAmt/100
--						end
--						else
--						begin
--							if @decMaxDedPerAmt >= @factor*@decflatAmount set @monDiscprice=@monListPrice-@factor*@decflatAmount
--							else  set @monDiscprice=@monListPrice-@decMaxDedPerAmt
--						end
--					end
--			end
--			else
--			begin
--				if @tintRuleType=0
--						begin
--							set @monDiscprice=@monListPrice-@monListPrice*@fltDedPercentage/100
--						end
--						else
--						begin
--							if @decflatAmount<=@monListPrice set @monDiscprice=@monListPrice-@decflatAmount
--						end
--			end
--
--	end
--	else if @bitTillDate=0
--	begin
--			if @bitVolDisc=1
--			begin
--				if @intQntyItems<>0
--      				begin
--						set @factor=@intQuantity/@intQntyItems
--						if @tintRuleType=0
--						begin
--							if @decMaxDedPerAmt >= @factor*@fltDedPercentage set @monDiscprice=@monListPrice-(@monListPrice*@factor*@fltDedPercentage/100)
--							else  set @monDiscprice=@monListPrice-@monListPrice*@decMaxDedPerAmt/100
--						end
--						else
--						begin
--							if @decMaxDedPerAmt >= @factor*@decflatAmount set @monDiscprice=@monListPrice-@factor*@decflatAmount
--							else  set @monDiscprice=@monListPrice-@decMaxDedPerAmt
--						end
--				end
--			end
--			else
--			begin
--				if @tintRuleType=0
--						begin
--							set @monDiscprice=@monListPrice-@monListPrice*@fltDedPercentage/100
--						end
--						else
--						begin
--							if @decflatAmount<=@monListPrice set @monDiscprice=@monListPrice-@decflatAmount
--						end
--			end
--	end
--if @intQuantity=0 set @intQuantity=1
--return isnull(@monDiscprice,0)/@intQuantity
RETURN 0
end
GO
