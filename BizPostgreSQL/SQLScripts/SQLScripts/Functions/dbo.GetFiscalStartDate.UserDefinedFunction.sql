/****** Object:  UserDefinedFunction [dbo].[GetFiscalStartDate]    Script Date: 07/26/2008 18:13:02 ******/

GO

GO
---select dbo.TO_FISCALPERIOD('10/05/2006')

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getfiscalstartdate')
DROP FUNCTION getfiscalstartdate
GO
CREATE FUNCTION [dbo].[GetFiscalStartDate](@Year as integer,@numDomainID numeric)
RETURNS datetime AS 
BEGIN 
 
	declare @dtFiscalStartDate as datetime
	declare @month as varchar(2)
	select @month=isnull(tintFiscalStartMonth,1) from Domain where numDomainID=@numDomainID
	set @dtFiscalStartDate='01/01/'+convert(varchar(4),@Year)
    set @dtFiscalStartDate=dateadd(month,@month-1,@dtFiscalStartDate)
RETURN @dtFiscalStartDate
END
GO
