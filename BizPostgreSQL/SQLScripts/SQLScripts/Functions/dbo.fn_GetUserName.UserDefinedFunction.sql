/****** Object:  UserDefinedFunction [dbo].[fn_GetUserName]    Script Date: 07/26/2008 18:12:48 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getusername')
DROP FUNCTION fn_getusername
GO
CREATE FUNCTION [dbo].[fn_GetUserName](@numuserID numeric)
Returns varchar(100) 
As
BEGIN
	DECLARE @RetName Varchar(20)
	select @RetName=vcUserName from usermaster  where numuserid=@numuserID
	RETURN @RetName
END
GO
