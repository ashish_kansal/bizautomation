
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckChildRecord')
DROP FUNCTION CheckChildRecord
GO
CREATE FUNCTION CheckChildRecord
(
	@numOppID AS NUMERIC(9)  = NULL,
    @numDomainID AS NUMERIC(9)  = 0
)
RETURNS NVARCHAR(MAX)
AS BEGIN
	
	DECLARE @ChildCount INT, @RecurringChildCount INT, @TotalCount INT

	SELECT @ChildCount = COUNT(distinct numChildOppID) 
		FROM [OpportunityLinking] OL 
			JOIN OpportunityMaster Opp  ON OL.[numParentOppID] = Opp.numOppId
		WHERE  
		Opp.numOppId = @numOppID
			AND 
			Opp.numDomainID = @numDomainID

	SELECT @RecurringChildCount = COUNT(*) 
		FROM [RecurringTransactionReport] RTR 
			JOIN OpportunityMaster Opp  ON RTR.[numRecTranSeedId] = Opp.numOppId
		WHERE  
		Opp.numOppId = @numOppID
			AND 
			Opp.numDomainID = @numDomainID

	SET @TotalCount = @ChildCount + @RecurringChildCount

	DECLARE @vcPoppName AS VARCHAR(MAX);	

	SELECT @vcPoppName = Opp.vcPoppName 
			FROM OpportunityMaster Opp         
			WHERE  
			Opp.numOppId = @numOppID
				AND 
				Opp.numDomainID = @numDomainID

	IF @TotalCount > 1
	BEGIN
		SET @vcPoppName =  CONCAT(@vcPoppName,' <a href=''#'' id=''anchChild'' onclick=''openChild(event,',@numOppID,')''><img src=''../images/GLReport.png'' style=''width: 20px; height: 20px;'' /></a>')
	END
	
    RETURN ISNULL(@vcPoppName,'')
END

GO


