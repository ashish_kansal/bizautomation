/****** Object:  UserDefinedFunction [dbo].[GetOverHrsWorked]    Script Date: 07/26/2008 18:13:08 ******/

GO

GO
--Created By Siva                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getoverhrsworked')
DROP FUNCTION getoverhrsworked
GO
CREATE FUNCTION [dbo].[GetOverHrsWorked](@bitOverTime bit,@bitOverTimeHrsDailyOrWeekly bit,@decOverTime decimal,@dtStartDate as datetime,@dtEndDate as datetime,@numUserCntID numeric,@numDomainId numeric,@ClientTimeZoneOffset int)                          
                        
 RETURNS decimal(10,2)                                                  
as                                                  
Begin                             
 --Declare @decOverTime as decimal(10,2)                          
 Declare @decTotalHrsWorked as decimal(10,2)                            
 Declare @decTotalOverHrsWorked as decimal(10,2)                             
 Declare @decTotalOverHrsWorked1 as decimal(10,2)                       
 Set @decTotalHrsWorked=0    
 Set   @decTotalOverHrsWorked=0    
Set   @decTotalOverHrsWorked1=0                 
 --Set  @decOverTime =12                           
                          
 if @bitOverTime=1                      
 Begin                      
  if @bitOverTimeHrsDailyOrWeekly=1                    
   Begin                    
                   
    --Set @decTotalHrsWorked=dbo.fn_GetTotalHrsWorked(@dtStartDate,@dtEndDate,@numUserCntID,@numDomainId)                  
     Select @decTotalHrsWorked=sum(x.Hrs) From (Select  isnull(Sum(Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)),0) as Hrs                
  from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1                     
  And (dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And @dtEndDate Or   
  dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate) And                 
  numUserCntID=@numUserCntID  And numDomainID=@numDomainId                 
  ) x        
   -- Set @decTotalOverHrsWorked= @decTotalHrsWorked-(@decOverTime*datediff(day,@dtStartDate,@dtendDate))            
       Set @decTotalOverHrsWorked= @decTotalHrsWorked-@decOverTime                
                   
   End     
                         
                    
                      
                           
  --print @decTotalOverHrsWorked                          
  If @decTotalOverHrsWorked>0                               
   Set @decTotalOverHrsWorked1=@decTotalOverHrsWorked                              
  Else                         
   Set @decTotalOverHrsWorked1=0                              
     End                       
 Return @decTotalOverHrsWorked1                           
 End
GO
