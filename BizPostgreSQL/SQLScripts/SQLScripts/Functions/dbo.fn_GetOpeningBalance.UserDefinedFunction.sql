     
/****** Object:  UserDefinedFunction [dbo].[fn_GetOpeningBalance]    Script Date: 10/05/2009 17:46:28 ******/

GO

GO
--DROP FUNCTION [dbo].[fn_GetOpeningBalance]
-- SELECT * FROM dbo.fn_GetOpeningBalance(',74,666,658,77,263,668,1156,1162,1166,1167,1170,1172,1174',72,'2009-01-01 00:00:00:000')
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_getopeningbalance')
DROP FUNCTION fn_getopeningbalance
GO
CREATE FUNCTION [dbo].[fn_GetOpeningBalance]
(@vcChartAcntId as varchar(500),
@numDomainId as numeric(9),
@dtFromDate as DATETIME,
@ClientTimeZoneOffset Int  --Added by Chintan to enable calculation of date according to client machine
)                                 
RETURNS 

@COAOpeningBalance table
(
	numAccountId int,
	vcAccountName varchar(100),
	numParntAcntTypeID int,
	vcAccountDescription varchar(100),
	vcAccountCode varchar(50),
	dtAsOnDate datetime,
	mnOpeningBalance DECIMAL(20,5)
)                               
AS                                  
BEGIN          
SET @dtFromDate = DateAdd(minute, -@ClientTimeZoneOffset, @dtFromDate);

	INSERT INTO 
		@COAOpeningBalance
	SELECT 
		COA.numAccountId
		,vcAccountName
		,numParntAcntTypeID
		,vcAccountDescription
		,vcAccountCode
		,@dtFromDate
		,(SELECT 
			ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
		FROM
			General_Journal_Details GJD 
		JOIN 
			Domain D 
		ON 
			D.numDOmainID = GJD.numDomainID 
		AND 
			COA.numDomainId = D.numDOmainID
		INNER JOIN
			General_Journal_Header GJH 
		ON 
			GJD.numJournalId =GJH.numJournal_Id 
			AND GJD.numDomainId=COA.numDomainId 
			AND GJD.numChartAcntId=COA.numAccountId 
			AND GJH.numDomainID = D.numDomainId 
			AND GJH.datEntry_Date <= @dtFromDate)
	FROM 
		Chart_of_Accounts COA
	WHERE 
		COA.numDomainId=@numDomainId 
		AND COA.numAccountId in (select CAST(ID AS NUMERIC(9)) from SplitIDs(@vcChartAcntId,','));

	RETURN 
END