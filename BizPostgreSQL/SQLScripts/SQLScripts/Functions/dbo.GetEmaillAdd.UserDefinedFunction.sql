/****** Object:  UserDefinedFunction [dbo].[GetEmaillAdd]    Script Date: 07/26/2008 18:13:01 ******/

GO

GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getemailladd')
DROP FUNCTION getemailladd
GO
CREATE FUNCTION [dbo].[GetEmaillAdd](@numEmailHSTRID as numeric,@tintType as tinyint)  
returns varchar(2000)  
as  
begin  
 DECLARE @EmailAdd VARCHAR(2000)  
 DECLARE @Name VARCHAR(50)  
 DECLARE @Email VARCHAR(50)  
 DECLARE @numEmailID AS NUMERIC(9)  
 SET @numEmailID = 0  
 SET @EmailAdd = ''  
 --@tintType=4 From 
 --@tintType=1 To
 select Distinct top 1 @numEmailID=EM.numEmailId,@Name=isnull(EHS.vcName,''),@Email=EM.vcEmailId from EmailHStrToBCCAndCC  EHS
join EmailMaster EM on em.numEmailid=  EHS.numEmailId
 where numEmailHstrID=@numEmailHSTRID and tintType=@tintType  

 while @numEmailID>0  
  begin  
   set @EmailAdd=@EmailAdd + Cast(@numEmailID AS VARCHAR) + '|' + @Name + '|' + @Email + '~'  
   
	select DISTINCT top 1  @numEmailID=EM.numEmailId,@Name=isnull(EHS.vcName,''),@Email=EM.vcEmailId from EmailHStrToBCCAndCC  EHS
	join EmailMaster EM on em.numEmailid=  EHS.numEmailId 
   where numEmailHstrID=@numEmailHSTRID and tintType=@tintType and EM.numEmailId>@numEmailID  

   if @@rowcount=0   
   begin  
    set @numEmailID=0  
    set @EmailAdd=substring(@EmailAdd,1,len(@EmailAdd)-1)  
   end 
 
  end  
   
 
--SELECT 
--    @EmailAdd = COALESCE(@EmailAdd + ',', '') + isnull(EHS.vcName,'')  + ' <' + EM.vcEmailId + '>'
-- from EmailHStrToBCCAndCC  EHS
--join EmailMaster EM on em.numEmailid=  EHS.numEmailId
-- where numEmailHstrID=@numEmailHSTRID and tintType=@tintType 
 
 return @EmailAdd  

--return (SELECT SUBSTRING(
--(SELECT ',' + isnull(EHS.vcName,'')  + ' <' + EM.vcEmailId + '>'
--from EmailHStrToBCCAndCC  EHS
--join EmailMaster EM on em.numEmailid=  EHS.numEmailId
--where numEmailHstrID=@numEmailHSTRID and tintType=@tintType 
--FOR XML PATH('')),2,200000) AS Email)
end
GO
