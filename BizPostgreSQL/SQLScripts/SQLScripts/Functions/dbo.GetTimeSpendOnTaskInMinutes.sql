GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetTimeSpendOnTaskInMinutes')
DROP FUNCTION GetTimeSpendOnTaskInMinutes
GO
CREATE FUNCTION [dbo].[GetTimeSpendOnTaskInMinutes]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)    
RETURNS INT  
AS
BEGIN
	DECLARE @numTotalMinutes INT = 0

	DECLARE @TEMTIMELong TABLE
	(
		ID INT IDENTITY(1,1)
		,tintAction TINYINT
		,dtActionTime DATETIME
	)

	INSERT INTO @TEMTIMELong
	(
		tintAction
		,dtActionTime
	)
	SELECT
		tintAction
		,dtActionTime
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		dtActionTime

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @tintLastAction TINYINT
	DECLARE @dtLastActionTime DATETIME

	DECLARE @tintAction TINYINT
	DECLARE @dtActionTime DATETIME
	SELECT @iCount = COUNT(*) FROM @TEMTIMELong

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@tintAction=tintAction
			,@dtActionTime=dtActionTime
		FROM
			@TEMTIMELong
		WHERE
			ID = @i

		IF @i = 1 
		BEGIN
			IF @tintAction <> 1
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE
			BEGIN
				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END
		ELSE
		BEGIN
			IF @tintLastAction = 1 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE IF @tintLastAction = 2 AND @tintAction <> 3
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE IF @tintLastAction = 3 AND @tintAction NOT IN (2,4)
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE IF @tintLastAction = 4
			BEGIN
				SET @numTotalMinutes = 0
				BREAK;
			END
			ELSE
			BEGIN
				IF @tintAction IN (2,4)
				BEGIN
					SET @numTotalMinutes = @numTotalMinutes + DATEDIFF(MINUTE,@dtLastActionTime,@dtActionTime)
				END

				SET @tintLastAction = @tintAction
				SET @dtLastActionTime = @dtActionTime
			END
		END

		SET @i = @i + 1
	END

	
	RETURN @numTotalMinutes
END
GO