/****** Object:  UserDefinedFunction [dbo].[fn_GetExpenseDtlsbyProStgID]    Script Date: 07/26/2008 18:12:36 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getexpensedtlsbyprostgid')
DROP FUNCTION fn_getexpensedtlsbyprostgid
GO
CREATE FUNCTION [dbo].[fn_GetExpenseDtlsbyProStgID] ( @numProID NUMERIC,
                                                        @numProStageID NUMERIC,
                                                        @tintType TINYINT )
RETURNS VARCHAR(100)
AS 
BEGIN    
    DECLARE @vcRetValue VARCHAR(100);SET @vcRetValue=0 
     
--Expenses =[ sum(item*rate) of PO linked with stage + Bills added to stage]

	IF @numProId>0
	BEGIN
	SELECT  @vcRetValue =CAST( ISNULL(SUM(X.ExpenseAmount),0) AS DECIMAL(10,2))
    FROM    ( SELECT    SUM(OPP.[monPrice] * OPP.[numUnitHour]) ExpenseAmount
              FROM      [OpportunityMaster] OM 
						JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
                        LEFT OUTER JOIN [Item] I ON OPP.[numItemCode] = I.[numItemCode]
                        LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
              WHERE     OPP.numProjectID = @numProId
						AND (OPP.numProjectStageID = @numProStageID OR @numProStageID =0)
						--AND PO.numStageID = @numProStageID
--                        AND OB.[bitAuthoritativeBizDocs] = 1
                        AND OM.[tintOppType] = 2
--                        AND I.[charItemType] = 'P'
--                        AND COA.[numAccountId] IS NOT NULL
              UNION
-- Bills expense
              SELECT    ISNULL(SUM(OBD.monamount), 0) ExpenseAmount
              FROM      [ProjectsOpportunities] PO
                        LEFT OUTER JOIN [OpportunityBizDocsDetails] OBD ON OBD.[numBizDocsPaymentDetId] = PO.numBillID
                        LEFT OUTER JOIN [Chart_Of_Accounts] COA ON OBD.[numExpenseAccount] = COA.[numAccountId]
              WHERE     PO.[numProId] = @numProId
						AND (PO.numStageID = @numProStageID OR @numProStageID =0)
					    and isnull(PO.numBillId,0)>0
						--AND PO.numStageID = @numProStageID
--                        AND COA.[numAccountId] IS NOT NULL
            ) X
     END

            

    
    RETURN @vcRetValue     
END
GO
