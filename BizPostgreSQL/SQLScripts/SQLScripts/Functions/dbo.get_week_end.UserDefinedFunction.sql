/****** Object:  UserDefinedFunction [dbo].[get_week_end]    Script Date: 07/26/2008 18:12:54 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='get_week_end')
DROP FUNCTION get_week_end
GO
CREATE FUNCTION [dbo].[get_week_end] (@date datetime)
RETURNS datetime AS
BEGIN
   return dateadd(yyyy, datepart(yyyy,
      dateadd(weekday,7-datepart(weekday, @date),@date))-1900, 0)
    + dateadd(ms, -3,
      dateadd(dy, datepart(dy,
     dateadd(weekday,7-datepart(weekday, @date),@date)),0) )
END
GO
