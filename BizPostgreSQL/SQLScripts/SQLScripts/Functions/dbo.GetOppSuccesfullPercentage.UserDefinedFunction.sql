/****** Object:  UserDefinedFunction [dbo].[GetOppSuccesfullPercentage]    Script Date: 07/26/2008 18:13:07 ******/

GO

GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getoppsuccesfullpercentage')
DROP FUNCTION getoppsuccesfullpercentage
GO
CREATE FUNCTION [dbo].[GetOppSuccesfullPercentage](@numUserCntID numeric(9),@numDomainID numeric(9),@dtDateFrom datetime,@dtDateTo datetime)  
 RETURNS decimal   
AS  
BEGIN  
declare @SucessPer as decimal  
declare @OppTCount as decimal  
declare @OppCount as decimal  
  
select @OppTCount=count(*)  from OpportunityMaster OM where  OM.tintOppType=1  and OM.numdomainid=@numDomainID and  OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo  
select @OppCount=count(*)  from OpportunityMaster OM where   OM.tintOppType=1 and tintOppStatus=1  and OM.numdomainid=@numDomainID and  OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo  
if @OppTCount= 0   
begin  
 set @SucessPer=0  
end  
else  
begin  
 set @SucessPer=(@OppCount/@OppTCount)*100  
end  
  
 RETURN @SucessPer  
END
GO
