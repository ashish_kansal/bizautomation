/****** Object:  UserDefinedFunction [dbo].[fn_GetCurrentOpeningBalanceForCashFlow]    Script Date: 07/26/2008 18:12:32 ******/

GO

GO
--Created By Siva                                                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcurrentopeningbalanceforcashflow')
DROP FUNCTION fn_getcurrentopeningbalanceforcashflow
GO
CREATE FUNCTION [dbo].[fn_GetCurrentOpeningBalanceForCashFlow](@numChartAcntId as numeric(9),@dtFromDate as datetime,@dtToDate as datetime)                                                              
returns DECIMAL(20,5)              
As                                                              
Begin                                                              
-- Declare @numTransactionId as integer                                                              
-- Declare @numDebitAmt as DECIMAL(20,5)                                                              
-- Declare @numCreditAmt as DECIMAL(20,5)                                                              
-- Declare @numAcntTypeId as integer                                                               
-- Declare @monCurrentOpeningBalance as DECIMAL(20,5)                                                              
-- Declare @numOriginalOpeningBal as DECIMAL(20,5)                                                    
-- Declare @numOpeningBal as DECIMAL(20,5)                                                       
-- Declare @count as integer                                                          
-- Declare @numJournalId as integer                               
-- Declare @numJournalChartAcntId as integer                              
--                                                        
-- Set @monCurrentOpeningBalance=0                                                            
-- Set @numOriginalOpeningBal=0                                                            
-- Set @count=0                                 
-- Set @numJournalId=0                              
-- Set @numOpeningBal=0                                
--                                                
--                                                             
--                                                            
--                                                      
--                                                   
--  Select @numTransactionId=min(numTransactionId) From General_Journal_Header  GJH                                                     
--  Inner Join  General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId Where GJD.numChartAcntId=@numChartAcntId                                                     
--  And GJH.datEntry_Date<'' + convert(varchar(300),@dtFromDate) +''                                              
--                                                            
-- While @numTransactionId <>0                                                              
-- Begin                                                       
--   Select @numAcntTypeId=numAcntType From Chart_Of_Accounts Where numAccountId=@numChartAcntId                                                           
--   if @numAcntTypeId=815 or @numAcntTypeId=816 or @numAcntTypeId=820  or @numAcntTypeId=821  or @numAcntTypeId=822  or @numAcntTypeId=825 or @numAcntTypeId=827                                                                                         
--    Begin                                                              
--      Select @numDebitAmt=isnull(numdebitAmt,0),@numCreditAmt=isnull(numCreditAmt,0),@numJournalId=numJournalId From General_Journal_Details Where numTransactionId=@numTransactionId                                                              
--      Select @numJournalChartAcntId=isnull(numChartAcntId,0)  From General_Journal_Header where numJournal_Id=@numJournalId                              
--                               
-- if @numChartAcntId <>15                                      
--     Begin                                    
--      if @numDebitAmt <>0                                                           
--        Set  @monCurrentOpeningBalance=@monCurrentOpeningBalance-@numDebitAmt                                                                
--       Else                                                                
--         Set  @monCurrentOpeningBalance=@monCurrentOpeningBalance+@numCreditAmt                                                                
--     End                                    
--     Else                                    
--     Begin                              
--       if  @numJournalChartAcntId<>0                               
--        Begin                                 
--      if @numDebitAmt <>0                                                           
--        Set  @monCurrentOpeningBalance=@monCurrentOpeningBalance-@numDebitAmt                                                                
--       Else                                                                
--         Set  @monCurrentOpeningBalance=@monCurrentOpeningBalance+@numCreditAmt                                                                
--        End                              
--       Else                              
--       Begin                              
--       if @numDebitAmt <>0                                                           
--        Set  @monCurrentOpeningBalance=@monCurrentOpeningBalance-@numDebitAmt                                                                
--       Else                                                                
--         Set  @monCurrentOpeningBalance=@monCurrentOpeningBalance+@numCreditAmt                                                                
--     End                                 
--     End                 
--                                                         
--    End                                         
--    Else                                                
--    Begin                                                              
--      Select @numDebitAmt=numdebitAmt ,@numCreditAmt=numCreditAmt From General_Journal_Details  Where numTransactionId=@numTransactionId                                                              
--       if @numDebitAmt <>0                                                               
--          Set @monCurrentOpeningBalance=@monCurrentOpeningBalance+@numDebitAmt                                                              
--        Else                                                              
--          Set @monCurrentOpeningBalance=@monCurrentOpeningBalance-@numCreditAmt                                                              
--     End                                                            
--                                                      
--      Select @numTransactionId=min(numTransactionId) From General_Journal_Header  GJH                                               
--      inner join  General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId Where  GJD.numChartAcntId=@numChartAcntId And GJD.numTransactionId>@numTransactionId                              
--     And  GJH.datEntry_Date<'' + convert(varchar(300),@dtFromDate) +''                                                              
--                                                        
--                                                      
--    End                                                      
--                                               
--                                      
--                                                  
--        if @numChartAcntId<>15 -- Not for Opening Balance Equity                                            
--  Begin                                              
--    Set @monCurrentOpeningBalance=@monCurrentOpeningBalance                                                              
--   End                                                            
--      return  isnull(@monCurrentOpeningBalance,0)           
--      --return @monCurrentOpeningBalance                                                         
RETURN 0
 End
GO
