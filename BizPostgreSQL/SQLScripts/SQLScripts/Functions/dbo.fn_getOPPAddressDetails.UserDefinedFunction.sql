GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_getOPPAddressDetails')
DROP FUNCTION fn_getOPPAddressDetails
GO
CREATE FUNCTION [dbo].[fn_getOPPAddressDetails] (@numOppId numeric,@numDomainID  NUMERIC,@tintMode AS TINYINT)
RETURNS 
@ParsedList table
(
	numCountry	NUMERIC(18),
	numState	NUMERIC(18),
	vcCountry	VARCHAR(50),
	vcState		VARCHAR(50),
	vcCity		VARCHAR(50),
	vcStreet	VARCHAR(100),
	vcPostalCode VARCHAR(20),
	vcAddressName VARCHAR(50),
	vcCompanyName VARCHAR(100),
	vcContact VARCHAR(200)
)
as
begin
declare @strAddress varchar(1000)

  DECLARE  @tintOppType  AS TINYINT
  DECLARE  @tintBillType  AS TINYINT
  DECLARE  @tintShipType  AS TINYINT
  DECLARE @numBillToAddressID NUMERIC(18,0)
  DECLARE @numShipToAddressID NUMERIC(18,0)
 
DECLARE @numParentOppID AS NUMERIC,@numDivisionID AS NUMERIC 
      
SELECT  @tintOppType = tintOppType,
        @tintBillType = tintBillToType,
        @tintShipType = tintShipToType,
        @numDivisionID = numDivisionID,
		@numBillToAddressID = ISNULL(numBillToAddressID,0)
		,@numShipToAddressID=ISNULL(numShipToAddressID,0)
      FROM   OpportunityMaster WHERE  numOppId = @numOppId

-- When Creating PO from SO and Bill type is Customer selected 
SELECT @numParentOppID=ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId
            
IF @tintMode=1 --Billing Address
BEGIN
	IF (@tintBillType IS NULL AND @tintOppType = 1) OR (@tintBillType = 1 AND @tintOppType = 1) --Primary Bill Address or When Sales order and bill to is set to customer	 
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
		SELECT  ISNULL(AD.numCountry,0),
				ISNULL(AD.numState,0),
				ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
				ISNULL(dbo.fn_GetState(AD.numState),''),
				ISNULL(AD.VcCity,'') ,
			    ISNULL(AD.vcStreet,''),
			    ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(@numDivisionID) AS vcCompanyName
				,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
        FROM AddressDetails AD 
		WHERE AD.numDomainID = @numDomainID 
		AND AD.numRecordID = @numDivisionID 
		AND AD.tintAddressOf = 2 
		AND AD.tintAddressType = 1 
		AND AD.bitIsPrimary=1                    
	END
	ELSE IF @tintBillType = 1 AND @tintOppType = 2 -- When Create PO from SO and Bill to is set to Customer
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
		SELECT numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact 
		FROM dbo.fn_getOPPAddressDetails(@numParentOppID,@numDomainID,@tintMode) 
	END
	ELSE IF ISNULL(@tintBillType,0) = 0
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
		SELECT  ISNULL(AD.numCountry,0),
				ISNULL(AD.numState,0),
				ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
				ISNULL(dbo.fn_GetState(AD.numState),''),
				ISNULL(AD.VcCity,'') ,
			    ISNULL(AD.vcStreet,''),
			    ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,Com1.vcCompanyName
				,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
		FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
		JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
		JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
		WHERE  D1.numDomainID = @numDomainID	
		
	END
	ELSE IF @tintBillType = 2 
	BEGIN
		IF ISNULL(@numBillToAddressID,0) > 0
		BEGIN
			INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
			SELECT  ISNULL(AD.numCountry,0),
					ISNULL(AD.numState,0),
					ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
					ISNULL(dbo.fn_GetState(AD.numState),''),
					ISNULL(AD.VcCity,'') ,
					ISNULL(AD.vcStreet,''),
					ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
            FROM AddressDetails AD WHERE numAddressID=@numBillToAddressID
		END
		ELSE
		BEGIN
			INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
			SELECT  numBillCountry,
					numBillState,
					isnull(dbo.fn_GetListItemName(numBillCountry),''),
					isnull(dbo.fn_GetState(numBillState),''),
					VcBillCity,
					vcBillStreet,
					vcBillPostCode,ISNULL(vcAddressName,'') AS vcAddressName,CASE WHEN LEN(vcBillCompanyName) > 0 THEN vcBillCompanyName ELSE dbo.fn_GetComapnyName(@numDivisionID) END AS vcCompanyName 
					,(CASE 
						WHEN ISNULL(bitAltBillingContact,0) = 1 
						THEN ISNULL(vcAltBillingContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numBillingContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numBillingContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation INNER JOIN DivisionMaster ON AdditionalContactsInformation.numDivisionId=DivisionMaster.numDivisionID WHERE AdditionalContactsInformation.numDomainID=@numDomainID AND DivisionMaster.numCompanyID=numBillCompanyId AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
			FROM   OpportunityAddress WHERE  numOppID = @numOppId
		END
	END
	ELSE IF @tintBillType = 3
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
		SELECT  numBillCountry,
				numBillState,
				isnull(dbo.fn_GetListItemName(numBillCountry),''),
				isnull(dbo.fn_GetState(numBillState),''),
				VcBillCity,
				vcBillStreet,
				vcBillPostCode,ISNULL(vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(@numDivisionID) AS vcCompanyName
				,(CASE 
						WHEN ISNULL(bitAltBillingContact,0) = 1 
						THEN ISNULL(vcAltBillingContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numBillingContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numBillingContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=@numDivisionID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
		FROM   OpportunityAddress WHERE  numOppID = @numOppId
	END
END
ELSE IF @tintMode=2 --Shipping Address
BEGIN
	IF @tintShipType IS NULL OR (@tintShipType = 1 AND @tintOppType = 1)
	BEGIN
			INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
			SELECT  ISNULL(AD.numCountry,0),
					ISNULL(AD.numState,0),
					ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
					ISNULL(dbo.fn_GetState(AD.numState),''),
					ISNULL(AD.VcCity,'') ,
					ISNULL(AD.vcStreet,''),
					ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(@numDivisionID) AS vcCompanyName 
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
            FROM AddressDetails AD 
				WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
	END
	ELSE IF @tintShipType = 1 AND @tintOppType = 2 -- When Create PO from SO and Ship to is set to Customer 
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
		SELECT numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact
		FROM dbo.fn_getOPPAddressDetails(@numParentOppID,@numDomainID,@tintMode) 
	END
	ELSE IF @tintShipType = 0
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
		SELECT  ISNULL(AD.numCountry,0),
				ISNULL(AD.numState,0),
				ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
				ISNULL(dbo.fn_GetState(AD.numState),''),
				ISNULL(AD.VcCity,'') ,
				ISNULL(AD.vcStreet,''),
				ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,Com1.vcCompanyName
				,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
         FROM   companyinfo [Com1] JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
         JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
         JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
         WHERE  D1.numDomainID = @numDomainID
	END
	ELSE IF @tintShipType = 2 
	BEGIN
		IF ISNULL(@numShipToAddressID,0) > 0
		BEGIN
			INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
			SELECT  ISNULL(AD.numCountry,0),
					ISNULL(AD.numState,0),
					ISNULL(dbo.fn_GetListItemName(AD.numCountry),''),
					ISNULL(dbo.fn_GetState(AD.numState),''),
					ISNULL(AD.VcCity,'') ,
					ISNULL(AD.vcStreet,''),
					ISNULL(AD.vcPostalCode,''),ISNULL(AD.vcAddressName,'') AS vcAddressName,dbo.fn_GetComapnyName(numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
            FROM AddressDetails AD WHERE numAddressID=@numShipToAddressID
		END
		ELSE
		BEGIN
			INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
			SELECT numShipCountry,numShipState,isnull(dbo.fn_GetListItemName(numShipCountry),''),isnull(dbo.fn_GetState(numShipState),''),VcShipCity,vcShipStreet,vcShipPostCode,ISNULL(vcAddressName,'') AS vcAddressName
			,(CASE WHEN LEN(vcShipCompanyName) > 0 THEN vcShipCompanyName ELSE dbo.fn_GetComapnyName(@numDivisionID) END) AS vcCompanyName
			,(CASE 
						WHEN ISNULL(bitAltShippingContact,0) = 1 
						THEN ISNULL(vcAltShippingContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numShippingContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numShippingContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=@numDivisionID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
			FROM   OpportunityAddress WHERE  numOppID = @numOppId
		END
	END
	ELSE IF @tintShipType = 3
	BEGIN
		INSERT INTO @ParsedList(numCountry,numState,vcCountry,vcState,vcCity,vcStreet,vcPostalCode,vcAddressName,vcCompanyName,vcContact)
		SELECT numShipCountry,numShipState,isnull(dbo.fn_GetListItemName(numShipCountry),''),isnull(dbo.fn_GetState(numShipState),''),VcShipCity,vcShipStreet,vcShipPostCode,ISNULL(vcAddressName,'') AS vcAddressName
		,dbo.fn_GetComapnyName(@numDivisionID) AS vcCompanyName
		,(CASE 
						WHEN ISNULL(bitAltShippingContact,0) = 1 
						THEN ISNULL(vcAltShippingContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numShippingContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numShippingContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=@numDivisionID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
		FROM   OpportunityAddress WHERE  numOppID = @numOppId
	END
END

RETURN
end
GO
