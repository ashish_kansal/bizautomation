/****** Object:  UserDefinedFunction [dbo].[fn_AdvSearchColumnName]    Script Date: 07/26/2008 18:12:26 ******/

GO

GO
--Created By: Debasish Nag      
--Created On: 24th Aug 2005      
--This function returns the ListItem Name, Territory Name or the Group Name and is extensively used in Advance Search  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_advsearchcolumnname')
DROP FUNCTION fn_advsearchcolumnname
GO
CREATE FUNCTION [dbo].[fn_AdvSearchColumnName](@numlistID numeric, @vclistType NChar(2))  
RETURNS Nvarchar(100)   
AS  
BEGIN  
 DECLARE @listName as NVarchar(100)  
 SET @listName = ''  
  
 IF @vclistType = 'L'  
 BEGIN  
  SELECT @listName=vcData FROM ListDetails WHERE numListItemID = @numlistID  
 END  
 IF @vclistType = 'G'  
 BEGIN  
  SELECT @listName=vcGrpName FROM Groups WHERE numGrpId = @numlistID  
 END  
 IF @vclistType = 'T'  
 BEGIN  
  SELECT @listName=vcTerName FROM TerritoryMaster WHERE numTerId = @numlistID  
 END  
 IF @vclistType = 'U'  
 BEGIN  
  SELECT @listName=vcUserName FROM UserMaster WHERE numUserId = @numlistID  
 END  
 IF @vclistType = 'S'  
 BEGIN  
  SELECT @listName=vcState FROM State WHERE numStateId = @numlistID  
 END 

 RETURN @listName  
END
GO
