GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetItemPromotions')
DROP FUNCTION fn_GetItemPromotions
GO
CREATE FUNCTION [dbo].[fn_GetItemPromotions] 
(
    @numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numItemClassification NUMERIC(18,0)
	,@numRelationship NUMERIC(18,0)
	,@numProfile NUMERIC(18,0)
	,@tintMode TINYINT
)
RETURNS @ItemPromotion TABLE
(
	numTotalPromotions INT
	,vcPromoDesc VARCHAR(MAX)
	,bitRequireCouponCode BIT
)
AS 
BEGIN
	INSERT INTO @ItemPromotion
	(
		numTotalPromotions
		,vcPromoDesc
		,bitRequireCouponCode
	)
	SELECT TOP 1
		COUNT(PO.numProId) OVER() numTotalPromotions
		,CONCAT((CASE WHEN COUNT(PO.numProId) OVER() > 1 THEN '' ELSE ISNULL((CASE WHEN @tintMode=2 THEN PO.vcLongDesc ELSE PO.vcShortDesc END),'-') END),(CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN ' <br/><i>(coupon code required)</i>' ELSE '' END)) vcShortDesc
		,(CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN 1 ELSE 0 END) bitRequireCouponCode
	FROM 
		PromotionOffer PO
	LEFT JOIN
		PromotionOffer POOrder
	ON
		PO.numOrderPromotionID = POOrder.numProId
	WHERE 
		PO.numDomainId=@numDomainID 
		AND ISNULL(PO.bitEnabled,0)=1 
		AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
		AND 1 = (CASE 
					WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
					THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
					ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
				END)
		AND 1 = (CASE 
					WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
					THEN
						(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
					ELSE
						(CASE PO.tintCustomersBasedOn 
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN 1
							ELSE 0
						END)
				END)
		AND 1 = (CASE 
					WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					WHEN PO.tintOfferBasedOn = 4 THEN 1 
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
		END

	RETURN
END
GO