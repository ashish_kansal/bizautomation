/****** Object:  UserDefinedFunction [dbo].[FormatedDateFromDate]    Script Date: 07/26/2008 18:12:52 ******/

GO

GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='formateddatefromdate')
DROP FUNCTION formateddatefromdate
GO
CREATE FUNCTION [dbo].[FormatedDateFromDate](@date as VARCHAR(50),@numDomainID as numeric)
returns varchar(30)
as
BEGIN

IF ISDATE(@date)=0
BEGIN
 SET @date=null
END

	declare @strDateFormat as varchar(30)
	declare @strDate as varchar(30)
	select @strDateFormat=vcDateFormat from domain where numDomainID=@numDomainID
	declare @str4Yr as varchar(4)
	declare @str2Yr as varchar(2)
	set @str4Yr=Year(@date)
	set @str2Yr=SUBSTRING(convert(varchar(4),Year(@date)), 3, 4)
	declare @strIntMonth as varchar(2)
	declare @strDay as varchar(2)
	declare @str3LetMon as varchar(3)
	declare @strFullMonth as varchar(15)
	set @strFullMonth = DATENAME(month, @date)
	set @str3LetMon= @strFullMonth
    set @strIntMonth=month(@date) 
    set @strDay=day(@date)
    if @strIntMonth<=9 set @strIntMonth='0'+@strIntMonth
	if @strDay<=9 set @strDay='0'+@strDay   

	set @strDate= Replace(@strDateFormat, 'DD', @strDay)
	set @strDate= Replace(@strDate, 'YYYY', @str4Yr)
	set @strDate= Replace(@strDate, 'MM', @strIntMonth)
	set @strDate= Replace(@strDate, 'YY', @str2Yr)
	set @strDate= Replace(@strDate, 'MONTH', @strFullMonth)
	set @strDate= Replace(@strDate, 'MON', @str3LetMon)


	Return @strDate
end
GO
