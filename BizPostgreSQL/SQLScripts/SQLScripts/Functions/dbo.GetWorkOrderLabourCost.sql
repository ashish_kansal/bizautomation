GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetWorkOrderLabourCost')
DROP FUNCTION GetWorkOrderLabourCost
GO
CREATE FUNCTION [dbo].[GetWorkOrderLabourCost]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
)    
RETURNS DECIMAL(20,5)    
AS
BEGIN
	DECLARE @monLabourCost DECIMAL(20,5)
	
	SET @monLabourCost = ISNULL((SELECT
									SUM(dbo.GetTimeSpendOnTaskInMinutes(@numDomainID,SPDT.numTaskId) * (ISNULL(SPDT.monHourlyRate,0)/60))
								FROM
									StagePercentageDetailsTask SPDT
								WHERE
									SPDT.numDomainID = @numDomainID
									AND SPDT.numWorkOrderId = @numWOID),0)

	RETURN @monLabourCost
END
GO
