
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckIfAssemblyKitChildItemsIsOnBackorder')
DROP FUNCTION CheckIfAssemblyKitChildItemsIsOnBackorder
GO
CREATE FUNCTION [dbo].[CheckIfAssemblyKitChildItemsIsOnBackorder] 
(
	@numDomainID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@numOppChildItemID AS NUMERIC(18,0),
	@numOppKitChildItemID AS NUMERIC(18,0),
	@bitKitParent BIT,
	@bitWorkOrder BIT,
	@numQuantity AS FLOAT
)
RETURNS BIT
AS
BEGIN

    DECLARE @bitBackOrder FLOAT = 0
	DECLARE @numOnHand FLOAT
	DECLARE @numAllocation FLOAT
	DECLARE @numQtyShipped FLOAT = 0
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numWOStatus NUMERIC(18,0)

	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID 

	IF ISNULL(@numOppChildItemID,0) > 0 AND ISNULL(@numOppKitChildItemID,0) = 0
	BEGIN
		SELECT @numQtyShipped = ISNULL(numQtyShipped,0),@numWarehouseItemID=ISNULL(numWareHouseItemId,0) FROM OpportunityKitItems WHERE numOppChildItemID = @numOppChildItemID
		SELECT @numWOStatus=ISNULL(numWOStatus,0) FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID
	END
	ELSE IF ISNULL(@numOppChildItemID,0) > 0 AND ISNULL(@numOppKitChildItemID,0) > 0
	BEGIN
		SELECT @numQtyShipped = ISNULL(numQtyShipped,0),@numWarehouseItemID=ISNULL(numWareHouseItemId,0) FROM OpportunityKitChildItems WHERE numOppKitChildItemID = @numOppKitChildItemID
		SELECT @numWOStatus=ISNULL(numWOStatus,0) FROM WorkOrder WHERE numOppId=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND numOppKitChildItemID=@numOppKitChildItemID
	END

	SELECT @numQuantity = @numQuantity - @numQtyShipped
	
	IF @bitWorkOrder = 1
	BEGIN
		IF @numWOStatus = 23184
		BEGIN
			IF @numQuantity > 0
			BEGIN
				IF (SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID AND numAllocation >= @numQuantity) > 0
				BEGIN
					SET @bitBackOrder = 0
				END
				ELSE
				BEGIN
					SET @bitBackOrder = 1
				END
			END	
			ELSE 
			BEGIN
				SET @bitBackOrder = 0
			END
		END
		ELSE
		BEGIN
			DECLARE @TEMP TABLE
			(
				ItemLevel INT
				,numParentWOID NUMERIC(18,0)
				,numWOID NUMERIC(18,0)
				,numItemCode NUMERIC(18,0)
				,numQty FLOAT
				,numQtyRequiredToBuild FLOAT
				,numWarehouseItemID NUMERIC(18,0)
				,bitWorkOrder BIT
				,bitReadyToBuild BIT
			)

			;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,numItemCode,numWarehouseItemID,bitWorkOrder,numQty,numQtyRequiredToBuild) AS
			(
				SELECT
					1,
					numParentWOID,
					numWOId,
					numItemCode,
					numWareHouseItemId,
					1 AS bitWorkOrder,
					@numQuantity,
					CAST(1 AS FLOAT)
				FROM
					WorkOrder
				WHERE
					numOppId=@numOppID 
					AND numOppItemID=@numOppItemID
					AND numOppChildItemID=@numOppChildItemID
					AND ISNULL(numOppKitChildItemID,0)=@numOppKitChildItemID
				UNION ALL
				SELECT 
					c.ItemLevel+2,
					c.numWOID,
					WorkOrder.numWOId,
					WorkOrder.numItemCode,
					WorkOrder.numWareHouseItemId,
					1 AS bitWorkOrder,
					c.numQty * WOD.numQtyItemsReq_Orig,
					WOD.numQtyItemsReq_Orig
				FROM 
					WorkOrder
				INNER JOIN 
					CTEWorkOrder c 
				ON 
					WorkOrder.numParentWOID = c.numWOID
				INNER JOIN
					WorkOrderDetails WOD
				ON
					WOD.numWOId = c.numWOID
					AND WOD.numChildItemID = WorkOrder.numItemCode
					AND ISNULL(WOD.numWareHouseItemId,0) = ISNULL(WorkOrder.numWareHouseItemId,0)
			)

			INSERT INTO @TEMP
			(
				ItemLevel
				,numParentWOID
				,numWOID
				,numItemCode
				,numQty
				,numQtyRequiredToBuild
				,numWarehouseItemID
				,bitWorkOrder
				,bitReadyToBuild
			)
			SELECT
				ItemLevel,
				numParentWOID,
				numWOID,
				numItemCode,
				numQty,
				numQtyRequiredToBuild,
				CTEWorkOrder.numWarehouseItemID,
				bitWorkOrder,
				0
			FROM 
				CTEWorkOrder
		
			INSERT INTO @TEMP
			(
				ItemLevel
				,numParentWOID
				,numWOID
				,numItemCode
				,numQty
				,numWarehouseItemID
				,bitWorkOrder
				,bitReadyToBuild
			)
			SELECT
				t1.ItemLevel + 1,
				t1.numWOID,
				NULL,
				WorkOrderDetails.numChildItemID,
				t1.numQty * WorkOrderDetails.numQtyItemsReq_Orig,
				WorkOrderDetails.numWarehouseItemID,
				0 AS bitWorkOrder,
				CASE 
					WHEN Item.charItemType='P'
					THEN
						CASE 
							WHEN @tintCommitAllocation=2 
							THEN (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
							ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
						END
					ELSE 1
				END
			FROM
				WorkOrderDetails
			INNER JOIN
				@TEMP t1
			ON
				WorkOrderDetails.numWOId = t1.numWOID
			INNER JOIN 
				Item
			ON
				WorkOrderDetails.numChildItemID = Item.numItemCode
				AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
			LEFT JOIN
				WareHouseItems
			ON
				WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
			LEFT JOIN
				Warehouses
			ON
				WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
			LEFT JOIN
				OpportunityMaster 
			ON
				WorkOrderDetails.numPOID = OpportunityMaster.numOppId

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT
			DECLARE @numTempWOID NUMERIC(18,0)

			DECLARE @TEMPWO TABLE
			(
				ID INT IDENTITY(1,1)
				,numWOID NUMERIC(18,0)
			)
			INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

			SELECT @iCount=COUNT(*) FROM @TEMPWO

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

				UPDATE
					T1
				SET
					bitReadyToBuild = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND bitReadyToBuild=0) > 0 THEN 0 ELSE 1 END)
				FROM
					@TEMP T1
				WHERE
					numWOID=@numTempWOID

				SET @i = @i + 1
			END

			IF (SELECT COUNT(*) FROM @TEMP WHERE ISNULL(bitReadyToBuild,0)=0) > 0
			BEGIN
				SET @bitBackOrder = 1
			END
			ELSE
			BEGIN
				SET @bitBackOrder = 0
			END
		END
	END
	ELSE IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			numOppKitChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitAssembly BIT,
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq FLOAT,
			numQtyItemReq_Orig FLOAT,
			numOnHand FLOAT,
			numAllocation FLOAT,
			numPrentItemBackOrder INT
		)

		IF ISNULL(@numOppChildItemID,0) = 0
		BEGIN
			INSERT INTO 
				@TEMPTABLE
			SELECT
				OKI.numChildItemID,
				0,
				OKI.numOppChildItemID,
				0,
				I.vcItemName,
				I.bitAssembly,
				I.bitKitParent,
				OKI.numWareHouseItemId,
				OKI.numQtyItemsReq_Orig * @numQuantity,
				OKI.numQtyItemsReq_Orig,
				ISNULL(WI.numOnHand,0),
				ISNULL(WI.numAllocation,0),
				0
			FROM
				OpportunityKitItems OKI
			INNER JOIN
				WareHouseItems WI
			ON
				OKI.numWareHouseItemId = WI.numWareHouseItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OKI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OKI.numChildItemID = I.numItemCode
			WHERE
				OI.numoppitemtCode = @numOppItemID
				AND ISNULL(I.bitKitParent,0) = 0
		END

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			OKCI.numOppKitChildItemID,
			I.vcItemName,
			I.bitAssembly,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0),
			0
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
			AND ISNULL(I.bitKitParent,0) = 0
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		IF EXISTS (
					SELECT
						numItemCode
					FROM
						@TEMPTABLE
					WHERE
						1 = CASE 
								WHEN ISNULL(bitAssembly,0) = 1
								THEN
									dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(@numDomainID,@numOppID,@numOppItemID,numOppChildItemID,numOppKitChildItemID,0,1,numQtyItemsReq)
								WHEN ISNULL(bitKitParent,0) = 1
								THEN
									dbo.CheckIfAssemblyKitChildItemsIsOnBackorder(@numDomainID,@numOppID,@numOppItemID,numOppChildItemID,numOppKitChildItemID,1,0,numQtyItemsReq)
								ELSE
									CASE 
										WHEN @tintCommitAllocation=2 
										THEN (CASE WHEN ISNULL(numQtyItemsReq,0) >= numOnHand THEN 0 ELSE 1 END)
										ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) >= numAllocation THEN 0 ELSE 1 END) 
									END
							END
					)
		BEGIN
			SET @bitBackOrder = 1
		END
		ELSE
		BEGIN
			SET @bitBackOrder = 0
		END
	END
	ELSE
	BEGIN
		SELECT
			@numOnHand = ISNULL(numOnHand,0)
			,@numAllocation = ISNULL(numAllocation,0)
		FROM
			WareHouseItems 
		WHERE
			numWareHouseItemID = @numWarehouseItemID

		IF @tintCommitAllocation=2 
		BEGIn
			IF (@numQuantity - @numQtyShipped) > @numOnHand
				SET @bitBackOrder = 1
			ELSE
				SET @bitBackOrder = 0
		END
		ELSE
		BEGIN
			IF (@numQuantity - @numQtyShipped) > @numAllocation
				SET @bitBackOrder = 1
			ELSE
				SET @bitBackOrder = 0
		END
	END

	RETURN @bitBackOrder

END
GO
