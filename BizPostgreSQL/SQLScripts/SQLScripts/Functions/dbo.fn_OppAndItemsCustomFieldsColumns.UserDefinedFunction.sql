/****** Object:  UserDefinedFunction [dbo].[fn_OppAndItemsCustomFieldsColumns]    Script Date: 07/26/2008 18:12:49 ******/

GO

GO
--Created By: Debasish Nag          
--Created On: 11th Nov 2005          
--Purpose: To Get the Custom Fields for Opportunities and Items as a string of columns        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_oppanditemscustomfieldscolumns')
DROP FUNCTION fn_oppanditemscustomfieldscolumns
GO
CREATE FUNCTION [dbo].[fn_OppAndItemsCustomFieldsColumns](@numLocationId tinyInt, @ReturnType tinyInt)         
 RETURNS NVarchar(4000)        
AS          
BEGIN          
 DECLARE @vcCustomOpportunityItemFieldList AS NVarchar(4000)          
 IF @numLocationId  = 2  --OPPORTUNITIES        
 BEGIN        
  IF @ReturnType = 0 --Column list along with their datatypes        
  BEGIN          
   SELECT @vcCustomOpportunityItemFieldList = Coalesce(@vcCustomOpportunityItemFieldList + ', ', '') +           
     'CFld' + Convert(NVarchar, cfm.fld_id) + ' NVarchar(50)'        
   FROM CFW_Fld_Master cfm          
   WHERE cfm.Grp_id = 2          
   GROUP BY cfm.Fld_id          
  END        
  IF @ReturnType = 1 --Column list        
  BEGIN          
   SELECT @vcCustomOpportunityItemFieldList = Coalesce(@vcCustomOpportunityItemFieldList + ', ', '') +           
     'CFld' + Convert(NVarchar, cfm.fld_id)        
   FROM CFW_Fld_Master cfm          
   WHERE cfm.Grp_id = 2          
   GROUP BY cfm.Fld_id          
  END        
  IF @ReturnType = 2 --Function list to populate the data        
  BEGIN          
   SELECT @vcCustomOpportunityItemFieldList = Coalesce(@vcCustomOpportunityItemFieldList + ', ', '') +           
     '(SELECT Case WHEN M.Fld_type = ''CheckBox'' THEN Opp' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value WHEN M.Fld_type = ''SelectBox'' THEN dbo.fn_AdvSearchColumnName(Opp' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value,''L'') ELSE Opp' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value   
     END FROM CFW_Fld_Master M, CFW_Fld_Values_Opp Opp' + Convert(NVarchar, cfm.fld_id) + ' WHERE M.Fld_ID = ' + Convert(NVarchar, cfm.fld_id) + ' AND recId = numOppId AND Opp' + Convert(NVarchar, cfm.fld_id) + '.Fld_Id = M.Fld_Id AND M.Grp_Id In (2,6))' 
       
   FROM CFW_Fld_Master cfm          
   WHERE cfm.Grp_id = 2          
   GROUP BY cfm.Fld_id          
  END        
 END        
 ELSE IF @numLocationId = 5 --ITEMS        
 BEGIN        
  IF @ReturnType = 0 --Column list along with their datatypes        
  BEGIN          
   SELECT @vcCustomOpportunityItemFieldList = Coalesce(@vcCustomOpportunityItemFieldList + ', ', '') +           
     'CFld' + Convert(NVarchar, cfm.fld_id) + ' NVarchar(50)'        
   FROM CFW_Fld_Master cfm          
   WHERE cfm.Grp_id = 5         
   GROUP BY cfm.Fld_id          
  END        
  IF @ReturnType = 1 --Column list        
  BEGIN          
   SELECT @vcCustomOpportunityItemFieldList = Coalesce(@vcCustomOpportunityItemFieldList + ', ', '') +           
     'CFld' + Convert(NVarchar, cfm.fld_id)        
   FROM CFW_Fld_Master cfm          
   WHERE cfm.Grp_id = 5        
   GROUP BY cfm.Fld_id          
  END        
  IF @ReturnType = 2 --Function list to populate the data        
  BEGIN          
   SELECT @vcCustomOpportunityItemFieldList = Coalesce(@vcCustomOpportunityItemFieldList + ', ', '') +           
    '(SELECT Case WHEN M.Fld_type = ''CheckBox'' THEN Item' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value WHEN M.Fld_type = ''SelectBox'' THEN dbo.fn_AdvSearchColumnName(Item' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value,''L'') ELSE Item' + Convert(NVarchar, cfm.fld_id) + '.Fld_Value  
     END FROM CFW_Fld_Master M, CFW_Fld_Values_Item Item' + Convert(NVarchar, cfm.fld_id) + ' WHERE M.Fld_ID = ' + Convert(NVarchar, cfm.fld_id) + ' AND recId = vcItemCode AND Item' + Convert(NVarchar, cfm.fld_id) + '.Fld_Id = M.Fld_Id AND M.Grp_Id = 5)' 
     
    
   FROM CFW_Fld_Master cfm          
   WHERE cfm.Grp_id = 5         
   GROUP BY cfm.Fld_id          
  END        
 END        
 RETURN @vcCustomOpportunityItemFieldList        
END
GO
