GO
SET ANSI_NULLS OFF
GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_SecondsConversion')
DROP FUNCTION fn_SecondsConversion
GO
CREATE FUNCTION [dbo].[fn_SecondsConversion]  
(  
	@TimeinSec INT 
)  
RETURNS VARCHAR(MAX)  
BEGIN  
DECLARE @op VARCHAR(MAX)  
SET @op = ''  
SELECT @op=RIGHT('0' + CAST(@TimeinSec / 3600 AS VARCHAR),2) + ':' +  
RIGHT('0' + CAST((@TimeinSec / 60) % 60 AS VARCHAR),2)
--+ ':' +  
--RIGHT('0' + CAST(@TimeinSec % 60 AS VARCHAR),2) 
RETURN @op  
End 
GO
