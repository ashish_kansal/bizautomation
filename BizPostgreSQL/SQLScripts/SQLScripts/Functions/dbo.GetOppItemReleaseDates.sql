GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOppItemReleaseDates')
DROP FUNCTION GetOppItemReleaseDates
GO
CREATE FUNCTION [dbo].[GetOppItemReleaseDates] 
(
	@numDomainID NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@bitReturnHtml BIT
)
RETURNS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @vcValue AS VARCHAR(MAX) = ''

	IF EXISTS (SELECT * FROM OpportunityItemsReleaseDates WHERE	numOppID=@numOppID AND numOppItemID=@numOppItemID)
	BEGIN
		SET @vcValue = (SELECT TOP 1 dbo.FormatedDateFromDate(dtReleaseDate,@numDomainID) FROM OpportunityItemsReleaseDates WHERE	numOppID=@numOppID AND numOppItemID=@numOppItemID)
	END
	ELSE
	BEGIN
		SET @vcValue = 'Not Available'
	END

	RETURN @vcValue
END