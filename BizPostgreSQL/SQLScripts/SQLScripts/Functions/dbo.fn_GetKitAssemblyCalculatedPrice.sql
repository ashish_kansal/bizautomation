GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetKitAssemblyCalculatedPrice')
DROP FUNCTION fn_GetKitAssemblyCalculatedPrice
GO
CREATE FUNCTION [dbo].[fn_GetKitAssemblyCalculatedPrice]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@numWarehouseItemID NUMERIC(18,0)
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@vcSelectedKitChildItems VARCHAR(MAX)
	,@numCurrencyID NUMERIC(18,0)
	,@fltExchangeRate FLOAT
)    
RETURNS @TempPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
	,monMSRPPrice DECIMAL(20,5)
)
AS    
BEGIN   
	DECLARE @monListPrice DECIMAL(20,5)

	SELECT 
		@monListPrice=ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode

	DECLARE @numWarehouseID NUMERIC(18,0)

	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID =@numWarehouseItemID
	END
	ELSE
	BEGIN
		SELECT TOP 1 @numWarehouseID=numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID
	END

	DECLARE @KitAssemblyPrice AS DECIMAL(20,5) = 0

	DECLARE @TEMPitems TABLE
	(
		numItemCode NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numUOMId NUMERIC(18,0)
		,bitKitParent BIT
		,bitFirst BIT
	)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
		)
		SELECT 
			OKI.numChildItemID
			,(OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1))
			,OKI.numUOMId
			,ISNULL(I.bitKitParent,0)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OKI.numOppId = @numOppID
			AND OKI.numOppItemID = @numOppItemID
			AND 1 = (CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1
						THEN (CASE 
								WHEN ISNULL(I.bitCalAmtBasedonDepItems,0) = 1 
								THEN (CASE WHEN ISNULL(I.tintKitAssemblyPriceBasedOn,1) = 4 THEN 1 ELSE 0 END)
								ELSE 1 
							END)
						ELSE 1
					END)

		INSERT INTO @TEMPitems
		(
			numItemCode
			,numQtyItemsReq
			,numUOMId
			,bitKitParent
		)
		SELECT 
			OKCI.numItemID
			,(OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,IOKI.numBaseUnit),1)) * (OKCI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,IOKCI.numBaseUnit),1))
			,OKCI.numUOMId
			,ISNULL(IOKCI.bitKitParent,0)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			Item IOKCI
		ON
			OKCI.numItemID = IOKCI.numItemCode
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OKCI.numOppChildItemID = OKI.numOppChildItemID
		INNER JOIN
			Item IOKI
		ON
			OKI.numChildItemID = IOKI.numItemCode
		WHERE
			OKCI.numOppId = @numOppID
			AND OKCI.numOppItemID = @numOppItemID		
			AND ISNULL(IOKI.bitKitParent,0) = 1 
			AND ISNULL(IOKI.bitCalAmtBasedonDepItems,0) = 1	
	END
	ELSE
	BEGIN
		DECLARE @TempExistingItems TABLE
		(
			vcItem VARCHAR(100)
		)

		INSERT INTO @TempExistingItems
		(
			vcItem
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcSelectedKitChildItems,',')

		DECLARE @TEMPSelectedKitChilds TABLE
		(
			ChildKitItemID NUMERIC(18,0),
			ChildKitWarehouseItemID NUMERIC(18,0),
			ChildKitChildItemID NUMERIC(18,0),
			ChildKitChildWarehouseItemID NUMERIC(18,0),
			ChildQty FLOAT
		)

		INSERT INTO @TEMPSelectedKitChilds
		(
			ChildKitItemID
			,ChildKitChildItemID
			,ChildQty
		)
		SELECT 
			Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 1))
			,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2))
			,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3))
		FROM  
		(
			SELECT 
				vcItem 
			FROM 
				@TempExistingItems
		) As [x]

		UPDATE 
			@TEMPSelectedKitChilds 
		SET 
			ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
			,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)


		IF (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
		BEGIN
			INSERT INTO @TEMPitems
			(
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			)
			SELECT
				I.numItemCode
				,ChildQty
				,I.numBaseUnit
				,0
			FROM
				@TEMPSelectedKitChilds T1
			INNER JOIN
				Item I
			ON
				T1.ChildKitChildItemID = I.numItemCOde
			WHERE
				ISNULL(ChildKitItemID,0)=0
		END
		ELSE
		BEGIN
			;WITH CTE (numItemCode, vcItemName, bitKitParent, bitCalAmtBasedonDepItems, numQtyItemsReq,numUOMId) AS
			(
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(I.bitCalAmtBasedonDepItems,0),
					CAST((ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT),
					ISNULL(numUOMId,0)
				FROM 
					[ItemDetails] ID
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE   
					[numItemKitID] = @numItemCode
					AND 1 = (CASE 
								WHEN ISNULL(I.bitKitParent,0) = 1 AND LEN(ISNULL(@vcSelectedKitChildItems,'')) > 0 THEN 
									(CASE 
										WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) 
										THEN 1 
										ELSE 0 
									END) 
								ELSE (CASE 
										WHEN (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
										THEN	
											(CASE 
												WHEN numChildItemID IN (SELECT ChildKitChildItemID FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) 
												THEN 1 
												ELSE 0 
											END) 
										ELSE 1
									END)
							END)
				UNION ALL
				SELECT
					ID.numChildItemID,
					I.vcItemName,
					ISNULL(I.bitKitParent,0),
					ISNULL(I.bitCalAmtBasedonDepItems,0),
					(CASE 
						WHEN ISNULL(((SELECT T2.ChildQty FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND t2.ChildKitItemID=ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)),0) > 0 
						THEN ISNULL(((SELECT T2.ChildQty FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND t2.ChildKitItemID=ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)),0) 
						ELSE CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT) 
					END),
					ISNULL(ID.numUOMId,0)
				FROM 
					CTE As Temp1
				INNER JOIN
					[ItemDetails] ID
				ON
					ID.numItemKitID = Temp1.numItemCode
				INNER JOIN 
					[Item] I 
				ON 
					ID.[numChildItemID] = I.[numItemCode]
				WHERE
					Temp1.bitKitParent = 1
					AND Temp1.bitCalAmtBasedonDepItems = 1
					AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND t2.ChildKitItemID=ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)
			)

			INSERT INTO @TEMPitems
			(
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			)
			SELECT
				numItemCode
				,numQtyItemsReq
				,numUOMId
				,bitKitParent
			FROM
				CTE
		END
	END

	IF EXISTS (SELECT ID.numItemCode FROM @TEMPitems ID INNER JOIN [Item] I ON ID.numItemCode = I.[numItemCode] WHERE I.charItemType = 'P' AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=I.numItemCode AND numWareHouseID=@numWarehouseID) = 0)
	BEGIN
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
			,monMSRPPrice
		)
		VALUES
		(
			0
			,0
			,0
		)
	END
	ELSE
	BEGIN
		DECLARE @monCalculatedPrice DECIMAL(20,5) = 0

		SET @monCalculatedPrice = ISNULL((SELECT
											(CASE WHEN @tintKitAssemblyPriceBasedOn=4 THEN ISNULL(@monListPrice,0) + ISNULL(SUM(CalculatedPrice),0) ELSE ISNULL(SUM(CalculatedPrice),0) END)
										FROM
										(
											SELECT  
												ISNULL(CASE 
														WHEN I.[charItemType]='P' 
														THEN 
															CASE 
															WHEN ISNULL(I.bitAssembly,0) = 1 AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
															THEN
																ISNULL((SELECT monPrice FROM dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,I.numItemCode,ID.[numQtyItemsReq],WI.numWareHouseItemID,I.tintKitAssemblyPriceBasedOn,0,0,'',@numCurrencyID,@fltExchangeRate)),0)
															WHEN ISNULL(I.bitKitParent,0) = 1  AND ISNULL(I.bitCalAmtBasedonDepItems,0) = 1
															THEN (CASE WHEN ISNULL(I.tintKitAssemblyPriceBasedOn,0) = 4 THEN (CASE 
																																WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=I.numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0))  
																																THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0)),0)
																																ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(I.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(I.monListPrice,0) END)
																															END) ELSE 0 END)
															ELSE
																(CASE @tintKitAssemblyPriceBasedOn
																		WHEN 2 THEN (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(I.monAverageCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(I.monAverageCost,0) END)
																		WHEN 3 THEN (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monCost,0) END) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
																		ELSE (CASE 
																				WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=I.numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0))  
																				THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0)),0)
																				ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(I.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(I.monListPrice,0) END)
																			END)
																END) 
															END
														ELSE (CASE @tintKitAssemblyPriceBasedOn 
																WHEN 4 THEN (CASE 
																				WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=I.numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0))  
																				THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0)),0)
																				ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(I.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(I.monListPrice,0) END)
																			END)
																WHEN 2 THEN (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(I.monAverageCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(I.monAverageCost,0) END)
																WHEN 3 THEN (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(V.monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(V.monCost,0) END) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
																ELSE (CASE 
																		WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=I.numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0))  
																		THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0)),0)
																		ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(I.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(I.monListPrice,0) END)
																	END) 
															END)
													END,0) * ID.[numQtyItemsReq] AS CalculatedPrice
											FROM 
												@TEMPitems ID
											INNER JOIN 
												[Item] I 
											ON 
												ID.numItemCode = I.[numItemCode]
											LEFT JOIN
												Vendor V
											ON
												I.numVendorID = V.numVendorID
												AND I.numItemCode = V.numItemCode
											OUTER APPLY
											(
												SELECT TOP 1 
													*
												FROM
													WareHouseItems WI
												WHERE 
													WI.numItemID=I.numItemCode 
													AND WI.numWareHouseID = @numWarehouseID
												ORDER BY
													WI.numWareHouseItemID
											) AS WI
										) T1),0)

		
		INSERT INTO @TempPrice
		(
			bitSuccess
			,monPrice
			,monMSRPPrice
		)
		SELECT
			1
			,monFinalPrice
			,@monCalculatedPrice
		FROM
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,@numItemCode,@numQty,1,@monCalculatedPrice,@numWarehouseItemID,@vcSelectedKitChildItems,@numCurrencyID)
	END

	RETURN
END
GO