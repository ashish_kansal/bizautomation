GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemPriceLevelDetail')
DROP FUNCTION fn_GetItemPriceLevelDetail
GO
CREATE FUNCTION [dbo].[fn_GetItemPriceLevelDetail] 
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)

)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @vcValue AS VARCHAR(MAX) = ''
   
	DECLARE @numWareHouseItemID AS BIGINT
	SELECT TOP 1 @numWareHouseItemID = ISNULL(numWareHouseItemID,0) FROM dbo.WareHouseItems WHERE numWareHouseID = @numWareHouseID AND numItemID = @numItemCode AND ISNULL(numWLocationID,0) <> -1

	IF @numWareHouseItemID > 0
	BEGIN
		SELECT 
			@vcValue = CONCAT('<b>Warehouse Price</b><br/>',ISNULL(W.vcWareHouse,''),' - ',ISNULL(WI.monWListPrice,0))
		FROM    
			Item I
		LEFT JOIN 
			dbo.WareHouseItems WI 
		ON 
			WI.numItemID = I.numItemCode AND WI.numDomainID = I.numDomainID
		LEFT JOIN 
			dbo.Warehouses W 
		ON 
			W.numWareHouseID = WI.numWareHouseID
		WHERE 
			ISNULL(I.numItemCode,0) = @numItemCode 
			AND (W.numWareHouseID = ISNULL(@numWareHouseID,0) OR ISNULL(@numWareHouseID,0) = 0) 
			AND ISNULL(numWLocationID,0) <> -1
	END

	DECLARE @monListPrice AS DECIMAL(20,5) = 0
	DECLARE @monVendorCost AS DECIMAL(20,5) = 0

	

	IF @numWareHouseItemID > 0    
	BEGIN      
		SELECT @monListPrice=ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numWareHouseItemID=@numWareHouseItemID     
	END      
	ELSE      
	BEGIN      
		SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode      
	END 

	SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)


	-- PRICE TABLE
	IF EXISTS (SELECT * FROM PricingTable WHERE numItemCode = @numItemCode AND ISNULL(numCurrencyID,0) = 0)
	BEGIN
		DECLARE @listPriceTable VARCHAR(MAX)

		SELECT
			@listPriceTable = COALESCE(@listPriceTable+', ' ,'') + 
			CONCAT(CAST(CASE WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
				THEN @monListPrice - (@monListPrice * ( decDiscount /100))
			 WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
				THEN @monListPrice - decDiscount
			 WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN @monVendorCost + (@monVendorCost * ( decDiscount /100))
			 WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN @monVendorCost + decDiscount
			 WHEN tintRuleType=3 --Named Price
				THEN decDiscount
			END AS DECIMAL(20,5)), ' (',intFromQty,'-',intToQty,')')
        FROM
			PricingTable PT
		WHERE 
			ISNULL(PT.numItemCode,0)=@numItemCode
			AND ISNULL(numCurrencyID,0) = 0
		ORDER BY 
			[numPricingID]


		IF LEN(@listPriceTable) > 0
		BEGIN
			SET @vcValue = CONCAT(@vcValue,'<br/><b>Price Table</b><br/>',@listPriceTable)
		END
	END

	IF EXISTS 
	(
		SELECT 
			*
		FROM    
			Item I
		JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
		LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
		LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
		LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
		JOIN [PricingTable] PT ON P.numPricRuleID = PT.numPriceRuleID AND ISNULL(PT.numCurrencyID,0) = 0    
		WHERE   
			I.numItemCode = @numItemCode 
			AND P.tintRuleFor=1 
			AND P.tintPricingMethod = 1
			AND 
			(
				((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
				OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
	)
	BEGIN
		DECLARE @listPriceRule VARCHAR(MAX)

		SELECT  
			@listPriceRule = COALESCE(@listPriceRule+', ' ,'') + 
			CONCAT(CAST(CASE WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
						THEN @monListPrice - (@monListPrice * ( PT.decDiscount /100))
					 WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
						THEN @monListPrice - PT.decDiscount
					 WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
						THEN @monVendorCost + (@monVendorCost * ( PT.decDiscount /100))
					 WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
						THEN @monVendorCost + PT.decDiscount END AS DECIMAL(20,5)),' (',PT.intFromQty,'-',PT.intToQty,')')
		FROM    
			Item I
			JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
			LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
			LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
			LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
			LEFT JOIN [PricingTable] PT ON P.numPricRuleID = PT.numPriceRuleID AND ISNULL(PT.numCurrencyID,0) = 0
		WHERE   
			I.numItemCode = @numItemCode 
			AND P.tintRuleFor=1 
			AND P.tintPricingMethod = 1
			AND 
			(
				((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
				OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
				OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
			)
		ORDER BY 
			PP.Priority ASC

		IF LEN(@listPriceRule) > 0
		BEGIN
			SET @vcValue = CONCAT(@vcValue,'<br/><b>Price Rule</b><br/>',@listPriceRule)
		END
	END

   RETURN @vcValue
END
GO