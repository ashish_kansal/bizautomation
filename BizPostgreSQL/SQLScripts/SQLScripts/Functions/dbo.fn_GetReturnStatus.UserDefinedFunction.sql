
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetReturnStatus]    Script Date: 01/22/2009 02:14:30 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getreturnstatus')
DROP FUNCTION fn_getreturnstatus
GO
CREATE FUNCTION [dbo].[fn_GetReturnStatus]
(@numOppItemCode BIGINT) 
RETURNS VARCHAR(20)
AS
BEGIN

DECLARE @Status VARCHAR(20);

SELECT  @Status = ISNULL(dbo.[GetListIemName]([numReturnStatus]),'') 
FROM [Returns] WHERE [numOppItemCode] = @numOppItemCode



  RETURN @Status
END
