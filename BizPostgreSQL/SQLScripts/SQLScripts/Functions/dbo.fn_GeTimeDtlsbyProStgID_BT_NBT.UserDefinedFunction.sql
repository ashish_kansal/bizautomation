/****** Object:  UserDefinedFunction [dbo].[fn_GeTimeDtlsbyProStgID]    Script Date: 07/26/2008 18:12:37 ******/

GO

GO
--tintType=1(Billable), 2 then non billable
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getimedtlsbyprostgid_bt_nbt')
DROP FUNCTION fn_getimedtlsbyprostgid_bt_nbt
GO
CREATE FUNCTION [dbo].[fn_GeTimeDtlsbyProStgID_BT_NBT](@numProID numeric, @numProStageID numeric,@tintType tinyint)            
 RETURNS varchar(100)             
AS            
BEGIN            
 DECLARE @BT varchar (250)  
 DECLARE @NBT varchar (250)            
          
 
--Now show sumtoal of amount and toal hours for selected stage
SELECT @BT= 'Billable Time (' +  CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 2), ( CONVERT(DECIMAL(10, 2), CONVERT(DECIMAL(10, 2),SUM( DATEDIFF(minute, dtFromDate, dtToDate)))
                                                        / 60) ))) + ')' 
				              
from  timeandexpense    
where 
numProID=@numProID 
AND (numStageID = @numProStageID OR @numProStageID =0)
--and numStageID=@numProStageID
 AND numCategory = 1 and tintTEType = 2
AND numType=1
GROUP BY numType

IF @BT IS NULL
BEGIN
	SET @BT='Billable Time (0)'
END

SELECT @NBT= ' Non Billable Time (' + CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 2), ( CONVERT(DECIMAL(10, 2), CONVERT(DECIMAL(10, 2),SUM( DATEDIFF(minute, dtFromDate, dtToDate)))
                                                        / 60) )))  + ')'             
from  timeandexpense    
where 
numProID=@numProID 
AND (numStageID = @numProStageID OR @numProStageID =0)
--and numStageID=@numProStageID
 AND numCategory = 1 and tintTEType = 2
AND numType=2
GROUP BY numType
     
IF @NBT IS NULL
BEGIN
	SET @NBT=' Non Billable Time (0)'
END
       
 RETURN @BT + @NBT           
END
GO
