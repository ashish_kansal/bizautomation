GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='tf'AND NAME ='fn_GetPayrollEmployeeExpense')
DROP FUNCTION fn_GetPayrollEmployeeExpense
GO
CREATE FUNCTION [dbo].[fn_GetPayrollEmployeeExpense] 
(
    @numDomainID NUMERIC(18,0)
	,@numUserCntID INT
	,@dtFromDate DATE
	,@dtToDate DATE
	,@ClientTimeZoneOffset INT
)
RETURNS @ItemPrice TABLE
(	
	numRecordID NUMERIC(18,0)
	,tintRecordType TINYINT
	,vcSource VARCHAR(200)
	,dtDate DATE
	,vcDate VARCHAR(50)
	,numType TINYINT
	,vcType VARCHAR(50)
	,monExpense DECIMAL(20,5)
	,vcDetails VARCHAR(MAX)
	,numApprovalComplete INT
)
AS 
BEGIN

	-- Time & Expense
	INSERT INTO @ItemPrice
	(
		numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,monExpense
		,vcDetails
		,numApprovalComplete
	)
	SELECT
		TimeAndExpense.numCategoryHDRID
		,1
		,'Expense' AS vcSource
		,dtFromDate
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,dtFromDate),@numDomainID) AS vcDate
		,numType
		,(CASE 
			WHEN TimeAndExpense.numType = 1 THEN 'Billable' 
			WHEN TimeAndExpense.numType = 2 THEN 'Reimbursable' 
			WHEN TimeAndExpense.numType = 6 Then 'Billable + Reimbursable' 
			ELSE '' 
		END) AS vcType
		,ISNULL(monAmount,0)
		,ISNULL(TimeAndExpense.txtDesc,'') vcDetails
		,ISNULL(numApprovalComplete,0) numApprovalComplete
	FROM
		TimeAndExpense 
	LEFT JOIN
		OpportunityMaster
	ON
		TimeAndExpense.numOppID=OpportunityMaster.numOppID
	WHERE
		TimeAndExpense.numDomainID = @numDomainID
		AND TimeAndExpense.numUserCntID=@numUserCntID
		AND ISNULL(numCategory,0) = 2
		AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,dtFromDate) AS Date) BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		dtFromDate

	
	RETURN 
END
GO