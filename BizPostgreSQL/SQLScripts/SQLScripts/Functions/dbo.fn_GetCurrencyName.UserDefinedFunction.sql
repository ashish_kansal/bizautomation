GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetCurrencyName')
DROP FUNCTION fn_GetCurrencyName
GO
CREATE FUNCTION [dbo].[fn_GetCurrencyName](@numCurrencyID numeric)
Returns varchar(100) 
As
BEGIN
	declare @varCurrSymbol as varchar(100)

	Select @varCurrSymbol=isnull(varCurrSymbol,'') from Currency where numCurrencyID=@numCurrencyID

return isnull(@varCurrSymbol,'-')
END
GO
