/****** Object:  UserDefinedFunction [dbo].[fn_getOppPreviousStage]    Script Date: 07/26/2008 18:12:41 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getopppreviousstage')
DROP FUNCTION fn_getopppreviousstage
GO
CREATE FUNCTION [dbo].[fn_getOppPreviousStage](@numOppId as numeric(9),@numCurrentStageId as numeric(9))
returns numeric(9)
as
begin

	declare @numOppStageId as numeric(9)
	declare @numPrevOppStageId as numeric(9)
	set @numPrevOppStageId =0
	set @numOppStageId =0

	select top 1 @numOppStageId=numOppStageId from OpportunityStageDetails 
	where numOppId = @numOppId and numstagepercentage not in (0,100)

	order by numOppStageId,numstagepercentage

	while @numOppStageId > 0
	begin 

		if @numOppStageId <> @numCurrentStageId	
			set @numPrevOppStageId = @numOppStageId
		if @numOppStageId = @numCurrentStageId
			set @numOppStageId = 0

		if @numOppStageId <> 0
		begin
			select top 1 @numOppStageId=numOppStageId from OpportunityStageDetails 
			where numOppId = @numOppId and numstagepercentage not in (0,100) and  numOppStageId>@numOppStageId
			order by numOppStageId,numstagepercentage
			 if @@rowcount=0 set @numOppStageID=0 
		end

	end

	return  @numPrevOppStageId

end
GO
