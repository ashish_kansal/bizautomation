/****** Object:  UserDefinedFunction [dbo].[GetCreditTerms]    Script Date: 07/26/2008 18:12:58 ******/

GO

GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcreditterms')
DROP FUNCTION getcreditterms
GO
CREATE FUNCTION [dbo].[GetCreditTerms](@numOppId numeric)
returns varchar(100)
as
begin
declare @strSQl as varchar(100)
declare @bitBillingTerms as bit
declare @intDays as varchar(10)
declare @InterestType as varchar(10)
declare @Interest as varchar(10)
select @intDays=dbo.fn_GetListItemName(OM.intBillingDays),@bitBillingTerms=OM.bitBillingTerms,
@InterestType=OM.bitInterestType,
@Interest=OM.fltInterest from dbo.OpportunityMaster OM where OM.numOppId=@numOppId

if @bitBillingTerms=1
begin
	set @strSQl='Net '+isnull(@intDays,0)
	if @InterestType=0 set @strSQl=@strSQl+', -'
	else set @strSQl=@strSQl+', +'
	set @strSQl=@strSQl+isnull(@Interest,0)
end


return isnull(@strSQl,'-')
end
GO
