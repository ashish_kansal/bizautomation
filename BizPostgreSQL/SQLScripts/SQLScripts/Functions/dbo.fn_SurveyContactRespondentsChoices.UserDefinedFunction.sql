IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_SurveyContactRespondentsChoices')
DROP FUNCTION fn_SurveyContactRespondentsChoices
GO


CREATE FUNCTION [fn_SurveyContactRespondentsChoices]
    (
      @numSurId AS NUMERIC,
      @numRespondantID AS NUMERIC,
      @numQuestionID AS NUMERIC
    )
RETURNS VARCHAR(MAX)
BEGIN

    DECLARE @listStr VARCHAR(MAX)
 
                 
    SELECT  @listStr = COALESCE(@listStr + ',<br />', '') + vcAnsLabel
    FROM  (SELECT DISTINCT
                        CASE WHEN sqm.tintAnsType = 2 THEN sr.vcAnsText
                             ELSE CASE WHEN sr.numMatrixID = 0
                                       THEN sam.vcAnsLabel
                                       ELSE sam.vcAnsLabel + '('
                                            + dbo.fn_SurveyMatrixAns(@numSurId, @numQuestionID, @numRespondantID, sam.numAnsID)
                                            + ')'
                                  END
                        END AS vcAnsLabel
              FROM      SurveyQuestionMaster sqm,
                        SurveyAnsMaster sam,
                        SurveyResponse sr
              WHERE     sqm.numSurId = sam.numSurId
                        AND sr.numParentSurId = sam.numSurId
                        AND sr.numSurId = @numSurId
                        AND sr.numQuestionID = sqm.numQID
                        AND sr.numQuestionID = sam.numQID
                        AND sr.numAnsID = sam.numAnsID
                        AND sr.numRespondantID = @numRespondantID
                        AND sam.numQID = @numQuestionID
            ) temptable
      
    RETURN @listStr
END