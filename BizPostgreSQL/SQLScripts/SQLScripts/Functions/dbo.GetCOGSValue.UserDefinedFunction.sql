GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCOGSValue')
DROP FUNCTION GetCOGSValue
GO
CREATE FUNCTION GetCOGSValue
				(@numDomainID numeric(9),
				@dtFromDate datetime,
				@dtToDate datetime)
				
RETURNS DECIMAL(20,5)
AS
BEGIN
	DECLARE @monCOGS DECIMAL(20,5);
				
	SELECT @monCOGS=sum(COGS) from View_COGSReport where numDomainID=@numDomainID and dtCreatedDate between @dtFromDate and @dtToDate
			
	RETURN @monCOGS				
END