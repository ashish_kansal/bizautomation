GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetPercentageChangeInGrossProfit')
DROP FUNCTION GetPercentageChangeInGrossProfit
GO
CREATE FUNCTION [dbo].[GetPercentageChangeInGrossProfit]
(
	 @numDomainID AS NUMERIC(18,0),
	 @dtStartDate DATE,
	 @dtEndDate DATE,
	 @tintDateRange TINYINT --1:YTD TO SAME PERIOD LAST YEAR, 2:MTD TO SAME PERIOD LAST YEAR
)    
RETURNS NUMERIC(18,2)
AS    
BEGIN   
	DECLARE @GrossProfitChange NUMERIC(18,2) = 0.00
	DECLARE @GrossProfit1 INT
	DECLARE @GrossProfit2 INT
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT

	SELECT 
		@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=numDiscountServiceItemID
		,@ProfitCost=numCost 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID


	SELECT 
		@GrossProfit1 = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																	WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																	THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																	ELSE (CASE @ProfitCost 
																			WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																			ELSE monAverageCost 
																		END)
																END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OI.numItemCode
				AND OIInner.vcAttrValues = OI.vcAttrValues
		) TEMPPO
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtStartDate AND @dtEndDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	SELECT 
		@GrossProfit2 = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																	WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																	THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																	ELSE (CASE @ProfitCost 
																			WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,I.numItemCode,@numDomainID,numPurchaseUnit) 
																			ELSE ISNULL(OI.monAvgCost,0) 
																		END)
																END)) Profit
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OI.numItemCode
				AND OIInner.vcAttrValues = OI.vcAttrValues
		) TEMPPO
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND 1 = (CASE 
						WHEN @tintDateRange=1 
						THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,@dtStartDate) AND DATEADD(YEAR,-1,@dtEndDate) THEN 1 ELSE 0 END)
						WHEN @tintDateRange=2 
						THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-1,@dtStartDate) AND DATEADD(MONTH,-1,@dtEndDate) THEN 1 ELSE 0 END)
						ELSE 0
					END)
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP


	SET @GrossProfitChange = 100.0 * (ISNULL(@GrossProfit1,0) - ISNULL(@GrossProfit2,0)) / (CASE WHEN ISNULL(@GrossProfit2,0) = 0 THEN 1 ELSE ISNULL(@GrossProfit2,0) END)


	RETURN ISNULL(@GrossProfitChange,0.00)
END
GO