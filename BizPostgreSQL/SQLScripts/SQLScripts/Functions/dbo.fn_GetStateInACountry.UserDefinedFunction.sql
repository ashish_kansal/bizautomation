/****** Object:  UserDefinedFunction [dbo].[fn_GetStateInACountry]    Script Date: 07/26/2008 18:12:45 ******/

GO

GO
--Created By: Debasish Nag      
--Purpose Created: To return a comma separated list of either States or state ids      
--Date Created: 21st Nov 2005      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getstateinacountry')
DROP FUNCTION fn_getstateinacountry
GO
CREATE FUNCTION [dbo].[fn_GetStateInACountry](@numCountryID Numeric, @numDomainID Numeric, @returnType Char(2))      
 RETURNS Nvarchar(4000)       
AS      
--Thsi function will take the ID for the Country and return the states or the state ids      
BEGIN      
 DECLARE @vcRetValue Nvarchar (4000)      
 IF @returnType = 'NM'      
 BEGIN      
  SELECT @vcRetValue=COALESCE(@vcRetValue + ',','') + '''' + Replace(Replace(vcState,Char(13),''),Char(10),'') + ''''      
  FROM dbo.AllCountriesAndStates      
  WHERE numCountryID = @numCountryID      
  AND (numDomainID = @numDomainID OR constFlag = 1)
 END      
 ELSE      
 BEGIN      
  SELECT @vcRetValue=COALESCE(@vcRetValue + ',','') + '''' + CAST(numStateID AS NVarchar(10)) + ''''      
  FROM dbo.AllCountriesAndStates      
  WHERE numCountryID = @numCountryID      
  AND (numDomainID = @numDomainID OR constFlag = 1)   
 END      
 RETURN @vcRetValue       
END
GO
