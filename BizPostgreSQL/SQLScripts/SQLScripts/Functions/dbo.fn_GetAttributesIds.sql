
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetAttributesIds')
DROP FUNCTION fn_GetAttributesIds
GO
CREATE FUNCTION [dbo].[fn_GetAttributesIds] (@numRecID numeric)
returns varchar(100)
as
begin
declare @strAttribute varchar(100)
declare @numCusFldID numeric(9)
declare @numCusFldValue varchar(100)
set @numCusFldID=0
set @strAttribute=''
select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' order by fld_id
while @numCusFldID>0
begin
	IF ISNUMERIC(@numCusFldValue) = 1
	BEGIN
		--set @strAttribute=@strAttribute+@numCusFldValue
		select @strAttribute=CONCAT(@strAttribute,Fld_id) from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
                
        IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=CONCAT(@strAttribute,':',numListItemID,',') from ListDetails where numListItemID=@numCusFldValue
        END
	END
	
	SELECT TOP 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and fld_id>@numCusFldID order by fld_id
	if @@rowcount=0 set @numCusFldID=0
	
end

If LEN(@strAttribute) > 0
BEGIN
	SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
END

return @strAttribute
end
GO
