GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemCountBasedOnAttribute')
DROP FUNCTION fn_GetItemCountBasedOnAttribute
GO
CREATE FUNCTION [dbo].[fn_GetItemCountBasedOnAttribute]        
(@fieldId numeric(9),@fieldValueId numeric(9)=0,@numCategoryId numeric(9)=0,@numDomainId numeric(9)=0)        
Returns varchar(10)        
As        
Begin         
    Declare @attributeCount as varchar(10)        
     
     SELECT  @attributeCount = COUNT(*) FROM (  SELECT  DISTINCT     
                     
                     WHI.numItemId ,
                     CFVSI.Fld_ID ,
                     CFVSI.Fld_Value 
          
         FROM 
                     dbo.Item I INNER JOIN dbo.ItemCategory IC ON IC.numItemID = i.numItemCode
         INNER JOIN  dbo.ItemGroupsDTL IGD ON I.numItemGroup = IGD.numItemGroupID
         INNER JOIN  dbo.CFW_Fld_Master CFM ON IGD.numOppAccAttrID = CFM.Fld_id AND CFM.numDomainID= 1
         LEFT JOIN   dbo.ListDetails LD ON LD.numListID = CFM.numlistid AND CFM.numDomainID = LD.numDomainID
         INNER JOIN  dbo.WareHouseItems WHI ON WHI.numItemID = I.numItemCode 
         INNER JOIN  dbo.CFW_Fld_Values_Serialized_Items CFVSI ON CFVSI.RecId = WHI.numWareHouseItemID
                   
         where 
                      ic.numCategoryID = @numCategoryId
                      AND i.numDomainID = @numDomainId
                      AND LD.numListItemID = CFVSI.Fld_Value 
                      AND  CFM.Fld_id  =  CFVSI.Fld_ID
                      AND  CFM.Fld_id = @fieldId
                      AND  LD.numListItemID = @fieldValueId 
         GROUP BY     WHI.numItemId, CFVSI.Fld_ID ,
                      CFVSI.Fld_Value 
                ) X    
 Return @attributeCount        
END
 
 


