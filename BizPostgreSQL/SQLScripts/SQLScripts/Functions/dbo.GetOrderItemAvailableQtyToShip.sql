GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderItemAvailableQtyToShip')
DROP FUNCTION GetOrderItemAvailableQtyToShip
GO
CREATE FUNCTION [dbo].[GetOrderItemAvailableQtyToShip] 
(	
	@numOppItemCode AS NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitKitParent BIT,
	@tintCommitAllocation TINYINT,
	@bitAllocateInventoryOnPickList BIT
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @numQuantity AS FLOAT = 0
    DECLARE @numAvailableQtyToShip FLOAT = 0
	DECLARE @numOnHand FLOAT
	DECLARE @numAllocation FLOAT
	DECLARE @numQtyShipped FLOAT = 0
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numWarehouseID NUMERIC(18,0)

	SELECT @numQuantity=ISNULL(numUnitHour,0),@numQtyShipped = ISNULL(numQtyShipped,0) FROM OpportunityItems WHERE numoppitemtCode = @numOppItemCode
	SELECT @numItemCode=numItemID,@numWarehouseID = numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID

	IF @bitKitParent = 1
	BEGIN
		DECLARE @TEMPTABLE TABLE
		(
			numItemCode NUMERIC(18,0),
			numParentItemID NUMERIC(18,0),
			numOppChildItemID NUMERIC(18,0),
			vcItemName VARCHAR(300),
			bitKitParent BIT,
			numWarehouseItmsID NUMERIC(18,0),
			numQtyItemsReq FLOAT,
			numQtyItemReq_Orig FLOAT,
			numOnHand FLOAT,
			numAllocation FLOAT,
			numAvailableQtyToShip FLOAT,
			numWOID NUMERIC(18,0)
		)

		INSERT INTO 
			@TEMPTABLE
		SELECT
			OKI.numChildItemID,
			0,
			OKI.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKI.numWareHouseItemId,
			OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
			OKI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0) + ISNULL((SELECT 
														SUM(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0))
													FROM 
														WareHouseItems WIInner
													WHERE 
														WIInner.numItemID=WI.numItemID 
														AND WIInner.numWareHouseID = WI.numWareHouseID
														AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),0),
			0,
			ISNULL((SELECT numWOId FROM WorkOrder WHERE numOppId=OKi.numOppId AND numOppItemID=OKi.numOppItemID AND numOppChildItemID=OKI.numOppChildItemID AND ISNULL(numOppKitChildItemID,0) = 0 AND ISNULL(numParentWOID,0)=0),0)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		WHERE
			OI.numoppitemtCode = @numOppItemCode  

		INSERT INTO
			@TEMPTABLE
		SELECT
			OKCI.numItemID,
			t1.numItemCode,
			t1.numOppChildItemID,
			I.vcItemName,
			I.bitKitParent,
			OKCI.numWareHouseItemId,
			OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
			OKCI.numQtyItemsReq_Orig,
			ISNULL(WI.numOnHand,0),
			ISNULL(WI.numAllocation,0) + ISNULL((SELECT 
														SUM(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0))
													FROM 
														WareHouseItems WIInner
													WHERE 
														WIInner.numItemID=WI.numItemID 
														AND WIInner.numWareHouseID = WI.numWareHouseID
														AND WIInner.numWareHouseItemID <> WI.numWareHouseItemID),0),
			0,
			ISNULL((SELECT numWOId FROM WorkOrder WHERE numOppId=OKCI.numOppId AND numOppItemID=OKCI.numOppItemID AND numOppChildItemID=OKCI.numOppChildItemID AND numOppKitChildItemID = OKCI.numOppKitChildItemID AND ISNULL(numParentWOID,0)=0),0)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			@TEMPTABLE t1
		ON
			OKCI.numOppChildItemID = t1.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		
		-- FIRST GET SUB KIT BACK ORDER NUMBER
		IF @tintCommitAllocation=2 AND ISNULL(@bitAllocateInventoryOnPickList,1)=0
		BEGIN
			UPDATE 
				@TEMPTABLE 
			SET 
				numAvailableQtyToShip = (CASE 
											WHEN ISNULL(numWOID,0) > 0
											THEN
												(CASE 
													WHEN ISNULL(numOnHand,0) = 0 THEN 0
													WHEN  dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)>= numQtyItemsReq THEN (numQtyItemsReq/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
													ELSE FLOOR(dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
												END)
											ELSE
												(CASE 
													WHEN ISNULL(numOnHand,0) = 0 THEN 0
													WHEN  numOnHand>= numQtyItemsReq THEN (numQtyItemsReq/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
													ELSE FLOOR(ISNULL(numOnHand,0)/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
												END)
										END)
			WHERE
				bitKitParent = 0
		END
		ELSE
		BEGIN
			UPDATE 
				@TEMPTABLE 
			SET 
				numAvailableQtyToShip = (CASE 
											WHEN ISNULL(numWOID,0) > 0
											THEN
												(CASE 
													WHEN ISNULL(numOnHand,0) = 0 THEN 0
													WHEN  dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)>= numQtyItemsReq THEN (numQtyItemsReq/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
													ELSE FLOOR(dbo.GetWorkOrderQtyReadyToBuild(numWOID,numQtyItemsReq)/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
												END)
											ELSE
												(CASE 
													WHEN ISNULL(numAllocation,0) = 0 THEN 0
													WHEN  numAllocation>= numQtyItemsReq THEN (numQtyItemsReq/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
													ELSE FLOOR(ISNULL(numAllocation,0)/(CASE WHEN ISNULL(numQtyItemReq_Orig,0) > 0 THEN numQtyItemReq_Orig ELSE 1 END))
												END)
										END)
			WHERE
				bitKitParent = 0
		END

		
		-- NOW USE SUB KIT BACK ORDER NUMBER TO GENERATE PARENT KIT BACK ORDER NUMBER
		UPDATE 
			t2
		SET 
			t2.numAvailableQtyToShip = CEILING((SELECT MAX(numAvailableQtyToShip) FROM @TEMPTABLE t1 WHERE t1.numParentItemID = t2.numItemCode) / (CASE WHEN ISNULL(t2.numQtyItemReq_Orig,0) > 0 THEN t2.numQtyItemReq_Orig ELSE 1 END))
		FROM
			@TEMPTABLE t2
		WHERE
			bitKitParent = 1

		IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE bitKitParent=0) > 0
		BEGIN
			SELECT @numAvailableQtyToShip = MIN(numAvailableQtyToShip) FROM @TEMPTABLE WHERE ISNULL(numParentItemID,0) = 0
		END
		ELSE
		BEGIN
			-- KIT WITH ALL CHILD ITEMS OF TYPE NON INVENTORY OR SERVICE ITEMS
			SET @numAvailableQtyToShip = @numQuantity
		END
	END
	ELSE
	BEGIN
		IF @tintCommitAllocation=2 AND ISNULL(@bitAllocateInventoryOnPickList,1)=0
		BEGIN
			SELECT
				@numOnHand = SUM(numOnHand)
			FROM
				WareHouseItems 
			WHERE
				numItemID=@numItemCode 
				AND numWareHouseID = @numWarehouseID

			IF (@numQuantity - @numQtyShipped) >= @numOnHand
				SET @numAvailableQtyToShip = @numOnHand
			ELSE
				SET @numAvailableQtyToShip = (@numQuantity - @numQtyShipped)
		END
		ELSE
		BEGIN
			SELECT
				@numAllocation = numAllocation
			FROM
				WareHouseItems 
			WHERE
				numWareHouseItemID = @numWarehouseItemID

		SET @numAllocation = ISNULL(@numAllocation,0) + ISNULL((SELECT 
															SUM(ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0))
														FROM 
															WareHouseItems 
														WHERE 
															numItemID=@numItemCode 
															AND numWareHouseID = @numWarehouseID
															AND numWareHouseItemID <> @numWarehouseItemID),0)

			IF (@numQuantity - @numQtyShipped) >= @numAllocation
				SET @numAvailableQtyToShip = @numAllocation
			ELSE
				SET @numAvailableQtyToShip = (@numQuantity - @numQtyShipped)
		END
	END

	RETURN @numAvailableQtyToShip
END
GO
