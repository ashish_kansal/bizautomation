     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_GetItemPriceRule')
DROP FUNCTION fn_GetItemPriceRule
GO
CREATE FUNCTION [dbo].[fn_GetItemPriceRule]
(  @numDomainID AS NUMERIC(9) = 0,
    @tintRuleFor AS TINYINT=0,
    @numItemCode AS NUMERIC(9)=0,
    @numDivisionID AS NUMERIC(9)=0
)                                 
RETURNS 

@PriceBookRules table
(
	numPricRuleID NUMERIC(9)
)                               
As                                  
Begin          

declare @numRelationship as numeric(9)                  
declare @numProfile as numeric(9)                  

/*Profile and relationship id */
select @numRelationship=numCompanyType,@numProfile=vcProfile from DivisionMaster D                  
join CompanyInfo C on C.numCompanyId=D.numCompanyID                  
where numDivisionID =@numDivisionID       

INSERT INTO @PriceBookRules

SELECT P.numPricRuleID
FROM    Item I
        LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2]
                                              AND PP.[Step3Value] = P.[tintStep3]
                                              
WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintRuleFor AND I.numDomainID=@numDomainId
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)

RETURN 

END