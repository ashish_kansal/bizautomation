GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCustFldValueOpp')
DROP FUNCTION GetCustFldValueOpp
GO
CREATE FUNCTION [dbo].[GetCustFldValueOpp]
(
	@numFldId NUMERIC(9),
	@numRecordId AS NUMERIC(9)
)
RETURNS VARCHAR (MAX) 
AS
BEGIN

	DECLARE @vcValue AS VARCHAR(MAX)
	DECLARE @fld_Type AS VARCHAR(50)
	DECLARE @numListID AS NUMERIC(9)
	SET @vcValue=''

    SELECT 
		@numListID=numListID,
		@fld_Type=Fld_type 
	FROM
		CFW_Fld_Master 
	WHERE 
		Fld_id=@numFldId

    SELECT 
		@vcValue=ISNULL(Fld_Value,0) 
	FROM 
		CFW_Fld_Values_Opp 
	WHERE 
		Fld_ID=@numFldId 
		AND RecId=@numRecordId
    
	IF (@fld_Type='TextBox' or @fld_Type='TextArea')
    BEGIN
		IF @vcValue='' SET @vcValue='-'
    END
    ELSE IF @fld_Type='SelectBox'
    BEGIN
		IF @vcValue='' 
			SET @vcValue='-'
		ELSE	
	   		SET @vcValue=dbo.GetListIemName(@vcValue)
	END 
    ELSE IF @fld_Type='CheckBox' 
	BEGIN
		SET @vcValue= (CASE WHEN @vcValue=0 THEN 'No' ELSE 'Yes' END)
	END
	ELSE IF @fld_type = 'CheckBoxList'
	BEGIN
		SELECT 
			@vcValue = STUFF((SELECT CONCAT(',', vcData) 
		FROM 
			ListDetails 
		WHERE 
			numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(@vcValue,',')) FOR XML PATH('')), 1, 1, '')
	END

	IF @vcValue IS NULL SET @vcValue=''

	RETURN @vcValue
END
GO
