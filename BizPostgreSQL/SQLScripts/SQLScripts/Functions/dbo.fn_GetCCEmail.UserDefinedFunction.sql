/****** Object:  UserDefinedFunction [dbo].[fn_GetCCEmail]    Script Date: 07/26/2008 18:12:30 ******/

GO

GO
--select dbo.fn_GetCCEmail(12)
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getccemail')
DROP FUNCTION fn_getccemail
GO
CREATE FUNCTION [dbo].[fn_GetCCEmail](@numAlertDTLID numeric,@numDomainId as numeric(9))
returns varchar(8000)
as
begin
declare @Email as varchar(8000)
declare @vcEmail as varchar(100)
declare @numAlertEmailID as varchar(15)
set @Email=''
set @vcEmail=''
select top 1 @numAlertEmailID=numAlertEmailID,@vcEmail=isnull(vcEmailID,'') from AlertEmailDTL 
where numAlertDTLId=@numAlertDTLID and numDomainID=@numDomainId

while @numAlertEmailID>0 
begin
set @Email=@Email+@vcEmail

select top 1 @numAlertEmailID=numAlertEmailID,@vcEmail=isnull(vcEmailID,'') from AlertEmailDTL 
where numAlertDTLId=@numAlertDTLID and numDomainID=@numDomainId and numAlertEmailID>@numAlertEmailID

if @@rowcount= 0 set @numAlertEmailID=0 else set @Email=@Email+' ; '
end

return isnull(@Email,'')

end
GO
