
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetEmployeeName]    Script Date: 02/19/2010 17:22:39 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getemployeename')
DROP FUNCTION fn_getemployeename
GO
CREATE FUNCTION [dbo].[fn_GetEmployeeName]
(@numBizDocAppID numeric(8),
 @numDomainId int)
Returns varchar(2000)
AS 
begin

declare @vcEmployee varchar(2000);
declare @vcUserName varchar(100);


declare Employee_Cursor CURSOR FOR
	
select ACI.vcFirstName+' '+ACI.vcLastName as vcUserName 
 from AdditionalContactsInformation ACI
	INNER JOIN BizDocApprovalRuleEmployee BAR
	ON BAR.numEmployeeID = ACI.numContactID AND 
	BAR.numBizDocAppID=@numBizDocAppID

set @vcEmployee=''

OPEN Employee_Cursor

FETCH NEXT FROM Employee_Cursor into @vcUserName
WHILE @@FETCH_STATUS = 0
   BEGIN
		set @vcEmployee= @vcEmployee + ',' + @vcUserName
		

      FETCH NEXT FROM Employee_Cursor into @vcUserName
   END;

CLOSE Employee_Cursor;
DEALLOCATE Employee_Cursor;

RETURN substring(@vcEmployee,2,len(@vcEmployee))

end