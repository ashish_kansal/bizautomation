GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetDateTimeDifference')
DROP FUNCTION fn_GetDateTimeDifference
GO

CREATE Function [dbo].[fn_GetDateTimeDifference]
(
@StartDateTime DateTime,
@EndDateTime DateTime
) Returns Varchar(50) 
As
Begin
Declare @Seconds Int,
@Minute INT,
@Hour INT,
@Day INT,
@Week INT,
@month INT,
@Elapsed Varchar(50)
SET @Elapsed =''
Select @Seconds = ABS(DateDiff(SECOND ,@StartDateTime,@EndDateTime))

If @Seconds >= 60 
Begin
select @Minute = @Seconds/60
select @Seconds = @Seconds%60

If @Minute >= 60
begin
select @Hour = @Minute/60
select @Minute = @Minute%60
end

if @Hour >= 24
begin
SELECT @Day = @Hour/24
SELECT @Hour = @Hour%24
end

IF @Day >= 7
BEGIN
SELECT @Week = @Day/7
SELECT @Day = @Day%7
END

Else
Goto Final 
End


Final:
--IF @Week > 0 
--BEGIN
--IF  @Week > 1 
--		BEGIN
--				SET @Elapsed = @Elapsed  + Cast(@Week as Varchar) + ' Weeks '
--		END
--	ELSE
--		BEGIN
--				SET @Elapsed = @Elapsed  + Cast(@Week as Varchar) + ' Week '
--		END
--END
--ELSE
IF @Day > 0 
BEGIN
IF  @Day > 1 
		BEGIN
				SET @Elapsed = @Elapsed  + Cast(@Day as Varchar) + ' Days '
		END
	ELSE
		BEGIN
				SET @Elapsed = @Elapsed  + Cast(@Day as Varchar) + ' Day '
		END
END

ELSE
IF @Hour > 0 
BEGIN
IF  @Hour > 1 
		BEGIN
				SET @Elapsed = @Elapsed  + Cast(@Hour as Varchar) + ' Hours '

		END
	ELSE
		BEGIN
				SET @Elapsed = @Elapsed  + Cast(@Hour as Varchar) + ' Hour '
		END

END
ELSE
IF @Minute > 0 
BEGIN
	IF  @Minute > 1 
		BEGIN
			SET @Elapsed = @Elapsed  + Cast(@Minute as Varchar) + ' Minutes '

		END
	ELSE
		BEGIN
			SET @Elapsed = @Elapsed  + Cast(@Minute as Varchar) + ' Minute '
		END
END

Return (@Elapsed)
End
