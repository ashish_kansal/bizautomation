/****** Object:  UserDefinedFunction [dbo].[fn_GetReortFieldsData]    Script Date: 07/26/2008 18:12:44 ******/

GO

GO
-- ALTER  By Anoop jayaraj
--select dbo.fn_GetReortFieldsData(1)

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getreortfieldsdata')
DROP FUNCTION fn_getreortfieldsdata
GO
CREATE FUNCTION [dbo].[fn_GetReortFieldsData]()  
 RETURNS varchar(8000)
as
BEGIN  
declare @strFields as varchar(8000)
declare @value as varchar(15)
declare @text as varchar(100)
declare @fldType as varchar(1)
set @strFields=''
select  top 1 @value=numRepFieldID,@text=vcFieldValue,@fldType=tintFldType 
from ReportFields
 where tintSelected=1 
while @value>0
begin
if @fldType=1 set @text='isnull(dbo.fn_GetListItemName('+@text+'),''-'')'
if @fldType=2 set @text='isnull(dbo.fn_GetTerritoryName('+@text+'),''-'')'
set @strFields=@strFields+@text+'+'', ''+'

select  top 1 @value=numRepFieldID,@text=vcFieldValue,@fldType=tintFldType 
from ReportFields
where tintSelected=1 and numRepFieldID>@value
if @@rowcount=0 set @value=0
end
set @strFields=@strFields+'''- '''



return @strFields

END
GO
