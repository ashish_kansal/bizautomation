GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetOrderItemProfitAmountOrMargin')
DROP FUNCTION GetOrderItemProfitAmountOrMargin
GO
CREATE FUNCTION [dbo].[GetOrderItemProfitAmountOrMargin]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@tintMode TINYINT --1: Profit Amount, 2:Profit Margin
)    
RETURNS DECIMAL(20,5)    
AS    
BEGIN  
	DECLARE @Value AS DECIMAL(20,5)

	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	If @tintMode = 1
	BEGIN
		SELECT
			@Value = SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																				WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																				THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																				ELSE
																					(CASE @ProfitCost 
																						WHEN 3 THEN ISNULL(TEMP.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,TEMP.numItemCode,@numDomainID,numPurchaseUnit) 
																						ELSE ISNULL(TEMP.monAverageCost,0) 
																					END)
																			END)))
		FROM
		(SELECT
			OM.numOppId
			,OI.numoppitemtCode
			,I.numItemCode
			,I.numBaseUnit
			,I.numPurchaseUnit
			,OI.vcAttrValues
			,OI.numUnitHour
			,OI.monTotAmount
			,V.monCost
			,(CASE 
				WHEN ISNULL(I.bitKitParent,0) = 1 
				THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
						ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
				ELSE ISNULL(OI.monAvgCost,0) 
			END) monAverageCost
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numOppId = @numOppID
			AND OI.numoppitemtCode = @numOppItemID
			AND I.numItemCode NOT IN (@numDiscountItemID)
		) TEMP
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = TEMP.numOppId
				AND SOLIPL.numSalesOrderItemID = TEMP.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = TEMP.numOppId
				AND OIInner.numItemCode = TEMP.numItemCode
				AND ISNULL(OIInner.vcAttrValues,'') = ISNULL(TEMP.vcAttrValues,'')
		) TEMPPO
		
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			@Value = (SUM(Profit)/CASE WHEN SUM(monTotAmount) = 0 THEN 1.0 ELSE SUM(monTotAmount) END) * 100
		FROM
		(
			SELECT
				ISNULL(monTotAmount,0) monTotAmount
				,ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * (CASE 
																		WHEN ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) IS NOT NULL 
																		THEN ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
																		ELSE (CASE @ProfitCost 
																				WHEN 3 THEN ISNULL(TEMP.monCost,0) * dbo.fn_UOMConversion(numBaseUnit,TEMP.numItemCode,@numDomainID,numPurchaseUnit) 
																				ELSE ISNULL(TEMP.monAverageCost,0) 
																				END)
																	END)) Profit
			FROM
			(SELECT
				OM.numOppId
				,OI.numoppitemtCode
				,I.numItemCode
				,I.numBaseUnit
				,I.numPurchaseUnit
				,OI.vcAttrValues
				,OI.numUnitHour
				,OI.monTotAmount
				,V.monCost
				,(CASE 
					WHEN ISNULL(I.bitKitParent,0) = 1 
					THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
							ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OM.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
					ELSE ISNULL(OI.monAvgCost,0) 
				END) monAverageCost
			FROM
				OpportunityItems OI
			INNER JOIN
				OpportunityMaster OM
			ON
				OI.numOppId = OM.numOppID
			INNER JOIN
				DivisionMaster DM
			ON
				OM.numDivisionId = DM.numDivisionID
			INNER JOIN
				CompanyInfo CI
			ON
				DM.numCompanyID = CI.numCompanyId
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			Left JOIN 
				Vendor V 
			ON 
				V.numVendorID=I.numVendorID 
				AND V.numItemCode=I.numItemCode
			WHERE
				OM.numDomainId=@numDomainID
				AND OM.numOppId = @numOppID
				AND OI.numoppitemtCode = @numOppItemID
				AND I.numItemCode NOT IN (@numDiscountItemID)
			) TEMP
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					SalesOrderLineItemsPOLinking SOLIPL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					SOLIPL.numPurchaseOrderID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
					AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
				WHERE
					SOLIPL.numSalesOrderID = TEMP.numOppId
					AND SOLIPL.numSalesOrderItemID = TEMP.numoppitemtCode
			) TEMPMatchedPO
			OUTER APPLY
			(
				SELECT TOP 1
					OMInner.numOppID
					,OMInner.vcPOppName
					,OIInner.numoppitemtCode
					,OIInner.monPrice
				FROM
					OpportunityLinking OL
				INNER JOIN
					OpportunityMaster OMInner
				ON
					OL.numChildOppID = OMInner.numOppId
				INNER JOIN
					OpportunityItems OIInner
				ON
					OMInner.numOppId = OIInner.numOppId
				WHERE
					OL.numParentOppID = TEMP.numOppId
					AND OIInner.numItemCode = TEMP.numItemCode
					AND OIInner.vcAttrValues = TEMP.vcAttrValues
			) TEMPPO
			
		) TEMP
	END

	RETURN CAST(@Value AS DECIMAL(20,5))
END
GO