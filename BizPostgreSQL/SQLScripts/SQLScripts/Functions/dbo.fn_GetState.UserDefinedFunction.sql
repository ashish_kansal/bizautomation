/****** Object:  UserDefinedFunction [dbo].[fn_GetState]    Script Date: 07/26/2008 18:12:44 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getstate')
DROP FUNCTION fn_getstate
GO
CREATE FUNCTION [dbo].[fn_GetState](@numValueID numeric)
	RETURNS varchar(100) 
AS
--Thsi function will take the ID fo rthe Territory or the List Details and return the corresponding name.
BEGIN
	DECLARE @vcRetValue varchar (250)


		SELECT @vcRetValue=vcState 
			FROM State
			WHERE numStateID=@numValueID
	RETURN @vcRetValue	
END
GO
