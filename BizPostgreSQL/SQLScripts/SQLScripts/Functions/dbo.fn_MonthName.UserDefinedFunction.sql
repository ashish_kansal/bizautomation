/****** Object:  UserDefinedFunction [dbo].[fn_MonthName]    Script Date: 07/26/2008 18:12:48 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_monthname')
DROP FUNCTION fn_monthname
GO
CREATE FUNCTION [dbo].[fn_MonthName] (@intMonth int)
	returns varchar (10) 
BEGIN
	RETURN
	CASE
		WHEN @intMonth = 1 THEN 'January'
		WHEN @intMonth = 2 THEN 'February'
		WHEN @intMonth = 3 THEN 'March'
		WHEN @intMonth = 4 THEN 'April'
		WHEN @intMonth = 5 THEN 'May'
		WHEN @intMonth = 6 THEN 'June'
		WHEN @intMonth = 7 THEN 'July'
		WHEN @intMonth = 8 THEN 'August'
		WHEN @intMonth = 9 THEN 'September'
		WHEN @intMonth = 10 THEN 'October'
		WHEN @intMonth = 11 THEN 'November'
		WHEN @intMonth = 12 THEN 'December'
	END
END
GO
