/****** Object:  UserDefinedFunction [dbo].[fnGetUserOppAmt]    Script Date: 07/26/2008 18:12:52 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fngetuseroppamt')
DROP FUNCTION fngetuseroppamt
GO
CREATE FUNCTION [dbo].[fnGetUserOppAmt]
	(@numContactID numeric)
RETURNS DECIMAL(20,5) 

AS
BEGIN
	Declare @monTotal DECIMAL(20,5)
	SELECT @monTotal=SUM(monPAmount) 
	FROM OpportunityMaster
	WHERE numContactId=@numContactID

	RETURN @monTotal
END
GO
