/****** Object:  UserDefinedFunction [dbo].[fn_GetListName]    Script Date: 07/26/2008 18:12:39 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getlistname')
DROP FUNCTION fn_getlistname
GO
CREATE FUNCTION [dbo].[fn_GetListName](@numValueID numeric, @bitTerritory bit=0)
	RETURNS varchar(100) 
AS
--Thsi function will take the ID fo rthe Territory or the List Details and return the corresponding name.
BEGIN
	DECLARE @vcRetValue varchar (250)
	SET @vcRetValue=''

	IF @bitTerritory=0
	BEGIN
		SELECT @vcRetValue=vcData 
			FROM ListDetails
			WHERE numListItemID=@numValueID
	END
	ELSE
	BEGIN
		SELECT @vcRetValue=vcTerName 
			FROM TerritoryMaster
			WHERE numTerID=@numValueID
	END
	RETURN @vcRetValue	
END
GO
