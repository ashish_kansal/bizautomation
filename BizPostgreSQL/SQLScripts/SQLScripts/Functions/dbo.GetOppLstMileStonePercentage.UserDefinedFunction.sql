/****** Object:  UserDefinedFunction [dbo].[GetOppLstMileStonePercentage]    Script Date: 07/26/2008 18:13:05 ******/

GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'fn'
                    AND NAME = 'getopplstmilestonepercentage' ) 
    DROP FUNCTION getopplstmilestonepercentage
GO
CREATE FUNCTION [dbo].[GetOppLstMileStonePercentage] ( @numoppid NUMERIC(9) )
RETURNS VARCHAR(15)
AS 
BEGIN
/*Don't know exact purpose of old function so changin it to show total progress of Business process */
    DECLARE @vcPercentage AS VARCHAR(15)
    SELECT @vcPercentage = intTotalProgress FROM dbo.ProjectProgress WHERE numOppId=@numoppid
    IF @vcPercentage IS NULL 
        SET @vcPercentage = 0
    RETURN @vcPercentage
END
GO
