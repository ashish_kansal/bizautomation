GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getserialnumberlist')
DROP FUNCTION getserialnumberlist
GO
CREATE FUNCTION dbo.GetSerialNumberList
    (
      @numOppItemtCode NUMERIC
    )
RETURNS VARCHAR(2000)
AS BEGIN
    DECLARE @SerialList VARCHAR(2000)


    SELECT  @SerialList = COALESCE(@SerialList + ', ', '') + ISNULL(vcSerialNo,'')
    FROM    WareHouseItmsDTL WID
            LEFT JOIN [OppWarehouseSerializedItem] Ser ON WID.[numWareHouseItmsDTLID] = Ser.[numWarehouseItmsDTLID]
    WHERE   Ser.[numOppItemID] = @numOppItemtCode
        
    RETURN ISNULL(@SerialList, '')
   END 