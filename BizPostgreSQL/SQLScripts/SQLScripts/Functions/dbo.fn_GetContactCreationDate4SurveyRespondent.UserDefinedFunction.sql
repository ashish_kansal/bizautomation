/****** Object:  UserDefinedFunction [dbo].[fn_GetContactCreationDate4SurveyRespondent]    Script Date: 07/26/2008 18:12:31 ******/

GO

GO
--Created By: Debasish Nag    
--Created On: 29th Sept 2005    
--Purpose Created: Returns the Date when the contact was created for Survey Report    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getcontactcreationdate4surveyrespondent')
DROP FUNCTION fn_getcontactcreationdate4surveyrespondent
GO
CREATE FUNCTION [dbo].[fn_GetContactCreationDate4SurveyRespondent] (@vcEmail Nvarchar(250))    
 RETURNS NVarchar(20)    
AS    
BEGIN    
 DECLARE @dtRetVal NVarchar(20)    
 SELECT Top 1 @dtRetVal = cast(CONVERT(char(10), bintCreatedDate, 101) AS NVarchar(10))
 FROM AdditionalContactsInformation    
 WHERE vcEmail = @vcEmail    
    
 RETURN @dtRetVal    
END
GO
