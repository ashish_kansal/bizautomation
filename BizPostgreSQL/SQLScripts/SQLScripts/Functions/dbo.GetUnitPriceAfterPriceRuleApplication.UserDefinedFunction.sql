GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetUnitPriceAfterPriceRuleApplication')
DROP FUNCTION GetUnitPriceAfterPriceRuleApplication
GO
CREATE FUNCTION [dbo].[GetUnitPriceAfterPriceRuleApplication]
(
	@numRuleID NUMERIC(18,0), 
	@numDomainID numeric(18,0), 
	@numItemCode NUMERIC(18,0), 
	@numQuantity FLOAT, 
	@numWarehouseItemID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
)  
RETURNS FLOAT  
AS  
BEGIN  

DECLARE @finalUnitPrice FLOAT = 0
DECLARE @ItemPrice FLOAT = 0
DECLARE @tintRuleType INT
DECLARE @tintPricingMethod INT
DECLARE @tintDiscountType INT
DECLARE @decDiscount FLOAT
DECLARE @decMaxDiscount FLOAT
DECLARE @tempDecMaxDiscount FLOAT
DECLARE @decDiscountPerQty INT

SELECT 
	@tintPricingMethod=tintPricingMethod, 
	@tintRuleType = tintRuleType, 
	@tintDiscountType = tintDiscountType,
	@decDiscount = decDiscount,
	@decMaxDiscount = decMaxDedPerAmt,
	@decDiscountPerQty = intQntyItems
FROM 
	PriceBookRules 
WHERE numPricRuleID = @numRuleID

DECLARE @TEMPPrice TABLE
(
	bitSuccess BIT
	,monPrice DECIMAL(20,5)
)

DECLARE @monCalculatePrice AS DECIMAL(20,5)

If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0
BEGIN
	INSERT INTO @TEMPPrice
	(
		bitSuccess
		,monPrice
	)
	SELECT
		bitSuccess
		,monPrice
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,(SELECT ISNULL(tintKitAssemblyPriceBasedOn,1) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode),0,0,'',0,1)

	IF (SELECT bitSuccess FROM @TEMPPrice) = 1
	BEGIN
		SET @monCalculatePrice = (SELECT monPrice FROM @TEMPPrice)
	END
END

IF @tintPricingMethod = 1 --Use Pricing Table
BEGIN
	EXEC @finalUnitPrice = GetUnitPriceAfterPriceLevelApplication @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID,  @isPriceRule = 1, @numPriceRuleID = @numRuleID, @numDivisionID = 0 -- PASS 0 FOR DIVISION ID BECAUSE THIS PRICE TABLE IS FROM PRICE BOOK RULE
END
ELSE IF @tintPricingMethod = 2 --Use Pricing Formula
BEGIN
	IF @tintRuleType = 1 --Deduct from List price
	BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
				SELECT @ItemPrice = @monCalculatePrice
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		/* If price > 0 then apply rule calculation */
		IF @ItemPrice > 0
		BEGIN
			IF @decDiscount > 0
			BEGIN
				IF @numQuantity < @decDiscountPerQty
					SET @tempDecMaxDiscount = 1 * @decDiscount
				ELSE
					SET @tempDecMaxDiscount = (@numQuantity/@decDiscountPerQty) * @decDiscount

				IF @decMaxDiscount > 0 AND @tempDecMaxDiscount > @decMaxDiscount
					SET @tempDecMaxDiscount = @decMaxDiscount

				IF @tintDiscountType = 1 --Percentage
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@tempDecMaxDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 --Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice - @tempDecMaxDiscount
				END
			END
		END
	END
	ELSE IF @tintRuleType = 2 --Add to Primary Vendor Cost
	BEGIN
		If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 -- Kit OR Assembly
			SELECT @ItemPrice = @monCalculatePrice
		ELSE
			SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)
		/* If price > 0 then apply rule calculation */
		IF @ItemPrice > 0
		BEGIN
			IF @decDiscount > 0
			BEGIN
				IF @numQuantity < @decDiscountPerQty
					SET @tempDecMaxDiscount = 1 * @decDiscount
				ELSE
					SET @tempDecMaxDiscount = (@numQuantity/@decDiscountPerQty) * @decDiscount

				IF @decMaxDiscount > 0 AND @tempDecMaxDiscount > @decMaxDiscount
					SET @tempDecMaxDiscount = @decMaxDiscount

				IF @tintDiscountType = 1 --Percentage
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@tempDecMaxDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 --Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + @tempDecMaxDiscount
				END
			END
		END
	END
END


return @finalUnitPrice
END
GO
