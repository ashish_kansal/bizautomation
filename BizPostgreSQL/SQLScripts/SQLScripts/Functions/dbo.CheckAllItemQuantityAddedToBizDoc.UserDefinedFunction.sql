
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckAllItemQuantityAddedToBizDoc')
DROP FUNCTION CheckAllItemQuantityAddedToBizDoc
GO
CREATE FUNCTION [dbo].[CheckAllItemQuantityAddedToBizDoc] 
(
      @numOppID AS NUMERIC(18,0),
	  @numOppBizDocID AS NUMERIC(18,0)
)
RETURNS BIT
AS 
BEGIN	
	DECLARE @isAllItemWithQuantityIncluded BIT = 0

	
	-- check if all quantity of for each item is added to bizdoc
	IF (SELECT
			SUM(QtyDifference) AS QtyDifference
		FROM
			(
				SELECT 
					OpportunityItems.numOppId,
					(ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(TEMP1.numUnitHour,0)) AS QtyDifference
				FROM
					OpportunityItems
				LEFT JOIN
					(
						SELECT
							TEMP2.numOppItemID,
							SUM(TEMP2.numUnitHour) AS numUnitHour
						FROM
						(
							SELECT
								OpportunityBizDocItems.numOppItemID,
								OpportunityBizDocItems.numUnitHour
							FROM
								OpportunityBizDocs
							INNER JOIN
								OpportunityBizDocItems
							ON
								OpportunityBizDocItems.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId
							WHERE
								OpportunityBizDocs.numOppId = @numOppID AND bitAuthoritativeBizDocs = 1) AS TEMP2
						GROUP BY
							TEMP2.numOppItemID
					) AS TEMP1
				ON
					OpportunityItems.numoppitemtCode = TEMP1.numOppItemID
				WHERE
					OpportunityItems.numOppId = @numOppID AND
					 numoppitemtCode In (SELECT numOppItemID FROM OpportunityBizDocItems WHERE OpportunityBizDocItems.numOppBizDocID = @numOppBizDocID)
			) AS TEMP	
		GROUP BY
			TEMP.numOppId) > 0
	BEGIN
		SET @isAllItemWithQuantityIncluded = 0
	END
	ELSE
	BEGIN
		SET @isAllItemWithQuantityIncluded = 1
	END
	
	RETURN @isAllItemWithQuantityIncluded
END
GO