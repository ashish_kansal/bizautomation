
GO
/****** Object:  StoredProcedure [dbo].[USP_GetItemsForAPI]    Script Date: 06/01/2009 23:51:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Chintan Prajapati
-- exec [dbo].[usp_GetItemsForAPI] 1,1000,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemsForAPI')
DROP PROCEDURE USP_GetItemsForAPI
GO
CREATE PROCEDURE [dbo].[USP_GetItemsForAPI]
    @numDomainID AS NUMERIC(9),
    @numItemCode AS NUMERIC(9),
    @bitMode BIT
AS 
    BEGIN
        IF @bitMode = 0  --select item for given itemcode
            BEGIN
                SELECT  I.numItemCode,
                        I.vcItemName,
                        I.txtItemDesc,
                        I.charItemType,
                        ISNULL(( SELECT [monWListPrice]
                                 FROM   [WareHouseItems]
                                 WHERE  numItemId = numItemCode
                                        AND [numWareHouseID] IN (
                                        SELECT  [numDefaultWareHouseID]
                                        FROM    [eCommerceDTL]
                                        WHERE   [numDomainId] = I.[numDomainID] )
                               ), I.monListPrice) AS monListPrice,
                        I.vcSKU,
                        --I.dtDateEntered,
                        I.bintCreatedDate,
                        (SELECT TOP 1 vcPathForImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1 AND II.bitIsImage = 1) vcPathForImage,
                        I.vcModelID,
                        I.numItemGroup,
                        (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) monAverageCost,
                        I.monCampaignLabourCost,
                        ISNULL(I.fltWeight, 0) fltWeight,
                        ISNULL(I.fltHeight, 0) fltHeight,
                        ISNULL(I.fltWidth, 0) fltWidth,
                        ISNULL(I.fltLength, 0) fltLength,
                        I.vcUnitofMeasure,
                        ISNULL(( SELECT [numOnHand]
                                 FROM   [WareHouseItems]
                                 WHERE  numItemId = numItemCode
                                        AND [numWareHouseID] IN (
                                        SELECT  [numDefaultWareHouseID]
                                        FROM    [eCommerceDTL]
                                        WHERE   [numDomainId] = I.[numDomainID] )
                               ), 0) AS QtyOnHand
                FROM    Item I
                WHERE   I.numDomainID = @numDomainID
                        AND I.[numItemCode] = @numItemCode
            END
        ELSE -- select items after the Itemcode provided
            BEGIN
                SELECT  I.numItemCode,
                        I.vcItemName,
                        I.txtItemDesc,
                        I.charItemType,
                        ISNULL(( SELECT [monWListPrice]
                                 FROM   [WareHouseItems]
                                 WHERE  numItemId = numItemCode
                                        AND [numWareHouseID] IN (
                                        SELECT  [numDefaultWareHouseID]
                                        FROM    [eCommerceDTL]
                                        WHERE   [numDomainId] = I.[numDomainID] )
                               ), I.monListPrice) AS monListPrice,
                        I.vcSKU,
                        --I.dtDateEntered,
                        I.bintCreatedDate,
                        (SELECT TOP 1 vcPathForImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1 AND II.bitIsImage = 1) vcPathForImage,
                        I.vcModelID,
                        I.numItemGroup,
                        (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE I.monAverageCost END) monAverageCost,
                        I.monCampaignLabourCost,
                        ISNULL(I.fltWeight, 0) fltWeight,
                        ISNULL(I.fltHeight, 0) fltHeight,
                        ISNULL(I.fltWidth, 0) fltWidth,
                        ISNULL(I.fltLength, 0) fltLength,
                        I.vcUnitofMeasure,
                        ISNULL(( SELECT [numOnHand]
                                 FROM   [WareHouseItems]
                                 WHERE  numItemId = numItemCode
                                        AND [numWareHouseID] IN (
                                        SELECT  [numDefaultWareHouseID]
                                        FROM    [eCommerceDTL]
                                        WHERE   [numDomainId] = I.[numDomainID] )
                               ), 0) AS QtyOnHand
                FROM    dbo.Item I
                WHERE   I.numDomainID = @numDomainID
                        AND I.[numItemCode] > @numItemCode
            END
    END
