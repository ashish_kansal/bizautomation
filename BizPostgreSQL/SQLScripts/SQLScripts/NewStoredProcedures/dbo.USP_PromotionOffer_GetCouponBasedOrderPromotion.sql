GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_GetCouponBasedOrderPromotion')
DROP PROCEDURE USP_PromotionOffer_GetCouponBasedOrderPromotion
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_GetCouponBasedOrderPromotion]
    @numDomainId AS NUMERIC(18,0)
AS 
BEGIN
	SELECT
		numProId
		,vcProName
	FROM
		PromotionOffer
	WHERE
		numDomainId=@numDomainId
		AND ISNULL(bitEnabled,0) = 1
		AND ISNULL(IsOrderBasedPromotion,0) = 1
		AND ISNULL(bitRequireCouponCode,0) = 1
	ORDER BY
		vcProName
END
GO