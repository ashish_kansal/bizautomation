SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10SalesOpportunityByRevenue')
DROP PROCEDURE USP_ReportListMaster_Top10SalesOpportunityByRevenue
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10SalesOpportunityByRevenue]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@numRecordCount NUMERIC(18,0)
	,@vcTimeLine VARCHAR(50)
	,@vcDealAmount VARCHAR(MAX)
AS
BEGIN 

	DECLARE @TodayDate DATE
	DECLARE @CreatedDate DATE
	SET @TodayDate = DATEADD(MINUTE,@ClientTimeZoneOffset,GETUTCDATE())

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -12, @TodayDate)
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -6, @TodayDate)
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -3, @TodayDate)
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @CreatedDate = DATEADD(DAY, -30, @TodayDate)
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @CreatedDate = DATEADD(DAY, -7, @TodayDate)
	END
	

	SELECT TOP 10
		OM.vcPOppName
		,CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) AS URL
		,ISNULL(monTotAmount,0) monTotAmount
		,ISNULL(OM.numPercentageComplete,0) numPercentageComplete
	FROM
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppID
	LEFT JOIN
		ListDetails LD
	ON
		--LD.numListID=50 
		LD.numListID=(@numRecordCount) 
		AND (ISNULL(LD.constFlag,0)=1 OR LD.numDomainID=@numDomainID)
		AND LD.numListItemID = OM.numPercentageComplete
	WHERE
		OM.numDomainId=@numDomainID
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=0
		--AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
		AND OM.bintCreatedDate >= @CreatedDate
		AND ( 1 = (CASE WHEN CHARINDEX('1-5K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 1000 AND 5000 THEN 1 ELSE 0 END) ELSE 0 END)
		OR 1 = (CASE WHEN CHARINDEX('5-10K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 5000 AND 10000 THEN 1 ELSE 0 END) ELSE 0 END)
		OR 1 = (CASE WHEN CHARINDEX('10-20K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 10000 AND 20000 THEN 1 ELSE 0 END) ELSE 0 END)
		OR 1 = (CASE WHEN CHARINDEX('20-50K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 20000 AND 50000 THEN 1 ELSE 0 END) ELSE 0 END)
		OR 1 = (CASE WHEN CHARINDEX('>50K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))
	ORDER BY
		ISNULL(monTotAmount,0) DESC
END
GO