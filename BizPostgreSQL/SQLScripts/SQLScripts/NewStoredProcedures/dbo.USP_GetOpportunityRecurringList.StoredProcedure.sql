GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpportunityRecurringList')
DROP PROCEDURE USP_GetOpportunityRecurringList
GO
create PROCEDURE [dbo].[USP_GetOpportunityRecurringList]
               @numSeedOppId    AS NUMERIC(9),
               @tintRecType TINYINT
AS
  BEGIN
    IF (@tintRecType = 1)
      BEGIN
        SELECT 
			RTR.[numRecTranOppID],
			0 AS numRecTranBizDocID,
			OM.[vcPOppName] AS RecurringName,
			dbo.getDealAmount(RTR.[numRecTranOppID],getutcdate(),0) AS TotalCostAmount,
			@tintRecType AS tintRecType
        FROM   OpportunityMaster AS OM
               INNER JOIN RecurringTransactionReport AS RTR
                 ON OM.numOppId = RTR.[numRecTranOppID]
        WHERE  RTR.tintRecType = 1
               AND RTR.[numRecTranSeedId] = @numSeedOppId
      END
    ELSE
      IF (@tintRecType = 2)
        BEGIN
          SELECT 
			  OPR.[numOppId] AS numRecTranOppID,
			  OPR.[numOppBizDocID] numRecTranBizDocID,
			  OBD.vcBizDocID AS RecurringName,
			  OBD.monDealAmount AS TotalCostAmount,
			  @tintRecType AS tintRecType
          FROM   dbo.OpportunityRecurring OPR
                 INNER JOIN OpportunityBizDocs AS OBD
                   ON OPR.numOppBizDocID = OBD.numOppBizDocsId
          WHERE  OPR.tintRecurringType = 2 
           AND OPR.numOppId IN (SELECT numOppId FROM dbo.OpportunityRecurring WHERE numOppBizDocID=@numSeedOppId) 
        END
  END

GO