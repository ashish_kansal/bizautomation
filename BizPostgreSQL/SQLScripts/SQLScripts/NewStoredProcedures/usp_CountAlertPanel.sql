/****** Object:  StoredProcedure [dbo].[usp_SetFollowUpStatusForSelectedRecords]    Script Date: 07/26/2008 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Prasanta	                            
--Purpose: Deletes the selected Records of Organization Alert Panel   
--Created Date: 02/09/2016  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_CountAlertPanel')
DROP PROCEDURE usp_CountAlertPanel
GO
CREATE PROCEDURE [dbo].[usp_CountAlertPanel]          
  @chrAction char(10)=NULL,
  @numDomainId Numeric(18,2)=0,
  @numUserId Numeric(18,2)=0,
  @numModuleId Numeric(18,2)=0,
  @numRecordId Numeric(18,2)=0      
AS                                          
BEGIN
	Declare @vcStartDate VARCHAR(30)='2/4/2016'

--All Count
IF(@chrAction='ALLC')
	BEGIN
		 SELECT (SELECT COUNT(D.numDivisionID)
			FROM 
				View_CompanyAlert as C
			LEFT JOIN
				DivisionMaster as D
			ON 
				C.numCompanyId=D.numCompanyID
			LEFT JOIN
				UserMaster as U 
			ON 
				C.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				D.numDivisionID=SH.numRecordID AND SH.numModuleID=1
			WHERE
				--(C.bintCreatedDate>=Cast(@vcStartDate as date) OR D.bintModifiedDate>=Cast(@vcStartDate as date)) AND 
				C.numDomainID=@numDomainId AND D.numDivisionID!=0 AND
				C.numCreatedBy!=@numUserId AND (D.numAssignedTo=@numUserId OR D.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND D.numDivisionID NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId=1 AND numDomainID=@numDomainId)) as company,

		(SELECT COUNT(C.numCaseId)
			FROM 
				View_CaseAlert as C
			LEFT JOIN
				DivisionMaster as D
			ON 
				C.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				C.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AC
			ON 
				C.numContactId=AC.numContactId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				C.numCaseId=SH.numRecordID AND SH.numModuleID=7
			WHERE
				--C.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				C.numDomainID=@numDomainId AND C.numCaseId!=0 AND
				C.numCreatedBy!=@numUserId AND (C.numAssignedTo=@numUserId OR C.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND C.numCaseId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId=2 AND numDomainID=@numDomainId)) as [case],
		(SELECT COUNT(O.numOppId)
			FROM 
				View_OpportunityAlert as O
			LEFT JOIN
				DivisionMaster as D
			ON 
				O.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				O.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AC
			ON 
				O.numContactId=AC.numContactId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				SH.numRecordID=O.numOppId AND SH.numModuleID=3
			WHERE
				--O.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				O.numDomainID=@numDomainId AND O.numOppId!=0 AND O.tintOppStatus=0 AND
				O.numCreatedBy!=@numUserId AND (O.numAssignedTo=@numUserId OR O.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND O.numOppId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																				numUserId=@numUserId and numModuleId IN(3,4,5,6) AND numDomainID=@numDomainId)) as opp,
	(SELECT COUNT(O.numOppId)
			FROM 
				View_OpportunityAlert as O
			LEFT JOIN
				DivisionMaster as D
			ON 
				O.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				O.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AC
			ON 
				O.numContactId=AC.numContactId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				SH.numRecordID=O.numOppId AND SH.numModuleID=3
			WHERE
				--O.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				O.numDomainID=@numDomainId AND O.numOppId!=0 AND O.tintOppStatus=1 AND
				O.numCreatedBy!=@numUserId AND (O.numAssignedTo=@numUserId OR O.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND O.numOppId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(3,4,5,6) AND numDomainID=@numDomainId)) as [order],

	(SELECT  COUNT(P.numProId)
			FROM 
				View_ProjectsAlert as P
			LEFT JOIN
				DivisionMaster as D
			ON 
				P.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				P.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				SH.numRecordID=P.numProId AND SH.numModuleID=5
			WHERE
				--P.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				P.numDomainID=@numDomainId AND P.numProId!=0 AND
				P.numCreatedBy!=@numUserId AND (P.numAssignedTo=@numUserId OR P.numRecOwner=@numUserId OR P.numIntPrjMgr=@numUserId OR SH.numAssignedTo=@numUserId) AND P.numProId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(7) AND numDomainID=@numDomainId)) as project,
	(SELECT COUNT(H.numEmailHstrID)
			FROM 
				View_EmailAlert as H
			LEFT JOIN
				UserMaster as U 
			ON 
				H.numUserCntId=U.numUserDetailId
			LEFT JOIN
				EmailMaster as T
			ON
				H.vcTo=CONCAT(T.numEmailId,'$^$',T.vcName,'$^$',T.vcEmailID)
			LEFT JOIN
				UserMaster as UT
			ON
				T.numContactId=UT.numUserDetailId
			WHERE
				--H.bintCreatedOn>=Cast(@vcStartDate as date) AND 
				H.numDomainID=@numDomainId AND H.numEmailHstrID!=0 AND
				H.numUserCntId!=@numUserId AND (T.numContactId=@numUserId) AND H.numEmailHstrID NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(8) AND numDomainID=@numDomainId)) as Email,
	(SELECT COUNT(C.numCommId)
			FROM 
				View_TicklerAlert as C
			LEFT JOIN
				DivisionMaster as D
			ON 
				C.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				C.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AD
			ON
				C.numContactId=AD.numContactId
			WHERE
				--C.dtModifiedDate>=Cast(@vcStartDate as date) AND 
				C.numDomainID=@numDomainId AND C.numCommId!=0 AND
				C.numCreatedBy!=@numUserId AND C.numAssignedBy!=@numUserId AND (C.numAssign=@numUserId ) AND C.numCommId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(9) AND numDomainID=@numDomainId)) as tickler,
	(SELECT COUNT(S.numStageDetailsId)
			FROM 
				View_ProcessAlert as S
			LEFT JOIN
				UserMaster as U 
			ON 
				S.numCreatedBy=U.numUserDetailId
			LEFT JOIN
				OpportunityMaster as O
			ON 
				O.numOppId=S.numOppID
			LEFT JOIN
				ProjectsMaster as P
			ON	
				P.numProId=S.numProjectID
			WHERE
				--S.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				S.numDomainID=@numDomainId AND S.numStageDetailsId!=0 AND
				(O.numAssignedTo=@numUserId OR O.numRecOwner=@numUserId OR P.numAssignedTo=@numUserId OR P.numRecOwner=@numUserId OR P.numIntPrjMgr=@numUserId)
				AND (O.numAssignedBy!=@numUserId OR P.numAssignedby!=@numUserId OR O.numCreatedBy!=@numUserId OR P.numCreatedBy!=@numUserId) AND
				S.numCreatedBy!=@numUserId AND  S.numStageDetailsId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(10) AND numDomainID=@numDomainId))as process
	END 



--Organization Alert
IF(@chrAction='V')
	BEGIN                           
	 SELECT COUNT(D.numDivisionID)
			FROM 
				View_CompanyAlert as C
			LEFT JOIN
				DivisionMaster as D
			ON 
				C.numCompanyId=D.numCompanyID
			LEFT JOIN
				UserMaster as U 
			ON 
				C.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				D.numDivisionID=SH.numRecordID AND SH.numModuleID=1
			WHERE
				--(C.bintCreatedDate>=Cast(@vcStartDate as date) OR D.bintModifiedDate>=Cast(@vcStartDate as date)) AND 
				C.numDomainID=@numDomainId AND D.numDivisionID!=0 AND
				C.numCreatedBy!=@numUserId AND (D.numAssignedTo=@numUserId OR D.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND D.numDivisionID NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId=@numModuleId AND numDomainID=@numDomainId)
	END

--Case Module Alert
IF(@chrAction='CV')
	BEGIN                           
	 SELECT COUNT(C.numCaseId)
			FROM 
				View_CaseAlert as C
			LEFT JOIN
				DivisionMaster as D
			ON 
				C.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				C.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AC
			ON 
				C.numContactId=AC.numContactId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				C.numCaseId=SH.numRecordID AND SH.numModuleID=7
			WHERE
				--C.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				C.numDomainID=@numDomainId AND C.numCaseId!=0 AND
				C.numCreatedBy!=@numUserId AND (C.numAssignedTo=@numUserId OR C.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND C.numCaseId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId=@numModuleId AND numDomainID=@numDomainId)
	END

--Puchase & Sales Oppourtunity Module Alert
IF(@chrAction='PO')
	BEGIN                           
	 SELECT COUNT(O.numOppId)
			FROM 
				View_OpportunityAlert as O
			LEFT JOIN
				DivisionMaster as D
			ON 
				O.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				O.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AC
			ON 
				O.numContactId=AC.numContactId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				SH.numRecordID=O.numOppId AND SH.numModuleID=3
			WHERE
				--O.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				O.numDomainID=@numDomainId AND O.numOppId!=0 AND O.tintOppStatus=0 AND
				O.numCreatedBy!=@numUserId AND (O.numAssignedTo=@numUserId OR O.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND O.numOppId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(3,4,5,6) AND numDomainID=@numDomainId)
	END

IF(@chrAction='SO')
	BEGIN                           
	 SELECT COUNT(O.numOppId)
			FROM 
				View_OpportunityAlert as O
			LEFT JOIN
				DivisionMaster as D
			ON 
				O.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				O.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AC
			ON 
				O.numContactId=AC.numContactId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				SH.numRecordID=O.numOppId AND SH.numModuleID=3
			WHERE
				--O.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				O.numDomainID=@numDomainId AND O.numOppId!=0 AND O.tintOppStatus=1 AND
				O.numCreatedBy!=@numUserId AND (O.numAssignedTo=@numUserId OR O.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND O.numOppId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(3,4,5,6) AND numDomainID=@numDomainId)
	END

--Projects Module Alert
IF(@chrAction='PV')
	BEGIN                           
	 SELECT  COUNT(P.numProId)
			FROM 
				View_ProjectsAlert as P
			LEFT JOIN
				DivisionMaster as D
			ON 
				P.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				P.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				SH.numRecordID=P.numProId AND SH.numModuleID=5
			WHERE
				--P.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				P.numDomainID=@numDomainId AND P.numProId!=0 AND
				P.numCreatedBy!=@numUserId AND (P.numAssignedTo=@numUserId OR P.numRecOwner=@numUserId OR P.numIntPrjMgr=@numUserId OR SH.numAssignedTo=@numUserId) AND P.numProId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(7) AND numDomainID=@numDomainId)
	END

--Email Module Alert
IF(@chrAction='EV')
	BEGIN                           
	 SELECT COUNT(H.numEmailHstrID)
			FROM 
				View_EmailAlert as H
			LEFT JOIN
				UserMaster as U 
			ON 
				H.numUserCntId=U.numUserDetailId
			LEFT JOIN
				EmailMaster as T
			ON
				(SELECT TOP 1 Items from  [dbo].[Split](H.vcTo,'$^$'))=T.numEmailId
			LEFT JOIN
				UserMaster as UT
			ON
				T.numContactId=UT.numUserDetailId
			WHERE
				--H.bintCreatedOn>=Cast(@vcStartDate as date) AND 
				H.numDomainID=@numDomainId AND H.numEmailHstrID!=0 AND
				H.numUserCntId!=@numUserId AND (T.numContactId=@numUserId) AND H.numEmailHstrID NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(8) AND numDomainID=@numDomainId)
	END
	--Tickler Module Alert
IF(@chrAction='TV')
	BEGIN                           
	 SELECT COUNT(C.numCommId)
			FROM 
				View_TicklerAlert as C
			LEFT JOIN
				DivisionMaster as D
			ON 
				C.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				C.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AD
			ON
				C.numContactId=AD.numContactId
			WHERE
				--C.dtModifiedDate>=Cast(@vcStartDate as date) AND 
				C.numDomainID=@numDomainId AND C.numCommId!=0 AND
				C.numCreatedBy!=@numUserId AND C.numAssignedBy!=@numUserId AND (C.numAssign=@numUserId ) AND C.numCommId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(9) AND numDomainID=@numDomainId)
	END
	--Process Module Alert 
IF(@chrAction='PPV')
	BEGIN                           
	 SELECT COUNT(S.numStageDetailsId)
			FROM 
				View_ProcessAlert as S
			LEFT JOIN
				UserMaster as U 
			ON 
				S.numCreatedBy=U.numUserDetailId
			LEFT JOIN
				OpportunityMaster as O
			ON 
				O.numOppId=S.numOppID
			LEFT JOIN
				ProjectsMaster as P
			ON	
				P.numProId=S.numProjectID
			WHERE
				--S.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				S.numDomainID=@numDomainId AND S.numStageDetailsId!=0 AND
				(O.numAssignedTo=@numUserId OR O.numRecOwner=@numUserId OR P.numAssignedTo=@numUserId OR P.numRecOwner=@numUserId OR P.numIntPrjMgr=@numUserId)
				AND (O.numAssignedBy!=@numUserId OR P.numAssignedby!=@numUserId OR O.numCreatedBy!=@numUserId OR P.numCreatedBy!=@numUserId) AND
				S.numCreatedBy!=@numUserId AND  S.numStageDetailsId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(10) AND numDomainID=@numDomainId)
	END
END

GO
