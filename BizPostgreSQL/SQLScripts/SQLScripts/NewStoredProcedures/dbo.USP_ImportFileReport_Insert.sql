GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ImportFileReport_Insert' ) 
    DROP PROCEDURE USP_ImportFileReport_Insert
GO

CREATE PROCEDURE USP_ImportFileReport_Insert
	@intImportFileID BIGINT,
	@intRowNumber INT,
	@vcMessage varchar(1000),
	@numRecordID NUMERIC(18,0),
	@vcErrorMessage VARCHAR(MAX),
	@vcStackStrace VARCHAR(MAX)
AS 
BEGIN
	INSERT INTO Import_File_Report
	(
		intImportFileID
		,intRowNumber
		,vcMessage
		,numRecordID
		,vcErrorMessage
		,vcStackStrace
	)
	VALUES
	(
		@intImportFileID
		,@intRowNumber
		,@vcMessage
		,@numRecordID
		,@vcErrorMessage
		,@vcStackStrace
	)
END