USE [Production.2014]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetListDetailAttributesUsingDomainID]    Script Date: 16-01-2018 08:27:56 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[USP_GetListDetailAttributesUsingDomainID]            
	@numDomainID as numeric(9)=0       
AS
BEGIN            
	SELECT * FROM ListDetails LD
	JOIN CFW_Fld_Master CFM ON CFM.numlistid=LD.numListID  
	WHERE Grp_id=9 AND LD.numDomainID = @numDomainID
END
