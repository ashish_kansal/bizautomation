
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ContractDetails')
DROP PROCEDURE USP_ContractDetails
GO
CREATE PROCEDURE [dbo].[USP_ContractDetails]
@numContractId AS NUMERIC(18,0),
@numDomainID AS NUMERIC(18,0),
@ClientTimeZoneOffset AS INT
AS
BEGIN
				SELECT 
								C.numContractId,
								C.numIncidents,
								ISNULL(CI.vcCompanyName,'') AS vcCompanyName,
								C.numDivisonId,
								(ISNULL(C.numIncidents,0)  - ISNULL(C.numIncidentsUsed,0)) AS [numIncidentLeft],
								C.[numHours],
								C.[numMinutes],
								dbo.fn_SecondsConversion(((ISNULL(C.numHours,0) * 60 * 60) + (ISNULL(C.numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0)) AS [timeLeft],
								C.[vcItemClassification],
								C.[numWarrantyDays],
								C.[vcNotes],
								C.[vcContractNo],
								C.timeUsed,
								dbo.fn_GetContactName(isnull(C.numCreatedBy,0)) AS CreatedBy,
								dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * @ClientTimeZoneOffset, C.dtmCreatedOn),@numDomainID) AS CreatedOn,
								dbo.fn_GetContactName(isnull(C.numModifiedBy,0)) AS ModifiedBy,
								ISNULL(dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * @ClientTimeZoneOffset, C.dtmModifiedOn),@numDomainID),'') AS ModifiedOn
							FROM 
								Contracts AS C
							LEFT JOIN 
								DivisionMaster AS D
							ON
								C.numDivisonId=D.numDivisionID
							LEFT JOIN
								CompanyInfo AS CI
							ON
								D.numCompanyId=CI.numCompanyId
							WHERE
								C.numDomainID=@numDomainID AND
								C.numContractId=@numContractId
END