
GO
/****** Object:  StoredProcedure [dbo].[USP_GETEmailFromDetail]    Script Date: 06/04/2009 15:12:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GETEmailFromDetail')
DROP PROCEDURE USP_GETEmailFromDetail
GO
CREATE PROCEDURE [dbo].[USP_GETEmailFromDetail] 
               @numDomainID NUMERIC(9)
AS
  BEGIN
    SELECT [vcEmail] FromEmail,
           ISNULL([vcFirstName],'')+ ' '+ ISNULL([vcLastName],'') AS FromName
           
    FROM   [AdditionalContactsInformation]
    WHERE  numcontactID IN (SELECT [numAdminID]
                            FROM   [Domain]
                            WHERE  [numDomainId] = @numDomainID)
                            
  --SELECT * FROM [UserMaster] WHERE [numUserDetailId] = [numAdminID]
  END
