GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ValidateFinancialYearClosingDate' ) 
    DROP PROCEDURE USP_ValidateFinancialYearClosingDate
GO

CREATE PROCEDURE USP_ValidateFinancialYearClosingDate
@numDomainID NUMERIC,
@dtEntryDate DATETIME=NULL, -- pass date on which journal will be passed to accounting 
@tintRecordType AS TINYINT=0,
@numRecordID AS NUMERIC=0
AS 
BEGIN
	IF @tintRecordType=0 --General for Date
	BEGIN
		IF EXISTS(SELECT * FROM dbo.FinancialYear WHERE numDomainId=@numDomainID AND ( ISNULL(bitCloseStatus,0)=1 OR ISNULL(bitAuditStatus,0)=1 )
			AND @dtEntryDate BETWEEN dtPeriodFrom AND dtPeriodTo)
			BEGIN
				RAISERROR ('FY_CLOSED',16,1);
				RETURN -1
			END
	END
	ELSE IF @tintRecordType=1 --For Item Delete
	BEGIN
		IF EXISTS(SELECT GJD.numJournalId FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	       JOIN FinancialYear FY ON GJH.numDomainId = FY.numDomainId WHERE GJH.numDomainId=@numDomainID AND GJD.numItemID=@numRecordID AND ( ISNULL(FY.bitCloseStatus,0)=1 OR ISNULL(FY.bitAuditStatus,0)=1 )
			AND GJH.datEntry_Date BETWEEN FY.dtPeriodFrom AND FY.dtPeriodTo )
			BEGIN
				RAISERROR ('FY_CLOSED',16,1);
				RETURN -1
			END
	END	
	ELSE IF @tintRecordType=2 --For Opportunity Delete
	BEGIN
		IF EXISTS(SELECT GJD.numJournalId FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	       JOIN FinancialYear FY ON GJH.numDomainId = FY.numDomainId WHERE GJH.numDomainId=@numDomainID AND GJH.numOppId=@numRecordID AND ( ISNULL(FY.bitCloseStatus,0)=1 OR ISNULL(FY.bitAuditStatus,0)=1 )
			AND GJH.datEntry_Date BETWEEN FY.dtPeriodFrom AND FY.dtPeriodTo )
			BEGIN
				RAISERROR ('FY_CLOSED',16,1);
				RETURN -1
			END
	END	
	
	
END