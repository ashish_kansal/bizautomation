SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetDomainsForReindex')
DROP PROCEDURE dbo.USP_ElasticSearch_GetDomainsForReindex
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetDomainsForReindex]
	
AS 
BEGIN
	IF CAST((SELECT LastReindexDate FROM ElasticSearchLastReindex WHERE ID = 1) AS DATE) <> CAST(GETDATE() AS DATE) AND DATEPART(hh, GETDATE()) > 2
	BEGIN
		SELECT
			numDomainId
		FROM
			Domain
		WHERE
			ISNULL(bitElasticSearch,0) = 1

		UPDATE ElasticSearchLastReindex SET LastReindexDate = GETDATE() WHERE ID = 1
	END
END