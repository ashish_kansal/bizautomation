GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskNotes_Save')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskNotes_Save
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskNotes_Save]
(
	@numTaskID NUMERIC(18,0)
	,@vcNotes NVARCHAR(MAX)
	,@bitDone BIT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskNotes WHERE numTaskID = @numTaskID)
	BEGIN
		UPDATE StagePercentageDetailsTaskNotes SET vcNotes=@vcNotes,bitDone=@bitDone WHERE numTaskID=@numTaskID
	END
	ELSE IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numTaskId = @numTaskID)
	BEGIN
		INSERT INTO StagePercentageDetailsTaskNotes
		(
			numTaskID
			,vcNotes
			,bitDone
		)
		VALUES
		(
			@numTaskID
			,@vcNotes
			,@bitDone
		)
	END	
END
GO