SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetHeaderReturnDetails' ) 
    DROP PROCEDURE USP_GetHeaderReturnDetails
GO

-- EXEC USP_GetHeaderReturnDetails 53, 1
CREATE PROCEDURE [dbo].[USP_GetHeaderReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @tintReceiveType NUMERIC(9)=0
    )
AS 
    BEGIN
    
		DECLARE @tintType TINYINT,@tintReturnType TINYINT;SET @tintType=0
        
		SELECT @tintReturnType=tintReturnType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
		DECLARE @monEstimatedBizDocAmount AS DECIMAL(20,5)
    
		SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				       When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				       When @tintReturnType=3 THEN 10
				       When @tintReturnType=4 THEN 9 END 
				       
		SELECT @monEstimatedBizDocAmount=monAmount + monTotalTax - monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
	
        SELECT  RH.numReturnHeaderID,
                vcRMA,
                ISNULL(vcBizDocName, '') AS vcBizDocName,
                RH.numDomainId,
                ISNULL(RH.numDivisionId, 0) AS [numDivisionId],
                ISNULL(RH.numContactId, 0) AS [numContactId],
                ISNULL(RH.numOppId,0) AS numOppId,
                tintReturnType,
                numReturnReason,
                numReturnStatus,
                monAmount,
                ISNULL(monBizDocAmount,0) AS monBizDocAmount,ISNULL(monBizDocUsedAmount,0) AS monAmountUsed,
                monTotalTax,
                monTotalDiscount,
                tintReceiveType,
                vcComments,
                RH.numCreatedBy,
                dtCreatedDate,
                RH.numModifiedBy,
                dtModifiedDate,
                numReturnItemID,
                RI.numItemCode,
                I.vcItemName,
				ISNULL(I.vcModelID,'') AS vcModelID,
				ISNULL(I.vcSKU,'') AS vcSKU,
                numUnitHour,
                numUnitHourReceived,
                FORMAT(monPrice,'0.################') monPrice,
				CONVERT(DECIMAL(18, 4),(ISNULL(monTotAmount,0) / RI.numUnitHour)) AS monUnitSalePrice,
                monTotAmount,
                vcItemDesc,
                RI.numWareHouseItemID,
                WItems.numWarehouseID,
                ISNULL(W.vcWareHouse,'') AS vcWareHouse,
                I.vcModelID,
                I.vcManufacturer,
                numUOMId,
				ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') vcBaseUOM,
				ISNULL(dbo.fn_GetUOMName(numUOMId),'') vcUOM,
                dbo.fn_GetListItemName(numReturnStatus) AS [vcStatus],
                dbo.fn_GetContactName(RH.numCreatedby) AS CreatedBy,
                dbo.fn_GetContactName(RH.numModifiedBy) AS ModifiedBy,
                ISNULL(C2.vcCompanyName, '')
                + CASE WHEN ISNULL(D.numCompanyDiff, 0) > 0
                       THEN '  '
                            + ISNULL(dbo.fn_getlistitemname(D.numCompanyDiff),
                                     '') + ':' + ISNULL(D.vcCompanyDiff, '')
                       ELSE ''
                  END AS vcCompanyname,
                ISNULL(D.tintCRMType, 0) AS [tintCRMType],
                ISNULL(numAccountID, 0) AS [numAccountID],
                ISNULL(vcCheckNumber, '') AS [vcCheckNumber],
                ISNULL(IsCreateRefundReceipt, 0) AS [IsCreateRefundReceipt],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnStatus
                ) AS [ReturnStatusName],
                ( SELECT    vcData
                  FROM      dbo.ListDetails
                  WHERE     numListItemID = numReturnReason
                ) AS [ReturnReasonName],
                ISNULL(con.vcEmail, '') AS vcEmail,
                ISNULL(RH.numBizDocTempID, 0) AS numBizDocTempID,
                case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
                charItemType,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
                ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
                ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
                ,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull((SELECT monAverageCost FROM OpportunityBizDocItems OBDI WHERE OBDI.numOppBizDocItemID=RI.numOppBizDocItemID),isnull(i.monAverageCost,'0')) END) as AverageCost
                ,ISNULL(RH.numItemCode,0) AS [numItemCodeForAccount]
				,ISNULL(I.bitKitParent,0) AS bitKitParent
				,ISNULL(I.bitAssembly,0) AS bitAssembly,
                CASE WHEN ISNULL(RH.numOppId,0) <> 0 THEN vcPOppName 
					 ELSE ''
				END AS [Source],ISNULL(I.bitSerialized,0) AS bitSerialized,ISNULL(I.bitLotNo,0) AS bitLotNo,
				SUBSTRING((SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
							FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
							ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo
				,@monEstimatedBizDocAmount AS monEstimatedBizDocAmount
				,ISNULL(RH.numBillPaymentIDRef,0) AS numBillPaymentIDRef
				,ISNULL(RH.numDepositIDRef,0) AS numDepositIDRef
				,ISNULL(numOppBizDocID,0) numOppBizDocID
				, ISNULL(((SELECT TOP 1 CASE WHEN ISNULL(DATEDIFF(DAY, O.bintClosedDate, GETDATE()),0) > 0 AND ISNULL((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId=@numDomainID AND IC.numItemClassification=C.vcItemClassification),0)>0 THEN ISNULL((SELECT vcNotes FROM Contracts AS C WHERE  numDomainId=@numDomainID AND IC.numItemClassification=C.vcItemClassification),'-') ELSE '-' END  FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId=OI.numOppId LEFT JOIN Item IC ON OI.numItemCode=IC.numItemCode WHERE 
				O.numDivisionId=RH.numDivisionId AND O.numDomainId=@numDomainID AND IC.numItemCode=I.numItemCode AND
				IC.bitTimeContractFromSalesOrder=1 AND O.tintOppType=1 AND O.tintOppStatus=1 AND O.tintshipped=1 ORDER BY O.bintCreatedDate DESC)),'') AS vcWarrantyNotes
				, ISNULL(((SELECT TOP 1 CASE WHEN ISNULL(DATEDIFF(DAY, O.bintClosedDate, GETDATE()),0) > 0 AND ISNULL((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId=@numDomainID AND IC.numItemClassification=C.vcItemClassification),0)>0 THEN  ISNULL((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId=@numDomainID AND IC.numItemClassification=C.vcItemClassification),0)-ISNULL(DATEDIFF(DAY, O.bintClosedDate, GETDATE()),0) ELSE 0 END  FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId=OI.numOppId LEFT JOIN Item IC ON OI.numItemCode=IC.numItemCode WHERE 
				O.numDivisionId=RH.numDivisionId AND O.numDomainId=@numDomainID AND IC.numItemCode=I.numItemCode AND
				IC.bitTimeContractFromSalesOrder=1 AND O.tintOppType=1 AND O.tintOppStatus=1 AND O.tintshipped=1 ORDER BY O.bintCreatedDate DESC)),0) AS numWarrantyDaysLeft
        FROM    dbo.ReturnHeader RH
                LEFT JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                LEFT JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
                LEFT JOIN Item I ON I.numItemCode = RI.numItemCode
                LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
                LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
                LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
                LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId
                                                               AND con.numContactid = RH.numContactId
                LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
				LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	    WHERE  RH.numDomainID = @numDomainId
                AND RH.numReturnHeaderID = @numReturnHeaderID
		
    END

GO