GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddAPIOpportunity')
DROP PROCEDURE USP_AddAPIOpportunity
GO
CREATE PROCEDURE [dbo].[USP_AddAPIOpportunity]
@numDomainId	numeric(18, 0),
@WebApiId	int,
@numOppId	numeric(18, 0),
@vcAPIOppId	varchar(50),
@numCreatedby	numeric(18, 0)
AS 
BEGIN
	INSERT INTO [OpportunityMasterAPI]
           (
           [WebApiId]
           ,[numDomainId]
           ,[numOppId]
           ,[vcAPIOppId]
           ,[numCreatedby]
           ,[dtCreated]
           ,[numModifiedby]
           ,[dtModified])
     VALUES
           (
           @WebApiId,
           @numDomainId,
           @numOppId,
           @vcAPIOppId,
           @numCreatedby,
           GETUTCDATE(),
           @numCreatedby,
           GETUTCDATE())
END