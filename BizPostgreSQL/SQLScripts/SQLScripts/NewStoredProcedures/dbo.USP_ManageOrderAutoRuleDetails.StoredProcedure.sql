--select * from OrderAutoRuleDetails

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageOrderAutoRuleDetails')
DROP PROCEDURE USP_ManageOrderAutoRuleDetails
GO
CREATE PROCEDURE USP_ManageOrderAutoRuleDetails
@numRuleID int,
@numItemTypeID numeric,
@numItemClassID numeric,
@numBillToID int,
@numShipToID int,
@btFullPaid bit,
@numBizDocStatus numeric

as

INSERT INTO OrderAutoRuleDetails SELECT @numRuleID,@numItemTypeID,@numItemClassID,@numBillToID,@numShipToID,@btFullPaid,@numBizDocStatus,1
SELECT @@IDENTITY


delete from OrderAutoRule