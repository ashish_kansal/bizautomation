GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Communication_Finish')
DROP PROCEDURE USP_Communication_Finish
GO
CREATE PROCEDURE [dbo].[USP_Communication_Finish]
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numCommID NUMERIC(18,0)
AS  
BEGIN  
	UPDATE 
		Communication 
	SET                                              
		nummodifiedby=@numUserCntID,                                                
		dtModifiedDate=GETUTCDATE(),                                                
		bitClosedFlag=1,   
		dtEventClosedDate=GETUTCDATE()
	WHERE 
		numDomainID = @numDomainID
		AND numCommId = @numCommID		 
END
GO