GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GenericDocuments_GetByFileName')
DROP PROCEDURE USP_GenericDocuments_GetByFileName
GO
CREATE PROCEDURE [dbo].[USP_GenericDocuments_GetByFileName]                          
(                          
	@numDomainID NUMERIC(18,0)
	,@VcFileName VARCHAR(200)
)                          
as 
BEGIN
	SELECT
		*
	FROM
		GenericDocuments
	WHERE
		numDomainID=@numDomainID
		AND VcFileName = @VcFileName
END
GO
