SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ReasonsForSalesReturns')
DROP PROCEDURE USP_ReportListMaster_Top10ReasonsForSalesReturns
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ReasonsForSalesReturns]
	@numDomainID NUMERIC(18,0),
	@numRecordCount INT
AS
BEGIN 
	DECLARE @TotalSalesReturn FLOAT = 0.0

	SELECT 
		@TotalSalesReturn = COUNT(numReturnHeaderID)
	FROM 
		ReturnHeader
	WHERE 
		numDomainId=@numDomainID 
		AND tintReturnType IN (1,3)
	
	SELECT TOP (@numRecordCount)
		Reason
		,(NoOfSalesReturn * 100)/ (CASE WHEN ISNULL(@TotalSalesReturn,0) = 0 THEN 1 ELSE @TotalSalesReturn END) AS TotalReturnPercent
	FROM
	(
		SELECT 
			ISNULL(ListDetails.vcData,'-') AS Reason
			,COUNT(numReturnHeaderID) AS NoOfSalesReturn
		FROM 
			ReturnHeader 
		LEFT JOIN
			ListDetails
		ON
			ReturnHeader.numReturnReason = ListDetails.numListItemID
			AND ListDetails.numListID = 48
			AND (ListDetails.numDomainID =@numDomainID OR ISNULL(ListDetails.constFlag,0) = 1)
		WHERE 
			ReturnHeader.numDomainId=@numDomainID 
			AND tintReturnType IN (1,3)
		GROUP BY
			ListDetails.vcData
	) TEMP
	ORDER BY
		NoOfSalesReturn DESC
		
END
GO

