GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetails_GetStagesByProcess')
DROP PROCEDURE USP_StagePercentageDetails_GetStagesByProcess
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetails_GetStagesByProcess]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numProcessID NUMERIC(18,0)
	,@vcMileStoneName VARCHAR(300)
AS                            
BEGIN
	SELECT
		numStageDetailsId
		,vcStageName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId=@numDomainID
		AND slp_id = @numProcessID
		AND vcMileStoneName = @vcMileStoneName
END
GO