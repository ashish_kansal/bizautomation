GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillmentConfiguration_GetByUser')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillmentConfiguration_GetByUser
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillmentConfiguration_GetByUser]
(
	@numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT * FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID
END
GO