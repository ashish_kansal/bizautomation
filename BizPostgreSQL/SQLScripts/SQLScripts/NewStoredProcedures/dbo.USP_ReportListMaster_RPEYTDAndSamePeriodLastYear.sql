SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RPEYTDAndSamePeriodLastYear')
DROP PROCEDURE USP_ReportListMaster_RPEYTDAndSamePeriodLastYear
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RPEYTDAndSamePeriodLastYear]
	@numDomainID NUMERIC(18,0)
	,@vcFilterBy TINYINT --added  for Filter the Period of Measure as mentioned in Slide 5
AS
BEGIN 
	DECLARE @ExpenseYTD DECIMAL(20,5)
	DECLARE @ExpenseSamePeriodLastYear DECIMAL(20,5)
	DECLARE @ProfitYTD DECIMAL(20,5)
	DECLARE @ProfitSamePeriodLastYear DECIMAL(20,5)
	DECLARE @RevenueYTD DECIMAL(20,5)
	DECLARE @RevenueSamePeriodLastYear DECIMAL(20,5)
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @dtCYFromDate AS DATETIME                                       
	DECLARE @dtCYToDate AS DATETIME
	DECLARE @dtLYFromDate AS DATETIME                                       
	DECLARE @dtLYToDate AS DATETIME

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainID)

	--SELECT @dtCYFromDate = @FinancialYearStartDate,@dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0));
	--SELECT @dtLYFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),@dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))));
	---alter sp
	IF @vcFilterBy= 1
	BEGIN 
	SELECT @dtCYFromDate = @FinancialYearStartDate,@dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0));
	SELECT @dtLYFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),@dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))));
	END
	ELSE IF @vcFilterBy=2
	BEGIN
	select @dtCYFromDate= DATEADD(month, datediff(month, 0, getdate()), 0)
	select @dtCYToDate =dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0))
	select @dtLYFromDate =dateadd(YEAR,-1,DATEADD(month, datediff(month, 0, getdate()), 0))
	select @dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))))
	END
	ELSE IF @vcFilterBy=3
	BEGIN
	select @dtCYFromDate =DATEADD(ms,-1,DATEADD(DAY, 2 -  DATEPART(WEEKDAY, GETDATE()), GETDATE()))
	select @dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0)) 
	select @dtLYFromDate =DATEADD(year,-1,DATEADD(ms,-1,DATEADD(DAY, 2 -  DATEPART(WEEKDAY, GETDATE()), GETDATE())))
	select @dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))))
	END
	ELSE
	BEGIN
	SELECT @dtCYFromDate = @FinancialYearStartDate,@dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0));
	SELECT @dtLYFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),@dtLYToDate = DATEADD(MS,-1,DATEADD(YEAR,-1,DATEADD(MS, -3, DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE())+1, 0))));
	END
	------
	SELECT 
		@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=numDiscountServiceItemID
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID


	-- GET EXPENSE YTD VS SAME PERIOD LAST YEAR
	SELECT @ExpenseYTD = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtCYFromDate AND @dtCYToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))
	SELECT @ExpenseSamePeriodLastYear = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtLYFromDate AND @dtLYToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))

	-- GET PROFIT YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@ProfitYTD = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtCYFromDate AND @dtCYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	SELECT 
		@ProfitSamePeriodLastYear = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLYFromDate AND @dtLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	-- GET REVENUE YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@RevenueYTD = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtCYFromDate AND @dtCYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT 
		@RevenueSamePeriodLastYear = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLYFromDate AND @dtLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT
		100.0 * (ISNULL(@RevenueYTD,0) - ISNULL(@RevenueSamePeriodLastYear,0)) / CASE WHEN ISNULL(@RevenueSamePeriodLastYear,0) = 0 THEN 1 ELSE @RevenueSamePeriodLastYear END As RevenueDifference
		,100.0 * (ISNULL(@ProfitYTD,0) - ISNULL(@ProfitSamePeriodLastYear,0)) / CASE WHEN ISNULL(@ProfitSamePeriodLastYear,0) = 0 THEN 1 ELSE @ProfitSamePeriodLastYear END As ProfitDifference
		,100.0 * (ISNULL(@ExpenseYTD,0) - ISNULL(@ExpenseSamePeriodLastYear,0)) / CASE WHEN ISNULL(@ExpenseSamePeriodLastYear,0) = 0 THEN 1 ELSE @ExpenseSamePeriodLastYear END As ExpenseDifference
END
GO