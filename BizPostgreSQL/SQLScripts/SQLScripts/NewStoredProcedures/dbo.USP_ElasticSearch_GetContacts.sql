GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetContacts')
DROP PROCEDURE dbo.USP_ElasticSearch_GetContacts
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetContacts]
	@numDomainID NUMERIC(18,0)
	,@vcContactIds VARCHAR(MAX)
AS 
BEGIN
	
	SELECT
		numContactId AS id
		,'contact' AS module
		,ISNULL(tintCRMType,0) AS tintOrgCRMType
		,ISNULL(numCompanyType,0) AS numOrgCompanyType
		,ISNULL(DivisionMaster.numTerID,0) numTerID
		,ISNULL(AdditionalContactsInformation.numRecOwner,0) numRecOwner
		,CONCAT('<b style="color:#0099cc">Contact:</b> ',(CASE WHEN LEN(vcFirstName) > 0 THEN vcFirstName ELSE '-' END),' ',(CASE WHEN LEN(vcLastName) > 0 THEN vcLastName ELSE '-' END),', ',(CASE WHEN LEN(vcEmail) > 0 THEN vcEmail ELSE '-' END), ', ', vcCompanyName) AS displaytext
		,CONCAT((CASE WHEN LEN(vcFirstName) > 0 THEN vcFirstName ELSE '-' END),' ',(CASE WHEN LEN(vcLastName) > 0 THEN vcLastName ELSE '-' END)) AS [text]
		,CONCAT('/contact/frmContacts.aspx?CntId=',numContactId) AS url
		,ISNULL(vcFirstName,'') AS Search_vcFirstName
		,ISNULL(vcLastName,'') AS Search_vcLastName
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS Search_vcFullName
		,ISNULL(vcEmail,'') AS Search_vcEmail
		,ISNULL(vcCompanyName,'') AS Search_vcCompanyName
		,ISNULL(AdditionalContactsInformation.numCell,'') Search_numCell 
		,ISNULL(AdditionalContactsInformation.numHomePhone,'') Search_numHomePhone
		,ISNULL(AdditionalContactsInformation.numPhone,'') Search_numPhone
	FROM
		AdditionalContactsInformation
	INNER JOIN
		DivisionMaster
	ON
		AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		AdditionalContactsInformation.numDomainId=@numDomainID
		AND (@vcContactIds IS NULL OR AdditionalContactsInformation.numContactId IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcContactIds,'0'),',')))
		
END