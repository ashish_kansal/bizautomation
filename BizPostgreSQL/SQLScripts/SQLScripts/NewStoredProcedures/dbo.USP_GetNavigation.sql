GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNavigation')
DROP PROCEDURE dbo.USP_GetNavigation
GO
CREATE PROCEDURE [dbo].[USP_GetNavigation]
(
	@numDomainID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0)
)
AS 
BEGIN

	/********  MAIN MENU  ********/

	SELECT  
		T.numTabID,
		ISNULL((SELECT TOP 1 numTabName FROM TabDefault WHERE numDomainid = @numDomainID AND numTabId = T.numTabId AND tintTabType = T.tintTabType),T.numTabName) vcTabName,
		CASE 
			WHEN (SELECT COUNT(*) FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1) > 0
			THEN
				REPLACE(ISNULL((SELECT TOP 1 Link FROM ShortCutBar WHERE (numDomainid = @numDomainID OR bitDefault=1) AND numTabId = T.numTabId AND id=(SELECT TOP 1 numLinkId FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1)),''),'../','')
			ELSE 
				ISNULL(vcURL,'')
		END vcURL,
		ISNULL(vcAddURL,'') vcAddURL,
		ISNULL(bitAddIsPopUp,0) AS bitAddIsPopUp,
		numOrder
	FROM    
		TabMaster T
	JOIN 
		GroupTabDetails G ON G.numTabId = T.numTabId
	WHERE   
		(numDomainID = @numDomainID OR bitFixed = 1)
		AND numGroupID = @numGroupID
		AND ISNULL(G.[tintType], 0) <> 1
		AND tintTabType =1
		AND T.numTabID NOT IN (2,68)
	ORDER BY 
		numOrder   


	/********  SUB MENU  ********/

	DECLARE @numTabID NUMERIC(18,0)
	DECLARE @numModuleID AS NUMERIC(18, 0)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numOrigParentID NUMERIC(18,0),
		numParentID NUMERIC(18,0),
		numListItemID NUMERIC(18,0),
		numTabID NUMERIC(18,0),
		numPageNavID NUMERIC(18,0),
		vcNodeName VARCHAR(500),
		vcURL VARCHAR(1000),
		vcAddURL VARCHAR(1000),
		bitAddIsPopUp BIT,
		[order] int,
		bitVisible BIT,
		vcImageURL VARCHAR(1000)
	)

	BEGIN /******************** OPPORTUNITY/ORDER ***********************/

		SET @numTabID = 1
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO
			@TEMP
		SELECT
			0,
			0,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID  
			AND PND.numTabID = TNA.numTabID       
		WHERE  
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND numModuleID = @numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND PND.numParentID <> 0
		ORDER BY 
			numPageNavID 

		INSERT INTO
			@TEMP
		SELECT  
			0,
			0,
			0,
			@numTabID,
			0,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'',
			0,
			0,
			1 ,
			''
		FROM 
			dbo.ListDetails LD
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			TNA.numTabID=1
			AND LD.numListItemID = TNA.numPageNavID 
			AND ISNULL(bitVisible,0) = 1  
		WHERE 
			numListID = 27 
			AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
			AND ISNULL(TNA.[numGroupID], 0) = @numGroupID
		ORDER BY 
			numPageNavID 

	END

	BEGIN /******************** Email ***************************/
		SET @numTabID = 44
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** MARKETING ***************************/
		SET @numTabID = 3
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** SUPPORT/CASE ***************************/

		SET @numTabID = 4
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** REPORTS ***************************/

		SET @numTabID = 6
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** RELATIONSHIP ***********************/

		SET @numTabID = 7
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO 
			@TEMP
		SELECT    
			ISNULL(PND.numParentID,0),
			PND.numParentID AS numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
			ISNULL(vcNavURL, '') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			ISNULL(TreeNodeOrder.numOrder,1000) AS numOrder,
			ISNULL(TNA.bitVisible,1) AS bitVisible,
			ISNULL(PND.vcImageURL,'')
		FROM      
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			TNA.numPageNavID = PND.numPageNavID AND
			TNA.numDomainID = @numDomainID AND 
			TNA.numGroupID = @numGroupID
			AND TNA.numTabID=@numTabID
		LEFT JOIN
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID = PND.numPageNavID
		WHERE     
			ISNULL(PND.bitVisible, 0) = 1
			AND ISNULL(TNA.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND numModuleID = @numModuleID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

		INSERT INTO 
			@TEMP
		SELECT
			0,
			0 AS numParentID,
			ListDetails.numListItemID,
			@numTabID,
			0 AS numPageNavID,
			ListDetails.vcData AS [vcNodeName],
			'../prospects/frmCompanyList.aspx?RelId='+ convert(varchar(10),ListDetails.numListItemID),
			CASE WHEN ListDetails.numListItemID = 93 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',ListDetails.numListItemID,'&FormID=36') END,
			CASE WHEN ListDetails.numListItemID = 93 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,2000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM
			ListDetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = ListDetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 6 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE
			numListID = 5 AND
			(ListDetails.numDomainID = @numDomainID  OR constFlag = 1) AND
			(ISNULL(bitDelete,0) = 0 OR constFlag = 1) AND
			ListDetails.numListItemID <> 46 

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID = FRDTL.numPrimaryListItemID),
			L2.numListItemID,   
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			CONCAT('../prospects/frmCompanyList.aspx?RelId=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID),
			--CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36'),
			CASE WHEN L2.numListItemID = 37257 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36') END,
			--1,
			CASE WHEN L2.numListItemID = 37257 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,3000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM      
			FieldRelationship FR
		join 
			FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
		join 
			ListDetails L1 on numPrimaryListItemID = L1.numListItemID
		join 
			ListDetails L2 on numSecondaryListItemID = L2.numListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE     
			numPrimaryListItemID <> 46
			and FR.numPrimaryListID = 5
			and FR.numSecondaryListID = 21
			and FR.numDomainID = @numDomainID
			and L2.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			ISNULL((SELECT ID FROM @TEMP WHERE numPageNavID=11),0),
			L2.numListItemID,
			@numTabID,
			11,
			L2.vcData AS [vcNodeName],
			'../prospects/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',numSecondaryListItemID,'&FormID=35'),
			1,
			ISNULL(TreeNodeOrder.numOrder,4000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			12,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',numSecondaryListItemID,'&FormID=36'),
			1,
			ISNULL(TreeNodeOrder.numOrder,5000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=11),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',FRD.numPrimaryListItemID,'&FormID=35','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,7000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',FRD.numPrimaryListItemID,'&FormID=36','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,8000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		

	END

	BEGIN /******************** DOCUMENTS ***********************/
		SET @numTabID = 8

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			0,
			0,
			@numTabID,
			0,
			'Regular Documents' as vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=0',
			'~/Documents/frmGenDocPopUp.aspx',
			1,
			1,
			1,
			''


		INSERT INTO 
			@TEMP
		SELECT  
			0,
			SCOPE_IDENTITY(),
			Ld.numListItemId,
			@numTabID,
			0,
			vcData vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=' + cast(Ld.numListItemId as varchar(10)),
			CASE WHEN vcData = 'Email Template' THEN '~/Marketing/frmEmailTemplate.aspx' ELSE '' END,
			CASE WHEN vcData = 'Email Template' THEN 1 ELSE 0 END,
			ISNULL(intSortOrder, LD.sintOrder) SortOrder,
			1,
			''
		FROM    
			ListDetails LD
		LEFT JOIN 
			ListOrder LO 
		ON 
			LD.numListItemID = LO.numListItemID
			AND LO.numDomainId = @numDomainID
		WHERE   
			Ld.numListID = 29
			AND (constFlag = 1 OR Ld.numDomainID = @numDomainID)

	END

	BEGIN /******************** ACCOUNTING **********************/
	
		SET @numTabID = 45
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO 
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1 ,
			ISNULL(PND.vcImageURL,'')     
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE  
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND numModuleID=@numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,PND.numPageNavID 
	
		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(CASE numFinancialViewID 
				WHEN 26770 THEN 96 /*Profit & Loss*/
				WHEN 26769 THEN 176 /*Income & Expense*/
				WHEN 26768 THEN 98 /*Cash Flow Statement*/
				WHEN 26767 THEN 97 /*Balance Sheet*/
				WHEN 26766 THEN 81 /*A/R & A/P Aging*/
				WHEN 26766 THEN 82 /*A/R & A/P Aging*/
				ELSE 0
			END),
			numFRID,
			@numTabID,
			0,
			vcReportName,
			CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END + 
			CASE CHARINDEX('?',(CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END)) 
				WHEN 0 THEN '?FRID='
				ELSE '&FRID='
			END + CAST(numFRID AS VARCHAR),
			'',
			0,
			0,
			1,
			''
		FROM 
			dbo.FinancialReport 
		WHERE 
			numDomainID=@numDomainID
	
		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	  
	END

	BEGIN /******************** ITEMS **********************/

		SET @numTabID = 80
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			isnull(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** Activities ***************************/

		SET @numTabID = 36
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** HUMAN RESOURCE ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** CONTRACTS ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contracts' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** Procurement ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END
	BEGIN /******************** CONTACTS ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ISNULL(ID,0) FROM @TEMP WHERE numPageNavID=13),
			listdetails.numListItemID,
			@numTabID,
			0,
			listdetails.vcData AS [vcNodeName],
			'../contact/frmContactList.aspx?ContactType=' + CONVERT(VARCHAR(10), listdetails.numListItemID) AS vcNavURL,
		   '~/Contact/newcontact.aspx',
		   1,
			ISNULL(TreeNodeOrder.numOrder,6000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			listdetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = listdetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 13 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			numListID = 8
			AND (constFlag = 1 OR listdetails.numDomainID = @numDomainID)

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ISNULL(ID,0) FROM @TEMP WHERE numPageNavID=13),
			101 AS numListItemID,
			@numTabID,
			0,
			'Primary Contact' AS [vcNodeName],
			'../Contact/frmcontactList.aspx?ContactType=101',
		 '~/Contact/newcontact.aspx',
		   1,
			ISNULL((
						SELECT 
							numOrder 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),7000) AS numOrder,
			ISNULL((
						SELECT 
							bitVisible 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),1) AS bitVisible,
					''

		UPDATE 
			t1
		SET 
			t1.numParentID = 13
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID AND numParentID IS NULL

	END

	BEGIN /******************** Manufacturing ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** Projects ***************************/

		SET @numTabID = (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1 AND tintTabType=1)
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END


	;WITH CTE(ID) AS 
	(
		SELECT
			ID
		FROM
			@TEMP t1
		WHERE 
			ISNULL(numOrigParentID,0) > 0
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM PageNavigationDTL WHERE numParentID=0 AND numTabID=t1.numTabID)
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM @TEMP t2 WHERE t2.numTabID = t1.numTabID)
		UNION ALL
		SELECT 
			t2.ID
		FROM
			@TEMP t2
		JOIN
			CTE c
		ON
			t2.numParentID=c.ID
	)

	DELETE FROM @TEMP WHERE ID IN (SELECT ID FROM CTE)

	
	/** Admin Menu **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-1,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 13 --AND bitVisible=1  
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		PND.intSortOrder,
		(CASE WHEN PND.numPageNavID=79 THEN 2000 ELSE 0 END),	
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -1
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-1 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -1 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -1)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-1

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-1 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -1


	/** Advance Search **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-3,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 9
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		sintOrder,
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -3
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-3 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -3 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -3)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-3

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-3 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -3

	UPDATE 
		t1
	SET 
		t1.bitVisible = (SELECT bitVisible FROM @TEMP t2 WHERE t2.ID = t1.numParentID)
	FROM
		@TEMP t1
	WHERE
		ISNULL(t1.numParentID,0) > 0

	SELECT * FROM @TEMP WHERE bitVisible=1 ORDER BY numTabID, numParentID, [order]
END
GO
