
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteSitePage]    Script Date: 08/08/2009 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteSitePage')
DROP PROCEDURE USP_DeleteSitePage
GO
CREATE PROCEDURE [dbo].[USP_DeleteSitePage]
	@numPageID NUMERIC(9),
	@numDomainID numeric(9)
AS
BEGIN
	DELETE FROM [MetaTags] WHERE [numReferenceID] = @numPageID
	DELETE FROM [StyleSheetDetails] WHERE [numPageID] = @numPageID
	DELETE FROM [SitePages]
	WHERE [numPageID] = @numPageID AND [numDomainID] = @numDomainID
END


