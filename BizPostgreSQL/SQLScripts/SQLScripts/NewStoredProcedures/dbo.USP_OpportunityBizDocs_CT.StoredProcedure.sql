/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_CT')
DROP PROCEDURE USP_OpportunityBizDocs_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,, 18thMay2014>
-- Description:	<Description,,Get only Order type :Sales /or Purchase>
-- =============================================
Create PROCEDURE [dbo].[USP_OpportunityBizDocs_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	SET NOCOUNT ON;
 -- Insert statements for procedure here
	declare @tblFields as table 
	(
		FieldName varchar(200),
		FieldValue varchar(200)
	)

	Declare @Type char(1)
	Declare @ChangeVersion bigint
	Declare @CreationVersion bigint
	Declare @last_synchronization_version bigInt=null

	SELECT 			
		@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
	FROM 
		CHANGETABLE(CHANGES dbo.OpportunityBizDocs, 0) AS CT
	WHERE 
		numOppBizDocsId=@numRecordID

	IF (@ChangeVersion=@CreationVersion)--Insert
	BEGIN
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityBizDocs, @last_synchronization_version) AS CT
		WHERE  
			CT.numOppBizDocsId=@numRecordID
	END
	ELSE--UPDATE
	BEGIN
		SET @last_synchronization_version=@ChangeVersion-1     
	
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityBizDocs, @last_synchronization_version) AS CT
		WHERE  
			CT.numOppBizDocsId=@numRecordID

		--Fields Update & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			    vcBizDocID varchar(70),
				monAmountPaid varchar(70),
				numBizDocStatus varchar(70),
				numBizDocId varchar(70),
				monDealAmount varchar(70),
				numModifiedBy varchar(70),
				numCreatedBy varchar(70),
				bitRecurred varchar(70)
			)

		INSERT INTO @UFFields
		(
			vcBizDocID
			,monAmountPaid
			,numBizDocStatus
			,numBizDocId
			,monDealAmount
			,numModifiedBy
			,numCreatedBy
			,bitRecurred
		)
		SELECT
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'vcBizDocID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcBizDocID ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'monAmountPaid', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monAmountPaid, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numBizDocStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBizDocStatus, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numBizDocId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBizDocId, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount	,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numModifiedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numModifiedBy ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'numCreatedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCreatedBy ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityBizDocs'), 'bitRecurred', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitRecurred 
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityBizDocs, @last_synchronization_version) AS CT
		WHERE 
			ct.numOppBizDocsId=@numRecordID


		INSERT INTO @tblFields
		(
			FieldName
			,FieldValue
		)
		SELECT
			FieldName
			,FieldValue
		FROM
		(
			SELECT  vcBizDocID,monAmountPaid,numBizDocStatus,numBizDocId,monDealAmount,bitRecurred
			FROM @UFFields	
		) AS UP
		UNPIVOT
		(
			FieldValue FOR FieldName IN (vcBizDocID,monAmountPaid,numBizDocStatus,numBizDocId,monDealAmount,bitRecurred)
		) AS upv
		WHERE 
			FieldValue<>0
	END

 select @Type
DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

--For Fields Update Exection Point
DECLARE @Columns_Updated VARCHAR(1000)
SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')

SELECT @Columns_Updated

--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 71, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated
END
GO

