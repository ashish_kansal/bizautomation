GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_AddNewShortCutBarToExistingDomain' ) 
    DROP PROCEDURE USP_AddNewShortCutBarToExistingDomain
GO
CREATE PROCEDURE USP_AddNewShortCutBarToExistingDomain
    @vcLinkName VARCHAR(2000)
AS 
BEGIN


INSERT INTO ShortCutGrpConf
SELECT AGM.numGroupID,SCB.ID,SCB.[order],SCB.bitType,D.numDomainId,SCB.numTabId,0,0 FROM dbo.Domain D JOIN dbo.AuthenticationGroupMaster AGM ON D.numDomainId=AGM.numDomainID,ShortCutBar SCB
WHERE SCB.vcLinkName IN (SELECT Items FROM dbo.Split(@vcLinkName,',')) 


INSERT INTO ShortCutUsrCnf
SELECT UM.numGroupID,SCB.ID,SCB.[order],SCB.bitType,D.numDomainId,UM.numUserDetailId,SCB.numTabId,0,0,0 FROM dbo.Domain D JOIN dbo.UserMaster UM ON D.numDomainId=UM.numDomainID,ShortCutBar SCB
WHERE SCB.vcLinkName IN (SELECT Items FROM dbo.Split(@vcLinkName,',')) 

END


