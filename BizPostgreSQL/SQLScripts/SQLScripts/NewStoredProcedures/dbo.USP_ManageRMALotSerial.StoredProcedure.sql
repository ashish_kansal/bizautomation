SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageRMALotSerial')
DROP PROCEDURE USP_ManageRMALotSerial
GO
CREATE PROCEDURE [dbo].[USP_ManageRMALotSerial]  
    @numDomainId as numeric(9)=0,  
    @numReturnHeaderID as numeric(9)=0,  
    @numWareHouseItemID as numeric(9)=0,
    @numReturnItemID as numeric(9)=0,
    @tintMode AS TINYINT,
    @strItems as VARCHAR(1000)=''
AS  

	CREATE TABLE #tempLotSerial 
	(                                                                    
		ROWNUMBER INT IDENTITY(1,1),
		numWarehouseItemDTLID NUMERIC(18,0),
		numQty INT                                              
	)                       
  
	DECLARE @posComma INT, @strKeyVal VARCHAR(20)

	SET @strItems=RTRIM(@strItems)

	IF RIGHT(@strItems, 1) != ',' 
		SET @strItems = @strItems + ','

	SET @posComma=PATINDEX('%,%', @strItems)

	WHILE @posComma>1
	BEGIN
		SET @strKeyVal=LTRIM(RTRIM(SUBSTRING(@strItems, 1, @posComma-1)))
	
		DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

		SET @posBStart=PATINDEX('%(%', @strKeyVal)
		SET @posBEnd=PATINDEX('%)%', @strKeyVal)

		IF( @posBStart>1 AND @posBEnd>1)
		BEGIN
			SET @strName=LTRIM(RTRIM(SUBSTRING(@strKeyVal, 1, @posBStart-1)))
			SET	@strQty=LTRIM(RTRIM(SUBSTRING(@strKeyVal, @posBStart+1,LEN(@strKeyVal)-@posBStart-1)))
		END		
		ELSE
		BEGIN
			SET @strName=@strKeyVal
			SET	@strQty=1
		END
	  
		INSERT INTO #tempLotSerial 
		(
			numWarehouseItemDTLID,
			numQty
		)  
		VALUES
		(
			CAST(@strName AS NUMERIC(18,0)),
			CAST(@strQty AS INT)
		) 
	  
		 SET @strItems=SUBSTRING(@strItems, @posComma +1, LEN(@strItems)-@posComma)
		 SET @posComma=PATINDEX('%,%',@strItems)
	END

	DELETE FROM OppWarehouseSerializedItem WHERE numReturnHeaderID=@numReturnHeaderID AND numReturnItemID=@numReturnItemID

	DECLARE @minROWNUMBER INT
	DECLARE @maxROWNUMBER INT
	DECLARE @numWareHouseItmsDTLID NUMERIC(9)
	DECLARE @numQty AS NUMERIC(9)

	SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #tempLotSerial

	WHILE  @minROWNUMBER <= @maxROWNUMBER
	BEGIN
		SELECT  @numWareHouseItmsDTLID=numWarehouseItemDTLID, @numQty=numQty FROM #tempLotSerial X WHERE X.ROWNUMBER=@minROWNUMBER
    
		INSERT INTO OppWarehouseSerializedItem
		(
			numWarehouseItmsDTLID,
			numReturnHeaderID,
			numReturnItemID,
			numWarehouseItmsID,
			numQty
		)  
		SELECT  
			@numWareHouseItmsDTLID,
			@numReturnHeaderID,
			@numReturnItemID,
			@numWareHouseItemID,
			@numQty 
		
		
		SET @minROWNUMBER = @minROWNUMBER + 1
	END	
				
	DROP TABLE #tempLotSerial
GO
