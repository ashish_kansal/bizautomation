GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BankReconcileMaster_DeleteByID')
DROP PROCEDURE USP_BankReconcileMaster_DeleteByID
GO
CREATE PROCEDURE USP_BankReconcileMaster_DeleteByID
	@numDomainID NUMERIC(18,0)
	,@numReconcileID numeric(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	UPDATE General_Journal_Details SET bitReconcile=0,bitCleared=0,numReconcileID=0 WHERE numDomainId=@numDomainID AND numReconcileID=@numReconcileID
	DELETE FROM BankReconcileFileData WHERE numDomainID=@numDomainID AND numReconcileID=@numReconcileID
	DELETE FROM BankReconcileMaster WHERE numDomainID=@numDomainID AND numReconcileID=@numReconcileID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END