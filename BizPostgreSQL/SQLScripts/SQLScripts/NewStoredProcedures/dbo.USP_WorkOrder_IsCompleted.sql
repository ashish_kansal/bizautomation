GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_IsCompleted')
DROP PROCEDURE USP_WorkOrder_IsCompleted
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_IsCompleted]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numWOID NUMERIC(18,0)
AS                            
BEGIN
	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOID AND numWOId=23184)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END
GO