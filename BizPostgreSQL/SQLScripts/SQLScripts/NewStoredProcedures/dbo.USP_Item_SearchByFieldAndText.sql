GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_SearchByFieldAndText')
DROP PROCEDURE dbo.USP_Item_SearchByFieldAndText
GO

CREATE PROCEDURE [dbo].[USP_Item_SearchByFieldAndText]
	@numDomainId NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numFieldID NUMERIC(18,0)
	,@vcSearchText VARCHAR(300)
AS 
BEGIN
	DECLARE @monCost AS DECIMAL(20,5) = 0

	DECLARE @avgCost INT
	SET @avgCost = ISNULL((SELECT 1 numCost FROM Domain WHERE numDOmainId=@numDomainID),0)

	IF @numFieldID = 281 --SKU
	BEGIN
		SELECT TOP 1 
			Item.numItemCode
			,Item.vcItemName
			,Item.vcModelID
			,Item.charItemType
			,Item.txtItemDesc
			,Item.bitKitParent
			,Item.vcSKU
			,(CASE @avgCost WHEN 3 THEN ISNULL(Vendor.monCost,0) WHEN 2 THEN 0 ELSE ISNULL(monAverageCost,0) END) AS monCost 
		FROM 
			Item 
		LEFT JOIN
			Vendor
		ON
			Item.numVendorID = Vendor.numVendorID
			AND Item.numItemCode = Vendor.numItemCode
		WHERE 
			Item.vcSKU = @vcSearchText 
			AND Item.numDomainID = @numDomainId 
		ORDER BY 
			numItemCode
	END
	ELSE IF @numFieldID = 203  -- UPC
	BEGIN
		SELECT TOP 1 
			Item.numItemCode
			,Item.vcItemName
			,Item.vcModelID
			,Item.charItemType
			,Item.txtItemDesc
			,Item.bitKitParent
			,Item.vcSKU
			,(CASE @avgCost WHEN 3 THEN ISNULL(Vendor.monCost,0) WHEN 2 THEN 0 ELSE ISNULL(monAverageCost,0) END) AS monCost
		FROM 
			Item 
		LEFT JOIN
			Vendor
		ON
			Item.numVendorID = Vendor.numVendorID
			AND Item.numItemCode = Vendor.numItemCode 
		WHERE 
			Item.numBarCodeId = @vcSearchText
			AND Item.numDomainID = @numDomainId 
		ORDER BY 
			numItemCode
	END
	ELSE IF @numFieldID = 189  -- ItemName
	BEGIN
		SELECT TOP 1 
			Item.numItemCode
			,Item.vcItemName
			,Item.vcModelID
			,Item.charItemType
			,Item.txtItemDesc
			,Item.bitKitParent
			,Item.vcSKU
			,(CASE @avgCost WHEN 3 THEN ISNULL(Vendor.monCost,0) WHEN 2 THEN 0 ELSE ISNULL(monAverageCost,0) END) AS monCost 
		FROM 
			Item 
		LEFT JOIN
			Vendor
		ON
			Item.numVendorID = Vendor.numVendorID
			AND Item.numItemCode = Vendor.numItemCode 
		WHERE 
			Item.vcItemName = @vcSearchText 
			AND Item.numDomainID = @numDomainId 
		ORDER BY 
			numItemCode
	END
	ELSE IF @numFieldID = 211   -- ItemCode
	BEGIN
		SELECT TOP 1 
			Item.numItemCode
			,Item.vcItemName
			,Item.vcModelID
			,Item.charItemType
			,Item.txtItemDesc
			,Item.bitKitParent
			,Item.vcSKU
			,(CASE @avgCost WHEN 3 THEN ISNULL(Vendor.monCost,0) WHEN 2 THEN 0 ELSE ISNULL(monAverageCost,0) END) AS monCost
		FROM 
			Item 
		LEFT JOIN
			Vendor
		ON
			Item.numVendorID = Vendor.numVendorID
			AND Item.numItemCode = Vendor.numItemCode 
		WHERE 
			Item.numItemCode = @vcSearchText 
			AND Item.numDomainID = @numDomainId 
		ORDER BY 
			numItemCode
	END
	ELSE IF @numFieldID = 899  -- CustomerPart#
	BEGIN
		DECLARE @numCompanyID NUMERIC(18,0)
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

		SELECT TOP 1
			I.numItemCode
			,I.vcItemName
			,I.vcModelID
			,I.charItemType
			,I.txtItemDesc
			,I.bitKitParent
			,I.vcSKU
			,(CASE @avgCost WHEN 3 THEN ISNULL(Vendor.monCost,0) WHEN 2 THEN 0 ELSE ISNULL(monAverageCost,0) END) AS monCost
		FROM 
			ITEM I
		LEFT JOIN
			Vendor
		ON
			I.numVendorID = Vendor.numVendorID
			AND I.numItemCode = Vendor.numItemCode
		INNER JOIN 
			CustomerPartNumber CPN 
		ON 
			I.numItemCode = CPN.numItemCode
		WHERE 
			CPN.numDomainId = @numDomainId 
			AND CPN.CustomerPartNo = @vcSearchText
			AND CPN.numCompanyId = @numCompanyID
		ORDER BY 
			I.numItemCode
	END
	ELSE IF @numFieldID = 291 -- VendorPart#
	BEGIN
		SELECT TOP 1
			I.numItemCode
			,I.vcItemName
			,I.vcModelID
			,I.charItemType
			,I.txtItemDesc
			,I.bitKitParent
			,I.vcSKU
			,(CASE @avgCost WHEN 3 THEN ISNULL(V.monCost,0) WHEN 2 THEN 0 ELSE ISNULL(monAverageCost,0) END) AS monCost
		FROM 
			ITEM I
		INNER JOIN 
			Vendor V 
		ON 
			I.numItemCode = V.numItemCode
		WHERE 
			V.numDomainID = @numDomainId
			AND V.vcPartNo = @vcSearchText
		ORDER BY 
			I.numItemCode
	END
END
GO


