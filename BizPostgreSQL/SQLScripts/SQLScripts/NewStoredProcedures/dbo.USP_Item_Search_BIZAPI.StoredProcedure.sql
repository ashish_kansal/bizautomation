GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_Search_BIZAPI' ) 
    DROP PROCEDURE USP_Item_Search_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Search list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_Search_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@SearchText VARCHAR(100),
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		Item 
	WHERE 
		Item.numDomainID = @numDomainID 
		AND	Item.vcItemName LIKE '%'+@SearchText+'%'
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)

	SELECT
		*
	INTO
		#Temp
    FROM
	(SELECT
		ROW_NUMBER() OVER (ORDER BY Item.vcItemName asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE Item.monAverageCost END) monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		SUM(numOnHand) as numOnHand,                      
		SUM(numOnOrder) as numOnOrder,                      
		SUM(numReorder)  as numReorder,                      
		SUM(numAllocation)  as numAllocation,                      
		SUM(numBackOrder)  as numBackOrder,
		bintCreatedDate,
		bintModifiedDate,
		STUFF((SELECT CONCAT(',',numCategoryID) FROM ItemCategory WHERE numItemID=Item.numItemCode GROUP BY numCategoryID FOR XML PATH('')),1,1,'') AS vcCategories
	FROM
		Item
	LEFT JOIN  
		WareHouseItems W                  
	ON 
		W.numItemID=Item.numItemCode
	WHERE 
		Item.numDomainID = @numDomainID AND
		Item.vcItemName LIKE '%'+@SearchText+'%'
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID=Item.numItemCode AND DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter) > 0)
	GROUP BY
		Item.numItemCode,
		Item.vcItemName,
		Item.txtItemDesc,
		Item.charItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		Item.monAverageCost,
		Item.bitVirtualInventory,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		Item.bintCreatedDate,
		Item.bintModifiedDate) AS I
	WHERE 
		(@numPageIndex = 0 OR @numPageSize = 0) OR
		(RowNumber > ((@numPageIndex - 1) * @numPageSize) and RowNumber < ((@numPageIndex * @numPageSize) + 1))

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		ItemDetails
	WHERE
		ItemDetails.numItemKitID IN (SELECT numItemCode FROM #Temp)

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		TEMPCustFields.Fld_id AS Id,
		TEMPCustFields.Fld_label AS Name,
		TEMPCustFields.Fld_type AS [Type],
		TEMPCustFields.Fld_Value AS ValueId,
		CASE 
			WHEN TEMPCustFields.Fld_type='TextBox' or TEMPCustFields.Fld_type='TextArea' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='0' OR TEMPCustFields.Fld_Value='' THEN '-' ELSE TEMPCustFields.Fld_Value END)
			WHEN TEMPCustFields.Fld_type='SelectBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(TEMPCustFields.Fld_Value) END)
			WHEN TEMPCustFields.Fld_type='CheckBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		Item I
	OUTER APPLY
	(
		SELECT
			CFW_Fld_Master.Fld_id,
			CFW_Fld_Master.Fld_label,
			CFW_Fld_Master.Fld_type,
			CFW_FLD_Values_Item.Fld_Value
		FROM
			CFW_Fld_Master
		LEFT JOIN
			CFW_FLD_Values_Item 
		ON
			CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
			AND I.numItemCode = CFW_FLD_Values_Item.RecId
		WHERE 
			numDomainID = @numDomainID
			AND Grp_id = 5
	) TEMPCustFields
	WHERE 
		I.numDomainID = @numDomainID
		AND I.numItemCode IN (SELECT numItemCode FROM #Temp)

	DROp TABLE #Temp
END
GO
