GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_IsTaskPending')
DROP PROCEDURE USP_WorkOrder_IsTaskPending
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_IsTaskPending]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numWOID NUMERIC(18,0)
AS                            
BEGIN
	IF EXISTS (SELECT 
					SPDT.numTaskId 
				FROM 
					StagePercentageDetailsTask SPDT 
				WHERE
					SPDT.numDomainID=@numDomainID 
					AND SPDT.numWorkOrderId=@numWOID 
					AND ISNULL((SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4),0) = 0)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END
GO