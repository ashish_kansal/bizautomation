SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetByOppIDAndBizDocType')
DROP PROCEDURE USP_OpportunityBizDocs_GetByOppIDAndBizDocType
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetByOppIDAndBizDocType]
(                        
 @numDomainID AS NUMERIC(18,0),
 @numOppId AS NUMERIC(18,0),
 @numBizDocId AS NUMERIC(18,0)
)                        
AS 
BEGIN
	DELETE FROM ShippingReportItems WHERE [numShippingReportId] IN (SELECT 
																		numShippingReportId 
																	FROM 
																		ShippingReport 
																	WHERE 
																		numOppBizDocId IN (SELECT DISTINCT
																								numOppBizDocsId
																							FROM
																								OpportunityBizDocs
																							INNER JOIN
																								OpportunityMaster
																							ON
																								OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
																							LEFT JOIN 
																								ShippingReport
																							ON
																								OpportunityBizDocs.numOppBizDocsId = ShippingReport.numOppBizDocId
																							LEFT JOIN
																								ShippingBox 
																							ON
																								ShippingReport.numShippingReportId = ShippingBox.numShippingReportId
																							WHERE
																								OpportunityMaster.numDomainId=@numDomainID
																								AND OpportunityBizDocs.numOppId=@numOppId
																								AND numBizDocId = @numBizDocId
																								AND (ShippingBox.numBoxID IS NULL OR ISNULL(ShippingBox.vcShippingLabelImage,'') = ''))
																	)

	DELETE FROM ShippingBox WHERE [numShippingReportId] IN (SELECT 
																numShippingReportId 
															FROM 
																ShippingReport 
															WHERE 
																numOppBizDocId IN (SELECT DISTINCT
																						numOppBizDocsId
																					FROM
																						OpportunityBizDocs
																					INNER JOIN
																						OpportunityMaster
																					ON
																						OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
																					LEFT JOIN 
																						ShippingReport
																					ON
																						OpportunityBizDocs.numOppBizDocsId = ShippingReport.numOppBizDocId
																					LEFT JOIN
																						ShippingBox 
																					ON
																						ShippingReport.numShippingReportId = ShippingBox.numShippingReportId
																					WHERE
																						OpportunityMaster.numDomainId=@numDomainID
																						AND OpportunityBizDocs.numOppId=@numOppId
																						AND numBizDocId = @numBizDocId
																						AND (ShippingBox.numBoxID IS NULL OR ISNULL(ShippingBox.vcShippingLabelImage,'') = ''))
															)

	DELETE FROM ShippingReport WHERE numOppBizDocId IN (SELECT DISTINCT
															numOppBizDocsId
														FROM
															OpportunityBizDocs
														INNER JOIN
															OpportunityMaster
														ON
															OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
														LEFT JOIN 
															ShippingReport
														ON
															OpportunityBizDocs.numOppBizDocsId = ShippingReport.numOppBizDocId
														LEFT JOIN
															ShippingBox 
														ON
															ShippingReport.numShippingReportId = ShippingBox.numShippingReportId
														WHERE
															OpportunityMaster.numDomainId=@numDomainID
															AND OpportunityBizDocs.numOppId=@numOppId
															AND numBizDocId = @numBizDocId
															AND (ShippingBox.numBoxID IS NULL OR ISNULL(ShippingBox.vcShippingLabelImage,'') = ''))



	SELECT DISTINCT
		numOppBizDocsId
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
	LEFT JOIN 
		ShippingReport
	ON
		OpportunityBizDocs.numOppBizDocsId = ShippingReport.numOppBizDocId
	LEFT JOIN
		ShippingBox 
	ON
		ShippingReport.numShippingReportId = ShippingBox.numShippingReportId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityBizDocs.numOppId=@numOppId
		AND numBizDocId = @numBizDocId
		AND (ShippingBox.numBoxID IS NULL OR ISNULL(ShippingBox.vcShippingLabelImage,'') = '')
END