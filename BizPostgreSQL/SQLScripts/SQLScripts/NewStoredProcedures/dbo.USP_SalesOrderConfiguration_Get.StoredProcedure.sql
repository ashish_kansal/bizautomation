GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SalesOrderConfiguration_Get' ) 
    DROP PROCEDURE USP_SalesOrderConfiguration_Get
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 18 Feb 2014
-- Description:	Gets new sales order form configuration by DomainID
-- =============================================
CREATE PROCEDURE USP_SalesOrderConfiguration_Get
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM dbo.SalesOrderConfiguration WHERE numDomainID = @numDomainID
    
END
GO