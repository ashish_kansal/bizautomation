
--EXEC USP_GetTopOrders 93871,85853,1,5,110
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTopOrders')
DROP PROCEDURE USP_GetTopOrders
GO
CREATE PROCEDURE USP_GetTopOrders
    @numDivisionID NUMERIC,
    @numContactID NUMERIC,
    @tintOppType TINYINT,
    @TopCount INT,
    @numDomainID NUMERIC
AS 
BEGIN
        SELECT TOP (@TopCount) dbo.[FormatedDateFromDate](OM.[bintCreatedDate],OM.[numDomainId]) AS bintCreatedDate,OM.[numOppId],OM.[monDealAmount],
        OM.[bintCreatedDate] AS DummyColumn,OM.vcPOppName -- Added dummy column because on production somehow without it doesn't sort result on date desc,might be because of formatted datetime
        FROM    [OpportunityMaster] OM
                INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
                INNER JOIN [AdditionalContactsInformation] ACI ON OM.[numContactId] = ACI.[numContactId]
        WHERE   OM.[tintOppType] = @tintOppType
                AND OM.[numDivisionId] = @numDivisionID
                AND OM.[numContactId] = @numContactID
                AND OM.[numDomainId]=@numDomainID
                AND OM.tintOppStatus=1
		ORDER BY OM.[bintCreatedDate] desc
       
       SELECT   OI.[numOppId],
                OI.[numoppitemtCode],
                OI.[vcPathForTImage],
                OI.[numItemCode],
                OI.[vcItemName],
                OI.[vcModelID],
                OI.[vcItemDesc],
                OI.[numUnitHour],
                OI.[monTotAmount],
                dbo.[GetSerialNumberList](OI.[numoppitemtCode]) AS vcSerialNo
       FROM     [OpportunityItems] OI
                INNER JOIN [Item] I ON OI.[numItemCode] = I.[numItemCode]
       WHERE    I.[numDomainID] = @numDomainID
                AND OI.[numOppId] IN (
                SELECT TOP ( @TopCount )
                        OM.[numOppId]
                FROM    [OpportunityMaster] OM
                        INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
                        INNER JOIN [AdditionalContactsInformation] ACI ON OM.[numContactId] = ACI.[numContactId]
                WHERE   OM.[tintOppType] = @tintOppType
                        AND OM.[numDivisionId] = @numDivisionID
                        AND OM.[numContactId] = @numContactID
                        AND OM.[numDomainId] = @numDomainID
                        AND OM.tintOppStatus=1
                ORDER BY OM.[bintCreatedDate] desc )
              
END