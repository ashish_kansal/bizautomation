GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_PACKAGING_RULE')
	DROP PROCEDURE USP_MANAGE_PACKAGING_RULE
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_MANAGE_PACKAGING_RULE]
	@numPackagingRuleID numeric(18, 0),
	@vcRuleName varchar(1000),
	@numPackagingTypeID numeric(18, 0),
	@numFromQty numeric(18, 0),
	@numShipClassId NUMERIC(18),
	@numDomainID numeric(18, 0),
	@numShippingRuleID numeric(18,0)--added by sachin ::Date-31stJuly||To consolidate packaging n shipping rule
AS

IF EXISTS(SELECT 'col1' FROM dbo.PackagingRules WHERE vcRuleName = @vcRuleName AND [numPackagingRuleID] <> @numPackagingRuleID AND numDomainID = @numDomainID )
	BEGIN
		RAISERROR ('ERROR: Rule Name already exists. Please provide another Rule Name.', -- Message text.
					16, -- Severity.
					1 -- State.
				  );
		RETURN
	END

IF EXISTS(SELECT * FROM dbo.PackagingRules WHERE numPackagingTypeID = @numPackagingTypeID  
												   AND numShipClassId = @numShipClassId
												   AND numDomainID = @numDomainID
												   AND numFromQty = @numFromQty
												   AND [numPackagingRuleID] <> @numPackagingRuleID )
	BEGIN
		RAISERROR ('ERROR: Rule Detail already exists in another rule. Please use existing rule OR create a new rule detail.', -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		RETURN	
	END
			
IF EXISTS(SELECT [numPackagingRuleID] FROM [dbo].[PackagingRules] WHERE [numPackagingRuleID] = @numPackagingRuleID)
BEGIN
	UPDATE [dbo].[PackagingRules] SET
		[vcRuleName] = @vcRuleName,
		[numPackagingTypeID] = @numPackagingTypeID,
		[numFromQty] = @numFromQty,
		[numShipClassId] = @numShipClassId,
		[numDomainID] = @numDomainID,
		[numShippingRuleID]=@numShippingRuleID
	WHERE
		[numPackagingRuleID] = @numPackagingRuleID
END
ELSE
BEGIN
	INSERT INTO [dbo].[PackagingRules] (
		[vcRuleName],
		[numPackagingTypeID],
		[numFromQty],
		[numShipClassId],
		[numDomainID],
		[numShippingRuleID]
	) VALUES (
		@vcRuleName,
		@numPackagingTypeID,
		@numFromQty,		
		@numShipClassId,
		@numDomainID,
		@numShippingRuleID
	)
END


GO

