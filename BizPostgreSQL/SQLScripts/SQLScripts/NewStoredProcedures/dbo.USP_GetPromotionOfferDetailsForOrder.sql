
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Priya
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpromotionofferdetailsfororder')
DROP PROCEDURE usp_getpromotionofferdetailsfororder
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOfferDetailsForOrder] 

@numPromotionID AS NUMERIC(9)=0,
@numDomainID AS NUMERIC(9)=0,
@ClientTimeZoneOffset AS INT

AS
BEGIN
	SELECT
		[numProId]
		,[vcProName]
		,[numDomainId]
		,(CASE WHEN [dtValidFrom] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidFrom) ELSE dtValidFrom END) dtValidFrom
		,(CASE WHEN [dtValidTo] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidTo) ELSE dtValidTo END) dtValidTo
		,[bitNeverExpires]
		,[bitApplyToInternalOrders]
		,[bitAppliesToSite]
		,[bitRequireCouponCode]
		,STUFF((SELECT ',' + vcDiscountCode FROM DiscountCodes WHERE numPromotionID = PromotionOffer.numProId FOR XML PATH ('')), 1, 1, '') AS vcDiscountCode
		,ISNULL((SELECT TOP 1 CodeUsageLimit FROM DiscountCodes WHERE numPromotionID = PromotionOffer.numProId),0) CodeUsageLimit
		,[numCreatedBy]
		,[dtCreated]
		,[numModifiedBy]
		,[dtModified]
		,[tintDiscountType]
		,[IsOrderBasedPromotion]
		,ISNULL(bitUseForCouponManagement,0) bitUseForCouponManagement
    FROM
		PromotionOffer
    WHERE
		numProId = @numPromotionID AND numDomainId = @numDomainID

	SELECT numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = @numPromotionID AND numDomainId = @numDomainID
END