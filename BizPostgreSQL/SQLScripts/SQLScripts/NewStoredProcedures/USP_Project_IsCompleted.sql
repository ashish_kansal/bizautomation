GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Project_IsCompleted')
DROP PROCEDURE USP_Project_IsCompleted
GO  
CREATE PROCEDURE [dbo].[USP_Project_IsCompleted]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numProId NUMERIC(18,0)
AS                            
BEGIN
	IF EXISTS (SELECT numProId FROM ProjectsMaster WHERE numDomainID=@numDomainID AND numProId=@numProId)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END
