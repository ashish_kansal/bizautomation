SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageDycFieldColorScheme' ) 
    DROP PROCEDURE USP_ManageDycFieldColorScheme
GO
CREATE PROCEDURE [dbo].[USP_ManageDycFieldColorScheme]
    @numFieldColorSchemeID AS NUMERIC(9)=0,
    @numFieldID AS NUMERIC(9)=0,
    @numFormID AS NUMERIC(9)=0,
    @numDomainID AS NUMERIC(9)=0,
    @vcFieldValue AS VARCHAR(100)='0',
    @vcFieldValue1 AS VARCHAR(100)='0',
    @vcColorScheme AS VARCHAR(50)=0,
    @tintMode AS TINYINT=0 
AS 
	IF @tintMode=0 --Select
	BEGIN 
		SELECT DFCS.numFieldColorSchemeID,VDC.vcFieldName,DFCS.numFieldID,DFCS.numFormID,DFCS.numDomainID,VDC.vcAssociatedControlType,
		CASE WHEN VDC.vcAssociatedControlType IN('SelectBox','ListBox') THEN 
			 CASE WHEN VDC.vcDbColumnName='tintSource' then dbo.fn_GetOpportunitySourceValue(DFCS.vcFieldValue,DFCS.vcFieldValue1,VDC.numDomainID)
			 ELSE dbo.fn_getSelectBoxValue(VDC.vcListItemType,DFCS.vcFieldValue,VDC.vcDbColumnName) END 
			 WHEN VDC.vcAssociatedControlType ='CheckBox' THEN CASE WHEN DFCS.vcFieldValue='1' THEN 'Yes' ELSE 'No' END 
			 ELSE DFCS.vcFieldValue END AS vcFieldValue,ISNULL(DFCS.vcFieldValue1,'0') AS vcFieldValue1,DFCS.vcColorScheme,DFM.vcFormName  
			from DycFieldColorScheme DFCS join View_DynamicDefaultColumns VDC ON DFCS.numFormID=VDC.numFormID AND DFCS.numFieldID=VDC.numFieldID
			JOIN DynamicFormMaster DFM ON DFM.numFormID=DFCS.numFormID
			where DFCS.numDomainID=@numDomainID AND VDC.numFormID=@numFormID AND VDC.numDomainID=@numDomainID and isnull(VDC.bitAllowGridColor,0)=1
	END
	ELSE IF @tintMode=1 --Insert
	BEGIN
		--Check if color scheme is set for other field then throw error
		IF EXISTS (SELECT numFieldID FROM DycFieldColorScheme WHERE numFormID=@numFormID AND numDomainID=@numDomainID)
		BEGIN
			IF (SELECT DISTINCT numFieldID FROM DycFieldColorScheme WHERE numFormID=@numFormID AND numDomainID=@numDomainID)<>@numFieldID
				BEGIN
					RAISERROR ( 'Field_DEPENDANT', 16, 1 ) ;
					RETURN ;		
				END
		END  
		
		--Check if color scheme is already exists for FieldValue then throw error
		IF EXISTS (SELECT numFieldID FROM DycFieldColorScheme WHERE numFieldID=@numFieldID AND numFormID=@numFormID AND numDomainID=@numDomainID AND vcFieldValue=@vcFieldValue AND vcFieldValue1=@vcFieldValue1)
		BEGIN
			RAISERROR ( 'Value_DEPENDANT', 16, 1 ) ;
            RETURN ;
		END 
		
		--Otherwise insert record
		INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES (@numFieldID,@numFormID,@numDomainID,@vcFieldValue,@vcFieldValue1,@vcColorScheme)	
	END
	ELSE IF @tintMode=2 --Delete
	BEGIN
		DELETE FROM DycFieldColorScheme WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND numFieldColorSchemeID=@numFieldColorSchemeID
	END
	ELSE IF @tintMode=3 --Sales Fulfillment color
	BEGIN 
		SELECT DFCS.numFieldColorSchemeID,DFCS.numFieldID,DFCS.numFormID,DFCS.numDomainID,
			 LD.vcData AS vcFieldValue,DFCS.vcColorScheme
			from DycFieldColorScheme DFCS JOIN dbo.ListDetails LD ON DFCS.vcFieldValue=LD.numListItemID
			where DFCS.numDomainID=@numDomainID AND DFCS.numFormID=64
	END
	ELSE IF @tintMode=4 --Delete All Saved Data For that Form
	BEGIN
		DELETE FROM DycFieldColorScheme WHERE numDomainID=@numDomainID AND numFormID=@numFormID
	END
    