GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSimilarItem')
DROP PROCEDURE USP_ManageSimilarItem
GO
CREATE PROCEDURE USP_ManageSimilarItem
	@numDomainID NUMERIC(9),
	@numParentItemCode NUMERIC(9),
	@numItemCode NUMERIC(9),
	@vcRelationship VARCHAR(500),
	@bitPreUpSell BIT,
	@bitPostUpSell BIT, 
	@bitRequired BIT,
	@vcUpSellDesc VARCHAR(1000),
	@byteMode TINYINT
AS 
BEGIN
	IF @byteMode = 0
	BEGIN
		IF NOT EXISTS( SELECT * FROM [SimilarItems] WHERE [numDomainID]=@numDomainID AND numItemCode = @numItemCode AND numParentItemCode = @numParentItemCode)
		BEGIN
			INSERT INTO [SimilarItems] (
				[numDomainID],
				[numItemCode],
				[numParentItemCode],
				[vcRelationship],
				[bitPreUpSell], 
				[bitPostUpSell], 
				[bitRequired],
				[vcUpSellDesc]
			) VALUES (  
				/* numDomainID - numeric(18, 0) */ @numDomainID,
				/* vcExportTables - varchar(100) */ @numItemCode,
				/* dtLastExportedOn - datetime */ @numParentItemCode,
				@vcRelationship,
				@bitPreUpSell,
				@bitPostUpSell, 
				@bitRequired,
				@vcUpSellDesc) 
		END
	END
	ELSE IF @byteMode = 1
	BEGIN
		UPDATE [SimilarItems]
		SET [bitPreUpSell] = @bitPreUpSell,
			[bitPostUpSell] = @bitPostUpSell, 
			[bitRequired] =	@bitRequired
		WHERE numParentItemCode = @numParentItemCode
			AND numItemCode = @numItemCode
			AND numDomainID = @numDomainID
	END
END