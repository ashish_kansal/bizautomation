GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentWM_Get')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentWM_Get
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentWM_Get]
(
	@numDomainID NUMERIC(18,0)
	,@numPageIndex INT
	,@numPageSize INT
	,@vcSources VARCHAR(MAX)
	,@vcCountries VARCHAR(MAX)
	,@vcStates VARCHAR(MAX)
)
AS 
BEGIN
	SELECT
		COUNT(*) OVER() numTotalRecords
		,MSFWM.ID
		,(CASE 
			WHEN ISNULL(numOrderSource,0) = 0 AND tintSourceType=1 THEN 'Internal Order'
			WHEN ISNULL(numOrderSource,0) > 0 AND tintSourceType=1 THEN ISNULL(LDOS.vcData,'-') 
			WHEN tintSourceType=2 THEN ISNULL(S.vcSiteName,'-') 
			ELSE '-'
		END) vcOrderSource
		,ISNULL(LDCountry.vcData,'-') vcCountry
		,STUFF((SELECT 
					CONCAT(', ',(CASE WHEN LEN(ISNULL(LDS.vcAbbreviations,'')) > 0 THEN LDS.vcAbbreviations ELSE ISNULL(LDS.vcState,'') END))
				FROM 
					MassSalesFulfillmentWMState MSFWMS
				INNER JOIN
					[State] LDS
				ON
					LDS.numDomainID = @numDomainID
					AND MSFWMS.numStateID = LDS.numStateID
				WHERE 
					MSFWMS.numMSFWMID = MSFWM.ID
				ORDER BY 
					(CASE WHEN LEN(ISNULL(LDS.vcAbbreviations,'')) > 0 THEN LDS.vcAbbreviations ELSE ISNULL(LDS.vcState,'') END)
				FOR XML PATH('')),1,1,'') vcStates
		,STUFF((SELECT 
					CONCAT(', (',intOrder,') ',ISNULL(vcWareHouse,'-'))
				FROM 
					MassSalesFulfillmentWP MSFWWP
				INNER JOIN
					Warehouses W
				ON
					W.numDomainID = @numDomainID
					AND MSFWWP.numWareHouseID = W.numWareHouseID
				WHERE 
					MSFWWP.numMSFWMID = MSFWM.ID
				ORDER BY 
					MSFWWP.intOrder
				FOR XML PATH('')),1,1,'') vcWarehouses
	FROM
		MassSalesFulfillmentWM MSFWM
	LEFT JOIN
		ListDetails LDOS
	ON
		LDOS.numListID = 9
		AND MSFWM.numOrderSource = LDOS.numListItemID
		AND MSFWM.tintSourceType = 1
	LEFT JOIN
		Sites S
	ON
		S.numDomainID = @numDomainID
		AND MSFWM.numOrderSource = S.numSiteID
		AND MSFWM.tintSourceType = 2
	LEFT JOIN
		ListDetails LDCountry
	ON
		LDCountry.numListID = 40
		AND MSFWM.numCountryID = LDCountry.numListItemID
	WHERE
		MSFWM.numDomainID=@numDomainID
		AND (LEN(ISNULL(@vcSources,'')) = 0 OR CONCAT(numOrderSource,'~',tintSourceType) IN (SELECT OutParam FROM dbo.SplitString(@vcSources,',')))
		AND (LEN(ISNULL(@vcCountries,'')) = 0 OR numCountryID IN (SELECT Id FROM dbo.SplitIDs(@vcCountries,',')))
		AND (LEN(ISNULL(@vcStates,'')) = 0 OR EXISTS (SELECT MSFWMS.ID FROM MassSalesFulfillmentWMState MSFWMS WHERE MSFWMS.numMSFWMID=MSFWM.ID AND MSFWMS.numStateID IN (SELECT Id FROM dbo.SplitIDs(@vcStates,','))))
	ORDER BY
		LDOS.vcData
	OFFSET (@numPageIndex-1) ROWS FETCH NEXT @numPageSize ROWS ONLY
END
GO