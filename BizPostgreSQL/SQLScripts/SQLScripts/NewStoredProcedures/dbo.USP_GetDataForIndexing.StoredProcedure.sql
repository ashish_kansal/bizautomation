GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDataForIndexing' ) 
    DROP PROCEDURE USP_GetDataForIndexing
GO
-- exec USP_GetDataForIndexing @tintMode=0,@numDomainID=172,@numSiteID=90,@bitSchemaOnly=0,@tintAddUpdateMode=1,@bitDeleteAll=1
CREATE PROCEDURE USP_GetDataForIndexing
    @tintMode AS TINYINT = 0,
    @numDomainID AS NUMERIC,
    @numSiteID AS NUMERIC,
    @bitSchemaOnly AS BIT=0,
    @tintAddUpdateMode AS TINYINT, -- 1 = add , 2= update ,3=Deleted items
    @bitDeleteAll BIT=0 --Used for rebuilding all index from scratch
AS
BEGIN
    IF @tintMode = 0 -- items 
        BEGIN
        DECLARE @strSQL VARCHAR(4000)
        DECLARE @Fields VARCHAR(1000)
        DECLARE @numFldID VARCHAR(15)
        DECLARE @vcFldname VARCHAR(200)
        DECLARE @vcDBFldname VARCHAR(200)
        DECLARE @vcLookBackTableName VARCHAR(200)
        DECLARE @intRowNum INT
        DECLARE @strSQLUpdate VARCHAR(4000)
        DECLARE @Custom AS BIT
--SELECT * FROM View_DynamicColumns WHERE numFormid=30


--Select fields set for simple search from e-com settings as well fields from advance search
					SELECT  ROW_NUMBER() 
        OVER (ORDER BY vcFormFieldName) AS tintOrder,*
					INTO    #tempCustomField
					FROM    ( SELECT   distinct numFieldID as numFormFieldId,
										vcDbColumnName,
										vcFieldName as vcFormFieldName,
										0 AS Custom,
										vcLookBackTableName
							  FROM      View_DynamicColumns
							  WHERE     numFormId = 30
										AND numDomainID = @numDomainID
--										AND DFC.tintPageType = 1 --Commented so can agreegate all fields of simple and advance search
										AND isnull(numRelCntType,0) = 0
										AND isnull(bitCustom,0) = 0
										AND ISNULL(bitSettingField, 0) = 1
							  UNION
							  SELECT    numFieldID as numFormFieldId,
										vcFieldName,
										vcFieldName,
										1 AS Custom,
										'' vcLookBackTableName
							  FROM      View_DynamicCustomColumns
							  WHERE     Grp_id = 5
										AND numFormId = 30
										AND numDomainID = @numDomainID
										AND isnull(numRelCntType,0) = 0
--										AND isnull(DFC.bitCustom,0) = 1
--										AND ISNULL(DFC.tintPageType,0) = 1 --Commented so can agreegate all fields of simple and advance search

							) X
       
       IF @bitSchemaOnly =1
       BEGIN
			SELECT * FROM #tempCustomField
	   		RETURN
	   END
	   IF @bitDeleteAll = 1
	   BEGIN
	   		DELETE FROM LuceneItemsIndex WHERE numDomainID = @numDomainID AND numSiteID = @numSiteID
	   END
       
       --Create Fields query 
		 SET @Fields = '';
         SELECT TOP 1  @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName,@vcDBFldName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName
				FROM #tempCustomField WHERE Custom=0 ORDER BY tintOrder
				while @intRowNum>0                                                                                  
				 BEGIN
--					PRINT @numFldID
					IF(@vcDBFldName='numItemCode')
					set @vcDBFldName ='I.numItemCode'
				 
					SET @Fields = @Fields + ','  + @vcDBFldName + ' as ['  + @vcFldname + ']' 
					 

						SELECT TOP 1 @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName ,@vcDBFldName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName
						FROM #tempCustomField WHERE tintOrder >=@intRowNum AND Custom=0 ORDER BY tintOrder
							           
						if @@rowcount=0 set @intRowNum=0                                                                                          
				 END
       
--       PRINT @Fields
       
       
       SET @strSQL =  'SELECT ' + (CASE WHEN @tintAddUpdateMode = 1 THEN 'TOP 1000' ELSE '' END) + ' I.numItemCode AS numItemCode
			' + @Fields + '            
            INTO #DataForIndexing
            FROM    dbo.Item I 
            LEFT JOIN dbo.ItemExtendedDetails IED
            ON I.numItemCode = IED.numItemCode
            WHERE   I.numDomainID = ' + convert(varchar(10),@numDomainID) + '
                    AND I.numItemCode IN (
                    SELECT  numItemID
                    FROM    dbo.ItemCategory
                    WHERE ISNULL(I.IsArchieve,0) = 0 AND  numCategoryID IN ( SELECT   numCategoryID
                                               FROM     dbo.SiteCategories
                                               WHERE    numSiteID = ' + convert(varchar(10),@numSiteID) +' )) '
            
        IF @tintAddUpdateMode = 1 -- Add mode
			SET @strSQL = @strSQL +' and I.numItemCode Not in (SELECT numItemCode FROM LuceneItemsIndex WHERE numDomainID = '+ CONVERT(VARCHAR(10),@numDomainID) +  ' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') '
		IF @tintAddUpdateMode = 2 -- Update mode
			SET @strSQL = @strSQL +' and bintModifiedDate BETWEEN  DATEADD( hour,-1,GETUTCDATE()) AND GETUTCDATE() 
			AND I.numItemCode in(SELECT I1.numItemCode FROM dbo.Item I1 INNER JOIN dbo.ItemCategory IC ON I1.numItemCode = IC.numItemID
			INNER JOIN dbo.SiteCategories SC ON IC.numCategoryID = SC.numCategoryID
			WHERE SC.numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ' AND I1.numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID) + ')'
		IF @tintAddUpdateMode = 3 -- Delete mode
		BEGIN
			SET @strSQL = 'SELECT numItemCode FROM LuceneItemsIndex WHERE bitItemDeleted=1 and numDomainID = '+ CONVERT(VARCHAR(10),@numDomainID) +  ' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) 
			EXEC(@strSQL);
			RETURN
		END
			
       
     
       
       

				SET @strSQLUpdate=''
			--Add  custom Fields and data to table
				SELECT TOP 1  @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName
				FROM #tempCustomField WHERE Custom=1 ORDER BY tintOrder
				while @intRowNum>0                                                                                  
				 BEGIN
--				 PRINT @numFldID
					
				 			SET @strSQLUpdate = @strSQLUpdate + 'alter table #DataForIndexing add ['+@vcFldname+'_CustomField] varchar(500);'
							set @strSQLUpdate=@strSQLUpdate+ 'update #DataForIndexing set [' +@vcFldname + '_CustomField]=dbo.GetCustFldValueItem('+@numFldID+',numItemCode) where numItemCode>0'
							
					 

						SELECT TOP 1 @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName
						FROM #tempCustomField WHERE tintOrder >=@intRowNum AND Custom=1 ORDER BY tintOrder
							           
						if @@rowcount=0 set @intRowNum=0
				 END
				 SET @strSQLUpdate = @strSQLUpdate + ' SELECT * FROM #DataForIndexing'
				 
				 
				 IF @bitSchemaOnly=0
				 BEGIN
				 	SET @strSQLUpdate = @strSQLUpdate + ' INSERT INTO LuceneItemsIndex
				 	SELECT numItemCode,1,0,'+ CONVERT(VARCHAR(10), @numDomainID) +','+ CONVERT(VARCHAR(10), @numSiteID) +'  FROM #DataForIndexing where numItemCode not in (select numItemCode from LuceneItemsIndex where numDomainID ='+ CONVERT(VARCHAR(10), @numDomainID) +' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ' )'
				 END
				 
				 
				 SET @strSQLUpdate = @strSQLUpdate + ' DROP TABLE #DataForIndexing
												DROP TABLE #tempCustomField '
				 
				 SET @strSQL =@strSQL + @strSQLUpdate;
				 
				 
				 PRINT @strSQL
				 exec (@strSQL)
				 
				 
				 
				 
			
        END
        
	IF @tintMode= 1 
	BEGIN
		RETURN	
	END
	
	IF @tintMode= 2
	BEGIN
		RETURN	
	END
	
END