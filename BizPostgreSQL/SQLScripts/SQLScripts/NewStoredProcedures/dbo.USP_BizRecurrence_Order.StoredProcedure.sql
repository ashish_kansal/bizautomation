GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_BizRecurrence_Order')
DROP PROCEDURE USP_BizRecurrence_Order
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_Order]  
	@numRecConfigID NUMERIC(18,0), 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numRecurOppID NUMERIC(18,0) OUT,
	@numFrequency INTEGER,
	@dtEndDate DATE,
	@Date AS DATE
AS  
BEGIN  
BEGIN TRANSACTION;

BEGIN TRY

--DECLARE @Date AS DATE = GETDATE()

DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numOppType AS TINYINT
DECLARE @tintOppStatus AS TINYINT
DECLARE @DealStatus AS TINYINT
DECLARE @numStatus AS NUMERIC(9)
DECLARE @numCurrencyID AS NUMERIC(9)

DECLARE @intOppTcode AS NUMERIC(18,0)
DECLARE @numNewOppID AS NUMERIC(18,0)
DECLARE @fltExchangeRate AS FLOAT


--Get Existing Opportunity Detail
SELECT 
	@numDivisionID = numDivisionId, 
	@numOppType=tintOppType, 
	@tintOppStatus = tintOppStatus,
	@numCurrencyID=numCurrencyID,
	@numStatus = numStatus,
	@DealStatus = tintOppStatus
FROM 
	OpportunityMaster 
WHERE 
	numOppId = @numOppID                                            

IF ISNULL(@numCurrencyID,0) = 0 
BEGIN
	SET @fltExchangeRate=1
	SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
END
ELSE 
BEGIN
	SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
END

INSERT INTO OpportunityMaster                                                                          
(                                                                             
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	intPEstimatedCloseDate, numCreatedBy, bintCreatedDate, numDomainId, numRecOwner, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, numCurrencyID, fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, 
	vcOppRefOrderNo, bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, 
	bitDiscountType,fltDiscount,bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete,
	numAccountClass, tintTaxOperator, bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, 
	intUsedShippingCompany, bintAccountClosingDate
)                                                                          
SELECT
	numContactId, numDivisionId, txtComments, numCampainID, bitPublicFlag, tintSource, tintSourceType, vcPOppName, 
	@Date, @numUserCntID, @Date, numDomainId, @numUserCntID, lngPConclAnalysis, tintOppType, 
	numSalesOrPurType, @numCurrencyID, @fltExchangeRate, numAssignedTo, numAssignedBy, [tintOppStatus], numStatus, vcOppRefOrderNo, 
	bitStockTransfer,bitBillingTerms, intBillingDays,bitInterestType,fltInterest,vcCouponCode, bitDiscountType, fltDiscount,
	bitPPVariance, vcMarketplaceOrderID, vcMarketplaceOrderReportId, numPercentageComplete, numAccountClass,tintTaxOperator, 
	bitUseShippersAccountNo, bitUseMarkupShippingRate, numMarkupShippingRate, intUsedShippingCompany, @Date 
FROM
	OpportunityMaster
WHERE                                                                        
	numOppId = @numOppID

SET @numNewOppID=SCOPE_IDENTITY() 

EXEC USP_OpportunityMaster_CT @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@numRecordID=@numNewOppID                                             
  
--Update OppName as per Name Template
EXEC dbo.USP_UpdateNameTemplateValue @numOppType,@numDomainID,@numNewOppID

--Map Custom Field	
DECLARE @tintPageID AS TINYINT;
SET @tintPageID=CASE WHEN @numOppType=1 THEN 2 ELSE 6 END 
  	
EXEC dbo.USP_AddParentChildCustomFieldMap @numDomainID = @numDomainID, @numRecordID = @numNewOppID, @numParentRecId = @numDivisionId, @tintPageID = @tintPageID
 	
IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	EXEC USP_ManageOpportunityAutomationQueue
			@numOppQueueID = 0, -- numeric(18, 0)
			@numDomainID = @numDomainID, -- numeric(18, 0)
			@numOppId = @numNewOppID, -- numeric(18, 0)
			@numOppBizDocsId = 0, -- numeric(18, 0)
			@numOrderStatus = @numStatus, -- numeric(18, 0)
			@numUserCntID = @numUserCntID, -- numeric(18, 0)
			@tintProcessStatus = 1, -- tinyint
			@tintMode = 1 -- TINYINT
END

IF ISNULL(@numStatus,0) > 0 AND @numOppType=1 AND isnull(@DealStatus,0)=1
BEGIN
	IF @numOppType=1 AND @tintOppStatus=1
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus
	END
END


IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId = @numOppID) > 0
BEGIN
	-- INSERT OPPORTUNITY ITEMS

	INSERT INTO OpportunityItems                                                                          
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,numRecurParentOppItemID
	)
	SELECT 
		@numNewOppID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,
		fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
		numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,
		monAvgCost,bitItemPriceApprovalRequired,OpportunityItems.numoppitemtCode
	FROM
		OpportunityItems
	WHERE
		numOppID = @numOppID 
           
	EXEC USP_BizRecurrence_WorkOrder @numDomainId,@numUserCntID,@numNewOppID,@numOppID

	INSERT INTO OpportunityKitItems                                                                          
	(
		numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped,monAvgCost
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,OKI.numChildItemID,OKI.numWarehouseItemID,OKI.numQtyItemsReq,OKI.numQtyItemsReq,OKI.numUOMId,0,ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityKitItems OKI
	INNER JOIN
		OpportunityItems OI 
	ON
		OKI.numOppItemID=OI.numRecurParentOppItemID
	INNER JOIN
		Item I
	ON
		OKI.numCHildItemID=I.numItemCode
	WHERE 
		OKI.numOppID=@numOppID
		AND OI.numOppId=@numNewOppID  

	INSERT INTO OpportunityKitChildItems                                                                          
	(
		numOppId,numOppItemID,numOppChildItemID,numItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped,monAvgCost
	)
	SELECT 
		@numNewOppId,OI.numoppitemtCode,OKI.numOppChildItemID,OKCI.numItemID,OKCI.numWarehouseItemID,OKCI.numQtyItemsReq,OKCI.numQtyItemsReq,OKCI.numUOMId,0,ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityKitChildItems OKCI
	INNER JOIN
		OpportunityItems OI 
	ON
		OKCI.numOppItemID=OI.numRecurParentOppItemID
	INNER JOIN
		OpportuntiyKitItems OKI
	ON
		OKI.numOppItemID=OI.numOppItemtCOde
	INNER JOIN
		Item I
	ON
		OKCI.numItemID=I.numItemCode
	WHERE 
		OKCI.numOppID=@numOppID
		AND OI.numOppId=@numNewOppID 
END

DECLARE @TotalAmount AS FLOAT = 0       
DECLARE @tintCommitAllocation TINYINT                                                    
   
SELECT @TotalAmount = sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numNewOppID                                                                          
UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numNewOppID 
SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
--Updating the warehouse items              
DECLARE @tintShipped AS TINYINT = 0                

IF @tintOppStatus=1 AND @tintCommitAllocation=1
BEGIN       
	EXEC USP_UpdatingInventoryonCloseDeal @numNewOppID,0,@numUserCntID
END              

DECLARE @tintCRMType AS NUMERIC(9)
       
DECLARE @AccountClosingDate AS DATETIME 
SELECT @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate FROM OpportunityMaster WHERE numOppID=@numNewOppID         
SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID = @numDivisionID 

                 
--Add/Update Address Details
DECLARE @vcStreet varchar(100), @vcCity varchar(50), @vcPostalCode varchar(15), @vcCompanyName varchar(100), @vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
DECLARE @numState numeric(9),@numCountry numeric(9), @numCompanyId numeric(9), @numContact NUMERIC(18,0)
DECLARE @bitIsPrimary BIT, @bitAltContact BIT

SELECT  
	@vcCompanyName=vcCompanyName,
	@numCompanyId=div.numCompanyID 
FROM 
	CompanyInfo Com                            
JOIN 
	divisionMaster Div                            
ON 
	div.numCompanyID=com.numCompanyID                            
WHERE 
	div.numdivisionID=@numDivisionId

--Bill Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcBillStreet,''),
		@vcCity=ISNULL(vcBillCity,''),
		@vcPostalCode=ISNULL(vcBillPostCode,''),
		@numState=ISNULL(numBillState,0),
		@numCountry=ISNULL(numBillCountry,0),
		@vcAddressName=vcAddressName,
		@numContact=numBillingContact,
		@bitAltContact=bitAltBillingContact,
		@vcAltContact=vcAltBillingContact
	FROM  
		dbo.OpportunityAddress 
	WHERE   
		numOppID = @numOppID

  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 0,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId = 0,
		@vcAddressName = @vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
END
  
--Ship Address
IF (SELECT COUNT(*) FROM OpportunityAddress WHERE numOppID = @numOppID) > 0
BEGIN
	SELECT  
		@vcStreet=ISNULL(vcShipStreet,''),
		@vcCity=ISNULL(vcShipCity,''),
		@vcPostalCode=ISNULL(vcShipPostCode,''),
		@numState=ISNULL(numShipState,0),
		@numCountry=ISNULL(numShipCountry,0),
		@vcAddressName=vcAddressName,
		@numContact=numShippingContact,
		@bitAltContact=bitAltShippingContact,
		@vcAltContact=vcAltShippingContact
    FROM  
		dbo.OpportunityAddress
	WHERE   
		numOppID = @numOppID
 
  	EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numNewOppID,
		@byteMode = 1,
		@vcStreet = @vcStreet,
		@vcCity = @vcCity,
		@vcPostalCode = @vcPostalCode,
		@numState = @numState,
		@numCountry = @numCountry,
		@vcCompanyName = @vcCompanyName,
		@numCompanyId =@numCompanyId,
		@vcAddressName = @vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
END


-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
--Insert Tax for Division   
IF @numOppType=1 OR (@numOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN  
	INSERT INTO dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numNewOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numNewOppID,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivisionId 
		AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numNewOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numNewOppID,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivisionID	
	UNION 
	SELECT
		@numNewOppID
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numNewOppID,1,NULL)	
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numNewOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems 
(
	numOppId,
	numOppItemID,
	numTaxItemID,
	numTaxID
) 
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	TI.numTaxItemID,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.ItemTax IT 
ON 
	OI.numItemCode=IT.numItemCode 
JOIN
	TaxItems TI 
ON 
	TI.numTaxItemID=IT.numTaxItemID 
WHERE 
	OI.numOppId=@numNewOppID AND 
	IT.bitApplicable=1 AND 
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT 
	@numNewOppID,
	OI.numoppitemtCode,
	0,
	0
FROM 
	dbo.OpportunityItems OI 
JOIN 
	dbo.Item I 
ON 
	OI.numItemCode=I.numItemCode
WHERE 
	OI.numOppId=@numNewOppID AND 
	I.bitTaxable=1 AND
	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)
UNION
SELECT
	@numNewOppID,
	OI.numoppitemtCode,
	1,
	TD.numTaxID
FROM
	dbo.OpportunityItems OI 
INNER JOIN
	ItemTax IT
ON
	IT.numItemCode = OI.numItemCode
INNER JOIN
	TaxDetails TD
ON
	TD.numTaxID = IT.numTaxID
	AND TD.numDomainId = @numDomainId
WHERE
	OI.numOppId = @numNewOppID
	AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numNewOppID)

  
UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numNewOppID,@Date,0) WHERE numOppId=@numNewOppID


IF @numNewOppID > 0
BEGIN
	-- Insert newly created opportunity to transaction
	INSERT INTO RecurrenceTransaction ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	-- Insert newly created opportunity to transaction history - Records are not deleted in this table when parent record is delete
	INSERT INTO RecurrenceTransactionHistory ([numRecConfigID],[numRecurrOppID],[dtCreatedDate]) VALUES (@numRecConfigID,@numNewOppID,GETDATE())

	--Get next recurrence date for order 
	DECLARE @dtNextRecurrenceDate DATE = NULL

	SET @dtNextRecurrenceDate = (CASE @numFrequency
								WHEN 1 THEN DATEADD(D,1,@Date) --Daily
								WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
								WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
								WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
								END)
	
	--Increase value of number of transaction completed by 1
	UPDATE RecurrenceConfiguration SET numTransaction = ISNULL(numTransaction,0) + 1 WHERE numRecConfigID = @numRecConfigID
			
	PRINT @dtNextRecurrenceDate
	PRINT @dtEndDate

	-- Set recurrence status as completed if next recurrence date is greater than end date
	If @dtNextRecurrenceDate > @dtEndDate
	BEGIN
		UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
	END
	ELSE -- Set next recurrence date
	BEGIN
		UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
	END
END


UPDATE OpportunityMaster SET bitRecurred = 1 WHERE numOppId = @numNewOppID

SET @numRecurOppID = @numNewOppID

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

END  
