SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetTotalProgressItems')
DROP PROCEDURE USP_DemandForecast_GetTotalProgressItems
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetTotalProgressItems]  
(
	@numDomainID NUMERIC(18,0)
	,@numDFID NUMERIC(18,0)
)
AS  
BEGIN
	DECLARE @numDays AS INT

	SELECT
		@numDays = ISNULL(DemandForecastDays.numDays,0)
	FROM
		DemandForecast
	INNER JOIN
		DemandForecastDays
	ON
		DemandForecast.numDFDaysID = DemandForecastDays.numDFDaysID
	WHERE
		numDFID = @numDFID


	DECLARE @dtTo AS DATE = CAST(DATEADD(D,@numDays,GETDATE()) AS DATE)




	DECLARE @bitWarehouseFilter BIT = 0
	DECLARE @bitItemClassificationFilter BIT = 0
	DECLARE @bitItemGroupFilter BIT = 0

	IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
	BEGIN
		SET @bitWarehouseFilter = 1
	END

	IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
	BEGIN
		SET @bitItemClassificationFilter = 1
	END

	IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
	BEGIN
		SET @bitItemGroupFilter = 1
	END

	DECLARE @TEMP TABLE
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,bitKit BIT
		,bitAssembly BIT
		,vcItemName VARCHAR(300)
		,vcCustomer VARCHAR(300)
		,vcOppName VARCHAR(300)
		,dtEstimatedClosedDate VARCHAR(15)
		,vcUOM VARCHAR(50)
		,vcTotalProgress VARCHAR(100)
		,numOrderReleaseQty FLOAT
		,numPurchasedQty FLOAT
		,fltUOMConversionFactor FLOAT
	)


	-- GET ITEMS QTY BASED ON SALES OPORTUNITIES TOTAL PROGRESS
	INSERT INTO 
		@TEMP
	SELECT
		OM.numOppID
		,OI.numoppitemtCode
		,I.numItemCode
		,(CASE
			WHEN ISNULL(I.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WI.numWareHouseItemID,0)) > 0
			THEN
				(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0)) 
					THEN
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0))
					ELSE
						WI.numWareHouseItemID
				END)
			ELSE
				(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID AND numWLocationID = -1)
		END) numWarehouseItemID
		,ISNULL(I.bitKitParent,0)
		,ISNULL(I.bitAssembly,0)
		,ISNULL(I.vcItemName,'')
		,ISNULL(CI.vcCompanyName,'')
		,ISNULL(OM.vcPOppName,'')
		,dbo.FormatedDateFromDate(OM.intPEstimatedCloseDate,@numDomainID)
		,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = OI.numUOMId),'-')
		,CONCAT(PP.intTotalProgress,'% of', OI.numUnitHour)
		,CEILING(CAST(OI.numUnitHour - ISNULL(TEMPItemRelease.numItemReleaseQty,0) AS FLOAT)  * (PP.intTotalProgress/100))
		,ISNULL(TEMPItemRelease.numItemReleaseQty,0)
		,dbo.fn_UOMConversion(ISNULL(OI.numUOMId, 0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit, 0))
	FROM 
		OpportunityMaster OM
	INNER JOIN
		DivisionMaster DM
	ON
		DM.numDivisionID = OM.numDivisionId
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	INNER JOIN 
		OpportunityItems OI
	ON
		OM.numOppID = OI.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	LEFT JOIN
		ProjectProgress PP
	ON
		OM.numOppId = PP.numOppId
	OUTER APPLY
	(
		SELECT
			SUM(numQty) AS numItemReleaseQty
		FROM
			OpportunityItemsReleaseDates OIRD
		WHERE
			OIRD.numOppID = OM.numOppId
			AND OIRD.numOppItemID = OI.numoppitemtCode
			AND ((OIRD.dtReleaseDate <= @dtTo AND ISNULL(OIRD.tintStatus,0) = 1) OR ISNULL(OIRD.tintStatus,0) = 2)
	) TEMPItemRelease
	WHERE
		OM.numDomainId = @numDomainID
		AND OM.tintOppType = 1
		AND ISNULL(OM.tintOppStatus,0) = 0
		AND (OM.intPEstimatedCloseDate IS NOT NULL AND CAST(OM.intPEstimatedCloseDate AS DATE) <= @dtTo)
		AND ISNULL(numReleaseStatus,1) = 1
		AND ISNULL(PP.intTotalProgress,0) > 0
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
		)
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
			I.numItemClassification IN (SELECT numItemClassification FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
			I.numItemGroup IN (SELECT numItemGroup FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	SELECT * FROM @TEMP WHERE ISNULL(numOrderReleaseQty,0) > 0 AND ISNULL(numWarehouseItemID,0) > 0 ORDER BY numItemCode, numOppID, numOppItemID
END