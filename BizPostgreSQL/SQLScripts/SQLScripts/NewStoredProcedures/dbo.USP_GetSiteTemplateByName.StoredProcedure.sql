GO
/*OBSOLETE PROCEDURE*/
/****** Object:  StoredProcedure [dbo].[USP_GetSiteTemplateByName]    Script Date: 08/08/2009 16:10:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSiteTemplateByName')
DROP PROCEDURE USP_GetSiteTemplateByName
GO
CREATE PROCEDURE [dbo].[USP_GetSiteTemplateByName]
                @vcTemplateName VARCHAR(50),
                @numSiteID      NUMERIC(9),
                @numDomainID    NUMERIC(9)
AS
  BEGIN
    SELECT 
			[numTemplateID],
		   [vcTemplateName],
		   [txtTemplateHTML]
    FROM   [SiteTemplates]
    WHERE  [numSiteID] = @numSiteID
           AND [numDomainID] = @numDomainID
           AND lower([vcTemplateName]) = LOWER(@vcTemplateName)
  END
