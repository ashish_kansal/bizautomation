GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_Delete')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_Delete
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@ID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)
AS 
BEGIN
	IF @ID = -1
	BEGIN
		DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID
		UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract=0,bitTaskClosed=0 WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID
	END
	ELSE
	BEGIN
		DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND ID=@ID
	END
END
GO