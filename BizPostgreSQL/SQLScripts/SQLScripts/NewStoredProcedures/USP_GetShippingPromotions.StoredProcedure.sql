GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingPromotions' ) 
    DROP PROCEDURE USP_GetShippingPromotions
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[USP_GetShippingPromotions]
    @numDomainID AS NUMERIC(9) = 0,
	@byteMode TinyInt=0,
	@numUserCntId NUMERIC = 0,
	@numShippingCountry AS NUMERIC(18,0)=0,
	@cookieId VARCHAR(MAX) = NULL
AS 
BEGIN
	IF @byteMode = 0
    BEGIN 
		SELECT  monFreeShippingOrderAmount, * FROM ShippingPromotions WHERE numDomainID = @numDomainID
	END
	ELSE IF @byteMode = 1
	BEGIN
		DECLARE @vcShippingDescription VARCHAR(MAX), @vcQualifiedShipping VARCHAR(MAX), @decmPrice DECIMAL(18,2)

			--SET @decmPrice=(select ISNULL(SUM(monTotAmount),0) from CartItems where numDomainId = @numDomainID AND vcCookieId = @cookieId AND numUserCntId = @numUserCntId)
			SET @decmPrice=(select ISNULL(SUM(monTotAmount),0) from CartItems where numDomainId = @numDomainID AND numUserCntId = @numUserCntId AND vcCookieId = @cookieId )

		SELECT @vcShippingDescription=(CONCAT
					(
						(CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice < monFixShipping1OrderAmount THEN CONCAT('Spend $',(CAST((CAST(ROUND(monFixShipping1OrderAmount-@decmPrice,2) AS DECIMAL(10,2)))AS VARCHAR)), CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',CAST((CAST(ROUND(monFixShipping1Charge,2) AS DECIMAL(10,2)))AS VARCHAR),' shipping. ') ELSE '' END)
						,(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice < monFixShipping2OrderAmount THEN CONCAT('Spend $',(CAST((CAST(ROUND(monFixShipping2OrderAmount-@decmPrice,2) AS DECIMAL(10,2)))AS VARCHAR)), CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',CAST((CAST(ROUND(monFixShipping2Charge,2) AS DECIMAL(10,2)))AS VARCHAR),' shipping. ') ELSE '' END)
						,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) AND @decmPrice<monFreeShippingOrderAmount THEN CONCAT('Spend $',(CAST((CAST(ROUND(monFreeShippingOrderAmount-@decmPrice,2) AS DECIMAL(10,2)))AS VARCHAR)), CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and shipping is FREE! ') ELSE '' END) 
					
					)),
					@vcQualifiedShipping=((CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice > monFixShipping1OrderAmount AND 1=(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice<monFixShipping2OrderAmount THEN 1 WHEN ISNULL(bitFixShipping2,0)=0 THEN 1 ELSE 0 END) THEN 'You are qualified for $'+CAST((CAST(ROUND(monFixShipping1Charge,2) AS DECIMAL(10,2)))AS VARCHAR)+' shipping'
							   WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice > monFixShipping2OrderAmount  AND 1=(CASE WHEN ISNULL(bitFreeShiping,0)=1 AND @decmPrice<monFreeShippingOrderAmount THEN 1 WHEN ISNULL(bitFreeShiping,0)=0 THEN 1 ELSE 0 END) THEN 'You are qualified for $'+CAST((CAST(ROUND(monFixShipping2Charge,2) AS DECIMAL(10,2)))AS VARCHAR)+' shipping'
							   WHEN (ISNULL(bitFreeShiping,0)=1 AND numFreeShippingCountry=@numShippingCountry AND @decmPrice>monFreeShippingOrderAmount) THEN 'You are qualified for Free Shipping' END
						))
			FROM
				PromotionOffer PO
				LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
			WHERE
				PO.numDomainId=@numDomainID 

		SELECT @vcShippingDescription AS ShippingDescription, @vcQualifiedShipping AS QualifiedShipping
	END
END  