GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ValidateItemDomain')
DROP PROCEDURE USP_ValidateItemDomain
GO
-- =============================================
-- Author:		<Author,,Manish Anjara>
-- Create date: <Create Date,,20-aug-2014>
-- Description:	<Description,, Validate whether item is in supplied domain or not>
-- =============================================
Create PROCEDURE [dbo].[USP_ValidateItemDomain]
	-- Add the parameters for the stored procedure here
	@numItemCode NUMERIC(18,0),
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	SELECT 
		@numDomainID
		,@numItemCode
		,(CASE WHEN EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode) THEN 1 ELSE 0 END) IsItemInDomain
END
