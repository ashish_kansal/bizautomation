SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

------- Created by Priya 
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getorderproaudittraildetails')
DROP PROCEDURE usp_getorderproaudittraildetails
GO
CREATE PROCEDURE [dbo].[USP_GetOrderProAuditTrailDetails]
    @numProId AS NUMERIC(9) = 0,   
    @numDomainID AS NUMERIC(9) = 0,
	@numItemCode AS NUMERIC(9) = 0
AS 
BEGIN
   SELECT DC.vcDiscountCode, DC.numDiscountId, vcPOppName, OM.numOppID, OM.numDivisionId,
   (
        SELECT 
			CASE WHEN COUNT(OBDI.numOppBizDocID) > 0 THEN 'Yes'
			ELSE 'No'
			END
		
        FROM OpportunityMaster OM
		INNER JOIN OpportunityItems OI
		ON OM.numOppId = OI.numOppId
		INNER JOIN OpportunityBizDocItems OBDI
		ON OI.numItemCode = OBDI.numItemCode
		INNER JOIN OpportunityBizDocs OBD
		ON OBDI.numOppBizDocID = OBD.numBizDocId
		WHERE OBDI.numItemCode = @numItemCode 
       
    ) As HasBizDoc
   
   FROM OpportunityMaster OM
   INNER JOIN DiscountCodes DC
   ON OM.numDiscountID = DC.numDiscountId
   WHERE OM.numDomainId = @numDomainID
	    AND DC.numPromotionID = @numProId
   	
END      
  
GO
