GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_SHIPPING_LABEL_RULEMASTER')
	DROP PROCEDURE USP_MANAGE_SHIPPING_LABEL_RULEMASTER
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_MANAGE_SHIPPING_LABEL_RULEMASTER]
	@numShippingRuleID numeric(18, 0) OUTPUT,
	@vcRuleName	VARCHAR(100),
	@tintShipBasedOn tinyint,
	@numSourceCompanyID numeric(18, 0),
	@numSourceShipID numeric(18, 0),
	@intItemAffected int,
	@tintType tinyint,
	@numDomainID numeric(18, 0)
AS

IF EXISTS(SELECT * FROM dbo.ShippingLabelRuleMaster WHERE vcRuleName = @vcRuleName AND [numShippingRuleID] <> @numShippingRuleID AND numDomainID = @numDomainID)
	BEGIN
	    RAISERROR ('ERROR: Rule Name already exists. Please provide another Rule Name.', -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
        RETURN
	END

IF EXISTS(SELECT * FROM dbo.ShippingLabelRuleMaster WHERE [numShippingRuleID] <> @numShippingRuleID 
													  AND [vcRuleName] = @vcRuleName
													  AND [tintShipBasedOn] = @tintShipBasedOn
													  AND [numSourceCompanyID] = @numSourceCompanyID
													  AND [numSourceCompanyID] = @numSourceCompanyID
													  AND [numSourceShipID] = @numSourceShipID
													  AND [intItemAffected] = @intItemAffected
												      AND [tintType] = @tintType
													  AND [numDomainID] = @numDomainID 			)
	BEGIN
		RAISERROR ('ERROR: Rule Detail already exists in another rule. Please use existing rule OR create a new rule detail.', -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
		RETURN	
	END	
	
IF EXISTS(SELECT [numShippingRuleID] FROM [dbo].[ShippingLabelRuleMaster] WHERE [numShippingRuleID] = @numShippingRuleID)
BEGIN
	UPDATE [dbo].[ShippingLabelRuleMaster] SET
		[vcRuleName] = @vcRuleName,
		[tintShipBasedOn] = @tintShipBasedOn,
		[numSourceCompanyID] = @numSourceCompanyID,
		[numSourceShipID] = @numSourceShipID,
		[intItemAffected] = @intItemAffected,
		[tintType] = @tintType,
		[numDomainID] = @numDomainID
	WHERE
		[numShippingRuleID] = @numShippingRuleID
	
--	IF EXISTS(SELECT * FROM ShippingRuleStateList WHERE numRuleID = @numShippingRuleID AND numDomainID = @numDomainID )
--	BEGIN
--		DELETE FROM ShippingRuleStateList WHERE numRuleID = @numShippingRuleID AND numDomainID = @numDomainID	
--	END
	
		
END
ELSE
BEGIN
	
	INSERT INTO [dbo].[ShippingLabelRuleMaster] (
		[vcRuleName],
		[tintShipBasedOn],
		[numSourceCompanyID],
		[numSourceShipID],
		[intItemAffected],
		[tintType],
		[numDomainID]
	) VALUES (
		@vcRuleName,
		@tintShipBasedOn,
		@numSourceCompanyID,
		@numSourceShipID,
		@intItemAffected,
		@tintType,
		@numDomainID
	)
	
	SET @numShippingRuleID = SCOPE_IDENTITY()
END

DELETE FROM ShippingLabelRuleChild WHERE numShippingRuleID = @numShippingRuleID 
