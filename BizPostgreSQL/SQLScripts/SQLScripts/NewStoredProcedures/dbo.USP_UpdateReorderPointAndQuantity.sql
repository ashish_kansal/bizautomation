GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateReorderPointAndQuantity')
DROP PROCEDURE dbo.USP_UpdateReorderPointAndQuantity
GO
CREATE PROCEDURE [dbo].[USP_UpdateReorderPointAndQuantity]

AS 
BEGIN
	IF DATENAME(WEEKDAY,GETDATE()) = 'Sunday' AND (DATEPART(HOUR,GETDATE()) >= 0 AND DATEPART(HOUR,GETDATE()) <= 7) -- BETWEEN 0 AM TO 7 AM
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numDomainID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numVendorID NUMERIC(18,0)
			,numLeadTimeDays NUMERIC(18,0)
			,tintReorderPointBasedOn TINYINT
			,intReorderPointDays INT
			,intReorderPointPercent INT
		)

		INSERT INTO @TEMP
		(
			numDomainID
			,numItemCode
			,numVendorID
			,tintReorderPointBasedOn
			,intReorderPointDays
			,intReorderPointPercent
		)
		SELECT TOP 500
			Item.numDomainID
			,Item.numItemCode
			,Item.numVendorID
			,ISNULL(tintReorderPointBasedOn,1)
			,ISNULL(intReorderPointDays,30)
			,ISNULL(intReorderPointPercent,0)
		FROM
			Item
		INNER JOIN
			Domain
		ON
			Item.numDomainID = Domain.numDomainId
		WHERE
			ISNULL(bitAutomateReorderPoint,0) = 1
			AND (dtLastAutomateReorderPoint IS NULL OR dtLastAutomateReorderPoint <> CAST(GETDATE() AS DATE))

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT 
		DECLARE @numDomainID NUMERIC(18,0)
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @numVendorID NUMERIC(18,0)
		DECLARE @fltUnitSold FLOAT
		DECLARE @numLeadTimeDays NUMERIC(18,0)
		DECLARE @tintReorderPointBasedOn TINYINT
		DECLARE @intReorderPointDays INT
		DECLARE @intReorderPointPercent INT

		SELECT @iCount = COUNT(*) FROM @TEMP

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numDomainID=numDomainID
				,@numItemCode=numItemCode
				,@numVendorID=numVendorID
				,@tintReorderPointBasedOn=tintReorderPointBasedOn
				,@intReorderPointDays=intReorderPointDays
				,@intReorderPointPercent=intReorderPointPercent
			FROM
				@TEMP
			WHERE
				ID=@i

			SET @numLeadTimeDays = ISNULL((SELECT TOP 1 
												numListValue 
											FROM 
												VendorShipmentMethod
											INNER JOIN
												AddressDetails 
											ON
												AddressDetails.numRecordID = VendorShipmentMethod.numVendorID
												AND tintAddressOf=2 
												AND tintAddressType=2 
												AND ISNULL(bitIsPrimary,0)=1
											WHERE
												VendorShipmentMethod.numVendorID = @numVendorID),0)

			SET @fltUnitSold = ISNULL((SELECT 
											SUM(OI.numUnitHour)
										FROM 
											OpportunityMaster OM
										INNER JOIN 
											OpportunityItems OI
										ON
											OM.numOppId = OI.numOppId
										WHERE
											OM.numDomainId=@numDomainID
											AND OM.tintOppType=1
											AND OM.tintOppStatus=1
											AND OI.numItemCode = @numItemCode
											AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CAST(DATEADD(DAY,-@intReorderPointDays,(CASE WHEN @tintReorderPointBasedOn=2 THEN DATEADD(YEAR,-1,GETDATE()) ELSE GETDATE() END)) AS DATE) AND CAST((CASE WHEN @tintReorderPointBasedOn=2 THEN DATEADD(YEAR,-1,GETDATE()) ELSE GETDATE() END) AS DATE)),0)
			
			PRINT CAST(DATEADD(DAY,-@intReorderPointDays,(CASE WHEN @tintReorderPointBasedOn=2 THEN DATEADD(YEAR,-1,GETDATE()) ELSE GETDATE() END)) AS DATE)
			PRINT @fltUnitSold
			
			UPDATE WareHouseItems SET numReorder = CEILING(ISNULL((@fltUnitSold/@intReorderPointDays) * (CASE WHEN ISNULL(@numLeadTimeDays,0) = 0 THEN 1 ELSE @numLeadTimeDays END),0)) WHERE numDomainID=@numDomainID AND numItemID=@numItemCode

			UPDATE
				Item
			SET
				fltReorderQty = CEILING(ISNULL((@fltUnitSold/@intReorderPointDays) * (CASE WHEN ISNULL(@numLeadTimeDays,0) = 0 THEN 1 ELSE @numLeadTimeDays END),0) + (ISNULL((@fltUnitSold/@intReorderPointDays) * (CASE WHEN ISNULL(@numLeadTimeDays,0) = 0 THEN 1 ELSE @numLeadTimeDays END),0) * @intReorderPointPercent / 100))  
				,dtLastAutomateReorderPoint = GETDATE()
			WHERE
				numItemCode=@numItemCode
	

			SET @i = @i + 1
		END
	END
END
GO