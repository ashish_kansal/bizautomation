GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_Restore' ) 
    DROP PROCEDURE USP_EmailHistory_Restore
GO
CREATE PROCEDURE USP_EmailHistory_Restore
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numEmailHstrID AS VARCHAR(MAX)
AS 
BEGIN
	
	DECLARE @numInboxNodeID NUMERIC(18,0)
	DECLARE @strSQL AS VARCHAR(MAX)

	SELECT @numInboxNodeID=ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND numFixID = 1 AND ISNULL(bitSystem,0) = 1

	SET @strSQL = 'UPDATE EmailHistory SET bitArchived=0, numNodeId = ' + CAST(@numInboxNodeID AS VARCHAR(18)) + ' WHERE numEmailHstrID IN (' + @numEmailHstrID + ')'

	EXEC(@strSQL)
END



