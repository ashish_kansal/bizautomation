
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAllTransactionsOfCOAId')
	DROP PROCEDURE USP_GetAllTransactionsOfCOAId
GO
/****** Added By : Joseph ******/
/****** Gets Transactions Details List for the Chart Of Account ID******/


CREATE PROCEDURE [dbo].[USP_GetAllTransactionsOfCOAId]
@numAccountID numeric(18, 0),
@CurrentPage AS INTEGER,
@PageSize AS INTEGER,
@TotRecs AS INTEGER OUTPUT

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN  


/*paging logic*/  
        DECLARE @firstRec AS INTEGER                                                              
        DECLARE @lastRec AS INTEGER                                                     
--        IF @vcOppBizDocIds <>'' SET @CurrentPage = 1; --bug id 1081
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )                                                                    

                                     
                                     
DECLARE @strSql AS VARCHAR(5000) 
DECLARE @strSql1 AS VARCHAR(5000) 

  SET @strSql = 'SELECT BST.numTransactionId,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
BST.vcTxType,BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName,
ISNULL((SELECT TOP 1 vcCompanyname + ''~'' + CONVERT(VARCHAR(10), D.numDivisionID) FROM dbo.companyinfo C  
Join DivisionMaster D  on D.numCompanyID=C.numCompanyID WHERE vcCompanyname LIKE  ''%''+  BST.vcPayeeName  + ''%''),'''')
AS CompanyName,
ISNULL((SELECT TOP 1 DM.numDepositId  FROM dbo.DepositMaster DM 
WHERE DM.monDepositAmount = ABS(BST.monAmount) AND 
DM.dtDepositDate  BETWEEN DATEADD(d , -10 , BST.dtDatePosted ) AND DATEADD (d , 10 , BST.dtDatePosted )),0) AS numReferenceID,
6 AS ReferenceTypeId
FROM dbo.bankStatementTransactions BST 
INNER JOIN dbo.BankStatementHeader BSH ON BST.numStatementId = BSH.numStatementID 
INNER JOIN dbo.BankDetails BD ON BSH.numBankDetailId = BD.numBankDetailId 
INNER JOIN dbo.BankMaster BM ON BD.numbankMasterId = BM.numBankMasterID'

SET @strSql = @strSql + ' WHERE BST.numAccountId = ' + CONVERT(VARCHAR(15), @numAccountID)  + ' AND BST.monAmount > 0 AND BST.bitIsActive = 1 AND
NOT EXISTS (SELECT * FROM dbo.BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID)
UNION ALL

SELECT BST.numTransactionId,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
BST.vcTxType,BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName,
ISNULL((SELECT TOP 1 vcCompanyname + ''~'' + CONVERT(VARCHAR(10), D.numDivisionID) FROM dbo.companyinfo C  
Join DivisionMaster D  on D.numCompanyID=C.numCompanyID WHERE vcCompanyname LIKE  ''%''+  BST.vcPayeeName  + ''%''),'''')
AS companyName,
ISNULL((SELECT TOP 1 CH.numCheckHeaderID  FROM dbo.CheckHeader CH 
WHERE CH.monAmount = ABS(BST.monAmount) AND 
CH.dtCheckDate  BETWEEN DATEADD(d , -10 , BST.dtDatePosted ) AND DATEADD (d , 10 , BST.dtDatePosted )),0) AS numReferenceID, 
1 AS ReferenceTypeId
FROM dbo.bankStatementTransactions BST 
INNER JOIN dbo.BankStatementHeader BSH ON BST.numStatementId = BSH.numStatementID 
INNER JOIN dbo.BankDetails BD ON BSH.numBankDetailId = BD.numBankDetailId 
INNER JOIN dbo.BankMaster BM ON BD.numbankMasterId = BM.numBankMasterID'
SET @strSql = @strSql + ' WHERE BST.numAccountId = ' + CONVERT(VARCHAR(15), @numAccountID)  + 'AND BST.monAmount < 0 AND BST.bitIsActive = 1 AND
 NOT EXISTS (SELECT * FROM dbo.BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID)'

Create table #tempTable (numTransactionId numeric(18),
		monAmount DECIMAL(20,5) ,
		vcCheckNumber VARCHAR(50), 
		dtDatePosted DATETIME,
		vcFITransactionID VARCHAR(50),    
		vcPayeeName VARCHAR(50),    
		vcMemo VARCHAR(50),    
		vcTxType VARCHAR(50),    
		vcAccountNumber VARCHAR(50),    
		vcAccountType VARCHAR(50),    
		vcFIName VARCHAR(50),    
		CompanyName VARCHAR(50),    
		numReferenceID numeric(18),
		ReferenceTypeId numeric(18)                                           
 ) 

INSERT into #tempTable execute (@strSql )

 SET @strSql1 = 'WITH Paging
         AS (SELECT Row_number() OVER(ORDER BY dtDatePosted) AS row,* from #tempTable) select * from Paging where  row >'+ convert(varchar(15),@firstRec) +' and row < '+ convert(varchar(15),@lastRec) 

   PRINT @strSql1
 exec (@strSql1)

 (SELECT @TotRecs =COUNT(*) from #tempTable)
        
 DROP TABLE #tempTable
 
  SELECT @TotRecs
  
END