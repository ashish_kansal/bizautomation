GO
IF EXISTS(SELECT * FROM sysobjects SYSOBJ WHERE SYSOBJ.xtype='p'AND SYSOBJ.NAME ='USP_GET_DROPDOWN_DATA_FOR_IMPORT')
DROP PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_DROPDOWN_DATA_FOR_IMPORT 1,1
CREATE PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
(
	@intImportFileID    BIGINT,
	@numDomainID		BIGINT
)
AS 
BEGIN
	IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME='#tempTable')
	DROP TABLE #tempTable
	
	SELECT ROW_NUMBER() OVER (ORDER BY intImportFileID) AS [ID],* 
			INTO #tempDropDownData FROM(
					SELECT  PARENT.intImportFileID,
							vcImportFileName,
							PARENT.numDomainID,
							DYNAMIC_FIELD.numFormFieldId,
							DYNAMIC_FIELD.vcAssociatedControlType,
							DYNAMIC_FIELD.vcDbColumnName,
							DYNAMIC_FIELD.vcFormFieldName,
							FIELD_MAP.intMapColumnNo,
							DYNAMIC_FIELD.numListID,
							0 AS [bitCustomField]					
					FROM Import_File_Master PARENT
					INNER JOIN Import_File_Field_Mapping FIELD_MAP ON PARENT.intImportFileID = FIELD_MAP.intImportFileID
					INNER JOIN dbo.DynamicFormFieldMaster DYNAMIC_FIELD ON FIELD_MAP.numFormFieldID = DYNAMIC_FIELD.numFormFieldId
					WHERE vcAssociatedControlType ='SelectBox'
					  AND PARENT.intImportFileID = @intImportFileID
					  AND PARENT.numDomainID = @numDomainID   
					
					UNION ALL
					
						  SELECT    FIELD_MAP.intImportFileID AS [intImportFileID],
									cfm.Fld_label  AS [vcImportFileName],
									numDomainID AS [numDomainID],
									CFm.Fld_id AS [numFormFieldId],
									CASE WHEN CFM.Fld_type = 'Drop Down List Box'
										 THEN 'SelectBox'
										 ELSE CFM.Fld_type 
									END	 AS [vcAssociatedControlType],
									cfm.Fld_label AS [vcDbColumnName],
									cfm.Fld_label AS [vcFormFieldName],
									FIELD_MAP.intMapColumnNo  AS [intMapColumnNo],
									CFM.numlistid AS [numListID],
									1 AS [bitCustomField]
						  FROM      dbo.CFW_Fld_Master CFM
						  INNER JOIN Import_File_Field_Mapping FIELD_MAP ON CFM.Fld_id = FIELD_MAP.numFormFieldID		
						  WHERE FIELD_MAP.intImportFileID = @intImportFileID     
						    AND numDomainID = @numDomainID) TABLE1 WHERE  vcAssociatedControlType = 'SelectBox' 
                    
	SELECT * FROM #tempDropDownData   
	
	DECLARE @intRow AS INT
	SELECT @intRow = COUNT(*) FROM #tempDropDownData
	PRINT @intRow
	
	DECLARE @intCnt AS INT
	DECLARE @numListIDValue AS BIGINT
	DECLARE @numDomainIDValue AS BIGINT
	DECLARE @numFormFieldIDValue AS BIGINT
	DECLARE @bitCustomField AS BIT
		
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @vcTableNames AS VARCHAR(MAX) 
	DECLARE @strDBColumnName AS NVARCHAR(1000)
	
	SET @intCnt = 0
	SET @strSQL = '' 
	SET @vcTableNames = ''

	WHILE(@intCnt < @intRow   )
		BEGIN
			SET @intCnt = @intCnt + 1
					
			SELECT  @numDomainIDValue = numDomainID, 
					@numListIDValue = numListID,
					@strDBColumnName = vcDbColumnName, 
					@numFormFieldIDValue = numFormFieldID,
					@bitCustomField = bitCustomField 	 
			FROM #tempDropDownData 
			WHERE ID = @intCnt
			  AND intImportFileID = @intImportFileID
								
			--SELECT @numListIDValue = numListID FROM #tempDropDownData WHERE ID = @intCnt
			--SELECT @strDBColumnName = vcDbColumnName FROM #tempDropDownData WHERE ID = @intCnt
			--SELECT @numFormFieldIDValue = numFormFieldID FROM #tempDropDownData WHERE ID  = @intCnt
			PRINT 'BIT'
			PRINT @bitCustomField
			SELECT @strSQL = CASE  
								WHEN @strDBColumnName = 'numItemGroup'
								THEN @strSQL + ' SELECT ITEM_GROUP.numItemGroupID AS [Key],ITEM_GROUP.vcItemGroup AS [Value] FROM dbo.ItemGroups ITEM_GROUP 
											    WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainIDValue)
											    
								WHEN @strDBColumnName = 'numWareHouseItemID'															  
								THEN @strSQL + ' SELECT WAREHOUSE.numWareHouseID AS [Key],WAREHOUSE.vcWareHouse AS [Value] FROM dbo.Warehouses WAREHOUSE 
												WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainIDValue) 
												
								WHEN @strDBColumnName = 'numChildItemID'				
								THEN @strSQL + ' SELECT ITEM.numItemCode AS [Key],ITEM.vcItemName AS [Value] FROM dbo.Item ITEM 
												WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainIDValue)
												
								WHEN @strDBColumnName = 'charItemType'
								THEN @strSQL + ' SELECT ''A'' AS [Key], ''asset'' AS [Value]
											    UNION 				
											    SELECT ''P'' AS [Key], ''inventory item'' AS [Value]
											    UNION 				
											    SELECT ''S'' AS [Key], ''service'' AS [Value]
											    UNION 				
											    SELECT ''N'' AS [Key], ''non-inventory item'' AS [Value]'																			    
								ELSE 
									 @strSQL + '  SELECT LIST.numListItemID AS [Key],vcData AS [Value] FROM ListDetails LIST          
												  LEFT JOIN listorder LIST_ORDER ON LIST.numListItemID = LIST_ORDER.numListItemID  
																				AND LIST_ORDER.numDomainId = ' + CONVERT(VARCHAR,@numDomainIDValue) + '
												  WHERE ( LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainIDValue) + ' OR LIST.constFlag = 1) 
												  AND LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainIDValue) + ' 
												  AND LIST.numListID = ' + CONVERT(VARCHAR,@numListIDValue) + ' ORDER BY intSortOrder '
						     END						
						     
			SET @vcTableNames = @vcTableNames + CONVERT(VARCHAR,@numFormFieldIDValue) + '_' + @strDBColumnName + ',' 
		END
	
	PRINT @vcTableNames
	PRINT @strSQL	
	
	SELECT @vcTableNames AS TableNames
	EXEC SP_EXECUTESQL @strSQL		
	
	--SELECT ITEM_GROUP.numItemGroupID,ITEM_GROUP.vcItemGroup FROM dbo.ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainIDValue)
	--SELECT WAREHOUSE.numWareHouseID,WAREHOUSE.vcWareHouse FROM dbo.Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainIDValue)
	--SELECT ITEM.numItemCode,ITEM.vcItemName FROM dbo.Item ITEM WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainIDValue)
END
