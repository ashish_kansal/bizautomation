-- EXEC USP_ManageHelpCategories 0,'asda',1,3,0,0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageHelpCategories')
DROP PROCEDURE USP_ManageHelpCategories
GO
CREATE PROCEDURE [dbo].[USP_ManageHelpCategories]
	@numHelpCategoryID NUMERIC = 0 output,
	@vcCatecoryName varchar(100),
	@bitLinkPage bit,
	@numPageID numeric,
	@numParentCatID numeric,
	@tintLevel tinyint
AS
IF @numHelpCategoryID = 0 BEGIN
	INSERT INTO HelpCategories (
		[vcCatecoryName],
		[bitLinkPage],
		[numPageID],
		[numParentCatID],
		[tintLevel]
	)
	VALUES (
		@vcCatecoryName,
		@bitLinkPage,
		@numPageID,
		@numParentCatID,
		@tintLevel
	)
	SELECT @numHelpCategoryID=SCOPE_IDENTITY();
END
ELSE BEGIN
	UPDATE HelpCategories SET 
		[vcCatecoryName] = @vcCatecoryName,
		[bitLinkPage] = @bitLinkPage,
		[numPageID] = @numPageID,
		[numParentCatID] = @numParentCatID,
		[tintLevel] = @tintLevel
	WHERE [numHelpCategoryID] = @numHelpCategoryID

END