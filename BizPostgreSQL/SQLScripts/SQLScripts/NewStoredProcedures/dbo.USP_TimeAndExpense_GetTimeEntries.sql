GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetTimeEntries')
DROP PROCEDURE USP_TimeAndExpense_GetTimeEntries
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_GetTimeEntries]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@vcEmployeeName VARCHAR(200)
	,@vcTeams VARCHAR(MAX)
	,@tintPayrollType TINYINT
	,@ClientTimeZoneOffset INT
	,@vcSortColumn VARCHAR(200)
	,@vcSortOrder VARCHAR(4)
	,@tintUserRightType TINYINT
)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		numUserCntID NUMERIC(18,0)
		,vcEmployee VARCHAR(200)
		,vcTeam VARCHAR(200)
		,numTotalHours VARCHAR(20)
		,vcPayrollType VARCHAR(50)
	)

	INSERT INTO @TEMP
	(
		numUserCntID
		,vcEmployee
		,vcTeam
		,numTotalHours
		,vcPayrollType
	)
	SELECT
		UserMaster.numUserDetailId
		,CONCAT(ISNULL(AdditionalContactsInformation.vcFirstName,'-'),' ',ISNULL(AdditionalContactsInformation.vcLastName,'-'))
		,dbo.GetListIemName(AdditionalContactsInformation.numTeam)
		,TEMPTime.vcHours
		,(CASE ISNULL(tintPayrollType,0) WHEN 2 THEN 'Salary' WHEN 1 THEN 'Hourly' ELSE '' END) 
	FROM
		UserMaster
	INNER JOIN
		AdditionalContactsInformation
	ON
		UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
	OUTER APPLY
	(
		SELECT 
			CONCAT(FORMAT(ISNULL(SUM(numMinutes),0) / 60,'00'),':',FORMAT(ISNULL(SUM(numMinutes),0) % 60,'00')) vcHours
		FROM
			dbo.fn_GetPayrollEmployeeTime(@numDomainID,UserMaster.numUserDetailId,@dtFromDate,@dtToDate,@ClientTimeZoneOffset)
		WHERE
			numType NOT IN (4)
	) TEMPTime
	WHERE
		UserMaster.numDomainID=@numDomainID
		AND ISNULL(UserMaster.bitActivateFlag,0) = 1
		AND (ISNULL(@vcEmployeeName,'') = '' OR AdditionalContactsInformation.vcFirstName LIKE CONCAT('%',@vcEmployeeName,'%') OR AdditionalContactsInformation.vcLastName LIKE CONCAT('%',@vcEmployeeName,'%'))
		AND (ISNULL(@vcTeams,'') = '' OR AdditionalContactsInformation.numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,',')))
		AND 1 = (CASE WHEN @tintPayrollType=0 THEN 1 ELSE (CASE WHEN ISNULL(UserMaster.tintPayrollType,0)=@tintPayrollType THEN 1 ELSE 0 END) END)
		AND 1 = (CASE ISNULL(@tintUserRightType,0) 
					WHEN 3 THEN 1 
					WHEN 1 THEN (CASE WHEN UserMaster.numUserDetailId=@numUserCntID THEN 1 ELSE 0 END)
					ELSE 0 
				END) 

	IF ISNULL(@vcSortColumn,'') = ''
	BEGIN
		SET @vcSortColumn = 'vcEmployee'
		SET @vcSortOrder = 'ASC'
	END

	IF ISNULL(@vcSortColumn,'') = ''
	BEGIN
		SET @vcSortOrder = 'ASC'
	END


	SELECT 
		* 
	FROM 
		@TEMP 
	ORDER BY
		CASE WHEN @vcSortColumn = 'vcEmployee' AND @vcSortOrder = 'ASC' then vcEmployee END ASC,
		CASE WHEN @vcSortColumn = 'vcEmployee' AND @vcSortOrder = 'DESC' then vcEmployee END DESC,
		CASE WHEN @vcSortColumn = 'vcTeam' AND @vcSortOrder = 'ASC' then vcTeam END ASC,
		CASE WHEN @vcSortColumn = 'vcTeam' AND @vcSortOrder = 'DESC' then vcTeam END DESC,
		CASE WHEN @vcSortColumn = 'numTotalHours' AND @vcSortOrder = 'ASC' then numTotalHours END ASC,
		CASE WHEN @vcSortColumn = 'numTotalHours' AND @vcSortOrder = 'DESC' then numTotalHours END DESC,
		CASE WHEN @vcSortColumn = 'vcPayrollType' AND @vcSortOrder = 'ASC' then vcPayrollType END ASC,
		CASE WHEN @vcSortColumn = 'vcPayrollType' AND @vcSortOrder = 'DESC' then vcPayrollType END DESC
END
GO