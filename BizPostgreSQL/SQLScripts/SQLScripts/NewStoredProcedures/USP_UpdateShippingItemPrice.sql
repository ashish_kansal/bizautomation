GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateShippingItemPrice')
DROP PROCEDURE USP_UpdateShippingItemPrice
GO
CREATE PROCEDURE USP_UpdateShippingItemPrice
    @numOppId NUMERIC(9)=0,
    @numItemCode NUMERIC(9)=0,
	@numItemPrice NUMERIC(18,2)=0
AS 
    BEGIN
       UPDATE 
			OpportunityItems 
	   SET
			monPrice=@numItemPrice,
			monTotAmount=@numItemPrice,
			monTotAmtBefDiscount=@numItemPrice 
		WHERE 
			numOppId=@numOppId and numItemCode=@numItemCode
    END
