
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOpportunityModifiedBizDocs' ) 
    DROP PROCEDURE USP_GetOpportunityModifiedBizDocs
GO
-- =============================================  
-- Modified by: <Author,,Neelam Kapila>  
-- Create date: <Create Date,,09/29/2017>  
-- Description: <Description,,To fetch modified BizDocs >  
-- =============================================  

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GetOpportunityModifiedBizDocs]    
	@numBizDocTempID AS NUMERIC(18,0)        
AS 
BEGIN
	SELECT Top 1 numOppId, numOppBizDocsId FROM OpportunityBizDocs 
	WHERE numBizDocTempID = @numBizDocTempID
	ORDER BY numOppBizDocsId DESC
END
GO