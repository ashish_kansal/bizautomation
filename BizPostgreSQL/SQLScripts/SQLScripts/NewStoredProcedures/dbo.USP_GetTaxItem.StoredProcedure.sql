
/****** Object:  StoredProcedure [dbo].[USP_GetTaxItem]    Script Date: 09/25/2009 16:24:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaxItem')
DROP PROCEDURE USP_GetTaxItem
GO
CREATE PROCEDURE [dbo].[USP_GetTaxItem]
@numDomainID as numeric(9)
as


select vcTaxName,numTaxItemID,numChartOfAcntID,C.vcAccountName from TaxItems TI LEFT OUTER JOIN [Chart_Of_Accounts] C
ON C.[numAccountId]=TI.numChartOfAcntID
Where TI.numDomainID=@numDomainID
