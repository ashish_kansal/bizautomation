SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecordHistory_FetchFromCDCOppOrder')
DROP PROCEDURE dbo.USP_RecordHistory_FetchFromCDCOppOrder
GO
CREATE PROCEDURE [dbo].[USP_RecordHistory_FetchFromCDCOppOrder]

AS
BEGIN
	
	DECLARE @TEMPOpportunityHistory TABLE
	(
		vbStartlsn VARBINARY(128),
		numRecordID NUMERIC(18,0),
		numDomainID NUMERIC(18,0),
		dtDate DATETIME,
		numUserCntID NUMERIC(18,0),
		vcEvent VARCHAR(200),
		vcFieldName VARCHAR(200),
		vcOldValue VARCHAR(MAX),
		vcNewValue VARCHAR(MAX),
		vcDescription VARCHAR(MAX),
		vcHiddenDescription VARCHAR(MAX),
		numFieldID NUMERIC(18,0),
		bitCustomField BIT
	)
	
	DECLARE @TempTable TABLE
	(
		ID NUMERIC(18,0),
		vbStartlsn VARBINARY(128)
	)

	INSERT INTO 
		@TempTable
	SELECT
		ROW_NUMBER() OVER(ORDER BY __$start_lsn),
		__$start_lsn
	FROM
		[cdc].[dbo_OpportunityMaster_CT]
	WHERE
		__$operation IN (2,3)
	GROUP BY
		__$start_lsn

	DECLARE @i AS NUMERIC(18,0) = 1
	DECLARE @iCOUNT AS NUMERIC(18,0)
	DECLARE @vbStartlsn AS VARBINARY(128)
	DECLARE @vcUpdatemask AS VARBINARY(128)
	DECLARE @tintOperation AS TINYINT
	DECLARE @sqlVal AS VARBINARY(128)
	DECLARE @numOppID AS NUMERIC(18,0)
	DECLARE @numOppBizDocsID AS NUMERIC(18,0)
	DECLARE @vcBizDocID AS VARCHAR(300)
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @bintModifiedDate DATETIME
	DECLARE @numModifiedBy AS NUMERIC(18,0)

	SELECT @iCOUNT = COUNT(*) FROM @TempTable

	WHILE @i <= @iCOUNT
	BEGIN
		SELECT @vbStartlsn = vbStartlsn FROM @TempTable WHERE ID=@i

		SELECT TOP 1
			@tintOperation = __$operation,
			@numOppID=numOppID,
			@numDomainID=numDomainID,
			@bintModifiedDate= (SELECT MAX(bintModifiedDate) FROM [cdc].[dbo_OpportunityMaster_CT] WHERE __$start_lsn = @vbStartlsn),
			@numModifiedBy=numModifiedBy
		FROM 
			[cdc].[dbo_OpportunityMaster_CT]
		WHERE 
			__$start_lsn = @vbStartlsn

		IF @tintOperation = 2 --INSERT
		BEGIN
			INSERT INTO @TEMPOpportunityHistory
			(
				vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				numFieldID,
				bitCustomField
			)
			VALUES
			(
				@vbStartlsn,
				@numOppID,
				@numDomainID,
				@bintModifiedDate,
				@numModifiedBy,
				'New',
				NULL,
				0
			)
		END
		ELSE IF @tintOperation = 3 --UPDATE - (COLUMN VALUES BEFORE UPDATE)
		BEGIN
			
			DECLARE @TMEPChangedColumns TABLE 
			(
				ID INT,
				dtDate DATETIME,
				vcColumnName VARCHAR(200),
				vcOldValue VARCHAR(MAX),
				vcNewValue VARCHAR(MAX)
			)

			DELETE FROM @TMEPChangedColumns

			INSERT INTO @TMEPChangedColumns
			(
				ID,
				dtDate,
				vcColumnName,
				vcOldValue,
				vcNewValue
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY TempUpdateBefore.__$start_lsn),
				DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), sys.fn_cdc_map_lsn_to_time(TempUpdateBefore.__$start_lsn)), 
				TempUpdateBefore.column_name, 
				TempUpdateBefore.old_value, 
				TempUpdateAfter.new_value
			FROM
				( 
					SELECT 
						__$start_lsn, 
						column_name, 
						old_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numAssignedTo'),__$update_mask) = 1) THEN CAST(numAssignedTo AS VARCHAR(MAX)) ELSE NULL END AS numAssignedTo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','bintModifiedDate'),__$update_mask) = 1) THEN CAST(bintModifiedDate AS VARCHAR(MAX)) ELSE NULL END AS bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS VARCHAR(MAX)) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numCampainID'),__$update_mask) = 1) THEN CAST(numCampainID as VARCHAR(MAX)) ELSE NULL END AS numCampainID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numStatus'),__$update_mask) = 1) THEN CAST(numStatus as VARCHAR(MAX)) ELSE NULL END AS numStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','txtComments'),__$update_mask) = 1) THEN CAST(txtComments as VARCHAR(MAX)) ELSE NULL END AS txtComments,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','vcPOppName'),__$update_mask) = 1) THEN CAST(vcPOppName AS VARCHAR(MAX)) ELSE NULL END AS vcPOppName,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','intPEstimatedCloseDate'),__$update_mask) = 1) THEN CAST(intPEstimatedCloseDate AS VARCHAR(MAX)) ELSE NULL END AS intPEstimatedCloseDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintSource'),__$update_mask) = 1) THEN CAST(tintSource AS VARCHAR(MAX)) ELSE NULL END AS tintSource,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numSalesOrPurType'),__$update_mask) = 1) THEN CAST(numSalesOrPurType AS VARCHAR(MAX)) ELSE NULL END AS numSalesOrPurType,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','lngPConclAnalysis'),__$update_mask) = 1) THEN CAST(lngPConclAnalysis AS VARCHAR(MAX)) ELSE NULL END AS lngPConclAnalysis,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintActive'),__$update_mask) = 1) THEN CAST(tintActive AS VARCHAR(MAX)) ELSE NULL END AS tintActive,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','vcOppRefOrderNo'),__$update_mask) = 1) THEN CAST(vcOppRefOrderNo AS VARCHAR(MAX)) ELSE NULL END AS vcOppRefOrderNo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numBusinessProcessID'),__$update_mask) = 1) THEN CAST(numBusinessProcessID AS VARCHAR(MAX)) ELSE NULL END AS numBusinessProcessID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numRecOwner'),__$update_mask) = 1) THEN CAST(numRecOwner AS VARCHAR(MAX)) ELSE NULL END AS numRecOwner,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintOppStatus'),__$update_mask) = 1) THEN CAST(tintOppStatus AS VARCHAR(MAX)) ELSE NULL END AS tintOppStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','bintClosedDate'),__$update_mask) = 1) THEN CAST(bintClosedDate AS VARCHAR(MAX)) ELSE NULL END AS bintClosedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintshipped'),__$update_mask) = 1) THEN CAST(tintshipped AS VARCHAR(MAX)) ELSE NULL END AS tintshipped,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numPercentageComplete'),__$update_mask) = 1) THEN CAST(numPercentageComplete AS VARCHAR(MAX)) ELSE NULL END AS numPercentageComplete
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_OpportunityMaster(@vbStartlsn,@vbStartlsn, N'all update old')
						WHERE 
							__$operation = 3
					) as t1
					UNPIVOT 
					(
						old_value FOR column_name IN (numAssignedTo,bintModifiedDate,numModifiedBy,numCampainID,numStatus,txtComments,vcPOppName,intPEstimatedCloseDate,tintSource,numSalesOrPurType,lngPConclAnalysis,tintActive,vcOppRefOrderNo,numBusinessProcessID,numRecOwner,tintOppStatus,bintClosedDate,tintshipped,numPercentageComplete) 
					) as unp
				) as TempUpdateBefore
				INNER JOIN
				(
					SELECT 
						__$start_lsn,
						column_name, 
						new_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numAssignedTo'),__$update_mask) = 1) THEN CAST(numAssignedTo AS VARCHAR(MAX)) ELSE NULL END AS numAssignedTo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','bintModifiedDate'),__$update_mask) = 1) THEN CAST(bintModifiedDate AS VARCHAR(MAX)) ELSE NULL END AS bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS VARCHAR(MAX)) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numCampainID'),__$update_mask) = 1) THEN CAST(numCampainID as VARCHAR(MAX)) ELSE NULL END AS numCampainID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numStatus'),__$update_mask) = 1) THEN CAST(numStatus as VARCHAR(MAX)) ELSE NULL END AS numStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','txtComments'),__$update_mask) = 1) THEN CAST(txtComments as VARCHAR(MAX)) ELSE NULL END AS txtComments,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','vcPOppName'),__$update_mask) = 1) THEN CAST(vcPOppName AS VARCHAR(MAX)) ELSE NULL END AS vcPOppName,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','intPEstimatedCloseDate'),__$update_mask) = 1) THEN CAST(intPEstimatedCloseDate AS VARCHAR(MAX)) ELSE NULL END AS intPEstimatedCloseDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintSource'),__$update_mask) = 1) THEN CAST(tintSource AS VARCHAR(MAX)) ELSE NULL END AS tintSource,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numSalesOrPurType'),__$update_mask) = 1) THEN CAST(numSalesOrPurType AS VARCHAR(MAX)) ELSE NULL END AS numSalesOrPurType,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','lngPConclAnalysis'),__$update_mask) = 1) THEN CAST(lngPConclAnalysis AS VARCHAR(MAX)) ELSE NULL END AS lngPConclAnalysis,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintActive'),__$update_mask) = 1) THEN CAST(tintActive AS VARCHAR(MAX)) ELSE NULL END AS tintActive,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','vcOppRefOrderNo'),__$update_mask) = 1) THEN CAST(vcOppRefOrderNo AS VARCHAR(MAX)) ELSE NULL END AS vcOppRefOrderNo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numBusinessProcessID'),__$update_mask) = 1) THEN CAST(numBusinessProcessID AS VARCHAR(MAX)) ELSE NULL END AS numBusinessProcessID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numRecOwner'),__$update_mask) = 1) THEN CAST(numRecOwner AS VARCHAR(MAX)) ELSE NULL END AS numRecOwner,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintOppStatus'),__$update_mask) = 1) THEN CAST(tintOppStatus AS VARCHAR(MAX)) ELSE NULL END AS tintOppStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','bintClosedDate'),__$update_mask) = 1) THEN CAST(bintClosedDate AS VARCHAR(MAX)) ELSE NULL END AS bintClosedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','tintshipped'),__$update_mask) = 1) THEN CAST(tintshipped AS VARCHAR(MAX)) ELSE NULL END AS tintshipped,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityMaster','numPercentageComplete'),__$update_mask) = 1) THEN CAST(numPercentageComplete AS VARCHAR(MAX)) ELSE NULL END AS numPercentageComplete
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_OpportunityMaster(@vbStartlsn, @vbStartlsn, N'all') -- 'all update old' is not necessary here
						WHERE 
							__$operation = 4
					) as t2
					UNPIVOT 
					(
						new_value FOR column_name IN (numAssignedTo,bintModifiedDate,numModifiedBy,numCampainID,numStatus,txtComments,vcPOppName,intPEstimatedCloseDate,tintSource,numSalesOrPurType,lngPConclAnalysis,tintActive,vcOppRefOrderNo,numBusinessProcessID,numRecOwner,tintOppStatus,bintClosedDate,tintshipped,numPercentageComplete) 
					) as unp 
				) as TempUpdateAfter -- after update
				ON 
					TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn 
					AND TempUpdateBefore.column_name = TempUpdateAfter.column_name

			DECLARE @j AS INT = 1
			DECLARE @jCOUNT AS INT
			DECLARE @dtDate AS DATETIME
			DECLARE @vcColumnName AS VARCHAR(200)
			DECLARE @vcOldValue AS VARCHAR(MAX)
			DECLARE @vcNewValue AS VARCHAR(MAX)

			SELECT @jCOUNT = COUNT(*) FROM @TMEPChangedColumns

			WHILE @j <= @jCOUNT
			BEGIN
				SELECT 
					@dtDate = dtDate,
					@vcColumnName = vcColumnName,
					@vcOldValue = vcOldValue,
					@vcNewValue = vcNewValue
				FROM 
					@TMEPChangedColumns
				WHERE
					ID = @j

				DECLARE @numNewModifiedBy As NUMERIC(18,0) = -1

				IF @vcColumnName = 'numModifiedBy'
				BEGIN
					-- KEEP VALUES IN TEMPORARY VARIABLE AND VALUES FOR ALL FIELDS UPDATE AFTER LOOP COMPLETES.
					SET @numNewModifiedBy = @vcNewValue
				END
				ELSE IF @vcColumnName = 'bintModifiedDate'
				BEGIN
					BEGIN TRY
						/* Opportunity Updated Items */
						INSERT INTO @TEMPOpportunityHistory
						(
							vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcFieldName,
							vcOldValue,
							vcNewValue,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField
						)
						SELECT 
							@vbStartlsn,
							@numOppID,
							@numDomainID,
							@bintModifiedDate,
							@numModifiedBy,
							'Changed Item',
							(CASE TempUpdateBefore.column_name
								WHEN 'numUnitHour' THEN 'Unit Hour'
								WHEN 'monPrice' THEN 'Price'
								WHEN 'vcItemName' THEN 'Item Name'
								WHEN 'vcItemDesc' THEN 'Item Description'
								WHEN 'numWarehouseItmsID' THEN 'Warehouse'
								WHEN 'bitDropship' THEN 'Drop Ship'
								WHEN 'numUnitHourReceived' THEN 'Received Qty'
								WHEN 'bitDiscountType' THEN 'Discount Type'
								WHEN 'fltDiscount' THEN 'Discount'
								WHEN 'numQtyShipped' THEN 'Shipped Qty'
								WHEN 'numUOMId' THEN 'UOM'
								WHEN 'numToWarehouseItemID' THEN 'Stock Transfer To Warehouse'
								ELSE TempUpdateBefore.column_name
							END), 
							(CASE 
								WHEN TempUpdateBefore.column_name = 'numWarehouseItmsID' OR TempUpdateBefore.column_name = 'numToWarehouseItemID' THEN 
									(SELECT 
										CONCAT(W.vcWareHouse, CASE WHEN WL.vcLocation IS NOT NULL THEN CONCAT(' (',WL.vcLocation,')') ELSE '' END)
									FROM
										WareHouseItems WI
									INNER JOIN
										Warehouses W
									ON
										WI.numWareHouseID = W.numWareHouseID
									LEFT JOIN
										WarehouseLocation WL
									ON
										WI.numWLocationID = WL.numWLocationID
									WHERE 
										numWarehouseItemID = TempUpdateBefore.old_value)
								WHEN TempUpdateBefore.column_name = 'bitDiscountType' THEN
									(CASE WHEN TempUpdateBefore.old_value = 1 THEN 'Flat Amount' ELSE 'Percentage' END)
								WHEN TempUpdateBefore.column_name = 'numUOMId' THEN
									(SELECT vcUnitName FROM UOM WHERE numDomainId=@numDomainID AND numUOMId=TempUpdateBefore.old_value)
								ELSE
									TempUpdateBefore.old_value
							END), 
							(CASE 
								WHEN TempUpdateBefore.column_name = 'numWarehouseItmsID' OR TempUpdateBefore.column_name = 'numToWarehouseItemID' THEN 
									(SELECT 
										CONCAT(W.vcWareHouse, CASE WHEN WL.vcLocation IS NOT NULL THEN CONCAT(' (',WL.vcLocation,')') ELSE '' END)
									FROM
										WareHouseItems WI
									INNER JOIN
										Warehouses W
									ON
										WI.numWareHouseID = W.numWareHouseID
									LEFT JOIN
										WarehouseLocation WL
									ON
										WI.numWLocationID = WL.numWLocationID
									WHERE 
										numWarehouseItemID = TempUpdateAfter.new_value)
								WHEN TempUpdateBefore.column_name = 'bitDiscountType' THEN
									(CASE WHEN TempUpdateAfter.new_value = 1 THEN 'Flat Amount' ELSE 'Percentage' END)
								WHEN TempUpdateBefore.column_name = 'numUOMId' THEN
									(SELECT vcUnitName FROM UOM WHERE numDomainId=@numDomainID AND numUOMId=TempUpdateAfter.new_value)
								ELSE
									TempUpdateAfter.new_value
							END), 
							CONCAT('Item Code:',TempUpdateAfter.ItemCode,', Item Name:', TempUpdateAfter.ItemName),
							CONCAT('OppItemID:',TempUpdateAfter.OppItemID,', numWarehouseItemID:', TempUpdateAfter.WarehouseItemID),
							NULL,
							0
						FROM
							( 
								SELECT 
									__$start_lsn, 
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name, 
									old_value
								FROM 
								(
									SELECT 
										__$start_lsn,
										numOppItemtCode As OppItemID,
										numItemCode As ItemCode,
										vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numItemCode'),__$update_mask) = 1) THEN CAST(numItemCode AS VARCHAR(MAX)) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS VARCHAR(MAX)) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','monPrice'),__$update_mask) = 1) THEN CAST(monPrice AS VARCHAR(MAX)) ELSE NULL END AS monPrice,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','vcItemName'),__$update_mask) = 1) THEN CAST(vcItemName as VARCHAR(MAX)) ELSE NULL END AS vcItemName,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','vcItemDesc'),__$update_mask) = 1) THEN CAST(vcItemDesc as VARCHAR(MAX)) ELSE NULL END AS vcItemDesc,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numWarehouseItmsID'),__$update_mask) = 1) THEN CAST(numWarehouseItmsID as VARCHAR(MAX)) ELSE NULL END AS numWarehouseItmsID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','bitDropship'),__$update_mask) = 1) THEN CAST(bitDropship AS VARCHAR(MAX)) ELSE NULL END AS bitDropship,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUnitHourReceived'),__$update_mask) = 1) THEN CAST(numUnitHourReceived AS VARCHAR(MAX)) ELSE NULL END AS numUnitHourReceived,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','bitDiscountType'),__$update_mask) = 1) THEN CAST(bitDiscountType AS VARCHAR(MAX)) ELSE NULL END AS bitDiscountType,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','fltDiscount'),__$update_mask) = 1) THEN CAST(fltDiscount AS VARCHAR(MAX)) ELSE NULL END AS fltDiscount,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numQtyShipped'),__$update_mask) = 1) THEN CAST(numQtyShipped AS VARCHAR(MAX)) ELSE NULL END AS numQtyShipped,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUOMId'),__$update_mask) = 1) THEN CAST(numUOMId AS VARCHAR(MAX)) ELSE NULL END AS numUOMId,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numToWarehouseItemID'),__$update_mask) = 1) THEN CAST(numToWarehouseItemID AS VARCHAR(MAX)) ELSE NULL END AS numToWarehouseItemID
									FROM 
										cdc.fn_cdc_get_all_changes_dbo_OpportunityItems(@vbStartlsn,@vbStartlsn, N'all update old')
									WHERE 
										__$operation = 3
								) as t1
								UNPIVOT 
								(
									old_value FOR column_name IN (numItemCode,numUnitHour,monPrice,vcItemName,vcItemDesc,numWarehouseItmsID,bitDropship,numUnitHourReceived,bitDiscountType,fltDiscount,numToWarehouseItemID) 
								) as unp
							) as TempUpdateBefore
							INNER JOIN
							(
								SELECT 
									__$start_lsn,
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name, 
									new_value
								FROM 
								(
									SELECT 
										__$start_lsn,
										numOppItemtCode As OppItemID,
										numItemCode As ItemCode,
										vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numItemCode'),__$update_mask) = 1) THEN CAST(numItemCode AS VARCHAR(MAX)) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS VARCHAR(MAX)) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','monPrice'),__$update_mask) = 1) THEN CAST(monPrice AS VARCHAR(MAX)) ELSE NULL END AS monPrice,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','vcItemName'),__$update_mask) = 1) THEN CAST(vcItemName as VARCHAR(MAX)) ELSE NULL END AS vcItemName,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','vcItemDesc'),__$update_mask) = 1) THEN CAST(vcItemDesc as VARCHAR(MAX)) ELSE NULL END AS vcItemDesc,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numWarehouseItmsID'),__$update_mask) = 1) THEN CAST(numWarehouseItmsID as VARCHAR(MAX)) ELSE NULL END AS numWarehouseItmsID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','bitDropship'),__$update_mask) = 1) THEN CAST(bitDropship AS VARCHAR(MAX)) ELSE NULL END AS bitDropship,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUnitHourReceived'),__$update_mask) = 1) THEN CAST(numUnitHourReceived AS VARCHAR(MAX)) ELSE NULL END AS numUnitHourReceived,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','bitDiscountType'),__$update_mask) = 1) THEN CAST(bitDiscountType AS VARCHAR(MAX)) ELSE NULL END AS bitDiscountType,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','fltDiscount'),__$update_mask) = 1) THEN CAST(fltDiscount AS VARCHAR(MAX)) ELSE NULL END AS fltDiscount,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numQtyShipped'),__$update_mask) = 1) THEN CAST(numQtyShipped AS VARCHAR(MAX)) ELSE NULL END AS numQtyShipped,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numUOMId'),__$update_mask) = 1) THEN CAST(numUOMId AS VARCHAR(MAX)) ELSE NULL END AS numUOMId,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityItems','numToWarehouseItemID'),__$update_mask) = 1) THEN CAST(numToWarehouseItemID AS VARCHAR(MAX)) ELSE NULL END AS numToWarehouseItemID
									FROM 
										cdc.fn_cdc_get_all_changes_dbo_OpportunityItems(@vbStartlsn,@vbStartlsn, N'all') -- 'all update old' is not necessary here
									WHERE 
										__$operation = 4
								) as t2
								UNPIVOT 
								(
									new_value FOR column_name IN (numItemCode,numUnitHour,monPrice,vcItemName,vcItemDesc,numWarehouseItmsID,bitDropship,numUnitHourReceived,bitDiscountType,fltDiscount,numToWarehouseItemID) 
								) as unp 
							) as TempUpdateAfter -- after update
							ON 
								TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn 
								AND TempUpdateBefore.column_name = TempUpdateAfter.column_name
								AND TempUpdateBefore.OppItemID = TempUpdateAfter.OppItemID
								AND ISNULL(TempUpdateBefore.WarehouseItemID,0) = ISNULL(TempUpdateAfter.WarehouseItemID,0)
					END TRY
					BEGIN CATCH
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
					END CATCH

					BEGIN TRY
						/* Opportunity Deleted Items */
						INSERT INTO @TEMPOpportunityHistory
						(
							vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField
						)
						SELECT 
							@vbStartlsn,
							@numOppID,
							@numDomainID,
							@bintModifiedDate,
							@numModifiedBy,
							'Removed Item',
							CONCAT('Item Code:',TempDelete.ItemCode,', Item Name:', TempDelete.ItemName),
							CONCAT('OppItemID:',TempDelete.OppItemID,', numWarehouseItemID:', TempDelete.WarehouseItemID),
							NULL,
							0
						FROM
							( 
								SELECT 
									__$start_lsn,
									numOppItemtCode As OppItemID,
									numItemCode As ItemCode,
									vcItemName As ItemName,
									numWarehouseItmsID As WarehouseItemID
								FROM 
									[cdc].[dbo_OpportunityItems_CT]
								WHERE 
									__$operation = 1
									AND __$start_lsn = @vbStartlsn
							) AS TempDelete
					END TRY
					BEGIN CATCH
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
					END CATCH
				END
				ELSE IF @vcColumnName = 'vcPOppName'
				BEGIN
					-- IF UPDATE FIELD ID vcPOppName
					-- IF OPPID IS EXISTS IN TEMP TABLE AND COMMIT DATE TIME IS SAME AS FOUND IN TEMP TABLE
					-- THEN IGNORE CHANGE BECUASE IT IS MODIFIED AFTER OPP/ORDER INSERT AND NOT SEPERATE TRANSACTION
					IF NOT EXISTS(SELECT * FROM @TEMPOpportunityHistory WHERE vbStartlsn = @vbStartlsn AND dtDate = @dtDate)
					BEGIN
						INSERT INTO @TEMPOpportunityHistory
						(
							vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcFieldName,
							vcOldValue,
							vcNewValue,
							numFieldID,
							bitCustomField
						)
						VALUES
						(
							@vbStartlsn,
							@numOppID,
							@numDomainID,
							@dtDate,
							@numModifiedBy,
							'Change',
							'Opp/Order Name',
							@vcOldValue,
							@vcNewValue,
							NULL,
							0
						)
					END
				END
				ELSE
				BEGIN
					INSERT INTO @TEMPOpportunityHistory
					(
						vbStartlsn,
						numRecordID,
						numDomainID,
						dtDate,
						numUserCntID,
						vcEvent,
						vcFieldName,
						vcOldValue,
						vcNewValue,
						numFieldID,
						bitCustomField
					)
					VALUES
					(
						@vbStartlsn,
						@numOppID,
						@numDomainID,
						@dtDate,
						@numModifiedBy,
						'Change',
						(CASE 
							WHEN @vcColumnName = 'numAssignedTo' THEN 'Assigned To'
							WHEN @vcColumnName = 'numCampainID' THEN 'Campaign'
							WHEN @vcColumnName = 'numStatus' THEN 'Status'
							WHEN @vcColumnName = 'txtComments' THEN 'Comments'
							WHEN @vcColumnName = 'intPEstimatedCloseDate' THEN 'Estimated Close Date'
							WHEN @vcColumnName = 'tintSource' THEN 'Source'
							WHEN @vcColumnName = 'numSalesOrPurType' THEN 'Sales/Purchase Type'
							WHEN @vcColumnName = 'lngPConclAnalysis' THEN 'Conclusion Reason'
							WHEN @vcColumnName = 'tintActive' THEN 'Active'
							WHEN @vcColumnName = 'vcOppRefOrderNo' THEN 'Customer PO#'
							WHEN @vcColumnName = 'numBusinessProcessID' THEN 'Business Process'
							WHEN @vcColumnName = 'numRecOwner' THEN 'Record Owner'
							WHEN @vcColumnName = 'tintOppStatus' THEN 'Deal Status'
							WHEN @vcColumnName = 'bintClosedDate' THEN 'Closed Date'
							WHEN @vcColumnName = 'tintshipped' THEN 'Order Closed'
							WHEN @vcColumnName = 'numPercentageComplete' THEN 'Percentage Complete'
						END),
						(CASE @vcColumnName
							WHEN 'numAssignedTo' THEN
								(SELECT vcUserName FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailID = @vcOldValue)
							WHEN 'numCampainID' THEN
								ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numDomainID=@numDomainID AND numCampaignID = @vcOldValue),'')
							WHEN 'numStatus' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=176 AND numListItemID=@vcOldValue),'')
							WHEN 'tintSource' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=9 AND numListItemID=@vcOldValue),'')
							WHEN 'numSalesOrPurType' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=45 AND numListItemID=@vcOldValue),'')
							WHEN 'tintActive' THEN
								(CASE WHEN @vcOldValue = 1 THEN 'Yes' ELSE 'No' END)
							WHEN 'numBusinessProcessID' THEN
								ISNULL((SELECT Slp_Name FROM Sales_process_List_Master WHERE numDomainID=@numDomainID AND Slp_Id=@vcOldValue),'')
							WHEN 'numRecOwner' THEN
								(SELECT vcUserName FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailID = @vcOldValue)
							WHEN 'tintOppStatus' THEN
								(CASE @vcOldValue WHEN 1 THEN 'Deal Won' WHEN 2 THEN 'Deal Lost' ELSE 'Open' END)
							WHEN 'tintshipped' THEN
								(CASE @vcOldValue WHEN 1 THEN 'Closed' ELSE 'Open' END)
							WHEN 'numPercentageComplete' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=50 AND numListItemID=@vcOldValue),'')
							ELSE
								@vcOldValue
						END),
						(CASE @vcColumnName
							WHEN 'numAssignedTo' THEN
								(SELECT vcUserName FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailID = @vcNewValue)
							WHEN 'numCampainID' THEN
								ISNULL((SELECT vcCampaignName FROM CampaignMaster WHERE numDomainID=@numDomainID AND numCampaignID = @vcNewValue),'')
							WHEN 'numStatus' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=176 AND numListItemID=@vcNewValue),'')
							WHEN 'tintSource' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=9 AND numListItemID=@vcNewValue),'')
							WHEN 'numSalesOrPurType' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=45 AND numListItemID=@vcNewValue),'')
							WHEN 'tintActive' THEN
								(CASE WHEN @vcNewValue = 1 THEN 'Yes' ELSE 'No' END)
							WHEN 'numBusinessProcessID' THEN
								ISNULL((SELECT Slp_Name FROM Sales_process_List_Master WHERE numDomainID=@numDomainID AND Slp_Id=@vcNewValue),'')
							WHEN 'numRecOwner' THEN
								(SELECT vcUserName FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailID = @vcNewValue)
							WHEN 'tintOppStatus' THEN
								(CASE @vcNewValue WHEN 1 THEN 'Deal Won' WHEN 2 THEN 'Deal Lost' ELSE 'Open' END)
							WHEN 'tintshipped' THEN
								(CASE @vcNewValue WHEN 1 THEN 'Closed' ELSE 'Open' END)
							WHEN 'numPercentageComplete' THEN
								ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=50 AND numListItemID=@vcNewValue),'')
							ELSE
								@vcNewValue
						END),
						NULL,
						0
					)
				END

				SET @j = @j + 1
			END

			IF @numNewModifiedBy <> -1
			BEGIN
				UPDATE @TEMPOpportunityHistory SET numUserCntID = @numNewModifiedBy WHERE vbStartlsn = @vbStartlsn
			END
		END

		SET @i = @i + 1
	END

	--CLEAR RETRIVED DATA FROM CDC TABLE
	DELETE FROM [cdc].[dbo_OpportunityItems_CT] WHERE __$start_lsn IN (SELECT vbStartlsn FROM @TempTable)
	DELETE FROM [cdc].[dbo_OpportunityMaster_CT] WHERE __$start_lsn IN (SELECT vbStartlsn FROM @TempTable)

	/************************************ OPP/ORDER BIZDOCS ************************************/
	DELETE FROM @TempTable

	INSERT INTO 
		@TempTable
	SELECT
		ROW_NUMBER() OVER(ORDER BY __$start_lsn),
		__$start_lsn
	FROM
		[cdc].[dbo_OpportunityBizDocs_CT]
	WHERE
		__$operation IN (1,2,3)
	GROUP BY
		__$start_lsn

	SET @i = 1
	SELECT @iCOUNT = COUNT(*) FROM @TempTable


	WHILE @i <= @iCOUNT
	BEGIN
		SELECT @vbStartlsn = vbStartlsn FROM @TempTable WHERE ID=@i

		SELECT TOP 1
			@tintOperation = __$operation,
			@numOppID=[cdc].[dbo_OpportunityBizDocs_CT].numOppID,
			@numOppBizDocsID = numOppBizDocsId,
			@vcBizDocID = vcBizDocID,
			@numDomainID=numDomainID,
			@bintModifiedDate=[cdc].[dbo_OpportunityBizDocs_CT].dtModifiedDate,
			@numModifiedBy=[cdc].[dbo_OpportunityBizDocs_CT].numModifiedBy
		FROM 
			[cdc].[dbo_OpportunityBizDocs_CT]
		JOIN
			OpportunityMaster
		ON
			[cdc].[dbo_OpportunityBizDocs_CT].numOppID = OpportunityMaster.numOppID
		WHERE 
			__$start_lsn = @vbStartlsn

		IF @tintOperation = 2 --INSERT
		BEGIN
			INSERT INTO @TEMPOpportunityHistory
			(
				vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcDescription,
				vcHiddenDescription,
				numFieldID,
				bitCustomField
			)
			VALUES
			(
				@vbStartlsn,
				@numOppID,
				@numDomainID,
				@bintModifiedDate,
				@numModifiedBy,
				'Added BizDoc',
				ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE numOppID=@numOppID AND numOppBizDocsId=@numOppBizDocsID),''),
				CONCAT('OppID:',@numOppID,', OppBizDocsID:',@numOppBizDocsID),
				NULL,
				0
			)
		END
		ELSE IF @tintOperation = 1 --DELETE
		BEGIN
			INSERT INTO @TEMPOpportunityHistory
			(
				vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcDescription,
				vcHiddenDescription,
				numFieldID,
				bitCustomField
			)
			VALUES
			(
				@vbStartlsn,
				@numOppID,
				@numDomainID,
				@bintModifiedDate,
				@numModifiedBy,
				'Removed BizDoc',
				@vcBizDocID,
				CONCAT('OppID:',@numOppID,', OppBizDocsID:',@numOppBizDocsID),
				NULL,
				0
			)
		END
		ELSE IF @tintOperation = 3 --UPDATE
		BEGIN
			DELETE FROM @TMEPChangedColumns

			INSERT INTO @TMEPChangedColumns
			(
				ID,
				dtDate,
				vcColumnName,
				vcOldValue,
				vcNewValue
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY TempUpdateBefore.__$start_lsn),
				DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), sys.fn_cdc_map_lsn_to_time(TempUpdateBefore.__$start_lsn)), 
				TempUpdateBefore.column_name, 
				TempUpdateBefore.old_value, 
				TempUpdateAfter.new_value
			FROM
				( 
					SELECT 
						__$start_lsn, 
						column_name, 
						old_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','dtModifiedDate'),__$update_mask) = 1) THEN CAST(dtModifiedDate AS VARCHAR(MAX)) ELSE NULL END AS dtModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS VARCHAR(MAX)) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','monAmountPaid'),__$update_mask) = 1) THEN CAST(monAmountPaid as VARCHAR(MAX)) ELSE NULL END AS monAmountPaid,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcComments'),__$update_mask) = 1) THEN CAST(vcComments as VARCHAR(MAX)) ELSE NULL END AS vcComments,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcBizDocID'),__$update_mask) = 1) THEN CAST(vcBizDocID as VARCHAR(MAX)) ELSE NULL END AS vcBizDocID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numShipVia'),__$update_mask) = 1) THEN CAST(numShipVia AS VARCHAR(MAX)) ELSE NULL END AS numShipVia,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcTrackingNo'),__$update_mask) = 1) THEN CAST(vcTrackingNo AS VARCHAR(MAX)) ELSE NULL END AS vcTrackingNo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numBizDocStatus'),__$update_mask) = 1) THEN CAST(numBizDocStatus AS VARCHAR(MAX)) ELSE NULL END AS numBizDocStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','dtFromDate'),__$update_mask) = 1) THEN CAST(dtFromDate AS VARCHAR(MAX)) ELSE NULL END AS dtFromDate
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocs(@vbStartlsn,@vbStartlsn, N'all update old')
						WHERE 
							__$operation = 3
					) as t1
					UNPIVOT 
					(
						old_value FOR column_name IN (dtModifiedDate,numModifiedBy,monAmountPaid,vcComments,vcBizDocID,numShipVia,vcTrackingNo,numBizDocStatus,dtFromDate) 
					) as unp
				) as TempUpdateBefore
				INNER JOIN
				(
					SELECT 
						__$start_lsn,
						column_name, 
						new_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','dtModifiedDate'),__$update_mask) = 1) THEN CAST(dtModifiedDate AS VARCHAR(MAX)) ELSE NULL END AS dtModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numModifiedBy'),__$update_mask) = 1) THEN CAST(numModifiedBy AS VARCHAR(MAX)) ELSE NULL END AS numModifiedBy,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','monAmountPaid'),__$update_mask) = 1) THEN CAST(monAmountPaid as VARCHAR(MAX)) ELSE NULL END AS monAmountPaid,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcComments'),__$update_mask) = 1) THEN CAST(vcComments as VARCHAR(MAX)) ELSE NULL END AS vcComments,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcBizDocID'),__$update_mask) = 1) THEN CAST(vcBizDocID as VARCHAR(MAX)) ELSE NULL END AS vcBizDocID,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numShipVia'),__$update_mask) = 1) THEN CAST(numShipVia AS VARCHAR(MAX)) ELSE NULL END AS numShipVia,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','vcTrackingNo'),__$update_mask) = 1) THEN CAST(vcTrackingNo AS VARCHAR(MAX)) ELSE NULL END AS vcTrackingNo,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','numBizDocStatus'),__$update_mask) = 1) THEN CAST(numBizDocStatus AS VARCHAR(MAX)) ELSE NULL END AS numBizDocStatus,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocs','dtFromDate'),__$update_mask) = 1) THEN CAST(dtFromDate AS VARCHAR(MAX)) ELSE NULL END AS dtFromDate
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocs(@vbStartlsn, @vbStartlsn, N'all') -- 'all update old' is not necessary here
						WHERE 
							__$operation = 4
					) as t2
					UNPIVOT 
					(
						new_value FOR column_name IN (dtModifiedDate,numModifiedBy,monAmountPaid,vcComments,vcBizDocID,numShipVia,vcTrackingNo,numBizDocStatus,dtFromDate) 
					) as unp 
				) as TempUpdateAfter -- after update
				ON 
					TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn 
					AND TempUpdateBefore.column_name = TempUpdateAfter.column_name

			SET  @j = 1

			SELECT @jCOUNT = COUNT(*) FROM @TMEPChangedColumns

			WHILE @j <= @jCOUNT
			BEGIN
				SELECT 
					@dtDate = dtDate,
					@vcColumnName = vcColumnName,
					@vcOldValue = vcOldValue,
					@vcNewValue = vcNewValue
				FROM 
					@TMEPChangedColumns
				WHERE
					ID = @j

				SET @numNewModifiedBy = -1

				IF @vcColumnName = 'numModifiedBy'
				BEGIN
					-- KEEP VALUES IN TEMPORARY VARIABLE AND VALUES FOR ALL FIELDS UPDATE AFTER LOOP COMPLETES.
					SET @numNewModifiedBy = @vcNewValue
				END
				ELSE IF @vcColumnName = 'dtModifiedDate'
				BEGIN
					BEGIN TRY
						/* OpportunityBizDocs Updated Items */
						INSERT INTO @TEMPOpportunityHistory
						(
							vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcFieldName,
							vcOldValue,
							vcNewValue,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField
						)
						SELECT 
							@vbStartlsn,
							@numOppID,
							@numDomainID,
							@bintModifiedDate,
							@numModifiedBy,
							'Changed BizDoc Item',
							(CASE TempUpdateBefore.column_name
								WHEN 'numUnitHour' THEN 'Unit Hour'
								WHEN 'vcNotes' THEN 'Notes'
								ELSE TempUpdateBefore.column_name
							END), 
							TempUpdateBefore.old_value, 
							TempUpdateAfter.new_value,
							CONCAT('Item Code:',TempUpdateAfter.ItemCode,', Item Name:', TempUpdateAfter.ItemName),
							CONCAT('OppBizDocItemID:',TempUpdateAfter.OppItemID,', numWarehouseItemID:', TempUpdateAfter.WarehouseItemID),
							NULL,
							0
						FROM
							( 
								SELECT 
									__$start_lsn, 
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name, 
									old_value
								FROM 
								(
									SELECT 
										__$start_lsn,
										numOppBizDocItemID As OppItemID,
										CDCOppBixDocItems.numItemCode As ItemCode,
										vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','numItemCode'),__$update_mask) = 1) THEN CAST(CDCOppBixDocItems.numItemCode AS VARCHAR(MAX)) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS VARCHAR(MAX)) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','vcNotes'),__$update_mask) = 1) THEN CAST(vcNotes AS VARCHAR(MAX)) ELSE NULL END AS vcNotes
									FROM 
										cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocItems(@vbStartlsn,@vbStartlsn, N'all update old') AS CDCOppBixDocItems
									JOIN
										Item
									ON
										CDCOppBixDocItems.numItemCode = Item.numItemCode
									WHERE 
										__$operation = 3
								) as t1 
								UNPIVOT 
								(
									old_value FOR column_name IN (numItemCode,numUnitHour,vcNotes) 
								) as unp
							) as TempUpdateBefore
							INNER JOIN
							(
								SELECT 
									__$start_lsn,
									OppItemID,
									ItemCode,
									ItemName,
									WarehouseItemID,
									column_name, 
									new_value
								FROM 
								(
									SELECT 
										__$start_lsn,
										numOppBizDocItemID As OppItemID,
										CDCOppBixDocItems.numItemCode As ItemCode,
										Item.vcItemName As ItemName,
										numWarehouseItmsID As WarehouseItemID,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','numItemCode'),__$update_mask) = 1) THEN CAST(CDCOppBixDocItems.numItemCode AS VARCHAR(MAX)) ELSE NULL END AS numItemCode,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','numUnitHour'),__$update_mask) = 1) THEN CAST(numUnitHour AS VARCHAR(MAX)) ELSE NULL END AS numUnitHour,
										CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_OpportunityBizDocItems','vcNotes'),__$update_mask) = 1) THEN CAST(vcNotes AS VARCHAR(MAX)) ELSE NULL END AS vcNotes
									FROM 
										cdc.fn_cdc_get_all_changes_dbo_OpportunityBizDocItems(@vbStartlsn,@vbStartlsn, N'all') AS CDCOppBixDocItems -- 'all update old' is not necessary here
									JOIN
										Item
									ON
										CDCOppBixDocItems.numItemCode = Item.numItemCode
									WHERE 
										__$operation = 4
								) as t2
								UNPIVOT 
								(
									new_value FOR column_name IN (numItemCode,numUnitHour,vcNotes) 
								) as unp 
							) as TempUpdateAfter -- after update
							ON 
								TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn 
								AND TempUpdateBefore.column_name = TempUpdateAfter.column_name
								AND TempUpdateBefore.OppItemID = TempUpdateAfter.OppItemID
								AND ISNULL(TempUpdateBefore.WarehouseItemID,0) = ISNULL(TempUpdateAfter.WarehouseItemID,0)
					END TRY
					BEGIN CATCH
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
					END CATCH
					
					BEGIN TRY
						/* OpportunityBizDocs Deleted Items */
						INSERT INTO @TEMPOpportunityHistory
						(
							vbStartlsn,
							numRecordID,
							numDomainID,
							dtDate,
							numUserCntID,
							vcEvent,
							vcDescription,
							vcHiddenDescription,
							numFieldID,
							bitCustomField
						)
						SELECT 
							@vbStartlsn,
							@numOppID,
							@numDomainID,
							@bintModifiedDate,
							@numModifiedBy,
							'Removed BizDoc Item',
							CONCAT('Item Code:',TempDelete.ItemCode,', Item Name:', TempDelete.ItemName),
							CONCAT('OppBizDocItemID:',TempDelete.OppItemID,', numWarehouseItemID:', TempDelete.WarehouseItemID),
							NULL,
							0
						FROM
							( 
								SELECT 
									__$start_lsn,
									CDCOppBixDocItems.numOppBizDocItemID As OppItemID,
									CDCOppBixDocItems.numItemCode As ItemCode,
									vcItemName As ItemName,
									numWarehouseItmsID As WarehouseItemID
								FROM 
									[cdc].[dbo_OpportunityBizDocItems_CT] CDCOppBixDocItems
								JOIN
									Item
								ON
									CDCOppBixDocItems.numItemCode = Item.numItemCode
								WHERE 
									__$operation = 1
									AND __$start_lsn = @vbStartlsn
							) AS TempDelete
					END TRY
					BEGIN CATCH
						/* If the specified LSN range does not fall within the change tracking timeline for the capture instance, the function returns error 208 
						("An insufficient number of arguments were supplied for the procedure or function cdc.fn_cdc_get_all_changes."). */
					END CATCH
				END
				ELSE
				BEGIN
					INSERT INTO @TEMPOpportunityHistory
					(
						vbStartlsn,
						numRecordID,
						numDomainID,
						dtDate,
						numUserCntID,
						vcEvent,
						vcFieldName,
						vcOldValue,
						vcNewValue,
						numFieldID,
						bitCustomField
					)
					VALUES
					(
						@vbStartlsn,
						@numOppID,
						@numDomainID,
						@dtDate,
						@numModifiedBy,
						'Changed BizDoc',
						CASE 
							WHEN @vcColumnName = 'monAmountPaid' THEN 'Paid Amount'
							WHEN @vcColumnName = 'vcComments' THEN 'Comments'
							WHEN @vcColumnName = 'vcBizDocID' THEN 'BizDoc ID'
							WHEN @vcColumnName = 'numShipVia' THEN 'Shipping Company'
							WHEN @vcColumnName = 'vcTrackingNo' THEN 'Tracking No'
							WHEN @vcColumnName = 'numBizDocStatus' THEN 'BizDoc Status'
							WHEN @vcColumnName = 'dtFromDate' THEN 'Billing Date'
						END,
						(
							CASE @vcColumnName
								WHEN 'numShipVia' THEN
									ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=82 AND numListItemID=@vcOldValue),'')
								WHEN 'numBizDocStatus' THEN
									ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=11 AND numListItemID=@vcOldValue),'')
								ELSE
									@vcOldValue
							END
						),
						(
							CASE @vcColumnName
								WHEN 'numShipVia' THEN
									ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=82 AND numListItemID=@vcNewValue),'')
								WHEN 'numBizDocStatus' THEN
									ISNULL((SELECT vcData FROM ListDetails WHERE numDomainID=@numDomainID AND numListID=11 AND numListItemID=@vcNewValue),'')
								ELSE
									@vcNewValue
							END
						),
						NULL,
						0
					)
				END

				SET @j = @j + 1
			END

			IF @numNewModifiedBy <> -1
			BEGIN
				UPDATE @TEMPOpportunityHistory SET numUserCntID = @numNewModifiedBy WHERE vbStartlsn = @vbStartlsn
			END
		END

		SET @i = @i + 1
	END

	--CLEAR RETRIVED DATA FROM CDC TABLE
	DELETE FROM [cdc].[dbo_OpportunityBizDocItems_CT] WHERE __$start_lsn IN (SELECT vbStartlsn FROM @TEMPOpportunityHistory)
	DELETE FROM [cdc].[dbo_OpportunityBizDocs_CT] WHERE __$start_lsn IN (SELECT vbStartlsn FROM @TEMPOpportunityHistory)
	

	/************************************ OPP/ORDER CUSTOM FIELDS **********************************/

	DELETE FROM @TempTable

	INSERT INTO 
		@TempTable
	SELECT
		ROW_NUMBER() OVER(ORDER BY __$start_lsn),
		__$start_lsn
	FROM
		[cdc].[dbo_CFW_Fld_Values_Opp_CT]
	WHERE
		__$operation IN (2,3)
	GROUP BY
		__$start_lsn

	SET @i = 1
	SELECT @iCOUNT = COUNT(*) FROM @TempTable
	DECLARE @Fld_ID NUMERIC(18,0)
	DECLARE @Fld_Value VARCHAR(MAX)

	WHILE @i <= @iCOUNT
	BEGIN
		SELECT @vbStartlsn = vbStartlsn FROM @TempTable WHERE ID=@i

		SELECT TOP 1
			@tintOperation = __$operation,
			@numOppID=numOppId,
			@numDomainID=numDomainID,
			@Fld_ID = Fld_Id,
			@Fld_Value = Fld_Value,
			@bintModifiedDate=[cdc].[dbo_CFW_Fld_Values_Opp_CT].bintModifiedDate,
			@numModifiedBy=[cdc].[dbo_CFW_Fld_Values_Opp_CT].numModifiedBy
		FROM 
			[cdc].[dbo_CFW_Fld_Values_Opp_CT]
		JOIN
			OpportunityMaster
		ON
			[cdc].[dbo_CFW_Fld_Values_Opp_CT].RecId = OpportunityMaster.numOppID
		WHERE 
			__$start_lsn = @vbStartlsn


		IF @tintOperation = 2 --INSERT
		BEGIN
			INSERT INTO @TEMPOpportunityHistory
			(
				vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcOldValue,
				vcNewValue,
				vcDescription,
				vcHiddenDescription,
				numFieldID,
				bitCustomField
			)
			VALUES
			(
				@vbStartlsn,
				@numOppID,
				@numDomainID,
				@bintModifiedDate,
				@numModifiedBy,
				'Change',
				'',
				dbo.GetCustFldValue(@Fld_ID,NULL,@numOppID),
				'',
				'',
				@Fld_ID,
				1
			)
		END
		ELSE IF @tintOperation = 3 --UPDATE
		BEGIN 
			INSERT INTO @TEMPOpportunityHistory
			(
				vbStartlsn,
				numRecordID,
				numDomainID,
				dtDate,
				numUserCntID,
				vcEvent,
				vcOldValue,
				vcNewValue,
				numFieldID,
				vcFieldName,
				bitCustomField
			)
			SELECT 
				@vbStartlsn,
				@numOppID,
				@numDomainID,
				TempUpdateAfter.bintModifiedDate,
				TempUpdateAfter.numModifiedBy, 
				'Change',
				dbo.fn_GetCustFldStringValue(TempUpdateBefore.Fld_ID,@numOppID,TempUpdateBefore.old_value), 
				dbo.fn_GetCustFldStringValue(TempUpdateBefore.Fld_ID,@numOppID,TempUpdateAfter.new_value),
				TempUpdateBefore.Fld_ID, 
				(SELECT Fld_label FROM CFW_Fld_Master DFM WHERE DFM.Fld_id = TempUpdateBefore.Fld_ID) vcFieldName,
				1
			FROM
				( 
					SELECT 
						__$start_lsn, 
						Fld_id,
						RecId,
						numModifiedBy,
						bintModifiedDate,
						old_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							Fld_id,
							RecId,
							numModifiedBy,
							bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_CFW_Fld_Values_Opp','Fld_Value'),__$update_mask) = 1) THEN CAST(Fld_Value AS VARCHAR(MAX)) ELSE NULL END AS Fld_Value
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_CFW_Fld_Values_Opp(@vbStartlsn,@vbStartlsn, N'all update old')
						WHERE 
							__$operation = 3
					) as t1
					UNPIVOT 
					(
						old_value FOR column_name IN (Fld_Value) 
					) as unp
				) as TempUpdateBefore
				INNER JOIN
				(
					SELECT 
						__$start_lsn,
						Fld_id,
						RecId,
						numModifiedBy,
						bintModifiedDate,
						new_value
					FROM 
					(
						SELECT 
							__$start_lsn,
							Fld_id,
							RecId,
							numModifiedBy,
							bintModifiedDate,
							CASE WHEN (sys.fn_cdc_is_bit_set (sys.fn_cdc_get_column_ordinal ('dbo_CFW_Fld_Values_Opp','Fld_Value'),__$update_mask) = 1) THEN CAST(Fld_Value AS VARCHAR(MAX)) ELSE NULL END AS Fld_Value
						FROM 
							cdc.fn_cdc_get_all_changes_dbo_CFW_Fld_Values_Opp(@vbStartlsn, @vbStartlsn, N'all') -- 'all update old' is not necessary here
						WHERE 
							__$operation = 4
					) as t2
					UNPIVOT 
					(
						new_value FOR column_name IN (Fld_Value) 
					) as unp 
				) as TempUpdateAfter -- after update
				ON 
					TempUpdateBefore.__$start_lsn = TempUpdateAfter.__$start_lsn 
					AND TempUpdateBefore.Fld_id = TempUpdateAfter.Fld_id
					AND TempUpdateBefore.RecId = TempUpdateAfter.RecId
		END

		SET @i = @i + 1
	END

	--CLEAR RETRIVED DATA FROM CDC TABLE
	DELETE FROM [cdc].[dbo_CFW_Fld_Values_Opp_CT] WHERE __$start_lsn IN (SELECT vbStartlsn FROM @TEMPOpportunityHistory)

	BEGIN TRY
	BEGIN TRANSACTION

		--INSERT DATA INTO OUR RECORD HISTORY TABLE
		INSERT INTO RecordHistory
		(
			numDomainID,
			numUserCntID,
			dtDate,
			numRecordID,
			numRHModuleMasterID,
			vcEvent,
			numFieldID,
			vcFieldName,
			bitCustomField,
			vcOldValue,
			vcNewValue,
			vcDescription,
			vcHiddenDescription
		)
		SELECT
			numDomainID,
			numUserCntID,
			ISNULL(dtDate,GETUTCDATE()),
			numRecordID,
			3,
			vcEvent,
			numFieldID,
			vcFieldName,
			bitCustomField,
			vcOldValue,
			vcNewValue,
			vcDescription,
			vcHiddenDescription
		FROM
			@TEMPOpportunityHistory
	COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END