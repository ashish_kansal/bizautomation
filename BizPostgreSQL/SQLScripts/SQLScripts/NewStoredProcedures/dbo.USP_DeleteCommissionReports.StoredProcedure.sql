SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteCommissionReports')
DROP PROCEDURE USP_DeleteCommissionReports
GO
CREATE PROCEDURE [dbo].[USP_DeleteCommissionReports]
@numComReports AS NUMERIC
AS
 
DELETE FROM Dashboard WHERE numReportID=@numComReports AND tintReportCategory=2

DELETE FROM CommissionReports WHERE numComReports=@numComReports


GO
