SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageReview')
DROP PROCEDURE USP_ManageReview
GO
CREATE PROCEDURE [dbo].[USP_ManageReview]   

@numReviewID as numeric(9)=0 OUTPUT,    
@tintTypeId as INT = 0,       
@vcReferenceId	 as varchar(100)='',  
@vcReviewTitle as varchar(50) = '',      
@vcReviewComment as varchar(1000) = '' ,
@bitEmailMe AS BIT ,
@vcIpAddress AS VARCHAR(100) ,
@numContactId AS NUMERIC(18,0) ,
@numSiteId AS NUMERIC(18,0) ,
@numDomainId AS NUMERIC(18,0) ,
@vcCreatedDate AS VARCHAR(100),
@bitHide AS BIT = FALSE,
@bitApproved AS BIT = TRUE 
      
AS

IF @numReviewId = 0      
    BEGIN      
      INSERT INTO Review(
        tintTypeId ,
        vcReferenceId,
        vcReviewTitle,
        vcReviewComment,
        bitEmailMe ,
        vcIpAddress,
        numContactId ,
        numSiteId ,
        numDomainId ,
        dtCreatedDate ,
        bitHide ,
		bitApproved
        )      
      values(
        
        @tintTypeId ,
        @vcReferenceId,
        @vcReviewTitle,
        @vcReviewComment,
        @bitEmailMe ,
        @vcIpAddress,
        @numContactId ,
        @numSiteId ,
        @numDomainId ,
        CONVERT(DATETIME , @vcCreatedDate),
        @bitHide ,
		@bitApproved
        )      
         SET @numReviewID = SCOPE_IDENTITY()
end      
else      
begin      
  UPDATE Review set 
        
        vcReviewTitle = @vcReviewTitle,
        vcReviewComment = @vcReviewComment,
        bitHide =  @bitHide ,
		bitApproved = @bitApproved
        where numReviewId = @numReviewId  
         
end

