SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EDIQueueLog_Insert')
DROP PROCEDURE dbo.USP_EDIQueueLog_Insert
GO
CREATE PROCEDURE [dbo].[USP_EDIQueueLog_Insert]
(
	@numEDIQueueID NUMERIC(18,0),
	@EDIType INT,
	@numDomainID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
	@vcLog NTEXT,
	@bitSuccess BIT,
	@vcExceptionMessage NTEXT
)
AS 
BEGIN
	INSERT INTO EDIQueueLog
	(
		numEDIQueueID
		,numDomainID
		,numOppID
		,vcLog
		,bitSuccess
		,vcExceptionMessage
		,dtDate
	)
	VALUES
	(
		@numEDIQueueID
		,@numDomainID
		,@numOppID
		,@vcLog
		,@bitSuccess
		,@vcExceptionMessage
		,GETUTCDATE()
	)

	IF ISNULL(@numEDIQueueID,0) > 0
	BEGIN
		UPDATE 
			EDIQueue
		SET
			bitExecuted=1
			,bitSuccess=@bitSuccess
			,dtExecutionDate=GETUTCDATE()
		WHERE
			ID=@numEDIQueueID
	END

	DECLARE @tintOppType AS TINYINT
	DECLARE @tintOppStatus AS TINYINT
	IF @EDIType = 940 
	BEGIN
		IF ISNULL(@bitSuccess,0) = 0
		BEGIN
			SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID
			UPDATE OpportunityMaster SET numStatus=15446,bintModifiedDate=GETUTCDATE(),tintEDIStatus=8 WHERE numOppId=@numOppID --15446: Shipment Request (940) Failed

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,0,@numOppID,15446
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= 15446)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = 15446, -- numeric(18, 0)
							@numUserCntID = 0, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		END
		ELSE
		BEGIN
			IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=3)
			BEGIN
				UPDATE OpportunityMaster SET tintEDIStatus=3,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --3: 940 Sent
			
				INSERT INTO EDIHistory
				(
					numDomainID,numOppID,tintEDIStatus,dtDate
				)
				VALUES
				(
					@numDomainID,@numOppID,3,GETUTCDATE()
				)
			END
			
		END
	END
	ELSE IF @EDIType = 940997 -- 940 Acknowledge
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=4)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=4,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --3: 940 Acknowledged
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,4,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 8565 AND ISNULL(@bitSuccess,0) = 1 -- 856 Received from 3PL
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=5)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=5,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --5: 856 Received
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,5,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 856997 AND ISNULL(@bitSuccess,0) = 1 -- 856 Acknowledged
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=6)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=6,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --6: 856 Acknowledged
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,6,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 856810997 AND ISNULL(@bitSuccess,0) = 1 -- 856/810 Acknowledged
	BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=10)
		BEGIN
			UPDATE OpportunityMaster SET tintEDIStatus=10,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --10: 856/810 Acknowledged
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,10,GETUTCDATE()
			)
		END
	END
	ELSE IF @EDIType = 8566 -- 856 Sent to EDI
	BEGIN
		IF ISNULL(@bitSuccess,0) = 0
		BEGIN
			SELECT @tintOppStatus=tintOppStatus,@tintOppType=tintOppType from OpportunityMaster where numOppID=@numOppID
			UPDATE OpportunityMaster SET numStatus=15448,bintModifiedDate=GETUTCDATE(),tintEDIStatus=9 WHERE numOppId=@numOppID --15448: Send 856 & 810 Failed

			IF @tintOppType=1 AND @tintOppStatus=1
			BEGIN
				EXEC USP_SalesFulfillmentQueue_Save @numDomainID,0,@numOppID,15448
			END

		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0) != 15448)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = 15448, -- numeric(18, 0)
							@numUserCntID = 0, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		END
		ELSE
		BEGIN
			IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=7)
			BEGIN
				UPDATE OpportunityMaster SET tintEDIStatus=7,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --7: 856 Sent
			
				INSERT INTO EDIHistory
				(
					numDomainID,numOppID,tintEDIStatus,dtDate
				)
				VALUES
				(
					@numDomainID,@numOppID,7,GETUTCDATE()
				)
			END
		END
	END

	ELSE IF @EDIType = 85011 AND ISNULL(@bitSuccess,0) = 1   --- 850 SO Partially Created
	BEGIN
		BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=11)
			UPDATE OpportunityMaster SET tintEDIStatus=11,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --11: 850 SO Partially Created
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,11,GETUTCDATE()
			)
		END
	END

	ELSE IF @EDIType = 85012 AND ISNULL(@bitSuccess,0) = 1   --- 850 SO Created
	BEGIN
		BEGIN
		IF NOT EXISTS (SELECT ID FROM EDIHistory WHERE numOppID=@numOppID AND tintEDIStatus=12)
			UPDATE OpportunityMaster SET tintEDIStatus=12,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID --12: 850 SO Created
			
			INSERT INTO EDIHistory
			(
				numDomainID,numOppID,tintEDIStatus,dtDate
			)
			VALUES
			(
				@numDomainID,@numOppID,12,GETUTCDATE()
			)
		END
	END
	
END
GO