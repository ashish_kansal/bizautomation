SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CurrencyConversionStatus_GetDomainByBaseCurrency')
DROP PROCEDURE USP_CurrencyConversionStatus_GetDomainByBaseCurrency
GO
CREATE PROCEDURE [dbo].[USP_CurrencyConversionStatus_GetDomainByBaseCurrency]  
	
AS  
BEGIN 
	SELECT 
		Currency.chrCurrency,
		STUFF((SELECT CONCAT(',', a.numDomainID) FROM Domain a INNER JOIN Currency b ON a.numCurrencyID = b.numCurrencyID WHERE a.numDomainId > 0 AND b.chrCurrency = Currency.chrCurrency FOR XML PATH('')), 1, 1, '') vcDomainIDs,
		STUFF((SELECT DISTINCT CONCAT(',', a.chrCurrency, Currency.chrCurrency) FROM Currency a WHERE bitEnabled = 1 AND a.chrCurrency <> Currency.chrCurrency FOR XML PATH('')), 1, 1, '') vcCurrencyCodes
	FROM 
		Domain
	INNER JOIN
		Currency
	ON
		Domain.numCurrencyID = Currency.numCurrencyID
	GROUP BY
		Currency.chrCurrency
END