
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWebSource')
DROP PROCEDURE USP_GetWebSource
GO
CREATE PROCEDURE [dbo].[USP_GetWebSource]
                @numDomainID NUMERIC(9),
                @dtStartDate DATETIME,
                @dtEndDate   DATETIME
AS
  BEGIN
  
	DECLARE @total numeric;
   
    SELECT   TOP 5
			[vcDomainName],
             SUM(1) AS percentage
             INTO WebSource
    FROM     [TrackingVisitorsHDR]
    WHERE    [numDomainID] = @numDomainID
             AND [dtCreated] >= @dtStartDate
             AND [dtCreated] <= @dtEndDate
    GROUP BY [vcDomainName]
    ORDER BY SUM(1) DESC
    
    
    SELECT @total=SUM(percentage) FROM WebSource
--    PRINT @total
    
   
    SELECT   [vcDomainName] AS Source,
			ISNULL(percentage*100/@total,0) AS percentage 
	FROM WebSource
	GROUP BY [vcDomainName],percentage
	ORDER BY percentage DESC

	DROP TABLE WebSource
  
  END

