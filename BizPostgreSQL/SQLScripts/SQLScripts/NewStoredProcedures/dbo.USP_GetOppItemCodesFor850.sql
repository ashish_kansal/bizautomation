SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppitemcodesfor850')
DROP PROCEDURE dbo.USP_GetOppItemCodesFor850
GO

/****** Author :  Priya Sharma
		Date : 26 June 2018
 ******/
CREATE PROCEDURE [dbo].[USP_GetOppItemCodesFor850]  

	@numOppId AS NUMERIC(18,0)
  AS

  BEGIN

	 SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId = @numOppId	

  END

GO


