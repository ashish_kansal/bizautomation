GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_DisassociatePO')
DROP PROCEDURE USP_OpportunityMaster_DisassociatePO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_DisassociatePO]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@vcOppItemIds VARCHAR(MAX)
AS
BEGIN
	IF ISNULL(@vcOppItemIds,'') <> ''
	BEGIN
		IF EXISTS (SELECT numComissionID FROM BizDocComission WHERE numDomainId=@numDomainID AND ISNULL(monCommissionPaid,0) <> 0 AND numOppItemID IN (SELECT Id FROM dbo.SplitIDs(@vcOppItemIds,',')))
		BEGIN
			RAISERROR('COMMISSION_PAID',16,1)
		END

		IF EXISTS (SELECT numComissionID FROM BizDocComission INNER JOIN OpportunityBizDocItems OBDI ON BizDocComission.numOppBizDocItemID=OBDI.numOppBizDocItemID WHERE numDomainId=@numDomainID AND ISNULL(monCommissionPaid,0) <> 0 AND OBDI.numOppItemID IN (SELECT Id FROM dbo.SplitIDs(@vcOppItemIds,',')))
		BEGIN
			RAISERROR('COMMISSION_PAID',16,1)
		END

		DELETE FROM SalesOrderLineItemsPOLinking WHERE numDomainId=@numDomainID AND numSalesOrderID=@numOppID AND numSalesOrderItemID IN (SELECT Id FROM dbo.SplitIDs(@vcOppItemIds,','))
	END
END
GO