GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateBillingTerms')
	DROP PROCEDURE USP_UpdateBillingTerms
GO

CREATE PROCEDURE [dbo].[USP_UpdateBillingTerms]
(
	 @numDomainID  numeric=0,                                          
     @numDivisionID  numeric=0, 
	 @numCompanyID numeric=0,
	 @numCompanyCredit numeric=0,                                   
	 @vcDivisionName  varchar (100)='',
	 @numUserCntID  numeric=0,                                                                                                                                     
	 @tintCRMType  tinyint=0,                                          
	 @tintBillingTerms as tinyint,                                          
	 @numBillingDays as numeric(9),                                         
	 @tintInterestType as tinyint,                                          
	 @fltInterest as float,                          
	 @bitNoTax as BIT,
	 @numCurrencyID AS numeric(9)=0,
	 @numDefaultPaymentMethod AS numeric(9)=0,         
	 @numDefaultCreditCard AS numeric(9)=0,         
	 @bitOnCreditHold AS bit=0,
	 @vcShipperAccountNo VARCHAR(100) = '',
	 @intShippingCompany INT = 0,
	 @bitEmailToCase BIT=0,
	 @numDefaultExpenseAccountID NUMERIC(18,0) = 0,
	 @numDefaultShippingServiceID NUMERIC(18,0),
	 @numAccountClass NUMERIC(18,0) = 0,
	 @tintPriceLevel INT = 0,
	 @bitShippingLabelRequired BIT,
	 @tintInbound850PickItem INT = 0,
	 @bitAllocateInventoryOnPickList BIT = 0,
	 @bitAutoCheckCustomerPart BIT=0
)
AS 
	BEGIN
	DECLARE @bitAllocateInventoryOnPickListOld AS TINYINT
	SELECT @bitAllocateInventoryOnPickListOld=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID


	IF @bitAllocateInventoryOnPickList <> @bitAllocateInventoryOnPickListOld AND (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND tintOppType=1 AND tintOppStatus=1 AND ISNULL(tintshipped,0)=0) > 0
	BEGIN
		RAISERROR('NOT_ALLOWED_CHANGE_ALLOCATE_INVENTORY_FROM_PACKING_SLIP_OPEN_SALES_ORDER',16,1)
		RETURN
	END

		UPDATE DivisionMaster 
		SET numCompanyID = @numCompanyID,
			vcDivisionName = @vcDivisionName,                                      
			numModifiedBy = @numUserCntID,                                            
			bintModifiedDate = GETUTCDATE(),                                         
			tintCRMType = @tintCRMType,                                          
			tintBillingTerms = @tintBillingTerms,                                          
			numBillingDays = @numBillingDays,                                         
			tintInterestType = @tintInterestType,                                           
			fltInterest =@fltInterest,                
			bitNoTax=@bitNoTax,
			numCurrencyID=@numCurrencyID,
			numDefaultPaymentMethod=@numDefaultPaymentMethod,                                                  
			numDefaultCreditCard=@numDefaultCreditCard,                                                 
			bitOnCreditHold=@bitOnCreditHold,
			vcShippersAccountNo = @vcShipperAccountNo,
			intShippingCompany = @intShippingCompany,
			numDefaultExpenseAccountID = @numDefaultExpenseAccountID,
			numDefaultShippingServiceID = @numDefaultShippingServiceID,
			numAccountClassID = ISNULL(@numAccountClass,0),
			tintPriceLevel = ISNULL(@tintPriceLevel,0),
			bitShippingLabelRequired=@bitShippingLabelRequired,
			tintInbound850PickItem = ISNULL(@tintInbound850PickItem,0),
			bitAllocateInventoryOnPickList=ISNULL(@bitAllocateInventoryOnPickList,0),
			bitAutoCheckCustomerPart=ISNULL(@bitAutoCheckCustomerPart,0)
		WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID     

		UPDATE CompanyInfo
		SET numCompanyCredit = @numCompanyCredit,               
			numModifiedBy = @numUserCntID,                
			bintModifiedDate = GETUTCDATE()               
		WHERE                 
			numCompanyId=@numCompanyId and numDomainID=@numDomainID    

	END