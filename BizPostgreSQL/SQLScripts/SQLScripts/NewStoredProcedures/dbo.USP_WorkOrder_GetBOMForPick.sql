GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetBOMForPick')
DROP PROCEDURE USP_WorkOrder_GetBOMForPick
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetBOMForPick]                             
	@numDomainID NUMERIC(18,0)
	,@vcWorkOrders VARCHAR(MAX)
	,@ClientTimeZoneOffset INT               
AS                            
BEGIN
	SELECT
		WorkOrder.numWOId
		,WorkOrderDetails.numWODetailId
		,ISNULL(WorkOrder.vcWorkOrderName,'-') vcWorkOrderName
		,CONCAT(Item.vcItemName, ' (',WorkOrder.numQtyItemsReq,')') vcAssembly
		,(CASE 
			WHEN OpportunityItems.ItemReleaseDate IS NOT NULL 
			THEN dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID) 
			ELSE dbo.FormatedDateFromDate(WorkOrder.dtmEndDate,@numDomainID)
		END) vcRequestedDate
		,ItemChild.numItemCode
		,CONCAT(ItemChild.vcItemName, ' (',WorkOrderDetails.numQtyItemsReq,')') vcBOM
		,WorkOrderDetails.numQtyItemsReq numRequiredQty
		,ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId=WorkOrderDetails.numWODetailId),0) numPickedQty
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,ISNULL(Warehouses.vcWareHouse,'') vcWarehouse
		,ISNULL(WareHouseItems.numAllocation,0) numAllocation
		,(CASE 
			WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=ItemChild.numItemCode AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID)
			THEN CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
									FROM 
										WareHouseItems WIInner
									INNER JOIN
										WarehouseLocation WL
									ON
										WIInner.numWLocationID = WL.numWLocationID
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=ItemChild.numItemCode
										AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
									ORDER BY
										WL.vcLocation
									FOR XML PATH('')),1,1,''),']')
			ELSE CONCAT('[',STUFF((SELECT 
								CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
							FROM 
								WareHouseItems WIInner
							WHERE 
								WIInner.numDomainID=@numDomainID 
								AND WIInner.numItemID=ItemChild.numItemCode
								AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
							FOR XML PATH('')),1,1,''),']')
		END)  vcWarehouseLocations
	FROM
		WorkOrder
	INNER JOIN
		Item
	ON
		WorkOrder.numItemCode = Item.numItemCode
	LEFT JOIN
		OpportunityItems
	ON
		WorkOrder.numOppId = OpportunityItems.numOppId
		AND WorkOrder.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		WorkOrderDetails
	ON
		WorkOrder.numWOId = WorkOrderDetails.numWOId
	INNER JOIN
		Item ItemChild
	ON
		WorkOrderDetails.numChildItemID = ItemChild.numItemCode
	INNER JOIN
		WareHouseItems
	ON
		WorkOrderDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId IN (SELECT Id FROM dbo.SplitIDs(@vcWorkOrders,','))
		AND (WorkOrderDetails.numQtyItemsReq - ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WOPI WHERE WOPI.numWODetailId=WorkOrderDetails.numWODetailId),0)) > 0
	ORDER BY
		WorkOrder.numWOId
END
GO