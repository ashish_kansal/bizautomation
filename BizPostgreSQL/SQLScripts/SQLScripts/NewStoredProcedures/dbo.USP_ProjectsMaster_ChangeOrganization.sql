GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectsMaster_ChangeOrganization')
DROP PROCEDURE USP_ProjectsMaster_ChangeOrganization
GO
CREATE PROCEDURE [dbo].[USP_ProjectsMaster_ChangeOrganization]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numUserCntID NUMERIC(18,0),
	@numProId NUMERIC(18,0),
	@numNewDivisionID NUMERIC(18,0),
	@numNewContactID NUMERIC(18,0)               
)                                    
as
BEGIN
	DECLARE @numOldDivisionID NUMERIC(18,0)

	SELECT @numOldDivisionID=numDivisionId FROM ProjectsMaster WHERE numDomainId=@numDomainID AND numProId = @numProId


	BEGIN TRY
	BEGIN TRANSACTION

		UPDATE 
			ProjectsMaster 
		SET 
			numDivisionId=@numNewDivisionID
			,numCustPrjMgr=@numNewContactID
		WHERE 
			numDomainId=@numDomainID 
			AND numProId = @numProId
		

		INSERT INTO [dbo].[RecordOrganizationChangeHistory]
		(
			numDomainID,numProjectID,numOldDivisionID,numNewDivisionID,numModifiedBy,dtModified
		)
		VALUES
		(
			@numDomainID,@numProId,@numOldDivisionID,@numNewDivisionID,@numUserCntID,GETUTCDATE()
		)

		DELETE FROM AddressDetails WHERE numRecordID=@numProId AND tintAddressOf=4

		INSERT INTO dbo.AddressDetails 
		(
			vcAddressName
			,vcStreet
			,vcCity
			,vcPostalCode
			,numState
			,numCountry
			,bitIsPrimary
			,tintAddressOf
			,tintAddressType
			,numRecordID
			,numDomainID
		)
		SELECT 
			vcAddressName
			,vcStreet
			,vcCity
			,vcPostalCode
			,numState
			,numCountry
			,bitIsPrimary
			,4,tintAddressType
			,@numProId
			,numDomainID
		FROM 
			dbo.AddressDetails
		WHERE 
			numDomainID=@numDomainID 
			AND numRecordID=@numNewDivisionID 
			AND tintAddressOf = 2 
			AND tintAddressType = 2
			AND bitIsPrimary=1      
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO