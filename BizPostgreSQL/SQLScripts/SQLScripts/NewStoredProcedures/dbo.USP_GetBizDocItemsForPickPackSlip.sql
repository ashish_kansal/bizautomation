--created by anoop jayaraj

-- exec USP_GetOppItemBizDocs @numBizDocID=644,@numDomainID=169,@numOppID=476291,@numOppBizDocID=0,@bitRentalBizDoc=0
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBizDocItemsForPickPackSlip' ) 
    DROP PROCEDURE USP_GetBizDocItemsForPickPackSlip
GO
CREATE PROCEDURE USP_GetBizDocItemsForPickPackSlip
    @numDomainID AS NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
    @numOppBizDocID AS NUMERIC(18,0),
	@numBizDocID AS NUMERIC(18,0),
	@numForBizDocID AS NUMERIC(18,0)
AS 
BEGIN
	SELECT
		OBDI.numOppItemID AS numoppitemtcode
		,OBD.numOppBizDocsId
		,OBDI.numUnitHour AS OrderedQty
		,TEMP.numUnitHour AS UsedQty
		,ISNULL(OBDI.numUnitHour,0) - ISNULL(TEMP.numUnitHour,0) AS RemainingQty
	FROM
		OpportunityBizDocs OBD
	INNER JOIN
		OpportunityBizDocItems OBDI
	ON
		OBDI.numOppBizDocID = OBD.numOppBizDocsId
	LEFT JOIN
		BizDocTemplate AS B
	ON
		OBD.numBizDocTempID=B.numBizDocTempID
	OUTER APPLY
	(
		SELECT
			SUM(numUnitHour) AS numUnitHour
		FROM
			OpportunityBizDocs OBDCHild
		INNER JOIN
			OpportunityBizDocItems OBDICHild
		ON
			OBDICHild.numOppBizDocID = OBDCHild.numOppBizDocsId
		WHERE
			OBDCHild.numSourceBizDocId = OBD.numOppBizDocsId
			AND OBDICHild.numOppItemID = OBDI.numOppItemID
			AND OBDChild.numBizDocID = @numForBizDocID
	) TEMP
	WHERE
		OBD.numOppId=@numOppID 
		AND OBD.numOppBizDocsId=@numOppBizDocID
END


