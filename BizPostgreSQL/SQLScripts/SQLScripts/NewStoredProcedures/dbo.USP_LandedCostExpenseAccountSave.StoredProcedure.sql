GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_LandedCostExpenseAccountSave' )
    DROP PROCEDURE [USP_LandedCostExpenseAccountSave]
GO
CREATE PROC [dbo].[USP_LandedCostExpenseAccountSave]
    @numDomainId NUMERIC(18, 0) ,
    @strItems VARCHAR(MAX),
	@vcLanedCostDefault VARCHAR(MAX)
AS
    BEGIN TRY
        BEGIN TRANSACTION 
		UPDATE Domain SET vcLanedCostDefault=@vcLanedCostDefault WHERE 
		numDomainId=@numDomainID
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> ''
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

                SELECT  *
                INTO    #temp
                FROM    OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                         WITH (numLCExpenseId NUMERIC(18,0), vcDescription VARCHAR(50), numAccountId NUMERIC(18, 0)) X
     
                DELETE  FROM [dbo].[LandedCostExpenseAccount]
                WHERE   [numDomainId] = @numDomainId
                        AND numLCExpenseId NOT IN ( SELECT  numLCExpenseId
                                                    FROM    #temp
                                                    WHERE   numLCExpenseId > 0 )
	       
                UPDATE  LCEA
                SET     [vcDescription] = T.vcDescription ,
                        [numAccountId] = T.numAccountId
                FROM    [LandedCostExpenseAccount] LCEA
                        JOIN #temp T ON LCEA.numLCExpenseId = T.numLCExpenseId
                WHERE   T.numLCExpenseId > 0
                        AND LCEA.[numDomainId] = @numDomainId
			                   
                INSERT  INTO [dbo].[LandedCostExpenseAccount]
                        ( [numDomainId] ,
                          [vcDescription] ,
                          [numAccountId]
                        )
                        SELECT  @numDomainId ,
                                vcDescription ,
                                numAccountId
                        FROM    #temp
                        WHERE   numLCExpenseId = 0

                EXEC sp_xml_removedocument @hDocItem
            END

        COMMIT TRANSACTION

    END TRY
    BEGIN CATCH
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 )
            BEGIN
                RAISERROR ( @strMsg, 16, 1 );
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH