SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BankReconcileMatchRule_Save')
DROP PROCEDURE dbo.USP_BankReconcileMatchRule_Save
GO
CREATE PROCEDURE [dbo].[USP_BankReconcileMatchRule_Save]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @vcName VARCHAR(200),
    @vcBankAccounts VARCHAR(MAX),
    @bitMatchAllConditions BIT,
	@vcConditions TEXT
)
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @tintOrder AS INT
	DECLARE @numRuleID AS NUMERIC(18,0)
	SET @tintOrder = ISNULL((SELECT MAX(tintOrder) FROM BankReconcileMatchRule),0) + 1

	INSERT INTO BankReconcileMatchRule
	(
		[numDomainID]
		,[vcName]
		,[vcBankAccounts]
		,[bitMatchAllConditions]
		,[numCreatedBy]
		,[dtCreatedDate]
		,[tintOrder]
	)
	VALUES
	(
		@numDomainID
		,ISNULL(@vcName,'')
		,ISNULL(@vcBankAccounts,'')
		,ISNULL(@bitMatchAllConditions,0)
		,@numUserCntID
		,GETUTCDATE()
		,@tintOrder
	)

	
	SELECT @numRuleID = SCOPE_IDENTITY()

	DECLARE @hDocItem INT
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcConditions

	INSERT INTO BankReconcileMatchRuleCondition
	(
		[numRuleID]
		,[tintColumn]
		,[tintConditionOperator]
		,[vcTextToMatch]
		,[numDivisionID]
	)
	SELECT
		@numRuleID
		,tintColumn
		,tintConditionOperator
		,vcTextToMatch
		,numDivisionID
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Condition',2)
	WITH
	(
		tintColumn TINYINT
		,tintConditionOperator TINYINT
		,vcTextToMatch VARCHAR(MAX)
		,numDivisionID NUMERIC(18,0)
	)
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END