GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShippingRuleStates' ) 
    DROP PROCEDURE USP_ManageShippingRuleStates
GO
CREATE PROCEDURE USP_ManageShippingRuleStates   
    @numCountryID NUMERIC,
    @numStateID NUMERIC,
    @numRuleID NUMERIC,
    @numDomainID NUMERIC,
	@vcZipPostalRange VARCHAR(200)
AS 
BEGIN
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)

	SELECT 
		@numRelationship=numRelationship
		,@numProfile=numProfile 
	FROM 
		ShippingRules 
	WHERE 
		numDomainID=@numDomainID 
		AND numRuleID=@numRuleID

	DECLARE @TEMP TABLE
	(
		numDomainID NUMERIC(18,0)
		,numRuleID NUMERIC(18,0)
		,numCountryID NUMERIC(18,0)
		,numStateID NUMERIC(18,0)
		,vcZipPostal VARCHAR(20)
	)

	IF LEN(ISNULL(@vcZipPostalRange,'')) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numDomainID
			,numRuleID
			,numCountryID
			,numStateID
			,vcZipPostal
		)
		SELECT
			@numDomainID
			,@numRuleID
			,@numCountryID
			,@numStateID
			,OutParam
		FROM
			dbo.SplitString(@vcZipPostalRange,',')
	END
	ELSE
	BEGIN
		INSERT INTO @TEMP
		(
			numDomainID
			,numRuleID
			,numCountryID
			,numStateID
			,vcZipPostal
		)
		SELECT
			@numDomainID
			,@numRuleID
			,@numCountryID
			,@numStateID
			,''
	END

	IF (SELECT 
			COUNT(*) 
		FROM 
			dbo.ShippingRules SR
		WHERE 
			numDomainId=@numDomainId 
			AND numRuleID <> ISNULL(@numRuleID,0) 
			AND ISNULL(numRelationship,0) = ISNULL(@numRelationship,0)
			AND ISNULL(numProfile,0) = ISNULL(@numProfile,0)
			AND EXISTS (SELECT 
							SRSL1.numRuleID
						FROM 
							ShippingRuleStateList SRSL1 
						INNER JOIN
							@Temp SRSL2
						ON
							ISNULL(SRSL1.numStateID,0) = ISNULL(SRSL2.numStateID,0)
							AND ISNULL(SRSL1.vcZipPostal,'') = ISNULL(SRSL2.vcZipPostal,'')
							AND ISNULL(SRSL1.numCountryID,0) = ISNULL(SRSL2.numCountryID,0)
						WHERE 
							SRSL1.numDomainID=@numDomainID 
							AND SRSL1.numRuleID = SR.numRuleID
							AND SRSL2.numRuleID = @numRuleID)
			) > 0
	BEGIN
 		RAISERROR ( 'DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP',16, 1 )
 		RETURN ;
	END

	INSERT INTO ShippingRuleStateList
	(
		numCountryID,				
		numStateID,
		numRuleID,
		numDomainID,
		vcZipPostal
	)
	SELECT 
		T1.numCountryID
		,T1.numStateID
		,T1.numRuleID
		,T1.numDomainID
		,T1.vcZipPostal
	FROM
		@TEMP T1
	LEFT JOIN
		ShippingRuleStateList SRSL
	ON
		SRSL.numRuleID = @numRuleID
		AND T1.numRuleID = SRSL.numRuleID
		AND T1.numCountryID = SRSL.numCountryID
		AND T1.numStateID = SRSL.numStateID
		AND ISNULL(T1.vcZipPostal,'') = ISNULL(SRSL.vcZipPostal,'')
	WHERE
		SRSL.numRuleStateID IS NULL							
					
END

