GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteCommissionRules')
DROP PROCEDURE USP_DeleteCommissionRules
GO
CREATE PROCEDURE [dbo].[USP_DeleteCommissionRules]
    @numComRuleID NUMERIC,
    @numDomainID NUMERIC
AS 
    DELETE  FROM CommissionRules
    WHERE   [numComRuleID] = @numComRuleID
            AND numDomainID = @numDomainID