GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUOMConversion')
DROP PROCEDURE USP_GetUOMConversion
GO
CREATE PROCEDURE [dbo].[USP_GetUOMConversion]                                                                          
 @numDomainID as numeric(9)                  
AS       
      
   SELECT c.numUOMConv,c.numUOM1,c.decConv1,c.numUOM2,c.decConv2
   FROM UOMConversion c INNER JOIN UOM u ON c.numUOM1=u.numUOMId INNER JOIN Domain d ON u.numDomainID=d.numDomainID
        WHERE c.numDomainID=@numDomainID AND u.numDomainID=@numDomainID AND d.numDomainID=@numDomainID AND 
        u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)
         
