GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_GetDomainDetailsForPriceMargin')
DROP PROCEDURE USP_GetDomainDetailsForPriceMargin
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetailsForPriceMargin]                                          
	@numDomainID AS NUMERIC(9) = 0,
	@numListItemID AS NUMERIC(9) = 0                                           
AS  

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID

IF @numListItemID > 0
BEGIN
	SELECT     
		ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
		ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
		ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
		ISNULL(AP.bitCostApproval,0) bitCostApproval,
		ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval,
		ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated,
		ISNULL(numListItemID,0) AS numListItemID,
		@listIds AS vcUnitPriceApprover
	FROM Domain D  
		LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
	WHERE (D.numDomainID=@numDomainID OR @numDomainID=-1)
		AND numListItemID = @numListItemID 
END
ELSE
BEGIN
	SELECT     
		ISNULL(AP.numAbovePercent,0) AS numAbovePercent,
		ISNULL(AP.numBelowPercent,0) AS numBelowPercent,
		ISNULL(AP.numBelowPriceField,0) AS numBelowPriceField,
		ISNULL(AP.bitCostApproval,0) bitCostApproval,
		ISNULL(AP.bitListPriceApproval,0) bitListPriceApproval,
		ISNULL(AP.bitMarginPriceViolated,0) bitMarginPriceViolated,
		ISNULL(numListItemID,0) AS numListItemID,
		@listIds AS vcUnitPriceApprover
	FROM Domain D  
		LEFT JOIN ApprovalProcessItemsClassification AP ON AP.numDomainID = D.numDomainID
	WHERE (D.numDomainID=@numDomainID OR @numDomainID=-1)
END

