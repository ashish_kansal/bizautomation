
GO
/****** Object:  StoredProcedure [dbo].[USP_ManagerBizDocStatusApprove]    Script Date: 02/11/2010 23:29:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from BizDocStatusApprove

create procedure [dbo].[USP_ManagerBizDocStatusApprove]
@numBizDocStatusAppID numeric(18,0),
@numDomainID int,
@numBizDocTypeID numeric(18,0),
@numBizDocStatusID numeric(18,0),
@numEmployeeID numeric(18,0),
@numActionTypeID numeric(18,0)

as

INSERT INTO BizDocStatusApprove SELECT @numDomainID,@numBizDocTypeID,@numBizDocStatusID,@numEmployeeID,@numActionTypeID
select @@IDENTITY