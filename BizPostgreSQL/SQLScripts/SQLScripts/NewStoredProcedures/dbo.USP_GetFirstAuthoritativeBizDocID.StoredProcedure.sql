
GO
/****** Object:  StoredProcedure [dbo].[USP_GetFirstAuthoritativeBizDocID]    Script Date: 09/01/2009 01:19:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetFirstAuthoritativeBizDocID')
DROP PROCEDURE USP_GetFirstAuthoritativeBizDocID
GO
CREATE PROCEDURE [dbo].[USP_GetFirstAuthoritativeBizDocID](
               @numOppId AS NUMERIC(9)  = 0)
AS
  BEGIN
    DECLARE  @numBizDocID NUMERIC(9)
    SELECT TOP 1 @numBizDocID = ISNULL([numOppBizDocsId],0)
    FROM   [OpportunityBizDocs]
    WHERE  [numOppId] = @numOppId
           AND ISNULL([bitAuthoritativeBizDocs],0) = 1
    SELECT ISNULL(@numBizDocID,0)
  END
