GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetWarehouseBasedonAddress')
DROP PROCEDURE USP_Item_GetWarehouseBasedonAddress
GO
CREATE PROCEDURE [dbo].[USP_Item_GetWarehouseBasedonAddress]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numAddressID NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numTempWarehouseID NUMERIC(18,0) = 0

	IF (SELECT COUNT(*) FROM Warehouses WHERE numDomainID=@numDomainID AND numAddressID=@numAddressID) > 0
	BEGIN
		IF EXISTS (SELECT WI.numWareHouseItemID FROM Warehouses W INNER JOIN WareHouseItems WI ON W.numWareHouseID=WI.numWareHouseID WHERE W.numDomainID=@numDomainID AND W.numAddressID=@numAddressID AND numWareHouseItemID=@numWarehouseItemID)
		BEGIN
			SELECT @numTempWarehouseID = WI.numWareHouseItemID FROM Warehouses W INNER JOIN WareHouseItems WI ON W.numWareHouseID=WI.numWareHouseID WHERE W.numDomainID=@numDomainID AND W.numAddressID=@numAddressID AND numWareHouseItemID=@numWarehouseItemID
		END
		ELSE
		BEGIN
			SELECT TOP 1
				@numTempWarehouseID = WI.numWareHouseItemID
			FROM
				Warehouses W 
			INNER JOIN
				WareHouseItems WI 
			ON 
				W.numWareHouseID=WI.numWareHouseID 
			WHERE 
				W.numDomainID=@numDomainID 
				AND W.numAddressID=@numAddressID
			ORDER BY
				ISNULL(numWLocationID,0) DESC
		END
	END

	SELECT @numTempWarehouseID AS numWarehouseItemID
END