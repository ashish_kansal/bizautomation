GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DuplicateCheckNo')
DROP PROCEDURE USP_DuplicateCheckNo
GO
CREATE PROCEDURE USP_DuplicateCheckNo
@numDomainId NUMERIC,
@numCheckNo INT
AS 
BEGIN

	SELECT COUNT(*) FROM [CheckDetails] WHERE [numDomainId]=@numDomainID AND [numCheckNo] = @numCheckNo
		
END