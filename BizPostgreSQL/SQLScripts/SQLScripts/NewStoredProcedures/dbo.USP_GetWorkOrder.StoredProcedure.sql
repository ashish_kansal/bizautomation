/****** Object:  StoredProcedure [dbo].[USP_GetWorkOrder]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkOrder')
DROP PROCEDURE USP_GetWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_GetWorkOrder]                             
@numDomainID NUMERIC(18,0),
@ClientTimeZoneOffset  INT,
@numWOStatus NUMERIC(18,0),
@SortExpression AS VARCHAR(50),
@CurrentPage int,                                                                            
@PageSize INT,
@numUserCntID AS NUMERIC(9),
@vcWOId  AS VARCHAR(4000),
@tintFilter TINYINT = 0,
@numOppID NUMERIC(18,0),
@numOppItemID NUMERIC(18,0)                                                                
AS                            
BEGIN
	SET @vcWOId=ISNULL(@vcWOId,'')
	
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID   
    
	DECLARE @TEMP TABLE
	(
		numID INT IDENTITY(1,1),ItemLevel INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
		vcItemName VARCHAR(500), numQty FLOAT, numWarehouseItemID NUMERIC(18,0), vcWarehouse VARCHAR(200), numOnHand FLOAT,
		numOnOrder FLOAT, numAllocation FLOAT, numBackOrder FLOAT, vcInstruction VARCHAR(2000),
		numAssignedTo NUMERIC(18,0), vcAssignedTo VARCHAR(500), numCreatedBy NUMERIC(18,0), vcCreated VARCHAR(500),
		bintCompliationDate VARCHAR(50), numWOStatus NUMERIC(18,0), numOppID NUMERIC(18,0), vcOppName VARCHAR(1000),
		numPOID NUMERIC(18,0), vcPOName VARCHAR(1000), bitWorkOrder BIT, bitReadyToBuild BIT
	)

	DECLARE @TEMPWO TABLE
	(
		ID INT IDENTITY(1,1)
		,numWOID NUMERIC(18,0)
	)

	DECLARE @TEMPWORKORDER TABLE
	(
		ItemLevel INT, RowNumber INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
		numOppID NUMERIC(18,0), numWarehouseItemID NUMERIC(18,0), numQty FLOAT, numWOStatus NUMERIC(18,0), vcInstruction VARCHAR(2000),
		numAssignedTo NUMERIC(18,0), bintCreatedDate DATETIME, bintCompliationDate DATETIME, numCreatedBy NUMERIC(18,0),  
		bitWorkOrder BIT, bitReadyToBuild BIT
	) 

	INSERT INTO 
		@TEMPWORKORDER
	SELECT 
		1 AS ItemLevel,
		ROW_NUMBER() OVER(ORDER BY numWOId) AS RowNumber,
		CAST(0 AS NUMERIC(18,0)) AS numParentId,
		numWOId,
		CAST(CONCAT('#',ISNULL(numWOId,'0'),'#') AS VARCHAR(1000)) AS ID,
		numItemCode,
		numOppId,
		numWareHouseItemId,
		CAST(numQtyItemsReq AS FLOAT),
		numWOStatus,
		vcInstruction,
		numAssignedTo,
		bintCreatedDate,
		bintCompliationDate,
		numCreatedBy,
		1 AS bitWorkOrder,
		(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS bitReadyToBuild
	FROM 
		WorkOrder
	WHERE  
		numDomainID=@numDomainID
		AND (numOppID=@numOppID OR ISNULL(@numOppID,0)=0)
		AND (numOppItemID=@numOppItemID OR ISNULL(@numOppItemID,0)=0)
		AND 1=(CASE WHEN ISNULL(@numWOStatus,0) = 0 THEN CASE WHEN WorkOrder.numWOStatus <> 23184 THEN 1 ELSE 0 END ELSE CASE WHEN WorkOrder.numWOStatus = @numWOStatus THEN 1 ELSE 0 END END)
		AND 1=(Case when @numUserCntID>0 then case when isnull(numAssignedTo,0)=@numUserCntID then 1 else 0 end else 1 end)
		AND 1=(Case when len(@vcWOId)>0 then Case when numWOId in (SELECT * from dbo.Split(@vcWOId,',')) then 1 else 0 end else 1 end)
		AND (ISNULL(numParentWOID,0) = 0 OR numParentWOID NOT IN (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID))
	ORDER BY
		bintCreatedDate DESC,
		numWOId ASC

	;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,ID,numItemKitID,numQtyItemsReq,numWarehouseItemID,vcInstruction,numAssignedTo,
						bintCreatedDate,numCreatedBy,bintCompliationDate,numWOStatus,numOppID,bitWorkOrder,bitReadyToBuild) AS
	(
		SELECT
			ItemLevel,
			numParentWOID,
			numWOId,
			ID,
			numItemCode,
			numQty,
			numWareHouseItemId,
			vcInstruction,
			numAssignedTo,
			bintCreatedDate,
			numCreatedBy,
			bintCompliationDate,
			numWOStatus,
			numOppId,
			1 AS bitWorkOrder,
			bitReadyToBuild
		FROM
			@TEMPWORKORDER t
		UNION ALL
		SELECT 
			c.ItemLevel+2,
			c.numWOID,
			WorkOrder.numWOId,
			CAST(CONCAT(c.ID,'-','#',ISNULL(WorkOrder.numWOId,'0'),'#') AS VARCHAR(1000)),
			WorkOrder.numItemCode,
			CAST(WorkOrder.numQtyItemsReq AS FLOAT),
			WorkOrder.numWareHouseItemId,
			CAST(WorkOrder.vcInstruction AS VARCHAR(2000)),
			WorkOrder.numAssignedTo,
			WorkOrder.bintCreatedDate,
			WorkOrder.numCreatedBy,
			WorkOrder.bintCompliationDate,
			WorkOrder.numWOStatus,
			WorkOrder.numOppId,
			1 AS bitWorkOrder,
			CASt((CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS BIT) AS bitReadyToBuild
		FROM 
			WorkOrder
		INNER JOIN 
			CTEWorkOrder c 
		ON 
			WorkOrder.numParentWOID = c.numWOID
	)

	INSERT 
		@TEMP
	SELECT
		ItemLevel,
		numParentWOID,
		numWOID,
		ID,
		numItemCode,
		vcItemName,
		CTEWorkOrder.numQtyItemsReq,
		CTEWorkOrder.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numOnOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		CTEWorkOrder.numAssignedTo,
		dbo.fn_GetContactName(isnull(CTEWorkOrder.numAssignedTo,0)) AS vcAssignedTo,
		CTEWorkOrder.numCreatedBy,
		CAST(ISNULL(dbo.fn_GetContactName(CTEWorkOrder.numCreatedBy), '&nbsp;&nbsp;-') + ',' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,CTEWorkOrder.bintCreatedDate)) AS VARCHAR(500)) ,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
		numWOStatus,
		CTEWorkOrder.numOppID,
		OpportunityMaster.vcPOppName,
		NULL,
		NULL,
		bitWorkOrder,
		bitReadyToBuild
	FROM 
		CTEWorkOrder
	INNER JOIN 
		Item
	ON
		CTEWorkOrder.numItemKitID = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN 
		OpportunityMaster
	ON
		CTEWorkOrder.numOppID = OpportunityMaster.numOppId

	INSERT INTO 
		@TEMP
	SELECT
		t1.ItemLevel + 1,
		t1.numWOID,
		NULL,
		CONCAT(t1.ID,'-#0#'),
		WorkOrderDetails.numChildItemID,
		Item.vcItemName,
		WorkOrderDetails.numQtyItemsReq,
		WorkOrderDetails.numWarehouseItemID,
		Warehouses.vcWareHouse,
		WareHouseItems.numOnHand,
		WareHouseItems.numOnOrder,
		WareHouseItems.numAllocation,
		WareHouseItems.numBackOrder,
		vcInstruction,
		NULL,
		NULL,
		NULL,
		NULL,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
		numWOStatus,
		NULL,
		NULL,
		OpportunityMaster.numOppId,
		OpportunityMaster.vcPOppName,
		0 AS bitWorkOrder,
		CASE 
			WHEN Item.charItemType='P' AND ISNULL(t1.bitReadyToBuild,0) = 0
			THEN
				CASE 
					WHEN (CASE WHEN ISNULL(t1.numOppId,0) > 0 THEN 1 ELSE 0 END)=1 AND @tintCommitAllocation=2 
					THEN (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numOnHand THEN 0 ELSE 1 END)
					ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END) 
				END
			ELSE 1
		END
	FROM
		WorkOrderDetails
	INNER JOIN
		@TEMP t1
	ON
		WorkOrderDetails.numWOId = t1.numWOID
	INNER JOIN 
		Item
	ON
		WorkOrderDetails.numChildItemID = Item.numItemCode
		AND 1= (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.bitWorkOrder=1 AND TInner.numParentWOID=t1.numWOID AND TInner.numItemCode=Item.numItemCode) > 0 THEN 0 ELSE 1 END)
	LEFT JOIN
		WareHouseItems
	ON
		WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		OpportunityMaster 
	ON
		WorkOrderDetails.numPOID = OpportunityMaster.numOppId

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempWOID NUMERIC(18,0)
	INSERT INTO @TEMPWO (numWOID) SELECT numWOID FROM @TEMP WHERE bitWorkOrder=1 ORDER BY ItemLevel DESC

	SELECT @iCount=COUNT(*) FROM @TEMPWO

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempWOID=numWOID FROM @TEMPWO WHERE ID=@i

		UPDATE
			T1
		SET
			bitReadyToBuild = (CASE WHEN (SELECT COUNT(*) FROM @TEMP TInner WHERE TInner.numParentWOID=T1.numWOID AND bitReadyToBuild=0) > 0 THEN 0 ELSE 1 END)
		FROM
			@TEMP T1
		WHERE
			numWOID=@numTempWOID

		SET @i = @i + 1
	END

	If @tintFilter = 2 --Buildable Orders
	BEGIN
		DELETE FROM @TEMP WHERE 1 = (CASE WHEN bitReadyToBuild=1 THEN 0 ELSE 1 END) AND bitWorkOrder=1
	END
	ELSE IF @tintFilter = 3 --Un-Buildable Orders
	BEGIN
		DELETE FROM @TEMP WHERE 1 = (CASE WHEN bitReadyToBuild=1 THEN 1 ELSE 0 END) AND bitWorkOrder=1
	END

	DECLARE @TEMPPagingID TABLE
	(
		ID INT
		,numWOID NUMERIC(18,0)
		,numParentWOID NUMERIC(18,0)
	)
	INSERT INTO @TEMPPagingID
	(
		ID
		,numWOID
		,numParentWOID
	)
	SELECT
		numID
		,numWOID
		,numParentWOID
	FROM	
		@TEMP 
	WHERE 
		ISNULL(numParentWOID,0) = 0
	ORDER BY
		numWOID
	OFFSET
		(@CurrentPage - 1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY

	;WITH CTEID (numID,numWOID,numParentWOID) AS
	(
		SELECT
			ID
			,numWOID
			,numParentWOID
		FROM	
			@TEMPPagingID 
		UNION ALL
		SELECT
			T.numID
			,T.numWOID
			,T.numParentWOID
		FROM
			@TEMP T
		INNER JOIN
			CTEID C
		ON
			T.numParentWOID = C.numWOID
		WHERE
			ISNULL(T.numParentWOID,0) > 0
	)

	SELECT * FROM @TEMP T1 INNER JOIN CTEID ON T1.numID = CTEID.numID ORDER BY T1.numParentWOID,T1.ItemLevel

	DECLARE @TOTALROWCOUNT AS NUMERIC(18,0)

	SELECT @TOTALROWCOUNT=COUNT(*) FROM @TEMP WHERE ISNULL(numParentWOID,0)=0

	SELECT @TOTALROWCOUNT AS TotalRowCount 
END
GO