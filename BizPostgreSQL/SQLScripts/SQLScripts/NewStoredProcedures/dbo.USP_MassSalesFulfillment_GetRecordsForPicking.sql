GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecordsForPicking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecordsForPicking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecordsForPicking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrder BIT
	,@numBatchID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(142,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=142 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(142,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=142 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END


	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	IF ISNULL(@numBatchID,0) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			numOppID
			,ISNULL(numOppItemID,0)
		FROM
			MassSalesFulfillmentBatchOrders
		WHERE
			numBatchID=@numBatchID
	END
	ELSE
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0)) numOppID
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC(18,0)) numOppItemID
		FROM
			dbo.SplitString(@vcSelectedRecords,',')
	END

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWareHouseItemID NUMERIC(18,0)
		,vcWarehouse VARCHAR(300)
		,vcLocation VARCHAR(300)
		,vcKitChildItems VARCHAR(MAX)
		,vcKitChildKitItems VARCHAR(MAX)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,bitSerial BIT
		,bitLot BIT
		,bitKit BIT
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcInclusionDetails VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyPicked FLOAT
		,numQtyRequiredKit FLOAT
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID 
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityItems.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,ISNULL(STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID) FROM OpportunityKitItems OKI WHERE OKI.numOppItemID=1 ORDER BY OKI.numChildItemID FOR XML PATH('')),1,1,''),'') AS vcKitChildItems
		,ISNULL(STUFF((SELECT CONCAT(', ',OpportunityItems.numItemCode,'-',OKI.numChildItemID,'-',OKCI.numItemID) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID = OKI.numOppChildItemID  WHERE OKCI.numOppItemID=1 ORDER BY OKI.numChildItemID,OKCI.numItemID FOR XML PATH('')),1,1,''),'') vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,ISNULL(Item.bitKitParent,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) numRemainingQty
		,0 numQtyPicked
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		Item 
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID
		,numOppChildItemID
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
		,numQtyRequiredKit
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityKitItems.numOppChildItemID
		,Item.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,'' AS vcKitChildItems
		,'' vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,ISNULL(Item.bitKitParent,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,(ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)) * ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0) numRemainingQty
		,0 numQtyPicked
		,ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0)
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		OpportunityKitItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityKitItems.numOppItemID = T1.numOppItemID OR OpportunityKitItems.numOppItemID = OpportunityItems.numoppitemtCode)
	INNER JOIN
		Item 
	ON
		OpportunityKitItems.numChildItemID = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityKitItems.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
		AND ISNULL(Item.bitKitParent,0) = 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	INSERT INTO @TEMPItems
	(	
		ID
		,numOppID
		,numOppItemID
		,numOppChildItemID
		,numOppKitChildItemID
		,numItemCode
		,numWarehouseID
		,numWareHouseItemID
		,vcWarehouse 
		,vcLocation
		,vcKitChildItems
		,vcKitChildKitItems
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,bitSerial
		,bitLot
		,bitKit
		,vcSKU
		,vcPathForTImage
		,vcInclusionDetails
		,numRemainingQty
		,numQtyPicked
		,numQtyRequiredKit
	)
	SELECT 
		T1.ID
		,OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtCode
		,OpportunityKitChildItems.numOppChildItemID
		,OpportunityKitChildItems.numOppKitChildItemID
		,Item.numItemCode
		,WareHouseItems.numWareHouseID
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,WarehouseLocation.vcLocation
		,'' AS vcKitChildItems
		,'' vcKitChildKitItems
		,OpportunityMaster.vcPoppName
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,ISNULL(Item.bitSerialized,0)
		,ISNULL(Item.bitLotNo,0)
		,ISNULL(Item.bitKitParent,0)
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcInclusionDetail,'') AS vcInclusionDetails
		,(ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)) * ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0) * ISNULL(OpportunityKitChildItems.numQtyItemsReq_Orig,0) numRemainingQty
		,0 numQtyPicked
		,ISNULL(OpportunityKitItems.numQtyItemsReq_Orig,0) * ISNULL(OpportunityKitChildItems.numQtyItemsReq_Orig,0)
	FROM
		@TEMP T1
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	INNER JOIN
		OpportunityKitChildItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityKitChildItems.numOppItemID = T1.numOppItemID OR OpportunityKitChildItems.numOppItemID = OpportunityItems.numoppitemtCode)
	INNER JOIN
		OpportunityKitItems
	ON
		OpportunityKitChildItems.numOppChildItemID = OpportunityKitItems.numOppChildItemID
	INNER JOIN
		Item 
	ON
		OpportunityKitChildItems.numItemID = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	INNER JOIN
		WareHouseItems
	ON
		OpportunityKitChildItems.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0
		AND ISNULL(Item.bitKitParent,0) = 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	IF ISNULL(@bitGroupByOrder,0) = 1
	BEGIN
		SELECT
			TEMP.numOppId
			,(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END) AS numOppItemID
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,STUFF((SELECT 
						CONCAT(',',T1.numOppItemID,'(',T1.numRemainingQty,')') 
					FROM 
						@TEMPItems T1 
					WHERE 
						T1.numOppId = TEMP.numOppId
						AND T1.numItemCode = TEMP.numItemCode
						AND T1.numWareHouseID = TEMP.numWareHouseID
						AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
						AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
					FOR XML PATH('')),1,1,'') vcOppItemIDs
			,vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,bitSerial
			,bitLot
			,bitKit
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode,0) vcAttributes
			,vcPathForTImage
			,MIN(vcInclusionDetails) vcInclusionDetails
			,SUM(TEMP.numRemainingQty) numRemainingQty
			,0 numQtyPicked
			,numQtyRequiredKit
			,(CASE 
				WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
				THEN CONCAT('[',STUFF((SELECT 
											CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
										FROM 
											WareHouseItems WIInner
										LEFT JOIN
											WarehouseLocation WL
										ON
											WIInner.numWLocationID = WL.numWLocationID
										WHERE 
											WIInner.numDomainID=@numDomainID 
											AND WIInner.numItemID=TEMP.numItemCode 
											AND WIInner.numWareHouseID = TEMP.numWareHouseID
											AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
										ORDER BY
											WL.vcLocation
										FOR XML PATH('')),1,1,''),']')
				ELSE CONCAT('[',STUFF((SELECT 
									CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
								FROM 
									WareHouseItems WIInner
								LEFT JOIN
									WarehouseLocation WL
								ON
									WIInner.numWLocationID = WL.numWLocationID
								WHERE 
									WIInner.numDomainID=@numDomainID 
									AND WIInner.numItemID=TEMP.numItemCode
									AND WIInner.numWareHouseID = TEMP.numWareHouseID
								FOR XML PATH('')),1,1,''),']')
			END)  vcWarehouseLocations
		FROM
			@TEMPItems TEMP
		GROUP BY
			TEMP.numOppId
			,(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END)
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWareHouseItemID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,TEMP.vcPoppName
			,TEMP.vcPathForTImage
			,TEMP.vcItemName
			,TEMP.bitSerial
			,TEMP.bitLot
			,TEMP.bitKit
			,TEMP.vcItemDesc
			,TEMP.vcSKU
			,TEMP.numBarCodeId
			,TEMP.vcWarehouse
			,TEMP.vcLocation
			,TEMP.numQtyRequiredKit
		ORDER BY
			MIN(ID)
			,numOppItemID
			,numOppChildItemID
			,numOppKitChildItemID
	END
	ELSE
	BEGIN
		SELECT
			0 AS numOppId
			,(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END) AS numOppItemID
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,STUFF((SELECT 
						CONCAT(', ',T1.numOppItemID,'(',T1.numRemainingQty,')') 
					FROM 
						@TEMPItems T1 
					WHERE 
						T1.numItemCode = TEMP.numItemCode
						AND T1.numWareHouseID = TEMP.numWareHouseID
						AND ISNULL(T1.vcKitChildItems,'') = ISNULL(TEMP.vcKitChildItems,'')
						AND ISNULL(T1.vcKitChildKitItems,'') = ISNULL(TEMP.vcKitChildKitItems ,'')
					FOR XML PATH('')),1,1,'') vcOppItemIDs
			,'' AS vcPoppName
			,numBarCodeId
			,CONCAT(TEMP.vcWarehouse, (CASE WHEN LEN(ISNULL(TEMP.vcLocation,'')) > 0 THEN CONCAT(' (',TEMP.vcLocation,')') ELSE '' END)) vcLocation
			,vcItemName
			,vcItemDesc
			,bitSerial
			,bitLot
			,bitKit
			,vcSKU
			,dbo.fn_GetItemAttributes(@numDomainID,TEMP.numItemCode,0) vcAttributes
			,vcPathForTImage
			,MIN(vcInclusionDetails) vcInclusionDetails
			,SUM(TEMP.numRemainingQty) numRemainingQty
			,0 numQtyPicked
			,numQtyRequiredKit
			,(CASE 
				WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=TEMP.numItemCode AND WIInner.numWareHouseID = TEMP.numWareHouseID)
				THEN CONCAT('[',STUFF((SELECT 
											CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":"',WL.vcLocation,'"}') 
										FROM 
											WareHouseItems WIInner
										LEFT JOIN
											WarehouseLocation WL
										ON
											WIInner.numWLocationID = WL.numWLocationID
										WHERE 
											WIInner.numDomainID=@numDomainID 
											AND WIInner.numItemID=TEMP.numItemCode 
											AND WIInner.numWareHouseID = TEMP.numWareHouseID
											AND (ISNULL(WL.numWLocationID,0) > 0 OR WIInner.numWareHouseItemID=TEMP.numWareHouseItemID)
										ORDER BY
											WL.vcLocation
										FOR XML PATH('')),1,1,''),']')
				ELSE CONCAT('[',STUFF((SELECT 
									CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "WarehouseLocationID":',ISNULL(WL.numWLocationID,0),', "Available":',(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)),', "Location":""}')
								FROM 
									WareHouseItems WIInner
								LEFT JOIN
									WarehouseLocation WL
								ON
									WIInner.numWLocationID = WL.numWLocationID
								WHERE 
									WIInner.numDomainID=@numDomainID 
									AND WIInner.numItemID=TEMP.numItemCode
									AND WIInner.numWareHouseID = TEMP.numWareHouseID
								FOR XML PATH('')),1,1,''),']')
			END)  vcWarehouseLocations
		FROM
			@TEMPItems TEMP
		GROUP BY
			(CASE WHEN TEMP.bitKit = 1 OR ISNULL(TEMP.numOppChildItemID,0) > 0 OR ISNULL(TEMP.numOppKitChildItemID,0) > 0 THEN TEMP.numOppItemID ELSE 0 END)
			,TEMP.numOppChildItemID
			,TEMP.numOppKitChildItemID
			,TEMP.numItemCode
			,TEMP.numWareHouseID
			,TEMP.numWareHouseItemID
			,TEMP.vcKitChildItems
			,TEMP.vcKitChildKitItems
			,TEMP.vcPathForTImage
			,TEMP.vcItemName
			,TEMP.vcItemDesc
			,TEMP.bitSerial
			,TEMP.bitLot
			,TEMP.vcSKU
			,TEMP.bitKit
			,TEMP.numBarCodeId
			,TEMP.vcWarehouse
			,TEMP.vcLocation
			,TEMP.numQtyRequiredKit
		ORDER BY
			MIN(ID)
			,numOppItemID
			,numOppChildItemID
			,numOppKitChildItemID
	END
	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO