
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetDashboardAllowedReports')
DROP PROCEDURE Usp_GetDashboardAllowedReports
GO
CREATE PROCEDURE [dbo].[Usp_GetDashboardAllowedReports]        
@numDomainId as numeric(9) ,
@numGroupId as numeric(9),
@tintMode AS TINYINT=0
as                                                 
      
IF @tintMode=0 --All Custom Reports
BEGIN
	SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName,  
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
	JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	LEFT join ReportDashboardAllowedReports DAR on RLM.numReportID=DAR.numReportID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=0
	where RLM.numDomainID=@numDomainID
	order by RLM.numReportID
END    
ELSE IF @tintMode=1 --Only Allowed Custom Reports
BEGIN
	SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName,  
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
	JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	join ReportDashboardAllowedReports DAR on RLM.numReportID=DAR.numReportID 
	where RLM.numDomainID=@numDomainID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=0
	order by RLM.numReportID 
END   
ELSE IF @tintMode=2 --All KPI Groups Reports
BEGIN
	SELECT KPI.numReportKPIGroupID,KPI.vcKPIGroupName,KPI.vcKPIGroupDescription,
	CASE KPI.tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType,
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportKPIGroupListMaster KPI                                                 
	LEFT join ReportDashboardAllowedReports DAR on KPI.numReportKPIGroupID=DAR.numReportID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=1
	where KPI.numDomainID=@numDomainID
	order by KPI.numReportKPIGroupID
END    
ELSE IF @tintMode=3 --Only Allowed KPI Groups Reports
BEGIN
	SELECT KPI.numReportKPIGroupID,KPI.vcKPIGroupName,KPI.vcKPIGroupDescription,
	CASE KPI.tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType,
	CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	from ReportKPIGroupListMaster KPI                                                  
	join ReportDashboardAllowedReports DAR on KPI.numReportKPIGroupID=DAR.numReportID 
	where KPI.numDomainID=@numDomainID AND DAR.numDomainID=@numDomainId AND DAR.numGrpID =  @numGroupId AND DAR.tintReportCategory=1
	order by KPI.numReportKPIGroupID 
END   
ELSE IF @tintMode=4 --Default Reports
BEGIN
	SELECT 
		RLM.numReportID
		,RLM.vcReportName
		,RLM.intDefaultReportID
	FROM 
		ReportListMaster RLM 
	INNER JOIN 
		ReportDashboardAllowedReports DAR 
	ON
		RLM.intDefaultReportID=DAR.numReportID 
		AND DAR.numDomainID=@numDomainId 
		AND DAR.numGrpID =  @numGroupId 
		AND DAR.tintReportCategory=2
	WHERE 
		ISNULL(RLM.bitDefault,0) = 1
	ORDER BY 
		RLM.vcReportName
END
ELSE IF @tintMode=5 -- Default Reports With Permission
BEGIN
	SELECT 
		RLM.intDefaultReportID
		,RLM.vcReportName
		,RLM.vcReportDescription
		,CASE WHEN ISNULL(DAR.numReportID,0)=0 THEN convert(bit,'false') ELSE convert(bit,'true') END AS bitallowed
	FROM 
		ReportListMaster RLM 
	LEFT JOIN 
		ReportDashboardAllowedReports DAR 
	ON
		RLM.intDefaultReportID=DAR.numReportID 
		AND DAR.numDomainID=@numDomainId 
		AND DAR.numGrpID =  @numGroupId 
		AND DAR.tintReportCategory=2
	WHERE 
		ISNULL(RLM.numDomainID,0)=0
		AND ISNULL(RLM.bitDefault,0) = 1
	ORDER BY 
		RLM.vcReportName
END   
GO
