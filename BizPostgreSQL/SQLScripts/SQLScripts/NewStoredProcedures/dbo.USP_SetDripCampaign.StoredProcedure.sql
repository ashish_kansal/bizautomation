-- exec usp_SetDripCampaign @vcContactIDList='27,10366',@numECampaignID=-1,@numUserCntID=1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SetDripCampaign')
DROP PROCEDURE USP_SetDripCampaign
GO
CREATE PROCEDURE USP_SetDripCampaign
    @vcContactIDList VARCHAR(8000),
    @numECampaignID NUMERIC,
    @numUserCntID NUMERIC
AS 
    BEGIN
	
		IF LEN( @vcContactIDList ) = 0 
		 RETURN
	
	
        UPDATE  [AdditionalContactsInformation]
        SET     [numECampaignID] = @numECampaignID
        WHERE   [numContactId] IN (
                SELECT  Id
                FROM    dbo.[SplitIDs](@vcContactIDList, ',') )
                SELECT  Id
                FROM    dbo.[SplitIDs](@vcContactIDList, ',')
--		IF @numECampaignID >0 
--		BEGIN
--			
--			UPDATE ConECampaign SET bitEngaged = 1 WHERE num
--		END
                
        CREATE TABLE #temp ( ContactID NUMERIC )
        INSERT  INTO #temp
                SELECT  Id
                FROM    dbo.[SplitIDs](@vcContactIDList, ',')
        
        DECLARE @numContactID NUMERIC
        DECLARE @Date AS DATETIME 
        IF @numECampaignID = -1 
            SET @numECampaignID = 0
            
            
        SELECT TOP 1 @numContactID = ContactID FROM    [#temp]
        WHILE @numContactID <> 0
            BEGIN
        
		/*Added by chintan BugID-262*/
                SELECT  @Date = dbo.[GetUTCDateWithoutTime]()
                EXEC [USP_ManageConEmailCampaign] @numConEmailCampID = 0, --  numeric(9, 0)
                    @numContactID = @numContactID, --  numeric(9, 0)
                    @numECampaignID = @numECampaignID, --  numeric(9, 0)
                    @intStartDate = @Date, --  datetime
                    @bitEngaged = 1, --  bit
                    @numUserCntID = @numUserCntID --  numeric(9, 0)
            
                DELETE  FROM [#temp] WHERE   [ContactID] = @numContactID
                SELECT TOP 1 @numContactID = ContactID FROM    [#temp]
					IF @@ROWCOUNT = 0 
						SET @numContactID = 0
            
            END
       
        DROP TABLE [#temp]         
    END