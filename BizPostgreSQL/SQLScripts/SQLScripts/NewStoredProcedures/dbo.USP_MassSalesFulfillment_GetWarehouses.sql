GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetWarehouses')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetWarehouses
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetWarehouses]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		numWareHouseID
		,ISNULL(vcWareHouse,'') AS vcWareHouse
	FROM 
		Warehouses 
	WHERE 
		numDomainID=@numDomainID 
	ORDER BY
		vcWareHouse
END
GO