GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentWM_Delete')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentWM_Delete
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentWM_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@numID NUMERIC(18,0)
)
AS 
BEGIN
	DELETE FROM MassSalesFulfillmentWM WHERE numDomainID=@numDomainID AND ID=@numID
END
GO