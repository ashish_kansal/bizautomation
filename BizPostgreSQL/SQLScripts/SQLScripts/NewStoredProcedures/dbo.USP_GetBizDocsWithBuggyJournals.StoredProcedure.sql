GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocsWithBuggyJournals')
DROP PROCEDURE USP_GetBizDocsWithBuggyJournals
GO
CREATE PROCEDURE  [dbo].USP_GetBizDocsWithBuggyJournals                                                                                                                                                
AS
BEGIN
	
	
	
	SELECT OM.tintOppType,datEntry_Date,numJournal_Id,GJH.numOppBizDocsId,GJH.numOppId,GJH.numDomainId,
	(SELECT numShipVia FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId=GJH.numOppBizDocsId) numShipVia,
	 OM.numDivisionID,0 AS numCreatedBy
	 FROM  dbo.General_Journal_Header GJH INNER JOIN 
	(SELECT SUM(numDebitAmt) DebitSum,SUM(numCreditAmt) CreditSum,numJournalId,numDomainId FROM dbo.General_Journal_Details GROUP BY numJournalId,numDomainId
	HAVING SUM(numDebitAmt)<>SUM(numCreditAmt) ) X 
	ON X.numJournalId = GJH.numJournal_Id 
	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
	
--	UNION
--	
--		SELECT /*GJH.numAmount,OBD.monDealAmount,*/OM.tintOppType,datEntry_Date,numJournal_Id,GJH.numOppBizDocsId,GJH.numOppId,GJH.numDomainId,
--	(SELECT numShipVia FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId=GJH.numOppBizDocsId) numShipVia,
--	 OM.numDivisionID,0 AS numCreatedBy
--	 FROM  dbo.General_Journal_Header GJH 
--	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
--	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
--	WHERE GJH.numAmount!=OBD.monDealAmount AND OM.numDomainId=153 AND OBD.bitAuthoritativeBizDocs=1 AND OM.tintOppType=2
--and OM.numOppID=57577 


--UNION
--
--	SELECT /*GJH.numAmount,OBD.monDealAmount,*/OM.tintOppType,datEntry_Date,numJournal_Id,GJH.numOppBizDocsId,GJH.numOppId,GJH.numDomainId,
--	(SELECT numShipVia FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId=GJH.numOppBizDocsId) numShipVia,
--	 OM.numDivisionID,0 AS numCreatedBy
--	 FROM  dbo.General_Journal_Header GJH 
--	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
--	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
--	WHERE GJH.numAmount!=OBD.monDealAmount AND OM.numDomainId=155 AND OBD.bitAuthoritativeBizDocs=1 AND OM.tintOppType=1
--and OM.numOppId in (40154,42870)

--	UNION
--	
--	SELECT OM.tintOppType,OBD.dtFromDate,0,OBD.numOppBizDocsId,OBD.numOppId,OM.numDomainId,ISNULL(obd.numShipVia,0),OM.numDivisionId,OM.numCreatedBy FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId = OBD.numOppId
--	WHERE OM.numDomainId=155 AND OBD.bitAuthoritativeBizDocs=1 AND OM.tintOppType=1  
--	AND OBD.numOppBizDocsId NOT IN (SELECT isnull(numOppBizDocsId,0) FROM dbo.General_Journal_Header WHERE numDomainId=155)
--
--	UNION
--	
--		SELECT OM.tintOppType,OBD.dtFromDate,0,OBD.numOppBizDocsId,OBD.numOppId,OM.numDomainId,ISNULL(obd.numShipVia,0),
--	OM.numDivisionId,OM.numCreatedBy FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId = OBD.numOppId
--	WHERE OM.numDomainId=153 AND OBD.bitAuthoritativeBizDocs=1 AND OM.tintOppType=2
--	AND OBD.numOppBizDocsId NOT IN (SELECT numOppBizDocsId FROM dbo.General_Journal_Header GJH 
--join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId WHERE GJH.numDomainId=153 
--and isnull(numBillPaymentID,0)=0 and isnull(numOppBizDocsId,0)>0)
--
--	UNION
--	
--	SELECT /*GJH.numAmount,OBD.monDealAmount,*/OM.tintOppType,datEntry_Date,numJournal_Id,GJH.numOppBizDocsId,GJH.numOppId,GJH.numDomainId,
--	(SELECT numShipVia FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId=GJH.numOppBizDocsId) numShipVia,
--	 OM.numDivisionID,0 AS numCreatedBy
--	 FROM  dbo.General_Journal_Header GJH 
--	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
--	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
--	WHERE GJH.numAmount!=OBD.monDealAmount AND OM.numDomainId=153 AND OBD.bitAuthoritativeBizDocs=1 AND OM.tintOppType=2
--	
--	UNION
--	
--	SELECT /*GJH.numAmount,OBD.monDealAmount,*/OM.tintOppType,datEntry_Date,numJournal_Id,GJH.numOppBizDocsId,GJH.numOppId,GJH.numDomainId,
--	(SELECT numShipVia FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId=GJH.numOppBizDocsId) numShipVia,
--	 OM.numDivisionID,0 AS numCreatedBy
--	 FROM  dbo.General_Journal_Header GJH 
--	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
--	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
--	WHERE GJH.numAmount!=OBD.monDealAmount AND OM.numDomainId=153 AND OBD.bitAuthoritativeBizDocs=1 AND OM.tintOppType=1
--	AND OBD.numOppBizDocsId=40335 AND OBD.numOppId=34563
--	
--	UNION
--	
--	SELECT OM.tintOppType,OBD.dtFromDate,0,OBD.numOppBizDocsId,OBD.numOppId,OM.numDomainId,ISNULL(obd.numShipVia,0),
--	OM.numDivisionId,OM.numCreatedBy FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId = OBD.numOppId
--	WHERE OM.numDomainId=153 AND OBD.bitAuthoritativeBizDocs=1 AND OM.tintOppType=1 and OM.tintOppStatus=1
--	AND OBD.numOppBizDocsId NOT IN (SELECT numOppBizDocsId FROM dbo.General_Journal_Header GJH 
--join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId WHERE GJH.numDomainId=153 
--and isnull(numBillPaymentID,0)=0 and isnull(numOppBizDocsId,0)>0) and OBD.monDealAmount>0
--		
--	UNION
--	
--	SELECT /*GJH.numAmount,OBD.monDealAmount,*/OM.tintOppType,datEntry_Date,numJournal_Id,GJH.numOppBizDocsId,GJH.numOppId,GJH.numDomainId,
--	(SELECT numShipVia FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId=GJH.numOppBizDocsId) numShipVia,
--	 OM.numDivisionID,0 AS numCreatedBy
--	 FROM  dbo.General_Journal_Header GJH 
--	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
--	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
--	WHERE GJH.numAmount!=OBD.monDealAmount AND OM.numDomainId=153 AND OBD.bitAuthoritativeBizDocs=1 AND OM.tintOppType=1
--	AND OBD.numOppBizDocsId=58165 AND OBD.numOppId=49676
	
--		UNION

--		SELECT /*GJH.numAmount,OBD.monDealAmount,*/OM.tintOppType,datEntry_Date,numJournal_Id,GJH.numOppBizDocsId,GJH.numOppId,GJH.numDomainId,
--	(SELECT numShipVia FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId=GJH.numOppBizDocsId) numShipVia,
--	 OM.numDivisionID,0 AS numCreatedBy
--	 FROM  dbo.General_Journal_Header GJH 
--	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
--	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
--	WHERE GJH.numAmount!=OBD.monDealAmount AND OM.numDomainId=153 AND OBD.bitAuthoritativeBizDocs=1 AND OM.tintOppType=1
--	AND OBD.numOppBizDocsId=63917 AND OBD.numOppId=50818
--
--UNION

--	SELECT /*GJH.numAmount,OBD.monDealAmount,*/OM.tintOppType,datEntry_Date,numJournal_Id,GJH.numOppBizDocsId,GJH.numOppId,GJH.numDomainId,
--	(SELECT numShipVia FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId=GJH.numOppBizDocsId) numShipVia,
--	 OM.numDivisionID,0 AS numCreatedBy
--	 FROM  dbo.General_Journal_Header GJH 
--	INNER JOIN dbo.OpportunityBizDocs OBD ON GJH.numOppBizDocsId = OBD.numOppBizDocsId AND GJH.numOppId = OBD.numOppId
--	JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId 
--	WHERE GJH.numAmount!=OBD.monDealAmount AND OM.numDomainId=153 AND OBD.bitAuthoritativeBizDocs=1 AND OM.tintOppType=1
--	AND OBD.numOppBizDocsId=63914 AND OBD.numOppId=50854
--

		
/*
	SELECT GHD.numJournalId,GH.numJournal_Id,GH.numOppBizDocsId,GH.numOppId,GH.numDomainId,
	(SELECT numShipVia FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId=GH.numOppBizDocsId) numShipVia,
	(SELECT numDivisionId FROM dbo.OpportunityMaster WHERE numOppId=GH.numOppId) numDivisionID
	FROM 
	dbo.General_Journal_Header GH LEFT JOIN dbo.General_Journal_Details GHD 
	ON GH.numJournal_Id = GHD.numJournalId
	WHERE 
	GH.datCreatedDate > '2011-07-15 00:00:00.000'
	AND 
	GHD.numJournalId IS NULL 
	AND 
	numOppBizDocsId IN (SELECT numOppBizDocsId FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId=OBD.numOppId WHERE bitAuthoritativeBizDocs=1 AND OM.tintOppType=1)
	ORDER BY GH.numDomainId
*/	
END
GO