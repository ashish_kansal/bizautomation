GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype='p' AND NAME = 'USP_Item_ElasticSearchBizCart_GetDynamicValues') 
	DROP PROCEDURE USP_Item_ElasticSearchBizCart_GetDynamicValues

GO
CREATE PROCEDURE [dbo].[USP_Item_ElasticSearchBizCart_GetDynamicValues]
(
	@numSiteId AS NUMERIC(18,0)
	,@numDivisionID AS NUMERIC(18,0) = 0
	,@vcItemCodes AS NVARCHAR(MAX) = ''
)
AS
BEGIN
	DECLARE @numDomainID AS NUMERIC(18,0) = 0
	DECLARE @numDefaultRelationship AS NUMERIC(18,0) = 0
	DECLARE @numDefaultProfile AS NUMERIC(18,0) = 0
	DECLARE @bitShowInStock AS BIT = 0

	SELECT  
		@numDomainID = numDomainID
    FROM 
		[Sites]
    WHERE 
		numSiteID = @numSiteID

	IF ISNULL(@numDivisionID,0) > 0
		BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END

	--KEEP THIS BELOW ABOVE IF CONDITION
	SELECT 
		@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@bitShowInStock = bitShowInStock
	FROM 
		dbo.eCommerceDTL 
	WHERE 
		numSiteID = @numSiteId

	SELECT 
		numItemCode
		,vcItemName
		,numTotalPromotions
		,vcPromoDesc
		,bitRequireCouponCode
		,(CASE WHEN charItemType <> 'S' AND charItemType <> 'N' AND ISNULL(bitKitParent,0)=0
					THEN (CASE 
							WHEN bitAllowBackOrder = 1 THEN 1
							WHEN (ISNULL(numOnHand,0)<=0) THEN 0
							ELSE 1
						END)
					ELSE 1
		END) AS bitInStock
		,(CASE WHEN charItemType <> 'S' AND charItemType <> 'N'
				THEN (CASE WHEN @bitShowInStock = 1
							THEN (CASE 
									WHEN bitAllowBackOrder = 1 AND numDomainID <> 172 AND ISNULL(numOnHand,0)<=0 AND ISNULL(numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(numVendorID) > 0 THEN CONCAT('<font class="vendorLeadTime" style="color:green">Usually ships in ',dbo.GetVendorPreferredMethodLeadTime(numVendorID),' days</font>')
									WHEN bitAllowBackOrder = 1 AND ISNULL(numOnHand,0)<=0 THEN (CASE WHEN numDomainID = 172 THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)
									WHEN (ISNULL(numOnHand,0)<=0) THEN (CASE WHEN numDomainID = 172 THEN '<font style="color:red">ON HOLD</font>' ELSE '<font style="color:red">Out Of Stock</font>' END)
									ELSE (CASE WHEN numDomainID = 172 THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)  
								END)
							ELSE ''
						END)
				ELSE ''
			END) AS InStock 
	FROM 
		(SELECT 
			I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,ISNULL(TablePromotion.numTotalPromotions,0) numTotalPromotions
			,ISNULL(TablePromotion.vcPromoDesc,'''') vcPromoDesc
			,ISNULL(TablePromotion.bitRequireCouponCode,0) bitRequireCouponCode
			,SUM(ISNULL(WHI.numOnHand,0)) AS numOnHand
		FROM Item I 
		INNER JOIN
		(
			SELECT [OutParam] FROM dbo.[SplitString](@vcItemCodes,',')
		) TEMPItem
		ON
			I.numItemCode = TEMPItem.OutParam
		LEFT JOIN WareHouseItems WHI ON (WHI.numItemID = I.numItemCode)
		OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(I.numDomainID,@numDivisionID,numItemCode,numItemClassification,@numDefaultRelationship,@numDefaultProfile,1)) TablePromotion
		WHERE 
			WHI.numdomainID = @numDomainID 
		GROUP BY 
			I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,ISNULL(TablePromotion.numTotalPromotions,0)
			,ISNULL(TablePromotion.vcPromoDesc,'''') 
			,ISNULL(TablePromotion.bitRequireCouponCode,0) 
		)T
END
GO