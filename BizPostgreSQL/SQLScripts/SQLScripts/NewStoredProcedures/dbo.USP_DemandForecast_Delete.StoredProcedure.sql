SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Delete')
DROP PROCEDURE dbo.USP_DemandForecast_Delete
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Delete]
	@numDomainID NUMERIC (18,0),
	@vcSelectedIDs VARCHAR(500)
AS 
BEGIN
	DECLARE @strSQL VARCHAR(MAX)

	SET @strSQL = 'DELETE FROM DemandForecast WHERE numDomainID = ' + CAST(@numDomainID AS VARCHAR(10)) + ' AND numDFID IN (' + @vcSelectedIDs + ')'

	EXEC(@strSQL)
END
