
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePayrollDetail')
DROP PROCEDURE USP_ManagePayrollDetail
GO
CREATE PROCEDURE [dbo].[USP_ManagePayrollDetail]
(
    @numDomainId AS NUMERIC(9),
    @numPayrollHeaderID AS NUMERIC(9),
    @numPayrollDetailID AS NUMERIC(9),
    @numCheckStatus AS NUMERIC(9),
    @tintMode AS TINYINT
)
AS
  BEGIN

IF @tintMode=1  --Approve Checks
BEGIN
	UPDATE PayrollDetail SET numCheckStatus=@numCheckStatus WHERE
	numDomainId=@numDomainId AND numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID
	
	--If Paid then check commission is fully paid
	IF @numCheckStatus=2
	BEGIN
		UPDATE dbo.BizDocComission SET bitCommisionPaid=1 WHERE numDomainId=@numDomainId
		AND numComissionID IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId
		JOIN dbo.PayrollDetail PD ON PT.numPayrollHeaderID = PD.numPayrollHeaderID AND PT.numPayrollDetailID = PD.numPayrollDetailID
		WHERE PD.numCheckStatus=2 AND PD.numDomainId=@numDomainId 
		AND BC.numComissionID IN (SELECT numComissionID FROM dbo.PayrollTracking WHERE numDomainId=@numDomainId AND numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID)
		GROUP BY BC.numComissionID HAVING SUM(PT.monCommissionAmt)=MIN(BC.numComissionAMount))
	END
END
ELSE IF @tintMode=2  --Delete
BEGIN
	DELETE FROM PayrollTracking WHERE numDomainId=@numDomainId AND numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID
	DELETE FROM PayrollDetail WHERE numDomainId=@numDomainId AND numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID
END

END
GO