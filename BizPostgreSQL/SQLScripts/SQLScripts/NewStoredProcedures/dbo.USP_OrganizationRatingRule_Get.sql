SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OrganizationRatingRule_Get')
DROP PROCEDURE dbo.USP_OrganizationRatingRule_Get
GO
CREATE PROCEDURE [dbo].[USP_OrganizationRatingRule_Get]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		numORRID
		,tintPerformance
		,numFromAmount
		,numToAmount
		,numOrganizationRatingID
		,CONCAT('If the performance (Order Sub-total for period selected) amount for ',(CASE tintPerformance WHEN 1 THEN 'Last 3 MTD' WHEN 2 THEN 'Last 6 MTD' WHEN 3 THEN 'Last 1 YTD' END),' is >= ',numFromAmount,' and <= ',numToAmount,' set Organization Rating to ',ISNULL((SELECT vcData FROM ListDetails WHERE numListID=2 AND numListItemID=numOrganizationRatingID),'')) AS vcOrganizationRatingRule
	FROM 
		OrganizationRatingRule
	WHERE
		numDomainID = @numDomainID
END