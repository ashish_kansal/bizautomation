GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetStatus')
DROP PROCEDURE USP_WorkOrder_GetStatus
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetStatus]                             
	@numDomainID NUMERIC(18,0)
	,@vcWorkOrderIds VARCHAR(1000)                   
AS                            
BEGIN
	DECLARE @TEMPWorkOrder TABLE
	(
		ID INT IDENTITY(1,1)
		,numWOID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numQtyToBuild FLOAT
		,numWarehouseID NUMERIC(18,0)
		,numWorkOrderStatus TINYINT
		,vcWorkOrderStatus VARCHAR(300)
	)

	INSERT INTO @TEMPWorkOrder
	(
		numWOID
		,numItemCode
		,numQtyToBuild
		,numWarehouseID
		,numWorkOrderStatus
		,vcWorkOrderStatus
	)
	SELECT
		numWOID
		,numItemCode
		,numQtyToBuild
		,numWarehouseID
		,numWorkOrderStatus
		,vcWorkOrderStatus
	FROM
		dbo.GetWOStatus(@numDomainID,@vcWorkOrderIds)
	

	SELECT * FROM @TEMPWorkOrder WHERE numWOID IN (SELECT Id FROM dbo.SplitIDs(@vcWorkOrderIds,','))
END
GO