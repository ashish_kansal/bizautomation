GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_CanBillAgainstVendorInvoice')
DROP PROCEDURE USP_OpportunityBizDocs_CanBillAgainstVendorInvoice
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_CanBillAgainstVendorInvoice]
(
	@numVendorInvoiceBizDocID NUMERIC(18,0)
)
AS
BEGIN
	IF (SELECT 
			COUNT(*) 
		FROM 
			OpportunityBizDocItems 
		WHERE 
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND ISNULL(numUnitHour,0) - ISNULL(numVendorInvoiceUnitReceived,0) > 0) > 0
	BEGIN
		-- IF VENDOR INVOICE QTY IS PENDING TO RECEIVE THAN BILL CAN NOT BE CREATED AGAINST VENDOR INVOICE
		SELECT 0
	END
	ELSE IF (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OBDI.numOppItemID
					,OI.numUnitHour AS OrderedQty
					,OBDI.numUnitHour AS VendorInvoiceQty
					,OtherBills.BilledQty
				FROM
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBD.numOppBizDocsId = OBDI.numOppBizDocID
				INNER JOIN
					OpportunityItems OI
				ON
					OI.numoppitemtCode = OBDI.numOppItemID
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) BilledQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocItems.numOppItemID=OBDI.numOppItemID
						AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
				) AS OtherBills
				WHERE
					OBD.numOppBizDocsId = @numVendorInvoiceBizDocID
			) X
			WHERE
				ISNULL(X.VendorInvoiceQty,0) > (ISNULL(X.OrderedQty,0) - ISNULL(X.BilledQty,0))) > 0
	BEGIN
		-- IF SOMEONE HAS ALREADY CREATED BILL AND ENOUGHT QTY IS NOT LEFT FOR ALL ITEMS OF VENDOR INVOICE THAN BILL CAN NOT BE CREATED AGAINST VENDOR INVOICE
		SELECT 0
	END
	ELSE
	BEGIN
		SELECT 1
	END
END

	