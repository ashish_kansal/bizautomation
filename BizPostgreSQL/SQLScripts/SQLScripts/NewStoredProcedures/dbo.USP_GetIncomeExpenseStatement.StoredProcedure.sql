GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetIncomeExpenseStatement')
DROP PROCEDURE USP_GetIncomeExpenseStatement
GO
CREATE PROCEDURE [dbo].[USP_GetIncomeExpenseStatement]
@numDomainId AS NUMERIC(9),                                          
@dtFromDate AS DATETIME,                                        
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numAccountClass AS NUMERIC(9)=0

AS                                                          
BEGIN   
DECLARE @CURRENTPL DECIMAL(20,5) ;  
DECLARE @PLOPENING DECIMAL(20,5);  
DECLARE @PLCHARTID NUMERIC(8)  
DECLARE @numFinYear INT;  
  
DECLARE @dtFinYearFrom DATETIME;  
  
  
SET @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND    
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);  
  
SET @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND    
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);  
  
SET @dtFromDate = DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate);  
SET @dtToDate = DATEADD(minute, @ClientTimeZoneOffset, @dtToDate);  
  
  
--select * from view_journal where numDomainid=72  
  
CREATE TABLE #PLSummary (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),bitIsSubAccount Bit);  
 

select  numDomainID,Debit,Credit,COAvcAccountCode into #view_journal
from View_Journal where  datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
AND numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) 

INSERT INTO  #PLSummary  
SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,  
  
0 AS OPENING,  
  
ISNULL((SELECT SUM(Debit) FROM #view_journal VJ  
WHERE VJ.numDomainId=@numDomainId AND  
 VJ.COAvcAccountCode like COA.vcAccountCode + '%'),0) /*VJ.numAccountId=COA.numAccountId*/   
  AS DEBIT,  
  
ISNULL((SELECT SUM(Credit) FROM #view_journal VJ  
WHERE VJ.numDomainId=@numDomainId AND  
 VJ.COAvcAccountCode like COA.vcAccountCode + '%'),0) /*VJ.numAccountId=COA.numAccountId*/   
  AS CREDIT,ISNULL(COA.bitIsSubAccount,0)  
FROM Chart_of_Accounts COA   
WHERE   
--COA.numAccountId not in (select AC.numAccountID from AccountingCharges AC,AccountingChargeTypes AT where  
-- AC.numChargeTypeId=AT.numChargeTypeId and AT.chChargeCode='CG' and AC.numDomainID=@numDomainId) and  
      COA.numDomainId=@numDomainId AND COA.bitActive = 1 AND  
      (COA.vcAccountCode LIKE '0103%' OR  
       COA.vcAccountCode LIKE '0104%' OR   
       COA.vcAccountCode LIKE '0106%')  ;  
  
  
CREATE TABLE #PLOutPut (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));  
  
INSERT INTO #PLOutPut  
SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,  
 ISNULL(SUM(Opening),0) AS Opening,  
ISNULL(SUM(Debit),0) AS Debit,ISNULL(SUM(Credit),0) AS Credit  
FROM   
 AccountTypeDetail ATD RIGHT OUTER JOIN   
#PLSummary PL ON  
PL.vcAccountCode LIKE ATD.vcAccountCode + '%'  
AND ATD.numDomainId=@numDomainId AND  
(ATD.vcAccountCode LIKE '0103%' OR  
 ATD.vcAccountCode LIKE '0104%' OR  
 ATD.vcAccountCode LIKE '0106%')  
 WHERE PL.bitIsSubAccount=0  
GROUP BY   
ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;  
  
  
  
--------------------------------------------------------  
-- GETTING P&L VALUE  
  
SET @CURRENTPL =0;   
SET @PLOPENING=0;  
  
SELECT @CURRENTPL = ISNULL(SUM(Opening),0)+ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit),0) FROM  
#PLOutPut P WHERE   
vcAccountCode IN ('0103','0104','0106')  
  
--SELECT @CURRENTPL = @CURRENTPL - ISNULL(SUM(Opening),0)+ISNULL(sum(Debit),0)-ISNULL(sum(Credit),0) FROM  
--#PLOutPut P WHERE   
--vcAccountCode IN ('0104')  
  
SET @CURRENTPL=@CURRENTPL * (-1)  
  
SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND  
bitProfitLoss=1;  
  
--SELECT   @PLOPENING = isnull((SELECT isnull(monOpening,0) from CHARTACCOUNTOPENING CAO WHERE  
--numFinYearId=@numFinYear and numDomainID=@numDomainId and  
--CAO.numAccountId=COA.numAccountId),0)  +   
--ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ  
--WHERE VJ.numDomainId=@numDomainId AND  
-- VJ.numAccountId=COA.numAccountId AND  
-- datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFromDate-1),0)  
-- FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and  
--bitProfitLoss=1;  
  
--SELECT  @CURRENTPL=@CURRENTPL +        
--ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ  
--WHERE VJ.numDomainId=@numDomainId AND  
-- VJ.numAccountId=@PLCHARTID AND  
-- datEntry_Date BETWEEN @dtFromDate AND @dtToDate),0) ;  
  
SET @CURRENTPL=@CURRENTPL * (-1)  
--SELECT @PLCHARTID,@CURRENTPL,@PLOPENING  
  
-----------------------------------------------------------------  
  
 CREATE TABLE #PLShow (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),  
Balance  VARCHAR(50),AccountCode1  VARCHAR(100),vcAccountName1 VARCHAR(250),[Type] INT);  
  
 CREATE TABLE #PLShow1 (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),  
Balance  VARCHAR(50),AccountCode1  VARCHAR(100),vcAccountName1 VARCHAR(250),[Type] INT);  
  
CREATE TABLE #PLShowGrid1 (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),  
Balance VARCHAR(50),AccountCode1  VARCHAR(100),vcAccountName1 VARCHAR(250),[Type] INT);  
  
  
CREATE TABLE #PLShowGrid2 (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),  
Balance  VARCHAR(50),AccountCode1  VARCHAR(100),vcAccountName1 VARCHAR(250),[Type] INT);  
  
DECLARE @monIncome DECIMAL(20,5);  
DECLARE @monCOGS DECIMAL(20,5);  
DECLARE @monOtherIncome DECIMAL(20,5);  
DECLARE @monOtherExpense DECIMAL(20,5);  
DECLARE @monExpenses DECIMAL(20,5);  
  
  
SELECT @monIncome=ISNULL(SUM(Credit) - SUM(Debit) ,0)  
 FROM #PLSummary P  
WHERE vcAccountCode LIKE '010301%' AND P.bitIsSubAccount=0  
   
 SELECT @monOtherIncome=ISNULL( ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit) ,0),0)  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010302%' AND P.bitIsSubAccount=0  
   
 SELECT @monExpenses=ISNULL( ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit) ,0),0)  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010401%' AND P.bitIsSubAccount=0  
   
SELECT @monOtherExpense=ISNULL( ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit) ,0),0)  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010402%' AND P.bitIsSubAccount=0  
   
  
 SET @monOtherIncome=@monOtherIncome * (-1)  
   
   
--SELECT  @monCOGS= dbo.GetCOGSValue(@numDomainId,@dtFromDate,@dtToDate )  
 SELECT @monCOGS=ISNULL( ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit) ,0),0)  
 FROM #PLSummary P  
 --WHERE vcAccountCode LIKE '010403%'   
 WHERE vcAccountCode LIKE '0106%' AND P.bitIsSubAccount=0  
   
ALTER TABLE #PLSummary  
DROP COLUMN bitIsSubAccount  
  
  
   
INSERT INTO #PLShow  
  
SELECT 0,'Direct Income',0,'Direct Income','0101A',0,0,0,'' ,'','Direct Income',2    
  
UNION  
  
SELECT numAccountId ,vcAccountName ,  
numParntAcntTypeID ,vcAccountDescription ,  
'0101A1',Opening ,Debit ,Credit, CAST((Credit - Debit) AS VARCHAR(50)) AS Balance,  
vcAccountCode AS  AccountCode1,  
 CASE   
 WHEN LEN(P.vcAccountCode)>4 THEN REPLICATE('&nbsp;', LEN('0101A1')-4) + P.[vcAccountName]  
 ELSE P.[vcAccountName]  
 END AS vcAccountName1, 1 AS Type  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010301%'   
-- WHERE numAccountId IN (  
-- SELECT IT.numIncomeChartAcntId FROM Item IT where IT.numDomainID=@numDomainId )  
UNION  
  
SELECT 0,'',0,'','0101B',0,0,0,'________________' ,'','',2    
UNION  
SELECT 0,'',0,'','0101B1',0,0,0,CAST(@monIncome AS VARCHAR(50)),'','A) Total Direct Income',2    
UNION  
SELECT 0,'',0,'','0101B2',0,0,0,'' ,'&nbsp;','',2    
UNION  
SELECT 0,'',0,'','0101C',0,0,0,'' ,'','Cost of Goods Sold',2    
UNION  
SELECT numAccountId ,vcAccountName ,  
numParntAcntTypeID ,vcAccountDescription ,  
'0101C1',Opening ,Debit ,Credit, CAST((Debit-Credit) AS VARCHAR(50)) AS Balance,  
vcAccountCode AS  AccountCode1,  
 CASE   
 WHEN LEN('0101C1')>4 THEN REPLICATE('&nbsp;', LEN('0101C1')-4) + P.[vcAccountName]  
 ELSE P.[vcAccountName]  
 END AS vcAccountName1, 1 AS Type  
 FROM #PLSummary P  
 --WHERE vcAccountCode LIKE '010403%'   
 WHERE vcAccountCode LIKE '0106%'   
--select -1,'',0,'','0101C1',0,0,0,CAST(@monCOGS AS VARCHAR(50)),'','COGS',1  
UNION  
SELECT 0,'',0,'','0101C2',0,0,0,'________________' ,'','',2   
UNION  
SELECT 0,'',0,'','0101C3',0,0,0,CAST(@monCOGS AS VARCHAR(50)),'','B) Total COGS',2  
UNION  
SELECT 0,'',0,'','0101C4',0,0,0,'________________' ,'','',2   
UNION  
SELECT 0,'',0,'','0101C5',0,0,0,CAST(@monIncome-@monCOGS AS VARCHAR(50)),'','Gross Profit= (A- B)',2  
UNION  
SELECT 0,'',0,'','0101C6',0,0,0,'&nbsp;' ,'','',2  
UNION  
SELECT 0,'',0,'','0101D',0,0,0,'' ,'','Direct Expenses',2  
UNION  
SELECT numAccountId ,vcAccountName ,  
numParntAcntTypeID ,vcAccountDescription ,  
'0101D1',Opening ,Debit ,Credit,CAST((Debit - Credit) AS VARCHAR(50)) AS Balance,  
vcAccountCode AS  AccountCode1,  
 CASE   
 WHEN LEN('0101D1')>4 THEN REPLICATE('&nbsp;', LEN('0101D1')-4) + P.[vcAccountName]  
 ELSE P.[vcAccountName]  
 END AS vcAccountName1, 1 AS Type  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010401%'   
UNION  
SELECT 0,'',0,'','0101D2',0,0,0,'________________' ,'','',2  
UNION  
SELECT 0,'',0,'','0101D3',0,0,0,CAST (@monExpenses AS VARCHAR(50)),'','C) Total Direct Expenses',2  
UNION  
SELECT 0,'',0,'','0101E',0,0,0,'________________' ,'','',2  
UNION  
SELECT 0,'',0,'','0101E1',0,0,0,CAST(@monIncome-@monCOGS-@monExpenses AS VARCHAR(50)),'','Operating Income = (A -B - C)',2  
UNION  
SELECT 0,'',0,'','0101E2',0,0,0,'&nbsp;' ,'','',2  
UNION  
SELECT 0,'',0,'','0101F',0,0,0,'' ,'','Other Income',2  
UNION  
SELECT numAccountId ,vcAccountName ,  
numParntAcntTypeID ,vcAccountDescription ,  
'0101F1',Opening ,Debit ,Credit, CAST((Debit - Credit) * (-1) AS VARCHAR(50))AS Balance,  
vcAccountCode AS  AccountCode1,  
 CASE   
 WHEN LEN('0101F1')>4 THEN REPLICATE('&nbsp;', LEN('0101F1')-4) + P.[vcAccountName]  
 ELSE P.[vcAccountName]  
 END AS vcAccountName1, 1 AS Type  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010302%'   
-- and numAccountId NOT IN (  
-- SELECT IT.numIncomeChartAcntId FROM Item IT where IT.numDomainID=@numDomainId )  
UNION  
SELECT 0,'',0,'','0101F2',0,0,0,'________________' ,'','',2  
UNION  
SELECT 0,'',0,'','0101F3',0,0,0,CAST(@monOtherIncome AS VARCHAR(50)),'','D) Total Other Income',2  
UNION  
SELECT 0,'',0,'','0101F4',0,0,0,'&nbsp;' ,'','',2  
UNION  
SELECT 0,'',0,'','0101G',0,0,0,'' ,'','Other Expense',2  
UNION  
SELECT numAccountId ,vcAccountName ,  
numParntAcntTypeID ,vcAccountDescription ,  
'0101G1',Opening ,Debit ,Credit, CAST((Debit - Credit) AS VARCHAR(50))AS Balance,  
vcAccountCode AS  AccountCode1,  
 CASE   
 WHEN LEN('0101G1')>4 THEN REPLICATE('&nbsp;', LEN('0101G1')-4) + P.[vcAccountName]  
 ELSE P.[vcAccountName]  
 END AS vcAccountName1, 1 AS Type  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010402%'  
-- and numAccountId NOT IN (  
-- SELECT IT.numIncomeChartAcntId FROM Item IT where IT.numDomainID=@numDomainId )  
UNION  
SELECT 0,'',0,'','0101G2',0,0,0,'________________' ,'','',2  
UNION  
SELECT 0,'',0,'','0101G3',0,0,0,CAST(@monOtherExpense AS VARCHAR(50)),'','E) Total Other Expense',2  
UNION  
--select 0,'',0,'','0101G4',0,0,0,'________________' ,'','',2  
--UNION  
  
  
SELECT 0,'',0,'','0101H4',0,0,0,'________________' ,'','',2  
UNION  
SELECT 0,'',0,'','0101H5',0,0,0,CAST( (@monIncome+@monOtherIncome) - (@monCOGS + @monExpenses +@monOtherExpense) AS VARCHAR(50)) ,'','Net Income = (A + D) - (B + C + E)',2  
UNION  
SELECT 0,'',0,'','0101H6',0,0,0,'&nbsp;' ,'','',2  
/*  
insert into #PLShowGrid1  
select * from #PLShow A  ORDER BY A.vcAccountCode  
insert into #PLShowGrid1  
SELECT * FROM #PLShow1 a ORDER BY A.vcAccountCode*/  
INSERT INTO #PLShowGrid1  
SELECT  A.numAccountId ,A.vcAccountName ,A.numParntAcntTypeID ,A.vcAccountDescription ,A.vcAccountCode ,A.Opening ,A.Debit ,A.Credit ,A.Balance ,A.AccountCode1 ,  
        CASE WHEN LEN(COA.[vcAccountCode]) > 4 AND  A.[TYPE] <> 2  
             THEN REPLICATE('&nbsp;', LEN(COA.[vcAccountCode]) - 4) + A.[vcAccountName]  
             WHEN A.[TYPE] = 2 THEN A.[vcAccountName1]  
             ELSE A.[vcAccountName]  
        END AS vcAccountName1 ,A.Type  
FROM    #PLShow A LEFT JOIN dbo.Chart_Of_Accounts COA ON A.numAccountId = COA.numAccountId ORDER BY A.vcAccountCode,COA.vcAccountCode  
  
INSERT INTO #PLShowGrid1  
SELECT  A.numAccountId ,A.vcAccountName ,A.numParntAcntTypeID ,A.vcAccountDescription ,A.vcAccountCode ,A.Opening ,A.Debit ,A.Credit ,A.Balance ,A.AccountCode1 ,  
        CASE WHEN LEN(COA.[vcAccountCode]) > 4  
             THEN REPLICATE('&nbsp;', LEN(COA.[vcAccountCode]) - 4 ) + A.[vcAccountName]  
             ELSE A.[vcAccountName]  
        END AS vcAccountName1 ,A.Type  
FROM    #PLShow1 A LEFT JOIN dbo.Chart_Of_Accounts COA ON A.numAccountId = COA.numAccountId ORDER BY A.vcAccountCode,COA.vcAccountCode  
  
--insert into #PLShowGrid2  
--SELECT * FROM #PLShow1 a ORDER BY A.vcAccountCode  
  
  
SELECT * FROM #PLShowGrid1  
  
--union  
--select * from #PLShowGrid2  
  
DROP TABLE #PLShowGrid1;  
DROP TABLE #PLShowGrid2;  
DROP TABLE #PLOutPut;  
DROP TABLE #PLSummary;  
DROP TABLE #PLShow;  
DROP TABLE #PLShow1;  
DROP TABLE #view_journal;
END  