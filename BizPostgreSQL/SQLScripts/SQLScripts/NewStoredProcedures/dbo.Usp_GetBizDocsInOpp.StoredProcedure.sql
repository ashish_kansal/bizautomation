/****** Object:  StoredProcedure [dbo].[Usp_GetBizDocsInOpp]    Script Date: 05/07/2009 22:10:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetBizDocsInOpp')
DROP PROCEDURE Usp_GetBizDocsInOpp
GO
CREATE PROCEDURE [dbo].[Usp_GetBizDocsInOpp]
    @numOppId NUMERIC(9),
    @bitAuth BIT = 0,
    @tintMode TINYINT = 0,
    @numOppBizDocID NUMERIC(9) = 0
AS 
    BEGIN
        IF @tintMode = 0 
            BEGIN
                SELECT  [numOppBizDocsId],
                        [vcBizDocID],
                        CASE WHEN LEN(vcBizDocName) > 2 THEN vcBizDocName
                             ELSE vcBizDocID
                        END AS vcBizDocName,
                        dbo.[GetListIemName]([numBizDocId]) AS vcBizDoc
                FROM    [OpportunityBizDocs]
                WHERE   [numOppId] = @numOppId
                        AND [bitAuthoritativeBizDocs] = @bitAuth
            END
		IF @tintMode=1
		BEGIN
			SELECT		[numOppBizDocsId],
                        [vcBizDocID],
                        CASE WHEN LEN(vcBizDocName) > 2 THEN vcBizDocName
                             ELSE vcBizDocID
                        END + ' (' + dbo.[GetListIemName]([numBizDocId]) +')'AS vcBizDocName
                FROM    [OpportunityBizDocs]
                WHERE   [numOppId] = @numOppId
                        AND [numOppBizDocsId] <> @numOppBizDocID
		END
    
    END
