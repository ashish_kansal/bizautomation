-- EXEC USP_GetCardTypes 72
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCardTypes' ) 
    DROP PROCEDURE USP_GetCardTypes
GO
CREATE PROCEDURE USP_GetCardTypes
    @numDomainID AS NUMERIC(9),
    @sintType AS SMALLINT=0
AS 
    BEGIN
        DECLARE @numListID NUMERIC(9) 
--        SELECT TOP 1
--                @numListID = [numListID]
--        FROM    [ListMaster]
--        WHERE   [vcListName] = 'Card Type'
--        ORDER BY numListID
		SET @numListID=120 --fixed list item for card type
        
        IF @sintType = 0 
            BEGIN
                SELECT DISTINCT
                        [vcData] /* + ' - '
                        + CONVERT(VARCHAR(10), ISNULL([fltTransactionCharge],
                                                      0)) + '%'*/ CardType,
                        vcData,
                        [numListItemID],
                        ISNULL([fltTransactionCharge], 0) fltTransactionCharge
                FROM    [ListDetails]
                        INNER JOIN [COACreditCardCharge] ON [COACreditCardCharge].[numCreditCardTypeId] = [ListDetails].[numListItemID]
                                                            AND COACreditCardCharge.[numDomainID] = @numDomainID
                WHERE   [numListID] = @numListID
						AND COACreditCardCharge.numAccountID>0
                        AND ( [ListDetails].constFlag = 1
                              OR [ListDetails].[numDomainID] = @numDomainID
                            )
--		ORDER BY [ListDetails].[sintOrder]
            END
        ELSE if @sintType = 1 
            BEGIN
                SELECT  numListItemID,
                        [vcData] /*+ ' - '
                        + CONVERT(VARCHAR(10), ISNULL([fltTransactionCharge],
                                                      0)) + '%'*/ CardType
                FROM    ListDetails
                        Left JOIN [COACreditCardCharge] ON [COACreditCardCharge].[numCreditCardTypeId] = [ListDetails].[numListItemID]
                                                            AND COACreditCardCharge.[numDomainID] = @numDomainID
                WHERE   [numListID] = @numListID
                        AND ( [ListDetails].constFlag = 1
                              OR [ListDetails].[numDomainID] = @numDomainID
                            )
                
            END
    END
    
