GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_UpdatePurchaseOrderCost')
DROP PROCEDURE USP_OpportunityMaster_UpdatePurchaseOrderCost
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_UpdatePurchaseOrderCost]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcSelectedRecords VARCHAR(MAX),
	@ClientTimeZoneOffset INT
AS  
BEGIN
	DECLARE @hDocItem INT

	DECLARE @TEMP TABLE
	(
		numVendorID NUMERIC(18,0)
		,numSelectedVendorID NUMERIC(18,0)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numUnits FLOAT
		,monUnitCost DECIMAL(20,5)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @TEMP
	(
		numVendorID
		,numSelectedVendorID
		,numOppID
		,numOppItemID
		,numUnits
		,monUnitCost
	)
	SELECT
		ISNULL(VendorID,0)
		,ISNULL(SelectedVendorID,0)
		,ISNULL(OppID,0)
		,ISNULL(OppItemID,0)
		,ISNULL(Units,0)
		,ISNULL(UnitCost,0)
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		VendorID NUMERIC(18,0)
		,SelectedVendorID NUMERIC(18,0)
		,OppID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
		,Units FLOAT
		,UnitCost DECIMAL(20,5)
	)

	EXEC sp_xml_removedocument @hDocItem

	IF EXISTS (SELECT OB.numOppBizDocsId FROM OpportunityBizDocs OB INNER JOIN @TEMP T ON OB.numOppId = T.numOppID)
	BEGIN
		RAISERROR('BIZ_DOC_EXISTS',16,1)
		RETURN
	END
	ELSE
	BEGIN
		DECLARE @TEMPVendorItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numVendorID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,monPrice DECIMAL(20,5)
		)

		INSERT INTO @TEMPVendorItems
		(
			numVendorID
			,numItemCode
			,numWarehouseItemID
			,numUnitHour
			,monPrice
		)
		SELECT 
			T.numSelectedVendorID
			,OI.numItemCode
			,OI.numWarehouseItmsID
			,OI.numUnitHour
			,T.monUnitCost
		FROM
			@TEMP T
		INNER JOIN
			OpportunityItems OI
		ON
			T.numOppID = OI.numOppId
			AND T.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppId
		WHERE
			OM.numDomainId = @numDomainID
			AND OM.numDivisionId <> T.numSelectedVendorID

		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @TEMPOpp TABLE
			(
				ID INT IDENTITY(1,1)
				,numOppID NUMERIC(18,0)
			)

			INSERT INTO @TEMPOpp
			(
				numOppID
			)
			SELECT DISTINCT
				numOppID
			FROM
				@TEMP

			DECLARE @i INT = 1
			DECLARE @iCount INT
			DECLARE @numOppID NUMERIC(18,0)

			SELECT @iCount=COUNT(*) FROM @TEMPOpp

			-- DELETE PO NOT ITEM LEFT IN ORDER OR UPDATE ORDER TOTAL AMOUNT AND TAXES
			WHILE @i <= @iCount
			BEGIN
				SELECT @numOppID=numOppID FROM @TEMPOpp WHERE ID = @i

				EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID

				-- DELETE ITEMS FROM ORDER FOR WHOM ANOTHER VENDOR IS SELECTED TO CREATE NEW ORDER
				DELETE 
					OI
				FROM
					OpportunityMaster OM
				INNER JOIN
					OpportunityItems OI
				ON
					OM.numOppId = OI.numOppId
				INNER JOIN
					@TEMP T
				ON
					OI.numOppId = T.numOppID
					AND OI.numoppitemtCode = T.numOppItemID
				WHERE
					OM.numDomainId = @numDomainID
					AND OM.numOppId = @numOppID
					AND OM.numDivisionId <> T.numSelectedVendorID

				-- UPDATE LINE ITEM UNITS AND COST WHERE VENDOR IS SAME
				UPDATE
					OI
				SET
					OI.numUnitHour = ISNULL(T.numUnits,0)
					,OI.monPrice = ISNULL(T.monUnitCost,0)
					,monTotAmount = (ISNULL(T.numUnits,0) * (CASE 
																	WHEN ISNULL(OI.bitDiscountType,0) = 0 
																	THEN (ISNULL(T.monUnitCost,0) - (ISNULL(T.monUnitCost,0) * (ISNULL(OI.fltDiscount,0) / 100)))
																	ELSE (ISNULL(T.monUnitCost,0) -ISNULL(OI.fltDiscount,0)) 
																END))  
					,monTotAmtBefDiscount = ISNULL(T.numUnits,0) * ISNULL(T.monUnitCost,0)
				FROM
					OpportunityMaster OM
				INNER JOIN
					OpportunityItems OI
				ON
					OM.numOppId = OI.numOppId
				INNER JOIN
					@TEMP T
				ON
					OI.numOppId = T.numOppID
					AND OI.numoppitemtCode = T.numOppItemID
				WHERE
					OM.numDomainId = @numDomainID
					AND OM.numOppId = @numOppID
					AND OM.numDivisionId = T.numSelectedVendorID

				EXEC USP_UpdatingInventoryonCloseDeal @numOppID=@numOppID,@numOppItemId=0,@numUserCntID=@numUserCntID

				IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID) AND NOT EXISTS (SELECT numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId=@numOppID)
				BEGIN
					EXEC USP_DeleteOppurtunity @numOppId=@numOppID,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
				END
				ELSE
				BEGIN
					UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,GETDATE(),0),bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numOppId=@numOppID

					--INSERT TAX FOR DIVISION   
					IF (select ISNULL(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
					BEGIN
						--Delete Tax for Opportunity Items if item deleted 
						DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

						--Insert Tax for Opportunity Items
						INSERT INTO dbo.OpportunityItemsTaxItems 
						(
							numOppId,
							numOppItemID,
							numTaxItemID,
							numTaxID
						) 
						SELECT 
							@numOppId,
							OI.numoppitemtCode,
							TI.numTaxItemID,
							0
						FROM 
							dbo.OpportunityItems OI 
						JOIN 
							dbo.ItemTax IT 
						ON 
							OI.numItemCode=IT.numItemCode 
						JOIN
							TaxItems TI 
						ON 
							TI.numTaxItemID = IT.numTaxItemID 
						WHERE 
							OI.numOppId=@numOppID 
							AND IT.bitApplicable=1 
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
						UNION
						SELECT 
							@numOppId,
							OI.numoppitemtCode,
							0,
							0
						FROM 
							dbo.OpportunityItems OI 
						JOIN 
							dbo.Item I 
						ON 
							OI.numItemCode=I.numItemCode
						WHERE 
							OI.numOppId=@numOppID 
							AND I.bitTaxable=1 
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
						UNION
						SELECT
							@numOppId,
							OI.numoppitemtCode,
							1,
							TD.numTaxID
						FROM
							dbo.OpportunityItems OI 
						INNER JOIN
							ItemTax IT
						ON
							IT.numItemCode = OI.numItemCode
						INNER JOIN
							TaxDetails TD
						ON
							TD.numTaxID = IT.numTaxID
							AND TD.numDomainId = @numDomainId
						WHERE
							OI.numOppId = @numOppID
							AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
					END
				END

				SET @i = @i + 1
			END

			DECLARE @TEMPVendor TABLE
			(
				ID INT IDENTITY(1,1)
				,numVendorID NUMERIC(18,0)
				,numContactID NUMERIC(18,0)
				,intBillingDays INT
			)

			INSERT INTO @TEMPVendor
			(
				numVendorID
				,numContactID
				,intBillingDays
			)
			SELECT DISTINCT
				numVendorID
				,ISNULL((SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId=numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC),0)
				,ISNULL(numBillingDays,0)
			FROM
				@TEMPVendorItems T
			INNER JOIN
				DivisionMaster
			ON
				T.numVendorID = DivisionMaster.numDivisionID

			DECLARE @j INT = 1
			DECLARE @jCount INT
			DECLARE @numVendorID NUMERIC(18,0)
			DECLARE @numContactID NUMERIC(18,0)
			DECLARE @numCurrencyID NUMERIC(18,0)
			DECLARE @intBillingDays INT
			DECLARE @strItems NVARCHAR(MAX) = ''
			DECLARE @dtEstimatedCloseDate DATETIME

			SELECT @numCurrencyID = ISNULL(numCurrencyID,0) FROM Domain WHERE numDomainId = @numDomainID

			SELECT @jCount=COUNT(*) FROM @TEMPVendor

			WHILE @j <= @jCount
			BEGIN
				SELECT @numVendorID=numVendorID,@numContactID=numContactID,@intBillingDays=intBillingDays FROM @TEMPVendor WHERE ID = @j

				SET @strItems =(
									SELECT 
										0 AS numoppitemtCode
										,T.numItemCode
										,CAST(numUnitHour AS VARCHAR) AS numUnitHour
										,CAST(monPrice AS VARCHAR)
										,CAST(ISNULL(T.numUnitHour,0) * ISNULL(T.monPrice,0) AS VARCHAR) monTotAmount
										,ISNULL(I.txtItemDesc,'')
										,T.numWarehouseItemID AS numWarehouseItmsID
										,0 AS numToWarehouseItemID
										,1 AS Op_Flag
										,I.charItemType AS  ItemType
										,0 AS DropShip
										,0 AS bitDiscountType
										,0 AS fltDiscount
										,CAST(ISNULL(T.numUnitHour,0) * ISNULL(T.monPrice,0) AS VARCHAR) AS monTotAmtBefDiscount
										,vcItemName
										,I.numBaseUnit AS numUOM
										,0 AS bitWorkOrder
										,0 AS numVendorWareHouse
										,0 AS numShipmentMethod
										,0
										,0 AS numProjectID
										,0 AS numProjectStageID
										,'' Attributes
										,0 AS bitItemPriceApprovalRequired
									FROM 
										@TEMPVendorItems T
									INNER JOIN
										Item I
									ON
										T.numItemCode = I.numItemCode
									WHERE
										T.numVendorID = @numVendorID
									FOR XML RAW ('Item'), ROOT ('NewDataSet'), ELEMENTS
								)
		
				SET @dtEstimatedCloseDate = DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,GETUTCDATE())

				--UPDATE MASTER PO AND ADD MERGE PO ITEMS
				EXEC USP_OppManage @numOppID=0,@numContactId=@numContactId,@numDivisionId=@numVendorID,@tintSource=0,@vcPOppName='',
									@Comments='',@bitPublicFlag=0,@numUserCntID=@numUserCntID,@monPAmount=0,@numAssignedTo=@numUserCntID,
									@numDomainId=@numDomainId,@strItems=@strItems,@strMilestone='',@dtEstimatedCloseDate=@dtEstimatedCloseDate,@CampaignID=0,
									@lngPConclAnalysis=0,@tintOppType=2,@tintActive=1,@numSalesOrPurType=0,
									@numRecurringId=0,@numCurrencyID=@numCurrencyID,@DealStatus=1,@numStatus=0,@vcOppRefOrderNo='',
									@numBillAddressId=0,@numShipAddressId=0,@bitStockTransfer=0,@WebApiId=0,
									@tintSourceType=0,@bitDiscountType=0,@fltDiscount=0,@bitBillingTerms=0,
									@intBillingDays=@intBillingDays,@bitInterestType=0,@fltInterest=0,@tintTaxOperator=0,
									@numDiscountAcntType=0,@vcWebApiOrderNo='',@vcCouponCode='',@vcMarketplaceOrderID=0,
									@vcMarketplaceOrderDate=NULL,@vcMarketplaceOrderReportId='',@numPercentageComplete=0,
									@bitUseShippersAccountNo=0,@bitUseMarkupShippingRate=0,
									@numMarkupShippingRate=0,@intUsedShippingCompany=0,@numShipmentMethod=0,@dtReleaseDate=NULL,@numPartner=0,@numPartenerContactId=0
									,@numAccountClass=0,@numVendorAddressID=0	

				SET @j = @j + 1
			END

		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH	
	END
END
GO