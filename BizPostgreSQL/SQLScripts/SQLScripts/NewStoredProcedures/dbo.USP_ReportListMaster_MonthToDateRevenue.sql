GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_MonthToDateRevenue')
DROP PROCEDURE USP_ReportListMaster_MonthToDateRevenue
GO

CREATE PROCEDURE USP_ReportListMaster_MonthToDateRevenue
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	SELECT  
		ISNULL(dbo.fn_GetContactName(OpportunityMaster.numAssignedTo),'') as AssignedTo
		,CONCAT('~/contact/frmContacts.aspx?CntID=',OpportunityMaster.numAssignedTo) AS URL
		,COUNT(ISNULL(dbo.fn_GetContactName(OpportunityMaster.numAssignedTo),'')) as TotalRecords
		,CAST(SUM(OpportunityBizDocs.monDealAmount) as NUMERIC(18,0))  as InvoiceGrandtotal
	FROM 
		OpportunityMaster 
	INNER JOIN DivisionMaster on OpportunityMaster.numDivisionId=DivisionMaster.numDivisionId  
	INNER JOIN CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid  
	INNER JOIN AdditionalContactsInformation on OpportunityMaster.numContactId=AdditionalContactsInformation.numContactId  
	LEFT JOIN OpportunityBizDocs on OpportunityMaster.numOppId = OpportunityBizDocs.numOppId 
	WHERE 
		OpportunityMaster.tintOppType = 1 
		AND OpportunityMaster.numDomainId = @numDomainID 
		AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0)=1
		AND (OpportunityMaster.bintCreatedDate  between dbo.get_month_start( dateadd(Minute,-330,convert(varchar,dateadd(minute,-330,getutcdate()),101))) and dbo.get_month_end(dateadd(Minute,-330,convert(varchar,dateadd(minute,-330,getutcdate()),101))))		
	GROUP BY 
		OpportunityMaster.numAssignedTo
END
GO