SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_GetByItemAndExternalInternalLocation')
DROP PROCEDURE dbo.USP_WarehouseItems_GetByItemAndExternalInternalLocation
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_GetByItemAndExternalInternalLocation]
@numDomainID NUMERIC(18,0),
@numItemCode NUMERIC(18,0),
@numWareHouseID NUMERIC(18,0),
@numWarehouseLocationID NUMERIC(18,0)                 
AS
BEGIN
	SELECT TOP 1
		numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND numItemID=@numItemCode
		AND numWareHouseID=@numWareHouseID
		AND (numWLocationID=@numWarehouseLocationID OR @numWarehouseLocationID=0)
	ORDER BY
		numWareHouseItemID
END