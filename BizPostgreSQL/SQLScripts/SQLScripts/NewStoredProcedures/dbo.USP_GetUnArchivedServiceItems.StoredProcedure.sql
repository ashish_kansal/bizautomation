GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUnArchivedServiceItems')
DROP PROCEDURE USP_GetUnArchivedServiceItems
GO
CREATE PROCEDURE [dbo].USP_GetUnArchivedServiceItems
@numDomainID as BIGINT = 0,
@str as varchar(20)=''
as
--SELECT * FROM Item WHERE numDomainId  = @numDomainID 
--AND charItemType = 'S' AND (Isarchieve IS NULL OR Isarchieve = 0)

SELECT numItemCode,vcItemName FROM Item WHERE numDomainId  = @numDomainID 
AND charItemType = 'S' AND Isarchieve = 0

GO