SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetVendors')
DROP PROCEDURE USP_Item_GetVendors
GO
CREATE PROCEDURE [dbo].[USP_Item_GetVendors]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@ClientTimeZoneOffset AS INT
)
AS
BEGIN
	SELECT
		Vendor.numVendorID
		,ISNULL(CompanyInfo.vcCompanyName,'') vcCompanyName
		,ISNULL(Vendor.monCost,0) as monVendorCost
		,ISNULL(CAST(Vendor.intMinQty AS FLOAT),0) as monVendorCost
		,CASE WHEN LastOrder.vcPOppName IS NULL THEN '-' ELSE CONCAT('<a href=''#'' onclick=''openVendorPriceHistory(',Vendor.numVendorID,');''>',monUnitCOst,'</a> (',CAST(numQty AS FLOAT),') ',dtOrderedDate,' ', vcPOppName) END vcLastOrder
	FROM
		Vendor
	INNER JOIN
		Item
	ON
		Vendor.numItemCode = Item.numItemCode
	INNER JOIN
		DivisionMaster 
	ON
		Vendor.numVendorID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	OUTER APPLY
	(
		SELECT TOP 1
			OM.vcPOppName
			,dbo.fn_UOMConversion(OI.numUOMId,@numItemCode,@numItemCode,Item.numBaseUnit) numQty
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate),@numDomainID) dtOrderedDate
			,CAST(ISNULL(OI.monPrice,0) AS DECIMAL(20,5)) AS monUnitCOst
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numDivisionId = Vendor.numVendorID
			AND OI.numItemCode = @numItemCode
			AND tintOppType = 2
			AND tintOppStatus = 1
			AND ISNULL(bitStockTransfer,0) = 0
		ORDER BY
			OM.numOppId DESC
	) AS LastOrder
	WHERE
		Vendor.numDomainID = @numDomainID
		AND Vendor.numItemCode = @numItemCode
END