SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TransactionHistory_GetAuthrizedTrasaction')
DROP PROCEDURE USP_TransactionHistory_GetAuthrizedTrasaction
GO
CREATE PROCEDURE [dbo].[USP_TransactionHistory_GetAuthrizedTrasaction] 
( 
	@numDomainID AS NUMERIC(18,0),    
	@numDivisionID AS NUMERIC(18,0),
	@monAmount AS DECIMAL(20,5),
	@tintMode AS TINYINT
)
AS
BEGIN
	SELECT TOP 1 
		*
	FROM
		TransactionHistory
	WHERE
		numDomainID=@numDomainID
		AND numDivisionID=@numDivisionID
		AND 1 = (CASE WHEN @tintMode = 1 THEN (CASE WHEN  ISNULL(monAuthorizedAmt,0) - ISNULL(monCapturedAmt,0) = @monAmount THEN 1 ELSE 0 END) ELSE 1 END)
		AND ISNULL(tintTransactionStatus,0) = 1
		AND 1 = (CASE WHEN @tintMode = 1 THEN (CASE WHEN  DATEDIFF(DAY, dtCreatedDate, GETUTCDATE()) BETWEEN 0 AND 7 THEN 1 ELSE 0 END) ELSE 1 END) 
	ORDER BY
		numTransHistoryID DESC
END
GO