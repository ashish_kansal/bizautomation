GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageErrorDetail')
DROP PROCEDURE USP_ManageErrorDetail
GO

CREATE PROCEDURE dbo.USP_ManageErrorDetail

@numDomainId AS NUMERIC(18),
@numSiteId AS NUMERIC(18),
@numCultureId AS NUMERIC(18),
@tintApplication AS INT,
@strErrorMessages  TEXT  = NULL

AS

BEGIN
 DECLARE  @hDocItem INT
	IF CONVERT(VARCHAR(500) , @strErrorMessages ) <> ''
	BEGIN
	   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strErrorMessages 
       UPDATE dbo.ErrorDetail   SET vcErrorDesc = z.vcErrorDesc
       FROM 
         (
               SELECT *
               FROM OPENXML (@hDocItem,'/NewDataSet/ErrorMessages[numErrorDetailId>0]',2)
               WITH  (                      
                       numErrorId        NUMERIC(18)  ,
                       vcErrorDesc       VARCHAR(100) , 
                       numErrorDetailId  NUMERIC(18)
                     )
          ) z
       WHERE ErrorDetail.numErrorDetailId = z.numErrorDetailId
     
       INSERT INTO dbo.ErrorDetail
                 (
                   numErrorId ,
                   vcErrorDesc ,
                   numDomainId ,
                   numSiteId ,
                   numCultureId 
                  )
       SELECT      X.numErrorId,
                   X.vcErrorDesc ,
                   @numDomainId,
                   @numSiteId ,
                   @numCultureId
       FROM      (
                      SELECT *
                      FROM   OPENXML (@hDocItem, '/NewDataSet/ErrorMessages[numErrorDetailId=0]', 2)
                      WITH   (  numErrorId      NUMERIC(18)  ,
                                vcErrorDesc     VARCHAR(100) 
                      )
                  ) X
	          
	END
	
	
	 EXEC sp_xml_removedocument @hDocItem
        
END
--exec USP_ManageErrorDetail @numDomainId=1,@numSiteId=65,@numCultureId=0,@tintApplication=3,@strErrorMessages='<NewDataSet> <ErrorMessages><numErrorDetailId>1032</numErrorDetailId><vcErrorDesc>kkkk</vcErrorDesc><numErrorId>3</numErrorId></ErrorMessages></NewDataSet>'
