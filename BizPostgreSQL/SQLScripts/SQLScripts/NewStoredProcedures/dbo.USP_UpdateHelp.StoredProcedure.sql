GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateHelp')
DROP PROCEDURE USP_UpdateHelp
GO
CREATE PROCEDURE  USP_UpdateHelp
    @numHelpId NUMERIC(9),
    @helpcategory NUMERIC(9),
    @helpMetatag NVARCHAR(400),
    @helpheader NVARCHAR(400),
    @helpDescription NVARCHAR(MAX),
    @helpshortdesc NVARCHAR(400),
    @helpLinkingPageURL VARCHAR(1000)
AS 
    BEGIN      
        UPDATE  [HelpMaster]
        SET     helpHeader = @helpheader,
				[helpcategory]=@helpcategory,
                [helpMetatag] = @helpMetatag,
                [helpDescription] = @helpDescription,
                [helpshortdesc] = @helpshortdesc,
                helpLinkingPageURL=@helpLinkingPageURL
        WHERE   numHelpId = @numHelpId       
    END   