SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetUnPaidInvoices')
DROP PROCEDURE USP_OpportunityBizDocs_GetUnPaidInvoices
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetUnPaidInvoices]
(                        
 @numDomainID AS NUMERIC(18,0),
 @numOppId AS NUMERIC(18,0)
)                        
AS 
BEGIN
	SELECT
		numOppBizDocsId,
		ISNULL(OpportunityBizDocs.monDealAmount,0) AS monDealAmount,
		ISNULL(monAmountPaid,0) AS monAmountPaid,
		ISNULL(OpportunityBizDocs.monDealAmount,0) -ISNULL(OpportunityBizDocs.monAmountPaid,0) AS monAmountToPay,
		ISNULL(OpportunityMaster.numCurrencyID,0) AS numCurrencyID,
		OpportunityMaster.numDivisionId
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityBizDocs.numOppId=@numOppId
		AND numBizDocId = 287
		AND bitAuthoritativeBizDocs=1
		AND ISNULL(OpportunityBizDocs.monDealAmount,0) -ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0
END