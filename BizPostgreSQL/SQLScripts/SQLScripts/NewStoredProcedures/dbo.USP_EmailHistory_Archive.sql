GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_Archive' ) 
    DROP PROCEDURE USP_EmailHistory_Archive
GO
CREATE PROCEDURE USP_EmailHistory_Archive
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numEmailHstrID AS VARCHAR(MAX)
AS 
BEGIN
	IF @numEmailHstrID = 'ALL'
	BEGIN
		--Moves all emails which are more than 180 days old to Email Archive folder
		UPDATE t1
			SET t1.numNodeId = ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 2 AND ISNULL(bitSystem,0) = 1),0),bitArchived=1
		FROM
			EmailHistory t1
		WHERE
			CAST(t1.dtReceivedOn AS DATE) < CAST(DATEADD(D,-180,GETDATE()) AS DATE) AND
			numNodeId = (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID=t1.numDomainID AND numUserCntID=t1.numUserCntID AND bitSystem = 1 AND numFixID = 1) AND
			ISNULL(t1.bitArchived,0) = 0
	END
	ELSE
	BEGIN
		DECLARE @numEmailArchiveNodeID NUMERIC(18,0)
		DECLARE @strSQL AS VARCHAR(MAX)

		SELECT @numEmailArchiveNodeID=ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0) = 2 AND ISNULL(bitSystem,0) = 1

		SET @strSQL = 'UPDATE EmailHistory SET bitArchived=1, numNodeId = ' + CAST(@numEmailArchiveNodeID AS VARCHAR(18)) + ' WHERE numEmailHstrID IN (' + @numEmailHstrID + ')'

		EXEC(@strSQL)
	END
END



