GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddItemToExistingInvoice')
DROP PROCEDURE USP_AddItemToExistingInvoice
GO
CREATE PROCEDURE USP_AddItemToExistingInvoice
    @numOppID NUMERIC(9),
    @numOppBizDocID NUMERIC(9),
    @numItemCode NUMERIC(9),
    @numUnitHour FLOAT,
    @monPrice DECIMAL(20,5),
    @vcItemDesc VARCHAR(1000),
--    @vcNotes VARCHAR(500),
    @numCategoryHDRID NUMERIC(9),
    @numProId numeric(9),
	@numStageId numeric(9),
	@numClassID numeric(9)
AS 
BEGIN
    DECLARE @numOppItemID NUMERIC(9)

            DECLARE @vcItemName VARCHAR(300)
            DECLARE @vcModelID VARCHAR(200)
            DECLARE @vcManufacturer VARCHAR(250)
            DECLARE @vcPathForTImage VARCHAR(300)
            SELECT  @vcItemName = vcItemName,
                    @vcModelID = vcModelID,
                    @vcManufacturer = vcManufacturer,
                    @vcPathForTImage = (SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = numItemCode AND II.numDomainId= numDomainId AND II.bitDefault=1)
            FROM    Item
            WHERE   numItemCode = @numItemCode
			
            INSERT  INTO [OpportunityItems] ( [numOppId],
                                              [numItemCode],
                                              [numUnitHour],
                                              [monPrice],
                                              [monTotAmount],
                                              [numSourceID],
                                              [vcItemDesc],
                                              [numWarehouseItmsID],
                                              [vcType],
                                              [vcAttributes],
                                              [bitDropShip],
                                              [numUnitHourReceived],
                                              [bitDiscountType],
                                              [fltDiscount],
                                              [monTotAmtBefDiscount],
                                              vcItemName,
                                              vcModelID,
                                              vcManufacturer,
                                              vcPathForTImage,numProjectID,numClassID,numProjectStageID)
            VALUES  (
                      @numOppID,
                      @numItemCode,
                      @numUnitHour,
                      @monPrice,
                      ( @monPrice * @numUnitHour ),
                      NULL,
                      @vcItemDesc,
                      NULL,
                      'Service',
                      NULL,
                      NULL,
                      NULL,
                      0,
                      0,
                      ( @monPrice * @numUnitHour ),
                      @vcItemName,
                      @vcModelID,
                      @vcManufacturer,
                      @vcPathForTImage,@numProId,@numClassID,@numStageId
                    )
           SET @numOppItemID = SCOPE_IDENTITY()

            
            IF @numCategoryHDRID>0
            BEGIN
				UPDATE dbo.TimeAndExpense SET numOppItemID=@numOppItemID,numOppId = @numOppId,numStageId=@numStageId WHERE numCategoryHDRID = @numCategoryHDRID
			END

END

