GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemImages]    Script Date: 04/13/2012 17:45:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageItemImages')
DROP PROCEDURE USP_ManageItemImages
GO
/*
BEGIN TRANSACTION
declare @p1 bigint
set @p1=0
exec USP_ManageItemImages @numItemImageId=@p1 output,@numItemCode=195142,@numDomainID=1,@vcPathForImage='i06314.jpg',@vcPathForTImage='i06314.jpg',@bitDefault=0,@intDisplayOrder=0
select @p1

ROLLBACK
*/
CREATE PROCEDURE [dbo].[USP_ManageItemImages]
(
	@numItemImageId  NUMERIC(18)=0 OUTPUT,
	@numItemCode	 NUMERIC(18),
	@numDomainId     NUMERIC(18),
	@vcPathForImage	 VARCHAR(300),
	@vcPathForTImage VARCHAR(300),
	@bitDefault	     BIT,
	@intDisplayOrder INT,
	@bitIsImage	     BIT
)

AS 

BEGIN
  
	IF ISNULL(@bitDefault,0) = 1
	BEGIN
		UPDATE ItemImages SET bitDefault = 0 WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId AND bitIsImage = 1
	END
  		
	IF ISNULL(@numItemImageId,0) = 0
	BEGIN
		IF NOT EXISTS(SELECT * FROM ItemImages WHERE numItemCode = @numItemCode AND bitIsImage = 1 AND numDomainId = @numDomainId )
		BEGIN
			SET @bitDefault = 1
		END 
	    ELSE
		BEGIN
			SET @bitDefault = 0
		END

		IF NOT EXISTS(SELECT * FROM ItemImages WHERE numItemCode = @numItemCode  AND numDomainId = @numDomainId )
		BEGIN
			SET @intDisplayOrder = 1 	
		END
		ELSE
		BEGIN
			SELECT 
				@intDisplayOrder = MAX(ISNULL(intDisplayOrder,0)) + 1 
			FROM 
				ItemImages 
			WHERE 
				numItemCode = @numItemCode 
				AND numDomainId = @numDomainId
		END
	       
		-- PRINT(@intDisplayOrder)
		
		IF((SELECT numItemCode FROM item WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId) > 0)
		BEGIN
			INSERT INTO [ItemImages] 
			( [numItemCode],[vcPathForImage],[vcPathForTImage],[bitDefault],[intDisplayOrder],[numDomainId] ,[bitIsImage])
			VALUES
			( @numItemCode,@vcPathForImage,ISNULL(@vcPathForTImage,''),@bitDefault,@intDisplayOrder,@numDomainId ,@bitIsImage)
		       
			SET @numItemImageId = SCOPE_IDENTITY()   
			--PRINT @numItemImageId
		END		  
	END
	ELSE
	BEGIN
		IF ISNULL(@vcPathForImage,'') = ''
		BEGIN
			SELECT @vcPathForImage=ISNULL(vcPathForImage,'') FROM ItemImages WHERE numItemImageId=@numItemImageId
		END

		IF ISNULL(@vcPathForTImage,'') = ''
		BEGIN
			SELECT @vcPathForTImage=ISNULL(vcPathForTImage,'') FROM ItemImages WHERE numItemImageId=@numItemImageId
		END

		UPDATE [ItemImages] SET bitDefault = @bitDefault, intDisplayOrder = @intDisplayOrder,vcPathForImage=ISNULL(@vcPathForImage,''),vcPathForTImage=ISNULL(@vcPathForTImage,'') WHERE numItemImageId = @numItemImageId    
	END

	 update item set  bintModifiedDate=getutcdate()where numItemCode=@numItemCode  AND numDomainID=@numDomainID
END

--exec USP_ManageItemImages @intDisplayOrder = 0 ,@numDomainId = 1 ,@vcPathForImage = "abc" ,@vcPathForTImage = "abc" ,@bitDefault = false ,@numItemCode = 197605