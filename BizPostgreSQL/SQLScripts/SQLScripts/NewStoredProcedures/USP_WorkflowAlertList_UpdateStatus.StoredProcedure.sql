GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkflowAlertList_UpdateStatus')
DROP PROCEDURE USP_WorkflowAlertList_UpdateStatus
GO
  
-- =============================================  
-- Author:  <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,2ndApril2014>  
-- Description: <Description,,To update Alert Message Status>  
-- =============================================  
Create PROCEDURE [dbo].[USP_WorkflowAlertList_UpdateStatus]    
 @numWFAlertID NUMERIC(18,0),  
 @intAlertStatus  INT  
AS    
  
BEGIN     
 UPDATE dbo.WorkFlowAlertList SET intAlertStatus=@intAlertStatus WHERE numWFAlertID=@numWFAlertID  
END   