--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AutoSuggestItemList')
DROP PROCEDURE USP_AutoSuggestItemList
GO
CREATE PROCEDURE [dbo].[USP_AutoSuggestItemList]   
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcSubChar as Varchar(400)                             
as     
DECLARE @vcSqlQuery VARCHAR(MAX)=''




SELECT distinct I.numItemCode,I.vcItemName,ISNULL(CASE WHEN I.[charItemType]='P' 
			THEN ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0) * W.[monWListPrice]  
			ELSE ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0) * I.monListPrice 
	   END,0) AS monListPrice,I.vcModelID,I.vcSKU,IM.vcPathForImage
FROM      
Item AS I
LEFT JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
LEFT JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
LEFT JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
LEFT JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID
OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = @numDomainID) AS W
WHERE I.numDomainID=@numDomainID  AND E.numSiteId=@numSiteId 
AND (I.numItemCode LIKE '%'+@vcSubChar+'%' OR I.numItemCode LIKE '%'+@vcSubChar+'%' 
OR I.vcItemName LIKE '%'+@vcSubChar+'%' OR I.vcModelID LIKE '%'+@vcSubChar+'%' OR I.vcSKU LIKE '%'+@vcSubChar+'%')





