
GO
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementMaster]    Script Date: 08/08/2009 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPageElementMaster')
DROP PROCEDURE USP_GetPageElementMaster
GO
CREATE PROCEDURE [dbo].[USP_GetPageElementMaster]
	@numSiteID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		[numElementID],
		[vcElementName],
		[vcUserControlPath],
		vcTagName,
		ISNULL(bitCustomization,0) bitCustomization
		,ISNULL(bitAdd,0) AS bitAdd
		,ISNULL(bitDelete,0) AS bitDelete
	FROM 
		[PageElementMaster]	
	WHERE
		(ISNULL(numSiteID,0) = 0 OR ISNULL(numSiteID,0) = @numSiteID)
	ORDER BY
		vcElementName
END

