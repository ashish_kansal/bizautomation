GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DycFieldMasterSynonym_Delete')
DROP PROCEDURE dbo.USP_DycFieldMasterSynonym_Delete
GO
CREATE PROCEDURE [dbo].[USP_DycFieldMasterSynonym_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@vcIDs VARCHAR(MAX)
)
AS 
BEGIN
	DELETE FROM DycFieldMasterSynonym WHERE numDomainID=@numDomainID AND ID IN (SELECT Id FROM dbo.SplitIDs(@vcIDs,',')) AND ISNULL(bitDefault,0) = 0
END
GO