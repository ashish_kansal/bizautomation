
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePayrollHeader')
DROP PROCEDURE USP_ManagePayrollHeader
GO
CREATE PROCEDURE [dbo].[USP_ManagePayrollHeader]
    @numDomainId numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @numPayrollHeaderID numeric(18, 0) OUTPUT,
    @numPayrolllReferenceNo numeric(18, 0),
    @dtFromDate datetime,
    @dtToDate datetime,
    @dtPaydate DATETIME,
    @strItems TEXT,
    @tintMode TINYINT=0,
    @ClientTimeZoneOffset INT,
    @numPayrollStatus INT
AS
BEGIN

	IF(SELECT COUNT(*) FROM dbo.PayrollHeader WHERE numPayrolllReferenceNo = @numPayrolllReferenceNo AND numDomainId = @numDomainId AND numPayrollHeaderID <> @numPayrollHeaderID)>0
	BEGIN
 		RAISERROR ( 'DUPLICATE',16, 1 )
 		RETURN ;
	END
	 
	IF @numPayrollHeaderID=0
	BEGIN
		
		INSERT INTO [dbo].[PayrollHeader] ([numPayrolllReferenceNo], [numDomainId], [dtFromDate], [dtToDate], [dtPaydate], [numCreatedBy], [dtCreatedDate], numPayrollStatus )
		SELECT @numPayrolllReferenceNo, @numDomainId, @dtFromDate, @dtToDate, @dtPaydate, @numUserCntID, GETUTCDATE(),@numPayrollStatus
		
		SET @numPayrollHeaderID=SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		UPDATE [dbo].[PayrollHeader]
		SET    [numPayrolllReferenceNo] = @numPayrolllReferenceNo, [dtFromDate] = @dtFromDate, [dtToDate] = @dtToDate, [dtPaydate] = @dtPaydate, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), numPayrollStatus = @numPayrollStatus
		WHERE  [numPayrollHeaderID] = @numPayrollHeaderID AND [numDomainId] = @numDomainId
		
		
			DECLARE @hDocItem INT
				IF CONVERT(VARCHAR(10), @strItems) <> '' 
				BEGIN
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
					
					SELECT  numUserCntID,decRegularHrs,decOvertimeHrs,decPaidLeaveHrs,monCommissionAmt,
					 monExpenses,monReimbursableExpenses,monDeductions,monTotalAmt INTO #temp
								FROM    ( SELECT    * FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
										WITH ( numUserCntID NUMERIC,decRegularHrs FLOAT,decOvertimeHrs FLOAT,
										decPaidLeaveHrs FLOAT,monCommissionAmt DECIMAL(20,5),monExpenses DECIMAL(20,5),
										monReimbursableExpenses DECIMAL(20,5),monDeductions DECIMAL(20,5),monTotalAmt DECIMAL(20,5))
								) X
					
					 EXEC sp_xml_removedocument @hDocItem
					 
					 UPDATE PD SET decRegularHrs=T.decRegularHrs,decOvertimeHrs=T.decOvertimeHrs,
						decPaidLeaveHrs=T.decPaidLeaveHrs,monCommissionAmt=T.monCommissionAmt,
						monExpenses=T.monExpenses,monReimbursableExpenses=T.monReimbursableExpenses,
						monDeductions=T.monDeductions,monTotalAmt=T.monTotalAmt
					 FROM PayrollDetail PD JOIN #temp T ON PD.numUserCntID=T.numUserCntID
					 WHERE PD.numDomainId=@numDomainId AND PD.numPayrollHeaderID=@numPayrollHeaderID
					 
					 INSERT INTO PayrollDetail([numPayrollHeaderID],[numDomainId],[numUserCntID]
							,[monHourlyRate],[bitOverTime],[bitOverTimeHrsDailyOrWeekly],[numLimDailHrs]
							,[monOverTimeRate],[decRegularHrs],[decOvertimeHrs],[decPaidLeaveHrs]
							,[monCommissionAmt],[monExpenses],[monReimbursableExpenses],[monDeductions],[monTotalAmt],numCheckStatus)
					 SELECT @numPayrollHeaderID,@numDomainId,T.numUserCntID,
							monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs,monOverTimeRate,
							T.decRegularHrs,T.decOvertimeHrs,T.decPaidLeaveHrs,T.monCommissionAmt,
							T.monExpenses,T.monReimbursableExpenses,T.monDeductions,T.monTotalAmt,0
					 FROM #temp T JOIN UserMaster UM ON T.numUserCntID=UM.numUserDetailId 
						WHERE UM.numDomainId=@numDomainId AND T.numUserCntID NOT IN (SELECT numUserCntID FROM PayrollDetail WHERE numDomainId=@numDomainId AND numPayrollHeaderID=@numPayrollHeaderID)
					 
					--LOGIC OF COMMISSION TYPE IS REMOVED BECAUSE WE hAVE REMOVES PROJECT COMMISSION(tintCommissionType=3) OPTION FROM GLOBAL SETTINGS
					IF @tintMode!=4
					BEGIN
						DELETE FROM PayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID
						AND ISNULL(numComissionID,0)>0
						AND numPayrollDetailID IN (SELECT numPayrollDetailID FROM PayrollDetail PD JOIN #temp T ON PD.numUserCntID=T.numUserCntID
						WHERE PD.numDomainId=@numDomainId AND PD.numPayrollHeaderID=@numPayrollHeaderID)  				 	
							
						IF @tintMode=1 --Only Paid
						BEGIN
							INSERT INTO PayrollTracking 
							(numDomainId,numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt)
							SELECT 
								@numDomainId,@numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt
							FROM 
								(SELECT 
									PD.numPayrollDetailID,numComissionID,BC.numComissionAmount - ISNULL((SELECT SUM(ISNULL(monCommissionAmt,0)) FROM PayrollTracking WHERE ISNULL(BC.numComissionID,0)=ISNULL(numComissionID,0) AND numPayrollHeaderID!=@numPayrollHeaderID),0) AS monCommissionAmt
								FROM 
									PayrollDetail PD 
								JOIN #temp T ON PD.numUserCntID=T.numUserCntID
								INNER JOIN BizDocComission BC ON BC.numUserCntId=PD.numUserCntID
								INNER JOIN OpportunityBizDocs oppBiz on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
								INNER JOIN OpportunityMaster Opp ON oppBiz.numOppId=opp.numOppId
								OUTER APPLY
								(
									SELECT 
										MAX(DM.dtDepositDate) dtDepositDate
									FROM 
										DepositMaster DM 
									JOIN 
										dbo.DepositeDetails DD
									ON 
										DM.numDepositId=DD.numDepositID 
									WHERE 
										DM.tintDepositePage=2 
										AND DM.numDomainId=@numDomainId
										AND DD.numOppID=OppBiz.numOppID 
										AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
								) TempDepositMaster
								WHERE PD.numDomainId=@numDomainId AND PD.numPayrollHeaderID=@numPayrollHeaderID AND
								Opp.numDomainId=@numDomainId AND BC.bitCommisionPaid=0 
								AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
								AND (TempDepositMaster.dtDepositDate IS NOT NULL AND dateadd(minute,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) between @dtFromDate And @dtToDate)) T
								WHERE monCommissionAmt>0
						END
						ELSE IF @tintMode=2 --Only UNPaid
						BEGIN
							INSERT INTO PayrollTracking (numDomainId,numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt)
							SELECT @numDomainId,@numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt
							FROM 
							(SELECT PD.numPayrollDetailID,numComissionID,
								BC.numComissionAmount - ISNULL((SELECT SUM(ISNULL(monCommissionAmt,0))
								FROM PayrollTracking WHERE ISNULL(BC.numComissionID,0)=ISNULL(numComissionID,0) AND numPayrollHeaderID!=@numPayrollHeaderID),0) AS monCommissionAmt
								FROM PayrollDetail PD JOIN #temp T ON PD.numUserCntID=T.numUserCntID
									INNER JOIN BizDocComission BC ON BC.numUserCntId=PD.numUserCntID
									INNER JOIN OpportunityBizDocs oppBiz on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
									INNER JOIN OpportunityMaster Opp ON oppBiz.numOppId=opp.numOppId 
									WHERE PD.numDomainId=@numDomainId AND PD.numPayrollHeaderID=@numPayrollHeaderID AND
									Opp.numDomainId=@numDomainId AND BC.bitCommisionPaid=0 
									AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
									AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtFromDate) between @dtFromDate And @dtToDate))) T
									WHERE monCommissionAmt>0
						END
						ELSE IF @tintMode=3 --Both Paid & UNPaid
						BEGIN
							INSERT INTO PayrollTracking (numDomainId,numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt)
								SELECT @numDomainId,@numPayrollHeaderID,numPayrollDetailID,numComissionID,monCommissionAmt
							FROM 
							(SELECT PD.numPayrollDetailID,numComissionID,
								BC.numComissionAmount - ISNULL((SELECT SUM(ISNULL(monCommissionAmt,0))
								FROM PayrollTracking WHERE ISNULL(BC.numComissionID,0)=ISNULL(numComissionID,0) AND numPayrollHeaderID!=@numPayrollHeaderID),0) AS monCommissionAmt
								FROM PayrollDetail PD JOIN #temp T ON PD.numUserCntID=T.numUserCntID
									INNER JOIN BizDocComission BC ON BC.numUserCntId=PD.numUserCntID
									INNER JOIN OpportunityBizDocs oppBiz on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
									INNER JOIN OpportunityMaster Opp ON oppBiz.numOppId=opp.numOppId
									OUTER APPLY
									(
										SELECT 
											MAX(DM.dtDepositDate) dtDepositDate
										FROM 
											DepositMaster DM 
										JOIN 
											dbo.DepositeDetails DD
										ON 
											DM.numDepositId=DD.numDepositID 
										WHERE 
											DM.tintDepositePage=2 
											AND DM.numDomainId=@numDomainId
											AND DD.numOppID=OppBiz.numOppID 
											AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
									) TempDepositMaster
									WHERE PD.numDomainId=@numDomainId AND PD.numPayrollHeaderID=@numPayrollHeaderID AND
									Opp.numDomainId=@numDomainId AND BC.bitCommisionPaid=0 
									AND oppBiz.bitAuthoritativeBizDocs = 1 
									AND 1 = CASE 
											WHEN isnull(oppBiz.monAmountPaid,0) < oppBiz.monDealAmount 
											THEN CASE WHEN dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtFromDate And @dtToDate THEN 1 ELSE 0 END 
											ELSE CASE WHEN TempDepositMaster.dtDepositDate IS NOT NULL AND dateadd(minute,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) between @dtFromDate And @dtToDate THEN 1 ELSE 0 END 
											END) T
									WHERE monCommissionAmt>0
						END
					END
					 
					 
					 DROP TABLE #temp
				END
	 END
END