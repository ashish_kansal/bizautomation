GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getDycModuleMaster')
DROP PROCEDURE usp_getDycModuleMaster
GO
CREATE PROCEDURE [dbo].[usp_getDycModuleMaster]              
@numDomainID as numeric(9)=0
as              
              
Select numModuleID,vcModuleName
from DycModuleMaster  
where numDomainID=@numDomainID or isnull(numDomainID,0)=0          
GO
