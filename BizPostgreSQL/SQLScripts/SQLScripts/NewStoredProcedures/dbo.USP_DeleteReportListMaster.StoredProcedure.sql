GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteReportListMaster')
DROP PROCEDURE USP_DeleteReportListMaster
GO
CREATE PROCEDURE [dbo].[USP_DeleteReportListMaster]     
    @numDomainID numeric(18, 0),
	@numReportID numeric(18, 0)
as                 

DELETE FROM ReportFieldsList WHERE numReportID=@numReportID 

DELETE FROM ReportFilterList WHERE numReportID=@numReportID 

DELETE FROM ReportSummaryGroupList WHERE numReportID=@numReportID 

--DELETE FROM ReportMatrixBreakList WHERE numReportID=@numReportID 

DELETE FROM ReportMatrixAggregateList WHERE numReportID=@numReportID 

DELETE FROM ReportListMaster WHERE numDomainID=@numDomainID AND numReportID=@numReportID
