SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdditionalContactsInformation_RollbackImport')
DROP PROCEDURE USP_AdditionalContactsInformation_RollbackImport
GO
CREATE PROCEDURE [dbo].[USP_AdditionalContactsInformation_RollbackImport]
 @numContactID numeric(9)=0
AS      
BEGIN
	DECLARE @Email AS VARCHAR(1000) 
	DECLARE @AddEmail AS VARCHAR(1000)

	SELECT @Email =vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactId=@numContactID
	SELECT @AddEmail =vcAsstEmail FROM dbo.AdditionalContactsInformation WHERE numContactId=@numContactID

	DECLARE @numDomainID AS NUMERIC(18,0) 
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitPrimaryContact BIT
	SELECT @numDomainID=[numDomainID],@numDivisionID=numDivisionID,@bitPrimaryContact=ISNULL(bitPrimaryContact,0) FROM AdditionalContactsInformation WHERE numContactID=@numContactID    


	IF EXISTS(SELECT * FROM UserMaster where numuserdetailId=@numContactID and numDomainID=@numDomainID and bitActivateFlag=1)
	BEGIN
		RAISERROR('USER', 16, 1);
	END

	IF EXISTS ( select * from [OpportunityMaster] WHERE [numContactId] =@numContactID)
	BEGIN
  		RAISERROR('CHILD_OPP', 16, 1);
	END

	IF EXISTS ( SELECT * FROM [Cases] WHERE [numContactId] = @numContactID)
	BEGIN
  		RAISERROR('CHILD_CASE', 16, 1 ) ;
		RETURN 1
	END

	BEGIN TRY
	BEGIN TRAN
		DELETE FROM dbo.CompanyAssociations WHERE numDomainID=@numDomainID AND numContactID=@numContactID
		DELETE FROM dbo.ImportActionItemReference WHERE numContactID=@numContactID
		delete ConECampaignDTL where numConECampID in (select numConEmailCampID from ConECampaign where numContactID=@numContactID)  
		delete ConECampaign where numContactID=@numContactID  
		delete AOIContactLink where numContactID=@numContactID    
		delete CaseContacts where numContactID=@numContactID   
		delete ProjectsContacts where numContactID=@numContactID  
		delete OpportunityContact where numContactID=@numContactID  
		delete UserTeams where numUserCntID=@numContactID 
		delete UserTerritory where numUserCntID=@numContactID 
		DELETE FROM dbo.UserMaster WHERE numUserDetailId = @numContactID AND bitActivateFlag=0
		DELETE AdditionalContactsInformation WHERE numContactID=@numContactID
		
		UPDATE EmailMaster SET numContactID=NULL WHERE vcEmailId = @Email AND numContactID =@numContactID
		UPDATE EmailMaster SET numContactID=NULL WHERE vcEmailID = @AddEmail AND numContactID =@numContactID
  
		IF @bitPrimaryContact = 1 AND (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID) > 0
		BEGIN
			UPDATE AdditionalContactsInformation SET bitPrimaryContact=1 WHERE numContactId=ISNULL((SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID),0)
		END

		COMMIT TRAN
    END TRY 
    BEGIN CATCH		
        IF (@@TRANCOUNT > 0) 
        BEGIN
            ROLLBACK TRAN
            DECLARE @error VARCHAR(1000)
			SET @error = ERROR_MESSAGE();
            RAISERROR(@error, 16, 1 ) ;
        END
    END CATCH	
END
GO
