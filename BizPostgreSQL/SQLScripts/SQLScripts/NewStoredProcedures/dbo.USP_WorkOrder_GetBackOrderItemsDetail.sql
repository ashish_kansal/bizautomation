GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetBackOrderItemsDetail')
DROP PROCEDURE USP_WorkOrder_GetBackOrderItemsDetail
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetBackOrderItemsDetail]
(
	@numDomainID NUMERIC(18,0)
	,@vcItems VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
	)
	SELECT
		SUBSTRING(items,0,CHARINDEX('-',items)),
		SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items))
	FROM
		dbo.Split(@vcItems,',')


	DECLARE @tintDefaultCost AS NUMERIC(18,0)
	DECLARE @bitReOrderPoint AS BIT
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0)
	DECLARE @tintOppStautsForAutoPOBackOrder AS TINYINT
	DECLARE @tintUnitsRecommendationForAutoPOBackOrder AS TINYINT
	DECLARE @bitIncludeRequisitions AS BIT

	SELECT 
		@tintDefaultCost = ISNULL(numCost,0)
		,@bitReOrderPoint=ISNULL(bitReOrderPoint,0)
		,@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0)
		,@tintOppStautsForAutoPOBackOrder=ISNULL(tintOppStautsForAutoPOBackOrder,0)
		,@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
		,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomainID


	SELECT
		@numReOrderPointOrderStatus AS numReOrderPointOrderStatus
		,@tintOppStautsForAutoPOBackOrder AS tintOppStautsForAutoPOBackOrder
		,@tintUnitsRecommendationForAutoPOBackOrder AS tintUnitsRecommendationForAutoPOBackOrder

	SELECT
		I.numItemCode
		,CONCAT(I.vcItemName, CASE WHEN dbo.fn_GetAttributes(T1.numWarehouseItemID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(T1.numWarehouseItemID,0),')') ELSE '' END) AS vcItemName
		,I.vcModelID
		,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))  ELSE ISNULL(I.vcSKU,'') END) vcSKU
		,V.vcNotes
		,I.charItemType
		,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=I.numItemCode AND bitDefault =1), '') AS vcPathForTImage
		,T1.numWarehouseItemID
		,(CASE 
			WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
			THEN 
				(CASE WHEN ISNULL(I.fltReorderQty,0) > ISNULL(v.intMinQty,0) THEN ISNULL(I.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END) 
				+ ISNULL(WI.numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																					SUM(numUnitHour) 
																				FROM 
																					OpportunityItems 
																				INNER JOIN 
																					OpportunityMaster 
																				ON 
																					OpportunityItems.numOppID=OpportunityMaster.numOppId 
																				WHERE 
																					OpportunityMaster.numDomainId=@numDomainID 
																					AND OpportunityMaster.tintOppType=2 
																					AND OpportunityMaster.tintOppStatus=0 
																					AND OpportunityItems.numItemCode=T1.numItemCode
																					AND OpportunityItems.numWarehouseItmsID=T1.numWarehouseItemID),0) ELSE 0 END))
			WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
			THEN 
				(CASE
					WHEN ISNULL(I.fltReorderQty,0) >= ISNULL(v.intMinQty,0) AND ISNULL(I.fltReorderQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(I.fltReorderQty,0)
					WHEN ISNULL(v.intMinQty,0) >= ISNULL(I.fltReorderQty,0) AND ISNULL(v.intMinQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(v.intMinQty,0)
					WHEN ISNULL(WI.numBackOrder,0) >= ISNULL(I.fltReorderQty,0) AND ISNULL(WI.numBackOrder,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(WI.numBackOrder,0)
					ELSE ISNULL(I.fltReorderQty,0)
				END) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=@numDomainID 
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=T1.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=T1.numWarehouseItemID),0) ELSE 0 END))
			WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
			THEN 
				(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
															SUM(numUnitHour) 
														FROM 
															OpportunityItems 
														INNER JOIN 
															OpportunityMaster 
														ON 
															OpportunityItems.numOppID=OpportunityMaster.numOppId 
														WHERE 
															OpportunityMaster.numDomainId=@numDomainID 
															AND OpportunityMaster.tintOppType=2 
															AND OpportunityMaster.tintOppStatus=0 
															AND OpportunityItems.numItemCode=T1.numItemCode 
															AND OpportunityItems.numWarehouseItmsID=T1.numWarehouseItemID),0) ELSE 0 END)) - ISNULL(WI.numBackOrder,0) + (CASE
					WHEN ISNULL(I.fltReorderQty,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(I.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END)
			ELSE ISNULL(WI.numBackOrder,0) + ISNULL(V.intMinQty,0) 
		END) AS numUnitHour
		,ISNULL(WI.numBackOrder,0) AS numBackOrder
		,ISNULL(I.numBaseUnit,0) AS numUOMID
		,1 AS fltUOMConversionFactor 
		,ISNULL(I.numVendorID, 0) AS numVendorID
		,CASE WHEN @tintDefaultCost = 3 THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) WHEN ISNULL(@tintDefaultCost,0) = 2 THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) ELSE ISNULL(I.monAverageCost,0) END AS monCost
		,ISNULL(V.intMinQty,0) AS intMinQty
		,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
		,ISNULL(dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
	FROM    
		@TEMP T1
	INNER JOIN
		dbo.Item I 
	ON 
		T1.numItemCode = I.numItemCode
	INNER JOIN 
		WarehouseItems WI 
	ON 
		T1.numWareHouseItemID = WI.numWareHouseItemID
	LEFT JOIN 
		Vendor V 
	ON 
		V.numVendorID = I.numVendorID 
		AND I.numItemCode = V.numItemCode
	WHERE   
		I.numDomainId = @numDomainId
		AND ISNULL(@bitReOrderPoint,0) = 1
		AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
		AND ISNULL(I.bitAssembly, 0) = 0
		AND ISNULL(I.bitKitParent, 0) = 0
END
