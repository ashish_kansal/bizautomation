GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSalesTemplateItems')
DROP PROCEDURE USP_ManageSalesTemplateItems
GO
CREATE PROCEDURE [dbo].[USP_ManageSalesTemplateItems]
    @numSalesTemplateItemID NUMERIC = 0,
    @numSalesTemplateID NUMERIC,
    @strItems TEXT,
    @numDomainID NUMERIC
AS 
    SET NOCOUNT ON
    IF @numSalesTemplateItemID = 0 
        BEGIN
            DECLARE @hDocItem INT
            IF CONVERT(VARCHAR(10), @strItems) <> '' 
                BEGIN
                    EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                    INSERT  INTO [SalesTemplateItems]
                            (
                              [numSalesTemplateID],
                              [numItemCode],
                              [numUnitHour],
                              [monPrice],
                              [monTotAmount],
                              [numSourceID],
                              [numWarehouseID],
                              [Warehouse],
                              [numWarehouseItmsID],
                              [ItemType],
                              [Attributes],
                              [AttrValues],
                              [FreeShipping],
                              [Weight],
                              [Op_Flag],
                              [bitDiscountType],
                              [fltDiscount],
                              [monTotAmtBefDiscount],
                              [ItemURL],
                              [numDomainID],
                              numUOM,
                              vcUOMName,
                              UOMConversionFactor
                                    
                            )
                            SELECT  @numSalesTemplateID,
                                    X.numItemCode,
                                    X.numUnitHour,
                                    X.monPrice,
                                    X.monTotAmount,
                                    X.numSourceID,
                                    X.numWarehouseId,
                                    X.Warehouse,
                                    X.numWarehouseItmsID,
                                    X.vcItemType,
                                    X.Attributes,
                                    X.AttrValues,
                                    X.FreeShipping,
                                    X.Weight,
                                    X.Op_Flag,
                                    X.bitDiscountType,
                                    X.fltDiscount,
                                    X.monTotAmtBefDiscount,
                                    X.ItemURL,
                                    @numDomainID,
                                    X.numUOM,
									X.vcUOMName,
									X.UOMConversionFactor
                            FROM    ( SELECT    *
                                      FROM      OPENXML (@hDocItem, '/NewDataSet/Table', 2)
                                                WITH ( numItemCode NUMERIC(18, 0), numUnitHour FLOAT, monPrice DECIMAL(20,5), monTotAmount DECIMAL(20,5), numSourceID NUMERIC(18, 0), numWarehouseId NUMERIC(18, 0), Warehouse VARCHAR(50), numWarehouseItmsID NUMERIC(18, 0), vcItemType VARCHAR(50), Attributes VARCHAR(1000), AttrValues VARCHAR(1000), FreeShipping BIT, Weight float, Op_Flag TINYINT, bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount DECIMAL(20,5), ItemURL VARCHAR(200),numUOM numeric,vcUOMName VARCHAR(50),UOMConversionFactor DECIMAL(30,16) )
                                    ) X
                            WHERE   X.numItemCode NOT IN (
                                    SELECT  numItemCode
                                    FROM    [SalesTemplateItems]
                                    WHERE   [numSalesTemplateID] = @numSalesTemplateID )
                    EXEC sp_xml_removedocument @hDocItem
                END


            SELECT  SCOPE_IDENTITY() AS InsertedID
        END			
    
    