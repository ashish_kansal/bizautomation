GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MapTransactionJournalUpdate')
	DROP PROCEDURE USP_MapTransactionJournalUpdate
GO

/****** Added By : Joseph ******/
/****** Updates Table General Journal Detail while Mapping Downloaded Transactions to existing Transactions. Set bitCleared to True if it is not Reconciled******/

CREATE PROCEDURE [dbo].[USP_MapTransactionJournalUpdate]
	@bitDeposit BIT = 0,
	@bitCheck BIT = 0,
	@Id NUMERIC(18,0)

AS

UPDATE dbo.General_Journal_Details SET bitCleared = 1 WHERE numTransactionId IN 
(
SELECT GJD.numTransactionId FROM dbo.General_Journal_Details GJD INNER JOIN 
dbo.General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id
WHERE GJH.numDepositId = @Id AND GJD.bitReconcile = 0 AND @bitDeposit = 1

UNION 

SELECT GJD.numTransactionId  FROM dbo.General_Journal_Details GJD INNER JOIN 
dbo.General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id
WHERE GJH.numCheckHeaderID = @Id AND GJD.bitReconcile = 0 AND @bitCheck = 1
)

GO