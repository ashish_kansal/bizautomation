--EXEC USP_GetAssetItem @numItemCode=173075

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAssetItem')
DROP PROCEDURE USP_GetAssetItem
GO
CREATE PROCEDURE USP_GetAssetItem
@numItemCode numeric(18)

as
begin

	select numDivId as numDivId,
		isnull(numDeptId,0) as numDeptId,
		dbo.fn_GetComapnyName(numDivId) as Company,
		dbo.fn_GetListItemName(numDeptId) as Department,
		isnull(btAppreciation,0) as btAppreciation,
		isnull(btDepreciation,0) as btDepreciation,
		isnull(numAppvalue,0) as numAppvalue,
		isnull(numDepValue,0) as numDepValue,
		dtPurchase,
		dtWarrentyTill
	from CompanyAssets CA
	where CA.numItemCode=@numItemCode

end
