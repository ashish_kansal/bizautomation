
GO
/****** Object:  StoredProcedure [dbo].[USP_SaveAPISettings]    Script Date: 05/07/2009 22:16:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveAPISettings')
DROP PROCEDURE USP_SaveAPISettings
GO
CREATE PROCEDURE [dbo].[USP_SaveAPISettings]
               @WebApiId    INT,
               @numDomainId NUMERIC(9),
               @vcValue1    VARCHAR(50)  = NULL,
               @vcValue2    VARCHAR(50)  = NULL,
               @vcValue3    VARCHAR(50)  = NULL,
               @vcValue4    VARCHAR(50)  = NULL,
               @vcValue5    VARCHAR(50)  = NULL,
               @vcValue6    VARCHAR(50)  = NULL,
               @vcValue7    VARCHAR(50)  = NULL,
                @vcValue8    VARCHAR(1000)  = NULL,
               @vcValue9    VARCHAR(50)  = NULL,
               @vcValue10   VARCHAR(50)  = NULL,
			   @vcValue11	VARCHAR(50)=NULL,
			    @vcValue12	VARCHAR(50)=NULL,
			   @bitEnableAPI bit,
				@numBizDocId NUMERIC,
				@numBizDocStatusId NUMERIC,
				@numOrderStatus NUMERIC,
				@numFBABizDocId NUMERIC,
				@numFBABizDocStatusId NUMERIC,
				@numUnShipmentOrderStatus NUMERIC,
				@numRecordOwner NUMERIC,
				@numAssignTo NUMERIC,
				@numWareHouseID NUMERIC,
				@numRelationshipId NUMERIC,
				@numProfileId NUMERIC,
				@numExpenseAccountId NUMERIC,
			    @numDiscountItemMapping NUMERIC,
				@numSalesTaxItemMapping NUMERIC,
				@bitEnableItemUpdate bit,
				@bitEnableOrderImport bit,
				@bitEnableTrackingUpdate bit,
				@bitEnableInventoryUpdate bit
AS
  BEGIN
    IF @numDomainId > 0
      BEGIN
       DECLARE @Check as INTEGER
		SELECT @Check = COUNT(*) FROM dbo.COARelationships WHERE numDomainID= @numDomainId
		AND numRelationshipID = @numRelationshipId AND ISNULL(numARAccountId,0)>0 AND ISNULL(numAPAccountId,0)>0
		IF @Check =0 AND @bitEnableAPI = 1
		BEGIN
			RAISERROR('AR_and_AP_Relationship_NotSet',16,1);
			RETURN ;
		END
		
        IF NOT EXISTS (SELECT * FROM   [WebAPIDetail] WHERE  [WebApiId] = @WebApiId AND [numDomainId] = @numDomainId)
          BEGIN
            INSERT INTO [webapidetail]
                       ([webapiid],
                        [numdomainid])
            VALUES     (@WebApiId,
                        @numDomainId)
          END
        UPDATE [WebAPIDetail]
        SET    vcFirstFldValue = @vcValue1,
               vcSecondFldValue = @vcValue2,
               vcThirdFldValue = @vcValue3,
               vcFourthFldValue = @vcValue4,
               vcFifthFldValue = @vcValue5,
               vcSixthFldValue = @vcValue6,
               vcSeventhFldValue = @vcValue7,
               vcEighthFldValue = @vcValue8,
               vcNinthFldValue = @vcValue9,
               vcTenthFldValue = @vcValue10,
			   vcEleventhFldValue = @vcValue11,
			   vcTwelfthFldValue =  @vcValue12,
			   bitEnableAPI = @bitEnableAPI,
			   numRelationshipId = @numRelationshipId,
			   numProfileId=@numProfileId,
			   numAssignTo = @numAssignTo,
			   numBizDocId=@numBizDocId,
			   numBizDocStatusId = @numBizDocStatusId,
			   numOrderStatus=@numOrderStatus,
	   		   numFBABizDocId = @numFBABizDocId,
			   numFBABizDocStatusId = @numFBABizDocStatusId,
			   numUnShipmentOrderStatus = @numUnShipmentOrderStatus,
			   numRecordOwner=@numRecordOwner,
			   numWareHouseID=@numWareHouseID,
			   numExpenseAccountId = @numExpenseAccountId,
			   numDiscountItemMapping = @numDiscountItemMapping,
			   numSalesTaxItemMapping = @numSalesTaxItemMapping,
			   bitEnableItemUpdate = @bitEnableItemUpdate,
			   bitEnableOrderImport = @bitEnableOrderImport,
			   bitEnableTrackingUpdate = @bitEnableTrackingUpdate,
			   bitEnableInventoryUpdate = @bitEnableInventoryUpdate
			   
        WHERE  [WebApiId] = @WebApiId
               AND [numDomainId] = @numDomainId
      END
  END

