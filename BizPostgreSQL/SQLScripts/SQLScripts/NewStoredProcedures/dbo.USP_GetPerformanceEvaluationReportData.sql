GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPerformanceEvaluationReportData')
DROP PROCEDURE USP_GetPerformanceEvaluationReportData
GO
CREATE PROCEDURE [dbo].[USP_GetPerformanceEvaluationReportData]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@tintProcessType TINYINT
	,@numProcessID NUMERIC(18,0)
	,@vcMilestone VARCHAR(300)
	,@numStageDetailsId NUMERIC(18,0)
	,@vcTaskIDs VARCHAR(200)
	,@vcTeams VARCHAR(200)
	,@vcEmployees VARCHAR(200)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@ClientTimeZoneOffset INT
AS                            
BEGIN
	DECLARE @TEMP TABLE
	(
		numStageDetailsId NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,vcTaskName VARCHAR(300)
		,numTaskTimeInMinutes NUMERIC(18,0)
	)

	INSERT INTO 
		@TEMP
	SELECT
		SPDT.numStageDetailsId
		,SPDT.numTaskId
		,SPDT.vcTaskName
		,(ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)
	FROM
		StagePercentageDetails SPD
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		SPD.numStageDetailsId = SPDT.numStageDetailsId
	WHERE
		SPD.numDomainID=@numDomainID
		AND SPD.slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR SPD.vcMileStoneName = @vcMilestone)
		AND (ISNULL(@numStageDetailsId,0) = 0 OR SPD.numStageDetailsId=@numStageDetailsId)
		AND (ISNULL(@vcTaskIDs,'') = '' OR SPDT.numTaskId IN (SELECT ID FROM dbo.SplitIDs(@vcTaskIDs,',')))

	DECLARE @TempTasks TABLE
	(
		numRecordID NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,numReferenceTaskId NUMERIC(18,0)
		,fltBuildQty FLOAT
		,numContactId NUMERIC(18,0)
		,numTeam NUMERIC(18,0)
		,numEstimatedTaskMinutes NUMERIC(18,0)
		,numActualTaskMinutes NUMERIC(18,0)
		,monEstimatedCost DECIMAL(20,5)
		,monActualCost DECIMAL(20,5)
	)

	INSERT INTO @TempTasks
	(
		numRecordID
		,numTaskID
		,numReferenceTaskId
		,fltBuildQty
		,numContactId
		,numTeam
		,numEstimatedTaskMinutes
		,numActualTaskMinutes
		,monEstimatedCost
		,monActualCost
	)
	SELECT
		(CASE WHEN @tintProcessType=3 THEN WO.numWOId ELSE PM.numProId END)
		,SPDT.numTaskID
		,SPDT.numReferenceTaskId
		,(CASE WHEN @tintProcessType=3 THEN ISNULL(WO.numQtyItemsReq,0) ELSE 1 END) numBuildQty
		,ACI.numContactId
		,ACI.numTeam
		,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (CASE WHEN @tintProcessType=3 THEN ISNULL(WO.numQtyItemsReq,0) ELSE 1 END)) numEstimatedTaskMinutes
		,ISNULL(TEMPTime.numTotalMinutes,0) numActualTaskMinutes
		,(((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (CASE WHEN @tintProcessType=3 THEN ISNULL(WO.numQtyItemsReq,0) ELSE 1 END)) * (ISNULL(SPDT.monHourlyRate,ISNULL(UM.monHourlyRate,0)) / 60) monEstimatedCost
		,ISNULL(TEMPTime.numTotalMinutes,0) * (ISNULL(SPDT.monHourlyRate,ISNULL(UM.monHourlyRate,0)) / 60) monActualCost
	FROM
		@TEMP T
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		T.numTaskID = SPDT.numReferenceTaskId
	INNER JOIN
		UserMaster UM
	ON
		SPDT.numAssignTo = UM.numUserDetailId
	INNER JOIN
		AdditionalContactsInformation ACI
	ON
		UM.numUserDetailId = ACI.numContactId
	LEFT JOIN
		WorkOrder WO
	ON
		SPDT.numWorkOrderId = WO.numWOId
	LEFT JOIN
		ProjectsMaster PM
	ON
		SPDT.numProjectId = PM.numProId
	CROSS APPLY
	(
		SELECT
			SUM(T.numTotalMinutes) numTotalMinutes
		FROM
		(
			SELECT 
				DATEDIFF(MINUTE,MIN(dtActionTime),MAX(dtActionTime)) AS numTotalMinutes
			FROM 
				StagePercentageDetailsTaskTimeLog SPDTTL
			WHERE 
				SPDTTL.numTaskID=SPDT.numTaskId
				AND dtActionTime BETWEEN @dtFromDate AND @dtToDate
			GROUP BY 
				CAST(dtActionTime AS DATE)
		) T
	) TEMPTime
	WHERE
		(ISNULL(@vcTeams,'') = '' OR ACI.numTeam IN (SELECT ID FROM dbo.SplitIDs(@vcTeams,',')))
		AND (ISNULL(@vcEmployees,'') = '' OR ACI.numContactId IN (SELECT ID FROM dbo.SplitIDs(@vcEmployees,',')))
		AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId AND tintAction = 4) > 0

	DECLARE @TEMPResult TABLE
	(
		numTotalRecords NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,numBuildUnits FLOAT
		,numAssignedTo NUMERIC(18,0)
		,numTeam NUMERIC(18,0)
		,vcEmployee VARCHAR(200)
		,vcTeam VARCHAR(200)
		,numOnTimeOrEarly NUMERIC(18,0)
		,monBudgetImpact DECIMAL(20,5)
		,numAvailability NUMERIC(18,0)
		,numProductivity NUMERIC(18,0)
		,fltUnits FLOAT
		,monLabourPerUnit DECIMAL(20,5)
	)

	INSERT INTO @TEMPResult
	(
		numTotalRecords
		,numTaskID
		,numBuildUnits
		,numAssignedTo
		,numTeam
		,vcEmployee
		,vcTeam
		,numOnTimeOrEarly
		,monBudgetImpact
		,fltUnits
		,monLabourPerUnit
	)
	SELECT
		COUNT(DISTINCT TT.numRecordID)
		,TT.numReferenceTaskId
		,ISNULL((SELECT SUM(fltBuildQty) FROM @TempTasks TTInner WHERE TTInner.numReferenceTaskId = TT.numReferenceTaskId),0)
		,TT.numContactId
		,TT.numTeam
		,dbo.fn_GetContactName(TT.numContactId)
		,dbo.fn_GetListItemName(TT.numTeam)
		,((SELECT 
				COUNT(*) 
			FROM 
				@TempTasks TTInner 
			WHERE 
				TTInner.numReferenceTaskId = TT.numReferenceTaskId 
				AND TTInner.numContactId = TT.numContactId 
				AND TTInner.numTeam=TT.numTeam
				AND numActualTaskMinutes <= numEstimatedTaskMinutes) * 100 / (SELECT 
																					COUNT(*) 
																				FROM 
																					@TempTasks TTInner 
																				WHERE 
																					TTInner.numReferenceTaskId = TT.numReferenceTaskId 
																					AND TTInner.numContactId = TT.numContactId 
																					AND TTInner.numTeam=TT.numTeam))
		,SUM(ISNULL(monActualCost,0) - ISNULL(monEstimatedCost,0))
		,SUM(fltBuildQty)
		,AVG(ISNULL(monActualCost,0)/fltBuildQty)
	FROM
		@TempTasks TT
	GROUP BY
		TT.numReferenceTaskId
		,TT.numContactId
		,TT.numTeam

	SELECT 
		vcMileStoneName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId = @numDomainID
		AND slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR vcMileStoneName = @vcMilestone)
	GROUP BY
		vcMileStoneName

	SELECT 
		numStageDetailsId
		,vcMileStoneName
		,vcStageName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId = @numDomainID
		AND slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR vcMileStoneName = @vcMilestone)
		AND (ISNULL(@numStageDetailsId,0) = 0 OR numStageDetailsId=@numStageDetailsId)

	SELECT * FROM @TEMP

	SELECT * FROM @TEMPResult ORDER BY numTaskID,vcEmployee

	SELECT
		ACI.numContactId
		,CONCAT(ACI.vcFirstName,' ',ACI.vcLastName) vcContctName
		,ISNULL(TEMP.numAvailabilityPercent,0) numAvailabilityPercent
		,ISNULL(TEMP.numProductivityPercent,0) numProductivityPercent
	FROM
		UserMaster UM
	INNER JOIN
		AdditionalContactsInformation ACI
	ON
		UM.numUserDetailId = ACI.numContactId
	OUTER APPLY
	(
		SELECT
			numTotalAvailableMinutes
			,numAvailabilityPercent
			,numProductiveMinutes
			,numProductivityPercent
		FROM
			dbo.GetEmployeeAvailabilityPercent(@numDomainID,ACI.numContactId,@dtFromDate,@dtToDate,@ClientTimeZoneOffset) T
	) TEMP
	WHERE
		UM.numDomainID = @numDomainID
		AND ISNULL(UM.bitActivateFlag,0) = 1

	SELECT ISNULL((SELECT SUM(numTotalRecords) FROM @TEMPResult),0) numTotalWorkOrders
END
GO