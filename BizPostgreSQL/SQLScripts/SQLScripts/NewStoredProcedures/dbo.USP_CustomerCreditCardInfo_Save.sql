GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomerCreditCardInfo_Save')
DROP PROCEDURE USP_CustomerCreditCardInfo_Save

GO
/****** Object:  StoredProcedure [dbo].[USP_AddCustomerCreditCardInfo]    Script Date: 05/07/2009 21:59:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CustomerCreditCardInfo_Save]
	@numCCInfoID NUMERIC(18,0),
    @numContactId NUMERIC(18,0),
    @vcCardHolder VARCHAR(500),
    @vcCreditCardNo VARCHAR(500),
    @vcCVV2 VARCHAR(200),
    @numCardTypeID NUMERIC(9),
    @tintValidMonth TINYINT,
    @intValidYear INT,
    @numUserCntId NUMERIC(18,0),
    @bitIsDefault BIT=0
AS
BEGIN
    IF @bitIsDefault = 1
    BEGIN
      	UPDATE CustomerCreditCardInfo SET dbo.CustomerCreditCardInfo.bitIsDefault = 0 WHERE numContactId=@numContactId AND bitIsDefault = 1
	END

	IF ISNULL(@numCCInfoID,0) > 0
	BEGIN
		UPDATE 
			CustomerCreditCardInfo
        SET    
            vcCardHolder = @vcCardHolder,
            vcCreditCardNo = @vcCreditCardNo,
            vcCVV2 = @vcCVV2,
            numCardTypeID = @numCardTypeID,
            tintValidMonth = @tintValidMonth,
            intValidYear = @intValidYear,
            numModifiedby = @numUserCntId,
            dtModified = GETUTCDATE(),
            bitIsDefault = @bitIsDefault
		WHERE 
			numCCInfoID = @numCCInfoID
	END
	ELSE 
	BEGIN
		INSERT INTO CustomerCreditCardInfo
        (
			[numContactId],
            [vcCardHolder],
            [vcCreditCardNo],
            [vcCVV2],
            [numCardTypeID],
            [tintValidMonth],
            [intValidYear],
            [numCreatedby],
            [dtCreated],
            [numModifiedby],
            [dtModified],
            bitIsDefault
		)
        VALUES     
		(
			@numContactId,
            @vcCardHolder,
            @vcCreditCardNo,
            @vcCVV2,
            @numCardTypeID,
            @tintValidMonth,
            @intValidYear,
            @numUserCntId,
            GETUTCDATE(),
            @numUserCntId,
            GETUTCDATE(),
            @bitIsDefault
		)
	END
END

