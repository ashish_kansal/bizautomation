GO
/****** Object:  StoredProcedure [dbo].[USP_GET_CUSTOM_BOXES]    Script Date: 30/07/2012 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_CUSTOM_BOXES' ) 
    DROP PROCEDURE USP_GET_CUSTOM_BOXES
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GET_CUSTOM_BOXES 
(
	@numPackageTypeID		NUMERIC(18)
)	
AS 
BEGIN

DECLARE @strSQL AS NVARCHAR(MAX)
DECLARE @strWhere AS VARCHAR(MAX) 

SET @strWhere = ' WHERE 1=1 '

IF ISNULL(@numPackageTypeID,0) <> 0
BEGIN
	SET @strWhere = @strWhere + ' AND numPackageTypeID = ' + CONVERT(VARCHAR(10),@numPackageTypeID)	
END

SET @strSQL = 'SELECT numCustomPackageID,numPackageTypeID,vcPackageName,fltWidth,fltHeight,fltLength,fltTotalWeight,
					  (CASE WHEN numPackageTypeID > 100 THEN 1 ELSE 0 END) AS IsUpdatable
               FROM dbo.CustomPackages CP ' + @strWhere + ' ORDER BY numPackageTypeID'

PRINT @strSQL
EXEC SP_EXECUTESQL @strSQL			   

END

--IF ISNULL(@intMode,0) = 1
--BEGIN
--	DELETE FROM dbo.CustomPackages WHERE numPackageTypeID = @numPackageTypeID 	
--END
--
--END

	