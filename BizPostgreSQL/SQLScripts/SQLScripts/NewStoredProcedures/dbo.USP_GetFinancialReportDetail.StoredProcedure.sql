GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetFinancialReportDetail' ) 
    DROP PROCEDURE USP_GetFinancialReportDetail
GO
--EXEC USP_GetFinancialReportDetail 0,5,1

CREATE PROC [dbo].[USP_GetFinancialReportDetail]
    @numFRDtlID NUMERIC(18, 0),
    @numFRID NUMERIC(18, 0),
    @numDomainID NUMERIC 
AS 
/*SELECT * FROM dbo.ListDetails WHERE  numListID=387*/
SELECT  [numFRDtlID],
        FRD.[numFRID],
        [numValue1],
        [numValue2],
        [vcValue1],
        FR.numFinancialDimensionID,
        CASE  dbo.GetListIemName(FR.numFinancialDimensionID)
        WHEN 'By Class' THEN dbo.GetListIemName(numValue1)
        WHEN 'By Country' THEN dbo.GetListIemName(numValue1)
        WHEN 'By Employee' THEN (SELECT vcUserName FROM dbo.UserMaster WHERE numUserDetailID = numValue1)
        WHEN 'By Item' THEN ISNULL((SELECT vcItemName FROM item WHERE numItemCode = numValue1),'')
        WHEN 'By Item Class' THEN dbo.GetListIemName(numValue1)
        WHEN 'By Lot#' THEN FRD.vcValue1
        WHEN 'By Project' THEN ISNULL((SELECT vcProjectName FROM dbo.ProjectsMaster WHERE numProID = numValue1),'')
        WHEN 'By Project Type' THEN dbo.GetListIemName(numValue1)
        WHEN 'By Relationship / Profile' THEN dbo.GetListIemName(numValue1)
        WHEN 'By State / Province' THEN ISNULL((SELECT vcState FROM dbo.State WHERE numStateID= numValue2),'')
        WHEN 'By Team' THEN dbo.GetListIemName(numValue1)
        WHEN 'By Territory' THEN dbo.GetListIemName(numValue1)
        ELSE ''
        END AS Value1,
        
        CASE dbo.GetListIemName(FR.numFinancialDimensionID)
       WHEN 'By Class' THEN dbo.GetListIemName(numValue2)
        WHEN 'By Country' THEN dbo.GetListIemName(numValue2)
        WHEN 'By Item' THEN ISNULL((SELECT vcItemName FROM item WHERE numItemCode = numValue2),'')
        WHEN 'By Item Class' THEN dbo.GetListIemName(numValue2)
        WHEN 'By Lot#' THEN FRD.vcValue1
        WHEN 'By Project' THEN ISNULL((SELECT vcProjectName FROM dbo.ProjectsMaster WHERE numProID = numValue2),'')
        WHEN 'By Project Type' THEN dbo.GetListIemName(numValue2)
        WHEN 'By Relationship / Profile' THEN dbo.GetListIemName(numValue2)
        WHEN 'By State / Province' THEN ISNULL((SELECT vcState FROM dbo.State WHERE numStateID= numValue2),'')
        WHEN 'By Team' THEN dbo.GetListIemName(numValue2)
        WHEN 'By Territory' THEN dbo.GetListIemName(numValue2)
        ELSE ''
        END AS Value2
        
FROM    [dbo].[FinancialReportDetail] FRD
INNER JOIN dbo.FinancialReport FR
ON FRD.numFRID = FR.numFRID
WHERE   (FRD.[numFRDtlID] = @numFRDtlID
          OR @numFRDtlID =0
        ) 
AND FR.numFRID = @numFRID
AND FR.numDomainID=@numDomainID


	
GO