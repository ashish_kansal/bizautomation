GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetUserClockInOutDetail')
DROP PROCEDURE USP_TimeAndExpense_GetUserClockInOutDetail
GO

CREATE PROCEDURE USP_TimeAndExpense_GetUserClockInOutDetail
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@dtFromDate DATETIME
	,@ClientTimeZoneOffset INT = 0
)
AS
BEGIN
	SELECT
		dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtFromDate),@numDomainID) vcDate
		,FORMAT(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate), 'h:mm tt', 'en-US') vcCheckedIn
		,FORMAT(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate), 'h:mm tt', 'en-US') vcCheckedOut
		,CONCAT(FORMAT(FLOOR(DATEDIFF(MINUTE,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate),DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate))/60),'00'),':',FORMAT(DATEDIFF(MINUTE,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate),DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate))%60,'00')) vcTime
		,DATEDIFF(MINUTE,dtFromDate,dtToDate) numTimeInMinutes
	FROM
		TimeAndExpense
	WHERE
		numDomainID=@numDomainID
		AND numUserCntID = @numUserCntID
		AND numCategory = 1
		AND numType = 7
		AND CAST(dtFromDate AS DATE) = CAST(@dtFromDate AS DATE)
END
GO