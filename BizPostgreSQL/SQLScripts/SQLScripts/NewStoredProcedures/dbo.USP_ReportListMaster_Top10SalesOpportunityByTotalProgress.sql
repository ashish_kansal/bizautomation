SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10SalesOpportunityByTotalProgress')
DROP PROCEDURE USP_ReportListMaster_Top10SalesOpportunityByTotalProgress
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10SalesOpportunityByTotalProgress]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	SELECT TOP 10
		OM.vcPOppName
		,CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) AS URL
		,ISNULL(monTotAmount,0) monTotAmount
		,ISNULL(OM.numPercentageComplete,0) numPercentageComplete
	FROM
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppID
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=50 
		AND (ISNULL(LD.constFlag,0)=1 OR LD.numDomainID=@numDomainID)
		AND LD.numListItemID = OM.numPercentageComplete
	WHERE
		OM.numDomainId=@numDomainID
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=0
	ORDER BY
		ISNULL(OM.numPercentageComplete,0) DESC
END
GO