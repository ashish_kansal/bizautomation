SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocItems' ) 
    DROP PROCEDURE USP_GetMirrorBizDocItems
GO
--- EXEC USP_GetMirrorBizDocItems 53,7,1
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocItems]
    (
      @numReferenceID NUMERIC(9) = NULL,
      @numReferenceType TINYINT = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 

  IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4  
            BEGIN
    			EXEC USP_MirrorOPPBizDocItems @numReferenceID,@numDomainID
END
ELSE
BEGIN
    DECLARE @DivisionID AS NUMERIC(9)                                                                                                                                                                                                                                                                                                                                            
    DECLARE @tintReturnType AS NUMERIC(9)                                                                                                                                                                                                                                                                                                                                            
    
    SELECT  @DivisionID = numDivisionID,@tintReturnType=tintReturnType
    FROM    dbo.ReturnHeader
    WHERE   numReturnHeaderID = @numReferenceID                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
    SET @numBizDocTempID = 0
        
    IF @numReferenceType = 5 OR @numReferenceType = 6 
        BEGIN
            SELECT  @numBizDocTempID = ISNULL(numRMATempID, 0)
            FROM    dbo.ReturnHeader
            WHERE   numReturnHeaderID = @numReferenceID                       
        END 
    ELSE IF @numReferenceType = 7 OR @numReferenceType = 8  OR @numReferenceType = 9  OR @numReferenceType = 10 
            BEGIN
                SELECT  @numBizDocTempID = ISNULL(numBizdocTempID, 0)
                FROM    dbo.ReturnHeader
                WHERE   numReturnHeaderID = @numReferenceID
            END  
	
    PRINT 'numBizdocTempID : ' + CONVERT(VARCHAR(10), @numBizdocTempID)
    SELECT  @numBizDocId = numBizDocId
    FROM    dbo.BizDocTemplate
    WHERE   numBizDocTempID = @numBizDocTempID
            AND numDomainID = @numDomainID
      
     IF @numReferenceType = 5 
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 6 
	 BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		
		SET @tintType = 8   
	 END
	 ELSE IF @numReferenceType = 7 
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 8
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		
		SET @tintType = 8 
	 END
	 ELSE IF @numReferenceType = 9
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		
		SET @tintType = 7   
	 END
	 ELSE IF @numReferenceType = 10
     BEGIN
		SELECT @numBizDocID =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		
		SET @tintType = 7   
	 END
														  

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT    numReturnItemID,I.vcitemname AS vcItemName,
                        0 OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        RI.vcItemDesc AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(RI.numUOMId, 0))
                        * (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) AS numUnitHourUOM,
						CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END AS numUnitHour,
                       RI.monPrice,
					   (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END)  As monUnitSalePrice,
                        (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) 
                        * (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END) AS  Amount,
                        (CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) 
                         * (CASE WHEN RI.numUnitHour = 0 THEN 0 ELSE CONVERT(DECIMAL(18, 4),ISNULL(RI.monTotAmount,0) / RI.numUnitHour) END) AS monTotAmount/*Fo calculating sum*/,
                        RH.monTotalTax AS [Tax],
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,i.numItemCode,
                        RI.numItemCode AS [numoppitemtCode],
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        0 AS numJournalId,
                        0 AS numTransactionId,
                        ISNULL(i.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(RI.numWarehouseItemID,
                                              bitSerialized) AS vcAttributes,
                        '' AS vcPartNo,
                        (CASE WHEN ISNULL(RH.monTotalDiscount,0) > 0 THEN (ISNULL(RH.monTotalDiscount,0) - RI.monTotAmount)  ELSE ((CASE WHEN @numReferenceType=5 OR @numReferenceType=6 THEN RI.numUnitHour ELSE RI.numUnitHourReceived END) * RI.monPrice) - RI.monTotAmount END) AS DiscAmt,
                        '' AS vcNotes,
                        '' AS vcTrackingNo,
                        '' AS vcShippingMethod,
                        0 AS monShipCost,
                        [dbo].[FormatedDateFromDate](RH.dtCreatedDate,
                                                     @numDomainID) dtDeliveryDate,
                        RI.numWarehouseItemID,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(RI.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        '' AS vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        0 AS DropShip,
                        ISNULL(RI.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(RI.numUOMId, RI.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        i.numShipClass,
                        [dbo].FormatedDateTimeFromDate(RH.dtCreatedDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(RH.dtCreatedDate,
                                                       @numDomainID) dtRentalReturnDate,
                       		SUBSTRING(
	(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
	FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numReturnHeaderID=RH.numReturnHeaderID and oppI.numReturnItemID=RI.numReturnItemID
	ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo
              FROM      dbo.ReturnHeader RH
                        JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
                        LEFT JOIN dbo.Item i ON RI.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = RI.numItemCode
                        LEFT JOIN UOM u ON u.numUOMId = RI.numUOMId
                        LEFT JOIN dbo.WareHouseItems WI ON RI.numWarehouseItemID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
              WHERE     RH.numReturnHeaderID = @numReferenceID
            ) X


	--SELECT * FROM #Temp1
	
    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000)
    DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SET @strSQLUpdate = ''
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID

    EXEC ('alter table #Temp1 add [TotalTax] DECIMAL(20,5)')
	EXEC ('alter table #Temp1 add [CRV] DECIMAL(20,5)')

    IF @tintReturnType = 1
	BEGIN
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numReferenceID)
            + ',numReturnItemID,2,Amount,numUnitHourUOM)'

		SET @strSQLUpdate = @strSQLUpdate
            + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
            + CONVERT(VARCHAR(20), @numReferenceID)
            + ',numReturnItemID,2,Amount,numUnitHourUOM)'
	END
    ELSE 
        IF @tintReturnType = 1
		BEGIN
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
			SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
		END
        ELSE 
            IF @tintReturnType = 1
			BEGIN
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                     + CONVERT(VARCHAR(20), @numReferenceID)
                    + ',numReturnItemID,2,Amount,numUnitHourUOM)'

				SET @strSQLUpdate = @strSQLUpdate
                    + ',[CRV]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',1,'
                     + CONVERT(VARCHAR(20), @numReferenceID)
                    + ',numReturnItemID,2,Amount,numUnitHourUOM)'
			END
            ELSE 
			BEGIN
                SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
				SET @strSQLUpdate = @strSQLUpdate + ',[CRV]= 0'
			END

    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] DECIMAL(20,5)'
                )

            IF @tintReturnType = 1
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numReferenceID) + ',numReturnItemID,2,Amount,numUnitHourUOM)'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END
	
    PRINT 'QUERY  :' + @strSQLUpdate
	
    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN  
            PRINT @vcDbColumnName  
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	  IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END
	
END 

    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
    
    END
GO
--- EXEC USP_GetMirrorBizDocItems 30,5,1