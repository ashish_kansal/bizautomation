SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RemoveCouponCode')
DROP PROCEDURE USP_RemoveCouponCode
GO
CREATE PROCEDURE [dbo].[USP_RemoveCouponCode]
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@vcSendCoupon VARCHAR(200)=0,
	@numSiteID NUMERIC(18,0) = 0
AS
BEGIN
	UPDATE
		CartItems
	SET
		PromotionID=0,
		PromotionDesc='',
		monTotAmount=monTotAmtBefDiscount,
		fltDiscount=0,
		vcCoupon='',
		bitParentPromotion=0
	WHERE
		numDomainId=@numDomainID AND
		vcCookieId=@cookieId AND
		numUserCntId=@numUserCntId AND 
		PromotionID IN (ISNULL((SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId=@numDomainID AND vcCookieId=@cookieId AND vcCoupon=@vcSendCoupon AND numUserCntId=@numUserCntId),0))

	EXEC USP_ManageECommercePromotion @numUserCntId,@numDomainId,@cookieId,0,@numSiteID
		
END