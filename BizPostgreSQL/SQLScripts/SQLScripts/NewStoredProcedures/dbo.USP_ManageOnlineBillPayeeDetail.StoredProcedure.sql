GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageOnlineBillPayeeDetail')
	DROP PROCEDURE USP_ManageOnlineBillPayeeDetail
GO

CREATE PROCEDURE [dbo].[USP_ManageOnlineBillPayeeDetail]
	@numPayeeDetailId numeric(18, 0),
	@vcPayeeFIId nchar(10),
	@vcPayeeFIListID varchar(50),
	@vcPayeeName varchar(50),
	@vcAccount varchar(50),
	@vcAddress1 varchar(350),
	@vcAddress2 varchar(350),
	@vcAddress3 varchar(350),
	@vcCity varchar(50),
	@vcState varchar(50),
	@vcPostalCode varchar(50),
	@vcPhone varchar(50),
	@vcCountry varchar(50),
	@numBankDetailId numeric(18, 0)
AS

SET NOCOUNT ON
DECLARE @Check as INTEGER 
  
IF EXISTS(SELECT [numPayeeDetailId] FROM [dbo].[OnlineBillPayeeDetails] WHERE [numPayeeDetailId] = @numPayeeDetailId)
BEGIN
	UPDATE [dbo].[OnlineBillPayeeDetails] SET
		[vcPayeeFIId] = @vcPayeeFIId,
		[vcPayeeFIListID] = @vcPayeeFIListID,
		[numBankDetailId]= @numBankDetailId,
		[vcPayeeName] = @vcPayeeName,
		[vcAccount] = @vcAccount,
		[vcAddress1] = @vcAddress1,
		[vcAddress2] = @vcAddress2,
		[vcAddress3] = @vcAddress3,
		[vcCity] = @vcCity,
		[vcState] = @vcState,
		[vcPostalCode] = @vcPostalCode,
		[vcPhone] = @vcPhone,
		[vcCountry] = @vcCountry,
		[dtModifiedDate] = GETDATE(),
		[bitIsActive] = 1
	WHERE
		[numPayeeDetailId] = @numPayeeDetailId
END
ELSE
BEGIN
SELECT @Check = COUNT(*) FROM   [OnlineBillPayeeDetails] WHERE  vcPayeeFIListID = @vcPayeeFIListID AND numBankDetailID = @numBankDetailId 
IF @Check =0  
BEGIN 

	INSERT INTO [dbo].[OnlineBillPayeeDetails] ( 
		[vcPayeeFIId],
		[vcPayeeFIListID],
		[numBankDetailId],  
		[vcPayeeName],
		[vcAccount],
		[vcAddress1],
		[vcAddress2],
		[vcAddress3],
		[vcCity],
		[vcState],
		[vcPostalCode],
		[vcPhone],
		[vcCountry],
		[dtCreatedDate],
		[bitIsActive]
	) VALUES ( 
		@vcPayeeFIId,
		@vcPayeeFIListID,
		@numBankDetailId,
		@vcPayeeName,
		@vcAccount,
		@vcAddress1,
		@vcAddress2,
		@vcAddress3,
		@vcCity,
		@vcState,
		@vcPostalCode,
		@vcPhone,
		@vcCountry,
		GETDATE(),
		1
	)
	SET  @numPayeeDetailId = SCOPE_IDENTITY()
END
END 

SELECT  @numPayeeDetailId

GO