GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageWorkFlowQueueCF' ) 
    DROP PROCEDURE USP_ManageWorkFlowQueueCF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,29thJuly2014>
-- Description: This customField change Tracking SP is used in Projects
-- =============================================
Create PROCEDURE [dbo].[USP_ManageWorkFlowQueueCF]
    @numWFQueueID numeric(18, 0)=0,
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0,
    @numFormID numeric(18, 0)=0,
    @tintProcessStatus TINYINT=1,
    @tintWFTriggerOn TINYINT,
    @tintMode TINYINT,
    @vcColumnsUpdated VARCHAR(1000)='',
    @numWFID NUMERIC(18,0)=0,
    @bitSuccess BIT=0,
    @vcDescription VARCHAR(1000)=''
    
AS 
BEGIN
	IF @tintMode=1
	BEGIN
		IF @numWFQueueID>0
		   BEGIN
	  		 UPDATE [dbo].[WorkFlowQueue] SET [tintProcessStatus] = @tintProcessStatus WHERE  [numWFQueueID] = @numWFQueueID AND [numDomainID] = @numDomainID
		   END
		ELSE
		   BEGIN
	 		 IF EXISTS(SELECT 1 FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1)
			 BEGIN
			 
			       --   PRINT 'schin'
					INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
					SELECT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, tintWFTriggerOn, numWFID
					FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1
					AND numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND tintProcessStatus IN(1,2))
					
					IF @tintWFTriggerOn=2 AND LEN(@vcColumnsUpdated)>0
					BEGIN
						SET @tintWFTriggerOn=4
						--PRINT 'dd'
						INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
						SELECT DISTINCT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, @tintWFTriggerOn, WF.numWFID
						FROM dbo.WorkFlowMaster WF JOIN WorkFlowTriggerFieldList WTFL ON WF.numWFID=WTFL.numWFID
						JOIN dbo.CFW_Fld_Master CFM ON WTFL.numFieldID=CFM.Fld_id 
						WHERE WF.numDomainID=@numDomainID AND WF.numFormID=@numFormID AND WF.tintWFTriggerOn=4 AND WF.bitActive=1 AND WTFL.bitCustom=1
						AND CFM.Fld_label IN (SELECT Items FROM dbo.Split(@vcColumnsUpdated,','))					
						AND WF.numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND tintWFTriggerOn=4 AND tintProcessStatus IN(1,2))
					END
			 END
			 --Added By:Sachin Sadhu||Date:10thFeb2014
			 --Description:@tintWFTriggerOn value is  coming from Trigger,I=1,U=2,D=3
			 --case of Field(s) Update
			 IF EXISTS(SELECT 1 FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=4)) AND bitActive=1)
			 BEGIN
			-- PRINT 'schindas'
					INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
					SELECT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, tintWFTriggerOn, numWFID
					FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1
					AND numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND tintProcessStatus IN(1,2))
					
					IF @tintWFTriggerOn=2 AND LEN(@vcColumnsUpdated)>0
					BEGIN
						SET @tintWFTriggerOn=4
					--print 'tu te'
						INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
						SELECT DISTINCT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, @tintWFTriggerOn, WF.numWFID
						FROM dbo.WorkFlowMaster WF JOIN WorkFlowTriggerFieldList WTFL ON WF.numWFID=WTFL.numWFID
						 JOIN dbo.CFW_Fld_Master CFM ON WTFL.numFieldID=CFM.Fld_id 
						WHERE WF.numDomainID=@numDomainID AND WF.numFormID=@numFormID AND WF.tintWFTriggerOn=4 AND WF.bitActive=1 AND WTFL.bitCustom=1
						AND CFM.Fld_label IN (SELECT items FROM dbo.Split(@vcColumnsUpdated,','))					
						AND WF.numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND tintWFTriggerOn=4 AND tintProcessStatus IN(1,2))
					END
			 END
			 --End of Script:Sachin
		  END
	 END
	  ELSE IF @tintMode = 2
        BEGIN
			INSERT INTO [dbo].[WorkFlowQueueExecution] ([numWFQueueID], [numWFID], [dtExecutionDate], [bitSuccess], [vcDescription])
			SELECT @numWFQueueID, @numWFID, GETUTCDATE(), @bitSuccess, @vcDescription
        END    
END


