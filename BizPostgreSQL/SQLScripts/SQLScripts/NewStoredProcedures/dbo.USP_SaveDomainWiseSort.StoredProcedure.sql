    
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Tuesday, September 02, 2008>  
-- Description: <This procedure is used for Store Module's Sorting Order according to Doamin ID>  
-- =============================================  

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveDomainWiseSort')
DROP PROCEDURE USP_SaveDomainWiseSort
GO
CREATE PROCEDURE [dbo].[USP_SaveDomainWiseSort]   
@vcModuleType as varchar(50),
@vcSortOn as varchar(50),
@numCreatedAndModifiedBY as numeric(18, 0),
@bintCreatedAndModifiedDate as datetime,
@numDomainId as numeric(18, 0)
as
declare  @numRecExist numeric(9)
begin
	select @numRecExist=count(*) from DomainWiseSort where vcModuleType=@vcModuleType and numDomainId=@numDomainId;
	
	if( @numRecExist <> 0 )
		begin
			update DomainWiseSort set vcSortOn=@vcSortOn, numModifiedBy=@numCreatedAndModifiedBY,
			bintModifiedDate=@bintCreatedAndModifiedDate 
			where vcModuleType=@vcModuleType and numDomainId=@numDomainId;
		end
	else
		begin
			insert into DomainWiseSort(vcModuleType , vcSortOn, numCreatedBY, bintCreatedDate, numDomainId)
			values (@vcModuleType , @vcSortOn, @numCreatedAndModifiedBY, @bintCreatedAndModifiedDate, @numDomainId);
		end
end
go