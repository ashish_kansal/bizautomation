GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PackagingRules_DeleteAll')
DROP PROCEDURE USP_PackagingRules_DeleteAll
GO

-- =============================================
-- Author:		Sachin Sadhu
-- Create date: 31stJult2014
-- Description:	To Delete Packaging rules on the Basis of Shipping Rule ID
-- =============================================
Create PROCEDURE [dbo].[USP_PackagingRules_DeleteAll]
	-- Add the parameters for the stored procedure here
	@numDomainID numeric(18, 0),
	@numShippingRuleID numeric(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Delete from PackagingRules where numDomainID=@numDomainID and numShippingRuleID=@numShippingRuleID
END

