SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_ActionItemPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_ActionItemPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_ActionItemPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@bitTask VARCHAR(MAX)
AS
BEGIN 
	DECLARE @TEMP TABLE
	(
		ID NUMERIC(18,0)
		,vcOrganizationName VARCHAR(300)
		,vcComments NVARCHAR(MAX)
		,vcType VARCHAR(50)
		,dtDate DATETIME
		,tintType TINYINT
	)

	INSERT INTO @TEMP
	(
		ID
		,vcOrganizationName
		,vcComments
		,vcType
		,dtDate
		,tintType
	)
	SELECT
		Communication.numCommId
		,CompanyInfo.vcCompanyName
		,ISNULL(Communication.textDetails,'')
		,ISNULL(LD.vcData,'')
		,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartTime)
		,1
	FROM
		Communication                               
	INNER JOIN 
		AdditionalContactsInformation
	ON 
		Communication.numContactId = AdditionalContactsInformation.numContactId                                  
	INNER JOIN 
		DivisionMaster                                                                         
	ON 
		AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID                                   
	INNER JOIN 
		CompanyInfo                                                                         
	ON 
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID = 73
		AND (LD.numDomainID=@numDomainID OR LD.constFlag = 1)
		AND LD.numListItemID = Communication.bitTask
	WHERE
		Communication.numDomainID = @numDomainID
		AND AdditionalContactsInformation.numDomainID=@numDomainID
		AND DivisionMaster.numDomainID=@numDomainID
		AND CompanyInfo.numDomainID=@numDomainID
		AND Communication.bitclosedflag=0                                                
		AND Communication.bitTask <> 973
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtEndTime) AS DATE) = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)
		AND 1= (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN (Communication.numAssign  = @numUserCntID  OR Communication.numCreatedBy = @numUserCntID) THEN 1 ELSE 0 END) ELSE 1 END)


	INSERT INTO @TEMP
	(
		ID
		,vcOrganizationName
		,vcComments
		,vcType
		,dtDate
		,tintType
	)
	SELECT
		Activity.Activityid
		,Comp.vcCompanyName
		,ISNULL(Activity.ActivityDescription,'')
		,'Calendar'
		,DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc)
		,2
	FROM 
		Activity 
	INNER JOIN             
		ActivityResource 
	ON 
		Activity.ActivityID=ActivityResource.ActivityID
	INNER JOIN 
		[Resource] Res 
	ON 
		ActivityResource.ResourceID=Res.ResourceID                                   
	INNER JOIN 
		AdditionalContactsInformation ACI 
	ON 
		ACI.numContactId = Res.numUserCntId                
	INNER JOIN 
		DivisionMaster Div                                                          
	ON 
		ACI.numDivisionId = Div.numDivisionID   
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numDivisionId = div.numDivisionID 
	INNER JOIN
		Communication
	ON
		Communication.numDivisionId = div.numDivisionID                                
	INNER JOIN 
		CompanyInfo Comp                             
	ON 
		Div.numCompanyID = Comp.numCompanyId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND Res.numDomainId =@numDomainID
		AND 1 = (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN Res.numUserCntId = @numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
		AND ACI.numDomainID=@numDomainID
		AND Div.numDomainID=@numDomainID
		AND Comp.numDomainID=@numDomainID
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) AS DATE) = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)
		--AND DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) >= DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AND DATEADD(MINUTE,-@ClientTimeZoneOffset,startdatetimeutc) < CAST(CONVERT(CHAR(8), GETUTCDATE(), 112) + ' 00:00:00.00' AS DATETIME)
		AND [status] <> -1                                                     
		AND Activity.activityid NOT IN (SELECT DISTINCT(numActivityId) FROM Communication WHERE numDomainID=@numDomainID AND 1 = (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN (numAssign =@numUserCntID OR numCreatedBy=@numUserCntID) THEN 1 ELSE 0 END) ELSE 1 END))
		AND 1 = (CASE WHEN LEN(ISNULL(@vcTeritorry,'')) > 0 THEN (CASE WHEN Div.numTerID IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE 
					WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)
					THEN 
						(CASE @vcFilterBy
							WHEN 1  -- Assign To
							THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 2  -- Record Owner
							THEN (CASE WHEN OpportunityMaster.numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 3  -- Teams (Based on Assigned To)
							THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
						END)
					ELSE 1 
				END)
		AND 1 = (CASE WHEN LEN(ISNULL(@bitTask,'')) > 0 THEN (CASE WHEN Communication.bitTask IN (SELECT Id FROM dbo.SplitIDs(@bitTask,',')) THEN 1 ELSE 0 END) ELSE 1 END)
		

	--INSERT INTO @TEMP
	--(
	--	ID
	--	,vcOrganizationName
	--	,vcComments
	--	,vcType
	--	,dtDate
	--	,tintType
	--)
	--SELECT
	--	(CASE BAD.btDocType WHEN 2 THEN BAD.numOppBizDocsId WHEN 3 THEN BAD.numOppBizDocsId ELSE BDA.numBizActionID END)
	--	,CI.vcCompanyName
	--	,ISNULL(BAD.vcComment,'')
	--	,(CASE BAD.btDocType WHEN 2  THEN  'Document Approval Request' ELSE 'BizDoc Approval Request' END)
	--	,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtCreatedDate)
	--	,2
	--FROM
	--	BizDocAction BDA 
	--INNER JOIN
	--	AdditionalContactsInformation ACI
	--ON
	--	BDA.numContactId = ACI.numContactId
	--INNER JOIN
	--	DivisionMaster DM
	--ON
	--	ACI.numDivisionId = DM.numDivisionID
	--INNER JOIN
	--	CompanyInfo CI
	--ON
	--	DM.numCompanyID = CI.numCompanyId
	--LEFT JOIN 
	--	dbo.BizActionDetails BAD
	--ON 
	--	BDA.numBizActionId = BAD.numBizActionId
	--WHERE
	--	BDA.numDomainId =@numDomainID
	--	AND 1 = (CASE WHEN ISNULL(@numUserCntID,0) > 0 THEN (CASE WHEN BDA.numContactId = @numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
	--	AND ACI.numDomainID=@numDomainID
	--	AND DM.numDomainID=@numDomainID
	--	AND CI.numDomainID=@numDomainID
	--	AND BDA.numStatus=0 
	--	AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtCreatedDate) AS DATE) = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)
	
	SELECT *,dbo.FormatedDateTimeFromDate(dtDate,@numDOmainID) AS dtFormatted FROM @TEMP ORDER BY dtDate ASC
END
GO