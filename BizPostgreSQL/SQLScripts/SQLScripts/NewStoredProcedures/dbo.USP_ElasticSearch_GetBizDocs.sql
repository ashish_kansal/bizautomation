GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetBizDocs')
DROP PROCEDURE dbo.USP_ElasticSearch_GetBizDocs
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetBizDocs]
	@numDomainID NUMERIC(18,0)
	,@vcOppBizDocIds VARCHAR(MAX)
AS 
BEGIN
	
	SELECT 
		numOppBizDocsId AS id
		,OpportunityMaster.numOppId
		,'bizdoc' AS module
		,'' AS url
		,ISNULL(tintOppType,0) tintOppType 
		,ISNULL(tintOppStatus,0) tintOppStatus
		,ISNULL(OpportunityMaster.numAssignedTo,0) numAssignedTo
		,ISNULL(DivisionMaster.numTerID,0) numTerID
		,ISNULL(OpportunityMaster.numRecOwner,0) numRecOwner
		,CONCAT(CASE WHEN tintOppType=2 THEN '<b style="color:#ff0000">' ELSE '<b style="color:#00aa50">' END, vcData, ' (BizDoc):</b> ' ,vcBizDocID,', ', vcCompanyName,', ',OpportunityBizDocs.monDealAmount) AS displaytext
		,vcBizDocID AS [text]
		,ISNULL(vcBizDocID,'') AS Search_vcBizDocID
	FROM
		OpportunityBizDocs WITH (NOLOCK)
	INNER JOIN
		ListDetails WITH (NOLOCK)
	ON
		OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
		AND ListDetails.numListID=27
	INNER JOIN
		OpportunityMaster WITH (NOLOCK)
	ON
		OpportunityBizDocs.numOppID=OpportunityMaster.numOppID
	INNER JOIN
		DivisionMaster WITH (NOLOCK)
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo WITH (NOLOCK)
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND (@vcOppBizDocIds IS NULL OR numOppBizDocsId IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcOppBizDocIds,'0'),',')))
END