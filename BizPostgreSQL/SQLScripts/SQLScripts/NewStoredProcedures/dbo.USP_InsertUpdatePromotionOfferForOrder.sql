SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertupdatepromotionofferfororder')
DROP PROCEDURE usp_insertupdatepromotionofferfororder
GO
CREATE PROCEDURE [dbo].[USP_InsertUpdatePromotionOfferForOrder]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATETIME,
    @dtValidTo AS DATETIME,
	@bitNeverExpires BIT,
	@bitUseForCouponManagement BIT,
	@bitRequireCouponCode BIT,
	@txtCouponCode VARCHAR(MAX),
	@tintUsageLimit TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	@strOrderAmount VARCHAR(MAX),
	@strfltDiscount VARCHAR(MAX),
	@tintDiscountType TINYINT
AS 
BEGIN

	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			,numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion 
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,0
		   ,1
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN	
		IF ISNULL(@bitRequireCouponCode,0) = 1 AND (SELECT 
															COUNT(*) 
														FROM 
															PromotionOffer POInner 
														INNER JOIN 
															DiscountCodes DCInner 
														ON 
															POInner.numProId=DCInner.numPromotionID 
														WHERE 
															POInner.numDomainId=@numDomainId 
															AND ISNULL(POInner.IsOrderBasedPromotion,0) = 1
															AND DCInner.numPromotionID <> @numProId 
															AND DCInner.vcDiscountCode IN (SELECT OutParam FROM dbo.SplitString(@txtCouponCode,','))) > 0
		BEGIN
			RAISERROR ( 'DUPLICATE_COUPON_CODE',16, 1 )
 			RETURN;
		END
		
		IF @tintCustomersBasedOn = 2
		BEGIN
			IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
			BEGIN
				UPDATE PromotionOffer SET bitEnabled=0 WHERE numProId=@numProId
				RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 				RETURN;
			END
		END

		IF (
			SELECT
				COUNT(*)
			FROM
				PromotionOffer
			LEFT JOIN
				PromotionOfferOrganizations
			ON
				PromotionOffer.numProId = PromotionOfferOrganizations.numProId
				AND 1 = (CASE 
							WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
							WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
							ELSE 1
						END)
			WHERE
				PromotionOffer.numDomainId=@numDomainID
				AND PromotionOffer.numProId <> @numProId
				AND tintCustomersBasedOn = @tintCustomersBasedOn 
				AND ISNULL(bitEnabled,0) = 1			 
				AND 1 = (CASE 							
							WHEN @tintCustomersBasedOn=2 AND IsOrderBasedPromotion = 1 THEN
								(CASE 
									WHEN 
										(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 AND ISNULL(@bitRequireCouponCode,0) = 0
									THEN 
										1 
									ELSE 
										0 
								END)
							
						END)
			) > 0
		BEGIN
			UPDATE PromotionOffer SET bitEnabled=0 WHERE numProId=@numProId
			RAISERROR('RELATIONSHIPPROFILE ALREADY EXISTS',16,1)
			RETURN
		END

		UPDATE 
			PromotionOffer
		SET 
			vcProName = @vcProName
			,numDomainId = @numDomainId
			,dtValidFrom = @dtValidFrom
			,dtValidTo = @dtValidTo
			,bitNeverExpires = @bitNeverExpires
			,bitRequireCouponCode = @bitRequireCouponCode
			,bitUseForCouponManagement=@bitUseForCouponManagement			
			,numModifiedBy = @numUserCntID
			,dtModified = GETUTCDATE()
			,tintCustomersBasedOn = @tintCustomersBasedOn
			,tintDiscountType = @tintDiscountType
		WHERE 
			numProId=@numProId

		IF (SELECT COUNT(*) FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId) > 0
		BEGIN
			DELETE FROM PromotionOfferOrder WHERE numPromotionID = @numProId AND numDomainId = @numDomainId
		END

		IF ISNULL(@bitUseForCouponManagement,0) = 0
		BEGIN
			DECLARE @Tempdata TABLE
			(
				numPromotionID NUMERIC,
				numDomainID NUMERIC,
				numOrderAmount VARCHAR(MAX),
				fltDiscountValue VARCHAR(MAX)
			)

			INSERT @Tempdata SELECT @numProId, @numDomainID, @strOrderAmount, @strfltDiscount

			;WITH tmp(numPromotionID, numDomainID, numOrderAmount, OrderAmount, numfltDiscountValue,fltDiscountValue) AS
			(
				SELECT
					numPromotionID,
					numDomainID,
					LEFT(numOrderAmount, CHARINDEX(',', numOrderAmount + ',') - 1),
					STUFF(numOrderAmount, 1, CHARINDEX(',', numOrderAmount + ','), ''),
					LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
					STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
				FROM @Tempdata
				UNION all

				SELECT
					numPromotionID,
					numDomainID,
					LEFT(OrderAmount, CHARINDEX(',', OrderAmount + ',') - 1),
					STUFF(OrderAmount, 1, CHARINDEX(',', OrderAmount + ','), ''),
					LEFT(fltDiscountValue, CHARINDEX(',', fltDiscountValue + ',') - 1),
					STUFF(fltDiscountValue, 1, CHARINDEX(',', fltDiscountValue + ','), '')
				FROM tmp
				WHERE
					OrderAmount > '' AND fltDiscountValue > ''
			)

			INSERT INTO PromotionOfferOrder
			SELECT
				numPromotionID,				
				numOrderAmount,
				numfltDiscountValue,
				numDomainID

			FROM tmp
		END
				
		IF @bitRequireCouponCode = 1
		BEGIN
			DECLARE @DiscountTempdata TABLE
			(
				numPromotionID NUMERIC,
				CodeUsageLimit TINYINT,
				vcDiscountCode VARCHAR(MAX)			
			)

			INSERT INTO @DiscountTempdata
			(
				numPromotionID,
				CodeUsageLimit,
				vcDiscountCode
			)
			SELECT 
				@numProId
				,@tintUsageLimit
				,OutParam 
			FROM 
				dbo.SplitString(@txtCouponCode,',')


			DELETE FROM 
				DiscountCodes 
			WHERE 
				numPromotionID = @numProId 
				AND vcDiscountCode NOT IN (SELECT vcDiscountCode FROM @DiscountTempdata)
				AND ISNULL((SELECT COUNT(*) FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DiscountCodes.numDiscountId),0) = 0
			 
			INSERT INTO DiscountCodes
			(
				numPromotionID
				,vcDiscountCode
				,CodeUsageLimit
			)
			SELECT
				tmp1.numPromotionID,				
				tmp1.vcDiscountCode,
				tmp1.CodeUsageLimit
			FROM 
				@DiscountTempdata tmp1
			WHERE 
				tmp1.vcDiscountCode NOT IN (SELECT vcDiscountCode FROM DiscountCodes WHERE numPromotionID = @numProId)

			UPDATE DiscountCodes SET CodeUsageLimit = @tintUsageLimit WHERE numPromotionID = @numProId
		END	

        SELECT  @numProId
	END
END


