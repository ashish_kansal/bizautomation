GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_PromotionOffer_GetCustomerSelections' ) 
    DROP PROCEDURE USP_PromotionOffer_GetCustomerSelections
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_GetCustomerSelections]    
	@numProId AS NUMERIC(18,0),
	@numDomainID as numeric(9)=0,  
	@tintCustomerSelectionType AS TINYINT
AS
BEGIN
	IF @tintCustomerSelectionType = 1
	BEGIN
		SELECT numProOrgId, numProId, vcCompanyName,
		dbo.[GetListIemName](dbo.CompanyInfo.numCompanyType) +',' + dbo.[GetListIemName](dbo.CompanyInfo.vcProfile) Relationship,
		(SELECT ISNULL(vcFirstName,'') + ' ' + ISNULL(vcLastName,'') + ', ' + ISNULL(vcEmail,'')  
			FROM [AdditionalContactsInformation] A 
			WHERE A.[numDivisionID]=[DivisionMaster].[numDivisionID] 
				AND ISNULL(A.bitPrimaryContact,0)=1) AS PrimaryContact  
			FROM DivisionMaster  
				JOIN Companyinfo ON DivisionMaster.numCompanyID = Companyinfo.numCompanyID  
				JOIN PromotionOfferOrganizations POO ON POO.numDivisionID = DivisionMaster.numDivisionID  
			WHERE DivisionMaster.numDomainID = @numDomainID AND numProId = @numProId AND tintType = 1   
		 
	END
	ELSE IF @tintCustomerSelectionType = 2
	BEGIN
		SELECT numProOrgId, numProId, L1.vcData+' - '+L2.vcData AS RelProfile FROM PromotionOfferOrganizations POO
			JOIN ListDetails L1 ON L1.numListItemID = POO.numRelationship 
			JOIN ListDetails L2 ON L2.numListItemID = POO.numProfile  
		WHERE numProId = @numProId 
			AND tintType = 2
	END
END 
