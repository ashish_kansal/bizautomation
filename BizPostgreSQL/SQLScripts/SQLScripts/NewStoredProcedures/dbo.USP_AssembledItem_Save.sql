SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AssembledItem_Save')
DROP PROCEDURE USP_AssembledItem_Save
GO
CREATE PROCEDURE [dbo].[USP_AssembledItem_Save]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@numQty NUMERIC(18,0)
AS
BEGIN
	INSERT INTO AssembledItem
	(
		numDomainID,
		numItemCode,
		numWarehouseItemID,
		numAssembledQty,
		numCreatedBy,
		dtCreatedDate
	)
	VALUES
	(
		@numDomainID,
		@numItemCode,
		@numWarehouseItemID,
		@numQty,
		@numUserCntID,
		GETUTCDATE()
	)

	SELECT SCOPE_IDENTITY()
END

