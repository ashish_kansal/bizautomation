GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPIErrorLog_Insert' ) 
    DROP PROCEDURE USP_BizAPIErrorLog_Insert
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 10 April 2014
-- Description:	Logs biz api errors
-- =============================================
CREATE PROCEDURE USP_BizAPIErrorLog_Insert
	@Type NVARCHAR(100),
	@Source NVARCHAR(100),
	@Message NVARCHAR(MAX),
    @StackStrace NVARCHAR(MAX),
    @DomainID NUMERIC(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO BizAPIErrorLog 
	(
		vcType,
		vcSource,
		vcMessage,
		vcStackStrace,
		numDomainID,
		CreatedDate
	) 
	OUTPUT INSERTED.vcErrorID
	VALUES 
	(
		@Type,
		@Source,
		@Message,
		@StackStrace,
		@DomainID,
		GETDATE()
	)
END
GO