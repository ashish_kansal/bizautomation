GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteFinancialReportDetail' ) 
    DROP PROCEDURE USP_DeleteFinancialReportDetail
GO
CREATE PROC [dbo].[USP_DeleteFinancialReportDetail]
    @numFRDtlID NUMERIC(18, 0)
AS 
DELETE  FROM [dbo].[FinancialReportDetail]
WHERE   [numFRDtlID] = @numFRDtlID 
GO
