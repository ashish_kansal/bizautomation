/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItems_GetMaxID')
DROP PROCEDURE USP_OpportunityItems_GetMaxID
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItems_GetMaxID] 

AS
BEGIN
	SELECT MAX(numOppitemtcode) + 1 AS numOppItemID FROM OpportunityItems
END
GO

