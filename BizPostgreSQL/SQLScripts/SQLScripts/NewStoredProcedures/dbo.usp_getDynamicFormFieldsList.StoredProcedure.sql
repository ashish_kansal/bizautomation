SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getDynamicFormFieldsList')
DROP PROCEDURE usp_getDynamicFormFieldsList
GO
CREATE PROCEDURE [dbo].[usp_getDynamicFormFieldsList]  
 @numDomainID NUMERIC(9),
 @numFormId NUMERIC(9),
 @tintMode TINYINT=0
AS  
BEGIN  

IF @tintMode=0
BEGIN
 select DFFM.numFormID,DFM.numFieldID as numFormFieldId,isnull(DFFM.vcFieldName,DFM.vcFieldName) as vcFormFieldName,
 ISNULL(DFV.vcNewFormFieldName,'') AS vcNewFormFieldName,
 isnull(DFFM.vcAssociatedControlType,DFM.vcAssociatedControlType) as vcAssociatedControlType,
ISNULL(DFMMaster.tintFlag,0) AS tintFlag,
  ISNULL(DFV.bitIsRequired,0) AS bitIsRequired,ISNULL(DFV.bitIsEmail,0) AS bitIsEmail,ISNULL(DFV.bitIsAlphaNumeric,0) AS bitIsAlphaNumeric,ISNULL(DFV.bitIsNumeric,0) AS bitIsNumeric,ISNULL(DFV.bitIsLengthValidation,0) AS bitIsLengthValidation,
  ISNULL(DFV.vcToolTip,ISNULL(DFM.vcToolTip,'')) AS vcToolTip
 FROM DycFormField_Mapping DFFM 
JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
left join DynamicFormField_Validation DFV on DFV.numDomainID=@numDomainID AND DFM.numFieldID=DFV.numFormFieldId AND DFV.numFormId=DFFM.numFormId
LEFT JOIN DynamicFormMaster DFMMaster ON DFFM.numFormId=DFMMaster.numFormId
where DFFM.numFormID=@numFormId AND ISNULL(DFM.bitDeleted,0)=0 ORDER BY vcFormFieldName
END

ELSE IF @tintMode=1
BEGIN
 select numFormID,CAST(numFieldID AS VARCHAR(10)) +'~'+ vcAssociatedControlType +'~'+ vcOrigDbColumnName +'~'+ ISNULL(vcListItemType,'') +'~'+ CAST(ISNULL(numListID,0) AS VARCHAR(10)) AS numFormFieldId,vcFieldName as vcFormFieldName
 FROM View_DynamicDefaultColumns
where numDomainID=@numDomainID AND numFormID=@numFormId AND ISNULL(bitAllowGridColor,0)=1 ORDER BY vcFormFieldName
END
END
GO
