/****** Object:  StoredProcedure [dbo].[usp_BusinessProcessList]    Script Date: 07/26/2008 16:14:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetBusinessProcessListConfiguration')
DROP PROCEDURE usp_GetBusinessProcessListConfiguration
GO
CREATE PROCEDURE [dbo].[usp_GetBusinessProcessListConfiguration] 
@numDomainId as numeric(9),
@Mode as tinyint,
@numSlpId as numeric(9)
as 

select * from Sales_process_List_Master 
where Pro_Type=@Mode and numDomainID= @numDomainId and slp_id=@numSlpId
GO
