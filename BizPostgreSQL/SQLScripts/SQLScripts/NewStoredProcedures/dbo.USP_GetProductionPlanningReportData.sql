GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProductionPlanningReportData')
DROP PROCEDURE USP_GetProductionPlanningReportData
GO
CREATE PROCEDURE [dbo].[USP_GetProductionPlanningReportData]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@tintProcessType TINYINT
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@numPageIndex INT
	,@numPageSize INT
	,@ClientTimeZoneOffset INT
AS                            
BEGIN
	DECLARE @TempData TABLE
	(
		[id] VARCHAR(50)
		,[text] VARCHAR(1000)
		,[start_date] DATETIME
		,[end_date] DATETIME
		,[duration] FLOAT
		,[progress] FLOAT
		,[sortorder] INT
		,[parent] VARCHAR(50)
		,[open] BIT
		,[color] VARCHAR(50)
		,[progressColor] VARCHAR(50)
		,numWOID NUMERIC(18,0)
		,vcMilestone VARCHAR(500)
		,numStageDetailsId NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,vcItem VARCHAR(300)
		,dtRequestedFinish VARCHAR(20)
		,dtProjectedFinish VARCHAR(20)
		,dtItemReleaseDate VARCHAR(20)
		,dtActualStartDate VARCHAR(20)
		,vcDurationHours VARCHAR(20)
	)

	DECLARE @TempLinks TABLE
	(
		[id] INT IDENTITY(1,1)
		,[source] VARCHAR(50)
		,[target] VARCHAR(50)
		,[type] INT
	)

	INSERT INTO @TempData
	(
		[id]
		,[text]
		,[start_date]
		,[duration]
		,[progress]
		,[sortorder]
		,[parent]
		,[open]
		,numWOID
		,vcItem
		,dtRequestedFinish
		,dtProjectedFinish
		,dtItemReleaseDate
		,dtActualStartDate
	)
	SELECT TOP 10
		CONCAT('WO',WorkOrder.numWOId)
		,CONCAT(ISNULL(WorkOrder.vcWorkOrderName,'-'),' (',dbo.GetTotalProgress(@numDomainID,WorkOrder.numWOId,1,1,'',0),') '
				,' <a href="javascript:void(0);" type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="right" data-html="true" title="<table><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Item (Qty):</td><td style=''white-space:nowrap;text-align:left;''>',CONCAT(Item.vcItemName,' (',ISNULL(WorkOrder.numQtyItemsReq,0),')'),'</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Requested Finish:</td><td style=''white-space:nowrap;text-align:left;''>',dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,WorkOrder.dtmEndDate),@numDomainID),'</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Projected Finish:</td><td style=''white-space:nowrap;text-align:left;''>',dbo.FormatedDateFromDate(dbo.GetProjectedFinish(@numDomainID,2,WorkOrder.numWOID,0,0,NULL,@ClientTimeZoneOffset,0),@numDomainID),'</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Item Release:</td><td style=''white-space:nowrap;text-align:left;''>',dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID),'</td></tr><tr><td style=''white-space:nowrap;text-align:right;padding:5px''>Actual Start:</td><td style=''white-space:nowrap;text-align:left;''>',(CASE 
											WHEN TEMP.dtActualStartDate IS NOT NULL 
											THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TEMP.dtActualStartDate),@numDomainID) 
											ELSE NULL 
										END),'</td></tr></table>"><i class="fa fa-bars"></i></a>')
		,NULL
		,NULL
		,dbo.GetTotalProgress(@numDomainID,WorkOrder.numWOId,1,1,'',0) / 100.0
		,ROW_NUMBER() OVER(ORDER BY WorkOrder.dtmStartDate)
		,0
		,1
		,WorkOrder.numWOId
		,CONCAT(Item.vcItemName,' (',ISNULL(WorkOrder.numQtyItemsReq,0),')')
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,WorkOrder.dtmEndDate),@numDomainID)
		,dbo.FormatedDateFromDate(dbo.GetProjectedFinish(@numDomainID,2,WorkOrder.numWOID,0,0,NULL,@ClientTimeZoneOffset,0),@numDomainID)
		,dbo.FormatedDateFromDate(OpportunityItems.ItemReleaseDate,@numDomainID)
		,(CASE 
			WHEN TEMP.dtActualStartDate IS NOT NULL 
			THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TEMP.dtActualStartDate),@numDomainID) 
			ELSE NULL 
		END)		
	FROM 
		WorkOrder
	INNER JOIN
		Item 
	ON
		WorkOrder.numItemCode = Item.numItemCode
	LEFT JOIN
		OpportunityItems
	ON
		WorkOrder.numOppItemID = OpportunityItems.numoppitemtCode
	OUTER APPLY
	(
		SELECT 
			MIN(SPDTTL.dtActionTime) dtActualStartDate
		FROM 
			StagePercentageDetailsTask SPDT
		INNER JOIN
			StagePercentageDetailsTaskTimeLog SPDTTL 
		ON
			SPDT.numTaskId = SPDTTL.numTaskID
		WHERE 
			SPDT.numWorkOrderId=WorkOrder.numWOId
	) TEMP
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOStatus <> 23184
		AND (CAST(ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) AS DATE) BETWEEN CAST(@dtFromDate AS DATE) AND CAST(@dtToDate AS DATE) OR EXISTS (SELECT * FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WorkOrder.numWOId AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId) > 0))
		AND EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WorkOrder.numWOId)
	ORDER BY
		WorkOrder.dtmStartDate

	INSERT INTO @TempData
	(
		[id]
		,[text]
		,[start_date]
		,[duration]
		,[progress]
		,[sortorder]
		,[parent]
		,[open]
		,numWOID
		,vcMilestone
	)
	SELECT
		CONCAT('MS',ROW_NUMBER() OVER(ORDER BY vcMileStoneName))
		,vcMileStoneName
		,NULL
		,NULL
		,dbo.GetTotalProgress(@numDomainID,TD.numWOID,1,2,vcMileStoneName,0) / 100.0
		,ROW_NUMBER() OVER(ORDER BY vcMileStoneName)
		,TD.[id]
		,1
		,numWOID
		,vcMileStoneName
	FROM
		StagePercentageDetails SPD
	INNER JOIN
		@TempData TD
	ON
		SPD.numWorkOrderId = TD.numWOID
	GROUP BY
		vcMileStoneName
		,TD.[id]
		,TD.numWOID

	INSERT INTO @TempData
	(
		[id]
		,[text]
		,[start_date]
		,[duration]
		,[progress]
		,[sortorder]
		,[parent]
		,[open]
		,numStageDetailsId
	)
	SELECT
		CONCAT('S',SPD.numStageDetailsId)
		,SPD.vcStageName
		,NULL
		,NULL
		,dbo.GetTotalProgress(@numDomainID,TD.numWOID,1,3,'',SPD.numStageDetailsId) / 100.0
		,ROW_NUMBER() OVER(ORDER BY SPD.numStageDetailsId)
		,TD.[id]
		,1
		,SPD.numStageDetailsId
	FROM
		StagePercentageDetails SPD
	INNER JOIN
		@TempData TD
	ON
		SPD.numWorkOrderId = TD.numWOID
		AND SPD.vcMileStoneName = TD.vcMilestone

	INSERT INTO @TempData
	(
		[id]
		,[text]
		,[start_date]
		,[end_date]
		,[duration]
		,[progress]
		,[sortorder]
		,[parent]
		,[open]
		,[color]
		,[progressColor]
		,numTaskID
		,vcDurationHours
	)
	SELECT
		CONCAT((CASE 
					WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 
					THEN 'TC'
					WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0
					THEN 'TS'
					ELSE 'T'
				END),SPDT.numTaskId)
		,SPDT.vcTaskName
		,(CASE 
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1) AND EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4)
			THEN (SELECT DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,MIN(dtActionTime)) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1)
			ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDT.dtStartTime)
		END)
		,(CASE 
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1) AND EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 
			THEN (SELECT DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,MAX(dtActionTime)) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4)
			ELSE DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDT.dtEndTime)
		END)
		,((ISNULL(numHours,0) * 60) + ISNULL(numMinutes,0)) * ISNULL(WorkOrder.numQtyItemsReq,0)
		,(CASE 
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 
			THEN 100 / 100
			ELSE 0
		END)
		,ROW_NUMBER() OVER(ORDER BY SPDT.numTaskId)
		,TD.[id]
		,1
		,(CASE 
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 
			THEN '#2ecc71'
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1)
			THEN '#fd7622'
			ELSE '#3498db' 
		END)
		,(CASE 
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) 
			THEN '#02ce58'
			WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1)
			THEN '#ff6200'
			ELSE '#0286de' 
		END)
		,SPDT.numTaskId
		,FORMAT(FLOOR((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * ISNULL(WorkOrder.numQtyItemsReq,0)) / 60),'00') + ':' + FORMAT((((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * CAST(ISNULL(WorkOrder.numQtyItemsReq,0) AS DECIMAL)) % 60,'00')
	FROM
		StagePercentageDetailsTask SPDT
	INNER JOIN
		WorkOrder
	ON
		SPDT.numWorkOrderId = WorkOrder.numWOId
	INNER JOIN
		@TempData TD
	ON
		SPDT.numStageDetailsId = TD.numStageDetailsId


	DECLARE @TempTaskAssignee TABLE
	(
		numWorkOrderID NUMERIC(18,0)
		,numAssignedTo NUMERIC(18,0)		
		,numLastTaskID NUMERIC(18,0)	
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTimeLog TABLE
	(
		numWrokOrder NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numAssignTo NUMERIC(18,0)
		,numProductiveMinutes NUMERIC(18,0)
		,dtDate DATETIME
		,numTotalMinutes NUMERIC(18,0)
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numWorkOrderID NUMERIC(18,0)
		,numAssignTo NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,intTaskType INT
		,dtStartDate DATETIME
		,dtFinishDate DATETIME
		,numQtyItemsReq FLOAT
		,numProcessedQty FLOAT
		,bitTaskStarted BIT
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numWorkOrderID
		,numAssignTo
		,numTaskTimeInMinutes
		,intTaskType
		,dtStartDate
		,numQtyItemsReq
		,numProcessedQty
		,bitTaskStarted
	)
	SELECT
		SPDT.numTaskId
		,WorkOrder.numWOId
		,SPDT.numAssignTo
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0))
		,SPDT.intTaskType
		,(CASE	
			WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1) > 0
			THEN (SELECT MIN(dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=1)
			ELSE ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate)
		END)
		,WorkOrder.numQtyItemsReq
		,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0)
		,(CASE	
			WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0
			THEN 1
			ELSE 0
		END)
	FROM
		@TempData TD
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		TD.numTaskID = SPDT.numTaskId
	INNER JOIN
		WorkOrder
	ON
		SPDT.numWorkOrderId = WorkOrder.numWOId
	WHERE
		TD.numTaskID > 0 
		AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) = 0 -- Task in not finished
	
	INSERT INTO @TempTaskAssignee
	(
		numWorkOrderID
		,numAssignedTo
	)
	SELECT
		numWorkOrderID
		,numAssignTo
	FROM
		@TempTasks
	GROUP BY
		numWorkOrderID
		,numAssignTo

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTempTaskID NUMERIC(18,0)
	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numWorkOrderID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT

	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @dtStartDate DATETIME
	DECLARE @bitTaskStarted BIT
		
	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTempTaskID=numTaskID
			,@numTaskAssignee=numAssignTo
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
			,@dtStartDate=dtStartDate
			,@numWorkOrderID=numWorkOrderID
			,@numQtyItemsReq=numQtyItemsReq
			,@bitTaskStarted = bitTaskStarted
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay
			,@vcWorkDays=CONCAT(',',vcWorkDays,',')
			,@dtStartDate = (CASE 
								WHEN @bitTaskStarted = 1 
								THEN @dtStartDate
								ELSE DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + tmStartOfDay)
							END)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			INSERT INTO @TempLinks
			(
				[source]
				,[target]
				,[type]
			)
			VALUES
			(
				CONCAT((CASE WHEN @bitTaskStarted=1 THEN 'TS' ELSE 'T' END),(SELECT numLastTaskID FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee))
				,CONCAT((CASE WHEN @bitTaskStarted=1 THEN 'TS' ELSE 'T' END),@numTempTaskID)
				,0
			)

			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee
		END
		ELSE
		BEGIN
			INSERT INTO @TempLinks
			(
				[source]
				,[target]
				,[type]
			)
			VALUES
			(
				CONCAT('WO',@numWorkOrderID)
				,CONCAT((CASE WHEN @bitTaskStarted=1 THEN 'TS' ELSE 'T' END),@numTempTaskID)
				,1
			)
		END

		UPDATE @TempTasks SET dtStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF CHARINDEX(CONCAT(',',DATEPART(WEEKDAY,@dtStartDate),','),@vcWorkDays) > 0 AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					-- CHECK TIME LEFT FOR DAY BASED
					SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))

					IF @numTimeLeftForDay > 0
					BEGIN
						INSERT INTO @TempTimeLog
						(
							numWrokOrder
							,numTaskID
							,numQtyItemsReq
							,numAssignTo
							,numProductiveMinutes
							,dtDate
							,numTotalMinutes
						) 
						VALUES 
						(	
							@numWorkOrderID
							,@numTempTaskID							
							,@numQtyItemsReq
							,@numTaskAssignee
							,@numProductiveTimeInMinutes
							,@dtStartDate
							,(CASE WHEN @numTimeLeftForDay > @numTotalTaskInMinutes THEN @numTotalTaskInMinutes ELSE @numTimeLeftForDay END)
						)

						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
							
						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
				END				
			END
		END	

		UPDATE @TempTasks SET dtFinishDate=@dtStartDate WHERE ID=@i 
		UPDATE @TempTaskAssignee SET numLastTaskID=@numTempTaskID, dtLastTaskCompletionTime=@dtStartDate WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END

	UPDATE
		TD
	SET
		TD.[start_date] = (CASE WHEN TD.[start_date] IS NULL THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtStartDate) ELSE TD.[start_date] END)
		,TD.[end_date] = (CASE WHEN TD.[end_date] IS NULL THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,TT.dtFinishDate) ELSE TD.[end_date] END)
		,TD.progress = (CASE WHEN ISNULL(TT.numQtyItemsReq,0) > 0 THEN (ISNULL(TT.numProcessedQty,0) * 100) / TT.numQtyItemsReq ELSE 0 END)
	FROM
		@TempData TD
	INNER JOIN
		@TempTasks TT
	ON
		TD.numTaskID = TT.numTaskID


	UPDATE
		@TempData
	SET
		[color] = (CASE 
						WHEN (dtItemReleaseDate IS NOT NULL AND dtProjectedFinish > dtItemReleaseDate) OR (dtRequestedFinish IS NOT NULL AND dtProjectedFinish > dtRequestedFinish)
						THEN '#f65a5b'
						ELSE '#2ecc71'
					END),
		[progressColor] = (CASE 
								WHEN (dtItemReleaseDate IS NOT NULL AND dtProjectedFinish > dtItemReleaseDate) OR (dtRequestedFinish IS NOT NULL AND dtProjectedFinish > dtRequestedFinish)
								THEN '#f91e1f'
								ELSE '#02ce58'
							END)
	WHERE
		numWOID > 0
		AND [parent] = '0'

	SELECT * FROM @TempData

	SELECT * FROM @TempLinks
END
GO