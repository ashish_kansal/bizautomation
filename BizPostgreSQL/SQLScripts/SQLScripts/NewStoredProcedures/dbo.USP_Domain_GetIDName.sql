SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_GetIDName')
DROP PROCEDURE dbo.USP_Domain_GetIDName
GO
CREATE PROCEDURE [dbo].[USP_Domain_GetIDName]
	
AS 
BEGIN
	SELECT 
		numDomainID,
		vcDomainName 
	FROM 
		Domain
	WHERE
		ISNULL(numDomainID,0) > 0
	ORDER BY
		vcDomainName ASC
END