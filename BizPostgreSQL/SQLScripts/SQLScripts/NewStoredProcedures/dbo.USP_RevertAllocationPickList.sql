GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO   
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertAllocationPickList')
DROP PROCEDURE USP_RevertAllocationPickList
GO
CREATE PROCEDURE [dbo].[USP_RevertAllocationPickList]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
	,@numUserCntID AS NUMERIC(18,0)
AS
BEGIN
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numWLocationID NUMERIC(18,0)                     
    DECLARE @numUnits AS FLOAT                                              
    DECLARE @onHand AS FLOAT                                            
    DECLARE @onOrder AS FLOAT                                            
    DECLARE @onBackOrder AS FLOAT                                              
    DECLARE @onAllocation AS FLOAT
    DECLARE @numQtyShipped AS FLOAT
    DECLARE @numUnitHourReceived AS FLOAT
	DECLARE @numDeletedReceievedQty AS FLOAT
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS DECIMAL(20,5)   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT		
    DECLARE @description AS VARCHAR(100)
	DECLARE @bitWorkOrder AS BIT

	Declare @numRentalIN as FLOAT
	Declare @numRentalOut as FLOAT
	Declare @numRentalLost as FLOAT
	DECLARE @bitAsset as BIT

	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numChildAllocation AS FLOAT
	DECLARE @numChildBackOrder AS FLOAT
	DECLARE @numChildOnHand AS FLOAT
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numKitChildAllocation AS FLOAT
	DECLARE @numKitChildBackOrder AS FLOAT
	DECLARE @numKitChildOnHand AS FLOAT
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @numFulfillmentBizDocFromPickList AS NUMERIC(18,0)
	SELECT @numFulfillmentBizDocFromPickList=numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=@numOppID AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocID
				
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(OBDI.numUnitHour,0),
            @numWareHouseItemID = ISNULL(OI.numWarehouseItmsID,0),
			@numWLocationID = ISNULL(numWLocationID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(OBDI.monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @numQtyShipped = ISNULL(OBDIFulfillment.numUnitHour,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
			@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0),
			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
    FROM  
		OpportunityBizDocItems OBDI
	LEFT JOIN
		OpportunityBizDocItems OBDIFulfillment
	ON
		OBDI.numOppItemID=OBDIFulfillment.numOppItemID
		AND OBDIFulfillment.numOppBizDocID = @numFulfillmentBizDocFromPickList
	INNER JOIN
		OpportunityItems OI
	ON
		OBDI.numOppItemID = OI.numoppitemtCode
	LEFT JOIN 
		WareHouseItems WI 
	ON 
		OBDI.numWarehouseItmsID=WI.numWareHouseItemID
	JOIN 
		dbo.OpportunityMaster OM 
	ON 
		OI.numOppId = OM.numOppId
    JOIN 
		Item I 
	ON 
		OI.numItemCode = I.numItemCode
    WHERE  
		(charitemtype='P' OR 1=(CASE
									WHEN tintOppType=1 
									THEN CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1 ELSE 0 END 
									ELSE 0 
								END)) 
		AND OBDI.numOppBizDocID=@numOppBizDocID
        AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OBDI.numUnitHour,0) > 0 
    ORDER BY 
		OI.numoppitemtCode

	
    WHILE @numoppitemtCode > 0                                  
    BEGIN    
        IF @numWareHouseItemID>0
        BEGIN
			SELECT  
				@onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE 
				numWareHouseItemID = @numWareHouseItemID                                               
        END
            
		SET @description=CONCAT('SO Pick List Deleted (Qty:',@numUnits,' Shipped:',@numQtyShipped,')')
  
        IF @Kit = 1 AND @numUnits <> @numQtyShipped
		BEGIN
			-- CLEAR DATA OF PREVIOUS ITERATION
			DELETE FROM @TempKitSubItems

			-- GET KIT SUB ITEMS DETAIL
			INSERT INTO @TempKitSubItems
			(
				ID,
				numOppChildItemID,
				numChildItemCode,
				bitChildIsKit,
				numChildWarehouseItemID,
				numChildQty,
				numChildQtyShipped
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
				ISNULL(numOppChildItemID,0),
				ISNULL(I.numItemCode,0),
				ISNULL(I.bitKitParent,0),
				ISNULL(numWareHouseItemId,0),
				((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numUnits),
				((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQtyShipped)
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				Item I 
			ON 
				OKI.numChildItemID=I.numItemCode    
			WHERE 
				charitemtype='P' 
				AND ISNULL(numWareHouseItemId,0) > 0 
				AND OKI.numOppID=@numOppID 
				AND OKI.numOppItemID=@numoppitemtCode 

			SET @j = 1
			SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

			--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
			WHILE @j <= @ChildCount
			BEGIN
				SELECT
					@numOppChildItemID=numOppChildItemID,
					@numChildItemCode=numChildItemCode,
					@bitChildIsKit=bitChildIsKit,
					@numChildWarehouseItemID=numChildWarehouseItemID,
					@numChildQty=numChildQty,
					@numChildQtyShipped=numChildQtyShipped
				FROM
					@TempKitSubItems
				WHERE 
					ID = @j

				SELECT @numChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

				-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
				IF @bitChildIsKit = 1
				BEGIN
					-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
					IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numoppitemtCode AND numOppChildItemID=@numOppChildItemID)
					BEGIN
						-- CLEAR DATA OF PREVIOUS ITERATION
						DELETE FROM @TempKitChildKitSubItems

						-- GET SUB KIT SUB ITEMS DETAIL
						INSERT INTO @TempKitChildKitSubItems
						(
							ID,
							numOppKitChildItemID,
							numKitChildItemCode,
							numKitChildWarehouseItemID,
							numKitChildQty,
							numKitChildQtyShipped
						)
						SELECT 
							ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
							ISNULL(numOppKitChildItemID,0),
							ISNULL(OKCI.numItemID,0),
							ISNULL(OKCI.numWareHouseItemId,0),
							((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
							((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQtyShipped)
						FROM 
							OpportunityKitChildItems OKCI 
						JOIN 
							Item I 
						ON 
							OKCI.numItemID=I.numItemCode    
						WHERE 
							charitemtype='P' 
							AND ISNULL(numWareHouseItemId,0) > 0 
							AND OKCI.numOppID=@numOppID 
							AND OKCI.numOppItemID=@numoppitemtCode 
							AND OKCI.numOppChildItemID = @numOppChildItemID

						SET @k = 1
						SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

						WHILE @k <= @CountKitChildItems
						BEGIN
							SELECT
								@numOppKitChildItemID=numOppKitChildItemID,
								@numKitChildItemCode=numKitChildItemCode,
								@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
								@numKitChildQty=numKitChildQty,
								@numKitChildQtyShipped=numKitChildQtyShipped
							FROM
								@TempKitChildKitSubItems
							WHERE 
								ID = @k

							SELECT @numKitChildAllocation=ISNULL(numAllocation,0),@numKitChildBackOrder = ISNULL(numBackOrder,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

							SET @vcKitChildDescription = CONCAT('SO Pick List Child Kit Deleted (Qty:',@numKitChildQty,' Shipped:',@numKitChildQtyShipped,')')

							IF @numKitChildQtyShipped > 0
							BEGIN
								SET @numKitChildQty = @numKitChildQty - @numKitChildQtyShipped
							END

							IF @numKitChildAllocation + @numKitChildBackOrder >= @numKitChildQty
							BEGIN
								UPDATE
									WarehouseItems
								SET 
									numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @numKitChildQty THEN 0 ELSE (@numKitChildQty - ISNULL(numBackOrder,0)) END)
									,numAllocation = (CASE WHEN ISNULL(numBackOrder,0) >= @numKitChildQty THEN ISNULL(numAllocation,0) ELSE ISNULL(numAllocation,0) - (@numKitChildQty - ISNULL(numBackOrder,0)) END)
									,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= @numKitChildQty THEN ISNULL(numBackOrder,0) - @numKitChildQty ELSE 0 END)
									,dtModified = GETDATE()
								WHERE
									numWareHouseItemID=@numKitChildWarehouseItemID
							END
							ELSE
							BEGIN
								RAISERROR ('INVALID_INVENTORY',16,1);
							END

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numKitChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcKitChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 

							SET @k = @k + 1
						END

						SET @vcChildDescription = CONCAT('SO Pick List KIT Deleted (Qty:',@numChildQty,' Shipped:',@numChildQtyShipped,')')

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numChildWarehouseItemID,
							@numReferenceID = @numOppId,
							@tintRefType = 3,
							@vcDescription = @vcChildDescription,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
					END
				END
				ELSE
				BEGIN
					SELECT @numChildAllocation=ISNULL(numAllocation,0),@numChildBackOrder=ISNULL(numBackOrder,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

					SET @vcChildDescription = CONCAT('SO Pick List Kit Deleted (Qty:',@numChildQty,' Shipped:',@numChildQtyShipped,')')

					IF @numChildQtyShipped > 0
					BEGIN
						SET @numChildQty = @numChildQty - @numChildQtyShipped
					END

					IF @numChildAllocation + @numChildBackOrder >= @numChildQty
					BEGIN
						UPDATE
							WarehouseItems
						SET 
							numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @numChildQty THEN 0 ELSE (@numChildQty - ISNULL(numBackOrder,0)) END)
							,numAllocation = (CASE WHEN ISNULL(numBackOrder,0) >= @numChildQty THEN ISNULL(numAllocation,0) ELSE ISNULL(numAllocation,0) - (@numChildQty - ISNULL(numBackOrder,0)) END)
							,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= @numChildQty THEN ISNULL(numBackOrder,0) - @numChildQty ELSE 0 END)
							,dtModified = GETDATE() 
						WHERE
							numWareHouseItemID=@numChildWarehouseItemID
					END
					ELSE
					BEGIN
						RAISERROR ('INVALID_INVENTORY',16,1);
					END

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numChildWarehouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 3,
						@vcDescription = @vcChildDescription,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID
				END

				SET @j = @j + 1
			END
		END
		ELSE IF @numUnits <> @numQtyShipped
		BEGIN
			IF @numQtyShipped>0
			BEGIN
				SET @numUnits = @numUnits - @numQtyShipped
			END 

			IF @onAllocation + @onBackOrder >= @numUnits
			BEGIN
				UPDATE
					WarehouseItems
				SET 
					numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @numUnits THEN 0 ELSE (@numUnits - ISNULL(numBackOrder,0)) END)
					,numAllocation = (CASE WHEN ISNULL(numBackOrder,0) >= @numUnits THEN ISNULL(numAllocation,0) ELSE ISNULL(numAllocation,0) - (@numUnits - ISNULL(numBackOrder,0)) END)
					,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) >= @numUnits THEN ISNULL(numBackOrder,0) - @numUnits ELSE 0 END)
					,dtModified = GETDATE() 
				WHERE
					numWareHouseItemID=@numWareHouseItemID
			END
			ELSE
			BEGIN
				PRINT @numUnits
				PRINT @onAllocation
				PRINT @onBackOrder
				PRINT @numWareHouseItemID
				RAISERROR ('INVALID_INVENTORY',16,1);
			END          
		END
			
		IF @numUnits <> @numQtyShipped
		BEGIN
			IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
			BEGIN 
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomainID
			END
		END

		
                                                                
		SELECT TOP 1
				@numoppitemtCode = numoppitemtCode,
				@itemcode = OI.numItemCode,
				@numUnits = ISNULL(OBDI.numUnitHour,0),
				@numWareHouseItemID = ISNULL(OI.numWarehouseItmsID,0),
				@numWLocationID = ISNULL(numWLocationID,0),
				@Kit = ( CASE WHEN bitKitParent = 1
								   AND bitAssembly = 1 THEN 0
							  WHEN bitKitParent = 1 THEN 1
							  ELSE 0
						 END ),
				@monAmount = ISNULL(OBDI.monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
				@numQtyShipped = ISNULL(OBDIFulfillment.numUnitHour,0),
				@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
				@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
				@bitKitParent=ISNULL(bitKitParent,0),
				@OppType = tintOppType,
				@numRentalIN=ISNULL(oi.numRentalIN,0),
				@numRentalOut=Isnull(oi.numRentalOut,0),
				@numRentalLost=Isnull(oi.numRentalLost,0),
				@bitAsset =ISNULL(I.bitAsset,0),
				@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
		FROM  
			OpportunityBizDocItems OBDI
		LEFT JOIN
			OpportunityBizDocItems OBDIFulfillment
		ON
			OBDI.numOppItemID=OBDIFulfillment.numOppItemID
			AND OBDIFulfillment.numOppBizDocID = @numFulfillmentBizDocFromPickList
		INNER JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID = OI.numoppitemtCode
		LEFT JOIN 
			WareHouseItems WI 
		ON 
			OBDI.numWarehouseItmsID=WI.numWareHouseItemID
		JOIN 
			dbo.OpportunityMaster OM 
		ON 
			OI.numOppId = OM.numOppId
		JOIN 
			Item I 
		ON 
			OI.numItemCode = I.numItemCode
		WHERE  
			(charitemtype='P' OR 1=(CASE
										WHEN tintOppType=1 
										THEN CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1 ELSE 0 END 
										ELSE 0 
									END)) 
			AND OBDI.numOppBizDocID=@numOppBizDocID
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OBDI.numUnitHour,0) > 0 
			AND OI.numoppitemtCode > @numoppitemtCode
		ORDER BY 
			OI.numoppitemtCode
		                                           
		IF @@rowcount = 0 
			SET @numoppitemtCode = 0
	END
END
GO