SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItmsDTL_CheckForDuplicate')
DROP PROCEDURE dbo.USP_WareHouseItmsDTL_CheckForDuplicate
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItmsDTL_CheckForDuplicate]
(
	@numDomainID NUMERIC(9),
	@numWarehouseID NUMERIC(18,0),
	@numWarehouseLocationID NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0),
	@bitSerial BIT,
    @vcSerialLot TEXT
)
AS 
BEGIN
	--IMPORTANT: THIS PROCEDURE IS ALSO CALLED FROM "USP_WarehouseItems_Save" 
	DECLARE @bitDuplicate AS BIT = 0

	DECLARE @TempSerialLot TABLE
	(
		vcSerialLot VARCHAR(100),
		numQty INT,
		dtExpirationDate DATETIME
	)

	DECLARE @idoc int
	EXEC sp_xml_preparedocument @idoc OUTPUT, @vcSerialLot;

	INSERT INTO
		@TempSerialLot
	SELECT
		vcSerialLot,
		numQty,
		dtExpirationDate
	FROM 
		OPENXML (@idoc, '/SerialLots/SerialLot',2)
	WITH 
		(
			vcSerialLot VARCHAR(100),
			numQty INT,
			dtExpirationDate DATETIME
		);

	--SERIAL ITEM
	IF @bitSerial = 1
	BEGIN
		--NOTE: USER CAN NOT ADD DUPLICATE SERIAL NUMBER DOESN�T MATTER IF WAREHOUSE & LOCATION COMBINATION IS UNIQUE
		IF (
			SELECT
				COUNT(*)
			FROM 
				Warehouses
			INNER JOIN
				WareHouseItems
			ON
				Warehouses.numWareHouseID = WareHouseItems.numWareHouseID
			INNER JOIN
				WareHouseItmsDTL
			ON
				WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID
			WHERE
				Warehouses.numDomainID = @numDomainID
				AND WareHouseItems.numItemID = @numItemCode
				AND WareHouseItmsDTL.vcSerialNo IN (SELECT ISNULL(vcSerialLot,'') FROM @TempSerialLot)) > 0
		BEGIN
			SET @bitDuplicate = 1
		END

	END
	ELSE --LOT ITEM
	BEGIN
		--NOTE: USER CAN ADD SAME LOT NUMBER IN UNIQUE WAREHOUSE & LOCATION COMBINATION BUT NOT IN SAME
		IF (
			SELECT
				COUNT(*)
			FROM 
				WareHouseItmsDTL
			INNER JOIN
				WareHouseItems
			ON
				WareHouseItmsDTL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			WHERE
				WareHouseItems.numWareHouseID = @numWarehouseID
				AND WareHouseItems.numItemID = @numItemCode
				AND ISNULL(WareHouseItems.numWLocationID,0) = ISNULL(@numWarehouseLocationID,0)
				AND WareHouseItmsDTL.vcSerialNo IN (SELECT ISNULL(vcSerialLot,'') FROM @TempSerialLot)) > 0
		BEGIN
			SET @bitDuplicate = 1
		END
	END

	SELECT @bitDuplicate
END
