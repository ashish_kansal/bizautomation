/****** Object:  StoredProcedure [dbo].[usp_SetFollowUpStatusForSelectedRecords]    Script Date: 07/26/2008 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Prasanta	                            
--Purpose: Deletes the selected Records of Organization Alert Panel   
--Created Date: 02/09/2016  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_AlertPanel')
DROP PROCEDURE usp_AlertPanel
GO
CREATE PROCEDURE [dbo].[usp_AlertPanel]          
  @chrAction char(10)=NULL,
  @numDomainId Numeric(18,2)=0,
  @numUserId Numeric(18,2)=0,
  @numModuleId Numeric(18,2)=0,
  @numRecordId Numeric(18,2)=0      
AS                                          
BEGIN
	Declare @vcStartDate VARCHAR(30)='2/4/2016'
--Organization Alert
IF(@chrAction='V')
	BEGIN                           
	 SELECT TOP 25 D.numDivisionID,C.vcCompanyName,ISNULL(U.vcUserName,'System Generated') as vcUserName,CAST(D.bintModifiedDate AS VARCHAR) as bintCreatedDate,
			CASE WHEN D.tintCRMType=2 THEN 'Accounts' WHEN D.tintCRMType=1 THEN 'Prospects' ELSE 'Leads' END as tintCRMType
			FROM 
				View_CompanyAlert as C
			LEFT JOIN
				DivisionMaster as D
			ON 
				C.numCompanyId=D.numCompanyID
			LEFT JOIN
				UserMaster as U 
			ON 
				C.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				D.numDivisionID=SH.numRecordID AND SH.numModuleID=1
			WHERE
				--(C.bintCreatedDate>=Cast(@vcStartDate as date) OR D.bintModifiedDate>=Cast(@vcStartDate as date)) AND 
				C.numDomainID=@numDomainId AND D.numDivisionID!=0 AND
				C.numCreatedBy!=@numUserId AND (D.numAssignedTo=@numUserId OR D.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND D.numDivisionID NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId=@numModuleId AND numDomainID=@numDomainId)
			ORDER BY D.numDivisionID DESC
	END

--Case Module Alert
IF(@chrAction='CV')
	BEGIN                           
	 SELECT TOP 25 C.numCaseId,CO.vcCompanyName as OrganizationName,ISNULL(U.vcUserName,'System Generated') as vcUserName,CAST(C.bintModifiedDate AS VARCHAR) as bintCreatedDate,
			AC.vcFirstName + ' '+AC.vcLastName as ContactName
			FROM 
				View_CaseAlert as C
			LEFT JOIN
				DivisionMaster as D
			ON 
				C.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				C.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AC
			ON 
				C.numContactId=AC.numContactId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				C.numCaseId=SH.numRecordID AND SH.numModuleID=7
			WHERE
				--C.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				C.numDomainID=@numDomainId AND C.numCaseId!=0 AND
				C.numCreatedBy!=@numUserId AND (C.numAssignedTo=@numUserId OR C.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND C.numCaseId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId=@numModuleId AND numDomainID=@numDomainId)
			ORDER BY C.numCaseId DESC
	END

--Puchase & Sales Oppourtunity Module Alert
IF(@chrAction='PSO')
	BEGIN                           
	 SELECT TOP 25 O.numOppId,CO.vcCompanyName,O.vcPOppName,ISNULL(U.vcUserName,'System Generated') as vcUserName,CAST(O.bintModifiedDate AS VARCHAR) as bintCreatedDate,
			AC.vcFirstName + ' '+AC.vcLastName as ContactName,O.tintOppType,O.tintOppStatus
			FROM 
				View_OpportunityAlert as O
			LEFT JOIN
				DivisionMaster as D
			ON 
				O.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				O.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AC
			ON 
				O.numContactId=AC.numContactId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				SH.numRecordID=O.numOppId AND SH.numModuleID=3
			WHERE
				--O.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				O.numDomainID=@numDomainId AND O.numOppId!=0 AND
				O.numCreatedBy!=@numUserId AND (O.numAssignedTo=@numUserId OR O.numRecOwner=@numUserId OR SH.numAssignedTo=@numUserId) AND O.numOppId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(3,4,5,6) AND numDomainID=@numDomainId)
			ORDER BY O.numOppId DESC
	END

--Projects Module Alert
IF(@chrAction='PV')
	BEGIN                           
	 SELECT TOP 25 P.numProId,CO.vcCompanyName,P.vcProjectName,ISNULL(U.vcUserName,'System Generated') as vcUserName,CAST(P.bintModifiedDate AS VARCHAR) as bintCreatedDate
			FROM 
				View_ProjectsAlert as P
			LEFT JOIN
				DivisionMaster as D
			ON 
				P.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				P.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				ShareRecord as SH
			ON 
				SH.numRecordID=P.numProId AND SH.numModuleID=5
			WHERE
				--P.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				P.numDomainID=@numDomainId AND P.numProId!=0 AND
				P.numCreatedBy!=@numUserId AND (P.numAssignedTo=@numUserId OR P.numRecOwner=@numUserId OR P.numIntPrjMgr=@numUserId OR SH.numAssignedTo=@numUserId) AND P.numProId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(7) AND numDomainID=@numDomainId)
			ORDER BY P.bintModifiedDate DESC
	END

--Email Module Alert
IF(@chrAction='EV')
	BEGIN                           
	 SELECT TOP 25 H.numEmailHstrID,H.vcSubject,ISNULL(U.vcUserName,'System Generated') as vcUserName,CAST(H.bintCreatedOn AS VARCHAR) as bintCreatedDate
			FROM 
				View_EmailAlert as H
			LEFT JOIN
				UserMaster as U 
			ON 
				H.numUserCntId=U.numUserDetailId
			LEFT JOIN
				EmailMaster as T
			ON
				(SELECT TOP 1 Items from  [dbo].[Split](H.vcTo,'$^$'))=T.numEmailId
			LEFT JOIN
				UserMaster as UT
			ON
				T.numContactId=UT.numUserDetailId
			WHERE
				--H.bintCreatedOn>=Cast(@vcStartDate as date) AND 
				H.numDomainID=@numDomainId AND H.numEmailHstrID!=0 AND
				H.numUserCntId!=@numUserId AND (T.numContactId=@numUserId) AND H.numEmailHstrID NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(8) AND numDomainID=@numDomainId)
			ORDER BY H.bintCreatedOn DESC
	END
	--Tickler Module Alert
IF(@chrAction='TV')
	BEGIN                           
	 SELECT TOP 25 C.numCommId,CO.vcCompanyName,AD.vcGivenName,ISNULL(U.vcUserName,'System Generated') as vcUserName,CAST(C.dtModifiedDate AS VARCHAR) as bintCreatedDate
			FROM 
				View_TicklerAlert as C
			LEFT JOIN
				DivisionMaster as D
			ON 
				C.numDivisionID=D.numDivisionID
			LEFT JOIN
				CompanyInfo as CO
			ON 
				D.numCompanyID=CO.numCompanyId
			LEFT JOIN
				UserMaster as U 
			ON 
				C.numCreatedBy=U.numUserDetailId
			LEFT JOIN 
				AdditionalContactsInformation as AD
			ON
				C.numContactId=AD.numContactId
			WHERE
				--C.dtModifiedDate>=Cast(@vcStartDate as date) AND 
				C.numDomainID=@numDomainId AND C.numCommId!=0 AND
				C.numCreatedBy!=@numUserId AND C.numAssignedBy!=@numUserId AND (C.numAssign=@numUserId ) AND C.numCommId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(9) AND numDomainID=@numDomainId)
			ORDER BY C.dtModifiedDate DESC
	END
	--Process Module Alert 
IF(@chrAction='PPV')
	BEGIN                           
	 SELECT TOP 25 S.numStageDetailsId,ISNULL(U.vcUserName,'System Generated') as vcUserName,CAST(S.bintModifiedDate AS VARCHAR) as bintCreatedDate,
			CASE WHEN S.numOppID<>0 THEN O.vcPOppName WHEN S.numProjectID<>0 THEN P.vcProjectName ELSE NULL END AS ProgressOn, 
			CASE WHEN S.numOppID<>0 THEN O.tintOppType WHEN S.numProjectID<>0 THEN -1 ELSE NULL END AS tintOppType,
			CASE WHEN S.numOppID<>0 THEN O.tintOppStatus WHEN S.numProjectID<>0 THEN -1 ELSE NULL END AS tintOppStatus,
			CASE WHEN S.numOppID<>0 THEN O.numOppId WHEN S.numProjectID<>0 THEN S.numProjectID ELSE NULL END AS RecordId
			FROM 
				View_ProcessAlert as S
			LEFT JOIN
				UserMaster as U 
			ON 
				S.numCreatedBy=U.numUserDetailId
			LEFT JOIN
				OpportunityMaster as O
			ON 
				O.numOppId=S.numOppID
			LEFT JOIN
				ProjectsMaster as P
			ON	
				P.numProId=S.numProjectID
			WHERE
				--S.bintModifiedDate>=Cast(@vcStartDate as date) AND 
				S.numDomainID=@numDomainId AND S.numStageDetailsId!=0 AND
				(O.numAssignedTo=@numUserId OR O.numRecOwner=@numUserId OR P.numAssignedTo=@numUserId OR P.numRecOwner=@numUserId OR P.numIntPrjMgr=@numUserId)
				AND (O.numAssignedBy!=@numUserId OR P.numAssignedby!=@numUserId OR O.numCreatedBy!=@numUserId OR P.numCreatedBy!=@numUserId) AND
				S.numCreatedBy!=@numUserId AND  S.numStageDetailsId NOT IN (SELECT numRecordId 
																				FROM 
																					TrackNotification 
																				WHERE
																					numUserId=@numUserId and numModuleId IN(10) AND numDomainID=@numDomainId)
			ORDER BY S.bintModifiedDate DESC
	END
	IF(@chrAction='D')
	BEGIN
		INSERT INTO TrackNotification
					(numDomainId,numUserId,numModuleId,numRecordId)
				VALUES
					(@numDomainId,@numUserId,@numModuleId,@numRecordId)

	END
END

GO
