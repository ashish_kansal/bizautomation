GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetVendorInvoice')
DROP PROCEDURE USP_OpportunityBizDocs_GetVendorInvoice
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetVendorInvoice]
(
	@numDomainID NUMERIC(18,0)
	,@numRecordID NUMERIC(18,0)
	,@tintMode TINYINT
)
AS
BEGIN
	SELECT DISTINCT
		numOppBizDocsId
		,CONCAT(vcBizDocID,(CASE WHEN LEN(ISNULL(vcVendorInvoice,'')) > 0 THEN CONCAT(' (',vcVendorInvoice,')') ELSE '' END)) vcBizDocID
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityBizDocItems
	ON
		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	INNER JOIN
		OpportunityMaster
	ON
		OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND tintOppType=2
		AND tintOppStatus=1
		AND ISNULL(bitStockTransfer,0) = 0
		AND OpportunityBizDocs.numBizDocId = 305 -- Vendor Invoice
		AND (ISNULL(numUnitHour,0) - ISNULL(numVendorInvoiceUnitReceived,0)) > 0
		AND 1 = (CASE @tintMode
					WHEN 3 -- Purchase Order
					THEN (CASE WHEN OpportunityMaster.numOppId=@numRecordID THEN 1 ELSE 0 END)
					WHEN 4 -- Vendor
					THEN (CASE WHEN OpportunityMaster.numDivisionId=@numRecordID THEN 1 ELSE 0 END)
				END)
END
