
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SimpleSearch')
DROP PROCEDURE USP_SimpleSearch
GO
CREATE PROCEDURE [dbo].[USP_SimpleSearch]                             
	@numDomainID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0),
	@searchText VARCHAR(100),
	@searchType VARCHAR(1),
	@searchCriteria VARCHAR(1),
	@orderType VARCHAR(1),
	@bizDocType AS INT ,
	@numSkip AS INT,@isStartWithSearch as int                                                                  
AS               
	DECLARE @SalesOppRights AS TINYINT                    
	DECLARE @PurchaseOppRights AS TINYINT                   
	DECLARE @SalesOrderRights AS TINYINT
	DECLARE @PurchaseOrderRights AS TINYINT
	DECLARE @BizDocsRights TINYINT           
	DECLARE @strSQL AS VARCHAR(MAX) = ''
	
	IF @searchType = '1' -- Organization
	BEGIN
		IF @searchCriteria = '1' --Organizations
		BEGIN
			EXEC USP_CompanyInfo_Search @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@isStartWithSearch=@isStartWithSearch,@searchText=@searchText
		END
		ELSE
		BEGIN
			DECLARE @ProspectsRights AS TINYINT                    
			DECLARE @accountsRights AS TINYINT                   
			DECLARE @leadsRights AS TINYINT 
               
			SET @ProspectsRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
									 FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									 WHERE  PM.numpageID = 2
											AND PM.numModuleID = 3
											AND UM.numUserID = ( SELECT numUserID
																 FROM   userMaster
																 WHERE  numUserDetailId = @numUserCntID
															   )
									 GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
								   )                        
                    
			SET @accountsRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
									FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE PM.numpageID = 2
											AND PM.numModuleID = 4
											AND UM.numUserID = ( SELECT numUserID
																 FROM   userMaster
																 WHERE  numUserDetailId = @numUserCntID
															   )
									GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
								  )                
              
			SET @leadsRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
								 FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								 WHERE  PM.numpageID = 2
										AND PM.numModuleID = 2
										AND UM.numUserID = ( SELECT numUserID
															 FROM   userMaster
															 WHERE  numUserDetailId = @numUserCntID
														   )
								 GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
							   )  

			--GET FIELDS CONFIGURED FOR ORGANIZATION SEARCH
			SELECT * INTO #tempOrgSearch FROM
			(
				SELECT 
					numFieldId,
					vcDbColumnName,
					ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
					vcLookBackTableName,
					tintRow AS tintOrder,
					0 as Custom
				FROM 
					View_DynamicColumns
				WHERE 
					numFormId=97 AND numUserCntID=0 AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
				UNION   
				SELECT 
					numFieldId,
					vcFieldName,
					vcFieldName,
					'CFW_FLD_Values',
					tintRow AS tintOrder,
					1 as Custom
				FROM 
					View_DynamicCustomColumns   
				WHERE 
					Grp_id=1 AND numFormId=97 AND numUserCntID=0 AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=1 AND numRelCntType=0
			)Y
			
			DECLARE @searchSQL AS VARCHAR(MAX) = ''

			IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0
			BEGIN
				if(@isStartWithSearch=0)
				begin
				SELECT  
					@searchSQL = STUFF(
										(SELECT ' OR ' + + CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),IIF(Custom = 1,'Fld_Value',vcDbColumnName)) + ' LIKE ''%' + @searchText + '%''' FROM #tempOrgSearch FOR XML PATH(''))
										, 1
										, 4
										, ''
										)
				end
				else 
				begin
					SELECT  
					@searchSQL = STUFF(
										(SELECT ' OR ' + + CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),IIF(Custom = 1,'Fld_Value',vcDbColumnName)) + ' LIKE '+ @searchText + '%''' FROM #tempOrgSearch FOR XML PATH(''))
										, 1
										, 4
										, ''
										)
				end
			END
			ELSE
			BEGIN
				if(@isStartWithSearch=0)
				begin
					SET @searchSQL = 'CompanyInfo.vcCompanyName LIKE ''%' + @searchText + '%'''
				end
				else
				begin
					SET @searchSQL = 'CompanyInfo.vcCompanyName LIKE '+ @searchText + '%'''
				end
			END

			CREATE TABLE #TEMPOrganization
			(
				numDivisionID NUMERIC(18,0),
				vcCompanyName VARCHAR(500)
			)


			SET @strSQL = 'INSERT INTO 
								#TEMPOrganization
							SELECT
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName
							FROM
								DivisionMaster 
							INNER JOIN
								CompanyInfo 
							ON
								CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								OpportunityMaster
							ON
								OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								Cases
							ON
								Cases.numDivisionID = DivisionMaster.numDivisionID
							LEFT JOIN
								ProjectsMaster
							ON
								ProjectsMaster.numDivisionId = DivisionMaster.numDivisionID
							OUTER APPLY 
								(
									SELECT
										isnull(vcStreet,'''') as vcBillStreet, 
										isnull(vcCity,'''') as vcBillCity, 
										isnull(dbo.fn_GetState(numState),'''') as numBillState,
										isnull(vcPostalCode,'''') as vcBillPostCode,
										isnull(dbo.fn_GetListName(numCountry,0),'''')  as numBillCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID=DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=1 
								)  AD1
							OUTER APPLY
								(
									SELECT
										isnull(vcStreet,'''') as vcShipStreet,
										isnull(vcCity,'''') as vcShipCity,
										isnull(dbo.fn_GetState(numState),'''')  as numShipState,
										isnull(vcPostalCode,'''') as vcShipPostCode, 
										isnull(dbo.fn_GetListName(numCountry,0),'''') numShipCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID= DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=2	
								) AD2	
							LEFT JOIN
								CFW_FLD_Values
							ON
								CFW_FLD_Values.RecId = DivisionMaster.numDivisionID
								AND CFW_FLD_Values.Fld_ID IN (SELECT numFieldID FROM #tempOrgSearch)
							WHERE
								DivisionMaster.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + ' AND (DivisionMaster.numAssignedTo = ' + CAST(@numUserCntID AS VARCHAR) + ' OR 
								1 = CASE 
									WHEN DivisionMaster.tintCRMType <> 0 and DivisionMaster.tintCRMType=1 THEN ' +
									CASE @ProspectsRights 
										WHEN 0 THEN ' CASE WHEN DivisionMaster.numRecOwner=0 THEN 1 ELSE 0 END'
										WHEN 1 THEN ' CASE WHEN DivisionMaster.numRecOwner= '+convert(varchar(15),@numUserCntID) + ' THEN 1 ELSE 0 END'                        
										WHEN 2 THEN ' CASE WHEN (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0) THEN 1 ELSE 0 END'
										ELSE '1'
									END + ' WHEN DivisionMaster.tintCRMType<>0 and DivisionMaster.tintCRMType=2 THEN' + 
									CASE @accountsRights 
										WHEN 0 THEN ' CASE WHEN DivisionMaster.numRecOwner=0 THEN 1 ELSE 0 END'
										WHEN 1 THEN ' CASE WHEN DivisionMaster.numRecOwner= '+convert(varchar(15),@numUserCntID) + ' THEN 1 ELSE 0 END'
										WHEN 2 THEN ' CASE WHEN (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0) THEN 1 ELSE 0 END'
										ELSE '1'
									END + ' WHEN DivisionMaster.tintCRMType=0 THEN' + 
									CASE @leadsRights 
										WHEN 0 THEN ' CASE WHEN DivisionMaster.numRecOwner=0 THEN 1 ELSE 0 END'
										WHEN 1 THEN ' CASE WHEN DivisionMaster.numRecOwner= '+convert(varchar(15),@numUserCntID) + ' THEN 1 ELSE 0 END'
										WHEN 2 THEN ' CASE WHEN (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0) THEN 1 ELSE 0 END'
										ELSE '1'
									END + ') AND (' + @searchSQL + ')
							GROUP BY
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName'

			PRINT @strSQL
			EXEC (@strSQL)
			IF @searchCriteria = '2' --Items
			BEGIN
				DECLARE @TEMPORGITEM TABLE
				(
					numOppId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcItemName VARCHAR(500),
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					numUnitHour FLOAT,
					monPrice NUMERIC(18,2),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO @TEMPORGITEM
				(
					numOppId,
					CreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					numUnitHour,
					monPrice,
					vcCompanyName,
					tintType
				)
				SELECT
					numOppId,
					bintCreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					CAST(numUnitHour AS FLOAT) numUnitHour,
					CAST(monPrice AS decimal(18,2)) monPrice,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						OpportunityItems.numUnitHour,
						OpportunityItems.monPrice,
						TEMP.vcCompanyName,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					INNER JOIN
						OpportunityItems
					ON
						OpportunityMaster.numOppId = OpportunityItems.numOppId
					INNER JOIN
						Item
					ON
						OpportunityItems.numItemCode = Item.numItemCode
					WHERE
						OpportunityMaster.numDomainId = @numDomainID
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						ReturnItems.numUnitHour,
						ReturnItems.monPrice,
						TEMP.vcCompanyName,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					INNER JOIN	
						ReturnItems
					ON
						ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
					INNER JOIN
						Item
					ON
						Item.numItemCode = ReturnItems.numItemCode
					WHERE
						ReturnHeader.numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT  
					ISNULL(numOppId,0) AS numOppId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcItemName,'') vcItemName,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(numUnitHour,0)numUnitHour,
					ISNULL(monPrice,0.00) monPrice,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,'') tintType
				FROM 
					@TEMPORGITEM 
				ORDER BY 
					CreatedDate DESC 
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY

				SELECT COUNT(numOppId) AS TotalItems FROM @TEMPORGITEM 
			END
			ELSE IF @searchCriteria = '3' -- Opp/Orders
			BEGIN
				SET @SalesOppRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 2
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)                        
                    
				SET @PurchaseOppRights = ( SELECT TOP 1
												MAX(GA.intViewAllowed) AS intViewAllowed
										FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE PM.numpageID = 8
												AND PM.numModuleID = 10
												AND UM.numUserID = ( SELECT numUserID
																		FROM   userMaster
																		WHERE  numUserDetailId = @numUserCntID
																	)
										GROUP BY UM.numUserId ,
												UM.vcUserName ,
												PM.vcFileName ,
												GA.numModuleID ,
												GA.numPageID
										)                
              
				SET @SalesOrderRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 10
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)  
		
				SET @PurchaseOrderRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 11
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)
					
				DECLARE @TEMPORGORDER TABLE
				(
					numOppId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO 
					@TEMPORGORDER
				SELECT
					numOppId,
					bintCreatedDate,
					vcPOppName,
					vcOppType,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						TEMP.vcCompanyName,
						bintCreatedDate,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID
						AND tintOppType <> 0
						AND (OpportunityMaster.numAssignedTo=@numUserCntID OR 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
					UNION 
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						TEMP.vcCompanyName,
						dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT
					ISNULL(numOppId,0) numOppId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,0) tintType
				FROM
					@TEMPORGORDER
				ORDER BY
					CreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGORDER			
			END
			ELSE IF @searchCriteria = '4' -- BizDocs
			BEGIN
				SET @SalesOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 2
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                        
                    
				SET @PurchaseOppRights = ( SELECT TOP 1
												MAX(GA.intViewAllowed) AS intViewAllowed
										FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE PM.numpageID = 8
												AND PM.numModuleID = 10
												AND UM.numUserID = ( SELECT numUserID
																		FROM   userMaster
																		WHERE  numUserDetailId = @numUserCntID
																	)
										GROUP BY UM.numUserId ,
												UM.vcUserName ,
												PM.vcFileName ,
												GA.numModuleID ,
												GA.numPageID
										)                
              
				SET @SalesOrderRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 10
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)  
		
				SET @PurchaseOrderRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 11
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									) 

				SET @BizDocsRights = ( SELECT TOP 1
												MAX(GA.intViewAllowed) AS intViewAllowed
											FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
											WHERE  PM.numpageID = 6
												AND PM.numModuleID = 10
												AND UM.numUserID = ( SELECT numUserID
																		FROM   userMaster
																		WHERE  numUserDetailId = @numUserCntID
																	)
											GROUP BY UM.numUserId ,
												UM.vcUserName ,
												PM.vcFileName ,
												GA.numModuleID ,
												GA.numPageID
										) 
				DECLARE @TEMPORGBizDoc TABLE
				(
					numOppBizDocsId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcBizDocID VARCHAR(200),
					vcBizDocType VARCHAR(100),
					vcCompanyName  VARCHAR(200)
				)

				INSERT INTO
					@TEMPORGBizDoc
				SELECT 
					numOppBizDocsId,
					OpportunityBizDocs.dtCreatedDate,
					vcBizDocID,
					ListDetails.vcData AS vcBizDocType,
					TEMP.vcCompanyName
				FROM 
					OpportunityBizDocs
				INNER JOIN
					OpportunityMaster
				ON
					OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
				INNER JOIN
					#TEMPOrganization TEMP
				ON
					OpportunityMaster.numDivisionId = TEMP.numDivisionID
				INNER JOIN
					ListDetails
				ON
					ListDetails.numListID = 27
					AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
				WHERE
					OpportunityMaster.numDomainId = @numDomainID
					AND (OpportunityMaster.numAssignedTo = @numUserCntID OR 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
					AND 1 = CASE @BizDocsRights
								WHEN 0 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = 0 THEN 1 ELSE 0 END)
								WHEN 1 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = @numUserCntID THEN 1 ELSE 0 END)
								ELSE 1
							END

				SELECT
					ISNULL(numOppBizDocsId,0) numOppBizDocsId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
					ISNULL(vcBizDocID,'') vcBizDocID,
					ISNULL(vcBizDocType,'') vcBizDocType,
					ISNULL(vcCompanyName,'') vcCompanyName
				FROM
					@TEMPORGBizDoc
				ORDER BY
					CreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGBizDoc					
			END

			DROP TABLE #tempOrgSearch
			DROP TABLE #TEMPOrganization
		END
	END
	ELSE IF @searchType = '2' -- Item
	BEGIN
		DECLARE @ItemListRights AS TINYINT                    
               
		SET @ItemListRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
								FROM  
									UserMaster UM 
								INNER JOIN 
									GroupAuthorization GA 
								ON 
									GA.numGroupID = UM.numGroupID 
								INNER JOIN 
									PageMaster PM 
								ON 
									PM.numPageID = GA.numPageID 
									AND PM.numModuleID = GA.numModuleID
								WHERE 
									PM.numpageID = 9
									AND PM.numModuleID = 37
									AND UM.numUserID = (SELECT numUserID FROM userMaster WHERE numUserDetailId = @numUserCntID)
								GROUP BY 
									UM.numUserId
									,UM.vcUserName
									,PM.vcFileName
									,GA.numModuleID
									,GA.numPageID
							)    

		CREATE TABLE #TempSearchedItem
		(
			numItemCode NUMERIC(18,0), 
			vcItemName VARCHAR(200), 
			vcPathForTImage VARCHAR(200), 
			vcCompanyName VARCHAR(200), 
			monListPrice NUMERIC(18,0), 
			txtItemDesc TEXT, 
			vcModelID VARCHAR(100), 
			numBarCodeId VARCHAR(100), 
			vcBarCode VARCHAR(100), 
			vcSKU VARCHAR(100),
			vcWHSKU VARCHAR(100), 
			vcPartNo VARCHAR(100),
			numWarehouseItemID NUMERIC(18,0), 
			vcAttributes VARCHAR(500)
		)

		DECLARE @strItemSearch AS VARCHAR(1000) = ''
		DECLARE @strAttributeSearch AS VARCHAR(1000) = ''
		DECLARE @strSearch AS VARCHAR(1000)

		IF CHARINDEX(',',@searchText) > 0
		BEGIN
			SET @strItemSearch = LTRIM(RTRIM(SUBSTRING(@searchText,0,CHARINDEX(',',@searchText))))
			SET @strAttributeSearch = SUBSTRING(@searchText,CHARINDEX(',',@searchText) + 1,LEN(@searchText)) 

			IF LEN(@strAttributeSearch) > 0
			BEGIN
				if(@isStartWithSearch=0)
				begin
					SET @strAttributeSearch = 'TEMPAttributes.vcAttributes LIKE ''%' + REPLACE(@strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') + '%'''
				end
				else
				begin
					SET @strAttributeSearch = 'TEMPAttributes.vcAttributes LIKE ''%' + REPLACE(@strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') + '%'''
				end
			END
			ELSE
			BEGIN
				SET @strAttributeSearch = ''
			END
		END
		ELSE
		BEGIN
			SET @strItemSearch = @searchText
		END

		DECLARE @vcCustomDisplayField AS VARCHAR(4000) = ''
		SELECT * INTO #TempItemCustomDisplayFields FROM
		(
			SELECT 
				numFieldId,
				vcFieldName
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=0 AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=0
		)TD

		IF (SELECT COUNT(*) FROM #TempItemCustomDisplayFields) > 0
		BEGIN
			SELECT @vcCustomDisplayField = STUFF(
									(SELECT ', ' +  'ISNULL(dbo.GetCustFldValueItem(' + CAST(numFieldId AS VARCHAR) + ',numItemCode),'''') AS [' + vcFieldName + ']' FROM #TempItemCustomDisplayFields FOR XML PATH(''))
									, 1
									, 0
									, ''
								)
		END

		SELECT * INTO #TempItemSearchFields FROM
		(
			SELECT 
				numFieldId,
				vcDbColumnName,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				vcLookBackTableName,
				tintRow AS tintOrder,
				0 as Custom
			FROM 
				View_DynamicColumns
			WHERE 
				numFormId=22 AND numUserCntID=0 AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=1
				AND vcDbColumnName <> 'vcAttributes'
			UNION   
			SELECT 
				numFieldId,
				vcFieldName,
				vcFieldName,
				'CFW_FLD_Values',
				tintRow AS tintOrder,
				1 as Custom
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=0 AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=1
		)Y

		IF (SELECT COUNT(*) FROM #TempItemSearchFields) > 0
		BEGIN
		if(@isStartWithSearch=0)
				begin
			SELECT  @strSearch = STUFF(
									(SELECT ' OR ' +  IIF(Custom = 1,'dbo.GetCustFldValueItem('+ CAST(numFieldId AS VARCHAR) +',numItemCode)',vcDbColumnName) + ' LIKE ''%' + @strItemSearch + '%''' FROM #TempItemSearchFields FOR XML PATH(''))
									, 1
									, 3
									, ''
								)
			end
			else
			begin
			SELECT  @strSearch = STUFF(
									(SELECT ' OR ' +  IIF(Custom = 1,'dbo.GetCustFldValueItem('+ CAST(numFieldId AS VARCHAR) +',numItemCode)',vcDbColumnName) + ' LIKE ''' + @strItemSearch + '%''' FROM #TempItemSearchFields FOR XML PATH(''))
									, 1
									, 3
									, ''
								)
			end
		END
		ELSE
		BEGIN
			if(@isStartWithSearch=0)
			begin
				SELECT @strSearch = 'vcItemName LIKE ''%' + @strItemSearch + '%'''
			end
			else
			begin
				SELECT @strSearch = 'vcItemName LIKE ''' + @strItemSearch + '%'''
			end
		END

		SET @strSQL = 'SELECT
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, MIN(monListPrice) monListPrice, txtItemDesc, 
							vcModelID, numBarCodeId, MIN(vcBarCode) vcBarCode, vcSKU, MIN(vcWHSKU) vcWHSKU, vcPartNo,
							bitSerialized, numItemGroup, Isarchieve, charItemType, 0 AS numWarehouseItemID, '''' vcAttributes
						FROM
						(
						SELECT
							Item.numItemCode,
							ISNULL(vcItemName,'''') AS vcItemName,
							ISNULL((SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = Item.numItemCode),'''') as vcPathForTImage,
							ISNULL(vcCompanyName,'''') AS vcCompanyName,
							ISNULL(monListPrice,''0.00'') AS monListPrice,
							ISNULL(txtItemDesc,'''') AS txtItemDesc,
							ISNULL(vcModelID,'''') AS vcModelID,
							ISNULL(numBarCodeId,'''') AS numBarCodeId,
							ISNULL(vcBarCode,'''') AS vcBarCode,
							ISNULL(vcWHSKU,'''') AS vcWHSKU,
							ISNULL(vcSKU,'''') AS vcSKU,
							ISNULL(vcPartNo,'''') AS vcPartNo,
							ISNULL(bitSerialized,0) AS bitSerialized,
							ISNULL(numItemGroup,0) AS numItemGroup,
							ISNULL(Isarchieve,0) AS Isarchieve,
							ISNULL(charItemType,'''') AS charItemType
						FROM
							Item
						LEFT JOIN
							DivisionMaster 
						ON
							Item.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						LEFT JOIN
							Vendor
						ON
							Item.numVendorID = Vendor.numVendorID AND
							Vendor.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							Item.numItemCode = WareHouseItems.numItemID
						WHERE 
							Item.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + ' AND 1=' +
									CASE @ItemListRights 
										WHEN 0 THEN ' CASE WHEN Item.numCreatedBy=0 THEN 1 ELSE 0 END'
										WHEN 1 THEN ' CASE WHEN Item.numCreatedBy= '+convert(varchar(15),@numUserCntID) + ' THEN 1 ELSE 0 END'                        
										ELSE '1'
									END + '
						)V WHERE ' + @strSearch  + '
						GROUP BY
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, txtItemDesc, 
							vcModelID, numBarCodeId, vcSKU, bitSerialized, numItemGroup, Isarchieve, charItemType, vcPartNo'
	
		IF LEN(@strAttributeSearch) > 0
		BEGIN
			SET @strSQL = 'SELECT
								numItemCode, vcItemName, vcPathForTImage, vcCompanyName, TEMPAttributes.monWListPrice AS monListPrice, txtItemDesc, 
								vcModelID, numBarCodeId, TEMPAttributes.vcBarCode, vcSKU, TEMPAttributes.vcWHSKU, vcPartNo, TEMPAttributes.numWareHouseItemID,
								TEMPAttributes.vcAttributes
							FROM
								(' + @strSQL + ') TEMP
							OUTER APPLY
							(
								SELECT 
									WHI.numWareHouseItemID,
									WHI.vcBarCode,
									WHI.vcWHSKU,
									WHI.monWListPrice,
									STUFF(
												(SELECT 
												'', '' +  vcAttribute
												FROM  
												(
												SELECT
													CONCAT(	CFM.Fld_label, '' : '',
													(CASE 
														WHEN CFM.Fld_type = ''SelectBox''
														THEN 
															(select vcData from ListDetails where numListID= CFM.numlistid AND numListItemID=CFVSI.Fld_Value)
														ELSE
															CFVSI.Fld_Value
													END)) vcAttribute
												FROM
													CFW_Fld_Values_Serialized_Items CFVSI
												INNER JOIN
													CFW_Fld_Master CFM
												ON
													CFM.Fld_id = CFVSI.Fld_ID
												WHERE
													RecId = WHI.numWareHouseItemID
													AND bitSerialized = TEMP.bitSerialized
												) TEMP
												FOR XML PATH(''''))
											, 1
											, 1
											, ''''
											) AS vcAttributes
								FROM
									dbo.WareHouseItems WHI
								WHERE 
									WHI.numItemID = TEMP.numItemCode
									AND ISNULL(TEMP.numItemGroup,0) > 0
									AND ISNULL(TEMP.Isarchieve, 0) <> 1
									AND TEMP.charItemType NOT IN (''A'')
							) AS TEMPAttributes
							WHERE
								' + @strAttributeSearch
		END

		EXEC ('INSERT INTO #TempSearchedItem SELECT numItemCode, vcItemName, vcPathForTImage, vcCompanyName, monListPrice, txtItemDesc, vcModelID, numBarCodeId, vcBarCode, vcSKU, vcWHSKU, vcPartNo, numWarehouseItemID, vcAttributes FROM (' + @strSQL + ')TEMPFinal')

		IF @searchCriteria = '1' --Items
		BEGIN
			SET @strSQL = 'SELECT * ' + @vcCustomDisplayField + ' FROM #TempSearchedItem ORDER BY numItemCode OFFSET ' + CAST(@numSkip AS VARCHAR(100)) + ' ROWS FETCH NEXT 10 ROWS ONLY'
			EXEC (@strSQL)
			SELECT COUNT(*) AS TotalRows FROM #TempSearchedItem
		END
		ELSE IF @searchCriteria = '3' -- Opp/Orders
		BEGIN   
			SET @SalesOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 2
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                        
                    
			SET @PurchaseOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
									FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE PM.numpageID = 8
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
									GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                
              
			SET @SalesOrderRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 10
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)  
		
			SET @PurchaseOrderRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 11
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)  

			DECLARE @TEMPItemOrder TABLE
			(
				numOppId NUMERIC(18,0),
				CreatedDate DATETIME,
				vcPOppName VARCHAR(200),
				vcOppType VARCHAR(50),
				vcCompanyName  VARCHAR(200),
				tintType TINYINT
			)

			INSERT INTO 
				@TEMPItemOrder
			SELECT
				numOppId,
				bintCreatedDate,
				vcPOppName,
				vcOppType,
				vcCompanyName,
				tintType
			FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						CompanyInfo.vcCompanyName,
						OpportunityMaster.bintCreatedDate,
						1 AS tintType
					FROM 
						OpportunityMaster
					INNER JOIN
						(
							SELECT
								numOppID
							FROM 
								OpportunityItems OI
							INNER JOIN
								#TempSearchedItem
							ON
								OI.numItemCode = #TempSearchedItem.numItemCode
								AND (OI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numOppID
						) TempOppItems
					ON
						OpportunityMaster.numOppId = TempOppItems.numOppId
					INNER JOIN
						DivisionMaster 
					ON
						OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						OpportunityMaster.numDomainId =@numDomainID
						AND tintOppType <> 0
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN 1
								 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 5 THEN 0
								 WHEN 6 THEN 0
								END
							)
						AND (OpportunityMaster.numAssignedTo=@numUserCntID AND 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						CompanyInfo.vcCompanyName,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						(
							SELECT
								numReturnHeaderID
							FROM 
								ReturnItems RI
							INNER JOIN
								#TempSearchedItem
							ON
								RI.numItemCode = #TempSearchedItem.numItemCode
								AND (RI.numWareHouseItemID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numReturnHeaderID
						) TempReturnItems
					ON
						ReturnHeader.numReturnHeaderID = TempReturnItems.numReturnHeaderID
					INNER JOIN
						DivisionMaster 
					ON
						ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						ReturnHeader.numDomainId = @numDomainID 
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
								 WHEN 1 THEN 0
								 WHEN 2 THEN 0
								 WHEN 3 THEN 0
								 WHEN 4 THEN 0
								 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
								 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
								END
							)
				) TEMPFinal

			SELECT 
				ISNULL(numOppId,0) numOppId,
				ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
				ISNULL(vcPOppName,'') vcPOppName,
				ISNULL(vcOppType,'') vcOppType,
				ISNULL(vcCompanyName,'') vcCompanyName,
				ISNULL(tintType,0) tintType
			FROM
				@TEMPItemOrder
			ORDER BY
				CreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemOrder	
		END
		ELSE IF @searchCriteria = '4' -- BizDocs
		BEGIN
			SET @SalesOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 2
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                        
                    
			SET @PurchaseOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
									FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE PM.numpageID = 8
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
									GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                
              
			SET @SalesOrderRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 10
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)  
		
			SET @PurchaseOrderRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 11
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								) 

			SET @BizDocsRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 6
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									) 

			DECLARE @TEMPItemBizDoc TABLE
			(
				numOppBizDocsId NUMERIC(18,0),
				CreatedDate DATETIME,
				vcBizDocID VARCHAR(200),
				vcBizDocType VARCHAR(100),
				vcCompanyName  VARCHAR(200)
			)

			INSERT INTO
				@TEMPItemBizDoc
			SELECT 
				numOppBizDocsId,
				OpportunityBizDocs.dtCreatedDate,
				vcBizDocID,
				ListDetails.vcData AS vcBizDocType,
				CompanyInfo.vcCompanyName
			FROM 
				OpportunityBizDocs
			INNER JOIN
				(
					SELECT
						numOppBizDocID
					FROM 
						OpportunityBizDocItems OBI
					INNER JOIN
						#TempSearchedItem
					ON
						OBI.numItemCode = #TempSearchedItem.numItemCode
						AND (OBI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
					GROUP BY
						numOppBizDocID
				) TempOppBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = TempOppBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityMaster
			ON
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
			INNER JOIN
				DivisionMaster
			ON
				OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
			INNER JOIN
				CompanyInfo
			ON
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
			INNER JOIN
				ListDetails
			ON
				ListDetails.numListID = 27
				AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
			WHERE
				OpportunityMaster.numDomainId = @numDomainID
				AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)
				AND (OpportunityMaster.numAssignedTo=@numUserCntID OR 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
				AND 1 = CASE @BizDocsRights
							WHEN 0 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = 0 THEN 1 ELSE 0 END)
							WHEN 1 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = @numUserCntID THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
							ELSE 1
						END

			SELECT
				ISNULL(numOppBizDocsId,0) numOppBizDocsId,
				ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
				ISNULL(vcBizDocID,'') vcBizDocID,
				ISNULL(vcBizDocType,'') vcBizDocType,
				ISNULL(vcCompanyName,'') vcCompanyName
			FROM
				@TEMPItemBizDoc
			ORDER BY
				CreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemBizDoc
		END

		
		DROP TABLE #TempItemSearchFields
		DROP TABLE #TempItemCustomDisplayFields
		DROP TABLE #TempSearchedItem
	END
	ELSE IF @searchType = '3' -- Opps/Orders
	BEGIN
		SET @SalesOppRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 2
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)                        
                    
		SET @PurchaseOppRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
								FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE PM.numpageID = 8
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
								GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)                
              
		SET @SalesOrderRights = ( SELECT TOP 1
									MAX(GA.intViewAllowed) AS intViewAllowed
								FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE  PM.numpageID = 10
									AND PM.numModuleID = 10
									AND UM.numUserID = ( SELECT numUserID
															FROM   userMaster
															WHERE  numUserDetailId = @numUserCntID
														)
								GROUP BY UM.numUserId ,
									UM.vcUserName ,
									PM.vcFileName ,
									GA.numModuleID ,
									GA.numPageID
							)  
		
		SET @PurchaseOrderRights = ( SELECT TOP 1
									MAX(GA.intViewAllowed) AS intViewAllowed
								FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE  PM.numpageID = 11
									AND PM.numModuleID = 10
									AND UM.numUserID = ( SELECT numUserID
															FROM   userMaster
															WHERE  numUserDetailId = @numUserCntID
														)
								GROUP BY UM.numUserId ,
									UM.vcUserName ,
									PM.vcFileName ,
									GA.numModuleID ,
									GA.numPageID
							)  

		PRINT @SalesOppRights
		PRINT @PurchaseOppRights
		PRINT @SalesOrderRights
		PRINT @PurchaseOrderRights
		DECLARE @TEMPORDER TABLE
		(
			numOppId NUMERIC(18,0),
			CreatedDate DATETIME,
			vcPOppName VARCHAR(200),
			vcOppType VARCHAR(50),
			vcCompanyName  VARCHAR(200),
			tintType TINYINT
		)

		INSERT INTO 
			@TEMPORDER
		SELECT
			numOppId,
			bintCreatedDate,
			vcPOppName,
			vcOppType,
			vcCompanyName,
			tintType
		FROM
			(
				SELECT
					OpportunityMaster.numOppId,
					OpportunityMaster.vcPOppName,
					CASE 
						WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
					END vcOppType,
					CompanyInfo.vcCompanyName,
					OpportunityMaster.bintCreatedDate,
					1 AS tintType
				FROM 
					OpportunityMaster
				INNER JOIN
					DivisionMaster 
				ON
					OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					OpportunityMaster.numDomainId =@numDomainID
					AND tintOppType <> 0
					AND (vcPOppName LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END OR numOppId LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END)
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN 1
							 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 5 THEN 0
							 WHEN 6 THEN 0
							END
						)
					AND (OpportunityMaster.numAssignedTo=@numUserCntID OR 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
				UNION
				SELECT
					ReturnHeader.numReturnHeaderID AS numOppID,
					ReturnHeader.vcRMA AS vcPOppName,
					CASE 
						WHEN tintReturnType = 1 THEN 'Sales Return'
						WHEN tintReturnType = 2 THEN 'Purchase Return'
					END vcOppType,
					CompanyInfo.vcCompanyName,
					ReturnHeader.dtCreatedDate AS bintCreatedDate,
					2 AS tintType
				FROM
					ReturnHeader
				INNER JOIN
					DivisionMaster 
				ON
					ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					ReturnHeader.numDomainId = @numDomainID 
					AND (vcRMA LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END OR ReturnHeader.numReturnHeaderID LIKE  CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END)
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
							 WHEN 1 THEN 0
							 WHEN 2 THEN 0
							 WHEN 3 THEN 0
							 WHEN 4 THEN 0
							 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
							 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
							END
						)
				) TEMPFinal

		SELECT 
			ISNULL(numOppId,0) numOppId,
			ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
			ISNULL(vcPOppName,'') vcPOppName,
			ISNULL(vcOppType,'') vcOppType,
			ISNULL(vcCompanyName,'') vcCompanyName,
			ISNULL(tintType,0) tintType
		FROM
			@TEMPORDER
		ORDER BY
			CreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPORDER		
	END
	ELSE IF @searchType = '4' -- BizDocs
	BEGIN
		SET @SalesOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 2
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                        
                    
		SET @PurchaseOppRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
								FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE PM.numpageID = 8
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
								GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)                
              
		SET @SalesOrderRights = ( SELECT TOP 1
									MAX(GA.intViewAllowed) AS intViewAllowed
								FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE  PM.numpageID = 10
									AND PM.numModuleID = 10
									AND UM.numUserID = ( SELECT numUserID
															FROM   userMaster
															WHERE  numUserDetailId = @numUserCntID
														)
								GROUP BY UM.numUserId ,
									UM.vcUserName ,
									PM.vcFileName ,
									GA.numModuleID ,
									GA.numPageID
							)  
		
		SET @PurchaseOrderRights = ( SELECT TOP 1
									MAX(GA.intViewAllowed) AS intViewAllowed
								FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE  PM.numpageID = 11
									AND PM.numModuleID = 10
									AND UM.numUserID = ( SELECT numUserID
															FROM   userMaster
															WHERE  numUserDetailId = @numUserCntID
														)
								GROUP BY UM.numUserId ,
									UM.vcUserName ,
									PM.vcFileName ,
									GA.numModuleID ,
									GA.numPageID
							) 

		SET @BizDocsRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 6
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								) 

		DECLARE @TEMPBizDoc TABLE
		(
			numOppBizDocsId NUMERIC(18,0),
			CreatedDate DATETIME,
			vcBizDocID VARCHAR(200),
			vcBizDocType VARCHAR(100),
			vcCompanyName  VARCHAR(200)
		)

		INSERT INTO
			@TEMPBizDoc
		SELECT 
			numOppBizDocsId,
			OpportunityBizDocs.dtCreatedDate,
			vcBizDocID,
			ListDetails.vcData AS vcBizDocType,
			CompanyInfo.vcCompanyName
		FROM 
			OpportunityBizDocs
		INNER JOIN
			OpportunityMaster
		ON
			OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			ListDetails
		ON
			ListDetails.numListID = 27
			AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID
			AND vcBizDocID LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END
			AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)
			AND (OpportunityMaster.numAssignedTo = @numUserCntID OR 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
				AND 1 = CASE @BizDocsRights
							WHEN 0 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = 0 THEN 1 ELSE 0 END)
							WHEN 1 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = @numUserCntID THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
							ELSE 1
						END

		SELECT
			ISNULL(numOppBizDocsId,0) numOppBizDocsId,
			ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
			ISNULL(vcBizDocID,'') vcBizDocID,
			ISNULL(vcBizDocType,'') vcBizDocType,
			ISNULL(vcCompanyName,'') vcCompanyName
		FROM
			@TEMPBizDoc
		ORDER BY
			CreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPBizDoc
	END
	ELSE IF @searchType = '5' -- Contact
	BEGIN
		DECLARE @ContactsRights TINYINT
		SET @ContactsRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 2
										AND PM.numModuleID = 11
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								) 

		DECLARE @TableContact TABLE
		(
			numContactId NUMERIC(18,0),
			vcFirstName VARCHAR(100),
			vcLastname VARCHAR(100),
			vcFullName VARCHAR(200),
			vcEmail VARCHAR(100),
			numPhone VARCHAR(100),
			numPhoneExtension VARCHAR(100),
			vcCompanyName VARCHAR(200),
			bitPrimaryContact VARCHAR(5)
		)

		INSERT INTO
			@TableContact
		SELECT 
			ADC.numContactId,
			ISNULL(vcFirstName,'') AS vcFirstName,
			ISNULL(vcLastName,'') AS vcLastName,
			ISNULL(vcFirstName,'') + ' ' + ISNULL(vcLastName,'') AS vcFullName,
			ISNULL(vcEmail,'') AS vcEmail,
			ISNULL(ADC.numPhone,'') AS numPhone,
			ISNULL(ADC.numPhoneExtension,'') AS numPhoneExtension,
			ISNULL(CI.vcCompanyName,'') AS vcCompanyName,
			(CASE WHEN ISNULL(bitPrimaryContact,0) = 1 THEN 'Yes' ELSE 'No' END) AS bitPrimaryContact
		FROM 
			AdditionalContactsInformation ADC
		INNER JOIN
			DivisionMaster DM
		ON
			ADC.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		WHERE 
			ADC.numDomainID = @numDomainID AND
			(vcFirstName LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END
			OR vcLastName LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END
			OR vcEmail LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END
			OR numPhone LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END)
			AND 1 = CASE @ContactsRights
							WHEN 0 THEN (CASE WHEN ISNULL(ADC.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
							WHEN 1 THEN (CASE WHEN ISNULL(ADC.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (DM.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DM.numTerID=0) THEN 1 ELSE 0 END)
							ELSE 1
						END

		SELECT 
			* 
		FROM 
			@TableContact	
		ORDER BY 
			vcCompanyName ASC, bitPrimaryContact DESC
		OFFSET 
			@numSkip 
		ROWS FETCH NEXT 
			10 
		ROWS ONLY

		SELECT COUNT(*) FROM @TableContact
	END
GO