--USP_GetCommissionTable 360
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCommissionTable')
DROP PROCEDURE USP_GetCommissionTable
GO
CREATE PROCEDURE USP_GetCommissionTable
    @numComRuleID NUMERIC
AS 
    BEGIN
        SELECT  [numComRuleDtlID],
                [numComRuleID],
                [intFrom],
                [intTo],
                [decCommission]
        FROM    [CommissionRuleDtl]
        WHERE   [numComRuleID] = @numComRuleID
    END
    