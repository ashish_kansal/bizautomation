SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageRuleCondition')
DROP PROCEDURE USP_ManageRuleCondition
GO
CREATE PROCEDURE [dbo].USP_ManageRuleCondition
    @numRuleID AS NUMERIC(9),
    @numDomainID AS NUMERIC(9),
    @vcCondition AS varchar(4000),
    @tintConditionOrder AS TINYINT, 
    @vcJSON AS text
AS 
    IF @tintConditionOrder = 0 
	BEGIN
		delete from RuleCondition where numRuleID=@numRuleID
		delete from RuleAction where numRuleID=@numRuleID
	END
           
			INSERT  INTO RuleCondition
                    (
                      numDomainId,numRuleID,vcCondition,tintConditionOrder,vcJSON
                    )
            VALUES  (
                      @numDomainId,@numRuleID,@vcCondition,@tintConditionOrder,@vcJSON
                    )

            SELECT  SCOPE_IDENTITY()  
GO
