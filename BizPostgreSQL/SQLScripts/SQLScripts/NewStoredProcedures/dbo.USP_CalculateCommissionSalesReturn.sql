GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionSalesReturn')
DROP PROCEDURE USP_CalculateCommissionSalesReturn
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionSalesReturn]
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	DECLARE @TempCommissionSalesReturn TABLE
	(
		numComPayPeriodID NUMERIC(18,0)
		,numComRuleID NUMERIC(18,0)
		,numUserCntID NUMERIC(18,0)
		,tintAssignTo TINYINT
		,numReturnHeaderID NUMERIC(18,0)
		,numReturnItemID NUMERIC(18,0)
		,tintComType TINYINT
		,tintComBasedOn TINYINT
		,decCommission FLOAT
		,monCommission DECIMAL(20,5)
	)

	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	DELETE FROM 
		SalesReturnCommission 
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitCommissionReversed,0)=0 
		AND numReturnItemID IN (SELECT 
									ReturnHeader.numReturnHeaderID
								FROM 
									ReturnHeader
								INNER JOIN
									ReturnItems
								ON
									ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
								CROSS APPLY
								(
									SELECT 
										MAX(datEntry_Date) dtDepositDate
									FROM 
										General_Journal_Header 
									WHERE 
										numDomainId=@numDomainId
										AND numReturnID=ReturnHeader.numReturnHeaderID
								) TempDepositMaster
								WHERE
									ReturnHeader.numDomainId = @numDomainID 
									AND ReturnHeader.numReturnStatus = 303
									AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

	INSERT INTO 
		@TempCommissionSalesReturn
	SELECT 
		@numComPayPeriodID
		,BizDocComission.numComRuleID
		,BizDocComission.numUserCntID
		,BizDocComission.tintAssignTo
		,ReturnHeader.numReturnHeaderID
		,ReturnItems.numReturnItemID
		,BizDocComission.tintComType
		,BizDocComission.tintComBasedOn
		,BizDocComission.decCommission
		,CASE tintComType 
			WHEN 1 --PERCENT
				THEN 
					CASE 
						WHEN ISNULL(bitDomainCommissionBasedOn,0) = 1
						THEN
							CASE tintDomainCommissionBasedOn
								--ITEM GROSS PROFIT (VENDOR COST)
								WHEN 1 THEN (ISNULL(ReturnItems.monTotAmount,0) - ISNULL(OpportunityItems.monVendorCost,0)) * (BizDocComission.decCommission / CAST(100 AS FLOAT))
								--ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 2 THEN(ISNULL(ReturnItems.monTotAmount,0) - ISNULL(OpportunityItems.monAvgCost,0)) * (BizDocComission.decCommission / CAST(100 AS FLOAT))
							END
						ELSE
							ISNULL(ReturnItems.monTotAmount,0) * (BizDocComission.decCommission / CAST(100 AS FLOAT))
					END
			ELSE  --FLAT
				BizDocComission.decCommission
		END monCommissionReversed
	FROM
		ReturnHeader
	INNER JOIN
		ReturnItems
	ON
		ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
	INNER JOIN
		BizDocComission
	ON
		ReturnItems.numOppItemID = BizDocComission.numOppItemID
	INNER JOIN
		OpportunityItems 
	ON
		ReturnItems.numOppItemID = OpportunityItems.numoppitemtCode
	CROSS APPLY
	(
		SELECT 
			MAX(datEntry_Date) dtDepositDate
		FROM 
			General_Journal_Header 
		WHERE 
			numDomainId=@numDomainId
			AND numReturnID=ReturnHeader.numReturnHeaderID
	) TempDepositMaster
	WHERE
		ReturnHeader.numDomainId = @numDomainID 
		AND ReturnHeader.numReturnStatus = 303
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
	
	-- UPDATE FLAT COMMMISSION AMOUNT 
	UPDATE
		T1
	SET
		T1.monCommission= decCommission/(SELECT COUNT(*) FROM @TempCommissionSalesReturn Tinner WHERE Tinner.numUserCntID=T1.numUserCntID AND Tinner.tintComType=2)
	FROM
		@TempCommissionSalesReturn T1
	WHERE
		tintComType=2

	INSERT INTO SalesReturnCommission
	(
		numDomainID
		,numComPayPeriodID
		,numComRuleID
		,numUserCntID
		,tintAssignTo
		,numReturnHeaderID
		,numReturnItemID
		,tintComType
		,tintComBasedOn
		,decCommission
		,monCommissionReversed
		,bitCommissionReversed
	)
	SELECT
		@numDomainID
		,numComPayPeriodID
		,numComRuleID
		,numUserCntID
		,tintAssignTo
		,numReturnHeaderID
		,numReturnItemID
		,tintComType
		,tintComBasedOn
		,decCommission
		,monCommission
		,0
	FROM
		@TempCommissionSalesReturn T1
	WHERE
		(SELECT 
			COUNT(*) 
		FROM 
			SalesReturnCommission SRC
		WHERE 
			SRC.numUserCntID=T1.numUserCntID 
			AND SRC.numReturnHeaderID=T1.numReturnHeaderID 
			AND SRC.numReturnItemID = T1.numReturnItemID 
			AND SRC.tintAssignTo = T1.tintAssignTo
		) = 0

END
GO