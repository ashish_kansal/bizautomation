/****** Object:  StoredProcedure [dbo].[usp_GetContactInfo]    Script Date: 07/26/2008 16:16:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OverrideAssigntoEmpList')
DROP PROCEDURE USP_OverrideAssigntoEmpList
GO
CREATE PROCEDURE [dbo].[USP_OverrideAssigntoEmpList]        
 @numDomainId numeric=0        
AS        
BEGIN        
		
    SELECT 
		O.numContactId,A.vcFirstName+' '+A.vcLastName as [Name],O.numAssignedContact
	FROM 
		OverrideAssignTo AS O 
	LEFT JOIN 
		AdditionalContactsInformation AS A  
	ON
		O.numContactId=A.numContactId
	WHERE 
		O.numDomainID=@numDomainId
	ORDER BY
		A.vcFirstName
END
GO