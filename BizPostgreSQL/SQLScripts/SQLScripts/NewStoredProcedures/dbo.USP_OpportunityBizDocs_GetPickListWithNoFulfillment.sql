GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetPickListWithNoFulfillment')
DROP PROCEDURE USP_OpportunityBizDocs_GetPickListWithNoFulfillment
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetPickListWithNoFulfillment]
(
    @numOppID AS NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		OBDPickList.numOppBizDocsId
		,OBDPickList.vcBizDocID
	FROM
		OpportunityBizDocs OBDPickList
	LEFT JOIN
		OpportunityBizDocs OBDFilfillment
	ON
		OBDFilfillment.numOppId=@numOppID
		AND OBDFilfillment.numBizDocId=296 --Fulfillment BizDoc
		AND OBDFilfillment.numSourceBizDocId = OBDPickList.numOppBizDocsId
	WHERE
		OBDPickList.numOppId=@numOppID
		AND OBDPickList.numBizDocId=29397
		AND OBDFilfillment.numOppBizDocsId IS NULL
END
GO