GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUOMConversionAllUnit')
DROP PROCEDURE USP_GetUOMConversionAllUnit
GO
CREATE PROCEDURE [dbo].[USP_GetUOMConversionAllUnit]                                                                          
 @numDomainID as numeric(9),    
 @numUOMId as numeric(9)
AS       
   
 SELECT u.numUOMId,u.vcUnitName,Cast(u.numUOMId AS VARCHAR(20))+ '~' + CAST(dbo.fn_UOMConversion(u.numUOMId,0,@numDomainID,@numUOMId) AS VARCHAR(20)) UOMConversionFactor FROM 
      UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
        WHERE u.numDomainID=@numDomainID AND d.numDomainID=@numDomainID AND 
        u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)