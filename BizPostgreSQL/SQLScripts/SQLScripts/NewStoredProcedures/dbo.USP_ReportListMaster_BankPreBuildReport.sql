SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_BankPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_BankPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_BankPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	SELECT 
		CAST(SUM(numOpeningBal) AS DECIMAL(20,5)) AS MoneyInBank
	FROM 
		Chart_Of_Accounts 
	WHERE 
		numDomainId=@numDomainID 
		AND numParntAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode='01010101')
		AND numAccountId NOT IN (SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainID AND numChargeTypeId=9)
END
GO