GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWebReferringInfo')
DROP PROCEDURE USP_ManageWebReferringInfo
GO
CREATE PROCEDURE [dbo].[USP_ManageWebReferringInfo]    
	@numDomainID AS NUMERIC(9) = 0,
	@searchText AS VARCHAR(500) = NULL,
	@numListItemID AS NUMERIC(9) = 0 
AS  
BEGIN
	
	DECLARE @LeadSource AS VARCHAR(100)
	--@numDivisionID AS NUMERIC(18,0), @LeadSource AS VARCHAR(100)

	--SELECT @numDivisionID = numDivisionID
	--FROM  CompanyInfo CMP    
	--	JOIN DivisionMaster DM ON DM.numCompanyID = CMP.numCompanyID   
	--WHERE vcHow = @numListItemID AND DM.numDomainID = @numDomainID    

	SELECT @LeadSource = vcData FROM ListDetails WHERE numListItemID = @numListItemID AND numDomainID = @numDomainID   
	
	--print 'SELECT * FROM TrackingVisitorsHDR 
	--			WHERE numDivisionID = @numDivisionID 
	--				AND numDomainID = @numDomainID 
	--				AND vcOrginalRef LIKE ''%' + @searchText + '%'''

	IF EXISTS(SELECT * FROM TrackingVisitorsHDR 
				WHERE numListItemID = @numListItemID --numDivisionID = @numDivisionID 
					AND numDomainID = @numDomainID 
					AND vcOrginalRef  = @searchText)
	BEGIN
 		RAISERROR ( 'DUPLICATE_REFERRER',16, 1 )
 		RETURN ;
	END
	ELSE
	BEGIN
		--IF @numDivisionID IS NULL
		--BEGIN
 	--		RAISERROR ( 'DIVISION_NULL',16, 1 )
 	--		RETURN ;
		--END
		INSERT INTO TrackingVisitorsHDR
			(vcOrginalRef,numListItemID,numDomainID,vcReferrer)
		VALUES
			(@searchText, @numListItemID, @numDomainID,@LeadSource)

		--SELECT numTrackingID,vcOrginalRef, (SELECT vcData FROM Listdetails WHERE numListID=18 AND numdomainid = @numdomainid) AS vcData
		--FROM TrackingVisitorsHDR 
		--WHERE numDomainID = @numDomainID
		--WHERE numDivisionID = @numDivisionID
	END

	/*DECLARE @Text AS VARCHAR(500)

	SELECT @Text = vcOrginalRef 
	FROM TrackingVisitorsHDR 
	WHERE numDivisionID = @numDivisionID AND numDomainID = @numDomainID */

END
