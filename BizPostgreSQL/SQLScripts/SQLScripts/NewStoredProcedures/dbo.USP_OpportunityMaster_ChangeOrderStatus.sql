SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_ChangeOrderStatus')
DROP PROCEDURE USP_OpportunityMaster_ChangeOrderStatus
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_ChangeOrderStatus]
(                        
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numStatus NUMERIC(18,0),
	@vcOppIds AS VARCHAR(MAX) -- Comma seperated list of order ids
)                        
AS 
BEGIN
	IF LEN(ISNULL(@vcOppIds,'')) > 0
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppID
		)
		SELECT
			ID
		FROM
			dbo.SplitIDs(@vcOppIds,',')


		UPDATE OpportunityMaster SET numStatus=@numStatus,numModifiedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() WHERE numDomainId=@numDomainID AND numOppId IN (SELECT numOppID FROM @TEMP)

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numOppID NUMERIC(18,0)
		SELECT @iCount=COUNT(*) FROM @TEMP

		WHILE @i <= @iCount
		BEGIN
			SELECT @numOppID=numOppID FROM @TEMP WHERE ID=@i

			EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numStatus

			SET @i = @i + 1
		END
	END
END