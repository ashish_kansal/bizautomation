GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailBroadcastConfiguration_GetByDomain' ) 
    DROP PROCEDURE USP_EmailBroadcastConfiguration_GetByDomain
GO

CREATE PROCEDURE USP_EmailBroadcastConfiguration_GetByDomain  
(  
 @numDomainID NUMERIC   
)  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

SELECT * FROM EmailBroadcastConfiguration WHERE numDomainId = @numDomainID

END  
