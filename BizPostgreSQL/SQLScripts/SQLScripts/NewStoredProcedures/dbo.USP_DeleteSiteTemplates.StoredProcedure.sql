
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteSiteTemplates]    Script Date: 08/08/2009 16:09:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteSiteTemplates')
DROP PROCEDURE USP_DeleteSiteTemplates
GO
CREATE PROCEDURE [dbo].[USP_DeleteSiteTemplates]
	@numTemplateID NUMERIC(9),
	@numDomainID numeric(9)
AS
BEGIN
	DELETE FROM SiteTemplates
	WHERE [numTemplateID] = @numTemplateID AND [numDomainID] = @numDomainID
	
END


