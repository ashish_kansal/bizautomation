SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_ManageUserReportList')
DROP PROCEDURE Usp_ManageUserReportList
GO
CREATE PROCEDURE [dbo].[Usp_ManageUserReportList]
@numReportId as numeric(9),
@numDomainId as numeric(9),
@numUserCntId as numeric(9),
@tintReportType AS TINYINT 
as

IF NOT EXISTS(SELECT 1 FROM UserReportList WHERE numDomainId=@numDomainId AND numUserCntId=@numUserCntId 
				AND numReportId=@numReportId AND tintReportType=@tintReportType)
BEGIN
	INSERT INTO dbo.UserReportList (numReportID,numDomainID,numUserCntID,tintReportType
	) VALUES (@numReportID,@numDomainID,@numUserCntID,@tintReportType) 
	
END				
GO
