GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportKPIGroupListMaster')
DROP PROCEDURE USP_GetReportKPIGroupListMaster
GO
CREATE PROCEDURE [dbo].[USP_GetReportKPIGroupListMaster] 
	@numDomainID NUMERIC(18,0),
	@numReportKPIGroupID NUMERIC(18,0),
    @tintMode AS TINYINT
as                 

IF @tintMode=0 --Select KPI Group Detail
BEGIN
	IF @numReportKPIGroupID=0
	BEGIN
		SELECT *,CASE tintKPIGroupReportType WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END AS vcKPIGroupReportType FROM ReportKPIGroupListMaster WHERE numDomainID=@numDomainID 
	END
	ELSE
	BEGIN
		SELECT * FROM ReportKPIGroupListMaster WHERE numDomainID=@numDomainID AND numReportKPIGroupID=@numReportKPIGroupID
	END
END
ELSE IF @tintMode=1  --Select KPI Group Reports Detail
BEGIN
	SELECT RLM.numReportID,RLM.vcReportName,RLM.vcReportDescription,RMM.vcModuleName,RMGM.vcGroupName
	from ReportListMaster RLM JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
	JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	join ReportKPIGroupDetailList KPI on RLM.numReportID=KPI.numReportID 
	where RLM.numDomainID=@numDomainID AND KPI.numReportKPIGroupID=@numReportKPIGroupID
	order by RLM.numReportID 
END
ELSE IF @tintMode=2 --Select Available KPI Reports
BEGIN
	DECLARE @tintKPIGroupReportType AS TINYINT
	SELECT @tintKPIGroupReportType=tintKPIGroupReportType FROM ReportKPIGroupListMaster WHERE numDomainID=@numDomainID AND numReportKPIGroupID=@numReportKPIGroupID
	
	SELECT numReportID,vcReportName FROM ReportListMaster WHERE numDomainID=@numDomainID AND tintReportType=@tintKPIGroupReportType
	AND numReportID NOT IN(SELECT numReportID FROM ReportKPIGroupDetailList WHERE numReportKPIGroupID=@numReportKPIGroupID)
END
	
