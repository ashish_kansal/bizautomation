GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAPIDetails')
DROP PROCEDURE USP_GetAPIDetails
GO
CREATE PROCEDURE [dbo].[USP_GetAPIDetails]
               @numDomainID NUMERIC(9)  = 0
AS
  BEGIN
    SELECT [numDomainId],
           [bitEnableAPI],
           vcFirstFldValue,
           vcSecondFldValue,
           vcThirdFldValue,
           vcFourthFldValue,
           vcFifthFldValue,
           vcSixthFldValue,
           bitEnableAPI
    FROM   [WebAPI] W
           LEFT JOIN [WebAPIDetail] WD
             ON W.[WebApiId] = WD.[WebApiId]
    WHERE  WD.bitEnableAPI = 1
           AND (numDomainID = @numDomainID
                 OR @numDomainID = 0)
  END
GO