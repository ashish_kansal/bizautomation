/****** Object:  StoredProcedure [dbo].[USP_GetStyleSheets]    Script Date: 08/08/2009 15:02:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetStyleSheets')
DROP PROCEDURE USP_GetStyleSheets
GO
CREATE PROCEDURE [dbo].[USP_GetStyleSheets]
	@numCssID NUMERIC(9)=0,
	@numSiteID NUMERIC(9),
    @numDomainID NUMERIC(9),
	@tintType TINYINT=0 -- 0- css , 1- javascript
AS
BEGIN
IF @numSiteID = 0
	SET @numSiteID = NULL;
	
	IF @tintType = 0	
	  BEGIN
            SELECT [numCssID], 
	               [StyleName], 
	               [StyleFileName], 
	               [numSiteID], 
	               [numDomainID],
	               0 AS intDisplayOrder
            FROM StyleSheets
            WHERE ([numCssID] = @numCssID OR @numCssID = 0)
                   AND (numSiteID = @numSiteID OR @numSiteID IS NULL)
                   AND [numDomainID] = @numDomainID
                   AND tintType = @tintType	  	
	  END
    
    ELSE IF @tintType = 1
      BEGIN
	  	        SELECT [numCssID], 
	               [StyleName], 
	               [StyleFileName], 
	               [numSiteID], 
	               [numDomainID],
	               ISNULL(intDisplayOrder,0) AS intDisplayOrder
            FROM StyleSheets
            WHERE ([numCssID] = @numCssID OR @numCssID = 0)
                   AND (numSiteID = @numSiteID OR @numSiteID IS NULL)
                   AND [numDomainID] = @numDomainID
                   AND tintType = @tintType	  	
	  END
	  ELSE IF @tintType = 3
      BEGIN
	  	        SELECT [numCssID], 
	               [StyleName], 
	               [StyleFileName], 
	               [numSiteID], 
	               [numDomainID],
	               ISNULL(intDisplayOrder,0) AS intDisplayOrder
            FROM StyleSheets
            WHERE ([numCssID] = @numCssID OR @numCssID = 0)
                   AND (numSiteID = @numSiteID OR @numSiteID IS NULL)
                   AND [numDomainID] = @numDomainID
                   AND tintType = @tintType	  	
	  END
	  ELSE IF @tintType = 4
      BEGIN
	  	        SELECT [numCssID], 
	               [StyleName], 
	               [StyleFileName], 
	               [numSiteID], 
	               [numDomainID],
	               ISNULL(intDisplayOrder,0) AS intDisplayOrder
            FROM StyleSheets
            WHERE ([numCssID] = @numCssID OR @numCssID = 0)
                   AND (numSiteID = @numSiteID OR @numSiteID IS NULL)
                   AND [numDomainID] = @numDomainID
                   AND tintType = @tintType	  	
	  END
END
--exec USP_GetStyleSheets @numCssID=0,@numSiteID=0,@numDomainID=1,@tintType=3
