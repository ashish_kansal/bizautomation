SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CategoryProfile_GetAll')
DROP PROCEDURE dbo.USP_CategoryProfile_GetAll
GO
CREATE PROCEDURE [dbo].[USP_CategoryProfile_GetAll]
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	SELECT ID,vcName FROM CategoryProfile WHERE numDomainID=@numDomainID
END