GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentShippingConfig_Get')
DROP PROCEDURE USP_MassSalesFulfillmentShippingConfig_Get
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentShippingConfig_Get]
(
	@numDomainID NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
	,@tintSourceType TINYINT
)
AS
BEGIN
	SELECT 
		* 
	FROM 
		MassSalesFulfillmentShippingConfig 
	WHERE 
		numDomainID=@numDomainID 
		AND numOrderSource=@numOrderSource 
		AND tintSourceType=@tintSourceType
END
GO