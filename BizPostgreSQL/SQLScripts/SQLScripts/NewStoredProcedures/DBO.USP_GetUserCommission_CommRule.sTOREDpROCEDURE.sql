/****** Object:  StoredProcedure [dbo].[USP_GetUserCommission_CommRule]    Script Date: 02/28/2009 13:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
-- [dbo].[USP_GetUserCommission_CommRule] 1,1,1,-333,'1/1/2009','1/15/2009'
-- exec USP_GetUserCommission_CommRule @numUserId=372,@numUserCntID=82979,@numDomainId=1,@ClientTimeZoneOffset=-330,@dtStartDate='2010-03-01 00:00:00:000',@dtEndDate='2010-04-01 00:00:00:000'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserCommission_CommRule')
DROP PROCEDURE USP_GetUserCommission_CommRule
GO
CREATE PROCEDURE [dbo].[USP_GetUserCommission_CommRule]
(
               @numDomainId          AS NUMERIC(9)  = 0,
               @numUserCntID         AS NUMERIC(9)  = 0,
			   @numComRuleID            AS NUMERIC(9)  = 0,
               @numItemCode numeric(9),
               @bitCommContact AS BIT 
)
AS
  BEGIN

DECLARE @tintComAppliesTo TINYINT 
DECLARE @tintAssignTo TINYINT,@tinComDuration TINYINT 
DECLARE @dFrom DATETIME,@dTo Datetime    

SELECT TOP 1 @tinComDuration=CR.tinComDuration,@tintAssignTo=CR.tintAssignTo, @tintComAppliesTo = ISNULL(CR.tintComAppliesTo,0)
			FROM CommissionRules CR WHERE CR.numDomainID=@numDomainID AND CR.numComRuleID=@numComRuleID
	
						--Get From and To date for Commission Calculation
					IF @tinComDuration=1 --Month
							SELECT @dFrom=DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0),
									@dTo=DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE()) + 1, 0)) 
	
						ELSE IF @tinComDuration=2 --Quarter
							SELECT @dFrom=DATEADD(q, DATEDIFF(q, 0, GETDATE()), 0),
									@dTo=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, GETDATE()) + 1, 0)) 
									
						ELSE IF @tinComDuration=3 --Year
							SELECT @dFrom=DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0),
									@dTo=DATEADD(d, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, 0)) 
																
 
 Select oppBiz.numBizDocID,oppBiz.vcBizDocID,Opp.numOppId,oppBiz.numOppBizDocsId,isnull(oppBiz.monAmountPaid,0) monAmountPaid,Opp.bintCreatedDate, Opp.vcPOppName AS Name ,Case when Opp.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, isnull(OppBiz.monAmountPaid,0) as DealAmount,
		BC.numComissionID,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
		Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour,BDI.monTotAmount,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost          
From OpportunityMaster Opp left join OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID 
INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
Where oppBiz.bitAuthoritativeBizDocs = 1 AND oppbiz.monAmountPaid > 0 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And 
1= (CASE @tintAssignTo WHEN 1 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN Opp.numAssignedTo=@numUserCntID THEN 1 ELSE 0 END 
										                WHEN 1 THEN CASE WHEN Opp.numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END  END
				       WHEN 2 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN Opp.numRecOwner=@numUserCntID THEN 1 ELSE 0 END 
														WHEN 1 THEN CASE WHEN Opp.numRecOwner IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END END END)	 				 	 

AND 1=(CASE @bitCommContact WHEN 0 THEN CASE WHEN BC.numUserCntId=@numUserCntID THEN 1 else 0 END 
							WHEN 1 THEN CASE WHEN BC.numUserCntId IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END  END)
									
And Opp.numDomainId=@numDomainId AND BC.numComRuleID=@numComRuleID 
AND 1 = (CASE @tintComAppliesTo WHEN 1 THEN CASE WHEN BDI.numItemCode=@numItemCode THEN 1 ELSE 0 END
								WHEN 2 THEN CASE WHEN BDI.numItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification=@numItemCode AND numDomainID=@numDomainID) THEN 1 ELSE 0 END
								WHEN 3 THEN 1 ELSE 0 END)	
AND oppBiz.numOppBizDocsId IN (SELECT OBD.numOppBizDocsId FROM OpportunityBizDocsDetails BDD
INNER JOIN dbo.OpportunityBizDocs OBD ON BDD.numBizDocsId = OBD.numOppBizDocsId
INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId
WHERE tintopptype = 1 AND tintoppstatus = 1 AND OM.numdomainid = @numDomainId 
AND OBD.bitAuthoritativeBizDocs = 1 AND BDD.dtCreationDate BETWEEN @dFrom AND @dTo AND 
1= (CASE @tintAssignTo WHEN 1 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN OM.numAssignedTo=@numUserCntID THEN 1 ELSE 0 END 
										                WHEN 1 THEN CASE WHEN OM.numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END  END
				       WHEN 2 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN OM.numRecOwner=@numUserCntID THEN 1 ELSE 0 END 
														WHEN 1 THEN CASE WHEN OM.numRecOwner IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END END END)	 				 	 
								
GROUP BY OBD.numOppBizDocsId HAVING SUM(BDD.monAmount)>=MAX(OBD.monDealAmount))
ORDER BY  oppBiz.numOppBizDocsId desc
  END
