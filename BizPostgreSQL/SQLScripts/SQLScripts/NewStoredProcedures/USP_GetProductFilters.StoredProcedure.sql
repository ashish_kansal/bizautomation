
GO
IF EXISTS(SELECT * FROM sysobjects where xtype='p'AND NAME ='USP_GetProductFilters')
DROP PROCEDURE USP_GetProductFilters
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GetProductFilters]
    @numDomainId NUMERIC ,
    @numCategoryID NUMERIC,
    @FilterQuery AS VARCHAR(2000) = ''
AS 
    BEGIN  
   
        DECLARE @sqlQuery AS NVARCHAR(MAX) 
        DECLARE @sqlwhere AS NVARCHAR(MAX) 
        DECLARE @sqlFilter AS NVARCHAR(MAX)
        DECLARE @dynamicFilterQuery NVARCHAR(max) 
        DECLARE @where NVARCHAR(max) 
        DECLARE @data AS VARCHAR(100)
        DECLARE @row AS INTEGER        
        DECLARE @checkbox VARCHAR(2000)
        DECLARE @checkboxGroupBy VARCHAR(2000)
        DECLARE @dropdown VARCHAR(2000)
        DECLARE @dropdownGroupBy VARCHAR(2000)
        DECLARE @textbox VARCHAR(2000)
        DECLARE @textboxGroupBy VARCHAR(2000)
        DECLARE @filterNumItemIds VARCHAR(2000)
        DECLARE @price VARCHAR(2000)
        DECLARE @manufacturer VARCHAR(2000)
        DECLARE @manufacturerGroupBy VARCHAR(2000)
        DECLARE @attributes VARCHAR(2000)
        DECLARE @filterByNumItemCode VARCHAR(3000)
        DECLARE @count NUMERIC(9,0)
       
        BEGIN
            
            SET @where = ''
            SET @checkbox = ''
            PRINT(@FilterQuery)
            IF LEN(@FilterQuery) > 0
				BEGIN 
				   
				    SELECT row_number() over(order by data) as row, data into #temp FROM dbo.SplitString(@FilterQuery,';')
				    
				    select top 1 @row = row ,@data =data from #temp 
				    PRINT(@data)
		            SET @dynamicFilterQuery = ''
		            SET @count = 0
		            WHILE @row>0
	                  BEGIN
	                     
	                     IF SUBSTRING(@data,1,1) = 'f'
	                        BEGIN	
	                            IF  CHARINDEX('-2',@data) >  0   
	                                BEGIN
	                                  SET @dynamicFilterQuery  =  'SELECT      I.numItemCode 
                               	                                   FROM 
                               	                                   dbo.Item    I 
                               	                                   INNER JOIN  dbo.ItemCategory  IC
                                                                   ON          I.numItemCode = IC.numItemID 
                                     
                                                                   WHERE       IC.numCategoryID =' + CONVERT(VARCHAR(20), @numCategoryID)+ ' 
                                                             
                                                                   AND         I.numDomainID    =' + CONVERT(VARCHAR(20), @numDomainID)+ '
                              	                                   AND         I.monListPrice 
	                                                               BETWEEN     SUBSTRING('''+@data+''',CHARINDEX(''='','''+@data+''')+1,(CHARINDEX(''~'','''+@data+''')) - (CHARINDEX(''='','''+@data+''')+1) )
                                                                   AND         SUBSTRING('''+@data+''',CHARINDEX(''~'','''+@data+''') + 1,(LEN('''+@data+'''))- (CHARINDEX(''~'','''+@data+''')))'
	                                                               PRINT(@dynamicFilterQuery)
	                                END 
                                ELSE IF CHARINDEX('-1',@data) > 0
                                    BEGIN
                              	     SET @dynamicFilterQuery  =   'SELECT      I.numItemCode 
                               	                                   FROM 
                               	                                   dbo.Item    I 
                               	                                   INNER JOIN  dbo.ItemCategory  IC
                                                                   ON          I.numItemCode = IC.numItemID 
                                     
                                                                   WHERE       IC.numCategoryID =' + CONVERT(VARCHAR(20), @numCategoryID)+ ' 
                                                                   AND         I.numDomainID    =' + CONVERT(VARCHAR(20), @numDomainID)+ '
                              	                                   AND         I.vcManufacturer = SUBSTRING('''+@data+''',CHARINDEX(''='','''+@data+''')+1,(LEN('''+@data+'''))-(CHARINDEX(''='','''+@data+''')))'
						            END
					        	ELSE   
						           BEGIN
						             SET @dynamicFilterQuery = ' SELECT        DISTINCT     
                                                                               I.numItemCode 
                                                                 FROM          dbo.Item I 
                                                                 INNER JOIN    dbo.ItemCategory IC 
                                                                 ON            IC.numItemID = i.numItemCode
                                                                 INNER JOIN    dbo.ItemGroupsDTL IGD 
                                                                 ON            I.numItemGroup = IGD.numItemGroupID
                                                                 INNER JOIN    dbo.CFW_Fld_Master CFM 
                                                                 ON                IGD.numOppAccAttrID = CFM.Fld_id 
                                                                               AND CFM.numDomainID= '+CONVERT(VARCHAR(10),@numDomainId) +'
                                                                 LEFT JOIN     dbo.ListDetails LD 
                                                                 ON                LD.numListID = CFM.numlistid 
                                                                               AND CFM.numDomainID = LD.numDomainID
                                                                 INNER JOIN    dbo.WareHouseItems WHI 
                                                                 ON            WHI.numItemID = I.numItemCode 
                                                                 INNER JOIN    dbo.CFW_Fld_Values_Serialized_Items CFVSI 
                                                                 ON            CFVSI.RecId = WHI.numWareHouseItemID
                   
                                                                 WHERE 
                                                                     IC.numCategoryID = '+CONVERT(VARCHAR(10),@numCategoryID)+' 
                                                                 AND I.numDomainID = '+CONVERT(VARCHAR(10),@numDomainId)+' 
                                                                 AND LD.numListItemID = CFVSI.Fld_Value 
                                                                 AND CFM.Fld_id  =  CFVSI.Fld_ID
                                                                 AND CFM.Fld_id = CONVERT(NUMERIC(9,0),  SUBSTRING('''+@data+''',CHARINDEX(''>'','''+@data+''')+1,CHARINDEX(''='','''+@data+''')-(CHARINDEX(''>'','''+@data+''')+1) )) 
                                                                 AND LD.numListItemID = CONVERT( VARCHAR(1000), SUBSTRING('''+@data+''',CHARINDEX(''='','''+@data+''')+1,LEN('''+@data+''')))  
                     '
                                                                       
						           END
	                         END
		                 ELSE IF SUBSTRING(@data,1,1) = 'c'
		                     BEGIN
		                           SET @dynamicFilterQuery =  ' 
	                                                             SELECT        I.numItemCode AS numItemCode 
	                                                             FROM          Item I 
                                                                 INNER JOIN    dbo.ItemCategory IC 
                                                                 ON            I.numItemCode = IC.numItemID 
                                                                 LEFT JOIN     dbo.CFW_FLD_Values_Item CFVI 
                                                                 ON            CFVI.RecId = I.numItemCode   
                                                                 where              IC.numCategoryID  = '+convert(varchar(10),@numCategoryID)+' AND  CFVI.Fld_ID =  SUBSTRING('''+@data+''', CHARINDEX(''>'','''+@data+''')+1, CHARINDEX(''='','''+@data+''' ) - (CHARINDEX(''>'','''+@data+''' )+1)    )     AND CFVI.Fld_Value =SUBSTRING('''+@data+''', CHARINDEX(''='','''+@data+''')+1,(LEN('''+@data+''') - CHARINDEX(''='','''+@data+''')))
                                                               ' 
		                     END
		                    
		                 DELETE from #temp where row=@row
                   		 SELECT top 1 @row=row ,@data = data from #temp
                   		
                         IF @@rowCount=0
                           BEGIN
                                 SET @row=0
                                 IF @count = 0
                                   BEGIN 
                                      SET @where = @where + @dynamicFilterQuery 
                                      SET @count = @count + 1   
                                                                   
                                   END
                                ELSE
                                  BEGIN
                                  	   SET @where =  @dynamicFilterQuery + '  And I.numItemCode IN( ' + @where + ' ) '  
								  	   SET @count = @count + 1
								  END
                           END
                         ELSE 
                           BEGIN
                                IF @count = 0
                                   BEGIN 
                                      SET @where = @where + @dynamicFilterQuery 
                                      SET @count = @count + 1   
                                      print(@count)                                
                                   END
                                ELSE
                                  BEGIN
                                   	   SET @where =  @dynamicFilterQuery + '  And I.numItemCode IN( ' + @where + ' ) '  
								  	   SET @count = @count + 1
								  END
                           END
                      END
                     
                      Drop table #temp
                      SET @sqlQuery  = 'SELECT numItemCode into #TempItemCode FROM (' + @where +') AS a'
                          
	           END
	           
	           ELSE
	           BEGIN
	               SET  @sqlQuery = '' 
               END
               
            SET @filterByNumItemCode = ' AND I.numItemCode IN ( SELECT * FROM #TempItemCode ) '
             
                      
            SET @sqlQuery = @sqlQuery +  ' 
                                           SELECT      DISTINCT  
                                                       CFM.Fld_id AS FilterID,
                                                       REPLACE(REPLACE(CFM.Fld_label,'';'','' '') ,''~'','' '')   AS FilterName,
                                                       LD.vcdata AS FilterValue,
                                                       LD.numListItemID AS FilterValueID,
                                                       ''f~'' +CFM.Fld_label + ''~''+ CAST(CFM.Fld_id AS VARCHAR(10)) +''^'' +  LD.vcdata+''~''+ CAST(LD.numListItemID AS VARCHAR(10)) + '';'' AS FilterQuery ,
                                                       dbo.fn_GetItemCountBasedOnAttribute(CFM.Fld_id,LD.numListItemID,'+CONVERT(VARCHAR(10),@numCategoryID)+','+CONVERT(VARCHAR(10),@numDomainId)+')  ItemCount 
                                           FROM        dbo.Item I 
                                           INNER JOIN  dbo.ItemCategory IC 
                                           ON          IC.numItemID = I.numItemCode
                                           INNER JOIN  dbo.ItemGroupsDTL IGD 
                                           ON          I.numItemGroup = IGD.numItemGroupID
                                           INNER JOIN  dbo.CFW_Fld_Master CFM 
                                           ON              IGD.numOppAccAttrID = CFM.Fld_id 
                                                       AND CFM.numDomainID= @numDomainID
                                           LEFT JOIN   dbo.ListDetails LD 
                                           ON              LD.numListID = CFM.numlistid 
                                                       AND CFM.numDomainID = LD.numDomainID
                                           INNER JOIN  dbo.WareHouseItems WHI 
                                           ON          WHI.numItemID = I.numItemCode 
                                           INNER JOIN  dbo.CFW_Fld_Values_Serialized_Items CFVSI 
                                           ON          CFVSI.RecId = WHI.numWareHouseItemID
                   
                                           WHERE 
                                           
                                                          IC.numCategoryID = @numCategoryID 
                                                       AND i.numDomainID = @numDomainID
                                                       AND LD.numListItemID = CFVSI.Fld_Value 
                                                       AND  CFM.Fld_id  =  CFVSI.Fld_ID 
                                           
                                           GROUP BY    LD.vcdata  ,
                                                       CFM.Fld_id ,
                                                       CFM.Fld_label,
                                                       LD.numListItemID
                                         '    

         PRINT(@sqlQuery)

	SET @manufacturer =  'UNION
	       
		 SELECT       DISTINCT 
		             -1 AS FilterID,
		             ''Manufacturer'' ,
		              vcManufacturer  FilterValue,
		              0 AS FilterValueID, 
		              ''f~Manufacturer~-1^''+vcManufacturer+''~-1''+'';'' AS FilterQuery,
		              Count(*) ItemCount 
		 FROM 
		              Item I INNER JOIN dbo.ItemCategory IC ON I.numItemCode = IC.numItemID 
		 where 
		              numDomainID=@numDomainId
		              AND IC.numCategoryID = @numCategoryID 
		              AND LEN(ISNULL(vcManufacturer,''''))>1 ' 
	
	SET @manufacturerGroupBy =	'GROUP BY     I.vcManufacturer '
          
        
        
    SET @price = '    UNION 
        
          
         SELECT       DISTINCT 
                      -2 AS FilterID,
                      ''Price'',
                      CAST(MIN(FLOOR(monListPrice)) AS VARCHAR(10))  AS FilterValue,
                      CEILING(MAX(monListPrice))  AS FilterVAlueID , 
                      ''f~Min~''+ CAST(MIN(FLOOR(monListPrice)) AS VARCHAR(10))+''^Max~''+CAST(MAX(CEILING(monListPrice)) AS VARCHAR(10))+'';'' AS FilterQuery,Count(*) as ItemCount   
         FROM 
                      dbo.ItemCategory IC JOIN Item I ON I.numItemCode = IC.numItemID
         where 
                      IC.numCategoryId = @numCategoryId 
                      AND I.numDomainId = @numDomainId  '
                    
                    
     SET  @textbox =     ' UNION
                   
        SELECT        DISTINCT
                      CFM.Fld_id   AS FilterId ,
                      REPLACE(REPLACE(CFM.Fld_label , '';'', '' '') , ''~'' ,'' '')   AS FilterName,
                      CFVI.Fld_Value  AS FilterValue,
                      CFM.Fld_id    AS FilterValueID,
                      ''c~'' +REPLACE(REPLACE(CFM.Fld_label , '';'', '' '') , ''~'' ,'' '')+ ''~''+ CAST(CFM.Fld_id AS VARCHAR(10)) +''^'' +  CFVI.Fld_Value  +''~''+CFVI.Fld_Value  + '';'' AS FilterQuery ,
                      Count(CFVI.Fld_Value) AS ItemCount    
        FROM          Item I 
        INNER JOIN    dbo.ItemCategory  IC ON IC.numItemID = I.numItemCode 
        INNER JOIN    dbo.CFW_FLD_Values_Item CFVI ON CFVI.RecId = I.numItemCode  
        LEFT JOIN     dbo.CFW_Fld_Master CFM ON CFM.Fld_id = CFVI.Fld_ID

        where             IC.numCategoryID = @numCategoryId 
                      AND I.numDomainId = @numDomainId 
                      AND CFVI.Fld_Value <> '''' 
                      AND CFM.Fld_Type = ''Text Box''
                      AND CFM.Grp_Id = 5  
                      
                      '
                      
  SET  @textboxGroupBy =' GROUP BY 
                      CFVI.Fld_Value ,
                      CFM.Fld_id,
                      CFVI.Fld_ID,
                      CFM.Fld_label
                     '              
                      
         
         
       
       SET @checkbox = ' UNION
         
         SELECT       DISTINCT
                      CFM.Fld_id   AS FilterId ,
                      REPLACE(REPLACE(CFM.Fld_label , '';'', '' '') , ''~'' ,'' '')   AS FilterName,
                      CASE WHEN CFVI.Fld_Value = 1 THEN ''yes'' WHEN CFVI.Fld_Value = 0 THEN ''no'' END  AS FilterValue,
                      CFVI.Fld_Value AS FilterValueID,
                      ''c~'' +REPLACE(REPLACE(CFM.Fld_label , '';'', '' '') , ''~'' ,'' '')+ ''~''+ CAST(CFM.Fld_id AS VARCHAR(10)) +''^'' +  CASE WHEN CFVI.Fld_Value = 1 THEN ''yes'' WHEN CFVI.Fld_Value = 0 THEN ''no'' END  +''~''+ CFVI.Fld_Value + '';'' AS FilterQuery ,
                      Count(CFM.fld_Type) AS ItemCount    
        FROM          Item I 
        INNER JOIN    dbo.ItemCategory  IC ON IC.numItemID = I.numItemCode 
        INNER JOIN    dbo.CFW_FLD_Values_Item CFVI ON CFVI.RecId = I.numItemCode  
        LEFT JOIN     dbo.CFW_Fld_Master CFM ON CFM.Fld_id = CFVI.Fld_ID

        where             IC.numCategoryID = @numCategoryId 
                      AND I.numDomainId =@numDomainId AND CFVI.Fld_Value <> '''' 
                      AND CFM.Fld_Type = ''Check Box''
                      AND CFM.Grp_Id = 5 ' 
          
        
          
      SET @checkboxGroupBy =' GROUP BY 
                      CFM.fld_Type ,
                      CFM.Fld_id,
                      CFVI.Fld_Value,
                      CFVI.Fld_ID,
                      CFM.Fld_label'
         
         
         SET @dropdown =' UNION
                  
         SELECT       DISTINCT
                      CFM.Fld_id   AS FilterId ,
                      REPLACE(REPLACE(CFM.Fld_label , '';'', '' '') , ''~'' ,'' '')   AS FilterName,
                      LD.vcdata AS FilterValue , 
                      LD.numListItemID AS FilterValueID,
                      ''c~'' +REPLACE(REPLACE(CFM.Fld_label , '';'', '' '') , ''~'' ,'' '')+ ''~''+ CAST(CFM.Fld_id AS VARCHAR(10)) +''^'' +  LD.vcdata+''~''+ CAST(LD.numListItemID AS VARCHAR(10)) + '';'' AS FilterQuery ,
                      Count(Fld_type) AS ItemCount 

        FROM          Item I 

        INNER JOIN    dbo.ItemCategory  IC ON IC.numItemID = I.numItemCode 
        INNER JOIN    dbo.CFW_FLD_Values_Item CFVI ON CFVI.RecId = I.numItemCode  
        LEFT JOIN     dbo.ListDetails LD ON LD.numListItemID = CFVI.Fld_Value
        LEFT JOIN     dbo.CFW_Fld_Master CFM ON CFM.Fld_id = CFVI.Fld_ID
        where         IC.numCategoryID = @numCategoryId and 
                      I.numDomainId = @numDomainId AND 
                      CFVI.Fld_Value <> ''''
                     
                      AND CFM.Fld_type = ''Drop Down List Box'' 
                      AND CFM.Grp_Id = 5
                      AND CFVI.Fld_Value != ''0'' '     
        
        
        SET @dropdownGroupBy =' GROUP BY 
                      CFM.fld_Type ,
                      CFM.Fld_id,
                      CFVI.Fld_Value,
                      LD.numListItemID,
                      CFVI.Fld_ID,
                      CFM.Fld_label,
                      LD.vcdata '   
       
       PRINT(LEN(@where))
        IF LEN(@where) > 0 
           BEGIN 
           
            SET   @sqlQuery = @sqlQuery +@price  + @filterByNumItemCode + @manufacturer + @filterByNumItemCode + @manufacturerGroupBy + @textbox + @filterByNumItemCode + @textboxGroupBy + @checkbox + @filterByNumItemCode + @checkboxGroupBy  + @dropdown  + @filterByNumItemCode + @dropdownGroupBy                  
              PRINT (@sqlQuery  )        
           END
        ELSE
          BEGIN
                 SET   @sqlQuery = @sqlQuery +@price + @manufacturer + @manufacturerGroupBy + @textbox + @textboxGroupBy + @checkbox  + @checkboxGroupBy + @dropdown  + @dropdownGroupBy             
                 PRINT('hhhh') 
                 END   
        
        
        
        
         END
         
         EXEC sp_executeSql @sqlQuery,
            N'@numDomainId numeric(9),@numCategoryID numeric(9)', @numDomainId,
            @numCategoryID
    END
--exec USP_GetProductFilters @numDomainId = 1 ,@numCategoryId = 1596
