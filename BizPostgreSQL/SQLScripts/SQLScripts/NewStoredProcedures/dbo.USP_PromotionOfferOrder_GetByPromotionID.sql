GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOfferOrder_GetByPromotionID')
DROP PROCEDURE USP_PromotionOfferOrder_GetByPromotionID
GO
CREATE PROCEDURE [dbo].[USP_PromotionOfferOrder_GetByPromotionID]
	@numDomainID NUMERIC(18,0)
	,@numPromotionID NUMERIC(18,0)
AS
BEGIN
	SELECT * FROM PromotionOffer WHERE numDomainId=@numDomainID AND numProId = @numPromotionID 
	 	
	SELECT ROW_NUMBER() OVER(ORDER BY numProOfferOrderID ASC) AS RowNum, numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = @numPromotionID ORDER BY numProOfferOrderID

	SELECT * FROM DiscountCodes WHERE numPromotionID=@numPromotionID
END
GO