GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetErrorMessages')
DROP PROCEDURE USP_GetErrorMessages
GO

CREATE PROCEDURE USP_GetErrorMessages
   @numDomainId AS NUMERIC(18),
   @numSiteId AS NUMERIC(18), 
   @numCultureId AS NUMERIC(18),
   @tintApplication AS INTEGER
AS 

BEGIN
      SELECT EM.numErrorID ,
             EM.vcErrorCode ,
             ISNULL( (SELECT numErrorDetailId FROM dbo.ErrorDetail WHERE numDomainId=@numDomainID AND numSiteId=@numSiteID AND numCultureId=@numCultureID AND numErrorID=EM.numErrorID),0) AS numErrorDetailId,
             CASE WHEN ISNULL((SELECT vcErrorDesc FROM dbo.ErrorDetail WHERE numDomainId=@numDomainID AND numSiteId=@numSiteID AND numCultureId=@numCultureID AND numErrorID=EM.numErrorID),vcErrorDesc) <> '' 
             THEN     
                  ISNULL((SELECT vcErrorDesc FROM dbo.ErrorDetail WHERE numDomainId=@numDomainID AND numSiteId=@numSiteID AND numCultureId=@numCultureID AND numErrorID=EM.numErrorID),vcErrorDesc) 
             ELSE
                  vcErrorDesc 
             END  AS vcErrorDesc ,
             EM.vcErrorDesc AS DescParent ,
             ISNULL((SELECT vcErrorDesc FROM dbo.ErrorDetail WHERE numDomainId=@numDomainID AND numSiteId=@numSiteID AND numCultureId=@numCultureID AND numErrorID=EM.numErrorID),'')  AS DescChild
             
      FROM   dbo.ErrorMaster EM
      WHERE tintApplication=@tintApplication
                 AND    (EM.tintApplication = @tintApplication)
END   

--EXEC USP_GetErrorMessages 1,65,0,3
--exec USP_GetErrorMessages @numDomainId=1,@numSiteId=64,@numCultureId=0,@tintApplication=3
