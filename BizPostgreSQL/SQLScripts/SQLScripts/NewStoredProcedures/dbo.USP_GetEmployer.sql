SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmployer')
DROP PROCEDURE USP_GetEmployer
GO
CREATE PROCEDURE [dbo].[USP_GetEmployer]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		DM.numDivisionID,
		CMP.vcCompanyName
	FROM 
		CompanyInfo CMP
	INNER JOIN
		DivisionMaster DM
	ON
		CMP.numCompanyId = DM.numCompanyID
	INNER JOIN
		Domain D
	ON
		DM.numDivisionID = D.numDivisionID
	WHERE 
		D.numDomainId=@numDomainID
END
