
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCategoryIDForBreadCrumb]    Script Date: 08/08/2009 16:32:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anoop Krishnan
-- Create date: 30-07-2009
-- Description:	To get item details for breadcrumb
-- =============================================
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCategoryIDForBreadCrumb')
DROP PROCEDURE USP_GetCategoryIDForBreadCrumb
GO
CREATE PROCEDURE [dbo].[USP_GetCategoryIDForBreadCrumb] 
	@numItemCode as numeric(9),
	@vcCategoryName varchar(1000),
	@numSiteID numeric(18)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT TOP 1 C.numCategoryID
	FROM dbo.Item I
	INNER JOIN dbo.ItemCategory IC
	ON I.numItemCode = IC.numItemID
	INNER JOIN dbo.Category C
	ON IC.numCategoryID = C.numCategoryID
	INNER JOIN [dbo].[SiteCategories] SC
	ON SC.numCategoryID = C.numCategoryID
	WHERE I.numItemCode = @numItemCode
		AND SC.numSiteID = @numSiteID
		AND dbo.fn_GenerateSEOFriendlyURL(C.vcCategoryName) = @vcCategoryName
		
END

