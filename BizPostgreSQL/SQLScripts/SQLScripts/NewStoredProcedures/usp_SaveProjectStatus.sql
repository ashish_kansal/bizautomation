/****** Object:  StoredProcedure [dbo].[USP_ProjectsDTLPL]    Script Date: 07/26/2008 16:20:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_SaveProjectStatus')
DROP PROCEDURE usp_SaveProjectStatus
GO
CREATE PROCEDURE [dbo].[usp_SaveProjectStatus]                                   
(                         
@numDomainID numeric(9),                             
@numProId numeric(9)=null  ,    
@ProjectStatus numeric(9),
@numContactId NUMERIC(9)=0
)                                    
as                                                             
begin     
	UPDATE ProjectsMaster SET numProjectStatus=@ProjectStatus,numCompletedBy=@numContactId WHERE numDomainId=@numDomainID AND numProId=@numProId
END