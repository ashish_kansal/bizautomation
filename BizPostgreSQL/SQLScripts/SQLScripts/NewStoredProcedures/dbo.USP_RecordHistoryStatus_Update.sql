
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecordHistoryStatus_Update')
DROP PROCEDURE dbo.USP_RecordHistoryStatus_Update
GO
CREATE PROCEDURE [dbo].[USP_RecordHistoryStatus_Update]
	@bitSuccess BIT
AS
BEGIN
	UPDATE 
		RecordHistoryStatus 
	SET 
		dtLastExecuted = GETDATE(),
		intNoOfTimesTried = (CASE WHEN @bitSuccess = 1 THEN 0 ELSE intNoOfTimesTried + 1 END),
		bitSuccess = @bitSuccess
	WHERE 
		Id = 1
END