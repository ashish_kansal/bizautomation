GO
/****** Object:  StoredProcedure [dbo].[USP_GET_ECampaign]    Script Date: 04/11/2012 15:15:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_ItemImages' ) 
                    DROP PROCEDURE USP_GET_ItemImages
                    GO

CREATE PROCEDURE [dbo].[USP_GET_ItemImages]

@numDomainID NUMERIC(9),
@numItemCode NUMERIC(9)

AS 

BEGIN

SELECT [numItemImageId],[numItemCode],[vcPathForImage],[vcPathForTImage],[bitDefault],[intDisplayOrder],[bitIsImage] FROM [ItemImages] 
WHERE [numDomainID] =@numDomainID  AND numItemCode = @numItemCode
 ORDER BY intDisplayOrder
--	kishan
END

--exec  USP_GET_ItemImages @numDomainId = 1 ,@numItemCode =197605

