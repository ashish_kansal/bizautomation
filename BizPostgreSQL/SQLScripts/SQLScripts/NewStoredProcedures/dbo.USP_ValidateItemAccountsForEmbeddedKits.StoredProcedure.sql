SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ValidateItemAccountsForEmbeddedKits')
DROP PROCEDURE USP_ValidateItemAccountsForEmbeddedKits
GO
CREATE PROCEDURE [dbo].[USP_ValidateItemAccountsForEmbeddedKits]    
	@numKitId as numeric(9)
as                            


WITH CTE(numItemKitID,numItemCode,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
StageLevel,charItemType)
AS
(
select convert(NUMERIC(18,0),0),numItemCode,numCOGsChartAcntId
,numAssetChartAcntId,numIncomeChartAcntId,1,charItemType
from item                                
INNER join ItemDetails Dtl on numChildItemID=numItemCode
where  numItemKitID=@numKitId

UNION ALL

select Dtl.numItemKitID,i.numItemCode,i.numCOGsChartAcntId
,i.numAssetChartAcntId,i.numIncomeChartAcntId,c.StageLevel + 1,i.charItemType
from item i                               
INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode
where Dtl.numChildItemID!=@numKitId
)

SELECT * INTO #temp FROM CTE

IF EXISTS  (SELECT *  FROM #temp 
where 0 = (CASE WHEN charItemType = 'P' OR charItemType = 'A' THEN 
					CASE WHEN numAssetChartAcntId > 0 AND numCOGsChartAcntId > 0 AND numIncomeChartAcntId > 0 THEN 1 ELSE 0 END
			WHEN charItemType = 'N' OR charItemType = 'S' THEN 
					CASE WHEN numIncomeChartAcntId > 0 THEN 1 ELSE 0 END END))
BEGIN
	SELECT 0
END
ELSE
BEGIN
	SELECT 1							
END
                     
  


GO
