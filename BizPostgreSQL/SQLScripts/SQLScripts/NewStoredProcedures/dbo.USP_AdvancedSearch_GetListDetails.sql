GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearch_GetListDetails')
DROP PROCEDURE USP_AdvancedSearch_GetListDetails
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch_GetListDetails]                      
	@numListID NUMERIC(18,0)                      
	,@vcItemType CHAR(3)                      
	,@numDomainID NUMERIC(18,0)                    
	,@vcSearchText VARCHAR(300)
	,@intOffset INT
	,@intPageSize INT
AS     
BEGIN
	IF @vcItemType = 'SYS'
	BEGIN
		SELECT
			numItemID as id
			,vcItemName as text
			,COUNT(*) OVER() AS numTotalRecords
		FROM
		(
			SELECT 0 AS numItemID,'Lead' AS vcItemName
			UNION ALL
			SELECT 1 AS numItemID,'Prospect' AS vcItemName
			UNION ALL
			SELECT 2 AS numItemID,'Account' AS vcItemName
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	
	IF @vcItemType='LI' AND @numListID=9 --Opportunity Source
	BEGIN
		DECLARE @TEMP TABLE
		(numItemID VARCHAR(50),vcItemName VARCHAR(100), constFlag BIT, bitDelete BIT,intSortOrder INT, numListItemID NUMERIC(9),vcData VARCHAR(100), vcListType VARCHAR(1000), vcOrderType VARCHAR(50), vcOppOrOrder VARCHAR(20), numListType NUMERIC(18,0), tintOppOrOrder TINYINT)

		INSERT INTO @TEMP
		SELECT '0~1' AS numItemID,'Internal Order' AS vcItemName, 1 AS  constFlag, 1 AS  bitDelete, 0 intSortOrder, -1 AS numListItemID,'Internal Order','Internal Order','','',0,0
		UNION
		SELECT CAST([numSiteID] AS VARCHAR(250)) +'~2' AS numItemID,[vcSiteName] AS vcItemName, 1 AS  constFlag, 1 AS  bitDelete, 0 intSortOrder, CAST([numSiteID] AS VARCHAR(18)) AS numListItemID,[vcSiteName],[Sites].[vcSiteName],'','',0,0
		FROM Sites WHERE [numDomainID] = @numDomainID
		UNION
		SELECT DISTINCT CAST(WebApiId  AS VARCHAR(250)) +'~3' AS numItemID,vcProviderName AS vcItemName,1 constFlag, 1 bitDelete, 0 intSortOrder, CAST(WebApiId  AS VARCHAR(18)) AS numListItemID,vcProviderName,[WebAPI].[vcProviderName],'','',0,0
		FROM dbo.WebAPI   
		UNION
		SELECT CAST(Ld.numListItemID  AS VARCHAR(250)) +'~1' AS numItemID, isnull(vcRenamedListName,vcData)  AS vcItemName, Ld.constFlag,bitDelete,isnull(intSortOrder,0) intSortOrder,CAST(Ld.numListItemID  AS VARCHAR(18)),isnull(vcRenamedListName,vcData) as vcData,
		(SELECT vcData FROM ListDetails WHERE numListId=9 and numListItemId = ld.numListType AND [ListDetails].[numDomainID] = @numDomainID),(CASE when ld.numListType=1 then 'Sales' when ld.numListType=2 then 'Purchase' else 'All' END)
		,(CASE WHEN ld.tintOppOrOrder=1 THEN 'Opportunity' WHEN ld.tintOppOrOrder=2 THEN 'Order' ELSE 'All' END),numListType,tintOppOrOrder
		FROM listdetails Ld  left join listorder LO 
		on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
		WHERE Ld.numListID=9 and (constFlag=1 or Ld.numDomainID=@numDomainID)

		SELECT 
			numItemID as id
			,vcItemName as text
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			@TEMP 
		WHERE 
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END                 
	ELSE IF @vcItemType = 'LI'    --Master List                    
	BEGIN
		SELECT 
			LD.vcData AS text
			,LD.numListItemID AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			ListDetails LD 
		LEFT JOIN 
			listorder LO 
		ON 
			Ld.numListItemID= LO.numListItemID 
			AND Lo.numDomainId = @numDomainID  
		WHERE 
			LD.numListID = @numListID 
			AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
			AND (LD.vcData LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY 
			ISNULL(intSortOrder,LD.sintOrder)
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;                        
	END      
	ELSE IF @vcItemType = 'L' AND ISNULL(@numListID,0) > 0    --Master List                    
	BEGIN
		SELECT 
			LD.vcData AS text
			,LD.numListItemID AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			ListDetails LD 
		LEFT JOIN 
			listorder LO 
		ON 
			Ld.numListItemID= LO.numListItemID 
			AND Lo.numDomainId = @numDomainID  
		WHERE 
			LD.numListID = @numListID 
			AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
			AND (LD.vcData LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY 
			ISNULL(intSortOrder,LD.sintOrder)
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;                       
	END                                          
	ELSE IF @vcItemType = 'T'    --Territories                    
	BEGIN
		SELECT 
			vcData AS text
			,numListItemID AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			ListDetails 
		WHERE 
			numListID = 78 
			AND numDomainID = @numDomainID
			AND (vcData LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcData
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;                                  
	END                        
	ELSE IF @vcItemType = 'AG'   --Lead Groups                     
	BEGIN
		SELECT 
			vcGrpName AS text
			,numGrpId AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			Groups                     
		WHERE
			(vcGrpName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcGrpName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END                         
	ELSE IF @vcItemType = 'S'    --States                    
	BEGIN
		SELECT 
			vcState AS text
			,numStateID AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			State 
		WHERE 
			numDomainID = @numDomainID
			AND (vcState LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcState
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;             
	END         
	ELSE IF @vcItemType = 'U'    --Users                    
	BEGIN
		SELECT
			numItemID id
			,vcItemName text
			,COUNT(*) OVER() AS numTotalRecords
		FROM
		(
			SELECT 
				A.numContactID AS numItemID
				,A.vcFirstName + ' '+ A.vcLastName AS vcItemName              
			FROM 
				UserMaster UM             
			JOIN 
				AdditionalContactsInformation A            
			ON 
				UM.numUserDetailId=A.numContactID              
			WHERE 
				UM.numDomainID=@numDomainID        
				AND ((vcFirstName LIKE '%' + @vcSearchText + '%' OR vcLastName LIKE '%' + @vcSearchText + '%' ) OR ISNULL(@vcSearchText,'') = '')
			UNION        
			SELECT 
				A.numContactID
				, vcCompanyName + ' - ' + A.vcFirstName + ' ' + A.vcLastName        
			FROM AdditionalContactsInformation A         
			join DivisionMaster D        
			on D.numDivisionID=A.numDivisionID        
			join ExtarnetAccounts E         
			on E.numDivisionID=D.numDivisionID        
			join ExtranetAccountsDtl DTL        
			on DTL.numExtranetID=E.numExtranetID        
			join CompanyInfo C        
			on C.numCompanyID=D.numCompanyID        
			where A.numDomainID=@numDomainID and bitPartnerAccess=1        
			and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID)
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;                 
	END
	ELSE IF @vcItemType = 'C'    --Organization Campaign                    
	BEGIN
		SELECT  
			numCampaignID AS id
			,vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS text
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			CampaignMaster
		WHERE 
			numDomainID = @numDomainID
			AND (vcCampaignName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcCampaignName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END               
	ELSE IF @vcItemType = 'IG'
	BEGIN
		SELECT 
			vcItemGroup AS text
			,numItemGroupID AS id
			,COUNT(*) OVER() AS numTotalRecords
		FROM 
			dbo.ItemGroups 
		WHERE 
			numDomainID=@numDomainID   	
			AND (vcItemGroup LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemGroup
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE IF @vcItemType = 'PP'
	BEGIN
		SELECT
			numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS numTotalRecords
		FROM
		(
			SELECT 'Inventory Item' AS vcItemName, 'P' AS numItemID, 'P' As vcItemType, 0 As flagConst 
			UNION 
			SELECT 'Non-Inventory Item' AS vcItemName, 'N' AS numItemID, 'P' As vcItemType, 0 As flagConst 
			UNION 
			SELECT 'Service' AS vcItemName, 'S' AS numItemID, 'P' As vcItemType, 0 As flagConst
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE IF @vcItemType = 'V' 
	BEGIN
		SELECT 
			ISNULL(C.vcCompanyName,'') AS text
			,numVendorID AS id
			,COUNT(*) OVER() AS numTotalRecords 
		FROM 
			dbo.Vendor V 
		INNER JOIN 
			dbo.DivisionMaster DM 
		ON 
			DM.numDivisionID= V.numVendorID 
		INNER JOIN 
			dbo.CompanyInfo C 
		ON 
			C.numCompanyId = DM.numCompanyID 
		WHERE 
			V.numDomainID =@numDomainID
			AND (vcCompanyName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			C.vcCompanyName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE IF @vcItemType = 'OC' 
	BEGIN
		SELECT DISTINCT 
			C.vcCurrencyDesc AS text
			,C.numCurrencyID AS id
			,COUNT(*) OVER() AS numTotalRecords 
		FROM 
			dbo.Currency C 
		WHERE 
			C.numDomainId=@numDomainID
			AND (vcCurrencyDesc LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
			AND C.numCurrencyID IN (SELECT ISNULL(numCurrencyID,0) FROM OpportunityMaster WHERE numDomainId=@numDomainID)
		ORDER BY
			vcCurrencyDesc
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE  IF @vcItemType = 'UOM'    --Organization Campaign                    
	BEGIN
		SELECT 
			u.numUOMId as id
			,u.vcUnitName as text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM 
			UOM u 
		INNER JOIN 
			Domain d 
		ON 
			u.numDomainID=d.numDomainID
		WHERE 
			u.numDomainID=@numDomainID 
			AND d.numDomainID=@numDomainID 
			AND u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)
			AND (vcUnitName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcUnitName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
    END   
	ELSE IF @vcItemType = 'O'    --Opp Type                   
	BEGIN
		SELECT
			numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM
		(
			SELECT  1 AS numItemID,'Sales' AS vcItemName
			UNion ALL
			SELECT  2 AS numItemID,'Purchase' AS vcItemName
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END 
	ELSE IF @vcItemType = 'OT'    --Opp Type                   
	BEGIN
		SELECT
			numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM
		(
			SELECT  1 AS numItemID,'Sales Opportunity' AS vcItemName
			UNION ALL
			SELECT  2 AS numItemID,'Purchase Opportunity' AS vcItemName
			UNION ALL
			SELECT  3 AS numItemID,'Sales Order' AS vcItemName
			UNION ALL
			SELECT  4 AS numItemID,'Purchase Order' AS vcItemName
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE  IF @vcItemType = 'CHK'    --Opp Type                   
	BEGIN
		SELECT
			numItemID AS id
			,vcItemName AS text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM
		(
			SELECT  1 AS numItemID,'Yes' AS vcItemName
			UNION ALL
			SELECT  0 AS numItemID,'No' AS vcItemName
		) TEMP
		WHERE
			(vcItemName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcItemName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END  
	ELSE IF @vcItemType='COA'
	BEGIN
		SELECT 
			C.[numAccountId] AS id
			,ISNULL(C.[vcAccountName],'') AS text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM    [Chart_Of_Accounts] C
		INNER JOIN [AccountTypeDetail] ATD 
		ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
		WHERE   C.[numDomainId] = @numDomainId
		AND (vcAccountName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY C.[vcAccountCode]
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
	ELSE IF @vcItemType='IC'
	BEGIN
		SELECT
			numCategoryID AS id
			,ISNULL(vcCategoryName,'') AS text
			,COUNT(*) OVER() AS numTotalRecords 
		FROM
			Category
		WHERE
			numDomainID=@numDomainID
			AND (vcCategoryName LIKE '%' + @vcSearchText + '%' OR ISNULL(@vcSearchText,'') = '')
		ORDER BY
			vcCategoryName
		OFFSET 
			@intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;
	END
END       
GO
