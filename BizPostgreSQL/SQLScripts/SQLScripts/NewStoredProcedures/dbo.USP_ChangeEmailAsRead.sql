
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ChangeEmailAsRead')
DROP PROCEDURE USP_ChangeEmailAsRead
GO
CREATE PROCEDURE [dbo].[USP_ChangeEmailAsRead]
@numDomainID AS numeric(9)=0,
@strXml AS TEXT
AS
BEGIN
 DECLARE @hDocItem int                                                                                                                            

 EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strXml  
  
   update emailHistory set bitIsRead= 1 
   from (SELECT numEmailHstrID
   FROM OPENXML (@hDocItem,'/NewDataSet/EmailHistory',2)
   WITH  (                      
    numEmailHstrID numeric(9)                                   
   ))X  where emailHistory.numEmailHstrID=X.numEmailHstrID AND numDomainID=@numDomainID

EXEC sp_xml_removedocument @hDocItem    

END