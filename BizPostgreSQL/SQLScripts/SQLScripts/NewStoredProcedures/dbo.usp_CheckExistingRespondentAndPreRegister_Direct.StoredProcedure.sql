Go
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_CheckExistingRespondentAndPreRegister_Direct')
DROP PROCEDURE usp_CheckExistingRespondentAndPreRegister_Direct
GO
CREATE PROCEDURE [dbo].[usp_CheckExistingRespondentAndPreRegister_Direct]          
 @numSurID Numeric,  
 @numContactId NUMERIC,
 @numDomainID NUMERIC
AS                
--This will insert response for the surveys.                
BEGIN            
   DECLARE @numDivisionId Numeric          
   DECLARE @numRespondantID Numeric          
   DECLARE @vcEmail NVarchar(50)      
           
   SELECT TOP 1 @numDivisionId = numDivisionID,@vcEmail = vcEmail  FROM AdditionalContactsInformation         
   WHERE numContactId = @numContactID and numDomainId = @numDomainID     
        
   IF @@ROWCOUNT > 0          
   BEGIN           
 INSERT INTO SurveyRespondentsMaster(numSurID, numSurRating, dtDateofResponse, bitRegisteredRespondant, numRegisteredRespondentContactId, numDomainId)        
 VALUES(@numSurID, 0, getutcdate(), 1, @numContactId, @numDomainID)              
         
 SELECT @numRespondantID = SCOPE_IDENTITY()          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 SELECT @numRespondantID, @numSurID, 'numCustomerId','Customer Id', 0, numCompanyID,          
 0, 1,'EditBox'  FROM DivisionMaster WHERE numDivisionId = @numDivisionId          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 VALUES (@numRespondantID, @numSurID, 'vcEmail','Email Address', 0, @vcEmail,          
 0, 2,'EditBox')          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 SELECT @numRespondantID, @numSurID, 'vcFirstName','First Name', 0, vcFirstName,          
 1, 1,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = @numContactId          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 SELECT @numRespondantID, @numSurID, 'vcLastName','Last Name', 0, vcLastName,          
 1, 2,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = @numContactId          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 SELECT @numRespondantID, @numSurID, 'numPhone','Phone', 0,numPhone ,          
 2, 1,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = @numContactId          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 SELECT @numRespondantID, @numSurID, 'numPhoneExtension','Extension', 0, numPhoneExtension,          
 2, 2,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = @numContactId          
         
 SELECT @numRespondantID          
   END          
   ELSE          
   BEGIN          
  SELECT 0          
   END           
END
GO
