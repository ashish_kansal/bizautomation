SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveJavascriptDisplayOrder')
DROP PROCEDURE USP_SaveJavascriptDisplayOrder
GO

CREATE PROCEDURE [dbo].[USP_SaveJavascriptDisplayOrder]
	@strItems TEXT=null
AS
BEGIN

	
	DECLARE  @hDocItem INT
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,@strItems
            
          UPDATE [dbo].[StyleSheets] SET StyleSheets.[intDisplayOrder] = X.intDisplayOrder
          FROM   OPENXML (@hDocItem, '/NewDataSet/CssStyles', 2)
                            WITH (numCssID NUMERIC(9),intDisplayOrder int) X
		  WHERE dbo.StyleSheets.numCssID = X.numCssID AND tintType = 1
          EXEC sp_xml_removedocument
            @hDocItem
        END
END
