SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingexceptionsfororder')
DROP PROCEDURE usp_getshippingexceptionsfororder
GO
CREATE PROCEDURE [dbo].[USP_GetShippingExceptionsForOrder]
	 @numDomainID NUMERIC(18,0)
	,@vcItemCodes VARCHAR(MAX)
	,@ShippingAmt Float
	,@bitFreeShipping BIT
AS
BEGIN	

	--DECLARE @vcItemCodes VARCHAR(MAX) = '8,240'
  
  DECLARE @tempExceptions TABLE
    (
		SNO INT IDENTITY(1,1),
		vcItemName VARCHAR(200),
		bitApplyException BIT,
		FlatAmt FLOAT,
		numItemCode NUMERIC
    )


	INSERT INTO @tempExceptions (vcItemName, bitApplyException, FlatAmt, numItemCode)
		SELECT '',0,0, id 
		FROM  dbo.SplitIDs(@vcItemCodes, ',')  
	
--select * from 	@tempExceptions	

	DECLARE @totalRecords INT
	DECLARE @I INT

	SELECT @I = 1
	SELECT @totalRecords = COUNT(numItemCode) FROM @tempExceptions
	WHILE (@I <= @totalRecords)
	BEGIN
		DECLARE @numShipClass NUMERIC

		SET @numShipClass = (SELECT I.numShipClass FROM @tempExceptions t
								INNER JOIN Item I ON I.numItemCode = t.numItemCode WHERE SNO = @I )

		UPDATE @tempExceptions
		SET vcItemName = (SELECT I.vcItemName FROM @tempExceptions t
								INNER JOIN Item I ON I.numItemCode = t.numItemCode WHERE SNO = @I),

		bitApplyException = (SELECT CASE WHEN  (SELECT COUNT(numClassificationID) FROM ShippingExceptions WHERE numDomainID = @numDomainID AND numClassificationID = @numShipClass) > 0 
											THEN 1
											ELSE 0 END
											),
		FlatAmt = (SELECT 
						CASE WHEN @ShippingAmt > 0 THEN
							(SELECT (@ShippingAmt * PercentAbove)/100 FROM ShippingExceptions WHERE numDomainID = @numDomainID)
						WHEN @ShippingAmt = 0 AND @bitFreeShipping = 1 THEN
						(SELECT FlatAmt FROM ShippingExceptions WHERE numDomainID = @numDomainID)
						ELSE 0 END )
		WHERE SNO = @I

	SET @I = @I + 1 

	END						
				
		SELECT vcItemName, bitApplyException, FlatAmt, numItemCode FROM @tempExceptions WHERE bitApplyException = 1
END 
GO