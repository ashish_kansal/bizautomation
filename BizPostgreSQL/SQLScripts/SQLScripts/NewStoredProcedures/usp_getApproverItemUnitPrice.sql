IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getApproverItemUnitPrice')
DROP PROCEDURE usp_getApproverItemUnitPrice
GO
CREATE PROCEDURE [dbo].[usp_getApproverItemUnitPrice]          
  @numDomainId Numeric(18,2)=0,
  @numoppId Numeric(18,2)=0    
AS                                          
BEGIN
DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(U.vcUserName AS VARCHAR(100)) FROM UnitPriceApprover AS UP
LEFT JOIN UserMaster AS U ON UP.numUserID=U.numUserId
WHERE UP.numDomainID = @numDomainID
SELECT @listIds AS UserNames
END