SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateCheckHeadePrintCheck')
DROP PROCEDURE USP_UpdateCheckHeadePrintCheck
GO
CREATE PROCEDURE [dbo].[USP_UpdateCheckHeadePrintCheck]  
@numDomainId as numeric(9)=0,                       
@strRow as text=''                                                                        
As                                                
Begin                                          
If convert(varchar(100),@strRow) <> ''                                                                                                            
Begin                                                                                                             
  Declare @hDoc3 int                                                                                                                                                                
   EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             
			
			Update CheckHeader  Set bitIsPrint=1,numCheckNo=X.numCheckNo                                                                                           
			From (SELECT * FROM OPENXML(@hDoc3,'/NewDataSet/Table1',2)                                                                                                              
			With(     
			numCheckHeaderID numeric(9),                                                                                        
			numCheckNo numeric(9) 
			))X                                                                                                  
			Where CheckHeader.numDomainId=@numDomainId AND CheckHeader.numCheckHeaderID=X.numCheckHeaderID                                                                                              
			               
			                                                                       
	EXEC sp_xml_removedocument @hDoc3 
	END
	END                                   
	                                                                                      
