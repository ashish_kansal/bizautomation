IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdditionalContactsInformation_GetByEmail')
DROP PROCEDURE USP_AdditionalContactsInformation_GetByEmail
GO

CREATE PROCEDURE USP_AdditionalContactsInformation_GetByEmail
	@numDomainID NUMERIC(18,0)
	,@numContactID NUMERIC(18,0)
	,@vcEmail VARCHAR(300)
AS 
BEGIN
	IF LEN(ISNULL(@vcEmail,'')) > 0 AND EXISTS (SELECT numContactID FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND vcEmail=ISNULL(@vcEmail,'') AND numContactID <> ISNULL(@numContactID,0))
	BEGIN
		SELECT
			AdditionalContactsInformation.numContactID
			,AdditionalContactsInformation.numDivisionID
			,CompanyInfo.vcCompanyName
			,CONCAT(ISNULL(vcFirstName,'-'),' ',ISNULL(vcLastName,'-')) vcContact
		FROM
			AdditionalContactsInformation 
		INNER JOIN
			DivisionMaster
		ON
			AdditionalContactsInformation.numDivisionID=DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
		WHERE 
			AdditionalContactsInformation.numDomainID=@numDomainID 
			AND vcEmail=ISNULL(@vcEmail,'') 
			AND AdditionalContactsInformation.numContactID <> ISNULL(@numContactID,0)
	END
END
GO