/****** Object:  StoredProcedure [dbo].[USP_MoveCopyItems]    Script Date: 07/26/2008 16:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EmailHistory_MoveCopyIDs')
DROP PROCEDURE USP_EmailHistory_MoveCopyIDs
GO
CREATE PROCEDURE [dbo].[USP_EmailHistory_MoveCopyIDs]                      
@numEmailHstrID AS NUMERIC(18,0),
@numNodeId AS NUMERIC(9),
@ModeType AS BIT,
@numUID AS NUMERIC(18,0)
AS          
--COPY
IF @ModeType = 1
BEGIN
	DECLARE @NewEmailHstrID INTEGER;   

	--MAKE A COPY IN EmailHistory TABLE
		INSERT INTO EmailHistory
		(
			vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,
			tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid,numNodeId,vcFrom,vcTO,numEMailId
		)                        
		SELECT 
			vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,
			tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,@numUID ,@numNodeId,vcFrom,vcTO,numEMailId            
		FROM 
			EmailHistory 
		WHERE 
			numEmailHstrID=@numEmailHstrID              
                  
		SELECT @NewEmailHstrID = SCOPE_IDENTITY();              
        
		--MAKE A COPY IN EmailHStrToBCCAndCC TABLE
		INSERT INTO EmailHStrToBCCAndCC 
		(
			numEmailHstrID,vcName,tintType,numDivisionId,numEmailId
		)              
		SELECT 
			@NewEmailHstrID,vcName,tintType,numDivisionId,numEmailId 
		FROM  
			EmailHStrToBCCAndCC 
		WHERE 
			numEmailHstrID=@numEmailHstrID              

		--MAKE A COPY IN EmailHstrAttchDtls TABLE                    
		INSERT INTO EmailHstrAttchDtls 
		( 
			numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType
		)              
		SELECT  
			@NewEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType 
		FROM 
			EmailHstrAttchDtls 
		WHERE 
			numEmailHstrID=@numEmailHstrID 
	
END
--MOVE
ELSE
BEGIN
	UPDATE emailHistory SET numNodeId=@numNodeId, numUid = @numUID WHERE numEmailHstrID = @numEmailHstrID
END
GO
