SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOfferForIncentiveSaleItems')
	DROP PROCEDURE USP_GetPromotionOfferForIncentiveSaleItems
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOfferForIncentiveSaleItems]  
	@numDomainID AS NUMERIC(18,0),
	@monTotAmtBefDiscount FLOAT,
	@itemIdSearch AS VARCHAR(100)
AS
BEGIN
	/*DECLARE @strSQL VARCHAR(1000), @strSQL1 VARCHAR(1000)

	-- CHECKING IF ORDER BASED PROMOTION = FALSE THEN RETURN

	SET @strSQL1 = 'SELECT numProID FROM PromotionOfferItems WHERE tintRecordType = 5 AND numValue IN (' + @itemIdSearch + ') '	

	CREATE TABLE #tempIsOrderBased
	( 
		ID INT IDENTITY (1, 1) NOT NULL, 
		numProID NUMERIC(18,0)
	)

	INSERT INTO #tempIsOrderBased
		EXEC (@strSQL1) 

	IF(SELECT COUNT(*) FROM PromotionOffer WHERE numProId IN (SELECT numProID FROM #tempIsOrderBased) AND numdomainid = @numDomainID AND IsOrderBasedPromotion = 1 ) = 0
	BEGIN
		SELECT 0 AS SubTotal
		DROP TABLE #tempIsOrderBased
		RETURN
	END */

	IF(SELECT COUNT(*) FROM PromotionOffer WHERE numdomainid = @numDomainID AND IsOrderBasedPromotion = 1 ) = 0
	BEGIN
		SELECT 0 AS SubTotal
		RETURN
	END 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	-- FETCHING MAX(SUBTOTAL) TO BE CONSIDERED

	--SET @strSQL = 'IF((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND numDomainId = ' + CAST(@numDomainID AS VARCHAR(100)) + ' AND numProItemId IN (' + @itemIdSearch + ')) > 0) '
	--			+ 'BEGIN '
	--			+	'SELECT * FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND numDomainId = ' + CAST(@numDomainID AS VARCHAR(100)) + ' AND numProItemId IN (' + @itemIdSearch + ') '
	--			+ 'END'

	--PRINT @strSQL
	--EXEC (@strSQL) 

	DECLARE @strSQL VARCHAR(1000)

	SET @strSQL = 'IF((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND bitEnabled = 1 AND numDomainId = ' + CAST(@numDomainID AS VARCHAR(100)) + ' ) > 0) '
				+ 'BEGIN '
				+	'SELECT * FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND bitEnabled = 1 AND numDomainId = ' + CAST(@numDomainID AS VARCHAR(100)) + '  '
				+ 'END'

	CREATE TABLE #tempPromotionOrderBased 
	( 
		ID INT IDENTITY (1, 1) NOT NULL, 
		numProOrderBasedID NUMERIC(18,0), 
		numProID NUMERIC(18,0), 
		numDomainID NUMERIC(18,0), 
		numProItemID NUMERIC(18,0), 
		fltSubTotal  FLOAT,
		cSaleType CHAR,
		cItems CHAR,
		bitEnabled BIT 
	)

	INSERT INTO #tempPromotionOrderBased
		EXEC (@strSQL) 

	--select * from #temp
	--drop table #temp

	IF(SELECT COUNT(*) from #tempPromotionOrderBased) > 0
	BEGIN
		
		DECLARE @max FLOAT 

		--IF (SELECT COUNT(*) from #tempPromotionOrderBased) > 1
		--BEGIN

			CREATE TABLE #temp 
			(
				ID int IDENTITY (1, 1) NOT NULL,
				fltSubTotal  FLOAT,
				numProID NUMERIC(18,0)
			)

			INSERT INTO #temp
				(fltSubTotal, numProID)
			SELECT fltSubTotal, numProID FROM #tempPromotionOrderBased

			DECLARE @Id INT, @ProID NUMERIC(18,0)

			WHILE (SELECT COUNT(*) FROM #temp) > 0
			BEGIN
				--SELECT @Id = Id, @max = MAX(fltSubTotal) FROM #temp GROUP BY Id

				SELECT @max =  MAX(fltSubTotal) FROM #temp 
				SELECT @Id = Id, @ProID = numProID FROM #temp WHERE fltSubTotal = @max

				IF(@monTotAmtBefDiscount >= @max)
				BEGIN
					SELECT @max AS SubTotal, @ProID AS PromotionID
					RETURN
					DROP TABLE #temp
				END

				DELETE #temp WHERE Id = @Id
			END
			SELECT -1 AS SubTotal
			DROP TABLE #temp
			DROP TABLE #tempPromotionOrderBased
		--END
		--ELSE
		--BEGIN

		--	SELECT @max = fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND numProItemId = @numItemId
		--	IF(@monTotAmtBefDiscount >= @max)
		--	BEGIN
		--		SELECT @max
		--		RETURN
		--	END
		--	ELSE
		--	BEGIN
		--		RETURN 0
		--	END
		--END
	END
	ELSE
	BEGIN
		SELECT 0 AS SubTotal
		RETURN
	END





	/*IF(SELECT COUNT(*) from PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND numProItemId = @numItemId) > 0
	BEGIN
		
		DECLARE @max FLOAT 

		IF (SELECT COUNT(*) from PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND numProItemId = @numItemId) > 1
		BEGIN

			CREATE TABLE #temp 
			(
				ID int IDENTITY (1, 1) NOT NULL,
				fltSubTotal  FLOAT
			)

			INSERT INTO #temp
			(fltSubTotal)
			SELECT fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND numProItemId = @numItemId

			DECLARE @Id INT

			WHILE (SELECT COUNT(*) FROM #temp) > 0
			BEGIN
				SELECT @Id = Id, @max = MAX(fltSubTotal) FROM #temp GROUP BY Id
				
				IF(@monTotAmtBefDiscount >= @max)
				BEGIN
					SELECT @max
					RETURN
					DROP TABLE #temp
				END

				DELETE #temp WHERE Id = @Id
			END
		END
		ELSE
		BEGIN

			SELECT @max = fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND numProItemId = @numItemId
			IF(@monTotAmtBefDiscount >= @max)
			BEGIN
				SELECT @max
				RETURN
			END
			ELSE
			BEGIN
				RETURN 0
			END
		END
	END
	ELSE
	BEGIN
		RETURN 0
	END*/
END





	/*DECLARE @vcItemName VARCHAR(200), @numBaseUnit VARCHAR(100), @monListPrice VARCHAR(100)

	SELECT vcProName FROM PromotionOffer WHERE numProId = @numProId AND numDomainId = @numDomainID 

	CREATE TABLE #temp 
	(
		ID int IDENTITY (1, 1) NOT NULL ,
		numProOrderBasedID numeric(18,0),
		numProId numeric(18,0),
		numDomainID NUMERIC(18,0),
		cSaleType  CHAR,
		cItems  CHAR,
		fltSubTotal  FLOAT
	)

	CREATE TABLE #temptbl 
	(
		ID int IDENTITY (1, 1) NOT NULL ,
		numProOrderBasedID numeric(18,0),
		numProId numeric(18,0),
		numDomainID NUMERIC(18,0),
		cSaleType  CHAR,
		cItems  CHAR,
		fltSubTotal  FLOAT,
		Items VARCHAR(200),
		numBaseUnit VARCHAR(100),
		monListPrice  VARCHAR(100)
	)

	Declare @Id int, @tot float

	insert into #temp
		(numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal)
	(select numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID )

	insert into #temptbl
		(numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal)
	(select numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID )
	
		While (Select Count(*) FROM #temp WHERE cSaleType = 'I' AND cItems = 'M') > 0
		BEGIN
			
			Select Top 1  @Id = Id, @tot = fltSubTotal From #temp WHERE cSaleType = 'I' AND cItems = 'M'

			SET @vcItemName = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
			SET @numBaseUnit = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.numBaseUnit,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
			SET @monListPrice = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.monListPrice,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
	
			UPDATE #temptbl
				SET Items = @vcItemName, numBaseUnit = @numBaseUnit, monListPrice = @monListPrice
			WHERE Id = @Id

			Delete #temp Where Id = @Id
		END
		While (Select Count(*) FROM #temp WHERE cSaleType = 'I' AND cItems = 'S') > 0
		BEGIN
			Select Top 1  @Id = Id,  @tot = fltSubTotal From #temp WHERE cSaleType = 'I' AND cItems = 'S'

			SELECT @vcItemName = Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
			SELECT @numBaseUnit = ISNULL(Item.numBaseUnit,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
			SELECT @monListPrice = ISNULL(Item.monListPrice,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
	
			UPDATE #temptbl
				SET Items = @vcItemName, numBaseUnit = @numBaseUnit, monListPrice = @monListPrice
			WHERE Id = @Id

			Delete #temp Where Id = @Id
		END

		SELECT DISTINCT(fltSubTotal), Items, numBaseUnit, monListPrice, cSaleType FROM #temptbl ORDER BY fltSubTotal

END 
GO











	IF ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
	BEGIN
		SET @vcItemName = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
		SET @numBaseUnit = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.numBaseUnit,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
		SET @monListPrice = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.monListPrice,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
	END
	ELSE IF EXISTS(SELECT * FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND cItems = 'S' ) 
	BEGIN
		SELECT @vcItemName = Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
		SELECT @numBaseUnit = ISNULL(Item.numBaseUnit,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
		SELECT @monListPrice = ISNULL(Item.monListPrice,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
	END

	SELECT numProOrderBasedID
		,fltSubTotal
		,cSaleType
		,@vcItemName AS Items
		,@numBaseUnit AS numBaseUnit
		,@monListPrice AS monListPrice
	FROM PromotionOfferOrderBased  
	WHERE numProId = @numProId
		AND numDomainId = @numDomainID 
END 
GO

*/


/*
SET @vcItemName = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )

	SET @numBaseUnit = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.numBaseUnit) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.numBaseUnit FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )

	SET @monListPrice = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.monListPrice) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.monListPrice FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
			
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )
*/