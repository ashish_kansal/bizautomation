
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDeleteTask')
DROP PROCEDURE USP_GetDeleteTask
GO
CREATE PROCEDURE [dbo].[USP_GetDeleteTask]
@numDomainID as numeric(9)=0,    
@numTaskId as numeric(18)=0 
as    
BEGIN  
	DELETE FROM  StagePercentageDetailsTask WHERE numTaskId=@numTaskId AND numDomainID=@numDomainID
END 