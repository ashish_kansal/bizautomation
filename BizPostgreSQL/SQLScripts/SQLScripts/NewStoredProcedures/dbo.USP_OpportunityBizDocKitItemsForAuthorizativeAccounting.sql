SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocKitItemsForAuthorizativeAccounting')
DROP PROCEDURE USP_OpportunityBizDocKitItemsForAuthorizativeAccounting
GO
CREATE PROCEDURE  [dbo].[USP_OpportunityBizDocKitItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
	@numOppId NUMERIC(18,0),
	@numOppBizDocsId NUMERIC(18,0)                                                            
)                                                                                                                                        
AS                           
BEGIN
	DECLARE @numBizDocId AS NUMERIC(18,0)                                                           

	SELECT 
		@numBizDocId=numBizDocId
	FROM 
		OpportunityBizDocs 
	WHERE 
		numOppBizDocsId=@numOppBizDocsId

	IF @numBizDocId = 296 --FULFILLMENT BIZDOC
	BEGIN
		--STORE AVERAGE COST FOR SHIPPED ITEM SO THAT WE CAN CALCULATE INVENTORY IMAPCT IN ACCOUNTING AND ALSO USE IT WHEN IETM RETURN
		INSERT INTO OpportunityBizDocKitItems
		(
			numOppBizDocID,
			numOppBizDocItemID,
			numOppChildItemID,
			numChildItemID,
			monAverageCost
		)
		SELECT
			OBI.numOppBizDocID
			,OBI.numOppBizDocItemID
			,OKI.numOppChildItemID
			,OKI.numChildItemID
			,(CASE 
				WHEN ISNULL(I.charItemType,'') = 'S' AND ISNULL(I.bitExpenseItem,0) = 1 
				THEN 
					(
						CASE 
							WHEN ISNULL(IMain.tintKitAssemblyPriceBasedOn,0) = 3 
							THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
							ELSE ISNULL(I.monListPrice,0) 
						END
					)
				ELSE 
					ISNULL(I.monAverageCost,0) 
			END) 
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityItems OI
		ON
			OBI.numOppItemID=OI.numoppitemtCode
		INNER JOIN 
			Item IMain
		ON
			OI.numItemCode = IMain.numItemCode
		INNER JOIN 
			OpportunityKitItems OKI 
		ON 
			OKI.numOppItemID=OI.numoppitemtCode 
			AND OI.numOppId=OKI.numOppId
		INNER JOIN 
			Item I
		ON 
			OKI.numChildItemID=i.numItemCode
		LEFT JOIN
			Vendor V
		ON
			I.numVendorID = V.numVendorID
			AND I.numItemCode = V.numItemCode
		WHERE 
			OBI.numOppBizDocID=@numOppBizDocsId
			AND OBI.numOppBizDocItemID NOT IN (SELECT numOppBizDocItemID FROM OpportunityBizDocKitItems WHERE numOppBizDocID=@numOppBizDocsId)

		INSERT INTO OpportunityBizDocKitChildItems
		(
			numOppBizDocID,
			numOppBizDocItemID,
			numOppBizDocKitItemID,
			numOppKitChildItemID,
			numChildItemID,
			monAverageCost
		)
		SELECT
			OBI.numOppBizDocID
			,OBI.numOppBizDocItemID
			,OBKI.numOppBizDocItemID
			,OKCI.numOppKitChildItemID
			,OKCI.numItemID
			,(CASE 
				WHEN ISNULL(I.charItemType,'') = 'S' AND ISNULL(I.bitExpenseItem,0) = 1 
				THEN 
					(
						CASE 
							WHEN ISNULL(IMain.tintKitAssemblyPriceBasedOn,0) = 3 
							THEN ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
							ELSE ISNULL(I.monListPrice,0) 
						END
					)
				ELSE 
					ISNULL(I.monAverageCost,0) 
			END) 
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityBizDocKitItems OBKI
		ON
			OBI.numOppBizDocItemID = OBKI.numOppBizDocItemID
		INNER JOIN 
			Item IMain
		ON
			OBKI.numChildItemID = IMain.numItemCode
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OBI.numOppItemID = OKCI.numOppItemID
			AND OBKI.numOppChildItemID = OKCI.numOppChildItemID
		INNER JOIN 
			Item I
		ON 
			OKCI.numItemID=I.numItemCode
		LEFT JOIN
			Vendor V
		ON
			I.numVendorID = V.numVendorID
			AND I.numItemCode = V.numItemCode
		WHERE 
			OBI.numOppBizDocID=@numOppBizDocsId
			AND OBI.numOppBizDocItemID NOT IN (SELECT numOppBizDocItemID FROM OpportunityBizDocKitChildItems WHERE numOppBizDocID=@numOppBizDocsId)
	END

     --OpportunityKitItems
    SELECT        
		I.charitemType as type,
		CONVERT(VARCHAR,I.numItemCode) as ItemCode,
		OBKI.numOppChildItemID AS numoppitemtCode,
		(ISNULL(OBI.numUnitHour,0) * (OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,i.numDomainID,i.numBaseUnit),1))) as Unit,       
		ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(I.numAssetChartAcntId,0) as itemInventoryAsset,
		ISNULL(I.numCOGsChartAcntId,0) AS itemCoGs
		,isnull(I.bitExpenseItem,0) as bitExpenseItem
		,isnull(I.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(OBKI.monAverageCost,0) END) AS ShippedAverageCost,
		ISNULL(OI.bitDropShip,0) AS bitDropShip,  
		NULLIF(OI.numProjectID,0) numProjectID,
		NULLIF(OI.numClassID,0) numClassID
	FROM  
		OpportunityBizDocItems OBI
	INNER JOIN 
		OpportunityBizDocKitItems OBKI 
	ON 
		OBKI.numOppBizDocItemID=OBI.numOppBizDocItemID
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKI.numOppChildItemID=OBKI.numOppChildItemID
	INNER JOIN
		OpportunityItems OI
	ON
		OKI.numOppItemID=OI.numoppitemtCode
	INNER JOIN 
		Item IMain
	ON 
		OI.numItemCode=IMain.numItemCode
	INNER JOIN 
		Item I
	ON 
		OBKI.numChildItemID=I.numItemCode     
	WHERE 
		OI.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
		AND ISNULL(I.bitKitParent,0) = 0
		AND ISNULL(iMain.bitAssembly,0) = 0
	UNION ALL
	SELECT 
		I.charitemType as type,              
		CONVERT(VARCHAR,I.numItemCode) as ItemCode,
		OBKCI.numOppKitChildItemID AS numoppitemtCode,
		(ISNULL(OBI.numUnitHour,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * (ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,i.numDomainID,i.numBaseUnit),1))) as Unit,     
		ISNULL(I.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(I.numAssetChartAcntId,0) as itemInventoryAsset,
		ISNULL(I.numCOGsChartAcntId,0) AS itemCoGs
		,isnull(I.bitExpenseItem,0) as bitExpenseItem
		,isnull(I.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(OBKCI.monAverageCost,0) END) AS ShippedAverageCost,
		ISNULL(OI.bitDropShip,0) AS bitDropShip,  
		NULLIF(OI.numProjectID,0) numProjectID,
		NULLIF(OI.numClassID,0) numClassID
	FROM  
		OpportunityBizDocItems OBI
	INNER JOIN 
		OpportunityBizDocKitChildItems OBKCI 
	ON 
		OBKCI.numOppBizDocItemID=OBI.numOppBizDocItemID
	INNER JOIN
		OpportunityKitChildItems OKCI
	ON
		OKCI.numOppKitChildItemID = OBKCI.numOppKitChildItemID
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKCI.numOppChildItemID = OKI.numOppChildItemID
	INNER JOIN
		OpportunityItems OI
	ON
		OKI.numOppItemID=OI.numoppitemtCode
	INNER JOIN 
		Item IMain
	ON 
		OI.numItemCode=IMain.numItemCode
	INNER JOIN 
		Item I
	ON 
		OBKCI.numChildItemID=I.numItemCode             
	WHERE 
		OI.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
END