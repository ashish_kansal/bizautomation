GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateSitePageStatus' ) 
    DROP PROCEDURE USP_UpdateSitePageStatus
GO
CREATE PROCEDURE USP_UpdateSitePageStatus
    @numPageID NUMERIC,
    @numSiteID NUMERIC,
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @bitIsActive BIT
AS 
BEGIN
    UPDATE  dbo.SitePages
    SET     bitIsActive = @bitIsActive,
            numModifiedBy = @numUserCntID,
            dtModifiedDate = GETUTCDATE()
    WHERE   numPageID = @numPageID
            AND numDomainID = @numDomainId
            AND numSiteID = @numSiteID
END


