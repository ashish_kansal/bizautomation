GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMultiAP')
DROP PROCEDURE USP_GetMultiAP
GO
CREATE PROCEDURE [dbo].[USP_GetMultiAP]
(@numParentDomainID  int,
 @dtFromDate datetime,
 @dtToDate datetime,
@numSubscriberID int,
@Rollup int=0)
as
begin

CREATE TABLE #APSUMMARY
(vcDomainName VARCHAR(150),
Total  DECIMAL(20,5))

INSERT INTO #APSUMMARY

select DN.vcDomainName, Sum(Total)* (-1) as Total
from VIEW_APDAILYSUMMARY VARD
INNER JOIN 
(SELECT * FROM DOMAIN DN WHERE DN.numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (SELECT * FROM DOMAIN DNV WHERE DNV.numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numParentDomainID = @numParentDomainID or DN.numDomainID=@numParentDomainID)
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.vcDomainName
union
select DN.vcDomainName, Sum(Total)* (-1) as Total
from VIEW_APDAILYSUMMARY VARD
INNER JOIN 
(SELECT * FROM DOMAIN DN WHERE DN.numSubscriberId=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (SELECT * FROM DOMAIN DNV where DNV.numSubscriberID=@numSubscriberID )DNV ON VARD.numDomainID = DNV.numDomainID
AND (DN.numDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.vcDomainName

----------------------------------------
union 
--------------------------
select DN.vcDomainName, Sum(Total) * (-1) as Total
from VIEW_APDAILYSUMMARY VARD
INNER JOIN 
(SELECT * FROM DOMAIN DN WHERE DN.numSubscriberId=@numSubscriberID) DN ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (SELECT * FROM DOMAIN DNV where DNV.numSubscriberID=@numSubscriberID )DNV  ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numParentDomainID = @numParentDomainID or DN.numDomainID=@numParentDomainID)
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.vcDomainName
union
select DN.vcDomainName, Sum(Total)* (-1) as Total
from VIEW_APDAILYSUMMARY VARD
INNER JOIN 
(SELECT * FROM DOMAIN DN WHERE DN.numSubscriberId=@numSubscriberID) DN  ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (SELECT * FROM DOMAIN DNV where DNV.numSubscriberID=@numSubscriberID )DNV   ON VARD.numDomainID = DNV.numDomainID
AND (DN.numParentDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.vcDomainName


if @Rollup=0
	begin
		SELECT vcDomainName, Sum(Total) as Total FROM #APSUMMARY GROUP BY vcDomainName;
	end
else if @Rollup=1
	begin
		SELECT 'AP Roll-Up' AS vcDomainName, ISNULL(Sum(Total),0) as Total FROM #APSUMMARY ;
	end

DROP TABLE #APSUMMARY;

end
