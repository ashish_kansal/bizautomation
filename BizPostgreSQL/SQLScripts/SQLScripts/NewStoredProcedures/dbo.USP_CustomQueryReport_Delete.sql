SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_Delete')
DROP PROCEDURE dbo.USP_CustomQueryReport_Delete
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_Delete]
	@numReportID NUMERIC(18,0)
AS 
BEGIN
	DELETE FROM CustomQueryReport WHERE numReportID = @numReportID
END