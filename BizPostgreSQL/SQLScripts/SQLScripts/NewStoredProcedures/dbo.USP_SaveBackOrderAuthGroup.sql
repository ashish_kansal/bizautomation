GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Priya
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savebackorderauthgroup')
DROP PROCEDURE usp_savebackorderauthgroup
GO
CREATE PROCEDURE [dbo].[USP_SaveBackOrderAuthGroup] 
	@numDomainID AS NUMERIC(18,0),
	@strAuthGroupID AS VARCHAR(MAX)
AS
BEGIN

	DELETE from AuthenticationGroupBackOrder WHERE numDomainID=@numDomainID 

	IF LEN(ISNULL(@strAuthGroupID,'')) > 0
	BEGIN
		DECLARE @Tempdata TABLE 
		(
			numDomainID NUMERIC,
			strGroupID VARCHAR(MAX)
		)

		INSERT @Tempdata SELECT @numDomainID, @strAuthGroupID

		;WITH tmp(numDomainID, numGroupID, GroupID) AS
		(
			SELECT
				numDomainID,
				LEFT(strGroupID, CHARINDEX(',', strGroupID + ',') - 1),
				STUFF(strGroupID, 1, CHARINDEX(',', strGroupID + ','), '')
			FROM @Tempdata
			UNION all

			SELECT
				numDomainID,
				LEFT(GroupID, CHARINDEX(',', GroupID + ',') - 1),
				STUFF(GroupID, 1, CHARINDEX(',', GroupID + ','), '')
			FROM tmp
			WHERE
				GroupID > ''
		)


		INSERT INTO 
			AuthenticationGroupBackOrder
		SELECT
			numDomainID,    
			numGroupID
		FROM tmp
	END
END                        
GO
