
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ChartofChildAccounts' ) 
    DROP PROCEDURE USP_ChartofChildAccounts
GO
/****** Object:  StoredProcedure [dbo].[USP_ChartofChildAccounts]    Script Date: 09/25/2009 16:21:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Friday, July 25, 2008>  
-- Description: <This procedure is use for fetching the Child Account records against Parent Account Id>  
-- =============================================  
-- exec USP_ChartofChildAccounts @numDomainId=169,@numUserCntId=1,@strSortOn='DESCRIPTION',@strSortDirection='ASCENDING'
CREATE PROCEDURE [dbo].[USP_ChartofChildAccounts]
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntId AS NUMERIC(9) = 0,
    @strSortOn AS VARCHAR(11) = NULL,
    @strSortDirection AS VARCHAR(10) = NULL
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @dtFinYearFrom DATETIME ;
	DECLARE @dtFinYearTo DATETIME ;
	SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                    FROM     FINANCIALYEAR
                    WHERE    dtPeriodFrom <= GETDATE()
                            AND dtPeriodTo >= GETDATE()
                            AND numDomainId = @numDomainId
                    ) ;
	SELECT @dtFinYearTo = MAX(datEntry_Date) FROM General_Journal_Header WHERE numDomainID=@numDomainId


	DECLARE @TEMP TABLE
	(
		numAccountID NUMERIC(18,0)
		,bitIsSubAccount BIT
		,bitActive BIT
		,vcAccountCode VARCHAR(300)
		,numParentAccontID NUMERIC(18,0)
		,OPENING DECIMAL(20,5)
		,numDebitAmt DECIMAL(20,5)
		,numCreditAmt DECIMAL(20,5)
	)

	DECLARE @TEMP1 TABLE
	(
		numAccountID NUMERIC(18,0)
		,bitIsSubAccount BIT
		,bitActive BIT
		,vcAccountCode VARCHAR(300)
		,numParentAccontID NUMERIC(18,0)
		,OPENING DECIMAL(20,5)
		,numDebitAmt DECIMAL(20,5)
		,numCreditAmt DECIMAL(20,5)
	)

	;WITH CTE (numAccountID,bitIsSubAccount,vcAccountCode,numParentAccontID) AS
	(
		SELECT 
			numAccountID
			,bitIsSubAccount
			,vcAccountCode
			,numParentAccId
		FROM
			Chart_Of_Accounts
		WHERE
			numDomainId=@numDomainId
			AND ISNULL(numParentAccId,0) = 0
		UNION ALL
		SELECT 
			COA.numAccountID
			,COA.bitIsSubAccount
			,COA.vcAccountCode
			,COA.numParentAccId
		FROM
			Chart_Of_Accounts COA
		INNER JOIN
			CTE C
		ON
			COA.numParentAccId = C.numAccountID
	)

	INSERT INTO @TEMP 
		(numAccountID,bitIsSubAccount,vcAccountCode,numParentAccontID,OPENING,numDebitAmt,numCreditAmt) 
	SELECT 
		numAccountID,bitIsSubAccount,vcAccountCode,numParentAccontID,0,0,0 
	FROM 
		CTE

	;WITH CTE (numAccountID,numDebitAmt,numCreditAmt) AS
	(
		SELECT
			GJD.numChartAcntId
			,ISNULL(SUM(GJD.numDebitAmt),0) numDebitAmt
			,ISNULL(SUM(GJD.numCreditAmt),0) numCreditAmt
		FROM
			General_Journal_Header GJH
		INNER JOIN
			dbo.General_Journal_Details GJD
		ON
			GJH.numJournal_Id = GJD.numJournalId
		INNER JOIN
			@TEMP T
		ON
			GJD.numChartAcntId = T.numAccountID
		WHERE
			GJH.numDomainId=@numDomainId
			AND datEntry_Date BETWEEN  @dtFinYearFrom AND @dtFinYearTo
		GROUP BY
			GJD.numChartAcntId
	)

	UPDATE
		COA
	SET
		numDebitAmt = ISNULL(C.numDebitAmt,0)
		,numCreditAmt = ISNULL(C.numCreditAmt,0)
	FROM 
		@TEMP COA 
	INNER JOIN
		CTE C
	ON
		COA.numAccountID = C.numAccountID

	INSERT INTO @TEMP1 SELECT * FROM @TEMP

	;WITH CTE (numAccountId,numDebitAmt,numCreditAmt,RootID) as
	(
		SELECT 
			COA.numAccountId,
			COA.numDebitAmt,
			COA.numCreditAmt,
			COA.numAccountId as RootID
		FROM 
			@TEMP COA 
		UNION ALL
		SELECT 
			COA.numAccountId,
			COA.numDebitAmt,
			COA.numCreditAmt,
			C.RootID as RootID
		FROM
			@TEMP COA 
		INNER JOIN 
			CTE C 
		ON 
			COA.numParentAccontID=C.numAccountId
	)

	UPDATE 
		COA
	SET 
		numDebitAmt=ISNULL(numDebitAmtIncludingChildren,0),
		numCreditAmt=ISNULL(numCreditAmtIncludingChildren,0)
	FROM 
		@TEMP COA
	INNER JOIN 
	(
            select RootID,
                sum(numDebitAmt) as numDebitAmtIncludingChildren,
				sum(numCreditAmt) as numCreditAmtIncludingChildren
            from CTE C 
            group by RootID
    ) as S
	ON 
		COA.numAccountId = S.RootID
	option (maxrecursion 0) 

	DECLARE @PLSummary TABLE (numAccountId numeric(9),vcAccountName varchar(250),
	numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
	vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),bitIsSubAccount Bit);

	INSERT INTO  @PLSummary
	SELECT COA.numAccountId,COA.vcAccountName,COA.numParntAcntTypeID,COA.vcAccountDescription,COA.vcAccountCode,
	0 AS OPENING,
	ISNULL(SUM(numDebitAmt),0) as DEBIT,
	ISNULL(SUM(numCreditAmt),0) as CREDIT,
	ISNULL(COA.bitIsSubAccount,0)
	FROM 
		Chart_of_Accounts COA
	LEFT JOIN 
		@TEMP VJ 
	ON 
		COA.numAccountId = VJ.numAccountID
	WHERE 
		COA.numDomainId=@numDomainId 
		AND COA.bitActive = 1 
		AND (COA.vcAccountCode LIKE '0103%' OR COA.vcAccountCode LIKE '0104%' OR COA.vcAccountCode LIKE '0106%')
	GROUP BY
		COA.numAccountId,COA.vcAccountName,COA.numParntAcntTypeID,COA.vcAccountDescription,COA.vcAccountCode,COA.bitIsSubAccount


	DECLARE @PLOutPut TABLE (numAccountId numeric(9),vcAccountName varchar(250),
	numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
	vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));

	INSERT INTO @PLOutPut
	SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
		ISNULL(SUM(Opening),0) as Opening,
	ISNUlL(Sum(Debit),0) as Debit,ISNULL(Sum(Credit),0) as Credit
	FROM 
		AccountTypeDetail ATD RIGHT OUTER JOIN 
	@PLSummary PL ON
	PL.vcAccountCode LIKE ATD.vcAccountCode + '%'
	AND ATD.numDomainId=@numDomainId AND
	(ATD.vcAccountCode LIKE '0103%' OR
			ATD.vcAccountCode LIKE '0104%' OR
			ATD.vcAccountCode LIKE '0106%')
	WHERE 
	PL.bitIsSubAccount=0
	GROUP BY 
	ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;


	DECLARE @CURRENTPL DECIMAL(20,5) ;
	DECLARE @PLOPENING DECIMAL(20,5);
	DECLARE @PLCHARTID NUMERIC(8)
	DECLARE @TotalIncome DECIMAL(20,5);
	DECLARE @TotalExpense DECIMAL(20,5);
	DECLARE @TotalCOGS DECIMAL(20,5);
	DECLARE  @CurrentPL_COA DECIMAL(20,5)
	SET @CURRENTPL =0;	
	SET @PLOPENING=0;

	SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
	bitProfitLoss=1;

	SELECT @CurrentPL_COA =  ISNULL(sum(numCreditAmt),0)-ISNULL(sum(numDebitAmt),0) FROM @TEMP VJ WHERE numAccountId=@PLCHARTID;

	SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
	@PLOutPut P WHERE 
	vcAccountCode IN ('0103')

	SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
	@PLOutPut P WHERE 
	vcAccountCode IN ('0104')

	SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
	@PLOutPut P WHERE 
	vcAccountCode IN ('0106')

	PRINT 'CurrentPL_COA = ' + CAST(@CurrentPL_COA AS VARCHAR(20))
	PRINT 'TotalIncome = ' + CAST(@TotalIncome AS VARCHAR(20))
	PRINT 'TotalExpense = ' + CAST(@TotalExpense AS VARCHAR(20))
	PRINT 'TotalCOGS = ' + CAST(@TotalCOGS AS VARCHAR(20))

	SELECT @CURRENTPL = @CurrentPL_COA + @TotalIncome + @TotalExpense + @TotalCOGS 
	PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))

	SET @CURRENTPL=@CURRENTPL * (-1)

	SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM view_journal VJ WHERE numDomainID=@numDomainID 
	AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
	AND datEntry_Date <=  DATEADD(MINUTE,0,@dtFinYearFrom);

	SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(numCreditAmt),0)-ISNULL(sum(numDebitAmt),0) FROM General_Journal_Header GJH INNER JOIN General_Journal_Details GJD ON GJH.numJournal_Id=GJD.numJournalId WHERE GJH.numDomainID=@numDomainID
	AND GJD.numChartAcntId=@PLCHARTID AND datEntry_Date <=  DATEADD(MINUTE,0,@dtFinYearFrom);

	SET @CURRENTPL=@CURRENTPL * (-1)

	PRINT '@PLOPENING = ' + CAST(@PLOPENING AS VARCHAR(20))
	PRINT '@CURRENTPL = ' + CAST(@CURRENTPL AS VARCHAR(20))
	PRINT '-(@PLOPENING + @CURRENTPL) = ' + CAST((@PLOPENING + @CURRENTPL) AS VARCHAR(20))

	IF (@strSortOn = 'ID' AND @strSortDirection = 'ASCENDING') 
    BEGIN  

        SELECT  c.numAccountId,
                [numAccountTypeID],
                c.[numParntAcntTypeId],
                c.[vcAccountCode],
                ISNULL(c.[vcAccountCode], '') + ' ~ '
                + c.vcAccountName AS vcAccountName,
                c.vcAccountName AS vcAccountName1,
                AT.[vcAccountType],
                CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
						WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
						ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
							WHERE COA.numDomainId=@numDomainId 
							AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0)))
				END AS [numOpeningBal],	 
                ISNULL(c.numParentAccId,0) AS [numParentAccId],
                COA.vcAccountName AS [vcParentAccountName],
                (SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
                ISNULL(c.intLevel,1) AS [intLevel]
				,ISNULL(c.bitActive,0) bitActive
        FROM    Chart_Of_Accounts c
		LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
		LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
		OUTER APPLY (SELECT   SUM(numDebitAmt - numCreditAmt) AS OPENING
                            FROM     @TEMP1 VJ
                            WHERE 
                                    VJ.numAccountID = c.numAccountID 
								AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                ) AS t1
        WHERE   c.numDomainId = @numDomainId
        --AND c.bitActive = 0
        ORDER BY numAccountID ASC  
  
        SELECT  c.numAccountId,
				[numAccountTypeID],
				c.[numParntAcntTypeId],
				c.[vcAccountCode],
				ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
				c.vcAccountName AS vcAccountName1,
				AT.[vcAccountType],
				CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
						ELSE CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
								WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)	
								ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
										WHERE COA.numDomainId=@numDomainId 
										AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) 
							END
				END AS numOpeningBal,
				ISNULL(c.numParentAccId,0) AS [numParentAccId],
				COA.vcAccountName AS [vcParentAccountName],
				(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
				ISNULL(c.intLevel,1) AS [intLevel],
				ISNULL(c.IsConnected,0) AS [IsConnected]                        
				,ISNULL(c.numBankDetailID,0) numBankDetailID
				,ISNULL(c.bitActive,0) bitActive
		FROM    Chart_Of_Accounts c
				LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
				LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
				OUTER APPLY (SELECT   SUM(numDebitAmt - numCreditAmt) AS OPENING
                            FROM     @TEMP1 VJ
                            WHERE VJ.numAccountID = c.numAccountID 
									AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')) AS t1
        WHERE   c.numDomainId = @numDomainID
        --AND c.bitActive = 0
        ORDER BY c.numAccountID ASC
    END  
  
    IF (@strSortOn = 'DESCRIPTION' AND @strSortDirection = 'ASCENDING') 
    BEGIN         
        SELECT  c.numAccountId,
				[numAccountTypeID],
				c.[numParntAcntTypeId],
				c.[vcAccountCode],
				ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
				c.vcAccountName AS vcAccountName1,
				AT.[vcAccountType],
				CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
						ELSE CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
								WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
								ELSE (SELECT SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA 
										WHERE COA.numDomainId=@numDomainId 
										AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) 
							END
				END AS numOpeningBal,
				ISNULL(c.numParentAccId,0) AS [numParentAccId],
				COA.vcAccountName AS [vcParentAccountName],
				(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
				ISNULL(c.intLevel,1) AS [intLevel],
				ISNULL(c.IsConnected,0) AS [IsConnected]                      
				,ISNULL(c.numBankDetailID,0) numBankDetailID
				,ISNULL(c.bitActive,0) bitActive
		FROM    Chart_Of_Accounts c
				LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
				LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
				OUTER APPLY (SELECT   SUM(numDebitAmt - numCreditAmt) AS OPENING
                            FROM  @TEMP1 VJ
                            WHERE VJ.numAccountID = c.numAccountID 
									AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')) AS t1

        WHERE   c.numDomainId = @numDomainId
            AND c.[numParntAcntTypeId] IS NULL
            --AND c.bitActive = 0
        ORDER BY AT.vcAccountCode, case IsNumeric(c.vcNumber) when 1 then Replicate(0, 50 - Len(c.vcNumber)) + c.vcNumber else Replicate(9,50) end  ASC
  
        SELECT  c.numAccountId,
				[numAccountTypeID],
				c.[numParntAcntTypeId],
				c.[vcAccountCode],
				ISNULL(c.[vcAccountCode], '') + ' ~ ' + c.vcAccountName AS vcAccountName,
				c.vcAccountName AS vcAccountName1,
				AT.[vcAccountType],
				CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
						ELSE  CASE WHEN (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%') THEN ISNULL(t1.[OPENING],0)
								WHEN c.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL)
								ELSE (SELECT  SUM(COA.numOpeningBal) FROM dbo.Chart_Of_Accounts COA WHERE COA.numDomainId=@numDomainId AND (COA.vcAccountCode LIKE c.vcAccountCode OR (COA.vcAccountCode LIKE c.vcAccountCode + '%' AND COA.numParentAccID>0))) END
				END AS numOpeningBal,
				ISNULL(c.numParentAccId,0) AS [numParentAccId],
				COA.vcAccountName AS [vcParentAccountName],
				(SELECT MAX(intLevel) FROM dbo.Chart_Of_Accounts) AS [intMaxLevel],
				ISNULL(c.intLevel,1) AS [intLevel],
				ISNULL(c.IsConnected,0) AS [IsConnected]
				,ISNULL(c.numBankDetailID,0) numBankDetailID
				,ISNULL(c.bitActive,0) bitActive
		FROM    Chart_Of_Accounts c
				LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
				LEFT OUTER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = c.numParentAccId
				OUTER APPLY (SELECT   SUM(numDebitAmt - numCreditAmt) AS OPENING
                            FROM  @TEMP1 VJ
                            WHERE   VJ.numAccountID = c.numAccountID 
									AND (c.[vcAccountCode] LIKE '0103%' OR c.[vcAccountCode] LIKE '0104%' OR c.[vcAccountCode] LIKE '0106%')
                                    ) AS t1
        WHERE   c.numDomainId = @numDomainID
                AND c.[numParntAcntTypeId] IS NOT NULL
                --AND c.bitActive = 0
        ORDER BY AT.vcAccountCode, case IsNumeric(c.vcNumber) when 1 then Replicate(0, 50 - Len(c.vcNumber)) + c.vcNumber else Replicate(9,50) end  ASC
                
	END  
END
GO
	