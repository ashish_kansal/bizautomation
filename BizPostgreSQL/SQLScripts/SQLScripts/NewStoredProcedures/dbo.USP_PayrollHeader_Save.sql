GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PayrollHeader_Save')
DROP PROCEDURE USP_PayrollHeader_Save
GO
CREATE PROCEDURE [dbo].[USP_PayrollHeader_Save]
    @numDomainId NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
	@numComPayPeriodID NUMERIC(18,0),
	@tintMode TINYINT,
    @strPayrollDetail TEXT
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @hDocItem INT
	DECLARE @i AS INT
	DECLARE @iCount AS INT
	DECLARE @j AS INT
	DECLARE @jCount AS INT
	DECLARE @numUserID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numOppBizDocID NUMERIC(18,0)
	DECLARE @numTimeAndExpenseCommissionID NUMERIC(18,0)
	DECLARE @monTimeCommisionTotalAmount DECIMAL(20,5)
	DECLARE @monAmountToPay DECIMAL(20,5)
	DECLARE @numPayrollHeaderID NUMERIC(18,0)
	DECLARE @numComissionID NUMERIC(18,0)
	DECLARE @monCommissionAmount DECIMAL(20,5)

	DECLARE @TEMPTimeExpenseCommission TABLE
	(
		ID INT
		,numTimeAndExpenseCommissionID NUMERIC(18,0)
		,monTotalAmount DECIMAL(20,5)
	)

	IF NOT EXISTS (SELECT numPayrollHeaderID FROM PayrollHeader WHERE numComPayPeriodID=@numComPayPeriodID)
	BEGIN
		INSERT INTO PayrollHeader
		(
			numDomainId
			,numCreatedBy
			,dtCreatedDate
			,numComPayPeriodID
		)
		VALUES
		(
			@numDomainId
			,@numUserCntID
			,GETUTCDATE()
			,@numComPayPeriodID
		)

		SET @numPayrollHeaderID = SCOPE_IDENTITY()
	END
	ELSE 
	BEGIN
		UPDATE PayrollHeader SET dtModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numComPayPeriodID=@numComPayPeriodID

		SELECT 
			@numPayrollHeaderID=numPayrollHeaderID 
		FROM 
			PayrollHeader 
		WHERE 
			numComPayPeriodID=@numComPayPeriodID
	END

	IF @tintMode = 1 -- Pay Hours Worked
	BEGIN
		DECLARE @TEMPEmployeeTime TABLE
		(
			ID INT IDENTITY(1,1)
			,numUserCntID NUMERIC(18,0)
			,monAmountToPay DECIMAL(20,5)
			,monAdditionalAmtToPay DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strPayrollDetail

		INSERT INTO @TEMPEmployeeTime
		(
			numUserCntID
			,monAmountToPay
			,monAdditionalAmtToPay
		)
		SELECT 
			numUserCntID
			,ISNULL(monTimeAmtToPay,0)
			,ISNULL(monAdditionalAmtToPay,0)
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Employee',2)                                                                          
		WITH                       
		(                                                                          
			numUserCntID NUMERIC(18,0)
			,monTimeAmtToPay DECIMAL(20,5)
			,monAdditionalAmtToPay DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem

		-- FIRST UPDATE EIXSTING RECORDS
		UPDATE
			PD
		SET
			monHourlyAmt = TET.monAmountToPay,
			monAdditionalAmt = TET.monAdditionalAmtToPay
		FROM
			PayrollDetail PD
		INNER JOIN
			@TEMPEmployeeTime TET
		ON
			PD.numUserCntID = TET.numUserCntID
		WHERE
			PD.numPayrollHeaderID=@numPayrollHeaderID

		-- INSERT NEW RECORDS
		INSERT INTO PayrollDetail
		(
			numPayrollHeaderID
			,numUserCntID
			,monHourlyAmt
			,monAdditionalAmt
		)
		SELECT
			@numPayrollHeaderID
			,TET.numUserCntID
			,TET.monAmountToPay
			,TET.monAdditionalAmtToPay
		FROM
			@TEMPEmployeeTime TET
		LEFT JOIN
			PayrollDetail PD
		ON
			PD.numPayrollHeaderID=@numPayrollHeaderID
			AND TET.numUserCntID = PD.numUserCntID
		WHERE
			PD.numPayrollDetailID IS NULL

		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPEmployeeTime)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numUserID = numUserCntID
				,@monAmountToPay=monAmountToPay
			FROM 
				@TEMPEmployeeTime 
			WHERE 
				ID=@i

			-- FIRST SET COMMISSION AS NOT PAID
			UPDATE 
				TimeAndExpenseCommission 
			SET 
				bitCommissionPaid=0
				,monAmountPaid=0 
			WHERE
				numComPayPeriodID=@numComPayPeriodID
				AND numUserCntID=@numUserID
				AND (numType=1 OR numType=2) 
				AND numCategory=1

			UPDATE 
				StagePercentageDetailsTaskCommission 
			SET 
				bitCommissionPaid=0
				,monAmountPaid=0 
			WHERE
				numComPayPeriodID=@numComPayPeriodID
				AND numUserCntID=@numUserID

			DELETE FROM @TEMPTimeExpenseCommission

			INSERT INTO @TEMPTimeExpenseCommission
			(
				ID
				,numTimeAndExpenseCommissionID
				,monTotalAmount
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY ID)
				,ID
				,ISNULL(monTotalAmount,0)
			FROM
				TimeAndExpenseCommission
			WHERE
				numComPayPeriodID=@numComPayPeriodID
				AND numUserCntID=@numUserID
				AND (numType=1 OR numType=2) 
				AND numCategory=1
			ORDER BY
				ID

			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPTimeExpenseCommission)

			WHILE @j <= @jCount AND @monAmountToPay > 0
			BEGIN
				SELECT
					@numTimeAndExpenseCommissionID=numTimeAndExpenseCommissionID
					,@monTimeCommisionTotalAmount=monTotalAmount
				FROM
					@TEMPTimeExpenseCommission
				WHERE
					ID=@j

				IF @monTimeCommisionTotalAmount >= @monAmountToPay
				BEGIN
					UPDATE TimeAndExpenseCommission SET bitCommissionPaid=1,monAmountPaid=@monAmountToPay WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = 0
				END
				ELSE
				BEGIN
				UPDATE TimeAndExpenseCommission SET bitCommissionPaid=1,monAmountPaid=@monTimeCommisionTotalAmount WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = @monAmountToPay - @monTimeCommisionTotalAmount
				END
				
				SET @j = @j + 1
			END

			DELETE FROM @TEMPTimeExpenseCommission

			INSERT INTO @TEMPTimeExpenseCommission
			(
				ID
				,numTimeAndExpenseCommissionID
				,monTotalAmount
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY ID)
				,ID
				,ISNULL(monTotalAmount,0)
			FROM
				StagePercentageDetailsTaskCommission
			WHERE
				numComPayPeriodID=@numComPayPeriodID
				AND numUserCntID=@numUserID
			ORDER BY
				ID

			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPTimeExpenseCommission)

			WHILE @j <= @jCount AND @monAmountToPay > 0
			BEGIN
				SELECT
					@numTimeAndExpenseCommissionID=numTimeAndExpenseCommissionID
					,@monTimeCommisionTotalAmount=monTotalAmount
				FROM
					@TEMPTimeExpenseCommission
				WHERE
					ID=@j

				IF @monTimeCommisionTotalAmount >= @monAmountToPay
				BEGIN
					UPDATE StagePercentageDetailsTaskCommission SET bitCommissionPaid=1,monAmountPaid=@monAmountToPay WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = 0
				END
				ELSE
				BEGIN
				UPDATE StagePercentageDetailsTaskCommission SET bitCommissionPaid=1,monAmountPaid=@monTimeCommisionTotalAmount WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = @monAmountToPay - @monTimeCommisionTotalAmount
				END
				
				SET @j = @j + 1
			END
			
			IF @monAmountToPay > 0
			BEGIN
				RAISERROR('INVALID_HOURS_WORKED_PAYMENT',16,1)
			END

			SET @i = @i + 1
		END
	END
	ELSE IF @tintMode = 2 -- Pay reimbursable expense
	BEGIN
		DECLARE @TEMPEmployeeReimburseExpense TABLE
		(
			ID INT IDENTITY(1,1)
			,numUserCntID NUMERIC(18,0)
			,monAmountToPay DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strPayrollDetail

		INSERT INTO @TEMPEmployeeReimburseExpense
		(
			numUserCntID
			,monAmountToPay
		)
		SELECT 
			numUserCntID
			,ISNULL(monReimbursableExpenseToPay,0)
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Employee',2)                                                                          
		WITH                       
		(                                                                          
			numUserCntID NUMERIC(18,0)
			,monReimbursableExpenseToPay DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem

		-- FIRST UPDATE EIXSTING RECORDS
		UPDATE
			PD
		SET
			monReimbursableExpenses = TET.monAmountToPay
		FROM
			PayrollDetail PD
		INNER JOIN
			@TEMPEmployeeReimburseExpense TET
		ON
			PD.numUserCntID = TET.numUserCntID
		WHERE
			PD.numPayrollHeaderID=@numPayrollHeaderID

		-- INSERT NEW RECORDS
		INSERT INTO PayrollDetail
		(
			numPayrollHeaderID
			,numUserCntID
			,monReimbursableExpenses
		)
		SELECT
			@numPayrollHeaderID
			,TET.numUserCntID
			,TET.monAmountToPay
		FROM
			@TEMPEmployeeReimburseExpense TET
		LEFT JOIN
			PayrollDetail PD
		ON
			PD.numPayrollHeaderID=@numPayrollHeaderID
			AND TET.numUserCntID = PD.numUserCntID
		WHERE
			PD.numPayrollDetailID IS NULL

		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPEmployeeReimburseExpense)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numUserID = numUserCntID
				,@monAmountToPay=monAmountToPay
			FROM 
				@TEMPEmployeeReimburseExpense 
			WHERE 
				ID=@i

			-- FIRST SET EXPENSE AS NOT PAID
			UPDATE 
				TimeAndExpenseCommission 
			SET 
				bitCommissionPaid=0
				,monAmountPaid=0 
			WHERE
				numComPayPeriodID = @numComPayPeriodID
				AND numUserCntID=@numUserID
				AND numCategory=2
				AND bitReimburse = 1

			DELETE FROM @TEMPTimeExpenseCommission

			INSERT INTO @TEMPTimeExpenseCommission
			(
				ID
				,numTimeAndExpenseCommissionID
				,monTotalAmount
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY ID)
				,ID
				,ISNULL(monTotalAmount,0)
			FROM
				TimeAndExpenseCommission
			WHERE
				numComPayPeriodID = @numComPayPeriodID
				AND numUserCntID=@numUserID
				AND numCategory=2
				AND bitReimburse = 1
			ORDER BY
				ID

			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPTimeExpenseCommission)

			WHILE @j <= @jCount AND @monAmountToPay > 0
			BEGIN
				SELECT
					@numTimeAndExpenseCommissionID=numTimeAndExpenseCommissionID
					,@monTimeCommisionTotalAmount=monTotalAmount
				FROM
					@TEMPTimeExpenseCommission
				WHERE
					ID=@j

				IF @monTimeCommisionTotalAmount >= @monAmountToPay
				BEGIN
					UPDATE TimeAndExpenseCommission SET bitCommissionPaid=1,monAmountPaid=@monAmountToPay WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = 0
				END
				ELSE
				BEGIN
					UPDATE TimeAndExpenseCommission SET bitCommissionPaid=1,monAmountPaid=@monTimeCommisionTotalAmount WHERE ID=@numTimeAndExpenseCommissionID
					SET @monAmountToPay = @monAmountToPay - @monTimeCommisionTotalAmount
				END
				
				SET @j = @j + 1
			END

			IF @monAmountToPay > 0
			BEGIN
				RAISERROR('INVALID_REIMBURSABLE_EXPENSE_PAYMENT',16,1)
			END
			

			SET @i = @i + 1
		END
	END
	ELSE IF @tintMode = 3 -- pay remaining amount (commission)
	BEGIN
		DECLARE @TEMPCommission TABLE
		(
			ID INT IDENTITY(1,1)
			,numUserCntID NUMERIC(18,0)
			,numDivisionID NUMERIC(18,0)
			,numOppID NUMERIC(18,0)
			,numOppBizDocID NUMERIC(18,0)
			,monCommissionPaid DECIMAL(20,5)
		)

		DECLARE @TEMPBizDocCommission TABLE
		(
			ID INT
			,numComissionID NUMERIC(18,0)
			,numComissionAmount DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strPayrollDetail

		INSERT INTO @TEMPCommission
		(
			numUserCntID
			,numDivisionID
			,numOppID
			,numOppBizDocID
			,monCommissionPaid
		)
		SELECT 
			numUserCntID
			,numDivisionID
			,numOppID
			,numOppBizDocID
			,monCommissionPaid
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Employee',2)                                                                          
		WITH                       
		(                                                                          
			numUserCntID NUMERIC(18,0)
			,numDivisionID NUMERIC(18,0)
			,numOppID NUMERIC(18,0)
			,numOppBizDocID NUMERIC(18,0)
			,monCommissionPaid DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem


		-- FIRST UPDATE EIXSTING RECORDS
		UPDATE
			PD
		SET
			monCommissionAmt = ISNULL(TEMP.monCommissionToPay,0)
		FROM
			PayrollDetail PD
		CROSS APPLY
		(
			SELECT
				SUM(monCommissionPaid) monCommissionToPay
			FROM
				@TEMPCommission T1
			WHERE
				ISNULL(T1.numUserCntID,0)=ISNULL(PD.numUserCntID,0)
				AND ISNULL(T1.numDivisionID,0) = ISNULL(PD.numDivisionID,0)
		) TEMP
		WHERE
			PD.numPayrollHeaderID=@numPayrollHeaderID

		-- INSERT NEW RECORDS
		INSERT INTO PayrollDetail
		(
			numPayrollHeaderID
			,numUserCntID
			,numDivisionID
			,monCommissionAmt
		)
		SELECT
			@numPayrollHeaderID
			,ISNULL(TEMP.numUserCntID,0)
			,ISNULL(TEMP.numDivisionID,0)
			,monCommissionToPay
		FROM
		(
			SELECT
				numUserCntID
				,numDivisionID
				,SUM(monCommissionPaid) monCommissionToPay
			FROM
				@TEMPCommission T1
			GROUP BY
				numUserCntID
				,numDivisionID
		) TEMP
		LEFT JOIN
			PayrollDetail PD
		ON
			PD.numPayrollHeaderID=@numPayrollHeaderID
			AND ISNULL(TEMP.numUserCntID,0) = ISNULL(PD.numUserCntID,0)
			AND ISNULL(TEMP.numDivisionID,0) = ISNULL(PD.numDivisionID,0)
		WHERE
			PD.numPayrollDetailID IS NULL

		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPCommission)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numUserID = ISNULL(numUserCntID,0)
				,@numDivisionID=ISNULL(numDivisionID,0)
				,@numOppID=ISNULL(numOppID,0)
				,@numOppBizDocID=ISNULL(numOppBizDocID,0)
				,@monAmountToPay=ISNULL(monCommissionPaid,0)
			FROM 
				@TEMPCommission 
			WHERE 
				ID=@i

			-- FIRST SET COMMISSION AS NOT PAID
			UPDATE 
				BizDocComission 
			SET 
				bitCommisionPaid=0
				,monCommissionPaid=0 
			WHERE
				numComPayPeriodID = @numComPayPeriodID
				AND 1 = (CASE 
							WHEN tintAssignTo=3 
							THEN (CASE WHEN ISNULL(numUserCntID,0)=@numDivisionID THEN 1 ELSE 0 END) 
							ELSE (CASE WHEN ISNULL(numUserCntID,0)=@numUserID THEN 1 ELSE 0 END) 
						END)
				AND ISNULL(numOppID,0)=@numOppID
				AND ISNULL(numOppBizDocID,0)=@numOppBizDocID

			DELETE FROM @TEMPBizDocCommission

			INSERT INTO @TEMPBizDocCommission
			(
				ID
				,numComissionID
				,numComissionAmount
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numComissionID)
				,numComissionID
				,ISNULL(numComissionAmount,0)
			FROM
				BizDocComission
			WHERE
				numComPayPeriodID = @numComPayPeriodID
				AND 1 = (CASE 
							WHEN tintAssignTo=3 
							THEN (CASE WHEN ISNULL(numUserCntID,0)=@numDivisionID THEN 1 ELSE 0 END) 
							ELSE (CASE WHEN ISNULL(numUserCntID,0)=@numUserID THEN 1 ELSE 0 END) 
						END)
				AND ISNULL(numOppID,0)=@numOppID
				AND ISNULL(numOppBizDocID,0)=@numOppBizDocID
			ORDER BY
				numComissionID


			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPBizDocCommission)

			WHILE @j <= @jCount AND @monAmountToPay > 0
			BEGIN
				SELECT
					@numComissionID=numComissionID
					,@monCommissionAmount=numComissionAmount
				FROM
					@TEMPBizDocCommission
				WHERE
					ID=@j

				IF @monCommissionAmount >= @monAmountToPay
				BEGIN
					UPDATE BizDocComission SET bitCommisionPaid=1,monCommissionPaid=@monAmountToPay WHERE numComissionID=@numComissionID
					SET @monAmountToPay = 0
				END
				ELSE
				BEGIN
					UPDATE BizDocComission SET bitCommisionPaid=1,monCommissionPaid=@monCommissionAmount WHERE numComissionID=@numComissionID
					SET @monAmountToPay = @monAmountToPay - @monCommissionAmount
				END
				
				SET @j = @j + 1
			END

			IF @monAmountToPay > 0
			BEGIN
				RAISERROR('INVALID_COMMISSION_PAYMENT',16,1)
			END

			SET @i = @i + 1
		END
	END
	ELSE IF @tintMode = 4 -- pay remaining amount (All e.g Hours worked, reimbursable, overpayment, commission)
	BEGIN
		DECLARE @numTempUserCntID NUMERIC(18,0)
				,@numTempDivisionID NUMERIC(18,0)
				,@monTempHoursWorked DECIMAL(20,5)
				,@monTempAdditionalAmt DECIMAL(20,5)
				,@monTempReimbursableExpense DECIMAL(20,5)
				,@monTempSalesReturn DECIMAL(20,5)
				,@monTempOverpayment DECIMAL(20,5)
				,@monTempCommissionAmount DECIMAL(20,5)

		DECLARE @TEMPCommissionPay TABLE
		(
			ID INT IDENTITY(1,1)
			,numUserCntID NUMERIC(18,0)
			,numDivisionID NUMERIC(18,0)
			,monHoursWorked DECIMAL(20,5)
			,monAdditionalAmt DECIMAL(20,5)
			,monReimbursableExpense DECIMAL(20,5)
			,monSalesReturn DECIMAL(20,5)
			,monOverpayment DECIMAL(20,5)
			,monCommissionAmount DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strPayrollDetail

		INSERT INTO @TEMPCommissionPay
		(
			numUserCntID
			,numDivisionID
			,monHoursWorked
			,monAdditionalAmt
			,monReimbursableExpense
			,monSalesReturn
			,monOverpayment
			,monCommissionAmount
		)
		SELECT 
			numUserCntID
			,numDivisionID
			,monHoursWorked
			,monAdditionalAmt
			,monReimbursableExpense
			,monSalesReturn
			,monOverpayment
			,monCommissionAmount
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Employee',2)                                                                          
		WITH                       
		(                                                                          
			numUserCntID NUMERIC(18,0)
			,numDivisionID NUMERIC(18,0)
			,monHoursWorked DECIMAL(20,5)
			,monAdditionalAmt DECIMAL(20,5)
			,monReimbursableExpense DECIMAL(20,5)
			,monSalesReturn DECIMAL(20,5)
			,monOverpayment DECIMAL(20,5)
			,monCommissionAmount DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem

		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPCommissionPay)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numTempUserCntID=ISNULL(numUserCntID,0)
				,@numTempDivisionID=ISNULL(numDivisionID,0)
				,@monTempHoursWorked=ISNULL(monHoursWorked,0)
				,@monTempAdditionalAmt=ISNULL(monAdditionalAmt,0)
				,@monTempReimbursableExpense=ISNULL(monReimbursableExpense,0)
				,@monTempSalesReturn=ISNULL(monSalesReturn,0)
				,@monTempOverpayment=ISNULL(monOverpayment,0)
				,@monTempCommissionAmount=ISNULL(monCommissionAmount,0)
			FROM
				@TEMPCommissionPay
			WHERE
				ID=@i

			IF @numTempUserCntID > 0
			BEGIN
				IF ISNULL((SELECT 
								SUM(monTotalAmount) 
							FROM 
								TimeAndExpenseCommission 
							WHERE 
								numComPayPeriodID=@numComPayPeriodID 
								AND numUserCntID=@numTempUserCntID 
								AND (numType=1 OR numType=2) 
								AND numCategory=1),0) <> @monTempHoursWorked
				BEGIN
					RAISERROR('INVALID_HOURS_WORKED_PAYMENT',16,1)
				END

				IF ISNULL((SELECT 
								SUM(monTotalAmount) 
							FROM 
								TimeAndExpenseCommission 
							WHERE 
								numComPayPeriodID=@numComPayPeriodID 
								AND numUserCntID=@numTempUserCntID 
								AND numCategory=2
								AND bitReimburse = 1),0) <> @monTempReimbursableExpense
				BEGIN
					RAISERROR('INVALID_REIMBURSABLE_EXPENSE_PAYMENT',16,1)
				END
			END

			IF ISNULL((SELECT 
							SUM(monCommissionReversed) 
						FROM 
							SalesReturnCommission
						WHERE 
							numComPayPeriodID=@numComPayPeriodID 
							AND 1  = (CASE 
										WHEN @numTempDivisionID > 0
										THEN (CASE WHEN numUserCntID=@numTempDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
										ELSE (CASE WHEN numUserCntID=@numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
									END)),0) <> @monTempSalesReturn
			BEGIN
				RAISERROR('INVALID_SALES_RETURN_PAYMENT',16,1)
			END

			IF ISNULL((SELECT 
							SUM(monDifference) 
						FROM 
							BizDocComissionPaymentDifference BDCPD
						INNER JOIN
							BizDocComission BDC
						ON
							BDCPD.numComissionID=BDC.numComissionID
						WHERE 
							BDC.numDomainID=@numDomainId
							AND ISNULL(bitDifferencePaid,0) = 0),0) <> @monTempOverpayment
			BEGIN
				RAISERROR('INVALID_OVER_PAYMENT',16,1)
			END

			PRINT @numTempUserCntID
			PRINT @monTempCommissionAmount
			IF ISNULL((SELECT 
							SUM(numComissionAmount) 
						FROM 
							BizDocComission 
						WHERE 
							numComPayPeriodID=@numComPayPeriodID 
							AND 1  = (CASE 
										WHEN @numTempDivisionID > 0
										THEN (CASE WHEN numUserCntID=@numTempDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
										ELSE (CASE WHEN numUserCntID=@numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
									END)),0) <> @monTempCommissionAmount
			BEGIN
				RAISERROR('INVALID_COMMISSION_PAYMENT',16,1)
			END

			IF EXISTS (SELECT numPayrollDetailID FROM PayrollDetail WHERE numPayrollHeaderID=@numPayrollHeaderID AND ISNULL(numUserCntID,0) = @numTempUserCntID AND ISNULL(numDivisionID,0)=@numTempDivisionID)
			BEGIN
				UPDATE
					PayrollDetail
				SET
					monHourlyAmt=@monTempHoursWorked
					,monAdditionalAmt=@monTempAdditionalAmt
					,monReimbursableExpenses=@monTempReimbursableExpense
					,monSalesReturn=@monTempSalesReturn
					,monOverPayment=@monTempOverpayment
					,monCommissionAmt=@monTempCommissionAmount
				WHERE
					numPayrollHeaderID=@numPayrollHeaderID 
					AND ISNULL(numUserCntID,0) = @numTempUserCntID 
					AND ISNULL(numDivisionID,0)=@numTempDivisionID
			END
			ELSE
			BEGIN
				INSERT INTO PayrollDetail
				(
					numDomainId
					,numPayrollHeaderID
					,numUserCntID
					,numDivisionID
					,monHourlyAmt
					,monAdditionalAmt
					,monReimbursableExpenses
					,monSalesReturn
					,monOverPayment
					,monCommissionAmt
				)
				VALUES
				(
					@numDomainId
					,@numPayrollHeaderID
					,@numTempUserCntID
					,@numTempDivisionID
					,@monTempHoursWorked
					,@monTempAdditionalAmt
					,@monTempReimbursableExpense
					,@monTempSalesReturn
					,@monTempOverpayment
					,@monTempCommissionAmount
				)
			END


			UPDATE 
				TimeAndExpenseCommission 
			SET 
				bitCommissionPaid=1
				,monAmountPaid=monTotalAmount 
			WHERE 
				numComPayPeriodID=@numComPayPeriodID 
				AND numUserCntID=@numTempUserCntID
			
			UPDATE 
				SalesReturnCommission 
			SET 
				bitCommissionReversed=1
			WHERE 
				numComPayPeriodID=@numComPayPeriodID 
				AND 1  = (CASE 
							WHEN @numTempDivisionID > 0
							THEN (CASE WHEN numUserCntID=@numTempDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
							ELSE (CASE WHEN numUserCntID=@numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
						END)

			UPDATE 
				BizDocComissionPaymentDifference
			SET
				bitDifferencePaid=1
				,numComPayPeriodID=@numComPayPeriodID
			WHERE 
				ISNULL(bitDifferencePaid,0) = 0

			UPDATE 
				BizDocComission 
			SET 
				bitCommisionPaid=1
				,monCommissionPaid=numComissionAmount
			WHERE 
				numComPayPeriodID=@numComPayPeriodID 
				AND 1  = (CASE 
							WHEN @numTempDivisionID > 0
							THEN (CASE WHEN numUserCntID=@numTempDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
							ELSE (CASE WHEN numUserCntID=@numTempUserCntID AND tintAssignTo <> 3 THEN 1 ELSE 0 END)
						END)

			SET @i = @i + 1
		END 
	END

	UPDATE
		PayrollDetail
	SET
		monTotalAmt = ISNULL(monHourlyAmt,0) + ISNULL(monAdditionalAmt,0) + ISNULL(monReimbursableExpenses,0) + ISNULL(monCommissionAmt,0) + ISNULL(monOverPayment,0) - ISNULL(monSalesReturn,0)
	WHERE
		numPayrollHeaderID=@numPayrollHeaderID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO