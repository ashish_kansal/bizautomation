GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateSingleFieldValue' ) 
    DROP PROCEDURE USP_UpdateSingleFieldValue
GO

CREATE PROCEDURE USP_UpdateSingleFieldValue
	@tintMode AS TINYINT,
	@numUpdateRecordID NUMERIC,
	@numUpdateValueID NUMERIC=0,
	@vcText AS TEXT,
	@numDomainID AS NUMERIC= 0 
AS 
BEGIN
	 -- For 26 and 37
     DECLARE @Status AS NUMERIC(9,0)
     DECLARE @SiteID       AS NUMERIC(9,0)
     DECLARE @OrderStatus  AS NUMERIC(9,0)
	 DECLARE @BizdocStatus AS NUMERIC(9,0)  
	 DECLARE @numDomain AS NUMERIC(18,0)
	 SET @numDomain = @numDomainID 

	IF @tintMode=1
	BEGIN
		UPDATE  dbo.OpportunityBizDocsDetails
		SET     bitAmountCaptured = @numUpdateValueID
		WHERE   numBizDocsPaymentDetId = @numUpdateRecordID
	END
--	IF @tintMode = 2 
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     numStatus = @numUpdateValueID
--			WHERE   numOppId = @numUpdateRecordID
--		END
	IF @tintMode = 3
		BEGIN
			 UPDATE AdditionalContactsInformation SET txtNotes=ISNULL(CAST(txtNotes AS VARCHAR(8000)),'') + ' ' +  ISNULL(CAST(@vcText AS VARCHAR(8000)),'') WHERE numContactId=@numUpdateRecordID
		END
	IF @tintMode = 4
		BEGIN
			 UPDATE dbo.OpportunityBizDocsDetails SET numBillPaymentJournalID=@numUpdateValueID WHERE numBizDocsPaymentDetId=@numUpdateRecordID
		END
		
	IF @tintMode = 5
		BEGIN
			 UPDATE dbo.BizDocComission SET bitCommisionPaid=@numUpdateValueID WHERE numOppBizDocID=@numUpdateRecordID
		END
		
	IF @tintMode = 6
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET numOppBizDocID=@numUpdateValueID WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 7
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET dtRecurringDate=CAST( CAST( @vcText AS VARCHAR) AS DATETIME) WHERE numOppRecID=@numUpdateRecordID
		END
		
	IF @tintMode = 8
		BEGIN
			 UPDATE dbo.OpportunityRecurring SET fltBreakupPercentage=CAST( CAST( @vcText AS VARCHAR) AS float) WHERE numOppRecID=@numUpdateRecordID
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--	IF @tintMode = 9
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     txtComments = CAST(@vcText AS VARCHAR(1000))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
--	IF @tintMode = 10
--		BEGIN
--			UPDATE  dbo.OpportunityMaster
--			SET     vcRefOrderNo = CAST(@vcText AS VARCHAR(100))
--			WHERE   numOppId = @numUpdateRecordID
--		END    
	IF @tintMode = 11
		BEGIN
			DELETE FROM dbo.LuceneItemsIndex WHERE numItemCode=@numUpdateRecordID
		END    
	IF @tintMode = 12
		BEGIN
			UPDATE dbo.Domain SET vcPortalName = CAST(@vcText AS VARCHAR(50))
			WHERE numDomainId=@numUpdateRecordID
		END 
	IF @tintMode = 13
		BEGIN
			UPDATE dbo.WareHouseItems SET numOnHand =numOnHand + CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(18,0)),dtModified = GETDATE() 
			WHERE numWareHouseItemID = @numUpdateRecordID
			
			DECLARE @numItemCode NUMERIC(18)

			SELECT @numItemCode=numItemID from WareHouseItems where numWareHouseItemID = @numUpdateRecordID

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numUpdateRecordID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = 'Inventory Adjustment', --  varchar(100)
				@numModifiedBy = @numUpdateValueID,
				@numDomainID = @numDomain
			 
		END
	IF @tintMode = 14
		BEGIN
			UPDATE dbo.Item SET monAverageCost=CAST(CAST(@vcText AS VARCHAR(50)) AS numeric(20,5))
			WHERE numItemCode = @numUpdateRecordID
		END
		IF @tintMode = 15
		BEGIN
			UPDATE dbo.EmailHistory  SET numListItemId = @numUpdateValueID  WHERE numEmailHstrID=@numUpdateRecordID
		END
		IF @tintMode = 16
		BEGIN
			UPDATE dbo.EmailHistory SET bitIsRead=@numUpdateValueID WHERE numEmailHstrID IN ( SELECT Id FROM dbo.SplitIDs(CAST(@vcText AS VARCHAR(200)) ,','))
		END
		IF @tintMode = 17
		BEGIN
			UPDATE CartItems SET numUserCntId = @numUpdateValueID WHERE vcCookieId =CAST(@vcText AS VARCHAR(MAX)) AND numUserCntId = 0
		END
		IF @tintMode = 18
		BEGIN
			UPDATE OpportunityBizDocs SET tintDeferred = 0 WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 19
		BEGIN
			UPDATE dbo.OpportunityMaster SET vcPOppName=CAST(@vcText AS VARCHAR(100)) WHERE numOppId=@numUpdateRecordID
		END
	    IF @tintMode = 20
		BEGIN
			UPDATE UserMaster SET tintTabEvent=@numUpdateValueID WHERE numUserId=@numUpdateRecordID
		END
		IF @tintMode = 21
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocTempID = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID
		END
		IF @tintMode = 22
		BEGIN
			UPDATE OpportunityBizDocItems SET tintTrackingStatus = 1 WHERE numOppBizDocItemID =@numUpdateRecordID
		END
		IF	 @tintMode = 23
		BEGIN
			UPDATE item SET bintModifiedDate = GETUTCDATE() WHERE numItemCode=@numUpdateRecordID			
		END
		IF @tintMode = 24
		BEGIN
			UPDATE dbo.OpportunityMaster SET numStatus=@numUpdateValueID WHERE numOppId in (select Items from dbo.split(@vcText,','))
		END
		
		IF @tintMode = 25
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcEighthFldValue = @vcText WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		IF @tintMode = 26
		BEGIN
			
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
				SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numOrderStatus,0) ,@BizdocStatus = ISNULL(numBizDocStatus,0)  FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		       
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    Update OpportunityBizDocs set monAmountPaid = ISNULL(monDealAmount,0),numBizDocStatus = @BizdocStatus  WHERE  numOppId = @numUpdateRecordID and bitAuthoritativeBizDocs =1
			  END   
		END
		IF @tintMode = 27
		BEGIN
			UPDATE OpportunityBizDocs SET numBizDocStatusOLD=numBizDocStatus,numBizDocStatus = @numUpdateValueID WHERE numOppBizDocsId=@numUpdateRecordID 
		END
	    IF @tintMode = 28
		BEGIN
			UPDATE General_Journal_Details SET bitReconcile=(CASE WHEN CAST(@vcText AS VARCHAR(5))='R' THEN 1 ELSE 0 END),
			bitCleared=(CASE WHEN CAST(@vcText AS VARCHAR(5))='C' THEN 1 ELSE 0 END),numReconcileID=0
			WHERE numDomainId=@numDomainID AND numTransactionId=@numUpdateRecordID 
		END
		
		IF @tintMode = 29
		BEGIN
			UPDATE dbo.ReturnHeader SET numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 30
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
		IF @tintMode = 31
		BEGIN
			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numUpdateValueID WHERE numOppId = @numUpdateRecordID 
		END
		/*Commented by chintan.. this is moved to USP_ShoppingCart SP*/
--		IF @tintMode = 32
--		BEGIN
--			DECLARE @numBizDocId AS NUMERIC(9,0)
--			SELECT @numBizDocId = numBizDocId FROM eCommercePaymentConfig WHERE  numPaymentMethodId = @numUpdateValueId AND numDomainID = @numDomainID   
--			UPDATE dbo.OpportunityMaster SET numOppBizDocTempID = @numBizDocId WHERE numOppId = @numUpdateRecordID 
--		END
		
		IF @tintMode = 33
		BEGIN
			UPDATE dbo.ReturnHeader SET numBizdocTempID = @numUpdateValueID,numRMATempID = @numUpdateValueID WHERE numReturnHeaderID = @numUpdateRecordID 
		END
		
			
		IF @tintMode = 34
		BEGIN
			UPDATE dbo.WebAPIDetail SET vcFourteenthFldValue = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 35
		BEGIN
			UPDATE OpportunityMasterTaxItems SET fltPercentage = CAST(CAST(@vcText AS VARCHAR(10)) AS FLOAT)
			WHERE numOppId = @numUpdateRecordID AND numTaxItemID = 0
		END
		
		IF @tintMode = 36
		BEGIN
			UPDATE dbo.Import_File_Master SET numProcessedCSVRowNumber = @numUpdateValueID
			WHERE intImportFileID = @numUpdateRecordID AND numDomainID = @numDomainID
		END
		IF @tintMode = 37
		BEGIN
		     SELECT  @Status = ISNULL(numStatus ,0)  FROM dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
		     
		     IF @Status = 0
		      BEGIN
			    SELECT @SiteID  = CONVERT(NUMERIC(9,0),tintSource)   FROM	dbo.OpportunityMaster WHERE numOppId = @numUpdateRecordID
			    SELECT @OrderStatus = ISNULL(numFailedOrderStatus,0)    FROM dbo.eCommercePaymentConfig WHERE numSiteId = @SiteID AND numPaymentMethodId = 31488
		        
			    UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@OrderStatus,0) WHERE numOppId = @numUpdateRecordID AND numDomainId = @numDomainID 
			    
			  END   
		END
		IF @tintMode = 38
		BEGIN
			UPDATE [WebAPIDetail] SET    vcNinthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END
		
		IF @tintMode = 39
		BEGIN
			UPDATE [WebAPIDetail] SET    vcSixteenthFldValue = CAST(CAST(@vcText AS VARCHAR(30)) AS DATETIME)
			WHERE  [WebApiId] =  @numUpdateRecordID AND [numDomainId] = @numDomainId
		END

		IF @tintMode = 40
		BEGIN
			UPDATE dbo.WebAPIDetail SET bitEnableAPI = @numUpdateValueID WHERE WebApiId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 41
		BEGIN
			UPDATE dbo.domain SET numShippingServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 42
		BEGIN
			UPDATE dbo.domain SET numDiscountServiceItemID = @numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
		IF @tintMode = 43
		BEGIN
			UPDATE dbo.Cases SET numStatus = @numUpdateValueID WHERE numCaseId = @numUpdateRecordID AND numDomainId = @numDomainID
		END
		
		IF @tintMode = 44
		BEGIN
			UPDATE dbo.UserMaster SET numDefaultClass = @numUpdateValueID WHERE numUserId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 46
		BEGIN
			UPDATE dbo.Communication SET bitClosedFlag = @numUpdateValueID WHERE numCommId = @numUpdateRecordID AND numDomainId = @numDomainID
		END

		IF @tintMode = 47
		BEGIN
			UPDATE dbo.Domain SET bitCloneLocationWithItem = @numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 48
		BEGIN
			IF ISNULL(CAST(@vcText AS VARCHAR),'') = ''
			BEGIN
				DELETE FROM DomainMarketplace WHERE numDomainID=@numDomainID
			END
			ELSE
			BEGIN
				DELETE FROM DomainMarketplace WHERE numDomainID=@numDomainID AND numMarketplaceID NOT IN (SELECT Id FROM SplitIDs(@vcText,','))
				INSERT INTO DomainMarketplace (numDomainID,numMarketplaceID) SELECT @numDomainID,Id FROM SplitIDs(@vcText,',') WHERE Id NOT IN (SELECT numMarketplaceID FROM DomainMarketplace WHERE numDomainID=@numDomainID)
			END
		END

		IF @tintMode = 49
		BEGIN
			UPDATE dbo.OpportunityMaster SET intUsedShippingCompany=@numUpdateValueID,numShippingService=-1 WHERE numDomainId = @numDomainID AND numOppId=@numUpdateRecordID
		END

		IF @tintMode = 50
		BEGIN
			UPDATE dbo.StagePercentageDetailsTask SET vcTaskName=@vcText WHERE numDomainId = @numDomainID AND numTaskId=@numUpdateRecordID
		END

		IF @tintMode = 51
		BEGIN
			UPDATE dbo.Domain SET bitReceiveOrderWithNonMappedItem=CAST(@numUpdateValueID AS BIT) WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 52
		BEGIN
			UPDATE dbo.Domain SET numItemToUseForNonMappedItem=@numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 53
		BEGIN
			UPDATE dbo.Domain SET bitUsePredefinedCustomer=CAST(@numUpdateValueID AS BIT) WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 54
		BEGIN
			UPDATE dbo.Domain SET tintReorderPointBasedOn=@numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 55
		BEGIN
			UPDATE dbo.Domain SET intReorderPointDays=@numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 56
		BEGIN
			UPDATE dbo.Domain SET intReorderPointPercent=@numUpdateValueID WHERE numDomainId = @numDomainID
		END

		IF @tintMode = 57
		BEGIN
			UPDATE dbo.Domain SET bitPurchaseTaxCredit=@numUpdateValueID WHERE numDomainId = @numDomainID
		END
		
    SELECT @@ROWCOUNT
END
--Exec  USP_UpdateSingleFieldValue 26,52205,0,'',156
--UPDATE dbo.OpportunityMaster SET numStatus = 0 where numOppId = 52185
--SELECT numStatus FROM dbo.OpportunityMaster where numOppId = 52203
--SELECT * FROM dbo.OpportunityMaster 
--SELECT * FROM dbo.OpportunityBizDocs where numOppId = 52203