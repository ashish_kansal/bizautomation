GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetARContact')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetARContact
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetARContact]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT TOP 1 
		ACI.numContactId 
		,ACI.vcEmail
	FROM
		OpportunityMaster OM
	INNER JOIN
		Domain D
	ON
		OM.numDomainId = D.numDomainId
	INNER JOIN
		AdditionalContactsInformation ACI
	ON
		OM.numDivisionId = ACI.numDivisionId
	WHERE 
		OM.numDomainId = @numDomainID
		AND OM.numOppID = @numOppID		
		AND 1 = (CASE WHEN ISNULL(D.numARContactPosition,0) > 0 AND ACI.vcPosition=D.numARContactPosition THEN 1 ELSE 0 END)
END
GO