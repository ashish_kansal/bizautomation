GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageWebAPIOrderDetails' ) 
    DROP PROCEDURE USP_ManageWebAPIOrderDetails
GO



--created by Joseph
CREATE PROC [dbo].[USP_ManageWebAPIOrderDetails]
		@numWebApiOrdDetailId AS NUMERIC(9),
          @numDomainID AS NUMERIC(9),
          @vcWebApiOrderId AS VARCHAR(25),
          @tintStatus AS TINYINT,
          @numWebApiId AS NUMERIC(9),
          @numOppid AS NUMERIC(9) = 0

AS
  BEGIN
  DECLARE @Check as integer
  IF @numWebApiOrdDetailId =0
  BEGIN
    SELECT @Check = COUNT(*) FROM   [WebApiOrderDetails] WHERE  [numDomainId] = @numDomainID AND [WebApiId] = @numWebApiId AND vcWebApiOrderId = @vcWebApiOrderId AND tintStatus = 0
  	IF @Check =0    
	 BEGIN
  	 INSERT INTO [WebApiOrderDetails]
                   ([WebApiId],
                    [numDomainId],
                    [vcWebApiOrderId],
                    [tintStatus],
                    [dtCreated],
                    numOppId)
        VALUES     (@numWebApiId,
                    @numDomainID,
                    @vcWebApiOrderId,
                    @tintStatus,
                    GETDATE(),
                    0)
      
      END  
        END
  ELSE 
  IF @numWebApiOrdDetailId <> 0
  BEGIN
  		UPDATE [WebApiOrderDetails]
        SET    [tintStatus] = @tintStatus, numOppId = @numOppid,[dtModified] = GETDATE()
        WHERE  [numDomainId] = @numDomainID AND [vcWebApiOrderId] = @vcWebApiOrderId
				AND [WebApiId] = @numWebApiId
  END
  END








