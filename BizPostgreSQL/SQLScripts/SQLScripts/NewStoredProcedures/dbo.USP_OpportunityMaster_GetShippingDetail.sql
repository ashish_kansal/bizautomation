GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetShippingDetail')
DROP PROCEDURE USP_OpportunityMaster_GetShippingDetail
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetShippingDetail]
	@numDomainId NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numShippingReportID NUMERIC(18,0)
AS
BEGIN
	DECLARE @IsCOD BIT,@IsDryIce BIT,@IsHoldSaturday BIT,@IsHomeDelivery BIT,@IsInsideDelevery BIT,@IsInsidePickup BIT,@IsReturnShipment BIT,@IsSaturdayDelivery BIT,@IsSaturdayPickup BIT,@IsAdditionalHandling BIT,@IsLargePackage BIT
	DECLARE @vcCODType VARCHAR(50),@vcDeliveryConfirmation VARCHAR(1000),@vcDescription VARCHAR(MAX)
	DECLARE @tintSignatureType TINYINT
	DECLARE @numOrderShippingCompany NUMERIC(18,0)
	DECLARE @numOrderShippingService NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numDivisionShippingCompany NUMERIC(18,0)
    DECLARE @numDivisionShippingService NUMERIC(18,0)
	DECLARE @numShippingCompany NUMERIC(18,0)
	DECLARE @numShippingService NUMERIC(18,0)
	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState NUMERIC(18,0)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry NUMERIC(18,0)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState NUMERIC(18,0)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry NUMERIC(18,0)
    DECLARE @bitToResidential BIT = 0
	DECLARE @numOppBizDocID NUMERIC(18,0) = 0
	DECLARE @tintPayorType TINYINT
	DECLARE @vcPayorAccountNo VARCHAR(100)
	DECLARE @numPayorCountry NUMERIC(18,0)
	DECLARE @vcPayorZip VARCHAR(20)


	IF EXISTS (SELECT numShippingReportId FROM ShippingReport WHERE numDomainId = @numDomainId AND numShippingReportId = @numShippingReportID) 
	BEGIN
		SELECT 
			@numOppBizDocID=numOppBizDocId
			,@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelevery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@vcCODType = vcCODType
			,@vcDeliveryConfirmation = vcDeliveryConfirmation
			,@vcDescription = vcDescription
			,@tintSignatureType = ISNULL(tintSignatureType,0)
			,@vcFromName=vcFromName
			,@vcFromCompany=vcFromCompany
			,@vcFromPhone=vcFromPhone
			,@vcFromAddressLine1=vcFromAddressLine1
			,@vcFromCity=vcFromCity
			,@vcFromState=CAST(vcFromState AS NUMERIC)
			,@vcFromZip=vcFromZip
			,@vcFromCountry=CAST(vcFromCountry AS NUMERIC)
			,@bitFromResidential=bitFromResidential
			,@vcToName=vcToName
			,@vcToCompany=vcToCompany
			,@vcToPhone=vcToPhone
			,@vcToAddressLine1=vcToAddressLine1
			,@vcToCity=vcToCity
			,@vcToState=CAST(vcToState AS NUMERIC)
			,@vcToZip=vcToZip
			,@vcToCountry=CAST(vcToCountry AS NUMERIC)
			,@bitToResidential=bitToResidential
			,@vcCODType=vcCODType
			,@vcDeliveryConfirmation=vcDeliveryConfirmation
			,@vcDescription=vcDescription
			,@numShippingCompany=numShippingCompany
			,@numShippingService=vcValue2
			,@tintPayorType=ISNULL(tintPayorType,0)
			,@vcPayorAccountNo=ISNULL(vcPayorAccountNo,'')
			,@numPayorCountry=ISNULL(numPayorCountry,0)
			,@vcPayorZip=ISNULL(vcPayorAccountNo,'')
		FROM
			ShippingReport
		WHERE
			numDomainId = @numDomainId
			AND numShippingReportId = @numShippingReportID
	END
	ELSE
	BEGIN
		SELECT
			@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelevery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@vcCODType = vcCODType
			,@vcDeliveryConfirmation = vcDeliveryConfirmation
			,@vcDescription = vcDescription
			,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
			,@numOrderShippingCompany = ISNULL(intUsedShippingCompany,0)
			,@numOrderShippingService = ISNULL(numShippingService,0)
			,@numDivisionID=DivisionMaster.numDivisionID
			,@numDivisionShippingCompany = ISNULL(intShippingCompany,0)
			,@numDivisionShippingService = ISNULL(numDefaultShippingServiceID,0)
			,@tintPayorType=(CASE WHEN ISNULL(bitUseShippersAccountNo,0) = 1 THEN 2 ELSE 0 END)
		FROM
			OpportunityMaster
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		LEFT JOIN
			DivisionMasterShippingConfiguration
		ON
			DivisionMasterShippingConfiguration.numDomainID=@numDomainId
			AND DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
		WHERE
			OpportunityMaster.numDomainId = @numDomainId
			AND DivisionMaster.numDomainID=@numDomainId
			AND OpportunityMaster.numOppId = @numOppID

		-- GET FROM ADDRESS
		SELECT
			@vcFromName=vcName
			,@vcFromCompany=vcCompanyName
			,@vcFromPhone=vcPhone
			,@vcFromAddressLine1=vcStreet
			,@vcFromCity=vcCity
			,@vcFromState=vcState
			,@vcFromZip=vcZipCode
			,@vcFromCountry=vcCountry
			,@bitFromResidential=bitResidential
		FROM
			dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)

		-- GET TO ADDRESS
		SELECT
			@vcToName=vcName
			,@vcToCompany=vcCompanyName
			,@vcToPhone=vcPhone
			,@vcToAddressLine1=vcStreet
			,@vcToCity=vcCity
			,@vcToState=vcState
			,@vcToZip=vcZipCode
			,@vcToCountry=vcCountry
			,@bitToResidential=bitResidential
		FROM
			dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

		-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
		IF ISNULL(@numOrderShippingCompany,0) > 0
		BEGIN
			IF ISNULL(@numOrderShippingService,0) = 0
			BEGIN
				SET @numOrderShippingService = CASE @numOrderShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
			END

			SET @numShippingCompany = @numOrderShippingCompany
			SET @numShippingService = @numOrderShippingService
		END
		ELSE IF ISNULL(@numDivisionShippingCompany,0) > 0 -- IF DIVISION SHIPPIGN SETTING AVAILABLE
		BEGIN
			IF ISNULL(@numDivisionShippingService,0) = 0
			BEGIN
				SET @numDivisionShippingService = CASE @numDivisionShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
			END

			SET @numShippingCompany = @numDivisionShippingCompany
			SET @numShippingService = @numDivisionShippingService
		END
		ELSE -- USE DOMAIN DEFAULT
		BEGIN
			SELECT @numShippingCompany=ISNULL(numShipCompany,0) FROM Domain WHERE numDomainId=@numDomainId
		
			IF @numShippingCompany <> 91 OR @numShippingCompany <> 88 OR @numShippingCompany <> 90
			BEGIN
				SET @numShippingCompany = 91
			END

			SET @numShippingService = CASE @numShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
		END

		IF @tintPayorType = 2 AND EXISTS (SELECT ID FROM DivisionMasterShippingAccount WHERE numDivisionID=@numDivisionID AND numShipViaID=@numShippingCompany)
		BEGIN
			SELECT
				@vcPayorAccountNo=ISNULL(vcAccountNumber,'')
				,@numPayorCountry=numCountry
				,@vcPayorZip=vcZipCode
			FROM
				DivisionMasterShippingAccount
			WHERE
				numDivisionID=@numDivisionID 
				AND numShipViaID=@numShippingCompany
		END
	END

	SELECT
		@numOppID numOppID
		,ISNULL(@numOppBizDocID,0) numOppBizDocID
		,@numShippingCompany numShippingCompany
		,@numShippingService numShippingService
		,@vcFromName vcFromName
		,@vcFromCompany vcFromCompanyName
		,@vcFromPhone vcFromPhone
		,@vcFromAddressLine1 vcFromStreet
		,@vcFromCity vcFromCity
		,@vcFromState numFromState
		,@vcFromZip vcFromZipCode
		,@vcFromCountry numFromCountry
		,@bitFromResidential bitFromResidential
		,@vcToName vcToName
		,@vcToCompany vcToCompanyName
		,@vcToPhone vcToPhone
		,@vcToAddressLine1 vcToStreet
		,@vcToCity vcToCity
		,@vcToState numToState
		,@vcToZip vcToZipCode
		,@vcToCountry numToCountry
		,@bitToResidential bitToResidential
		,@IsCOD bitCOD
		,@IsDryIce bitDryIce
		,@IsHoldSaturday bitHoldSaturday
		,@IsHomeDelivery bitHomeDelivery
		,@IsInsideDelevery bitInsideDelevery
		,@IsInsidePickup bitIsInsidePickup
		,@IsReturnShipment bitReturnShipment
		,@IsSaturdayDelivery bitSaturdayDelivery
		,@IsSaturdayPickup bitIsSaturdayPickup
		,@IsAdditionalHandling bitAdditionalHandling
		,@IsLargePackage bitLargePackage
		,@vcCODType vcCODType
		,@vcDeliveryConfirmation vcDeliveryConfirmation
		,@vcDescription vcDescription
		,@tintSignatureType tintSignatureType
		,@tintPayorType tintPayorType
		,@vcPayorAccountNo vcPayorAccountNo
		,@numPayorCountry numPayorCountry
		,@vcPayorZip vcPayorZip
END
GO