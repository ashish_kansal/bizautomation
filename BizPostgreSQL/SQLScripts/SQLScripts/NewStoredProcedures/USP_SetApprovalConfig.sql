IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SetApprovalConfig')
DROP PROCEDURE USP_SetApprovalConfig
GO
CREATE PROCEDURE [dbo].[USP_SetApprovalConfig]          
  @chrAction char(10)=NULL,
  @numDomainId Numeric(18,2)=0,
  @numUserId Numeric(18,2)=0,
  @numModuleId Numeric(18,2)=0,
  @numConfigId Numeric(18,2)=0,
  @vchXML XML=null   
AS                                          
BEGIN
	IF(@chrAction='A')
	BEGIN
		DECLARE @COUNT INT =0
		DECLARE @INT_COUNTER INT= 0
		DECLARE @numLevel1Authority NUMERIC
		DECLARE @numLevel2Authority NUMERIC
		DECLARE @numLevel3Authority NUMERIC
		DECLARE @numLevel4Authority NUMERIC
		DECLARE @numLevel5Authority NUMERIC
		SELECT                                          
			cast(colx.query('data(numUserId)') as varchar(300)) as numUserId,                                      
			cast(colx.query('data(numLevel1Authority)') as varchar(300)) as numLevel1Authority,                                      
			cast(colx.query('data(numLevel2Authority)') as varchar(300)) as numLevel2Authority,                                      
			cast(colx.query('data(numLevel3Authority)') as varchar(300)) as numLevel3Authority,                                     
			cast(colx.query('data(numLevel4Authority)') as varchar(300)) as numLevel4Authority,                                     
			cast(colx.query('data(numLevel5Authority)') as varchar(300)) as numLevel5Authority                                     
		INTO	
			#TMP_APPROVAL 
		FROM 
			@vchXML.nodes('NewDataSet/ApprovalConfig') AS Tabx(colx)

		ALTER TABLE #TMP_APPROVAL ADD ID INT IDENTITY(1,1)
		SELECT * FROM  #TMP_APPROVAL
		SET @INT_COUNTER=1
		SELECT @COUNT=COUNT(*) FROM #TMP_APPROVAL

		WHILE @INT_COUNTER<=@COUNT
		BEGIN
			IF(@numDomainId>0)
			BEGIN
			SELECT 
				@numUserId=numUserId,@numLevel1Authority=numLevel1Authority,
				@numLevel2Authority=numLevel2Authority,@numLevel3Authority=numLevel3Authority,
				@numLevel4Authority=numLevel4Authority,@numLevel5Authority=numLevel5Authority
			FROM 
				#TMP_APPROVAL 
			WHERE 
				ID=@INT_COUNTER

			IF((SELECT COUNT(*) FROM ApprovalConfig WHERE numUserId=@numUserId AND numModule=@numModuleId AND numDomainID=@numDomainId)>0)
				BEGIN
					SET @numConfigId=(SELECT TOP 1 numConfigID FROM ApprovalConfig WHERE numUserId=@numUserId AND numModule=@numModuleId AND numDomainID=@numDomainId)
					UPDATE
						ApprovalConfig
					SET
						numLevel1Authority=@numLevel1Authority,
						numLevel2Authority=@numLevel2Authority,
						numLevel3Authority=@numLevel3Authority,
						numLevel4Authority=@numLevel4Authority,
						numLevel5Authority=@numLevel5Authority
					WHERE
						 numUserId=@numUserId AND numModule=@numModuleId AND numDomainID=@numDomainId
				END
			ELSE
				BEGIN
					INSERT INTO 
						ApprovalConfig
						(
							numDomainID,numModule,numUserId,numLevel1Authority,
							numLevel2Authority,numLevel3Authority,numLevel4Authority,numLevel5Authority
						)
					VALUES
						(
							@numDomainId,@numModuleId,@numUserId,@numLevel1Authority,
							@numLevel2Authority,@numLevel3Authority,@numLevel4Authority,@numLevel5Authority
						)
				END

			SET @INT_COUNTER=@INT_COUNTER+1
			END
		END
	END
END