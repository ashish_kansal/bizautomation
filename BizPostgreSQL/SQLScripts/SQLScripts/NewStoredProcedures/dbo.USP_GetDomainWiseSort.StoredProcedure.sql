  
  
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Wednesday, September 03, 2008>  
-- Description: <This procedure is used for fetching the record against DomainID from DomainWiseSort Table.>  
-- =============================================  

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDomainWiseSort')
DROP PROCEDURE USP_GetDomainWiseSort
GO
CREATE PROCEDURE [dbo].[USP_GetDomainWiseSort]   
@numDomainId as numeric(9),        
@vcModuleType as varchar(50)
as              
Begin            
	select vcSortOn from DomainWiseSort where vcModuleType=@vcModuleType and numDomainId=@numDomainId;
End  
go