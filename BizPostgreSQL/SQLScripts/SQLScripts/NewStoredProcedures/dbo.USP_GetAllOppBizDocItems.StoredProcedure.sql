
GO
/****** Object:  StoredProcedure [dbo].[USP_GetAllOppBizDocItems] ******/
--created by Joseph
--Used To get few Item Details which is used to created WebAPI Order Item Details

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAllOppBizDocItems')
DROP PROCEDURE USP_GetAllOppBizDocItems
GO

CREATE PROCEDURE [dbo].[USP_GetAllOppBizDocItems]                                                                                                                                                                                
(                                                                                                                                                                                                                                                              
 @numOppId as numeric(9)=null,                                                                                                                                                                                
 @numOppBizDocsId as numeric(9)=null,                                                                                                
 @numDomainID as numeric(9)=0      
)                                                                                                                                                                                
                                                                                                                                                                            
as       
 

SELECT OBI.numOppBizDocID, OBI.numItemCode,IT.vcSKU,OBI.numOppBizDocItemID, OMAPI.vcAPIOppId,OBI.monShipCost,
OBI.vcTrackingNo,OBI.vcShippingMethod,OBI.vcNotes FROM  dbo.OpportunityBizDocItems OBI 
INNER JOIN dbo.OpportunityBizDocs OB ON OBI.numOppBizDocID = OB.numOppBizDocsId
INNER JOIN dbo.OpportunityMaster OM ON OB.numOppId = OM.numOppId 
INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
INNER JOIN dbo.Item IT ON OBI.numItemCode = IT.numItemCode
WHERE OB.numOppId = @numOppId AND OB.numOppBizDocsId = @numOppBizDocsId
        