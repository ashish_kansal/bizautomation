SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOffer')
DROP PROCEDURE USP_GetPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @numDomainID AS NUMERIC(9) = 0,
	@ClientTimeZoneOffset AS INT
AS 
BEGIN
    IF @byteMode = 0 
    BEGIN    
		SELECT
			[numProId]
			,[vcProName]
			,[numDomainId]
			,(CASE WHEN [dtValidFrom] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidFrom) ELSE dtValidFrom END) dtValidFrom
			,(CASE WHEN [dtValidTo] IS NOT NULL AND ISNULL(bitNeverExpires,0) = 0 THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,dtValidTo) ELSE dtValidTo END) dtValidTo
			,[bitNeverExpires]
			,[tintOfferTriggerValueType]
			,[fltOfferTriggerValue]
			,[tintOfferBasedOn]
			,[fltDiscountValue]
			,[tintDiscountType]
			,[tintDiscoutBaseOn]
			,[numCreatedBy]
			,[dtCreated]
			,[numModifiedBy]
			,[dtModified]
			,[tintCustomersBasedOn]
			,[tintOfferTriggerValueTypeRange]
			,[fltOfferTriggerValueRange]
			,[IsOrderBasedPromotion]
			,[vcShortDesc]
			,[vcLongDesc]
			,[tintItemCalDiscount]
			,ISNULL(bitUseOrderPromotion,0) bitUseOrderPromotion
			,ISNULL(numOrderPromotionID,0) numOrderPromotionID
			,ISNULL(monDiscountedItemPrice,0) monDiscountedItemPrice
        FROM 
			PromotionOffer
        WHERE 
			numProId = @numProId AND ISNULL(IsOrderBasedPromotion,0) = 0
    END  
	ELSE IF @byteMode = 1
    BEGIN  
	   
        SELECT  
			PO.numProId,
            PO.vcProName,
			(CASE WHEN ISNULL(PO.IsOrderBasedPromotion,0) = 1 THEN 'Order based' ELSE 'Item based' END) vcPromotionType,
			(CASE 
				WHEN ISNULL(PO.numOrderPromotionID,0) > 0
				THEN
					(CASE WHEN ISNULL(POOrder.bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate((CASE WHEN POOrder.[dtValidFrom] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,POOrder.dtValidFrom) ELSE POOrder.dtValidFrom END),@numDomainID),' to ',dbo.FormatedDateFromDate((CASE WHEN POOrder.[dtValidTo] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,POOrder.dtValidTo) ELSE POOrder.dtValidTo END),@numDomainID)) END)
				ELSE
					(CASE WHEN ISNULL(PO.bitNeverExpires,0) = 1 THEN 'Promotion Never Expires' ELSE CONCAT(dbo.FormatedDateFromDate((CASE WHEN PO.[dtValidFrom] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,PO.dtValidFrom) ELSE PO.dtValidFrom END),@numDomainID),' to ',dbo.FormatedDateFromDate((CASE WHEN PO.[dtValidTo] IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,PO.dtValidTo) ELSE PO.dtValidTo END),@numDomainID)) END)
			END) AS vcDateValidation
			,(CASE 
				WHEN ISNULL(PO.bitRequireCouponCode,0) = 1 OR ISNULL(PO.numOrderPromotionID,0) > 0 THEN
			       STUFF((SELECT CONCAT (', ' , vcDiscountCode , ' (', ISNULL(SUM(DCU.intCodeUsed),0),')')
									   FROM DiscountCodes  D
									   LEFT JOIN DiscountCodeUsage DCU ON D.numDiscountId = DCU.numDiscountId
									   WHERE D.numPromotionID = (CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN PO.numOrderPromotionID ELSE PO.numProId END) GROUP BY D.vcDiscountCode, DCU.numDiscountId
									   FOR XML PATH ('')), 1, 1, '')
				ELSE '' 
			END) AS vcCouponCode
			,(CASE WHEN ISNULL(PO.bitApplyToInternalOrders,0) = 1 THEN 'Yes' ELSE 'No' END) AS vcInternalOrders
			,(CASE WHEN ISNULL(PO.bitAppliesToSite,0)=1 THEN (SELECT Stuff((SELECT CONCAT(', ',Sites.vcSiteName) FROM PromotionOfferSites POS INNER JOIN Sites ON pos.numSiteID=Sites.numSiteID WHERE POS.numPromotionID=PO.numProId FOR XML PATH('')), 1, 2, '')) ELSE '' END) AS vcSites

			,CASE WHEN ISNULL(PO.IsOrderBasedPromotion,0) = 1 THEN
					(CASE WHEN ISNULL(PO.bitUseForCouponManagement,0) = 0 THEN CONCAT('Buy ', 
															CASE PO.tintDiscountType WHEN 1 THEN
																		STUFF((SELECT CONCAT (', ' , '$', numOrderAmount , ' & get ', ISNULL(fltDiscountValue,0),' % off')
																		   FROM PromotionOfferOrder POO					   
																		   WHERE POO.numPromotionID =  PO.numProId
																		   FOR XML PATH ('')), 1, 1, '')  
																	WHEN 2 THEN 
																		STUFF((SELECT CONCAT (', ','$', numOrderAmount , ' & get $', ISNULL(fltDiscountValue,0),' off')
																				   FROM PromotionOfferOrder POO					   
																				   WHERE POO.numPromotionID =  PO.numProId
																				   FOR XML PATH ('')), 1, 1, '')
															ELSE '' END) ELSE '' END)
			ELSE
			CONCAT('Buy '
					,CASE WHEN PO.tintOfferTriggerValueType=1 THEN CAST(PO.fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',PO.fltOfferTriggerValue) END
					,CASE PO.tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE  
						WHEN (PO.tintDiscountType = 1 AND PO.tintDiscoutBaseOn <> 4) THEN 
							CONCAT(PO.fltDiscountValue,'% off on') 
						WHEN (PO.tintDiscountType = 2 AND PO.tintDiscoutBaseOn <> 4) THEN 
							CONCAT('$',PO.fltDiscountValue, ' off on')
						WHEN (PO.tintDiscountType = 3 AND PO.tintDiscoutBaseOn <> 4) THEN 
						CONCAT(PO.fltDiscountValue,' of') END
					, CASE 
						WHEN PO.tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN PO.tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN PO.tintDiscoutBaseOn = 3 THEN ' related items.'
						WHEN PO.tintDiscoutBaseOn = 4 THEN CONCAT(PO.fltDiscountValue,' quantity free for any item of equal or lesser value')
						WHEN PO.tintDiscoutBaseOn = 5 THEN CONCAT(PO.fltDiscountValue,' quantity for any item of equal or lesser value at $',ISNULL(PO.monDiscountedItemPrice,0))
						WHEN PO.tintDiscoutBaseOn = 6 THEN CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=4 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN CONCAT(' at $',ISNULL(PO.monDiscountedItemPrice,0)) ELSE '' END)
								,'.'
							)
						ELSE '' 
					END)
				 END AS vcPromotionRule,
				(CASE WHEN (SELECT COUNT(*) FROM OpportunityItems JOIN OpportunityMaster ON OpportunityItems.numOppId=OpportunityMaster.numOppId WHERE numDomainId=@numDomainID AND OpportunityItems.numPromotionID=PO.numProId) > 0 THEN 0 ELSE 1 END) AS bitCanEdit
				,PO.bitEnabled
				, PO.tintCustomersBasedOn
				, PO.tintOfferTriggerValueTypeRange
				, PO.fltOfferTriggerValueRange
				, PO.[IsOrderBasedPromotion]
				, ISNULL(PO.monDiscountedItemPrice,0) monDiscountedItemPrice
        FROM  
			PromotionOffer PO
		LEFT JOIN
			PromotionOffer POOrder
		ON
			PO.numOrderPromotionID = POOrder.numProId
		WHERE 
			PO.numDomainID = @numDomainID
		ORDER BY
			PO.dtCreated DESC
    END
	  
   	
END      
  
GO
