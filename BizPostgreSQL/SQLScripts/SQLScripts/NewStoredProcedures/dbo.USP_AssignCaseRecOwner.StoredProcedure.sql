/****** Object:  StoredProcedure [dbo].[USP_AssignCaseRecOwner]    Script Date: 07/26/2008 16:21:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AssignCaseRecOwner')
DROP PROCEDURE USP_AssignCaseRecOwner
GO
CREATE PROCEDURE [dbo].[USP_AssignCaseRecOwner]   
@numRecOwner as numeric(9)=0,
@numAssignedTo as numeric(9)=0,  
@numCaseId as numeric(9)=0,
@numDomainID as numeric(9)=0  
as  
	 update [Cases] set [numRecOwner]=@numRecOwner,
	 [numAssignedBy]=@numRecOwner,
	 [numAssignedTo]=@numAssignedTo where [numCaseId]=@numCaseId  and numDomainID=@numDomainID
GO
