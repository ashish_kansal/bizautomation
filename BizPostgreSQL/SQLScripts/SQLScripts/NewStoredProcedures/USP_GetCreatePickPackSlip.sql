--created by Prasanta

-- exec USP_GetOppItemBizDocs @numBizDocID=644,@numDomainID=169,@numOppID=476291,@numOppBizDocID=0,@bitRentalBizDoc=0
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCreatePickPackSlip' ) 
    DROP PROCEDURE USP_GetCreatePickPackSlip
GO
CREATE PROCEDURE USP_GetCreatePickPackSlip
    @numBizDocTypeID AS NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
	@numWarehouseID AS NUMERIC(18,0),
	@numShipToAddressID AS NUMERIC(18,0),
	@bitFirstIteration BIT
AS 
BEGIN
	SELECT 
		O.numOppBizDocsId,
		O.vcBizDocID+'      '+B.vcTemplateName AS vcBizDocName
	FROM 
		OpportunityBizDocs AS O
	LEFT JOIN
		BizDocTemplate AS B
	ON
		O.numBizDocTempID=B.numBizDocTempID
	WHERE
		O.numOppId=@numOppID 
		AND 1 = (CASE 
					WHEN @numBizDocTypeID=296 THEN (CASE WHEN O.numBizDocId IN (296,29397) THEN 1 ELSE 0 END) 
					ELSE (CASE WHEN O.numBizDocId=@numBizDocTypeID THEN 1 ELSE 0 END) 
				END)
		AND 1 = (CASE 
					WHEN (SELECT 
							COUNT(*) 
						FROM 
							OpportunityBizDocItems 
						WHERE 
							numOppBizDocID=O.numOppBizDocsId) = (SELECT 
																	COUNT(*) 
																FROM 
																	OpportunityBizDocItems OBDI 
																INNER JOIN 
																	OpportunityItems OI 
																ON 
																	OBDI.numOppItemID = OI.numoppitemtCode
																LEFT JOIN
																	WareHouseItems WI
																ON
																	OI.numWarehouseItmsID = WI.numWareHouseItemID
																WHERE 
																	numOppBizDocID=O.numOppBizDocsId 
																	AND (((ISNULL(WI.numWareHouseID,0)=@numWarehouseID OR (ISNULL(WI.numWareHouseID,0)=0 AND @numShipToAddressID > 0 AND ISNULL(OI.numShipToAddressID,0) = @numShipToAddressID)) AND (ISNULL(OI.numShipToAddressID,0) = @numShipToAddressID OR ISNULL(OI.numShipToAddressID,0) =0)) OR (@bitFirstIteration=1 AND ISNULL(OI.numWarehouseItmsID,0)=0 AND ISNULL(OI.numShipToAddressID,0) = 0)))
					THEN 1 
					ELSE 0 
				END)
END

