GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageDefaultAccountsForDomain')
DROP PROCEDURE USP_ManageDefaultAccountsForDomain
GO
CREATE PROCEDURE USP_ManageDefaultAccountsForDomain
    @numDomainID NUMERIC(9),
    @str AS VARCHAR(8000)
AS 
    BEGIN
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(10), @str) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @str
            
                DELETE  FROM [AccountingCharges]
                WHERE   numDomainID = @numDomainID
            
                INSERT  INTO [AccountingCharges]
                        (
                          [numChargeTypeId],
                          [numAccountID],
                          [numDomainID]
                        )
                        SELECT  
								ISNULL((SELECT numChargeTypeId FROM [AccountingChargeTypes] WHERE chChargeCode= X.chChargeCode),0) AS numChargeTypeId,
                                X.numAccountID,
                                @numDomainID
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                            WITH ( chChargeCode CHAR(2), numAccountID NUMERIC(9) )
                                ) X
                                
                EXEC sp_xml_removedocument @hDocItem
            END
END

