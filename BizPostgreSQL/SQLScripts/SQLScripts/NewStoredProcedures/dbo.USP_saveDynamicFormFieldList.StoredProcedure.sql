SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_saveDynamicFormFieldList')
DROP PROCEDURE USP_saveDynamicFormFieldList
GO
CREATE PROCEDURE [dbo].[USP_saveDynamicFormFieldList]    
@numDomainID as NUMERIC(9)=0,    
@numFormId as NUMERIC(9)=0,  
@numFormFieldId as NUMERIC(9)=0,  
@vcFormFieldName as NVARCHAR(50)=0,
@vcToolTip AS VARCHAR(1000) 
as    
	
   	if exists(select * from DynamicFormField_Validation where numDomainID=@numDomainID and numFormId=@numFormId and numFormFieldId=@numFormFieldId)
	   begin
          UPDATE DynamicFormField_Validation SET vcNewFormFieldName=@vcFormFieldName,vcToolTip=@vcToolTip 
				where numDomainID=@numDomainID and numFormId=@numFormId and numFormFieldId=@numFormFieldId
        END    
    ELSE
    begin
        insert into DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,vcToolTip)
			 	values(@numFormId,@numFormFieldId,@numDomainId,@vcFormFieldName,@vcToolTip)
    END
    
GO
