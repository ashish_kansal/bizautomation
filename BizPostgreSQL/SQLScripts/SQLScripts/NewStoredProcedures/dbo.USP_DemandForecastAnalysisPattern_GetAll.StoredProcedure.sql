SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecastAnalysisPattern_GetAll')
DROP PROCEDURE dbo.USP_DemandForecastAnalysisPattern_GetAll
GO
CREATE PROCEDURE [dbo].[USP_DemandForecastAnalysisPattern_GetAll]
	@numDomainID NUMERIC (18,0)
AS 
BEGIN
    IF EXISTS (SELECT numDFAPID FROM DemandForecastAnalysisPattern WHERE ISNULL(numDomainID,0) = @numDomainID) 
		SELECT * FROM DemandForecastAnalysisPattern WHERE ISNULL(numDomainID,0) = @numDomainID
	ELSE
		SELECT * FROM DemandForecastAnalysisPattern WHERE ISNULL(numDomainID,0) = 0 
END
