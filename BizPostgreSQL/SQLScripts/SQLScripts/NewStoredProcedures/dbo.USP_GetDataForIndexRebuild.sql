SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDataForIndexRebuild')
DROP PROCEDURE USP_GetDataForIndexRebuild
GO
CREATE PROCEDURE [dbo].[USP_GetDataForIndexRebuild]  
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numCurrentPage INT = 1
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @TotalRecords INT
	DECLARE @BatchSize INT = 500

	IF @numCurrentPage = 1
	BEGIN
		DELETE FROM LuceneItemsIndex WHERE numDomainID = @numDomainID AND numSiteID = @numSiteID
	END

	SELECT  
		ROW_NUMBER() OVER (ORDER BY vcFormFieldName) AS tintOrder
		,*
	INTO    
		#tempCustomField
	FROM    
	( 
		SELECT 
			DISTINCT numFieldID as numFormFieldId
			,vcDbColumnName
			,vcFieldName as vcFormFieldName
			,0 AS Custom
			,vcLookBackTableName
		FROM 
			View_DynamicColumns
		WHERE 
			numFormId = 30
			AND numDomainID = @numDomainID
			AND ISNULL(numRelCntType,0) = 0
			AND ISNULL(bitCustom,0) = 0
			AND ISNULL(bitSettingField, 0) = 1
		UNION
		SELECT 
			numFieldID as numFormFieldId
			,vcFieldName
			,vcFieldName
			,1 AS Custom
			,'' vcLookBackTableName
		FROM 
			View_DynamicCustomColumns
		WHERE 
			Grp_id = 5
			AND numFormId = 30
			AND numDomainID = @numDomainID
			AND ISNULL(numRelCntType,0) = 0
	) X

	DECLARE @Fields VARCHAR(1000)
    DECLARE @numFldID VARCHAR(15)
    DECLARE @vcFldname VARCHAR(200)
    DECLARE @vcDBFldname VARCHAR(200)
    DECLARE @vcLookBackTableName VARCHAR(200)
    DECLARE @intRowNum INT
    DECLARE @strSQLUpdate VARCHAR(4000)
    DECLARE @Custom AS BIT
	SET @Fields = '';
    SELECT TOP 1  
		@intRowNum = (tintOrder +1)
		,@numFldID=numFormFieldId
		,@vcFldname=vcFormFieldName
		,@vcDBFldName=vcDbColumnName
		,@vcLookBackTableName=vcLookBackTableName
	FROM 
		#tempCustomField 
	WHERE 
		Custom=0 
	ORDER BY 
		tintOrder

	WHILE @intRowNum > 0                                                                                  
	BEGIN
		IF(@vcDBFldName='numItemCode')
			SET @vcDBFldName ='I.numItemCode'
				 
		SET @Fields = @Fields + ','  + @vcDBFldName + ' as ['  + @vcFldname + ']' 
					 

		SELECT TOP 1 
			@intRowNum = (tintOrder +1)
			,@numFldID=numFormFieldId
			,@vcFldname=vcFormFieldName
			,@vcDBFldName=vcDbColumnName
			,@vcLookBackTableName=vcLookBackTableName
		FROM 
			#tempCustomField 
		WHERE 
			tintOrder >= @intRowNum 
			AND Custom=0 
		ORDER BY tintOrder
							           
		IF @@rowcount=0 SET @intRowNum=0                                                                                          
	END

	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		dbo.Item I 
    LEFT JOIN 
		dbo.ItemExtendedDetails IED
    ON 
		I.numItemCode = IED.numItemCode
    WHERE 
		I.numDomainID = @numDomainID
        AND I.numItemCode IN (SELECT numItemID FROM dbo.ItemCategory WHERE ISNULL(I.IsArchieve,0) = 0 AND numCategoryID IN (SELECT numCategoryID FROM dbo.SiteCategories WHERE numSiteID=@numSiteID))

	DECLARE @strSQL VARCHAR(MAX)
	SET @strSQL =  CONCAT('SELECT  
								I.numItemCode AS numItemCode'
								,@Fields,'            
							INTO 
								#DataForIndexing
							FROM 
								dbo.Item I 
							LEFT JOIN 
								dbo.ItemExtendedDetails IED
							ON 
								I.numItemCode = IED.numItemCode
							WHERE 
								I.numDomainID =',@numDomainID,'
								AND I.numItemCode IN (SELECT numItemID FROM dbo.ItemCategory WHERE ISNULL(I.IsArchieve,0) = 0 AND numCategoryID IN (SELECT numCategoryID FROM dbo.SiteCategories WHERE numSiteID = ',@numSiteID,'))
							ORDER BY 
								I.numItemCode
							OFFSET ', (@numCurrentPage-1) * @BatchSize,' ROWS
							FETCH NEXT ',@BatchSize,' ROWS ONLY; ')

	SET @strSQLUpdate=''

	SELECT TOP 1 
		@intRowNum = (tintOrder +1)
		,@numFldID=numFormFieldId
		,@vcFldname=vcFormFieldName
	FROM 
		#tempCustomField 
	WHERE 
		Custom=1 
	ORDER BY 
		tintOrder
				
	WHILE @intRowNum > 0                                                                                  
	BEGIN
		SET @strSQLUpdate = @strSQLUpdate + 'alter table #DataForIndexing add ['+@vcFldname+'_CustomField] varchar(500);'
		set @strSQLUpdate=@strSQLUpdate+ 'update #DataForIndexing set [' +@vcFldname + '_CustomField]=dbo.GetCustFldValueItem('+@numFldID+',numItemCode) where numItemCode>0'
							
		SELECT TOP 1 @intRowNum = (tintOrder +1), @numFldID=numFormFieldId,@vcFldname=vcFormFieldName
		FROM #tempCustomField WHERE tintOrder >=@intRowNum AND Custom=1 ORDER BY tintOrder
							           
		IF @@rowcount=0 SET @intRowNum=0
	END

	SET @strSQLUpdate = @strSQLUpdate + CONCAT('SELECT ',@BatchSize,' AS BatchSize,',@TotalRecords,' AS TotalRecords; SELECT * FROM #DataForIndexing')
				 
				 
	SET @strSQLUpdate = @strSQLUpdate + ' INSERT INTO LuceneItemsIndex SELECT numItemCode,1,0,'+ CONVERT(VARCHAR(10), @numDomainID) +','+ CONVERT(VARCHAR(10), @numSiteID) +'  FROM #DataForIndexing where numItemCode not in (select numItemCode from LuceneItemsIndex where numDomainID ='+ CONVERT(VARCHAR(10), @numDomainID) +' and numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ' )'
				 
				 
	SET @strSQLUpdate = @strSQLUpdate + ' DROP TABLE #DataForIndexing; DROP TABLE #tempCustomField;'
				 
	SET @strSQL =@strSQL + @strSQLUpdate;
				 
				 
	PRINT @strSQL
	EXEC (@strSQL)
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END