GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetUsersForReport' ) 
    DROP PROCEDURE USP_GetUsersForReport
GO
CREATE PROCEDURE [dbo].[USP_GetUsersForReport]
    @numUserCntID AS NUMERIC(9),
    @numDomainId AS NUMERIC(9),
    @tintType AS TINYINT
AS 

IF @tintType=1
BEGIN
SELECT distinct numSelectedUserCntID,
        ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') AS vcUserName,
        tintUserType
FROM    ForReportsByUser F
        INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = F.numSelectedUserCntID
		join UserMaster UM on ACI.numContactID=UM.numUserDetailID
		LEFT OUTER JOIN [UserTeams] UT ON UT.[numUserCntID] = UM.[numUserDetailId]
WHERE   F.numContactID = @numUserCntID
        AND F.numDomainId = @numDomainID
        AND tintType = @tintType and UT.numTeam in (SELECT    numTeam
                                  FROM      UserTeams
                                  WHERE     numUserCntID = @numUserCntID
                                            AND numDomainID = @numDomainID)
END
ELSE
BEGIN
SELECT  numSelectedUserCntID,
        ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') AS vcUserName,
        tintUserType
FROM    ForReportsByUser F
        INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = F.numSelectedUserCntID
WHERE   F.numContactID = @numUserCntID
        AND F.numDomainId = @numDomainID
        AND tintType = @tintType
END