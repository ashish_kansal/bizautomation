GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageBillHeader' ) 
    DROP PROCEDURE USP_ManageBillHeader
GO

CREATE PROCEDURE USP_ManageBillHeader
    @numBillID [numeric](18, 0) OUTPUT,
    @numDivisionID [numeric](18, 0),
    @numAccountID [numeric](18, 0),
    @dtBillDate [datetime],
    @numTermsID [numeric](18, 0),
    @numDueAmount [numeric](18, 2),
    @dtDueDate [datetime],
    @vcMemo [varchar](1000),
    @vcReference [varchar](500),
    @numBillTotalAmount [numeric](18, 2),
    @bitIsPaid [bit],
    @numDomainID [numeric](18, 0),
    @strItems VARCHAR(MAX),
    @numUserCntID NUMERIC(18, 0),
	@numOppId NUMERIC(18,0)=0,
	@bitLandedCost BIT=0,
	@numCurrencyID NUMERIC(18,0) = 0
AS 
    SET NOCOUNT ON  
    BEGIN TRY 
        BEGIN TRAN  
        --Validation of closed financial year
		EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtBillDate

		DECLARE @fltExchangeRate AS FLOAT

		IF ISNULL(@numCurrencyID,0) = 0
		BEGIN
			SET @fltExchangeRate=1
			SELECT @numCurrencyID=numCurrencyID FROM Domain WHERE numDomainId = @numDomainID
		END
		ELSE
		BEGIN 
			SET @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
        END  
        
        IF EXISTS ( SELECT  [numBillID]
                    FROM    [dbo].[BillHeader]
                    WHERE   [numBillID] = @numBillID ) 
            BEGIN
                UPDATE  [dbo].[BillHeader]
                SET     numDivisionID = @numDivisionID,
                        numAccountID = @numAccountID,
                        dtBillDate = @dtBillDate,
                        numTermsID = @numTermsID,
                        monAmountDue = @numDueAmount,
                        dtDueDate = @dtDueDate,
                        vcMemo = @vcMemo,
                        vcReference = @vcReference,
                        --monAmtPaid = @numBillTotalAmount,
                        bitIsPaid = (CASE WHEN ISNULL(monAmtPaid,0)=@numDueAmount THEN 1
									ELSE 0 END),
                        numDomainID = @numDomainID,
                        dtModifiedDate = GETUTCDATE(),
                        numModifiedBy = @numUserCntID,
						numCurrencyID=@numCurrencyID,
						fltExchangeRate = @fltExchangeRate
                WHERE   [numBillID] = @numBillID
                        AND numDomainID = @numDomainID
            END
        ELSE 
            BEGIN

				-- GET ACCOUNT CLASS IF ENABLED
				DECLARE @numAccountClass AS NUMERIC(18) = 0

				DECLARE @tintDefaultClassType AS INT = 0
				SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

				IF @tintDefaultClassType = 1 --USER
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numDefaultClass,0) 
					FROM 
						dbo.UserMaster UM 
					JOIN 
						dbo.Domain D 
					ON 
						UM.numDomainID=D.numDomainId
					WHERE 
						D.numDomainId=@numDomainId 
						AND UM.numUserDetailId=@numUserCntID
				END
				ELSE IF @tintDefaultClassType = 2 --COMPANY
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numAccountClassID,0) 
					FROM 
						dbo.DivisionMaster DM 
					WHERE 
						DM.numDomainId=@numDomainId 
						AND DM.numDivisionID=@numDivisionID
				END
				ELSE
				BEGIN
					SET @numAccountClass = 0
				END
            
                INSERT  INTO [dbo].[BillHeader]
                        (
                          [numDivisionID],
                          [numAccountID],
                          [dtBillDate],
                          [numTermsID],
                          [monAmountDue],
                          [dtDueDate],
                          [vcMemo],
                          [vcReference],
                          --[monAmtPaid],
                          [bitIsPaid],
                          [numDomainID],
                          [numCreatedBy],
                          [dtCreatedDate],numAccountClass,[numOppId],[bitLandedCost],[numCurrencyID],[fltExchangeRate]
                        )
                VALUES  (
                          @numDivisionID,
                          @numAccountID,
                          @dtBillDate,
                          @numTermsID,
                          @numDueAmount,
                          @dtDueDate,
                          @vcMemo,
                          @vcReference,
                          --@numBillTotalAmount,
                          @bitIsPaid,
                          @numDomainID,
                          @numUserCntID,
                          GETUTCDATE(),@numAccountClass,@numOppId,@bitLandedCost,@numCurrencyID,@fltExchangeRate
                        )
                SET @numBillID = SCOPE_IDENTITY()
            END
				
		PRINT @numBillID		
--        DELETE  FROM dbo.BillDetails
--        WHERE   numBillID = @numBillID 
				
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> '' 
        BEGIN
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                
            DELETE FROM BillDetails WHERE numBillID = @numBillID AND numBillDetailID NOT IN (SELECT X.numBillDetailID FROM OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2) WITH(numBillDetailID NUMERIC(18,0))X)
			                                                           
			--Update transactions
			Update 
				BillDetails 
			SET 
				numExpenseAccountID=X.numExpenseAccountID
				,monAmount=X.monAmount
				,vcDescription=X.vcDescription
				,numProjectID=X.numProjectID
				,numClassID=X.numClassID
				,numCampaignID=X.numCampaignID
				,numOppItemID=X.numOppItemID
				,numOppID=X.numOppID
			FROM 
			(
				SELECT 
					* 
				FROM 
					OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID>0]',2)                                                                                                              
				WITH
				(                                                                                       
					numBillDetailID NUMERIC(18,0)
					,[numExpenseAccountID] NUMERIC(18,0)
					,[monAmount] DECIMAL(20,5)
					,[vcDescription] VARCHAR(1000)
					,[numProjectID] NUMERIC(18,0)
					,[numClassID] NUMERIC(18,0)
					,[numCampaignID] NUMERIC(18,0)
					,numOppItemID NUMERIC(18,0)
					,numOppID NUMERIC(18,0)                                                                                             
				)
			)X                                                                                                  
			WHERE 
				BillDetails.numBillDetailID=X.numBillDetailID 
				AND numBillID = @numBillID                                                                                             
			               
			
            INSERT INTO [dbo].[BillDetails]
            (
                [numBillID],
                [numExpenseAccountID],
                [monAmount],
                [vcDescription],
                [numProjectID],
                [numClassID],
                [numCampaignID]
				,numOppItemID
				,numOppID
            )
            SELECT  
				@numBillID,
                X.[numExpenseAccountID],
                X.[monAmount],
                X.[vcDescription],
                X.[numProjectID],
                X.[numClassID],
                X.[numCampaignID]
				,X.numOppItemID
				,X.numOppID
            FROM
			(
				SELECT 
					*
                FROM 
					OPENXML(@hDocItem,'/NewDataSet/Table1 [numBillDetailID=0]',2)
                WITH 
				(
					numBillDetailID NUMERIC(18,0)
					,[numExpenseAccountID] NUMERIC(18,0)
					,[monAmount] DECIMAL(20,5)
					,[vcDescription] VARCHAR(1000)
					,[numProjectID] NUMERIC(18,0)
					,[numClassID] NUMERIC(18,0)
					,[numCampaignID] NUMERIC(18,0)
					,numOppItemID NUMERIC(18,0)
					,numOppID NUMERIC(18,0)
				)
            ) X
            
			EXEC sp_xml_removedocument @hDocItem
        END

		IF EXISTS (SELECT numBillDetailID FROM BillDetails WHERE numBillID=@numBillID AND numExpenseAccountID NOT IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID))
		BEGIN
			RAISERROR('INVALID_EXPENSE_ACCOUNT',16,1)
		END
		-- Update leads,prospects to Accounts
		EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
		
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	