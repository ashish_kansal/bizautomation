
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemOrderDependencies')
DROP PROCEDURE USP_GetItemOrderDependencies
GO
CREATE PROCEDURE [dbo].[USP_GetItemOrderDependencies]         
	@numDomainID as numeric(9)=0,    
	@numItemCode as numeric(9)=0,
	@ClientTimeZoneOffset Int,
	@tintOppType as tinyint=0,
	@byteMode as tinyint=0,
	@numWareHouseId as numeric(9)=0
as        
IF @byteMode=0 
BEGIN

	DECLARE @vcItemName AS VARCHAR(100)
	DECLARE @OnOrder AS numeric(18)
	DECLARE @vcShippedReceivedStatus VARCHAR(100) = ''

	Create table #temp (numoppid numeric(18), vcPOppName varchar(100), tintOppType tinyint, bintCreatedDate datetime, bintAccountClosingDate datetime, OppType varchar(20),
		OppStatus varchar(20), vcCompanyname varchar(200), vcItemName varchar(200), numReturnHeaderID numeric(18), vcWareHouse varchar(100), 
		numQtyOrdered FLOAT, numQtyReleasedReceived FLOAT, numQtyOnAllocationOnOrder FLOAT)

	-- NOT Sales and Purchase Return
	IF @tintOppType <> 5 AND @tintOppType <> 6 AND ISNULL(@tintOppType,0) <> 11
	BEGIN
		--Regular Item
		INSERT INTO 
			#temp
		SELECT 
			OM.numoppid
			,OM.vcPoppName
			,OM.tintOppType
			,OM.bintCreatedDate
			,OM.bintAccountClosingDate
			,CASE 
				WHEN OM.tintOppType=1 
				THEN CASE WHEN OM.tintOppStatus=0 THEN 'Sales Opp' ELSE 'Sales Order' END 
				ELSE CASE WHEN OM.tintOppStatus=0 THEN 'Purchase Opp' ELSE 'Purchase Order' END 
			END as OppType
			,CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN 'Open' WHEN 1 THEN 'Closed' END OppStatus
			,CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(DM.numCompanyDiff) + ':' + isnull(DM.vcCompanyDiff,'') else '' end as vcCompanyname
			,''
			,0
			,W.vcWareHouse 
			,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * numUnitHour AS numQtyOrdered
			,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(numQtyShipped,0) ELSE ISNULL(numUnitHourReceived,0) END) AS numQtyReleasedReceived
			,(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * numUnitHour) - (ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,I.numDomainId, ISNULL(Opp.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(numQtyShipped,0) ELSE ISNULL(numUnitHourReceived,0) END)) As numQtyOnAllocationOnOrder
		FROM 
			OpportunityItems Opp
		INNER JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
		INNER JOIN item I ON Opp.numItemCode = i.numItemcode
		INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
		LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
		LEFT JOIN WareHouseItems WHI ON WHI.[numWareHouseItemID] = Opp.numWarehouseItmsID
		LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
		WHERE   
			OM.numDomainId = @numDomainID
			AND I.numDomainId = @numDomainID 
			AND I.numItemCode=@numItemCode
			AND 1 = (CASE WHEN @numWareHouseId > 0 THEN 
							Case WHEN W.numWareHouseID = @numWareHouseId then 1 else 0 end
						Else 1 end) 
			AND 1=(CASE 
						WHEN ISNULL(@tintOppType,0) = 1 -- Sales Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 2 -- Purchase Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 3 -- Sales Opportunity
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 4 -- Purchase Opportunity
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 7 -- Open Sales Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 8 -- Closed Sales Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 9 -- Open Purchase Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
						WHEN ISNULL(@tintOppType,0) = 10 -- Closed Purchase Order
						THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
						ELSE 1 
					END)
		ORDER BY 
			OM.bintCreatedDate desc
	END
	

	DECLARE @strSQL AS NVARCHAR(MAX) = ''
	DECLARE @strWHERE AS NVARCHAR(MAX)
	SET @strWHERE = ' WHERE 1=1 '

	IF @tintOppType = 5 
	BEGIN
	 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] = 1 '
	END
	ELSE IF @tintOppType = 6
	BEGIN
	 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] = 2 '
	END
	ELSE IF @tintOppType = 0
	BEGIN
	 SET @strWHERE = @strWHERE + ' AND [RH].[tintReturnType] IN (1,2) '
	END


	-- NOT Sales and Purchase Return
	IF @tintOppType <> 5 AND @tintOppType <> 6 AND ISNULL(@tintOppType,0) <> 11
	BEGIN
		SET @strSQL = CONCAT('INSERT INTO 
								#temp
							SELECT 
								OM.numoppid
								,OM.vcPoppName
								,OM.tintOppType
								,OM.bintCreatedDate
								,OM.bintAccountClosingDate
								,CASE 
									WHEN OM.tintOppType=1 
									THEN CASE WHEN OM.tintOppStatus=0 THEN ''Sales Opp'' ELSE ''Sales Order'' END 
									ELSE CASE WHEN OM.tintOppStatus=0 THEN ''Purchase Opp'' ELSE ''Purchase Order'' END 
								END as OppType
								,CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN ''Open'' WHEN 1 THEN ''Closed'' END OppStatus
								,CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + isnull(DM.vcCompanyDiff,'''') else '''' end as vcCompanyname
								,IParent.vcItemName + Case when IParent.bitAssembly=1 then '' (Assembly Member)'' When IParent.bitKitParent=1 then '' (Kit Member)'' else '''' end
								,0 [numReturnHeaderID]
								,W.vcWareHouse
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq AS numQtyOrdered
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END) AS numQtyReleasedReceived
								,(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq) - (ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END)) As numQtyOnAllocationOnOrder
							FROM 
								OpportunityKitItems OKI 
							INNER JOIN OpportunityItems OI
							ON OKI.numOppItemID=OI.numoppitemtcode
							INNER JOIN Item IParent on OI.numItemCode =IParent.numItemCode 
							INNER JOIN Item I on OKI.numChildItemID =I.numItemCode  
							INNER JOIN OpportunityMaster OM ON OKI.numOppID = oM.numOppID
							INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
							LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
							LEFT JOIN WareHouseItems WHI ON WHI.numWarehouseItemID = OKI.numWarehouseItemID
							LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
							WHERE 
								ISNULL(I.bitAssembly,0)=0 
								AND OKI.numChildItemID=',@numItemCode,'  
								AND om.numDomainId = ',@numDomainID,' 
								AND 1 = (CASE WHEN ',@numWareHouseId,' > 0 THEN 
												Case WHEN W.numWareHouseID = ',@numWareHouseId,' then 1 else 0 end
											Else 1 end) 
								AND I.numDomainId = ',@numDomainID,'  
								AND 1=(CASE 
										WHEN ',ISNULL(@tintOppType,0),' = 1
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 2
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 3
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 4
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 7
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 8
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 9
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 10
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
										ELSE 1 
									END)')

		SET @strSQL = CONCAT(@strSQL,' INSERT INTO 
								#temp
							SELECT 
								OM.numoppid
								,OM.vcPoppName
								,OM.tintOppType
								,OM.bintCreatedDate
								,OM.bintAccountClosingDate
								,CASE 
									WHEN OM.tintOppType=1 
									THEN CASE WHEN OM.tintOppStatus=0 THEN ''Sales Opp'' ELSE ''Sales Order'' END 
									ELSE CASE WHEN OM.tintOppStatus=0 THEN ''Purchase Opp'' ELSE ''Purchase Order'' END 
								END as OppType
								,CASE ISNULL(OM.tintshipped, 0) WHEN 0 THEN ''Open'' WHEN 1 THEN ''Closed'' END OppStatus
								,CI.vcCompanyName + Case when isnull(DM.numCompanyDiff,0)>0 then  ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + isnull(DM.vcCompanyDiff,'''') else '''' end as vcCompanyname
								,IParent.vcItemName + Case when IParent.bitAssembly=1 then '' (Assembly Member)'' When IParent.bitKitParent=1 then '' (Kit Member)'' else '''' end
								,0 [numReturnHeaderID]
								,W.vcWareHouse
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq AS numQtyOrdered
								,ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END) AS numQtyReleasedReceived
								,(ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * OKI.numQtyItemsReq) - (ISNULL(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), I.numItemCode,I.numDomainId, ISNULL(OKI.numUOMId, 0)),1) * (CASE WHEN OM.tintOppType=1 THEN ISNULL(OKI.numQtyShipped,0) ELSE 0 END)) As numQtyOnAllocationOnOrder
							FROM 
								OpportunityKitChildItems OKI 
							INNER JOIN OpportunityItems OI
							ON OKI.numOppItemID=OI.numoppitemtcode
							INNER JOIN Item IParent on OI.numItemCode =IParent.numItemCode 
							INNER JOIN Item I on OKI.numItemID =I.numItemCode  
							INNER JOIN OpportunityMaster OM ON OKI.numOppID = oM.numOppID
							INNER JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
							LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
							LEFT JOIN WareHouseItems WHI ON WHI.numWarehouseItemID = OKI.numWarehouseItemID
							LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
							WHERE 
								ISNULL(I.bitAssembly,0)=0 
								AND OKI.numItemID=',@numItemCode,'  
								AND om.numDomainId = ',@numDomainID,' 
								AND 1 = (CASE WHEN ',@numWareHouseId,' > 0 THEN 
												Case WHEN W.numWareHouseID = ',@numWareHouseId,' then 1 else 0 end
											Else 1 end) 
								AND I.numDomainId = ',@numDomainID,'  
								AND 1=(CASE 
										WHEN ',ISNULL(@tintOppType,0),' = 1
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 2
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 3
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 4
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 7
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 8
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 1 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 9
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 0 THEN 1 ELSE 0 END
										WHEN ',ISNULL(@tintOppType,0),' = 10
										THEN CASE WHEN ISNULL(OM.tintOppType,0) = 2 AND ISNULL(OM.tintOppStatus,0)=1 AND ISNULL(OM.tintshipped,0) = 1 THEN 1 ELSE 0 END 
										ELSE 1 
									END)')
	END

	IF @tintOppType = 5 AND @tintOppType = 6 OR @tintOppType = 0
	BEGIN
		SET @strSQL = @strSQL + CONCAT(' INSERT INTO #temp
										SELECT 
											RH.[numReturnHeaderID] numoppid
											,RH.[vcRMA] vcPoppName
											,[RH].[tintReturnType] tintOppType
											,RH.[dtCreatedDate] bintCreatedDate
											,NULL bintAccountClosingDate
											,CASE [RH].[tintReturnType] WHEN 1 THEN ''Sales Return'' WHEN 2 THEN ''Purchase Return'' ELSE '''' END OppType
											,'''' OppStatus
											,CI.vcCompanyName + CASE WHEN ISNULL(DM.numCompanyDiff,0) > 0 THEN ''  '' + dbo.fn_getlistitemname(DM.numCompanyDiff) + '':'' + ISNULL(DM.vcCompanyDiff,'''') ELSE '''' END AS vcCompanyname
											,I.vcItemName + CASE WHEN I.bitAssembly = 1 THEN '' (Assembly)'' WHEN I.bitKitParent = 1 THEN '' (Kit)'' ELSE '''' END
											,RH.[numReturnHeaderID]
											,W.vcWareHouse
											,0
											,0
											,0
										FROM [dbo].[ReturnHeader] AS RH 
										INNER JOIN [dbo].[ReturnItems] AS RI  ON [RH].[numReturnHeaderID] = [RI].[numReturnHeaderID] AND [RH].[tintReturnType] IN (1,2)
										INNER JOIN [dbo].[Item] AS I ON [RI].[numItemCode] = [I].[numItemCode] AND [I].[numDomainID] = RH.[numDomainId]
										INNER JOIN divisionMaster DM ON RH.numDivisionID = DM.numDivisionID
										LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID 
										LEFT JOIN WareHouseItems WHI ON WHI.numWareHouseItemID = RI.numWareHouseItemID  
										LEFT JOIN Warehouses W ON W.numWareHouseID = WHI.numWareHouseID
										', @strWHERE,' AND RH.[numDomainId] = ',@numDomainID,' AND [RI].[numItemCode] = ',@numItemCode,' ORDER BY bintCreatedDate desc')
	END

	-- Stock Transfer
	IF ISNULL(@tintOppType,0) = 0 OR ISNULL(@tintOppType,0) = 11
	BEGIN
		INSERT INTO 
			#temp
		SELECT 
			MST.ID
			,CONCAT('Stock Transfer (To Warehouse:',CONCAT(WTo.vcWareHouse, CASE WHEN LEN(ISNULL(WLTo.vcLocation,'')) > 0 THEN CONCAT(' (',WLTo.vcLocation,')') ELSE '' END),')')
			,0
			,MST.dtTransferredDate
			,NULL
			,'Stock Transfer' as OppType
			,'Closed' OppStatus
			,'' as vcCompanyname
			,''
			,0
			,CONCAT(WFrom.vcWareHouse, CASE WHEN LEN(ISNULL(WLFrom.vcLocation,'')) > 0 THEN CONCAT(' (',WLFrom.vcLocation,')') ELSE '' END)
			,MST.numQty AS numQtyOrdered
			,MST.numQty AS numQtyReleasedReceived
			,0 As numQtyOnAllocationOnOrder
		FROM 
			MassStockTransfer MST
		INNER JOIN 
			Item I 
		ON 
			MST.numItemCode = I.numItemcode
		INNER JOIN 
			WareHouseItems WHIFrom 
		ON 
			WHIFrom.[numWareHouseItemID] = MST.numFromWarehouseItemID
		INNER JOIN 
			Warehouses WFrom 
		ON 
			WFrom.numWareHouseID = WHIFrom.numWareHouseID
		LEFT JOIN
			WarehouseLocation WLFrom
		ON
			WHIFrom.numWLocationID = WLFrom.numWLocationID
		INNER JOIN 
			WareHouseItems WHITo 
		ON 
			WHITo.[numWareHouseItemID] = MST.numToWarehouseItemID
		INNER JOIN 
			Warehouses WTo 
		ON 
			WTo.numWareHouseID = WHITo.numWareHouseID
		LEFT JOIN
			WarehouseLocation WLTo
		ON
			WHITo.numWLocationID = WLTo.numWLocationID
		WHERE   
			MST.numDomainId = @numDomainID
			AND MST.numItemCode=@numItemCode
			AND I.numDomainId = @numDomainID 
	END


PRINT CAST(@strSQL AS NTEXT)
EXEC (@strSQL)


select numoppid,vcPoppName,tintOppType,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	else dbo.FormatedDateFromDate(DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate ),1) end  CreatedDate,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	else dbo.FormatedDateFromDate(DateAdd(minute, -@ClientTimeZoneOffset,bintAccountClosingDate ),1) end  AccountClosingDate,
 OppType,OppStatus,vcCompanyName,vcItemName,numReturnHeaderID, vcWareHouse, ISNULL(numQtyOrdered,0) AS numOnOrder, ISNULL(numQtyOnAllocationOnOrder,0) AS numAllocation,ISNULL(numQtyReleasedReceived,0) numQtyReleasedReceived  from #temp 
  ORDER by bintCreatedDate desc 

drop table #temp
END


ELSE IF @byteMode=1 --Parent Items of selected item
BEGIN

;WITH CTE(numItemCode,vcItemName,bitAssembly,bitKitParent)
AS
(
select numItemKitID,vcItemName,bitAssembly,bitKitParent
from item                                
INNER join ItemDetails Dtl on numItemKitID=numItemCode
where  numChildItemID=@numItemCode

UNION ALL

select dtl.numItemKitID ,i.vcItemName,i.bitAssembly,i.bitKitParent
from item i                               
INNER JOIN ItemDetails Dtl on Dtl.numItemKitID=i.numItemCode
INNER JOIN CTE c ON Dtl.numChildItemID = c.numItemCode
where Dtl.numChildItemID!=@numItemCode
)

select numItemCode,vcItemName,Case when bitAssembly=1 then 'Assembly' When bitKitParent=1 then 'Kit' end as ItemType
from CTE                                
END
GO