/****** Object:  StoredProcedure [dbo].[Usp_GetChildOpportunity]    Script Date: 05/07/2009 22:05:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC Usp_GetChildOpportunity 1145,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetChildOpportunity')
DROP PROCEDURE Usp_GetChildOpportunity
GO
CREATE PROCEDURE [dbo].[Usp_GetChildOpportunity]
               @numOppId    NUMERIC(9),
               @numDomainId NUMERIC(9)
AS
  BEGIN
  
  SELECT vcPOppName, OM.numContactID, OM.numDivisionId, CAST(OM.txtComments AS VARCHAR(MAX)) AS Comments,
			ISNULL(LD.vcData,'') AS vcStatus, 
			ISNULL(ACI.vcFirstName +' '+ ACI.vcLastName,'') AS RecordContact,
			ISNULL(CI.vcCompanyName,'') AS RecordOrganization, OM.numOppId,
			OM.tintOppType
			FROM  OpportunityLinking OL
				INNER JOIN OpportunityMaster OM	ON OL.[numChildOppID] = OM.numOppID
				LEFT JOIN ListDetails LD ON LD.numListItemID = OM.numStatus
					AND (LD.numDomainID = @numDomainId OR constFlag = 1)
				LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
				LEFT JOIN DivisionMaster DM ON DM.numDivisionId = OM.numDivisionId
				LEFT JOIN CompanyInfo CI ON CI.numCompanyId = DM.numCompanyID
				LEFT OUTER JOIN OpportunityItems OI ON OI.numOppID = OM.numOppID
				LEFT OUTER JOIN item I ON OI.numItemCode = i.numItemcode
			WHERE OM.numDomainId = @numDomainId
				AND OL.numParentOppID = @numOppId
		UNION 
		
		SELECT vcPOppName, OM.numContactID, OM.numDivisionId, CAST(OM.txtComments AS VARCHAR(MAX)) AS Comments,
			ISNULL(LD.vcData,'') AS vcStatus, 
			ISNULL(ACI.vcFirstName +' '+ ACI.vcLastName,'') AS RecordContact,
			ISNULL(CI.vcCompanyName,'') AS RecordOrganization, OM.numOppId,
			OM.tintOppType
			FROM [RecurringTransactionReport] RTR
				INNER JOIN OpportunityMaster OM ON RTR.[numRecTranOppID] = OM.numOppID
				LEFT JOIN ListDetails LD ON LD.numListItemID = OM.numStatus
					AND (LD.numDomainID = @numDomainId OR constFlag = 1)
				LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
				LEFT JOIN DivisionMaster DM ON DM.numDivisionId = OM.numDivisionId
				LEFT JOIN CompanyInfo CI ON CI.numCompanyId = DM.numCompanyID
				LEFT OUTER JOIN OpportunityItems OI ON OI.numOppID = OM.numOppID
				LEFT OUTER JOIN item I ON OI.numItemCode = i.numItemcode
			WHERE  OM.numDomainId = @numDomainId
				AND [numRecTranSeedId] = @numOppId
  END


