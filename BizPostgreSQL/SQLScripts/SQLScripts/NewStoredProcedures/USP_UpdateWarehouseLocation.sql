
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateWarehouseLocation' ) 
    DROP PROCEDURE USP_UpdateWarehouseLocation
GO
-- EXEC USP_GetWarehouseLocation 1 , 58 , 11
CREATE PROCEDURE [dbo].USP_UpdateWarehouseLocation
    @numDomainID AS NUMERIC(9) ,
    @numWLocationID AS numeric(9) = 0,
	@vcAisle VARCHAR(500)='',
	@vcRack VARCHAR(500)='',
	@vcShelf VARCHAR(500)='',
	@vcBin VARCHAR(500)='',
	@vcLocation VARCHAR(500)=''
AS 
BEGIN
	IF LEN(ISNULL(@vcLocation,'')) = 0
	BEGIN
		SET @vcLocation=@vcAisle+','+@vcRack+','+@vcShelf+','+@vcBin
		SET @vcLocation = Replace(@vcLocation,',,,', ',')
		SET @vcLocation = Replace(@vcLocation,',,', ',')
		SET @vcLocation= REPLACE(LTRIM(RTRIM(REPLACE(@vcLocation, ',', ' '))), ' ', ',')
	END

	UPDATE 
		WarehouseLocation
	SET
		vcAisle=@vcAisle,
		vcRack=@vcRack,
		vcShelf=@vcShelf,
		vcBin=@vcBin,
		vcLocation=@vcLocation
	WHERE
		numDomainId=@numDomainID AND
		numWLocationID=@numWLocationID

	UPDATE
		WareHouseItems 
	SET
		vcLocation=@vcLocation
	WHERE
		numDomainId=@numDomainID AND
		numWLocationID=@numWLocationID
END
GO