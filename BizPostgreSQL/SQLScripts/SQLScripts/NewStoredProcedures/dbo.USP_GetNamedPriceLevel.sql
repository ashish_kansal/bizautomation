SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNamedPriceLevel')
DROP PROCEDURE USP_GetNamedPriceLevel
GO
CREATE PROCEDURE [dbo].[USP_GetNamedPriceLevel]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		pnt.tintPriceLevel AS Id
		,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',pnt.tintPriceLevel)) Value
	FROM 
		PricingNamesTable pnt
	WHERE
		pnt.numDomainID = @numDomainID
	ORDER BY
		pnt.tintPriceLevel
END
