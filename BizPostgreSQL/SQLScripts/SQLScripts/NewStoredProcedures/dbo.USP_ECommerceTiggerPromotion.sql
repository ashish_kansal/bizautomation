SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ECommerceTiggerPromotion') 
DROP PROCEDURE USP_ECommerceTiggerPromotion
GO
CREATE  PROCEDURE [dbo].[USP_ECommerceTiggerPromotion]
(
	@numDomainId numeric(9,0),
	@numUserCntId numeric(9,0),
	@vcCookieId varchar(100),
	@numCartID NUMERIC(18,0),
	@numItemCode NUMERIC(18, 0),
	@numItemClassification NUMERIC(18,0),
    @numUnitHour NUMERIC(18, 0),
	@monPrice DECIMAL(20,5),
	@numSiteID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @monTotalAmount DECIMAL(20,5) = @numUnitHour * @monPrice
	DECLARE @numAppliedPromotion NUMERIC(18,0) = 0
	DECLARE @numPromotionID NUMERIC(18,0)
	DECLARE @fltOfferTriggerValue FLOAT
	DECLARE @tintOfferTriggerValueType TINYINT
	DECLARE @tintOfferBasedOn TINYINT
	DECLARE @fltDiscountValue FLOAT
	DECLARE @tintDiscountType TINYINT
	DECLARE @tintDiscoutBaseOn TINYINT
	DECLARE @vcPromotionDescription VARCHAR(MAX)

	DECLARE @TEMPPromotion TABLE
	(
		ID INT IDENTITY(1,1),
		numPromotionID NUMERIC(18,0),
		fltOfferTriggerValue FLOAT,
		tintOfferTriggerValueType TINYINT,
		tintOfferBasedOn TINYINT,
		fltDiscountValue FLOAT,
		tintDiscountType TINYINT,
		tintDiscoutBaseOn TINYINT,
		vcPromotionDescription VARCHAR(MAX)
	)

	INSERT INTO 
		@TEMPPromotion
	SELECT
		PO.numProId,
		PO.fltOfferTriggerValue,
		PO.tintOfferTriggerValueType,
		PO.tintOfferBasedOn,
		PO.fltDiscountValue,
		PO.tintDiscountType,
		PO.tintDiscoutBaseOn,
		(SELECT CONCAT('Buy '
						,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
						,CASE tintOfferBasedOn
								WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
							END
						,' & get '
						, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
						, CASE 
							WHEN tintDiscoutBaseOn = 1 THEN 
								CONCAT
								(
									CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
									,'.'
								)
							WHEN tintDiscoutBaseOn = 2 THEN 
								CONCAT
								(
									CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
									,'.'
								)
							WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
							ELSE '' 
						END
		))
	FROM 
		PromotionOffer PO
	INNER JOIN
		PromotionOfferSites POS
	ON
		PO.numProId=POS.numPromotionID
		AND POS.numSiteID=@numSiteID
	WHERE
		PO.numDomainId=@numDomainId
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitRequireCouponCode,0) = 0
		AND 1 = (CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 1 ELSE (CASE WHEN GETDATE() BETWEEN dtValidFrom AND dtValidTo THEN 1 ELSE 0 END) END)
		AND 1=(CASE PO.tintOfferBasedOn
				WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemCode AND tintRecordType=5 AND tintType=1) > 0 THEN 1 ELSE 0 END)
				WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=PO.numProId AND POI.numValue=@numItemClassification AND tintRecordType=5 AND tintType=2) > 0 THEN 1 ELSE 0 END)
				ELSE 0
			END)
	ORDER BY
		CASE 
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
		END

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT
		
	SELECT @Count=COUNT(*) FROM @TEMPPromotion

	IF @Count > 0
	BEGIN
		WHILE @i <= @Count
		BEGIN
			SELECT 
				@numPromotionID=numPromotionID,
				@fltOfferTriggerValue = fltOfferTriggerValue,
				@tintOfferTriggerValueType=tintOfferTriggerValueType,
				@tintOfferBasedOn=tintOfferBasedOn,
				@fltDiscountValue=fltDiscountValue,
				@tintDiscountType=tintDiscountType,
				@tintDiscoutBaseOn=tintDiscoutBaseOn,
				@vcPromotionDescription=vcPromotionDescription
			FROM 
				@TEMPPromotion 
			WHERE 
				ID=@i

			IF @tintOfferBasedOn = 1 -- individual items
				AND (SELECT COUNT(*) FROm CartItems WHERE numDomainId=@numDomainId AND numUserCntId=@numUserCntId AND vcCookieId=@vcCookieId AND PromotionID=@numPromotionID AND bitParentPromotion=1) = 0
				AND 1 = (CASE 
							WHEN @tintOfferTriggerValueType=1 -- Quantity
							THEN CASE WHEN @numUnitHour >= @fltOfferTriggerValue THEN 1 ELSE 0 END
							WHEN @tintOfferTriggerValueType=2 -- Amount
							THEN CASE WHEN @monTotalAmount >= @fltOfferTriggerValue THEN 1 ELSE 0 END
							ELSE 0
						END)
			BEGIN
				UPDATE 
					CartItems 
				SET 
					PromotionID=@numPromotionID ,
					PromotionDesc=@vcPromotionDescription,
					bitParentPromotion=1
				WHERE 
					numCartId=@numCartId

				IF 1 =(CASE @tintDiscoutBaseOn
						WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND POI.numValue=@numItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
						WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND POI.numValue=@numItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
						WHEN 3 THEN (CASE @tintOfferBasedOn
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END) 
						ELSE 0
					END)
				BEGIN
					IF @tintDiscountType=1  AND @fltDiscountValue > 0 --Percentage
					BEGIN
						UPDATE 
							CartItems
						SET
							fltDiscount=@fltDiscountValue,
							bitDiscountType=0,
							monTotAmount=(@monTotalAmount-((@monTotalAmount*@fltDiscountValue)/100))
						WHERE
							numCartId=@numCartId
					END
					ELSE IF @tintDiscountType=2 AND @fltDiscountValue > 0 --Flat Amount
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
						DECLARE @usedDiscountAmount AS FLOAT
				
						SELECT
							@usedDiscountAmount = SUM(fltDiscount)
						FROM
							CartItems
						WHERE
							PromotionID=@numPromotionID

						IF @usedDiscountAmount < @fltDiscountValue
						BEGIN
							UPDATE 
								CartItems 
							SET 
								monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTotalAmount THEN 0 ELSE (@monTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
								bitDiscountType=1,
								fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTotalAmount THEN @monTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
							WHERE 
								numCartId=@numCartId
						END
					END
					ELSE IF @tintDiscountType=3  AND @fltDiscountValue > 0 --Quantity
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
						DECLARE @usedDiscountQty AS FLOAT

						SELECT
							@usedDiscountQty = SUM(fltDiscount/monPrice)
						FROM
							CartItems
						WHERE
							PromotionID=@numPromotionID

						DECLARE @remainingDiscountValue FLOAT
						SET @remainingDiscountValue = (@fltDiscountValue - @usedDiscountQty)

						IF @usedDiscountQty < @fltDiscountValue
						BEGIN
							IF (SELECT COUNT(*) FROM CartItems WHERE numDomainId=@numDomainId AND numCartId=@numCartId AND bitParentPromotion=1 AND @tintOfferBasedOn=1) > 0
							BEGIN	
								IF @tintOfferTriggerValueType=1
								BEGIN
									IF @numUnitHour > @fltOfferTriggerValue
									BEGIN
										DECLARE @numTempUnitHour FLOAT
										SET @numTempUnitHour = @numUnitHour - @fltOfferTriggerValue

										UPDATE 
											CartItems 
										SET 
											monTotAmount=(CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN @fltOfferTriggerValue * monPrice ELSE (@monTotalAmount-(@remainingDiscountValue * @monPrice)) END),
											bitDiscountType=1,
											fltDiscount=(CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN (@numTempUnitHour * @monPrice) ELSE (@remainingDiscountValue * @monPrice) END) 
										WHERE 
											numCartId=@numCartId

										SET @remainingDiscountValue = @remainingDiscountValue - (CASE WHEN @remainingDiscountValue >= @numTempUnitHour THEN @numTempUnitHour ELSE @remainingDiscountValue END)
									END
									ELSE
									BEGIN
										UPDATE
											CartItems
										SET
											fltDiscount=0,
											monTotAmount=numUnitHour*monPrice,
											monTotAmtBefDiscount=numUnitHour*monPrice
										WHERE	
											numCartId=@numCartId
									END
								END
								ELSE IF @tintOfferTriggerValueType = 2
								BEGIN
									IF (@numUnitHour * @monPrice) > @fltOfferTriggerValue
									BEGIN
										DECLARE @discountQty AS FLOAT
										SET @discountQty = FLOOR(((@numUnitHour * @monPrice) - @fltOfferTriggerValue) / (CASE WHEN ISNULL(@monPrice,0) = 0 THEN 1 ELSE @monPrice END))

										IF @discountQty > 0
										BEGIN
											UPDATE 
												CartItems 
											SET 
												monTotAmount=(CASE WHEN @remainingDiscountValue >= @discountQty THEN (@numTempUnitHour - @discountQty) * monPrice ELSE ((numUnitHour * monPrice)-(@remainingDiscountValue * monPrice)) END),
												bitDiscountType=1,
												fltDiscount=(CASE WHEN @remainingDiscountValue >= @discountQty THEN (@discountQty * monPrice) ELSE (@remainingDiscountValue * monPrice) END) 
											WHERE 
												numCartId=@numCartId

											SET @remainingDiscountValue = @remainingDiscountValue - @discountQty
										END
										ELSE
										BEGIN
											UPDATE
												CartItems
											SET
												fltDiscount=0,
												monTotAmount=@numUnitHour*@monPrice,
												monTotAmtBefDiscount=@numUnitHour*@monPrice
											WHERE	
												numCartId=@numCartId
										END
									END
									ELSE
									BEGIN
										UPDATE
											CartItems
										SET
											fltDiscount=0,
											monTotAmount=numUnitHour*monPrice,
											monTotAmtBefDiscount=numUnitHour*monPrice
										WHERE	
											numCartId=@numCartId
									END
								END
							END
							ELSE 
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN 0 ELSE (@monTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monPrice)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN (@numUnitHour*@monPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) * @monPrice) END) 
								WHERE 
									numCartId=@numCartId
							END
						END
					END
				END

				SET @numAppliedPromotion = @numPromotionID
				BREAK
			END
			ELSE IF @tintOfferBasedOn = 2 -- item classifications
			BEGIN
				DECLARE @TEMPItems TABLE
				(
					ID INT,
					numCartID NUMERIC(18,0),
					numItemCode NUMERIC(18,0),
					numItemClassification NUMERIC(18,0),
					numUnitHour NUMERIC(18,0),
					monPrice NUMERIC(18,0),
					monTotalAmount NUMERIC(18,0)
				)

				INSERT INTO 
					@TEMPItems
				SELECT
					1,
					numCartId,
					CI.numItemCode,
					I.numItemClassification,
					CI.numUnitHour,
					CI.monPrice,
					(CI.numUnitHour * CI.monPrice)
				FROM
					CartItems CI
				INNER JOIN
					Item I
				ON
					CI.numItemCode = I.numItemCode
				WHERE
					numCartId=@numCartID
				UNION
				SELECT
					(ROW_NUMBER() OVER(ORDER BY numCartID ASC) + 1),
					numCartId,
					CI.numItemCode,
					I.numItemClassification,
					CI.numUnitHour,
					CI.monPrice,
					(CI.numUnitHour * CI.monPrice)
				FROM
					CartItems CI
				INNER JOIN
					Item I
				ON
					CI.numItemCode = I.numItemCode
				WHERE
					numCartId <> @numCartID
					AND CI.numDomainId=@numDomainId
					AND CI.numUserCntId=@numUserCntId
					AND CI.vcCookieId=@vcCookieId
					AND ISNULL(CI.PromotionID,0) = 0
					AND I.numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@numPromotionID AND tintRecordType=5 AND tintType=2)

				IF(SELECT 
						(CASE @tintOfferTriggerValueType WHEN  1 THEN SUM(numUnitHour) WHEN 2 THEN SUM(monTotalAmount) ELSE 0 END)
					FROM
						@TEMPItems) >= @fltOfferTriggerValue
				BEGIN
					DECLARE @remainingOfferValue FLOAT = @fltOfferTriggerValue
					DECLARE @numTempCartID NUMERIC(18,0)
					DECLARE @numTempItemCode NUMERIC(18,0)
					DECLARE @numTempItemClassification NUMERIC(18,0)
					DECLARE @monTempPrice NUMERIC(18,0)
					DECLARE @monTempTotalAmount NUMERIC(18,0)
					DECLARE @j AS INT = 1 
					DECLARE @jCount AS INT
					SELECT @jCount=COUNT(*) FROM @TEMPItems

					WHILE @j <= @jCount
					BEGIN
						SELECT 
							@numTempCartID=numCartID,
							@numTempItemCode=numItemCode,
							@numTempItemClassification=ISNULL(numItemClassification,0),
							@numTempUnitHour=numUnitHour,
							@monTempPrice=monPrice,
							@monTempTotalAmount = (numUnitHour * monPrice)
						FROM 
							@TEMPItems 
						WHERE 
							ID=@j

						UPDATE
							CartItems
						SET
							PromotionID=@numPromotionID,
							PromotionDesc=@vcPromotionDescription,
							fltDiscount=0,
							bitDiscountType=0,
							bitParentPromotion=1
						WHERE
							numCartId=@numCartID


						-- CHECK IF DISCOUNT FROM PROMOTION CAN ALSO BE APPLIED TO ITEM
						IF 1=(CASE @tintDiscoutBaseOn
								WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND POI.numValue=@numTempItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
								WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND POI.numValue=@numTempItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
								WHEN 3 THEN (CASE @tintOfferBasedOn
												WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
												WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numPromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
												ELSE 0
											END) 
								ELSE 0
							END)
						BEGIN
							IF @tintDiscountType=1 --Percentage
							BEGIN
								UPDATE 
									CartItems
								SET
									fltDiscount=@fltDiscountValue,
									bitDiscountType=0,
									monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
								WHERE
									numCartId=@numCartId
							END
							ELSE IF @tintDiscountType=2 --Flat Amount
							BEGIN
								SELECT
									@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
								FROM
									CartItems
								WHERE
									PromotionID=@numPromotionID

								IF @usedDiscountAmount < @fltDiscountValue
								BEGIN
									UPDATE 
										CartItems 
									SET 
										monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
										bitDiscountType=1,
										fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
									WHERE 
										numCartId=@numCartId
								END
							END
							ELSE IF @tintDiscountType=3 --Quantity
							BEGIN
								-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
								SELECT
									@usedDiscountQty = SUM(fltDiscount/monPrice)
								FROM
									CartItems
								WHERE
									PromotionID=@numPromotionID

								IF @usedDiscountQty < @fltDiscountValue
								BEGIN
									UPDATE 
										CartItems 
									SET 
										monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
										bitDiscountType=1,
										fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN (@numTempUnitHour*@monTempPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
									WHERE 
										numCartId=@numCartId
								END
							END
						END

						SET @remainingOfferValue = @remainingOfferValue - (CASE @tintOfferTriggerValueType WHEN  1 THEN @numTempUnitHour WHEN 2 THEN @monTempTotalAmount ELSE 0 END)

						IF @remainingOfferValue <= 0
						BEGIN
							BREAK
						END

						SET @j = @j + 1
					END

					SET @numAppliedPromotion = @numPromotionID
					BREAK
				END
			END

			SET @i = @i + 1
		END
	END

	-- CHECK IF Promotion discount can be applied to other items already added to cart.
	IF ISNULL(@numAppliedPromotion,0) > 0
	BEGIN
		DELETE FROM @TEMPItems
				
		INSERT INTO 
			@TEMPItems
		SELECT
			ROW_NUMBER() OVER(ORDER BY numCartID ASC),
			numCartId,
			CI.numItemCode,
			I.numItemClassification,
			CI.numUnitHour,
			CI.monPrice,
			(CI.numUnitHour * CI.monPrice)
		FROM
			CartItems CI
		INNER JOIN
			Item I
		ON
			CI.numItemCode = I.numItemCode
		WHERE
			CI.numDomainId=@numDomainId
			AND CI.numUserCntId=@numUserCntId
			AND CI.vcCookieId=@vcCookieId
			AND ISNULL(CI.PromotionID,0) = 0
			AND 1 = (CASE @tintDiscoutBaseOn
						WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemCode AND numProId=@numAppliedPromotion AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
						WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemClassification AND numProId=@numAppliedPromotion AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
						WHEN 3 THEN (CASE @tintOfferBasedOn 
								WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numAppliedPromotion AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
								WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@numAppliedPromotion AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
								ELSE 0
							END) 
						ELSE 0
					END)

		DECLARE @k AS INT = 1
		DECLARE @kCount AS INT 

		SELECT @kCount = COUNT(*) FROM @TEMPItems
		
		IF @kCount > 0
		BEGIN
			WHILE @k <= @kCount
			BEGIN
				SELECT 
					@numTempCartID=numCartID,
					@numTempItemCode=numItemCode,
					@numTempItemClassification=ISNULL(numItemClassification,0),
					@numTempUnitHour=numUnitHour,
					@monTempPrice=monPrice,
					@monTempTotalAmount = (numUnitHour * monPrice)
				FROM 
					@TEMPItems 
				WHERE 
					ID=@k


				IF @tintDiscountType=1 --Percentage
				BEGIN
					UPDATE 
						CartItems
					SET
						PromotionID=@numPromotionID,
						PromotionDesc=@vcPromotionDescription,
						bitParentPromotion=0,
						fltDiscount=@fltDiscountValue,
						bitDiscountType=0,
						monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
					WHERE
						numCartId=@numTempCartID
				END
				ELSE IF @tintDiscountType=2 --Flat Amount
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
					SELECT
						@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
					FROM
						CartItems
					WHERE
						PromotionID=@numAppliedPromotion

					IF @usedDiscountAmount < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount)  >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
						WHERE 
							numCartId=@numTempCartID
					END
					ELSE
					BEGIN
						BREAK
					END
				END
				ELSE IF @tintDiscountType=3 --Quantity
				BEGIN
					-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
					SELECT
						@usedDiscountQty = SUM(ISNULL(fltDiscount,0)/ISNULL(monPrice,1))
					FROM
						CartItems
					WHERE
						PromotionID=@numAppliedPromotion

					IF @usedDiscountQty < @fltDiscountValue
					BEGIN
						UPDATE 
							CartItems 
						SET 
							monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty)  >= numUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
							PromotionID=@numPromotionID ,
							PromotionDesc=@vcPromotionDescription,
							bitDiscountType=1,
							fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN @monTempTotalAmount ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
						WHERE 
							numCartId=@numTempCartID
					END
					ELSE
					BEGIN
						BREAK
					END
				END

				SET @k = @k + 1
			END
		END
	END
END