
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_SaveTopicMessageAttachments')
DROP PROCEDURE usp_SaveTopicMessageAttachments
GO
CREATE PROCEDURE [dbo].[usp_SaveTopicMessageAttachments]                                   
(
@numAttachmentId as NUMERIC(18,0)=0, 
@numTopicId as NUMERIC(18,0)=0, 
@numMessageId as NUMERIC(18,0)=0, 
@vcAttachmentName as varchar(500)='', 
@vcAttachmentUrl as varchar(500)='', 
@numCreatedBy as NUMERIC(18,0)=0,  
@numUpdatedBy as NUMERIC(18,0)=0,  
@numDomainId as NUMERIC(18,0)=0,
@status AS VARCHAR(100) OUTPUT
)                                    
AS                                                           
	IF @numAttachmentId = 0     
		BEGIN      
			INSERT INTO TopicMessageAttachments
			(
				numTopicId, numMessageId, vcAttachmentName, vcAttachmentUrl, numCreatedBy, dtmCreatedOn, numUpdatedBy, dtmUpdatedOn, numDomainId
			)
			VALUES
			(
				@numTopicId, @numMessageId, @vcAttachmentName, @vcAttachmentUrl, @numCreatedBy, GETDATE(), @numUpdatedBy, GETDATE(), @numDomainId
			)     
			SET @status='1'
		END      
		ELSE      
		BEGIN      
			UPDATE 
				TopicMessageAttachments 
			SET 
				numTopicId=@numTopicId, 
				numMessageId=@numMessageId, 
				vcAttachmentName=@vcAttachmentName, 
				vcAttachmentUrl=@vcAttachmentUrl, 
				numUpdatedBy=@numUpdatedBy, 
				dtmUpdatedOn=GETDATE(), 
				numDomainId=@numDomainId
			WHERE 
				numAttachmentId=@numAttachmentId 
				AND numDomainID=@numDomainID  
			SET @status='3'
		END
GO