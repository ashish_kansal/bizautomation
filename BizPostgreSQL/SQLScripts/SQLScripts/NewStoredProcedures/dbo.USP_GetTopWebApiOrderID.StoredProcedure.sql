/****** Object:  StoredProcedure [dbo].[USP_GetTopWebApiOrderID]    Script Date: 26/01/2013 012:40:30 ******/
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTopWebApiOrderID' ) 
    DROP PROCEDURE USP_GetTopWebApiOrderID
GO
--created by Joseph
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



CREATE PROC [dbo].[USP_GetTopWebApiOrderID]
          @numDomainID AS NUMERIC(9),
          @numWebApiId AS NUMERIC(9)

AS
  BEGIN

SELECT ISNULL((SELECT TOP 1 vcWebApiOrderId FROM WebApiOrderDetails 
WHERE numDomainId = @numDomainID AND WebApiId = @numWebApiId AND tintStatus = 0),0) AS WebApiOrderId

END 


