SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetReceivePayment' ) 
    DROP PROCEDURE USP_GetReceivePayment
GO
CREATE PROCEDURE [dbo].USP_GetReceivePayment
    @numDomainId AS NUMERIC(9) = 0 ,
    @numDepositID NUMERIC = 0,
    @numDivisionID NUMERIC(9) = 0,
    @numCurrencyID numeric(9),
    @numAccountClass NUMERIC(18,0)=0,
	@numCurrentPage INT=1
AS 
BEGIN
	DECLARE @numPageSize INT = 40

	DECLARE @numDiscountItemID AS NUMERIC(18,0)
	DECLARE @numShippingServiceItemID AS NUMERIC(18,0)

	SELECT @numDiscountItemID = ISNULL(numDiscountServiceItemID,0),@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0) FROM dbo.Domain WHERE numDomainID = @numDomainID
	PRINT @numDiscountItemID
			
	DECLARE @BaseCurrencySymbol 
	nVARCHAR(3);SET @BaseCurrencySymbol='$'
	SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId)
    
	SELECT [T].[numDivisionID],T.bitChild,dbo.[fn_GetComapnyName](T.[numDivisionID]) AS vcCompanyName,AD2.[vcCity]
	,isnull(dbo.fn_GetState(AD2.numState),'') AS vcState
	INTO #tempOrg
	FROM (SELECT @numDivisionID AS numDivisionID,cast(0 AS BIT) AS bitChild
	UNION
	SELECT [CA].[numDivisionID],CAST(1 AS BIT) AS bitChild
	FROM [dbo].[CompanyAssociations] AS CA 
	WHERE [CA].[numDomainID]=@numDomainID AND  ca.[numAssociateFromDivisionID]=@numDivisionID AND CA.[bitChildOrg]=1) AS T
	LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=@numDomainID 
	AND AD2.numRecordID= T.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppBizDocID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numOppBizDocID
	)
	SELECT 
		numOppBizDocsID
	FROM
	(
		SELECT
			OpportunityBizdocs.numOppBizDocsID
			,T.bitChild
			,OpportunityBizdocs.dtCreatedDate
		FROM    
			OpportunityMaster
		INNER JOIN 
			[#tempOrg] AS T 
		ON 
			OpportunityMaster.[numDivisionId] = T.[numDivisionID]
		INNER JOIN 
			OpportunityBizdocs
		ON
			OpportunityMaster.[numoppid] = OpportunityBizdocs.[numoppid]
		INNER JOIN
			DepositeDetails 
		ON 
			DepositeDetails.numOppBizDocsID = OpportunityBizdocs.numOppBizDocsID
		WHERE   
			OpportunityBizdocs.[bitauthoritativebizdocs] = 1
			AND OpportunityMaster.tintOppType = 1
			AND OpportunityMaster.numDomainID = @numDomainID
			AND DepositeDetails.numDepositID = @numDepositID
			AND (ISNULL(OpportunityMaster.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		UNION
		SELECT
			numOppBizDocsID
			,T.bitChild
			,OpportunityBizdocs.dtCreatedDate
		FROM 
			OpportunityMaster
		INNER JOIN
			OpportunityBizdocs 
		ON 
			OpportunityMaster.[numoppid] = OpportunityBizdocs.[numoppid]
		INNER JOIN 
			[#tempOrg] AS T 
		ON 
			OpportunityMaster.[numDivisionId] = T.[numDivisionID]
		WHERE
			OpportunityMaster.numDomainID = @numDomainID
			AND OpportunityMaster.tintOppType = 1
			AND (OpportunityMaster.numCurrencyID = @numCurrencyID OR  @numCurrencyID = 0 )
			AND (ISNULL(OpportunityMaster.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
			AND OpportunityBizdocs.[bitauthoritativebizdocs] = 1
			AND (@numDepositID= 0 OR (@numDepositID > 0 AND T.bitChild = 0))
			AND OpportunityBizdocs.monDealAmount - OpportunityBizdocs.monAmountPaid > 0
			AND (SELECT COUNT(*) FROM DepositeDetails WHERE numOppID=OpportunityMaster.numOppId AND numOppBizDocsId=OpportunityBizdocs.numOppBizDocsID AND numDepositID=@numDepositID) = 0
			
	) TEMP
	ORDER BY 
		bitChild,dtCreatedDate DESC


	DECLARE @TEMPFinal TABLE
	(
		numOppBizDocID NUMERIC(18,0)
	)

	INSERT INTO @TEMPFinal SELECT numOppBizDocID FROM @TEMP ORDER BY ID OFFSET (@numCurrentPage - 1) * @numPageSize ROWS FETCH NEXT @numPageSize ROWS ONLY


	SELECT  
		om.vcPOppName ,
		om.numoppid ,
		OBD.[numoppbizdocsid] ,
		OBD.[vcbizdocid] ,
		OBD.[numbizdocstatus] ,
		OBD.[vccomments] ,
		OBD.monAmountPaid AS monAmountPaid ,
		OBD.monDealAmount ,
		OBD.monCreditAmount monCreditAmount ,
		ISNULL(C.varCurrSymbol, '') varCurrSymbol ,
		[dbo].[FormatedDateFromDate](dtFromDate, @numDomainID) dtFromDate ,
		dtFromDate AS [dtOrigFromDate],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1
			--THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays,0)), 0)), dtFromDate), 1)
			THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)), dtFromDate), @numDomainID)
			WHEN 0
			THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate], @numDomainID)
		END AS dtDueDate ,
		om.numDivisionID ,
                
		(OBD.monDealAmount - ISNULL(OBD.monCreditAmount, 0) - ISNULL( OBD.monAmountPaid , 0) + ISNULL(DD.monAmountPaid,0)  ) AS baldue ,
		T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
		DD.numDepositID ,
		DD.monAmountPaid monAmountPaidInDeposite ,
		ISNULL(DD.numDepositeDetailID,0) numDepositeDetailID,
		obd.dtCreatedDate,
		OM.numContactId,obd.vcRefOrderNo
		,OM.numCurrencyID
		,ISNULL(NULLIF(C.fltExchangeRate,0),1) fltExchangeRateCurrent
		,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
		,@BaseCurrencySymbol BaseCurrencySymbol,om.vcPOppName,
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numDiscount],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numDiscountPaidInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numDiscountPaidInDays],
		CASE ISNULL(om.bitBillingTerms, 0)
			WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
			ELSE 0
		END AS [numNetDueInDays],
		CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityBizDocItems OBDI
					JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
					WHERE 1=1 AND numOppId = om.[numoppid] AND numOppBizDocID = obd.numOppBizDocsId 
					AND OBDI.numItemCode = @numDiscountItemID
					AND OI.vcItemDesc LIKE '%Term Discount%') > 0 THEN 1 ELSE 0 END AS [IsDiscountItemAdded],
		(SELECT ISNULL(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsID AND numItemCode = @numShippingServiceItemID) AS monShippingCharge
		,ISNULL(numARAccountID,0) numARAccountID
	FROM    opportunitymaster om
			INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = OBD.[numoppid]
			INNER JOIN @TEMPFinal T1 ON obd.numOppBizDocsId = T1.numOppBizDocID
			INNER JOIN [#tempOrg] AS T ON OM.[numDivisionId] = T.[numDivisionID]
			LEFT OUTER JOIN [ListDetails] ld ON ld.[numListItemID] = OBD.[numbizdocstatus]
			LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = om.numCurrencyID
			JOIN DepositeDetails DD ON DD.numOppBizDocsID = OBD.numOppBizDocsID
	WHERE   
		om.numDomainID = @numDomainID
		AND om.tintOppType = 1
		AND OBD.[bitauthoritativebizdocs] = 1
		AND (ISNULL(OM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		AND DD.numDepositID = @numDepositID
	UNION
	SELECT  om.vcPOppName ,
			om.numoppid ,
			OBD.[numoppbizdocsid] ,
			OBD.[vcbizdocid] ,
			OBD.[numbizdocstatus] ,
			OBD.[vccomments] ,
			OBD.monAmountPaid AS monAmountPaid ,
			OBD.monDealAmount ,
			OBD.monCreditAmount monCreditAmount ,
			ISNULL(C.varCurrSymbol, '') varCurrSymbol ,
			[dbo].[FormatedDateFromDate](dtFromDate, @numDomainID) dtFromDate ,
			dtFromDate AS [dtOrigFromDate],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1
				--THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays,0)), 0)),dtFromDate), @numDomainID)
				THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)),dtFromDate), @numDomainID)
				WHEN 0
				THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate], @numDomainID)
			END AS dtDueDate ,
			om.numDivisionID ,
			(OBD.monDealAmount - ISNULL(monCreditAmount, 0) - ISNULL( OBD.monAmountPaid , 0) ) AS baldue ,
			T.vcCompanyName ,T.vcCity,T.vcState,T.bitChild,
			0 numDepositID ,
			0 monAmountPaidInDeposite ,
			0 numDepositeDetailID,
			dtCreatedDate,
			OM.numContactId,obd.vcRefOrderNo
			,OM.numCurrencyID
			,ISNULL(NULLIF(C.fltExchangeRate,0),1) fltExchangeRateCurrent
			,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
			,@BaseCurrencySymbol BaseCurrencySymbol,om.vcPOppName,
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numDiscount FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numDiscount],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numDiscountPaidInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numDiscountPaidInDays],
			CASE ISNULL(om.bitBillingTerms, 0)
				WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE om.intBillingDays = numTermsID AND numDomainID = om.numDomainID)
				ELSE 0
			END AS [numNetDueInDays],
			CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityBizDocItems OBDI
						JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
						WHERE 1=1 AND numOppId = om.[numoppid] AND numOppBizDocID = obd.numOppBizDocsId 
						AND OBDI.numItemCode = @numDiscountItemID
						AND OI.vcItemDesc LIKE '%Term Discount%') > 0 THEN 1 ELSE 0 END AS [IsDiscountItemAdded],
			(SELECT ISNULL(SUM(monTotAmount),0) FROM OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsID AND numItemCode = @numShippingServiceItemID) AS monShippingCharge
			,ISNULL(numARAccountID,0) numARAccountID
	FROM    opportunitymaster om
			INNER JOIN [opportunitybizdocs] obd ON om.[numoppid] = OBD.[numoppid]
			INNER JOIN @TEMPFinal T1 ON obd.numOppBizDocsId = T1.numOppBizDocID
			INNER JOIN [#tempOrg] AS T ON OM.[numDivisionId] = T.[numDivisionID]
			LEFT OUTER JOIN [ListDetails] ld ON ld.[numListItemID] = OBD.[numbizdocstatus]
			LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = om.numCurrencyID
	WHERE   
		om.numDomainID = @numDomainID
		AND om.tintOppType = 1
		AND OBD.[bitauthoritativebizdocs] = 1
		AND (OM.numCurrencyID = @numCurrencyID OR  @numCurrencyID = 0 )
		AND (ISNULL(OM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		AND (@numDepositID= 0 OR (@numDepositID > 0 AND T.bitChild = 0))
		AND OBD.monDealAmount - OBD.monAmountPaid > 0
		AND (SELECT COUNT(*) FROM DepositeDetails WHERE numOppID=OM.numOppId AND numOppBizDocsId=OBD.numOppBizDocsID AND numDepositID=@numDepositID) = 0		
	ORDER BY 
		T.bitChild,dtCreatedDate DESC
	
	SELECT COUNT(*) AS TotalRecords,@numPageSize AS PageSize FROM @TEMP

	DROP TABLE #tempOrg
END


