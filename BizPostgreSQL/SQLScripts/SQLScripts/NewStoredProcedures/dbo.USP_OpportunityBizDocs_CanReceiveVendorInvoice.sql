GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_CanReceiveVendorInvoice')
DROP PROCEDURE USP_OpportunityBizDocs_CanReceiveVendorInvoice
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_CanReceiveVendorInvoice]
(
	@numVendorInvoiceBizDocID NUMERIC(18,0)
)
AS
BEGIN
	IF (SELECT
			COUNT(*)
		FROM
			OpportunityBizDocItems OBDI
		INNER JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID=OI.numoppitemtCode
		WHERE
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND (ISNULL(OBDI.numUnitHour,0) - ISNULL(OBDI.numVendorInvoiceUnitReceived,0)) > (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))
			) > 0
	BEGIN
		SELECT 0
	END
	ELSE
	BEGIN
		SELECT 1
	END
END