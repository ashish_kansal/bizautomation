GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateCommunicationAttendees_Status')
DROP PROCEDURE USP_UpdateCommunicationAttendees_Status
GO
CREATE PROCEDURE [dbo].[USP_UpdateCommunicationAttendees_Status]
@numCommId AS numeric(9)=0,
@numUserCntId AS NUMERIC(9)=0,
@numStatus AS NUMERIC(9)=0
AS
BEGIN
 UPDATE CommunicationAttendees SET numStatus=@numStatus where 
   numCommId=@numCommId AND numContactId=@numUserCntId
END