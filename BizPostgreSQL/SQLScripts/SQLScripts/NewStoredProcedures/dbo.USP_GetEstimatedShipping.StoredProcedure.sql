set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



-- USP_GetEstimateShipping 1,18,4626,1596,0,0,89658

ALTER PROCEDURE [dbo].[USP_GetEstimateShipping]
    @numDomainID NUMERIC,
    @numSiteID NUMERIC,
    @numCountryId NUMERIC(18,0),
    @numStateId NUMERIC(18,0)
    --kishan
    --@numItemID NUMERIC,
    --@numCategoryID NUMERIC,
    --@numRelationship NUMERIC,
    --@numProfile NUMERIC,
    --@numDivisionID NUMERIC
    --kishan
AS 
BEGIN

--New
IF EXISTS(SELECT * FROM dbo.ShippingRules SR 
		  LEFT JOIN dbo.ShippingServiceTypes SST ON SR.numRuleID = SST.numRuleID 
		  LEFT JOIN dbo.ShippingRuleStateList SRSL ON SR.numRuleID =  SRSL.numRuleId 
		  WHERE   SR.numDomainId = @numDomainId 
		  AND (SR.numSiteId = @numSiteId OR ISNULL(@numSiteId,0) = -1) 
		  AND SST.bitEnabled = 1  
		  AND (((SRSL.numCountryId = @numCountryId) AND (SRSL.numStateId = @numStateId))))

			SELECT SST.numServiceTypeID, 
	        SRSL.numCountryId ,
	        SRSL.numStateId,
            SR.numRuleID ,
            SR.numDomainId,
            SST.numShippingCompanyID ,
            SR.numSiteId,
            SR.tinShippingMethod ,
            SR.tintTaxMode , 
            SR.tintFixedShippingQuotesMode ,
            SST.vcServiceName , 
            SST.monRate ,
            SST.intNsoftEnum,
            SST.intFrom,
            SST.intTo,
            SST.bitEnabled,
             SST.fltMarkup,
            SST.bitMarkUpType 
           FROM dbo.ShippingRules SR 
           LEFT JOIN dbo.ShippingServiceTypes SST ON SR.numRuleID = SST.numRuleID 
           LEFT JOIN dbo.ShippingRuleStateList SRSL ON SR.numRuleID =  SRSL.numRuleId
		  
		   WHERE   SR.numDomainId = @numDomainId 
		   AND (SR.numSiteId = @numSiteId OR ISNULL(@numSiteId,0) = -1)
           AND SST.bitEnabled = 1  
           AND 
			(		
				1  = CASE WHEN ISNULL(SRSL.numCountryId,0) = @numCountryId AND ISNULL(SRSL.numStateId,0) = 0
						  THEN 1 
						  ELSE 0 
					 END
			)
			OR
			(	
				1  = CASE WHEN (ISNULL(SRSL.numCountryId,0)  = @numCountryId AND ISNULL(SRSL.numStateId,0) = @numStateId) 
						  THEN 1
						  ELSE 0
					 END
			) 
		   --AND SRSL.numCountryId = @numCountryId AND SRSL.numStateId = @numStateId
ELSE
           SELECT SST.numServiceTypeID, 
	        SRSL.numCountryId ,
	        SRSL.numStateId,
            SR.numRuleID ,
            SR.numDomainId,
            SST.numShippingCompanyID ,
            SR.numSiteId,
            SR.tinShippingMethod ,
            SR.tintTaxMode , 
            SR.tintFixedShippingQuotesMode ,
            SST.vcServiceName , 
            SST.monRate ,
            SST.intNsoftEnum,
            SST.intFrom,
            SST.intTo,
            SST.fltMarkup,
            SST.bitMarkUpType,
            SST.bitEnabled       
           FROM dbo.ShippingRules SR 
           LEFT JOIN dbo.ShippingServiceTypes SST ON SR.numRuleID = SST.numRuleID 
           LEFT JOIN dbo.ShippingRuleStateList SRSL ON SR.numRuleID =  SRSL.numRuleId
		  
		   WHERE   SR.numDomainId = @numDomainId 
		   AND (ISNULL(SR.numSiteId,0) = @numSiteId OR ISNULL(SR.numSiteId,0) = -1 OR ISNULL(@numSiteId,0) = -1)
           AND ISNULL(SST.bitEnabled,0) = 1  
           AND 
				(
					(
					  (
						ISNULL(SRSL.numCountryId,0) = @numCountryId OR 
						ISNULL(SRSL.numCountryId,0) = 0 OR 
					    ISNULL(@numCountryId,0) = 0
					  ) 
					  AND 
					  (
					    ISNULL(SRSL.numStateId,0) = @numStateId OR
					    ISNULL(SRSL.numStateId,0) = 0  OR 
					    ISNULL(@numStateId,0) = 0
					  ) 
					)
--					OR
--					(
--					  (
--						ISNULL(SRSL.numCountryId,0) = @numCountryId OR
--						ISNULL(SRSL.numCountryId,0) = 0 OR 
--						ISNULL(@numCountryId,0) = 0
--					  )
--					  AND 
--					  (
--						ISNULL(SRSL.numStateId,0) = @numStateId OR 
--						ISNULL(@numStateId,0) = 0) 
--					  )
				)
		   --AND (((SRSL.numCountryId = @numCountryId) AND (SRSL.numStateId = @numStateId)) OR ((SRSL.numCountryId = @numCountryId) AND (SRSL.numStateId = 0)) ) 
--exec USP_GetEstimateShipping @numDomainID=1,@numSiteID=106,@numCountryId=600,@numStateId=0

--DECLARE @numRuleID NUMERIC
--        
----Step 1 : Select a rule id based on criteria
--            
---- Individual Item            
--SELECT TOP 1 @numRuleID=RuleID FROM
--(
--SELECT numProId AS RuleID,3 AS Priority FROM dbo.PromotionOfferItems WHERE numValue=@numItemID AND tintRecordType=2 AND tintType=1
--UNION 
---- Item Category
--SELECT numProId AS RuleID,2 AS Priority FROM dbo.PromotionOfferItems WHERE numValue=@numItemID AND tintRecordType=2 AND tintType=2
--) AS X 
--ORDER BY Priority DESC
--
----Individual Organization
--SELECT numProId AS RuleID ,3 AS Priority FROM dbo.PromotionOfferContacts WHERE numValue = @numDivisionID AND tintRecordType=2 AND tintType=1
----Relationship & Profile
--SELECT numProId AS RuleID,2 AS Priority FROM dbo.PromotionOfferContacts WHERE numValue =@numRelationship AND numProfile=@numItemID AND tintRecordType=2 AND tintType=2
--
--
----Step 2 : Select data of given Rule ID
--
--SELECT * FROM ShippingRules SR
--LEFT JOIN dbo.PromotionOfferItems SI
--ON SI.numProId = SR.numRuleID
--WHERE tintAppliesTo=1 AND numValue=@numItemID AND tintRecordType=2 AND tintType=1
--
--
--
--    SELECT  numServiceTypeID,
--            vcServiceName
--    FROM    dbo.ShippingServiceTypes SST
--            INNER JOIN dbo.ShippingRules SR ON SST.numRuleID = SR.numRuleID
--                                               AND SST.numDomainID = SR.numDomainID
--    WHERE   bitEnabled = 1
--            AND SST.numDomainID = @numDomainID
--            AND SST.intNsoftEnum = 0
--            AND SR.numSiteID = @numSiteID
--            AND SR.numRuleID= @numRuleID

--kishan
--SELECT DISTINCT SST.numShippingCompanyID,
--				SST.numServiceTypeID,
--				SST.vcServiceName,
--				SST.intNsoftEnum,
--				SST.intFrom,
--				SST.intTo,
--				SST.fltMarkup,
--				SST.bitMarkupType,
--				SST.monRate
--				
--FROM   dbo.ShippingServiceTypes SST
--            INNER JOIN dbo.ShippingRules SR ON SST.numRuleID = SR.numRuleID
--                                               AND SST.numDomainID = SR.numDomainID
--        LEFT JOIN dbo.PromotionOfferContacts POC ON SR.numRuleID = POC.numProID
--        LEFT JOIN dbo.PromotionOfferItems POI ON SR.numRuleID = POI.numProId
--WHERE
--SST.numDomainID=@numDomainID
--AND SR.numSiteID=@numSiteID
--AND SST.bitEnabled=1
--AND  
--(
--((tintAppliesTo = 1 AND POI.numValue = @numItemID) AND (tintContactsType = 1 AND POC.numValue = @numDivisionID)) -- Priority 1
--OR ((tintAppliesTo = 2 AND POI.numValue = @numCategoryID) AND (tintContactsType= 1 AND POC.numValue = @numDivisionID)) -- Priority 2
--OR ((tintAppliesTo = 3) AND (tintContactsType = 1 AND POC.numValue = @numDivisionID)) -- Priority 3
--
--OR ((tintAppliesTo = 1 AND POI.numValue = @numItemID) AND (tintContactsType = 2 AND POC.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
--OR ((tintAppliesTo = 2 AND POI.numValue = @numCategoryID) AND (tintContactsType = 2 AND POC.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
--OR ((tintAppliesTo = 3) AND (tintContactsType = 2 AND POC.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6
--
--OR ((tintAppliesTo = 1 AND POI.numValue = @numItemID) AND (tintContactsType = 3)) -- Priority 7
--OR ((tintAppliesTo = 2 AND POI.numValue = @numCategoryID) AND (tintContactsType = 3)) -- Priority 8
--OR ((tintAppliesTo = 3) and (tintContactsType = 3)) -- Priority 9
--)
--ORDER BY PP.Priority ASC
      END      



------------------------------------------------------------------------------------------
------GO
------IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShipping')
------DROP PROCEDURE USP_GetEstimateShipping
------GO
------
-------- USP_GetEstimateShipping 1,18,4626,1596,0,0,89658
------
------CREATE PROCEDURE USP_GetEstimateShipping
------    @numDomainID NUMERIC,
------    @numSiteID NUMERIC,
------    @numCountryId NUMERIC(18,0),
------    @numStateId NUMERIC(18,0)
------    --kishan
------    --@numItemID NUMERIC,
------    --@numCategoryID NUMERIC,
------    --@numRelationship NUMERIC,
------    --@numProfile NUMERIC,
------    --@numDivisionID NUMERIC
------    --kishan
------AS 
------BEGIN
------
--------New
------IF EXISTS(SELECT * FROM dbo.ShippingRules SR LEFT JOIN dbo.ShippingServiceTypes SST ON SR.numRuleID = SST.numRuleID LEFT JOIN dbo.ShippingRuleStateList SRSL ON SR.numRuleID =  SRSL.numRuleId WHERE   SR.numDomainId = @numDomainId AND (SR.numSiteId = @numSiteId OR ISNULL(@numSiteId,0) = -1) AND SST.bitEnabled = 1  AND (((SRSL.numCountryId = @numCountryId) AND (SRSL.numStateId = @numStateId))))
------			SELECT SST.numServiceTypeID, 
------	        SRSL.numCountryId ,
------	        SRSL.numStateId,
------            SR.numRuleID ,
------            SR.numDomainId,
------            SST.numShippingCompanyID ,
------            SR.numSiteId,
------            SR.tinShippingMethod ,
------            SR.tintTaxMode , 
------            SR.tintFixedShippingQuotesMode ,
------            SST.vcServiceName , 
------            SST.monRate ,
------            SST.intNsoftEnum,
------            SST.intFrom,
------            SST.intTo,
------            SST.bitEnabled,
------             SST.fltMarkup,
------            SST.bitMarkUpType 
------           FROM dbo.ShippingRules SR 
------           LEFT JOIN dbo.ShippingServiceTypes SST ON SR.numRuleID = SST.numRuleID 
------           LEFT JOIN dbo.ShippingRuleStateList SRSL ON SR.numRuleID =  SRSL.numRuleId
------		  
------		   WHERE   SR.numDomainId = @numDomainId 
------		   AND (SR.numSiteId = @numSiteId OR ISNULL(@numSiteId,0) = -1)
------           AND SST.bitEnabled = 1  
------           AND SRSL.numCountryId = @numCountryId AND SRSL.numStateId = @numStateId   
------ELSE
------           SELECT SST.numServiceTypeID, 
------	        SRSL.numCountryId ,
------	        SRSL.numStateId,
------            SR.numRuleID ,
------            SR.numDomainId,
------            SST.numShippingCompanyID ,
------            SR.numSiteId,
------            SR.tinShippingMethod ,
------            SR.tintTaxMode , 
------            SR.tintFixedShippingQuotesMode ,
------            SST.vcServiceName , 
------            SST.monRate ,
------            SST.intNsoftEnum,
------            SST.intFrom,
------            SST.intTo,
------            SST.fltMarkup,
------            SST.bitMarkUpType,
------            SST.bitEnabled 
--------       CASE WHEN EXISTS (SELECT numRuleStateId FROM ShippingRuleStateList WHERE numRuleId =SR.numRuleId AND numCountryId = @numCountryId)
--------                 THEN 'TRUE' 
--------            ELSE 'FALSE' 
--------       END AS IsValid
------       
------           FROM dbo.ShippingRules SR 
------           LEFT JOIN dbo.ShippingServiceTypes SST ON SR.numRuleID = SST.numRuleID 
------           LEFT JOIN dbo.ShippingRuleStateList SRSL ON SR.numRuleID =  SRSL.numRuleId
------		  
------		   WHERE   SR.numDomainId = @numDomainId 
------		   AND (SR.numSiteId = @numSiteId  OR ISNULL(@numSiteId,0) = -1)
------           AND SST.bitEnabled = 1  
------           AND SRSL.numCountryId = @numCountryId AND SRSL.numStateId = 0  
------    
------     AND (((SRSL.numCountryId = @numCountryId) AND (SRSL.numStateId = @numStateId)) OR ((SRSL.numCountryId = @numCountryId) AND (SRSL.numStateId = 0)) ) 
------     AND SRSL.numCountryId = @numCountryId AND SRSL.numStateId = 0  
--------exec USP_GetEstimateShipping @numDomainID=1,@numSiteID=106,@numCountryId=600,@numStateId=0
------
--------DECLARE @numRuleID NUMERIC
--------        
----------Step 1 : Select a rule id based on criteria
--------            
---------- Individual Item            
--------SELECT TOP 1 @numRuleID=RuleID FROM
--------(
--------SELECT numProId AS RuleID,3 AS Priority FROM dbo.PromotionOfferItems WHERE numValue=@numItemID AND tintRecordType=2 AND tintType=1
--------UNION 
---------- Item Category
--------SELECT numProId AS RuleID,2 AS Priority FROM dbo.PromotionOfferItems WHERE numValue=@numItemID AND tintRecordType=2 AND tintType=2
--------) AS X 
--------ORDER BY Priority DESC
--------
----------Individual Organization
--------SELECT numProId AS RuleID ,3 AS Priority FROM dbo.PromotionOfferContacts WHERE numValue = @numDivisionID AND tintRecordType=2 AND tintType=1
----------Relationship & Profile
--------SELECT numProId AS RuleID,2 AS Priority FROM dbo.PromotionOfferContacts WHERE numValue =@numRelationship AND numProfile=@numItemID AND tintRecordType=2 AND tintType=2
--------
--------
----------Step 2 : Select data of given Rule ID
--------
--------SELECT * FROM ShippingRules SR
--------LEFT JOIN dbo.PromotionOfferItems SI
--------ON SI.numProId = SR.numRuleID
--------WHERE tintAppliesTo=1 AND numValue=@numItemID AND tintRecordType=2 AND tintType=1
--------
--------
--------
--------    SELECT  numServiceTypeID,
--------            vcServiceName
--------    FROM    dbo.ShippingServiceTypes SST
--------            INNER JOIN dbo.ShippingRules SR ON SST.numRuleID = SR.numRuleID
--------                                               AND SST.numDomainID = SR.numDomainID
--------    WHERE   bitEnabled = 1
--------            AND SST.numDomainID = @numDomainID
--------            AND SST.intNsoftEnum = 0
--------            AND SR.numSiteID = @numSiteID
--------            AND SR.numRuleID= @numRuleID
------
--------kishan
--------SELECT DISTINCT SST.numShippingCompanyID,
--------				SST.numServiceTypeID,
--------				SST.vcServiceName,
--------				SST.intNsoftEnum,
--------				SST.intFrom,
--------				SST.intTo,
--------				SST.fltMarkup,
--------				SST.bitMarkupType,
--------				SST.monRate
--------				
--------FROM   dbo.ShippingServiceTypes SST
--------            INNER JOIN dbo.ShippingRules SR ON SST.numRuleID = SR.numRuleID
--------                                               AND SST.numDomainID = SR.numDomainID
--------        LEFT JOIN dbo.PromotionOfferContacts POC ON SR.numRuleID = POC.numProID
--------        LEFT JOIN dbo.PromotionOfferItems POI ON SR.numRuleID = POI.numProId
--------WHERE
--------SST.numDomainID=@numDomainID
--------AND SR.numSiteID=@numSiteID
--------AND SST.bitEnabled=1
--------AND  
--------(
--------((tintAppliesTo = 1 AND POI.numValue = @numItemID) AND (tintContactsType = 1 AND POC.numValue = @numDivisionID)) -- Priority 1
--------OR ((tintAppliesTo = 2 AND POI.numValue = @numCategoryID) AND (tintContactsType= 1 AND POC.numValue = @numDivisionID)) -- Priority 2
--------OR ((tintAppliesTo = 3) AND (tintContactsType = 1 AND POC.numValue = @numDivisionID)) -- Priority 3
--------
--------OR ((tintAppliesTo = 1 AND POI.numValue = @numItemID) AND (tintContactsType = 2 AND POC.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
--------OR ((tintAppliesTo = 2 AND POI.numValue = @numCategoryID) AND (tintContactsType = 2 AND POC.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
--------OR ((tintAppliesTo = 3) AND (tintContactsType = 2 AND POC.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6
--------
--------OR ((tintAppliesTo = 1 AND POI.numValue = @numItemID) AND (tintContactsType = 3)) -- Priority 7
--------OR ((tintAppliesTo = 2 AND POI.numValue = @numCategoryID) AND (tintContactsType = 3)) -- Priority 8
--------OR ((tintAppliesTo = 3) and (tintContactsType = 3)) -- Priority 9
--------)
--------ORDER BY PP.Priority ASC
------            
------
------END
