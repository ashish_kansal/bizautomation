/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItems]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMasterListItemsUsingListType')
DROP PROCEDURE USP_GetMasterListItemsUsingListType
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItemsUsingListType]          
@ListID as numeric(9)=0,        
@numDomainID as numeric(9)=0,
@numListType numeric(9,0),
@tintOppOrOrder TINYINT     
as          
BEGIN
	IF @ListID = 176
	BEGIN
		DECLARE @bitEDI BIT
		DECLARE @bit3PL BIT
		SELECT @bitEDI=ISNULL(bitEDI,0),@bit3PL=ISNULL(bit3PL,0) FROM Domain WHERE numDomainId=@numDomainID

		SELECT Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData,ISNULL(ld.numListType,0) as numListType FROM listdetails Ld        
		left join listorder LO 
		on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
		WHERE Ld.numListID=@ListID and (constFlag=1 or Ld.numDomainID=@numDomainID )
		and ISNULL(ld.numListType,0) in (0,@numListType) AND ISNULL(ld.tintOppOrOrder,0) IN (0,@tintOppOrOrder)
		AND 1 = (CASE WHEN Ld.numListItemID IN (15445,15446) THEN (CASE WHEN @bit3PL=1 THEN 1 ELSE 0 END) WHEN Ld.numListItemID IN (15447,15448) THEN (CASE WHEN @bitEDI=1 THEN 1 ELSE 0 END) ELSE 1 END)
		order by ISNULL(intSortOrder,LD.sintOrder)

	END
	ELSE
	BEGIN
		SELECT Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData,ISNULL(ld.numListType,0) as numListType FROM listdetails Ld        
		left join listorder LO 
		on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
		WHERE Ld.numListID=@ListID and (constFlag=1 or Ld.numDomainID=@numDomainID )
		and ISNULL(ld.numListType,0) in (0,@numListType) AND ISNULL(ld.tintOppOrOrder,0) IN (0,@tintOppOrOrder)
		order by ISNULL(intSortOrder,LD.sintOrder)
	END



END
--select * from ListDetails


--SELECT Ld.numListItemID, vcData FROM listdetails Ld        
--left join listorder LO on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID   
--WHERE Ld.numListID=@ListID and (constFlag=1 or Ld.numDomainID=@numDomainID)      
--order by intSortOrder
GO
