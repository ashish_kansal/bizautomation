SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemTransit')
DROP PROCEDURE USP_GetItemTransit
GO
CREATE PROCEDURE [dbo].[USP_GetItemTransit] 
( 
@numDomainID as numeric(9)=0,    
@numItemcode AS NUMERIC(9)=0,
@numVendorID AS NUMERIC(9)=0,
@numAddressID AS NUMERIC(9)=0,
@tintMode AS TINYINT,
@ClientTimeZoneOffset      AS INT
)
AS 
--tintOppStatus = 1 -> Won 
--[tintOppType] = 2 AND [tintOppStatus] = 1 -> Purchase Order 
--tintShipped = 0 

IF @tintMode=1
BEGIN
select dbo.fn_GetItemTransitCount(@numItemcode,@numDomainID) AS Total
--	select COUNT(*) AS Total
--      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId 
--		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
--		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
--           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
--				 and Opp.numDomainId=@numDomainID AND OppI.numItemcode=@numItemcode
--				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)

END

ELSE IF @tintMode=2
BEGIN 
select opp.numOppId,Opp.vcPOppName,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' else dbo.FormatedDateFromDate(Opp.bintCreatedDate,1) end  bintCreatedDate,
dbo.fn_GetContactName(Opp.numContactId) AS vcOrderedBy,OppI.vcItemName,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),OppI.numItemCode,OPP.numDomainId,ISNULL(OppI.numUOMId,0)) * (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0))) as numeric(18,0)) as numUnitHour
,ISNULL(u.vcUnitName,'') vcUOMName,OppI.vcManufacturer,ISNULL(OppI.vcItemDesc,'') as vcItemDesc,OppI.vcModelID,
dbo.fn_GetListItemName(Opp.intUsedShippingCompany) vcShipmentMethod,
CASE WHEN ISNULL(VSM.numListValue,0)>0 THEN 
case when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	 else dbo.FormatedDateFromDate(DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)),@numDomainID) end 
	 ELSE '' end bintExpectedDelivery
      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId  LEFT JOIN WareHouseItems WI ON OppI.numWarehouseItmsID=WI.numWareHouseItemID
		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
		   OUTER APPLY (SELECT TOP 1 * FROM VendorShipmentMethod VSM WHERE VSM.numAddressID=Opp.numVendorAddressID AND VSM.numVendorID=Opp.numDivisionId ORDER BY (CASE WHEN VSM.numWarehouseID=WI.numWareHouseID THEN 1 ELSE 0 END) DESC, ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC) VSM
           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
				 and Opp.numDomainId=@numDomainID AND OppI.numItemcode=@numItemcode
				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)
END

ELSE IF @tintMode=3
BEGIN 
select opp.numOppId,Opp.vcPOppName,case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' else dbo.FormatedDateFromDate(Opp.bintCreatedDate,1) end  bintCreatedDate,
dbo.fn_GetContactName(Opp.numContactId) AS vcOrderedBy,OppI.vcItemName,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),OppI.numItemCode,OPP.numDomainId,ISNULL(OppI.numUOMId,0)) * (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0))) as numeric(18,0)) as numUnitHour
,ISNULL(u.vcUnitName,'') vcUOMName,OppI.vcManufacturer,ISNULL(OppI.vcItemDesc,'') as vcItemDesc,OppI.vcModelID,
dbo.fn_GetListItemName(OppI.numShipmentMethod) vcShipmentMethod,
CASE WHEN ISNULL(VSM.numListValue,0)>0 THEN 
case when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	 else dbo.FormatedDateFromDate(DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)),1) end 
	 ELSE '' end bintExpectedDelivery
      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId LEFT JOIN WareHouseItems WI ON OppI.numWarehouseItmsID=WI.numWareHouseItemID
		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
		   OUTER APPLY (SELECT TOP 1 * FROM VendorShipmentMethod VSM WHERE VSM.numAddressID=Opp.numVendorAddressID AND VSM.numVendorID=Opp.numDivisionId ORDER BY (CASE WHEN VSM.numWarehouseID=WI.numWareHouseID THEN 1 ELSE 0 END) DESC, ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC) VSM
           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
				 and Opp.numDomainId=@numDomainID AND Opp.numDivisionId=@numVendorID AND OppI.numVendorWareHouse=@numAddressID
				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)
END