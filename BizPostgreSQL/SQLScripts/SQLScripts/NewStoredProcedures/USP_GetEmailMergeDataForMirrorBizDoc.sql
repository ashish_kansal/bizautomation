

-- =============================================
-- Author:		Priya
-- Create date: 5 March 2018
-- Description:	Email Merge Data for Mirror Biz Doc
-- =============================================

GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmailMergeDataForMirrorBizDoc')
DROP PROCEDURE USP_GetEmailMergeDataForMirrorBizDoc
GO
CREATE PROCEDURE [dbo].[USP_GetEmailMergeDataForMirrorBizDoc]
 (
      @numReferenceID NUMERIC(9) = 0,
      @numReferenceType TINYINT,
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @ClientTimeZoneOffset INT
    )
AS 
    BEGIN
        DECLARE @numBizDocType AS NUMERIC
        
        IF @numReferenceType = 1
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
        ELSE IF @numReferenceType = 2
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
        ELSE IF @numReferenceType = 3
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 4
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 5 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		ELSE IF @numReferenceType = 6 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		ELSE IF @numReferenceType = 7 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 8
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 9
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		ELSE IF @numReferenceType = 10
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 11
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE vcData='Invoice' AND constFlag=1

IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4 
	BEGIN
		SELECT ACI.vcFirstName ContactFirstName,
                ACI.vcLastName ContactLastName,
				CMP.vcCompanyName OrganizationName,
				'' As BizDocTemplateFromGlobalSettings
		 FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID						 
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						 
				  WHERE  Mst.numOppID = @numReferenceID AND Mst.numDomainID = @numDomainID
	END

ELSE IF @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 7 OR @numReferenceType = 8 
					OR @numReferenceType = 9 OR @numReferenceType = 10
    BEGIN
		DECLARE @numBizDocTempID AS NUMERIC(9)
		DECLARE @numDivisionID AS NUMERIC(9)
         
        SELECT @numDivisionID=numDivisionID,@numBizDocTempID=(CASE WHEN @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 9 OR @numReferenceType = 10 THEN numRMATempID ELSE numBizDocTempID END)				
        FROM    dbo.ReturnHeader RH
        WHERE   RH.numReturnHeaderID = @numReferenceID

		SELECT	ACI.vcFirstName ContactFirstName,
                ACI.vcLastName ContactLastName,
				CMP.vcCompanyName OrganizationName,
				'' As BizDocTemplateFromGlobalSettings
		FROM    dbo.ReturnHeader RH
				JOIN Domain D ON D.numDomainID = RH.numDomainID
				JOIN [DivisionMaster] DM ON DM.numDivisionID = RH.numDivisionID
				LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId
												AND CMP.numDomainID = @numDomainID
				LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = RH.numContactID
		WHERE   RH.numReturnHeaderID = @numReferenceID
	END

ELSE IF @numReferenceType = 11
	BEGIN
		SELECT	ACI.vcFirstName ContactFirstName,
				ACI.vcLastName ContactLastName,
				CMP.vcCompanyName OrganizationName,
				'' As BizDocTemplateFromGlobalSettings
		FROM OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
			LEFT JOIN OpportunityBizDocs AS OBD ON Mst.numOppid=OBD.numOppId
			LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
			LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
		
		WHERE  OBD.numOppBizDocsId = @numReferenceID AND  Mst.numDomainID = @numDomainID
		SET @numReferenceType=1
		SET @numReferenceID=(SELECT TOP 1 numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numReferenceID)
	END
		      
END

GO


