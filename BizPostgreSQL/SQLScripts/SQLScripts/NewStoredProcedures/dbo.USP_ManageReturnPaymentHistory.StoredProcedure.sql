        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnPaymentHistory' ) 
    DROP PROCEDURE USP_ManageReturnPaymentHistory
GO

CREATE PROCEDURE [dbo].[USP_ManageReturnPaymentHistory]
    @numReferenceID NUMERIC(18, 0) =0,
    @tintMode AS TINYINT,
    @numUserCntID NUMERIC(18, 0) ,
    @numDomainID NUMERIC(18, 0) ,
    @strItems TEXT
AS 
				
				DECLARE @hDocItem INT
				IF CONVERT(VARCHAR(10), @strItems) <> '' 
					BEGIN
						EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
						
							SELECT  numReturnHeaderID,monAmount,tintRefType INTO #tempCredits
											FROM    ( SELECT    *
													  FROM      OPENXML (@hDocItem, '/NewDataSet/Credits', 2)
													WITH (numReturnHeaderID TINYINT,monAmount DECIMAL(20,5),tintRefType TINYINT)
													) X 	
						EXEC sp_xml_removedocument @hDocItem
				
				IF @tintMode=1 --Deposit (Sales Credit Memo,Credit Memo)
				BEGIN
					DELETE FROM ReturnPaymentHistory WHERE numReferenceID=@numReferenceID AND tintReferenceType IN(1,3)
				END	
				ELSE IF @tintMode=2 --Bill Payment (Purchase Credit Memo)
				BEGIN
					DELETE FROM ReturnPaymentHistory WHERE numReferenceID=@numReferenceID AND tintReferenceType=2
				END	
				
				INSERT INTO dbo.ReturnPaymentHistory (
					numReturnHeaderID,
					numReferenceID,
					monAmount,
					tintReferenceType
				)  	SELECT  numReturnHeaderID,@numReferenceID,monAmount,tintRefType
				FROM #tempCredits WHERE tintRefType!=0
										
				
					UPDATE dbo.ReturnHeader SET monBizDocUsedAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM ReturnPaymentHistory WHERE numReturnHeaderID=ReturnHeader.numReturnHeaderID)
					WHERE numReturnHeaderID IN (SELECT  numReturnHeaderID FROM #tempCredits WHERE tintRefType!=0)						
					
				DROP TABLE #tempCredits
				END
			