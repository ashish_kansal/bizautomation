GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostVendorsDelete')
DROP PROCEDURE USP_LandedCostVendorsDelete
GO
CREATE PROC [dbo].[USP_LandedCostVendorsDelete] 
    @numDomainId numeric(18, 0),
    @numLCVendorId numeric(18, 0)
AS 
	DELETE
	FROM   [dbo].[LandedCostVendors]
	WHERE  [numLCVendorId] = @numLCVendorId and numDomainId = @numDomainId
