GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectCommission')
DROP PROCEDURE USP_GetProjectCommission
GO
CREATE PROCEDURE [dbo].[USP_GetProjectCommission]
(
               @numUserCntID         AS NUMERIC(9)  = 0,
               @numDomainId          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT,
               @dtStartDate          AS DATETIME,
               @dtEndDate            AS DATETIME,
               @numProID AS NUMERIC,
               @tintMode AS TINYINT)
AS
  BEGIN
  
   IF @tintMode=1
   BEGIN
   	SELECT PM.numProId,PM.vcProjectID,PM.vcProjectName,
   dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset, BC.dtProCompletionDate),PM.numDomainID) AS dtCompletionDate,
   PM.monTotalIncome,PM.monTotalExpense,PM.monTotalGrossProfit,
   BC.monProTotalIncome,BC.monProTotalExpense,BC.monProTotalGrossProfit,
		Case When (PM.numRecOwner=@numUserCntID and PM.numAssignedTo=@numUserCntID) then  'Project Owner/Assigned'
									When PM.numRecOwner=@numUserCntID then 'Project Owner' 
									When PM.numAssignedTo=@numUserCntID then 'Project Assigned' end as EmpRole,
									BC.numComissionAmount as decTotalCommission,BC.decCommission
   FROM ProjectsMaster PM 
        INNER JOIN BizDocComission BC on BC.numProId=PM.numProId 
   where PM.numDomainId=@numDomainId-- AND PM.numProjectStatus=27492
   And (PM.numRecOwner=@numUserCntID or PM.numAssignedTo=@numUserCntID)  
   AND ((dateadd(minute,-@ClientTimeZoneOffset,PM.dtCompletionDate) between @dtStartDate And @dtEndDate)) AND 
   BC.numUserCntId=@numUserCntID AND BC.numDomainId=@numDomainId AND BC.bitCommisionPaid=0 and ISNULL(BC.numProId,0)>0
	ORDER BY PM.dtCompletionDate
   END
   ELSE IF @tintMode=2
   BEGIN
   	SELECT PM.numProId,PM.vcProjectID,PM.vcProjectName,
   dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset, BC.dtProCompletionDate),PM.numDomainID) AS dtCompletionDate,
   PM.monTotalIncome,PM.monTotalExpense,PM.monTotalGrossProfit,
   BC.monProTotalIncome,BC.monProTotalExpense,BC.monProTotalGrossProfit,
		Case When (PM.numRecOwner=BC.numUserCntID and PM.numAssignedTo=BC.numUserCntID) then  'Project Owner/Assigned'
									When PM.numRecOwner=BC.numUserCntID then 'Project Owner' 
									When PM.numAssignedTo=BC.numUserCntID then 'Project Assigned' end as EmpRole,
									BC.numComissionAmount as decTotalCommission,BC.decCommission,
									ISNULL((SELECT SUM(ISNULL(monCommissionAmt,0))
	FROM PayrollTracking WHERE ISNULL(BC.numComissionID,0)=ISNULL(numComissionID,0)),0) monPaidAmount,dbo.fn_GetContactName(ISNULL(BC.numUserCntID,0)) AS vcUserName
   FROM ProjectsMaster PM 
        INNER JOIN BizDocComission BC on BC.numProId=PM.numProId 
   WHERE PM.numProId=@numProID AND PM.numDomainId=@numDomainId -- AND PM.numProjectStatus=27492
	ORDER BY PM.dtCompletionDate
   END
   
  END
