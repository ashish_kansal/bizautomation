SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionDTLForCartItems')
	DROP PROCEDURE USP_GetPromotionDTLForCartItems
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionDTLForCartItems]  
	@numDomainID AS NUMERIC(18,0),
	@itemIdSearch AS VARCHAR(100)
AS
BEGIN

	DECLARE @strSQL VARCHAR(1000)

	SET @strSQL = 'IF((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND numProItemId IN (' + @itemIdSearch + ')) > 0) '
				+ 'BEGIN '
					+ 'SELECT * FROM PromotionOfferOrderBased WHERE cSaleType=''I'' AND numProItemId IN (' + @itemIdSearch + ') '
				+ 'END'

	PRINT @strSQL
	EXEC (@strSQL) 
END