SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetVendorsWareHouse')
DROP PROCEDURE USP_GetVendorsWareHouse
GO
CREATE PROCEDURE [dbo].[USP_GetVendorsWareHouse]         
@numDomainID as numeric(9)=0,    
@numItemCode as numeric(9)=0,
@numVendorid AS NUMERIC(9)=0,
@tintMode AS TINYINT        
as        

IF @tintMode=1
BEGIN 
--tintAddressType: 2 Shipping tintAddressOf:2 Organization
select div.numCompanyID,Vendor.numItemCode,Vendor.numVendorID,vcPartNo,vcCompanyName as Vendor,        
vcFirstName+ ' '+vcLastName as ConName,        
convert(varchar(15),numPhone)+ ', '+ convert(varchar(15),numPhoneExtension) as Phone,ISNULL(monCost,0) monCost,vcItemName,ISNULL([intMinQty],0) intMinQty
,numContactId,Ad.numAddressID,AD.vcAddressName,
isnull(AD.vcStreet,'') + ' ,' + isnull(AD.vcCity,'') + ' ,'
                            + isnull(dbo.fn_GetState(AD.numState),'') + ' '
                            + isnull(AD.vcPostalCode,'') + ' ,'
                            + isnull(dbo.fn_GetListItemName(AD.numCountry),'') vcFullAddress,
                         dbo.fn_GetVendorTransitCount(Vendor.numVendorID,Ad.numAddressID,Vendor.numDomainID) AS VendorTransitCount
from Vendor join divisionMaster div  on div.numdivisionid=Vendor.numVendorid        
join companyInfo com on com.numCompanyID=div.numCompanyID        
left join AdditionalContactsInformation ADC on ADC.numdivisionID=Div.numdivisionID   
join Item I on I.numItemCode= Vendor.numItemCode      
JOIN AddressDetails AD ON AD.numRecordID=div.numdivisionid
WHERE ISNULL(ADC.bitPrimaryContact,0)=1 and Vendor.numDomainID=@numDomainID and Vendor.numItemCode= @numItemCode
AND AD.tintAddressOf=2 AND AD.tintAddressType=2 AND (Vendor.numVendorid=@numVendorid OR @numVendorid=0)
END

ELSE IF @tintMode=2
BEGIN 
select Ad.numAddressID,ISNULL(AD.vcAddressName,'') + ' - ' + 
isnull(AD.vcStreet,'') + ' ,' + isnull(AD.vcCity,'') + ' ,'
                            + isnull(dbo.fn_GetState(AD.numState),'') + ' '
                            + isnull(AD.vcPostalCode,'') + ' ,'
                            + isnull(dbo.fn_GetListItemName(AD.numCountry),'') vcFullAddress
from divisionMaster div   
join companyInfo com on com.numCompanyID=div.numCompanyID        
left join AdditionalContactsInformation ADC on ADC.numdivisionID=Div.numdivisionID   
JOIN AddressDetails AD ON AD.numRecordID=div.numdivisionid
WHERE ISNULL(ADC.bitPrimaryContact,0)=1 and div.numDomainID=@numDomainID 
AND AD.tintAddressOf=2 AND AD.tintAddressType=2 AND div.numdivisionid=@numVendorid ORDER BY ISNULL(AD.bitIsPrimary,0) DESC
END

GO
