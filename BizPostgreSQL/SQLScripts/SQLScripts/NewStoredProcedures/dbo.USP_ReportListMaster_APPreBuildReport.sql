SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_APPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_APPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_APPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @TotalAP DECIMAL(20,5) = (SELECT numOpeningBal FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId IN (SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainID AND numChargeTypeId=8))
	DECLARE @APWithin30Days DECIMAL(20,5) = 0
	DECLARE @APWithin31to60Days DECIMAL(20,5) = 0
	DECLARE @APWithin61to90Days DECIMAL(20,5) = 0
	DECLARE @APPastDue DECIMAL(20,5) = 0
	DECLARE @dtFromDate DATETIME = '1990-01-01 00:00:00.000'
	DECLARE @dtToDate DATETIME = GETUTCDATE()

	SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	DECLARE @AuthoritativePurchaseBizDocId AS INTEGER
    SELECT 
		@AuthoritativePurchaseBizDocId = ISNULL(numAuthoritativePurchase,0)
    FROM 
		AuthoritativeBizDocs
    WHERE 
		numDomainId = @numDomainId
  
	DECLARE @TEMPAPRecord TABLE
	(
		DealAmount DECIMAL(20,5),
		dtDueDate DATETIME,
		AmountPaid DECIMAL(20,5),
		monUnAppliedAmount DECIMAL(20,5)
	)

	BEGIN TRY
		INSERT INTO @TEMPAPRecord
		(
			DealAmount
			,dtDueDate
			,AmountPaid
		)
		SELECT  
			ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) DealAmount
			,DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 THEN convert(int, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0)) ELSE 0 END, dtFromDate) AS dtDueDate
			,ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) AmountPaid
        FROM 
			OpportunityMaster Opp
        INNER JOIN 
			OpportunityBizDocs OB 
		ON 
			OB.numOppId = Opp.numOppID
        LEFT OUTER JOIN 
			[Currency] C 
		ON 
			Opp.[numCurrencyID] = C.[numCurrencyID]
        WHERE 
			tintOpptype = 2
            AND tintoppStatus = 1
            AND opp.numDomainID = @numDomainId
            AND OB.bitAuthoritativeBizDocs = 1
            AND ISNULL(OB.tintDeferred, 0) <> 1
            AND numBizDocId = @AuthoritativePurchaseBizDocId
			AND OB.[dtCreatedDate] BETWEEN @dtFromDate AND @dtToDate
        GROUP BY 
			OB.numOppId,
            OB.numOppBizDocsId,
            Opp.fltExchangeRate,
            OB.numBizDocId,
            OB.dtCreatedDate,
            Opp.bitBillingTerms,
            Opp.intBillingDays,
            OB.monDealAmount,
            Opp.numCurrencyID,
            OB.[dtFromDate],
			OB.monAmountPaid
        HAVING 
			(ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate,1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
        UNION ALL 
		--Add Bill Amount
        SELECT
            ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) DealAmount,
            BH.dtDueDate AS dtDueDate,
            ISNULL(SUM(BH.monAmtPaid), 0) AS AmountPaid
        FROM 
			BillHeader BH
        WHERE 
			BH.numDomainID = @numDomainId
            AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
			AND BH.[dtBillDate] BETWEEN @dtFromDate AND @dtToDate
        GROUP BY 
			BH.numDivisionId
			,BH.dtDueDate
        HAVING 
			ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) > 0
        UNION ALL
		--Add Write Check entry (As Amount Paid)
        SELECT  
            0 DealAmount,
            CH.dtCheckDate AS dtDueDate,
            ISNULL(SUM(CD.monAmount), 0) AS AmountPaid
        FROM 
			CheckHeader CH
		INNER JOIN 
			dbo.CheckDetails CD 
		ON 
			CH.numCheckHeaderID=CD.numCheckHeaderID
        INNER JOIN 
			dbo.Chart_Of_Accounts COA 
		ON 
			COA.numAccountId = CD.numChartAcntId
        WHERE 
			CH.numDomainID = @numDomainId 
			AND CH.tintReferenceType=1
			AND COA.vcAccountCode LIKE '01020102%'
			AND CH.[dtCheckDate] BETWEEN @dtFromDate AND @dtToDate
        GROUP BY 
			CD.numCustomerId,
            CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
        UNION ALL
        SELECT 
			CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt ELSE GJD.numCreditAmt END,
            GJH.datEntry_Date,
            0 AS AmountPaid
        FROM 
			dbo.General_Journal_Header GJH
        INNER JOIN 
			dbo.General_Journal_Details GJD 
		ON 
			GJH.numJournal_Id = GJD.numJournalId
            AND GJH.numDomainId = GJD.numDomainId
		INNER JOIN 
			dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
        WHERE 
			GJH.numDomainId = @numDomainId 
			AND COA.vcAccountCode LIKE '01020102%'
            AND ISNULL(numOppId, 0) = 0
            AND ISNULL(numOppBizDocsId, 0) = 0
			AND ISNULL(GJD.numCustomerId,0) > 0
            AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
            AND ISNULL(GJH.numPayrollDetailID,0)=0
			AND [GJH].[datEntry_Date] BETWEEN @dtFromDate AND @dtToDate
        UNION ALL
		--Add Commission Amount
        SELECT 
			ISNULL(SUM(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0)  DealAmount,
            OBD.dtCreatedDate AS dtDueDate,
            0 AS AmountPaid
        FROM 
			BizDocComission BDC
        INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			BDC.numOppBizDocId = OBD.numOppBizDocsId
        INNER JOIN 
			OpportunityMaster Opp 
		ON 
			OPP.numOppId = OBD.numOppId
        INNER JOIN 
			Domain D 
		ON 
			D.numDomainID = BDC.numDomainID
        LEFT JOIN 
			dbo.PayrollTracking PT 
		ON 
			BDC.numComissionID=PT.numComissionID
        WHERE 
			OPP.numDomainID = @numDomainId
            AND BDC.numDomainID = @numDomainId
			AND OBD.[dtCreatedDate] BETWEEN @dtFromDate AND @dtToDate
            AND OBD.bitAuthoritativeBizDocs = 1
        GROUP BY 
			D.numDivisionId,
            OBD.numOppId,
            BDC.numOppBizDocId,
            OPP.numCurrencyID,
            BDC.numComissionAmount,
            OBD.dtCreatedDate
        HAVING 
			ISNULL(BDC.numComissionAmount, 0) > 0
		UNION ALL --Standalone Refund against Account Receivable
		SELECT 
			CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount,			
			GJH.datEntry_Date
			,0 AS AmountPaid
		FROM 
			dbo.ReturnHeader RH 
		JOIN 
			dbo.General_Journal_Header GJH 
		ON 
			RH.numReturnHeaderID = GJH.numReturnID
		JOIN 
			General_Journal_Details GJD 
		ON 
			GJH.numJournal_Id = GJD.numJournalId 
		INNER JOIN 
			dbo.Chart_Of_Accounts COA 
		ON 
			COA.numAccountId = GJD.numChartAcntId
		WHERE 
			RH.tintReturnType=4 
			AND RH.numDomainId=@numDomainId 
			AND COA.vcAccountCode LIKE '01020102%'
			AND (monBizDocAmount > 0) 
			AND ISNULL(RH.numBillPaymentIDRef,0)>0
			AND RH.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
			
		
		INSERT INTO @TEMPAPRecord
        (
			[DealAmount],
			[dtDueDate],
			AmountPaid,
			monUnAppliedAmount
        )
		SELECT 
			0,
			NULL,
			0,
			SUM(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0))       
		FROM 
			BillPaymentHeader BPH
		WHERE 
			BPH.numDomainId=@numDomainId   
			AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
			AND [BPH].[dtPaymentDate]  BETWEEN @dtFromDate AND @dtToDate
		GROUP BY 
			BPH.numDivisionId

		SET @APWithin30Days = (SELECT SUM(DealAmount) FROM @TEMPAPRecord WHERE DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30)
		SET @APWithin31to60Days = (SELECT SUM(DealAmount) FROM @TEMPAPRecord WHERE DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60)
		SET @APWithin61to90Days = (SELECT SUM(DealAmount) FROM @TEMPAPRecord WHERE DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90)
		SET @APPastDue = (SELECT SUM(DealAmount) FROM @TEMPAPRecord WHERE dbo.GetUTCDateWithoutTime() > [dtDueDate])
	END TRY
	BEGIN CATCH
		-- DO NOT RAISE ERROR
	END CATCH

	SELECT 
		ISNULL(@TotalAP,0) AS TotalAP
		,ISNULL(@APWithin30Days,0) AS APWithin30Days
		,ISNULL(@APWithin31to60Days,0) AS APWithin31to60Days
		,ISNULL(@APWithin61to90Days,0) AS APWithin61to90Days
		,ISNULL(@APPastDue,0) AS APPastDue
END
GO