
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSiteCategories]    Script Date: 08/08/2009 16:18:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSiteCategories')
DROP PROCEDURE USP_ManageSiteCategories
GO
CREATE PROCEDURE [dbo].[USP_ManageSiteCategories]
          @numSiteID NUMERIC(9),
          @strItems  TEXT  = NULL
AS
  DELETE FROM [SiteCategories]
  WHERE       [numSiteID] = @numSiteID;
  DECLARE  @hDocItem INT
  IF CONVERT(VARCHAR(10),@strItems) <> ''
    BEGIN
      EXEC sp_xml_preparedocument
        @hDocItem OUTPUT ,
        @strItems
      INSERT INTO [SiteCategories]
                 ([numSiteID],
                  [numCategoryID])
      SELECT X.numSiteID,
             X.numCategoryID
      FROM   (SELECT *
              FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                        WITH (numSiteID     NUMERIC(9),
                              numCategoryID NUMERIC(9))) X
      EXEC sp_xml_removedocument
        @hDocItem
    END

