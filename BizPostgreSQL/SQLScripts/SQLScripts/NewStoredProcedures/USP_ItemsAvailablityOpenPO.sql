--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsAvailablityOpenPO' ) 
    DROP PROCEDURE USP_ItemsAvailablityOpenPO
GO
CREATE PROCEDURE [dbo].[USP_ItemsAvailablityOpenPO]
    @numItemCode NUMERIC(18,0)=0,
    @numDomainID NUMERIC(18,0)=0
   
AS 
   BEGIN
		if((select COUNT(W.numOnHand)
			from 
			WarehouseItems  AS W
		LEFT JOIN
			Warehouses AS WH ON WH.numWareHouseId=W.numWareHouseId
		LEFT JOIN
			WarehouseLocation AS WL ON WL.numWLocationID=W.numWLocationID
		WHERE 
			W.numItemID=@numItemCode)=0)
		BEGIN
			SELECT (CASE WHEN @numDomainID=209 THEN 'RFQ' ELSE '0' END) AS numOnHand,0 AS numOnOrder,'NOT AVAILABLE' AS vcWareHouse,NULL AS bintCreatedDate,NULL AS dtDeliveryDate,0 AS LeadDays
		END
		ELSE
		BEGIN
			select distinct
			CASE WHEN ISNULL(W.numOnHand,0) = 0 THEN (CASE WHEN @numDomainID=209 THEN 'RFQ' ELSE '0' END) ELSE CAST(W.numOnHand AS VARCHAR) END numOnHand,
			W.numOnOrder,
			WH.vcWareHouse +' '+ (CASE WHEN W.numWLocationID=-1 THEN 'Global' WHEN W.numWLocationID=0 THEN '' ELSE WL.vcLocation END) AS vcWareHouse,
			(select TOP 1 bintCreatedDate FROM OpportunityMaster where tintShipped=0 AND tintOppType=2 AND numDomainId=@numDomainID AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode) ORDER BY bintCreatedDate desc) as bintCreatedDate,
			(select TOP 1 dtDeliveryDate FROM OpportunityBizDocs where dtDeliveryDate IS NOT NULL AND numOppId=(select TOP 1 numOppId FROM OpportunityMaster where tintShipped=0 AND tintOppType=2 AND numDomainId=@numDomainID AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode))) AS dtDeliveryDate,
			ISNULL(DATEDIFF(DAY,CAST((select TOP 1 bintCreatedDate FROM OpportunityMaster where tintShipped=0 AND numDomainId=@numDomainID AND tintOppType=2 AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode) ORDER BY bintCreatedDate desc) AS DATE),
			CAST((select TOP 1 dtDeliveryDate FROM OpportunityBizDocs where dtDeliveryDate IS NOT NULL AND numOppId=(select TOP 1 numOppId FROM OpportunityMaster where tintShipped=0 AND tintOppType=2 AND numDomainId=@numDomainID AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode))) AS DATE)),0) AS LeadDays
	from 
			WarehouseItems  AS W
		LEFT JOIN
			Warehouses AS WH ON WH.numWareHouseId=W.numWareHouseId
		LEFT JOIN
			WarehouseLocation AS WL ON WL.numWLocationID=W.numWLocationID
		WHERE 
			W.numItemID=@numItemCode
    END
    END
GO