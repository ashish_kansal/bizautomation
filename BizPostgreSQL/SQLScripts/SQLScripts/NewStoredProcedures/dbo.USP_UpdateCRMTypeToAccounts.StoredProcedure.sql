-- EXEC USP_UpdateCRMTypeToAccounts	255208,170,229543
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateCRMTypeToAccounts' ) 
    DROP PROCEDURE USP_UpdateCRMTypeToAccounts
GO
CREATE PROCEDURE [dbo].[USP_UpdateCRMTypeToAccounts]
    (
      @numDivisionID NUMERIC(18),
      @numDomainID NUMERIC(18),
      @numUserCntID NUMERIC(18)
    )
AS 
    BEGIN
/* Biz usually automatically promotes the record from Lead/Prospect to Account When add a bill, create credit memo,  against it.*/  
-- Promote Lead to Account when Sales/Purchase Order is created against it
        DECLARE @tintCRMType TINYINT
        SELECT  @tintCRMType = ISNULL(tintCRMType,0)
        FROM    dbo.DivisionMaster
        WHERE   numDomainID = @numDomainID
                AND numDivisionID = @numDivisionID
		
		-- Whether given company type are leads,prospects, update & promote them as Accounts
          
        IF @tintCRMType = 0 
            BEGIN        
                UPDATE  divisionmaster
                SET     tintCRMType = 2,
                        bintLeadProm = GETUTCDATE(),
                        bintLeadPromBy = @numUserCntID,
                        bintProsProm = GETUTCDATE(),
                        bintProsPromBy = @numUserCntID
                WHERE   numDivisionID = @numDivisionID        
            END
		
		--Promote Prospect to Account
        ELSE 
            IF @tintCRMType = 1 
                BEGIN        
                    UPDATE  divisionmaster
                    SET     tintCRMType = 2,
                            bintProsProm = GETUTCDATE(),
                            bintProsPromBy = @numUserCntID
                    WHERE   numDivisionID = @numDivisionID        
                END            
    END        