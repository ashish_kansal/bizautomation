SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRatings')
DROP PROCEDURE USP_GetRatings
GO
CREATE PROCEDURE [dbo].[USP_GetRatings]   

   
@tintTypeId as INT = 0,       
@vcReferenceId	 as varchar(100)='', 
@numContactId AS NUMERIC(18,0) ,
@numSiteId AS NUMERIC(18,0) ,
@numDomainId AS NUMERIC(18,0) ,
@Mode AS INT

AS

BEGIN
	IF @Mode = 1
		BEGIN
				SELECT intRatingCount FROM Ratings WHERE tintTypeId = @tintTypeId AND vcReferenceId = @vcReferenceId AND numContactId = @numContactId AND numSiteId = @numSiteId AND numDomainId = @numDomainId 		
		END
    ELSE IF @Mode = 2
        BEGIN
			  SELECT AVG(CONVERT(NUMERIC(9,0), intRatingCount)) AS AvgRatings FROM Ratings WHERE tintTypeId = @tintTypeId AND vcReferenceId = @vcReferenceId AND  numSiteId = @numSiteId  AND numDomainId = @numDomainId 		
		END
  	ELSE IF @Mode = 3 --To get Rating Count ...Display on Product Page ...
	    BEGIN
			  SELECT COUNT(*) AS RatingCount FROM Ratings WHERE tintTypeId = @tintTypeId AND vcReferenceId = @vcReferenceId AND  numSiteId = @numSiteId AND numDomainId = @numDomainId 		
		END	
END

	
--SELECT AVG(CONVERT(NUMERIC (9,0) , intRatingCount)) FROM Ratings
--UPDATE Ratings SET intRatingCount = 4 WHERE numRatingId = 1

--INSERT INTO Ratings VALUES(2,'19017',1,20,'255.124.13.1',92,1 ,2012-10-03)