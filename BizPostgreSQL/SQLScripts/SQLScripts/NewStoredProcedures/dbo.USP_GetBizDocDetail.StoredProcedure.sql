/****** Object:  StoredProcedure [dbo].[USP_GetBizDocDetail]    Script Date: 09/01/2009 19:00:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocDetail')
DROP PROCEDURE USP_GetBizDocDetail
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocDetail]
               @numOppBizDocsId NUMERIC(9),
               @numDomainId     NUMERIC(9)
AS
  BEGIN
    SELECT BD.vcBizDocID,
           AD1.vcCity vcBillCity,
           isnull(dbo.fn_GetListName(AD1.numCountry,0),'')  as vcBillCountry,
           isnull(dbo.fn_GetState(AD1.numState),'') as vcBilState,
		   isnull(AD1.vcPostalCode,'') as vcBillPostCode,	
           AD1.vcStreet vcBillStreet,
           AC.vcFirstName,
           AC.vcLastName,
           AC.vcEmail,
           AC.numPhone,
           AC.numContactId,
           ISNULL(OM.[bitBillingTerms],0) bitBillingTerms,
           ISNULL(OM.[intBillingDays],0) intBillingDays,
           OM.[numDivisionId],
           OM.[tintOppType],
           ISNULL(BD.monAmountPaid,0) monAmountPaid,
           ISNULL(BD.monDealAmount,0) monDealAmount,
           dbo.fn_GetComapnyName(DM.numDivisionID) AS vcCompanyName ,--added by chintan, to be used in Receive payment
           BD.numOppId,
           Bd.numBizDocTempID,
           Bd.numBizDocId
    FROM   OpportunityBizDocs BD
           INNER JOIN OpportunityMaster OM
             ON BD.numOppId = OM.numOppId
           INNER JOIN AdditionalContactsInformation AC
             ON OM.numContactId = AC.numContactId
           INNER JOIN DivisionMaster DM
             ON AC.numDivisionId = DM.numDivisionID
           LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
    WHERE  bd.numOppBizDocsId = @numOppBizDocsId
           AND OM.numDomainId = @numDomainId

  END
