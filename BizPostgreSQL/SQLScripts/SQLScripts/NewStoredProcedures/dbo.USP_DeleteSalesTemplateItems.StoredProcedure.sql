
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteSalesTemplateItems')
DROP PROCEDURE USP_DeleteSalesTemplateItems
GO
CREATE PROCEDURE [dbo].[USP_DeleteSalesTemplateItems]
	@numSalesTemplateItemID NUMERIC,
	@numDomainID NUMERIC
AS
SET NOCOUNT ON

DELETE FROM SalesTemplateItems
WHERE [numSalesTemplateItemID] = @numSalesTemplateItemID
AND [numDomainID] = @numDomainID