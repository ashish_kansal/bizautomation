GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentConfiguration_Save')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentConfiguration_Save]
(
	@numDomainID NUMERIC(18,0)
	,@bitGroupByOrderForPick BIT
	,@bitGroupByOrderForShip BIT
	,@bitGroupByOrderForInvoice BIT
	,@bitGroupByOrderForPay BIT
	,@bitGroupByOrderForClose BIT
	,@tintInvoicingType TINYINT
	,@tintScanValue TINYINT
	,@tintPackingMode TINYINT
	,@tintPendingCloseFilter TINYINT
	,@bitGeneratePickListByOrder BIT
	,@numOrderStatusPicked NUMERIC(18,0)
	,@numOrderStatusPacked1 NUMERIC(18,0)
	,@numOrderStatusPacked2 NUMERIC(18,0)
	,@numOrderStatusPacked3 NUMERIC(18,0)
	,@numOrderStatusPacked4 NUMERIC(18,0)
	,@numOrderStatusInvoiced NUMERIC(18,0)
	,@numOrderStatusPaid NUMERIC(18,0)
	,@numOrderStatusClosed NUMERIC(18,0)
	,@bitPickWithoutInventoryCheck BIT
	,@bitEnablePickListMapping BIT
	,@bitEnableFulfillmentBizDocMapping BIT
	,@bitAutoWarehouseSelection BIT
	,@bitBOOrderStatus BIT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		UPDATE
			MassSalesFulfillmentConfiguration
		SET
			bitGroupByOrderForPick=@bitGroupByOrderForPick
			,bitGroupByOrderForShip=@bitGroupByOrderForShip
			,bitGroupByOrderForInvoice=@bitGroupByOrderForInvoice
			,bitGroupByOrderForPay=@bitGroupByOrderForPay
			,bitGroupByOrderForClose=@bitGroupByOrderForClose
			,tintInvoicingType=@tintInvoicingType
			,tintScanValue=@tintScanValue
			,tintPackingMode=@tintPackingMode
			,tintPendingCloseFilter=@tintPendingCloseFilter
			,bitGeneratePickListByOrder=@bitGeneratePickListByOrder
			,numOrderStatusPicked=@numOrderStatusPicked
			,numOrderStatusPacked1=@numOrderStatusPacked1
			,numOrderStatusPacked2=@numOrderStatusPacked2
			,numOrderStatusPacked3=@numOrderStatusPacked3
			,numOrderStatusPacked4=@numOrderStatusPacked4
			,numOrderStatusInvoiced=@numOrderStatusInvoiced
			,numOrderStatusPaid=@numOrderStatusPaid
			,numOrderStatusClosed=@numOrderStatusClosed
			,bitPickWithoutInventoryCheck=@bitPickWithoutInventoryCheck
			,bitEnablePickListMapping=@bitEnablePickListMapping
			,bitEnableFulfillmentBizDocMapping=@bitEnableFulfillmentBizDocMapping
			,bitAutoWarehouseSelection=@bitAutoWarehouseSelection
			,bitBOOrderStatus=@bitBOOrderStatus
		WHERE
			numDomainID=@numDomainID 
	END
	ELSE
	BEGIN
		INSERT INTO MassSalesFulfillmentConfiguration
		(
			numDomainID 
			,bitGroupByOrderForPick
			,bitGroupByOrderForShip
			,bitGroupByOrderForInvoice
			,bitGroupByOrderForPay
			,bitGroupByOrderForClose
			,tintInvoicingType
			,tintScanValue
			,tintPackingMode
			,tintPendingCloseFilter
			,bitGeneratePickListByOrder
			,numOrderStatusPicked
			,numOrderStatusPacked1
			,numOrderStatusPacked2
			,numOrderStatusPacked3
			,numOrderStatusPacked4
			,numOrderStatusInvoiced
			,numOrderStatusPaid
			,numOrderStatusClosed
			,bitPickWithoutInventoryCheck
			,bitEnablePickListMapping
			,bitEnableFulfillmentBizDocMapping
			,bitAutoWarehouseSelection
			,bitBOOrderStatus
		)
		VALUES
		(
			@numDomainID 
			,@bitGroupByOrderForPick
			,@bitGroupByOrderForShip
			,@bitGroupByOrderForInvoice
			,@bitGroupByOrderForPay
			,@bitGroupByOrderForClose
			,@tintInvoicingType
			,@tintScanValue
			,@tintPackingMode
			,@tintPendingCloseFilter
			,@bitGeneratePickListByOrder
			,@numOrderStatusPicked
			,@numOrderStatusPacked1
			,@numOrderStatusPacked2
			,@numOrderStatusPacked3
			,@numOrderStatusPacked4
			,@numOrderStatusInvoiced
			,@numOrderStatusPaid
			,@numOrderStatusClosed
			,@bitPickWithoutInventoryCheck
			,@bitEnablePickListMapping
			,@bitEnableFulfillmentBizDocMapping
			,@bitAutoWarehouseSelection
			,@bitBOOrderStatus
		)
	END
END
GO