SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_GetItemSelections')
DROP PROCEDURE USP_PromotionOffer_GetItemSelections
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_GetItemSelections]    
	@numProId AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0),
	@tintRecordType AS TINYINT,
	@tintItemSelectionType AS TINYINT
AS
BEGIN
	IF @tintItemSelectionType = 1
	BEGIN
		SELECT 
			numProItemId
			,numProId
			,numValue
			,vcItemName
			,txtItemDesc
			,vcModelID 
		FROM 
			Item   
		JOIN 
			PromotionOfferItems
		ON 
			numValue=numItemCode  
		WHERE 
			numProId=@numProId
			AND tintType = 1
			AND tintRecordType = @tintRecordType
	END
	ELSE IF @tintItemSelectionType = 4
	BEGIN
		SELECT 
			numProItemId
			,numProId
			,numValue
			,vcItemName
			,txtItemDesc
			,vcModelID 
		FROM 
			Item   
		JOIN 
			PromotionOfferItems
		ON 
			numValue=numItemCode  
		WHERE 
			numProId=@numProId
			AND tintType = 4
			AND tintRecordType = @tintRecordType
	END
	ELSE IF @tintItemSelectionType = 2
	BEGIN
		SELECT 
			numProItemId
			,numProId
			,numValue
			,dbo.[GetListIemName](numValue) ItemClassification,
			(SELECT COUNT(*) FROM Item I1 WHERE I1.numItemClassification=numValue) AS ItemsCount 
		FROM 
			PromotionOfferItems   
		WHERE 
			numProId=@numProId
			AND tintType = 2
			AND tintRecordType = @tintRecordType
	END
END 
GO
