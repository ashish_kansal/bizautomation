SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemByProfitPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10ItemByProfitPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemByProfitPreBuildReport]
	@numDomainID NUMERIC(18,0)
	--,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcGroupBy VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@numRecordCount INT
	,@tintControlField INT
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)

	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP (@numRecordCount)
		numItemCode
		,vcItemName,
		(CASE WHEN @tintControlField = 1 THEN SUM(Profit) 
		   WHEN @tintControlField = 2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField = 3 THEN SUM(monTotAmount)  END) as Profit 	 		
		--,SUM(Profit) Profit
	FROM
	(
		SELECT
			I.numItemCode
			,I.vcItemName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP	
	GROUP BY
		numItemCode
		,vcItemName
	HAVING
		   (CASE 
		   WHEN @tintControlField = 1 THEN SUM(Profit) 
		   WHEN @tintControlField = 2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField = 3 THEN SUM(monTotAmount) 
		   END) > 0
		
	ORDER BY
		   (CASE 
		   WHEN @tintControlField = 1 THEN SUM(Profit) 
		   WHEN @tintControlField = 2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100 
		   WHEN @tintControlField = 3 THEN SUM(monTotAmount) 
		   END) DESC
END
GO