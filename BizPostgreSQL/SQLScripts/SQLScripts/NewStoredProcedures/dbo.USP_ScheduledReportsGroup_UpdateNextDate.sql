GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_UpdateNextDate')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_UpdateNextDate
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_UpdateNextDate]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @tintFrequency TINYINT
	DECLARE @dtStartDate DATETIME

	SELECT @tintFrequency=tintFrequency,@dtStartDate=dtStartDate FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID AND ID=@numSRGID

	IF ISNULL(@tintFrequency,0) = 1
	BEGIN
		IF @dtStartDate < GETUTCDATE()
		BEGIN
			WHILE CAST(@dtStartDate AS DATE) <= CAST(GETUTCDATE() AS DATE)
			BEGIN
				SET @dtStartDate = DATEADD(DAY,1,@dtStartDate)
			END

			UPDATE ScheduledReportsGroup SET dtNextDate=@dtStartDate,intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
		ELSE
		BEGIN
			UPDATE ScheduledReportsGroup SET dtNextDate=DATEADD(DAY,1,dtStartDate),intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
	END
	ELSE IF ISNULL(@tintFrequency,0) = 2
	BEGIN
		IF @dtStartDate < GETUTCDATE()
		BEGIN
			WHILE CAST(@dtStartDate AS DATE) <= CAST(GETUTCDATE() AS DATE)
			BEGIN
				SET @dtStartDate = DATEADD(WEEK,1,@dtStartDate)
			END

			UPDATE ScheduledReportsGroup SET dtNextDate=@dtStartDate,intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
		ELSE
		BEGIN
			UPDATE ScheduledReportsGroup SET dtNextDate=DATEADD(WEEK,1,dtStartDate),intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
	END
	ELSE IF ISNULL(@tintFrequency,0) = 3
	BEGIN
		IF @dtStartDate < GETUTCDATE()
		BEGIN
			WHILE CAST(@dtStartDate AS DATE) <= CAST(GETUTCDATE() AS DATE)
			BEGIN
				SET @dtStartDate = DATEADD(MONTH,1,@dtStartDate)
			END

			UPDATE ScheduledReportsGroup SET dtNextDate=@dtStartDate,intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
		ELSE
		BEGIN
			UPDATE ScheduledReportsGroup SET dtNextDate=DATEADD(MONTH,1,dtStartDate),intNotOfTimeTried=0 WHERE numDomainID=@numDomainID AND ID=@numSRGID
		END
	END
END
GO