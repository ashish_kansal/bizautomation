/****** Object:  StoredProcedure [dbo].[USP_GetAccountPayableAging_Invoice]    Script Date: 03/06/2009 00:30:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [dbo].USP_GetAccountPayableAging_Invoice 1,7281,'90-'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountPayableAging_Invoice')
DROP PROCEDURE USP_GetAccountPayableAging_Invoice
GO
CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging_Invoice]
(
    @numDomainId AS NUMERIC(9) = 0,
    @vcFlag VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0
)
AS
BEGIN
    DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    
	SELECT 
		@AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM 
		AuthoritativeBizDocs
    WHERE 
		numDomainId =  @numDomainId
	------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()

    DECLARE @strSql NVARCHAR(MAX);
	DECLARE @strSql1 Nvarchar(MAX);
	DECLARE @strCheck Nvarchar(MAX);
	DECLARE @strJournal NVARCHAR(MAX);
	DECLARE @strCommission Nvarchar(MAX);
	DECLARE @strReturn Nvarchar(MAX);
	DECLARE @strSqlCredit NVARCHAR(MAX);
    
    
    SET @strSql = '  SELECT X.* INTO #TempRecords from (SELECT 
               OM.[numOppId],
               OM.[vcPOppName],
               OM.[tintOppType],
               OB.[numOppBizDocsId],
               OB.[vcBizDocID],
               isnull(OB.monDealAmount * OM.fltExchangeRate,
                      0) TotalAmount,--OM.[monPAmount]
               (ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
               isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0)  BalanceDue,
               CASE ISNULL(bitBillingTerms,0) 
                 --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd, convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),OB.dtFromDate),
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd, convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                          '+ CONVERT(VARCHAR(15),@numDomainId) +')
                 WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
               END AS DueDate,
               bitBillingTerms,
               intBillingDays,
               OB.dtCreatedDate,
               ISNULL(C.[varCurrSymbol],'''') varCurrSymbol,
               OM.fltExchangeRate,
               OB.dtFromDate,
			   0 AS numBizDocsPaymentDetId,CO.vcCompanyName,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName,
                           dbo.fn_GetContactName(OM.numRecOwner) AS RecordOwner,
						   dbo.fn_GetContactName(OM.numAssignedTo) AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone,
						 OM.numCurrencyID,OB.vcRefOrderNo
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
               INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
               LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=OM.numContactId
               INNER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativePurchaseBizDocId,0)) +'
			   AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate])  <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (ISNULL(OB.monDealAmount * ISNULL(OM.fltExchangeRate,1), 0) - ISNULL(ISNULL(TablePayment.monPaidAmount,0) * ISNULL(OM.fltExchangeRate, 1), 0)) > 0'
               
		SET @strSql1 = ' UNION 
		SELECT  
               0 AS numOppId,
               ''Bill'' vcPOppName,
               -1 AS [tintOppType],-- flag for bills 
               0 numOppBizDocsId,
               CASE WHEN len(BH.vcReference)=0 THEN ''Open Bill'' ELSE BH.vcReference END AS [vcBizDocID],
               BH.monAmountDue as TotalAmount,
               ISNULL(TablePayment.monAmtPaid, 0) as AmountPaid,
               ISNULL(BH.monAmountDue, 0) - ISNULL(TablePayment.monAmtPaid, 0) as BalanceDue,
               [dbo].[FormatedDateFromDate](BH.dtDueDate,BH.numDomainID) AS DueDate,
               0 bitBillingTerms,
               0 intBillingDays,
               BH.dtCreatedDate dtCreatedDate,
               '''' varCurrSymbol,
               1 AS fltExchangeRate,
               BH.dtDueDate AS dtFromDate,
               0 numBizDocsPaymentDetId
			   ,CO.vcCompanyName
			   ,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName
			   ,'''' AS RecordOwner
			   ,'''' AS AssignedTo
				,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone
				,'+ CONVERT(VARCHAR(15),@baseCurrency) +',''''  as vcRefOrderNo 
        FROM    BillHeader BH INNER JOIN [DivisionMaster] DM ON BH.[numDivisionId] = DM.[numDivisionID]
                  INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
                  LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ISNULL(ADC.bitPrimaryContact,0)=1
				  OUTER APPLY
				(
					SELECT
						SUM(monAmount) monAmtPaid
					FROM 
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numBillID=BH.numBillID
						AND CAST(BPH.dtPaymentDate AS DATE) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				) TablePayment
        WHERE   
                BH.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +'
                AND ISNULL(BH.monAmountDue, 0) - ISNULL(TablePayment.monAmtPaid, 0) > 0
				AND (BH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND CONVERT(DATE,BH.[dtBillDate])  <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
				
		SET @strCheck = ' UNION ALL
                SELECT  
					0 AS numOppId,
						   ''Check'' vcPOppName,
						   -3 AS [tintOppType],-- flag for Checks
						   0 numOppBizDocsId,
						   '''' AS [vcBizDocID],
						   ISNULL(CD.monAmount, 0) as TotalAmount,
						   ISNULL(CD.monAmount, 0) as AmountPaid,
					       0 as BalanceDue,
						   [dbo].[FormatedDateFromDate](CH.dtCheckDate,CH.numDomainID) AS DueDate,
						   0 bitBillingTerms,
						   0 intBillingDays,
						   CH.dtCreatedDate dtCreatedDate,
						   '''' varCurrSymbol,
						   1 AS fltExchangeRate,
						   CH.dtCheckDate AS dtFromDate,
						   0 as numBizDocsPaymentDetId
						  ,CO.vcCompanyName
						   ,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName
						   ,'''' AS RecordOwner
						   ,'''' AS AssignedTo
						   ,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone
						   ,'+ CONVERT(VARCHAR(15),@baseCurrency) +'
						   ,'''' as vcRefOrderNo 
                FROM    CheckHeader CH
				INNER JOIN [DivisionMaster] DM ON CH.[numDivisionId] = DM.[numDivisionID]
					INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
					LEFT OUTER JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ISNULL(ADC.bitPrimaryContact,0)=1
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
                WHERE   CH.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +' AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE ''01020102%''
						AND (CH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
						AND CH.[dtCheckDate] <= ''' + CAST(@dtToDate AS VARCHAr) + ''''

        SET @strJournal = ' UNION ALL
                SELECT  
					GJH.numJournal_Id [numOppId],
					''Journal'' [vcPOppName],
					-1 [tintOppType],
					0 [numOppBizDocsId],
					'''' [vcBizDocID],
					CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt ELSE GJD.numCreditAmt END TotalAmount,
					0 AmountPaid,
					CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt ELSE GJD.numCreditAmt END BalanceDue,
					[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
					0 bitBillingTerms,
					0 intBillingDays,
					GJH.datCreatedDate,
					'''' [varCurrSymbol],
					1 fltExchangeRate,
					GJH.datEntry_Date AS dtFromDate,
					0 numBizDocsPaymentDetId
					,CO.vcCompanyName
					,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName
					,'''' AS RecordOwner
					,'''' AS AssignedTo
					,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone
					,GJD.numCurrencyID
					,'''' as vcRefOrderNo
                FROM    dbo.General_Journal_Header GJH
                        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                                      AND GJH.numDomainId = GJD.numDomainId
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
						INNER JOIN [DivisionMaster] DM ON GJD.numCustomerID = DM.[numDivisionID]
						INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
						Left JOIN AdditionalContactsInformation ADC ON ADC.numContactId=GJD.numContactId 
                WHERE   GJH.numDomainId = '+ CONVERT(VARCHAR(15),@numDomainId) +' 
                        AND COA.vcAccountCode LIKE ''01020102%''
                        AND ISNULL(numOppId, 0) = 0
                        AND ISNULL(numOppBizDocsId, 0) = 0
                        AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
                        AND ISNULL(GJH.numPayrollDetailID,0)=0
						AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
						AND [GJH].[datEntry_Date] <= ''' + CAST(@dtToDate AS VARCHAr) + ''''

          SET @strCommission =  ' UNION ALL
                SELECT  
					Opp.[numOppId],
					Opp.[vcPOppName],
					Opp.[tintOppType],
					OBD.[numOppBizDocsId],
					OBD.[vcBizDocID],
					BDC.numComissionAmount,
					0 AmountPaid,
					BDC.numComissionAmount BalanceDue,
					CASE ISNULL(Opp.bitBillingTerms,0) 
						--WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)),OBD.dtFromDate),
						WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0)),OBD.dtFromDate),
																'+ CONVERT(VARCHAR(15),@numDomainId) +')
						WHEN 0 THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
					END AS DueDate,
					Opp.bitBillingTerms,
					Opp.intBillingDays,
					OBD.dtCreatedDate,
					'''' varCurrSymbol,
					Opp.fltExchangeRate,
					OBD.dtFromDate,
					0 AS numBizDocsPaymentDetId
					,CO.vcCompanyName
					,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName
					,'''' AS RecordOwner
					,'''' AS AssignedTo
					,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone
					,Opp.numCurrencyID
					,OBD.vcRefOrderNo
                FROM    BizDocComission BDC
                        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
                        join OpportunityMaster Opp on OPP.numOppId = OBD.numOppId
						INNER JOIN [DivisionMaster] DM ON Opp.[numDivisionId] = DM.[numDivisionID]
					   INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
					   LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=Opp.numContactId
                        JOIN Domain D on D.numDomainID = BDC.numDomainID
                        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID=PT.numComissionID
                WHERE   OPP.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +'
                        and BDC.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +'
                        AND (Opp.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
						AND OBD.[dtCreatedDate] <= ''' + CAST(@dtToDate AS VARCHAr) + '''
						AND ISNULL(BDC.bitCommisionPaid,0)=0
                        AND OBD.bitAuthoritativeBizDocs = 1
						AND ISNULL(BDC.numComissionAmount, 0) > 0'

		SET @strReturn = ' UNION ALL
			 SELECT 
				0 AS numOppId,
				''Return'' vcPOppName,
				-1 AS [tintOppType],
				0 numOppBizDocsId,
				'''' AS [vcBizDocID],
				(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END) as TotalAmount,
				0 as AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END as BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,RH.numDomainID) AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				RH.dtCreatedDate dtCreatedDate,
				'''' varCurrSymbol,
				1 AS fltExchangeRate,
				GJH.datEntry_Date AS dtFromDate,
				0 as numBizDocsPaymentDetId
				,CO.vcCompanyName
				,ADC.vcFirstname + '' ''+ ADC.vcLastName AS vcContactName
				,'''' AS RecordOwner
				,'''' AS AssignedTo
				,CASE WHEN ADC.numPhone IS NULL THEN '''' ELSE + ADC.numPhone + (CASE WHEN numPhoneExtension IS NULL THEN '''' WHEN numPhoneExtension='''' THEN '''' ELSE '','' + ADC.numPhoneExtension END) END AS vcCustPhone
				,GJD.numCurrencyID
				,'''' as vcRefOrderNo
			 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			INNER JOIN [DivisionMaster] DM ON RH.[numDivisionId] = DM.[numDivisionID]
			INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
			LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=RH.numContactId
			WHERE RH.tintReturnType=4 AND RH.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +'  AND COA.vcAccountCode LIKE ''01020102%''
					AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)>0
					AND (Rh.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
					AND GJH.datEntry_Date <= ''' + CAST(@dtToDate AS VARCHAr) + ''''

					SET @strSqlCredit = CONCAT(' UNION ALL
							SELECT 
								0 AS numOppId,
								''Credit'' vcPOppName,
								-1 AS [tintOppType],
								0 numOppBizDocsId,
								'''' AS [vcBizDocID],
								ISNULL(monPaymentAmount,0) as TotalAmount,
								ISNULL(monAppliedAmount,0) as AmountPaid,
								(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) * -1 as BalanceDue,
								[dbo].[FormatedDateFromDate]([BPH].[dtPaymentDate],BPH.numDomainID) AS DueDate,
								0 bitBillingTerms,
								0 intBillingDays,
								BPH.dtCreateDate dtCreatedDate,
								'''' varCurrSymbol,
								1 AS fltExchangeRate,
								BPH.dtCreateDate AS dtFromDate,
								0 as numBizDocsPaymentDetId
								,CO.vcCompanyName
								,'''' AS vcContactName
								,'''' AS RecordOwner
								,'''' AS AssignedTo
								,'''' AS vcCustPhone
								,0
								,'''' as vcRefOrderNo							
							FROM 
								BillPaymentHeader BPH
							LEFT JOIN [DivisionMaster] DM ON BPH.[numDivisionId] = DM.[numDivisionID]
							LEFT JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
							WHERE 
								BPH.numDomainId=',@numDomainId,'
								AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
								AND (BPH.numAccountClass=',@numAccountClass,' OR ',@numAccountClass,'=0)
								AND [BPH].[dtPaymentDate] <= ''',@dtToDate,'''') 
    
   
    IF (@vcFlag = '0+30')
      BEGIN
			SET @strSql =@strSql +' AND 
               dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                 ELSE 0
                                               END,dtFromDate) '
                                               
          SET @strSql1 =@strSql1 +' AND BH.dtDueDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())'
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
			SET @strSql =@strSql +' AND  
                 dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
																WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                   ELSE 0
                                                 END,dtFromDate)
                 '
           SET @strSql1 =@strSql1 +' AND BH.dtDueDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
				SET @strSql =@strSql +' AND  
                   dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                                  --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
																  WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                     ELSE 0
                                                   END,dtFromDate) '
			SET @strSql1 =@strSql1 +' AND BH.dtDueDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
              SET @strSql =@strSql +' AND  
                     dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                       ELSE 0
                                                     END,dtFromDate)
                     '
				SET @strSql1 =@strSql1 +' AND BH.dtDueDate > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
					SET @strSql =@strSql +' AND  
                       dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30
                       '
				SET @strSql1 =@strSql1 +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
						SET @strSql =@strSql +' AND  
                         dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60
                         '
				SET @strSql1 =@strSql1 +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
						SET @strSql =@strSql +' AND  
                           dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																											WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
                           '
					SET @strSql1 =@strSql1 +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
						SET @strSql =@strSql +' AND  
                             dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                              --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                              WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																											WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                              ELSE 0
                                                            END,dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms 
																											WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate),dbo.GetUTCDateWithoutTime()) > 90
                             '
						SET @strSql1 =@strSql1 +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) > 90 '
                    END
                    
       SET @strSql =@strSql + @strSql1 + @strCheck + @strJournal + @strCommission + @strReturn + @strSqlCredit

    SET @strSql=@strSql +' ) X SELECT *,CASE when DATEDIFF (day ,DueDate ,GETDATE()) > 0 THEN DATEDIFF (day ,DueDate ,GETDATE()) ELSE NULL END AS DaysLate,CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount
    FROM #TempRecords 
    WHERE  isnull(TotalAmount,0) <> 0 AND ISNULL(BalanceDue,0) <> 0
    DROP TABLE #TempRecords'
              
	PRINT CAST(@strSql AS NTEXT)
	EXEC(@strSql)
  END
