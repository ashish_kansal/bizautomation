SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_SetDoNotMerge')
DROP PROCEDURE USP_OpportunityMaster_SetDoNotMerge
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_SetDoNotMerge]  
	@numDomainID NUMERIC(18,0),
	@vcPOID VARCHAR(2000)
AS  
BEGIN  
	UPDATE
		OpportunityMaster
	SET
		bitStopMerge = 1
	WHERE
		numDomainId = @numDomainID 
		AND numOppId IN (SELECT * FROM dbo.Split(@vcPOID,','))
END
GO
