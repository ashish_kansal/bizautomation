GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive')
DROP PROCEDURE USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetPOItemsPendingToReceive]
	@numDomainID as numeric(18,0),
	@numOppID numeric(18,0)=0
AS
BEGIN
	SELECT
		OpportunityMaster.numOppID AS OppID
		,ISNULL(OpportunityMaster.bitStockTransfer,0) AS IsStockTransfer
		,OpportunityMaster.numDivisionID AS DivisionID
		,ISNULL(OpportunityMaster.vcPoppName,'''') AS OppName
		,ISNULL(OpportunityMaster.bitPPVariance,0) AS IsPPVariance
		,ISNULL(OpportunityMaster.numCurrencyID,0) AS CurrencyID
		,ISNULL(OpportunityMaster.fltExchangeRate,0) AS ExchangeRate
		,ISNULL(OpportunityItems.numoppitemtCode,0) AS OppItemID 
        ,ISNULL(Item.vcItemName,0) AS ItemName
        ,ISNULL(OpportunityItems.numWarehouseItmsID,0) AS WarehouseItemID
        ,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) AS RemainingQty
        ,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) AS QtyToReceive
        ,'' AS SerialLot
        ,ISNULL(Item.bitSerialized,0) AS IsSerial
        ,ISNULL(Item.bitLotNo,0) AS IsLot
        ,ISNULL(Item.charItemType,'') AS CharItemType
        ,ISNULL(OpportunityItems.vcType,'') AS ItemType
        ,ISNULL(OpportunityItems.monPrice,0) AS Price
        ,ISNULL(OpportunityItems.bitDropShip,0) AS DropShip
        ,ISNULL(OpportunityItems.numProjectID,0) AS ProjectID
        ,ISNULL(OpportunityItems.numClassID,0) AS ClassID
        ,ISNULL(Item.numAssetChartAcntId,0) AS AssetChartAcntId
        ,ISNULL(Item.numCOGsChartAcntId,0) AS COGsChartAcntId
        ,ISNULL(Item.numIncomeChartAcntId,0) AS IncomeChartAcntId
		,1 AS IsSuccess
		,'' AS ErrorMessage
	FROM
		OpportunityMaster
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	WHERE
		OpportunityMaster.numDomainID=@numDomainID
		AND OpportunityMaster.numOppId=@numOppID
		AND OpportunityMaster.tintOppType = 2
		AND OpportunityMaster.tintOppStatus = 1
		AND ISNULL(OpportunityMaster.tintshipped,0) = 0
		AND ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) - ISNULL(OpportunityItems.numQtyReceived,0) > 0
END
GO