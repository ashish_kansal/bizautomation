SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created by Priya
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getduplicatecouponcode')
DROP PROCEDURE usp_getduplicatecouponcode
GO
CREATE PROCEDURE [dbo].[USP_GetDuplicateCouponCode]
    @numProId AS NUMERIC(9) = 0, 
    @numDomainId AS NUMERIC(18,0),   
	@txtCouponCode VARCHAR(100)
	
AS 
BEGIN	
		SELECT   numDiscountID
				,vcDiscountCode
				,numPromotionID
				,vcProName 
		FROM PromotionOffer PO
		INNER JOIN DiscountCodes D ON D.numPromotionID = PO.numProId
		WHERE vcDiscountCode IN (
                    SELECT Id
                    FROM dbo.SplitIDs(@txtCouponCode, ',') )
					AND PO.numDomainId = @numDomainId
					AND	D.numPromotionID <> @numProId
					AND ISNULL(PO.bitEnabled, 0) = 1		

END
