GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecurrenceConfiguration_SetDisable')
DROP PROCEDURE USP_RecurrenceConfiguration_SetDisable
GO
  
-- =============================================  
-- Author:  Sandeep
-- Create date: 1 Oct 2014
-- Description: Recurring order or bizdoc
-- =============================================  
Create PROCEDURE [dbo].[USP_RecurrenceConfiguration_SetDisable]   
	@numRecConfigID NUMERIC(18,0)
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  
 
UPDATE RecurrenceConfiguration SET bitDisabled = 1 WHERE numRecConfigID = @numRecConfigID


END  
