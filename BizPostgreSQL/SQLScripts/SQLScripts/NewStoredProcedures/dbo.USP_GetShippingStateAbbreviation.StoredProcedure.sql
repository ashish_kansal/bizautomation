GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingStateAbbreviation')
DROP PROCEDURE USP_GetShippingStateAbbreviation
GO
CREATE  PROCEDURE [dbo].[USP_GetShippingStateAbbreviation]
AS 
SELECT vcStateName,vcStateCode FROM [ShippingStateMaster]
GO

