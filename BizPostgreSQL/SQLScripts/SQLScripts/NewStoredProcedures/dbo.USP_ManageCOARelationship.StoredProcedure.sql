
/****** Object:  StoredProcedure [dbo].[USP_ManageCOARelationship]    Script Date: 09/25/2009 16:12:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCOARelationship')
DROP PROCEDURE USP_ManageCOARelationship
GO
CREATE PROCEDURE [dbo].[USP_ManageCOARelationship]
	@numCOARelationshipID numeric(9)=0 output,
	@numRelationshipID numeric(9),
	@numARAccountId numeric(9),
	@numAPAccountId numeric(9),
	@numDomainID numeric(9),
	@numUserCntId numeric(9),
	@numARParentAcntTypeID numeric(9),
	@numAPParentAcntTypeID numeric(9)
AS
SET NOCOUNT ON
	IF @numARParentAcntTypeID=0
	BEGIN
		SELECT @numARParentAcntTypeID = [numParntAcntTypeId] FROM [Chart_Of_Accounts] WHERE [numAccountId]=@numARAccountId
	END
	IF @numAPParentAcntTypeID=0
	BEGIN
		SELECT @numAPParentAcntTypeID = [numParntAcntTypeId] FROM [Chart_Of_Accounts] WHERE [numAccountId]=@numAPAccountId
	END
IF @numCOARelationshipID = 0 BEGIN
	

	INSERT INTO COARelationships (
		[numRelationshipID],
		[numARParentAcntTypeID],
		[numAPParentAcntTypeID],
		[numARAccountId],
		[numAPAccountId],
		[numDomainID],
		[dtCreateDate],
		[dtModifiedDate],
		[numCreatedBy],
		[numModifiedBy]
	)
	VALUES (
		@numRelationshipID,
		@numARParentAcntTypeID,
		@numAPParentAcntTypeID,
		@numARAccountId,
		@numAPAccountId,
		@numDomainID,
		GETUTCDATE(),
		GETUTCDATE(),
		@numUserCntId,
		@numUserCntId
	)
	SELECT @numCOARelationshipID = SCOPE_IDENTITY() 
END
ELSE BEGIN
PRINT @numAPParentAcntTypeID
	UPDATE COARelationships SET 
		[numARAccountId] = @numARAccountId,
		[numAPAccountId] = @numAPAccountId,
		[numARParentAcntTypeID]=@numARParentAcntTypeID,
		[numAPParentAcntTypeID]=@numAPParentAcntTypeID,
		[dtModifiedDate] = GETUTCDATE(),
		[numModifiedBy] = @numUserCntId
	WHERE [numCOARelationshipID] = @numCOARelationshipID
AND numDomainID = @numDomainID

END