SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_ValidateContainerQty')
DROP PROCEDURE USP_OpportunityMaster_ValidateContainerQty
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_ValidateContainerQty]
(                        
 @numDomainID AS NUMERIC(18,0),
 @numOppId AS NUMERIC(18,0)
)                        
AS 
BEGIN
	DECLARE @bitAddedEnoughContainer AS BIT = 1

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numContainerItem NUMERIC(18,0),
		fltRequiredQty FLOAT
	)

	DECLARE @TEMPItems TABLE
	(
		ID INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		numContainer NUMERIC(18,0),
		numItemQty FLOAT,
		numUnitHour FLOAT,
		bitContainer BIT
	)

	INSERT INTO 
		@TEMPItems
	SELECT
		OI.numItemCode,
		ISNULL(I.numContainer,0),
		ISNULL(I.numNoItemIntoContainer,0),
		OI.numUnitHour,
		(CASE WHEN ISNULL(I.bitContainer,0) = 1 THEN 1 ELSE 0 END)
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId=OI.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
		AND ISNULL(I.charItemType,0) = 'P'
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.numOppId=@numOppId
	ORDER BY
		ISNULL(I.bitContainer,0)

	IF (SELECT COUNT(*) FROM @TEMPItems WHERE bitContainer=0 AND numContainer=0) > 0
	BEGIN
		SET @bitAddedEnoughContainer = 0
	END
	ELSE
	BEGIN
		INSERT INTO 
			@TEMP
		SELECT
			T1.numContainer
			,SUM(T1.numNoItemIntoContainer)
		FROM
		(
			SELECT
				I.numContainer,
				CEILING(SUM(OI.numUnitHour) / CAST(I.numNoItemIntoContainer AS FLOAT)) AS numNoItemIntoContainer
			FROM
				OpportunityMaster OM
			INNER JOIN
				OpportunityItems OI
			ON
				OM.numOppId=OI.numOppId
			INNER JOIN
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
				AND ISNULL(I.charItemType,0) = 'P'
			WHERE
				OM.numDomainId=@numDomainID
				AND OM.numOppId=@numOppId
				AND numContainer > 0
			GROUP BY
				I.numContainer
				,I.numNoItemIntoContainer
		) AS T1
		GROUP BY
			T1.numContainer



		DECLARE @i AS INT = 1
		DECLARE @COUNT AS INT
		DECLARE @numTempContainer NUMERIC(18,0)
		DECLARE @numRequiredQty FLOAT

		SELECT @COUNT = COUNT(*) FROM @TEMP

		WHILE @i <= @COUNT
		BEGIN
			SELECT
				@numTempContainer=numContainerItem
				,@numRequiredQty=fltRequiredQty
			FROM
				@TEMP
			WHERE
				ID = @i


			IF ISNULL((SELECT SUM(numUnitHour) FROM OpportunityItems WHERE numOppId=@numOppId AND numItemCode=@numTempContainer),0) < @numRequiredQty
			BEGIN
				SET @bitAddedEnoughContainer = 0
				BREAK
			END

			SET @i = @i + 1
		END
	END

	SELECT @bitAddedEnoughContainer
END