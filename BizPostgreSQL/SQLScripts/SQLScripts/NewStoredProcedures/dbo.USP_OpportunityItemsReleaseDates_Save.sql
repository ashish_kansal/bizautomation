
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItemsReleaseDates_Save')
DROP PROCEDURE USP_OpportunityItemsReleaseDates_Save
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItemsReleaseDates_Save] 
(
	@ID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@dtReleaseDate DATE,
	@numQuantity NUMERIC(18,0),
	@tintReleaseStatus TINYINT
)
AS 
BEGIN
	UPDATE 
		OpportunityItems
	SET 
		ItemReleaseDate = @dtReleaseDate
	WHERE
		numOppID=@numOppID 
		AND numoppitemtCode=@numOppItemID
END
