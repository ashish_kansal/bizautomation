GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DelCompanyAssetSerial')
DROP PROCEDURE USP_DelCompanyAssetSerial
GO
CREATE PROCEDURE USP_DelCompanyAssetSerial
(@numAssetItemDTLID numeric(18))
as
begin
	delete from CompanyAssetSerial where numAssetItemDTLID=@numAssetItemDTLID;
end