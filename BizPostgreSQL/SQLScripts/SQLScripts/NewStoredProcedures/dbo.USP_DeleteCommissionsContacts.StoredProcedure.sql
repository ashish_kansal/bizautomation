SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteCommissionsContacts')
DROP PROCEDURE USP_DeleteCommissionsContacts
GO
CREATE PROCEDURE [dbo].[USP_DeleteCommissionsContacts]
@numCommContactID AS NUMERIC
AS
 
DELETE FROM CommissionContacts WHERE numCommContactID=@numCommContactID
GO
