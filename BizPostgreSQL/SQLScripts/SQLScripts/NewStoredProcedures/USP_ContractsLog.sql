
GO
/****** Object:  StoredProcedure [dbo].[Activity_Add]    Script Date: 07/26/2008 16:14:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
**  Inserts a new Activity added by a given Resource.  
**  
**  If no Resource is specified, then the Activity is related to  
**  the Unassigned Resource.  
*/  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ContractsLog')
DROP PROCEDURE USP_ContractsLog
GO
CREATE PROCEDURE [dbo].[USP_ContractsLog] 
	@numDomainID NUMERIC(18,0)
	,@numContractId NUMERIC(18,0)
AS 
BEGIN
	DECLARE @numTotalTime NUMERIC(18,0)
	SET @numTotalTime = ISNULL((SELECT ((ISNULL(numHours,0) * 60 * 60) + (ISNULL(numMinutes,0) * 60)) FROM Contracts WHERE numDomainId=@numDomainID AND numContractId=@numContractId),0)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numContractId NUMERIC(18,0)
		,vcType VARCHAR(300)
		,dtmCreatedOn DATETIME
		,numBalance VARCHAR(300)
		,dtStartTime DATETIME
		,dtEndTime DATETIME
	)

	INSERT INTO @TEMP
	(
		numContractId
		,vcType
		,dtmCreatedOn
		,numBalance
		,dtStartTime
		,dtEndTime
	)
	SELECT
		CL.numContractId
		,CASE 
			WHEN C.intType=3 THEN 'Incidents' 
			WHEN C.intType=1 THEN (CASE 
										WHEN ISNULL(CL.tintRecordType,0) = 1 THEN CONCAT('<a href="../projects/frmProjects.aspx?ProId=',SP.numProjectId,'" target="_blank">Project Task</a>')
										WHEN ISNULL(CL.tintRecordType,0) = 2 THEN CONCAT('<a href="../admin/ActionItemDetailsOld.aspx?CommId=',CL.numReferenceId ,'&lngType=0" target="_blank">Communication</a>')
										WHEN ISNULL(CL.tintRecordType,0) = 3 THEN 'Calendar'
										WHEN CP.numCaseId IS NOT NULL THEN CONCAT('<a href="../cases/frmCases.aspx?CaseID=', CL.numReferenceId,'" target="_blank">Case</a>')
										ELSE '-'
									END)
			ELSE '-' 
		END AS vcType,
		CL.dtmCreatedOn,
		dbo.fn_SecondsConversion(CAST((@numTotalTime - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=@numContractId AND CLInner.numContractsLogId <= CL.numContractsLogId),0)) AS VARCHAR)),
		CASE 
			WHEN COM.numCommId > 0 
			THEN COM.dtStartTime 
			WHEN SP.numTaskId > 0 THEN (SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog where numTaskId=SP.numTaskId AND tintAction=1) 
			ELSE NULL 
		END AS dtStartTime,
		CASE 
			WHEN COM.numCommId > 0 
			THEN COM.dtEndTime
			WHEN SP.numTaskId>0 
			THEN (SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog where numTaskId=SP.numTaskId AND tintAction=4) 
			ELSE NULL 
		END AS dtEndTime
	FROM
		Contracts C
	INNER JOIN
		ContractsLog CL
	ON
		C.numContractId = CL.numContractId
	LEFT JOIN
		Cases AS CP 
	ON 
		CL.numReferenceId=CP.numCaseId
		AND ISNULL(CL.tintRecordType,0) NOT IN (1,2,3)
	LEFT JOIN 
		Communication AS COM 
	ON 
		CL.numReferenceId=COM.numCommId 
		AND ISNULL(CL.tintRecordType,0)= 2
	LEFT JOIN 
		StagePercentageDetailsTask AS SP 
	ON 
		CL.numReferenceId=SP.numTaskId
		AND ISNULL(CL.tintRecordType,0)= 1
	WHERE
		C.numDomainId=@numDomainID
		AND C.numContractId=@numContractId
	ORDER BY
		CL.numContractsLogId ASC

	SELECT * FROM @TEMP
END
GO