--SELECT * FROM [AccountTypeDetail] Where len(vcaccountcode)>4
--DELETE FROM [AccountTypeDetail] WHERE [numAccountTypeID]>2232
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageAccountType')
DROP PROCEDURE USP_ManageAccountType
GO
CREATE PROCEDURE USP_ManageAccountType
(@numAccountTypeID NUMERIC(9)=0 OUTPUT,
@vcAccountCode varchar(50),
@vcAccountType varchar(100),
@numParentID NUMERIC(9)=null,
@numDomainID NUMERIC(9),
@tintMode TINYINT,
@bitActive BIT = 1)
AS 
BEGIN
DECLARE @newAccountCode VARCHAR(50)
IF @tintMode = 1
BEGIN

	SELECT @newAccountCode=dbo.GetAccountTypeCode(@numDomainID,@numParentID,0);
	INSERT INTO AccountTypeDetail (
		vcAccountCode,
		vcAccountType,
		numParentID,
		numDomainID,
		dtCreateDate,
		dtModifiedDate,
		bitSystem,
		bitActive
	) VALUES ( 
		@newAccountCode,
		@vcAccountType,
		@numParentID,
		@numDomainID,
		GETUTCDATE(),
		GETUTCDATE(),
		0,
		ISNULL(@bitActive,1))
	SET @numAccountTypeID = SCOPE_IDENTITY()
END

IF @tintMode = 2
BEGIN
	IF	(SELECT  LEN(vcAccountCode) FROM [AccountTypeDetail] WHERE numAccountTypeID=@numAccountTypeID AND numDomainID=@numDomainID) > 4
	BEGIN
--		SELECT @newAccountCode= dbo.GetAccountTypeCode(@numDomainID,@numParentID);
--		UPDATE [AccountTypeDetail]
--		SET vcAccountCode=@newAccountCode,
--			vcAccountType=@vcAccountType,
--			numParentID=@numParentID
--		WHERE numAccountTypeID=@numAccountTypeID AND numDomainID=@numDomainID
--		-- Update Child if any 
		
		
--Commented by chintan, reason:any account faling under given account type is using prefix of account type code,when changing account type code,then child account codes become invalid.
--		EXEC USP_UpdateAccountTypeCode @numDomainID,@numAccountTypeID,@numParentID
		
		UPDATE [AccountTypeDetail] SET [vcAccountType]  = @vcAccountType,bitActive=ISNULL(@bitActive,1) WHERE [numAccountTypeID]=@numAccountTypeID AND [numDomainID]=@numDomainID
--		UPDATE [AccountTypeDetail]
--		SET vcAccountCode=dbo.GetAccountTypeCode(numDomainID,@numAccountTypeID),
--			numParentID=@numAccountTypeID
--		WHERE numParentID=@numAccountTypeID AND numDomainID=@numDomainID
--		
	END
		
END

IF @tintMode = 3
BEGIN
		IF EXISTS(SELECT * FROM dbo.AccountTypeDetail WHERE bitSystem=1 AND numDomainID=@numDomainID AND numAccountTypeID=@numAccountTypeID)
		BEGIN
			RAISERROR('SYSTEM', 16, 1);
			RETURN
		END
		
		IF EXISTS(SELECT * FROM dbo.AccountTypeDetail WHERE (vcAccountCode LIKE '01020102%' OR vcAccountCode LIKE '01010105') AND numDomainID=@numDomainID AND numAccountTypeID=@numAccountTypeID)
		BEGIN
			RAISERROR('AR/AP', 16, 1);
			RETURN
		END
		IF EXISTS(SELECT * FROM dbo.Chart_Of_Accounts WHERE numParntAcntTypeId=@numAccountTypeID)
		BEGIN
			RAISERROR('CHILD_ACCOUNT', 16, 1);
			RETURN
		END
		IF (SELECT COUNT(*) FROM [AccountTypeDetail] WHERE numParentID=@numAccountTypeID) >0
		BEGIN
			RAISERROR('CHILD', 16, 1);
			RETURN
		END
		ELSE
		BEGIN
		--only allow deltion of where account code lenth is > 4
			DELETE FROM [AccountTypeDetail] WHERE numDomainID=@numDomainID AND  numAccountTypeID=@numAccountTypeID AND LEN(vcAccountCode) > 4	
		END
		

END


	
	
	
END