GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPIThrottleCounter_Get' ) 
    DROP PROCEDURE USP_BizAPIThrottleCounter_Get
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 16 April 2014
-- Description:	Gets Throttle counter for id
-- =============================================
CREATE PROCEDURE USP_BizAPIThrottleCounter_Get
	@ID VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT * FROM BizAPIThrottleCounter WHERE vcID = @ID
	
END
GO