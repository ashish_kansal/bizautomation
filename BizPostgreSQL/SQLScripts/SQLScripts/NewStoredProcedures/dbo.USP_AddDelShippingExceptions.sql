GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_AddDelShippingExceptions' ) 
    DROP PROCEDURE USP_AddDelShippingExceptions
GO
CREATE PROCEDURE USP_AddDelShippingExceptions
   
     @numDomainID NUMERIC(9)
	,@numClassificationID NUMERIC(18, 0) 
	,@PercentAbove FLOAT 
	,@FlatAmt FLOAT 
	,@tintMode	TINYINT
	,@numShippingExceptionID NUMERIC(9) =0
AS 
BEGIN
      IF @tintMode = 0 
        BEGIN
			IF EXISTS(
						SELECT * FROM dbo.ShippingExceptions SE
						 WHERE SE.numClassificationID = @numClassificationID AND SE.numDomainID = @numDomainID
			          )
                BEGIN
	                  raiserror('DUPLICATE',16,1);
		              RETURN ;
                END		
			
			INSERT INTO ShippingExceptions
			(
				 numDomainID
				,numClassificationID
				,PercentAbove
				,FlatAmt
			)
			VALUES
			(
				 @numDomainID
				,@numClassificationID
				,@PercentAbove
				,@FlatAmt
			)
	END
	IF @tintMode = 1 
    BEGIN
        DELETE FROM dbo.ShippingExceptions
        WHERE numShippingExceptionID = @numShippingExceptionID 
            AND numDomainID=@numDomainID
    END	
END