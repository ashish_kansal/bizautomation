GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_GetByItem')
DROP PROCEDURE USP_WarehouseItems_GetByItem
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_GetByItem]
(   
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@numWLocationID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @numSaleUOMFactor AS FLOAT
	DECLARE @numPurchaseUOMFactor AS FLOAT
	DECLARE @bitKitParent BIT

	SELECT 
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = ISNULL(numSaleUnit,0),
		@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
		@bitKitParent = ISNULL(bitKitParent,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode   

	SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
	SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)

	DECLARE @TEMPChildItems TABLE
	(
		numItemKitID NUMERIC(18,0), numItemCode NUMERIC(18,0), numQtyItemsReq FLOAT, numCalculatedQty FLOAT
	)

	IF @bitKitParent = 1
	BEGIN
		;WITH CTE(numItemKitID, numItemCode, numQtyItemsReq, numCalculatedQty) AS 
		( 
			SELECT   
				CONVERT(NUMERIC(18,0),0),
				numItemCode,
				DTL.numQtyItemsReq,
				CAST(DTL.numQtyItemsReq AS NUMERIC(9, 0)) AS numCalculatedQty
			FROM
				Item
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID = numItemCode
			WHERE 
				numItemKitID = @numItemCode
			UNION ALL
			SELECT 
				Dtl.numItemKitID,
				i.numItemCode,
				DTL.numQtyItemsReq,
				CAST(( DTL.numQtyItemsReq * c.numCalculatedQty ) AS NUMERIC(9, 0)) AS numCalculatedQty
			FROM 
				Item i
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				Dtl.numChildItemID = i.numItemCode
			INNER JOIN 
				CTE c 
			ON 
				Dtl.numItemKitID = c.numItemCode
			WHERE    
				Dtl.numChildItemID != @numItemCode
		)

		INSERT INTO @TEMPChildItems (numItemKitID, numItemCode, numQtyItemsReq, numCalculatedQty) SELECT numItemKitID, numItemCode, numQtyItemsReq, numCalculatedQty FROM CTE
	END

	SELECT
		WarehouseItems.numWareHouseItemID
		,WarehouseItems.numWarehouseID
		,WarehouseItems.numWLocationID
		,ISNULL(Warehouses.vcWareHouse,'') vcExternalLocation
		,numItemID
		,(CASE WHEN ISNULL(Item.bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(WarehouseItems.numItemID,WarehouseItems.numWareHouseID) ELSE 0 END) numBuildableQty
		,Item.numAssetChartAcntId
		,CASE 
			WHEN ISNULL(Item.bitKitParent,0)=1 
			THEN ISNULL((SELECT CAST(FLOOR(MIN(CASE 
									WHEN ISNULL(WIINNER.numOnHand, 0) = 0 THEN 0
                                    WHEN ISNULL(WIINNER.numOnHand, 0) >= TCI.numQtyItemsReq AND TCI.numQtyItemsReq > 0 THEN ISNULL(WIINNER.numOnHand, 0) / TCI.numQtyItemsReq
                                    ELSE 0
                                    END)
						) AS FLOAT) FROM @TEMPChildItems TCI INNER JOIN WareHouseItems WIINNER ON TCI.numItemCode=WIINNER.numItemID AND WIINNER.numWareHouseID=WareHouseItems.numWareHouseID),0)
			ELSE ISNULL(numOnHand,0) + ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID),0)
		END AS [OnHand]
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
		,CASE 
			WHEN ISNULL(Item.bitKitParent,0)=1 OR ISNULL(Item.bitAssembly,0)=1 
			THEN 0 
			ELSE CAST(ISNULL((SELECT 
									SUM(numUnitHour) 
								FROM 
									OpportunityItems OI 
								INNER JOIN 
									OpportunityMaster OM 
								ON 
									OI.numOppID=OM.numOppID 
								WHERE 
									OM.numDomainID=@numDomainID 
									AND tintOppType=1 
									AND tintOppStatus=0 
									AND OI.numItemCode=WareHouseItems.numItemID 
									AND OI.numWarehouseItmsID=WareHouseItems.numWareHouseItemID),0) AS FLOAT) 
		END AS [Requisitions]
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
		,CASE WHEN ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder]
		,ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) + ISNULL((SELECT SUM(ISNULL(WI.numOnHand,0) + ISNULL(WI.numAllocation,0)) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=WareHouseItems.numWareHouseID),0) AS TotalOnHand
		,ISNULL(Item.monAverageCost,0) monAverageCost
		,((ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)) * (CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0) END)) AS monCurrentValue
		,ROUND(ISNULL(WareHouseItems.monWListPrice,0),2) Price
		,ISNULL(WareHouseItems.vcWHSKU,'') as SKU
		,ISNULL(WareHouseItems.vcBarCode,'') as BarCode
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 
			THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
			ELSE ''
		END AS vcAttribute
		,dbo.fn_GetUOMName(Item.numBaseUnit) As vcBaseUnit
		,dbo.fn_GetUOMName(Item.numSaleUnit) As vcSaleUnit
		,dbo.fn_GetUOMName(Item.numPurchaseUnit) As vcPurchaseUnit
		,CAST((@numSaleUOMFactor * CASE 
										WHEN ISNULL(Item.bitKitParent,0)=1 
										THEN ISNULL((SELECT CAST(FLOOR(MIN(CASE 
																			WHEN ISNULL(WIINNER.numOnHand, 0) = 0 THEN 0
																			WHEN ISNULL(WIINNER.numOnHand, 0) >= TCI.numQtyItemsReq AND TCI.numQtyItemsReq > 0 THEN ISNULL(WIINNER.numOnHand, 0) / TCI.numQtyItemsReq
																			ELSE 0
																		END)) AS FLOAT) 
													FROM @TEMPChildItems TCI INNER JOIN WareHouseItems WIINNER ON TCI.numItemCode=WIINNER.numItemID AND WIINNER.numWareHouseID=WareHouseItems.numWareHouseID),0)
										ELSE ISNULL(numOnHand,0) 
									END) AS FLOAT) as OnHandUOM
		,CAST((@numPurchaseUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM
		,CAST((@numPurchaseUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM
		,CAST((@numSaleUOMFactor * Case when ISNULL(Item.bitKitParent,0)=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM
		,CONCAT('[',STUFF((SELECT 
								CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,',"WarehouseLocationID":',WIInner.numWLocationID,',"Warehouse":"',ISNULL(W.vcWareHouse,''),'", "OnHand":',ISNULL(WIInner.numOnHand,0),', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
							FROM 
								WareHouseItems WIInner
							INNER JOIN
								Warehouses W
							ON
								WIInner.numWareHouseID = W.numWareHouseID
							INNER JOIN
								WarehouseLocation WL
							ON
								WIInner.numWLocationID = WL.numWLocationID
							WHERE 
								WIInner.numDomainID=@numDomainID 
								AND WIInner.numItemID=@numItemCode
								AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID
							ORDER BY
								WL.vcLocation ASC
							FOR XML PATH('')),1,1,''),']') vcInternalLocations
		,(CASE WHEN EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=WareHouseItems.numWareHouseItemID AND ISNULL(numQty,0) > 0) THEN 1 ELSE 0 END) bitSerialLotExists
	FROM
		WareHouseItems
	INNER JOIN
		Item
	ON
		WareHouseItems.numItemID = Item.numItemCode
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		WareHouseItems.numDomainID=@numDomainID
		AND WareHouseItems.numItemID=@numItemCode
		AND ISNULL(WareHouseItems.numWLocationID,0) = 0
		AND (ISNULL(@numWarehouseID,0) = 0 OR WareHouseItems.numWareHouseID=@numWarehouseID)
		AND (ISNULL(@numWLocationID,0) = 0 OR EXISTS (SELECT WI.numWareHouseItemID FROM WareHouseItems WI WHERE WI.numDomainID=@numDomainID AND WI.numItemID=@numItemCode AND WI.numWareHouseID=@numWarehouseID AND ISNULL(WI.numWLocationID,0) = @numWLocationID))
END
GO