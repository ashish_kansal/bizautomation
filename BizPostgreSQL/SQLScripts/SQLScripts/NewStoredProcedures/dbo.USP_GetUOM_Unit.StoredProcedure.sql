GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUOM_Unit')
DROP PROCEDURE USP_GetUOM_Unit
GO
CREATE PROCEDURE [dbo].[USP_GetUOM_Unit]                                                                          
 @numDomainID as numeric(9),
 @numUOMId as numeric(9)
AS       
      
   SELECT numUOMId,vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainID=@numDomainID AND numUOMId=@numUOMId
