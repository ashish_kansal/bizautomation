GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemCurrencyPrice_Save')
DROP PROCEDURE USP_ItemCurrencyPrice_Save
GO
CREATE PROCEDURE [dbo].[USP_ItemCurrencyPrice_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@vcRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @hDocItem INT
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcRecords

	--VALIDATE PROMOTION OF COUPON CODE
	DECLARE @TEMP TABLE
	(
		numCurrencyID NUMERIC(18,0),
		monListPrice DECIMAL(20,5)
	)

	INSERT INTO @TEMP
	(
		numCurrencyID,
		monListPrice
	)
	SELECT
		numCurrencyID,
		monListPrice
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		numCurrencyID NUMERIC(18,0),
		monListPrice DECIMAL(20,5)
	)

	EXEC sp_xml_removedocument @hDocItem 


	UPDATE
		ICP
	SET
		monListPrice = ISNULL(T.monListPrice,0)
	FROM
		ItemCurrencyPrice ICP
	INNER JOIN
		@TEMP T
	ON
		ICP.numCurrencyID = T.numCurrencyID
	WHERE
		ICP.numDomainID = @numDomainID
		AND ICP.numItemCode = @numItemCode
		
	INSERT INTO ItemCurrencyPrice
	(
		numDomainID
		,numItemCode
		,numCurrencyID
		,monListPrice
	)
	SELECT
		@numDomainID
		,@numItemCode
		,T.numCurrencyID
		,ISNULL(T.monListPrice,0)
	FROM
		@TEMP T
	WHERE
		numCurrencyID NOT IN (SELECT numCurrencyID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
END
GO