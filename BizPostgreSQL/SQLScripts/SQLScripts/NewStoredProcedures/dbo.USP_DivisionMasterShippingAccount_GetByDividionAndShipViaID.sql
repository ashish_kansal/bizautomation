GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DivisionMasterShippingAccount_GetByDividionAndShipViaID')
DROP PROCEDURE USP_DivisionMasterShippingAccount_GetByDividionAndShipViaID
GO
CREATE PROCEDURE [dbo].[USP_DivisionMasterShippingAccount_GetByDividionAndShipViaID]    
	@numDivisionID NUMERIC(18,0)
	,@numShipViaID NUMERIC(18,0)
AS
BEGIN
	SELECT * FROM DivisionMasterShippingAccount WHERE numDivisionID=@numDivisionID AND numShipViaID=@numShipViaID
END
GO