/****** Object:  StoredProcedure [dbo].[USP_GetOpportunitySalesTemplate]    Script Date: 09/01/2009 01:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SELECT * FROM  [OpportunitySalesTemplate]
-- EXEC USP_GetOpportunitySalesTemplate 0,0,1,1,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpportunitySalesTemplate')
DROP PROCEDURE USP_GetOpportunitySalesTemplate
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunitySalesTemplate]
          @numSalesTemplateID NUMERIC(9)  = 0,
          @numOppId           NUMERIC(9),
          @numDomainID NUMERIC(9),
          @tintMode TINYINT,
          @numDivisionID NUMERIC(9)=0,
          @ClientTimeZoneOffset INT=0,
		  @numSiteID NUMERIC(18,0) = 0
AS
    
IF @tintMode = 0 
BEGIN
	IF @numOppId > 0
  BEGIN
  	SELECT @numDivisionID = numDivisionId
    FROM   OpportunityMaster
    WHERE  numOppId = @numOppId  AND numDomainID= @numDomainID
  END
    PRINT @numDivisionID
    
  IF (@numDivisionID > 0)
    BEGIN
      SELECT numSalesTemplateID,
             vcTemplateName,
             tintType,
             CASE tintType
             WHEN 0 THEN 'Specific'
             WHEN 1 THEN 'Generic'
             END AS TemplateType ,
             numOppId,
             numDivisionID,
             [dbo].[fn_GetComapnyName](numDivisionID) AS OrganizationName,
             numDomainID, tintAppliesTo,
			 dbo.fn_GetContactName(OpportunitySalesTemplate.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunitySalesTemplate.dtCreatedDate)) AS CreatedBy
      FROM   OpportunitySalesTemplate
      WHERE  (numSalesTemplateID = @numSalesTemplateID
               OR @numSalesTemplateID = 0)
             AND numDivisionID = @numDivisionID
             AND numDomainID= @numDomainID
    END	
END  
ELSE IF @tintMode =1 
BEGIN
	IF ISNULL(@numSiteID,0) > 0 AND ISNULL((SELECT COUNT(*) FROM OpportunitySalesTemplate WHERE numDomainID= @numDomainID AND numDivisionID=@numDivisionID AND tintType=0),0) = 0
	BEGIN
		INSERT INTO OpportunitySalesTemplate
		(
			vcTemplateName,tintType,numDivisionID,numDomainID,dtCreatedDate
		)
		VALUES
		(
			CONCAT('List - ',ISNULL((SELECT vcSiteName FROM Sites WHERE numDomainID=@numDomainID AND numSiteID=@numSiteID),'')),0,@numDivisionID,@numDomainID,GETUTCDATE()
		)
	END

    SELECT 
		numSalesTemplateID,
        vcTemplateName,
		CONVERT(VARCHAR(20),numSalesTemplateID) + ',' + CONVERT(VARCHAR(20),ISNULL(numOppId,0)  ) AS numSalesTemplateID1,
        (SELECT COUNT(*) FROM SalesTemplateItems WHERE numSalesTemplateID =OpportunitySalesTemplate.[numSalesTemplateID]) ItemCount, tintAppliesTo, numDivisionID,
		dbo.fn_GetContactName(OpportunitySalesTemplate.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunitySalesTemplate.dtCreatedDate)) AS CreatedBy
    FROM
		OpportunitySalesTemplate
    WHERE  
		numDomainID= @numDomainID
        AND tintType  = 1 -- Generic  
    UNION
    SELECT 
		numSalesTemplateID,
        vcTemplateName,
        CONVERT(VARCHAR(20),numSalesTemplateID) + ',' + CONVERT(VARCHAR(20),ISNULL(numOppId,0)  ) AS numSalesTemplateID1,
        (SELECT COUNT(*) FROM SalesTemplateItems WHERE numSalesTemplateID =OpportunitySalesTemplate.[numSalesTemplateID]) ItemCount, tintAppliesTo, numDivisionID,
		dbo.fn_GetContactName(OpportunitySalesTemplate.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunitySalesTemplate.dtCreatedDate)) AS CreatedBy
    FROM 
		OpportunitySalesTemplate
    WHERE  
		numDomainID= @numDomainID
		AND [numDivisionID] = @numDivisionID
        AND tintType  = 0 -- specific     
END
ELSE IF @tintMode =2 
BEGIN
	   SELECT numSalesTemplateID,
             vcTemplateName,
             tintType,
             CASE tintType
             WHEN 0 THEN 'Specific'
             WHEN 1 THEN 'Generic'
             END AS TemplateType ,
             numOppId,
             numDivisionID,
             [dbo].[fn_GetComapnyName](numDivisionID) AS OrganizationName,
             numDomainID, tintAppliesTo,
			 dbo.fn_GetContactName(OpportunitySalesTemplate.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunitySalesTemplate.dtCreatedDate)) AS CreatedBy
      FROM   OpportunitySalesTemplate
      WHERE  (numSalesTemplateID = @numSalesTemplateID
               OR @numSalesTemplateID = 0)
             AND numDomainID= @numDomainID
END
ELSE IF @tintMode =3 --select all generic template
BEGIN
	   SELECT numSalesTemplateID,
             vcTemplateName,
             tintType,
             CASE tintType
             WHEN 0 THEN 'Specific'
             WHEN 1 THEN 'Generic'
             END AS TemplateType ,
             numOppId,
             numDivisionID,
             [dbo].[fn_GetComapnyName](numDivisionID) AS OrganizationName,
             numDomainID, tintAppliesTo,
			 dbo.fn_GetContactName(OpportunitySalesTemplate.numCreatedby)+ ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunitySalesTemplate.dtCreatedDate)) AS CreatedBy
      FROM   OpportunitySalesTemplate
      WHERE  numDomainID= @numDomainID
             AND [tintType] = 1
END

  
  
