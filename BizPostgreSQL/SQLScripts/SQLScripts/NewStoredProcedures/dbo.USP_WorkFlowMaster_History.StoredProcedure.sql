GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkFlowMaster_History')
DROP PROCEDURE USP_WorkFlowMaster_History
GO
  
 
-- =============================================      
-- Author:  <Author,,Sachin Sadhu>      
-- Create date: <Create Date,,3rdApril2014>      
-- Description: <Description,,To maintain Work rule execution history>      
-- =============================================      
Create PROCEDURE [dbo].[USP_WorkFlowMaster_History]       
 -- Add the parameters for the stored procedure here      
@numDomainID numeric(18,0),      
@numFormID INT  ,    
@CurrentPage int,                                                              
@PageSize int,                                                              
@TotRecs int output,           
@SortChar char(1)='0' ,                                                             
@columnName as Varchar(50),                                                              
@columnSortOrder as Varchar(50)  ,      
@SearchStr  as Varchar(50)         
AS      
BEGIN      
 -- SET NOCOUNT ON added to prevent extra result sets from      
 -- interfering with SELECT statements.      
 SET NOCOUNT ON;      
      
    -- Insert statements for procedure here      
    Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                               
        numWFID NUMERIC(18,0) ,    
        numFormID NUMERIC(18,0),    
        vcWFName VARCHAR(50),      
        vcPOppName VARCHAR(500),      
        vcWFDescription VARCHAR(max),      
        numRecordID NUMERIC(18,0),      
        tintProcessStatus int,      
        bitSuccess int,      
        vcDescription VARCHAR(max),      
        vcFormName VARCHAR(50),      
        dtExecutionDate DATETIME,    
        numOppId NUMERIC(18,0),      
        STATUS VARCHAR(50),    
        TriggeredOn VARCHAR(50) ,  
        vcUserName VARCHAR(200)   ,
		dtCreatedDate     DATETIME                                                   
 )           
declare @strSql as varchar(8000)                                                        
          
set  @strSql='  SELECT       
      
   m.numWFID,      
   m.numFormID,    
   m.vcWFName,      
   ISNULL(o.vcPOppName,'''') as vcPOppName,      
   m.vcWFDescription,      
   q.numRecordID,      
   q.tintProcessStatus,      
   e.bitSuccess,      
  ISNULL( e.vcDescription,''Waiting'') as  vcDescription,      
   DFM.vcFormName,      
   e.dtExecutionDate,    
   o.numOppId,     
   CASE WHEN q.tintProcessStatus=1 THEN ''Pending Execution'' WHEN q.tintProcessStatus= 2 THEN ''In Progress''  WHEN q.tintProcessStatus=3 THEN ''Success'' WHEN q.tintProcessStatus=4 then ''Failed'' WHEN q.tintProcessStatus=6 then ''Condition Not Matched'' else ''No WF Found'' END AS STATUS,      
   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN ''Create'' WHEN m.tintWFTriggerOn=2 THEN ''Edit'' WHEN m.tintWFTriggerOn=3 THEN ''Create or Edit'' WHEN m.tintWFTriggerOn=4 THEN ''Fields Update'' WHEN m.tintWFTriggerOn=5 THEN ''Delete'' WHEN m.intDays>0 THEN     
''Date Field'' ELSE ''NA'' end AS TriggeredOn,  
 dbo.fn_GetContactName(ISNULL(m.numCreatedBy,0)) AS vcUserName   ,
 q.dtCreatedDate   
  FROM dbo.WorkFlowMaster as m       
  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
  LEFT JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId  AND o.numDomainID=m.numDomainID
  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3 AND q.tintProcessStatus NOT IN (6,5)  WHERE m.numDomainID='+ convert(varchar(15),@numDomainID)       
          
if @SortChar<>'0' set @strSql=@strSql + ' And m.vcWFName like '''+@SortChar+'%'''           
      
if @numFormID <> 0 set @strSql=@strSql + ' And m.numFormID = '+ convert(varchar(15),@numFormID) +''         
      
if @SearchStr<>'' set @strSql=@strSql + ' And (m.vcWFName like ''%'+@SearchStr+'%'' or       
m.vcWFDescription like ''%'+@SearchStr+'%'') '       
          
set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder          
     
insert into #tempTable( numWFID ,    
            numFormID ,    
            vcWFName ,    
            vcPOppName ,    
            vcWFDescription ,    
            numRecordID ,    
            tintProcessStatus ,    
            bitSuccess ,    
            vcDescription ,    
            vcFormName ,    
            dtExecutionDate ,    
            numOppId ,    
            STATUS ,    
            TriggeredOn,vcUserName,dtCreatedDate) exec(@strSql)          
          
 declare @firstRec as integer                                                              
 declare @lastRec as integer                                                   
 set @firstRec= (@CurrentPage-1) * @PageSize                                                              
 set @lastRec= (@CurrentPage*@PageSize+1)                                                               
      
 SELECT  @TotRecs = COUNT(*)  FROM #tempTable       
       
  SELECT       
   ID AS RowNo,*    
   From #tempTable T        
   WHERE ID > @firstRec and ID < @lastRec  order by ID      
       
       
--   SELECT       
--   ROW_NUMBER() OVER(ORDER BY m.numWFID ASC) AS RowNo,    
--   m.numWFID,      
--   m.vcWFName,      
--   o.vcPOppName,      
--   m.vcWFDescription,      
--   q.numRecordID,      
--   q.tintProcessStatus,      
--   e.bitSuccess,      
--   e.vcDescription ,      
--   DFM.vcFormName,      
--   e.dtExecutionDate,    
--   o.numOppId,      
--   CASE WHEN q.tintProcessStatus=1 THEN 'Pending Execution' WHEN q.tintProcessStatus= 2 THEN 'In Progress' WHEN q.tintProcessStatus=3 THEN 'Success' WHEN q.tintProcessStatus=4 then 'Failed' else 'No WF Found' END AS STATUS,      
--   CASE WHEN (m.tintWFTriggerOn=1 AND m.intDays=0)THEN 'Create' WHEN m.tintWFTriggerOn=2 THEN 'Edit' WHEN m.tintWFTriggerOn=3 THEN 'Create or Edit' WHEN m.tintWFTriggerOn=4 THEN 'Fields Update' WHEN m.tintWFTriggerOn=5 THEN 'Delete' WHEN m.intDays>0 TH
  
--E--N     
--'Date Field' ELSE 'NA' end AS TriggeredOn      
--  FROM dbo.WorkFlowMaster as m       
--  INNER JOIN dbo.WorkFlowQueue AS q ON m.numWFID=q.numWFID       
--  LEFT JOIN dbo.WorkFlowQueueExecution AS e ON e.numWFQueueID=q.numWFQueueID      
--  INNER JOIN dbo.OpportunityMaster o ON q.numRecordID =o.numOppId      
--  JOIN DynamicFormMaster DFM ON m.numFormID=DFM.numFormID AND DFM.tintFlag=3  join #tempTable T on T.numWFID=m.numWFID      
--    
--   WHERE ID > @firstRec and ID < @lastRec AND m.numDomainID=@numDomainID order by ID      
         
   DROP TABLE #tempTable      
        
        
        
        
        
        
        
        
        
        
     
    
      
      
END 
