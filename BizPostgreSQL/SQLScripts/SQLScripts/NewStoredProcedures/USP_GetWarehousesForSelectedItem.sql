GO

/****** Object:  StoredProcedure [dbo].[USP_GetWarehousesForSelectedItem]    Script Date: 04-05-2018 10:59:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWarehousesForSelectedItem')
DROP PROCEDURE USP_GetWarehousesForSelectedItem
GO

CREATE PROCEDURE [dbo].[USP_GetWarehousesForSelectedItem]    
(
	@numItemCode AS VARCHAR(20)=''  ,
	@numWareHouseID AS NUMERIC(18,0) = 0,
	@numQty FLOAT,
	@numOrderSource NUMERIC(18,0),
	@tintSourceType TINYINT,
	@numShipToCountry NUMERIC(18,0) = 0,
	@numShipToState NUMERIC(18,0) = 0,
	@vcSelectedKitChildItems VARCHAR(MAX) = ''
)
AS
BEGIN  
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numBaseUnit AS NUMERIC(18,0)
	DECLARE @vcUnitName as VARCHAR(100) 
	DECLARE @numSaleUnit AS NUMERIC(18,0)
	DECLARE @vcSaleUnitName as VARCHAR(100) 
	DECLARE @numPurchaseUnit AS NUMERIC(18,0)
	DECLARE @vcPurchaseUnitName as VARCHAR(100) 
	declare @bitSerialize as bit     
	DECLARE @bitKitParent BIT
	DECLARE @bitAssembly BIT
 
	SELECT 
		@numDomainID = numDomainID,
		@numBaseUnit = ISNULL(numBaseUnit,0),
		@numSaleUnit = ISNULL(numSaleUnit,0),
		@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
		@bitSerialize=bitSerialized,
		@bitAssembly=ISNULL(bitAssembly,0),
		@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
								   AND ISNULL(bitAssembly,0) = 1 THEN 0
							  WHEN ISNULL(bitKitParent,0) = 1 THEN 1
							  ELSE 0
						 END ) 
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode    

	DECLARE @TEMPWarehouse TABLE
	(
		numWarehouseItemID NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numOnHand FLOAT
	)

	INSERT INTO @TEMPWarehouse
	(
		numWarehouseItemID
		,numWarehouseID
		,numOnHand
	)
	SELECT
		WareHouseItems.numWarehouseItemID
		,WareHouseItems.numWareHouseID
		,ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WareHouseItems.numItemID AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID),0)
	FROM
		WareHouseItems
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		WareHouseItems.numItemID = @numItemCode
		AND WarehouseLocation.numWLocationID IS NULL


	DECLARE @TEMP TABLE
	(	
		ID INT IDENTITY(1,1)
		,numWareHouseItemID NUMERIC(18,0)
		,bitWarehouseMapped BIT
	)

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND ISNULL(bitAutoWarehouseSelection,0)= 1)
	BEGIN
		INSERT INTO @TEMP
		(
			numWareHouseItemID
			,bitWarehouseMapped
		)
		SELECT 
			numWareHouseItemID
			,1
		FROM
		(
			SELECT 
				ROW_NUMBER() OVER(PARTITION BY titOnHandOrder ORDER BY titOnHandOrder ASC) numRowIndex	
				,*
			FROM
			(
				SELECT
					TW.numWareHouseItemID
					,(CASE 
						WHEN @bitKitParent = 1 
						THEN dbo.fn_IsKitCanBeBuild(@numDomainID,@numItemCode,@numQty,TW.numWarehouseID,@vcSelectedKitChildItems)
						ELSE (CASE WHEN TW.numOnHand >= @numQty THEN 1 ELSE 0 END) 
					END) titOnHandOrder
					,MassSalesFulfillmentWP.intOrder
				FROM
					MassSalesFulfillmentWM
				INNER JOIN
					MassSalesFulfillmentWMState
				ON
					MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
				INNER JOIN
					MassSalesFulfillmentWP
				ON
					MassSalesFulfillmentWM.ID = MassSalesFulfillmentWP.numMSFWMID
				INNER JOIN
					@TEMPWarehouse TW
				ON
					MassSalesFulfillmentWP.numWarehouseID	= TW.numWarehouseID
				WHERE
					MassSalesFulfillmentWM.numDomainID = @numDomainID
					AND MassSalesFulfillmentWM.numOrderSource = @numOrderSource
					AND MassSalesFulfillmentWM.tintSourceType = @tintSourceType
					AND MassSalesFulfillmentWM.numCountryID = @numShipToCountry
					AND MassSalesFulfillmentWMState.numStateID = @numShipToState
					
			) TEMP
		) T1
		ORDER BY
			titOnHandOrder DESC, intOrder ASC
	END

	SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
	SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
	SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit

	DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
	DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

	SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numSaleUnit,@numItemCode,@numDOmainID,@numBaseUnit)
	SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numPurchaseUnit,@numItemCode,@numDOmainID,@numBaseUnit)

	SELECT 
		(WareHouseItems.numWareHouseItemId),
		ISNULL(WareHouseItems.numWLocationID,0) numWLocationID,
		ISNULL(vcWareHouse,'') AS vcWareHouse, 
		dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemId,@bitSerialize) as Attr, 
		WareHouseItems.monWListPrice,
		CASE 
			WHEN @bitKitParent=1 
			THEN dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID) 
			ELSE ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numItemID=WareHouseItems.numItemID AND WI.numWarehouseID=WareHouseItems.numWarehouseID),0) + ISNULL(numOnHand,0) 
		END numOnHand,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numOnOrder,0) END numOnOrder,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numReorder,0) END numReorder,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numAllocation,0) END numAllocation,
		CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numBackOrder,0) END numBackOrder,
		Warehouses.numWareHouseID,
		@vcUnitName AS vcUnitName,
		@vcSaleUnitName AS vcSaleUnitName,
		@vcPurchaseUnitName AS vcPurchaseUnitName,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN CAST(CAST(dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID)/@numSaleUOMFactor AS INT) AS VARCHAR) else CAST(CAST(ISNULL((SELECT SUM(WI.numOnHand) FROM WareHouseItems WI INNER JOIN WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID WHERE WI.numItemID=WareHouseItems.numItemID AND WI.numWarehouseID=WareHouseItems.numWarehouseID),0) + ISNULL(numOnHand,0) /@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numOnHandUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numOnOrder,0)/@numPurchaseUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numOnOrderUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numReorder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numReorderUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numAllocation,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numAllocationUOM,
		CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
		THEN
			CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numBackOrder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
		ELSE
			'-'
		END AS numBackOrderUOM
		,T.bitWarehouseMapped
	FROM 
		WareHouseItems
	JOIN 
		Warehouses 
	ON 
		Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
	LEFT JOIN
		@TEMP T
	ON
		WareHouseItems.numWareHouseItemID = T.numWareHouseItemID
	WHERE 
		numItemID=@numItemCode
		AND ISNULL(WareHouseItems.numWLocationID,0) = 0
	ORDER BY
		(CASE 
			WHEN T.ID IS NULL 
			THEN (CASE 
					WHEN WareHouseItems.numWareHouseID = @numWareHouseID then 99991
					ELSE 99992
				END)
			ELSE T.ID 
		END) ASC
END
GO


