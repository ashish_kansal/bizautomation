GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Project_GetStages')
DROP PROCEDURE USP_Project_GetStages
GO
CREATE PROCEDURE [dbo].[USP_Project_GetStages]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@numMileStoneID NUMERIC(18,0)
	,@vcMilestoneName VARCHAR(500)
)
AS 
BEGIN
	SELECT 
		@numMileStoneID numMileStoneID
		,StagePercentageDetails.numStageDetailsId
		,StagePercentageDetails.vcStageName AS vcStageName
		,dbo.GetTotalProgress(@numDomainID,@numProId,2,3,'',StagePercentageDetails.numStageDetailsId) numTotalProgress
	FROM
		StagePercentageDetails
	WHERE
		StagePercentageDetails.numDomainID = @numDomainID
		AND StagePercentageDetails.numProjectID = @numProId
		AND vcMileStoneName = @vcMilestoneName
END
