GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateProjectAccount')
DROP PROCEDURE USP_UpdateProjectAccount
GO
CREATE PROCEDURE  USP_UpdateProjectAccount
@numDomainID NUMERIC(9),
@numProID NUMERIC(9),
@numAccountID NUMERIC(9)
AS 
BEGIN
	UPDATE [ProjectsMaster] SET numAccountID =@numAccountID WHERE [numProId]=@numProID AND [numDomainId]=@numDomainID
END