
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBankDetailsList')
	DROP PROCEDURE USP_GetBankDetailsList
GO

/****** Added By : Joseph ******/
/****** Gets Bank Account List from Table BankDetails for a UserContactID******/


CREATE PROCEDURE [dbo].[USP_GetBankDetailsList]
	@tintMode TINYINT=0,
	@numDomainID numeric(18, 0),
	@numUserContactID numeric(18, 0),
	@numBankMasterID numeric(18, 0),
	@vcUserName VARCHAR(50)='',
	@vcPassword VARCHAR(50)=''

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	IF @tintMode = 0
	BEGIN
		SELECT
		[numBankDetailID],
		[numBankMasterID],
		[numDomainID],
		[numUserContactID],
		[numAccountID],
		[vcBankID],
		[vcAccountNumber],
		[vcAccountType],
		[vcUserName],
		[vcPassword],
		[dtCreatedDate],
		[dtModifiedDate]
	FROM
		[dbo].[BankDetails]
	WHERE numDomainID = @numDomainID AND numUserContactID = @numUserContactID AND bitIsActive= 1
		

	END
	
	IF @tintMode = 1
	BEGIN
		SELECT
		BD.[numBankDetailID],
		[numBankMasterID],
		BD.[numDomainID],
		BD.[numUserContactID],
		BD.[numAccountID],
		[vcBankID],
		[vcAccountNumber],
		[vcAccountType],
		[vcUserName],
		[vcPassword],
		[dtCreatedDate],
		[dtModifiedDate],
		COA.IsConnected
	FROM
		[dbo].[BankDetails] BD LEFT JOIN dbo.Chart_Of_Accounts COA ON BD.numDomainID = COA.numDomainId AND BD.numAccountID = COA.numAccountId AND BD.numBankDetailID = COA.numBankDetailID
	WHERE BD.numDomainID = @numDomainID AND 
			BD.numUserContactID = @numUserContactID AND 
			numBankMasterID = @numBankMasterID AND 
			vcUserName= @vcUserName AND 
			vcPassword = @vcPassword AND 
			BD.bitIsActive= 1
					

	END
GO