
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemAPILinking]    Script Date: 05/07/2009 17:52:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageItemAPILinking')
DROP PROCEDURE USP_ManageItemAPILinking
GO
CREATE PROCEDURE [dbo].[USP_ManageItemAPILinking]
                @numDomainID  AS NUMERIC(9),
                @intWebApiId  INT  = NULL,
                @numItemCode  AS NUMERIC(9)  = 0,
                @vcApiItemId  AS VARCHAR(50),
                @numUserCntID AS VARCHAR(50),
                @vcSKU  AS VARCHAR(100)
AS
  BEGIN
    IF NOT EXISTS (SELECT *
                   FROM   [ItemAPI]
                   WHERE  
--                   [vcAPIItemID] = @vcApiItemId
                          [numItemID] = @numItemCode
                          AND [WebApiId] = @intWebApiId
                          AND [numDomainId] = @numDomainID)
      BEGIN
        INSERT INTO [ItemAPI]
                   ([WebApiId],
                    [numDomainId],
                    [numItemID],
                    [vcAPIItemID],
                    [numCreatedby],
                    [dtCreated],
                    [numModifiedby],
                    [dtModified],[vcSKU])
        VALUES     (@intWebApiId,
                    @numDomainID,
                    @numItemCode,
                    @vcApiItemId,
                    @numUserCntID,
                    GETUTCDATE(),
                    @numUserCntID,
                    GETUTCDATE(),@vcSKU)
      END
      ELSE
      BEGIN
	  	 UPDATE [ItemAPI]
         SET    vcApiItemId = @vcApiItemId,
				[dtModified] = GETUTCDATE()
         WHERE  [numItemID] = @numItemCode
                AND [WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
         
	  END
  END
