GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetProjectRevenueUtilization' ) 
    DROP PROCEDURE USP_GetProjectRevenueUtilization
GO
CREATE PROCEDURE USP_GetProjectRevenueUtilization
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @dtStartDate DATETIME,
    @dtEndDate DATETIME
AS 
BEGIN
  
  CREATE TABLE #tempProject(numProId NUMERIC(9),vcProjectID VARCHAR(100),vcProjectName VARCHAR(100),vcCompanyName VARCHAR(100),
  vcRecordOwner VARCHAR(100),vcInternalProjectManager varchar(100),vcCustomerProjectManager VARCHAR(100),vcReviewer VARCHAR(500),monIncome DECIMAL(20,5),monExpense DECIMAL(20,5),
  monBalance DECIMAL(20,5),numSO INT,numPO INT,numBills INT)
   
   INSERT INTO #tempProject
   SELECT numProId,vcProjectID,vcProjectName,dbo.fn_GetComapnyName(Pro.numDivisionId) AS vcCompanyName,dbo.fn_GetContactName(Pro.numRecOwner) AS vcRecordOwner,dbo.fn_GetContactName(Pro.numIntPrjMgr) AS vcInternalProjectManager,
   dbo.fn_GetContactName(Pro.numCustPrjMgr) AS vcCustomerProjectManager,
   (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
				AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS vcReviewer, 
   0 AS monIncome,0 AS monExpense,0 AS monBalance,0 AS numSO,0 AS numPO,0 AS numBills
   FROM ProjectsMaster Pro WHERE Pro.numDomainID=@numDomainId


			--SO
			UPDATE temp SET monIncome=temp.monIncome + OPP.monIncome,numSO=temp.numSO + Opp.numSO
			FROM #tempProject temp JOIN (
			SELECT temp.numProId,SUM(ISNULL(OPP.[monPrice], 0) * CASE numType
                      WHEN 1 THEN ISNULL(OPP.numUnitHour, 0)
                      ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),
                                  0) 
                    END)AS monIncome ,COUNT(DISTINCT OM.numOppId) AS numSO
            FROM    [TimeAndExpense] TE
					INNER JOIN #tempProject temp ON TE.numProId=temp.numProId
                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
            WHERE   TE.[numDomainID] = @numDomainID
                    AND numType =1 --Billable 
					AND OM.[tintOppType] = 1 
					AND (TE.[dtTCreatedOn] >= @dtStartDate AND TE.[dtTCreatedOn] <= @dtEndDate) GROUP BY temp.numProId) AS OPP ON OPP.numProId=temp.numProId

			--SO 
			UPDATE temp SET monIncome=temp.monIncome + OPP.monIncome,numSO=temp.numSO + Opp.numSO
			FROM #tempProject temp JOIN (
			SELECT temp.numProId,SUM(ISNULL(OPP.[monPrice], 0) * ISNULL(OPP.numUnitHour, 0)) AS monIncome,COUNT(DISTINCT OM.numOppId) AS numSO
            FROM    OpportunityMaster OM 
					INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
					INNER JOIN #tempProject temp ON OPP.numProjectID=temp.numProId
            WHERE  OM.[numDomainID] = @numDomainID AND OPP.numProjectStageID=0 AND OM.[tintOppType] = 1
            AND (OM.bintCreatedDate >= @dtStartDate AND OM.bintCreatedDate <= @dtEndDate)
					GROUP BY temp.numProId) AS OPP ON OPP.numProId=temp.numProId

			--PO
             UPDATE temp SET monExpense=temp.monExpense + OPP.monExpense,numPO=temp.numPO + Opp.numPO
             FROM #tempProject temp JOIN (
             SELECT temp.numProId,SUM((OPP.[monPrice] * OPP.[numUnitHour])) AS monExpense,COUNT(DISTINCT OM.numOppId) AS numPO
			 from  [OpportunityMaster] OM 
						JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
						INNER JOIN #tempProject temp ON OPP.numProjectID=temp.numProId
			 where  OM.numDomainId=@numDomainID  
					AND OM.[tintOppType] = 2 
					AND (OM.bintCreatedDate >= @dtStartDate AND OM.bintCreatedDate <= @dtEndDate) GROUP BY temp.numProId) AS OPP ON OPP.numProId=temp.numProId	

			--Bill
			UPDATE temp SET monExpense=temp.monExpense + Opp.monExpense,numBills=temp.numBills + Opp.numBills
			FROM #tempProject temp JOIN (
			SELECT temp.numProId,SUM(OBD.monamount) AS monExpense,COUNT(DISTINCT PO.numBillId) AS numBills
			 from  ProjectsOpportunities PO 
				   INNER JOIN #tempProject temp ON PO.numProID=temp.numProId
				   LEFT OUTER JOIN [OpportunityBizDocsDetails] OBD ON OBD.[numBizDocsPaymentDetId] = PO.numBillID
                   LEFT OUTER JOIN [Chart_Of_Accounts] COA ON OBD.[numExpenseAccount] = COA.[numAccountId]
			 where  PO.numDomainId=@numDomainID  
					and isnull(PO.numBillId,0)>0 
					AND (OBD.dtCreationDate >= @dtStartDate AND OBD.dtCreationDate <= @dtEndDate) 
					GROUP BY temp.numProId) AS OPP ON OPP.numProId=temp.numProId	
			
			--Employee Expense
			UPDATE temp SET monExpense=temp.monExpense + Opp.monExpense	
			FROM #tempProject temp JOIN (
			SELECT temp.numProId,SUM(isnull(Cast(datediff(minute,TE.dtfromdate,dttodate) as float)/Cast(60 as float),0) * isnull(UM.monHourlyRate,0)) AS monExpense
			FROM    [TimeAndExpense] TE 
					INNER JOIN #tempProject temp ON TE.numProID=temp.numProId
					join UserMaster UM on UM.numUserDetailId=TE.numUserCntID
            WHERE   TE.[numDomainID] = @numDomainID
                    AND numType in (1,2) --Non Billable & Billable
					AND TE.numUserCntID>0 
					AND (TE.[dtTCreatedOn] >= @dtStartDate AND TE.[dtTCreatedOn] <= @dtEndDate)
					GROUP BY temp.numProId) AS OPP ON OPP.numProId=temp.numProId	
					
		
		UPDATE #tempProject SET monBalance=monIncome - monExpense 
		SELECT * FROM #tempProject WHERE numSO>0 OR numPO>0 OR numBills>0 OR monExpense!=0 OR monIncome!=0
		DROP TABLE #tempProject
END    

