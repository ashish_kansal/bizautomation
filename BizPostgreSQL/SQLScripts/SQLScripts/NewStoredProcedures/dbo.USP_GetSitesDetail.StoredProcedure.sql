GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitesDetail')
DROP PROCEDURE USP_GetSitesDetail
GO
CREATE PROCEDURE [dbo].[USP_GetSitesDetail]
(
	@numSiteID NUMERIC(9)
)
AS 
BEGIN
    SELECT  
		S.[numDomainID],
        D.[numDivisionID],
        ISNULL(S.[bitIsActive], 1),
        ISNULL(E.[numDefaultWareHouseID],0) numDefaultWareHouseID,
        ISNULL(E.[numRelationshipId],0) numRelationshipId,
        ISNULL(E.[numProfileId],0) numProfileId,
        ISNULL(E.[bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
		dbo.fn_GetContactEmail(1,0,numAdminID) AS DomainAdminEmail,
		S.vcLiveURL,
		ISNULL(E.bitAuthOnlyCreditCard,0) bitAuthOnlyCreditCard,
		ISNULL(D.numDefCountry,0) numDefCountry,
		ISNULL(E.bitSendEMail,0) bitSendMail,
		ISNULL((SELECT numAuthoritativeSales FROM dbo.AuthoritativeBizDocs WHERE numDomainId=S.numDomainID),0) AuthSalesBizDoc,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 27261 AND bitEnable =1 ),0) numCreditTermBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId =1 AND bitEnable =1 ),0) numCreditCardBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 31488 AND bitEnable =1 ),0) numGoogleCheckoutBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 35141 AND bitEnable =1 ),0) numPaypalCheckoutBizDocID,
		ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 84 AND bitEnable =1 ),0) numSalesInquiryBizDocID,
		ISNULL(D.bitSaveCreditCardInfo,0) bitSaveCreditCardInfo,
		ISNULL(bitOnePageCheckout,0) bitOnePageCheckout,
		ISNULL(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.numShippingServiceItemID,0) numShippingServiceItemID,
		ISNULL(D.bitAutolinkUnappliedPayment,0) bitAutolinkUnappliedPayment,
		ISNULL(D.numDiscountServiceItemID,0) numDiscountServiceItemID,
		ISNULL(D.tintBaseTax,0) AS [tintBaseTax],
		ISNULL(E.bitSkipStep2,0) AS [bitSkipStep2],
		ISNULL(E.bitDisplayCategory,0) AS [bitDisplayCategory],
		ISNULL(E.bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
		ISNULL(E.[bitPreSellUp],0) [bitPreSellUp],
		ISNULL(E.[bitPostSellUp],0) [bitPostSellUp],
		ISNULL(E.[dcPostSellDiscount],0) [dcPostSellDiscount],
		ISNULL(E.numDefaultClass,0) numDefaultClass,
		E.vcPreSellUp,E.vcPostSellUp,ISNULL(E.vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl,
		ISNULL(D.vcSalesOrderTabs,'') AS vcSalesOrderTabs,
		ISNULL(D.vcSalesQuotesTabs,'') AS vcSalesQuotesTabs,
		ISNULL(D.vcItemPurchaseHistoryTabs,'') AS vcItemPurchaseHistoryTabs,
		ISNULL(D.vcItemsFrequentlyPurchasedTabs,'') AS vcItemsFrequentlyPurchasedTabs,
		ISNULL(D.vcOpenCasesTabs,'') AS vcOpenCasesTabs,
		ISNULL(D.vcOpenRMATabs,'') AS vcOpenRMATabs,
		ISNULL(D.bitSalesOrderTabs,0) AS bitSalesOrderTabs,
		ISNULL(D.bitSalesQuotesTabs,0) AS bitSalesQuotesTabs,
		ISNULL(D.bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs,
		ISNULL(D.bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs,
		ISNULL(D.bitOpenCasesTabs,0) AS bitOpenCasesTabs,
		ISNULL(D.bitOpenRMATabs,0) AS bitOpenRMATabs,
		ISNULL(D.bitSupportTabs,0) AS bitSupportTabs,
		ISNULL(D.vcSupportTabs,'') AS vcSupportTabs,
		ISNULL(bitHideAddtoCart,0) AS bitHideAddtoCart,
		ISNULL(bitShowPromoDetailsLink,0) bitShowPromoDetailsLink,
		ISNULL(D.bitDefaultProfileURL,0) AS bitDefaultProfileURL
    FROM
		Sites S
    INNER JOIN 
		[Domain] D 
	ON 
		D.[numDomainId] = S.[numDomainID]
    LEFT OUTER JOIN 
		[eCommerceDTL] E 
	ON 
		D.[numDomainId] = E.[numDomainId] 
		AND S.numSiteID = E.numSiteId
    WHERE
		S.[numSiteID] = @numSiteID
END
GO