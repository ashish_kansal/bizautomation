GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ApprovePORequistion' ) 
    DROP PROCEDURE USP_ApprovePORequistion
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 14 July 2014
-- Description:	sets unit price approval required flag to false for all items added in order
-- =============================================
CREATE PROCEDURE USP_ApprovePORequistion
	@numOppId numeric(18,0),
	@numDomainID numeric(18,0),
	@ApprovalType AS INT = 0
AS
BEGIN
	UPDATE
		OpportunityMaster
	SET
		intReqPOApproved = @ApprovalType
	WHERE
		numOppId = @numOppId
	
END
GO