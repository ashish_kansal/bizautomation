GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShippingGlobalValues' ) 
    DROP PROCEDURE USP_ManageShippingGlobalValues
GO
CREATE PROCEDURE USP_ManageShippingGlobalValues   
     @numDomainID NUMERIC(9)
	,@minShippingCost FLOAT
	,@bitEnableStaticShipping BIT
	,@bitEnableShippingExceptions BIT
AS 
BEGIN
      
	IF EXISTS(
				SELECT * FROM dbo.Domain 
					WHERE numDomainID = @numDomainID
			    )
        BEGIN
	           UPDATE Domain
				   SET	 minShippingCost = @minShippingCost
						,bitEnableStaticShippingRule = @bitEnableStaticShipping 
						,bitEnableShippingExceptions = @bitEnableShippingExceptions
				WHERE numDomainID = @numDomainID  
        END	
		
	ELSE
		BEGIN
			INSERT INTO Domain
			(
				 numDomainID
				,minShippingCost
				,bitEnableStaticShippingRule
				,bitEnableShippingExceptions
			)
			VALUES
			(
				 @numDomainID
				,@minShippingCost
				,@bitEnableStaticShipping
				,@bitEnableShippingExceptions
			)
		END
			
END