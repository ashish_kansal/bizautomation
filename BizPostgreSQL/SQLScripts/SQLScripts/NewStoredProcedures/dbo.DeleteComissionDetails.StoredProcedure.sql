GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='DeleteComissionDetails')
DROP PROCEDURE DeleteComissionDetails
GO
CREATE PROCEDURE DeleteComissionDetails
    (     
      @numDomainId NUMERIC,
      @numOppBizDocID numeric(9)      
    )

AS BEGIN
BEGIN TRY
BEGIN TRANSACTION
	create table #TempComJournal
	(numJournalId numeric(9));

	insert into #TempComJournal

	select GJD.numJournalId from General_Journal_Details GJD where GJD.numCommissionID in 
	(select BC.numComissionID from BizDocComission BC where BC.numOppBizDocId=@numOppBizDocId)

	delete from General_Journal_Details  where numJournalId in (select numJournalId from #TempComJournal);
	delete from General_Journal_Header  where numJOurnal_Id in (select numJournalId from #TempComJournal);

	DELETE FROM BizDocComission WHERE numOppBizDocId=@numOppBizDocId;
COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH  
end