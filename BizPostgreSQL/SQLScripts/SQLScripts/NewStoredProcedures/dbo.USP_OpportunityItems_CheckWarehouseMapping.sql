GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItems_CheckWarehouseMapping')
DROP PROCEDURE USP_OpportunityItems_CheckWarehouseMapping
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItems_CheckWarehouseMapping]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT * FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND ISNULL(bitAutoWarehouseSelection,0)=1)
	BEGIN
		DECLARE @numOrderSource NUMERIC(18,0)
		DECLARE @tintSourceType TINYINT
		DECLARE @numShipToState NUMERIC(18,0)
		DECLARE @numShipToCountry NUMERIC(18,0)

		SELECT
			@numOrderSource = ISNULL(OpportunityMaster.tintSource,0)
			,@tintSourceType = ISNULL(OpportunityMaster.tintSourceType,0)
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND numOppId=@numOppID

		SELECT
			@numShipToState=ISNULL(numState,0)
			,@numShipToCountry=ISNULL(numCountry,0)
		FROM
			fn_getOPPAddressDetails(@numOppID,@numDomainID,2)

		DECLARE @bitBOOrderStatus BIT
		SELECT @bitBOOrderStatus=ISNULL(bitBOOrderStatus,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID

		DECLARE @TEMP TABLE
		(
			numWarehouseID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numWarehouseID
		)
		SELECT  
			MSFWP.numWarehouseID
		FROM 
			MassSalesFulfillmentWM MSFWM
		INNER JOIN
			MassSalesFulfillmentWMState MSFWMS
		ON
			MSFWM.ID = MSFWMS.numMSFWMID
		INNER JOIN
			MassSalesFulfillmentWP MSFWP
		ON
			MSFWM.ID = MSFWP.numMSFWMID
		WHERE 
			numDomainID=@numDomainID 
			AND numOrderSource=@numOrderSource 
			AND tintSourceType=@tintSourceType 
			AND numCountryID=@numShipToCountry
			AND numStateID=@numShipToState
		ORDER BY
			intOrder

		IF (SELECT COUNT(*) FROM @TEMP) > 0
		BEGIN
			DECLARE @tintCommitAllocation AS TINYINT	
			SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId

			UPDATE
				OI
			SET
				bitRequiredWarehouseCorrection = 1
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			INNER JOIN 
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID = WI.numWareHouseItemID
			WHERE
				numOppId=@numOppID 
				AND ISNULL(bitDropShip,0)=0
				AND (ISNULL(bitWorkOrder,0)=0 OR (ISNULL(bitWorkOrder,0)=1 AND ISNULL(numUnitHour,0) - ISNULL(numWOQty,0) > 0))
				AND bitRequiredWarehouseCorrection IS NULL
				AND 1 = (CASE 
							WHEN @bitBOOrderStatus = 0 
							THEN (CASE WHEN WI.numWareHouseID NOT IN (SELECT T.numWarehouseID FROM @TEMP T) THEN 1 ELSE 0 END)
							ELSE (CASE 
									WHEN WI.numWareHouseID NOT IN (SELECT T.numWarehouseID FROM @TEMP T)
											OR 1 = (CASE 
														WHEN @tintCommitAllocation=1 
														THEN (CASE 
																WHEN ISNULL(I.bitKitParent,0) = 1 
																THEN (CASE 
																		WHEN EXISTS(SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI INNER JOIN WareHouseItems WIInner ON OKI.numWareHouseItemId=WIInner.numWareHouseItemID WHERE OKI.numOppId=@numOppID AND OKI.numOppItemID=OI.numoppitemtCode AND ISNULL(WIInner.numBackOrder,0) > 0)
																				OR EXISTS(SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI INNER JOIN WareHouseItems WIInner ON OKCI.numWareHouseItemId=WIInner.numWareHouseItemID WHERE OKCI.numOppId=@numOppID AND OKCI.numOppItemID=OI.numoppitemtCode AND ISNULL(WIInner.numBackOrder,0) > 0)
																		THEN 1
																		ELSE 0
																	END)
																ELSE (CASE WHEN ISNULL(WI.numBackOrder,0) > 0 THEN 1 ELSE 0 END)
															END) 
														ELSE (CASE 
																WHEN ISNULL(I.bitKitParent,0) = 1 
																THEN (CASE 
																		WHEN EXISTS(SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI INNER JOIN WareHouseItems WIInner ON OKI.numWareHouseItemId=WIInner.numWareHouseItemID WHERE OKI.numOppId=@numOppID AND OKI.numOppItemID=OI.numoppitemtCode AND ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WIInner.numItemID AND WIInner.numWareHouseID=WIInner.numWareHouseID),0) < ISNULL(OKI.numQtyItemsReq,0))
																				OR EXISTS(SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI INNER JOIN WareHouseItems WIInner ON OKCI.numWareHouseItemId=WIInner.numWareHouseItemID WHERE OKCI.numOppId=@numOppID AND OKCI.numOppItemID=OI.numoppitemtCode AND ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WIInner.numItemID AND WIInner.numWareHouseID=WIInner.numWareHouseID),0) < ISNULL(OKCI.numQtyItemsReq,0))
																		THEN 1
																		ELSE 0
																	END)
																ELSE (CASE WHEN ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID=WI.numWareHouseID),0) < (ISNULL(numUnitHour,0) - ISNULL(numWOQty,0)) THEN 1 ELSE 0 END)
															END)
													END) 
									THEN 1 
									ELSE 0 
								END)
						END)
		END
		ELSE
		BEGIN
			UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = 1 WHERE numOppId=@numOppID AND numWarehouseItmsID > 0 AND bitRequiredWarehouseCorrection IS NULL
		END

		UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = 0 WHERE numOppId=@numOppID AND bitRequiredWarehouseCorrection IS NULL

		IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND bitRequiredWarehouseCorrection = 1)
		BEGIN
			UPDATE OpportunityMaster SET numStatus=15448 WHERE numDomainId=@numDomainID AND numOppId=@numOppID
		END
	END
	ELSE
	BEGIN
		UPDATE OpportunityItems SET bitRequiredWarehouseCorrection = 0 WHERE numOppId=@numOppID AND bitRequiredWarehouseCorrection IS NULL
	END
END
GO

