GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportListMasterDetail')
DROP PROCEDURE USP_GetReportListMasterDetail
GO
CREATE PROCEDURE [dbo].[USP_GetReportListMasterDetail]     
    @numDomainID numeric(18, 0),
	@numReportID numeric(18, 0)
as                 

SELECT RLM.*,RMM.vcModuleName,RMGM.vcGroupName FROM ReportListMaster RLM 
JOIN ReportModuleMaster RMM ON RLM.numReportModuleID=RMM.numReportModuleID
JOIN ReportModuleGroupMaster RMGM ON RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID    
 WHERE RLM.numDomainID=@numDomainID AND RLM.numReportID=@numReportID

SELECT * FROM ReportFieldsList WHERE numReportID=@numReportID ORDER BY numReportFieldID

SELECT * FROM ReportFilterList WHERE numReportID=@numReportID ORDER BY numReportFilterID

SELECT * FROM ReportSummaryGroupList WHERE numReportID=@numReportID ORDER BY numReportGroupID

--SELECT * FROM ReportMatrixBreakList WHERE numReportID=@numReportID ORDER BY numReportMatrixID

SELECT * FROM ReportMatrixAggregateList WHERE numReportID=@numReportID ORDER BY numReportAggregateID

SELECT * FROM ReportKPIThresold WHERE numReportID=@numReportID ORDER BY numReportThresoldID