GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetWorkFlowQueue' ) 
    DROP PROCEDURE USP_GetWorkFlowQueue
GO
CREATE PROCEDURE USP_GetWorkFlowQueue
AS 
BEGIN
	--IF Workflow is not active or deleted
	UPDATE 
		 WFQ
	SET 
		WFQ.tintProcessStatus=5
	FROM
		WorkFlowQueue WFQ
	LEFT JOIN	
		WorkFlowMaster WF 
	ON
		WFQ.numDomainID=WF.numDomainID
		AND WFQ.numFormID=WF.numFormID
		AND WF.tintWFTriggerOn=WFQ.tintWFTriggerOn
		AND WF.bitActive=1  
	WHERE 
		WF.numWFID IS NULL

	DELETE
		WFQ
	FROM
		WorkFlowQueue WFQ
	INNER JOIN
		WorkFlowMaster WF
	ON
		WFQ.numWFID = WF.numWFID
	WHERE
		ISNULL(WF.vcDateField,'') <> '' AND intDays > 0
			   
	--Select top 25 WorkFlow which are Pending Execution			  	
	SELECT TOP 25 
		WorkFlowQueue.[numWFQueueID]
		,WorkFlowQueue.[numDomainID]
		,WorkFlowQueue.[numRecordID]
		,WorkFlowQueue.[numFormID]
		,WorkFlowQueue.[tintProcessStatus]
		,WorkFlowQueue.[tintWFTriggerOn]
		,WorkFlowQueue.[numWFID]
	FROM 
		WorkFlowQueue 
	INNER JOIN 
		WorkFlowMaster 
	ON 
		WorkFlowQueue.numWFID=WorkFlowMaster.numWFID and WorkFlowQueue.numFormID=WorkFlowMaster.numFormID
		AND WorkFlowQueue.numDomainID=WorkFlowMaster.numDomainID AND WorkFlowMaster.tintWFTriggerOn=WorkFlowQueue.tintWFTriggerOn
	WHERE 
		WorkFlowQueue.tintProcessStatus IN (1) 
		AND WorkFlowMaster.bitActive=1 
	ORDER BY 
		numWFQueueID
END



