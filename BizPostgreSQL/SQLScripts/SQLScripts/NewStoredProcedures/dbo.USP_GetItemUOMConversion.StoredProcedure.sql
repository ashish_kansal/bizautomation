GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemUOMConversion')
DROP PROCEDURE USP_GetItemUOMConversion
GO
Create PROCEDURE [dbo].[USP_GetItemUOMConversion]                                                                          
 @numDomainID as numeric(9),    
 @numItemCode as numeric(9),
 @numUOMId as numeric(9)
AS       
   
   
   SELECT dbo.fn_UOMConversion(@numUOMId,@numItemCode,@numDomainId,numBaseUnit) AS BaseUOMConversion,
   dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) AS PurchaseUOMConversion,
   dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numSaleUnit,0)>0 THEN numSaleUnit ELSE numBaseUnit END) AS SaleUOMConversion,
   dbo.fn_GetUOMName(numBaseUnit) AS vcBaseUOMName 
   FROM item WHERE numDomainID=@numDomainID and numItemCode=@numItemCode
   
