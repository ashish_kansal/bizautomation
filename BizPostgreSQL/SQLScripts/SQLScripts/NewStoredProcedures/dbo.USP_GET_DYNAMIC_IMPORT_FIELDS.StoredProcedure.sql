GO
/****** Object:  StoredProcedure [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_DYNAMIC_IMPORT_FIELDS' ) 
    DROP PROCEDURE USP_GET_DYNAMIC_IMPORT_FIELDS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- USP_GET_DYNAMIC_IMPORT_FIELDS 48,2,40,1
CREATE PROCEDURE [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]
    (
      @numFormID BIGINT,
      @intMode INT,
      @numImportFileID NUMERIC = 0,
      @numDomainID NUMERIC = 0,
	  @tintInsertUpdateType TINYINT = 0,
	  @tintItemLinkingID NUMERIC(18,0) = 0
    )
AS 
BEGIN
		
    IF @intMode = 1 -- TO BIND CATEGORY DROP DOWN
    BEGIN
        SELECT 20 AS [intImportMasterID],'Item' AS [vcImportName]
        UNION ALL
        SELECT  48 AS [intImportMasterID],'Item WareHouse' AS [vcImportName]
        UNION ALL
        SELECT  31 AS [intImportMasterID],'Kit sub-items' AS [vcImportName]
        UNION ALL
        SELECT  54 AS [intImportMasterID],'Item Images' AS [vcImportName]
        UNION ALL
        SELECT  55 AS [intImportMasterID],'Item Vendor' AS [vcImportName]
        UNION ALL
        SELECT  133 AS [intImportMasterID],'Organization' AS [vcImportName]
        UNION ALL
		SELECT  134 AS [intImportMasterID],'Contact' AS [vcImportName]
        UNION ALL
        SELECT  43 AS [intImportMasterID],'Organization Correspondence' AS [vcImportName]                        
        UNION ALL
        SELECT  98 AS [intImportMasterID],'Address Details' AS [vcImportName]
		UNION ALL
        SELECT  136 AS [intImportMasterID],'Import Sales Order(s)' AS [vcImportName]
		UNION ALL
        SELECT  137 AS [intImportMasterID],'Import Purchase Order(s)' AS [vcImportName]
		UNION ALL
        SELECT  140 AS [intImportMasterID],'Import Accounting Entries' AS [vcImportName]           		
    END
	
    IF @intMode = 2 -- TO GET DETAILS OF DYNAMIC FORM FIELDS
    BEGIN
        SELECT DISTINCT
            intSectionID AS [Id],
            vcSectionName AS [Data]
        FROM 
			dbo.DycFormSectionDetail
        WHERE 
			numFormID = @numFormID			
		
        SELECT 
			ROW_NUMBER() OVER (ORDER BY X.intSectionID, X.numFormFieldID) AS SrNo
			,X.*
        FROM  
			(SELECT    
				ISNULL((SELECT 
							intImportTransactionID
						FROM 
							dbo.Import_File_Field_Mapping FFM
						INNER JOIN 
							Import_File_Master IFM 
						ON 
							IFM.intImportFileID = FFM.intImportFileID
							AND IFM.intImportFileID = @numImportFileID
						WHERE 
							FFM.numFormFieldID = DCOL.numFieldID),0) AS [intImportFieldID]
				,DCOL.numFieldID AS [numFormFieldID]
				,'' AS [vcSectionNames]
				,DCOL.vcFieldName AS [vcFormFieldName]
				,DCOL.vcDbColumnName
				,DCOL.intSectionID AS [intSectionID]
				,0 [IsCustomField]
				,ISNULL((SELECT 
							intMapColumnNo
						FROM 
							dbo.Import_File_Field_Mapping FFM
						WHERE  
							1 = 1
							AND FFM.numFormFieldID = DCOL.numFieldID
							AND FFM.intImportFileID = @numImportFileID),0) intMapColumnNo
				,DCOL.vcPropertyName
				,CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END AS [vcReqString]
				,DCOL.bitRequired AS [bitRequired]
				,CAST(DCOL.numFieldID AS VARCHAR) + '~' + CAST(0 AS VARCHAR(10)) AS [keyFieldID]
				,DCOL.tintOrder
			FROM 
				dbo.View_DynamicDefaultColumns DCOL
            WHERE 
				DCOL.numDomainID = @numDomainID
                AND DCOL.numFormID = @numFormID
                AND ISNULL(DCOL.bitImport, 0) = 1
                AND ISNULL(DCOL.bitDeleted, 0) = 0
				AND 1 = (CASE 
							WHEN @numFormID=20 THEN (CASE WHEN DCOL.numFieldID IN (189,294,271,272,270) THEN 0 ELSE 1 END)
							ELSE 1
						END)
            UNION ALL
            SELECT 
				-1
				,CAST(CFm.Fld_id AS NUMERIC(18,0))
				,'CustomField'
				,cfm.Fld_label
				,cfm.Fld_label
				,DFSD.intSectionId
				,1
				,ISNULL((SELECT 
							intMapColumnNo
                        FROM 
							dbo.Import_File_Field_Mapping IIFM
                        LEFT JOIN 
							dbo.DycFormField_Mapping DMAP 
						ON 
							IIFM.numFormFieldID = DMAP.numFieldID
                        WHERE 
							IIFM.intImportFileID = @numImportFileID
                            AND IIFM.numFormFieldID = CFM.Fld_id
                            AND bitCustomField = 1
                            AND ISNULL(DMAP.bitImport, 0) = 1),0) intMapColumnNo
				,'' AS [vcPropertyName]
				,'' AS [vcReqString]
				,0 AS [bitRequired]
				,CAST(CFm.Fld_id AS VARCHAR) + '~' + CAST(1 AS VARCHAR(10)) AS [keyFieldID]
				,100 AS [tintOrder]
            FROM 
				dbo.CFW_Fld_Master CFM
                LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
                LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
            WHERE 
				Grp_id = DFSD.Loc_Id
                AND numDomainID = @numDomainID
                AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
        ) X
		ORDER BY 
			X.tintOrder,
			X.intSectionID        		
        
        
		SELECT 
			ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.numFormFieldID ) AS SrNo
			,X.*
		FROM 
			(SELECT 
				ISNULL((SELECT 
							intImportTransactionID
						FROM 
							dbo.Import_File_Field_Mapping FFM
						INNER JOIN 
							Import_File_Master IFM 
						ON 
							IFM.intImportFileID = FFM.intImportFileID
							AND IFM.intImportFileID = @numImportFileID
						WHERE 
							FFM.numFormFieldID = DCOL.numFieldID),0) AS [intImportFieldID]
				,DCOL.numFieldID AS [numFormFieldID]
				,'' AS [vcSectionNames]
				,DCOL.vcFieldName AS [vcFormFieldName]
				,DCOL.vcDbColumnName
				,DCOL.intSectionID AS [intSectionID]
				,0 [IsCustomField]
				,ISNULL((SELECT 
							intMapColumnNo
						FROM 
							dbo.Import_File_Field_Mapping FFM
						WHERE 
							1 = 1
							AND FFM.numFormFieldID = DCOL.numFieldID
							AND FFM.intImportFileID = @numImportFileID), 0) intMapColumnNo
				,DCOL.vcPropertyName
				,CASE 
					WHEN @numFormID = 20 
					THEN CASE 
							WHEN DCOl.numFieldID IN (189,294,271,272,270,(CASE @tintItemLinkingID
																						WHEN 1 THEN 211
																						WHEN 2 THEN 281
																						WHEN 3 THEN 203
																						WHEN 4 THEN 193
																						ELSE 0 
																					END)) THEN '*' ELSE (CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END) END
					WHEN @numFormID = 133
					THEN CASE 
							WHEN DCOl.numFieldID IN (3,6,380,451,(CASE @tintItemLinkingID
																						WHEN 1 THEN 380
																						WHEN 2 THEN 539
																						ELSE 0 
																					END)) THEN '*' ELSE (CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END) END
					WHEN @numFormID = 134
					THEN CASE 
							WHEN DCOl.numFieldID IN (51,52,386,(CASE @tintItemLinkingID
																	WHEN 3 THEN 380
																	WHEN 4 THEN 539
																	ELSE 0 
																END)) THEN '*' ELSE (CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END) END
					WHEN @numFormID IN (136,137)
					THEN CASE WHEN DCOL.numFieldID IN (258,259,293,ISNULL(@tintItemLinkingID,0)) THEN '*' ELSE (CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END) END
					ELSE (CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0 THEN '' ELSE '*' END)
				END AS [vcReqString]
				,CASE 
					WHEN @numFormID = 20 
					THEN CASE 
							WHEN DCOl.numFieldID IN (189,294,271,272,270,(CASE @tintItemLinkingID
																						WHEN 1 THEN 211
																						WHEN 2 THEN 281
																						WHEN 3 THEN 203
																						WHEN 4 THEN 193
																						ELSE 0 
																					END)) THEN 1 ELSE ISNULL(DCOL.bitRequired,0) END
					WHEN @numFormID = 133
					THEN CASE 
							WHEN DCOl.numFieldID IN (3,6,380,451,(CASE @tintItemLinkingID
																						WHEN 1 THEN 380
																						WHEN 2 THEN 539
																						ELSE 0 
																					END)) THEN 1 ELSE ISNULL(DCOL.bitRequired,0) END
					WHEN @numFormID = 134
					THEN CASE 
							WHEN DCOl.numFieldID IN (51,52,386,(CASE @tintItemLinkingID
																	WHEN 3 THEN 380
																	WHEN 4 THEN 539
																	ELSE 0 
																END)) THEN 1 ELSE ISNULL(DCOL.bitRequired,0) END
					WHEN @numFormID IN (136,137)
					THEN CASE WHEN DCOL.numFieldID IN (258,259,293,ISNULL(@tintItemLinkingID,0)) THEN 1 ELSE ISNULL(DCOL.bitRequired,0) END
					ELSE DCOL.bitRequired
				END AS [bitRequired]
				,CAST(DCOL.numFieldID AS VARCHAR) + '~' + CAST(0 AS VARCHAR(10)) AS [keyFieldID]
				,ISNULL(DCOL.tintColumn, 0) AS SortOrder
				,ISNULL(DCOL.tintRow,0) AS tintRow
			FROM 
				dbo.View_DynamicColumns DCOL
			WHERE 
				DCOL.numDomainID = @numDomainID
				AND DCOL.numFormID = @numFormID
				AND ISNULL(DCOL.bitImport, 0) = 1
				AND ISNULL(tintPageType, 0) = 4
			UNION ALL
			SELECT    
				ISNULL((SELECT 
							intImportTransactionID
						FROM 
							dbo.Import_File_Field_Mapping FFM
						INNER JOIN 
							Import_File_Master IFM 
						ON 
							IFM.intImportFileID = FFM.intImportFileID
							AND IFM.intImportFileID = @numImportFileID
						WHERE 
							FFM.numFormFieldID = DCOL.numFieldID),0) AS [intImportFieldID]
				,DCOL.numFieldID AS [numFormFieldID]
				,'' AS [vcSectionNames]
				,DCOL.vcFieldName AS [vcFormFieldName]
				,DCOL.vcDbColumnName
				,DCOL.intSectionID AS [intSectionID]
				,0 [IsCustomField]
				,ISNULL((SELECT 
							intMapColumnNo
						FROM 
							dbo.Import_File_Field_Mapping FFM
						WHERE  
							1 = 1
							AND FFM.numFormFieldID = DCOL.numFieldID
							AND FFM.intImportFileID = @numImportFileID),0) intMapColumnNo
				,DCOL.vcPropertyName
				,'*' AS [vcReqString]
				,1 AS [bitRequired]
				,CAST(DCOL.numFieldID AS VARCHAR) + '~' + CAST(0 AS VARCHAR(10)) AS [keyFieldID]
				,0 AS SortOrder
				,DCOL.tintOrder
			FROM 
				dbo.View_DynamicDefaultColumns DCOL
            WHERE 
				DCOL.numDomainID = @numDomainID
                AND DCOL.numFormID = @numFormID
                AND ISNULL(DCOL.bitImport, 0) = 1
                AND ISNULL(DCOL.bitDeleted, 0) = 0
				AND DCOL.numFieldID NOT IN (SELECT 
												VDC.numFieldID 
											FROM 
												dbo.View_DynamicColumns VDC
											WHERE 
												VDC.numDomainID = @numDomainID
												AND VDC.numFormID = @numFormID
												AND ISNULL(VDC.bitImport, 0) = 1
												AND ISNULL(VDC.tintPageType, 0) = 4)
				AND @tintInsertUpdateType IN (2,3)
				AND 1 = (CASE 
							WHEN @numFormID=20
							THEN (CASE 
									WHEN DCOL.numFieldID IN (189,294,271,272,270,(CASE @tintItemLinkingID
																						WHEN 1 THEN 211
																						WHEN 2 THEN 281
																						WHEN 3 THEN 203
																						WHEN 4 THEN 193
																						ELSE 0 
																					END)) 
									THEN 1 
									ELSE 0 
								END)
							WHEN @numFormID=133
							THEN (CASE 
									WHEN DCOL.numFieldID IN (3,6,380,451,(CASE @tintItemLinkingID
																				WHEN 1 THEN 380
																				WHEN 2 THEN 539
																				ELSE 0 
																			END)) 
									THEN 1 
									ELSE 0 
								END)
							WHEN @numFormID = 134
							THEN (CASE 
									WHEN DCOl.numFieldID IN (51,52,386,(CASE @tintItemLinkingID
																			WHEN 3 THEN 380
																			WHEN 4 THEN 539
																			ELSE 0 
																		END)) 
									THEN 1 
									ELSE 0
								END)
							WHEN @numFormID IN (136,137)
							THEN (CASE WHEN DCOL.numFieldID IN (258,259,293,ISNULL(@tintItemLinkingID,0)) THEN 1 ELSE 0 END)
							WHEN @numFormID IN (140)
							THEN (CASE WHEN DCOL.numFieldID IN (931,932,933,934) THEN 1 ELSE 0 END)
							ELSE 0
						END)
			UNION ALL
			SELECT 
				-1
				,CAST(CFm.Fld_id AS NUMERIC(18,0))
				,'CustomField'
				,cfm.Fld_label
				,cfm.Fld_label
				,DFSD.intSectionId
				,1
				,ISNULL((SELECT 
							intMapColumnNo
						FROM 
							dbo.Import_File_Field_Mapping IIFM
						LEFT JOIN 
							dbo.DycFormField_Mapping DMAP 
						ON 
							IIFM.numFormFieldID = DMAP.numFieldID
						WHERE 
							IIFM.intImportFileID = @numImportFileID
							AND IIFM.numFormFieldID = CFM.Fld_id
							AND bitCustomField = 1
							AND ISNULL(DMAP.bitImport, 0) = 1),0) intMapColumnNo
				,'' AS [vcPropertyName]
				,'' AS [vcReqString]
				,0 AS [bitRequired]
				,CAST(CFm.Fld_id AS VARCHAR) + '~' + CAST(1 AS VARCHAR(10)) AS [keyFieldID]
				,DFCD.intColumnNum AS [SortOrder]
				,ISNULL(DFCD.intRowNum,0) AS tintRow
			FROM      
				dbo.CFW_Fld_Master CFM
			LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
			LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
			JOIN DycFormConfigurationDetails DFCD ON DFCD.numFieldID = CFM.Fld_Id AND DFCD.numFormID = @numFormID AND DFCD.bitCustom = 1
			WHERE 
				Grp_id = DFSD.Loc_Id
				AND DFCD.numDomainID = @numDomainID
				AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
				AND (DFCD.tintPageType = 4 OR DFCD.tintPageType IS NULL)                          
		) X
		ORDER BY 
			X.SortOrder
			,X.intSectionID 
	END	
END
GO

