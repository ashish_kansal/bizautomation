SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetSurveyRating')
DROP PROCEDURE usp_GetSurveyRating
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyRating]    
 @numDomainID Numeric,       
 @numSurId NUMERIC,
 @numSurRating NUMERIC   
AS      
BEGIN      
  SELECT TOP 1 * FROM SurveyCreateRecord WHERE     
   numSurId = @numSurId AND (numRatingMin<=@numSurRating AND numRatingMax>=@numSurRating)
END
GO
