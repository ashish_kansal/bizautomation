/****** Object:  StoredProcedure [dbo].[Usp_GetAddressDetailsForImport]    Script Date: 06/19/2014 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_GetAddressDetailsForImport' )
    DROP PROCEDURE Usp_GetAddressDetailsForImport
GO
CREATE PROCEDURE [dbo].[Usp_GetAddressDetailsForImport]
    (
      @numDomainID AS NUMERIC(9) ,
      @tintUpdateMode AS TINYINT  -- Pass 1 for Adding / updating Contact's address
								  -- Pass 2 for Adding / updating Organization's Address
    )
AS
    BEGIN    
        IF @tintUpdateMode = 2
            BEGIN
                SELECT DISTINCT
                        CI.[vcCompanyName] ,
                        'Organization' [AddressOf] ,
                        CASE AD.[tintAddressType]
                          WHEN 1 THEN 'BILL TO'
                          ELSE 'SHIP TO'
                        END AS [AddressType] ,
                        CASE AD.[bitIsPrimary]
                          WHEN 1 THEN 'YES'
                          ELSE 'NO'
                        END AS [Is Primary] ,
                        AD.[vcAddressName] AS [Address Name] ,
                        AD.[vcStreet] AS [Street] ,
                        AD.[vcCity] AS [City] ,
                        ( SELECT    [S].[vcState]
                          FROM      [dbo].[State] AS S
                          WHERE     S.[numStateID] = AD.[numState]
                        ) AS [State] ,
                        ( SELECT    [LD].[vcData]
                          FROM      [dbo].[ListDetails] AS LD
                          WHERE     [LD].[numListID] = 40
                                    AND [LD].[numListItemID] = AD.[numCountry]
                        ) AS [Country] ,
                        AD.[vcPostalCode] AS [Postal Code] ,
                        AD.[numAddressID] AS [AddressID] ,
                        DM.[numDivisionID] AS [RecordID]
                FROM    [dbo].[AddressDetails] AS AD
                        LEFT JOIN [dbo].[DivisionMaster] AS DM ON AD.numDomainID = DM.numDomainID
                                                              AND AD.[numRecordID] = DM.[numDivisionID]
                                                              AND Ad.[tintAddressOf] = 2
                        JOIN [dbo].[CompanyInfo] AS CI ON CI.[numCompanyId] = DM.[numCompanyID]
                WHERE   [AD].[numDomainID] = @numDomainID
                ORDER BY CI.[vcCompanyName] ,
                        AD.[numAddressID]

            END
        ELSE
            IF @tintUpdateMode = 1
                BEGIN
                    SELECT  ACI.[vcFirstName] AS [First Name],
                            ACI.[vcLastName] AS [Last Name],
                            'Contact' [AddressOf] ,
                            CASE AD.[tintAddressType]
                              WHEN 1 THEN 'BILL TO'
                              ELSE 'SHIP TO'
                            END AS [AddressType] ,
                            CASE AD.[bitIsPrimary]
                              WHEN 1 THEN 'YES'
                              ELSE 'NO'
                            END AS [IsPrimary] ,
                            AD.[vcAddressName] AS [Address Name] ,
                            AD.[vcStreet] AS [Street] ,
                            AD.[vcCity] AS [City] ,
                            ( SELECT    [S].[vcState]
                              FROM      [dbo].[State] AS S
                              WHERE     S.[numStateID] = AD.[numState]
                            ) AS [State] ,
                            ( SELECT    [LD].[vcData]
                              FROM      [dbo].[ListDetails] AS LD
                              WHERE     [LD].[numListID] = 40
                                        AND [LD].[numListItemID] = AD.[numCountry]
                            ) AS [Country] ,
                            AD.[vcPostalCode] AS [Postal Code] ,
                            AD.[numAddressID] AS [AddressID] ,
                            [ACI].[numContactId] AS [RecordID]
                    FROM    [dbo].[AddressDetails] AS AD
                            JOIN [dbo].[AdditionalContactsInformation] AS ACI ON [AD].[numRecordID] = ACI.[numContactId]
                                                              AND ACI.[numDomainID] = [AD].[numDomainID]
                                                              AND [AD].[tintAddressOf] = 1
                    WHERE   [AD].[numDomainID] = @numDomainID
                            AND ( ISNULL(ACI.[vcFirstName], '') <> ''
                                  OR ISNULL(ACI.[vcLastName], '') <> ''
                                )
                    ORDER BY ACI.[vcFirstName] ,
                            ACI.[vcLastName] ,
                            AD.[numAddressID]

                END
    END