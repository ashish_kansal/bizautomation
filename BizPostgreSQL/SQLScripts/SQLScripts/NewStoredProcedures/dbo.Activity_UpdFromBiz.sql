GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Activity_UpdFromBiz')
DROP PROCEDURE Activity_UpdFromBiz
GO
CREATE PROCEDURE [dbo].[Activity_UpdFromBiz]
	@DataKey integer,		
	@Priority numeric(18,0),	
	@Activity numeric(18,0),		
	@FollowUpStatus	numeric(18,0),	
	@Comments	ntext,
	@numContactID numeric(18,0)
AS
BEGIN

     DECLARE @numCompanyId NUMERIC(9)          
        DECLARE @numDivisionId NUMERIC(9)  
        DECLARE @numDomainID NUMERIC(9)        
 
		SELECT  @numDivisionId = [numDivisionId] FROM [AdditionalContactsInformation] WHERE [numContactId] = @numContactId
		SELECT @numCompanyId = [numCompanyID],@numDomainID=[numDomainID] FROM [DivisionMaster] WHERE [numDivisionID] =@numDivisionId

	UPDATE
		[Activity]
	SET 
		[Activity].[Priority] = @Priority, 
		[Activity].[Activity] = @Activity, 
		[Activity].[FollowUpStatus] = @FollowUpStatus, 
		[Activity].[Comments] = @Comments
	WHERE
		( [Activity].[ActivityID] = @DataKey );


		UPDATE  DivisionMaster
        SET     [numFollowUpStatus] = @FollowUpStatus
        WHERE   numDivisionID = @numDivisionId
                AND numCompanyID = @numCompanyId
                
		INSERT INTO [FollowUpHistory] (
			[numFollowUpstatus],
			[numDivisionID],
			[bintAddedDate],
			[numDomainID]
		) VALUES ( 
			/* numFollowUpstatus - numeric(18, 0) */ @FollowUpStatus,
			/* numDivisionID - numeric(18, 0) */ @numDivisionId,
			/* bintAddedDate - datetime */ GETUTCDATE(),
			/* numDomainID - numeric(18, 0) */@numDomainID  )



END
GO



