GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageCheckHeader' )
    DROP PROCEDURE USP_ManageCheckHeader
GO
CREATE PROCEDURE USP_ManageCheckHeader
    @tintMode AS TINYINT ,
    @numDomainID NUMERIC(18, 0) ,
    @numCheckHeaderID NUMERIC(18, 0) ,
    @numChartAcntId NUMERIC(18, 0) ,
    @numDivisionID NUMERIC(18, 0) ,
    @monAmount DECIMAL(20,5) ,
    @numCheckNo NUMERIC(18, 0) ,
    @tintReferenceType TINYINT ,
    @numReferenceID NUMERIC(18, 0) ,
    @dtCheckDate DATETIME ,
    @bitIsPrint BIT ,
    @vcMemo VARCHAR(1000) ,
    @numUserCntID NUMERIC(18, 0)
AS --Validation of closed financial year
BEGIN
    EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID, @dtCheckDate
	
    IF @tintMode = 1 --SELECT Particulat Check Header
    BEGIN
        SELECT 
			CH.numCheckHeaderID ,
            CH.numChartAcntId ,
            CH.numDomainID ,
            CH.numDivisionID ,
            CH.monAmount ,
            CH.numCheckNo ,
            CH.tintReferenceType ,
            CH.numReferenceID ,
            CH.dtCheckDate ,
            CH.bitIsPrint ,
            CH.vcMemo ,
            CH.numCreatedBy ,
            CH.dtCreatedDate ,
            CH.numModifiedBy ,
            CH.dtModifiedDate ,
            ISNULL(GJD.numTransactionId, 0) AS numTransactionId
        FROM 
			dbo.CheckHeader CH
        LEFT OUTER JOIN 
			General_Journal_Details GJD 
		ON 
			GJD.tintReferenceType = 1
            AND GJD.numReferenceID = CH.numCheckHeaderID
        WHERE 
			numCheckHeaderID = @numCheckHeaderID
            AND CH.numDomainID = @numDomainID
    END
    ELSE
    IF @tintMode = 2 --Insert/Update Check Header
    BEGIN
        IF @numCheckHeaderID > 0 --Update
        BEGIN
            UPDATE 
				dbo.CheckHeader
            SET 
				numChartAcntId = @numChartAcntId ,
                numDivisionID = @numDivisionID ,
                monAmount = @monAmount ,
                numCheckNo = @numCheckNo ,
                tintReferenceType = @tintReferenceType ,
                numReferenceID = @numReferenceID ,
                dtCheckDate = @dtCheckDate ,
                bitIsPrint = @bitIsPrint ,
                vcMemo = @vcMemo ,
                numModifiedBy = @numUserCntID ,
                dtModifiedDate = GETUTCDATE()
            WHERE 
				numCheckHeaderID = @numCheckHeaderID
                AND numDomainID = @numDomainID
        END
        ELSE --Insert
        BEGIN
			--Set Default Class If enable User Level Class Accountng 
            DECLARE @numAccountClass AS NUMERIC(18);
            SET @numAccountClass = 0
					  
            IF @tintReferenceType = 1 --Only for Direct Check
            BEGIN
                DECLARE @tintDefaultClassType AS INT = 0
				SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

				IF @tintDefaultClassType = 1 --USER
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numDefaultClass,0) 
					FROM 
						dbo.UserMaster UM 
					JOIN 
						dbo.Domain D 
					ON 
						UM.numDomainID=D.numDomainId
					WHERE 
						D.numDomainId=@numDomainId 
						AND UM.numUserDetailId=@numUserCntID
				END
				ELSE IF @tintDefaultClassType = 2 --COMPANY
				BEGIN
					SELECT 
						@numAccountClass=ISNULL(numAccountClassID,0) 
					FROM 
						dbo.DivisionMaster DM 
					WHERE 
						DM.numDomainId=@numDomainId 
						AND DM.numDivisionID=@numDivisionID
				END
				ELSE
				BEGIN
					SET @numAccountClass = 0
				END
            END
					  
            INSERT  INTO dbo.CheckHeader
            ( 
				numChartAcntId ,
                numDomainID ,
                numDivisionID ,
                monAmount ,
                numCheckNo ,
                tintReferenceType ,
                numReferenceID ,
                dtCheckDate ,
                bitIsPrint ,
                vcMemo ,
                numCreatedBy ,
                dtCreatedDate ,
                numAccountClass
            )
            SELECT 
				@numChartAcntId ,
				@numDomainID ,
				@numDivisionID ,
				@monAmount ,
				@numCheckNo ,
				@tintReferenceType ,
				@numReferenceID ,
				@dtCheckDate ,
				@bitIsPrint ,
				@vcMemo ,
				@numUserCntID ,
				GETUTCDATE() ,
				@numAccountClass

			SET @numCheckHeaderID = SCOPE_IDENTITY()
		END	
			
        SELECT  
			numCheckHeaderID ,
            numChartAcntId ,
            numDomainID ,
            numDivisionID ,
            monAmount ,
            numCheckNo ,
            tintReferenceType ,
            numReferenceID ,
            dtCheckDate ,
            bitIsPrint ,
            vcMemo ,
            numCreatedBy ,
            dtCreatedDate ,
            numModifiedBy ,
            dtModifiedDate
        FROM 
			dbo.CheckHeader
        WHERE 
			numCheckHeaderID = @numCheckHeaderID
            AND numDomainID = @numDomainID	
    END
    ELSE IF @tintMode = 3 --Print Check List
    BEGIN
        SELECT  CH.numCheckHeaderID ,
                CH.numChartAcntId ,
                CH.numDomainID ,
                CH.numDivisionID ,
                CH.monAmount ,
                CH.numCheckNo ,
                CH.tintReferenceType ,
                CH.numReferenceID ,
                CH.dtCheckDate ,
                CH.bitIsPrint ,
                CH.vcMemo ,
                CH.numCreatedBy ,
                CH.dtCreatedDate ,
                CH.numModifiedBy ,
                CH.dtModifiedDate ,
                CASE CH.tintReferenceType
                    WHEN 11
                    THEN dbo.fn_GetUserName(ISNULL(PD.numUserCntID,
                                                    0))
                    ELSE dbo.fn_GetComapnyName(ISNULL(CH.numDivisionID,
                                                    0))
                END AS vcCompanyName ,
                CASE CH.tintReferenceType
                    WHEN 1 THEN 'Checks'
                    WHEN 8 THEN 'Bill Payment'
                    WHEN 10 THEN 'RMA'
                    WHEN 11 THEN 'Payroll'
                END AS vcReferenceType ,
                dbo.fn_GetContactName(CH.numCreatedBy) AS EmployerName ,
                CASE CH.tintReferenceType
                    WHEN 1 THEN ''
                    WHEN 8
                    THEN ( SELECT ISNULL(( SELECT SUBSTRING(( SELECT
                                                    ','
                                                    + CASE
                                                    WHEN ISNULL(BPD.numBillID,
                                                    0) > 0
                                                    THEN 'Bill-'
                                                    + CAST(BPD.numBillID AS VARCHAR(18))
                                                    ELSE CAST(OBD.vcBizDocID AS VARCHAR(18))
                                                    END
                                                    FROM
                                                    BillPaymentHeader BPH
                                                    JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
                                                    LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
                                                    WHERE
                                                    BPH.numDomainID = CH.numDomainID
                                                    AND BPH.numBillPaymentID = CH.numReferenceID
                                                    FOR
                                                    XML
                                                    PATH('')
                                                    ), 2, 200000)
                                        ), '')
                        )
                END AS vcBizDoc ,
                CASE CH.tintReferenceType
                    WHEN 1 THEN ''
                    WHEN 8
                    THEN ( SELECT ISNULL(( SELECT SUBSTRING(( SELECT
                                                    ','
                                                    + CASE
                                                    WHEN BPD.numOppBizDocsID > 0
                                                    THEN CAST(ISNULL(OBD.vcRefOrderNo,
                                                    '') AS VARCHAR(18))
                                                    WHEN BPD.numBillID > 0
                                                    THEN CAST(ISNULL(BH.vcReference,
                                                    '') AS VARCHAR(500))
                                                    END
                                                    FROM
                                                    BillPaymentHeader BPH
                                                    JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
                                                    LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
                                                    LEFT JOIN BillHeader BH ON BPD.numBillID = BH.numBillID
                                                    WHERE
                                                    BPH.numDomainID = CH.numDomainID
                                                    AND BPH.numBillPaymentID = CH.numReferenceID
                                                    FOR
                                                    XML
                                                    PATH('')
                                                    ), 2, 200000)
                                        ), '')
                        )
                    ELSE ''
                END AS vcRefOrderNo
        FROM    dbo.CheckHeader CH
                LEFT JOIN PayrollDetail PD ON CH.numReferenceID = PD.numPayrollDetailID
                                                AND CH.tintReferenceType = 11
        WHERE   CH.numDomainID = @numDomainID
                AND ( numCheckHeaderID = @numCheckHeaderID
                        OR @numCheckHeaderID = 0
                    )
                AND ( CH.tintReferenceType = @tintReferenceType
                        OR @tintReferenceType = 0
                    )
                AND ( CH.numReferenceID = @numReferenceID
                        OR @numReferenceID = 0
                    )
                AND CH.numChartAcntId = @numChartAcntId
                AND ISNULL(bitIsPrint, 0) = 0	 
    END
	ELSE IF @tintMode = 4 --New Print Check List
	BEGIN
		SELECT  
			CH.numCheckHeaderID ,
			CH.numChartAcntId ,
			CH.numDomainID ,
			CH.numDivisionID ,
			(CASE WHEN tintReferenceType=10 OR tintReferenceType=1 THEN ISNULL(CH.monAmount,0) ELSE  ISNULL([BPD].[monAmount],0) END) monAmount,
			CH.numCheckNo ,
			CH.tintReferenceType ,
			CH.numReferenceID ,
			CH.dtCheckDate ,
			CH.bitIsPrint ,
			ISNULL(CH.vcMemo,'') vcMemo,
			CH.numCreatedBy ,
			CH.dtCreatedDate ,
			CH.numModifiedBy ,
			CH.dtModifiedDate ,
			CASE CH.tintReferenceType
				WHEN 11
				THEN dbo.fn_GetUserName(ISNULL(PD.numUserCntID,
											0))
				ELSE dbo.fn_GetComapnyName(ISNULL(CH.numDivisionID,
											0))
			END AS vcCompanyName ,
			CASE CH.tintReferenceType
				WHEN 1 THEN 'Checks'
				WHEN 8 THEN 'Bill Payment'
				WHEN 10 THEN 'RMA'
				WHEN 11 THEN 'Payroll'
			END AS vcReferenceType ,
			dbo.fn_GetContactName(CH.numCreatedBy) AS EmployerName ,
			CASE CH.tintReferenceType
				WHEN 1 THEN ''
				WHEN 8
				THEN CASE WHEN ISNULL(BPD.numBillID, 0) > 0
						THEN 'Bill-'
								+ CAST(BPD.numBillID AS VARCHAR(18))
						ELSE CAST(OBD.vcBizDocID AS VARCHAR(18))
					END
			END AS vcBizDoc ,
			CASE CH.tintReferenceType
				WHEN 1 THEN ''
				WHEN 8
				THEN CASE WHEN BPD.numOppBizDocsID > 0
						THEN CAST(ISNULL(OBD.vcRefOrderNo,
											'') AS VARCHAR(18))
						WHEN BPD.numBillID > 0
						THEN CAST(ISNULL(BH.vcReference,
											'') AS VARCHAR(500))
					END
				ELSE ''
			END AS vcRefOrderNo
		FROM  
			dbo.CheckHeader CH
			LEFT JOIN PayrollDetail PD ON CH.numReferenceID = PD.numPayrollDetailID AND CH.tintReferenceType = 11
			LEFT JOIN BillPaymentHeader BPH ON BPH.numBillPaymentID = CH.numReferenceID AND BPH.numDomainID = CH.numDomainID
			LEFT JOIN BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
			LEFT JOIN OpportunityBizDocs OBD ON OBD.numOppBizDocsId = BPD.numOppBizDocsID
			LEFT JOIN BillHeader BH ON BPD.numBillID = BH.numBillID
		WHERE   CH.numDomainID = @numDomainID
				AND ([CH].[numDivisionID] = @numDivisionID OR @numDivisionID = 0)
				AND ( numCheckHeaderID = @numCheckHeaderID
						OR @numCheckHeaderID = 0
					)
				AND ( CH.tintReferenceType = @tintReferenceType
						OR @tintReferenceType = 0
					)
				AND ( CH.numReferenceID = @numReferenceID
						OR @numReferenceID = 0
					)
				AND CH.numChartAcntId = @numChartAcntId
				AND ISNULL(bitIsPrint, 0) = 0	
	END
END
GO