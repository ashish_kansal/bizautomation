GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceLevelItemPrice')
DROP PROCEDURE USP_GetPriceLevelItemPrice
GO
CREATE PROCEDURE [dbo].[USP_GetPriceLevelItemPrice]                                        
@numItemCode as numeric(9),
@numDomainID as numeric(9)=0,            
--@numPricingID as numeric(9)=0,
@numWareHouseItemID as numeric(9),
@numDivisionID as numeric(9)=0                  
as                 
BEGIN
	DECLARE @monListPrice AS DECIMAL(20,5);	SET @monListPrice=0
	DECLARE @monVendorCost AS DECIMAL(20,5);	SET @monVendorCost=0

	IF((@numWareHouseItemID>0) and exists(select * from item where numItemCode=@numItemCode and charItemType='P'))      
	BEGIN      
		SELECT 
			@monListPrice=ISNULL(monWListPrice,0) 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID      
		
		IF @monListPrice=0 
		BEGIN
			SELECT @monListPrice=monListPrice FROM Item WHERE numItemCode=@numItemCode       
		END
	END      
	ELSE      
	BEGIN      
		 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode      
	END 

	SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)

	SELECT
		[numPricingID],
		[numPriceRuleID],
		[intFromQty],
		[intToQty],
		[tintRuleType],
		[tintDiscountType],
		ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',rowId)) AS [vcName],
		decDiscount
	FROM
	(
		SELECT  
			ROW_NUMBER() OVER(ORDER BY numPricingID) as rowId,
			[numPricingID],
			[numPriceRuleID],
			[intFromQty],
			[intToQty],
			[tintRuleType],
			[tintDiscountType],
			CASE 
				WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
					THEN @monListPrice - (@monListPrice * ( ISNULL(decDiscount,0) /100))
				WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
					THEN @monListPrice - ISNULL(decDiscount,0)
				WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
					THEN @monVendorCost + (@monVendorCost * ( ISNULL(decDiscount,0) /100))
				WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
					THEN @monVendorCost + ISNULL(decDiscount,0)
				WHEN tintRuleType=3 --Named Price
					THEN ISNULL(decDiscount,0)
				WHEN tintDiscountType=3 --Named Price
					THEN ISNULL(decDiscount,0)
			END AS decDiscount
		FROM
			[PricingTable]
		WHERE
			ISNULL(numItemCode,0)=@numItemCode
			AND ISNULL(numCurrencyID,0) = 0
	) T1
	LEFT JOIN 
		PricingNamesTable pnt ON pnt.tintPriceLevel = T1.rowId AND pnt.numDomainID=@numDomainID
	WHERE 
		rowId IS NOT NULL 
		OR (pnt.vcPriceLevelName IS NOT NULL AND pnt.vcPriceLevelName <> '')
	ORDER BY
		T1.rowId
			
  
	DECLARE @numRelationship AS NUMERIC(9)
	DECLARE @numProfile AS NUMERIC(9)

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile 
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID
	WHERE 
		numDivisionID =@numDivisionID       

  
	SELECT 
		PT.numPricingID,
		PT.numPriceRuleID,
		ISNULL(PT.intFromQty,0) AS intFromQty,
		ISNULL(PT.intToQty,0) AS intToQty,
		PT.tintRuleType,
		PT.tintDiscountType,
		ISNULL(PT.vcName,'') vcName,
		CASE 
			WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
				THEN @monListPrice - (@monListPrice * ( ISNULL(PT.decDiscount,0) /100))
            WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
				THEN @monListPrice - ISNULL(PT.decDiscount,0)
            WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN @monVendorCost + (@monVendorCost * ( ISNULL(PT.decDiscount,0) /100))
            WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN @monVendorCost + ISNULL(PT.decDiscount,0)
			WHEN PT.tintRuleType=3 --Named Price
				THEN ISNULL(PT.decDiscount,0)
			WHEN PT.tintDiscountType=3 --Named Price
				THEN ISNULL(PT.decDiscount,0)
		END AS decDiscount
	FROM 
		Item I
    JOIN 
		PriceBookRules P 
	ON 
		I.numDomainID = P.numDomainID
    LEFT JOIN 
		PriceBookRuleDTL PDTL 
	ON 
		P.numPricRuleID = PDTL.numRuleID
    LEFT JOIN 
		PriceBookRuleItems PBI 
	ON 
		P.numPricRuleID = PBI.numRuleID
    LEFT JOIN 
		[PriceBookPriorities] PP 
	ON 
		PP.[Step2Value] = P.[tintStep2] 
		AND PP.[Step3Value] = P.[tintStep3]
    JOIN 
		[PricingTable] PT 
	ON 
		P.numPricRuleID = PT.numPriceRuleID
		AND ISNULL(PT.numCurrencyID,0) = 0
	WHERE
		I.numItemCode = @numItemCode 
		AND P.tintRuleFor=1 
		AND P.tintPricingMethod = 1
		AND 
		(
		((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
		OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
		OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

		OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
		OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
		OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
		)
	ORDER BY PP.Priority ASC
END
      
