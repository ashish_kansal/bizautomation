GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemImageDetails')
DROP PROCEDURE USP_GetItemImageDetails
GO
CREATE PROCEDURE USP_GetItemImageDetails
(
	@numItemImageId		BIGINT,
	@numDomainID			NUMERIC(18)
)
AS 
BEGIN
	SELECT  ISNULL(II.numItemImageId,0) AS [numItemImageId],
			ISNULL(I.numItemCode,0) AS [numItemCode],
			ISNULL(II.vcPathForImage,'') AS [vcPathForImage],
			ISNULL(II.vcPathForTImage,'') AS [vcPathForTImage],
			ISNULL(II.bitDefault,0) AS [bitDefault],
			ISNULL(II.intDisplayOrder,0) AS [intDisplayOrder],
			ISNULL(II.numDomainId,0) AS [numDomainId],
			ISNULL(II.bitIsImage,0) AS [bitIsImage]
	FROM  dbo.ItemImages II	
	JOIN dbo.Item I ON II.numItemCode = I.numItemCode AND II.numDomainID =  I.numDomainID 
	WHERE II.numItemImageId = @numItemImageId 
	AND I.numDomainID = @numDomainID
	
END