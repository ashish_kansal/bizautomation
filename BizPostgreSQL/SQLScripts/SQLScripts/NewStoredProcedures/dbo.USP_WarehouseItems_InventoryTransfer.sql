GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_WarehouseItems_InventoryTransfer' ) 
    DROP PROCEDURE USP_WarehouseItems_InventoryTransfer
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_InventoryTransfer]
@numDomainID NUMERIC(18,0),
@numUserCntID NUMERIC(18,0),
@numFromWarehouseItemID NUMERIC(18,0),
@numToWarehouseItemID	NUMERIC(18,0),
@numQty NUMERIC(18,0),
@strItems as VARCHAR(3000)=''
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numOnHand FLOAT
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
	DECLARE @numItemCode AS NUMERIC(18,0)
	DECLARE @vcTemp AS VARCHAR(MAX) = ''

	SELECT @numOnHand=numOnHand,@numItemCode=numItemID FROM WarehouseItems WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numFromWarehouseItemID 

	IF ISNULL(@numOnHand,0) < @numQty
	BEGIN
		RAISERROR('INSUFFICIENT_ONHAND_QUANTITY',16,1)
	END
	ELSE 
	BEGIN
		-- IF SERIAL/LOT ITEM THAN CHECK SERIAL/LOT NUMBER ARE VALID AND ITS QTY ARE VALID
		SELECT @bitSerial=bitSerialized,@bitLot=bitLotNo FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
		
		IF @bitSerial = 1 OR @bitLot = 1
		BEGIN
			DECLARE @posComma int, @strKeyVal varchar(20)

			SET @strItems=RTRIM(@strItems)
			IF RIGHT(@strItems, 1)!=',' SET @strItems=@strItems+','

			SET @posComma=PatIndex('%,%', @strItems)
			WHILE @posComma>1
			BEGIN
				SET @strKeyVal=ltrim(rtrim(substring(@strItems, 1, @posComma-1)))
	
				DECLARE @posBStart INT,@posBEnd int, @strQty INT,@strName NUMERIC(18,0)

				IF @bitLot=1 
				BEGIN
					SET @posBStart=PatIndex('%(%', @strKeyVal)
					SET @posBEnd=PatIndex('%)%', @strKeyVal)
					IF( @posBStart>1 AND @posBEnd>1)
					BEGIN
						SET @strName=LTRIM(RTRIM(SUBSTRING(@strKeyVal, 1, @posBStart-1)))
						SET	@strQty=LTRIM(RTRIM(SUBSTRING(@strKeyVal, @posBStart+1,LEN(@strKeyVal)-@posBStart-1)))
					END
					ELSE
					BEGIN
						SET @strName=@strKeyVal
						SET	@strQty=1
					END

					IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numFromWarehouseItemID AND numWareHouseItmsDTLID=@strName AND numQty >= @strQty)
					BEGIN
						
						UPDATE 
							WareHouseItmsDTL 
						SET 
							numQty = numQty - @strQty,
							@vcTemp = CONCAT(@vcTemp,(CASE WHEN LEN(@vcTemp) = 0 THEN '' ELSE ',' END),ISNULL(vcSerialNo,'') ,'(', @strQty ,')')
						WHERE 
							numWareHouseItemID=@numFromWarehouseItemID 
							AND numWareHouseItmsDTLID=@strName

						IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numToWarehouseItemID AND numWareHouseItmsDTLID=@strName)
						BEGIN
							UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) + @strQty WHERE numWareHouseItemID=@numToWarehouseItemID AND numWareHouseItmsDTLID=@strName
						END
						ELSE
						BEGIN
							INSERT INTO WareHouseItmsDTL
							(
								numWareHouseItemID,
								vcSerialNo,
								numQty,
								dExpirationDate,
								bitAddedFromPO
							)
							SELECT
								@numToWarehouseItemID,
								vcSerialNo,
								@strQty,
								dExpirationDate,
								bitAddedFromPO
							FROM 
								WareHouseItmsDTL
							WHERE
								numWareHouseItemID=@numFromWarehouseItemID AND numWareHouseItmsDTLID=@strName
						END
					END
					ELSE
					BEGIN
						RAISERROR('INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY',16,1)
					END
				END
				ELSE
				BEGIN
					SET @strName=@strKeyVal
					SET	@strQty=1

					IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numFromWarehouseItemID AND numWareHouseItmsDTLID=@strName AND numQty = 1)
					BEGIN
						UPDATE 
							WareHouseItmsDTL 
						SET 
							numWareHouseItemID=@numToWarehouseItemID
							,@vcTemp = @vcTemp + (CASE WHEN LEN(@vcTemp) = 0 THEN '' ELSE ',' END) + ISNULL(vcSerialNo,'') 
						WHERE 
							numWareHouseItemID=@numFromWarehouseItemID 
							AND numWareHouseItmsDTLID=@strName
					END
					ELSE
					BEGIN
						RAISERROR('INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY',16,1)
					END
				END  
	  
				SET @strItems=substring(@strItems, @posComma +1, len(@strItems)-@posComma)
				SET @posComma=PatIndex('%,%',@strItems)
			END
		END

		DECLARE @description VARCHAR(MAX) = ''

		IF @bitSerial = 1 OR @bitLot = 1
		BEGIN
			SET @description = CONCAT('Inventory Transfer - Transferred (Qty:',@numQty,', Serial/Lot#:',')')
		END
		ELSE
		BEGIN
			SET @description = CONCAT('Inventory Transfer - Transferred (Qty:',@numQty,')')
		END

		UPDATE
			WarehouseItems
		SET
			numOnHand = ISNULL(numOnHand,0) - @numQty
			,dtModified = GETDATE() 
		WHERE
			numWareHouseItemID=@numFromWarehouseItemID

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numFromWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID


		IF @bitSerial = 1 OR @bitLot = 1
		BEGIN
			SET @description = CONCAT('Inventory Transfer - Receieved (Qty:',@numQty,', Serial/Lot#:',')')
		END
		ELSE
		BEGIN
			SET @description = CONCAT('Inventory Transfer - Receieved (Qty:',@numQty,')')
		END

		UPDATE
			WarehouseItems
		SET
			numOnHand = (CASE WHEN numBackOrder >= @numQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numQty - ISNULL(numBackOrder,0)) END) 
			,numAllocation = (CASE WHEN numBackOrder >= @numQty THEN ISNULL(numAllocation,0) + @numQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END) 
			,numBackOrder = (CASE WHEN numBackOrder >= @numQty THEN ISNULL(numBackOrder,0) - @numQty ELSE 0 END) 
			,dtModified = GETDATE() 
		WHERE
			numWareHouseItemID=@numToWarehouseItemID

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numToWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID

	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END