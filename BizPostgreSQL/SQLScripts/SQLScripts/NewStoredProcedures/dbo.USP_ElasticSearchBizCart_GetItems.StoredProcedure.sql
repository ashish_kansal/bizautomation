GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_ElasticSearchBizCart_GetItems')
DROP PROCEDURE USP_ElasticSearchBizCart_GetItems
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_ElasticSearchBizCart_GetItems]
	@numDomainID NUMERIC(18,0),
	@vcItemCodes VARCHAR(MAX) = NULL
AS 
BEGIN
	SET NOCOUNT ON;
	-- @vcItemCodes is corrently set for single item
	
    DECLARE @strSQL NVARCHAR(MAX)

    DECLARE @fldList AS NVARCHAR(MAX)
	DECLARE @fldList1 AS NVARCHAR(MAX)
	SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
	SELECT @fldList1=COALESCE(@fldList1 + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)) FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=9
     
	-- Item Details
	SET @strSQL = CONCAT(@strSQL,'
	SELECT --I.*,
	I.numDomainID
	,I.numItemCode
	,ISNULL(I.bitMatrix,0) AS bitMatrix
	,ISNULL(I.bitKitParent,0) AS bitKitParent
	,I.numItemGroup
	,vcItemName
	,I.bintCreatedDate
	,vcManufacturer
	,ISNULL(txtItemDesc,'''') txtItemDesc
	,vcSKU
	,fltHeight
	,fltWidth
	,fltLength
	,vcModelID
	,fltWeight
	,numBaseUnit
	,numSaleUnit
	,numPurchaseUnit
	,bitFreeShipping
	,charItemType
	,monListPrice
	,bitAllowBackOrder
	--,bitShowInStock
	,numVendorID
	,numItemClassification
	--,SUM(numOnHand) numOnHand
	--,SUM(numAllocation) numAllocation
	,ISNULL(I.numManufacturer,0) numManufacturer
	,ISNULL(IED.txtDesc,'''') txtDesc
	,ISNULL(IED.txtShortDesc,'''') txtShortDesc 
	,IM.vcPathForImage
	,IM.vcpathForTImage
	,UOM AS UOMConversionFactor
	,UOMPurchase AS UOMPurchaseConversionFactor
	
	,CONVERT(nvarchar(200),'''') AS vcCategoryName
	,CONVERT(nvarchar(200),'''') AS CategoryDesc
	,CONVERT(NUMERIC(9),0) AS numTotalPromotions
	,CONVERT(nvarchar(200),'''') AS vcPromoDesc
	,CONVERT(bit,0) AS bitRequireCouponCode
	,CONVERT(bit,0) AS bitInStock
	,CONVERT(nvarchar(200),'''') AS InStock

	,CONVERT(NUMERIC(9,2),0) AS monFirstPriceLevelPrice
	,CONVERT(NUMERIC(9,2),0) AS fltFirstPriceLevelDiscount
	,CONVERT(NUMERIC(9,2),0) AS monListPrice
	,CONVERT(NUMERIC(9,2),0) AS monMSRP
	,CONVERT(bit,0) AS bitHasChildKits
	,CONVERT(NUMERIC(9),0) AS [numWareHouseItemID]

	',(CASE WHEN LEN(ISNULL(@fldList,'')) > 0 THEN CONCAT(',',@fldList) ELSE '' END),(CASE WHEN LEN(ISNULL(@fldList1,'')) > 0 THEN CONCAT(',',@fldList1) ELSE '' END),'

	,ISNULL(I.IsArchieve,0) AS IsArchieve
	,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(I.bitMatrix,0) = 1 THEN dbo.fn_GetItemAttributes(I.numDomainID,I.numItemCode,1) ELSE '''' END) AS vcAttributes
	,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(I.bitMatrix,0) = 1 THEN CONCAT(''['',STUFF((SELECT 
																								CONCAT('',{"FieldID":'',IA.FLD_ID,'',"FieldVaue":'',IA.FLD_Value,''}'')
																							FROM
																								ItemAttributes IA
																							WHERE
																								IA.numItemCode = I.numItemCode
																							ORDER BY 
																								ID
																							FOR XML PATH('''')),1,1,''''),'']'') ELSE ''[]'' END) AS vcAttributeValues
	FROM Item I
	LEFT JOIN ItemExtendedDetails IED ON (IED.numItemCode = I.numItemCode)
	LEFT JOIN ItemImages IM ON I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
	INNER JOIN
	(
		SELECT [OutParam] FROM dbo.[SplitString](''' + @vcItemCodes + ''','','')
	) TEMPItem ON I.numItemCode = TEMPItem.OutParam
	CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
	CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase
	')

	IF LEN(ISNULL(@fldList,'')) > 0
	BEGIN
		SET @strSQL = @strSQL +'
		left join (
						SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
							CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
												WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
												WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
												ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
						JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
						--AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
						AND t2.RecId IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcItemCodes + ''','',''))
						) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId' 
	END

	IF LEN(ISNULL(@fldList1,'')) > 0
	BEGIN
					
		SET @strSQL = @strSQL +'
		left join (
					SELECT  t2.numItemCode AS RecId, REPLACE(t1.Fld_label,'' '','''') + ''_'' + CAST(t1.Fld_Id AS VARCHAR) as Fld_label, 
						CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
											WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
											WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
											ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
					JOIN ItemAttributes AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 9 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
					--AND t2.numItemCode IN (SELECT numItemCode FROM #tmpPagedItems )
					AND t2.numItemCode IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcItemCodes + ''','',''))
					) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList1 + ')) AS pvt1 on I.numItemCode=pvt1.RecId'
	END

	DECLARE @tmpSQL AS NVARCHAR(MAX)
	SET @tmpSQL = @strSQL
	
	PRINT CAST(@tmpSQL AS NTEXT)
	EXEC sp_executeSQL @strSQL

	-- Item Warehouse Details
	SELECT numItemID,vcWarehouse,numWarehouseItemID,WI.numWareHouseID,ISNULL(SUM(numOnHand),0) AS numOnHand ,ISNULL(SUM(numAllocation),0) AS numAllocation
	FROM WareHouseItems WI
	INNER JOIN Warehouses W ON WI.numwarehouseId = W.numWarehouseID
	INNER JOIN
	(
		SELECT [OutParam] FROM dbo.[SplitString](@vcItemCodes,',')
	) TEMPItem ON WI.numItemID = TEMPItem.OutParam
	GROUP BY numItemID,vcWarehouse,numWarehouseItemID,WI.numWareHouseID

	-- Item Category
	SELECT numItemID,IC.numCategoryID,C.vcCategoryName
	,C.vcDescription as CategoryDesc
	FROM ItemCategory IC
	INNER JOIN Category C ON IC.numCategoryID = C.numCategoryID
	INNER JOIN
	(
		SELECT [OutParam] FROM dbo.[SplitString](@vcItemCodes,',')
	) TEMPItem ON IC.numItemID = TEMPItem.OutParam


END
GO
