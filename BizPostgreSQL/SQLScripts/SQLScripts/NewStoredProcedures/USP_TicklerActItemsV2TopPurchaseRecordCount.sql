GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2TopPurchaseRecordCount')
DROP PROCEDURE USP_TicklerActItemsV2TopPurchaseRecordCount
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2TopPurchaseRecordCount]     
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,
@numViewID AS NUMERIC(18,0),
@numTotalRecords AS NUMERIC(18,0) OUTPUT
AS
BEGIN
	DECLARE @bitGroupByOrder AS BIT =0
	DECLARE @numBatchID AS NUMERIC(18,0)=0
	DECLARE @bitIncludeSearch AS BIT =1
	DECLARE @tintPrintBizDocViewMode AS INT =1
	DECLARE @tintPendingCloseFilter AS INT =1
	DECLARE @numShippingZone AS NUMERIC(18,0)=0
	DECLARE @vcOrderStauts VARCHAR(MAX) ='' 
	DECLARE @tintFilterBy TINYINT =0
	DECLARE @vcFilterValue VARCHAR(MAX)=''
	,@numPageIndex INT=1
	,@vcSortColumn VARCHAR(100)=''
	,@vcSortOrder VARCHAR(4)=''
	,@numWarehouseID VARCHAR(100)
	,@bitRemoveBO BIT =0


	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6
	BEGIN
		SET @bitGroupByOrder = 1
	END
	DECLARE @bitShowOnlyFullyReceived BIT
	DECLARE @tintBillType INT
	DECLARE @tintPackingViewMode AS INT =1
	DECLARE @tintPackingMode AS INT
	DECLARE @tintInvoicingType AS INT 
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			@bitShowOnlyFullyReceived=ISNULL(bitShowOnlyFullyReceived,0)
			,@tintBillType = ISNULL(tintBillType,1)
		FROM 
			MassPurchaseFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END

	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	DECLARE @vcCustomSearchValue AS VARCHAR(MAX) ='' 
	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC)')
	--SET @vcCustomSearchValue  = ' AND OpportunityMaster.bintCreatedDate BETWEEN ' +@startDate +' AND '+ @endDate
CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)
CREATE TABLE #TempWareHouse
(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	numWareHouseId NUMERIC(18,0)
)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	DECLARE @WarehouseCount AS INT = 0
	DECLARE @I AS INT = 1
INSERT INTO #TempWareHouse 	SELECT 
		[numWareHouseID]         
   FROM [dbo].[Warehouses] W 
   LEFT JOIN
		AddressDetails AD
	ON
        W.numAddressID = AD.numAddressID
   WHERE 
		W.numDomainID=@numDomainID

	SELECT @WarehouseCount = COUNT(*) FROM #TempWareHouse
	WHILE(@I<=@WarehouseCount)
	BEGIN
		SELECT @numWarehouseID = numWareHouseId FROM #TempWareHouse WHERE ID=@I
		
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 2
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID=1 
										THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=2
										THEN (CASE WHEN ISNULL(OpportunityItems.numQtyReceived,0) > 0 AND ISNULL(OpportunityItems.numQtyReceived,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID=3
										THEN (CASE 
												WHEN (CASE 
														WHEN @tintBillType=2 
														THEN ISNULL(OpportunityItems.numQtyReceived,0) 
														WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
														END)  - ISNULL((SELECT 
																			SUM(OpportunityBizDocItems.numUnitHour)
																		FROM
																			OpportunityBizDocs
																		INNER JOIN
																			OpportunityBizDocItems
																		ON
																			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																		WHERE
																			OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																			AND OpportunityBizDocs.numBizDocId=644
																			AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID=4 AND ISNULL(@bitShowOnlyFullyReceived,0) = 1
										THEN (CASE 
												WHEN (SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppID AND ISNULL(OpportunityItems.numUnitHour,0) <> ISNULL(OpportunityItems.numUnitHourReceived,0) ) > 0 
												THEN 0 
												ELSE 1 
											END)
										ELSE 1
									END)
							AND 1 = (CASE WHEN @numViewID IN (1,2) AND ISNULL(@numWarehouseID,0) > 0 AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN @numWarehouseID=WareHouseItems.numWarehouseID THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)'
							,@vcCustomSearchValue
							,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId' ELSE '' END));

	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @numViewID NUMERIC(18,0), @numWarehouseID NUMERIC(18,0), @bitShowOnlyFullyReceived BIT, @tintBillType TINYINT', @numDomainID, @vcOrderStauts, @numViewID, @numWarehouseID, @bitShowOnlyFullyReceived, @tintBillType;


	--PRINT @numDomainID 
	--PRINT @vcOrderStauts 
	--PRINT  @vcFilterValue 
	--PRINT @tintFilterBy 
	--PRINT  @numViewID 
	--PRINT  @tintPackingViewMode 
	--PRINT  @numShippingServiceItemID 
	--PRINT @numShippingZone 
	--PRINT  @numDefaultSalesShippingDoc 
	--PRINT  @tintInvoicingType 
	--PRINT  @tintPackingMode 
	--PRINT @bitRemoveBO 
	PRINT @numWarehouseID
	--SELECT * FROM #TEMPMSRecords
	SET @I = @I +1
	END
	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords) 
END