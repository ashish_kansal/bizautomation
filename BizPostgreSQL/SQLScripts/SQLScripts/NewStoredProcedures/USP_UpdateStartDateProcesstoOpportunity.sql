
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateStartDateProcesstoOpportunity')
DROP PROCEDURE USP_UpdateStartDateProcesstoOpportunity
GO
CREATE PROCEDURE [dbo].[USP_UpdateStartDateProcesstoOpportunity]
@numStageDetailsId as numeric(18)=0,   
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,
@dtmStartDate AS DATETIME=NULL
as    
BEGIN   
	UPDATE 
		StagePercentageDetails
	SET
		dtStartDate=@dtmStartDate
	WHERE
		numOppId=@numOppId AND numStageDetailsId=@numStageDetailsId
	
END 