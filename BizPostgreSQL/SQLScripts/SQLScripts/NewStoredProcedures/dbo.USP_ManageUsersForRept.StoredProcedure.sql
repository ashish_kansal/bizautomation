GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageUsersForRept' ) 
    DROP PROCEDURE USP_ManageUsersForRept
GO
CREATE PROCEDURE USP_ManageUsersForRept
    @numUserCntId AS NUMERIC(9),
    @numDomainId AS NUMERIC(9),
    @tintType AS TINYINT,
    @strUser AS VARCHAR(4000),
    @tintUserType AS TINYINT
AS 
DECLARE @separator_position AS INTEGER    
DECLARE @strPosition AS VARCHAR(1000)    
    
DELETE  FROM ForReportsByUser
WHERE   numContactID = @numUserCntId
        AND numDomainId = @numDomainID
        AND tintType = @tintType    
     
    
WHILE PATINDEX('%,%', @strUser) <> 0    
    BEGIN -- Begin for While Loop    
        SELECT  @separator_position = PATINDEX('%,%', @strUser)    
        SELECT  @strPosition = LEFT(@strUser, @separator_position - 1)    
        SELECT  @strUser = STUFF(@strUser, 1, @separator_position,
                                      '')    
        INSERT  INTO ForReportsByUser ( numContactID,
                                             numSelectedUserCntID,
                                             numDomainID,
                                             tintType,tintUserType )
        VALUES  (
                  @numUserCntId,
                  @strPosition,
                  @numDomainID,
                  @tintType,
                  @tintUserType
                )    
     
      
    END
