
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCOACreditCardCharge]    Script Date: 09/25/2009 16:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCOACreditCardCharge')
DROP PROCEDURE USP_GetCOACreditCardCharge
GO
CREATE PROCEDURE [dbo].[USP_GetCOACreditCardCharge]
	@numTransChargeID numeric(9),
    @numCreditCardTypeId NUMERIC(9),
	@numDomainID NUMERIC(9)

AS
begin
	SELECT [numTransChargeID], 
		[numAccountId], 
		[fltTransactionCharge], 
		[numCreditCardTypeId],
		tintBareBy
	FROM COACreditCardCharge
	WHERE ([numTransChargeID] = @numTransChargeID OR @numTransChargeID=0)
	AND (numCreditCardTypeId=@numCreditCardTypeId OR @numCreditCardTypeId = 0)
	AND numDomainID =@numDomainID
end


