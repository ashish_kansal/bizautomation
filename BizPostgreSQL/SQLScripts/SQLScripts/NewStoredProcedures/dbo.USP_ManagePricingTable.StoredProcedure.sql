GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePricingTable')
DROP PROCEDURE USP_ManagePricingTable
GO
CREATE PROCEDURE USP_ManagePricingTable
    @numPriceRuleID NUMERIC(18,0),
    @numItemCode NUMERIC(18,0),
	@numCurrencyID NUMERIC(18,0),
    @strItems TEXT
AS 
BEGIN
    DELETE FROM [PricingTable] WHERE ISNULL([numPriceRuleID],0) = @numPriceRuleID AND ISNULL(numCurrencyID,0) = @numCurrencyID AND ISNULL(numItemCode,0)=@numItemCode
		
    DECLARE @hDocItem INT
    IF CONVERT(VARCHAR(10), @strItems) <> '' 
    BEGIN
        EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
          
        INSERT  INTO [PricingTable]
        (
            numPriceRuleID,
            [intFromQty],
            [intToQty],
            [tintRuleType],
            [tintDiscountType],
            [decDiscount],
			numItemCode,
			numCurrencyID
        )
        SELECT  
			X.numPriceRuleID,
            X.[intFromQty],
            X.[intToQty],
            X.[tintRuleType],
            CASE ISNULL(X.[tintDiscountType],0) WHEN 0 THEN 1 ELSE ISNULL(X.[tintDiscountType],0) END,
			CAST(CONVERT(DECIMAL(20,5), X.[decDiscount]) AS VARCHAR) AS decDiscount,
			X.numItemCode,
			@numCurrencyID
        FROM    
		(
			SELECT 
				*
            FROM 
				OPENXML (@hDocItem, '/NewDataSet/Item', 2)
            WITH 
			( 
				numPriceRuleID NUMERIC
				,intFromQty INT
				,intToQty INT
				,tintRuleType TINYINT
				,tintDiscountType TINYINT
				,decDiscount DECIMAL(20,5)
				,numItemCode NUMERIC
				,vcName VARCHAR(300)
			)
        ) X
               

		DECLARE @DecDiscount AS DECIMAL(18, 4)

		SELECT 
			@DecDiscount = CAST(CONVERT(DECIMAL(18, 4), X.[decDiscount]) AS VARCHAR) 
        FROM  
		( 
			SELECT 
				*
            FROM 
				OPENXML (@hDocItem, '/NewDataSet/Item', 2)
            WITH 
			( 
				numPriceRuleID NUMERIC
				,intFromQty INT
				,intToQty INT
				,tintRuleType TINYINT
				,tintDiscountType TINYINT
				,decDiscount DECIMAL(20,5)
				,numItemCode NUMERIC
				,vcName VARCHAR(300)
			)
        ) X
		
		EXEC sp_xml_removedocument @hDocItem

		IF EXISTS(SELECT * FROM SalesTemplateItems WHERE numItemCode = @numItemCode)
		BEGIN
			UPDATE SalesTemplateItems 
			SET monPrice = @DecDiscount, monTotAmount = @DecDiscount, monTotAmtBefDiscount = @DecDiscount 
			WHERE numItemCode = @numItemCode 
		END
    END
END