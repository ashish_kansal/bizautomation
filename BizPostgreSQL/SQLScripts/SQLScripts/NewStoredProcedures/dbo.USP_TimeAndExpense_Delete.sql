GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_Delete')
DROP PROCEDURE USP_TimeAndExpense_Delete
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_Delete]
( 
	@numDomainID NUMERIC(18,0)
    ,@numCategoryHDRID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT 
					OpportunityItems.numoppitemtCode 
				FROM 
					TimeAndExpense 
				INNER JOIN 
					OpportunityItems 
				ON 
					TimeAndExpense.numOppId=OpportunityItems.numOppId 
					AND TimeAndExpense.numOppItemID=OpportunityItems.numoppitemtCode 
				WHERE 
					TimeAndExpense.numDomainId=@numDomainID 
					AND TimeAndExpense.numCategoryHDRID=@numCategoryHDRID)
	BEGIN
		RAISERROR('ORDER_EXISTS',16,1)
		RETURN
	END

	IF EXISTS (SELECT 
					OpportunityBizDocItems.numOppBizDocItemID
				FROM 
					TimeAndExpense 
				INNER JOIN 
					OpportunityBizDocItems
				ON 
					TimeAndExpense.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID 
					AND TimeAndExpense.numOppBizDocItemID=OpportunityBizDocItems.numOppBizDocItemID 
				WHERE 
					TimeAndExpense.numDomainId=@numDomainID 
					AND TimeAndExpense.numCategoryHDRID=@numCategoryHDRID)
	BEGIN
		RAISERROR('BIZDOC_EXISTS',16,1)
		RETURN
	END
	
	IF EXISTS (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainID AND numCategoryHDRID=@numCategoryHDRID)
	BEGIN
		RAISERROR('JOURNAL_ENTRIES_EXISTS',16,1)
		RETURN
	END

	DELETE FROM TimeAndExpense WHERE numDomainID=@numDomainID AND numCategoryHDRID=@numCategoryHDRID
END