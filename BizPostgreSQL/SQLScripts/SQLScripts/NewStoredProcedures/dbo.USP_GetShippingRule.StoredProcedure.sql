GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingRule' ) 
    DROP PROCEDURE USP_GetShippingRule
GO
CREATE PROCEDURE USP_GetShippingRule
    @numRuleID NUMERIC(9),
    @numDomainID NUMERIC(9),
    @byteMode AS TINYINT,
	@numRelProfileID AS VARCHAR(50)
AS 
BEGIN
    IF @byteMode = 0 
        BEGIN  
            SELECT  numRuleID
                    ,vcRuleName
                    ,vcDescription
                    ,tintBasedOn
                    ,tinShippingMethod
                    ,tintIncludeType
                    ,tintTaxMode
                    ,CASE WHEN tintFixedShippingQuotesMode = 0 THEN 1
                             ELSE tintFixedShippingQuotesMode END 
						AS tintFixedShippingQuotesMode
					,bitFreeShipping
					,FreeShippingOrderAmt
					,numRelationship
					,numProfile
					,numWareHouseID
					,numSiteID
					,bitItemClassification
                    
            FROM    dbo.ShippingRules SR
            WHERE   ( numRuleID = @numRuleID
                      OR @numRuleID = 0
                    )
                    AND numDomainID = @numDomainID
        END
    ELSE IF @byteMode = 1 
    BEGIN 
		DECLARE @numRelationship AS NUMERIC
		DECLARE @numProfile AS NUMERIC
				
		SET @numRelationship=(parsename(replace(@numRelProfileID,'-','.'),2))
		SET @numProfile=(parsename(replace(@numRelProfileID,'-','.'),1))

		SELECT minShippingCost,bitEnableStaticShippingRule,bitEnableShippingExceptions FROM Domain WHERE numDomainID = @numDomainID

		IF @numRelationship > 0 AND @numProfile > 0
		BEGIN
			   SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
                        WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
                        WHEN tintTaxMode = 3 THEN 'Always Tax'
                        WHEN tintTaxMode = 0 THEN 'Do Not Tax'
                        ELSE 'Tax Mode Not Selected'
                END vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode= 1 THEN 'Flat rate per item'
                        WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
                        WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
                        WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
                        WHEN tintFixedShippingQuotesMode= 0 THEN 'Flat rate per item'
                        ELSE 'Fixed rate shipping quotes not selected'
                END  vcFixedShippingQuotesMode
				,(SELECT STUFF((SELECT ', ' + W.vcWareHouse
					FROM   Warehouses W			
					WHERE   W.numWareHouseID  in  ( SELECT  Id
							FROM    dbo.SplitIDs(SR.numWareHouseID, ','))			
					FOR XML PATH ('')),1,2,'')) AS vcShipFrom
                , (SELECT STUFF((SELECT ', ' + dbo.fn_GetState(numStateID) + ' ,' + SS.vcZipPostal
					FROM ShippingRuleStateList SS
					join ShippingRules S on ss.numRuleID = s.numRuleID
					WHERE SR.numRuleID = SS.numRuleID			
					FOR XML PATH ('')),1,2,'')) AS vcShipTo

					,(CASE WHEN ISNULL(SR.bitFreeShipping,0) = 1
						THEN  (SELECT CONCAT ( (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) , ' , Free shipping when order amount reaches $' , SR.FreeShippingOrderAmt))
					ELSE (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) 
					END ) AS vcShippingCharges
				, (SELECT CONCAT( (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
					WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID) , ', ' , (SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
					WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID))) AS vcAppliesTo	
                        
        FROM    dbo.ShippingRules SR
        WHERE  
                numDomainID = @numDomainID AND numRelationship = @numRelationship AND numprofile = @numProfile
		END

		ELSE
		BEGIN
			   SELECT  numRuleID,
                vcRuleName,
                tinShippingMethod,
                CASE WHEN tintTaxMode = 1 THEN 'Do Not Tax'
                        WHEN tintTaxMode = 2 THEN 'Tax when taxable items exists'
                        WHEN tintTaxMode = 3 THEN 'Always Tax'
                        WHEN tintTaxMode = 0 THEN 'Do Not Tax'
                        ELSE 'Tax Mode Not Selected'
                END vcTaxMode,
                CASE WHEN tintFixedShippingQuotesMode= 1 THEN 'Flat rate per item'
                        WHEN tintFixedShippingQuotesMode = 2 THEN 'Ship by weight'
                        WHEN tintFixedShippingQuotesMode = 3 THEN 'Ship by order total'
                        WHEN tintFixedShippingQuotesMode = 4 THEN 'Flat rate per order'
                        WHEN tintFixedShippingQuotesMode= 0 THEN 'Flat rate per item'
                        ELSE 'Fixed rate shipping quotes not selected'
                END  vcFixedShippingQuotesMode

				,(SELECT STUFF((SELECT ', ' + W.vcWareHouse
					FROM   Warehouses W			
					WHERE   W.numWareHouseID  in  ( SELECT  Id
							FROM    dbo.SplitIDs(SR.numWareHouseID, ','))			
					FOR XML PATH ('')),1,2,'')) AS vcShipFrom

                ,  
					STUFF((SELECT  ', ' + dbo.fn_GetListName(numCountryID,0) + ' - ' + 
						STUFF((SELECT ', ' + ISNULL(dbo.fn_GetState(numStateID),'All States') +
								(CASE WHEN LEN( STUFF((SELECT ',' + s.vcZipPostal From ShippingRuleStateList s
														WHERE SR.numRuleID = s.numRuleID AND s.numCountryID =SRSL.numCountryID and s.numStateID = SRSL.numStateID FOR XML PATH ('')),1,1,'')) > 0
						THEN CONCAT (' (',STUFF((SELECT ',' + s.vcZipPostal From ShippingRuleStateList s
															WHERE SR.numRuleID = s.numRuleID AND s.numCountryID =SRSL.numCountryID and s.numStateID = SRSL.numStateID FOR XML PATH ('')),1,1,''), ') ')
									ELSE ''
								END)
						From ShippingRuleStateList SRSL
						WHERE SR.numRuleID = SRSL.numRuleID AND SRSL.numCountryID =SS.numCountryID GROUP BY SRSL.numStateID,numCountryID FOR XML PATH ('')),1,1,'')
										
					FROM ShippingRuleStateList SS					
					WHERE SR.numRuleID = SS.numRuleID
					GROUP BY numCountryID
					FOR XML PATH ('')),1,1,'') AS vcShipTo

				,(CASE WHEN ISNULL(SR.bitFreeShipping,0) = 1
						THEN  (SELECT CONCAT ( (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $',  CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) , ' , Free shipping when order amount reaches $' , SR.FreeShippingOrderAmt))
					ELSE (SELECT STUFF((SELECT CONCAT (', ' , intfrom , ' - ', intTo, ' pays $', CAST(monRate AS DECIMAL(20,2)))
								FROM ShippingServiceTypes SS
								join ShippingRules S on ss.numRuleID = s.numRuleID
								WHERE SR.numRuleID = SS.numRuleID			
								FOR XML PATH ('')),1,2,'')) 
					END ) AS vcShippingCharges
				,(SELECT CONCAT(ISNULL((SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
					WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID),'All Relationships') , ', ' ,ISNULL((SELECT vcData from ListDetails LD 
					JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
					WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID),'All Profiles'))) AS vcAppliesTo	
                         
        FROM    dbo.ShippingRules SR
        WHERE  
                numDomainID = @numDomainID  
		END

    END 
      ELSE IF @byteMode = 2 --It is for calculating Tax on Shipping Charge (Use it later on Order Summery Page)
            BEGIN
				SELECT tintTaxMode FROM ShippingRules WHERE numRuleID = @numRuleID AND numDomainID = @numDomainID
			END
END