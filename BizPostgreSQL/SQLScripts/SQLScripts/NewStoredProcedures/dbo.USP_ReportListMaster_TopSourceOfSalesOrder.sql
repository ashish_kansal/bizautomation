SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_TopSourceOfSalesOrder')
DROP PROCEDURE USP_ReportListMaster_TopSourceOfSalesOrder
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_TopSourceOfSalesOrder]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcDealAmount VARCHAR(MAX)
AS
BEGIN 
	DECLARE @TotalSalesOrderCount AS NUMERIC(18,4)

	SET @vcDealAmount=ISNULL(@vcDealAmount,'')

	SELECT DISTINCT
		@TotalSalesOrderCount = COUNT(*)
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=1 
		AND tintOppStatus=1 
		AND bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,tintSource VARCHAR(100)
		,bitPartner BIT
		,numPartner NUMERIC(18,0)
		,vcSource VARCHAR(300)
		,TotalOrdersCount INT
		,TotalOrdersPercent NUMERIC(18,2)
		,TotalOrdersAmount DECIMAL(20,5)
	)

	INSERT INTO @TEMP
	(
		tintSource
		,vcSource
		,bitPartner
		,numPartner
	)
	SELECT
		'0~0'
		,'-'
		,0
		,0
	UNION
	SELECT 
		'0~1'
		,'Internal Order'
		,0
		,0
	UNION
	SELECT 
		CONCAT(numSiteID,'~2')
		,vcSiteName
		,0
		,0
	FROM 
		Sites 
	WHERE 
		numDomainID = @numDomainID
	UNION
	SELECT DISTINCT 
		CONCAT(WebApiId,'~3')
		,vcProviderName
		,0
		,0
	FROM 
		dbo.WebAPI
	UNION
	SELECT 
		CONCAT(Ld.numListItemID,'~1')
		,ISNULL(vcRenamedListName,vcData)
		,0
		,0 
	FROM 
		ListDetails Ld  
	LEFT JOIN 
		listorder LO 
	ON 
		Ld.numListItemID= LO.numListItemID 
		AND Lo.numDomainId = @numDomainID 
	WHERE 
		Ld.numListID=9 
		AND (constFlag=1 OR Ld.numDomainID=@numDomainID)  
	UNION
	SELECT DISTINCT
		''
		,''
		,1
		,numPartner
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=1 
		AND tintOppStatus=1
		AND ISNULL(numPartner,0) > 0
		AND bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @tintSource AS VARCHAR(100)
	DECLARE @bitPartner BIT
	DECLARE @numPartner NUMERIC(18,0)
	DECLARE @TotalSalesOrderBySource AS INT
	DECLARE @TotalSalesOrderAmountBySource AS DECIMAL(20,5)


	SELECT @iCount = COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT @tintSource=ISNULL(tintSource,''),@bitPartner=bitPartner,@numPartner=numPartner FROM @TEMP WHERE ID=@i

		SELECT 
			@TotalSalesOrderBySource = COUNT(numOppId)
		FROM 
			OpportunityMaster
		WHERE 
			numDomainId=@numDomainID 
			AND tintOppType=1
			AND tintOppStatus=1
			AND 1 = (CASE 
						WHEN @bitPartner=1 
						THEN CASE WHEN numPartner=@numPartner THEN 1 ELSE 0 END
						ELSE CASE WHEN CONCAT(ISNULL(tintSource,0),'~',ISNULL(tintSourceType,0))=@tintSource AND ISNULL(numPartner,0)=0 THEN 1 ELSE 0 END
					END)
			AND bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())

		SELECT 
			@TotalSalesOrderAmountBySource = SUM(monTotAmount)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE 
			OM.numDomainId=@numDomainID 
			AND I.numDomainID = @numDomainID
			AND tintOppType=1 
			AND tintOppStatus=1
			AND 1 = (CASE 
						WHEN @bitPartner=1 
						THEN CASE WHEN numPartner=@numPartner THEN 1 ELSE 0 END
						ELSE CASE WHEN CONCAT(ISNULL(tintSource,0),'~',ISNULL(tintSourceType,0))=@tintSource AND ISNULL(numPartner,0)=0 THEN 1 ELSE 0 END
					END)
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			--
			AND ( 1 = (CASE WHEN CHARINDEX('1-5K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 1000 AND 5000 THEN 1 ELSE 0 END) ELSE 0 END)
					OR 1 = (CASE WHEN CHARINDEX('5-10K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 5000 AND 10000 THEN 1 ELSE 0 END) ELSE 0 END)
					OR 1 = (CASE WHEN CHARINDEX('10-20K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 10000 AND 20000 THEN 1 ELSE 0 END) ELSE 0 END)
					OR 1 = (CASE WHEN CHARINDEX('20-50K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount BETWEEN 20000 AND 50000 THEN 1 ELSE 0 END) ELSE 0 END)
					OR 1 = (CASE WHEN CHARINDEX('>50K',@vcDealAmount) > 0 THEN (CASE WHEN monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))

		UPDATE
			@TEMP
		SET
			vcSource = (CASE 
						WHEN @bitPartner=1 
						THEN ISNULL((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE DM.numDomainID=@numDomainID AND numDivisionID=@numPartner),'-')
						ELSE vcSource
					END)
			,TotalOrdersPercent = (@TotalSalesOrderBySource * 100)/ISNULL(@TotalSalesOrderCount,1)
			,TotalOrdersAmount = ISNULL(@TotalSalesOrderAmountBySource,0)
			,TotalOrdersCount = @TotalSalesOrderBySource
		WHERE
			ID=@i

		SET @i = @i + 1
	END

	SELECT vcSource,TotalOrdersPercent,TotalOrdersAmount FROM @TEMP WHERE ISNULL(TotalOrdersPercent,0) > 0 ORDER BY TotalOrdersPercent DESC
END
GO
