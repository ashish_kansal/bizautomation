SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AssembledItem_Delete')
DROP PROCEDURE USP_AssembledItem_Delete
GO
CREATE PROCEDURE [dbo].[USP_AssembledItem_Delete]  
	@ID NUMERIC(18,0),
	@numQty AS NUMERIC(18,0)
AS
BEGIN
	UPDATE AssembledItem SET numDisassembledQty=ISNULL(numDisassembledQty,0) + @numQty  WHERE ID=@ID

	IF (SELECT COUNT(*) FROM AssembledItem WHERE ID=@ID AND numAssembledQty=ISNULL(numDisassembledQty,0)) > 0
	BEGIN
		DELETE FROM AssembledItem WHERE ID=@ID
	END
END