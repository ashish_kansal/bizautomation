SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
-- USP_GetOpportunitySource 1
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpportunitySource')
DROP PROCEDURE USP_GetOpportunitySource 
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunitySource]
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @vcMarketPlaces VARCHAR(500)

	SELECT @vcMarketPlaces = ISNULL(vcMarketplaces,'') FROM Domain WHERE numDomainId=@numDomainID

	CREATE TABLE #temp 
	(
		numItemID VARCHAR(50)
		,vcItemName VARCHAR(100)
		,constFlag BIT
		,bitDelete BIT
		,intSortOrder INT
		,numListItemID NUMERIC(9)
		,vcData VARCHAR(100)
		,vcListType VARCHAR(1000)
		,vcOrderType VARCHAR(50)
		,vcOppOrOrder VARCHAR(20)
		,numListType NUMERIC(18,0)
		,tintOppOrOrder TINYINT
		,bitEnforceMinOrderAmount BIT
		,fltMinOrderAmount DECIMAL(20,5)
		,vcOriginalBizDocName VARCHAR(100)
		,numListItemGroupId NUMERIC(18,0)
		,vcListItemGroupName VARCHAR(100)
		,vcColorScheme VARCHAR(100)
		,tintSourceType TINYINT
	)

	INSERT INTO 
		#temp
	SELECT 
		'0~1','Internal Order',1 AS constFlag,0 AS bitDelete,0 intSortOrder,-1 AS numListItemID,'Internal Order','Internal Order','','',0,0,0,0,'',0,'','',1
	UNION
	SELECT 
		CAST(Ld.numListItemID  AS VARCHAR(250)) +'~1'
		,ISNULL(vcRenamedListName,vcData) AS vcData
		,Ld.constFlag,bitDelete
		,1 intSortOrder
		,CAST(Ld.numListItemID  AS VARCHAR(18))
		,ISNULL(vcRenamedListName,vcData) AS vcData
		,(SELECT vcData FROM ListDetails WHERE numListId=9 AND numListItemId = ld.numListType AND [ListDetails].[numDomainID] = @numDomainID)
		,(CASE when ld.numListType=1 then 'Sales' when ld.numListType=2 then 'Purchase' else 'All' END)
		,(CASE WHEN ld.tintOppOrOrder=1 THEN 'Opportunity' WHEN ld.tintOppOrOrder=2 THEN 'Order' ELSE 'All' END)
		,numListType
		,tintOppOrOrder
		,0
		,0
		,''
		,ISNULL(numListItemGroupId,0)
		,''
		,''
		,1
	FROM 
		ListDetails Ld  
	LEFT JOIN 
		ListOrder LO 
	ON 
		Ld.numListItemID=LO.numListItemID 
		AND Lo.numDomainId = @numDomainID 
	WHERE 
		Ld.numListID=9 AND (constFlag=1 OR Ld.numDomainID=@numDomainID)
	UNION
	SELECT 
		CONCAT(numSiteID,'~2')
		,[vcSiteName]
		,1 AS constFlag
		,0 AS bitDelete
		,2 intSortOrder
		,CAST([numSiteID] AS VARCHAR(18)) AS numListItemID
		,[vcSiteName]
		,[Sites].[vcSiteName]
		,''
		,''
		,0
		,0
		,0
		,0
		,''
		,0
		,''
		,''
		,2
	FROM
		Sites
	WHERE 
		[numDomainID] = @numDomainID
	UNION
	SELECT DISTINCT 
		CONCAT(WebApiId,'~3')
		,vcProviderName
		,1 constFlag
		,0 bitDelete
		,3 intSortOrder
		,CAST(WebApiId AS VARCHAR(18)) AS numListItemID
		,vcProviderName
		,[WebAPI].[vcProviderName]
		,''
		,''
		,0
		,0
		,0
		,0
		,''
		,0
		,''
		,''
		,3
	FROM 
		dbo.WebAPI
	UNION
	SELECT DISTINCT 
		CONCAT(eChannelHub.ID,'~4')
		,vcMarketplace
		,1 constFlag
		,0 bitDelete
		,4 intSortOrder
		,CAST(eChannelHub.ID AS VARCHAR(18)) AS numListItemID
		,vcMarketplace
		,''
		,''
		,''
		,0
		,0
		,0
		,0
		,''
		,0
		,''
		,''
		,3
	FROM 
		dbo.eChannelHub
	INNER JOIN
	(
		SELECT
			Id
		FROM
			dbo.SplitIDs(@vcMarketPlaces,',')
	) TEMP
	ON
		eChannelHub.ID = TEMP.Id
		 
	SELECT * FROM #temp ORDER BY intSortOrder,vcItemName
	
	DROP TABLE #temp		 	
END
GO