GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderDetailForBluePayHPF')
DROP PROCEDURE USP_GetOrderDetailForBluePayHPF
GO
CREATE PROCEDURE [dbo].[USP_GetOrderDetailForBluePayHPF]
(
	@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
)
AS
BEGIN	
	SELECT
		OpportunityMaster.numDomainId
		,OpportunityMaster.numOppId
		,OpportunityMaster.numDivisionId
		,OpportunityMaster.numContactId
		,OpportunityMaster.numCreatedBy
		,OpportunityMaster.numCurrencyID
		,ISNULL(OpportunityMaster.fltExchangeRate,1) fltExchangeRate
		,Domain.numCurrencyID AS numBaseCurrencyID
		,ISNULL(vcBluePaySuccessURL,'') vcBluePaySuccessURL
		,ISNULL(vcBluePayDeclineURL,'') vcBluePayDeclineURL
	FROM
		OpportunityMaster
	INNER JOIN
		Domain
	ON
		OpportunityMaster.numDomainId = Domain.numDomainId
	INNER JOIN
		OpportunityBizDocs
	ON
		OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
	WHERE
		OpportunityMaster.numOppId=@numOppID		
		AND OpportunityBizDocs.numOppBizDocsId = @numOppBizDocID
END
GO