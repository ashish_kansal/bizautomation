GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecurringBillRemaining')
DROP PROCEDURE USP_RecurringBillRemaining
GO
CREATE PROCEDURE USP_RecurringBillRemaining
@numOppID NUMERIC ,
@numDomainID NUMERIC,
@numOppRecID NUMERIC=0 OUTPUT
AS 
BEGIN

IF EXISTS(SELECT * FROM dbo.OpportunityRecurring
	WHERE numOppId=@numOppID AND numDomainID=@numDomainID AND ISNULL(numOppBizDocID,0)>0 )
	BEGIN
	
		DELETE FROM dbo.OpportunityRecurring WHERE numOppId=@numOppID AND numDomainID=@numDomainID
		AND ISNULL(numOppBizDocID,0)=0
	
		DECLARE @newPercentage FLOAT 
		SELECT @newPercentage=(100-SUM(fltBreakupPercentage)) FROM dbo.OpportunityRecurring
		WHERE numOppId=@numOppID AND numDomainID=@numDomainID
		
		IF @newPercentage > 0.0001
		BEGIN
			INSERT INTO dbo.OpportunityRecurring (
				numOppId,
				numOppBizDocID,
				numRecurringId,
				tintRecurringType,
				dtRecurringDate,
				bitRecurringZeroAmt,
				numNoTransactions,
				bitBillingTerms,
				numBillingDays,
				fltBreakupPercentage,
				numDomainID
			) 
			SELECT TOP 1
				   numOppId,
				   NULL,
				   numRecurringId,
				   tintRecurringType,
				   GETDATE(),
				   bitRecurringZeroAmt,
				   numNoTransactions,
				   bitBillingTerms,
				   numBillingDays,
				   @newPercentage,
				   numDomainID 
			FROM 
			dbo.OpportunityRecurring WHERE numOppId=@numOppID AND numDomainID=@numDomainID	
			
			SET @numOppRecID = SCOPE_IDENTITY()	
		END
		ELSE 
			BEGIN
				SET @numOppRecID = 0	
			END
	END
END