GO
/****** Object:  StoredProcedure [dbo].[USP_DELETE_TOPICMASTER]    Script Date: 17/07/2020 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DELETE_TOPICMASTER' ) 
    DROP PROCEDURE USP_DELETE_TOPICMASTER
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_DELETE_TOPICMASTER 
(
	@numTopicId	AS	NUMERIC(18,0)=0,
	@numDomainID AS NUMERIC(18,0)=0
)	
AS 
BEGIN
IF(@numTopicId != 0)
	BEGIN
	DELETE FROM dbo.MessageMaster WHERE numTopicId=@numTopicId AND numDomainId=@numDomainID
	DELETE FROM dbo.TOPICMASTER WHERE numTopicId = @numTopicId 	AND numDomainID=@numDomainID
	END
END

	