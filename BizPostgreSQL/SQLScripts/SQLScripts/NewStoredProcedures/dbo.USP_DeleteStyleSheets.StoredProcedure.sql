
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteStyleSheets]    Script Date: 08/08/2009 15:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteStyleSheets')
DROP PROCEDURE USP_DeleteStyleSheets
GO
CREATE PROCEDURE [dbo].[USP_DeleteStyleSheets]
	@numCssID NUMERIC(9),
	@numDomainId NUMERIC(9)
AS
BEGIN
IF (select COUNT(*) FROM StyleSheetDetails WHERE numCssID = @numCssID) = 0
	DELETE FROM StyleSheets
	WHERE [numCssID] = @numCssID AND numDomainID = @numDomainId
ELSE
    RAISERROR('Dependant',16,1)
END