SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_EmployeeSalesPerformance1')
DROP PROCEDURE USP_ReportListMaster_EmployeeSalesPerformance1
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_EmployeeSalesPerformance1]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,@vcFilterValue VARCHAR(MAX)
AS
BEGIN 

	DECLARE @dtCreatedDate DATETIME
	DECLARE @dtEndDate AS DATETIME = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @dtCreatedDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @dtCreatedDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @dtCreatedDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @dtCreatedDate = DATEADD(DAY, DATEDIFF(DAY, -30, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @dtCreatedDate = DATEADD(DAY, DATEDIFF(DAY, -7, @dtEndDate), 0)
	END

	DECLARE @TempFinal TABLE
	(
		vcUsername VARCHAR(300)
		,LeadsToAccount FLOAT
		,LeadsToProspect FLOAT
		,ProspectToAccount FLOAT
	)

	DECLARE @TEMPEmployees TABLE
	(
		ID INT IDENTITY(1,1)
		,numAssignedTo NUMERIC(18,0)
	)

	INSERT INTO @TEMPEmployees
	(
		numAssignedTo
	)
	SELECT DISTINCT
		numAssignedTo
	FROM
		DivisionMaster
	WHERE
		numDomainID=@numDomainID
		AND bintCreatedDate >= @dtCreatedDate


	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,numAssignedTo NUMERIC(18,0)
		,FirstRecordID NUMERIC(18,0)
		,LastRecordID NUMERIC(18,0)
		,tintFirstCRMType TINYINT
		,tintLastCRMType TINYINT
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,numAssignedTo
		,FirstRecordID
		,LastRecordID
	)
	SELECT 
		DivisionMasterPromotionHistory.numDivisionID
		,DM.numAssignedTo
		,MIN(ID)
		,MAX(ID)
	FROM 
		DivisionMasterPromotionHistory 
	INNER JOIN
		DivisionMaster DM
	ON
		DivisionMasterPromotionHistory.numDivisionID = DM.numDivisionID
	WHERE 
		DivisionMasterPromotionHistory.numDomainID=@numDomainID 
		AND DM.numDomainID = @numDomainID
		AND DM.bintCreatedDate >= @dtCreatedDate
	GROUP BY
		DivisionMasterPromotionHistory.numDivisionID
		,DM.numAssignedTo

	UPDATE
		T1
	SET
		tintFirstCRMType = DMPH1.tintPreviousCRMType
		,tintLastCRMType = DMPH2.tintNewCRMType
	FROM
		@TEMP T1
	INNER JOIN
		DivisionMasterPromotionHistory DMPH1
	ON
		T1.FirstRecordID = DMPH1.ID
	INNER JOIN
		DivisionMasterPromotionHistory DMPH2
	ON
		T1.LastRecordID = DMPH2.ID


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT 
	DECLARE @TotalLeads FLOAT
	DECLARE @LeadsToProspect FLOAT
	DECLARE @LeadsToAccount FLOAT
	DECLARE @TotalProspects FLOAT
	DECLARE @ProspectToAccount FLOAT

	DECLARE @TempAssignedTo NUMERIC(18,0)
	SELECT @iCount=COUNT(*) FROM @TEMPEmployees

	WHILE @i <= @iCount
	BEGIN
		SELECT @TempAssignedTo=numAssignedTo FROM @TEMPEmployees WHERE ID=@i

		SET @TotalLeads = ISNULL((SELECT 
								COUNT(*) 
							FROM 
								DivisionMaster 
							WHERE 
								numDomainID=@numDomainID 
								AND numAssignedTo=@TempAssignedTo
								AND bintCreatedDate >= @dtCreatedDate
								AND tintCRMType=0 
								AND numDivisionID NOT IN (SELECT numDivisionID FROM @TEMP)),0) + ISNULL((SELECT COUNT(*) FROM @TEMP WHERE tintFirstCRMType=0 AND numAssignedTo=@TempAssignedTo),0)


		SET @LeadsToProspect = ISNULL((SELECT COUNT(*) FROM @TEMP WHERE tintFirstCRMType=0 ANd tintLastCRMType=1 AND numAssignedTo=@TempAssignedTo),0)
		SET @LeadsToAccount = ISNULL((SELECT COUNT(*) FROM @TEMP WHERE tintFirstCRMType=0 ANd tintLastCRMType=2 AND numAssignedTo=@TempAssignedTo),0)


		SET @TotalProspects = ISNULL((SELECT 
										COUNT(*) 
									FROM 
										DivisionMaster 
									WHERE 
										numDomainID=@numDomainID 
										AND numAssignedTo=@TempAssignedTo
										AND bintCreatedDate >= @dtCreatedDate 
										AND tintCRMType=1
										AND numDivisionID NOT IN (SELECT numDivisionID FROM @TEMP)),0) + ISNULL((SELECT COUNT(*) FROM @TEMP WHERE tintFirstCRMType=1 AND numAssignedTo=@TempAssignedTo),0)

		SET @ProspectToAccount = ISNULL((SELECT COUNT(*) FROM @TEMP WHERE tintFirstCRMType=1 ANd tintLastCRMType=2 AND numAssignedTo=@TempAssignedTo),0)
	

		INSERT INTO @TempFinal
		(
			vcUsername
			,LeadsToAccount
			,LeadsToProspect
			,ProspectToAccount
		)
		SELECT
			vcUserName
			,(@LeadsToAccount * 100) / (CASE WHEN ISNULL(@TotalLeads,0) = 0 THEN 1 ELSE @TotalLeads END)
			,(@LeadsToProspect * 100) / (CASE WHEN ISNULL(@TotalLeads,0) = 0 THEN 1 ELSE @TotalLeads END)
			,(@ProspectToAccount * 100) / (CASE WHEN ISNULL(@TotalProspects,0) = 0 THEN 1 ELSE @TotalProspects END)
		FROM
			UserMaster

		JOIN
			userteams UT
		ON 
			UT.numDomainID=@numDomainId 
			AND UserMaster.numUserDetailId=UT.numUserCntID

		WHERE 
			UserMaster.numDomainID=@numDomainID
			AND UserMaster.numUserDetailId=@TempAssignedTo
			AND 1 = (CASE WHEN @vcFilterBy = 4  THEN (CASE WHEN UT.numUserCntID IN (SELECT Id FROM dbo.SplitIDs((@vcFilterValue),',')) THEN 1 ELSE 0 END) 
					ELSE 
				(CASE WHEN LEN(ISNULL(@vcFilterValue,'')) > 0 THEN (CASE WHEN UT.numTeam IN (SELECT Id FROM dbo.SplitIDs((@vcFilterValue),',')) THEN 1 ELSE 0 END) ELSE 1 END)
					END)

		SET @i = @i + 1
	END

	SELECT vcUsername,LeadsToProspect FROM @TempFinal WHERE ISNULL(LeadsToProspect,0.0) > 0
	SELECT vcUsername,LeadsToAccount FROM @TempFinal WHERE ISNULL(LeadsToAccount,0.0) > 0
	SELECT vcUsername,ProspectToAccount FROM @TempFinal WHERE ISNULL(ProspectToAccount,0.0) > 0
END
GO




