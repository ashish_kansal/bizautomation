SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_EmailReport')
DROP PROCEDURE dbo.USP_CustomQueryReport_EmailReport
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_EmailReport]

AS 
BEGIN
	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1),
		numReportID NUMERIC(18,0),
		numReportDomainID NUMERIC(18,0),
		vcReportName VARCHAR(300),
		vcEmailTo VARCHAR(1000),
		tintEmailFrequency TINYINT,
		vcCSS VARCHAR(MAX),
		dtCreatedDate DATETIME
	)	

	DECLARE @TempFinal TABLE
	(
		numReportID NUMERIC(18,0),
		numReportDomainID NUMERIC(18,0),
		vcReportName VARCHAR(300),
		vcEmailTo VARCHAR(1000),
		tintEmailFrequency TINYINT,
		vcCSS VARCHAR(MAX),
		dtDateToSend DATE
	)	

	INSERT INTO @Temp
	(
		numReportID,
		numReportDomainID,
		vcReportName,
		vcEmailTo,
		tintEmailFrequency,
		vcCSS,
		dtCreatedDate
	)
	SELECT
		numReportID,
		numDomainID,
		vcReportName,
		vcEmailTo,
		tintEmailFrequency,
		vcCSS,
		dtCreatedDate
	FROM	
		CustomQueryReport
	WHERE
		LEN(ISNULL(vcEmailTo,'')) > 0
		AND ISNULL(tintEmailFrequency,0) > 0

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT

	DECLARE @bitAddForEmail AS BIT = 0
	DECLARE @numReportID AS NUMERIC(18,0)
	DECLARE @numReportDomainID AS NUMERIC(18,0)
	DECLARE @dtCreatedDate AS DATE
	DECLARE @dtFiscalStartDate AS DATE
	DECLARE @tintEmailFrequency AS TINYINT
	DECLARE @dtDateToSend AS DATE
	

	SELECT @COUNT=COUNT(*) FROM @Temp

	WHILE @i <= @COUNT
	BEGIN
		SET @bitAddForEmail = 0

		SELECT 
			@numReportID=numReportID, 
			@numReportDomainID=numReportDomainID, 
			@tintEmailFrequency=tintEmailFrequency,
			@dtCreatedDate=CAST(dtCreatedDate AS DATE)
		FROM 
			@Temp 
		WHERE 
			ID=@i

		SELECT @dtFiscalStartDate = CAST(dbo.GetFiscalStartDate(YEAR(GETDATE()),@numReportDomainID) AS DATE)

		IF @tintEmailFrequency = 1 --DAILY
		BEGIN
			SELECT @dtDateToSend = CAST(GETDATE() AS DATE)

			IF (SELECT 
					COUNT(*) 
				FROM 
					CustomQueryReportEmail 
				WHERE 
					numReportID=@numReportID
					AND tintEmailFrequency=@tintEmailFrequency
					AND dtDateToSend = (CAST(GETDATE() AS DATE))) = 0
			BEGIN
				SET @bitAddForEmail = 1
			END
		END
		ELSE IF @tintEmailFrequency = 2 --WEEKLY
		BEGIN
			SELECT @dtDateToSend = CAST(dbo.get_week_start(GETDATE()) AS DATE)
			
			IF @dtCreatedDate < @dtDateToSend AND ((CAST(GETDATE() AS DATE) = @dtDateToSend) OR
				(SELECT 
					COUNT(*) 
				FROM 
					CustomQueryReportEmail 
				WHERE 
					numReportID=@numReportID
					AND tintEmailFrequency=@tintEmailFrequency
					AND dtDateToSend = @dtDateToSend) = 0)
			BEGIN
				SET @bitAddForEmail = 1
			END
		END
		ELSE IF @tintEmailFrequency = 3 --MONTHLY
		BEGIN
			SELECT @dtDateToSend = CAST(dbo.get_month_start(GETDATE()) AS DATE)

			IF @dtCreatedDate < @dtDateToSend AND ((CAST(GETDATE() AS DATE) = @dtDateToSend) OR
				(SELECT 
					COUNT(*) 
				FROM 
					CustomQueryReportEmail 
				WHERE 
					numReportID=@numReportID
					AND tintEmailFrequency=@tintEmailFrequency
					AND dtDateToSend = @dtDateToSend) = 0)
			BEGIN
				SET @bitAddForEmail = 1
			END
		END
		ELSE IF @tintEmailFrequency = 4 --YEARLY
		BEGIN
			SELECT @dtDateToSend = @dtFiscalStartDate

			IF @dtCreatedDate < @dtDateToSend AND ((CAST(GETDATE() AS DATE) = @dtDateToSend) OR
				(SELECT 
					COUNT(*) 
				FROM 
					CustomQueryReportEmail 
				WHERE 
					numReportID=@numReportID
					AND tintEmailFrequency=@tintEmailFrequency
					AND dtDateToSend = @dtDateToSend) = 0)
			BEGIN
				SET @bitAddForEmail = 1
			END
		END

		IF @bitAddForEmail = 1
		BEGIN
			INSERT INTO 
				@TempFinal
			SELECT
				numReportID,
				numReportDomainID,
				vcReportName,
				vcEmailTo,
				tintEmailFrequency,
				ISNULL(vcCSS,''),
				@dtDateToSend
			FROM
				@Temp
			WHERE
				ID = @i
		END

		SET @i = @i + 1
	END

	SELECT * FROM @TempFinal
END