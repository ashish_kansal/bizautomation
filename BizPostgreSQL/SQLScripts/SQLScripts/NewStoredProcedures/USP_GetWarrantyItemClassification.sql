
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWarrantyItemClassification')
DROP PROCEDURE USP_GetWarrantyItemClassification
GO
CREATE PROCEDURE [dbo].[USP_GetWarrantyItemClassification]
@numDomainID AS NUMERIC(18,0)
AS
BEGIN
				SELECT 
								C.[vcItemClassification]
							FROM 
								Contracts AS C
							LEFT JOIN 
								DivisionMaster AS D
							ON
								C.numDivisonId=D.numDivisionID
							LEFT JOIN
								CompanyInfo AS CI
							ON
								D.numCompanyId=CI.numCompanyId
							WHERE
								C.numDomainID=@numDomainID AND
								C.intType=2
END