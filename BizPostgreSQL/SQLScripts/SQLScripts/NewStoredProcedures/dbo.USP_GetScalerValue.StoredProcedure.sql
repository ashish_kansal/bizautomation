GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetScalerValue')
DROP PROCEDURE USP_GetScalerValue
GO
CREATE PROCEDURE USP_GetScalerValue 
@numDomainID AS NUMERIC,--Will receive SiteID when mode =2,12,9
@tintMode AS TINYINT,
@vcInput AS VARCHAR(1000),
@numUserCntId AS NUMERIC(9)=0
AS
BEGIN
	IF @tintMode = 1
	BEGIN
		SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainId=@numDomainID and numBizDocsPaymentDetId= CONVERT(NUMERIC, @vcInput)
	END
	IF @tintMode = 2 
	BEGIN
		SELECT COUNT(*) FROM dbo.Sites WHERE vcHostName=@vcInput AND numSiteID <> @numDomainID 
	END
	IF @tintMode = 3 
	BEGIN
		SELECT COUNT(*) FROM dbo.Domain WHERE vcPortalName=@vcInput AND numDomainId <> @numDomainID
	END
	IF @tintMode = 4 --Item Name
	BEGIN
		SELECT dbo.GetItemName(@vcInput)
	END
	IF @tintMode = 5 --Company Name
	BEGIN
		SELECT dbo.fn_GetComapnyName(@vcInput)
	END
	IF @tintMode = 6 --Vendor Cost
	BEGIN
		SELECT dbo.fn_GetVendorCost(@vcInput)
	END
	IF @tintMode = 7 -- tintRuleFor
	BEGIN
		SELECT tintRuleFor FROM PriceBookRules WHERE numPricRuleID=@vcInput AND numDomainId =@numDomainId
	End
	IF @tintMode = 8 -- monSCreditBalance
	BEGIN
		SELECT isnull(monSCreditBalance,0) as monCreditBalance FROM DivisionMaster WHERE numDivisionID=@vcInput AND numDomainId =@numDomainId
	End
    IF @tintMode = 9 -- Check Site Page Exists
	BEGIN
		if exists(SELECT [vcPageName] FROM   SitePages WHERE  [numSiteID] = @numDomainId AND LOWER([vcPageURL]) = LOWER(@vcInput))
			SELECT 1 as SitePageExists
		else
			SELECT 0 as SitePageExists
	END
	IF @tintMode = 10 -- Category Name
	BEGIN
		SELECT ISNULL(vcCategoryName,'NA') FROM dbo.Category WHERE numCategoryID=CAST(@vcInput AS NUMERIC(9)) AND numDomainId =@numDomainId
	End
	IF @tintMode = 11 -- monPCreditBalance
	BEGIN
		SELECT isnull(monPCreditBalance,0) as monCreditBalance FROM DivisionMaster WHERE numDivisionID=@vcInput AND numDomainId =@numDomainId
	END
	 IF @tintMode = 12 -- Get Style ID from filename
	BEGIN
		SELECT TOP 1 ISNULL([numCssID],0) FROM  dbo.StyleSheets WHERE  [numSiteID] = @numDomainId AND LOWER([styleFileName]) = LOWER(@vcInput)
	END
	IF @tintMode = 13 -- numItemClassification
	BEGIN
		SELECT ISNULL(numItemClassification,0) as numItemClassification FROM  Item WHERE  numDomainID = @numDomainId AND numItemCode = @vcInput
	END
	IF @tintMode = 14 -- numAssignedTo
	BEGIN
		SELECT ISNULL(numAssignedTo,0) as numAssignedTo FROM  DivisionMaster WHERE  numDomainID = @numDomainId AND numDivisionID = @vcInput
	END
	IF @tintMode = 15 -- Get Currency ID 
	BEGIN
		SELECT numCurrencyID FROM Currency WHERE numDomainID = @numDomainID AND chrCurrency = @vcInput
	END
	IF @tintMode = 16 -- Total Order of Items where Warehouse exists
	BEGIN
		SELECT count(*) as TotalRecords FROM OpportunityMaster OM join OpportunityItems OI on OM.numOppId=OI.numOppId
			 WHERE OI.numItemCode = @vcInput AND OM.numDomainID = @numDomainID and OI.numWarehouseItmsID>0
	END
	IF @tintMode = 17 -- Get numBizDocID
	BEGIN
		SELECT numBizDocTempID FROM dbo.BizDocTemplate 
								WHERE numDomainID = @numDomainID
								  AND tintTemplateType = 0
								  AND bitDefault = 1
								  AND bitEnabled = 1
								  AND numBizDocID = @vcInput
	END
	IF @tintMode = 18 -- Get Domain DivisionID
	BEGIN
		SELECT numDivisionID FROM Domain WHERE numDomainID = @numDomainID
	END
	IF @tintMode = 19
	BEGIN
		SELECT numItemID FROM ItemAPI WHERE numDomainID = @numDomainID AND  vcAPIItemID = @vcInput
	END
	IF @tintMode = 20
	BEGIN
		SELECT numItemCode FROM Item WHERE numDomainID = @numDomainID AND  vcSKU = @vcInput
	END
	IF @tintMode = 21
	BEGIN
		SELECT numStateId FROM dbo.State S inner JOIN webApiStateMapping SM
		ON S.vcState =  SM.vcStateName 
		AND S.numDomainId = @numDomainId 
		AND ( LOWER(SM.vcAmazonStateCode) = LOWER(@vcInput) OR LOWER(SM.vcStateName) = LOWER(@vcInput))
	END
	IF @tintMode = 22
	BEGIN
		SELECT LD.numListItemId FROM ListDetails LD INNER Join WebApiCountryMapping CM ON 
		LD.vcData = CM.vcCountryName WHERE LD.numListId = 40 AND LD.numDomainId = @numDomainId AND
		CM.vcAmazonCountryCode = @vcInput
	END
   IF @tintMode = 23
   BEGIN
       select isnull(txtSignature,'') from userMaster where numUserDetailId = convert(numeric , @vcInput) and numDomainId = @numDomainId 
   END
   
   IF @tintMode = 24
   BEGIN
         DECLARE @dtEmailStartDate AS varchar(20),
                 @dtEmailEndDate AS varchar(20) ,
                 @intNoOfEmail AS numeric
            
        select @dtEmailStartDate =convert(varchar(20) , dtEmailStartDate) ,@dtEmailEndDate =convert(varchar(20), dtEmailEndDate), @intNoOfEmail = intNoOfEmail from Subscribers where numTargetDomainID=@numDomainID
       
        IF  Convert(datetime, Convert(int, GetDate())) >=  @dtEmailStartDate AND Convert(datetime, Convert(int, GetDate())) <=  @dtEmailEndDate  
            BEGIN
		          select  (convert(varchar(10) , count(*)) + '-' +(select top 1 convert(varchar(10), intNoOfEmail) from Subscribers where numTargetDomainID = @numDomainID)) as EmailData
                  from broadcastDTLs BD inner join BroadCast B on B.numBroadCastID = BD.numBroadCastID 
                  where B.numDomainId = @numDomainId AND bintBroadCastDate between @dtEmailStartDate and @dtEmailEndDate and bitBroadcasted = 1 and tintSucessfull = 1     		
			END       
   END  
   IF @tintMode = 25
	BEGIN
		SELECT IsNull(vcMsg,'') AS vcMsg FROM dbo.fn_GetEmailAlert(@numDomainId ,@vcInput,@numUserCntId)
	END
	IF @tintMode = 26
	BEGIN
		SELECT COUNT(*) AS Total FROM dbo.Communication WHERE numDomainID = @numDomainID and numContactId = @vcInput
	END 
	IF @tintMode = 27 -- Check Order Closed
	BEGIN
		SELECT ISNULL(tintshipped,0) AS tintshipped FROM OpportunityMaster  
			 WHERE numOppId = convert(numeric , @vcInput) AND numDomainID = @numDomainID
	END
	IF @tintMode = 28 -- General Journal Header for Bank Recon. Adjustments
	BEGIN
		SELECT ISNULL(numJournal_Id,0) AS numJournal_Id FROM General_Journal_Header  
			 WHERE numReconcileID = convert(numeric , @vcInput) AND numDomainID = @numDomainID AND ISNULL(bitReconcileInterest,0) = 0
	END
	IF @tintMode = 29 -- General Journal Header for Write Check
	BEGIN
		SELECT ISNULL(numJournal_Id,0) AS numJournal_Id FROM General_Journal_Header  
			 WHERE numCheckHeaderID = convert(numeric , @vcInput) AND numDomainID = @numDomainID
	END
	
	IF @tintMode = 30 -- Get Units for sample item to be created
	BEGIN
	 SELECT ISNULL(u.numUOMId,0) FROM 
        UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
        WHERE u.numDomainID=@numDomainID 
        AND d.numDomainID=@numDomainID 
        AND u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)
        AND u.vcUnitName ='Units'
	END
	
	IF @tintMode = 31 -- Get Magento Attribute Set for Current Domain
	BEGIN
		SELECT vcEighthFldValue FROM dbo.WebAPIDetail WHERE numDomainId = @numDomainID  AND webApiId = @vcInput
	END
	
	IF @tintMode = 32 -- Get WareHouseItemID for ItemID is existis for Current Domain
	BEGIN
		 SELECT ISNULL((SELECT TOP 1 numWareHouseItemID FROM  dbo.WareHouseItems WHERE numDomainId = @numDomainID  AND numItemID = @vcInput),0)AS  numWareHouseItemID
	END
	
	IF @tintMode = 33
	BEGIN
	SELECT ISNULL(MAX(CAST(ISNULL(numSequenceId,0) AS BigInt)),0) + 1 FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId
			 WHERE OM.numDomainId=@numDomainID AND OBD.numBizDocId=@vcInput
	END	
	
	IF @tintMode = 34
	BEGIN
	SELECT ISNULL(vcOppRefOrderNo,'') FROM dbo.OpportunityMaster OM 
			 WHERE OM.numDomainId=@numDomainID AND OM.numOppID=@vcInput
	END
	
	IF @tintMode = 35
	BEGIN 
	SELECT ISNULL(bitAutolinkUnappliedPayment,0) FROM dbo.Domain  WHERE numDomainId=@numDomainID 
	END		 
	
	IF @tintMode = 36
	BEGIN 
		SELECT ISNULL(numJournalId,0) FROM dbo.General_Journal_Details WHERE numDomainId=@numDomainID AND numChartAcntId=@vcInput AND chBizDocItems='OE'
	END	
	
	IF @tintMode = 37
	BEGIN 
		SELECT ISNULL(numShippingServiceItemID,0) FROM dbo.Domain WHERE numDomainId=@numDomainID
	END	
	IF @tintMode = 38
	BEGIN
		DECLARE @numItemCode AS NUMERIC(18,0)
		
		--FIRST CHECK FOR ITEM LEVEL SKU
		SELECT TOP 1 @numItemCode=numItemCode FROM Item WHERE numDomainID = @numDomainID AND  vcSKU = @vcInput

		--IF ITEM NOT AVAILABLE WITH SKU CHECK FOR MATRIX ITEM (WAREHOUSE LEVEL SKU)
		IF ISNULL(@numItemCode,0) = 0
		BEGIN
			SELECT TOP 1 @numItemCode=numItemID FROM WarehouseItems WHERE numDomainID = @numDomainID AND vcWHSKU = @vcInput
		END

		SELECT CONVERT(VARCHAR(50),numItemCode)+'~'+CharItemType as numItemCode FROM Item WHERE numDomainID = @numDomainID AND numItemCode=@numItemCode
	END	
	IF @tintMode = 39 -- Get WareHouseItemID for SKU is exists for Current Domain
	BEGIN
	IF CHARINDEX('~',@vcInput) > 0
		BEGIN
			DECLARE @numItemID NUMERIC(18,0)
			DECLARE @vcSKU VARCHAR(50)
			SELECT @numItemID = strItem FROM dbo.SplitWordWithPosition(@vcInput,'~') WHERE RowNumber = 1
			SELECT @vcSKU = strItem FROM dbo.SplitWordWithPosition(@vcInput,'~') WHERE RowNumber = 2

			
			SELECT   ISNULL(( SELECT TOP 1
                            numWareHouseItemID
                     FROM   dbo.WareHouseItems
                     WHERE  numDomainId = @numDomainID
                            AND vcWHSKU = @vcSKU
                            AND numItemID = @numItemID
                   ), 0) AS numWareHouseItemID
		END
	ELSE
		BEGIN
			SELECT   ISNULL(( SELECT TOP 1
                            numWareHouseItemID
                     FROM   dbo.WareHouseItems
                     WHERE  numDomainId = @numDomainID
                            AND numItemID = @vcInput
                   ), 0) AS numWareHouseItemID
		END
	END
	
	IF @tintMode = 40 -- General Journal Header for Bank Recon Interest Service Charge.
	BEGIN
		SELECT ISNULL(numJournal_Id,0) AS numJournal_Id FROM General_Journal_Header  
			 WHERE numReconcileID = convert(numeric , @vcInput) AND numDomainID = @numDomainID AND ISNULL(bitReconcileInterest,0) = 1
	END 
	IF @tintMode = 41 -- Item On Hand
	BEGIN
		SELECT SUM(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @vcInput
	END 
END
--exec USP_GetScalerValue @numDomainID=1,@tintMode=24,@vcInput='0'
 