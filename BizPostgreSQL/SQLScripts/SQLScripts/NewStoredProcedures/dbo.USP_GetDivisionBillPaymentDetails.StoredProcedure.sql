SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDivisionBillPaymentDetails')
DROP PROCEDURE USP_GetDivisionBillPaymentDetails
GO
CREATE PROCEDURE [dbo].[USP_GetDivisionBillPaymentDetails](
               @numDomainId          AS NUMERIC(9)  = 0,
				@numDivisionId numeric(9),
               @ClientTimeZoneOffset AS INT
			    )
AS
  BEGIN
  
    SELECT oppbizdocspaydet.numbizdocspaymentdetailsid AS numbizdocspaymentdetailsid,
           oppbizdocsdet.numbizdocspaymentdetid AS numbizdocspaymentdetid,
           0 AS numoppid,
           oppbizdocsdet.numdivisionid,
           oppbizdocsdet.numbizdocsid AS numbizdocsid,
           CASE WHEN LEN(oppbizdocsdet.vcreference)=0 THEN 'Open bill' ELSE oppbizdocsdet.vcreference END AS [NAME]  ,
           oppbizdocspaydet.monamount AS amount,
           dbo.fn_GetListItemName(OppBizDocsDet.numPaymentMethod) AS paymentmethod,
           dbo.Fn_getcontactname(oppbizdocsdet.numcreatedby)
             + ' - '
             + dbo.Formateddatetimefromdate(Dateadd(MINUTE,-@ClientTimeZoneOffset,oppbizdocsdet.dtcreationdate),
                                            @numDomainId) AS createdby,
           Isnull(oppbizdocsdet.vcmemo,'') AS memo,
           Isnull(oppbizdocsdet.vcreference,'') AS reference,
           c.numcompanyid AS numcompanyid,
           c.vccompanyname AS vccompanyname,
           CASE oppbizdocsdet.tintPaymentType 
             WHEN 1 THEN 'Purchase Order'
             WHEN 2 THEN 'Bill'
             WHEN 4 THEN 'Bill (liability)'
             WHEN 3 THEN 'Sales Returns'
             WHEN 5 THEN 'Commission Expense'
             ELSE '-'
           END AS PaymentType,
           ISNULL(OppBizDocsDet.numReturnID,0) AS numReturnID,
			OppBizDocsDet.numCurrencyID,
			OppBizDocsDet.fltExchangeRate, 
			OppBizDocsPayDet.monAmount* OppBizDocsDet.fltExchangeRate as ConAmt,
			tintPaymentType,
			0 AS fltTransactionCharge,
			0 AS TransactionChargeAccountID,
			[dbo].[FormatedDateFromDate](OppBizDocsPayDet.[dtDueDate],@numDomainId) AS DueDate,
			'' vcRefOrderNo,
			'' AS EmployerName,
			ISNULL(numCommissionID,0) numCommissionID
    FROM   opportunitybizdocsdetails oppbizdocsdet
           INNER JOIN OpportunityBizDocsPaymentDetails OppBizDocsPayDet
             ON OppBizDocsDet.numBizDocsPaymentDetId = OppBizDocsPayDet.numBizDocsPaymentDetId
           INNER JOIN DivisionMaster Div
             ON OppBizDocsDet.numDivisionId = Div.numDivisionID
           INNER JOIN CompanyInfo C
             ON Div.numCompanyID = C.numCompanyId
    WHERE  OppBizDocsDet.numDomainId = @numDomainId
		   AND OppBizDocsDet.numDivisionId=	@numDivisionId
           AND OppBizDocsDet.bitAuthoritativeBizDocs = 1
           --AND OppBizDocsPayDet.bitIntegrated = 0
           AND OppBizDocsDet.numBizDocsId = 0
           AND oppbizdocsdet.tintPaymentType =2
           
	
 END

