GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteBankDetailConnection')
	DROP PROCEDURE USP_DeleteBankDetailConnection
GO

/****** Added By : Joseph ******/
/******  Deletes the Active Connection Flag between Bank Account(mapped in Biz) and Financial Institution ******/

CREATE PROCEDURE [dbo].[USP_DeleteBankDetailConnection]
    @numBankDetailID NUMERIC(18, 0)
AS 
    SET NOCOUNT ON

    IF EXISTS ( SELECT  [numBankDetailID]
                FROM    [dbo].[BankDetails]
                WHERE   [numBankDetailID] = @numBankDetailID ) 
        BEGIN
            UPDATE  [dbo].[BankDetails]
            SET     [bitIsActive] = 0,
                    [vcUserName] = '',
                    [vcPassword] = '',
                    [dtModifiedDate] = GETDATE()
            WHERE   [numBankDetailID] = @numBankDetailID
    
            UPDATE  dbo.Chart_Of_Accounts
            SET     numBankDetailID = 0,
                    IsConnected = 0,
                    IsBankAccount = 1
            WHERE   [numBankDetailID] = @numBankDetailID
        END
go


