GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_Update_StagePercentageDetails')
DROP PROCEDURE usp_Update_StagePercentageDetails
GO
CREATE PROCEDURE usp_Update_StagePercentageDetails
    @strStageDetail TEXT
AS 
    BEGIN
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(10), @strStageDetail) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strStageDetail
                
                UPDATE  StagePercentageDetails
                SET     tintPercentage = X.tintPercentage,bintModifiedDate=getdate()
                FROM    ( SELECT    numStageDetailsId,
                                    tintPercentage
                          FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                    WITH ( numStageDetailsId NUMERIC(9), tintPercentage tinyint )
                        ) X
                WHERE  StagePercentageDetails.numStageDetailsId = X.numStageDetailsId
                EXEC sp_xml_removedocument @hDocItem
            END
    END