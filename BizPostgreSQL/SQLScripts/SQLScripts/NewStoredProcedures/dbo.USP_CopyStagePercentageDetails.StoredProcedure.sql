GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_CopyStagePercentageDetails')
DROP PROCEDURE usp_CopyStagePercentageDetails
GO
CREATE PROCEDURE [dbo].[usp_CopyStagePercentageDetails]
    @numDomainid NUMERIC = 0,
    @numProjectID NUMERIC = 0 ,
    @slp_id NUMERIC = 0,
    @numContactId NUMERIC = 0 ,
    @numOppID NUMERIC = 0 
AS 

if (select count(*) from StagePercentageDetails where numOppID=@numOppID and numProjectID=@numProjectID and numDomainid=@numDomainid)=0
BEGIN

declare @id as int
SEt @id=IDENT_CURRENT('[StagePercentageDetails]');

 
;WITH tNewRow AS
(SELECT [numStageDetailsId],[numParentStageID],[numStagePercentageId],[tintConfiguration],[vcStageName]
      ,[numDomainId],[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[intDueDays],
	   [vcDescription],(@id + ROW_NUMBER() OVER (ORDER BY numStageDetailsId)) AS numNewParentStageID
FROM [StagePercentageDetails] where slp_id=@slp_id and ISNULL(numProjectid,0)=0 and ISNULL(numOppid,0)=0 and numDomainid=@numDomainid) 


SELECT *
INTO #TempTable
FROM tNewRow order by numStagePercentageId,numStageDetailsId

--WITH tNewRow AS
--(SELECT [numStageDetailsId],[numParentStageID],[numStagePercentageId],[tintConfiguration],[vcStageName]
--      ,[numDomainId],[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[intDueDays],
--	   [vcDescription],(@id + ROW_NUMBER() OVER (ORDER BY numStageDetailsId)) AS numNewParentStageID
--FROM [StagePercentageDetails] where slp_id=@slp_id and ISNULL(numProjectid,0)=0 and ISNULL(numOppid,0)=0 and numDomainid=@numDomainid) 

;WITH tParent as(SELECT t1.numNewParentStageID numStageDetailsId,isnull(t2.numNewParentStageID,0) numParentStageID,t1.[numStagePercentageId],
	   t1.[tintConfiguration],t1.[vcStageName],t1.[numDomainId],t1.[slp_id],t1.[numAssignTo],t1.[vcMileStoneName],t1.[tintPercentage],t1.[bitClose],t1.[intDueDays],
	   t1.[vcDescription] FROM #TempTable t1 LEFT JOIN #TempTable t2 ON t1.numParentStageID = t2.numStageDetailsId)

,tStageOrder AS (
    SELECT 0 AS Lvl, [numStageDetailsId],[numParentStageID],[numStagePercentageId],[tintConfiguration],[vcStageName]
      ,[numDomainId],[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[intDueDays],[vcDescription],
	CAST(ROW_NUMBER() OVER (ORDER BY numStagePercentageId,numStageDetailsId) AS VARCHAR(MAX)) as stage
    FROM tParent where [numParentStageID]=0
    UNION ALL
    SELECT p.lvl + 1, s.[numStageDetailsId],s.[numParentStageID],s.[numStagePercentageId],s.[tintConfiguration],s.[vcStageName]
      ,s.[numDomainId],s.[slp_id],s.[numAssignTo],s.[vcMileStoneName],s.[tintPercentage],s.[bitClose],s.[intDueDays],s.[vcDescription],
	p.stage + '.' + CAST(ROW_NUMBER() OVER (ORDER BY s.numStagePercentageId,s.numStageDetailsId) AS VARCHAR(MAX))
    FROM tParent s INNER JOIN tStageOrder p ON p.numStageDetailsId = s.numParentStageID)

----, tRowNumber as (SELECT *,ROW_NUMBER() OVER (ORDER BY stage) ROWNUMBER FROM tStageOrder)

--, tRowNumber as (SELECT *,ROW_NUMBER() OVER (ORDER BY cast(cast(substring(stage,1,case when(charindex('.',stage)>0) then charindex('.',stage) else len(stage) end) AS varchar(10))
-- +''+ cast(replace(substring(stage,charindex('.',stage),len(stage)),'.','') as varchar(10)) as numeric(18,10))) AS ROWNUMBER FROM tStageOrder)
--
--,tFinal as (select top 1 *,getdate() as [dtStartDate],getdate() + [intDueDays] - 1 as [dtEndDate] from tRowNumber
--
--union all select f.*,CASE WHEN f.numParentStageID=0 THEN getdate() ELSE p.[dtEndDate]+1 end as [dtStartDate],
--                    CASE WHEN f.numParentStageID=0 THEN getdate() + f.[intDueDays] - 1 else p.[dtEndDate] + f.[intDueDays] end  as [dtEndDate]
--		from tRowNumber f INNER JOIN tFinal p ON p.ROWNUMBER = (f.ROWNUMBER-1))

INSERT INTO [StagePercentageDetails] (numParentStageID,[numStagePercentageId],[tintConfiguration],[vcStageName]
      ,[numDomainId],[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],
		[intDueDays],[numOppID],[vcDescription],[numProjectID],[numCreatedBy],[bintCreatedDate],[numModifiedBy],
		[bintModifiedDate],[dtStartDate],[dtEndDate],bitFromTemplate)
select [numParentStageID],[numStagePercentageId],[tintConfiguration],[vcStageName]
      ,[numDomainId],[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[intDueDays],
		@numOppID,[vcDescription],@numProjectID,@numContactId,getdate(),@numContactId,getdate(),getdate() as [dtStartDate],getdate() + [intDueDays] - CASE intDueDays WHEN 0 THEN 0 ELSE 1 END  as [dtEndDate],1
 from tStageOrder order by numStageDetailsId


;WITH tDependency as(SELECT [numStageDependancyID]
      ,SD.[numStageDetailID]
      ,SD.[numDependantOnID],S.numNewParentStageID as StageDetailID,D.numNewParentStageID as DependantOnID
  FROM [StageDependency] SD join #TempTable S on SD.numStageDetailID=S.numStageDetailsId
                            join #TempTable D on SD.numDependantOnID=D.numStageDetailsId)


INSERT INTO [StageDependency]([numStageDetailID],[numDependantOnID])
select StageDetailID,DependantOnID from tDependency

drop table #TempTable


EXEC usp_AssignProjectStageDate @numDomainid,@numProjectID,@numOppID


/*Added by chintan, Bug ID*/
IF @numOppID >0 AND @slp_id>0
BEGIN
	UPDATE dbo.OpportunityMaster SET numBusinessProcessID = @slp_id WHERE numOppId=@numOppID
END

END
--declare @id as int
--SEt @id=IDENT_CURRENT('[StagePercentageDetails]');
--
--WITH t AS
--(SELECT [numStageDetailsId],[numParentStageID],[numStagePercentageId],[tintConfiguration],[vcStageName]
--      ,[numDomainId] ,[numCreatedBy],getdate() [bintCreatedDate],[numModifiedBy],getdate() [bintModifiedDate]
--      ,[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[dtStartDate]
--      ,[dtEndDate],[intDueDays],[numProjectID],[numOppID],[vcDescription],
--(@id
--+ ROW_NUMBER() OVER (ORDER BY numStageDetailsId))
--AS numNewParentStageID
--FROM [StagePercentageDetails] where slp_id=@slp_id and ISNULL(numProjectid,0)=0 and numDomainid=@numDomainid)
--
--INSERT INTO [StagePercentageDetails] (numParentStageID,[numStagePercentageId],[tintConfiguration],[vcStageName]
--      ,[numDomainId] ,[numCreatedBy],[bintCreatedDate],[numModifiedBy],[bintModifiedDate]
--      ,[slp_id],[numAssignTo],[vcMileStoneName],[tintPercentage],[bitClose],[dtStartDate]
--      ,[dtEndDate],[intDueDays],[numProjectID],[numOppID],[vcDescription])
--SELECT isnull(t2.numNewParentStageID,0),t1.numStagePercentageId,t1.tintConfiguration,t1.vcStageName
--      ,t1.numDomainId ,t1.numCreatedBy,t1.bintCreatedDate,t1.numModifiedBy,t1.bintModifiedDate
--      ,t1.slp_id,t1.numAssignTo,t1.vcMileStoneName,t1.tintPercentage,t1.bitClose,t1.dtStartDate
--      ,t1.dtEndDate,t1.intDueDays,@numProjectID,t1.numOppID,t1.vcDescription
--FROM t t1
--LEFT JOIN t t2
--ON t1.numParentStageID = t2.numStageDetailsId;

