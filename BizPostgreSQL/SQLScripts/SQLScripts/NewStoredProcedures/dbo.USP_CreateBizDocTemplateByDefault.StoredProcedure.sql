GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CreateBizDocTemplateByDefault' ) 
    DROP PROCEDURE USP_CreateBizDocTemplateByDefault
GO
-- EXEC USP_GetWarehouseLocation 1 , 58 , 11
CREATE PROCEDURE [dbo].USP_CreateBizDocTemplateByDefault
    @numDomainID AS NUMERIC(9) ,
    @txtBizDocTemplate AS TEXT,
    @txtCss AS TEXT,
    @tintMode AS tinyint=0, --1=create default template for all domain, 2 = create default template for selected domain, 3 = update template data for selected domain where its empty,4 = repeate same as 3 for all domains
    @txtPackingSlipBizDocTemplate AS TEXT
    
AS 
   BEGIN
   	IF @tintMode=1
   	BEGIN
--   		DECLARE @numDomainID NUMERIC

			DECLARE  @maxDomainID NUMERIC(9)
			DECLARE  @minDomainID NUMERIC(9)
			SELECT @maxDomainID = max([numDomainId])
			FROM   [Domain] WHERE numDomainId>0
			SELECT @minDomainID = min([numDomainId])
			FROM   [Domain] WHERE numDomainId>0
			WHILE @minDomainID <= @maxDomainID
			  BEGIN
				PRINT 'DomainID=' + CONVERT(VARCHAR(20),@minDomainID)
				PRINT '--------------'
				
				
			SET @numDomainID = @minDomainID


			INSERT INTO dbo.BizDocTemplate
					( numDomainID ,
					  numBizDocID ,
					  numOppType ,
					  txtBizDocTemplate ,
					  txtCSS ,
					  bitEnabled ,
					  tintTemplateType ,
					  vcTemplateName ,
					  bitDefault
					)

			SELECT @numDomainID,numListItemID AS numBizDocID,
			CASE numListItemID 
			 WHEN 292 THEN 2
			 WHEN 295 THEN 2
			 WHEN 644 THEN 2
			 WHEN 300 THEN 2
			 ELSE 1 END AS numOppType,'' AS txtBizDocTemplate,'' AS txtCss,1 AS bitEnabled,0 AS tintTemplateType,LD.vcData AS vcTemplateName,1 AS bitDefault 
			 FROM dbo.ListDetails LD  WHERE numListID=27 AND constFlag=1
			 AND LD.numListItemID NOT IN ( SELECT numBizDocID FROM dbo.BizDocTemplate WHERE numBizDocID>0 AND numDomainID=@numDomainID AND tintTemplateType=0)
			 


--				SELECT * FROM dbo.BizDocTemplate WHERE numDomainID=@numDomainID AND tintTemplateType=0


				---------------------------------------------------
				SELECT @minDomainID = min([numDomainId])
				FROM   [Domain] WHERE  [numDomainId]> @minDomainID 
			 END
  
   	END
   	ELSE IF @tintMode=2
   	BEGIN
			INSERT INTO dbo.BizDocTemplate
					( numDomainID ,
					  numBizDocID ,
					  numOppType ,
					  txtBizDocTemplate ,
					  txtCSS ,
					  bitEnabled ,
					  tintTemplateType ,
					  vcTemplateName ,
					  bitDefault
					)

			SELECT @numDomainID,numListItemID AS numBizDocID,
			CASE numListItemID 
			 WHEN 292 THEN 2
			 WHEN 295 THEN 2
			 WHEN 644 THEN 2
			 WHEN 300 THEN 2
			 ELSE 1 END AS numOppType,'' AS txtBizDocTemplate,'' AS txtCss,1 AS bitEnabled,0 AS tintTemplateType,LD.vcData AS vcTemplateName,1 AS bitDefault 
			 FROM dbo.ListDetails LD  WHERE numListID=27 AND constFlag=1
			 AND LD.numListItemID NOT IN ( SELECT numBizDocID FROM dbo.BizDocTemplate WHERE numBizDocID>0 AND numDomainID=@numDomainID AND tintTemplateType=0)
			 

   	END
   	ELSE IF @tintMode=3
   	BEGIN
   		
   		
   		UPDATE dbo.BizDocTemplate SET txtBizDocTemplate = @txtBizDocTemplate ,txtCSS=@txtCss WHERE numDomainID=@numDomainID AND tintTemplateType=0 
   		AND LEN( CAST (txtBizDocTemplate AS VARCHAR(8000)) )=0 AND LEN( CAST (txtCSS AS VARCHAR(8000)) )=0 AND numBizDocID <> 29397
   		
   		UPDATE dbo.BizDocTemplate SET txtBizDocTemplate = @txtPackingSlipBizDocTemplate ,txtCSS=@txtCss WHERE numDomainID=@numDomainID AND tintTemplateType=0 
   		AND LEN( CAST (txtBizDocTemplate AS VARCHAR(8000)) )=0 AND LEN( CAST (txtCSS AS VARCHAR(8000)) )=0 AND numBizDocID = 29397
   		
   		
   		--SELECT * FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1
   		
   	END
   	ELSE IF @tintMode=4
   	BEGIN
   		
   		
   		UPDATE dbo.BizDocTemplate SET txtBizDocTemplate = @txtBizDocTemplate ,txtCSS=@txtCss WHERE tintTemplateType=0 
   		AND LEN( CAST (txtBizDocTemplate AS VARCHAR(8000)) )=0 AND LEN( CAST (txtCSS AS VARCHAR(8000)) )=0 AND numBizDocID <> 29397
   		
   		UPDATE dbo.BizDocTemplate SET txtBizDocTemplate = @txtPackingSlipBizDocTemplate ,txtCSS=@txtCss WHERE tintTemplateType=0 
   		AND LEN( CAST (txtBizDocTemplate AS VARCHAR(8000)) )=0 AND LEN( CAST (txtCSS AS VARCHAR(8000)) )=0 AND numBizDocID = 29397
   		
   	END
   	
   	
   	
   	
   END
   GO 