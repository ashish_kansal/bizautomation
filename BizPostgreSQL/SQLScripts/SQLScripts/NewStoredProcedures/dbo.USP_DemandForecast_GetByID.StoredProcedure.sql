SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetByID')
DROP PROCEDURE dbo.USP_DemandForecast_GetByID
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetByID]
	@numDFID NUMERIC(18,0)
AS 
BEGIN
	SELECT * FROM DemandForecast WHERE numDFID = @numDFID
	SELECT * FROM DemandForecastItemClassification WHERE numDFID = @numDFID
	SELECT * FROM DemandForecastItemGroup WHERE numDFID = @numDFID
	SELECT * FROM DemandForecastWarehouse WHERE numDFID = @numDFID
END