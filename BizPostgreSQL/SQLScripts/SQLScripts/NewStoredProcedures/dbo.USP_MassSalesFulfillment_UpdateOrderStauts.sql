GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateOrderStauts')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateOrderStauts
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateOrderStauts]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintViewID TINYINT
	,@tintPackingMode TINYINT
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		SELECT 
			@numStatus = (CASE 
								WHEN @tintViewID = 1 THEN ISNULL(numOrderStatusPicked,0) 
								WHEN @tintViewID = 2 AND @tintPackingMode = 1 THEN ISNULL(numOrderStatusPacked1,0)
								WHEN @tintViewID = 2 AND @tintPackingMode = 2 THEN ISNULL(numOrderStatusPacked2,0)
								WHEN @tintViewID = 2 AND @tintPackingMode = 3 THEN ISNULL(numOrderStatusPacked3,0)
								WHEN @tintViewID = 2 AND @tintPackingMode = 4 THEN ISNULL(numOrderStatusPacked4,0)
								WHEN @tintViewID = 3 THEN ISNULL(numOrderStatusInvoiced,0)
								WHEN @tintViewID = 4 THEN ISNULL(numOrderStatusPaid,0)
								WHEN @tintViewID = 5 THEN ISNULL(numOrderStatusClosed,0)
								ELSE 0
							END) 
		FROM 
			MassSalesFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID
	END

	DECLARE @canUpdateStatus AS BIT = 0

	IF @tintViewID = 1 AND NOT EXISTS (SELECT
											numOppItemtcode
										FROM
											OpportunityItems
										WHERE
											numOppId = @numOppID
											AND ISNULL(numWarehouseItmsID,0) > 0
											AND ISNULL(bitDropShip,0) = 0
											AND ISNULL(numUnitHour,0) <> ISNULL(numQtyPicked,0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 2 AND @tintPackingMode = 1
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 2 AND @tintPackingMode = 2 AND NOT EXISTS (SELECT
																		numOppItemtcode
																	FROM
																		OpportunityItems
																	WHERE
																		numOppId = @numOppID
																		AND ISNULL(numWarehouseItmsID,0) > 0
																		AND ISNULL(bitDropShip,0) = 0
																		AND ISNULL(numUnitHour,0) <> ISNULL((SELECT 
																												SUM(numUnitHour) 
																											FROM 
																												OpportunityBizDocs OBD 
																											INNER JOIN 
																												OpportunityBizDocItems OBDI 
																											ON
																												OBD.numOppBizDocsId = OBDI.numOppBizDocID  
																											WHERE 
																												OBD.numOppId = OpportunityItems.numOppId 
																						AND OBD.numBizDocId=296
																						AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 2 AND @tintPackingMode = 4 AND NOT EXISTS (SELECT
																		numOppItemtcode
																	FROM
																		OpportunityItems
																	WHERE
																		numOppId = @numOppID
																		AND ISNULL(numWarehouseItmsID,0) > 0
																		AND ISNULL(bitDropShip,0) = 0
																		AND ISNULL(numUnitHour,0) <> ISNULL((SELECT 
																												SUM(SRI.intBoxQty) 
																											FROM 
																												ShippingReport SR
																											INNER JOIN
																												ShippingBox SB
																											ON
																												SR.numShippingReportId = SB.numShippingReportId
																											INNER JOIN
																												ShippingReportItems SRI
																											ON
																												SB.numBoxID = SRI.numBoxID 
																											INNER JOIN 
																												OpportunityBizDocItems OBDI 
																											ON
																												SRI.numOppBizDocItemID = OBDI.numOppBizDocItemID  
																											WHERE 
																												SR.numOppId = OpportunityItems.numOppId 
																												AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 3 AND NOT EXISTS (SELECT
												numOppItemtcode
											FROM
												OpportunityItems
											WHERE
												numOppId = @numOppID
												AND ISNULL(numUnitHour,0) <> ISNULL((SELECT 
																						SUM(numUnitHour) 
																					FROM 
																						OpportunityBizDocs OBD 
																					INNER JOIN 
																						OpportunityBizDocItems OBDI 
																					ON
																						OBD.numOppBizDocsId = OBDI.numOppBizDocID  
																					WHERE 
																						OBD.numOppId = OpportunityItems.numOppId 
																						AND OBD.numBizDocId=287
																						AND OBDI.numOppItemID=OpportunityItems.numoppitemtCode),0))
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 4 AND EXISTS (SELECT 
											numOppId 
										FROM 
											OpportunityMaster 
										WHERE 
											numOppId=@numOppID 
											AND monDealAmount = ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)) 
	BEGIN
		SET @canUpdateStatus = 1
	END
	ELSE IF @tintViewID = 5
	BEGIN
		SET @canUpdateStatus = 1
	END
	

	IF ISNULL(@numStatus,0) > 0 AND @canUpdateStatus = 1
	BEGIN
		UPDATE
			OpportunityMaster
		SET
			numStatus=@numStatus
		WHERE
			numDomainId=@numDomainID
			AND numOppId = @numOppID
	END
END