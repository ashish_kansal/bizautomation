SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_EmployeeSalesPerformancePanel1')
DROP PROCEDURE USP_ReportListMaster_EmployeeSalesPerformancePanel1
GO

CREATE PROCEDURE [dbo].[USP_ReportListMaster_EmployeeSalesPerformancePanel1]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,@vcFilterValue VARCHAR(MAX)
	--,@vcRunDisplay VARCHAR(MAX) -- added by hitesh 
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID FROM Domain WHERE numDomainId=@numDomainID
	---alter sp
 DECLARE @dtStartDate DATETIME --= DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(MONTH,-12,GETUTCDATE()))  
 DECLARE @dtEndDate AS DATETIME = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) 
 -------
	DECLARE @TotalSalesReturn FLOAT = 0.0
	----alterd sp-----------

IF @vcTimeLine = 'Last12Months'  
 BEGIN
 set  @dtStartDate =   dateadd(month,datediff(month,0,@dtEndDate)-12,0)  
  --SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)  
 END  
ELSE IF @vcTimeLine = 'Last6Months'  
 BEGIN
 set  @dtStartDate =   dateadd(month,datediff(month,0,@dtEndDate)-6,0)  
  --SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)  
 END  
ELSE IF @vcTimeLine = 'Last3Months'  
BEGIN  
set  @dtStartDate =   dateadd(month,datediff(month,0,@dtEndDate)-3,0)
--SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)  
END  
ELSE IF @vcTimeLine = 'Last30Days'  
BEGIN  
SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -30, @dtEndDate), 0)  
END  
ELSE IF @vcTimeLine = 'Last7Days'  
	BEGIN  
	SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -7, @dtEndDate), 0) 
	END
ELSE IF @vcTimeLine = 'Today' 
BEGIN 
SET @dtStartDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
END
ELSE IF @vcTimeLine = 'Yesterday'
BEGIN 
SET @dtStartDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
 END 
ELSE  
BEGIN
 Set @dtStartDate = LEFT(@vcTimeLine,CHARINDEX('-',@vcTimeLine)-1)
 set @dtEndDate = RIGHT(@vcTimeLine,CHARINDEX('-',@vcTimeLine)-1)
 END
  
	-----------------------------------
	SELECT 
		UserMaster.numUserDetailId,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate,
		UserMaster.vcUserName,@vcFilterBy as vcFilterBy
		,(SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numAssignedTo=TEMP1.numAssignedTo AND tintOppType=1 AND tintOppStatus=1 AND bintOppToOrder IS NOT NULL) * 100.0 / (SELECT COUNT(*) FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numAssignedTo=TEMP1.numAssignedTo AND tintOppType=1 AND ((tintOppStatus=1 AND bintOppToOrder IS NOT NULL) OR tintOppStatus=2)) * 1.0 WonPercent
	FROM
	(
		SELECT DISTINCT
			--numAssignedTo
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND ((tintOppStatus=1 AND bintOppToOrder IS NOT NULL) OR tintOppStatus=2)
		--	--alter sp
			AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
	 AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
	 ------
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
	WHERE
		UserMaster.numDomainID = @numDomainID
		

--#2
	SELECT
		UserMaster.numUserDetailId,
		@vcFilterBy as vcFilterBy,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate
		,UserMaster.vcUserName
		,Temp2.Profit
	FROM
	(
		SELECT DISTINCT
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
				AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
			 AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			SUM(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1)) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode		
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numAssignedTo = TEMP1.numAssignedTo
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate) OR (DATEADD(MINUTE,-@ClientTimeZoneOffset,Om.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate))
	) TEMP2
	ORDER BY
		 TEMP2.Profit DESC
--#3
	SELECT
		UserMaster.numUserDetailId
		,UserMaster.vcUserName,@vcFilterBy as vcFilterBy,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate
		,AVG(ProfitPercentByOrder) AS AvgGrossProfitMargin
	FROM
	(
		SELECT DISTINCT
			--numAssignedTo
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
				AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
			 AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			OM.numOppID
			,(SUM(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1))/SUM(ISNULL(monTotAmount,0))) * 100.0 ProfitPercentByOrder
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numAssignedTo = TEMP1.numAssignedTo
			AND ISNULL(monTotAmount,0) <> 0
			AND ISNULL(numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
			AND (  
		  (DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
		  OR   
		  (DATEADD(MINUTE,-@ClientTimeZoneOffset,Om.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate) 
		  )
		GROUP BY
			OM.numOppId
	) TEMP2
	GROUP BY
		UserMaster.vcUserName,UserMaster.numUserDetailId
	HAVING
		AVG(ProfitPercentByOrder) > 0
	ORDER BY
		AVG(ProfitPercentByOrder) DESC


--#4---Gross revenue
		SELECT
		UserMaster.numUserDetailId,UserMaster.vcUserName,Temp3.InvoiceTotal,@vcFilterBy as vcFilterBy,CAst(@dtStartDate as date) as StartDate,CAST(@dtEndDate  as date) as EndDate
	FROM
	(
		SELECT DISTINCT
			--numAssignedTo
			CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN numAssignedTo
		   --else numRecOwner
		   WHEN 2 
		   THEN numRecOwner
		   else  numAssignedTo
		   End as numAssignedTo
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
				AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)  
     ) 
			AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
	   THEN (CASE WHEN numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)  
      END)  
     ELSE 1   
    END)  
			
	) TEMP1
	INNER JOIN
		UserMaster
	ON
		TEMP1.numAssignedTo = UserMaster.numUserDetailId
		AND UserMaster.numDomainID = @numDomainID
	CROSS APPLY
	(
		SELECT
			Sum(
			ISNULL(OBD.monDealAmount,0) 
			 )
			 InvoiceTotal
		FROM
		
		--	OpportunityItems OI
		--INNER JOIN
		
			OpportunityMaster OM
		--ON
		--	OI.numOppId = OM.numOppID
		Inner join 
			OpportunityBizDocs OBD 
		ON
			OBD.numOppId = OM.numOppId and OBD.bitAuthoritativeBizDocs=1
		--INNER JOIN
		--	Item I
		--ON
		--	OI.numItemCode = I.numItemCode
		--Left JOIN 
		--	Vendor V 
		--ON 
		--	V.numVendorID=I.numVendorID 
		--	AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID

			and CASE @vcFilterBy  
		   WHEN 1  -- Assign To  
		   THEN Om.numAssignedTo
		   WHEN 2 
		   THEN OM.numRecOwner
		   else  OM.numAssignedTo
		   End  = TEMP1.numAssignedTo
			--AND OM.numRecOwner = TEMP1.numAssignedTo
			--AND ISNULL(OI.monTotAmount,0) <> 0
			--AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OBD.numBizDocId = 287
			
			--AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID) /*if we keep this condition then sales order invoice total will not match. also it was producing duplicates.*/
			AND (  
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate)   
      OR   
      (DATEADD(MINUTE,-@ClientTimeZoneOffset,Om.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate) 
	  )

	) TEMP3
	
	
	ORDER BY
		 TEMP3.InvoiceTotal DESC
		-----
END
GO
