SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGrossProfitEstimate')
DROP PROCEDURE USP_GetGrossProfitEstimate
GO
CREATE PROCEDURE [dbo].[USP_GetGrossProfitEstimate] 
( 
@numDomainID as numeric(9)=0,    
@numOppID AS NUMERIC(9)=0
)
AS
BEGIN
	DECLARE @avgCost INT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	SELECT 
		@avgCost=numCost
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
	FROM 
		Domain 
	WHERE 
		numDOmainId=@numDomainID

	DECLARE @monShippingProfit DECIMAL(20,5)
	DECLARE @monShippingAverageCost DECIMAL(20,5) = 0
	DECLARE @monMarketplaceShippingCost DECIMAL(20,5)

	SELECT @monMarketplaceShippingCost = ISNULL(monMarketplaceShippingCost,0) FROM OpportunityMaster WHERE numDomainID=@numDomainID AND numOppID=@numOppID 

	IF ISNULL(@monMarketplaceShippingCost,0) > 0
	BEGIN
		IF EXISTS (SELECT * FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID)
		BEGIN
			SET @monShippingAverageCost = @monMarketplaceShippingCost / ISNULL((SELECT COUNT(*) FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID),0)
			SET @monShippingProfit = ISNULL((SELECT SUM(monTotAmount) FROM OpportunityItems WHERE numOppID=@numOppID AND numItemCode=@numShippingServiceItemID),0) - @monMarketplaceShippingCost
		END
		ELSE
		BEGIN
			SET @monShippingProfit = 0
		END
	END
	ELSE
	BEGIN
		SET @monShippingProfit = 0
	END

	SELECT 
		numOppId
		,@monShippingProfit AS monShippingProfit
		,vcPOppName
		,vcItemName
		,monAverageCost
		,vcVendor
		,(ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1) AS monPrice
		,VendorCost
		,numUnitHour
		,(ISNULL(monTotAmount,0) * fltExchangeRate) - (CASE WHEN numItemCode=@numShippingServiceItemID THEN (ISNULL(numUnitHour,0) *  @monShippingAverageCost) ELSE (ISNULL(numUnitHour,0) * (CASE @avgCost WHEN 3 THEN VendorCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numPurchaseUnit) WHEN 2 THEN numCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numUOMId) ELSE monAverageCost END)) END) AS Profit
		,((((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1)) - (CASE 
																							WHEN numItemCode=@numShippingServiceItemID 
																							THEN @monShippingAverageCost
																							ELSE
																								(CASE 
																									WHEN numPOItemID IS NOT NULL 
																									THEN ISNULL(monPOCost,0)
																									ELSE
																										(CASE @avgCost 
																											WHEN 3 THEN VendorCost * dbo.fn_UOMConversion(numBaseUnit,numItemCode,@numDomainID,numPurchaseUnit) 																									
																											ELSE monAverageCost 
																										END) 
																								END)
																							END)) / (CASE WHEN ((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1))=0 then 1 ELSE ((ISNULL(monTotAmount,0) * fltExchangeRate)/ISNULL(NULLIF(numUnitHour,0),1)) end)) * 100 ProfitPer
		,bitItemPriceApprovalRequired
		,(CASE WHEN numPOID IS NOT NULL THEN CONCAT('<a href="javascript:OpenOpp(',numPOID,')">',vcPOName,'</a>') ELSE '' END) vcPurchaseOrder
	FROM
	(
		SELECT 
			opp.numOppId
			,Opp.vcPOppName
			,OppI.vcItemName
			,(CASE 
				WHEN ISNULL(I.bitVirtualInventory,0) = 1 
				THEN 0 
				ELSE (CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKI.monAvgCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=Opp.numOppId AND OKI.numOppItemID=OppI.numoppitemtCode),0) + 
							 ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.monAvgCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=Opp.numOppId AND OKI.numOppItemID=OppI.numoppitemtCode),0)
						ELSE ISNULL(monAverageCost,0) 
					END)
			END) monAverageCost,
			V.numVendorID AS numVendorID
			,dbo.fn_getcomapnyname(V.numVendorID) as vcVendor
			,oppI.numUnitHour
			,ISNULL(OPPI.monPrice,0) AS monPrice
			,ISNULL(OPPI.monTotAmount,0) AS monTotAmount
			,ISNULL(V.monCost,0) VendorCost
			,ISNULL(OppI.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired
			,ISNULL(OppI.numCost,0) numCost
			,I.numItemCode
			,numBaseUnit
			,numPurchaseUnit
			,ISNULL(OppI.numUOMId,numBaseUnit) AS numUOMId
			,ISNULL(Opp.fltExchangeRate,1) fltExchangeRate
			,ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId) AS numPOID
			,ISNULL(TEMPMatchedPO.vcPOppName,TEMPPO.vcPOppName) vcPOName
			,ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode) AS numPOItemID
			,ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice) AS monPOCost
		FROM 
			OpportunityMaster Opp 
		INNER JOIN 
			OpportunityItems OppI 
		ON 
			opp.numOppId=OppI.numOppId 
		OUTER APPLY
		(
			SELECT TOP 1
				OM.numOppID
				,OM.vcPOppName
				,OI.numoppitemtCode
				,OI.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OM
			ON
				SOLIPL.numPurchaseOrderID = OM.numOppId
			INNER JOIN
				OpportunityItems OI
			ON
				OM.numOppId = OI.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OI.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = Opp.numOppId
				AND SOLIPL.numSalesOrderItemID = OppI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OM.numOppID
				,OM.vcPOppName
				,OI.numoppitemtCode
				,OI.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OM
			ON
				OL.numChildOppID = OM.numOppId
			INNER JOIN
				OpportunityItems OI
			ON
				OM.numOppId = OI.numOppId
			WHERE
				OL.numParentOppID = Opp.numOppId
				AND OI.numItemCode = OppI.numItemCode
				AND OI.vcAttrValues = OppI.vcAttrValues
		) TEMPPO
		INNER JOIN 
			Item I 
		ON 
			OppI.numItemCode=i.numItemcode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE 
			Opp.numDomainId=@numDomainID 
			AND opp.numOppId=@numOppID
			AND ISNULL(I.bitContainer,0) = 0
	) temp
END