GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ApproveOpportunityItemsPrice' ) 
    DROP PROCEDURE USP_ApproveOpportunityItemsPrice
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 14 July 2014
-- Description:	sets unit price approval required flag to false for all items added in order
-- =============================================
CREATE PROCEDURE USP_ApproveOpportunityItemsPrice
	@numOppId numeric(18,0),
	@numDomainID numeric(18,0),
	@ApprovalType AS INT = 0
AS
BEGIN
	--	 SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--SET NOCOUNT ON;
	IF(@ApprovalType=2)
	BEGIN
		EXEC USP_ItemUnitPriceApprovalUpdateConfigurePrice @numOppId=@numOppId,@numDomainID=@numDomainID
	END


	UPDATE
		OpportunityMaster
	SET
		tintOppStatus=CASE WHEN bitIsInitalSalesOrder=1 THEN 1 ELSE tintOppStatus END
	WHERE
		numOppId = @numOppId
	
	UPDATE
		OpportunityItems
	SET
		bitItemPriceApprovalRequired = 0
	WHERE
		numOppId = @numOppId
END
GO