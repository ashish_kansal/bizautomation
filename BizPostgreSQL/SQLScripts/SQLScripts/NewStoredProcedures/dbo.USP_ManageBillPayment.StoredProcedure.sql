        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageBillPayment' ) 
    DROP PROCEDURE USP_ManageBillPayment
GO

CREATE PROCEDURE [dbo].[USP_ManageBillPayment]
    @numBillPaymentID NUMERIC(18, 0) =0 output,
    @dtPaymentDate DATETIME ,
    @numAccountID NUMERIC(18, 0) ,
    @numPaymentMethod NUMERIC(18, 0) ,
    @numUserCntID NUMERIC(18, 0) ,
    @numDomainID NUMERIC(18, 0) ,
    @strItems TEXT,
    @tintMode TINYINT=0,
    @monPaymentAmount DECIMAL(20,5)=0,
    @numDivisionID NUMERIC(18,0),
    @numReturnHeaderID NUMERIC(18,0),
    @numCurrencyID NUMERIC(18,0)=0,
    @fltExchangeRate FLOAT=1,
    @numAccountClass NUMERIC(18,0)=0
AS 
BEGIN
SET NOCOUNT ON
    DECLARE @numBillPaymentID_X NUMERIC(18,0)
    DECLARE @dtPaymentDate_X DATETIME
    DECLARE @numAccountID_X NUMERIC(18,0)
    DECLARE @numPaymentMethod_X NUMERIC(18,0)
    DECLARE @numUserCntID_X NUMERIC(18,0)
    DECLARE @numDomainID_X NUMERIC(18,0)
    DECLARE @strItems_X VARCHAR(MAX)
    DECLARE @tintMode_X TINYINT=0
    DECLARE @monPaymentAmount_X DECIMAL(20,5)
    DECLARE @numDivisionID_X NUMERIC(18,0)
    DECLARE @numReturnHeaderID_X NUMERIC(18,0)
    DECLARE @numCurrencyID_X NUMERIC(18,0)
    DECLARE @fltExchangeRate_X FLOAT
    DECLARE @numAccountClass_X NUMERIC(18,0)

    SET @numBillPaymentID_X = @numBillPaymentID
	SET @dtPaymentDate_X = @dtPaymentDate
	SET @numAccountID_X = @numAccountID
	SET @numPaymentMethod_X = @numPaymentMethod
	SET @numUserCntID_X = @numUserCntID
	SET @numDomainID_X = @numDomainID
	SET @strItems_X = @strItems
	SET @tintMode_X = @tintMode
	SET @monPaymentAmount_X = @monPaymentAmount
	SET @numDivisionID_X = @numDivisionID
	SET @numReturnHeaderID_X = @numReturnHeaderID
	SET @numCurrencyID_X = @numCurrencyID
	SET @fltExchangeRate_X = @fltExchangeRate
	SET @numAccountClass_X = @numAccountClass
    
BEGIN TRY 
        BEGIN TRAN  
        
			IF ISNULL(@numCurrencyID_X,0) = 0 
			BEGIN
			 SET @fltExchangeRate_X=1
			 SELECT @numCurrencyID_X= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainID_X	
			END
 
        
			IF @tintMode_X=1
			BEGIN
				--Validation of closed financial year
				EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID_X,@dtPaymentDate_X
				
				
				IF EXISTS ( SELECT  [numBillPaymentID] FROM    [dbo].[BillPaymentHeader] WHERE   [numBillPaymentID] = @numBillPaymentID_X ) 
					BEGIN
						UPDATE  [dbo].[BillPaymentHeader] 
						SET     [dtPaymentDate] = @dtPaymentDate_X ,[numAccountID] = @numAccountID_X ,
								[numPaymentMethod] = @numPaymentMethod_X ,[numModifiedDate] = GETUTCDATE() ,
								[numModifiedBy] = @numUserCntID_X,monPaymentAmount=@monPaymentAmount_X,numCurrencyID=@numCurrencyID_X,fltExchangeRate=@fltExchangeRate_X
						WHERE   [numBillPaymentID] = @numBillPaymentID_X AND numDomainID =@numDomainID_X
					END
				ELSE 
					BEGIN
						INSERT  INTO [dbo].[BillPaymentHeader]
								( [dtPaymentDate] ,[numAccountID] ,[numPaymentMethod] ,[dtCreateDate] ,[numCreatedBy] ,[numModifiedDate] ,
								  [numModifiedBy],numDomainID,monPaymentAmount,numDivisionID,numReturnHeaderID,numCurrencyID,fltExchangeRate,numAccountClass
								)
						VALUES  ( 
								  @dtPaymentDate_X ,@numAccountID_X ,@numPaymentMethod_X ,GETUTCDATE() ,@numUserCntID_X ,GETUTCDATE() ,
								  @numUserCntID_X,@numDomainID_X,@monPaymentAmount_X,@numDivisionID_X,@numReturnHeaderID_X,@numCurrencyID_X,@fltExchangeRate_X,@numAccountClass_X
								)
						SET @numBillPaymentID = SCOPE_IDENTITY()
						SET @numBillPaymentID_X = @numBillPaymentID
					END
				END	
				
				
				DECLARE @hDocItem INT
				IF CONVERT(VARCHAR(10), @strItems_X) <> '' 
				BEGIN
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems_X
--						INSERT  INTO [dbo].[BillPaymentDetails]
--								( [numBillPaymentID] ,[tintBillType] ,[numOppBizDocsID] ,[numBillID] ,[monAmount])
					SELECT  X.tintBillType ,ISNULL(X.numOppBizDocsID,0) AS  numOppBizDocsID,ISNULL(X.numBillID,0) AS numBillID ,X.monAmount INTO #temp
								FROM    ( SELECT    * FROM      OPENXML (@hDocItem, '/NewDataSet/Item', 2)
										WITH ( tintBillType TINYINT,numOppBizDocsID NUMERIC(18, 0),numBillID NUMERIC(18, 0),monAmount DECIMAL(20,5))
								) X
					
					 EXEC sp_xml_removedocument @hDocItem
					 
					IF @tintMode_X = 1
					BEGIN			
						--Revert back all amount for BillHeader
						 UPDATE BH SET monAmtPaid = monAmtPaid - BPD.monAmount,
						 bitIsPaid = CASE WHEN monAmountDue -( monAmtPaid - BPD.monAmount) = 0 THEN 1 ELSE 0 END 
						 FROM BillHeader BH JOIN BillPaymentDetails BPD ON BPD.numBillID > 0 AND BH.numBillID = BPD.numBillID
						 WHERE BPD.numBillPaymentID = @numBillPaymentID_X 
						 
						 --Revert back all amount for OpportunityBizDocs
						UPDATE OBD SET monAmountPaid = monAmountPaid - BPD.monAmount
						FROM OpportunityBizDocs OBD JOIN BillPaymentDetails BPD ON BPD.numOppBizDocsID > 0 AND OBD.numOppBizDocsId = BPD.numOppBizDocsID
						WHERE BPD.numBillPaymentID = @numBillPaymentID_X
						
						--Delete all BillPaymentDetails entries 				 
						DELETE FROM dbo.BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID_X 
					END		
						
				UPDATE BPD SET monAmount=BPD.monAmount + X.monAmount,dtAppliedDate=GETUTCDATE()
						FROM dbo.BillPaymentDetails BPD JOIN  #temp X ON BPD.numBillID=X.numBillID 
						WHERE BPD.numBillPaymentID=@numBillPaymentID_X AND X.tintBillType=2
				
				UPDATE BPD SET monAmount=BPD.monAmount + X.monAmount,dtAppliedDate=GETUTCDATE()
						FROM dbo.BillPaymentDetails BPD JOIN  #temp X ON BPD.numOppBizDocsID=X.numOppBizDocsID 
						WHERE BPD.numBillPaymentID=@numBillPaymentID_X AND X.tintBillType=1	
				
				INSERT  INTO [dbo].[BillPaymentDetails]
					( [numBillPaymentID] ,[tintBillType] ,[numOppBizDocsID] ,[numBillID] ,[monAmount],dtAppliedDate)
				 SELECT @numBillPaymentID_X,X.tintBillType ,X.numOppBizDocsID ,X.numBillID ,X.monAmount,GETUTCDATE() 
						FROM #temp X WHERE X.tintBillType=1 AND X.numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
				UNION
				SELECT @numBillPaymentID_X,X.tintBillType ,X.numOppBizDocsID ,X.numBillID ,X.monAmount,GETUTCDATE() 
						FROM #temp X WHERE X.tintBillType=2 AND X.numBillID NOT IN (SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
						

			   UPDATE dbo.BillHeader SET monAmtPaid = monAmtPaid + X.monAmount,
					bitIsPaid = CASE WHEN monAmountDue -( monAmtPaid + X.monAmount) = 0 THEN 1 ELSE 0 END 
					FROM  #temp X 
					WHERE X.numBillID>0 AND dbo.BillHeader.numBillID = X.numBillID  
			
									 
				UPDATE dbo.OpportunityBizDocs SET monAmountPaid = monAmountPaid + monAmount
						FROM #temp X  
						WHERE X.numOppBizDocsID>0 AND dbo.OpportunityBizDocs.numOppBizDocsId = X.numOppBizDocsID
				
				--Add to OpportunityAutomationQueue if full Amount Paid	
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID_X, GETUTCDATE(), 1,15	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainID_X AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X AND ISNULL(numOppBizDocsID,0)>0)
								  AND ISNULL(OBD.monDealAmount,0)=ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

				--Add to OpportunityAutomationQueue if Balance due
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID)
						SELECT OM.numDomainID, OM.numOppId, OBD.numOppBizDocsId, 0, @numUserCntID_X, GETUTCDATE(), 1,16	
						    FROM OpportunityBizDocs OBD JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								WHERE OM.numDomainId=@numDomainID_X AND OBD.numOppBizDocsId IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X AND ISNULL(numOppBizDocsID,0)>0)
								  AND ISNULL(OBD.monDealAmount,0)>ISNULL(OBD.monAmountPaid,0) AND ISNULL(OBD.monAmountPaid,0)>0

				DROP TABLE #temp
				
				UPDATE BillPaymentHeader SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
  							WHERE numBillPaymentID=@numBillPaymentID_X 
			END
			ELSE IF @tintMode_X=2
			BEGIN
				--Revert back all amount for BillHeader
				 UPDATE BH SET monAmtPaid = monAmtPaid - BPD.monAmount,
				 bitIsPaid = CASE WHEN monAmountDue -( monAmtPaid - BPD.monAmount) = 0 THEN 1 ELSE 0 END 
				 FROM BillHeader BH JOIN BillPaymentDetails BPD ON BPD.numBillID > 0 AND BH.numBillID = BPD.numBillID
				 WHERE BPD.numBillPaymentID = @numBillPaymentID_X 
						 
				--Revert back all amount for OpportunityBizDocs
				UPDATE OBD SET monAmountPaid = monAmountPaid - BPD.monAmount
				FROM OpportunityBizDocs OBD JOIN BillPaymentDetails BPD ON BPD.numOppBizDocsID > 0 AND OBD.numOppBizDocsId = BPD.numOppBizDocsID
				WHERE BPD.numBillPaymentID = @numBillPaymentID_X
						
				--Delete all BillPaymentDetails entries 				 
				DELETE FROM dbo.BillPaymentDetails WHERE numBillPaymentID = @numBillPaymentID_X 
				
				UPDATE BillPaymentHeader SET monAppliedAmount=(SELECT ISNULL(SUM(ISNULL(monAmount,0)),0) FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID_X)
  							WHERE numBillPaymentID=@numBillPaymentID_X 
			END
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

	BEGIN TRY
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppBizDocsId NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numOppBizDocsId
	)
	SELECT 
		numOppBizDocsId 
	FROM 
		BillPaymentDetails 
	WHERE 
		numBillPaymentID=@numBillPaymentID_X
		AND ISNULL(numOppBizDocsId,0) > 0

	DECLARE @i INT = 1
	DECLARE @iCount INT 
	DECLARE @numOppBizDocsId NUMERIC(18,0)

	SELECT @iCount = COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT @numOppBizDocsId=numOppBizDocsId FROM @TEMP WHERE ID=@i
		EXEC USP_OpportunityBizDocs_CT @numDomainID,@numUserCntID,@numOppBizDocsId
		SET @i = @i + 1
	END

	
  END TRY
  BEGIN CATCH
	-- DO NOT RAISE ERROR
  END CATCH   
END