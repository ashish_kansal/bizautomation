SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CurrencyConversionStatus_Save')
DROP PROCEDURE USP_CurrencyConversionStatus_Save
GO
CREATE PROCEDURE [dbo].[USP_CurrencyConversionStatus_Save]  
	@dtLastExecuted DATE,
	@bitSucceed BIT,
	@intNoOfTimesTried INT,
	@bitFailureNotificationSent BIT
AS  
BEGIN 
	UPDATE
		CurrencyConversionStatus
	SET
		dtLastExecuted = @dtLastExecuted,
		bitSucceed = @bitSucceed,
		intNoOfTimesTried = @intNoOfTimesTried,
		bitFailureNotificationSent = @bitFailureNotificationSent
	WHERE
		Id = 1 --WE ARE PASSING FIX ID BECUASE TABLE WILL ALWAYS HAVE 1 ROW AND WE WILL UPDATE DATA TO SAME ROW
END