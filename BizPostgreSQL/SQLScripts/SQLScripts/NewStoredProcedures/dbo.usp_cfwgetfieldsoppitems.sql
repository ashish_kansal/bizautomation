GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwgetfieldsoppitems')
DROP PROCEDURE dbo.usp_cfwgetfieldsoppitems
GO
CREATE PROCEDURE [dbo].[usp_cfwgetfieldsoppitems]
	@numDomainID as numeric(18,0),                       
	@numOppItemId as numeric(18,0),                  
    @numItemCode AS NUMERIC(18,0)                        
AS
BEGIN
	IF EXISTS (SELECT * FROM CFW_Fld_Values_OppItems WHERE RecId=@numOppItemId)
	BEGIN
		SELECT 
			CFW_Fld_Master.fld_id
			,fld_type,fld_label
			,numlistid
			,ISNULL(CFW_Fld_Values_OppItems.FldDTLID,0) FldDTLID
			,(CASE WHEN (ISNULL(CFW_Fld_Values_OppItems.Fld_Value, '') = '' OR UPPER(ISNULL(CFW_Fld_Values_OppItems.Fld_Value, '')) = 'NO' OR UPPER(ISNULL(CFW_Fld_Values_OppItems.Fld_Value, '')) = 'FALSE') AND fld_type = 'Checkbox' THEN '0' ELSE ISNULL(CFW_Fld_Values_OppItems.Fld_Value, 0) END) AS Value
			,subgrp as TabId
			,Grp_Name as tabname
			,vcURL
			,CFW_Fld_Master.vcToolTip  
		FROM 
			CFW_Fld_Master                               
		LEFT JOIN 
			CFw_Grp_Master                               
		ON 
			subgrp=CFw_Grp_Master.Grp_id  
        LEFT JOIN
			CFW_Fld_Values_OppItems
		ON
			CFW_Fld_Master.Fld_id = CFW_Fld_Values_OppItems.Fld_ID
			AND CFW_Fld_Values_OppItems.RecId=@numOppItemId
		WHERE 
			CFW_Fld_Master.grp_id=5 
			and CFW_Fld_Master.numDomainID=@numDomainID 
	END
	ELSE
	BEGIN
		SELECT 
			CFW_Fld_Master.fld_id
			,fld_type,fld_label
			,numlistid
			,ISNULL(CFW_Fld_Values_Item.FldDTLID,0) FldDTLID
			,(CASE WHEN (ISNULL(CFW_Fld_Values_Item.Fld_Value, '') = '' OR UPPER(ISNULL(CFW_Fld_Values_Item.Fld_Value, '')) = 'NO' OR UPPER(ISNULL(CFW_Fld_Values_Item.Fld_Value, '')) = 'FALSE') AND fld_type = 'Checkbox' THEN '0' ELSE ISNULL(CFW_Fld_Values_Item.Fld_Value, 0) END) AS Value
			,subgrp as TabId
			,Grp_Name as tabname
			,vcURL
			,CFW_Fld_Master.vcToolTip  
		FROM 
			CFW_Fld_Master                               
		LEFT JOIN 
			CFw_Grp_Master                               
		ON 
			subgrp=CFw_Grp_Master.Grp_id  
        LEFT JOIN
			CFW_Fld_Values_Item
		ON
			CFW_Fld_Master.Fld_id = CFW_Fld_Values_Item.Fld_ID
			AND CFW_Fld_Values_Item.RecId=@numItemCode
		WHERE 
			CFW_Fld_Master.grp_id=5 
			and CFW_Fld_Master.numDomainID=@numDomainID 
	END
END
