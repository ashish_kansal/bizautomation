GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_GetMailCount' ) 
    DROP PROCEDURE Usp_GetMailCount
GO
CREATE PROCEDURE Usp_GetMailCount
(
	@numDomainID	NUMERIC(10,0),
	@numUserCntID	NUMERIC(10,0)
)
AS 
BEGIN
	SELECT COUNT(*) FROM dbo.EmailHistory 
	WHERE ISNULL(bitIsRead,0) = 0 
	AND numDomainID = @numDomainID 
	AND numUserCntId = @numUserCntID
	AND tintType = 1
END    