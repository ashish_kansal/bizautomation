GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteEmbeddedCost')
DROP PROCEDURE USP_DeleteEmbeddedCost
GO
CREATE PROCEDURE USP_DeleteEmbeddedCost
    @numOppBizDocID NUMERIC,
    @numCostCatID NUMERIC,
    @numDomainID NUMERIC
AS 
    BEGIN
    
    IF exists(SELECT * FROM [OpportunityBizDocsPaymentDetails] WHERE bitIntegrated = 1 AND [numBizDocsPaymentDetId] IN (SELECT numBizDocsPaymentDetId FROM EmbeddedCost WHERE [numBizDocsPaymentDetId]>0 AND numOppBizDocID =@numOppBizDocID) )
    BEGIN
		raiserror('PAID',16,1);
		RETURN ;
	END
	
	DELETE [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT numBizDocsPaymentDetId FROM EmbeddedCost WHERE [numBizDocsPaymentDetId]>0 AND numOppBizDocID =@numOppBizDocID)
	DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocID AND ([numCostCatID] = @numCostCatID OR @numCostCatID=0) AND [numDomainID] = @numDomainID)
	
	CREATE TABLE #temp(numBizDocsPaymentDetId numeric)
	INSERT INTO #temp SELECT numBizDocsPaymentDetId FROM EmbeddedCost WHERE [numBizDocsPaymentDetId]>0 AND numOppBizDocID =@numOppBizDocID
	
	DELETE  FROM [EmbeddedCost]
        WHERE   [numOppBizDocID] = @numOppBizDocID
                AND ([numCostCatID] = @numCostCatID OR @numCostCatID=0)
                AND [numDomainID] = @numDomainID
	DELETE [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT numBizDocsPaymentDetId FROM #temp )
	
	DROP TABLE #temp
	
     
    END 