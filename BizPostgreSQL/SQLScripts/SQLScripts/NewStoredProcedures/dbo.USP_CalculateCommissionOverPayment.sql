GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionOverPayment')
DROP PROCEDURE USP_CalculateCommissionOverPayment
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionOverPayment]
	 @numDomainID AS NUMERIC(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numComissionID NUMERIC(18,0),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		bitDomainCommissionBasedOn BIT,
		tintDomainCommissionBasedOn TINYINT,
		numUnitHour FLOAT,
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		monTotAmount DECIMAL(20,5),
		decCommission DECIMAL(18,2),
		monCommission DECIMAL(20,5)
	)

	DECLARE @tintCommissionType TINYINT

	SELECT 
		@tintCommissionType=tintCommissionType
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	DELETE 
		BDCPD
	FROM 
		BizDocComissionPaymentDifference BDCPD 
	INNER JOIN 
		BizDocComission BC 
	ON 
		BDCPD.numComissionID = BC.numComissionID 
	WHERE 
		BC.numDomainID=@numDomainID 
		AND ISNULL(bitDifferencePaid,0) = 0

	INSERT INTO BizDocComissionPaymentDifference
	(
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	)
	SELECT
		BC.numComissionID
		,1
		,'Sales Order deleted'
		,ISNULL(numComissionAmount,0)  * -1
		,0
	FROM 
		BizDocComission BC
	LEFT JOIN
		BizDocComissionPaymentDifference BCPD
	ON
		BC.numComissionID = BCPD.numComissionID
		AND BCPD.tintReason = 1
	LEFT JOIN
		OpportunityMaster OM
	ON
		BC.numOppID = OM.numOppId
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=1
		AND ISNULL(BC.bitNewImplementation,0) = 1
		AND OM.numOppId IS NULL
		AND BCPD.ID IS NULL 

	INSERT INTO BizDocComissionPaymentDifference
	(
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	)
	SELECT
		BC.numComissionID
		,1
		,'Sales Order line item deleted'
		,ISNULL(numComissionAmount,0)  * -1
		,0 
	FROM 
		BizDocComission BC
	LEFT JOIN
		BizDocComissionPaymentDifference BCPD
	ON
		BC.numComissionID = BCPD.numComissionID
		AND BCPD.tintReason = 1
	LEFT JOIN
		OpportunityItems OI
	ON
		BC.numOppItemID = OI.numoppitemtCode
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=1
		AND ISNULL(BC.bitNewImplementation,0) = 1
		AND OI.numoppitemtCode IS NULL
		AND BCPD.ID IS NULL 

	INSERT INTO BizDocComissionPaymentDifference
	(
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	)
	SELECT
		BC.numComissionID
		,1
		,'Invoice deleted'
		,ISNULL(numComissionAmount,0)  * -1
		,0 
	FROM 
		BizDocComission BC
	LEFT JOIN
		BizDocComissionPaymentDifference BCPD
	ON
		BC.numComissionID = BCPD.numComissionID
		AND BCPD.tintReason = 1
	LEFT JOIN
		OpportunityBizDocs OBD
	ON
		BC.numOppBizDocId = OBD.numOppBizDocsId
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppBizDocId,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=1
		AND ISNULL(BC.bitNewImplementation,0) = 1
		AND OBD.numOppBizDocsId IS NULL
		AND BCPD.ID IS NULL

	INSERT INTO BizDocComissionPaymentDifference
	(
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	)
	SELECT
		BC.numComissionID
		,1
		,'Invoice line item deleted'
		,ISNULL(numComissionAmount,0)  * -1
		,0 
	FROM 
		BizDocComission BC
	LEFT JOIN
		BizDocComissionPaymentDifference BCPD
	ON
		BC.numComissionID = BCPD.numComissionID
		AND BCPD.tintReason = 1
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON
		BC.numOppBizDocItemID = OBDI.numOppBizDocItemID
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppBizDocItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=1
		AND ISNULL(BC.bitNewImplementation,0) = 1
		AND OBDI.numOppBizDocItemID IS NULL
		AND BCPD.ID IS NULL

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE	
	IF @tintCommissionType = 3 --Sales Order Sub-Total Amount
	BEGIN
		INSERT INTO @TABLEPAID
		(
			numComissionID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			bitDomainCommissionBasedOn,
			tintDomainCommissionBasedOn,
			numUnitHour,
			monVendorCost,
			monAvgCost,
			monTotAmount,
			decCommission,
			monCommission
		)
		SELECT 
			BC.numComissionID,
			BC.numComRuleID,
			BC.tintComType,
			BC.tintComBasedOn,
			BC.bitDomainCommissionBasedOn,
			BC.tintDomainCommissionBasedOn,
			ISNULL(OI.numUnitHour,0),
			(ISNULL(OI.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OI.numUnitHour,0)),
			(ISNULL(OI.monAvgCost,0) * ISNULL(OI.numUnitHour,0)),
			ISNULL(OI.monTotAmount,0),
			ISNULL(decCommission,0),
			ISNULL(numComissionAmount,0)
		FROM 
			BizDocComission BC
		INNER JOIN
			OpportunityMaster OM
		ON
			BC.numOppID = OM.numOppId
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityItems OI 
		ON
			BC.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item
		ON
			OI.numItemCode = Item.numItemCode
		WHERE
			BC.numDomainId=@numDomainID
			AND ISNULL(BC.bitCommisionPaid,0)=1
			AND ISNULL(BC.bitNewImplementation,0) = 1
	END
	ELSE IF @tintCommissionType = 2 --Invoice Sub-Total Amount (Paid or Unpaid)
	BEGIN
		INSERT INTO @TABLEPAID
		(
			numComissionID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			bitDomainCommissionBasedOn,
			tintDomainCommissionBasedOn,
			numUnitHour,
			monVendorCost,
			monAvgCost,
			monTotAmount,
			decCommission,
			monCommission
		)
		SELECT 
			BC.numComissionID,
			BC.numComRuleID,
			BC.tintComType,
			BC.tintComBasedOn,
			BC.bitDomainCommissionBasedOn,
			BC.tintDomainCommissionBasedOn,
			ISNULL(OppBizItems.numUnitHour,0),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(decCommission,0),
			ISNULL(numComissionAmount,0)
		FROM 
			BizDocComission BC
		INNER JOIN
			OpportunityMaster OM
		ON
			BC.numOppID = OM.numOppId
		INNER JOIN	
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND BC.numOppBizDocId=OBD.numOppBizDocsId
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			BC.numOppBizDocItemID=OppBizItems.numOppBizDocItemID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			BC.numOppItemID=OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		WHERE
			BC.numDomainId=@numDomainID
			AND ISNULL(BC.bitCommisionPaid,0) = 1
			AND ISNULL(BC.bitNewImplementation,0) = 1
	END
	ELSE
	BEGIN
		INSERT INTO @TABLEPAID
		(
			numComissionID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			bitDomainCommissionBasedOn,
			tintDomainCommissionBasedOn,
			numUnitHour,
			monVendorCost,
			monAvgCost,
			monTotAmount,
			decCommission,
			monCommission
		)
		SELECT 
			BC.numComissionID,
			BC.numComRuleID,
			BC.tintComType,
			BC.tintComBasedOn,
			BC.bitDomainCommissionBasedOn,
			BC.tintDomainCommissionBasedOn,
			ISNULL(OppBizItems.numUnitHour,0),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			ISNULL(OppBizItems.monTotAmount,0),
			ISNULL(decCommission,0),
			ISNULL(numComissionAmount,0)
		FROM 
			BizDocComission BC
		INNER JOIN
			OpportunityMaster OppM
		ON
			BC.numOppID = OppM.numOppId
		INNER JOIN
			OpportunityBizDocs OppBiz
		ON
			OppM.numOppId = OppBiz.numOppId
			AND BC.numOppBizDocId=OppBiz.numOppBizDocsId
		INNER JOIN
			DivisionMaster 
		ON
			OppM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			BC.numOppBizDocItemID=OppBizItems.numOppBizDocItemID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			BC.numOppItemID=OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		WHERE
			BC.numDomainId=@numDomainID
			AND ISNULL(BC.bitCommisionPaid,0) = 1
			AND ISNULL(BC.bitNewImplementation,0) = 1
	END	

	INSERT INTO BizDocComissionPaymentDifference
	(
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	)
	SELECT
		numComissionID
		,tintReason
		,vcReason
		,monDifference
		,bitDifferencePaid
	FROM
	(SELECT
		T1.numComissionID
		,2 tintReason
		,'Change in qty/amount' vcReason
		,ISNULL(CASE tintComType 
					WHEN 1 --PERCENT
						THEN 
							CASE 
								WHEN ISNULL(bitDomainCommissionBasedOn,0) = 1
								THEN
									CASE tintDomainCommissionBasedOn
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T1.decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T1.decCommission / CAST(100 AS FLOAT))
									END
								ELSE
									monTotAmount * (T1.decCommission / CAST(100 AS FLOAT))
							END
					ELSE  --FLAT
						T1.decCommission
				END,0) - (ISNULL(monCommission,0) + ISNULL((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference WHERE numComissionID=T1.numComissionID),0)) monDifference
		,0 bitDifferencePaid
	FROM 
		@TABLEPAID AS T1
	WHERE
		ISNULL(CASE tintComType 
					WHEN 1 --PERCENT
						THEN 
							CASE 
								WHEN ISNULL(bitDomainCommissionBasedOn,0) = 1
								THEN
									CASE tintDomainCommissionBasedOn
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T1.decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T1.decCommission / CAST(100 AS FLOAT))
									END
								ELSE
									monTotAmount * (T1.decCommission / CAST(100 AS FLOAT))
							END
					ELSE  --FLAT
						T1.decCommission
				END,0) <> (ISNULL(monCommission,0) + ISNULL((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference WHERE numComissionID=T1.numComissionID),0))
	) TEMP
	WHERE
		CAST(TEMP.monDifference AS DECIMAL(20,4)) <> 0
END
GO