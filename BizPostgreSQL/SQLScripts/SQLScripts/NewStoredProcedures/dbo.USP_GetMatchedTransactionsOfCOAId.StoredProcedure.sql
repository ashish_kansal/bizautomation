GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMatchedTransactionsOfCOAId')
	DROP PROCEDURE USP_GetMatchedTransactionsOfCOAId
GO

/****** Added By : Joseph ******/
/****** Gets Transactions Details List for the Chart Of Account ID******/


CREATE PROCEDURE [dbo].[USP_GetMatchedTransactionsOfCOAId]
@numAccountID numeric(18, 0),
@CurrentPage AS INTEGER,
@PageSize AS INTEGER,
@TotRecs AS INTEGER OUTPUT

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN  


/*paging logic*/  
        DECLARE @firstRec AS INTEGER                                                              
        DECLARE @lastRec AS INTEGER                                                     
--        IF @vcOppBizDocIds <>'' SET @CurrentPage = 1; --bug id 1081
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )                                                                    
                                     
DECLARE @strSql AS VARCHAR(5000) 
DECLARE @strSql1 AS VARCHAR(5000) 



SET @strSql = '
SELECT 
BST.dtDatePosted,
BST.numTransactionID,
BST.monAmount,
DM.monDepositAmount AS MAmount, 
BST.vcPayeeName + BST.vcMemo AS vcDescription,
C.vcAccountName, 
BTM.tintReferenceType,
BTM.numReferenceID, 
LEFT(CONVERT(VARCHAR, BST.dtDatePosted, 120), 10) + '' Deposit ('' +ISNULL(C.[vcAccountName],'''') +  '') to '' +  ISNULL(CI.vcCompanyname,''-'') 
+ '' for $'' + CONVERT(VARCHAR, ABS(BST.monAmount)) AS TransMatchDetails

FROM dbo.BankStatementTransactions  BST 
LEFT JOIN dbo.BankTransactionMapping BTM ON BST.numTransactionID = BTM.numTransactionID
LEFT JOIN dbo.DepositMaster DM  ON BTM.numReferenceID = DM.numDepositId
LEFT JOIN dbo.DepositeDetails DD ON DD.numDepositId = DM.numDepositId
LEFT JOIN dbo.Chart_Of_Accounts C ON DD.numAccountID = C.numAccountId
LEFT JOIN dbo.DivisionMaster D ON DD.numReceivedFrom	= D.numDivisionID
LEFT JOIN dbo.companyinfo CI ON D.numCompanyID= CI.numCompanyID '

SET @strSql = @strSql + ' WHERE BST.numAccountId = ' + CONVERT(VARCHAR(15), @numAccountID)  +
' AND BST.bitIsActive = 1 AND

EXISTS (SELECT * FROM dbo.BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID AND BTM.numReferenceID > 0 AND  BTM.tintReferenceType = 6 AND BTM.bitIsActive = 1)

UNION ALL

SELECT 
BST.dtDatePosted,
BST.numTransactionID,
BST.monAmount, 
CD.monAmount  * -1 AS MAmount,
BST.vcPayeeName + BST.vcMemo AS vcDescription,
C.vcAccountName, 
BTM.tintReferenceType,
BTM.numReferenceID, 
LEFT(CONVERT(VARCHAR, BST.dtDatePosted, 120), 10) + '' Expense ('' +ISNULL(C.[vcAccountName],'''') +  '') to '' + ISNULL(CI.vcCompanyname,''-'') + '' for $'' + CONVERT(VARCHAR, ABS(BST.monAmount))AS TransMatchDetails

FROM dbo.BankStatementTransactions  BST 
LEFT JOIN dbo.BankTransactionMapping BTM ON BST.numTransactionID = BTM.numTransactionID
LEFT JOIN dbo.CheckHeader CH ON BTM.numReferenceID = CH.numCheckHeaderID
LEFT JOIN dbo.CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
LEFT JOIN dbo.Chart_Of_Accounts C ON CD.numChartAcntId = C.numAccountId
LEFT JOIN dbo.DivisionMaster D ON CD.numCustomerId	= D.numDivisionID
LEFT JOIN dbo.companyinfo CI ON D.numCompanyID= CI.numCompanyID '

SET @strSql = @strSql + ' WHERE BST.numAccountId = ' + CONVERT(VARCHAR(15), @numAccountID)  +

' AND BST.bitIsActive = 1 AND
EXISTS (SELECT * FROM dbo.BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID AND BTM.numreferenceId > 0 AND  BTM.tintReferenceType = 1 AND BTM.bitIsActive = 1)'

 Create table #tempTable (dtDatePosted DATETIME,
	numTransactionID numeric(18),
	monAmount DECIMAL(20,5) ,
	MAmount DECIMAL(20,5),
	vcDescription VARCHAR(150), 
	vcAccountName VARCHAR(50),   
	tintReferenceType numeric(18),
	numReferenceID numeric(18),
	TransMatchDetails VARCHAR(150)                 
 ) 
 
INSERT into #tempTable execute (@strSql )

 SET @strSql1 = 'WITH Paging
         AS (SELECT Row_number() OVER(ORDER BY dtDatePosted) AS row,* from #tempTable) select * from Paging where  row >'+ convert(varchar(15),@firstRec) +' and row < '+ convert(varchar(15),@lastRec) 

 exec (@strSql1)

 (SELECT @TotRecs =COUNT(*) from #tempTable)
        
  
 DROP TABLE #tempTable

 SELECT @TotRecs
 
END

GO