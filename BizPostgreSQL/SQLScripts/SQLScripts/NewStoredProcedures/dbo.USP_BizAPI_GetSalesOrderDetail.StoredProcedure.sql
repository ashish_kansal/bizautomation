GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPI_GetSalesOrderDetail' ) 
    DROP PROCEDURE USP_BizAPI_GetSalesOrderDetail
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 23 April 2014
-- Description:	Gets details of sales order created between supplied date
-- =============================================
CREATE PROCEDURE USP_BizAPI_GetSalesOrderDetail
	@numDomainID NUMERIC(18,0),
	@dtFromDate DATETIME,
	@dtToDate DATETIME = NULL,
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @dtToDate IS NOT NULL
		SELECT @dtToDate = DATEADD(d,1,@dtToDate)
	
		SELECT 
			@TotalRecords = COUNT(*) 
		FROM 
			OpportunityMaster
		WHERE 
			tintOppType = 1 AND
			OpportunityMaster.numDomainId = @numDomainID AND
			(OpportunityMaster.bintCreatedDate >= @dtFromDate) AND
			(OpportunityMaster.bintCreatedDate <= @dtToDate OR @dtToDate IS NULL)
	
	
		SELECT
			*
		FROM
			(
			SELECT
				ROW_NUMBER() OVER (ORDER BY OpportunityMaster.numOppId asc) AS RowNumber,
				OpportunityMaster.numOppId,
				OpportunityMaster.vcPOppName,
				CampaignMaster.numCampaignId,
				CampaignMaster.vcCampaignName,
				CompanyInfo.numCompanyId,
				CompanyInfo.vcCompanyName,
				OpportunityMaster.numContactId,
				dbo.Fn_getcontactname(OpportunityMaster.numContactId) numContactIdName,
				OpportunityMaster.tintSource,    
				dbo.fn_GetOpportunitySourceValue(ISNULL(OpportunityMaster.tintSource,0),
											 ISNULL(OpportunityMaster.tintSourceType, 0),
											OpportunityMaster.numDomainID) AS tintSourceName ,
				dbo.fn_getOPPAddress(OpportunityMaster.numOppId, @numDomainID, 1) AS BillingAddress ,
				dbo.fn_getOPPAddress(OpportunityMaster.numOppId, @numDomainID, 2) AS ShippingAddress,
				dbo.[getdealamount](OpportunityMaster.numOppId, GETUTCDATE(), 0) AS CalAmount,
				CONVERT(DECIMAL(10, 2), ISNULL(monPAmount, 0)) AS monPAmount,
				Isnull(tintshipped,0) AS tintshipped,
				OpportunityMaster.bintCreatedDate,
				OpportunityMaster.bintClosedDate,
				OpportunityMaster.numAssignedTo,
				( 
					SELECT    
						A.vcFirstName + ' ' + A.vcLastName
					FROM      
						AdditionalContactsInformation A
					WHERE     
						A.numcontactid = OpportunityMaster.numAssignedTo
				) AS numAssignedToName,
				ISNULL(Currency.varCurrSymbol, '') varCurrSymbol ,
				ISNULL(OpportunityMaster.fltExchangeRate, 0) AS [fltExchangeRate] ,
				ISNULL(Currency.chrCurrency, '') AS [chrCurrency] ,
				ISNULL(Currency.numCurrencyID, 0) AS [numCurrencyID],
				(SELECT    
					vcdata
				 FROM      
					listdetails
				 WHERE     
					numlistitemid = OpportunityMaster.numStatus) AS numStatusName,
				(
				-- Wrapped select result with another select to generate correct xml atrribute
				-- Do not remove wrapping
				SELECT 
					*
				FROM
				(SELECT  
					OpportunityItems.numItemCode AS ItemCode,
					OpportunityItems.vcItemName AS ItemName,
					OpportunityItems.vcType AS ItemType,
					OpportunityItems.vcModelID AS ModelID,
					OpportunityItems.vcManufacturer AS Manufacturer,
					Item.numBarCodeId AS UPC,
					Item.vcSKU AS SKU,
					Item.txtItemDesc AS [Description],
					Warehouses.numWareHouseID AS WarehouseID,
					warehouses.vcWareHouse AS Warehouse,
					numUnitHour AS Quantity,
					monPrice AS UnitPrice,
					monTotAmount AS TotalAmount
				FROM 
					OpportunityItems
				LEFT JOIN
					Item 
				ON
					OpportunityItems.numItemCode = Item.numItemCode
				LEFT JOIN
					WareHouseItems
				ON
					OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
				LEFT JOIN
					Warehouses
				ON
					WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
				WHERE 
					numOppId = OpportunityMaster.numOppId) AS Item
				FOR XML AUTO) AS Items
			FROM
				OpportunityMaster
			INNER JOIN 
				AdditionalContactsInformation ADC 
			ON 
				OpportunityMaster.numContactId = ADC.numContactId                                                               
			INNER JOIN 
				DivisionMaster Div 
			ON 
				OpportunityMaster.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
			INNER JOIN 
				CompanyInfo cmp 
			ON 
				Div.numCompanyID = cmp.numCompanyId
			LEFT JOIN
				DivisionMaster 
			ON 
				OpportunityMaster.numDivisionID = DivisionMaster.numDivisionID
			LEFT JOIN 
				CompanyInfo 
			ON 
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
			LEFT JOIN
				CampaignMaster
			ON
				OpportunityMaster.numCampainID = CampaignMaster.numCampaignId
			LEFT JOIN 
				Currency
			ON 
				OpportunityMaster.numCurrencyID = Currency.numCurrencyID
			WHERE 
				tintOppType = 1 AND
				tintOppstatus = 1 AND
				OpportunityMaster.numDomainId = @numDomainID AND
				(OpportunityMaster.bintCreatedDate >= @dtFromDate) AND
				(OpportunityMaster.bintCreatedDate <= @dtToDate OR @dtToDate IS NULL)) AS I
		WHERE
				(@numPageIndex = 0 OR @numPageSize = 0) OR 
				(RowNumber > ((@numPageIndex - 1) * @numPageSize) and RowNumber < ((@numPageIndex * @numPageSize) + 1))
	 
END
GO