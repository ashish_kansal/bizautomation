SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GeBizDocType')
DROP PROCEDURE USP_GeBizDocType
GO
CREATE PROCEDURE [dbo].[USP_GeBizDocType]                
@BizDocType as TINYINT,                  
@numDomainID as numeric(9)=0                   
as                    

IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
BEGIN
	EXEC USP_GetMasterListItems 27,@numDomainID
END
ELSE
BEGIN
  SELECT Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData 
  FROM BizDocFilter BDF INNER JOIN listdetails Ld ON BDF.numBizDoc=Ld.numListItemID
  left join listorder LO on Ld.numListItemID= LO.numListItemID AND Lo.numDomainId = @numDomainID
  WHERE Ld.numListID=27 AND BDF.tintBizocType=@BizDocType AND BDF.numDomainID=@numDomainID 
  AND (constFlag=1 or Ld.numDomainID=@numDomainID)
 END
GO
   