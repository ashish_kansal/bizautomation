SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_GetByDomainID')
DROP PROCEDURE dbo.USP_CustomQueryReport_GetByDomainID
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_GetByDomainID]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		numReportID,
		numDomainID,
		vcReportName,
		vcReportDescription,
		tintEmailFrequency
	FROM 
		CustomQueryReport
	WHERE
		numDomainID=@numDomainID
END