SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCustProjectManager')
DROP PROCEDURE USP_GetCustProjectManager
GO
-- =============================================
-- Author:		PRIYA
-- Create date: <2 FEB 2018>
-- Description:	<Get custProjectMGR email id on click of email from Project details screen>
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetCustProjectManager]
@ProID numeric ,
@numDomainId numeric

  AS

  Select ACI.vcEmail from ProjectsMaster PM
  INNER JOIN AdditionalContactsInformation ACI
  ON PM.numCustPrjMgr = ACI.numContactId
  WHERE PM.numProId = @ProID  AND PM.numDomainId = @numDomainId

 

GO


