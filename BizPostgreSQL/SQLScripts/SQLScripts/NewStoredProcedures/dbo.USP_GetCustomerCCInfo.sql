
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCustomerCCInfo')
DROP PROCEDURE USP_GetCustomerCCInfo
GO
CREATE PROCEDURE [dbo].[USP_GetCustomerCCInfo]  
 @numCCInfoId as bigint=0,
@numContactId as bigint=0,
@bitdefault as int=0,
@numDivisionID NUMERIC(18,0) = 0
as
begin
	if @numCCInfoID=0
	begin
		IF @bitdefault=0
		BEGIN
			IF @numContactID = 0 AND @numDivisionID > 0
			BEGIN
				SELECT TOP 1 @numContactId = numContactID FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID ORDER BY bitPrimaryContact DESC
			END

			SELECT 
				numCCInfoID,numContactId,vcCardHolder,vcCreditCardNo,vcCVV2,tintValidMonth,intValidYear,numCardTypeID,bitIsDefault ,vcData
			FROM 
				CustomerCreditCardInfo 
			LEFT JOIN 
				ListDetails 
			ON 
				ListDetails.numListItemID=CustomerCreditCardInfo.numCardTypeID
			WHERE 
				numContactId=@numContactId
			ORDER BY 
				bitIsDefault DESC,numCCInfoID ASC
			-- IMPORTY DEFAULT CARD SOULD BE RETURN FIRST
		END
		ELSE
		BEGIN
			select numCCInfoID,numContactId,vcCardHolder,vcCreditCardNo,vcCVV2,tintValidMonth,intValidYear,numCardTypeID,bitIsDefault ,vcData
			from CustomerCreditCardInfo left join ListDetails on ListDetails.numListItemID=CustomerCreditCardInfo.numCardTypeID
			where numContactId=@numContactId ORDER BY 
				bitIsDefault DESC,numCCInfoID ASC
			-- IMPORTY DEFAULT CARD SOULD BE RETURN FIRST
		end
	end
	else
	begin
		select numCCInfoID,numContactId,vcCardHolder,vcCreditCardNo,vcCVV2,tintValidMonth,intValidYear,numCardTypeID,bitIsDefault , vcData
		from CustomerCreditCardInfo left join ListDetails on ListDetails.numListItemID=CustomerCreditCardInfo.numCardTypeID
		where numCCInfoID=@numCCInfoID
		order by numCCInfoID desc
	end
		
end