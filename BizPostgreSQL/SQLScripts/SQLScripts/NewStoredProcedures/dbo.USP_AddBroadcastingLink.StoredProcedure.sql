GO
/****** Object:  StoredProcedure [dbo].[USP_AddBroadcastingLink]    Script Date: 30/07/2012 17:47:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_AddBroadcastingLink' ) 
    DROP PROCEDURE USP_AddBroadcastingLink
GO
CREATE PROCEDURE [dbo].[USP_AddBroadcastingLink] 
    @strBroadcastingLink AS TEXT = '',
    @numLinkId as numeric = 0,
    @numContactId as numeric = 0
   
AS 
BEGIN


IF EXISTS(SELECT numLinkId FROM [dbo].[BroadcastingLink] WHERE [numLinkId] = @numLinkId)
   BEGIN
         declare @intNoOfClick  int 
         declare @vcLinkIds    varchar(1000) 
         declare @numBroadcastID  numeric   
         
         SELECT  @numBroadcastID = numBroadcastID from BroadcastingLink where numLinkId = @numLinkId
         SELECT  @intNoOfClick = (intNoOfClick + 1) ,@vcLinkIds = isnull(vcLinkIds,'') + convert(varchar(20) ,@numLinkID) + ',' FROM [dbo].[BroadCastDtls] WHERE [numBroadcastID] = @numBroadcastID and [numContactID] = @numContactID 
         
         UPDATE BroadCastDtls set  vcLinkIds = @vcLinkIds , intNoOfClick = @intNoOfClick  where [numBroadcastId] = @numBroadcastID   and [numContactID] = @numContactID
   END
ELSE
   BEGIN
         DECLARE @hDoc4 INT                                                
         EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strBroadcastingLink

         INSERT  INTO BroadcastingLink(numBroadcastId,vcOriginalLink)
         SELECT  X.numBroadcastId,
                 X.vcOriginalLink
         FROM    ( SELECT    *
                   FROM      OPENXML (@hDoc4, '/LinkRecords/Table',2)
         WITH 
                 ( numBroadcastId NUMERIC(18),
                   vcOriginalLink VARCHAR(2000)
                 )
             ) X            
   END
END
--exec USP_AddBroadcastingLink @strBroadcastingLink=NULL,@numLinkId=83,@numContactId=613
