/****** Object:  StoredProcedure [dbo].[USP_GetEmailToCaseCheck]    Script Date: 07/26/2008 16:19:07 ******/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmailToCaseCheck')
DROP PROCEDURE USP_GetEmailToCaseCheck
GO

-- EXEC USP_GetEmailToCaseCheck 1,'kamal@bizautomation.com',0
Create PROCEDURE [dbo].[USP_GetEmailToCaseCheck]            
@numDomainID as numeric(9),            
@vcFromEmail VARCHAR(50),
@vcCaseNumber VARCHAR(50)
as             

   Declare @numcontactid numeric(9);SET @numcontactid=0;
   DECLARE @numDivisionId NUMERIC(9);SET @numDivisionId=0;
   DECLARE @numCaseId NUMERIC(9);SET @numCaseId=0;
   
   SELECT TOP 1 @numcontactid=ADC.numcontactid,@numDivisionId=ADC.numDivisionId from AdditionalContactsInformation ADC 
   JOIN DivisionMaster DM ON ADC.numDivisionId=DM.numDivisionId 
	WHERE ISNULL(DM.bitEmailToCase,0)=1 AND
	 DM.numDomainID=@numDomainID AND ADC.vcEmail = @vcFromEmail
		AND ADC.numDomainId = @numDomainId
		
	IF LEN(@vcCaseNumber)>0 AND @vcCaseNumber!='0' 
		AND ISNULL(@numcontactid,0) <> 0 AND ISNULL(@numDivisionId,0)<> 0
	BEGIN
		SELECT TOP 1 @numCaseId=numCaseId FROM dbo.Cases WHERE numDomainID=@numDomainID
		AND numDivisionID=@numDivisionId AND numContactId=@numcontactid
		AND vcCaseNumber LIKE '%' + @vcCaseNumber
	END
	
	SELECT ISNULL(@numcontactid,0) AS numcontactid,ISNULL(@numDivisionId,0) AS numDivisionId,ISNULL(@numCaseId,0) AS numCaseId
          
GO
    