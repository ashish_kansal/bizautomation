GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCommissionRule')
DROP PROCEDURE USP_ManageCommissionRule
GO
CREATE PROCEDURE [dbo].[USP_ManageCommissionRule]
    @numComRuleID NUMERIC,
    @vcCommissionName VARCHAR(100),
    @tintComAppliesTo TINYINT,
    @tintComBasedOn TINYINT,
    @tinComDuration TINYINT,
    @tintComType TINYINT,
    @numDomainID NUMERIC,
    @strItems TEXT,
    @tintAssignTo TINYINT,
    @tintComOrgType TINYINT
AS 
BEGIN TRY
BEGIN TRANSACTION
    IF @numComRuleID = 0 
    BEGIN
		INSERT  INTO CommissionRules
				(
					[vcCommissionName],
					[numDomainID]
				)
		VALUES  (
					@vcCommissionName,
					@numDomainID
				)
		SET @numComRuleID=@@identity
		Select @numComRuleID;
    END
    ELSE 
    BEGIN		
		DECLARE @isCommissionRuleUsed BIT = 0

		IF (SELECT COUNT(*) FROM BizDocComission WHERE numDomainId=@numDomainID AND numComRuleID=@numComRuleID) > 0
		BEGIN
			SET @isCommissionRuleUsed = 1
		END

		UPDATE  
			CommissionRules
        SET     
			[vcCommissionName] = @vcCommissionName,
            [tintComBasedOn] = CASE WHEN @isCommissionRuleUsed=1 THEN tintComBasedOn ELSE @tintComBasedOn END,
            [tinComDuration] = @tinComDuration,
            [tintComType] = CASE WHEN @isCommissionRuleUsed=1 THEN tintComType ELSE @tintComType END,
			[tintComAppliesTo] = @tintComAppliesTo,
			[tintComOrgType] = @tintComOrgType,
			[tintAssignTo] = @tintAssignTo
        WHERE   
			[numComRuleID] = @numComRuleID 
			AND [numDomainID] = @numDomainID

		IF @tintComAppliesTo = 3
		BEGIN
			DELETE FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID
		END
				
		
		DELETE FROM [CommissionRuleContacts] WHERE [numComRuleID] = @numComRuleID

		DECLARE @hDocItem INT
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
        
		IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
			IF @isCommissionRuleUsed = 0
			BEGIN
				DELETE FROM [CommissionRuleDtl] WHERE [numComRuleID] = @numComRuleID
				INSERT  INTO [CommissionRuleDtl]
						(
							numComRuleID,
							[intFrom],
							[intTo],
							[decCommission]
						)
						SELECT  @numComRuleID,
								X.[intFrom],
								X.[intTo],
								X.[decCommission]
						FROM    ( SELECT    *
									FROM      OPENXML (@hDocItem, '/NewDataSet/CommissionTable', 2)
											WITH ( intFrom DECIMAL(18,2), intTo DECIMAL(18,2), decCommission DECIMAL(18,2) )
								) X  

			END
			
            
			INSERT  INTO [CommissionRuleContacts]
                    (
                        numComRuleID,
                        numValue,bitCommContact
                    )
                    SELECT  @numComRuleID,
                            X.[numValue],X.[bitCommContact]
                    FROM    ( SELECT    *
                                FROM      OPENXML (@hDocItem, '/NewDataSet/ContactTable', 2)
                                        WITH ( numValue NUMERIC,bitCommContact bit)
                            ) X
			
            EXEC sp_xml_removedocument @hDocItem
        END
        

		DECLARE @bitItemDuplicate AS BIT = 0
		DECLARE @bitOrgDuplicate AS BIT = 0
		DECLARE @bitContactDuplicate AS BIT = 0

		/*Duplicate Rule values checking */
		-- CHECK IF OTHER COMMISSION RULE IS EXISTS IN ACCOUNT WIRH SAME OPTION IN SETP 2,3,4 and 5
		IF (SELECT COUNT(*) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID) > 0
		BEGIN
			-- ITEM
			IF @tintComAppliesTo = 1
			BEGIN
				-- ITEM ID
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleItems 
					WHERE 
						tintType=1
						AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitItemDuplicate = 1
				END
			END
			ELSE IF @tintComAppliesTo = 2
			BEGIN
				-- ITEM CLASSIFICATION
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleItems 
					WHERE 
						tintType=2
						AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitItemDuplicate = 1
				END
			END
			ELSE IF @tintComAppliesTo = 3
			BEGIN
				-- ALL ITEMS
				SET @bitItemDuplicate = 1
			END

			--ORGANIZATION
			IF @tintComOrgType = 1
			BEGIN
				-- ORGANIZATION ID
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleOrganization 
					WHERE 
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF @tintComOrgType = 2
			BEGIN
				-- ORGANIZATION RELATIONSHIP OR PROFILE
				IF (SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleOrganization 
					WHERE 
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numProfile IN (SELECT ISNULL(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitOrgDuplicate = 1
				END
			END
			ELSE IF  @tintComOrgType = 3
			BEGIN
				-- ALL ORGANIZATIONS
				SET @bitOrgDuplicate = 1
			END

			--CONTACT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					CommissionRuleContacts 
				WHERE 
					numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleContacts WHERE numComRuleID=@numComRuleID) 
					AND numComRuleID IN (SELECT 
											ISNULL(numComRuleID,0) 
										FROM 
											CommissionRules 
										WHERE 
											numDomainID=@numDomainID 
											AND tintComAppliesTo = @tintComAppliesTo
											AND 1 = (CASE tintComAppliesTo 
														WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems CRIInner WHERE tintType=1 AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) AND CRIInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems CRIInner WHERE tintType=2 AND numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID) AND CRIInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 3 THEN 1 
													END)
											AND tintComOrgType=@tintComOrgType
											AND 1 = (CASE tintComOrgType 
														WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization CROInner WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND CROInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization CROInner WHERE numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND numProfile IN (SELECT ISNULL(numProfile,0) FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID) AND CROInner.numComRuleID = CommissionRuleContacts.numComRuleID) > 0 THEN 1 ELSE 0 END)
														WHEN 3 THEN 1 
													END)
											AND tintAssignTo = @tintAssignTo 
											AND numComRuleID <> @numComRuleID
										)
					) > 0
			BEGIN
				SET @bitContactDuplicate = 1
			END
		END

		IF (ISNULL(@bitItemDuplicate,0) = 1 AND ISNULL(@bitOrgDuplicate,0) = 1 AND ISNULL(@bitContactDuplicate,0) = 1)
		BEGIN
			RAISERROR ( 'DUPLICATE',16, 1 )
			RETURN ;
		END

		SELECT  @numComRuleID
    END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH