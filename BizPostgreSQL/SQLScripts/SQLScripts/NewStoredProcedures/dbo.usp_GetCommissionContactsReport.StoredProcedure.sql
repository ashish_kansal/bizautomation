SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
             
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetCommissionContactsReport')
DROP PROCEDURE usp_GetCommissionContactsReport
GO
CREATE PROCEDURE [dbo].[usp_GetCommissionContactsReport]                  
@numDivisionID as numeric(9),
@numDomainID AS NUMERIC(9)                 
as  

 
 
DECLARE @tintComAppliesTo TINYINT
SELECT  @tintComAppliesTo = ISNULL(tintComAppliesTo,0) FROM domain WHERE numDomainID = @numDomainID

if @tintComAppliesTo=1    
begin   

select I.vcItemName + CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END + ' - Commission Report' AS vcHeader,                 
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' + CAST(cus.numItemCode AS VARCHAR(10)) + ',@tintAssignTo=' + CAST(cus.tintAssignTo AS VARCHAR(10)) +',@bitCommContact=1' AS textQueryGrp       
from CommissionReports Cus JOIN Item I ON Cus.numItemCode=I.numItemCode        
where Cus.numDomainID=@numDomainID  AND Cus.bitCommContact=1 AND Cus.numContactID=@numDivisionID  
    
end    
ELSE if @tintComAppliesTo=2    
begin    

select dbo.[GetListIemName]([numItemCode]) + CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END + ' - Commission Report' AS vcHeader,                 
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' + CAST(cus.numItemCode AS VARCHAR(10)) + ',@tintAssignTo=' + CAST(cus.tintAssignTo AS VARCHAR(10)) +',@bitCommContact=1' AS textQueryGrp       
from                 
 CommissionReports Cus         
where Cus.numDomainID=@numDomainID  AND Cus.bitCommContact=1 AND Cus.numContactID=@numDivisionID  
 
end  
ELSE if @tintComAppliesTo=3    
begin    

select 'All Items' + CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END + ' - Commission Report' AS vcHeader,                 
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' + CAST(cus.numItemCode AS VARCHAR(10)) + ',@tintAssignTo=' + CAST(cus.tintAssignTo AS VARCHAR(10)) +',@bitCommContact=1' AS textQueryGrp       
from                 
 CommissionReports Cus         
 where Cus.numDomainID=@numDomainID  AND Cus.bitCommContact=1 AND Cus.numContactID=@numDivisionID  
 
end 


GO
