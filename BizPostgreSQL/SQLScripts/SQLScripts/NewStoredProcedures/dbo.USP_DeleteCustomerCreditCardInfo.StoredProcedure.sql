/****** Object:  StoredProcedure [dbo].[USP_GetCustomerCreditCardInfo]    Script Date: 05/07/2009 22:00:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteCustomerCreditCardInfo' ) 
    DROP PROCEDURE USP_DeleteCustomerCreditCardInfo
GO
CREATE PROCEDURE [dbo].[USP_DeleteCustomerCreditCardInfo]
  @numCCInfoID INT
AS 
    BEGIN
  DELETE FROM dbo.CustomerCreditCardInfo WHERE numCCInfoID=@numCCInfoID
  
    END
