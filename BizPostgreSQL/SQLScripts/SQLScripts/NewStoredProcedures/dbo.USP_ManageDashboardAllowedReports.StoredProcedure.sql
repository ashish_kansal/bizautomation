SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageDashboardAllowedReports')
DROP PROCEDURE USP_ManageDashboardAllowedReports
GO
CREATE PROCEDURE [dbo].[USP_ManageDashboardAllowedReports]
@numDomainId as numeric(9) ,
@numGroupId as numeric(9),
@strReportIds as varchar(2000),
@strKPIGroupReportIds as varchar(2000),
@strPredefinedReportIds AS VARCHAR(MAX)
as
BEGIN
	DELETE ReportDashboardAllowedReports where numDomainId=@numDomainId AND numGrpId=@numGroupId

	IF @strReportIds<>''
	BEGIN  
		INSERT INTO ReportDashboardAllowedReports (numDomainId,numGrpId,numReportId,tintReportCategory)
		select @numDomainId,@numGroupId,x.items,0 from (select * from dbo.split(@strReportIds,','))x
	END

	IF @strKPIGroupReportIds<>''
	BEGIN  
		INSERT INTO ReportDashboardAllowedReports (numDomainId,numGrpId,numReportId,tintReportCategory)
		select @numDomainId,@numGroupId,x.items,1 from (select * from dbo.split(@strKPIGroupReportIds,','))x
	END

	IF @strPredefinedReportIds<>''
	BEGIN  
		INSERT INTO ReportDashboardAllowedReports (numDomainId,numGrpId,numReportId,tintReportCategory)
		select @numDomainId,@numGroupId,x.items,2 from (select * from dbo.split(@strPredefinedReportIds,','))x

		DELETE FROM 
			ReportDashboard 
		WHERE 
			numUserCntID IN (SELECT numUserDetailId FROM UserMaster WHERE numDomainID=@numDomainId AND numGroupID=@numGroupId)	 
			AND numReportID IN (SELECT 
									numReportID 
								FROM 
									ReportListMaster 
								WHERE 
									ISNULL(numDomainID,0)=0 
									AND ISNULL(bitDefault,0)=1 
									AND intDefaultReportID NOT IN (SELECT 
																		numReportID 
																	FROM 
																		ReportDashboardAllowedReports 
																	WHERE 
																		numDomainID=@numDomainId 
																		AND numGrpID=@numGroupId 
																		AND tintReportCategory=2)
								)
	END
END
