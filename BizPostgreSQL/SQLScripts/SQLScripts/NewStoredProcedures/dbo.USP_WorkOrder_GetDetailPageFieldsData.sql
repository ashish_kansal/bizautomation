GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetDetailPageFieldsData')
DROP PROCEDURE USP_WorkOrder_GetDetailPageFieldsData
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetDetailPageFieldsData]
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
AS
BEGIN 
	SELECT
		WorkOrder.numWOId
		,ISNULL(OpportunityMaster.numOppId,0) AS numOppId
		,(CASE 
			WHEN OpportunityMaster.numOppId IS NOT NULL 
			THEN CONCAT('<a href="../opportunity/frmOpportunities.aspx?frm=deallist&amp;OpID=',OpportunityMaster.numOppId,'">',OpportunityMaster.vcPOppName,' (',ISNULL(CompanyInfo.vcCompanyName,'-'),')</a>')
			ELSE ''
		END) AS vcPoppName
		,ISNULL(WorkOrder.numAssignedTo,0) numAssignedTo
		,dbo.fn_GetContactName(WorkOrder.numAssignedTo) numAssignedToName
		,ISNULL(Warehouses.vcWareHouse,'') vcWarehouse
		,ISNULL(WorkOrder.vcInstruction,'') vcInstruction
		,DATEADD(MINUTE,@ClientTimeZoneOffset * -1,WorkOrder.dtmStartDate) dtmStartDate
		,DATEADD(MINUTE,@ClientTimeZoneOffset * -1,WorkOrder.dtmEndDate) dtmEndDate
		,DATEADD(MINUTE,@ClientTimeZoneOffset * -1,(SELECT MIN(SPDTTL.dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID IN (SELECT SPDT.numTaskID FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WorkOrder.numWOId) AND tintAction=1)) dtActualStartDate
		,dbo.GetProjectedFinish(@numDomainID,2,@numWOID,0,0,NULL,@ClientTimeZoneOffset,0) AS vcProjectedFinish
		,'<label class="lblWorkOrderStatus"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>' vcWorkOrderStatus
		,ISNULL(WorkOrder.numBuildPlan,0) numBuildPlan
		,dbo.GetListIemName(WorkOrder.numBuildPlan) numBuildPlanName
		,WorkOrder.numQtyItemsReq
	FROM
		WorkOrder
	INNER JOIN
		WareHouseItems
	ON
		WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		OpportunityMaster
	ON
		WorkOrder.numOppId = OpportunityMaster.numOppId
	LEFT JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionID = DivisionMaster.numDivisionID
	LEFT JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId = @numWOID
END
GO