/****** Object:  StoredProcedure [dbo].[USP_DeleteOpportunitySalesTemplate]    Script Date: 09/01/2009 01:16:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteOpportunitySalesTemplate')
DROP PROCEDURE USP_DeleteOpportunitySalesTemplate
GO
CREATE PROCEDURE [dbo].[USP_DeleteOpportunitySalesTemplate]
	@numSalesTemplateID numeric(9),
	@numDomainID numeric(9)
AS

DELETE FROM [SalesTemplateItems] WHERE [numSalesTemplateID]=@numSalesTemplateID AND [numDomainID]=@numDomainID

DELETE FROM OpportunitySalesTemplate
WHERE [numSalesTemplateID] = @numSalesTemplateID AND numDomainID =@numDomainID