SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetChildMembership')
DROP PROCEDURE dbo.USP_Item_GetChildMembership
GO
CREATE PROCEDURE [dbo].[USP_Item_GetChildMembership]
    (
	  @numDomainID NUMERIC(18,0),
      @numItemCode NUMERIC(18,0)
    )
AS 
BEGIN
	SELECT 
		Item.numItemCode,
		Item.vcItemName,
		CASE 
			WHEN ISNULL(Item.bitAssembly,0)=1 THEN 'Assembly' 
			WHEN ISNULL(Item.bitKitParent,0)=1 THEN 'Kit'
		END AS ParentItemType,
		CAST(ItemDetails.numQtyItemsReq AS VARCHAR(18)) AS numQtyItemsReq
	FROM 
		ItemDetails 
	INNER JOIN
		Item
	ON
		ItemDetails.numItemKitID = Item.numItemCode
	WHERE 
		numChildItemID = @numItemCode
	UNION ALL
	SELECT
		Item.numItemCode,
		ItemGroups.vcItemGroup AS vcItemName,
		'Item Group' AS ParentItemType,
		'' AS numQtyItemsReq
	FROM
		Item
	INNER JOIN
		ItemGroups
	ON
		Item.numItemGroup = ItemGroups.numItemGroupID
	WHERE
		Item.numDomainID = @numDomainID 
		AND numItemCode = @numItemCode
	UNION ALL
	SELECT 
		Item.numItemCode,
		Item.vcItemName,
		'Related Items' AS ParentItemType,
		'' AS numQtyItemsReq
	FROM 
		SimilarItems 
	INNER JOIN
		Item
	ON
		SimilarItems.numParentItemCode = Item.numItemCode
	WHERE 
		SimilarItems.numDomainID = @numDomainID
		AND SimilarItems.numItemCode = @numItemCode
END
