GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ContractManagment_GetDetail' ) 
    DROP PROCEDURE USP_ContractManagment_GetDetail
GO
CREATE PROCEDURE USP_ContractManagment_GetDetail  
(  
 @numDomainID NUMERIC(18,0),  
 @numContractID NUMERIC(18,0)    
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
  DECLARE @TEMP TABLE
  (
	RecordID NUMERIC(18,0),
	RecordType VARCHAR(10),
	RecordName VARCHAR(500),
	Amount NUMERIC(18,2),
	HoursUsed NUMERIC(18,2) 
  )

  --GET CASES WHERE CONTRACT IS ASSIGNED
  INSERT INTO @TEMP SELECT numCaseId,'Case',vcCaseNumber,0,0 FROM Cases WHERE ISNULL(numContractId,0) = @numContractID AND numDomainID=@numDomainID
  
  --GET PROJECTS WHERE CONTRACT IS ASSIGNED 

	INSERT INTO 
		@TEMP 
	SELECT 
		ProjectsMaster.numProId,
		'Project', 
		ProjectsMaster.vcProjectName,
		ISNULL(SUM(TE.monAmount),0),
		ISNULL(SUM(TE.HoursUsed),0)
	FROM 
		ProjectsMaster 
	OUTER APPLY
		(
			SELECT
				monAmount,
				ISNULL(CONVERT(NUMERIC(18,2), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, dtFromDate, dtToDate)) / 60 ))),0) AS HoursUsed
			FROM 
				TimeAndExpense
			WHERE
				ISNULL(numProId,0) = ProjectsMaster.numProId
		) AS TE
	WHERE 
		ProjectsMaster.numContractId = @numContractID  AND numDomainID=@numDomainID
	GROUP BY
		ProjectsMaster.numProId,
		ProjectsMaster.vcProjectName

	SELECT * FROM @TEMP
END  


