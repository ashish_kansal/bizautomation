SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Activity_FutureDate')
DROP PROCEDURE Activity_FutureDate
GO
CREATE PROCEDURE [dbo].[Activity_FutureDate]  
 @StartDateTimeUtc datetime, -- the start date before which no activities are retrieved  
 @OrganizerName nvarchar(64) -- resource name (will be used to lookup ID)  
AS  
BEGIN  
 --  
 -- First step is to get the ResourceID for the primary resource name supplied.  
 --  
 DECLARE @ResourceID AS integer;
if isnumeric(@OrganizerName) =1
begin
   set @ResourceID=convert(numeric(9), @OrganizerName)
end
else
begin 
	set @ResourceID =-999	
end 

 SELECT   
  [Activity].[AllDayEvent],   
  [Activity].[ActivityDescription],   
  [Activity].[Duration],   
  [Activity].[Location],   
  [Activity].[ActivityID],   
  [Activity].[StartDateTimeUtc],  
  --DATEADD(mi, [Activity].[Duration]/60, [Activity].[StartDateTimeUtc] )  AS EndDateTimeUtcm,
  [Activity].[Subject],   
  [Activity].[EnableReminder],   
  [Activity].[ReminderInterval],  
  [Activity].[ShowTimeAs],  
  [Activity].[Importance],  
  [Activity].[Status],  
  [Activity].[RecurrenceID],  
  [Activity].[VarianceID],  
  [Activity].[_ts],  
  [Activity].[ItemId],  
  [Activity].[ChangeKey],Activity.GoogleEventId  
 FROM   
  [Activity] INNER JOIN [ActivityResource] ON [Activity].[ActivityID] = [ActivityResource].[ActivityID]  
 WHERE   
  (([Activity].[ActivityID] = [ActivityResource].[ActivityID]) AND ([ActivityResource].[ResourceID] = @ResourceID)) 
  AND (( [Activity].[StartDateTimeUtc] >= @StartDateTimeUtc 
 OR ( [Activity].[RecurrenceID] <> -999 )) AND ([Activity].[OriginalStartDateTimeUtc] IS NULL));  
END
GO
