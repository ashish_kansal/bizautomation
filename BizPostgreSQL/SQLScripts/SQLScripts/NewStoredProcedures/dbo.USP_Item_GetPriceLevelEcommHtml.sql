GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetPriceLevelEcommHtml')
DROP PROCEDURE USP_Item_GetPriceLevelEcommHtml
GO
CREATE PROCEDURE [dbo].[USP_Item_GetPriceLevelEcommHtml]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numItemCode AS NUMERIC(18,0)
AS  
BEGIN  
	DECLARE @vcHtml VARCHAR(MAX) = ''

	IF (SELECT numDefaultSalesPricing FROM Domain WHERE numDomainId=@numDomainID) = 1
	BEGIN
		DECLARE @numCurrencyID NUMERIC(18,0)

		DECLARE @TEMPPriceLevel TABLE
		(
			ID INT IDENTITY(1,1)
			,numQtyFrom FLOAT
			,numQtyTo FLOAT
			,vcPrice VARCHAR(30)
			,bitLastRow BIT
		)

		-- FIRST CHECK SITE CURRENY
		SELECT @numCurrencyID=numCurrencyID FROM Sites WHERE numDomainID=@numDomainID AND numSiteID=@numSiteID

		IF NOT EXISTS (SELECT numPricingID FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0 AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0))
		BEGIN
			SELECT @numCurrencyID=numCurrencyID FROM Domain WHERE numDomainId=@numDomainID
		END

		IF NOT EXISTS (SELECT numPricingID FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0 AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0))
		BEGIN
			SET @numCurrencyID = 0
		END

		IF EXISTS (SELECT numPricingID FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0 AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0))
		BEGIN
			DECLARE @numBaseUnit NUMERIC(18,0)
			DECLARE @numSaleUnit NUMERIC(18,0)
			DECLARE @monListPrice DECIMAL(20,5)
			DECLARE @monVendorCost DECIMAL(20,5)
			DECLARE @fltExchangeRate FLOAT 
			DECLARE @fltUOMConversionFactor FLOAT

			SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=ISNULL(@numCurrencyID,0)),1)

			IF ISNULL(@fltExchangeRate,0) = 0
			BEGIN
				SET @fltExchangeRate = 1
			END

			SELECT
				@numBaseUnit = ISNULL(numBaseUnit,0),
				@numSaleUnit = ISNULL(numSaleUnit,0),
				@monListPrice = (CASE 
									WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0))  
									THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0)),0)
									ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(Item.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(Item.monListPrice,0) END)
								END),
				@monVendorCost = ISNULL((SELECT (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monCost,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monCost,0) END) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) FROM Vendor WHERE numVendorID=Item.numVendorID AND Vendor.numItemCode=Item.numItemCode),0)
			FROM
				Item
			WHERE
				numDomainID = @numDomainID
				AND numItemCode= @numItemCode

			SET @fltUOMConversionFactor = dbo.fn_UOMConversion(@numSaleUnit,@numItemCode,@numDomainId,@numBaseUnit)
			IF ISNULL(@fltUOMConversionFactor,0) = 0
			BEGIN
				SET @fltUOMConversionFactor = 1
			END

			SET @monListPrice = @monListPrice * @fltUOMConversionFactor
			SET @monVendorCost = @monVendorCost * @fltUOMConversionFactor


			DECLARE @varCurrSymbol VARCHAR(100)
			SET @varCurrSymbol = ISNULL((SELECT varCurrSymbol FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),'$')

			INSERT INTO @TEMPPriceLevel
			(
				numQtyFrom
				,numQtyTo
				,vcPrice
			)
			SELECT
				intFromQty
				,intToQty
				,(CASE tintRuleType
					WHEN 1 -- Deduct From List Price
					THEN
						(CASE tintDiscountType 
							WHEN 1 -- PERCENT
							THEN FORMAT(ISNULL((CASE WHEN @monListPrice > 0 THEN @monListPrice - (@monListPrice * (ISNULL(decDiscount,0)/100)) ELSE 0 END),0),'#,##0.#####')
							WHEN 2 -- FLAT AMOUNT
							THEN CONCAT(FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END),'#,##0.#####'),' off on total amount')
							WHEN 3 -- NAMED PRICE
							THEN FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN (CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) * @fltUOMConversionFactor) ELSE (ISNULL(decDiscount,0) * @fltUOMConversionFactor) END),'#,##0.#####')
						END)
					WHEN 2 -- Add to primary vendor cost
					THEN
						(CASE tintDiscountType 
							WHEN 1  -- PERCENT
							THEN FORMAT(@monVendorCost + (@monVendorCost * (ISNULL(decDiscount,0)/100)),'#,##0.#####')
							WHEN 2 -- FLAT AMOUNT
							THEN CONCAT(FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) ELSE ISNULL(decDiscount,0) END),'#,##0.#####'),' extra on total amount')
							WHEN 3 -- NAMED PRICE
							THEN FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN (CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) * @fltUOMConversionFactor) ELSE (ISNULL(decDiscount,0) * @fltUOMConversionFactor) END),'#,##0.#####')
						END)
					WHEN 3 -- Named price
					THEN
						FORMAT((CASE WHEN @numCurrencyID <> numCurrencyID THEN  (CAST((ISNULL(decDiscount,0) / @fltExchangeRate) AS INT) * @fltUOMConversionFactor) ELSE (ISNULL(decDiscount,0) * @fltUOMConversionFactor) END),'#,##0.#####')
					END
				) 
			FROM
				PricingTable
			WHERE
				numItemCode=@numItemCode
				AND ISNULL(numPriceRuleID,0) = 0
				AND ISNULL(numCurrencyID,0)=ISNULL(@numCurrencyID,0)
			ORDER BY 
				numPricingID
		END

		IF EXISTS (SELECT * FROM @TEMPPriceLevel)
		BEGIN
			DECLARE @vcUnitName VARCHAR(100)
			SET @vcUnitName = ISNULL((SELECT vcUnitName FROM UOM WHERE numDomainId=@numDomainID AND numUOMId=@numSaleUnit),'each')

			SET @vcHtml = CONCAT('<table class="table table-bordered table-pricelevel"><tr><th>Qty (',@vcUnitName,')</th><th>Price per ',@vcUnitName,'</th></tr>')
			SET @vchtml = CONCAT(@vchtml,STUFF((SELECT
													CONCAT('<tr><td>',FORMAT(numQtyFrom,'#,##0.#####'),' - ',FORMAT(numQtyTo,'#,##0.#####'),'</td><td><span>',@varCurrSymbol,'</span>',vcPrice,'</td></tr>')
												FROM 
													@TEMPPriceLevel 
												ORDER BY
													ID
												FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''))

			SET @vcHtml = CONCAT(@vcHtml,'</table>')
		END
	END

	SELECT @vcHtml AS vcHtml
END
GO