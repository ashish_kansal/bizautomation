/****** Object:  StoredProcedure [dbo].[USP_DeleteShippingReport]    Script Date: 05/07/2009 17:40:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteShippingReport')
DROP PROCEDURE USP_DeleteShippingReport
GO
CREATE PROCEDURE [dbo].[USP_DeleteShippingReport]
                @ShippingReportId NUMERIC(9)
AS
BEGIN
	
    DELETE FROM [ShippingReportItems]
    WHERE ShippingReportItemId IN (SELECT [ShippingReportItemId] FROM [ShippingReportItems] WHERE [numShippingReportId] =@ShippingReportId)
    
	DELETE  FROM [ShippingBox] WHERE   [numShippingReportId] = @ShippingReportId
        
    DELETE FROM [ShippingReport] WHERE numShippingReportId =@ShippingReportId
END
