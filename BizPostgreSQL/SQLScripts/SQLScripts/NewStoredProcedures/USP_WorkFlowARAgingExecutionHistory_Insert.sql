GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkFlowARAgingExecutionHistory_Insert')
DROP PROCEDURE USP_WorkFlowARAgingExecutionHistory_Insert
GO
  
-- =============================================  
-- Author:  <Author,,Priya Sharma>  
-- Create date: <Create Date,,5thSep2018>  
-- Description: <Description,,To save records that are executed for a day>  
-- =============================================  
Create PROCEDURE [dbo].[USP_WorkFlowARAgingExecutionHistory_Insert]   
 -- Add the parameters for the stored procedure here  
 
	  @numDomainID numeric(18,0) 
	 ,@numRecordID NUMERIC(18, 0)
	 ,@numFormID NUMERIC(18, 0)
	 ,@numWFID NUMERIC(18,0)

AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
  
   INSERT INTO WorkFlowARAgingExecutionHistory
    (
		numDomainID
		,dtCreatedDate
		,numRecordID
		,numFormID
		,numWFID
	)  
    VALUES     
	 (
		@numDomainID
		,GETUTCDATE()
		,@numRecordID
		,@numFormID
		,@numWFID
	)   
END  