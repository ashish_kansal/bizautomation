

--Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemTaxTypes')
DROP PROCEDURE USP_ItemTaxTypes
GO
CREATE PROCEDURE USP_ItemTaxTypes
@numItemCode as numeric(9),
@numDomainID as numeric(9)
AS
BEGIN
	SELECT 
		TaxItems.numTaxItemID,
		vcTaxName,
		ISNULL(bitApplicable,0) AS bitApplicable 
	FROM 
		TaxItems
	LEFT JOIN 
		ItemTax 
	ON 
		ItemTax.numTaxItemID=TaxItems.numTaxItemID 
		AND numItemCode=@numItemCode
	WHERE 
		numDomainID=@numDomainID 
	ORDER BY 
		TaxItems.numTaxItemID
END

