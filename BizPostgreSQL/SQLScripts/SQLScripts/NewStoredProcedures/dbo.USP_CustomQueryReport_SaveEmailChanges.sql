SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_SaveEmailChanges')
DROP PROCEDURE dbo.USP_CustomQueryReport_SaveEmailChanges
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_SaveEmailChanges]
	@numReportID NUMERIC(18,0),
	@vcReportName VARCHAR(300),
	@vcReportDescription VARCHAR(300),
	@vcEmailTo VARCHAR(1000)
AS 
BEGIN
	UPDATE
		CustomQueryReport
	SET
		vcReportName=@vcReportName,
		vcReportDescription=@vcReportDescription,
		vcEmailTo=@vcEmailTo
	WHERE	
		numReportID=@numReportID	
END