SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShippingEcommerce')
DROP PROCEDURE USP_GetEstimateShippingEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetEstimateShippingEcommerce]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	IF @numDomainID IN (187,214)
	BEGIN
		SELECT
			*
		FROM
		(
			SELECT 
				intNsoftEnum AS numServiceTypeID
				,0 AS numRuleID
				,numShippingCompanyID
				,vcServiceName
				,monRate
				,intNsoftEnum
				,intFrom
				,intTo
				,fltMarkup
				,bitMarkUpType
				,bitEnabled
				,S.vcItemClassification
		,SR.bitItemClassification
		,SR.numWareHouseID
		,SR.numSiteID
		,SR.numRuleID AS numItemClassificationRuleID
			FROM 
				ShippingServiceTypes  AS S
				LEFT JOIN 
		ListDetails AS L
	ON
		S.numShippingCompanyID=L.numListItemID
	LEFT JOIN ShippingRules AS SR ON S.numRuleID=SR.numRuleID
			WHERE 
				S.numDomainID=@numDomainID 
				AND ISNULL(bitEnabled,0)=1
			UNION
			SELECT 
				-1 numServiceTypeID
				,0 AS numRuleID
				,92 numShippingCompanyID
				,'Will call' vcServiceName
				,0 monRate
				,0 intNsoftEnum
				,0 intFrom
				,0 intTo
				,0 fltMarkup
				,0 bitMarkUpType
				,1 bitEnabled
				,'' AS vcItemClassification
		,0 bitItemClassification
		,0 numWareHouseID
		,0 numSiteID
		,0 numItemClassificationRuleID
		) TEMP
		ORDER BY
			numShippingCompanyID,intNsoftEnum
	END
	ELSE
	BEGIN
		SELECT 
			numServiceTypeID
			,0 AS numRuleID
			,numShippingCompanyID
			,vcServiceName
			,monRate
			,intNsoftEnum
			,intFrom
			,intTo
			,fltMarkup
			,bitMarkUpType
			,bitEnabled
			,S.vcItemClassification
		,SR.bitItemClassification
		,SR.numWareHouseID
		,SR.numSiteID
		,SR.numRuleID AS numItemClassificationRuleID
		FROM 
			ShippingServiceTypes  AS S
			LEFT JOIN 
		ListDetails AS L
	ON
		S.numShippingCompanyID=L.numListItemID
	LEFT JOIN ShippingRules AS SR ON S.numRuleID=SR.numRuleID
		WHERE 
			S.numDomainID=@numDomainID 
			AND ISNULL(bitEnabled,0)=1
	END
END      
GO