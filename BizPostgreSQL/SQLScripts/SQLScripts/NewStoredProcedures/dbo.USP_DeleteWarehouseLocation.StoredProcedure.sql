GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteWarehouseLocation' ) 
    DROP PROCEDURE USP_DeleteWarehouseLocation
GO

CREATE PROCEDURE [dbo].USP_DeleteWarehouseLocation
    @numDomainID AS NUMERIC(18,0),
    @numWLocationID AS NUMERIC(18,0)
AS 
BEGIN
	IF EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numWLocationID=@numWLocationID)
	BEGIN
		RAISERROR('WAREHOUSE_LOCATION_USED_FOR_ITEM',16,1)
		RETURN
	END
	ELSE
	BEGIN
		DELETE FROM dbo.WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=@numWLocationID
	END
END 
GO 