
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateAPISettingDate]    Script Date: 05/07/2009 17:51:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateAPISettingDate')
DROP PROCEDURE USP_UpdateAPISettingDate
GO
CREATE PROCEDURE [dbo].[USP_UpdateAPISettingDate]
                @WebApiId    INT,
                @numDomainId NUMERIC(9),
                @tintMode     TINYINT
AS
  BEGIN
    IF (@tintMode = 1) --OrdersLastUpdatedDate'
      BEGIN
        UPDATE [WebAPIDetail]
        SET    vcNinthFldValue = GETUTCDATE()
        WHERE  [WebApiId] = @WebApiId
               AND [numDomainId] = @numDomainId
      END
    ELSE
      IF (@tintMode = 0) --ProductsLastUpdatedDate
        BEGIN
          UPDATE [WebAPIDetail]
          SET    vcTenthFldValue = GETUTCDATE()
          WHERE  [WebApiId] = @WebApiId
                 AND [numDomainId] = @numDomainId
        END
  ELSE
      IF (@tintMode = 2) --ProductsLastUpdatedDate_ItemAPI
        BEGIN
--        SELECT * FROM dbo.WebAPI
--        SELECT * FROM WebAPIDetail
          UPDATE [WebAPIDetail]
          SET    vcEleventhFldValue = GETUTCDATE()
          WHERE  [WebApiId] = @WebApiId
                 AND [numDomainId] = @numDomainId
        END
  ELSE
      IF (@tintMode = 3) --InventoryLastUpdatedDate
        BEGIN
          UPDATE [WebAPIDetail]
          SET    vcTwelfthFldValue = GETDATE()
          WHERE  [WebApiId] = @WebApiId
                 AND [numDomainId] = @numDomainId
        END
   ELSE
      IF (@tintMode = 4) --SyncProductListingInBiz
        BEGIN
          UPDATE [WebAPIDetail]
          SET    vcThirteenthFldValue = GETUTCDATE()
          WHERE  [WebApiId] = @WebApiId
                 AND [numDomainId] = @numDomainId
        END
	ELSE
      IF (@tintMode = 5) --Last Updated FBA Order Id list(Only Of Amazon)
        BEGIN
          UPDATE [WebAPIDetail]
          SET    vcSixteenthFldValue = GETDATE()
          WHERE  [WebApiId] = @WebApiId
                 AND [numDomainId] = @numDomainId
        END
  END
