GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetStages')
DROP PROCEDURE dbo.USP_WorkOrder_GetStages
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetStages]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@vcMilestoneName VARCHAR(500)
)
AS 
BEGIN
	SELECT 
		StagePercentageDetails.numStageDetailsId
		,StagePercentageDetails.vcStageName AS vcStageName
		,dbo.GetTotalProgress(@numDomainID,@numWOID,1,3,'',StagePercentageDetails.numStageDetailsId) numTotalProgress
	FROM
		StagePercentageDetails
	WHERE
		StagePercentageDetails.numDomainID = @numDomainID
		AND StagePercentageDetails.numWorkOrderId = @numWOID
		AND vcMileStoneName = @vcMilestoneName
END
GO