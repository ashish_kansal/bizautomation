/****** Object:  StoredProcedure [dbo].[USP_GetProjectTeamRights]    Script Date: 09/17/2010 17:35:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectTeamRights')
DROP PROCEDURE USP_GetProjectTeamRights
GO
CREATE PROCEDURE [dbo].[USP_GetProjectTeamRights]
    @numDomainID AS NUMERIC(9) = 0,
    @numProId AS NUMERIC(9) = 0,
	@tintUserType as tinyint
AS 

IF @tintUserType=1 --Internal User
BEGIN	
 SELECT A.numContactID,A.numContactID AS numContactId,ISNULL(A.vcFirstName,'')+' '+ISNULL(A.vcLastName,'') as vcUserName
,isnull(TR.numRights,1) numRights,isnull(TR.numProjectRole,0) numProjectRole,A.vcEmail AS Email
 from UserMaster UM         
 join AdditionalContactsInformation A on UM.numUserDetailId=A.numContactID        
 left join ProjectTeamRights TR on TR.numContactId=A.numContactId and TR.numProId=@numProId and TR.tintUserType=1	        
 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID    
 AND [bitActivateFlag]=1
END

Else IF @tintUserType=2 --External User
BEGIN	
 SELECT A.numContactID,A.numContactID AS numContactId,ISNULL(A.vcFirstName,'')+' '+ISNULL(A.vcLastName,'') as vcUserName
,isnull(TR.numRights,0) numRights,isnull(TR.numProjectRole,0) numProjectRole,
 isnull(C.vcCompanyName,'-') as vcCompanyName,A.vcEmail AS Email
 from ProjectTeamRights TR join AdditionalContactsInformation A on TR.numContactId=A.numContactId 
join DivisionMaster DM on DM.numDivisionID=A.numDivisionID
join CompanyInfo C on DM.numCompanyID=C.numCompanyID	        
 where A.numDomainID=@numDomainID and TR.numProId=@numProId and TR.tintUserType=2   
END


--Commented by chintan reason :Temperary change Bring it back when portal is ready for permission based projects
--Bug id - 386
--Union All Select 0,'Portal Contacts',isnull((select TR.numRights from 
--ProjectTeamRights TR where TR.numContactId=0 and TR.numProId=@numProId),0)

--UNION ALL
-- select  A.numContactID,A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')'as vcUserName,
--isnull((select TR.numRights from 
--ProjectTeamRights TR where TR.numContactId=A.numContactId  and TR.numProId=@numProId),0) numRights  
--		from CommissionContacts CC JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
--		JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
--		join CompanyInfo C on C.numCompanyID=D.numCompanyID  
--		where CC.numDomainID=@numDomainID 