GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSimilarItem')
DROP PROCEDURE USP_GetSimilarItem
GO
CREATE PROCEDURE USP_GetSimilarItem
@numDomainID NUMERIC(9),
@numParentItemCode NUMERIC(9),
@byteMode TINYINT,
@vcCookieId VARCHAR(MAX)='',
@numUserCntID NUMERIC(18)=0,
@numSiteID NUMERIC(18,0)=0,
@numOppID NUMERIC(18,0) = 0,
@numWarehouseID NUMERIc(18,0) = 0
AS 
BEGIN

If @byteMode=1
BEGIN
 select count(*) as Total from SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode 
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END
ELSE IF @byteMode=3
BEGIN
SELECT DISTINCT I.vcItemName AS vcItemName,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0) monListPrice,
dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(bitRequired,0) [bitRequired],ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM PromotionOffer AS P 
LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
) AS PromotionOffers
FROM 
Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
                      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
                      SimilarItems SI ON I.numItemCode = SI.numItemCode LEFT  JOIN
                       WareHouseItems W ON I.numItemCode = W.numItemID
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END
ELSE IF @byteMode=2
BEGIN
	SELECT DISTINCT I.vcItemName AS vcItemName,I.txtItemDesc AS txtItemDesc,I.numSaleUnit,ISNULL(I.numContainer,0) AS numContainer,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip 
	,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID, ISNULL(vcRelationship,'') vcRelationship
	,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
	,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as fltUOMConversionFactor, I.vcSKU, I.vcManufacturer
	,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
	,ISNULL(bitRequired,0) [bitRequired], ISNULL(vcUpSellDesc,'') [vcUpSellDesc], dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName
	,CASE WHEN I.[charItemType]='P' THEN ISNULL([numOnHand],0) ELSE -1 END as numOnHand
	,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
	FROM PromotionOffer AS P 
	LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
	--WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID 
	WHERE 
	(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
	I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
	AND P.numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
	) AS PromotionOffers,SI.numParentItemCode
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN Item I ON ItemCategory.numItemID = I.numItemCode 
	--INNER JOIN SimilarItems SI ON I.numItemCode = SI.numItemCode 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR ISNULL(@numWarehouseID,0)=0) 
		) AS W
	WHERE 
		SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode
END 

ELSE IF @byteMode=4 -- Pre up sell
BEGIN
SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

SELECT DISTINCT I.vcItemName AS vcItemName,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage ,I.txtItemDesc AS txtItemDesc,  SI.* ,Category.vcCategoryName,
ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0) monListPrice,
dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor,I.vcSKU,I.vcManufacturer
, ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(bitRequired,0) [bitRequired],ISNULL(vcUpSellDesc,'') [vcUpSellDesc],
(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM PromotionOffer AS P 
LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
) AS PromotionOffers
FROM 
Category INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID INNER JOIN
                      Item I ON ItemCategory.numItemID = I.numItemCode INNER JOIN
                      SimilarItems SI ON I.numItemCode = SI.numItemCode 
                       OUTER APPLY
	(
		SELECT
			TOP 1 *
		FROM
			WareHouseItems
		WHERE
			 numItemID = I.numItemCode
			 AND (numWareHouseID = @numWarehouseID OR @numWarehouseID=0)
	) AS W
 --FROM SimilarItems SI INNER JOIN Item I ON I.numItemCode = SI.numItemCode  INNER JOIN dbo.Category cat ON cat.numCategoryID = I.num
 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPreUpSell,0)=1
 AND 1 = (CASE WHEN I.charItemType='P' THEN (CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END)
END 

ELSE IF @byteMode=5 -- Post up sell
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

	DECLARE @numPromotionID NUMERIC(18,0) = 0
	DECLARE @vcPromotionDescription VARCHAR(MAX) = ''
	DECLARE @intPostSellDiscount INT=0
	DECLARE @vcPromotionName VARCHAR(100) = ''
	
	SELECT TOP 1 
		@intPostSellDiscount=ISNULL(P.intPostSellDiscount,0)
		,@numPromotionID=C.numPromotionID
		,@vcPromotionDescription= CONCAT('Post sell Discount: ', OM.vcPOppName)
		,@vcPromotionName = P.vcProName 
	FROM 
		OpportunityItems AS C 
	INNER JOIN
		OpportunityMaster OM
	ON
		C.numOppId=OM.numOppId
	LEFT JOIN 
		PromotionOffer AS P 
	ON 
		P.numProId=C.numPromotionID 
	WHERE 
		OM.numDomainId=@numDomainID
		AND OM.numOppID = @numOppID
		AND ISNULL(OM.bitPostSellDiscountUsed,0) = 0
		AND ISNULL(C.numPromotionID,0) > 0
		AND ISNULL(P.bitDisplayPostUpSell,0)=1
		AND ISNULL(P.intPostSellDiscount,0) BETWEEN 1 AND 100

	SELECT DISTINCT 
		I.numItemCode
		,I.vcItemName AS vcItemName
		,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
		,I.txtItemDesc AS txtItemDesc
		,I.vcModelID
		,Category.vcCategoryName
		,1 AS numUnitHour
		,ISNULL(CASE WHEN I.[charItemType]='P' THEN [monWListPrice] ELSE monListPrice END,0) monListPrice
		,ISNULL(CASE WHEN I.[charItemType]='P' THEN (monWListPrice-((monWListPrice*(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END))/100)) ELSE (monListPrice-((monListPrice*(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END))/100)) END,0) monOfferListPrice
		,ISNULL(W.numWarehouseItemID,0) AS numWarehouseItemID
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @intPostSellDiscount ELSE 0 END) AS fltDiscount
		,0 AS bitDiscountType
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @numPromotionID ELSE 0 END) AS numPromotionID
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @vcPromotionDescription ELSE '' END) AS vcPromotionDescription
		,(CASE WHEN OI.numPromotionID = @numPromotionID THEN @vcPromotionName ELSE '' END) AS vcPromotionName
		,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0)
		,I.numItemCode
		,I.numDomainId
		,ISNULL(I.numBaseUnit,0)) as UOMConversionFactor
		,I.vcSKU
		,I.vcManufacturer
		,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc]
		,ISNULL(bitPreUpSell,0) [bitPreUpSell]
		,ISNULL(bitPostUpSell,0) [bitPostUpSell]
		,ISNULL(bitRequired,0) [bitRequired]
		,ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
	FROM 
		Category 
	INNER JOIN 
		ItemCategory 
	ON 
		Category.numCategoryID = ItemCategory.numCategoryID 
	INNER JOIN
        Item I 
	ON 
		ItemCategory.numItemID = I.numItemCode
	INNER JOIN
		OpportunityItems OI
	ON
		OI.numItemCode = I.numItemCode
	INNER JOIN 
		OpportunityMaster OM 
	ON 
		OI.numOppId=OM.numOppId	
	INNER JOIN
        SimilarItems SI 
	ON 
		SI.numParentItemCode=OI.numItemCode
		AND SI.bitPostUpSell = 1
	OUTER APPLY
	(
		SELECT
			TOP 1 *
		FROM
			WareHouseItems
		WHERE
			 numItemID = I.numItemCode
			 AND numWareHouseID = @numWarehouseID
	) AS W
	WHERE 
		SI.numDomainId = @numDomainID 
		AND OM.numDomainID=@numDomainID 
		AND OM.numOppId=@numOppID
		AND 1 = (CASE WHEN charItemType='P' THEN CASE WHEN ISNULL(W.numWareHouseItemID,0) > 0 THEN 1 ELSE 0 END ELSE 1 END)
		
		

	UPDATE OpportunityMaster SET bitPostSellDiscountUsed=1 WHERE numDomainId=@numDomainID AND numOppID = @numOppID
END 

ELSE IF @byteMode=6 -- Pre up sell for E-Commerce
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID
	

	SELECT DISTINCT I.vcItemName AS vcItemName
	,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID, ISNULL(vcRelationship,'') vcRelationship
	,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
	,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
	,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
	,ISNULL(bitRequired,0) [bitRequired], ISNULL(vcUpSellDesc,'') [vcUpSellDesc], dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName
	,CASE WHEN I.[charItemType]='P' THEN ISNULL([numOnHand],0) ELSE -1 END as numOnHand
	,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
	FROM PromotionOffer AS P 
	LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
	WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
	(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
	I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
	AND P.numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
	) AS PromotionOffers,SI.numParentItemCode
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN Item I ON ItemCategory.numItemID = I.numItemCode 
	--INNER JOIN SimilarItems SI ON I.numItemCode = SI.numItemCode 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR @numWarehouseID=0) 
		) AS W
	 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPreUpSell,0)=1 
	 AND 1 = (CASE WHEN I.charItemType='P' THEN (CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END)

END 
ELSE IF @byteMode=7 -- Post up sell for E-Commerce
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

	SELECT DISTINCT I.vcItemName AS vcItemName
	,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID, ISNULL(vcRelationship,'') vcRelationship
	,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
	,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
	,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
	,ISNULL(bitRequired,0) [bitRequired], ISNULL(vcUpSellDesc,'') [vcUpSellDesc], dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName
	,CASE WHEN I.[charItemType]='P' THEN ISNULL([numOnHand],0) ELSE -1 END as numOnHand
	,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
	FROM PromotionOffer AS P 
	LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
	WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
	(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
	I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
	AND P.numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
	) AS PromotionOffers, PO.vcProName
	,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionRule, PO.numProId, SI.numParentItemCode 
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	LEFT JOIN PromotionOfferItems AS POI ON POI.numValue = I.numItemCode
	LEFT JOIN PromotionOffer PO on PO.numProId = POI.numProId 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR @numWarehouseID=0) 
		) AS W
	 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPostUpSell,0)=1 
	 AND 1 = (CASE WHEN I.charItemType='P' THEN (CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END)

END 
ELSE IF @byteMode=8 -- Pre up sell for Internal Orders
BEGIN
	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM eCommerceDTL WHERE numSiteId=@numSiteID

	SELECT DISTINCT 
		I.vcItemName AS vcItemName
		,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
		,I.txtItemDesc AS txtItemDesc
		,SI.*
		,ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID
		,ISNULL(vcRelationship,'') vcRelationship
		,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
		,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as UOMConversionFactor, I.vcSKU, I.vcManufacturer
		,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
		,ISNULL(bitRequired,0) [bitRequired]
		,ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
		,dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName
		,CASE WHEN I.[charItemType]='P' THEN ISNULL([numOnHand],0) ELSE -1 END as numOnHand
		,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
		FROM PromotionOffer AS P 
		LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
		WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND 
		(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
		I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
		AND P.numDomainId=@numDomainID 
				AND ISNULL(bitEnabled,0)=1 
				AND ISNULL(bitAppliesToSite,0)=1 
				AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
				AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
				AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		) AS PromotionOffers
		,SI.numParentItemCode
		,(
			CASE WHEN ISNULL(bitKitParent,0) = 1
			THEN
				(CASE 
					WHEN ((SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID = IInner.numItemCode WHERE ID.numItemKitID = I.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1)) > 0 
					THEN 1 
					ELSE 0 
				END)
			ELSE
				0
			END
		) bitHasChildKits
		,Category.vcCategoryName
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR ISNULL(@numWarehouseID,0)=0) 
		) AS W
	 WHERE SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND ISNULL(bitPreUpSell,0)=1 
	 AND 1 = (CASE WHEN I.charItemType='P' THEN (CASE WHEN W.numWareHouseItemID IS NULL THEN 0 ELSE 1 END) ELSE 1 END)
END 
END


