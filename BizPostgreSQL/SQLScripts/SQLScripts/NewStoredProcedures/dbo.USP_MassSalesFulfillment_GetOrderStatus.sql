GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetOrderStatus')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetOrderStatus
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetOrderStatus]
(
	@numDomainID NUMERIC(18,0)
	,@tintOppType TINYINT
)
AS 
BEGIN
	SELECT 
		ListDetails.numListItemID AS ListItemID
		,ListDetails.vcData AS ListItemValue
		,COUNT(numOppId) AS StatusCount 
	FROM 
		ListDetails
	LEFT JOIN
		OpportunityMaster 
	ON
		OpportunityMaster.numStatus = ListDetails.numListItemID
		AND OpportunityMaster.numDomainID=@numDomainID 
		AND OpportunityMaster.tintOppType = @tintOppType
		AND ISNULL(OpportunityMaster.tintshipped,0) = 0
	WHERE
		ListDetails.numDomainID = @numDomainID
		AND ListDetails.numListID=176
		AND ListDetails.numListType = @tintOppType
		AND (ListDetails.tintOppOrOrder = 2 OR ISNULL(ListDetails.tintOppOrOrder,0) = 0)
	GROUP BY
		ListDetails.numListItemID
		,ListDetails.vcData
		,ListDetails.sintOrder
	ORDER BY
		ListDetails.sintOrder
END
GO