GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_PACKAGING_RULE')
	DROP PROCEDURE USP_GET_PACKAGING_RULE
GO

CREATE PROCEDURE [dbo].[USP_GET_PACKAGING_RULE]
	@numPackagingRuleID numeric(18, 0),
	@numDomainID	NUMERIC(18,0)
AS

DECLARE @strSQL AS NVARCHAR(MAX)
DECLARE @strWhere AS NVARCHAR(MAX)

SET @strWhere = ' AND 1=1 '

IF ISNULL(@numPackagingRuleID,0) <> 0
BEGIN
	SET @strWhere = @strWhere + ' AND [numPackagingRuleID] = ' + CONVERT(VARCHAR(10),@numPackagingRuleID)
END

SET @strSQL = 'SELECT DISTINCT  [numPackagingRuleID],
						[vcRuleName],
						[numPackagingTypeID] AS [numPackageTypeID],
						[numFromQty],
						[numShipClassId],
						vcData AS [ShipClass],
						PR.[numDomainID],
						CP.vcPackageName
					FROM
						[dbo].[PackagingRules] PR
					LEFT JOIN ListDetails LD ON PR.numShipClassId = LD.numListItemId
					LEFT JOIN CustomPackages CP ON CP.numCustomPackageID = PR.numPackagingTypeID
					WHERE 1=1
					AND PR.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)

SET @strSQL = @strSQL + @strWhere
PRINT @strSQL
EXEC SP_EXECUTESQL @strSQL

GO