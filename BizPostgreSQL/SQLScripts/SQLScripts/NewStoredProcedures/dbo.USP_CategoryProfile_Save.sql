SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CategoryProfile_Save')
DROP PROCEDURE dbo.USP_CategoryProfile_Save
GO
CREATE PROCEDURE [dbo].[USP_CategoryProfile_Save]
	@ID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcName VARCHAR(200),
	@vcSiteIDs VARCHAR(500)
AS
BEGIN

BEGIN TRY
BEGIN TRANSACTION
	IF @ID = 0 --INSERT 
	BEGIN
		INSERT INTO CategoryProfile 
		(
			vcName,
			numDomainID,
			numCreatedBy,
			dtCreated
		)
		VALUES
		(
			@vcName,
			@numDomainID,
			@numUserCntID,
			GETUTCDATE()
		)

		SELECT @ID = SCOPE_IDENTITY()
	END
	ELSE --UPDATE
	BEGIN
		UPDATE
			CategoryProfile
		SET
			vcName=@vcName,
			numModifiedBy=@numUserCntID,
			dtModified=GETUTCDATE()
		WHERE
			ID=@ID
	END

	IF LEN(@vcSiteIDs) = 0
	BEGIN
		-- REMOVE SITE CATEGORY ASSIGNEDMENT AS SITES DOES NOT BELONG TO THIS CATEGORY PROFILE
		DELETE FROM SiteCategories WHERE numSiteID IN (SELECT ISNULL(numSiteID,0) FROM CategoryProfileSites WHERE numCategoryProfileID=@ID)

		-- REMOVE SITES ASSIGNED TO CATEGORY PROFILE
		DELETE FROM CategoryProfileSites WHERE numCategoryProfileID=@ID
	END
	ELSE
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID NUMERIC(18,0)
		)

		INSERT INTO @TEMP (ID) SELECT Id FROM dbo.SplitIDs(@vcSiteIDs,',')

		--DELETE SITE CATEGORY CONFIGURATION AS THEY BELONGS TO NEW CATEGORY PROFILE NOW
		DELETE FROM SiteCategories WHERE numSiteID IN (SELECT ISNULL(ID,0) FROM @TEMP)
		--DELETE SITES WHICH ARE ASSIGNED TO OTHER CATEGORY AS THEY BELONGS TO NEW CATEGORY PROFILE NOW
		DELETE FROM CategoryProfileSites WHERE numCategoryProfileID <> @ID AND numSiteID IN (SELECT ISNULL(ID,0) FROM @TEMP)


		--DELETE SITE CATEGORY CONFIGURATION FOR SITES AS THEY DOES NOT BELONGS TO THIS CATEGORY PROFILE
		DELETE FROM SiteCategories WHERE numSiteID IN (SELECT ISNULL(numSiteID,0) FROM CategoryProfileSites WHERE numCategoryProfileID=@ID AND numSiteID NOT IN (SELECT ISNULL(ID,0) FROM @TEMP))
		-- DELETE SITES WHICH ARE REMOVED FROM CATEGORY PROFILE
		DELETE FROM CategoryProfileSites WHERE numCategoryProfileID=@ID AND numSiteID NOT IN (SELECT ISNULL(ID,0) FROM @TEMP)

		-- INSERT NEW SELECTED SITES
		INSERT INTO CategoryProfileSites
		(
			numCategoryProfileID,
			numSiteID
		)
		SELECT
			@ID,
			ID
		FROM 
			@TEMP
		WHERE
			ID NOT IN (SELECT numSiteID FROM CategoryProfileSites WHERE numCategoryProfileID=@ID)

		INSERT INTO SiteCategories
		(
			numSiteID,
			numCategoryID
		)
		SELECT 
			CategoryProfileSites.numSiteID,
			Category.numCategoryID
		FROm 
			CategoryProfileSites 
		JOIN
			Category
		ON
			CategoryProfileSites.numCategoryProfileID = Category.numCategoryProfileID
		WHERE 
			CategoryProfileSites.numCategoryProfileID=@ID
		ORDER BY
			CategoryProfileSites.numSiteID
	END
COMMIT
END TRY
BEGIN CATCH
DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END