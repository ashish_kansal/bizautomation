SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,07-Dec-13>
-- Description:	<Description,,To Update MailBox Order>
-- =============================================
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxTreeSort_UpdateSortOrder' ) 
    DROP PROCEDURE USP_InboxTreeSort_UpdateSortOrder
GO
Create PROCEDURE [dbo].[USP_InboxTreeSort_UpdateSortOrder]
	-- Add the parameters for the stored procedure here
	@numSortOrder int,
    @numUserCntID as int,        
	@numDomainID as int,        
	@NodeID as int,        
	@ParentId as int   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE InboxTreeSort 
			SET numSortOrder= @numSortOrder, 
				numParentId = @ParentId 
			WHERE numNodeID= @NodeID AND 
				  numDomainID=@numDomainID AND
				  numUserCntID=@numUserCntID
END