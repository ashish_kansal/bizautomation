GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetAuthDetailsForDomain' ) 
    DROP PROCEDURE USP_GetAuthDetailsForDomain
GO
--USP_GetAuthDetailsForDomain 147
CREATE PROCEDURE [dbo].USP_GetAuthDetailsForDomain
    @numDomainID NUMERIC(9)
AS 
    BEGIN
  
  
  
  SELECT   AGM.numDomainID ,
                AGM.numGroupID ,
                PM.numModuleID ,
                PM.numPageID ,
                0 intExportAllowed ,
                0 intPrintAllowed ,
                0 intViewAllowed ,
                0 intAddAllowed ,
                0 intUpdateAllowed ,
                0 intDeleteAllowed,
				PM.vcPageDesc,
				(SELECT MM.vcModuleName FROM dbo.ModuleMaster MM WHERE numModuleID=PM.numModuleID) AS vcModuleName,
				0 AS bitCustomRelationship
INTO #TempAuth
FROM dbo.AuthenticationGroupMaster AGM CROSS JOIN dbo.PageMaster PM 
WHERE AGM.numDomainID=@numDomainID 
UNION
SELECT
	AGM.numDomainID ,
    AGM.numGroupID ,
    32 ,
    TEMP.numListItemID,
    0 intExportAllowed ,
    0 intPrintAllowed ,
    0 intViewAllowed ,
    0 intAddAllowed ,
    0 intUpdateAllowed ,
    0 intDeleteAllowed,
	TEMP.vcData + ' List' AS vcPageDesc,
	(SELECT MM.vcModuleName FROM dbo.ModuleMaster MM WHERE numModuleID=32) AS vcModuleName,
	1 AS bitCustomRelationship
FROM 
	dbo.AuthenticationGroupMaster AGM
CROSS JOIN 
	(
		SELECT 
			numListItemID,
			vcData
		FROM 
			ListDetails 
		WHERE 
			numListID = 5 AND 
			(ISNULL(constFlag,0) = 1 OR (numDomainID = @numDomainID AND ISNULL(bitDelete,0) = 0)) AND 
			numListItemID <> 46
	) AS TEMP
WHERE 
	AGM.numDomainID=@numDomainID


--SELECT * FROM #TempAuth

UPDATE T SET
intExportAllowed = ISNULL(GA.intExportAllowed,0),
intPrintAllowed=ISNULL(GA.intPrintAllowed,0),
intViewAllowed=ISNULL(GA.intViewAllowed,0),
intAddAllowed=ISNULL(GA.intAddAllowed,0),
intUpdateAllowed=ISNULL(GA.intUpdateAllowed,0),
intDeleteAllowed=ISNULL(GA.intDeleteAllowed,0)
from #TempAuth T LEFT JOIN dbo.GroupAuthorization GA ON GA.numPageID=T.numPageID AND GA.numDomainID=T.numDomainID AND GA.numGroupID=T.numGroupID AND GA.numModuleID=T.numModuleID WHERE T.bitCustomRelationship = 0

UPDATE T SET
intExportAllowed = ISNULL(GA.intExportAllowed,0),
intPrintAllowed=ISNULL(GA.intPrintAllowed,0),
intViewAllowed=ISNULL(GA.intViewAllowed,3),
intAddAllowed=ISNULL(GA.intAddAllowed,0),
intUpdateAllowed=ISNULL(GA.intUpdateAllowed,0),
intDeleteAllowed=ISNULL(GA.intDeleteAllowed,0)
from #TempAuth T LEFT JOIN dbo.GroupAuthorization GA ON GA.numPageID=T.numPageID AND GA.numDomainID=T.numDomainID AND GA.numGroupID=T.numGroupID AND GA.numModuleID=T.numModuleID WHERE T.bitCustomRelationship = 1

 
 SELECT * FROM #TempAuth
 --RETURN;

  --      SELECT  G.numDomainID ,
  --              G.numGroupID ,
  --              GA.numModuleID ,
  --              GA.numPageID ,
  --              GA.intExportAllowed ,
  --              GA.intPrintAllowed ,
  --              GA.intViewAllowed ,
  --              GA.intAddAllowed ,
  --              GA.intUpdateAllowed ,
  --              GA.intDeleteAllowed,
		--		PM.vcPageDesc,
		--		MM.vcModuleName
		--FROM    dbo.PageMaster PM ,
  --              dbo.GroupAuthorization GA
  --              left JOIN dbo.AuthenticationGroupMaster G ON G.numGroupID = GA.numGroupID
		--		left JOIN dbo.ModuleMaster MM ON GA.numModuleID = MM.numModuleID
  --      WHERE   GA.numModuleID = PM.numModuleID
  --              AND GA.numPageID = PM.numPageID
  --              AND G.numDomainID = @numDomainID


    --    FROM    dbo.PageMaster PM left JOIN dbo.GroupAuthorization GA
				--ON GA.numModuleID = PM.numModuleID AND GA.numPageID = PM.numPageID
    --            left JOIN dbo.AuthenticationGroupMaster G ON G.numGroupID = GA.numGroupID
				--left JOIN dbo.ModuleMaster MM ON GA.numModuleID = MM.numModuleID
        --WHERE  
        --       G.numDomainID = @numDomainID


			   
        --SELECT  G.numDomainID ,
        --        G.numGroupID ,
        --        GA.numModuleID ,
        --        GA.numPageID ,
        --        GA.intExportAllowed ,
        --        GA.intPrintAllowed ,
        --        GA.intViewAllowed ,
        --        GA.intAddAllowed ,
        --        GA.intUpdateAllowed ,
        --        GA.intDeleteAllowed
        --FROM    dbo.PageMaster PM ,
        --        dbo.GroupAuthorization GA
        --        INNER JOIN dbo.AuthenticationGroupMaster G ON G.numGroupID = GA.numGroupID
        --WHERE   GA.numModuleID = PM.numModuleID
        --        AND GA.numPageID = PM.numPageID
        --        AND G.numDomainID = @numDomainID
	
    END