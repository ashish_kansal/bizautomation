GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkOrder2')
DROP PROCEDURE USP_GetWorkOrder2
GO
CREATE PROCEDURE [dbo].[USP_GetWorkOrder2]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@tintUserRightType TINYINT
	,@CurrentPage INT                                                
	,@PageSize INT
	,@ClientTimeZoneOffset INT
	,@numWOStatus NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)
	,@vcCustomSearchCriteria VARCHAR(MAX)
	,@SearchText VARCHAR(300) = ''
)
AS                            
BEGIN
	DECLARE @vcCurrencySymbol VARCHAR(20) = ''
	DECLARE @tintDecimalPoints TINYINT

	SELECT 
		@vcCurrencySymbol = ISNULL(Currency.varCurrSymbol ,'')
		,@tintDecimalPoints = ISNULL(Domain.tintDecimalPoints,0)
	FROM 
		Domain 
	LEFT JOIN 
		Currency 
	ON 
		Currency.numDomainID=@numDomainID
		AND Domain.numCurrencyID=Currency.numCurrencyID 
	WHERE 
		Domain.numDomainID=@numDomainID 

	CREATE TABLE #TEMPWorkOrderStatus
	(
		ID INT IDENTITY(1,1)
		,numWOID NUMERIC(18,0)
		,numWorkOrderStatus TINYINT
	)


	DECLARE @vcDynamicQuery AS NVARCHAR(MAX)=''
	DECLARE @vcStatusQuery VARCHAR(500) = ''
	IF(@vcRegularSearchCriteria<>'')
	BEGIN
		IF CHARINDEX('WO.TotalProgress',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.TotalProgress','dbo.GetTotalProgress(WO.numDomainID,WO.numWOID,1,1,'''',0)')
		END

		IF CHARINDEX('WO.dtmStartDate',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.dtmStartDate',CONCAT('dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ',@ClientTimeZoneOffset,', WO.dtmStartDate)',',',@numDomainId,')'))
		END

		IF CHARINDEX('WO.dtmActualStartDate',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.dtmActualStartDate',CONCAT('dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ',@ClientTimeZoneOffset,',(SELECT TOP 1 SPDTTL.dtActionTime FROM StagePercentageDetailsTask SPDT INNER JOIN StagePercentageDetailsTaskTimeLog SPDTTL ON SPDT.numTaskId = SPDTTL.numTaskID WHERE SPDT.numWorkOrderId=WO.numWOID ORDER BY SPDTTL.dtActionTime))',',',@numDomainId,')'))
		END

		IF CHARINDEX('WO.dtmEndDate',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.dtmEndDate',CONCAT('dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ',@ClientTimeZoneOffset,', WO.dtmEndDate)',',',@numDomainId,')'))
		END

		IF CHARINDEX('WO.dtmProjectFinishDate',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.dtmProjectFinishDate',(CASE WHEN @numWOStatus=1 THEN CONCAT('dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ',@ClientTimeZoneOffset,', WO.bintCompletedDate)',',',@numDomainId,')') ELSE 'CAST(TableProjectedFinish.dtProjectedFinish AS DATE)' END))
		END

		IF CHARINDEX('WO.ForecastDetails=1',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ForecastDetails=1','DATEDIFF(DAY,WO.dtmEndDate,TableProjectedFinish.dtProjectedFinish) > 0')
		END

		IF CHARINDEX('WO.ForecastDetails=2',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ForecastDetails=2','DATEDIFF(DAY,WO.dtmEndDate,TableProjectedFinish.dtProjectedFinish) < 0')
		END

		IF CHARINDEX('WO.SchedulePerformance=1',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ForecastDetails=1','DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) > 0')
		END

		IF CHARINDEX('WO.SchedulePerformance=2',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ForecastDetails=2','DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) < 0')
		END

		IF CHARINDEX('WO.ActualProjectDifference=1',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ActualProjectDifference=1','ISNULL(TableHourCost.numEstimatedTimeInMinutes,0) > 0 AND ISNULL(TableHourCost.numActualTimeInMinutes,0) > ISNULL(TableHourCost.numEstimatedTimeInMinutes,0)')
		END

		IF CHARINDEX('WO.ActualProjectDifference=2',@vcRegularSearchCriteria) > 0
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.ActualProjectDifference=2','ISNULL(TableHourCost.numEstimatedTimeInMinutes,0) > 0 AND ISNULL(TableHourCost.numActualTimeInMinutes,0) < ISNULL(TableHourCost.numEstimatedTimeInMinutes,0)')
		END

		IF CHARINDEX('WO.CapacityLoad',@vcRegularSearchCriteria) > 0 
		BEGIN
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.CapacityLoad','TableCapacity.numCapacityLoad')
		END

		IF CHARINDEX('WO.vcWorkOrderStatus',@vcRegularSearchCriteria) > 0
		BEGIN
			INSERT INTO #TEMPWorkOrderStatus
			(
				numWOID
				,numWorkOrderStatus
			)
			SELECT
				numWOID
				,numWorkOrderStatus
			FROM
				dbo.GetWOStatus(@numDomainID,'')
			WHERE
				numWorkOrderStatus = (CASE 
										WHEN CHARINDEX('WO.vcWorkOrderStatus like ''%1%''',@vcRegularSearchCriteria) > 0 THEN 1
										WHEN CHARINDEX('WO.vcWorkOrderStatus like ''%2%''',@vcRegularSearchCriteria) > 0 THEN 2
										WHEN CHARINDEX('WO.vcWorkOrderStatus like ''%3%''',@vcRegularSearchCriteria) > 0 THEN 3
										WHEN CHARINDEX('WO.vcWorkOrderStatus like ''%4%''',@vcRegularSearchCriteria) > 0 THEN 4
										ELSE 1
									END)

			SET @vcStatusQuery = 'INNER JOIN 
										#TEMPWorkOrderStatus TWOS 
									ON 
										WO.numWOID = TWOS.numWOID'
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.vcWorkOrderStatus like ''%1%''','1=1')
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.vcWorkOrderStatus like ''%2%''','1=1')
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.vcWorkOrderStatus like ''%3%''','1=1')
			SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WO.vcWorkOrderStatus like ''%4%''','1=1')
		END

		SET @vcRegularSearchCriteria = ' AND '+@vcRegularSearchCriteria
	END

	SET @vcDynamicQuery = ' SELECT 
								COUNT(*) OVER() AS TotalRecords,
								WO.numWOID,
								WO.vcWorkOrderName,
								WO.numQtyItemsReq,
								WHI.numWareHouseID,
								WO.numWarehouseItemID,
								WHI.numWareHouseID,
								(select dbo.fn_CalculateMinBuildQtyByItem(WO.numItemCode,WHI.numWareHouseID)) AS MaxBuildQty,
								I.vcItemName,
								(SELECT dbo.FormatedDateFromDate( DateAdd(minute,+'+convert(varchar(15),-@ClientTimeZoneOffset)+', WO.dtmStartDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +')) AS dtmStartDate,
								(SELECT TOP 1 SPDTTL.dtActionTime FROM StagePercentageDetailsTask SPDT INNER JOIN StagePercentageDetailsTaskTimeLog SPDTTL ON SPDT.numTaskId = SPDTTL.numTaskID WHERE SPDT.numWorkOrderId=WO.numWOID ORDER BY SPDTTL.dtActionTime) AS dtmActualStartDate,
								DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', WO.dtmEndDate) dtmEndDate,
								WO.bintCompletedDate AS dtmProjectFinishDate,
								OI.ItemReleaseDate,
								'''' AS ForecastDetails,
								SP.Slp_Name,
								dbo.fn_GetContactName(isnull(WO.numAssignedTo,0)) AS vcAssignedTo,
								0 AS CapacityLoad,
								Opp.vcPOppName,
								WO.numWOStatus,
								WO.numWOID AS [numWOID~0~0],
								CONCAT(''<a target="_blank" href="../items/frmWorkOrder.aspx?WOID='',WO.numWOID,''">'',ISNULL(WO.vcWorkOrderName,''-''),'' ('',ISNULL(WO.numQtyItemsReq,0),'')'',''</a>'') AS [vcWorkOrderName~16~0],
								CONCAT(''<a target="_blank" href="../items/frmWorkOrder.aspx?WOID='',WO.numWOID,''">'',ISNULL(WO.vcWorkOrderName,''-''),'' ('',ISNULL(WO.numQtyItemsReq,0),'')'',''</a>'') AS [vcWorkOrderNameWithQty~1~0],
								CONCAT(I.vcItemName,(CASE WHEN ISNULL(I.vcSKU,'''') = '''' THEN '''' ELSE CONCAT('' ('',I.vcSKU,'')'') END)) AS ' + (CASE WHEN @numWOStatus=1 THEN '[AssemblyItemSKU~17~0]' ELSE '[AssemblyItemSKU~4~0]' END) + ',
								(CASE 
									WHEN ISNULL(TableHourCost.numEstimatedTimeInMinutes,0) > 0 
									THEN CONCAT((CASE 
													WHEN ISNULL(TableHourCost.numActualTimeInMinutes,0) > ISNULL(TableHourCost.numEstimatedTimeInMinutes,0) 
													THEN ''<lable style="color: #dd4b39;border: 2px solid #dd4b39;padding: 4px;font-weight: 600;">''
													ELSE ''<lable style="color: #00a65a;border: 2px solid #00a65a;padding: 4px;font-weight: 600;">''
												END),ISNULL(TableHourCost.vcActualTaskTime,''-''),'' & '',''' + @vcCurrencySymbol + ''',CAST(TableHourCost.monActualLabourCost AS DECIMAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) + ')),(CASE WHEN ISNULL(TableHourCost.numActualTimeInMinutes,0) > ISNULL(TableHourCost.numEstimatedTimeInMinutes,0) THEN '' > Projected'' ELSE '' < Projected'' END),''</lable>'') 
									ELSE '''' 
								END) AS [ActualProjectDifference~23~0],
								WO.numQtyItemsReq AS [numQtyItemsReq~18~0],
								WHI.numWareHouseID AS [numWareHouseID~0~0],
								WO.numWarehouseItemID AS [numWarehouseItemID~0~0],
								WHI.numWareHouseID AS [numWareHouseID~0~0],
								(select dbo.fn_CalculateMinBuildQtyByItem(WO.numItemCode,WHI.numWareHouseID)) AS [MaxBuildQty~2~0],
								dbo.GetTotalProgress(WO.numDomainID,WO.numWOID,1,1,'''',0) AS [TotalProgress~3~0],
								I.vcItemName AS [vcItemName~0~0],
								(SELECT dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', WO.dtmStartDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +')) AS [dtmStartDate~5~0],
								(SELECT dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', (SELECT TOP 1 SPDTTL.dtActionTime FROM StagePercentageDetailsTask SPDT INNER JOIN StagePercentageDetailsTaskTimeLog SPDTTL ON SPDT.numTaskId = SPDTTL.numTaskID WHERE SPDT.numWorkOrderId=WO.numWOID ORDER BY SPDTTL.dtActionTime))'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +')) AS [dtmActualStartDate~6~0],
								(SELECT dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', WO.dtmEndDate)'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +')) AS ' + (CASE WHEN @numWOStatus=1 THEN '[dtmEndDate~19~0]' ELSE '[dtmEndDate~7~0]' END) + ',
								' + (CASE WHEN @numWOStatus=1 THEN CONCAT('dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ',@ClientTimeZoneOffset,', WO.bintCompletedDate)',',',@numDomainId,') AS [dtmProjectFinishDate~20~0]') ELSE '''<label class="lblProjectedFinish"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></lable>'' AS [dtmProjectFinishDate~8~0]' END) + ',
								(SELECT dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' + convert(varchar(15),@ClientTimeZoneOffset)+', CAST(OI.ItemReleaseDate AS DATETIME))'+ ','+ CONVERT(VARCHAR(10),@numDomainId) +')) AS ' + (CASE WHEN @numWOStatus=1 THEN '[ItemReleaseDate~21~0]' ELSE '[ItemReleaseDate~9~0]' END) + ',
								''<label class="lblForecastDetail"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>'' AS [ForecastDetails~10~0],
								(CASE 
									WHEN DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) > 0 
									THEN CONCAT(''<lable style="color: #dd4b39;border: 2px solid #dd4b39;padding: 4px;font-weight: 600;">'',DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate),(CASE WHEN DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) > 1 THEN '' Days'' ELSE '' Day'' END),'' Late</lable>'')
									WHEN DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) <= 0
									THEN CONCAT(''<lable style="color: #00a65a;border: 2px solid #00a65a;padding: 4px;font-weight: 600;">'',DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) * -1,(CASE WHEN DATEDIFF(DAY,WO.dtmEndDate,WO.bintCompletedDate) * -1 > 1 THEN '' Days'' ELSE '' Day'' END),'' Early</lable>'')
									ELSE ''''
								END) AS [SchedulePerformance~22~0],
								SP.Slp_Name AS ' + (CASE WHEN @numWOStatus=1 THEN '[Slp_Name~24~0]' ELSE '[Slp_Name~11~0]' END) + ',
								dbo.fn_GetContactName(isnull(WO.numAssignedTo,0)) AS ' + (CASE WHEN @numWOStatus=1 THEN '[vcAssignedTo~25~0]' ELSE '[vcAssignedTo~12~0]' END) + ',
								''<label class="lblCapacityLoad"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>''	AS [CapacityLoad~13~0],
								Opp.vcPOppName AS [vcPOppName~0~0],
								WO.numWOStatus AS [numWOStatus~0~0],
								''<label class="lblWorkOrderStatus"><i class="fa fa-refresh fa-spin" style="font-size:24px"></i></label>''AS [vcWorkOrderStatus~14~0],
								dbo.GetWorkOrderPickedRemainingQty(WO.numWOId,WO.numQtyItemsReq) [vcPickedRemaining~15~0]
							FROM 
								WorkOrder AS WO 
							' + @vcStatusQuery + '
							LEFT JOIN  
								Item I 
							ON 
								WO.numItemCode=I.numItemCode 
							LEFT JOIN
								WareHouseItems AS WHI
							ON
								WO.numWarehouseItemID = WHI.numWareHouseItemID
							LEFT JOIN
								Warehouses AS WH
							ON
								WHI.numWareHouseID = WH.numWareHouseID
							LEFT JOIN
								OpportunityMaster AS Opp
							ON
								WO.numOppId = Opp.numOppId 
							LEFT JOIN
								OpportunityItems AS OI
							ON
								OPP.numOppId=OI.numOppId
								AND OI.numOppItemtcode = WO.numOppItemID
							LEFT JOIN
								Sales_process_List_Master AS SP
							ON
								WO.numBuildProcessId = SP.Slp_id
							OUTER APPLY
							(
								SELECT
									dbo.GetProjectedFinish('+ CAST(@numDomainID AS VARCHAR) +',2,WO.numWOID,0,0,NULL,'+ CAST(ISNULL(@ClientTimeZoneOffset,0) AS VARCHAR) +',0) AS dtProjectedFinish
							) TableProjectedFinish
							OUTER APPLY
							(
								SELECT
									dbo.GetCapacityLoad(' + CAST(@numDomainID AS VARCHAR) + ',0,0,WO.numWOID,0,4,ISNULL(WO.dtmStartDate,WO.bintCreatedDate),' + CAST(ISNULL(@ClientTimeZoneOffset,0) AS VARCHAR) + ') AS numCapacityLoad
							) TableCapacity
							OUTER APPLY
							(
								SELECT
									FORMAT(FLOOR(SUM(numEstimatedTimeInMinutes) / 60),''00'') + '':'' + FORMAT(SUM(numEstimatedTimeInMinutes) % 60,''00'') vcEstimatedTaskTime
									,FORMAT(FLOOR(SUM(numActualTimeInMinutes) / 60),''00'') + '':'' + FORMAT(SUM(numActualTimeInMinutes) % 60,''00'') vcActualTaskTime
									,SUM(numEstimatedTimeInMinutes) numEstimatedTimeInMinutes
									,SUM(numActualTimeInMinutes) numActualTimeInMinutes
									,SUM(monEstimatedLabourCost) monEstimatedLabourCost
									,SUM(monActualLabourCost) monActualLabourCost
								FROM
								(
									SELECT 
										((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * ISNUll(WO.numQtyItemsReq,0)  numEstimatedTimeInMinutes
										,dbo.GetTimeSpendOnTaskInMinutes(' + CAST(@numDomainID AS VARCHAR) + ',SPDT.numTaskId) numActualTimeInMinutes
										,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * ISNUll(WO.numQtyItemsReq,0) * (ISNULL(SPDT.monHourlyRate,0)/60) monEstimatedLabourCost
										,dbo.GetTimeSpendOnTaskInMinutes(' + CAST(@numDomainID AS VARCHAR) + ',SPDT.numTaskId) * (ISNULL(SPDT.monHourlyRate,0)/60) monActualLabourCost
									FROM 
										StagePercentageDetailsTask SPDT
									WHERE 
										numWorkOrderId=WO.numWOId
								) TEMP
							) TableHourCost
							WHERE
								WO.numDomainID='+ CAST(@numDomainID AS VARCHAR(MAX)) +' 
								AND 1 = (CASE ' + CAST(@tintUserRightType AS VARCHAR) + '
											WHEN 3 THEN 1 
											WHEN 1 THEN (CASE 
															WHEN WO.numAssignedTo=' + CAST(@numUserCntID AS VARCHAR) + ' 
																	OR EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WO.numWOID AND numAssignTo=' + CAST(@numUserCntID AS VARCHAR) + ') 
																	OR (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID='+ CAST(@numDomainID AS VARCHAR(MAX)) +' AND numUserDetailID=' + CAST(@numUserCntID AS VARCHAR) + ') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=' + CAST(@numDomainID AS VARCHAR) + ' AND EAD.numContactID=' + CAST(@numUserCntID AS VARCHAR) + ' AND EA.numDivisionID=Opp.numDivisionID))
															THEN 1 
															ELSE 0 
														END) 
											ELSE 0
										END)
								AND 1 = (CASE WHEN ISNULL('+CAST(@numWOStatus AS VARCHAR(MAX))+',0) = 1 AND WO.numWOStatus=23184 THEN 1  WHEN ISNULL('''+CAST(@numWOStatus AS VARCHAR(500))+''',0)=0 AND WO.numWOStatus<>23184 THEN 1 ELSE 0 END) '+@vcRegularSearchCriteria+' ORDER BY WO.bintCreatedDate desc OFFSET '+CAST(((@CurrentPage - 1) * @PageSize) AS VARCHAR(500))+' ROWS FETCH NEXT '+CAST(@PageSize AS VARCHAR(500))+' ROWS ONLY '

PRINT CAST(@vcDynamicQuery AS NTEXT)
EXEC(@vcDynamicQuery)

	DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
	)

	IF @numWOStatus = 1
	BEGIN
		INSERT INTO @TempColumns
		(
			vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			,bitClosedColumn
		)
		VALUES
			('Work Order','vcWorkOrderName','vcWorkOrderName',0,16,0,0,'TextBox','',0,0,'WorkOrder',1,'V',1)
			,('Assembly Item (SKU)','AssemblyItemSKU','AssemblyItemSKU',0,17,0,0,'TextBox','',0,0,'Item',1,'V',1)
			,('Quantity','numQtyItemsReq','numQtyItemsReq',0,18,0,0,'TextBox','',0,0,'WorkOrder',1,'V',1)
			,('Requested Finish','dtmEndDate','dtmEndDate',0,19,0,0,'DateField','',0,0,'WorkOrder',1,'V',1)
			,('Actual Finish','dtmProjectFinishDate','dtmProjectFinishDate',0,20,0,0,'DateField','',0,0,'WorkOrder',1,'V',1)
			,('Item Release','ItemReleaseDate','ItemReleaseDate',0,21,0,0,'DateField','',0,0,'OpportunityItems',1,'V',1)
			,('Schedule Performance','SchedulePerformance','SchedulePerformance',0,22,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',1)
			,('Actual Time & $ vs Projected','ActualProjectDifference','ActualProjectDifference',0,23,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',1)
			,('Build Process','Slp_Name','Slp_Name',0,24,0,0,'SelectBox','',0,0,'Sales_process_List_Master',1,'V',1)
			,('Build Manager','vcAssignedTo','vcAssignedTo',0,25,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempColumns
		(
			vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			,bitClosedColumn
		)
		VALUES
		('Work Order(Qty)','vcWorkOrderNameWithQty','vcWorkOrderNameWithQty',0,1,0,0,'TextBox','',0,0,'WorkOrder',1,'V',0)
		,('Max Build Qty','MaxBuildQty','MaxBuildQty',0,2,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',0)
		,('Total Progress','TotalProgress','TotalProgress',0,3,0,0,'TextBoxRangePercentage','',0,0,'WorkOrder',1,'V',0)
		,('Item','AssemblyItemSKU','AssemblyItemSKU',0,4,0,0,'TextBox','',0,0,'Item',1,'V',0)
		,('Planned Start','dtmStartDate','dtmStartDate',0,5,0,0,'DateField','',0,0,'WorkOrder',1,'V',0)
		,('Actual Start','dtmActualStartDate','dtmActualStartDate',0,6,0,0,'DateField','',0,0,'WorkOrder',1,'V',0)
		,('Requested Finish','dtmEndDate','dtmEndDate',0,7,0,0,'DateField','',0,0,'WorkOrder',1,'V',0)
		,('Projected Finish','dtmProjectFinishDate','dtmProjectFinishDate',0,8,0,0,'DateField','',0,0,'WorkOrder',1,'V',0)
		,('Item Release','ItemReleaseDate','ItemReleaseDate',0,9,0,0,'DateField','',0,0,'OpportunityItems',1,'V',0)
		,('Forecast','ForecastDetails','ForecastDetails',0,10,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',0)
		,('Build Process','Slp_Name','Slp_Name',0,11,0,0,'SelectBox','',0,0,'Sales_process_List_Master',1,'V',0)
		,('Build Manager','vcAssignedTo','vcAssignedTo',0,12,0,0,'SelectBox','',0,0,'WorkOrder',1,'V',0)
		,('Capacity Load','CapacityLoad','CapacityLoad',0,13,0,0,'TextBoxRangePercentage','',0,0,'WorkOrder',1,'V',0)
		,('Work Order Status','vcWorkOrderStatus','WorkOrderStatus',0,14,0,0,'Label','',0,0,'WorkOrder',1,'V',0)
		,('Picked | Remaining','vcPickedRemaining','PickedRemaining',0,15,0,0,'Label','',0,0,'WorkOrder',1,'V',0)
	END

	UPDATE
		TC
	SET
		TC.intColumnWidth = ISNULL(DFCD.intColumnWidth,0)
	FROM
		@TempColumns TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 145
		AND DFCD.tintPageType = 1


	IF @numWOStatus <> 1 AND EXISTS (SELECT numFieldId FROM DycFormConfigurationDetails WHERE numDomainId = @numDomainID AND numUserCntID = @numUserCntID AND numFormId = 145 AND tintPageType = 1)
	BEGIN
		SELECT 
			TC.*
		FROM 
			DycFormConfigurationDetails DFCD 
		INNER JOIN 
			@TempColumns TC 
		ON 
			DFCD.numFieldId=TC.numFieldId
		WHERE 
			numDomainId = @numDomainID 
			AND numUserCntID = @numUserCntID 
			AND numFormId = 145 
			AND tintPageType = 1
		ORDER BY
			DFCD.intRowNum
	END
	ELSE
	BEGIN
		SELECT * FROM @TempColumns
	END
END
GO