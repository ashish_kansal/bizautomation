SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaskDataForGanttChart')
DROP PROCEDURE USP_GetTaskDataForGanttChart
GO
CREATE PROCEDURE [dbo].[USP_GetTaskDataForGanttChart]
@numDomainID as numeric(9)=0,    
@numOppId AS NUMERIC(18,0)=0,
@numProjectID AS NUMERIC(18,0)=0
as    
BEGIN   
SELECT 
	S.vcMileStoneName AS MileStoneName,
	S.vcStageName,
	T.vcTaskName,
	S.numStageDetailsId,
	T.numTaskId,
	CAST(S.tintPercentage As INT) AS tinProgressPercentage,
	T.bitSavedTask,
	T.bitTaskClosed,
	S.bitIsDueDaysUsed,
	T.numHours,
	CONVERT(VARCHAR, T.numHours / 8) AS TaskDays,
	CASE WHEN T.numAssignTo>0 THEN (SELECT TOP 1 vcFirstName+' '+vcLastName FROM AdditionalContactsInformation WHERE numContactId=T.numAssignTo) ELSE '-' END AS vcContactName,
	S.dtStartDate As StageStartDate,
	CAST(CASE WHEN S.bitIsDueDaysUsed=1 THEN DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE))
		ELSE DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),'_')),CAST(S.dtStartDate AS DATE)) END AS DATETIME) AS StageEndDate
FROM 
	StagePercentageDetails AS S,
	StagePercentageDetailsTask AS T
	LEFT JOIN ProjectProcessStageDetails AS PPD
	ON T.numStageDetailsId=PPD.numStageDetailsId AND (PPD.numOppId=@numOppId OR PPD.numProjectId=@numProjectID) AND (T.numOppId=@numOppId OR T.numProjectId=@numProjectID)
WHERE
	T.numStageDetailsId=S.numStageDetailsId AND
	1 =(CASE WHEN @numOppId>0 AND S.numOppID=@numOppId AND T.numOppID=@numOppId THEN 1 WHEN @numProjectID>0 AND S.numProjectID=@numProjectID AND T.numProjectID=@numProjectID THEN 1 ELSE 0 END) AND
	T.bitSavedTask=1 AND
	S.numDomainId=@numDomainID AND
	T.numDomainID=@numDomainID
ORDER BY
	S.vcMileStoneName,
	S.numStageOrder,
	T.numOrder
END