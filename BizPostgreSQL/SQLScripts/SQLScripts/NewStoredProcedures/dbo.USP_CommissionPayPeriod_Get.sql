GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CommissionPayPeriod_Get')
DROP PROCEDURE USP_CommissionPayPeriod_Get
GO
Create PROCEDURE [dbo].[USP_CommissionPayPeriod_Get]
	-- Add the parameters for the stored procedure here
	 @numDomainID AS NUMERIC(18,0),
	 @dtStart DATE,
	 @dtEnd DATE  
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT * FROM CommissionPayPeriod WHERE numDomainID=@numDomainID AND dtStart=@dtStart AND dtEnd=@dtEnd

END
GO

