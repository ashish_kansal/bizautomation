GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkFlowAlertList_Insert')
DROP PROCEDURE USP_WorkFlowAlertList_Insert
GO
  
-- =============================================  
-- Author:  <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,1stApr2014>  
-- Description: <Description,,To Save Alert Message on condition match>  
-- =============================================  
Create PROCEDURE [dbo].[USP_WorkFlowAlertList_Insert]   
 -- Add the parameters for the stored procedure here  
 @vcAlertMessage NTEXT=Null,   
 @numWFID NUMERIC(18,0),  
 @intAlertStatus int,  
 @numDomainID numeric(18,0)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
  
   INSERT INTO WorkFlowAlertList  
                      (vcAlertMessage,numWFID,intAlertStatus,numDomainID)  
     VALUES     (@vcAlertMessage,@numWFID,@intAlertStatus,@numDomainID)   
END  
