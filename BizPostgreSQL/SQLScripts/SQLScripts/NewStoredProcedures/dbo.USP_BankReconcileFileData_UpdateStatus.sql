GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BankReconcileFileData_UpdateStatus')
DROP PROCEDURE USP_BankReconcileFileData_UpdateStatus
GO
CREATE PROCEDURE USP_BankReconcileFileData_UpdateStatus
    @numDomainID NUMERIC(18, 0),
	@numReconcileID NUMERIC(18,0),
    @strStatement VARCHAR(MAX)
AS
BEGIN
	DECLARE  @hDocItem INT

    IF LEN(@strStatement) > 0
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strStatement

		UPDATE 
			BankReconcileFileData
        SET    
			bitReconcile = 0,
			bitCleared=X.bitCleared,
			numTransactionId=X.numTransactionId 				
        FROM   
			OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
        WITH 
		(
			ID NUMERIC(18,0),
			bitCleared BIT,
			numTransactionId NUMERIC(18,0)                 
		) X
        WHERE  
			BankReconcileFileData.ID = X.ID
			AND numReconcileID=@numReconcileID
			AND numDomainID=@numDomainID

        EXEC sp_xml_removedocument @hDocItem
	END
END


