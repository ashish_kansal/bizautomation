SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCommissionAppliesTo')
DROP PROCEDURE USP_CheckCommissionAppliesTo
GO
CREATE PROCEDURE [dbo].[USP_CheckCommissionAppliesTo]      
(@numDomainId as numeric(9)=0,      
 @tintComAppliesTo as TINYINT,
 @tintCommissionType AS TINYINT      
)      
As      
Begin      
	DECLARE @tintCommissionType_Old AS TINYINT,@Total AS INT 
	
	SELECT @tintCommissionType_Old=ISNULL(tintCommissionType,1) FROM dbo.Domain WHERE numDomainId=@numDomainId
	
	Select @Total=count(*) From CommissionRules Where numDomainID=@numDomainID
	
	IF (@tintCommissionType!=@tintCommissionType_Old) 
	BEGIN
		SELECT @Total
	END
	ELSE
	BEGIN
		SELECT 0		
	END		
End
GO
