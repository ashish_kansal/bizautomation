
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmployeesWithCapacityLoadForProject')
DROP PROCEDURE USP_GetEmployeesWithCapacityLoadForProject
GO
CREATE PROCEDURE [dbo].[USP_GetEmployeesWithCapacityLoadForProject]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@tintDateRange TINYINT -- 1:Today, 2:Week, 3:Month
	,@dtFromDate DATETIME
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	 SELECT 
		A.numContactID numEmployeeID
		,CONCAT(A.vcFirstName,' ',A.vcLastName) as vcEmployeeName
		,[dbo].[GetProjectCapacityLoad](@numDomainID,A.numContactID,0,@numProId,0,@tintDateRange,@dtFromDate,@ClientTimeZoneOffset) numCapacityLoad
		,'A+' vcGrade
	FROM 
		UserMaster UM         
	INNER JOIN 
		AdditionalContactsInformation A        
	ON 
		UM.numUserDetailId=A.numContactID          
	WHERE 
		UM.numDomainID=@numDomainID 
		AND UM.numDomainID=A.numDomainID 
		AND UM.intAssociate=1  
END
