GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteFinancialYear')
DROP PROCEDURE USP_DeleteFinancialYear
GO
CREATE PROCEDURE [dbo].[USP_DeleteFinancialYear]
	@numFinYearId NUMERIC(9),
	@numDomainID NUMERIC(9)
AS
SET NOCOUNT ON

IF (SELECT bitCurrentYear FROM FinancialYear WHERE [numFinYearId] = @numFinYearId AND numDomainID=@numDomainID) = 1
BEGIN
   raiserror ('IS_CURRENT_YEAR', 16, 1 ) ;
   RETURN 1
END
BEGIN TRY
   BEGIN TRANSACTION 
   
--			DECLARE @dtPeriodFrom DATETIME
--			DECLARE @dtPeriodTo DATETIME
--
--			SELECT @dtPeriodFrom = dtPeriodFrom,@dtPeriodTo = dtPeriodTo FROM dbo.FinancialYear  WHERE [numFinYearId] = @numFinYearId AND numDomainID=@numDomainID
--
--
--
--			SELECT numJournalId INTO #temp FROM dbo.General_Journal_Details WHERE chBizDocItems='OE' 
--			AND numChartAcntId IN (SELECT numAccountId FROM [ChartAccountOpening] WHERE [numDomainId]=@numDomainID AND [numFinYearId]=@numFinYearId)
--			AND numJournalId IN ( SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE datEntry_Date BETWEEN @dtPeriodFrom AND @dtPeriodTo )
--
--
--			DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN ( SELECT numJournalId FROM  #temp)
--			DELETE FROM dbo.General_Journal_Header WHERE numJournal_Id IN ( SELECT numJournalId FROM #temp)
--


			DELETE FROM [ChartAccountOpening] WHERE [numDomainId]=@numDomainID AND [numFinYearId]=@numFinYearId

			DELETE FROM FinancialYear
			WHERE [numFinYearId] = @numFinYearId
			AND numDomainID=@numDomainID



 COMMIT
 
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH