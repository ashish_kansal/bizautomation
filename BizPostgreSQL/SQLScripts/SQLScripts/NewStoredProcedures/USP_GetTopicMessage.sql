SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTopicMessage')
DROP PROCEDURE USP_GetTopicMessage
GO
CREATE PROCEDURE [dbo].[USP_GetTopicMessage]
	@numTopicId As NUMERIC(18,0),
	@numDomainID As NUMERIC(18,0),
	@numMessageId As NUMERIC(18,0),
	@bitExternalUser AS BIT = 0
	
AS
BEGIN
	DECLARE @sqlQuery AS NVARCHAR(MAX)=''
	SET @sqlQuery = 'SELECT numMessageId, 
				mm.numTopicId, 
				numParentMessageId, 
				mm.intRecordType, 
				mm.numRecordId, 
				vcMessage, 
				mm.numCreatedBy, 
				mm.dtmCreatedOn, 
				mm.numUpdatedBy, 
				mm.dtmUpdatedOn, 
				mm.bitIsInternal, 
				mm.numDomainId,
				(select Count(*) FROM MessageMaster where numParentMessageId= mm.numMessageId) AS MessageCount,
				dbo.fn_GetContactName(mm.numCreatedBy) AS CreatedBy,
				dbo.fn_GetContactName(mm.numUpdatedBy) AS UpdatedBy,
				C.vcCompanyName AS CreatedByOrganization,
				CUP.vcCompanyName AS UpdatedByOrganization,
				ISNULL(LI.vcData,''-'') AS CreatedByDesignation,
				ISNULL(LIUP.vcData,''-'') AS UpdatedByDesignation,
				(select
				   distinct  
					stuff((
						select '','' + TM.vcAttachmentName +''$@#''+CAST(TM.numAttachmentId AS VARCHAR(100))
						from TopicMessageAttachments TM
						where TM.vcAttachmentName = vcAttachmentName AND numMessageId=mm.numMessageId
						order by TM.numAttachmentId desc
						for xml path('''')
					),1,1,'''') as userlist
				from TopicMessageAttachments WHERE numMessageId=mm.numMessageId
				group by vcAttachmentName) AS vcAttachmentName,
				C.vcCompanyName AS CreatedByOrgName,
				ADC.vcEmail AS CreatedByEmail,
				TP.dtmCreatedOn AS TopicPostedDate,
				dbo.fn_GetContactName(TP.numCreateBy) AS TopicCreatedBy,
				TPC.vcCompanyName AS TopicOrgName,
				TPLI.vcData AS TopicCreatedByDesignation,
				TP.vcTopicTitle
				From MessageMaster As mm
				LEFT JOIN AdditionalContactsInformation AS ADC ON ADC.numContactID=mm.numCreatedBy
				LEFT JOIN AdditionalContactsInformation AS ADCUP ON ADCUP.numContactID=mm.numUpdatedBy
				LEFT JOIN DivisionMaster AS Div ON Div.numDivisionID=ADC.numDivisionId
				LEFT JOIN DivisionMaster AS DivUP ON DivUP.numDivisionID=ADCUP.numDivisionId
				LEFT JOIN CompanyInfo as C on Div.numCompanyID=C.numCompanyId
				LEFT JOIN CompanyInfo as CUP on DivUP.numCompanyID=CUP.numCompanyId
				LEFT JOIN ListDetails AS LI ON LI.numListItemID=ADC.vcPosition
				LEFT JOIN ListDetails AS LIUP ON LIUP.numListItemID=ADCUP.vcPosition
				LEFT JOIN TopicMaster AS TP ON mm.numTopicId=TP.numTopicId
				LEFT JOIN AdditionalContactsInformation AS TPADC ON TPADC.numContactID=TP.numCreateBy
				LEFT JOIN DivisionMaster AS TPDiv ON TPDiv.numDivisionID=TPADC.numDivisionId
				LEFT JOIN CompanyInfo as TPC on TPDiv.numCompanyID=TPC.numCompanyId
				LEFT JOIN ListDetails AS TPLI ON TPLI.numListItemID=TPADC.vcPosition '
	SET @sqlQuery = @sqlQuery + ' WHERE mm.numDomainId= '+CAST(@numDomainID AS VARCHAR(200))+'	 '
	IF(@numMessageId = 0)
	BEGIN
		SET @sqlQuery = @sqlQuery + ' AND mm.numTopicId='+CAST(@numTopicId AS VARCHAR(200))+' '	
	END
	ELSE IF(@numMessageId >0)
	BEGIN
		SET @sqlQuery = @sqlQuery + ' AND mm.numMessageId='+CAST(@numMessageId AS VARCHAR(200))+' '	
	END
	
	IF(@bitExternalUser=1)
	BEGIN
		SET @sqlQuery = @sqlQuery + ' AND ISNULL(mm.bitIsInternal,0)=0 '
	END
	SET @sqlQuery = @sqlQuery + ' ORDER BY dtmCreatedOn desc '	
	PRINT @sqlQuery
	EXEC(@sqlQuery)
END