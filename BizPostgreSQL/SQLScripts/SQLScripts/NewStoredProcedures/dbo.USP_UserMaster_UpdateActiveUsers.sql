GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
--Modified By Sachin Sadhu
--Worked on AND-OR condition       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_UpdateActiveUsers')
DROP PROCEDURE USP_UserMaster_UpdateActiveUsers
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_UpdateActiveUsers]                          
	@numDomainID NUMERIC(18,0)
	,@vcUsers VARCHAR(MAX)                         
AS                  
BEGIN 
	DECLARE @hDocItem INT
	
	IF ISNULL(@vcUsers,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcUsers

		DECLARE @TEMP TABLE
		(
			numUserID NUMERIC(18,0),
			bitActivateFlag BIT
		)

		INSERT INTO @TEMP
		(
			numUserID,
			bitActivateFlag
		)
		SELECT
			numUserID,
			bitActivateFlag
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Users',2)
		WITH
		(
			numUserID NUMERIC(18,0),
			bitActivateFlag BIT
		)

		EXEC sp_xml_removedocument @hDocItem 


		DECLARE @numNewFullUsers AS INT
		DECLARE @numNewLimitedAccessUsers AS INT
		
		SET @numNewFullUsers = ISNULL((SELECT
											COUNT(*)
										FROM
											UserMaster 
										INNER JOIN
											AuthenticationGroupMaster
										ON
											UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
										LEFT JOIN
											@TEMP TEMP
										ON
											UserMaster.numUserId = TEMP.numUserID
										WHERE
											UserMaster.numDomainID = @numDomainID
											AND AuthenticationGroupMaster.tintGroupType=1
											AND (CASE WHEN ISNULL(TEMP.numUserID,0) > 0 THEN ISNULL(TEMP.bitActivateFlag,0) ELSE ISNULL(UserMaster.bitActivateFlag,0) END) = 1),0)

		SET @numNewLimitedAccessUsers = ISNULL((SELECT
											COUNT(*)
										FROM
											UserMaster 
										INNER JOIN
											AuthenticationGroupMaster
										ON
											UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
										LEFT JOIN
											@TEMP TEMP
										ON
											UserMaster.numUserId = TEMP.numUserID
										WHERE
											UserMaster.numDomainID = @numDomainID
											AND AuthenticationGroupMaster.tintGroupType=4
											AND (CASE WHEN ISNULL(TEMP.numUserID,0) > 0 THEN ISNULL(TEMP.bitActivateFlag,0) ELSE ISNULL(UserMaster.bitActivateFlag,0) END) = 1),0)
		
		IF EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND @numNewFullUsers > ISNULL(intNoofUsersSubscribed,0))
		BEGIN
			RAISERROR('FULL_USERS_EXCEED',16,1)
		END
		ELSE IF EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND @numNewLimitedAccessUsers > ISNULL(intNoofLimitedAccessUsers,0))
		BEGIN
			RAISERROR('LIMITED_ACCESS_USERS_EXCEED',16,1)
		END
		ELSE
		BEGIN
			UPDATE
				UM
			SET
				UM.bitActivateFlag = T.bitActivateFlag
			FROM
				UserMaster UM
			INNER JOIN
				@TEMP T
			ON
				UM.numUserId = T.numUserID
			WHERE
				UM.numDomainID=@numDomainID
		END
	END 
END
GO