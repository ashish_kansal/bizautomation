SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_VerifySerialLot')
DROP PROCEDURE USP_VerifySerialLot
GO
CREATE PROCEDURE [dbo].[USP_VerifySerialLot]  
	@numOppItemID NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@vcSerialLot VARCHAR(100),
	@bitLotNo BIT 
AS  
BEGIN 
	IF @bitLotNo = 1 --LOT ITEM
	BEGIN
		DECLARE @TempLot TABLE
		(
			vcLotNo VARCHAR(100),
			numQty INT 
		)

		DECLARE @vcLotNo AS VARCHAR(200) = ''
		DECLARE @numQty AS VARCHAR(200) = 0

		SELECT
			@vcLotNo = SUBSTRING(OutParam,0,PATINDEX('%(%', @vcSerialLot)),
			@numQty = CAST(SUBSTRING(OutParam,(PATINDEX('%(%', @vcSerialLot) + 1),(PATINDEX('%)%', @vcSerialLot) - PATINDEX('%(%', @vcSerialLot) - 1)) AS INT)
		FROM
			dbo.SplitString(@vcSerialLot,',')


		IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo = @vcLotNo AND ISNULL(numQty,0) >= @numQty)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
	ELSE --SERIALIZED ITEM
	BEGIN
		IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo = @vcSerialLot AND ISNULL(numQty,0) > 0)
		BEGIN
			SELECT 1
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
END