GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_IMPORT_FILE_FIELD_MAPPING')
DROP PROCEDURE USP_MANAGE_IMPORT_FILE_FIELD_MAPPING
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MANAGE_IMPORT_FILE_FIELD_MAPPING]
(
  @numImportFileID			BIGINT,
  @numFormFieldID			NUMERIC,
  @intMapColumnNo			INT,
  @bitCustomField			BIT  
)
AS 
BEGIN
	INSERT INTO Import_File_Field_Mapping
				   (					
					intImportFileID
					,numFormFieldID
					,intMapColumnNo
					,bitCustomField
				   )
			VALUES
				   (					
					@numImportFileID
					,@numFormFieldID
					,@intMapColumnNo  					        
					,@bitCustomField
				   )
END
