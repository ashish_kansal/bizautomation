GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DiscountCodes_Validate')
DROP PROCEDURE USP_DiscountCodes_Validate
GO
CREATE PROCEDURE [dbo].[USP_DiscountCodes_Validate]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numDivisionId NUMERIC(18,0)
	,@txtCouponCode VARCHAR(100)
AS
BEGIN
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=ISNULL(numCompanyType,0)
			,@numProfile=ISNULL(vcProfile,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0)
			,@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteID=@numSiteID
	END

	DECLARE @numPromotionID NUMERIC(18,0)
	DECLARE @bitOrderBasedPromotion AS BIT = 0
	DECLARE @bitPromotionNeverExpires BIT
	DECLARE @dtPromotionStartDate DATETIME
	DECLARE @dtPromotionEndDate DATETIME
	DECLARE @intUsageLimit INT
	DECLARE @numDisocuntID NUMERIC(18,0)
	DECLARE @bitUseForCouponManagement BIT

	-- FIRST CHECK WHETHER COUNPON CODE IS AVAILABLE
	SELECT 
		@numPromotionID=PO.numProId
		,@intUsageLimit=ISNULL(DC.CodeUsageLimit,0)
		,@bitPromotionNeverExpires=ISNULL(bitNeverExpires,0)
		,@dtPromotionStartDate=PO.dtValidFrom
		,@dtPromotionEndDate=PO.dtValidTo
		,@numDisocuntID=numDiscountId
		,@bitOrderBasedPromotion=ISNULL(PO.IsOrderBasedPromotion,0)
		,@bitUseForCouponManagement=ISNULL(bitUseForCouponManagement,0)
	FROM 
		PromotionOffer PO
	INNER JOIN 
		PromotionOfferOrganizations PORG
	ON 
		PO.numProId = PORG.numProId
	INNER JOIN 
		DiscountCodes DC
	ON 
		PO.numProId = DC.numPromotionID
	WHERE
		PO.numDomainId = @numDomainID
		AND 1 =(CASE WHEN numRelationship=@numRelationship AND numProfile=@numProfile THEN 1 ELSE 0 END)
		AND DC.vcDiscountCode = @txtCouponCode

	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- NOW CHECK IF PROMOTION EXPIRES OR NOT
		IF ISNULL(@bitPromotionNeverExpires,0) = 0
		BEGIN
			IF (@dtPromotionStartDate <= GETUTCDATE() AND @dtPromotionEndDate >= GETUTCDATE())
			BEGIN
				IF ISNULL(@intUsageLimit,0) <> 0
				BEGIN
					IF ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=@numDisocuntID AND numDivisionId=@numDivisionId),0) >= @intUsageLimit
					BEGIN
						RAISERROR('COUPON_USAGE_LIMIT_EXCEEDED',16,1)
						RETURN
					END
				END
			END
			ELSE
			BEGIN
				RAISERROR('COUPON_CODE_EXPIRED',16,1)
				RETURN
			END
		END
	END
	ELSE
	BEGIN
		RAISERROR('INVALID_COUPON_CODE',16,1)
		RETURN
	END

	SELECT 
		@numPromotionID AS numPromotionID
		,@numDisocuntID AS numDisocuntID
		,@txtCouponCode AS vcCouponCode
		,@bitOrderBasedPromotion AS bitOrderBasedPromotion
		,@bitUseForCouponManagement AS bitUseForCouponManagement
END
GO