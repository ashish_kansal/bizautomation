GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetExpenseEntriesDetail')
DROP PROCEDURE USP_TimeAndExpense_GetExpenseEntriesDetail
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_GetExpenseEntriesDetail]
(
    @numDomainID NUMERIC(18,0)
	,@numUserCntID INT
	,@dtFromDate DATE
	,@dtToDate DATE
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	SELECT 
		*
		,@numUserCntID AS numUserCntID
	FROM
		dbo.fn_GetPayrollEmployeeExpense(@numDomainID,@numUserCntID,@dtFromDate,@dtToDate,@ClientTimeZoneOffset)
	ORDER BY
		dtDate
END
GO