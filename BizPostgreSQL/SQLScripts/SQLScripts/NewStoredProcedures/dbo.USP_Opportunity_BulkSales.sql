SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Opportunity_BulkSales')
DROP PROCEDURE USP_Opportunity_BulkSales
GO
CREATE PROCEDURE [dbo].[USP_Opportunity_BulkSales]  
  
AS  
BEGIN
	DECLARE @TEMP TABLE
	(
		ID INT,
		numMSOQID NUMERIC(18,0),
		numDivisionID NUMERIC(18,0),
		numContactID NUMERIC(18,0)
	)

	DECLARE @TEMPMasterOpp TABLE
	(
		ID INT IDENTITY(1,1),
		numOppID NUMERIC(18,0)
	)

	DECLARE @TEMPOppItems TABLE
	(
		ID INT
		,numoppitemtCode NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,monPrice DECIMAL(30,16)
		,monTotAmount DECIMAL(20,5)
		,vcItemDesc VARCHAR(1000)
		,numWarehouseItmsID NUMERIC(18,0)
		,ItemType VARCHAR(30)
		,DropShip BIT
		,bitDiscountType BIT
		,fltDiscount DECIMAL(30,16)
		,monTotAmtBefDiscount DECIMAL(20,5)
		,vcItemName VARCHAR(300)
		,numUOM NUMERIC(18,0)
		,bitWorkOrder BIT
		,numVendorWareHouse NUMERIC(18,0)
		,numShipmentMethod NUMERIC(18,0)
		,numSOVendorId NUMERIC(18,0)
		,numProjectID numeric(18,0)
		,numProjectStageID numeric(18,0)
		,numToWarehouseItemID NUMERIC(18,0)
		,Attributes varchar(500)
		,AttributeIDs VARCHAR(500)
		,vcSKU VARCHAR(100)
		,bitItemPriceApprovalRequired BIT
		,numPromotionID NUMERIC(18,0)
		,bitPromotionTriggered BIT
		,vcPromotionDetail VARCHAR(2000)
		,numSortOrder NUMERIC(18,0)
	)


	-- FIRST GET UNIQUE OPPID THAT NEEDS TO BE RE CREATED WHICH ARE NOT EXECUTED
	INSERT INTO 
		@TEMPMasterOpp
	SELECT
		DISTINCT numOppId
	FROM
		dbo.MassSalesOrderQueue
	WHERE
		ISNULL(bitExecuted,0) = 0
	
	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount = COUNT(*) FROM @TEMPMasterOpp

	DECLARE @tempOppID AS INT
	DECLARE @numNewOppID AS NUMERIC(18,0)
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numUserCntID AS NUMERIC(18,0)
	DECLARE @tintSource AS NUMERIC(9)
	DECLARE @txtComments AS VARCHAR(1000)
	DECLARE @bitPublicFlag AS BIT
	DECLARE @monPAmount AS DECIMAL(20,5)
	DECLARE @numAssignedTo AS NUMERIC(18,0)                                                                                                                                                                              
	DECLARE @strItems AS VARCHAR(MAX)
	DECLARE @dtEstimatedCloseDate DATETIME                                                            
	DECLARE @lngPConclAnalysis AS NUMERIC(18,0)
	DECLARE @numCurrencyID AS NUMERIC(18,0)
	DECLARE @numOrderStatus NUMERIC(18,0)

	DECLARE @bitBillingTerms AS BIT
	DECLARE @intBillingDays as integer
	DECLARE @bitInterestType as bit
	DECLARE @fltInterest as float
	DECLARE @intShippingCompany AS NUMERIC(18,0)
	DECLARE @numDefaultShippingServiceID AS NUMERIC(18,0)
	DECLARE @vcCouponCode VARCHAR(100)
	DECLARE @numShipFromWarehouse NUMERIC(18,0)
	DECLARE @numWillCallWarehouseID NUMERIC(18,0)
	DECLARE @dtReleaseDate DATE

	WHILE @i <= @iCount
	BEGIN
		SELECT @tempOppID=numOppID FROM @TEMPMasterOpp WHERE ID = @i

		IF EXISTS (SELECT * FROM OpportunityMaster WHERE numOppId=@tempOppID)
		BEGIN

			SELECT 
				@numDomainID=numDomainId
				,@numUserCntID=numCreatedBy
				,@tintSource=tintSource
				,@txtComments=txtComments
				,@bitPublicFlag=bitPublicFlag
				,@dtEstimatedCloseDate=GETUTCDATE()
				,@lngPConclAnalysis=lngPConclAnalysis
				,@numCurrencyID=numCurrencyID
				,@vcCouponCode=vcCouponCode
				,@numAssignedTo=numAssignedTo
				,@numOrderStatus=numStatus
				,@numShipFromWarehouse=ISNULL(numShipFromWarehouse,0)
				,@dtReleaseDate=dtReleaseDate
			FROM
				OpportunityMaster
			WHERE
				numOppId=@tempOppID

			IF ISNULL(@numShipFromWarehouse,0) > 0
			BEGIN
				SET @numWillCallWarehouseID = @numShipFromWarehouse
			END
			ELSE
			BEGIN
				SET @numWillCallWarehouseID = ISNULL((SELECT TOP 1 numWarehouseItmsID FROM OpportunityItems WHERE numOppId=@tempOppID AND ISNULL(numWarehouseItmsID,0) > 0),0)
			END


			-- DELETE ENTRIES OF PREVIOUS LOOP
			DELETE FROM @TEMPOppItems

			INSERT INTO 
				@TEMPOppItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numoppitemtCode)
				,numoppitemtCode
				,numItemCode
				,numUnitHour
				,monPrice
				,monTotAmount
				,vcItemDesc
				,numWarehouseItmsID
				,vcType
				,bitDropShip
				,bitDiscountType
				,fltDiscount
				,monTotAmtBefDiscount
				,vcItemName
				,numUOMId
				,bitWorkOrder
				,numVendorWareHouse
				,numShipmentMethod
				,numSOVendorId
				,0 AS numProjectID
				,0 AS numProjectStageID
				,numToWarehouseItemID
				,vcAttributes AS Attributes
				,vcAttrValues AS AttributeIDs
				,vcSKU
				,bitItemPriceApprovalRequired
				,numPromotionID
				,bitPromotionTriggered
				,vcPromotionDetail
				,numSortOrder
			FROM 
				OpportunityItems
			WHERE
				numOppId = @tempOppID

			DECLARE @k AS INT = 1
			DECLARE @numTempOppItemID NUMERIC(18,0)
			DECLARE @kCount AS INT
			SELECT @kCount=COUNT(*) FROM @TEMPOppItems
	
			--TODO CREATE ITEMS
			SET @strItems = '<?xml version="1.0" encoding="iso-8859-1" ?><NewDataSet>'

			WHILE @k <= @kCount
			BEGIN
				SELECT 
					@numTempOppItemID = numoppitemtCode
					,@strItems = CONCAT(@strItems
										,'<Item><Op_Flag>1</Op_Flag>'
										,'<numoppitemtCode>',ID,'</numoppitemtCode>'
										,'<numItemCode>',numItemCode,'</numItemCode>'
										,'<numUnitHour>',numUnitHour,'</numUnitHour>'
										,'<monPrice>',monPrice,'</monPrice>'
										,'<monTotAmount>',monTotAmount,'</monTotAmount>'
										,'<vcItemDesc>',vcItemDesc,'</vcItemDesc>'
										,'<numWarehouseItmsID>',ISNULL(numWarehouseItmsID,0),'</numWarehouseItmsID>'
										,'<ItemType>',ItemType,'</ItemType>'
										,'<DropShip>',ISNULL(DropShip,0),'</DropShip>'
										,'<bitDiscountType>',ISNULL(bitDiscountType,0),'</bitDiscountType>'
										,'<fltDiscount>',ISNULL(fltDiscount,0),'</fltDiscount>'
										,'<monTotAmtBefDiscount>',monTotAmtBefDiscount,'</monTotAmtBefDiscount>'
										,'<vcItemName>',vcItemName,'</vcItemName>'
										,'<numUOM>',ISNULL(numUOM,0),'</numUOM>'
										,'<bitWorkOrder>',ISNULL(bitWorkOrder,0),'</bitWorkOrder>'
										,'<numVendorWareHouse>',ISNULL(numVendorWareHouse,0),'</numVendorWareHouse>'
										,'<numShipmentMethod>',ISNULL(numShipmentMethod,0),'</numShipmentMethod>'
										,'<numSOVendorId>',ISNULL(numSOVendorId,0),'</numSOVendorId>'
										,'<numProjectID>',ISNULL(numProjectID,0),'</numProjectID>'
										,'<numProjectStageID>',ISNULL(numProjectStageID,0),'</numProjectStageID>'
										,'<numToWarehouseItemID>',ISNULL(numToWarehouseItemID,0),'</numToWarehouseItemID>'
										,'<Attributes>',Attributes,'</Attributes>'
										,'<AttributeIDs>',AttributeIDs,'</AttributeIDs>'
										,'<vcSKU>',vcSKU,'</vcSKU>'
										,'<bitItemPriceApprovalRequired>',ISNULL(bitItemPriceApprovalRequired,0),'</bitItemPriceApprovalRequired>'
										,'<numPromotionID>',ISNULL(numPromotionID,0),'</numPromotionID>'
										,'<bitPromotionTriggered>',ISNULL(bitPromotionTriggered,0),'</bitPromotionTriggered>'
										,'<vcPromotionDetail>',vcPromotionDetail,'</vcPromotionDetail>'
										,'<numSortOrder>',ISNULL(numSortOrder,0),'</numSortOrder>')
									
				FROM 
					@TEMPOppItems 
				WHERE 
					ID=@k

			
				IF (SELECT COUNT(*) FROM OpportunityKitChildItems WHERE numOppID = @tempOppID AND numOppItemID=@numTempOppItemID) > 0
				BEGIN
					SET @strItems = CONCAT(@strItems,'<bitHasKitAsChild>true</bitHasKitAsChild>')
					SET @strItems = CONCAT(@strItems,'<KitChildItems>',
											STUFF((SELECT 
											',' + CONCAT(OKI.numChildItemID,'#',OKI.numWareHouseItemId,'-',OKCI.numItemID,'#',OKCI.numWareHouseItemId)
											FROM
												OpportunityKitChildItems OKCI
											INNER JOIN
												OpportunityKitItems OKI
											ON
												OKCI.numOppChildItemID = OKI.numOppChildItemID
											WHERE
												OKCI.numOppID=@tempOppID
												AND OKCI.numOppItemID=@numTempOppItemID
																		 FOR XML PATH('')), 
																		1, 1, ''),'</KitChildItems>')
				END
				ELSE
				BEGIN
					SET @strItems = CONCAT(@strItems,'<bitHasKitAsChild>false</bitHasKitAsChild>')
					SET @strItems = CONCAT(@strItems,'<KitChildItems />')
				END
			

				SET @strItems = CONCAT(@strItems,'</Item>')

				SET @k = @k + 1
			END

			SET @strItems = CONCAT(@strItems,'</NewDataSet>')
		
			-- DELETE ENTRIES OF PREVIOUS LOOP
			DELETE FROM @TEMP

			-- GET ALL ORGANIZATION FOR WHICH ORDER NEEDS TO BE CREATED
			INSERT INTO @TEMP
			(
				ID,
				numMSOQID,
				numDivisionID,
				numContactID
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numMSOQID),
				numMSOQID,
				numDivisionId,
				numContactId
			FROM
				MassSalesOrderQueue
			WHERE
				numOppId = @tempOppID
				AND ISNULL(bitExecuted,0) = 0

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempMSOQID NUMERIC(18,0)
			DECLARE @numTempDivisionID NUMERIC(18,0)
			DECLARE @numTempContactID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMP

			WHILE @j <= @jCount
			BEGIN
			
				SELECT 
					@numTempMSOQID=numMSOQID
					,@numTempDivisionID=numDivisionID
					,@numTempContactID=numContactID
				FROM 
					@TEMP 
				WHERE 
					ID = @j

				SELECT 
					@bitBillingTerms=(CASE WHEN ISNULL(tintBillingTerms,0) = 1 THEN 1 ELSE 0 END) 
					,@intBillingDays = ISNULL(numBillingDays,0)
					,@bitInterestType= (CASE WHEN ISNULL(tintInterestType,0) = 1 THEN 1 ELSE 0 END)
					,@fltInterest=ISNULL(fltInterest ,0)
					,@numAssignedTo=ISNULL(numAssignedTo,0)
					,@intShippingCompany=ISNULL(intShippingCompany,0)
					,@numDefaultShippingServiceID=ISNULL(numDefaultShippingServiceID,0)
				FROM 
					DivisionMaster
				WHERE 
					numDomainID=@numDomainID AND numDivisionID=@numTempDivisionID

				BEGIN TRY
					SET @numNewOppID = 0

					EXEC USP_OppManage 
					@numNewOppID OUTPUT
					,@numTempContactID
					,@numTempDivisionID
					,@tintSource
					,''
					,@txtComments
					,@bitPublicFlag
					,@numUserCntID
					,0
					,@numAssignedTo
					,@numDomainID
					,@strItems
					,''
					,@dtEstimatedCloseDate
					,0
					,@lngPConclAnalysis
					,1
					,0
					,0
					,0
					,@numCurrencyID
					,1
					,@numOrderStatus
					,NULL
					,0
					,0
					,0
					,0
					,1
					,0
					,0
					,@bitBillingTerms
					,@intBillingDays
					,@bitInterestType
					,@fltInterest
					,0
					,0
					,NULL
					,@vcCouponCode
					,NULL
					,NULL
					,NULL
					,NULL
					,0
					,0
					,0
					,@intShippingCompany
					,0
					,@dtReleaseDate
					,0
					,0
					,0
					,0
					,@numWillCallWarehouseID
					,0
					,@numShipFromWarehouse
					,@numDefaultShippingServiceID
					,0

					EXEC USP_OpportunityMaster_CT @numDomainID,@numUserCntID,@numNewOppID

					-- UPDATE STATUS TO EXECUTED
					UPDATE 
						MassSalesOrderQueue
					SET
						bitExecuted = 1
					WHERE
						numMSOQID = @numTempMSOQID


					-- MAKE ENTRY INTO MASS SALES ORDER LOG
					INSERT INTO MassSalesOrderLog
					(
						numMSOQID
						,bitSuccess
						,numCreatedOrderID
					)
					VALUES
					(
						@numTempMSOQID
						,1
						,@numNewOppID
					)


					INSERT INTO CFW_Fld_Values_Opp
					(
						Fld_ID
						,Fld_Value
						,RecId
						,bintModifiedDate
						,numModifiedBy
					)
					SELECT
						Fld_ID
						,Fld_Value
						,@numNewOppID
						,GETUTCDATE()
						,numModifiedBy
					FROM
						CFW_Fld_Values_Opp
					WHERE
						RecId=@tempOppID
				END TRY
				BEGIN CATCH
					SELECT ERROR_MESSAGE(),ERROR_LINE()
						
					-- UPDATE STATUS TO EXECUTED
					UPDATE 
						MassSalesOrderQueue
					SET
						bitExecuted = 1
					WHERE
						numMSOQID = @numTempMSOQID


					-- MAKE ENTRY INTO MASS SALES ORDER LOG
					INSERT INTO MassSalesOrderLog
					(
						numMSOQID
						,bitSuccess
						,vcError
					)
					VALUES
					(
						@numTempMSOQID
						,0
						,ERROR_MESSAGE()
					)
				END CATCH

				SET @j = @j + 1
			END
		END

		SET @i = @i + 1
	END
END
