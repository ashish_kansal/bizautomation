
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteProcessFromOpportunity')
DROP PROCEDURE USP_DeleteProcessFromOpportunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteProcessFromOpportunity] 
@numOppId AS NUMERIC(18,0)=0,
@numProjectId AS NUMERIC(18,0)=0
as    
BEGIN   
	IF(@numOppId>0)
	BEGIN
		UPDATE dbo.OpportunityMaster SET numBusinessProcessID=NULL WHERE numOppID=@numOppId
		DELETE FROM StagePercentageDetails WHERE numOppId=@numOppId
		DELETE FROM ProjectProcessStageDetails WHERE numOppId=@numOppId
		DELETE FROM ProjectProgress WHERE numOppId=@numOppId
	
		DELETE FROM StagePercentageDetailsTask WHERE numOppId=@numOppId
		DELETE FROM Sales_process_List_Master WHERE numOppId=@numOppId
	END
	IF(@numProjectId>0)
	BEGIN
		UPDATE dbo.ProjectsMaster SET numBusinessProcessID=NULL, dtCompletionDate=NULL WHERE numProId=@numProjectId
		DELETE FROM StagePercentageDetails WHERE numProjectID=@numProjectId
		DELETE FROM ProjectProcessStageDetails WHERE numProjectId=@numProjectId
		DELETE FROM ProjectProgress WHERE numProId=@numProjectId
		DELETE FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId=@numProjectId)
		DELETE FROM StagePercentageDetailsTask WHERE numProjectId=@numProjectId
		DELETE FROM Sales_process_List_Master WHERE numProjectId=@numProjectId
	END
END 