SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_RevertFulFillment')
DROP PROCEDURE USP_OpportunityBizDocs_RevertFulFillment
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_RevertFulFillment] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @vcDescription AS VARCHAR(300)

	DECLARE @TEMPSERIALLOT TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,vcSerialLot VARCHAR(100)
		,numQty FLOAT
	)

	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @TempFinalTable TABLE
	(
		ID INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		vcItemType CHAR(1),
		bitSerial BIT,
		bitLot BIT,
		bitAssembly BIT,
		bitKit BIT,
		numOppItemID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numQtyShipped FLOAT,
		numQty FLOAT,
		bitDropShip BIT
	)

	INSERT INTO @TempFinalTable
	(
		numItemCode,
		vcItemType,
		bitSerial,
		bitLot,
		bitAssembly,
		bitKit,
		numOppItemID,
		numWarehouseItemID,
		numQtyShipped,
		numQty,
		bitDropShip
	)
	SELECT
		OpportunityBizDocItems.numItemCode,
		ISNULL(Item.charItemType,''),
		ISNULL(Item.bitSerialized,0),
		ISNULL(Item.bitLotNo,0),
		ISNULL(Item.bitAssembly,0),
		ISNULL(Item.bitKitParent,0),
		ISNULL(OpportunityItems.numoppitemtcode,0),
		ISNULL(OpportunityItems.numWarehouseItmsID,0),
		ISNULL(OpportunityItems.numQtyShipped,0),
		ISNULL(OpportunityBizDocItems.numUnitHour,0),
		ISNULL(OpportunityItems.bitDropShip,0)
	FROM
		OpportunityBizDocs
	INNER JOIN
		OpportunityBizDocItems
	ON
		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityBizDocItems.numItemCode = Item.numItemCode
	WHERE
		numOppBizDocsId = @numOppBizDocID

	BEGIN TRANSACTION
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			IF @numQty > @numQtyShipped
			BEGIN
				RAISERROR ('INVALID_SHIPPED_QTY',16,1);
			END

			--REVERT QUANTITY TO ON ALLOCATION IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription=CONCAT('SO Qty Shipped Return (Qty:',CAST(@numQty AS NUMERIC),' Shipped:',CAST(@numQtyShipped AS NUMERIC),')')
				
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(OKI.numWareHouseItemId,0),
						((ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(OKI.numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
					
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						IF @numChildQty > @numChildQtyShipped
						BEGIN
							RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
						END

						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems


								--LOOP OVER ALL CHILD ITEMS AND REVERT ALLOCATION
								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									IF @numKitChildQty > @numKitChildQtyShipped
									BEGIN
										RAISERROR ('INVALID_KIT_SUB_ITEM_SHIPPED_QTY',16,1);
									END

									IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
									BEGIN						
										--PLACE QTY BACK ON ALLOCATION
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = ISNULL(numAllocation,0) + @numKitChildQty,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID
									END
									ELSE -- When added fulfillment order
									BEGIN
										--PLACE QTY BACK ON ONHAND
										UPDATE  
											WareHouseItems
										SET     
											numOnHand = ISNULL(numOnHand,0) + @numKitChildQty,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID
									END

									--DECREASE QTY SHIPPED OF ORDER ITEM	
									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = CONCAT('SO CHILD KIT Qty Shipped Return (Qty:',CAST(@numKitChildQty AS NUMERIC),' Shipped:',CAST(@numKitChildQtyShipped AS NUMERIC),')')

									-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 
									
									SET @k = @k + 1
								END
							END

							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped Return (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE
						BEGIN
							IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
							BEGIN
								--PLACE QTY BACK ON ALLOCATION
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = ISNULL(numAllocation,0) + @numChildQty,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID
							END
							ELSE  -- When added fulfillment order
							BEGIN
								--PLACE QTY BACK ON ONHAND
								UPDATE  
									WareHouseItems
								SET     
									numOnHand = ISNULL(numOnHand,0) + @numChildQty,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID
							END
							      	
						
							--DECREASE QTY SHIPPED OF ORDER ITEM	
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) - ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped Return (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					IF @bitSerial = 1 OR @bitLot = 1
					BEGIN 
						IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
						BEGIN
							UPDATE
								WareHouseItems
							SET
								numAllocation = ISNULL(numAllocation,0) + @numQty
							WHERE
								numWareHouseItemID=@numWarehouseItemID
						END
						ELSE -- When added fulfillment order
						BEGIN
							-- ADD QUANTITY BACK ON ONHAND
							UPDATE  
								WareHouseItems
							SET     
								numOnHand = ISNULL(numOnHand,0) + @numQty
								,dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END

						-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
						EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numWareHouseItemID,
									@numReferenceID = @numOppId,
									@tintRefType = 3,
									@vcDescription = @vcDescription,
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	 


						DELETE FROM @TEMPSERIALLOT

						INSERT INTO @TEMPSERIALLOT
						(
							ID
							,numWarehouseItemID
							,vcSerialLot
							,numQty
						)
						SELECT 
							ROW_NUMBER() OVER(ORDER BY OWSI.numWarehouseItmsDTLID)
							,OWSI.numWarehouseItmsID
							,ISNULL(WID.vcSerialNo,'')
							,ISNULL(OWSI.numQty,0)
						FROM
							OppWarehouseSerializedItem OWSI
						INNER JOIN
							WareHouseItmsDTL WID
						ON
							OWSI.numWarehouseItmsDTLID = WID.numWareHouseItmsDTLID
						INNER JOIN	
							WarehouseItems WI
						ON
							OWSI.numWarehouseItmsID = WI.numWareHouseItemID
						WHERE 
							numOppID=@numOppId 
							AND numOppItemID=@numOppItemID 
							AND ISNULL(numOppBizDocsId,0) = @numOppBizDocID

						DECLARE @l INT = 1
						DECLARE @lCount INT
						DECLARE @numTempWarehouseItemID NUMERIC(18,0)
						DECLARE @vcSerialLot VARCHAR(100)
						DECLARE @vcTempDescription VARCHAR(500)
						DECLARE @numTempQty FLOAT
						SELECT @lCount = COUNT(*) FROM @TEMPSERIALLOT

						WHILE @l <= @lCount
						BEGIN
							SELECT
								@numTempWarehouseItemID=numWarehouseItemID
								,@vcSerialLot=vcSerialLot
								,@numTempQty=numQty 
							FROM 
								@TEMPSERIALLOT 
							WHERE 
								ID=@l

							IF @numWarehouseItemID <> @numTempWarehouseItemID
							BEGIN
								UPDATE 
									WareHouseItems
								SET
									numOnHand = ISNULL(numOnHand,0) + @numTempQty
								WHERE
									numWareHouseItemID=@numTempWarehouseItemID

								SET @vcTempDescription = CONCAT('Serial/Lot(',@vcSerialLot,') Shipped Return Qty(',@numTempQty,')') 

								EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numOppId, --  numeric(9, 0)
								@tintRefType = 3, --  tinyint
								@vcDescription = @vcTempDescription, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID
								
								UPDATE
									WareHouseItems
								SET
									numOnHand = (CASE WHEN ISNULL(numOnHand,0) >= @numTempQty THEN (ISNULL(numOnHand,0) - @numTempQty) ELSE 0 END)
									,numAllocation = ISNULL(numAllocation,0) - (CASE WHEN ISNULL(numOnHand,0) >= @numTempQty THEN 0 ELSE (CASE WHEN ISNULL(numAllocation,0) > ISNULL(numOnHand,0) THEN ISNULL(numOnHand,0) ELSE 0 END) END)
									,numBackOrder = ISNULL(numBackOrder,0) + (CASE WHEN ISNULL(numOnHand,0) >= @numTempQty THEN 0 ELSE (CASE WHEN ISNULL(numAllocation,0) > ISNULL(numOnHand,0) THEN ISNULL(numOnHand,0) ELSE 0 END) END)
								WHERE
									numWareHouseItemID=@numWarehouseItemID

								SET @vcTempDescription = CONCAT('Serial/Lot(',@vcSerialLot,') Shipped Return Qty(',@numTempQty,')') 

								EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numOppId, --  numeric(9, 0)
								@tintRefType = 3, --  tinyint
								@vcDescription = @vcTempDescription, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
							END

							SET @l = @l + 1
						END

						UPDATE OppWarehouseSerializedItem SET numOppBizDocsId=NULL WHERE numOppID=@numOppID AND numOppItemID= @numOppItemID AND numOppBizDocsId=@numOppBizDocID
					END
					ELSE
					BEGIN
						IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
						BEGIN
							--PLACE QTY BACK ON ALLOCATION
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = ISNULL(numAllocation,0) + @numQty
								,dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
						ELSE -- When added fulfillment order
						BEGIN
							-- ADD QUANTITY BACK ON ONHAND
							UPDATE  
								WareHouseItems
							SET     
								numOnHand = ISNULL(numOnHand,0) + @numQty
								,dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
					END
				END

				IF NOT (@bitSerial = 1 OR @bitLot = 1)
				BEGIN
					-- CREATE WAREHOUSE INVENTORY TRACKING ENTRY
					EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numWareHouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
				END
				
			END

			--DECREASE QTY SHIPPED OF ORDER ITEM	
			UPDATE 
				OpportunityItems
			SET     
				numQtyShipped = (ISNULL(numQtyShipped,0) - @numQty)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END
	COMMIT TRANSACTION 
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRAN

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END