--created by Prasanta

-- exec USP_GetOppItemBizDocs @numBizDocID=644,@numDomainID=169,@numOppID=476291,@numOppBizDocID=0,@bitRentalBizDoc=0
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTotalReceivedItemByBizDocType' ) 
    DROP PROCEDURE USP_GetTotalReceivedItemByBizDocType
GO
CREATE PROCEDURE USP_GetTotalReceivedItemByBizDocType
    @numBizDocTypeID AS NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		ISNULL(SUM(numUnitHour),0) AS totalUnit 
	FROM 
		OpportunityBizDocs OB
	INNER JOIN
		OpportunityBizDocItems OBDI
	ON
		OB.numOppBizDocsId = OBDI.numOppBizDocId
	WHERE
		numOppId=@numOppID 
		AND numBizDocId=@numBizDocTypeID
		AND numOppItemID=@numOppItemID
END




