GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DivisionMaster_GetDefaultSettingByFieldName')
DROP PROCEDURE USP_DivisionMaster_GetDefaultSettingByFieldName
GO
CREATE PROCEDURE [dbo].[USP_DivisionMaster_GetDefaultSettingByFieldName]
(
    @numDivisionID AS NUMERIC(18,0)
	,@vcDbColumnName VARCHAR(300)
)
AS 
BEGIN
	IF @vcDbColumnName = 'bitAllocateInventoryOnPickList'
	BEGIN
		SELECT ISNULL(bitAllocateInventoryOnPickList,0) AS bitAllocateInventoryOnPickList FROM DivisionMaster WHERE numDivisionID=@numDivisionID
	END
	ELSE IF @vcDbColumnName = 'numPrimaryContact'
	BEGIN
		SELECT 
			ISNULL(AdditionalContactsInformation.numContactId,0) AS numContactId 
		FROM 
			DivisionMaster 
		LEFT JOIN 
			AdditionalContactsInformation 
		ON  
			DivisionMaster.numDivisionID = AdditionalContactsInformation.numDivisionId
		WHERE 
			DivisionMaster.numDivisionID=@numDivisionID
			AND ISNULL(AdditionalContactsInformation.bitPrimaryContact,0) = 1
	END
END
GO