GO
/****** Object:  StoredProcedure [dbo].[USP_CheckDuplicateRefIDForJournal]    Script Date: 10th Mar,2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CheckDuplicateRefIDForJournal' ) 
    DROP PROCEDURE USP_CheckDuplicateRefIDForJournal
GO
CREATE PROCEDURE dbo.USP_CheckDuplicateRefIDForJournal
(
	@numJournalReferenceNo NUMERIC(18,0),
	@numDomainID NUMERIC(18,0)
)

AS 
BEGIN
	
	SELECT COUNT([GJH].[numJournal_Id]) AS [RowCount]
	FROM [dbo].[General_Journal_Header] AS GJH 
	WHERE [GJH].[numDomainId] = @numDomainID
	AND [GJH].[numJournalReferenceNo] = @numJournalReferenceNo
END