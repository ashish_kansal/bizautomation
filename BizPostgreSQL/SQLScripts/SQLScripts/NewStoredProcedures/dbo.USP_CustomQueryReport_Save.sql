SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_Save')
DROP PROCEDURE dbo.USP_CustomQueryReport_Save
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_Save]
	@numReportID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0),
	@vcReportName VARCHAR(300),
	@vcReportDescription VARCHAR(300),
	@vcEmailTo VARCHAR(1000),
	@tintEmailFrequency TINYINT,
	@vcQuery TEXT,
	@vcCSS VARCHAR(MAX)
AS 
BEGIN
	IF ISNULL(@numReportID,0) = 0 --INSERT
	BEGIN
		INSERT INTO CustomQueryReport
		(
			numDomainID,
			vcReportName,
			vcReportDescription,
			vcEmailTo,
			tintEmailFrequency,
			vcQuery,
			vcCSS,
			dtCreatedDate
		)
		VALUES
		(
			@numDomainID,
			@vcReportName,
			@vcReportDescription,
			@vcEmailTo,
			@tintEmailFrequency,
			@vcQuery,
			@vcCSS,
			GETDATE()
		)
	END
	ELSE --UPDATE
	BEGIN
		UPDATE
			CustomQueryReport
		SET
			numDomainID=@numDomainID,
			vcReportName=@vcReportName,
			vcReportDescription=@vcReportDescription,
			vcEmailTo=@vcEmailTo,
			tintEmailFrequency=@tintEmailFrequency,
			vcQuery=@vcQuery,
			vcCSS=@vcCSS
		WHERE	
			numReportID=@numReportID
	END		
END