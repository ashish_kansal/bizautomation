GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingPackageType_GetByShippingCompany')
DROP PROCEDURE USP_ShippingPackageType_GetByShippingCompany
GO
CREATE PROCEDURE [dbo].[USP_ShippingPackageType_GetByShippingCompany]  
	@numShippingCompanyID NUMERIC(18,0)
	,@bitEndicia BIT
AS  
BEGIN  
	SELECT
		ID
		,numNsoftwarePackageTypeID
		,vcPackageName
	FROM
		ShippingPackageType
	WHERE
		(numShippingCompanyID=ISNULL(@numShippingCompanyID,0) OR @numShippingCompanyID = 0)
		AND 1 = (CASE 
					WHEN (numShippingCompanyID = 88 OR numShippingCompanyID=91) THEN 1 
					ELSE (CASE WHEN ISNULL(bitEndicia,0) = @bitEndicia THEN 1 ELSE 0 END) 
				END)
END