/****** Object:  StoredProcedure [dbo].[Usp_cflList]    Script Date: 07/26/2008 16:15:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ServiceItemList')
DROP PROCEDURE USP_ServiceItemList
GO
CREATE PROCEDURE [dbo].[USP_ServiceItemList]    
@numDomainID as numeric(9)=0 ,   
@ItemType as char(10)=null
as    
    
SELECT numItemCode,vcItemName FROM Item WHERE numDomainID=@numDomainID AND charItemType=@ItemType
order by vcItemName desc
GO
