GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetCalculatedPrice')
DROP PROCEDURE USP_Item_GetCalculatedPrice
GO
CREATE PROCEDURE [dbo].[USP_Item_GetCalculatedPrice]
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numQty FLOAT
	,@vcChildKitSelectedItem VARCHAR(MAX)
AS
BEGIN
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numWarehouseItemID NUMERIC(18,0)

	SELECT @numWarehouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE  numSiteId=@numSiteID
	

	IF ISNULL(@numWarehouseID,0) > 0 AND EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID)
	BEGIN
		SELECT TOP 1 @numWarehouseItemID = numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID ASC
	END
	ELSE 
	BEGIN
		SELECT TOP 1 @numWarehouseItemID = numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemCode ORDER BY numWareHouseItemID ASC
	END


	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	SELECT @tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1) FROM Item WHERE numItemCode=@numItemCode

	SELECT
		ISNULL(monPrice,0) monPrice
		,ISNULL(monMSRPPrice,0) monMSRP
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQty,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,@vcChildKitSelectedItem,0,1)
END
GO