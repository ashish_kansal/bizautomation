GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetContracts2')
DROP PROCEDURE USP_GetContracts2
GO
CREATE PROCEDURE [dbo].[USP_GetContracts2]    
@numUserCntID NUMERIC(9),
@numDomainID NUMERIC(18,0),
@CurrentPage int,                                                         
@PageSize INT,
@TotRecs INT  OUTPUT,
@ClientTimeZoneOffset  INT,         
@intType INT,      
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@SearchText VARCHAR(300) = ''
AS                            
BEGIN

	DECLARE @vcDynamicQuery AS NVARCHAR(MAX)=''
	DECLARE @vcStatusQuery VARCHAR(500) = ''
	IF(LEN(@vcRegularSearchCriteria)>0)
	BEGIN
		SET @vcRegularSearchCriteria = ' AND '+@vcRegularSearchCriteria
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'.vcCompanyName','CI.vcCompanyName')
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'.vcContractsItemClassificationName','C.vcItemClassification')
	END
	SET @vcDynamicQuery = ' SELECT 
								COUNT(*) OVER() AS TotalRecords,
								C.numContractId,
								C.numIncidents,
								CI.vcCompanyName,
								C.numDivisonId AS numDivisionID,
								C.[numIncidentLeft],
								C.[numHours],
								C.[numMinutes],
								C.[timeLeft],
								C.[vcItemClassification],
								C.[numWarrantyDays],
								C.[vcNotes],
								C.[vcContractNo],
								C.timeUsed,
								(select
								   distinct  
									stuff((
										select '','' + vcData
										from ListDetails
										where numListID=36
										AND numListItemID IN (
								SELECT Items FROM dbo.Split(C.[vcItemClassification],'','') WHERE Items<>'''')
										order by vcData
										for xml path('''')
									),1,1,'''') as userlist
								from ListDetails where numListID=36
								AND numListItemID IN (
								SELECT Items FROM dbo.Split(C.[vcItemClassification],'','') WHERE Items<>'''')
								group by vcData) AS vcContractsItemClassificationName,
								dbo.fn_GetContactName(isnull(C.numCreatedBy,0)) + dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', C.dtmCreatedOn),'''+CAST(@numDomainID AS VARCHAR(MAX))+''') AS CreatedByOn,
								dbo.fn_GetContactName(isnull(C.numModifiedBy,0)) + dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', C.dtmModifiedOn),'''+CAST(@numDomainID AS VARCHAR(MAX))+''') AS ModifiedByOn,
								CONCAT(''<a href="javascript:void(0)" onclick="openContractPopup(''+CAST(C.numContractId AS VARCHAR)+'',''+CAST(C.intType AS VARCHAR)+'')">'',ISNULL(C.vcContractNo,''-''),''</a>'') AS [vcContractNo~1~0],
								dbo.FormatedDateFromDate(DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', C.dtmCreatedOn) ,'''+CAST(@numDomainID AS VARCHAR(MAX))+''')+'',''+dbo.fn_GetContactName(isnull(C.numCreatedBy,0)) AS [CreatedByOn~2~0],
								dbo.FormatedDateFromDate( DateAdd(MINUTE,-1 * ' +convert(varchar(15),@ClientTimeZoneOffset)+', C.dtmModifiedOn),'''+CAST(@numDomainID AS VARCHAR(MAX))+''')+'',''+dbo.fn_GetContactName(isnull(C.numModifiedBy,0)) AS [ModifiedByOn~3~0],
								CI.vcCompanyName AS [vcCompanyName~4~0],
								D.tintCRMType,
								(select
								   distinct  
									stuff((
										select '','' + vcData
										from ListDetails
										where numListID=36
										AND numListItemID IN (
								SELECT Items FROM dbo.Split(C.[vcItemClassification],'','') WHERE Items<>'''')
										order by vcData
										for xml path('''')
									),1,1,'''') as userlist
								from ListDetails where numListID=36
								AND numListItemID IN (
								SELECT Items FROM dbo.Split(C.[vcItemClassification],'','') WHERE Items<>'''')
								group by vcData) AS [vcContractsItemClassificationName~5~0],
								
								(ISNULL(C.numIncidents,0)  - ISNULL(C.numIncidentsUsed,0)) AS [numIncidentLeft~6~0],
								dbo.fn_SecondsConversion(((ISNULL(C.numHours,0) * 60 * 60) + (ISNULL(C.numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0)) AS [timeLeft~7~0],
								C.numWarrantyDays AS [numWarrantyDays~8~0],
								C.numCreatedBy AS numRecOwner,
								D.numTerID
							FROM 
								Contracts AS C
							LEFT JOIN 
								DivisionMaster AS D
							ON
								C.numDivisonId=D.numDivisionID
							LEFT JOIN
								CompanyInfo AS CI
							ON
								D.numCompanyId=CI.numCompanyId
							WHERE
								C.numDomainID='''+CAST(@numDomainID AS VARCHAR(MAX))+''' 
								AND C.intType='+CAST(@intType AS VARCHAR(MAX))+' '+@vcRegularSearchCriteria+' ORDER BY C.dtmCreatedOn desc OFFSET '+CAST(((@CurrentPage - 1) * @PageSize) AS VARCHAR(500))+' ROWS FETCH NEXT '+CAST(@PageSize AS VARCHAR(500))+' ROWS ONLY '

PRINT CAST(@vcDynamicQuery AS NTEXT)
EXEC(@vcDynamicQuery)

	DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
	)
INSERT INTO @TempColumns
		(
			vcFieldName
			,vcDbColumnName
			,vcOrigDbColumnName
			,bitAllowSorting
			,numFieldId
			,bitAllowEdit
			,bitCustomField
			,vcAssociatedControlType
			,vcListItemType
			,numListID
			,ListRelID
			,vcLookBackTableName
			,bitAllowFiltering
			,vcFieldDataType
			,bitClosedColumn
		)
		VALUES
		('Contract ID','vcContractNo','vcContractNo',0,1,0,0,'TextBox','',0,0,'Contracts',0,'V',0)
		,('Create on, by','CreatedByOn','CreatedByOn',0,2,0,0,'TextBox','',0,0,'Contracts',0,'V',0)
		,('Modified on, by','ModifiedByOn','ModifiedByOn',0,3,0,0,'TextBox','',0,0,'Contracts',0,'V',0)
		,('Organizations','vcCompanyName','vcCompanyName',0,4,0,0,'TextBox','',0,0,'Contracts',1,'V',0)
		,('Item Classifications','vcContractsItemClassificationName','vcContractsItemClassificationName',0,5,0,0,'SelectBox','',0,0,'Contracts',1,'V',0)
		,('Incidents Left','numIncidentLeft','numIncidentLeft',0,6,0,0,'TextBox','',0,0,'Contracts',0,'V',0)
		,('Time Left','timeLeft','timeLeft',0,7,0,0,'TextBox','',0,0,'Contracts',0,'V',0)
		,('Days Left','numWarrantyDays','numWarrantyDays',0,8,0,0,'TextBox','',0,0,'Contracts',0,'V',0)

	UPDATE
		TC
	SET
		TC.intColumnWidth = ISNULL(DFCD.intColumnWidth,0)
	FROM
		@TempColumns TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 146
		AND DFCD.tintPageType = 1

		SELECT * FROM @TempColumns
	--IF @numWOStatus <> 1 AND EXISTS (SELECT numFieldId FROM DycFormConfigurationDetails WHERE numDomainId = @numDomainID AND numUserCntID = @numUserCntID AND numFormId = 145 AND tintPageType = 1)
	--BEGIN
	--	SELECT 
	--		TC.*
	--	FROM 
	--		DycFormConfigurationDetails DFCD 
	--	INNER JOIN 
	--		@TempColumns TC 
	--	ON 
	--		DFCD.numFieldId=TC.numFieldId
	--	WHERE 
	--		numDomainId = @numDomainID 
	--		AND numUserCntID = @numUserCntID 
	--		AND numFormId = 145 
	--		AND tintPageType = 1
	--	ORDER BY
	--		DFCD.intRowNum
	--END
	--ELSE
	--BEGIN
	--	SELECT * FROM @TempColumns
	--END
END
GO