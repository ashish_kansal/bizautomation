SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Execute')
DROP PROCEDURE dbo.USP_DemandForecast_Execute
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Execute]
	@numDFID NUMERIC (18,0)
	,@vcSelectedIDs VARCHAR(MAX)
	,@numUserCntID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@CurrentPage INT
    ,@PageSize INT
	,@columnName VARCHAR(MAX)                                                          
	,@columnSortOrder VARCHAR(10)
	,@numDomainID NUMERIC(18,0)
	,@vcRegularSearchCriteria VARCHAR(MAX)=''
	,@bitShowAllItems BIT
	,@bitShowHistoricSales BIT
	,@numHistoricalAnalysisPattern BIT
	,@bitBasedOnLastYear BIT
	,@bitIncludeOpportunity BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@tintDemandPlanBasedOn TINYINT
	,@tintPlanType TINYINT
AS 
BEGIN
	DECLARE @dtLastExecution DATETIME
	DECLARE @bitWarehouseFilter BIT = 0
	DECLARE @bitItemClassificationFilter BIT = 0
	DECLARE @bitItemGroupFilter BIT = 0

	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END				
	END

	DECLARE @tintUnitsRecommendationForAutoPOBackOrder TINYINT
	DECLARE @bitIncludeRequisitions BIT

	SELECT 
		@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
		,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainID

	DECLARE @TEMP TABLE
	(
		numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numUnitHour FLOAT
		,dtReleaseDate DATE
	)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		Item.numItemCode
		,OpportunityItems.numWarehouseItmsID
		,OpportunityItems.numUnitHour - ISNULL(OpportunityItems.numQtyShipped,0)
		,ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate)
	FROM
		OpportunityMaster
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.tintOppType=1
		AND OpportunityMaster.tintOppStatus=1
		AND ISNULL(OpportunityMaster.tintshipped,0)=0
		AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		Item.numItemCode
		,OKI.numWareHouseItemId
		,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
		,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OI.numoppitemtCode=OKI.numOppItemID
	INNER JOIN
		Item
	ON
		OKI.numChildItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OKI.numWareHouseItemId = WI.numWareHouseItemID
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0)=0
		AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OI.bitWorkOrder,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		Item.numItemCode
		,OKCI.numWareHouseItemId
		,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
		,ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		OpportunityKitChildItems OKCI
	ON
		OI.numoppitemtCode = OKCI.numOppItemID
	INNER JOIN
		Item
	ON
		OKCI.numItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OKCI.numWareHouseItemId = WI.numWareHouseItemID
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0)=0
		AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OI.bitWorkOrder,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	INSERT INTO @TEMP
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
	)
	SELECT
		WOD.numChildItemID
		,WOD.numWareHouseItemId
		,WOD.numQtyItemsReq
		,(CASE WHEN OM.numOppId IS NOT NULL THEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) ELSE WO.dtmEndDate END)
	FROM
		WorkOrderDetails WOD
	INNER JOIN
		Item
	ON
		WOD.numChildItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		WOD.numWareHouseItemId = WI.numWareHouseItemID
	INNER JOIN
		WorkOrder WO
	ON
		WOD.numWOId=WO.numWOId
	LEFT JOIN
		OpportunityItems OI
	ON
		WO.numOppItemID = OI.numoppitemtCode
	LEFT JOIN
		OpportunityMaster OM
	ON
		OI.numOppId=OM.numOppId
	WHERE
		WO.numDomainID=@numDomainID
		AND WO.numWOStatus <> 23184 -- NOT COMPLETED
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END)
		AND ISNULL(Item.bitKitParent,0) = 0
		AND ISNULL(WOD.numQtyItemsReq,0) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.dtmEndDate IS NOT NULL)
		AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcSelectedIDs,'')) > 0 
					THEN (CASE WHEN Item.numItemCode IN (SELECT Id FROM dbo.SplitIDs(@vcSelectedIDs,',')) THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND
		(
			@bitWarehouseFilter = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID =@numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			Item.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			Item.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	CREATE TABLE #TEMPItems
	(
		numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numUnitHour FLOAT
		,dtReleaseDate VARCHAR(MAX)
		,dtReleaseDateHidden VARCHAR(MAX)
	)

	INSERT INTO #TEMPItems
	(
		numItemCode
		,numWarehouseItemID
		,numUnitHour
		,dtReleaseDate
		,dtReleaseDateHidden
	)
	SELECT
		numItemCode
		,numWarehouseItemID
		,SUM(numUnitHour)
		,STUFF((SELECT 
					CONCAT(', <a href="#" onclick="return OpenDFReleaseDateRecords(',numItemCode,',',numWarehouseItemID,',''',CONVERT(VARCHAR(10), dtReleaseDate, 101),''');">',CONVERT(VARCHAR(10), dtReleaseDate, 101),'</a> (',SUM(numUnitHour),')')
				FROM 
					@TEMP
				WHERE 
					numItemCode=T1.numItemCode
					AND numWarehouseItemID=T1.numWarehouseItemID
				GROUP BY
					numItemCode
					,numWarehouseItemID
					,dtReleaseDate
				FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				,1,2,'')
		,STUFF((SELECT 
					CONCAT(', ',CONVERT(VARCHAR(10), dtReleaseDate, 101),' (',SUM(numUnitHour),')')
				FROM 
					@TEMP
				WHERE 
					numItemCode=T1.numItemCode
					AND numWarehouseItemID=T1.numWarehouseItemID
				GROUP BY
					numItemCode
					,numWarehouseItemID
					,dtReleaseDate
				FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
				,1,2,'')
	FROM
		@TEMP T1
	GROUP BY
		numItemCode
		,numWarehouseItemID

	---------------------------- Dynamic Query -------------------------------
		
	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
			AND 1 = (CASE 
						WHEN vcOrigDbColumnName='numBuildableQty' 
						THEN (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END) 
						ELSE 1 
					END)
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=139 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	IF @Nocolumns=0
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			139,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=139 
			AND bitDefault=1 
			AND ISNULL(bitSettingField,0)=1 
			AND numDomainID=@numDomainID
		ORDER BY 
			tintOrder asc   
	END

	INSERT INTO 
		#tempForm
	SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
		vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
		vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
	FROM 
		View_DynamicColumns 
	WHERE 
		numFormId=139 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1 
		AND ISNULL(bitSettingField,0)=1 
		AND ISNULL(bitCustom,0)=0  
		AND numRelCntType = 0
		AND 1 = (CASE 
						WHEN vcOrigDbColumnName='numBuildableQty' 
						THEN (CASE WHEN @tintPlanType=2 THEN 1 ELSE 0 END) 
						ELSE 1 
					END)
	UNION
	SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=139 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1 
		AND ISNULL(bitCustom,0)=1 
		AND numRelCntType = 0
	ORDER BY 
		tintOrder asc
					
	DECLARE  @strColumns AS VARCHAR(MAX) = ''
	SET @strColumns = CONCAT(' COUNT(*) OVER() TotalRowsCount, Item.numItemCode, Item.vcItemName, Item.vcSKU
	,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') AS vcUOM
	,Item.charItemType AS vcItemType
	,Item.numAssetChartAcntId
	,Item.monAverageCost
	,Item.numBusinessProcessId
	,SPLM.Slp_Name
	,SPLM.numBuildManager
	,dbo.fn_GetContactName(SPLM.numBuildManager) vcBuildManager
	,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = Item.numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
	,ISNULL(Item.numPurchaseUnit,0) AS numUOM
	,dbo.fn_UOMConversion(ISNULL(numBaseUnit, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numPurchaseUnit, 0)) fltUOMConversionfactor
	,dbo.fn_GetAttributes(WI.numWareHouseItemId,1) AS vcAttributes
	,WI.numWareHouseID
	,WI.numWareHouseItemID
	,W.vcWarehouse
	,ISNULL(V.numVendorID,0) numVendorID
	,ISNULL(V.intMinQty,0) intMinQty
	,ISNULL(monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,Item.numDomainID,Item.numPurchaseUnit) AS monVendorCost
	,T1.numUnitHour AS numQtyToPurchase, T1.dtReleaseDateHidden, T1.dtReleaseDate,(CASE
		WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'= 3
		THEN 
			(ISNULL(numBackOrder,0) - ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  
											THEN ISNULL((SELECT 
														SUM(numUnitHour) 
													FROM 
														OpportunityItems 
													INNER JOIN 
														OpportunityMaster 
													ON 
														OpportunityItems.numOppID=OpportunityMaster.numOppId 
													WHERE 
														OpportunityMaster.numDomainId=',@numDomainID,'
														AND OpportunityMaster.tintOppType=2 
														AND OpportunityMaster.tintOppStatus=0 
														AND OpportunityItems.numItemCode=Item.numItemCode 
														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) 
											ELSE 0 
											END)) +
			(CASE WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
		WHEN ',@tintUnitsRecommendationForAutoPOBackOrder,'=2
		THEN 
			(CASE
				WHEN ISNULL(Item.fltReorderQty,0) >= ISNULL(V.intMinQty,0) AND ISNULL(Item.fltReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(Item.fltReorderQty,0)
				WHEN ISNULL(V.intMinQty,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(V.intMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(V.intMinQty,0)
				WHEN ISNULL(numBackOrder,0) >= ISNULL(Item.fltReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(V.intMinQty,0) THEN ISNULL(numBackOrder,0)
				ELSE ISNULL(Item.fltReorderQty,0)
			END) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
														SUM(numUnitHour) 
													FROM 
														OpportunityItems 
													INNER JOIN 
														OpportunityMaster 
													ON 
														OpportunityItems.numOppID=OpportunityMaster.numOppId 
													WHERE 
														OpportunityMaster.numDomainId=',@numDomainID,'
														AND OpportunityMaster.tintOppType=2 
														AND OpportunityMaster.tintOppStatus=0 
														AND OpportunityItems.numItemCode=Item.numItemCode 
														AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
		ELSE
			(CASE WHEN ISNULL(Item.fltReorderQty,0) > ISNULL(V.intMinQty,0) THEN ISNULL(Item.fltReorderQty,0) ELSE ISNULL(V.intMinQty,0) END) 
			+ ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN ',@bitIncludeRequisitions,' = 1  THEN ISNULL((SELECT 
																													SUM(numUnitHour) 
																												FROM 
																													OpportunityItems 
																												INNER JOIN 
																													OpportunityMaster 
																												ON 
																													OpportunityItems.numOppID=OpportunityMaster.numOppId 
																												WHERE 
																													OpportunityMaster.numDomainId=',@numDomainID,'
																													AND OpportunityMaster.tintOppType=2 
																													AND OpportunityMaster.tintOppStatus=0 
																													AND OpportunityItems.numItemCode=Item.numItemCode 
																													AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) 
	END) AS numQtyBasedOnPurchasePlan ')

	--Custom field 
	DECLARE @tintOrder AS TINYINT = 0                                                 
	DECLARE @vcFieldName AS VARCHAR(50)                                                  
	DECLARE @vcListItemType AS VARCHAR(3)                                             
	DECLARE @vcAssociatedControlType VARCHAR(20)     
	declare @numListID AS numeric(9)
	DECLARE @vcDbColumnName VARCHAR(50)                      
	DECLARE @WhereCondition VARCHAR(MAX) = ''                   
	DECLARE @vcLookBackTableName VARCHAR(2000)                
	DECLARE @bitCustom AS BIT
	DECLARE @bitAllowEdit AS CHAR(1)                   
	Declare @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)    
	DECLARE @vcColumnName AS VARCHAR(500)             
	Declare @ListRelID as numeric(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = '' 
	   
	SELECT TOP 1 
		@tintOrder=tintOrder+1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName
		,@bitCustom=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=bitAllowEdit
		,@ListRelID=ListRelID
	FROM  
		#tempForm 
	ORDER BY 
		tintOrder ASC   

	WHILE @tintOrder>0                                                  
	BEGIN
	
		IF @bitCustom = 0  
		BEGIN
			DECLARE @Prefix AS VARCHAR(10)

			IF @vcLookBackTableName = 'AdditionalContactsInformation'
				SET @Prefix = 'ADC.'
			ELSE IF @vcLookBackTableName = 'DivisionMaster'
				SET @Prefix = 'Div.'
			ELSE IF @vcLookBackTableName = 'OpportunityMaster'
				SET @PreFix = 'Opp.'			
			ELSE IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			ELSE IF @vcLookBackTableName = 'DemandForecastGrid'
				SET @PreFix = 'DF.'
			ELSE IF @vcLookBackTableName = 'Warehouses'
				SET @PreFix = 'W.'
			ELSE IF @vcLookBackTableName = 'WareHouseItems'
				SET @PreFix = 'WI.'
			ELSE 
				SET @PreFix = CONCAT(@vcLookBackTableName,'.')
			
			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

			IF @vcAssociatedControlType = 'TextBox'
			BEGIN
				IF @vcDbColumnName = 'numQtyToPurchase'
				BEGIN
					SET @strColumns=@strColumns+',' + ' (CASE WHEN ISNULL(WI.numBackOrder,0) > ISNULL(WI.numOnOrder,0) THEN  CEILING(ISNULL(WI.numBackOrder,0) - ISNULL(WI.numOnOrder,0))  ELSE 0 END) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'moncost'
				BEGIN
					SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = Item.numBaseUnit),''-'') '  + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 
				END
			END
			ELSE IF  @vcAssociatedControlType='Label'                                              
			BEGIN
				IF @vcDbColumnName = 'vc7Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,7,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc15Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,15,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc30Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,30,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc60Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,60,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc90Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,90,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF @vcDbColumnName = 'vc180Days'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_DemandForecastDaysDisplay(',@numDomainID,',',@ClientTimeZoneOffset,',Item.numItemCode,ISNULL(TempLeadDays.numLeadDays,0),WI.numWarehouseItemID,180,',@bitShowHistoricSales,',',@numHistoricalAnalysisPattern,',',@bitBasedOnLastYear,',',@bitIncludeOpportunity,',',@numOpportunityPercentComplete,',',@tintDemandPlanBasedOn,') [',@vcColumnName,']')
				END
				ELSE IF (@vcDbColumnName = 'vcBuyUOM')
				BEGIN
					SET @strColumns=@strColumns+',' + ' ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = ISNULL(Item.numPurchaseUnit,0)),'''')' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numOnOrderReq'
				BEGIN
					SET @strColumns=@strColumns+',' + ' (ISNULL(WI.numOnOrder,0) + ISNULL(WI.numReorder,0)) '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numAvailable'
				BEGIN
					SET @strColumns=@strColumns+',' + 'CONCAT(ISNULL(WI.numOnHand,0) + ISNULL(WI.numAllocation,0),'' ('',ISNULL(WI.numOnHand,0),'')'')'  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numTotalOnHand'
				BEGIN
					SET @strColumns=@strColumns+',' + 'CONCAT(ISNULL((SELECT SUM(WIInner.numOnHand) + SUM(WIInner.numAllocation) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0),'' ('',ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0),'')'')'  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numBuildableQty'
				BEGIN
					SET @strColumns=@strColumns+ CONCAT(',dbo.fn_GetAssemblyPossibleWOQty(Item.numItemCode,WI.numWareHouseID) [',@vcColumnName,']')
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
				END   
			END
			ELSE IF  @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'  
			BEGIN
				IF @vcDbColumnName = 'numVendorID'
				BEGIN
					SET @strColumns=@strColumns+',' + ' V.numVendorID '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'ShipmentMethod'
				BEGIN
					SET @strColumns=@strColumns+',' + ' '''' '  + ' [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
				END
			END
			ELSE IF @vcAssociatedControlType='DateField'   
			BEGIN
				IF @vcDbColumnName = 'dtExpectedDelivery'
				BEGIN
					SET @strColumns=@strColumns+',' + ' GETDATE() '  + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'ItemRequiredDate'
				BEGIN
					SET @strColumns=@strColumns+',T1.dtReleaseDate [' + @vcColumnName + ']'
				END
				ELSE
				BEGIN
					SET @strColumns=@strColumns + ',' + @Prefix + @vcDbColumnName  +' ['+ @vcColumnName+']' 
				END					
			END
		END

		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,
			@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
		FROM
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 SET @tintOrder=0 

		END
	  
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	SET @StrSql = CONCAT('SELECT ',@strColumns,' 
						FROM
							#TEMPItems T1
						INNER JOIN
							Item
						ON
							T1.numItemCode = Item.numItemCode
						LEFT JOIN
							Sales_process_List_Master SPLM
						ON
							Item.numBusinessProcessId = SPLM.Slp_Id
						INNER JOIN
							WarehouseItems WI
						ON
							Item.numItemCode=WI.numItemID
							AND T1.numWarehouseItemID=WI.numWarehouseItemID
						INNER JOIN
							Warehouses W
						ON
							WI.numWarehouseID = W.numWareHouseID
						LEFT JOIN
							Vendor V
						ON
							Item.numVendorID = V.numVendorID
							AND Item.numItemCode = V.numItemCode
						OUTER APPLY
						(
							SELECT TOP 1
								numListValue AS numLeadDays
							FROM
								VendorShipmentMethod VSM
							WHERE
								VSM.numVendorID = V.numVendorID
							ORDER BY
								(CASE WHEN VSM.numWarehouseID=W.numWareHouseID THEN 1 ELSE 0 END) DESC, ISNULL(VSM.numWarehouseID,0) DESC, ISNULL(bitPrimary,0) DESC, bitPreferredMethod DESC, numListItemID ASC
						) TempLeadDays
						WHERE
							Item.numDomainID=',@numDomainID,'
							AND 1 = (CASE 
										WHEN ',@bitShowAllItems,' = 1 
										THEN 1 
										ELSE 
											(CASE 
												WHEN ', ISNULL(@tintDemandPlanBasedOn,1),' = 2
												THEN (CASE 
														WHEN ((ISNULL(WI.numBackOrder,0) 
																- ISNULL((SELECT SUM(WIInner.numOnOrder) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0) 
																- ISNULL((SELECT SUM(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0)) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode AND WIInner.numWarehouseItemID <> WI.numWarehouseItemID AND WIInner.numWarehouseID = WI.numWarehouseID),0)) > 0 
																OR (ISNULL(numReorder,0) > 0 AND ISNULL((SELECT SUM(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0) + ISNULL(WIInner.numOnOrder,0)) FROM WarehouseItems WIInner WHERE WIInner.numItemID=Item.numItemCode),0) < ISNULL(WI.numReorder,0))) 
														THEN 1
														ELSE 0
													END)
												ELSE
													(CASE 
														WHEN 
															T1.numUnitHour > 0 
														THEN 1 
														ELSE 0 
													END)
											END)
									END)
						')

	IF CHARINDEX('I.',@vcRegularSearchCriteria) > 0
	BEGIN			   
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'I.','Item.')
	END
	IF CHARINDEX('WH.',@vcRegularSearchCriteria) > 0
	BEGIN			   
		SET @vcRegularSearchCriteria = REPLACE(@vcRegularSearchCriteria,'WH.','W.')
	END
	
	IF @vcRegularSearchCriteria<>'' 
	BEGIN
		SET @strSql=@strSql +' AND ' + @vcRegularSearchCriteria 
	END
	  
	DECLARE  @firstRec  AS INTEGER
	DECLARE  @lastRec  AS INTEGER
  
	SET @firstRec = (@CurrentPage - 1) * @PageSize
	SET @lastRec = (@CurrentPage * @PageSize + 1)

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS NVARCHAR(MAX)
	SET @strFinal = CONCAT(@StrSql,' ORDER BY ',CASE WHEN CHARINDEX(@columnName,@strColumns) > 0 THEN @columnName ELSE 'Item.numItemCode' END,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

	PRINT CAST(@strFinal AS NTEXT)
	EXEC sp_executesql @strFinal

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #TEMPItems


	UPDATE DemandForecast SET dtExecutionDate = dateadd(MINUTE,-@ClientTimeZoneOffset,GETDATE()) WHERE numDFID = @numDFID

	SELECT CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,@dtLastExecution)) AS dtLastExecution
END
GO