GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DomainMarketplace_Save')
DROP PROCEDURE USP_DomainMarketplace_Save
GO
CREATE PROCEDURE [dbo].[USP_DomainMarketplace_Save]  
	@numDomainID NUMERIC(18,0)
	,@numMarketplaceID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
AS
BEGIN
	UPDATE
		DomainMarketplace
	SET
		numDivisionID = @numDivisionID
	WHERE	
		numDomainID = @numDomainID
		AND numMarketplaceID = @numMarketplaceID
END