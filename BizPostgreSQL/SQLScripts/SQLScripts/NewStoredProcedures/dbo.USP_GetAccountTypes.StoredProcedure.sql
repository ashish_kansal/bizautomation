--Created by chintan
--EXEC [dbo].USP_GetAccountTypes 72,0,0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountTypes')
DROP PROCEDURE USP_GetAccountTypes
GO
CREATE PROCEDURE [dbo].[USP_GetAccountTypes]
               @numDomainId      AS NUMERIC(9)  = 0,
               @numAccountTypeID AS NUMERIC(9)  = 0,
               @tintMode         TINYINT  = 0
AS
  BEGIN
    IF @tintMode = 1 -- select all account type with len(accountcode) = 4
      BEGIN
        SELECT [numAccountTypeID],
               [vcAccountCode],
               [vcAccountType]
                 + '-'
                 + [vcAccountCode] AS [vcAccountType],
                 [vcAccountType] AS [vcAccountType1],
               ISNULL([numParentID],0) AS [numParentID],
               [numDomainID],
               [vcAccountCode] + '~' + CAST ([numAccountTypeID] AS VARCHAR(20)) AS CandidateKey
        FROM   [AccountTypeDetail]
        WHERE  [numDomainID] = @numDomainId
               AND LEN([vcAccountCode]) = 4
      END
    ELSE IF @tintMode = 2
		BEGIN
		DECLARE @vcParentAccountCode VARCHAR(50)
			SELECT @vcParentAccountCode=vcAccountCode FROM [AccountTypeDetail] WHERE numAccountTypeID=@numAccountTypeID
			SELECT [numAccountTypeID],
               [vcAccountCode],
               [vcAccountType]
                 + '-'
                 + [vcAccountCode] AS [vcAccountType],
                  [vcAccountType] AS [vcAccountType1],
               ISNULL([numParentID],0) AS [numParentID],
               [numDomainID]
        FROM   [AccountTypeDetail]
        WHERE  [numDomainID] = @numDomainId
               AND LEN([vcAccountCode]) > 4
               AND [vcAccountCode] LIKE  @vcParentAccountCode +'%' 
		END
    ELSE IF @tintMode = 3 --Select all account type which has atleast one account 
	  SELECT    [numAccountTypeID],
				[vcAccountCode],
				[vcAccountType] /*+ '-' + [vcAccountCode]*/ AS [vcAccountType],
				ISNULL([numParentID],0) AS [numParentID],
				[numDomainID]
	  FROM      [AccountTypeDetail]
	  WHERE     [numDomainID] = @numDomainId
				AND [numAccountTypeID] IN (
				SELECT  ISNULL([numParntAcntTypeId], 0)
				FROM    [Chart_Of_Accounts]
				WHERE   [numDomainId] = @numDomainId )
    ELSE 
      BEGIN
        IF @numAccountTypeID = 0
          BEGIN
            SELECT [numAccountTypeID],
                   [vcAccountCode],
                   [vcAccountType]
                     + '-'
                     + [vcAccountCode] AS [vcAccountType],
                   [vcAccountType] AS [vcAccountType1],
                   ISNULL([numParentID],0) AS [numParentID],
                   [numDomainID],
				   ISNULL(bitActive,0) bitActive
            FROM   [AccountTypeDetail]
            WHERE  [numDomainID] = @numDomainId
                   AND [numParentID] IS NULL

            SELECT [numAccountTypeID],
                   [vcAccountCode],
                   [vcAccountType]
                     + '-'
                     + [vcAccountCode] AS [vcAccountType],
                     [vcAccountType] AS [vcAccountType1],
                   ISNULL([numParentID],0) AS [numParentID],
                   [numDomainID],
				   ISNULL(bitActive,0) bitActive
            FROM   [AccountTypeDetail]
            WHERE  [numDomainID] = @numDomainId
                   AND [numParentID] IS NOT NULL
                   ORDER BY (CASE vcAccountType WHEN 'Assets' THEN 1 WHEN 'Liabilities' THEN 2 WHEN 'Equity' THEN 3 WHEN 'Income' THEN 4 WHEN 'Cost Of Goods' THEN 5 WHEN 'Expense' THEN 6 ELSE 7 END),numParentID,ISNULL(tintSortOrder,0),vcAccountCode
                   
          END
        ELSE
          IF @numAccountTypeID > 0
            BEGIN
              SELECT [numAccountTypeID],
                     [vcAccountCode],
                     [vcAccountType],
                     ISNULL([numParentID],0) AS [numParentID],
                     [numDomainID],
					 [bitActive]
              FROM   [AccountTypeDetail]
              WHERE  [numDomainID] = @numDomainId
                     AND [numAccountTypeID] = @numAccountTypeID
            END
      END
  END
