
--Created by Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateCurrencyDetails')
DROP PROCEDURE USP_UpdateCurrencyDetails
GO
CREATE PROCEDURE USP_UpdateCurrencyDetails
@numDomanID as numeric(9),
@fltExchangeRate as float,
@varCurrSymbol as nvarchar(3),
@numCurrencyId as numeric(9),
@bitEnabled as bit,
@vcCurrencyDesc varchar(100),
@numCountryID AS NUMERIC(9)
as

update Currency set vcCurrencyDesc=@vcCurrencyDesc,
varCurrSymbol=@varCurrSymbol, fltExchangeRate=@fltExchangeRate, 
bitEnabled=@bitEnabled,numCountryID=@numCountryID
where numCurrencyId=@numCurrencyId