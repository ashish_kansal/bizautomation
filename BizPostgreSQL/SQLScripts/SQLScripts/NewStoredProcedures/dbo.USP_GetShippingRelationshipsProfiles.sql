GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingRelationshipsProfiles' ) 
    DROP PROCEDURE USP_GetShippingRelationshipsProfiles
GO
CREATE PROCEDURE USP_GetShippingRelationshipsProfiles
   
    @numDomainID NUMERIC(9)
AS 
BEGIN
    SELECT DISTINCT
	(SELECT CONCAT( (SELECT vcData from ListDetails LD 
							JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship
							WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID ) , ' / ' , 
								(SELECT vcData from ListDetails LD 
								JOIN  ShippingRules S ON LD.numListItemID = S.numProfile
								WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID )
							)) AS vcRelProfile,

	(SELECT CONCAT( (SELECT numListItemID from ListDetails LD 
							JOIN  ShippingRules S ON LD.numListItemID = S.numRelationship 
							WHERE LD.numListItemID = S.numRelationship AND S.numRuleID = SR.numRuleID) , '-' , 
								(SELECT numListItemID from ListDetails LD 
								JOIN  ShippingRules S ON LD.numListItemID = S.numProfile 
								WHERE LD.numListItemID = S.numProfile  AND S.numRuleID = SR.numRuleID)
							)) AS numRelProfileID

	FROM ShippingRules SR 
	WHERE numDomainID = @numDomainID AND SR.numRelationship > 0 AND SR.numProfile > 0
END