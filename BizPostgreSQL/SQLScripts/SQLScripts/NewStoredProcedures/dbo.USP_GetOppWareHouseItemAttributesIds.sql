/****** Object:  StoredProcedure [dbo].[USP_GetOppWareHouseItemAttributes]    Script Date: 07/26/2008 16:18:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppWareHouseItemAttributesIds')
DROP PROCEDURE USP_GetOppWareHouseItemAttributesIds
GO
CREATE PROCEDURE [dbo].[USP_GetOppWareHouseItemAttributesIds]
@numRecID as numeric(9)=0
AS
BEGIN
	IF ISNULL(@numRecID,0) > 0
	BEGIN
		SELECT dbo.fn_GetAttributesIds(@numRecID)
	END
	BEGIN
		SELECT ''
	END
END
GO
