GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddressDetails_UpdateRecordID')
DROP PROCEDURE USP_AddressDetails_UpdateRecordID
GO
CREATE PROCEDURE [dbo].[USP_AddressDetails_UpdateRecordID]    
	@numDomainID NUMERIC(18,0)
	,@numAddressID NUMERIC(18,0)
	,@numRecordID NUMERIC(18,0)
AS
BEGIN
	UPDATE
		AddressDetails
	SET
		numRecordID=@numRecordID
	WHERE
		numDomainID=@numDomainID
		AND numAddressID=@numAddressID
END
GO