
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_AutoFulFill')
DROP PROCEDURE USP_OpportunityBizDocs_AutoFulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_AutoFulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT
	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS FLOAT

	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	--IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	--BEGIN
	--	RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	--END

	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
	SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

	BEGIN TRANSACTION

		-- CHANGE BIZDIC QTY TO AVAIALLBE QTY TO SHIP
		UPDATE
			OBI
		SET
			OBI.numUnitHour = (CASE 
								WHEN OM.tintOppType = 1 AND OM.tintOppStatus = 1 
								THEN 
									(CASE 
										WHEN (I.charItemType = 'p' AND ISNULL(OBI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
										THEN 
											(CASE WHEN OBI.numUnitHour <= dbo.GetOrderItemAvailableQtyToShip(OBI.numOppItemID,OBI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) THEN OBI.numUnitHour ELSE dbo.GetOrderItemAvailableQtyToShip(OBI.numOppItemID,OBI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) END)
										ELSE OBI.numUnitHour
									END) 
								ELSE OBI.numUnitHour
							END)
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityItems OI
		ON
			OBI.numOppItemID=OI.numOppItemtcode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBI.numItemCode = I.numItemCode
		WHERE
			numOppBizDocID=@numOppBizDocID

		--CHANGE TOTAL AMOUNT
		UPDATE
			OBI
		SET
			OBI.monTotAmount = (OI.monTotAmount/OI.numUnitHour) * ISNULL(OBI.numUnitHour,0),
			OBI.monTotAmtBefDiscount = (OI.monTotAmtBefDiscount/OI.numUnitHour) * ISNULL(OBI.numUnitHour,0)
		FROM
			OpportunityBizDocItems OBI
		INNER JOIN
			OpportunityItems OI
		ON
			OBI.numOppItemID=OI.numOppItemtcode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OBI.numItemCode = I.numItemCode
		WHERE
			numOppBizDocID=@numOppBizDocID

		DECLARE @TempFinalTable TABLE
		(
			ID INT IDENTITY(1,1),
			numItemCode NUMERIC(18,0),
			vcItemType CHAR(1),
			bitSerial BIT,
			bitLot BIT,
			bitAssembly BIT,
			bitKit BIT,
			numOppItemID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numQtyShipped FLOAT,
			numQty FLOAT,
			bitDropShip BIT
		)

		INSERT INTO @TempFinalTable
		(
			numItemCode,
			vcItemType,
			bitSerial,
			bitLot,
			bitAssembly,
			bitKit,
			numOppItemID,
			numWarehouseItemID,
			numQtyShipped,
			numQty,
			bitDropShip
		)
		SELECT
			OpportunityBizDocItems.numItemCode,
			ISNULL(Item.charItemType,''),
			ISNULL(Item.bitSerialized,0),
			ISNULL(Item.bitLotNo,0),
			ISNULL(Item.bitAssembly,0),
			ISNULL(Item.bitKitParent,0),
			ISNULL(OpportunityItems.numoppitemtcode,0),
			ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
			ISNULL(OpportunityItems.numQtyShipped,0),
			ISNULL(OpportunityBizDocItems.numUnitHour,0),
			ISNULL(OpportunityItems.bitDropShip,0)
		FROM
			OpportunityBizDocs
		INNER JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
		INNER JOIN
			OpportunityItems
		ON
			OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OpportunityBizDocItems.numItemCode = Item.numItemCode
		WHERE
			numOppBizDocsId = @numOppBizDocID

		/* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		SELECT @COUNT=COUNT(*) FROM @TempFinalTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT
				@numOppItemID=numOppItemID,
				@numWarehouseItemID=numWarehouseItemID,
				@numQty=numQty,
				@bitSerial=bitSerial,
				@bitLot=bitLot 
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
			IF @bitSerial = 1
			BEGIN
				IF ISNULL((SELECT SUM(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND ISNULL(numOppBizDocsId,0) = 0),0) <> @numQty
				BEGIN
					RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT 
									OppWarehouseSerializedItem.numWarehouseItmsDTLID
								FROM 
									OppWarehouseSerializedItem 
								LEFT JOIN	
									WareHouseItmsDTL
								ON
									OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
								WHERE 
									numOppID=@numOppId 
									AND numOppItemID=@numOppItemID 
									AND ISNULL(numOppBizDocsId,0) = 0
									AND WareHouseItmsDTL.numWareHouseItmsDTLID IS NULL)
					BEGIN
						RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
					END
				END
			END

			IF @bitLot = 1
			BEGIN
				IF ISNULL((SELECT SUM(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND ISNULL(numOppBizDocsId,0) = 0),0) <> @numQty
				BEGIN
					RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT 
									OppWarehouseSerializedItem.numWarehouseItmsDTLID
								FROM 
									OppWarehouseSerializedItem 
								LEFT JOIN	
									WareHouseItmsDTL
								ON
									OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
								WHERE 
									numOppID=@numOppId 
									AND numOppItemID=@numOppItemID 
									AND ISNULL(numOppBizDocsId,0) = 0
									AND WareHouseItmsDTL.numWareHouseItmsDTLID IS NULL)
					BEGIN
						RAISERROR('INVALID_LOT_NUMBERS',16,1)
					END
				END
			END

			SET @i = @i + 1
		END
		
			
	
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS FLOAT
		DECLARE @numOnHand AS FLOAT

		DECLARE @TEMPSERIALLOT TABLE
		(
			ID INT
			,numWarehouseItemID NUMERIC(18,0)
			,vcSerialLot VARCHAR(100)
			,numQty FLOAT
		)

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS FLOAT
		DECLARE @numChildOnHand AS FLOAT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS FLOAT
		DECLARE @numChildQtyShipped AS FLOAT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty FLOAT,
			numChildQtyShipped FLOAT
		)

		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS FLOAT
		DECLARE @numKitChildOnHand AS FLOAT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS FLOAT
		DECLARE @numKitChildQtyShipped AS FLOAT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty FLOAT,
			numKitChildQtyShipped FLOAT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription=CONCAT('SO Qty Shipped (Qty:',CAST(@numQty AS NUMERIC),' Shipped:',CAST(@numQtyShipped AS NUMERIC),')')
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0),@numChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0),@numKitChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
									BEGIN
										--REMOVE QTY FROM ALLOCATION
										SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

										IF @numKitChildAllocation >= 0
										BEGIN
											UPDATE  
												WareHouseItems
											SET     
												numAllocation = @numKitChildAllocation,
												dtModified = GETDATE() 
											WHERE   
												numWareHouseItemID = @numKitChildWarehouseItemID
										END
										ELSE
										BEGIN
											RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
										END
									END
									ELSE  -- When added fulfillment order
									BEGIN
										--REMOVE QTY FROM ON HAND
										SET @numKitChildOnHand = @numKitChildOnHand - @numKitChildQty

										IF @numKitChildOnHand >= 0
										BEGIN
											UPDATE  
												WareHouseItems
											SET     
												numOnHand = @numKitChildOnHand,
												dtModified = GETDATE() 
											WHERE   
												numWareHouseItemID = @numKitChildWarehouseItemID
										END
										ELSE
										BEGIN
											RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
										END
									END

									UPDATE  
										OpportunityKitChildItems
									SET 
										numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
									WHERE   
										numOppKitChildItemID = @numOppKitChildItemID
										AND numOppChildItemID = @numOppChildItemID 
										AND numOppId=@numOppId
										AND numOppItemID=@numOppItemID

									SET @vcKitChildDescription = CONCAT('SO CHILD KIT Qty Shipped (Qty:',CAST(@numKitChildQty AS NUMERIC),' Shipped:',CAST(@numKitChildQtyShipped AS NUMERIC),')')

									EXEC dbo.USP_ManageWareHouseItems_Tracking
										@numWareHouseItemID = @numKitChildWarehouseItemID,
										@numReferenceID = @numOppId,
										@tintRefType = 3,
										@vcDescription = @vcKitChildDescription,
										@numModifiedBy = @numUserCntID,
										@numDomainID = @numDomainID	 

									SET @k = @k + 1
								END
							END 

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	
						END
						ELSE
						BEGIN
							IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
							BEGIN
								--REMOVE QTY FROM ALLOCATION
								SET @numChildAllocation = @numChildAllocation - @numChildQty

								IF @numChildAllocation >= 0
								BEGIN
									UPDATE  
										WareHouseItems
									SET     
										numAllocation = @numChildAllocation,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numChildWarehouseItemID 
								END
								ELSE
								BEGIN
									RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
								END
							END
							ELSE -- When added fulfillment order
							BEGIN
								--REMOVE QTY FROM ONHAND
								SET @numChildOnHand = @numChildOnHand - @numChildQty

								IF @numChildOnHand >= 0
								BEGIN
									UPDATE  
										WareHouseItems
									SET     
										numOnHand = @numChildOnHand,
										dtModified = GETDATE() 
									WHERE   
										numWareHouseItemID = @numChildWarehouseItemID 
								END
								ELSE
								BEGIN
									RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
								END
							END     	
							
							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped (Qty:',CAST(@numChildQty AS NUMERIC),' Shipped:',CAST(@numChildQtyShipped AS NUMERIC),')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					IF @bitSerial = 1 OR @bitSerial = 1
					BEGIN
						DELETE FROM @TEMPSERIALLOT

						INSERT INTO @TEMPSERIALLOT
						(
							ID
							,numWarehouseItemID
							,vcSerialLot
							,numQty
						)
						SELECT 
							ROW_NUMBER() OVER(ORDER BY OWSI.numWarehouseItmsDTLID)
							,OWSI.numWarehouseItmsID
							,ISNULL(WID.vcSerialNo,'')
							,ISNULL(OWSI.numQty,0)
						FROM
							OppWarehouseSerializedItem OWSI
						INNER JOIN
							WareHouseItmsDTL WID
						ON
							OWSI.numWarehouseItmsDTLID = WID.numWareHouseItmsDTLID
						INNER JOIN	
							WarehouseItems WI
						ON
							OWSI.numWarehouseItmsID = WI.numWareHouseItemID
						WHERE 
							numOppID=@numOppId 
							AND numOppItemID=@numOppItemID 
							AND ISNULL(numOppBizDocsId,0) = 0

						DECLARE @l INT = 1
						DECLARE @lCount INT
						DECLARE @numTempWarehouseItemID NUMERIC(18,0)
						DECLARE @vcSerialLot VARCHAR(100)
						DECLARE @vcTempDescription VARCHAR(500)
						DECLARE @numTempQty FLOAT
						SELECT @lCount = COUNT(*) FROM @TEMPSERIALLOT

						WHILE @l <= @lCount
						BEGIN
							SELECT
								@numTempWarehouseItemID=numWarehouseItemID
								,@vcSerialLot=vcSerialLot
								,@numTempQty=numQty 
							FROM 
								@TEMPSERIALLOT 
							WHERE 
								ID=@l

							IF @numWarehouseItemID <> @numTempWarehouseItemID
							BEGIN
								IF ISNULL((SELECT numOnHand FROM WareHouseItems WHERE numWareHouseItemID=@numTempWarehouseItemID),0) >= @numTempQty
								BEGIN
									UPDATE 
										WareHouseItems
									SET
										numOnHand = ISNULL(numOnHand,0) - @numTempQty
									WHERE
										numWareHouseItemID=@numTempWarehouseItemID

									SET @vcTempDescription = CONCAT('Serial/Lot(',@vcSerialLot,') Picked Qty(',@numTempQty,')') 

									EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @vcTempDescription, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	 
								END
								ELSE
								BEGIN
									RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
								END
								
								UPDATE
									WareHouseItems
								SET
									numBackOrder = (CASE WHEN @numTempQty >= ISNULL(numBackOrder,0) THEN 0 ELSE (ISNULL(numBackOrder,0) - @numTempQty) END)
									,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN @numTempQty >= ISNULL(numBackOrder,0) THEN ISNULL(numBackOrder,0) ELSE 0 END)
									,numOnHand = ISNULL(numOnHand,0) + (CASE WHEN @numTempQty >= ISNULL(numBackOrder,0) THEN (@numTempQty - ISNULL(numBackOrder,0)) ELSE 0 END)
								WHERE
									numWareHouseItemID=@numWarehouseItemID

								SET @vcTempDescription = CONCAT('Serial/Lot(',@vcSerialLot,') Received Qty(',@numTempQty,')') 

								EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numOppId, --  numeric(9, 0)
								@tintRefType = 3, --  tinyint
								@vcDescription = @vcTempDescription, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
							END

							SET @l = @l + 1
						END
					END
					
					SELECT @numAllocation = ISNULL(numAllocation,0),@numOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					IF @tintCommitAllocation = 1 OR (@tintCommitAllocation = 2 AND @bitAllocateInventoryOnPickList=1)--When order is created OR Allocation on pick list add
					BEGIN
						--REMOVE QTY FROM ALLOCATION
						SET @numAllocation = @numAllocation - @numQty

						IF (@numAllocation >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET     
								numAllocation = @numAllocation,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
						ELSE
						BEGIN
							RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
						END
					END
					ELSE 
					BEGIN
						--REMOVE QTY FROM ON HAND
						SET @numOnHand = @numOnHand - @numQty

						IF (@numOnHand >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET     
								numOnHand = @numOnHand,
								dtModified = GETDATE() 
							WHERE   
								numWareHouseItemID = @numWareHouseItemID
						END
						ELSE
						BEGIN
							RAISERROR ('NOTSUFFICIENTQTY_ONHAND',16,1);
						END
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OppWarehouseSerializedItem SET numOppBizDocsId=@numOppBizDocID WHERE numOppID=@numOppId AND ISNULL(numOppBizDocsId,0) = 0
		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=GETUTCDATE() WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

