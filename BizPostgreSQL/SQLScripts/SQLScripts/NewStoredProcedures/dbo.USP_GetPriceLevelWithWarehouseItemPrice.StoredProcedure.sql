GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceLevelWithWarehouseItemPrice')
DROP PROCEDURE USP_GetPriceLevelWithWarehouseItemPrice
GO
CREATE PROCEDURE [dbo].[USP_GetPriceLevelWithWarehouseItemPrice]                                        
@numItemCode as numeric(9),
@numDomainID as numeric(9)=0,            
@numWareHouseID as numeric(9),
@numDivisionID as numeric(9)=0                  
as                 

DECLARE @numWareHouseItemID AS BIGINT
SELECT TOP 1 @numWareHouseItemID = ISNULL(numWareHouseItemID,0) FROM dbo.WareHouseItems WHERE numWareHouseID = @numWareHouseID AND numItemID = @numItemCode AND ISNULL(numWLocationID,0) <> -1

SELECT  ISNULL(I.numItemCode,0) AS [numItemCode],
		ISNULL(W.vcWareHouse,'') AS [vcWareHouse],
		--ISNULL(I.monListPrice,ISNULL(WI.monWListPrice,0)) AS decDiscount,
		ISNULL(WI.monWListPrice,0) AS decDiscount,
		CONVERT(VARCHAR(10),W.numWareHouseID) + '~' + CONVERT(VARCHAR(10),WI.numWareHouseItemID) + '~' + CONVERT(VARCHAR(10),ISNULL(monWListPrice,0)) + '~' + ISNULL(W.vcWareHouse,'') AS [KeyValue]
FROM    Item I
LEFT JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode AND WI.numDomainID = I.numDomainID
LEFT JOIN dbo.Warehouses W ON W.numWareHouseID = WI.numWareHouseID
WHERE ISNULL(I.numItemCode,0)= @numItemCode AND (W.numWareHouseID = ISNULL(@numWareHouseID,0) OR ISNULL(@numWareHouseID,0) = 0) AND ISNULL(numWLocationID,0) <> -1

IF @numWareHouseID > 0
BEGIN

DECLARE @monListPrice AS DECIMAL(20,5);SET @monListPrice=0
DECLARE @monVendorCost AS DECIMAL(20,5);SET @monVendorCost=0

if ((@numWareHouseItemID>0) and exists(select * from item where numItemCode=@numItemCode and charItemType='P'))      
	begin      
		select @monListPrice=isnull(monWListPrice,0) from WareHouseItems where numWareHouseItemID=@numWareHouseItemID      
		if @monListPrice=0 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode       
	end      
	else      
	begin      
		 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode      
	end 

SELECT @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)

SELECT  [numPricingID],
		[numPriceRuleID],
		[intFromQty],
		[intToQty],
		[tintRuleType],
		[tintDiscountType],
        CASE WHEN tintRuleType=1 AND tintDiscountType=1 --Deduct from List price & Percentage
			THEN @monListPrice - (@monListPrice * ( decDiscount /100))
         WHEN tintRuleType=1 AND tintDiscountType=2 --Deduct from List price & Flat discount
			THEN @monListPrice - decDiscount
         WHEN tintRuleType=2 AND tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
			THEN @monVendorCost + (@monVendorCost * ( decDiscount /100))
         WHEN tintRuleType=2 AND tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
			THEN @monVendorCost + decDiscount
         WHEN tintRuleType=3 --Named Price
			THEN decDiscount
        END AS decDiscount,ISNULL(W.vcWareHouse,'') AS [vcWareHouse],WI.numWareHouseID,WI.numWareHouseItemID
        FROM    [PricingTable] PT,dbo.WareHouseItems WI JOIN dbo.Warehouses W ON W.numWareHouseID = WI.numWareHouseID
WHERE   ISNULL(PT.numItemCode,0)=@numItemCode AND ISNULL(PT.numCurrencyID,0) = 0 AND ISNULL(WI.numItemID,0)= @numItemCode AND (W.numWareHouseID = ISNULL(@numWareHouseID,0) OR ISNULL(@numWareHouseID,0) = 0) AND ISNULL(numWLocationID,0) <> -1
ORDER BY [numPricingID] 
  
  
declare @numRelationship as numeric(9)                  
declare @numProfile as numeric(9)                  

  select @numRelationship=numCompanyType,@numProfile=vcProfile from DivisionMaster D                  
join CompanyInfo C on C.numCompanyId=D.numCompanyID                  
where numDivisionID =@numDivisionID       
  
SELECT  PT.numPricingID,
		PT.numPriceRuleID,
		PT.intFromQty,
		PT.intToQty,
		PT.tintRuleType,
		PT.tintDiscountType,
		CASE WHEN PT.tintRuleType=1 AND PT.tintDiscountType=1 --Deduct from List price & Percentage
				THEN @monListPrice - (@monListPrice * ( PT.decDiscount /100))
             WHEN PT.tintRuleType=1 AND PT.tintDiscountType=2 --Deduct from List price & Flat discount
				THEN @monListPrice - PT.decDiscount
             WHEN PT.tintRuleType=2 AND PT.tintDiscountType=1 --Add to Primary Vendor Cost & Percentage
				THEN @monVendorCost + (@monVendorCost * ( PT.decDiscount /100))
             WHEN PT.tintRuleType=2 AND PT.tintDiscountType=2 --Add to Primary Vendor Cost & Flat discount
				THEN @monVendorCost + PT.decDiscount END AS decDiscount,ISNULL(W.vcWareHouse,'') AS [vcWareHouse],WI.numWareHouseID,WI.numWareHouseItemID
FROM    Item I
        JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
        JOIN [PricingTable] PT ON P.numPricRuleID = PT.numPriceRuleID AND ISNULL(PT.numCurrencyID,0) = 0
        ,dbo.WareHouseItems WI JOIN dbo.Warehouses W ON W.numWareHouseID = WI.numWareHouseID        
WHERE   I.numItemCode = @numItemCode AND P.tintRuleFor=1 AND P.tintPricingMethod = 1
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
) AND ISNULL(WI.numItemID,0)= @numItemCode AND (W.numWareHouseID = ISNULL(@numWareHouseID,0) OR ISNULL(@numWareHouseID,0) = 0)
ORDER BY PP.Priority ASC

END 
     