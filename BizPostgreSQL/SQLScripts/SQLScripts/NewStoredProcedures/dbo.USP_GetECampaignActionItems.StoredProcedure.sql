
/****** Object:  StoredProcedure [dbo].[USP_GetECampaignActionItems]    Script Date: 06/04/2009 15:15:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECampaignActionItems')
DROP PROCEDURE USP_GetECampaignActionItems
GO
CREATE PROCEDURE [dbo].[USP_GetECampaignActionItems]
AS
  BEGIN
  
  	DECLARE @LastDateTime DATETIME
	DECLARE @CurrentDateTime DATETIME
	
	SELECT @LastDateTime = [dtLastDateTimeECampaign] FROM [WindowsServiceHistory]
	SET @CurrentDateTime = DATEADD(minute, DATEpart(minute,GETUTCDATE()) ,DATEADD(hour,DATEpart(hour,GETUTCDATE()),dbo.[GetUTCDateWithoutTime]()))
	PRINT @LastDateTime
	PRINT @CurrentDateTime


    SELECT   C.numContactID,
             C.numRecOwner,
             intStartDate,
             E.numActionItemTemplate,
             A.numDomainID,
             A.numDivisionId,
             DTL.[numConECampDTLID]
    FROM     ConECampaign C
             JOIN ConECampaignDTL DTL
               ON numConEmailCampID = numConECampID
             JOIN ECampaignDTLs E
               ON DTL.numECampDTLId = E.numECampDTLID
             JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID]
             JOIN AdditionalContactsInformation A
               ON A.numContactID = C.[numContactID]
    WHERE    bitSend IS NULL 
            AND [bitEngaged]= 1
            AND [numActionItemTemplate] > 0
		    AND ((DTL.dtExecutionDate BETWEEN DATEADD(HOUR,-2,@LastDateTime) AND @CurrentDateTime) OR
			(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND ISNULL(bitSend,0) = 1) = 0)
              

  END
