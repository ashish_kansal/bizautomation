GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetOppOrders')
DROP PROCEDURE dbo.USP_ElasticSearch_GetOppOrders
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetOppOrders]
	@numDomainID NUMERIC(18,0)
	,@vcOppIds VARCHAR(MAX)
AS 
BEGIN
	
	SELECT
		numOppId AS id
		,'order' AS module
		,ISNULL(tintOppType,0) tintOppType 
		,ISNULL(tintOppStatus,0) tintOppStatus
		,ISNULL(OpportunityMaster.numAssignedTo,0) numAssignedTo
		,ISNULL(DivisionMaster.numTerID,0) numTerID
		,ISNULL(OpportunityMaster.numRecOwner,0) numRecOwner
		,CONCAT(CASE WHEN tintOppType=1 THEN '<b style="color:#00aa50">' ELSE '<b style="color:#ff0000">' END,CASE WHEN tintOppType=1 THEN 'Sales ' ELSE 'Purchase ' END ,CASE WHEN tintOppStatus=1 THEN ' (order)' ELSE ' (opportunity)' END,':</b> ',vcPOppName,', ',vcCompanyName, ', ', monDealAmount) AS displaytext
		,vcPOppName AS [text]
		,CONCAT('/opportunity/frmOpportunities.aspx?opId=',numOppId) AS url
		,vcPOppName AS Search_vcPOppName
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND (@vcOppIds IS NULL OR OpportunityMaster.numOppId IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcOppIds,'0'),',')))
	ORDER BY
		numOppID DESC
END