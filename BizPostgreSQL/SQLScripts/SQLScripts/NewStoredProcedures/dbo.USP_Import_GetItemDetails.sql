SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_GetItemDetails')
DROP PROCEDURE USP_Import_GetItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_GetItemDetails]  
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
AS  
BEGIN
	SELECT
		Item.numItemCode
		,Item.numShipClass
		,Item.vcItemName
		,Item.monListPrice
		,Item.txtItemDesc
		,Item.vcModelID
		,Item.vcManufacturer
		,Item.numBarCodeId
		,Item.fltWidth
		,Item.fltHeight
		,Item.fltWeight
		,Item.fltLength
		,Item.monAverageCost
		,Item.bitSerialized
		,Item.bitKitParent
		,Item.bitAssembly
		,Item.IsArchieve
		,Item.bitLotNo
		,Item.bitAllowBackOrder
		,Item.numIncomeChartAcntId
		,Item.numAssetChartAcntId
		,Item.numCOGsChartAcntId
		,Item.numItemClassification
		,Item.numItemGroup
		,Item.numPurchaseUnit
		,Item.numSaleUnit
		,Item.numItemClass
		,Item.numBaseUnit
		,Item.vcSKU
		,Item.charItemType
		,Item.bitTaxable
		,ItemExtendedDetails.txtDesc
		,Item.vcExportToAPI
		,Item.bitAllowDropShip
		,Item.bitMatrix
		,ISNULL((SELECT MAX(numReOrder) FROM WarehouseItems WHERE numItemID=Item.numItemCOde),0) numReOrder
	FROM
		Item
	LEFT JOIN
		ItemExtendedDetails
	ON
		Item.numItemCode = ItemExtendedDetails.numItemCode
	WHERE
		numDomainID = @numDomainID
		AND Item.numItemCode = @numItemCode
END