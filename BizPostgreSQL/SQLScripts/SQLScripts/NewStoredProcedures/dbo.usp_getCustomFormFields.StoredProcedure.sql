/****** Object:  StoredProcedure [dbo].[usp_getCustomFormFields]    Script Date: 07/26/2008 16:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getCustomFormFields' ) 
    DROP PROCEDURE usp_getCustomFormFields
GO
CREATE PROCEDURE [dbo].[usp_getCustomFormFields]
    @numDomainId NUMERIC(9),
    @vcLocID VARCHAR(50)--Comma seperated location id from CFW_Loc_Master
AS 
    CREATE TABLE #tempAvailableFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(50),
        vcFieldType VARCHAR(15),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
        vcListItemType CHAR(1),intColumnNum INT,intRowNum INT,boolRequired tinyint,
          vcFieldDataType CHAR(1),boolAOIField tinyint,vcLookBackTableName NVARCHAR(50))
  
   
        INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,
          vcFieldDataType,boolAOIField,vcLookBackTableName)                         
          
            SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                   CASE GRP_ID WHEN 4 then 'C' 
								WHEN 1 THEN 'D' 
								WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
								ELSE 'C' 
					END AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    Fld_label vcDbColumnName,
                    CASE WHEN C.numListID > 0 THEN 'L'
                         ELSE ''
                    END vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(V.bitIsRequired,0) as boolRequired,
                    CASE WHEN C.numListID > 0 THEN 'N'
                         ELSE 'V'
                    END vcFieldDataType,
                    0 boolAOIField,
                    '' AS vcLookBackTableName
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
            WHERE   C.numDomainID = @numDomainId
                    AND GRP_ID IN (SELECT id from dbo.SplitIDs(@vcLocID,','))

        
  		SELECT * FROM #tempAvailableFields 
        
        DROP TABLE #tempAvailableFields
        
GO
