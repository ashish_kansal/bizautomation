GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InsertAssemblyItems')
DROP PROCEDURE USP_InsertAssemblyItems
GO

CREATE PROCEDURE [dbo].[USP_InsertAssemblyItems]
@numItemKitID AS NUMERIC(18,0)=0,
@numChildItemID AS numeric(18,0)=0,
@numQtyItemsReq AS FLOAT=0,
@numUOMId AS NUMERIC(18,0)=0,
@numWareHouseId AS NUMERIC(18,0)=0,
@vcItemDesc AS TEXT,
@sintOrder AS INT=0,
@numItemDetailID NUMERIC(18,0) OUTPUT
AS
BEGIN
 
IF (SELECT COUNT(*) FROM ItemDetails WHERE numItemKitID = @numItemKitID AND numChildItemID = @numChildItemID) = 0
BEGIN
	insert into ItemDetails(numItemKitID,numChildItemID,numQtyItemsReq,numUOMId,vcItemDesc,sintOrder)
	values(@numItemKitID,@numChildItemID,@numQtyItemsReq,@numUOMId,@vcItemDesc,@sintOrder)
	
	SET @numItemDetailID = SCOPE_IDENTITY()
	PRINT @numItemDetailID
END 
END