GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingBox')
DROP PROCEDURE USP_ManageShippingBox
GO
CREATE PROCEDURE USP_ManageShippingBox
(
	@numShippingReportId NUMERIC(18,0),
    @strItems TEXT,
    @numUserCntId NUMERIC(18,0),
	@tintMode TINYINT,
	@numBoxID NUMERIC(18,0)	OUT,
	@PageMode TINYINT
)
AS 
BEGIN
	DECLARE @hDocItem INT
    
    IF @tintMode = 1 
    BEGIN
		IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                  
            UPDATE 
				dbo.ShippingBox
            SET
				dtDeliveryDate = X.dtDeliveryDate ,
                monShippingRate = X.monShippingRate,
				fltTotalWeight =X.[fltTotalWeight],
				[fltHeight] =X.[fltHeight],
				[fltWidth] = X.[fltWidth],
				[fltLength] = X.[fltLength],
				[numPackageTypeID] = X.numPackageTypeID,
				[numServiceTypeID] = X.numServiceTypeID,
				[fltDimensionalWeight] = X.fltDimensionalWeight,
				numShipCompany = X.numShipCompany
            FROM
			(
				SELECT 
					*
                FROM 
					OPENXML (@hDocItem, '/NewDataSet/Box', 2)
				WITH 
				(
					dtDeliveryDate DATETIME, 
					monShippingRate DECIMAL(20,5), 
					numBoxID NUMERIC(18,0), 
					[fltTotalWeight] FLOAT, 
					[fltHeight] FLOAT, 
					[fltWidth] FLOAT, 
					[fltLength] FLOAT, 
					numPackageTypeID BIGINT, 
					numServiceTypeID BIGINT, 
					fltDimensionalWeight FLOAT,
					numShipCompany INT
				)
            ) X
            WHERE 
				X.numBoxID = dbo.ShippingBox.numBoxID
				AND numShippingReportID = @numShippingReportId

            EXEC sp_xml_removedocument @hDocItem
                
        END
    END 
  
	IF @tintMode=0
	BEGIN
		DELETE FROM [ShippingReportItems] WHERE [numShippingReportId]=@numShippingReportId	
		DELETE  FROM [ShippingBox] WHERE [numShippingReportId] = @numShippingReportId

        IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                                
            INSERT  INTO [ShippingBox]
            (
                [vcBoxName],
                [numShippingReportId],
                [fltTotalWeight],
                [fltHeight],
                [fltWidth],
                [fltLength],
				numCreatedBy,
				numPackageTypeID,
				numServiceTypeID,
				fltDimensionalWeight,
				numShipCompany
	        )
            SELECT  
				X.vcBoxName,
                @numShippingReportId,
                X.[fltTotalWeight],
                X.[fltHeight],
                X.[fltWidth],
                X.[fltLength],
				@numUserCntId,
				numPackageTypeID,
				numServiceTypeID,
				fltDimensionalWeight,
				numShipCompany
			FROM
			(
				SELECT
					*
                FROM
					OPENXML (@hDocItem, '/NewDataSet/Box', 2)
                WITH 
				(
					vcBoxName VARCHAR(20), 
					[fltTotalWeight] FLOAT, 
					[fltHeight] FLOAT, 
					[fltWidth] FLOAT, 
					[fltLength] FLOAT, 
					numPackageTypeID BIGINT, 
					numServiceTypeID BIGINT,
					fltDimensionalWeight FLOAT,
					numShipCompany INT
				)
            ) X
				
			SET @numBoxID = SCOPE_IDENTITY()
                                                  
            EXEC sp_xml_removedocument @hDocItem
                                               
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
  
            INSERT  INTO [ShippingReportItems]
            (
                [numShippingReportId],
                [numItemCode],
                [tintServiceType],
                [dtDeliveryDate],
                [monShippingRate],
                [fltTotalWeight],
                [intNoOfBox],
                [fltHeight],
                [fltWidth],
                [fltLength],
                [dtCreateDate],
                [numCreatedBy],
                [numBoxID],
                numOppBizDocItemID,
                intBoxQty
            )
            SELECT 
				@numShippingReportId,
                OBI.[numItemCode],
                ISNULL((SELECT TOP 1 numServiceTypeID FROM ShippingBox WHERE numBoxID=@numBoxID),0),--Unspecified
                NULL,
                0.0
                ,(CASE 
					WHEN ISNULL(bitKitParent,0)=1 
							AND (CASE 
									WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
									THEN 1
									ELSE 0
								END) = 0
					THEN 
						dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
					ELSE 
						ISNULL(fltWeight,0) 
				END),
                1,
                I.[fltHeight],
                I.[fltWidth],
                I.[fltLength],
                GETUTCDATE(),
                @numUserCntId,
                ( SELECT    [numBoxID]
                    FROM      [ShippingBox]
                    WHERE     vcBoxName = X.vcBoxName
                            AND numShippingReportId = @numShippingReportId
                ),
                X.[numOppBizDocItemID],
                X.intBoxQty
            FROM
			(
				SELECT
					* 
                FROM 
					OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                WITH 
				( 
					vcBoxName VARCHAR(20)
					,numOppBizDocItemID NUMERIC
					,intBoxQty BIGINT 
				)
            ) X     
            INNER JOIN [OpportunityBizDocItems] OBI ON X.numOppBizDocItemID = OBI.[numOppBizDocItemID]
			INNER JOIN OpportunityItems OI ON OBI.numOppItemID=OI.numoppitemtCode
            INNER JOIN Item I ON OBI.[numItemCode] = I.[numItemCode]
                        
            EXEC sp_xml_removedocument @hDocItem

        END
	END
END
GO
