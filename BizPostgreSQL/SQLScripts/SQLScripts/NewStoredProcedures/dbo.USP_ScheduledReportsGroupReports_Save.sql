GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroupReports_Save')
DROP PROCEDURE dbo.USP_ScheduledReportsGroupReports_Save
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroupReports_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@vcReports VARCHAR(MAX)
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		DECLARE @TEMP TABLE
		(
			tintReportType TINYINT
			,numReportID NUMERIC(18,0)
			,intSortOrder INT
		)

		DECLARE @hDoc as INTEGER
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcReports

		INSERT INTO @TEMP
		(
			tintReportType
			,numReportID
			,intSortOrder
		)
		SELECT
			ReportType
			,ReportID
			,SortOrder
		FROM
			OPENXML(@hDoc,'/NewDataSet/Table1',2)                                                                          
		WITH                       
		(
			ReportType TINYINT
			,ReportID NUMERIC(18,0)
			,SortOrder INT
		)

		EXEC sp_xml_removedocument @hDoc

		IF (SELECT COUNT(*) FROM @TEMP) > 5
		BEGIN
			RAISERROR('MAX_5_REPORTS',16,1)
		END
		ELSE
		BEGIN
			DELETE SRGR FROM ScheduledReportsGroup SRG INNER JOIN ScheduledReportsGroupReports SRGR ON SRG.ID = SRGR.numSRGID WHERE SRG.numDomainID = @numDomainID AND numSRGID=@numSRGID

			INSERT INTO ScheduledReportsGroupReports
			(
				numSRGID
				,tintReportType
				,numReportID
				,intSortOrder
			)
			SELECT
				@numSRGID
				,tintReportType
				,numReportID
				,intSortOrder
			FROM
				@TEMP
		END
	END
	ELSE IF @tintMode = 2
	BEGIN
		IF EXISTS (SELECT numReportID FROM ReportListMaster WHERE numDomainID=@numDomainID AND numReportID=CAST(@vcReports AS NUMERIC))
		BEGIN
			IF EXISTS (SELECT ID FROM ScheduledReportsGroupReports WHERE numSRGID=@numSRGID AND tintReportType=1 AND numReportID=CAST(@vcReports AS NUMERIC))
			BEGIN
				RAISERROR('REPORT_ALREADY_ADDED',16,1)
			END
			ELSE
			BEGIN
				IF (SELECT COUNT(*) FROM ScheduledReportsGroupReports WHERE numSRGID=@numSRGID) >= 5
				BEGIN
					RAISERROR('MAX_5_REPORTS',16,1)
				END
				ELSE
				BEGIN
					INSERT INTO ScheduledReportsGroupReports
					(
						numSRGID
						,tintReportType
						,numReportID
						,intSortOrder
					)
					VALUES
					(
						@numSRGID
						,1
						,CAST(@vcReports AS NUMERIC)
						,0
					)
				END
			END
		END
		ELSE
		BEGIN
			RAISERROR('REPORT_DOES_NOT_EXISTS',16,1)
		END
	END
END
GO