
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DisableSMTP')
DROP PROCEDURE USP_DisableSMTP
GO
CREATE PROCEDURE USP_DisableSMTP
@numUserId NUMERIC(9),
@numDomainId NUMERIC(9)
AS 
BEGIN
	UPDATE [UserMaster] SET [bitSMTPServer]=0 WHERE [numUserId]=@numUserId AND [numDomainID]=@numDomainId
END