--EXEC USP_ManageOpportunityLinking 0,5760,'Projects',0,191
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageOpportunityLinking')
DROP PROCEDURE USP_ManageOpportunityLinking
GO
CREATE PROCEDURE USP_ManageOpportunityLinking
    @numParentOppID NUMERIC(9) = 0,
    @numChildOppID NUMERIC(9) = 0,
    @vcSource VARCHAR(50) = NULL,
    @numParentOppBizDocID NUMERIC(9) = NULL,
    @numParentProjectID NUMERIC(9) = NULL,
    @numWebApiId NUMERIC(9) = NULL
AS 
    BEGIN
    IF @numParentOppID = 0
		SET @numParentOppID = NULL
    IF @numParentOppBizDocID = 0
		SET @numParentOppBizDocID = NULL
	IF @numParentProjectID = 0
		SET @numParentProjectID = NULL
    
        INSERT  INTO OpportunityLinking
                (
                  numParentOppID,
                  numChildOppID,
                  vcSource,
                  numParentOppBizDocID,
                  numParentProjectID,
                  numWebApiId
	          )
        VALUES  (
                  @numParentOppID,
                  @numChildOppID,
                  @vcSource,
                  @numParentOppBizDocID,
                  @numParentProjectID,
                  @numWebApiId
                ) 
    END