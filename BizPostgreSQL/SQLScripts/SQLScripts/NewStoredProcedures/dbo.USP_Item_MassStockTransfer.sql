GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_MassStockTransfer' ) 
    DROP PROCEDURE USP_Item_MassStockTransfer
GO
CREATE PROCEDURE [dbo].[USP_Item_MassStockTransfer]
@numDomainID NUMERIC(18,0),
@numUserCntID NUMERIC(18,0),
@strItems as NVARCHAR(MAX)=''
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	IF LEN(ISNULL(@strItems,'')) > 0
	BEGIN
		DECLARE @hDocItem INT
		DECLARE @description VARCHAR(MAX)

		DECLARE @TEMP AS TABLE
		(
			ID INT IDENTITY(1,1)
			,numFromItemCode NUMERIC(18,0)
			,numQty NUMERIC(18,0)
			,numFromWarehouseItemID NUMERIC(18,0)
			,numToItemCode NUMERIC(18,0)
			,numToWarehouseItemID NUMERIC(18,0)
			,bitSerial BIT
			,bitLot BIT
			,vcSelectedSerialLotNumbers VARCHAR(MAX)
		)

		DECLARE @j INT 
		DECLARE @jCount INT
		DECLARE @numTempWareHouseItmsDTLID NUMERIC(18,0)
		DECLARE @numTempLotQty NUMERIC(18,0)
		DECLARE @vcTemp VARCHAR(MAX)

		DECLARE @TEMPSetialLot AS TABLE
		(
			ID INT
			,vcSerialLot VARCHAR(100)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

		INSERT INTO @TEMP
		(
			numFromItemCode
			,numQty
			,numFromWarehouseItemID
			,numToItemCode
			,numToWarehouseItemID
			,bitSerial
			,bitLot
			,vcSelectedSerialLotNumbers
		)
		SELECT
			ISNULL(numFromItemCode,0),
			ISNULL(numUnitHour,0),
			ISNULL(numFromWarehouseItemID,0),
			ISNULL(numToItemCode,0),
			ISNULL(numToWarehouseItemID,0),
			ISNULL(IsSerial,0),
			ISNULL(IsLot,0),
			ISNULL(vcSelectedSerialLotNumbers,'')
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Item',2)
		WITH
		(
			numFromItemCode NUMERIC(18,0),
			numUnitHour NUMERIC(18,0),
			numFromWarehouseItemID NUMERIC(18,0),
			numToItemCode NUMERIC(18,0),
			numToWarehouseItemID NUMERIC(18,0),
			IsSerial BIT,
			IsLot BIT,
			vcSelectedSerialLotNumbers VARCHAR(MAX)
		)

		DECLARE @i AS INT = 1
		DECLARE @numTempItemCode NUMERIC(18,0)
		DECLARE @numTempToItemCode NUMERIC(18,0)
		DECLARE @numTempQty NUMERIC(18,0)
		DECLARE @numTempFromWarehouseItemID NUMERIC(18,0)
		DECLARE @numTempToWarehouseItemID NUMERIC(18,0)
		DECLARE @bitTempSerial BIT
		DECLARE @bitTempLot BIT
		DECLARE @vcTempSelectedSerialLotNumbers VARCHAR(MAX)
		DECLARE @numFromOnHandBefore FLOAT
		DECLARE @numFromOnAllocationBefore FLOAT
		DECLARE @numFromOnHandAfter FLOAT
		DECLARE @numToOnHandBefore FLOAT
		DECLARE @numToOnHandAfter FLOAT

		DECLARE @iCount AS INT

		SELECT @iCount = COUNT(ID) FROM @TEMP


		IF @iCount = 0
		BEGIN
			RAISERROR('NO_ITEMS_ADDED_FOR_STOCK_TRANSFER',16,1)
		END
		ELSE 
		BEGIN
			WHILE @i <= @iCount
			BEGIN
				SELECT
					@numTempItemCode=numFromItemCode
					,@numTempToItemCode=numToItemCode
					,@numTempQty=numQty
					,@numTempFromWarehouseItemID=numFromWarehouseItemID
					,@numTempToWarehouseItemID=numToWarehouseItemID
					,@bitTempSerial=bitSerial
					,@bitTempLot=bitLot
					,@vcTempSelectedSerialLotNumbers=vcSelectedSerialLotNumbers
				FROM
					@TEMP
				WHERE
					ID=@i
				
				IF @numTempQty > 0
				BEGIN
					IF NOT EXISTS (SELECT numItemCode FROM Item WHERE numDomainID = @numDomainID AND numItemCode=@numTempItemCode)
					BEGIN
						RAISERROR('INVLID_ITEM_ID',16,1)
					END

					IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = @numDomainID AND numWareHouseItemID=@numTempFromWarehouseItemID)
					BEGIN
						RAISERROR('INVLID_FROM_LOCATION',16,1)
					END

					IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID = @numDomainID AND numWareHouseItemID=@numTempToWarehouseItemID)
					BEGIN
						RAISERROR('INVLID_TO_LOCATION',16,1)
					END

					IF (@bitTempSerial = 1 OR @bitTempLot=1) AND LEN(@vcTempSelectedSerialLotNumbers) = 0
					BEGIN
						RAISERROR('SERIAL/LOT#_ARE_NOT_SELECTED',16,1)
					END


					SELECT @numFromOnHandBefore=ISNULL(numOnHand,0),@numFromOnAllocationBefore=ISNULL(numAllocation,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numTempFromWarehouseItemID 

					IF (@numFromOnHandBefore + @numFromOnAllocationBefore) < @numTempQty
					BEGIN
						RAISERROR('INSUFFICIENT_ONHAND_QUANTITY',16,1)
					END
					ELSE
					BEGIN
						IF @bitTempSerial = 1 OR @bitTempLot=1
						BEGIN
							SET @vcTemp = ''
							DELETE FROM @TEMPSetialLot
							INSERT INTO @TEMPSetialLot
							(
								ID,
								vcSerialLot
							)
							SELECT 
								ROW_NUMBER() OVER(ORDER BY OutParam)
								,OutParam
							FROM 
								dbo.SplitString(@vcTempSelectedSerialLotNumbers,',')

							
							SET @j = 1
							SELECT @jCount=COUNT(*) FROM @TEMPSetialLot

							IF @jCount > 0
							BEGIN
								WHILE @j <= @jCount
								BEGIN
									SELECT
										@numTempWareHouseItmsDTLID=SUBSTRING(vcSerialLot,0,CHARINDEX('-',vcSerialLot))
										,@numTempLotQty=SUBSTRING(vcSerialLot,CHARINDEX('-',vcSerialLot)+1,LEN(vcSerialLot) + 1)
									FROM 
										@TEMPSetialLot
									WHERE
										ID=@j
									
									IF ISNULL(@numTempLotQty,0) > 0
									BEGIN
										IF @bitTempLot=1 
										BEGIN
											IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempFromWarehouseItemID AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID AND numQty >= @numTempLotQty)
											BEGIN
												UPDATE 
													WareHouseItmsDTL 
												SET 
													numQty = numQty - @numTempLotQty
													,@vcTemp = CONCAT(@vcTemp,(CASE WHEN LEN(@vcTemp) = 0 THEN '' ELSE ',' END),ISNULL(vcSerialNo,'') ,'(', @numTempLotQty ,')')
												WHERE 
													numWareHouseItemID=@numTempFromWarehouseItemID 
													AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID

												INSERT INTO WareHouseItmsDTL
												(
													numWareHouseItemID,
													vcSerialNo,
													numQty,
													dExpirationDate,
													bitAddedFromPO
												)
												SELECT
													@numTempToWarehouseItemID,
													vcSerialNo,
													@numTempLotQty,
													dExpirationDate,
													bitAddedFromPO
												FROM 
													WareHouseItmsDTL
												WHERE
													numWareHouseItemID=@numTempFromWarehouseItemID AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID
											END
											ELSE
											BEGIN
												RAISERROR('INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY',16,1)
											END
										END
										ELSE
										BEGIN
											IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempFromWarehouseItemID AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID AND numQty = 1)
											BEGIN
												UPDATE 
													WareHouseItmsDTL 
												SET 
													numWareHouseItemID=@numTempToWarehouseItemID
													,@vcTemp = @vcTemp + (CASE WHEN LEN(@vcTemp) = 0 THEN '' ELSE ',' END) + ISNULL(vcSerialNo,'') 
												WHERE 
													numWareHouseItemID=@numTempFromWarehouseItemID 
													AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID
											END
											ELSE
											BEGIN
												RAISERROR('INVALID SERIAL/LOT OR INSUFFICIENT SERIAL/LOT QUANTITY',16,1)
											END
										END  
									END
									ELSE
									BEGIN
										RAISERROR('INVALID_SERIAL/LOT#_QTY',16,1)
									END

									SET @j = @j + 1
								END
							END
							ELSE 
							BEGIN
								RAISERROR('SERIAL/LOT#_ARE_NOT_SELECTED',16,1)
							END
						END

						SET @description = ''

						IF @bitTempSerial = 1 OR @bitTempLot = 1
						BEGIN
							SET @description = CONCAT('Mass Stock Transfer - Transferred (Qty:',@numTempQty,', Serial/Lot#:',')')
						END
						ELSE
						BEGIN
							SET @description = CONCAT('Mass Stock Transfer - Transferred (Qty:',@numTempQty,')')
						END

						UPDATE
							WarehouseItems
						SET
							numOnHand = (CASE WHEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) >= 0 THEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) ELSE 0 END)
							,numAllocation = (CASE WHEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) < 0 THEN (CASE WHEN ISNULL(numAllocation,0) - (ISNULL(@numTempQty,0) - ISNULL(numOnHand,0)) >= 0 THEN ISNULL(numAllocation,0) - (ISNULL(@numTempQty,0) - ISNULL(numOnHand,0)) ELSE 0 END) ELSE ISNULL(numAllocation,0) END)
							,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) < 0 THEN ISNULL(numBackOrder,0) + (CASE WHEN ISNULL(numAllocation,0) - (ISNULL(@numTempQty,0) - ISNULL(numOnHand,0)) >= 0 THEN ISNULL(@numTempQty,0) - ISNULL(numOnHand,0) ELSE ISNULL(numAllocation,0) END) ELSE ISNULL(numBackOrder,0) END)
							,@numFromOnHandAfter = (CASE WHEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) >= 0 THEN ISNULL(numOnHand,0) - ISNULL(@numTempQty,0) ELSE 0 END)
							,dtModified = GETDATE() 
						WHERE
							numWareHouseItemID=@numTempFromWarehouseItemID
						
						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempFromWarehouseItemID,
							@numReferenceID = @numTempItemCode,
							@tintRefType = 1,
							@vcDescription = @description,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID

						IF @bitTempSerial = 1 OR @bitTempLot = 1
						BEGIN
							SET @description = CONCAT('Mass Stock Transfer - Receieved (Qty:',@numTempQty,', Serial/Lot#:',')')
						END
						ELSE
						BEGIN
							SET @description = CONCAT('Mass Stock Transfer - Receieved (Qty:',@numTempQty,')')
						END


						SELECT @numToOnHandBefore = numOnHand FROM WareHouseItems WHERE numWareHouseItemID=@numTempToWarehouseItemID
						UPDATE
							WarehouseItems
						SET
							numOnHand = (CASE WHEN numBackOrder >= @numTempQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numTempQty - ISNULL(numBackOrder,0)) END) 
							,numAllocation = (CASE WHEN numBackOrder >= @numTempQty THEN ISNULL(numAllocation,0) + @numTempQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END) 
							,numBackOrder = (CASE WHEN numBackOrder >= @numTempQty THEN ISNULL(numBackOrder,0) - @numTempQty ELSE 0 END) 
							,dtModified = GETDATE() 
						WHERE
							numWareHouseItemID=@numTempToWarehouseItemID
						SELECT @numToOnHandAfter = numOnHand FROM WareHouseItems WHERE numWareHouseItemID=@numTempToWarehouseItemID

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempToWarehouseItemID,
							@numReferenceID = @numTempToItemCode,
							@tintRefType = 1,
							@vcDescription = @description,
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID


						INSERT INTO MassStockTransfer
						(
							numDomainID
							,numItemCode
							,numFromWarehouseItemID
							,numToItemCode
							,numToWarehouseItemID
							,numQty
							,numTransferredBy
							,dtTransferredDate
							,bitMassTransfer
							,vcSerialLot
							,numFromQtyBefore
							,numToQtyBefore
							,numFromQtyAfter
							,numToQtyAfter
						)
						VALUES
						(
							@numDomainID
							,@numTempItemCode
							,@numTempFromWarehouseItemID
							,@numTempToItemCode
							,@numTempToWarehouseItemID
							,@numTempQty
							,@numUserCntID
							,GETUTCDATE()
							,1
							,@vcTemp
							,@numFromOnHandBefore
							,@numToOnHandBefore
							,@numFromOnHandAfter
							,@numToOnHandAfter
						)
					END
				END
				ELSE 
				BEGIN
					RAISERROR('QTY_TO_TRANSFER_CAN_NOT_BE_0',16,1)
				END

				SET @i = @i + 1
			END
		END
	END
	ELSE
	BEGIN
		RAISERROR('NO_ITEMS_ADDED_FOR_STOCK_TRANSFER',16,1)
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END