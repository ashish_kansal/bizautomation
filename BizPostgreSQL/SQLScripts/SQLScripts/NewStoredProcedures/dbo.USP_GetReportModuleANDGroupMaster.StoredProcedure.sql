GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportModuleANDGroupMaster')
DROP PROCEDURE USP_GetReportModuleANDGroupMaster
GO
CREATE PROCEDURE [dbo].[USP_GetReportModuleANDGroupMaster]                                        
as                 


SELECT [numReportModuleID]
      ,[vcModuleName]
      ,[bitActive]
  FROM [dbo].[ReportModuleMaster] WHERE bitActive=1
	

	
SELECT [numReportModuleGroupID]
      ,[numReportModuleID]
      ,[vcGroupName]
      ,[bitActive]
  FROM [dbo].[ReportModuleGroupMaster] WHERE bitActive=1