
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Priya Sharma
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CreateCloneShippingRule')
DROP PROCEDURE Usp_CreateCloneShippingRule
GO
CREATE PROCEDURE [dbo].[Usp_CreateCloneShippingRule]
(
	 @numRuleId BIGINT OUTPUT
	,@vcRuleName VARCHAR(50)
	,@numDomainID NUMERIC(18,0)
	,@numRelationship NUMERIC(18,0)
	,@numProfile NUMERIC(18,0)
)
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF(SELECT COUNT(*) FROM dbo.ShippingRules WHERE vcRuleName = @vcRuleName AND numDomainId=@numDomainId) > 0
	BEGIN
 		RAISERROR ( 'DUPLICATE RULE',16, 1 )
 		RETURN ;
	END
	ELSE IF(SELECT 
				COUNT(*) 
			FROM 
				dbo.ShippingRules SR
			WHERE 
				numDomainId=@numDomainId 
				AND numRuleID <> ISNULL(@numRuleID,0) 
				AND ISNULL(numRelationship,0) = ISNULL(@numRelationship,0)
				AND ISNULL(numProfile,0) = ISNULL(@numProfile,0)
				AND EXISTS (SELECT 
								SRSL1.numRuleID
							FROM 
								ShippingRuleStateList SRSL1 
							INNER JOIN
								ShippingRuleStateList SRSL2
							ON
								ISNULL(SRSL1.numStateID,0) = ISNULL(SRSL2.numStateID,0)
								AND ISNULL(SRSL1.vcZipPostal,'') = ISNULL(SRSL2.vcZipPostal,'')
								AND ISNULL(SRSL1.numCountryID,0) = ISNULL(SRSL2.numCountryID,0)
							WHERE 
								SRSL1.numDomainID=@numDomainID 
								AND SRSL1.numRuleID = SR.numRuleID
								AND SRSL2.numRuleID = @numRuleID)
			) > 0
	BEGIN
 			RAISERROR ( 'DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP',16, 1 )
 			RETURN ;
	END
	ELSE
	BEGIN
		DECLARE @numNewRuleID AS BIGINT
	
		INSERT INTO ShippingRules
		(vcRuleName,vcDescription,tintBasedOn,tinShippingMethod,tintIncludeType,numSiteID,numDomainID,tintFixedShippingQuotesMode,tintTaxMode,bitFreeShipping,FreeShippingOrderAmt,
		numRelationship,numProfile, numWareHouseID)
		SELECT @vcRuleName,vcDescription,tintBasedOn,tinShippingMethod,tintIncludeType,numSiteID,numDomainID,tintFixedShippingQuotesMode,tintTaxMode,bitFreeShipping,FreeShippingOrderAmt,
		@numRelationship,@numProfile, numWareHouseID
		FROM ShippingRules WHERE numRuleID = @numRuleId AND numDomainID = @numDomainID
	 
		SELECT @numNewRuleID = SCOPE_IDENTITY()
	 
		INSERT INTO dbo.ShippingServiceTypes 
		(numShippingCompanyID,vcServiceName,intNsoftEnum,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled,numRuleID,numDomainID)
		SELECT numShippingCompanyID,vcServiceName,intNsoftEnum,intFrom,intTo,fltMarkup,bitMarkupType,monRate,bitEnabled,@numNewRuleID,numDomainID
		FROM dbo.ShippingServiceTypes WHERE numRuleID = @numRuleId AND numDomainId = @numDomainId
	
		INSERT INTO dbo.ShippingRuleStateList( numRuleID,numCountryID,numStateID,numDomainID,tintMode,vcFromZip,vcToZip,vcZipPostal )
		SELECT @numNewRuleID, numCountryID,numStateID,numDomainID,tintMode,vcFromZip,vcToZip,vcZipPostal FROM ShippingRuleStateList WHERE numRuleID = @numRuleId

		SET @numRuleId = @numNewRuleID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END