
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_FulFill')
DROP PROCEDURE USP_OpportunityBizDocs_FulFill
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_FulFill] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@dtShippedDate DATETIME
AS  
BEGIN 
BEGIN TRY
	DECLARE @vcItemType AS CHAR(1)
	DECLARE @bitDropShip AS BIT
	DECLARE @numOppItemID AS INT
	DECLARE @numWarehouseItemID AS INT
	DECLARE @numQty AS FLOAT
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @numXmlQty AS FLOAT
	DECLARE @vcSerialLot AS VARCHAR(MAX)
	DECLARE @numSerialCount AS INT
	DECLARE @bitSerial AS BIT
	DECLARE @bitLot AS BIT
	DECLARE @bitAssembly AS BIT
	DECLARE @bitKit AS BIT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT  
	DECLARE @numNewQtyShipped AS FLOAT


	/* VERIFY BIZDOC IS EXISTS OR NOT */
	IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID)
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_IS_DELETED',16,1)
	END

	/* VERIFY WHETHER BIZDOC IS ALREADY FULLFILLED BY SOMEBODY ELSE */
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID AND bitFulfilled = 1) 
	BEGIN
		RAISERROR('FULFILLMENT_BIZDOC_ALREADY_FULFILLED',16,1)
	END

	--IF EXISTS(SELECT numWOId FROm WorkOrder WHERE numOppId=@numOppId AND numWOStatus <> 23184) --WORK ORDER NOT YET COMPLETED
	--BEGIN
	--	RAISERROR('WORK_ORDER_NOT_COMPLETED',16,1)
	--END

	BEGIN TRANSACTION

		BEGIN /* REVERT SEIRAL/LOT# NUMBER ASSIGNED FROM PRODCUT&SERVICES TAB */
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numOppID=@numOppId
				AND OWSI.numOppBizDocsId IS NULL
		END

		BEGIN /* GET FULFILLMENT BIZDOC ITEMS */
			DECLARE @TempFinalTable TABLE
			(
				ID INT IDENTITY(1,1),
				numItemCode NUMERIC(18,0),
				vcItemType CHAR(1),
				bitSerial BIT,
				bitLot BIT,
				bitAssembly BIT,
				bitKit BIT,
				numOppItemID NUMERIC(18,0),
				numWarehouseItemID NUMERIC(18,0),
				numQtyShipped FLOAT,
				numQty FLOAT,
				bitDropShip BIT,
				vcSerialLot VARCHAR(MAX)
			)

			INSERT INTO @TempFinalTable
			(
				numItemCode,
				vcItemType,
				bitSerial,
				bitLot,
				bitAssembly,
				bitKit,
				numOppItemID,
				numWarehouseItemID,
				numQtyShipped,
				numQty,
				bitDropShip,
				vcSerialLot
			)
			SELECT
				OpportunityBizDocItems.numItemCode,
				ISNULL(Item.charItemType,''),
				ISNULL(Item.bitSerialized,0),
				ISNULL(Item.bitLotNo,0),
				ISNULL(Item.bitAssembly,0),
				ISNULL(Item.bitKitParent,0),
				ISNULL(OpportunityItems.numoppitemtcode,0),
				ISNULL(OpportunityBizDocItems.numWarehouseItmsID,0),
				ISNULL(OpportunityItems.numQtyShipped,0),
				ISNULL(OpportunityBizDocItems.numUnitHour,0),
				ISNULL(OpportunityItems.bitDropShip,0),
				''
			FROM
				OpportunityBizDocs
			INNER JOIN
				OpportunityBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityItems
			ON
				OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				Item
			ON
				OpportunityBizDocItems.numItemCode = Item.numItemCode
			WHERE
				numOppBizDocsId = @numOppBizDocID
				AND ISNULL(OpportunityItems.bitDropShip,0) = 0
				AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0
		END

		BEGIN /* CONVERT vcItems XML to TABLE */
			DECLARE @TempItemXML TABLE
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			)

			DECLARE @idoc int
			EXEC sp_xml_preparedocument @idoc OUTPUT, @vcItems;

			INSERT INTO
				@TempItemXML
			SELECT
				numOppItemID,
				numQty,
				numWarehouseItemID,
				vcSerialLot
			FROM 
				OPENXML (@idoc, '/Items/Item',2)
			WITH 
			(
				numOppItemID NUMERIC(18,0),
				numQty FLOAT,
				numWarehouseItemID NUMERIC(18,0),
				vcSerialLot VARCHAR(MAX)
			);
		END

		BEGIN /* VALIDATE ALL THE DETAILS BEFORE FULFILLING ITEMS */
		
			SELECT @COUNT=COUNT(*) FROM @TempFinalTable

			DECLARE @TempLot TABLE
			(
				vcLotNo VARCHAR(100),
				numQty FLOAT 
			)

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@numOppItemID=numOppItemID,
					@numWarehouseItemID=numWarehouseItemID,
					@numQty=numQty,
					@bitSerial=bitSerial,
					@bitLot=bitLot 
				FROM 
					@TempFinalTable 
				WHERE 
					ID = @i

				--GET QUENTITY RECEIVED FROM XML PARAMETER
				SELECT 
					@numXmlQty = numQty, 
					@vcSerialLot=vcSerialLot, 
					@numSerialCount = (CASE WHEN (@bitSerial = 1 OR @bitLot = 1) THEN (SELECT COUNT(*) FROM dbo.SplitString(vcSerialLot,',')) ELSE 0 END)  
				FROM 
					@TempItemXML 
				WHERE 
					numOppItemID = @numOppItemID 

				--CHECK IF QUANTITY OF ITEM IN OpportunityBizDocItems TBALE IS SAME AS QUANTITY SUPPLIED FROM XML
				IF @numQty <> @numXmlQty
				BEGIN
					RAISERROR('QTY_MISMATCH',16,1)
				END

				--IF ITEM IS SERIAL ITEM CHEKC THAT VALID AND REQUIRED NUMBER OF SERIALS ARE PROVIDED BY USER
				IF @bitSerial = 1 
				BEGIN
					IF @numSerialCount <> @numQty
					BEGIN
						RAISERROR('REQUIRED_SERIALS_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND numQty > 0 AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))) <> @numSerialCount
						BEGIN
							RAISERROR('INVALID_SERIAL_NUMBERS',16,1)
						END
					END
				END

				IF @bitLot = 1
				BEGIN
					-- DELETE DATA OF PREVIOUS ITERATION FROM TABLE
					DELETE FROM @TempLot

					--CONVERTS COMMA SEPERATED STRING TO LOTNo AND QTY 
					INSERT INTO
						@TempLot
					SELECT
						SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
						CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
					FROM
						dbo.SplitString(@vcSerialLot,',')

					--CHECKS WHETHER PROVIDED QTY OF LOT NO IS SAME IS REQUIRED QTY
					IF (SELECT SUM(numQty) FROM @TempLot) <> @numQty
					BEGIN
						RAISERROR('REQUIRED_LOTNO_NOT_PROVIDED',16,1)
					END
					ELSE
					BEGIN
						-- CHECKS WHETHER LOT NO EXISTS IN WareHouseItmsDTL
						IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numWarehouseItemID AND vcSerialNo IN (SELECT vcLotNo FROM @TempLot)) <> (SELECT COUNT(*) FROM @TempLot)
						BEGIN
							RAISERROR('INVALID_LOT_NUMBERS',16,1)
						END
						ELSE
						BEGIN
							-- CHECKS WHETHER LOT NO HAVE ENOUGH QTY TO FULFILL ITEM QTY
							IF (SELECT 
									COUNT(*) 
								FROM 
									WareHouseItmsDTL 
								INNER JOIN
									@TempLot TEMPLOT
								ON
									WareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
								WHERE 
									numWareHouseItemID = @numWarehouseItemID
									AND TEMPLOT.numQty > WareHouseItmsDTL.numQty) > 0
							BEGIN
								RAISERROR('SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY',16,1)
							END
						END
					END
				END

		
				UPDATE @TempFinalTable SET vcSerialLot = @vcSerialLot WHERE ID=@i

				SET @i = @i + 1
			END
		END
	
		SET @i = 1
		SELECT @COUNT = COUNT(*) FROM @TempFinalTable
		DECLARE @vcDescription AS VARCHAR(300)
		DECLARE @numAllocation AS FLOAT

		DECLARE @j INT = 0
		DECLARE @ChildCount AS INT
		DECLARE @vcChildDescription AS VARCHAR(300)
		DECLARE @numChildAllocation AS FLOAT
		DECLARE @numOppChildItemID AS INT
		DECLARE @numChildItemCode AS INT
		DECLARE @bitChildIsKit AS BIT
		DECLARE @numChildWarehouseItemID AS INT
		DECLARE @numChildQty AS FLOAT
		DECLARE @numChildQtyShipped AS FLOAT

		DECLARE @TempKitSubItems TABLE
		(
			ID INT,
			numOppChildItemID NUMERIC(18,0),
			numChildItemCode NUMERIC(18,0),
			bitChildIsKit BIT,
			numChildWarehouseItemID NUMERIC(18,0),
			numChildQty FLOAT,
			numChildQtyShipped FLOAT
		)


		DECLARE @k AS INT = 1
		DECLARE @CountKitChildItems AS INT
		DECLARE @vcKitChildDescription AS VARCHAR(300)
		DECLARE @numKitChildAllocation AS FLOAT
		DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
		DECLARE @numKitChildItemCode AS NUMERIC(18,0)
		DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
		DECLARE @numKitChildQty AS FLOAT
		DECLARE @numKitChildQtyShipped AS FLOAT

		DECLARE @TempKitChildKitSubItems TABLE
		(
			ID INT,
			numOppKitChildItemID NUMERIC(18,0),
			numKitChildItemCode NUMERIC(18,0),
			numKitChildWarehouseItemID NUMERIC(18,0),
			numKitChildQty FLOAT,
			numKitChildQtyShipped FLOAT
		)

		WHILE @i <= @COUNT
		BEGIN
			SELECT 
				@vcItemType=vcItemType,
				@bitSerial=bitSerial,
				@bitLot=bitLot,
				@bitAssembly=bitAssembly,
				@bitKit=bitKit,
				@bitDropShip=bitDropShip,
				@numOppItemID=numOppItemID,
				@numQty=numQty,
				@numQtyShipped = numQtyShipped,
				@numWarehouseItemID=numWarehouseItemID,
				@vcSerialLot = vcSerialLot
			FROM 
				@TempFinalTable 
			WHERE 
				ID = @i

			--MAKE INVENTIRY CHANGES IF INVENTORY ITEM
			IF (UPPER(@vcItemType) = 'P' AND @bitDropShip = 0)
			BEGIN
				SET @vcDescription=CONCAT('SO Qty Shipped (Qty:',@numQty,' Shipped:',@numQtyShipped,')')
						 
				IF @bitKit = 1
				BEGIN
					-- CLEAR DATA OF PREVIOUS ITERATION
					DELETE FROM @TempKitSubItems

					-- GET KIT SUB ITEMS DETAIL
					INSERT INTO @TempKitSubItems
					(
						ID,
						numOppChildItemID,
						numChildItemCode,
						bitChildIsKit,
						numChildWarehouseItemID,
						numChildQty,
						numChildQtyShipped
					)
					SELECT 
						ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
						ISNULL(numOppChildItemID,0),
						ISNULL(I.numItemCode,0),
						ISNULL(I.bitKitParent,0),
						ISNULL(numWareHouseItemId,0),
						((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numQty),
						ISNULL(numQtyShipped,0)
					FROM 
						OpportunityKitItems OKI 
					JOIN 
						Item I 
					ON 
						OKI.numChildItemID=I.numItemCode    
					WHERE 
						charitemtype='P' 
						AND ISNULL(numWareHouseItemId,0) > 0 
						AND OKI.numOppID=@numOppID 
						AND OKI.numOppItemID=@numOppItemID 

					SET @j = 1
					SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

					--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
					WHILE @j <= @ChildCount
					BEGIN
						SELECT
							@numOppChildItemID=numOppChildItemID,
							@numChildItemCode=numChildItemCode,
							@bitChildIsKit=bitChildIsKit,
							@numChildWarehouseItemID=numChildWarehouseItemID,
							@numChildQty=numChildQty,
							@numChildQtyShipped=numChildQtyShipped
						FROM
							@TempKitSubItems
						WHERE 
							ID = @j

						SELECT @numChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

						-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
						IF @bitChildIsKit = 1
						BEGIN
							-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
							IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
							BEGIN
								-- CLEAR DATA OF PREVIOUS ITERATION
								DELETE FROM @TempKitChildKitSubItems

								-- GET SUB KIT SUB ITEMS DETAIL
								INSERT INTO @TempKitChildKitSubItems
								(
									ID,
									numOppKitChildItemID,
									numKitChildItemCode,
									numKitChildWarehouseItemID,
									numKitChildQty,
									numKitChildQtyShipped
								)
								SELECT 
									ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
									ISNULL(numOppKitChildItemID,0),
									ISNULL(OKCI.numItemID,0),
									ISNULL(OKCI.numWareHouseItemId,0),
									((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
									ISNULL(numQtyShipped,0)
								FROM 
									OpportunityKitChildItems OKCI 
								JOIN 
									Item I 
								ON 
									OKCI.numItemID=I.numItemCode    
								WHERE 
									charitemtype='P' 
									AND ISNULL(numWareHouseItemId,0) > 0 
									AND OKCI.numOppID=@numOppID 
									AND OKCI.numOppItemID=@numOppItemID 
									AND OKCI.numOppChildItemID = @numOppChildItemID

								SET @k = 1
								SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

								WHILE @k <= @CountKitChildItems
								BEGIN
									SELECT
										@numOppKitChildItemID=numOppKitChildItemID,
										@numKitChildItemCode=numKitChildItemCode,
										@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
										@numKitChildQty=numKitChildQty,
										@numKitChildQtyShipped=numKitChildQtyShipped
									FROM
										@TempKitChildKitSubItems
									WHERE 
										ID = @k

									SELECT @numKitChildAllocation=ISNULL(numAllocation,0) FROM WareHouseItems WHERE numWareHouseItemID=@numKitChildWarehouseItemID

									--REMOVE QTY FROM ALLOCATION
									SET @numKitChildAllocation = @numKitChildAllocation - @numKitChildQty

									IF @numKitChildAllocation >= 0
									BEGIN
										UPDATE  
											WareHouseItems
										SET     
											numAllocation = @numKitChildAllocation,
											dtModified = GETDATE() 
										WHERE   
											numWareHouseItemID = @numKitChildWarehouseItemID      	
							
										UPDATE  
											OpportunityKitChildItems
										SET 
											numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numKitChildQty,0)
										WHERE   
											numOppKitChildItemID = @numOppKitChildItemID
											AND numOppChildItemID = @numOppChildItemID 
											AND numOppId=@numOppId
											AND numOppItemID=@numOppItemID

										SET @vcKitChildDescription = CONCAT('SO CHILD KIT Qty Shipped (Qty:',@numKitChildQty,' Shipped:',@numKitChildQtyShipped,')')

										EXEC dbo.USP_ManageWareHouseItems_Tracking
											@numWareHouseItemID = @numKitChildWarehouseItemID,
											@numReferenceID = @numOppId,
											@tintRefType = 3,
											@vcDescription = @vcKitChildDescription,
											@numModifiedBy = @numUserCntID,
											@numDomainID = @numDomainID	 
									END
									ELSE
									BEGIN
										RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
									END

									SET @k = @k + 1
								END
							END

							UPDATE  
								OpportunityKitItems
							SET 
								numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
							WHERE   
								numOppChildItemID = @numOppChildItemID 
								AND numOppId=@numOppId
								AND numOppItemID=@numOppItemID

							SET @vcChildDescription = CONCAT('SO KIT Qty Shipped (Qty:',@numChildQty,' Shipped:',@numChildQtyShipped,')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 
						END
						ELSE 
						BEGIN
							--REMOVE QTY FROM ALLOCATION
							SET @numChildAllocation = @numChildAllocation - @numChildQty
							IF @numChildAllocation >= 0
							BEGIN
								UPDATE  
									WareHouseItems
								SET     
									numAllocation = @numChildAllocation,
									dtModified = GETDATE() 
								WHERE   
									numWareHouseItemID = @numChildWarehouseItemID      	
							
								UPDATE  
									OpportunityKitItems
								SET 
									numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numChildQty,0)
								WHERE   
									numOppChildItemID = @numOppChildItemID 
									AND numOppId=@numOppId
									AND numOppItemID=@numOppItemID

								SET @vcChildDescription = CONCAT('SO KIT Qty Shipped (Qty:',@numChildQty,' Shipped:',@numChildQtyShipped,')')

								EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numChildWarehouseItemID,
									@numReferenceID = @numOppId,
									@tintRefType = 3,
									@vcDescription = @vcChildDescription,
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomainID	 
							END
							ELSE
							BEGIN
								RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
							END
						END

						SET @j = @j + 1
					END
				END
				ELSE
				BEGIN
					SELECT @numAllocation = ISNULL(numAllocation, 0) FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID
				       
					--REMOVE QTY FROM ALLOCATION
					SET @numAllocation = @numAllocation - @numQty	
				       
					IF (@numAllocation >= 0 )
					BEGIN
						UPDATE  
							WareHouseItems
						SET     
							numAllocation = @numAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID      	

						IF @bitSerial = 1
						BEGIN								
							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=OutParam AND  numQty = 1),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								1,
								@numOppBizDocID
							FROM
								dbo.SplitString(@vcSerialLot,',')


							UPDATE WareHouseItmsDTL SET numQty=0 WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo IN (SELECT OutParam FROM dbo.SplitString(@vcSerialLot,','))
						END

						IF @bitLot = 1
						BEGIN
							DELETE FROM @TempLot

							INSERT INTO
								@TempLot
							SELECT
								SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam)),
								CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS INT)
							FROM
								dbo.SplitString(@vcSerialLot,',')

							DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID=@numWarehouseItemID

							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID,
								numOppID,
								numOppItemID,
								numWarehouseItmsID,
								numQty,
								numOppBizDocsId	
							)
							SELECT
								(SELECT TOP 1 numWarehouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=TEMPLOT.vcLotNo AND numQty >= TEMPLOT.numQty),
								@numOppID,
								@numOppItemID,
								@numWarehouseItemID,
								TEMPLOT.numQty,
								@numOppBizDocID
							FROM
								@TempLot TEMPLOT


							UPDATE	TEMPWareHouseItmsDTL
								SET TEMPWareHouseItmsDTL.numQty = ISNULL(TEMPWareHouseItmsDTL.numQty,0) - ISNULL(TEMPLOT.numQty,0)
							FROM
								WareHouseItmsDTL TEMPWareHouseItmsDTL
							INNER JOIN
								@TempLot TEMPLOT
							ON
								TEMPWareHouseItmsDTL.vcSerialNo = TEMPLOT.vcLotNo
							WHERE
								TEMPWareHouseItmsDTL.numWareHouseItemID = @numWarehouseItemID
						END
					END
					ELSE
					BEGIN
						PRINT 'PARENT: ' + CAST(@numChildWarehouseItemID AS VARCHAR)
						RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
					END
				END

				EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @vcDescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomainID	 
			END

			UPDATE  
				OpportunityItems
			SET 
				numQtyShipped = ISNULL(numQtyShipped,0) + ISNULL(@numQty,0)
			WHERE   
				numoppitemtCode = @numOppItemID 
				AND numOppId=@numOppId

			SET @i = @i + 1
		END

		UPDATE OpportunityBizDocs SET bitFulfilled = 1,dtShippedDate=@dtShippedDate WHERE numOppBizDocsId=@numOppBizDocID
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

