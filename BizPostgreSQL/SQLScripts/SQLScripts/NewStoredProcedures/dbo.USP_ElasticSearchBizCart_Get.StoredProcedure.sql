GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_ElasticSearchBizCart_Get')
DROP PROCEDURE USP_ElasticSearchBizCart_Get
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearchBizCart_Get]

AS 
BEGIN
	--DELETE ESB
	--FROM 
	--	ElasticSearchBizCart ESB
	--LEFT JOIN Item I ON ESB.vcValue=I.numItemCOde AND ISNULL(I.IsArchieve,0)=0
	--INNER JOIN ItemCategory IC ON (IC.numItemID = I.NumItemCode)
	--INNER JOIN Category C ON (C.numCategoryID = IC.numCategoryID)
	--INNER JOIN CategoryProfile CP ON (CP.ID = C.numCategoryProfileID)
	--INNER JOIN CategoryProfileSites CPS ON (CPS.numCategoryProfileID = CP.ID)
	--INNER JOIN Sites S ON (S.numSiteID = CPS.numSiteID)
	--INNER JOIN eCommerceDTL ECD ON (ECD.numSiteId = S.numSiteId AND ECD.bitElasticSearch = 1)
	--WHERE 
	--	vcAction != 'Delete' AND vcModule = 'Item' AND I.numItemCode IS NULL

	DELETE FROM ElasticSearchBizCart 
	where numDomainID IN (
		select DISTINCT EC.numDomainId
		from eCommerceDTL EC
		LEFT JOIN eCommerceDTL OtherSite ON 
			(EC.numDomainId = OtherSite.numDomainId AND 
			(OtherSite.dtElasticSearchDisableDatetime Is NOT null OR OtherSite.bitElasticSearch = 1))
		WHERE OtherSite.numDomainId IS NULL
		AND EC.bitElasticSearch = 0)

	DECLARE @TEMP TABLE
	(
		ID NUMERIC(18,0)
		,numDomainID NUMERIC(18,0)
		,vcModule VARCHAR(15)
		,vcValue NVARCHAR(200)
		,vcAction VARCHAR(10)
		--,numSiteID NUMERIC(18,0)
	)
	INSERT INTO @TEMP (ID,numDomainID,vcModule,vcValue,vcAction)--,numSiteID) 
	SELECT TOP 400 numElasticSearchBizCartID,numDomainID,vcModule,vcValue,vcAction--,numSiteID 
	FROM ElasticSearchBizCart ORDER BY numElasticSearchBizCartID
	DELETE FROM ElasticSearchBizCart WHERE numElasticSearchBizCartID IN (SELECT ID FROM @TEMP)

	SELECT * FROM (
		SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,vcValue,vcAction--,T1.numSiteId 
		FROM @TEMP T1 
		--INNER JOIN eCommerceDTL ECD ON T1.numDomainID=ECD.numDomainId 
		WHERE vcAction='Insert' --AND ISNULL(ECD.bitElasticSearch,0)=1 AND T1.numDomainID > 0 
		GROUP BY T1.numDomainID,vcModule,vcValue,vcAction--,T1.numSiteId
		) TEMP 
	ORDER BY ID
	
	
	SELECT * FROM (
		SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,vcValue,vcAction --,T1.numSiteId
		FROM @TEMP T1 
		--INNER JOIN eCommerceDTL ECD ON T1.numDomainID=ECD.numDomainId 
		WHERE vcAction='Update' --AND ISNULL(ECD.bitElasticSearch,0)=1 AND T1.numDomainID > 0 
		GROUP BY T1.numDomainID,vcModule,vcValue,vcAction--,T1.numSiteId
		) TEMP 
	ORDER BY ID
	
	SELECT * FROM (
		SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,vcValue,vcAction --,T1.numSiteId
		FROM @TEMP T1 
		--INNER JOIN eCommerceDTL ECD ON T1.numDomainID=ECD.numDomainId 
		WHERE vcAction='Delete' --AND ISNULL(ECD.bitElasticSearch,0)=1 AND T1.numDomainID > 0 
		GROUP BY T1.numDomainID,vcModule,vcValue,vcAction--,T1.numSiteId
		) TEMP 
	ORDER BY ID
END