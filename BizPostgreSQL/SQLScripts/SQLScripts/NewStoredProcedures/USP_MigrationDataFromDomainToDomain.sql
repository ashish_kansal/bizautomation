GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MigrationDataFromDomainToDomain')
DROP PROCEDURE USP_MigrationDataFromDomainToDomain
GO
CREATE PROCEDURE [dbo].[USP_MigrationDataFromDomainToDomain]    
@sourceDomainId AS NUMERIC(18,0)=0, -- 153 Kirk & Matz
@destinationDomainId AS NUMERIC(18,0)=0, --172 Boneta, Inc
@sourceGroupId AS NUMERIC(18,0)=0,--710
@destinationGroupId AS NUMERIC(18,0)=0,--858,
@sourceSiteId AS NUMERIC(18,0)=0,
@destinationSiteId AS NUMERIC(18,0)=0,
@taskToPerform AS VARCHAR(100)=NULL--'1,2,3'
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION;  

-----------------------------------------------------------Dropdown List Sync----------------------------------------------------------------------
IF(1 IN(SELECT Items FROM dbo.Split(@taskToPerform,',')))
BEGIN
PRINT 'Start Dropdown sync'
CREATE TABLE #tempSrcListMaster(
	numExistListID NUMERIC(18,0),
	vcListName VARCHAR(MAX), 
	bitDeleted Bit, 
	bitFixed Bit, 
	bitFlag Bit, 
	vcDataType VARCHAR(MAX), 
	numModuleID NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)

INSERT INTO #tempSrcListMaster
SELECT numListID,vcListName,bitDeleted,bitFixed,bitFlag,vcDataType,numModuleID FROM listmaster WHERE numDomainID=@sourceDomainId OR bitFlag=1

DECLARE @totalListRecords AS INT=0
SET @totalListRecords =(SELECT COUNT(*) FROM #tempSrcListMaster)
DECLARE @i AS INT =1
DECLARE @vcListName AS  VARCHAR(MAX)
DECLARE @bitDeleted AS BIT
DECLARE @bitFixed AS BIT
DECLARE @bitFlag AS BIT
DECLARE @vcDataType AS VARCHAR(MAX)
DECLARE @numModuleID AS NUMERIC(18,0)
DECLARE @numListID AS NUMERIC(18,0)
DECLARE @numExistListID AS NUMERIC(18,0)

WHILE(@i<=@totalListRecords)
BEGIN
	SELECT @numExistListID=numExistListID,@vcListName=vcListName,@bitDeleted=bitDeleted,@bitFixed=bitFixed,@bitFlag=bitFlag,@vcDataType=vcDataType,@numModuleID=numModuleID FROM #tempSrcListMaster WHERE ID=@i
	IF(@bitFlag=1)
	BEGIN
		SET @numListID =@numExistListID
	END
	ELSE IF NOT EXISTS(SELECT * FROM ListMaster WHERE vcListName=@vcListName AND numDomainID=@destinationDomainId AND numModuleID=@numModuleID)
	BEGIN
		INSERT INTO ListMaster(vcListName, numCreatedBy, bintCreatedDate, numModifiedBy, bintModifiedDate, bitDeleted, bitFixed, numDomainID, bitFlag, vcDataType, numModuleID)
		VALUES(@vcListName,1,GETDATE(),1,GETDATE(),@bitDeleted,@bitFixed,@destinationDomainId,@bitFlag,@vcDataType,@numModuleID)
		SET @numListID = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		SET @numListID =(SELECT TOP 1 numListID FROM ListMaster WHERE vcListName=@vcListName AND numDomainID=@destinationDomainId AND numModuleID=@numModuleID)
	END

	INSERT INTO ListDetails(numListID, vcData, bitDelete, numDomainID, constFlag, sintOrder, numListType, tintOppOrOrder,numListItemGroupId)
	SELECT @numListID,vcData, bitDelete, @destinationDomainId, constFlag, sintOrder, numListType, tintOppOrOrder,
	CASE WHEN ISNULL(numListItemGroupId,0)>0 THEN 
	(SELECT TOP 1 numListItemID FROM ListDetails WHERE numDomainID=@destinationDomainId AND numListID=@numListID AND 
	vcData=(SELECT TOP 1 vcData FROM ListDetails As L where L.numListItemID=numListItemGroupId AND numDomainID=@sourceDomainId)) ELSE 0 END 
	FROM ListDetails WHERE 
	numDomainID=@sourceDomainId AND  numListID=@numExistListID AND 
	vcData NOT IN(SELECT vcData FROM ListDetails WHERE numDomainID=@destinationDomainId AND numListID=@numListID)


	INSERT INTO ListOrder(numListId,numListItemId,numDomainId,intSortOrder)
	SELECT @numListID,(SELECT TOP 1 numListItemId FROM ListDetails WHERE numListId=@numListID AND vcData=LD.vcData),@destinationDomainId,intSortOrder FROM 
	ListOrder AS LO LEFT JOIN ListDetails AS LD ON Lo.numListItemId=LD.numListItemID where LO.numListId=@numExistListID AND LO.numDomainId=@sourceDomainId
	SET @i = @i+1
END
DROP TABLE #tempSrcListMaster
END
-----------------------------------------------------------Biz Form Wizard Configuration Sync----------------------------------------------------------------------
IF(2 IN(SELECT Items FROM dbo.Split(@taskToPerform,',')))
BEGIN
CREATE TABLE #tempFormFieldGroupConfigurarionMaster(
	numOldFormFieldGroupId NUMERIC(18,0),
	numFormFieldGroupId NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)
INSERT INTO #tempFormFieldGroupConfigurarionMaster(numOldFormFieldGroupId,numFormFieldGroupId)
SELECT numFormFieldGroupId,0 FROM FormFieldGroupConfigurarion WHERE numDomainId=@sourceDomainId 

SET @i=1;
SELECT @totalListRecords = COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster
DECLARE @numFormFieldGroupId AS NUMERIC(18,0)=0
DECLARE @numOldFormFieldGroupId AS NUMERIC(18,0)=0

WHILE(@i<=@totalListRecords)
BEGIN
	SELECT @numOldFormFieldGroupId = numOldFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE ID=@i
	IF EXISTS(SELECT numFormFieldGroupId FROM FormFieldGroupConfigurarion WHERE vcGroupName=(SELECT TOP 1 vcGroupName FROM FormFieldGroupConfigurarion WHERE numFormFieldGroupId=@numOldFormFieldGroupId) AND numDomainId=@destinationDomainId)
	BEGIN
		SELECT @numFormFieldGroupId = numFormFieldGroupId FROM FormFieldGroupConfigurarion WHERE vcGroupName=(SELECT TOP 1 vcGroupName FROM FormFieldGroupConfigurarion WHERE numFormFieldGroupId=@numOldFormFieldGroupId) AND numDomainId=@destinationDomainId
	END
	ELSE
	BEGIN
		INSERT INTO FormFieldGroupConfigurarion(numFormId, numGroupId, vcGroupName, numDomainId, numOrder)
		SELECT F.numFormId,F.numGroupId,F.vcGroupName,@destinationDomainId,F.numOrder FROM #tempFormFieldGroupConfigurarionMaster AS T LEFT JOIN FormFieldGroupConfigurarion AS F ON T.numOldFormFieldGroupId=F.numFormFieldGroupId WHERE F.numDomainId=@sourceDomainId AND ID=@i 
		SET @numFormFieldGroupId=SCOPE_IDENTITY();
	END
	UPDATE #tempFormFieldGroupConfigurarionMaster SET numFormFieldGroupId=@numFormFieldGroupId WHERE ID=@i
	SET @i=@i+1;
END
--ADD Custom Text Field to DycFieldMaster
CREATE TABLE #tempDycFieldMaster(
	numModuleId NUMERIC(18,0),
	numOldFieldId NUMERIC(18,0),
	numFieldId NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)

INSERT INTO #tempDycFieldMaster(numOldFieldId,numFieldId,numModuleId)
SELECT  numFieldId,0,numModuleId FROM DycFieldMaster where numDomainID>0 AND numDomainID=@sourceDomainId
SET @i=1;
DECLARE @numFieldId AS NUMERIC(18,0)=0
DECLARE @numOldFieldId AS NUMERIC(18,0)=0
DECLARE @numOldModuleId AS NUMERIC(18,0)=0
SELECT @totalListRecords = COUNT(*) FROM #tempDycFieldMaster
WHILE(@i<=@totalListRecords)
BEGIN
	SELECT @numOldFieldId = numOldFieldId,@numOldModuleId=numModuleId FROM #tempDycFieldMaster WHERE ID=@i
	IF EXISTS(SELECT numFieldId FROM DycFieldMaster WHERE vcFieldName=(SELECT TOP 1 vcFieldName FROM DycFieldMaster WHERE numFieldId=@numOldFieldId AND numModuleID=@numOldModuleId) AND numDomainId=@destinationDomainId AND numModuleID=@numOldModuleId)
	BEGIN
		SELECT @numFieldId = numFieldId FROM DycFieldMaster WHERE vcFieldName=(SELECT TOP 1 vcFieldName FROM DycFieldMaster WHERE numFieldId=@numOldFieldId AND numModuleID=@numOldModuleId) AND numDomainId=@destinationDomainId  AND numModuleID=@numOldModuleId
	END
	ELSE
	BEGIN
		INSERT INTO DycFieldMaster(numModuleID, numDomainID, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, intColumnWidth, intFieldMaxLength, intWFCompare, vcGroup, vcWFCompareField)
		SELECT D.numModuleID, @destinationDomainId, vcFieldName, vcDbColumnName, vcOrigDbColumnName, vcPropertyName, vcLookBackTableName, vcFieldDataType, vcFieldType, vcAssociatedControlType, vcToolTip, vcListItemType, numListID, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitAllowEdit, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitInlineEdit, bitRequired, intColumnWidth, intFieldMaxLength, intWFCompare, vcGroup, vcWFCompareField
		FROM #tempDycFieldMaster AS T LEFT JOIN DycFieldMaster AS D ON T.numOldFieldId=D.numFieldId WHERE D.numDomainId=@sourceDomainId AND D.numModuleID=@numOldModuleId AND ID=@i 
		SET @numFieldId=SCOPE_IDENTITY();
	END
	UPDATE #tempDycFieldMaster SET numFieldId=@numFieldId WHERE ID=@i
	SET @i=@i+1;
END

DELETE FROM DycFormField_Mapping WHERE  numDomainID>0 AND numDomainID=@destinationDomainId

INSERT INTO DycFormField_Mapping(numModuleID, numFieldID, numDomainID, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitRequired, numFormFieldID, intSectionID, bitAllowGridColor)
SELECT numModuleID, CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE 0 END, @destinationDomainId, numFormID, bitAllowEdit, bitInlineEdit, vcFieldName, vcAssociatedControlType, vcPropertyName, PopupFunctionName, [order], tintRow, tintColumn, bitInResults, bitDeleted, bitDefault, bitSettingField, bitAddField, bitDetailField, bitAllowSorting, bitWorkFlowField, bitImport, bitExport, bitAllowFiltering, bitRequired, 
CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldID) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldID) ELSE 0 END
, intSectionID, bitAllowGridColor
FROM DycFormField_Mapping AS D WHERE numDomainID=@sourceDomainId AND numDomainID>0 AND 
(SELECT COUNT(*) FROM DycFormField_Mapping As DM WHERE DM.numModuleID=D.numModuleID AND numDomainID>0 AND numDomainID=@destinationDomainId AND DM.numFormID=D.numFormID AND DM.numFieldID=CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END)=0
AND (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldID) > 0


DELETE FROM DynamicFormField_Validation WHERE  numDomainID>0 AND  numDomainId=@destinationDomainId
INSERT INTO DynamicFormField_Validation(numFormId, numFormFieldId, numDomainId, vcNewFormFieldName, bitIsRequired, bitIsNumeric, bitIsAlphaNumeric, bitIsEmail, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, vcToolTip)
SELECT numFormId, CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFormFieldId) ELSE D.numFormFieldId END, @destinationDomainId, vcNewFormFieldName, bitIsRequired, bitIsNumeric, bitIsAlphaNumeric, bitIsEmail, bitIsLengthValidation, intMaxLength, intMinLength, bitFieldMessage, vcFieldMessage, vcToolTip FROM DynamicFormField_Validation AS D WHERE numDomainID=@sourceDomainId

--Dynamic Configuration For User
DELETE FROM BizFormWizardMasterConfiguration  WHERE  numDomainId=@destinationDomainId AND numGroupID=@destinationGroupId  AND bitCustom=0

INSERT INTO BizFormWizardMasterConfiguration
		(
			numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			numDomainID,
			numGroupID,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			numFormFieldGroupId
		)
SELECT  numFormID,
			CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END,
			intColumnNum,
			intRowNum,
			@destinationDomainId,
			@destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			CASE WHEN (SELECT COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) > 0 THEN (SELECT TOP 1  numFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) ELSE D.numFormFieldGroupId END
			FROM BizFormWizardMasterConfiguration AS D WHERE numDomainID=@sourceDomainId AND numGroupID=@sourceGroupId  AND bitCustom=0



CREATE TABLE #tempDycUserMaster(
	numUserCntID NUMERIC(18,0),
	ID NUMERIC(18,0) IDENTITY(1,1)
)
INSERT INTO #tempDycUserMaster(numUserCntID) SELECT numUserDetailId FROM UserMaster WHERE numGroupID=@destinationGroupId AND numDomainID=@destinationDomainId
SET @i=1;
DECLARE @numUserCntID AS NUMERIC(18,0)=0
SELECT @totalListRecords = COUNT(*) FROM #tempDycUserMaster

WHILE(@i<=@totalListRecords)
BEGIN
	
	SELECT @numUserCntID = numUserCntID FROM #tempDycUserMaster WHERE ID=@i
	DELETE FROM DycFormConfigurationDetails WHERE numDomainId=@destinationDomainId AND numUserCntID=@numUserCntID  AND bitCustom=0
	INSERT INTO DycFormConfigurationDetails(numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin)
	SELECT  numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			@destinationDomainId,
			@destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			@numUserCntID
			bitGridConfiguration,
			CASE WHEN (SELECT COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) > 0 THEN (SELECT TOP 1  numFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) ELSE D.numFormFieldGroupId END,
			1
FROM BizFormWizardMasterConfiguration AS D WHERE numDomainID=@destinationDomainId AND numGroupID=@destinationGroupId  AND bitCustom=0 
	

	SET @i = @i+1
END

delete  DycFormConfigurationDetails where ISNULL(numUserCntId,0)=0 AND numAuthGroupID=@destinationGroupId
			and numFormId>0
			 AND numDomainId =@destinationDomainId
INSERT INTO DycFormConfigurationDetails(numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin)
SELECT	    numFormID,
			CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END,
			intColumnNum,
			intRowNum,
			@destinationDomainId,
			@destinationGroupId,
			numRelCntType,
			tintPageType,
			bitCustom,
			NULL,
			CASE WHEN (SELECT COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) > 0 THEN (SELECT TOP 1  numFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) ELSE D.numFormFieldGroupId END,
			bitDefaultByAdmin
FROM DycFormConfigurationDetails AS D WHERE ISNULL(numUserCntId,0)=0  AND numDomainId=@sourceDomainId  AND numAuthGroupID=@sourceGroupId and numFormId>0

--delete  DycFormConfigurationDetails where ISNULL(numUserCntId,0)=0  AND numAuthGroupID=@destinationGroupId
--			and numFormId>0
--			AND tintPageType=2 AND numDomainId =@destinationDomainId
--INSERT INTO DycFormConfigurationDetails(numFormID,
--				numFieldID,
--				intColumnNum,
--				intRowNum,
--				numDomainID,
--				numAuthGroupID,
--				numRelCntType,
--				tintPageType,
--				bitCustom,
--				numUserCntID,
--				numFormFieldGroupId,
--				bitDefaultByAdmin)
--SELECT	    numFormID,
--			CASE WHEN (SELECT COUNT(*) FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) > 0 THEN (SELECT TOP 1  numFieldId FROM #tempDycFieldMaster WHERE numOldFieldId=D.numFieldId) ELSE D.numFieldId END,
--			intColumnNum,
--			intRowNum,
--			@destinationDomainId,
--			NULL,
--			numRelCntType,
--			tintPageType,
--			bitCustom,
--			0,
--			CASE WHEN (SELECT COUNT(*) FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) > 0 THEN (SELECT TOP 1  numFormFieldGroupId FROM #tempFormFieldGroupConfigurarionMaster WHERE numOldFormFieldGroupId=D.numFormFieldGroupId) ELSE D.numFormFieldGroupId END,
--			bitDefaultByAdmin
--FROM DycFormConfigurationDetails AS D WHERE numUserCntId=0 AND numDomainId=@sourceDomainId  AND numAuthGroupID=@sourceGroupId

DROP TABLE #tempFormFieldGroupConfigurarionMaster
DROP TABLE #tempDycFieldMaster
DROP TABLE #tempDycUserMaster
END
-----------------------------------------------------------Global Setting Configuration----------------------------------------------------------------------
--ADD Custom Text Field to DycFieldMaster
IF(3 IN(SELECT Items FROM dbo.Split(@taskToPerform,',')))
BEGIN
CREATE TABLE #tempTabMaster(
	numOldTabId NUMERIC(18,0),
	numTabId NUMERIC(18,0),
	bitFixed BIT,
	ID NUMERIC(18,0) IDENTITY(1,1)
)
INSERT INTO #tempTabMaster(numOldTabId, numTabId,bitFixed)
SELECT numTabId,0,bitFixed FROM TabMaster WHERE numDomainID=@sourceDomainId OR bitFixed=1

SET @i=1;
DECLARE @numOldTabId AS NUMERIC(18,0)=0
DECLARE @numTabId AS NUMERIC(18,0)=0
DECLARE @bitTabFixed AS BIT=0
SELECT @totalListRecords = COUNT(*) FROM #tempTabMaster

WHILE(@i<=@totalListRecords)
BEGIN
	SET @numTabId=0
	SET @numOldTabId=0
	SELECT @numOldTabId=numOldTabId,@bitTabFixed=bitFixed FROM #tempTabMaster WHERE ID=@i
	PRINT @numOldTabId
	IF(@bitTabFixed=1)
	BEGIN
		PRINT 'FIXED'
		SELECT @numTabId =@numOldTabId
	END
	ELSE IF EXISTS(SELECT numTabId FROM TabMaster WHERE numTabName=(SELECT TOP 1 numTabName FROM TabMaster WHERE numTabId=@numOldTabId) AND numDomainId=@destinationDomainId)
	BEGIN
		PRINT 'EXIST'
		SELECT @numTabId = numTabId FROM TabMaster WHERE numTabName=(SELECT TOP 1 numTabName FROM TabMaster WHERE numTabId=@numOldTabId) AND numDomainId=@destinationDomainId
	END
	ELSE
	BEGIN
		PRINT 'NEW'
		INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
		SELECT numTabName, Remarks, tintTabType, vcURL, bitFixed, @destinationDomainId, vcImage, vcAddURL, bitAddIsPopUp FROM TabMaster WHERE numTabId=@numOldTabId
		SET @numTabId = SCOPE_IDENTITY()
	END
	
	UPDATE #tempTabMaster SET numTabId=@numTabId WHERE ID=@i
	IF EXISTS(SELECT  * FROM TabDefault WHERE numTabId=@numOldTabId AND numDomainId=@sourceDomainId)
	BEGIN
		IF EXISTS(SELECT  * FROM TabDefault WHERE numTabId=@numTabId AND numDomainId=@destinationDomainId)
		BEGIN
			UPDATE TabDefault SET numTabName=(SELECT  TOP 1 numTabName FROM TabDefault WHERE numTabId=@numOldTabId AND numDomainId=@sourceDomainId)  WHERE numTabId=@numTabId AND numDomainId=@destinationDomainId
		END
		ELSE
		BEGIN
			INSERT INTO TabDefault(numTabId, numTabName, tintTabType, numDomainId)
			SELECT  @numTabId,numTabName,tintTabType,@destinationDomainId FROM TabDefault WHERE numTabId=@numOldTabId AND numDomainId=@sourceDomainId
		END
	END
	SET @i=@i+1;
END

delete from GroupTabDetails where numGroupID=@destinationGroupId  AND tintType<>1
insert into GroupTabDetails(numGroupId,numTabId,bitallowed,numOrder,numRelationShip,tintType,[numProfileID])
SELECT @destinationGroupId, CASE WHEN (SELECT COUNT(*) FROM #tempTabMaster WHERE numTabId=TB.numTabID) > 0 THEN 
(SELECT TOP 1  numTabID FROM #tempTabMaster WHERE numTabId=TB.numTabID) ELSE TB.numTabID END,
TB.bitallowed,TB.numOrder,0,TB.tintType,0 FROM 
GroupTabDetails AS TB WHERE numGroupId=@sourceGroupId  AND tintType<>1


delete from GroupTabDetails where numGroupID=@destinationGroupId  AND tintType=1
insert into GroupTabDetails(numGroupId,numTabId,bitallowed,numOrder,numRelationShip,tintType,[numProfileID])
SELECT @destinationGroupId,(SELECT TOP 1 Grp_id FROM [CFw_Grp_Master] CTG WHERE CTG.numDomainID=@destinationDomainId AND CTG.Grp_Name=CFW.Grp_Name AND CTG.Loc_Id=CFW.Loc_Id),
TB.bitallowed,TB.numOrder,0,TB.tintType,0 FROM 
GroupTabDetails AS TB LEFT JOIN [CFw_Grp_Master] AS CFW ON TB.numTabId=CFW.Grp_id WHERE numGroupId=@sourceGroupId  AND TB.tintType=1

DELETE FROM TreeNavigationAuthorization WHERE numDomainID=@destinationDomainId AND numGroupID=@destinationGroupId

INSERT INTO TreeNavigationAuthorization (numGroupID, numTabID, numPageNavID, bitVisible, numDomainID, tintType)
SELECT  @destinationGroupId, 
CASE WHEN (SELECT COUNT(*) FROM #tempTabMaster WHERE numOldTabId=T.numTabID) > 0 THEN (SELECT TOP 1  numTabID FROM #tempTabMaster WHERE numOldTabId=T.numTabID) ELSE T.numTabID END,
numPageNavID, bitVisible, @destinationDomainId, tintType FROM 
TreeNavigationAuthorization AS T WHERE T.numDomainID=@sourceDomainId AND T.numGroupID=@sourceGroupId
DROP TABLE #tempTabMaster



END
-----------------------------------------------------------eCommerce Configuration----------------------------------------------------------------------
IF(4 IN(SELECT Items FROM dbo.Split(@taskToPerform,',')))
BEGIN
BEGIN TRY
    BEGIN TRANSACTION
	    DELETE FROM PageElementDetail
		WHERE       numSiteID = @destinationSiteId
		INSERT INTO PageElementDetail
                   (numElementID,
                    numAttributeID,
                    vcAttributeValue,
                    numSiteID,
                    numDomainID,vcHtml)
		SELECT numElementID,
                    numAttributeID,
                    vcAttributeValue,
                    @destinationSiteId,
                    @destinationDomainId,vcHtml FROM PageElementDetail WHERE numSiteID=@sourceSiteId
		DELETE FROM SiteTemplates
		WHERE       numSiteID = @destinationSiteId
		INSERT INTO SiteTemplates
                   (
		[vcTemplateName],
		[txtTemplateHTML],
		[numSiteID],
		[numDomainID],
		[numCreatedBy],
		[dtCreateDate],
		[numModifiedBy],
		[dtModifiedDate])
		SELECT [vcTemplateName],
		[txtTemplateHTML],
		@destinationSiteId,
		@destinationDomainId,
		[numCreatedBy],
		[dtCreateDate],
		[numModifiedBy],
		[dtModifiedDate] FROM SiteTemplates WHERE numSiteID=@sourceSiteId
		DELETE FROM SiteMenu  WHERE numPageID IN(SELECT numPageID FROM SitePages WHERE numDomainID=@destinationDomainId AND numSiteID=@destinationSiteId)
		DELETE FROM StyleSheetDetails WHERE numPageID IN(SELECT numPageID FROM SitePages WHERE numDomainID=@destinationDomainId AND numSiteID=@destinationSiteId)
		DELETE FROM SitePages WHERE numDomainID=@destinationDomainId AND numSiteID=@destinationSiteId

		INSERT INTO SitePages(
			vcPageName, vcPageURL, tintPageType, vcPageTitle, numTemplateID, numSiteID, numDomainID, numCreatedBy, dtCreateDate, numModifiedBy, dtModifiedDate, bitIsActive, bitIsMaintainScroll
		)SELECT 
			vcPageName, vcPageURL, tintPageType, vcPageTitle, 
			(SELECT TOP 1 numTemplateID FROM SiteTemplates WHERE vcTemplateName=(SELECT TOP 1 S.vcTemplateName FROM SiteTemplates AS S WHERE S.numTemplateID=ST.numTemplateID AND numDomainID=@sourceDomainId AND numSiteID=@sourceSiteId) 
			AND numDomainID=@destinationDomainId AND numSiteID=@destinationSiteId), 
			@destinationSiteId, @destinationDomainId, numCreatedBy, dtCreateDate, numModifiedBy, dtModifiedDate, bitIsActive, bitIsMaintainScroll
		FROM SitePages As ST WHERE numDomainID=@sourceDomainId AND numSiteID=@sourceSiteId

		INSERT INTO SiteMenu(vcTitle, vcNavigationURL, numPageID, tintLevel, numParentID, numSiteID, numDomainID, bitStatus, intDisplayOrder)
		SELECT vcTitle, vcNavigationURL, 
		(SELECT  TOP 1  numPageID FROM SitePages WHERE numSiteID=@destinationSiteId AND numDomainID=@destinationDomainId AND vcPageName = (SELECT TOP 1 vcPageName FROM SitePages WHERE numSiteID=@sourceSiteId AND numDomainID=@sourceDomainId AND numPageID=SiteMenu.numPageID))
		, tintLevel, numParentID, numSiteID, numDomainID, bitStatus, intDisplayOrder FROM SiteMenu WHERE numSiteID=@sourceSiteId AND numDomainID=@sourceDomainId

		INSERT INTO StyleSheetDetails(numPageID, numCssID, numSurTemplateId)
		SELECT 
		(SELECT TOP 1 numPageID FROM SitePages WHERE numSiteID=@destinationSiteId AND numDomainID=@destinationDomainId AND vcPageName = (SELECT TOP 1 vcPageName FROM SitePages WHERE numSiteID=@sourceSiteId AND numDomainID=@sourceDomainId AND numPageID=StyleSheetDetails.numPageID))
		, numCssID, numSurTemplateId
		FROM StyleSheetDetails WHERE numPageID IN(SELECT numPageID FROM SitePages WHERE numSiteID=@sourceSiteId AND numDomainID=@sourceDomainId)
  COMMIT TRAN -- Transaction Success!
END TRY
BEGIN CATCH
	SELECT ERROR_MESSAGE(),ERROR_LINE()
    ROLLBACK TRAN --RollBack in case of Error
END CATCH
END
--ADD Custom Text Field to DycFieldMaster
--DELETE FROM [AccountTypeDetail] WHERE numDomainID=@destinationDomainId
--INSERT INTO 
--UPDATE Domain SET 
--	numARContactPosition=(SELECT TOP 1 numListID FROM ListDetails WHERE numDomainId=@destinationDomainId AND numListID=41 AND vcData = (SELECT TOP 1 L.vcData FROM Domain AS D LEFT JOIN 
--						 ListDetails AS L ON L.numListItemID=numARContactPosition AND L.numDomainID=@sourceDomainId WHERE D.numDomainId=@sourceDomainId)),
--	bitShowCardConnectLink = (SELECT TOP 1 bitShowCardConnectLink FROM Domain WHERE numDomainId=@sourceDomainId),
--	tintReceivePaymentTo =  (SELECT TOP 1 tintReceivePaymentTo FROM Domain WHERE numDomainId=@sourceDomainId),

--WHERE 
--	numDomainId=@destinationDomainId
COMMIT;  
END TRY 
BEGIN CATCH 
	SELECT ERROR_MESSAGE()
	ROLLBACK TRANSACTION
END CATCH
END