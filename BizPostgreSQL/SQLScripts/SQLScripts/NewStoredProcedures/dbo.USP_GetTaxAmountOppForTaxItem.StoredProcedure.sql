--Created By Anoop Jayaraj    
    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaxAmountOppForTaxItem')
DROP PROCEDURE USP_GetTaxAmountOppForTaxItem
GO
CREATE PROCEDURE [dbo].[USP_GetTaxAmountOppForTaxItem]    
@numDomainID as numeric(9),    
@numTaxItemID as numeric(9),    
@numOppID as numeric(9),    
@numDivisionID as numeric(9),
@numOppBizDocID as numeric(9),
@tintMode AS TINYINT=0    
AS    
IF @tintMode=0 --Order
BEGIN
	SELECT ISNULL(dbo.fn_CalOppItemTotalTaxAmt(@numDomainID,@numTaxItemID,@numOppID,@numOppBizDocID),0)
END
ELSE IF @tintMode=5 OR @tintMode=7 OR @tintMode=8 OR @tintMode=9 --RMA Return
BEGIN
	DECLARE @fltPercentage FLOAT = 0
	DECLARE @TotalTaxAmt AS DECIMAL(20,5) = 0    
	DECLARE @numUnitHour AS NUMERIC(18,0) = 0
	DECLARE @ItemAmount AS DECIMAL(20,5)

	SELECT 
		@TotalTaxAmt = SUM(ISNULL(CASE 
					WHEN OMTI.tintTaxType = 2 
					THEN 
						ISNULL(OMTI.fltPercentage,0) * ISNULL((CASE WHEN @tintMode=5 OR @tintMode=6 THEN numUnitHour ELSE numUnitHourReceived END) * dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(RI.numUOMId, 0)),0)
					ELSE 
						(ISNULL(OMTI.fltPercentage,0) * (CASE WHEN @tintMode=5 OR @tintMode=6 THEN ISNULL(numUnitHour,0) ELSE ISNULL(numUnitHourReceived,0) END) * (CASE
			WHEN ISNULL(RI.fltDiscount,0) > 0 THEN 
				CASE WHEN RI.bitDiscountType = 1 THEN monPrice - (RI.fltDiscount / RI.numUnitHour) ELSE	monPrice - (monPrice * (RI.fltDiscount / 100)) END
			ELSE 
				monPrice
		END)) / 100 END,0))
	FROM 
		ReturnItems RI
	JOIN
		Item I
	ON
		RI.numItemCode = I.numItemCode
	JOIN
		OpportunityItemsTaxItems OITI
	ON
		RI.numReturnItemID = OITI.numReturnItemID
		AND RI.numReturnHeaderID = OITI.numReturnHeaderID
	JOIN
		OpportunityMasterTaxItems OMTI
	ON
		OITI.numReturnHeaderID = OMTI.numReturnHeaderID
		AND OITI.numTaxItemID = OMTI.numTaxItemID
		AND ISNULL(OITI.numTaxID,0) = ISNULL(OMTI.numTaxID,0)
	WHERE 
		RI.numReturnHeaderID=@numOppID   
		AND OITI.numTaxItemID=@numTaxItemID
		 
	SELECT ISNULL(@TotalTaxAmt,0) AS TotalTaxAmt				 
END  
    
