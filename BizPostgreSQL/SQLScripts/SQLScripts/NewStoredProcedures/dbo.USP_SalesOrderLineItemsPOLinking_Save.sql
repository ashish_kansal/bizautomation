GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesOrderLineItemsPOLinking_Save')
DROP PROCEDURE USP_SalesOrderLineItemsPOLinking_Save
GO
CREATE PROCEDURE [dbo].[USP_SalesOrderLineItemsPOLinking_Save]
	@numDomainID NUMERIC(18,0)
	,@numSalesOrderID NUMERIC(18,0)
	,@numSalesOrderItemID NUMERIC(18,0)
	,@numPurchaseOrderID NUMERIC(18,0)
	,@numPurchaseOrderItemID NUMERIC(18,0)
AS
BEGIN
	IF EXISTS (SELECT ID FROM SalesOrderLineItemsPOLinking WHERE numDomainID=@numDomainID AND numSalesOrderID=@numSalesOrderID AND numSalesOrderItemID=@numSalesOrderItemID)
	BEGIN
		UPDATE
			SalesOrderLineItemsPOLinking
		SET
			numPurchaseOrderID=@numPurchaseOrderID
			,numPurchaseOrderItemID=@numPurchaseOrderItemID
		WHERE 
			numDomainID=@numDomainID 
			AND numSalesOrderID=@numSalesOrderID 
			AND numSalesOrderItemID=@numSalesOrderItemID
	END
	ELSE
	BEGIN
		INSERT INTO SalesOrderLineItemsPOLinking
		(
			numDomainID,numSalesOrderID,numSalesOrderItemID,numPurchaseOrderID,numPurchaseOrderItemID
		)
		VALUES
		(
			@numDomainID,@numSalesOrderID,@numSalesOrderItemID,@numPurchaseOrderID,@numPurchaseOrderItemID
		)
	END
END
GO