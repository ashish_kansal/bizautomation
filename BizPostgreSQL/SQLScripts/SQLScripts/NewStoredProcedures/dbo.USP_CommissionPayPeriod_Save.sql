GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CommissionPayPeriod_Save')
DROP PROCEDURE USP_CommissionPayPeriod_Save
GO
Create PROCEDURE [dbo].[USP_CommissionPayPeriod_Save]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0) OUTPUT,
	 @numDomainID AS NUMERIC(18,0),
	 @dtStart DATE,
	 @dtEnd DATE,
	 @tintPayPeriod INT,
	 @numUserID NUMERIC(18,0),
	 @ClientTimeZoneOffset INT     
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
	--NEW PAY PERIOD COMMISSION CALCULATION
	IF @numComPayPeriodID = 0
	BEGIN
		INSERT INTO CommissionPayPeriod
		(
			numDomainID,
			dtStart,
			dtEnd,
			tintPayPeriod,
			numCreatedBy,
			dtCreated
		)
		VALUES
		(
			@numDomainID,
			@dtStart,
			@dtEnd,
			@tintPayPeriod,
			@numUserID,
			GETDATE()
		)

		SET @numComPayPeriodID = SCOPE_IDENTITY()
	END
	-- RECALCULATE PAY PERIOD COMMISSION
	ELSE
	BEGIN
		UPDATE 
			CommissionPayPeriod
		SET 
			numModifiedBy = @numUserID,
			dtModified = GETDATE()
		WHERE
			numComPayPeriodID = @numComPayPeriodID
	END

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	BEGIN TRANSACTION
	
	EXEC USP_CalculateCommission @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
	EXEC USP_CalculateTimeAndExpense @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
	EXEC USP_CalculateCommissionSalesReturn @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=@ClientTimeZoneOffset
	EXEC USP_CalculateCommissionOverPayment @numDomainID=@numDomainID
	
	COMMIT

	SELECT @numComPayPeriodID
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

