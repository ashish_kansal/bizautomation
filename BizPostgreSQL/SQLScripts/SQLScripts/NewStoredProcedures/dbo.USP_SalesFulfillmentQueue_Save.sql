SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentQueue_Save')
DROP PROCEDURE USP_SalesFulfillmentQueue_Save
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentQueue_Save]      
	@numDomainId NUMERIC(18,0),
	@numUserCntId NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numStatus NUMERIC(18,0)
AS
BEGIN
	IF (
			SELECT 
				COUNT(*) 
			FROM 
				SalesFulfillmentConfiguration 
			WHERE 
				numDomainID=@numDomainId 
				AND ISNULL(bitActive,0)=1
				AND 1 = (CASE 
							WHEN ISNULL(bitRule1IsActive,0)=1 AND tintRule1Type=2 AND numRule1OrderStatus=@numStatus THEN 1
							WHEN ISNULL(bitRule2IsActive,0)=1 AND numRule2OrderStatus=@numStatus THEN 1
							WHEN ISNULL(bitRule3IsActive,0)=1 AND numRule3OrderStatus=@numStatus THEN 1
							WHEN ISNULL(bitRule4IsActive,0)=1 AND numRule4OrderStatus=@numStatus THEN 1
							ELSE 0
						END)
		) > 0
	BEGIN
		INSERT INTO SalesFulfillmentQueue
		( 
			numDomainID
			,numUserCntID
			,numOppID
			,numOrderStatus
			,dtDate
			,bitExecuted
			,intNoOfTimesTried
		)
		VALUES
		(
			@numDomainId
			,@numUserCntId
			,@numOppID
			,@numStatus
			,GETUTCDATE()
			,0
			,0
		)
	END
END