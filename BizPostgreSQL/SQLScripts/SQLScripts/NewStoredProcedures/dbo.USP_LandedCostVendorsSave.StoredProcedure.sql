GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostVendorsSave')
DROP PROCEDURE USP_LandedCostVendorsSave
GO
CREATE PROC [dbo].[USP_LandedCostVendorsSave] 
    @numDomainId numeric(18, 0),
    @numVendorId numeric(18, 0)
AS 

BEGIN TRY
	BEGIN TRANSACTION 

		IF EXISTS(SELECT 1 FROM [dbo].[LandedCostVendors] AS LCV WHERE [LCV].[numDomainId]=@numDomainId AND [LCV].[numVendorId]=@numVendorId)
		BEGIN
			RAISERROR('Vendor_Exists', 16, 1)
					return 1
		END
		ELSE
		BEGIN
			INSERT INTO [dbo].[LandedCostVendors] ([numDomainId], [numVendorId])
				SELECT @numDomainId, @numVendorId
		END

	COMMIT TRANSACTION

END TRY
BEGIN CATCH
 DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
END CATCH
	
