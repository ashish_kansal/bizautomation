SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Communication_SnoozeReminder')
DROP PROCEDURE USP_Communication_SnoozeReminder
GO
CREATE PROCEDURE [dbo].[USP_Communication_SnoozeReminder]  
	@numCommID NUMERIC(18,0),
	@LastSnoozDateTimeUtc DATETIME
AS  
BEGIN  
	UPDATE Communication SET LastSnoozDateTimeUtc = @LastSnoozDateTimeUtc WHERE numCommId=@numCommID
END
GO
