
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageCOACreditCardCharge]    Script Date: 09/25/2009 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCOACreditCardCharge')
DROP PROCEDURE USP_ManageCOACreditCardCharge
GO
CREATE PROCEDURE [dbo].[USP_ManageCOACreditCardCharge]
	@numTransChargeID numeric(9)=0 OUTPUT,
	@numAccountId numeric(9),
	@fltTransactionCharge float,
	@tintBareBy TINYINT=0,
	@numCreditCardTypeId numeric(9),
	@numDomainID numeric(9)
AS
IF NOT EXISTS(SELECT * FROM  dbo.COACreditCardCharge WHERE numDomainId=@numDomainID AND numCreditCardTypeId=@numCreditCardTypeId) BEGIN
	INSERT INTO COACreditCardCharge (
		[numAccountId],
		[fltTransactionCharge],
		tintBareBy,
		[numCreditCardTypeId],
		numDomainID
	)
	VALUES (
		@numAccountId,
		@fltTransactionCharge,
		@tintBareBy,
		@numCreditCardTypeId,
		@numDomainID
	)
	SELECT @numTransChargeID=SCOPE_IDENTITY() 
END
ELSE BEGIN
	UPDATE COACreditCardCharge SET 
		[numAccountId] = @numAccountId,
		[fltTransactionCharge] = @fltTransactionCharge,
		tintBareBy=@tintBareBy,
		[numCreditCardTypeId] = @numCreditCardTypeId
	WHERE [numTransChargeID] = @numTransChargeID

END




