GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSimilarRequiredItem')
-- exec USP_GetSimilarRequiredItem 72,4648
DROP PROCEDURE USP_GetSimilarRequiredItem 
GO
Create PROCEDURE [dbo].[USP_GetSimilarRequiredItem] 
	@numDomainID NUMERIC(9),
	@numParentItemCode NUMERIC(9),
	@numWarehouseID NUMERIc(18,0) = 0
AS 
BEGIN
	
	--SELECT * FROM SimilarItems WHERE numParentItemCode = @numParentItemCode AND bitRequired = 1
	/*SELECT 
		I.numItemCode
		,I.txtItemDesc
		,I.numSaleUnit
		,ISNULL(I.numContainer,0) AS numContainer
		,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip
		,dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName
		,ISNULL(WarehouseItems.numWareHouseItemID,0) AS numWareHouseItemID
		,I.charItemType
		,dbo.fn_UOMConversion(I.numSaleUnit,I.numItemCode,@numDomainId,I.numBaseUnit) AS fltUOMConversionFactor
		,SI.numParentItemCode
	FROM 
		SimilarItems SI 
	INNER JOIN 
		Item I 
	ON 
		I.numItemCode = SI.numItemCode
	OUTER APPLY
	(
		SELECT TOP 1
			numWareHouseItemID,
			monWListPrice
		FROM
			WarehouseItems
		WHERE
			WarehouseItems.numItemID = I.numItemCode
	) AS WarehouseItems*/
	SELECT DISTINCT I.vcItemName AS vcItemName,I.txtItemDesc AS txtItemDesc,I.numSaleUnit,ISNULL(I.numContainer,0) AS numContainer,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip 
	,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage 
	,I.txtItemDesc AS txtItemDesc, SI.*, Category.vcCategoryName, ISNULL(W.numWareHouseItemID,0) AS numWareHouseItemID, ISNULL(vcRelationship,'') vcRelationship
	,ROUND(ISNULL(CASE WHEN I.[charItemType]='P' THEN CASE WHEN I.bitSerialized = 1 THEN 1.00000 * monListPrice ELSE 1.00000 * W.[monWListPrice] END ELSE 1.00000 * monListPrice END,0), 2) AS monListPrice
	,dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit,0)) as fltUOMConversionFactor, I.vcSKU, I.vcManufacturer
	,ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc], ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell]
	,ISNULL(bitRequired,0) [bitRequired], ISNULL(vcUpSellDesc,'') [vcUpSellDesc], dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName, ISNULL(numOnHand,0) AS numOnHand
	,(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
					THEN (I.monListPrice*fltDiscountValue)/100
					WHEN P.tintDiscountType=2
					THEN fltDiscountValue
					WHEN P.tintDiscountType=3
					THEN fltDiscountValue*I.monListPrice
					ELSE 0 END
	FROM PromotionOffer AS P 
	LEFT JOIN CartItems AS C ON P.numProId=C.PromotionID
	--WHERE C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID 
	WHERE 
	(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
	I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
	AND P.numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1= (CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END)
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
	) AS PromotionOffers,SI.numParentItemCode
	FROM SimilarItems SI --Category 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory ON ItemCategory.numItemID = I.numItemCode 
	LEFT JOIN Category ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN ItemCategory ON Category.numCategoryID = ItemCategory.numCategoryID 
	--INNER JOIN Item I ON ItemCategory.numItemID = I.numItemCode 
	--INNER JOIN SimilarItems SI ON I.numItemCode = SI.numItemCode 
	OUTER APPLY
		(
			SELECT
				TOP 1 *
			FROM
				WareHouseItems
			WHERE
				 numItemID = I.numItemCode
				 AND (numWareHouseID = @numWarehouseID OR ISNULL(@numWarehouseID,0)=0) 
		) AS W
	WHERE 
		SI.numDomainId = @numDomainID AND SI.numParentItemCode = @numParentItemCode AND bitRequired = 1
END
