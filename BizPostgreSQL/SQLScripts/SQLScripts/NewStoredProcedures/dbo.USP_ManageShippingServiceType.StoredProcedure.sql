GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShippingServiceType' ) 
    DROP PROCEDURE USP_ManageShippingServiceType
GO 
CREATE PROCEDURE USP_ManageShippingServiceType
    @byteMode TINYINT = 0,
    @numServiceTypeID NUMERIC,
    @numDomainID NUMERIC,
    @numRuleID NUMERIC,
    @vcServiceName VARCHAR(100),
    @intNsoftEnum INT=0,
    @intFrom INT,
    @intTo INT,
    @fltMarkup FLOAT,
    @bitMarkupType BIT,
    @monRate DECIMAL(20,5),
    @bitEnabled BIT,
	@vcItemClassification VARCHAR(500),
    @strItems AS text
AS 
BEGIN
	
    IF @byteMode = 0 
        BEGIN
            IF @numServiceTypeID = 0 
                BEGIN
                    INSERT  INTO dbo.ShippingServiceTypes ( vcServiceName,
                                                            intNsoftEnum,
                                                            intFrom,
                                                            intTo,
                                                            fltMarkup,
                                                            bitMarkupType,
                                                            monRate,
                                                            numRuleID,
                                                            numDomainID,
															vcItemClassification
															)
                    VALUES  (
                              @vcServiceName,
                              @intNsoftEnum,
                              @intFrom,
                              @intTo,
                              @fltMarkup,
                              @bitMarkupType,
                              @monRate,
                              @numRuleID,
                              @numDomainID,
							  @vcItemClassification
                            ) 
                END
            ELSE 
                BEGIN
                    UPDATE  dbo.ShippingServiceTypes
                    SET     vcServiceName = @vcServiceName,
                            intNsoftEnum = @intNsoftEnum,
                            intFrom = @intFrom,
                            intTo = @intTo,
                            fltMarkup = @fltMarkup,
                            bitMarkupType = @bitMarkupType,
                            monRate = @monRate,
                            bitEnabled=@bitEnabled,
							vcItemClassification=@vcItemClassification
                    WHERE   numServiceTypeID = @numServiceTypeID
                            AND numDomainID = @numDomainID
			
                END
		
        END
	IF @byteMode = 1
	BEGIN
		DELETE FROM dbo.ShippingServiceTypes WHERE numServiceTypeID=@numServiceTypeID AND numDomainID=@numDomainID
	END
	
	IF @byteMode = 2 
	BEGIN
	    IF @numServiceTypeID = 2
		BEGIN
		    UPDATE dbo.ShippingServiceTypes SET bitEnabled = 0 WHERE intNsoftEnum = 0 AND numRuleID = @numRuleID
		END
	     
		DECLARE  @hDocItem INT

		IF CONVERT(VARCHAR(10),@strItems) <> ''
		BEGIN
			EXEC sp_xml_preparedocument @hDocItem OUTPUT,@strItems
			
			UPDATE 
				dbo.ShippingServiceTypes
			SET 
				vcServiceName=X.vcServiceName,
				intFrom =X.intFrom,
				intTo=X.intTo,
				fltMarkup=X.fltMarkup,
				bitMarkupType=X.bitMarkupType,
				monRate=X.monRate,
				bitEnabled=X.bitEnabled
			FROM 
			(
				SELECT 
					*
				FROM 
					OPENXML (@hDocItem, '/NewDataSet/Item', 2)
				WITH 
				(
					numServiceTypeID NUMERIC(9),
					vcServiceName VARCHAR(100),
					intFrom INT,
					intTo INT,
					fltMarkup FLOAT,
					bitMarkupType BIT,
					monRate DECIMAL(20,5),
					bitEnabled bit
				)
			) X					
			WHERE 
				dbo.ShippingServiceTypes.numServiceTypeID= X.numServiceTypeID
				AND dbo.ShippingServiceTypes.numDomainID=@numDomainID
				
				EXEC sp_xml_removedocument @hDocItem
		END
	END
END
GO
