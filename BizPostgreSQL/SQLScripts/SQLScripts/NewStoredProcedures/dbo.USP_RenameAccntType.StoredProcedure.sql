    
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Friday, August 01, 2008>  
-- Description: <This procedure is use Store Renamed Account Type against Account Type Id>  
-- =============================================  

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RenameAccntType')
DROP PROCEDURE USP_RenameAccntType
GO
CREATE PROCEDURE [dbo].[USP_RenameAccntType]   
@numDomainId as numeric(9)=0,        
@numAcntType as numeric(9)=0,        
@vcRenamedListName as varchar(50)        
as 
declare  @numRecExist numeric(9)
declare @numListId numeric(9)
begin
	
	select @numRecExist=count(numListItemId) from ListOrder 
	where numListItemId =@numAcntType and numDomainId=@numDomainId;

	-- To fetch the numListId Against Account Id.
	select @numListId= numListId from ListDetails where numListItemID= @numAcntType;
	
	if (@numRecExist <> 0)
	begin
		update ListOrder set vcRenamedListName = @vcRenamedListName 
		where numListItemId =@numAcntType and numDomainId=@numDomainId;
	end
	else
	begin	
		insert into ListOrder(numListId , numListItemId, numDomainId, intSortOrder, vcRenamedListName)
		values (@numListId, @numAcntType, @numDomainId, null, @vcRenamedListName );
	end
end
go


