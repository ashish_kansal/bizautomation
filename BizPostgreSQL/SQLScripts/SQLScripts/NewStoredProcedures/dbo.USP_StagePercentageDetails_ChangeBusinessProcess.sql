GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetails_ChangeBusinessProcess')
DROP PROCEDURE USP_StagePercentageDetails_ChangeBusinessProcess
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetails_ChangeBusinessProcess]                             
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)                        
	,@tintProcessType TINYINT
	,@numRecordID NUMERIC(18,0)
	,@numProcessID NUMERIC(18,0)
AS                            
BEGIN
	IF EXISTS (SELECT 
					ID 
				FROM 
					StagePercentageDetailsTaskTimeLog SPDTTL 
				WHERE 
					SPDTTL.numTaskID IN (SELECT 
												SPDT.numTaskID 
											FROM 
												StagePercentageDetailsTask SPDT 
											WHERE 
												1 = (CASE 
														WHEN @tintProcessType = 1 
														THEN (CASE WHEN SPDT.numProjectId = @numRecordID THEN 1 ELSE 0 END)  
														WHEN @tintProcessType = 3
														THEN (CASE WHEN SPDT.numWorkOrderId=@numRecordID THEN 1 ELSE 0 END)
														ELSE 0
													END)))
	BEGIN
		RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
	END
	ELSE
	BEGIN
		DECLARE @numNewProcessId AS NUMERIC(18,0)=0

		BEGIN TRY
		BEGIN TRANSACTION

			IF @tintProcessType = 1
			BEGIN
				DELETE FROM StagePercentageDetailsTask WHERE numProjectId = @numRecordID
				DELETE FROM StagePercentageDetails WHERE numProjectId = @numRecordID
				DELETE FROM Sales_process_List_Master WHERE numProjectId = @numRecordID

				IF(@numProcessID>0)
				BEGIN
					INSERT INTO Sales_process_List_Master
					(
						Slp_Name,
						numdomainid,
						pro_type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,
						tintConfiguration,
						bitAutomaticStartTimer,
						bitAssigntoTeams,
						numOppId,
						numTaskValidatorId,
						numProjectId,
						numWorkOrderId
					)
					 SELECT 
						Slp_Name,
						numdomainid,
						pro_type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,
						tintConfiguration,
						bitAutomaticStartTimer,
						bitAssigntoTeams,
						0,
						numTaskValidatorId,
						@numRecordID,
						0
					FROM 
						Sales_process_List_Master 
					WHERE 
						Slp_Id=@numProcessID    
					
					SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
					
					UPDATE ProjectsMaster SET numBusinessProcessId=@numNewProcessId,numModifiedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() WHERE numProId=@numRecordID

					INSERT INTO StagePercentageDetails
					(
						numStagePercentageId, 
						tintConfiguration, 
						vcStageName, 
						numDomainId, 
						numCreatedBy, 
						bintCreatedDate, 
						numModifiedBy, 
						bintModifiedDate, 
						slp_id, 
						numAssignTo, 
						vcMileStoneName, 
						tintPercentage, 
						tinProgressPercentage, 
						dtStartDate, 
						numParentStageID, 
						intDueDays, 
						numProjectID, 
						numOppID, 
						vcDescription, 
						bitIsDueDaysUsed,
						numTeamId, 
						bitRunningDynamicMode,
						numStageOrder,
						numWorkOrderId
					)
					SELECT
						numStagePercentageId, 
						tintConfiguration, 
						vcStageName, 
						numDomainId, 
						@numUserCntID, 
						GETDATE(), 
						@numUserCntID, 
						GETDATE(), 
						@numNewProcessId, 
						numAssignTo, 
						vcMileStoneName, 
						tintPercentage, 
						tinProgressPercentage, 
						GETDATE(), 
						numParentStageID, 
						intDueDays, 
						@numRecordID, 
						0, 
						vcDescription, 
						bitIsDueDaysUsed,
						numTeamId, 
						bitRunningDynamicMode,
						numStageOrder,
						0
					FROM
						StagePercentageDetails
					WHERE
						slp_id=@numProcessID	

					INSERT INTO StagePercentageDetailsTask
					(
						numStageDetailsId, 
						vcTaskName, 
						numHours, 
						numMinutes, 
						numAssignTo, 
						numDomainID, 
						numCreatedBy, 
						dtmCreatedOn,
						numOppId,
						numProjectId,
						numParentTaskId,
						bitDefaultTask,
						bitSavedTask,
						numOrder,
						numReferenceTaskId,
						numWorkOrderId
					)
					SELECT 
						(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numProjectID=@numRecordID),
						vcTaskName,
						numHours,
						numMinutes,
						ST.numAssignTo,
						@numDomainID,
						@numUserCntID,
						GETDATE(),
						0,
						@numRecordID,
						0,
						1,
						CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
						numOrder,
						numTaskId,
						0
					FROM 
						StagePercentageDetailsTask AS ST
					LEFT JOIN
						StagePercentageDetails As SP
					ON
						ST.numStageDetailsId=SP.numStageDetailsId
					WHERE
						ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numWorkOrderId,0)=0 AND
						ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
						StagePercentageDetails As ST
					WHERE
						ST.slp_id=@numProcessID)
					ORDER BY ST.numOrder
				END
			END
			ELSE IF @tintProcessType = 3
			BEGIN
				DELETE FROM StagePercentageDetailsTask WHERE numWorkOrderId = @numRecordID
				DELETE FROM StagePercentageDetails WHERE numWorkOrderId = @numRecordID
				DELETE FROM Sales_process_List_Master WHERE numWorkOrderId = @numRecordID

				IF(@numProcessID>0)
				BEGIN
					INSERT INTO Sales_process_List_Master
					(
						Slp_Name,
						numdomainid,
						pro_type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,
						tintConfiguration,
						bitAutomaticStartTimer,
						bitAssigntoTeams,
						numOppId,
						numTaskValidatorId,
						numProjectId,
						numWorkOrderId
					)
					 SELECT 
						Slp_Name,
						numdomainid,
						pro_type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,
						tintConfiguration,
						bitAutomaticStartTimer,
						bitAssigntoTeams,
						0,
						numTaskValidatorId,
						0,
						@numRecordID 
					FROM 
						Sales_process_List_Master 
					WHERE 
						Slp_Id=@numProcessID    
					
					SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
					
					UPDATE WorkOrder SET numBuildProcessId=@numNewProcessId,numModifiedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() WHERE numWOId=@numRecordID

					INSERT INTO StagePercentageDetails
					(
						numStagePercentageId, 
						tintConfiguration, 
						vcStageName, 
						numDomainId, 
						numCreatedBy, 
						bintCreatedDate, 
						numModifiedBy, 
						bintModifiedDate, 
						slp_id, 
						numAssignTo, 
						vcMileStoneName, 
						tintPercentage, 
						tinProgressPercentage, 
						dtStartDate, 
						numParentStageID, 
						intDueDays, 
						numProjectID, 
						numOppID, 
						vcDescription, 
						bitIsDueDaysUsed,
						numTeamId, 
						bitRunningDynamicMode,
						numStageOrder,
						numWorkOrderId
					)
					SELECT
						numStagePercentageId, 
						tintConfiguration, 
						vcStageName, 
						numDomainId, 
						@numUserCntID, 
						GETDATE(), 
						@numUserCntID, 
						GETDATE(), 
						@numNewProcessId, 
						numAssignTo, 
						vcMileStoneName, 
						tintPercentage, 
						tinProgressPercentage, 
						GETDATE(), 
						numParentStageID, 
						intDueDays, 
						0, 
						0, 
						vcDescription, 
						bitIsDueDaysUsed,
						numTeamId, 
						bitRunningDynamicMode,
						numStageOrder,
						@numRecordID
					FROM
						StagePercentageDetails
					WHERE
						slp_id=@numProcessID	

					INSERT INTO StagePercentageDetailsTask
					(
						numStageDetailsId, 
						vcTaskName, 
						numHours, 
						numMinutes, 
						numAssignTo, 
						numDomainID, 
						numCreatedBy, 
						dtmCreatedOn,
						numOppId,
						numProjectId,
						numParentTaskId,
						bitDefaultTask,
						bitSavedTask,
						numOrder,
						numReferenceTaskId,
						numWorkOrderId
					)
					SELECT 
						(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numWorkOrderId=@numRecordID),
						vcTaskName,
						numHours,
						numMinutes,
						ST.numAssignTo,
						@numDomainID,
						@numUserCntID,
						GETDATE(),
						0,
						0,
						0,
						1,
						CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
						numOrder,
						numTaskId,
						@numRecordID
					FROM 
						StagePercentageDetailsTask AS ST
					LEFT JOIN
						StagePercentageDetails As SP
					ON
						ST.numStageDetailsId=SP.numStageDetailsId
					WHERE
						ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
						ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
						StagePercentageDetails As ST
					WHERE
						ST.slp_id=@numProcessID)
					ORDER BY ST.numOrder
				END
			END

		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END
END
GO