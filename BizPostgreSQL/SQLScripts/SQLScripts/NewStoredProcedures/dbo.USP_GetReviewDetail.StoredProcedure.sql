SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReviewDetail')
DROP PROCEDURE USP_GetReviewDetail
GO
CREATE PROCEDURE [dbo].[USP_GetReviewDetail]   

@numReviewId AS NUMERIC(18,0) , 
@numContactId AS NUMERIC(18,0),
@Mode AS INT 

AS

BEGIN
	IF @Mode = 1
	  BEGIN
	         SELECT 
	         * 
	         FROM ReviewDetail
             WHERE numReviewId = @numReviewId
             AND numContactId = @numContactId             
      END
END
