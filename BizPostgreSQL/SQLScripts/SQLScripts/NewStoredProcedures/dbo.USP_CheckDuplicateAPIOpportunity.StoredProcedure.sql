GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckDuplicateAPIOpportunity')
DROP PROCEDURE USP_CheckDuplicateAPIOpportunity
GO
/****** Object:  StoredProcedure [dbo].[USP_CheckDuplicateAPIOpportunity]    Script Date: 05/07/2009 22:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CheckDuplicateAPIOpportunity]
                @WebApiId    INT,
                @numDomainId NUMERIC(18,0),
                @vcAPIOppId  VARCHAR(50)
AS
  BEGIN
  
    SELECT COUNT(*)
    FROM   dbo.OpportunityMaster OM 
    WHERE  OM.[tintSourceType] = 3
		   AND OM.[tintSource] = @WebApiId
           AND OM.[numDomainId] = @numDomainId
           AND OM.[vcMarketplaceOrderID] = @vcAPIOppId

  END
