GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DycFieldMasterSynonym_Save')
DROP PROCEDURE dbo.USP_DycFieldMasterSynonym_Save
GO
CREATE PROCEDURE [dbo].[USP_DycFieldMasterSynonym_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numFieldID NUMERIC(18,0)
	,@vcSynonym VARCHAR(200)
	,@bitCustomField BIT
)
AS 
BEGIN
	INSERT INTO DycFieldMasterSynonym
	(
		numDomainID
		,numFieldID
		,bitCustomField
		,vcSynonym
		,bitDefault
	)
	VALUES
	(
		@numDomainID
		,@numFieldID
		,@bitCustomField
		,@vcSynonym
		,0
	)
END
GO