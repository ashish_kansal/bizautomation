SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItems_ValidateLocation')
DROP PROCEDURE USP_WareHouseItems_ValidateLocation
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItems_ValidateLocation]  
	@numDomainID AS NUMERIC(18,0),
	@numWareHouseID AS NUMERIC(18,0),
	@numWLocationID AS NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0)
AS
BEGIN
	DECLARE @bitDuplicate AS BIT = 0
	
	IF                                                                 
		(SELECT
			COUNT(*)
		FROM
			WareHouseItems WI                               
		WHERE
			WI.numDomainID = @numDomainID
			AND WI.numItemID = @numItemCode
			AND WI.numWarehouseID = @numWareHouseID
			AND ISNULL(WI.numWLocationID,0) = ISNULL(@numWLocationID,0)			 
		) > 0
	BEGIN
		SET @bitDuplicate = 1
	END

	SELECT @bitDuplicate
END

