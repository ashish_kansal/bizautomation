GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderExpenseItemsAddBill')
DROP PROCEDURE USP_GetOrderExpenseItemsAddBill
GO
CREATE PROCEDURE [dbo].[USP_GetOrderExpenseItemsAddBill]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		SELECT 
			OI.numoppitemtCode
			,ISNULL(OI.vcItemName,I.vcItemName) vcItemName
		FROM 
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OI.numoppitemtCode = @numOppItemID
	END
	ELSE
	BEGIN

		SELECT 
			OI.numoppitemtCode
			,ISNULL(OI.vcItemName,I.vcItemName) vcItemName
		FROM 
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OI.numOppId = @numOppID
			AND ISNULL(I.charItemType,0) = 'S'
			AND ISNULL(bitExpenseItem,0) = 1
			AND OI.numoppitemtCode NOT IN (SELECT BD.numOppItemID FROM BillDetails BD WHERE numOppID=@numOppID)
		ORDER BY
			ISNULL(OI.numSortOrder,0)
	END
END
GO