
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetContactDetails')
DROP PROCEDURE USP_OpportunityMaster_GetContactDetails
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetContactDetails]  
 @cntIds as varchar(1000)
as
begin
	select  ADC.numDivisionId,adc.numContactId,vcGivenName,vcFirstName,vcLastName, c.vcCompanyName,dm.vcDivisionName 
		FROM AdditionalContactsInformation ADC                                                   
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
		JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
	 where ADC.numContactId in (select * from dbo.SplitString(@cntIds,','))
	
end