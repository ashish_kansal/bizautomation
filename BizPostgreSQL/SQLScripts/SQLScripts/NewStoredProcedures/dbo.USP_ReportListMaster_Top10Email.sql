SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10Email')
DROP PROCEDURE USP_ReportListMaster_Top10Email
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10Email]
	@numDomainID NUMERIC(18,0)
	,@numUserCntId NUMERIC(18,0)
AS
BEGIN 
	DECLARE @Temp TABLE
	(
		numEmailHstrID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
	)

	INSERT INTO @Temp
	(
		numEmailHstrID
		,vcCompanyName
	)
	SELECT TOP 10
		EH.numEmailHstrID
		,CI.vcCompanyName
	From 
		EmailHistory EH 
	INNER JOIN 
		EmailMaster EM 
	ON 
		EM.numEMailId=EH.numEmailId
	INNER JOIN
		AdditionalContactsInformation ACI
	ON 
		ACI.numContactId = EM.numContactId
	INNER JOIN
		DivisionMaster DM
	ON
		ACI.numDivisionId = DM.numDivisionID
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		EH.numDomainID = @numDomainID
		AND EH.numUserCntId = @numUserCntId
	ORDER BY 
		bintCreatedOn DESC

	SELECT 
		T1.*
		,CASE 
			WHEN LEN(Cast(EH.vcBodyText AS VARCHAR(1000)))>150 
			THEN SUBSTRING(EH.vcBodyText,0,150) + '...' 
			ELSE EH.vcBodyText END 
		AS vcBodyText
		,EH.vcSubject
		,REVERSE(SUBSTRING(REVERSE(EH.vcFrom),0,CHARINDEX('$^$',REVERSE(EH.vcFrom)))) vcFrom
	FROM
		EmailHistory EH
	INNER JOIN
		@Temp T1
	ON
		EH.numEmailHstrID = T1.numEmailHstrID
	WHERE
		EH.numDomainID = @numDomainID
		AND EH.numUserCntId = @numUserCntId
END
GO

