GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetChildKitsOfKit' ) 
    DROP PROCEDURE USP_ItemDetails_GetChildKitsOfKit
GO

CREATE PROCEDURE USP_ItemDetails_GetChildKitsOfKit  
(  
	@numDomainID NUMERIC(18,0),  
	@numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;
	SELECT
		ISNULL(vcItemName,'') vcItemName
		,ISNULL(txtItemDesc,'') txtItemDesc
		,ISNULL(monListPrice,0) monListPrice
		,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1),'') vcPathForTImage
		,ISNULL(bitCalAmtBasedonDepItems,0) bitCalAmtBasedonDepItems
		,isnull(D.vcCurrency,'') as vcCurrency
	FROM
		Item
	INNER JOIN
		Domain D
	ON
		Item.numDomainID = D.numDomainId
	WHERE
		Item.numDomainID=@numDomainID
		AND Item.numItemCode=@numItemCode


	SELECT
		Item.numItemCode
		,Item.vcItemName
		,ISNULL(Item.bitKitSingleSelect,0) bitKitSingleSelect
		,ISNULL(ItemDetails.tintView,1) tintView
		,ISNULL(STUFF((SELECT 
					CONCAT(',',numSecondaryListID)
				FROM 
					FieldRelationship FR
				WHERE 
					FR.numDomainID=@numDomainID
					AND FR.numModuleID = 14
					AND numPrimaryListID=Item.numItemCode
					AND numSecondaryListID IN (SELECT numChildItemID FROM ItemDetails WHERE numItemKitID=@numItemCode)
				GROUP BY
					numSecondaryListID
				FOR XML PATH('')),1,1,''),'') vcDependedKits
		,ISNULL(STUFF((SELECT 
					CONCAT(',',numPrimaryListID)
				FROM 
					FieldRelationship FR 
				WHERE 
					FR.numDomainID=@numDomainID
					AND FR.numModuleID = 14 
					AND numSecondaryListID=Item.numItemCode
					AND numPrimaryListID IN (SELECT numChildItemID FROM ItemDetails WHERE numItemKitID=@numItemCode)
				GROUP BY
					numPrimaryListID
				FOR XML PATH('')),1,1,''),'') vcParentKits
		,ISNULL(bitOrderEditable,0) bitOrderEditable
	FROM
		ItemDetails
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND ISNULL(Item.bitKitParent,0) = 1
	WHERE
		ItemDetails.numItemKitID = @numItemCode
	ORDER BY
		ISNULL(sintOrder,0)
END  
GO

