SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ValidateBizDocFulfillment')
DROP PROCEDURE USP_ValidateBizDocFulfillment
GO
CREATE PROCEDURE [dbo].[USP_ValidateBizDocFulfillment]
    (
	  @numDomainID NUMERIC(9),
      @numOppID NUMERIC(9),
      @numFromOppBizDocsId NUMERIC(9),
      @numBizDocId NUMERIC(9)
    )
AS 
    BEGIN
   
		DECLARE @numPackingSlipBizDocId NUMERIC(9),@numAuthInvoice NUMERIC(9),@numFulfillmentOrderBizDocId NUMERIC(9)
		
		SELECT @numPackingSlipBizDocId=numListItemID FROM ListDetails WHERE vcData='Packing Slip' AND constFlag=1 AND numListID=27
	    
		Select @numAuthInvoice=isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=@numDomainId 
	    
	    SET @numFulfillmentOrderBizDocId=296
	    
	    IF @numFromOppBizDocsId>0
	    BEGIN
			PRINT '1'
	    END
	    ELSE
	    BEGIN
			IF @numFulfillmentOrderBizDocId=@numBizDocId
			BEGIN
				--Only a Sales Order can create a Fulfillment Order (Don�t allow an FO to be created from an Opportunity)
				IF EXISTS (SELECT numOppID FROM OpportunityMaster OM WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID AND OM.tintOppstatus=0)
				BEGIN
					RAISERROR ('NOT_ALLOWED_FulfillmentOrder',16,1);
					RETURN -1
				END
			END	
		END
    END
