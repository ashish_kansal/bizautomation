GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AccountTypeDefaultValue')
DROP PROCEDURE USP_AccountTypeDefaultValue
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select  * FROM [AccountTypeDetail]

--Created by Chintan                               
--exec [dbo].[USP_AccountTypeDefaultValue] 177
CREATE PROCEDURE [dbo].[USP_AccountTypeDefaultValue]
@numDomainId as numeric(9)=0
As                                                    
Begin                                                    
                                                    
 Declare @intCount as numeric(9)                                                    
 Set @intCount=0                                  
 
 
DECLARE  @AccountTypeIdPrimary  AS NUMERIC(9)
DECLARE  @AccountTypeIdAsset  AS NUMERIC(9)
DECLARE  @AccountTypeIdCurrentAsset  AS NUMERIC(9)
DECLARE  @AccountTypeIdFixedAsset  AS NUMERIC(9)
DECLARE  @AccountTypeIdLiability  AS NUMERIC(9)
DECLARE  @AccountTypeIdCurrentLiability  AS NUMERIC(9)
DECLARE  @AccountTypeIdIncome  AS NUMERIC(9)
DECLARE  @AccountTypeIdExpense  AS NUMERIC(9)
DECLARE  @AccountTypeIdDirectExpense  AS NUMERIC(9)
DECLARE  @AccountTypeIdInDirectExpense  AS NUMERIC(9)
DECLARE  @AccountTypeIdInEquity  AS NUMERIC(9)
DECLARE  @AccountTypeIdShareHolderFunds  AS NUMERIC(9)
DECLARE  @AccountTypeIdCOGS  AS NUMERIC(9)
DECLARE  @AccountTypeIdNonLiability  AS NUMERIC(9)


 
                                           
 Select @intCount=count(*) From [AccountTypeDetail] Where numDomainId=@numDomainId                                                    
 
 If @intCount=0                                                     
  Begin    
	                                                
   --1  
    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01','Primary',NULL,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
   SET @AccountTypeIdPrimary =SCOPE_IDENTITY()
   INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('0101','Assets',@AccountTypeIdPrimary,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
   SET @AccountTypeIdAsset =SCOPE_IDENTITY()

   --2
   INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010101','Current Assets',@AccountTypeIdAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
   SET @AccountTypeIdCurrentAsset =SCOPE_IDENTITY()
   INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010102','Fixed Assets',@AccountTypeIdAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
   SET @AccountTypeIdFixedAsset =SCOPE_IDENTITY()

   INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010103','Non Current Assets',@AccountTypeIdAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)

   INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010104','Other Assets',@AccountTypeIdAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
   
   DECLARE @AccountTypeIdOtherAsset AS NUMERIC(9)
   SET @AccountTypeIdOtherAsset = SCOPE_IDENTITY()
    --commneted by chintan
--   INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010401','Long Term Investment',@AccountTypeIdOtherAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)

   --3
    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010101','Bank Accounts',@AccountTypeIdCurrentAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010102','Cash-in-Hand',@AccountTypeIdCurrentAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010103','Investments',@AccountTypeIdCurrentAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010104','Stock',@AccountTypeIdCurrentAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010105','Account Receivable',@AccountTypeIdCurrentAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
    
    DECLARE  @AccountTypeIdAccountReceivable  AS NUMERIC(9)
    SET @AccountTypeIdAccountReceivable = SCOPE_IDENTITY()
    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('0101010501','Accounts Receivable Clearing Acct',@AccountTypeIdAccountReceivable,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
  
    --4
    --commneted by chintan
--    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010201','Statutory Deposits',@AccountTypeIdFixedAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010201','Property & Equipment',@AccountTypeIdFixedAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
--	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010202','Long Term Investments',@AccountTypeIdFixedAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01010202','Intangible Assets',@AccountTypeIdFixedAsset,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)

	--5
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('0102','Liabilities',@AccountTypeIdPrimary,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	SET @AccountTypeIdLiability =SCOPE_IDENTITY()
	--6
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010201','Current Liabilities',@AccountTypeIdLiability,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
    SET @AccountTypeIdCurrentLiability =SCOPE_IDENTITY()

    --7
    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01020101','Duties & Taxes',@AccountTypeIdCurrentLiability,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01020102','Account Payable',@AccountTypeIdCurrentLiability,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	
	DECLARE  @AccountTypeIdAccountPayable  AS NUMERIC(9)
    SET @AccountTypeIdAccountPayable = SCOPE_IDENTITY()
    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('0102010201','Accounts Payable Clearing Acct',@AccountTypeIdAccountPayable,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01020103','Short Term Borrowings',@AccountTypeIdCurrentLiability,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
--	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01020104','Suspense A/c',@AccountTypeIdCurrentLiability,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	
	--8
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010202','Non Current Liabilities',@AccountTypeIdLiability,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	SET @AccountTypeIdNonLiability = SCOPE_IDENTITY()
	
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01020201','Notes Payable',@AccountTypeIdNonLiability,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	
	--9
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('0103','Income',@AccountTypeIdPrimary,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	SET @AccountTypeIdIncome =SCOPE_IDENTITY()
	--10
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010301','Direct Income',@AccountTypeIdIncome,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010302','Other Income',@AccountTypeIdIncome,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	--11
    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('0104','Expense',@AccountTypeIdPrimary,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
    SET @AccountTypeIdExpense =SCOPE_IDENTITY()
    --12
    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010401','Direct Expense',@AccountTypeIdExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
    SET @AccountTypeIdDirectExpense =SCOPE_IDENTITY()
    --13
    INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040101','Other Direct Expenses',@AccountTypeIdDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040102','Selling & Distribution Costs',@AccountTypeIdDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040103','Operating Expenses',@AccountTypeIdDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	--C.patel says we do not need seperate account type for Price Variance
--	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040104','Price Variance',@AccountTypeIdDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),0)
	--14
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010402','Other Expenses',@AccountTypeIdExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	SET @AccountTypeIdInDirectExpense =SCOPE_IDENTITY()
	--15
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040201','Administrative Expenses',@AccountTypeIdInDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040202','Staff Expenses',@AccountTypeIdInDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040203','Finance Costs',@AccountTypeIdInDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040204','Establishment Costs',@AccountTypeIdInDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040205','Non-Cash Expenses',@AccountTypeIdInDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01040206','UnCategorized Expense',@AccountTypeIdInDirectExpense,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	--16
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('0105','Equity',@AccountTypeIdPrimary,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	SET @AccountTypeIdInEquity =SCOPE_IDENTITY()
	--17
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010501','Shareholders Funds',@AccountTypeIdInEquity,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	SET @AccountTypeIdShareHolderFunds =SCOPE_IDENTITY()
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010502','Owner''s Capital Account',@AccountTypeIdInEquity,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	
	--18
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01050101','Reserves & Surplus',@AccountTypeIdShareHolderFunds,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('01050102','Common Stock',@AccountTypeIdShareHolderFunds,@numDomainId,GETUTCDATE(),GETUTCDATE(),0,1)
   
   -- 19
   INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('0106','Cost Of Goods',@AccountTypeIdPrimary,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
   SET @AccountTypeIdCOGS =SCOPE_IDENTITY()
   
   -- 20
   INSERT INTO [AccountTypeDetail] ([vcAccountCode],[vcAccountType],[numParentID],[numDomainID],[dtCreateDate],[dtModifiedDate],[bitSystem],[bitActive]) Values('010601','Supplies & Materials - COGS',@AccountTypeIdCOGS,@numDomainId,GETUTCDATE(),GETUTCDATE(),1,1)
	
	
	----- 30th Nov,2012
	UPDATE dbo.AccountTypeDetail SET bitSystem = 1 
	WHERE vcAccountCode IN 
	('01','0101','010101','01010101','01010102','01010104','01010105','010102','0102','010201','01020101','01020102','010202','0103','010301','010302','0104','010401',
	'01040101','010402','01040203','01040205','0105','010501','01050101','0106','010601','0102010201','0101010501') 
	AND numDomainID=@numDomainId
	-----
	UPDATE dbo.AccountTypeDetail SET bitSystem = 0
	WHERE vcAccountCode IN 
	('01010103','01010202','01010204','010103','010104','01020103','01020104','01020201','01040102','01040103','01040201','01040202','01040204','010502') 
	AND numDomainID=@numDomainId
	
   return                         
  END 
                                                  
End
