SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_DeleteDycCartFilter')
	DROP PROCEDURE usp_DeleteDycCartFilter
GO

CREATE PROCEDURE [dbo].[usp_DeleteDycCartFilter]
	@numFieldID numeric(18, 0),
	@numFormID numeric(18, 0),
	@numSiteID numeric(18, 0),
	@numDomainID numeric(18, 0)
AS

DELETE FROM [dbo].[DycCartFilters]
WHERE
	[numFieldID] = @numFieldID
	AND [numFormID] = @numFormID
	AND [numSiteID] = @numSiteID
	AND [numDomainID] = @numDomainID

GO
