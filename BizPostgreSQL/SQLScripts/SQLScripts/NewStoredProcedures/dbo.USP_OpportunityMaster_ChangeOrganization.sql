GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_ChangeOrganization')
DROP PROCEDURE USP_OpportunityMaster_ChangeOrganization
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_ChangeOrganization]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numNewDivisionID NUMERIC(18,0),
	@numNewContactID NUMERIC(18,0)               
)                                    
as
BEGIN
	DECLARE @numOldDivisionID NUMERIC(18,0)

	SELECT @numOldDivisionID=numDivisionId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId = @numOppID

	DECLARE @vcStreet varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)      
	DECLARE @numState numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
	DECLARE @bitIsPrimary BIT, @bitAltContact BIT;
	DECLARE @tintAddressOf TINYINT

	SELECT 
		@vcCompanyName=vcCompanyName
		,@numCompanyId=div.numCompanyID 
	FROM 
		CompanyInfo Com                            
	JOIN 
		divisionMaster Div                            
	ON 
		div.numCompanyID=com.numCompanyID                            
	WHERE 
		div.numdivisionID=@numNewDivisionID

	BEGIN TRY
	BEGIN TRANSACTION

		UPDATE OpportunityMaster SET numDivisionId=@numNewDivisionID,numContactId=@numNewContactID,tintBillToType=2,numBillToAddressID=0,tintShipToType=2,numShipToAddressID=0 WHERE numDomainId=@numDomainID AND numOppId = @numOppID
		
		--Update Billing Address Details
		SELECT 
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@tintAddressOf=ISNULL(tintAddressOf,0)
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails AD
		WHERE 
			AD.numDomainID=@numDomainID 
			AND AD.numRecordID=@numNewDivisionID 
			AND AD.tintAddressOf = 2 
			AND AD.tintAddressType = 1 
			AND AD.bitIsPrimary=1       

  		EXEC dbo.USP_UpdateOppAddress
			@numOppID = @numOppID, --  numeric(9, 0)
			@byteMode = 0, --  tinyint
			@vcStreet = @vcStreet, --  varchar(100)
			@vcCity = @vcCity, --  varchar(50)
			@vcPostalCode = @vcPostalCode, --  varchar(15)
			@numState = @numState, --  numeric(9, 0)
			@numCountry = @numCountry, --  numeric(9, 0)
			@vcCompanyName = @vcCompanyName, --  varchar(100)
			@numCompanyId = 0, --  numeric(9, 0)
			@vcAddressName = @vcAddressName,
			@bitCalledFromProcedure = 1,
			@numContact = @numContact,
			@bitAltContact = @bitAltContact,
			@vcAltContact = @vcAltContact

		--Update Shipping Address Details
		SELECT 
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@tintAddressOf=ISNULL(tintAddressOf,0)
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails AD
		WHERE 
			AD.numDomainID=@numDomainID 
			AND AD.numRecordID=@numNewDivisionID 
			AND AD.tintAddressOf = 2 
			AND AD.tintAddressType = 2 
			AND AD.bitIsPrimary=1       

  		EXEC dbo.USP_UpdateOppAddress
			@numOppID = @numOppID, --  numeric(9, 0)
			@byteMode = 1, --  tinyint
			@vcStreet = @vcStreet, --  varchar(100)
			@vcCity = @vcCity, --  varchar(50)
			@vcPostalCode = @vcPostalCode, --  varchar(15)
			@numState = @numState, --  numeric(9, 0)
			@numCountry = @numCountry, --  numeric(9, 0)
			@vcCompanyName = @vcCompanyName, --  varchar(100)
			@numCompanyId =0, --  numeric(9, 0)
			@vcAddressName = @vcAddressName,
			@bitCalledFromProcedure = 1,
			@numContact = @numContact,
			@bitAltContact = @bitAltContact,
			@vcAltContact = @vcAltContact


		--Delete Tax of old organization and add for new organization
		DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId

		--Insert Tax for Opportunity Items
		INSERT INTO dbo.OpportunityItemsTaxItems 
		(
			numOppId,
			numOppItemID,
			numTaxItemID,
			numTaxID
		) 
		SELECT 
			@numOppId,
			OI.numoppitemtCode,
			TI.numTaxItemID,
			0
		FROM 
			dbo.OpportunityItems OI 
		JOIN 
			dbo.ItemTax IT 
		ON 
			OI.numItemCode=IT.numItemCode 
		JOIN
			TaxItems TI 
		ON 
			TI.numTaxItemID = IT.numTaxItemID 
		WHERE 
			OI.numOppId=@numOppID 
			AND IT.bitApplicable=1
		UNION
		SELECT 
			@numOppId,
			OI.numoppitemtCode,
			0,
			0
		FROM 
			dbo.OpportunityItems OI 
		JOIN 
			dbo.Item I 
		ON 
			OI.numItemCode=I.numItemCode
		WHERE 
			OI.numOppId=@numOppID 
			AND I.bitTaxable=1 
		UNION
		SELECT
			@numOppId,
			OI.numoppitemtCode,
			1,
			TD.numTaxID
		FROM
			dbo.OpportunityItems OI 
		INNER JOIN
			ItemTax IT
		ON
			IT.numItemCode = OI.numItemCode
		INNER JOIN
			TaxDetails TD
		ON
			TD.numTaxID = IT.numTaxID
			AND TD.numDomainId = @numDomainId
		WHERE
			OI.numOppId = @numOppID

		INSERT INTO [dbo].[RecordOrganizationChangeHistory]
		(
			numDomainID,numOppID,numOldDivisionID,numNewDivisionID,numModifiedBy,dtModified
		)
		VALUES
		(
			@numDomainID,@numOppID,@numOldDivisionID,@numNewDivisionID,@numUserCntID,GETUTCDATE()
		)
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO