USE [BizExch2007DB]
GO
/****** Object:  StoredProcedure [dbo].[USP_RecurringTransactionDetailReport]    Script Date: 03/25/2009 22:49:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC [dbo].[USP_RecurringTransactionDetailReport] 1
ALTER PROCEDURE [dbo].[USP_RecurringTransactionDetailReport]
               @numDomainID NUMERIC(9,0)
AS
  BEGIN
    SELECT   X.*
    FROM     (
				--BizDoc Recurrring
				SELECT distinct
                     RTR.numRecTranSeedId,
                     RTR.tintRecType,
                     (CASE 
                        WHEN RTR.tintRecType = 1 THEN 'Recurring Sales Order'
                        WHEN RTR.tintRecType = 2 THEN 'Recurring Biz Doc'
                        ELSE 'Unknown'
                      END) AS TranType,
                     RT.varRecurringTemplateName,
					 OM.vcPOppName AS RecurringName,
                     OPR.numRecurringId,
                     CASE RT.chrIntervalType
                     WHEN 'D' THEN 'Daily'
                     WHEN 'M' THEN 'Monthly'
                     WHEN 'Y' THEN 'Yearly'
                     ELSE 'Unknown'
                     END AS Frequency,
                     dbo.FormatedDateFromDate(RT.[dtStartDate],OM.[numDomainId]) StartDate,
                     dbo.FormatedDateFromDate(RT.[dtEndDate],OM.[numDomainId]) EndDate,
					(SELECT COUNT(*) FROM [RecurringTransactionReport] WHERE numRecTranSeedId = RTR.numRecTranSeedId) AS numNoTransactions,
                     CASE RT.chrIntervalType
                     WHEN 'D' THEN DATEDIFF(DAY,RT.[dtStartDate],ISNULL(RT.[dtEndDate],GETUTCDATE()))
                     WHEN 'M' THEN DATEDIFF(month,RT.[dtStartDate],ISNULL(RT.[dtEndDate],GETUTCDATE()))
                     WHEN 'Y' THEN DATEDIFF(year,RT.[dtStartDate],ISNULL(RT.[dtEndDate],GETUTCDATE()))
                     ELSE DATEDIFF(DAY,RT.[dtStartDate],ISNULL(RT.[dtEndDate],GETUTCDATE()))
                     END AS ExpectedChildRecords
              FROM   OpportunityRecurring AS OPR
                     INNER JOIN OpportunityMaster AS OM
                       ON OPR.numOppId = OM.numOppId
                     INNER JOIN RecurringTemplate AS RT
                       ON OPR.numRecurringId = RT.numRecurringId
                     INNER JOIN RecurringTransactionReport AS RTR
                       ON OPR.numOppId = RTR.numRecTranSeedId
                     INNER JOIN OpportunityBizDocs AS OBD
                       ON OM.numOppId = OBD.numOppId
                          AND RTR.numRecTranBizDocID = OBD.numOppBizDocsId
              WHERE  OM.numDomainID = @numDomainID AND RTR.tintRecType = 2
              UNION 
              --Sales Order Recurring 
              SELECT DISTINCT
                     RTR.numRecTranSeedId,
                     RTR.tintRecType,
                     (CASE 
                        WHEN RTR.tintRecType = 1 THEN 'Recurring Sales Order'
                        WHEN RTR.tintRecType = 2 THEN 'Recurring Biz Doc'
                        ELSE 'Unknown'
                      END) AS TranType,
                     RT.varRecurringTemplateName,
                     o2.vcPOppName AS RecurringName,
                     OPR.numRecurringId,
                      CASE RT.chrIntervalType
                     WHEN 'D' THEN 'Daily'
                     WHEN 'M' THEN 'Monthly'
                     WHEN 'Y' THEN 'Yearly'
                     ELSE 'Unknown'
                     END AS Frequency,
                     dbo.FormatedDateFromDate(RT.[dtStartDate],o2.[numDomainId]) StartDate,
                     dbo.FormatedDateFromDate(RT.[dtEndDate],o2.[numDomainId]) EndDate,
                     (SELECT COUNT(*) FROM [RecurringTransactionReport] WHERE numRecTranSeedId = RTR.numRecTranSeedId) AS numNoTransactions,
                     CASE RT.chrIntervalType
                     WHEN 'D' THEN DATEDIFF(DAY,RT.[dtStartDate],ISNULL(RT.[dtEndDate],GETUTCDATE()))
                     WHEN 'M' THEN DATEDIFF(month,RT.[dtStartDate],ISNULL(RT.[dtEndDate],GETUTCDATE()))
                     WHEN 'Y' THEN DATEDIFF(year,RT.[dtStartDate],ISNULL(RT.[dtEndDate],GETUTCDATE()))
                     ELSE DATEDIFF(DAY,RT.[dtStartDate],ISNULL(RT.[dtEndDate],GETUTCDATE()))
                     END AS ExpectedChildRecords
              FROM   RecurringTransactionReport RTR
                     INNER JOIN OpportunityMaster AS o1
                       ON RTR.numRecTranOppID = o1.numOppId
                     INNER JOIN OpportunityMaster AS o2
                       ON RTR.numRecTranSeedId = o2.numOppId
                     LEFT OUTER JOIN [OpportunityRecurring] OPR
                       ON OPR.numOppId = o2.numOppId
                     INNER JOIN RecurringTemplate RT
                       ON OPR.numRecurringId = RT.numRecurringId
                          AND o1.numDomainId = @numDomainID
                          AND o2.numdomainId = @numDomainID
				WHERE RTR.tintRecType = 1
				
              ) AS X
  END
