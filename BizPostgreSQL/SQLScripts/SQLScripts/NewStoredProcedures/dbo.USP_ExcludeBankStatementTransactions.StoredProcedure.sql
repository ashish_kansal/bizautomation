
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ExcludeBankStatementTransactions')
	DROP PROCEDURE USP_ExcludeBankStatementTransactions
GO

/****** Added By : Joseph ******/
/****** Exclude the Transaction Detail in Biz Mapping from Table BankStatementTransaction******/


CREATE PROCEDURE [dbo].[USP_ExcludeBankStatementTransactions]
	@numTransactionID numeric(18, 0),
	@numDomainID numeric(18, 0)
AS

SET NOCOUNT ON

UPDATE dbo.BankStatementTransactions
SET bitIsActive = 0 
WHERE
	[numTransactionID] = @numTransactionID

GO
