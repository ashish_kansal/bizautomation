/****** Object:  StoredProcedure [dbo].[usp_GetCustomTabLayoutInfoDdl]    Script Date: 07/26/2008 16:17:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetCustomTabLayoutInfoDdl')
DROP PROCEDURE usp_GetCustomTabLayoutInfoDdl
GO
CREATE PROCEDURE [dbo].[usp_GetCustomTabLayoutInfoDdl]                                
@numUserCntId as numeric(9)=0,                              
@intcoulmn as numeric(9),                              
@numDomainID as numeric(9)=0 ,                    
@PageId as numeric(9)=4,              
@numRelation as numeric(9)=0,
@numFormID as numeric(9)=0,
@tintPageType TINYINT,
@numTabID as numeric(9)=0            
as                        
   
Declare @sql as nvarchar(4000)
   
if( @intcoulmn <> 0 )--Fields added to layout
begin                         
   
SET @sql='select fld_label as vcfieldName ,fld_id as numFieldId,DFCD.intRowNum as tintRow,1 as bitCustomField
from CFW_Fld_Master CFM join CFw_Grp_Master CGM on CFM.subgrp=CGM.Grp_id 
join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID=CGM.Grp_id and DFCD.numFieldId=CFM.fld_id' 

IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14 or @PageId= 4 
BEGIN
	SET @sql = @sql + ' join CFW_Fld_Dtl CFD on CFM.Fld_id=CFD.numFieldId'                              
END

SET @sql = @sql + ' where CGM.numDomainID=@numDomainID and CGM.Grp_id=@numTabID and CFM.numDomainID=@numDomainID                          
and DFCD.numUserCntID=@numUserCntID and DFCD.numDomainID=@numDomainID and DFCD.numFormId=@numFormID and
DFCD.numRelCntType=@numRelation AND DFCD.tintPageType=@tintPageType AND DFCD.numAuthGroupID=@numTabID
and DFCD.intColumnNum = @intcoulmn'

IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14 
BEGIN
	SET @sql = @sql + ' AND (CFM.grp_id=@PageId or CFM.grp_id=1) and CFD.numRelation=@numRelation'
END
IF  @PageId= 4 
BEGIN
	SET @sql = @sql + ' AND CFM.grp_id=@PageId and CFD.numRelation=@numRelation'
END
ELSE
BEGIN
	SET @sql = @sql + ' AND CFM.grp_id=@PageId'
END
 
SET @sql = @sql + ' order by tintRow'

print @sql

EXECUTE sp_executeSQL @sql, N'@numUserCntId numeric(9),@intcoulmn numeric(9),@numDomainID numeric(9) ,                    
@PageId numeric(9),@numRelation numeric(9),@numFormID numeric(9),
@tintPageType TINYINT,@numTabID numeric(9)  ',@numUserCntId,@intcoulmn,@numDomainID,@PageId,@numRelation,@numFormID
,@tintPageType,@numTabID

end                        
                        
                          
if @intcoulmn = 0 --Available Fields
begin                          
 
SET @sql='select fld_label as vcfieldName ,fld_id as numFieldId,1 as bitCustomField
from CFW_Fld_Master CFM join CFw_Grp_Master CGM on CFM.subgrp=CGM.Grp_id '

IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14 or @PageId= 4
BEGIN
	SET @sql = @sql + ' join CFW_Fld_Dtl CFD on CFM.Fld_id=CFD.numFieldId '
END
  
SET @sql = @sql + ' where CGM.numDomainID=@numDomainID and CGM.Grp_id=@numTabID and CFM.numDomainID=@numDomainID  
AND fld_id not IN (SELECT numFieldId FROM DycFormConfigurationDetails where           
 numUserCntID=@numUserCntID and numDomainID=@numDomainID  
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType
AND numAuthGroupID=@numTabID)'

IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14 
BEGIN
	SET @sql = @sql + ' AND (CFM.grp_id=@PageId or CFM.grp_id=1) and CFD.numRelation=@numRelation'
END
IF  @PageId= 4 
BEGIN
	SET @sql = @sql + ' AND CFM.grp_id=@PageId and CFD.numRelation=@numRelation'
END
ELSE
BEGIN
	SET @sql = @sql + ' AND CFM.grp_id=@PageId'
END
 
SET @sql = @sql + ' order by subgrp'

print @sql
EXECUTE sp_executeSQL @sql, N'@numUserCntId numeric(9),@intcoulmn numeric(9),@numDomainID numeric(9) ,                    
@PageId numeric(9),@numRelation numeric(9),@numFormID numeric(9),
@tintPageType TINYINT,@numTabID numeric(9)  ',@numUserCntId,@intcoulmn,@numDomainID,@PageId,@numRelation,@numFormID
,@tintPageType,@numTabID

   
end
GO
