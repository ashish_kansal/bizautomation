GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetProductFilters' ) 
                    DROP PROCEDURE USP_GetProductFilters
Go

CREATE PROCEDURE [dbo].[USP_GetProductFilters]
    @numDomainId NUMERIC ,
    @numWareHouseID AS NUMERIC(9) = 0,
    @FilterQuery AS VARCHAR(2000) = '',
    @numCategoryID AS NUMERIC(18) = 0
AS 
    BEGIN  
   
        DECLARE @sqlQuery AS NVARCHAR(MAX) 
        DECLARE @sqlwhere AS NVARCHAR(MAX) 
        DECLARE @sqlFilter AS NVARCHAR(MAX)
        DECLARE @dynamicFilterQuery NVARCHAR(max) 
        DECLARE @where NVARCHAR(max) 
        DECLARE @data AS VARCHAR(100)
        DECLARE @row AS INTEGER        
        DECLARE @checkbox VARCHAR(2000)
        DECLARE @checkboxGroupBy VARCHAR(2000)
        DECLARE @dropdown VARCHAR(2000)
        DECLARE @dropdownGroupBy VARCHAR(2000)
        DECLARE @textbox VARCHAR(2000)
        DECLARE @textboxGroupBy VARCHAR(2000)
        DECLARE @filterNumItemIds VARCHAR(2000)
        DECLARE @priceStart VARCHAR(3000)
        DECLARE @priceInnerQuery VARCHAR(3000)
        DECLARE @priceEnd VARCHAR(3000)
        DECLARE @manufacturer VARCHAR(2000)
        DECLARE @manufacturerGroupBy VARCHAR(2000)
        DECLARE @attributes VARCHAR(2000)
        DECLARE @filterByNumItemCode VARCHAR(3000)
        DECLARE @count NUMERIC(9,0)
       
        BEGIN
            
            SET @where = ''
            SET @checkbox = ''
          
            
            IF @numWareHouseID = 0 
            BEGIN
                SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
                FROM    [eCommerceDTL]
                WHERE   [numDomainID] = @numDomainID	
            END

            IF LEN(@FilterQuery) > 0
				BEGIN 
				   
				    SELECT row_number() over(order by OutParam) as row, OutParam into #temp FROM dbo.SplitString(@FilterQuery,';')
				    
				    select top 1 @row = row ,@data =data from #temp 
				   
		            SET @dynamicFilterQuery = ''
		            SET @count = 0
		            WHILE @row>0
	                  BEGIN
	                     
	                     IF SUBSTRING(@data,1,1) = 'f'
	                        BEGIN	
	                            IF  CHARINDEX('-2',@data) >  0   
	                                BEGIN
	                                  SET @dynamicFilterQuery  =  ' SELECT   I.numItemCode 
                                                                    FROM 
                                                                         ( 
                                                                           ( SELECT              
                                                                                  numItemCode ,
                                                                                  CEILING
                                                                                  (
                                                                                     ISNULL
                                                                                     (
                                                                                       CASE WHEN I.[charItemType]=''P'' 
                                                                                            THEN 
                                                                                                 CASE WHEN I.bitSerialized = 1 
                                                                                                      THEN 
                                                                                                          (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
                                                                                                      ELSE 
                                                                                                          (SELECT TOP 1 (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * ISNULL([monWListPrice],0)) FROM [WareHouseItems] 
                                                                                                           WHERE [numItemID]= I.numItemCode 
                                                                                                           and  [numWareHouseID]=' + CONVERT(VARCHAR(10), @numWareHouseID)
                                                                                                      + ' ) 
                                                                                                  END
								                                                             ELSE 
								                                                                  (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
								                                                             END,0) 
								                                                    )  
                                                                             AS Price
                                                                             FROM 
                                                                                    dbo.ItemCategory IC 
                                                                             JOIN   Item I 
                                                                             ON     I.numItemCode = IC.numItemID
                                                                             WHERE
                                                                                       IC.numCategoryId = '+CONVERT(VARCHAR(10) , @numCategoryID)+'
                                                                                    AND I.numDomainId = '+CONVERT(VARCHAR(10) , @numDomainId)+'
                                                                          ))
                                                                    AS I 
                                                                    WHERE       I.Price 
                                                                    BETWEEN     SUBSTRING('''+@data+''',CHARINDEX(''='','''+@data+''')+1,(CHARINDEX(''~'','''+@data+''')) - (CHARINDEX(''='','''+@data+''')+1) )
                                                                    AND         SUBSTRING('''+@data+''',CHARINDEX(''~'','''+@data+''') + 1,(LEN('''+@data+'''))- (CHARINDEX(''~'','''+@data+''')))
    
    '
    PRINT(@dynamicFilterQuery)
	                                                                
	                                END 
                               ELSE IF CHARINDEX('-1',@data) > 0
                                    BEGIN
                              	     SET @dynamicFilterQuery  =   'SELECT      I.numItemCode 
                               	                                   FROM 
                               	                                   dbo.Item    I 
                               	                                   INNER JOIN  dbo.ItemCategory  IC
                                                                   ON          I.numItemCode = IC.numItemID 
                                     
                                                                   WHERE       IC.numCategoryID =' + CONVERT(VARCHAR(20), @numCategoryID)+ ' 
                                                                   AND         I.numDomainID    =' + CONVERT(VARCHAR(20), @numDomainID)+ '
                              	                                   AND         I.vcManufacturer = SUBSTRING('''+@data+''',CHARINDEX(''='','''+@data+''')+1,(LEN('''+@data+'''))-(CHARINDEX(''='','''+@data+''')))'
						            END
					        	ELSE   
						           BEGIN
						             SET @dynamicFilterQuery = ' SELECT        DISTINCT     
                                                                               I.numItemCode 
                                                                 FROM          dbo.Item I 
                                                                 INNER JOIN    dbo.ItemCategory IC 
                                                                 ON            IC.numItemID = i.numItemCode
                                                                 INNER JOIN    dbo.ItemGroupsDTL IGD 
                                                                 ON            I.numItemGroup = IGD.numItemGroupID
                                                                 INNER JOIN    dbo.CFW_Fld_Master CFM 
                                                                 ON                IGD.numOppAccAttrID = CFM.Fld_id 
                                                                               AND CFM.numDomainID= '+CONVERT(VARCHAR(10),@numDomainId) +'
                                                                 LEFT JOIN     dbo.ListDetails LD 
                                                                 ON                LD.numListID = CFM.numlistid 
                                                                               AND CFM.numDomainID = LD.numDomainID
                                                                 INNER JOIN    dbo.WareHouseItems WHI 
                                                                 ON            WHI.numItemID = I.numItemCode 
                                                                 INNER JOIN    dbo.CFW_Fld_Values_Serialized_Items CFVSI 
                                                                 ON            CFVSI.RecId = WHI.numWareHouseItemID
                   
                                                                 WHERE 
                                                                     IC.numCategoryID = '+CONVERT(VARCHAR(10),@numCategoryID)+' 
                                                                 AND I.numDomainID = '+CONVERT(VARCHAR(10),@numDomainId)+' 
                                                                 AND LD.numListItemID = CFVSI.Fld_Value 
                                                                 AND CFM.Fld_id  =  CFVSI.Fld_ID
                                                                 AND CFM.Fld_id = CONVERT(NUMERIC(9,0),  SUBSTRING('''+@data+''',CHARINDEX(''>'','''+@data+''')+1,CHARINDEX(''='','''+@data+''')-(CHARINDEX(''>'','''+@data+''')+1) )) 
                                                                 AND LD.numListItemID = CONVERT( VARCHAR(1000), SUBSTRING('''+@data+''',CHARINDEX(''='','''+@data+''')+1,LEN('''+@data+''')))  
                     '
                                                                       
						           END
	                         END
		                
		                    
		                 DELETE from #temp where row=@row
                   		 SELECT top 1 @row=row ,@data = data from #temp
                   		
                         IF @@rowCount=0
                           BEGIN
                                 SET @row=0
                                 IF @count = 0
                                   BEGIN 
                                      SET @where = @where + @dynamicFilterQuery 
                                      SET @count = @count + 1   
                                                                   
                                   END
                                ELSE
                                  BEGIN
                                  	   SET @where =  @dynamicFilterQuery + '  And I.numItemCode IN( ' + @where + ' ) '  
								  	   SET @count = @count + 1
								  END
                           END
                         ELSE 
                           BEGIN
                                IF @count = 0
                                   BEGIN 
                                      SET @where = @where + @dynamicFilterQuery 
                                      SET @count = @count + 1   
                                                                 
                                   END
                                ELSE
                                  BEGIN
                                   	   SET @where =  @dynamicFilterQuery + '  And I.numItemCode IN( ' + @where + ' ) '  
								  	   SET @count = @count + 1
								  END
                           END
                      END
                     
                      Drop table #temp
                      SET @sqlQuery  = 'SELECT numItemCode into #TempItemCode FROM (' + @where +') AS a'
                          
	           END
	           
	           ELSE
	           BEGIN
	               SET  @sqlQuery = '' 
               END
               
            SET @filterByNumItemCode = ' AND I.numItemCode IN ( SELECT * FROM #TempItemCode ) '
             
                      
            SET @sqlQuery = @sqlQuery +  ' SELECT      DISTINCT  
                                                       CFM.Fld_id AS FilterID,
                                                       REPLACE(REPLACE(CFM.Fld_label,'';'','' '') ,''~'','' '')   AS FilterName,
                                                       LD.vcdata AS FilterValue,
                                                       LD.numListItemID AS FilterValueID,
                                                       ''f~'' +CFM.Fld_label + ''~''+ CAST(CFM.Fld_id AS VARCHAR(10)) +''^'' +  LD.vcdata+''~''+ CAST(LD.numListItemID AS VARCHAR(10)) + '';'' AS FilterQuery ,
                                                       dbo.fn_GetItemCountBasedOnAttribute(CFM.Fld_id,LD.numListItemID,'+CONVERT(VARCHAR(10),@numCategoryID)+','+CONVERT(VARCHAR(10),@numDomainId)+')  ItemCount, LD.numListID
                                           FROM        dbo.Item I 
                                           INNER JOIN  dbo.ItemCategory IC 
                                           ON          IC.numItemID = I.numItemCode
                                           INNER JOIN  dbo.ItemGroupsDTL IGD 
                                           ON          I.numItemGroup = IGD.numItemGroupID
                                           INNER JOIN  dbo.CFW_Fld_Master CFM 
                                           ON              IGD.numOppAccAttrID = CFM.Fld_id 
                                                       AND CFM.numDomainID= @numDomainID
                                           LEFT JOIN   dbo.ListDetails LD 
                                           ON              LD.numListID = CFM.numlistid 
                                                       AND CFM.numDomainID = LD.numDomainID
                                           INNER JOIN  dbo.WareHouseItems WHI 
                                           ON          WHI.numItemID = I.numItemCode 
                                           INNER JOIN  dbo.CFW_Fld_Values_Serialized_Items CFVSI 
                                           ON          CFVSI.RecId = WHI.numWareHouseItemID
                   
                                           WHERE 
                                           
                                                          IC.numCategoryID = @numCategoryID 
                                                       AND i.numDomainID = @numDomainID
                                                       AND CONVERT(VARCHAR(1000) ,LD.numListItemID)  = CFVSI.Fld_Value 
                                                       AND  CFM.Fld_id  =  CFVSI.Fld_ID 
                                           
                                           GROUP BY    LD.vcdata  ,
                                                       CFM.Fld_id ,
                                                       CFM.Fld_label,
                                                       LD.numListItemID,
													   LD.numListID
                                         '    

        

	SET @manufacturer =  'UNION
	       
		 SELECT       DISTINCT 
		             -1 AS FilterID,
		             ''Manufacturer'' ,
		              vcManufacturer  FilterValue,
		              0 AS FilterValueID, 
		              ''f~Manufacturer~-1^''+vcManufacturer+''~-1''+'';'' AS FilterQuery,
		              Count(*) ItemCount, -1 AS numListID 
		 FROM 
		              Item I INNER JOIN dbo.ItemCategory IC ON I.numItemCode = IC.numItemID 
		 where 
		              numDomainID=@numDomainId
		              AND IC.numCategoryID = @numCategoryID 
		              AND LEN(ISNULL(vcManufacturer,''''))>1 ' 
	
	SET @manufacturerGroupBy =	'GROUP BY     I.vcManufacturer '
   
     SET @priceStart = '    UNION 
        
                    SELECT  
                      -2 AS FilterID,
                      ''Price'' AS FilterName,
                      CAST(MIN(FLOOR(a.Price)) AS VARCHAR(10))  AS FilterValueId,
                      CEILING(MAX(a.Price))  AS FilterVAlueID , 
                      ''f~Min~''+ CAST(MIN(FLOOR(a.Price)) AS VARCHAR(10))+''^Max~''+CAST(MAX(CEILING(a.Price)) AS VARCHAR(10))+'';'' AS FilterQuery ,
                      COUNT(price) AS ItemCount, -1 AS numListID 
                      FROM  ('
                               

       SET @priceInnerQuery ='SELECT              
                                     CEILING(
                                               ISNULL
                                               (
                                                   CASE WHEN I.[charItemType]=''P'' 
                                                   THEN 
                                                       CASE WHEN I.bitSerialized = 1 
                                                            THEN 
                                                                (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
                                                            ELSE 
                                                                (SELECT TOP 1 (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * ISNULL([monWListPrice],0)) FROM [WareHouseItems] 
                                                                 WHERE [numItemID]= I.numItemCode 
                                                                 and  [numWareHouseID]=' + CONVERT(VARCHAR(10), @numWareHouseID)
                                                         + ' ) 
                                                         END
								                   ELSE 
								                        (dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)) * monListPrice) 
								                   END,0) 
								                  
                                            )  
                              AS Price
                              FROM 
                                     dbo.ItemCategory IC JOIN Item I ON I.numItemCode = IC.numItemID
                              WHERE
                                         IC.numCategoryId = @numCategoryId
                                     AND I.numDomainId = @numDomainId 
                              '
                       
       SET @priceEnd =' )  AS a
                '
    
     
        IF LEN(@where) > 0 
           BEGIN 
           
              SET   @sqlQuery = @sqlQuery +@priceStart + @priceInnerQuery + @priceEnd +  @manufacturer + @filterByNumItemCode + @manufacturerGroupBy                   
           END
        ELSE
          BEGIN
              SET   @sqlQuery = @sqlQuery +@priceStart + @priceInnerQuery + @priceEnd +  @manufacturer + @manufacturerGroupBy              
          END   
        
         END
		 print @sqlQuery
         EXEC sp_executeSql @sqlQuery,
            N'@numDomainId numeric(9),@numCategoryID numeric(9)', @numDomainId,
            @numCategoryID
    END

--exec USP_GetProductFilters @numDomainId=1,@numCategoryID=1735,@FilterQuery='f>-2=335~700'
--exec USP_GetProductFilters @numDomainId=154,@numCategoryID=2297,@FilterQuery=NULL
