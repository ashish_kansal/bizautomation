GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_GetDateFormat')
DROP PROCEDURE USP_Domain_GetDateFormat
GO
Create PROCEDURE [dbo].[USP_Domain_GetDateFormat]
	-- Add the parameters for the stored procedure here
	@numDomainId numeric(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	

	Select vcDateFormat,tintDecimalPoints from Domain where numDomainId=@numDomainId





END

GO


