GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPI_GetSecretKey' ) 
    DROP PROCEDURE USP_BizAPI_GetSecretKey
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 15 April 2014
-- Description:	Gets domain user based on api key
-- =============================================
CREATE PROCEDURE USP_BizAPI_GetSecretKey
	@BizAPIPublicKey VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

                                                                
 SELECT TOP 1
        U.numUserID ,
        numUserDetailId ,
        ISNULL(vcEmailID, '') vcEmailID ,
        ISNULL(numGroupID, 0) numGroupID ,
        bitActivateFlag ,
        ISNULL(vcMailNickName, '') vcMailNickName ,
        ISNULL(txtSignature, '') txtSignature ,
        ISNULL(U.numDomainID, 0) numDomainID ,
        ISNULL(Div.numDivisionID, 0) numDivisionID ,
        ISNULL(vcCompanyName, '') vcCompanyName ,
        ISNULL(E.bitExchangeIntegration, 0) bitExchangeIntegration ,
        ISNULL(E.bitAccessExchange, 0) bitAccessExchange ,
        ISNULL(E.vcExchPath, '') vcExchPath ,
        ISNULL(E.vcExchDomain, '') vcExchDomain ,
        ISNULL(D.vcExchUserName, '') vcExchUserName ,
        ISNULL(vcFirstname, '') + ' ' + ISNULL(vcLastName, '') AS ContactName ,
        CASE WHEN E.bitAccessExchange = 0 THEN E.vcExchPassword
             WHEN E.bitAccessExchange = 1 THEN D.vcExchPassword
        END AS vcExchPassword ,
        tintCustomPagingRows ,
        vcDateFormat ,
        numDefCountry ,
        tintComposeWindow ,
        DATEADD(day, -sintStartDate, GETUTCDATE()) AS StartDate ,
        DATEADD(day, sintNoofDaysInterval,
                DATEADD(day, -sintStartDate, GETUTCDATE())) AS EndDate ,
        tintAssignToCriteria ,
        bitIntmedPage ,
        tintFiscalStartMonth ,
        numAdminID ,
        ISNULL(A.numTeam, 0) numTeam ,
        ISNULL(D.vcCurrency, '') AS vcCurrency ,
        ISNULL(D.numCurrencyID, 0) AS numCurrencyID ,
        ISNULL(D.bitMultiCurrency, 0) AS bitMultiCurrency ,
        bitTrial ,
        ISNULL(tintChrForComSearch, 0) AS tintChrForComSearch ,
        ISNULL(tintChrForItemSearch, 0) AS tintChrForItemSearch ,
        CASE WHEN bitSMTPServer = 1 THEN vcSMTPServer
             ELSE ''
        END AS vcSMTPServer ,
        CASE WHEN bitSMTPServer = 1 THEN ISNULL(numSMTPPort, 0)
             ELSE 0
        END AS numSMTPPort ,
        ISNULL(bitSMTPAuth, 0) bitSMTPAuth ,
        ISNULL([vcSmtpPassword], '') vcSmtpPassword ,
        ISNULL(bitSMTPSSL, 0) bitSMTPSSL ,
        ISNULL(bitSMTPServer, 0) bitSMTPServer ,
        ISNULL(IM.bitImap, 0) bitImapIntegration ,
        ISNULL(charUnitSystem, 'E') UnitSystem ,
        DATEDIFF(day, GETUTCDATE(), dtSubEndDate) AS NoOfDaysLeft ,
        ISNULL(bitCreateInvoice, 0) bitCreateInvoice ,
        ISNULL([numDefaultSalesBizDocId], 0) numDefaultSalesBizDocId ,
        ISNULL([numDefaultPurchaseBizDocId], 0) numDefaultPurchaseBizDocId ,
        vcDomainName ,
        S.numSubscriberID ,
        D.[tintBaseTax] ,
        ISNULL(D.[numShipCompany], 0) numShipCompany ,
        ISNULL(D.[bitEmbeddedCost], 0) bitEmbeddedCost ,
        ( SELECT    ISNULL(numAuthoritativeSales, 0)
          FROM      AuthoritativeBizDocs
          WHERE     numDomainId = D.numDomainID
        ) numAuthoritativeSales ,
        ( SELECT    ISNULL(numAuthoritativePurchase, 0)
          FROM      AuthoritativeBizDocs
          WHERE     numDomainId = D.numDomainID
        ) numAuthoritativePurchase ,
        ISNULL(D.[numDefaultSalesOppBizDocId], 0) numDefaultSalesOppBizDocId ,
        ISNULL(tintDecimalPoints, 2) tintDecimalPoints ,
        ISNULL(D.tintBillToForPO, 0) tintBillToForPO ,
        ISNULL(D.tintShipToForPO, 0) tintShipToForPO ,
        ISNULL(D.tintOrganizationSearchCriteria, 0) tintOrganizationSearchCriteria ,
        ISNULL(D.tintLogin, 0) tintLogin ,
        ISNULL(bitDocumentRepositary, 0) bitDocumentRepositary ,
        ISNULL(D.tintComAppliesTo, 0) tintComAppliesTo ,
        ISNULL(D.vcPortalName, '') vcPortalName ,
        ( SELECT    COUNT(*)
          FROM      dbo.Subscribers
          WHERE     GETUTCDATE() BETWEEN dtSubStartDate
                                 AND     dtSubEndDate
                    AND bitActive = 1
        ) ,
        ISNULL(D.bitGtoBContact, 0) bitGtoBContact ,
        ISNULL(D.bitBtoGContact, 0) bitBtoGContact ,
        ISNULL(D.bitGtoBCalendar, 0) bitGtoBCalendar ,
        ISNULL(D.bitBtoGCalendar, 0) bitBtoGCalendar ,
        ISNULL(bitExpenseNonInventoryItem, 0) bitExpenseNonInventoryItem ,
        ISNULL(D.bitInlineEdit, 0) bitInlineEdit ,
        ISNULL(U.numDefaultClass, 0) numDefaultClass ,
        ISNULL(D.bitRemoveVendorPOValidation, 0) bitRemoveVendorPOValidation ,
        ISNULL(D.bitTrakDirtyForm, 1) bitTrakDirtyForm ,
        ISNULL(D.tintBaseTaxOnArea, 0) tintBaseTaxOnArea ,
        ISNULL(D.bitAmountPastDue, 0) bitAmountPastDue ,
        CONVERT(DECIMAL(18, 2), ISNULL(D.monAmountPastDue, 0)) monAmountPastDue ,
        ISNULL(D.bitSearchOrderCustomerHistory, 0) bitSearchOrderCustomerHistory ,
        ISNULL(U.tintTabEvent, 0) AS tintTabEvent ,
        ISNULL(D.bitRentalItem, 0) AS bitRentalItem ,
        ISNULL(D.numRentalItemClass, 0) AS numRentalItemClass ,
        ISNULL(D.numRentalHourlyUOM, 0) AS numRentalHourlyUOM ,
        ISNULL(D.numRentalDailyUOM, 0) AS numRentalDailyUOM ,
        ISNULL(D.tintRentalPriceBasedOn, 0) AS tintRentalPriceBasedOn ,
        ISNULL(D.tintCalendarTimeFormat, 24) AS tintCalendarTimeFormat ,
        ISNULL(D.tintDefaultClassType, 0) AS tintDefaultClassType ,
        ISNULL(U.numDefaultWarehouse, 0) numDefaultWarehouse ,
        ISNULL(D.tintPriceBookDiscount, 0) tintPriceBookDiscount ,
        ISNULL(D.bitAutoSerialNoAssign, 0) bitAutoSerialNoAssign ,
        ISNULL(IsEnableProjectTracking, 0) AS [IsEnableProjectTracking] ,
        ISNULL(IsEnableCampaignTracking, 0) AS [IsEnableCampaignTracking] ,
        ISNULL(IsEnableResourceScheduling, 0) AS [IsEnableResourceScheduling] ,
        ISNULL(D.numSOBizDocStatus, 0) AS numSOBizDocStatus ,
        ISNULL(D.bitAutolinkUnappliedPayment, 0) AS bitAutolinkUnappliedPayment ,
        ISNULL(numShippingServiceItemID, 0) AS [numShippingServiceItemID] ,
        ISNULL(D.numDiscountServiceItemID, 0) AS numDiscountServiceItemID ,
        ISNULL(D.tintCommissionType, 1) AS tintCommissionType ,
        ISNULL(D.bitDiscountOnUnitPrice, 0) AS bitDiscountOnUnitPrice ,
        ISNULL(D.intShippingImageWidth, 0) AS intShippingImageWidth ,
        ISNULL(D.intShippingImageHeight, 0) AS intShippingImageHeight ,
        ISNULL(D.numTotalInsuredValue, 0) AS numTotalInsuredValue ,
        ISNULL(D.numTotalCustomsValue, 0) AS numTotalCustomsValue ,
        ISNULL(D.bitUseBizdocAmount, 1) AS [bitUseBizdocAmount] ,
        ISNULL(D.bitDefaultRateType, 1) AS [bitDefaultRateType] ,
        ISNULL(D.bitIncludeTaxAndShippingInCommission, 1) AS [bitIncludeTaxAndShippingInCommission],
        U.vcBizAPISecretKey
 FROM   UserMaster U
        LEFT JOIN ExchangeUserDetails E ON E.numUserID = U.numUserID
        LEFT JOIN ImapUserDetails IM ON IM.numUserCntID = U.numUserDetailID
        JOIN Domain D ON D.numDomainID = U.numDomainID
        LEFT JOIN DivisionMaster Div ON D.numDivisionID = Div.numDivisionID
        LEFT JOIN CompanyInfo C ON C.numCompanyID = Div.numDivisionID
        LEFT JOIN AdditionalContactsInformation A ON A.numContactID = U.numUserDetailId
        JOIN Subscribers S ON S.numTargetDomainID = D.numDomainID
 WHERE  vcBizAPIPublicKey = @BizAPIPublicKey COLLATE Latin1_General_CS_AS
        AND bitActive IN ( 1, 2 )
        AND U.bitBizAPIAccessEnabled = 1
        AND GETUTCDATE() BETWEEN dtSubStartDate
                         AND     dtSubEndDate
 
END
GO