GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CheckInventoryForLandedCost' ) 
    DROP PROCEDURE USP_CheckInventoryForLandedCost
GO
CREATE PROCEDURE [dbo].[USP_CheckInventoryForLandedCost]
	@numDomainID NUMERIC(18,0),
	@numOppID NUMERIC(18,0)
AS
BEGIN
	IF (SELECT COUNT(*) FROM OpportunityMaster WHERE numOppId=@numOppID) > 0
	BEGIN
		IF
		(
			SELECT
				COUNT(*)
			FROM
				OpportunityItems
			INNER JOIN
				Item 
			ON
				OpportunityItems.numItemCode = Item.numItemCode
			OUTER APPLY
			(
				SELECT
					ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) AS Qty
				FROM
					WarehouseItems
				WHERE 
					numItemID = Item.numItemCode
			) AS TotalOnHand
			WHERE
				numOppId = @numOppID
				AND ISNULL(numWarehouseItmsID,0) > 0
				AND TotalOnHand.Qty < OpportunityItems.numUnitHour
		) > 0
		BEGIN
			SELECT 0
		END
		ELSE
		BEGIN
			SELECT 1
		END
	END
	ELSE
	BEGIN
		SELECT 0
	END
END