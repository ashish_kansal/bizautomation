
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSiteMenu]    Script Date: 08/08/2009 16:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSiteMenu')
DROP PROCEDURE USP_GetSiteMenu
GO
CREATE PROCEDURE [dbo].[USP_GetSiteMenu]
          @numMenuID NUMERIC(9),
          @numSiteID NUMERIC(9)
AS
  SELECT numMenuID,
         vcTitle,
         CASE 
           WHEN numPageID > 0 THEN (SELECT vcPageURL
                                    FROM   [SitePages]
                                    WHERE  numPageID = SM.numPageID)
           ELSE vcNavigationURL
         END AS vcNavigationURL,
         ISNULL(numPageID,0) AS numPageID,
         tintLevel,
         numParentID,
         numSiteID,
         numDomainID,
         bitStatus,
         intDisplayOrder
  FROM   SiteMenu SM
  WHERE  (numMenuID = @numMenuID
           OR @numMenuID = 0)
         AND numSiteID = @numSiteID
  ORDER BY SM.[intDisplayOrder] Asc

