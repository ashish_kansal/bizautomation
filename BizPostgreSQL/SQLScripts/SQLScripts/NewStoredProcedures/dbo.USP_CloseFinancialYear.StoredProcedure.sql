GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CloseFinancialYear')
DROP PROCEDURE USP_CloseFinancialYear
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE PROCEDURE [dbo].[USP_CloseFinancialYear]
(@numDomainID NUMERIC(9),
@numFinYearId NUMERIC(9), --- ACTIVE FINCIALYEAR 
@numNextFinYearId NUMERIC(9), --- Selected FIN year
@tintCloseMode TINYINT) --- Trial Close or Audit Close (1=Tral close , 2 =Audit Close)

AS

BEGIN


DECLARE @dtFromDate datetime;
DECLARE @dtToYear datetime;
DECLARE @dtOpening DATETIME;

set @dtFromDate = (select dtPeriodFrom from Financialyear where numFinYearID= @numFinYearId and  numDomainID=@numDomainId );
set @dtToYear = (select dtPeriodTo from Financialyear where numFinYearID= @numFinYearId and  numDomainID=@numDomainId );
set @dtOpening = (select dtPeriodFrom from Financialyear where numFinYearID= @numNextFinYearId and  numDomainID=@numDomainId );


DELETE FROM ChartAccountOpening WHERE [numFinYearId]=@numNextFinYearId AND numDomainID=@numDomainID;

--Insert New Opening balance for given new financial year for all Asset,liability and Equity Accounts
-- New Opening Balance = (Sum(Debit) - Sum(Credit)) of ledger + Opening Balance of CurrentFinancial Year
INSERT INTO ChartAccountOpening

SELECT DISTINCT @numDomainId,COA.numAccountId,@numNextFinYearId, 
ISNULL((SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
FROM
General_Journal_Details GJD INNER JOIN 
General_Journal_Header GJH ON GJD.numJournalId =GJH.numJournal_Id AND 
GJD.numDomainId=COA.numDomainId AND 
GJD.numChartAcntId=COA.numAccountId AND
GJH.datEntry_Date between @dtFromDate and @dtToYear),0)/*+
ISNULL((SELECT monOpening FROM ChartAccountOpening CAO WHERE 
CAO.numAccountId=COA.numAccountId AND numFinYearId=@numFinYearId),0)*/,@dtOpening
 FROM Chart_of_Accounts COA
WHERE COA.numDomainId=@numDomainId AND
	( COA.vcAccountCode LIKE '0101%' OR COA.vcAccountCode LIKE '0102%' OR COA.vcAccountCode LIKE '0105%');

----------------------------
--UPDATE PL account with Profit for Active Finacial Year
----------------------------
DECLARE @PROFIT DECIMAL(20,5);
SET @PROFIT=0;

SELECT @PROFIT= ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
FROM
General_Journal_Details GJD INNER JOIN 
General_Journal_Header GJH ON GJD.numJournalId =GJH.numJournal_Id INNER JOIN 
Chart_of_Accounts COA ON GJD.numChartAcntId=COA.numAccountId AND 
COA.numDomainID =@numDomainID AND
GJD.numDomainId=COA.numDomainId AND 
GJD.numChartAcntId=COA.numAccountId AND
GJH.datEntry_Date between @dtFromDate and @dtToYear AND 
(COA.vcAccountCode like '0103%' OR COA.vcAccountCode like '0104%' OR COA.vcAccountCode like '0106%');
--set @PROFIT=@PROFIT*(-1) -- chintan patel said that store profit and loss as it is.

UPDATE ChartAccountOpening  set monOpening= ISNULL(monOpening,0) + isnull(@PROFIT,0)
WHERE numAccountId = ( SELECT TOP 1 numAccountId from Chart_of_Accounts where numDomainID=@numDomainId and bitProfitLoss=1) 
and numDomainId=@numDomainId and numFinYearId= @numNextFinYearId;

IF @tintCloseMode=1 
	BEGIN
		UPDATE Financialyear SET bitCloseStatus=1 where numFinYearID= @numFinYearId and numDomainID=@numDomainId;
	END
	ELSE
	BEGIN
		UPDATE Financialyear SET bitAuditStatus=1 where numFinYearID= @numFinYearId and  numDomainID=@numDomainId ;
	END
	
END
--SELECT  DISTINCT CAO.numAccountId,COA.vcAccountName ,monOpening,dtOpenDate FROM ChartAccountOpening CAO INNER JOIN dbo.Chart_Of_Accounts COA ON CAO.numAccountId = COA.numAccountId WHERE CAO.numDomainId=@numDomainID AND numFinYearId=@numNextFinYearId
--select * from Financialyear
