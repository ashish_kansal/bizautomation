GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetKitAssemblyCalculatedPrice')
DROP PROCEDURE USP_Item_GetKitAssemblyCalculatedPrice
GO
CREATE PROCEDURE [dbo].[USP_Item_GetKitAssemblyCalculatedPrice]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numQty FLOAT,
	@tintKitAssemblyPriceBasedOn TINYINT
AS
BEGIN
	SELECT
		bitSuccess
		,monPrice
	FROM
		dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQty,0,@tintKitAssemblyPriceBasedOn,0,0,'',0,1)
END
GO