/****** Object:  StoredProcedure [dbo].[USP_OppShippingorReceiving]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GlobalLocationFulfilled')
DROP PROCEDURE USP_OpportunityMaster_GlobalLocationFulfilled
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GlobalLocationFulfilled]
@numDomainID NUMERIC(18,0),
@numOppID AS NUMERIC(18,0)
AS
BEGIN
	IF (SELECT 
			COUNT(*) 
		FROM 
			OpportunityMaster OM 
		INNER JOIN 
			OpportunityItems OI 
		ON 
			OM.numOppId=OI.numOppId 
		INNER JOIN 
			WareHouseItems WI 
		ON 
			OI.numWarehouseItmsID=WI.numWareHouseItemID 
		WHERE 
			OM.numDomainId=@numDomainID 
			AND OM.numOppId=@numOppID
			AND WI.numWLocationID = -1
			AND OI.numUnitHour <> ISNULL(OI.numUnitHourReceived,0)) > 0
	BEGIN
		SELECT 0
	END
	ELSE
	BEGIN
		SELECT 1
	END
END