
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSiteTemplates]    Script Date: 08/08/2009 16:07:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSiteTemplates')
DROP PROCEDURE USP_GetSiteTemplates
GO
CREATE PROCEDURE [dbo].[USP_GetSiteTemplates]
          @numTemplateID NUMERIC(9)  = 0,
          @numSiteID     NUMERIC(9),
          @numDomainID   NUMERIC(9)
AS
  SELECT [numTemplateID],
         [vcTemplateName],
         '{template:' + REPLACE(LOWER([vcTemplateName]),' ','_') + '}' vcTemplateCode,
         ISNULL([txtTemplateHTML],'') txtTemplateHTML,
         [numSiteID]
  FROM   SiteTemplates
  WHERE  ([numTemplateID] = @numTemplateID
           OR @numTemplateID = 0)
         AND numSiteID = @numSiteID
         AND numDomainID = @numDomainID

