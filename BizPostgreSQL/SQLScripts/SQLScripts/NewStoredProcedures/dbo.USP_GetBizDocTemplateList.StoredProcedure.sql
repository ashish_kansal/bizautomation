GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocTemplateList')
DROP PROCEDURE USP_GetBizDocTemplateList
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocTemplateList]
	@numDomainID NUMERIC,
	@numBizDocID NUMERIC,
	@numOppType numeric=0,
	@byteMode as tinyint,
	@numRelationship AS NUMERIC(18,0) = 0,
	@numProfile AS NUMERIC(18,0) = 0,
	@numAccountClass NUMERIC(18,0) = 0
AS
SET NOCOUNT ON

IF @byteMode=0
BEGIN

DECLARE @numAuthoritativePurchase AS NUMERIC(18,0) = 0
DECLARE @numAuthoritativeSales AS NUMERIC(18,0) = 0

SELECT 
	@numAuthoritativePurchase=numAuthoritativePurchase,
	@numAuthoritativeSales=numAuthoritativeSales 
FROM 
	AuthoritativeBizDocs 
WHERE 
	numDomainId=@numDomainID

SELECT 
	numDomainID, 
	numBizDocID,
	numBizDocTempID,
	vcTemplateName,
	ISNULL(bitEnabled,0) as bitEnabled,
	ISNULL(bitDefault,0) as bitDefault,
	numOppType,
	numRelationship,
	numProfile,
	dbo.fn_GetListName(numRelationship,0) Relationship,
	dbo.fn_GetListName(numProfile,0) [Profile],
	(CASE WHEN [numOppType]=1 THEN 'Sales' ELSE 'Purchase' END) AS vcOppType,
	dbo.fn_GetListItemName(numBizDocID) +  (CASE 
												WHEN numOppType=1 
												THEN (CASE WHEN ISNULL(@numAuthoritativeSales,0)=numBizDocId THEN ' - Authoritative' ELSE '' END) 
												WHEN numOppType=2 
												THEN (CASE WHEN ISNULL(@numAuthoritativePurchase,0)=numBizDocId THEN ' - Authoritative' ELSE '' END)  
											END) AS vcBizDocType
FROM 
	BizDocTemplate
WHERE 
	[numDomainID] = @numDomainID 
	AND tintTemplateType=0 
	AND	(numBizDocID=@numBizDocID OR @numBizDocID=0) 
	AND (numOppType=@numOppType OR @numOppType=0)

END

ELSE IF @byteMode=1
BEGIN
	DECLARE @numBizDocTempID AS NUMERIC(18) = 0

	If ISNULL(@numRelationship,0) > 0 OR ISNULL(@numProfile,0) > 0 OR ISNULL(@numAccountClass,0) > 0
	BEGIN
		SELECT
			@numBizDocTempID = numBizDocTempID
		FROM
			BizDocTemplate
		WHERE 
			[numDomainID] = @numDomainID 
			AND tintTemplateType=0 
			AND numBizDocID=@numBizDocID 
			AND numOppType=@numOppType 
			AND ISNULL(bitEnabled,0)=1
			AND 1 = CASE 
						WHEN ISNULL(numRelationship,0) > 0 AND ISNULL(numProfile,0) = 0 AND ISNULL(numAccountClass,0) = 0
						THEN 
							(CASE WHEN numRelationship = @numRelationship THEN 1 ELSE 0 END)
						WHEN ISNULL(numRelationship,0) > 0 AND ISNULL(numProfile,0) > 0 AND ISNULL(numAccountClass,0) = 0
						THEN
							(CASE WHEN numRelationship = @numRelationship AND numProfile=@numProfile THEN 1 ELSE 0 END)
						WHEN ISNULL(numRelationship,0) > 0 AND ISNULL(numProfile,0) = 0 AND ISNULL(numAccountClass,0) > 0
						THEN	
							(CASE WHEN numRelationship = @numRelationship AND numAccountClass=@numAccountClass THEN 1 ELSE 0 END)
						WHEN ISNULL(numRelationship,0) = 0 AND ISNULL(numProfile,0) > 0 AND ISNULL(numAccountClass,0) = 0
						THEN
							(CASE WHEN numProfile = @numProfile THEN 1 ELSE 0 END)
						WHEN ISNULL(numRelationship,0) = 0 AND ISNULL(numProfile,0) > 0 AND ISNULL(numAccountClass,0) > 0
						THEN
							(CASE WHEN numProfile = @numProfile AND numAccountClass=@numAccountClass THEN 1 ELSE 0 END)
						WHEN ISNULL(numRelationship,0) = 0 AND ISNULL(numProfile,0) = 0 AND ISNULL(numAccountClass,0) > 0
						THEN
							(CASE WHEN numAccountClass = @numAccountClass THEN 1 ELSE 0 END)
						WHEN ISNULL(numRelationship,0) > 0 AND ISNULL(numProfile,0) > 0 AND ISNULL(numAccountClass,0) > 0
						THEN
							(CASE WHEN numRelationship = @numRelationship AND numProfile=@numProfile AND numAccountClass=@numAccountClass THEN 1 ELSE 0 END)
						ELSE	
							0
					END

	END

	IF ISNULL(@numBizDocTempID,0) = 0
	BEGIN
		SELECT 
			@numBizDocTempID=numBizDocTempID
		FROM 
			BizDocTemplate
		WHERE 
			[numDomainID] = @numDomainID 
			AND tintTemplateType=0 
			AND	numBizDocID=@numBizDocID 
			AND numOppType=@numOppType 
			AND isnull(bitDefault,0)=1 
			AND isnull(bitEnabled,0)=1
	END

	SELECT @numBizDocTempID as numBizDocTempID
END