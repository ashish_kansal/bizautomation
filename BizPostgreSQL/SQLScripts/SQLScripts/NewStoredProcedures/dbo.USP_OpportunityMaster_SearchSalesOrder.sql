GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_SearchSalesOrder')
DROP PROCEDURE USP_OpportunityMaster_SearchSalesOrder
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_SearchSalesOrder]
	 @numDomainID AS NUMERIC(18,0),
	 @vcOppName VARCHAR(300)
AS
BEGIN
	SELECT
		numOppId
		,vcPOppName
	FROM
		OpportunityMaster
	WHERE
		numDomainId=@numDomainID
		AND tintOppType=1
		AND tintOppStatus=1
		AND vcPOppName LIKE CONCAT('%',@vcOppName,'%')
END
GO