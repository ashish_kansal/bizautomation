SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageFormFieldValidation')
DROP PROCEDURE USP_ManageFormFieldValidation
GO
CREATE PROCEDURE [dbo].[USP_ManageFormFieldValidation] 
( 
 @numDomainID NUMERIC(9),
 @numFormId NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @strFieldList AS TEXT = '')
AS 
DECLARE @hDoc INT
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList

if exists(select * from DynamicFormField_Validation where numDomainID=@numDomainID and numFormId=@numFormId and numFormFieldId=@numFormFieldId)
BEGIN
	
UPDATE  DynamicFormField_Validation
SET     bitIsRequired = X.bitIsRequired,
        bitIsNumeric = X.bitIsNumeric,
        bitIsAlphaNumeric = X.bitIsAlphaNumeric,
        bitIsEmail = X.bitIsEmail,
        bitIsLengthValidation = X.bitIsLengthValidation,
        intMaxLength = X.intMaxLength,
        intMinLength = X.intMinLength,
        bitFieldMessage = X.bitFieldMessage,
        vcFieldMessage = X.vcFieldMessage,
        vcToolTip=X.vcToolTip
FROM    ( SELECT    *
          FROM      OPENXML (@hDoc, '/NewDataSet/Table',2) WITH (bitIsRequired TINYINT, bitIsNumeric BIT, bitIsAlphaNumeric BIT, bitIsEmail BIT, bitIsLengthValidation BIT, intMaxLength INT, intMinLength INT, bitFieldMessage BIT, vcFieldMessage VARCHAR(500),vcToolTip VARCHAR(1000) )
        ) X
WHERE   numDomainID=@numDomainID and numFormId=@numFormId and numFormFieldId=@numFormFieldId
END 
ELSE 
BEGIN
	INSERT INTO DynamicFormField_Validation (
		numFormId,numFormFieldId,numDomainId,
		bitIsRequired,
		bitIsNumeric,
		bitIsAlphaNumeric,
		bitIsEmail,
		bitIsLengthValidation,
		intMaxLength,
		intMinLength,
		bitFieldMessage,
		vcFieldMessage,vcToolTip
	) 
	SELECT 
		@numFormId,@numFormFieldId,@numDomainId,
		X.bitIsRequired,
		X.bitIsNumeric,
		X.bitIsAlphaNumeric,
		X.bitIsEmail,
		X.bitIsLengthValidation,
		X.intMaxLength,
		X.intMinLength,
		X.bitFieldMessage,
		X.vcFieldMessage,X.vcToolTip
		FROM 	( SELECT    *
          FROM      OPENXML (@hDoc, '/NewDataSet/Table',2) WITH (bitIsRequired TINYINT, bitIsNumeric BIT, bitIsAlphaNumeric BIT, bitIsEmail BIT, bitIsLengthValidation BIT, intMaxLength INT, intMinLength INT, bitFieldMessage BIT, vcFieldMessage VARCHAR(500),vcToolTip VARCHAR(1000) )
        ) X

END
EXEC sp_xml_removedocument @hDoc

GO