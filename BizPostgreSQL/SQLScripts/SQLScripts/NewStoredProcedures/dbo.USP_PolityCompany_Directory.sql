GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PolityCompany_Directory')
DROP PROCEDURE dbo.USP_PolityCompany_Directory
GO
CREATE PROCEDURE [dbo].[USP_PolityCompany_Directory]
(
	@numRecordIndex INT
	,@numPageSize INT
	,@vcSortColumn VARCHAR(5)
	,@vcSortDirection VARCHAR(4)
	,@vcSearchText VARCHAR(500)
	,@vcPrimaryBusinessIds VARCHAR(1000)
	,@vcSubCategoryIds VARCHAR(1000)
	,@vcBusinessStructureIds VARCHAR(1000)
	,@vcHeadquarterIds VARCHAR(1000)
	,@vcPoliticalScoreIds VARCHAR(1000)
)
AS 
BEGIN
	SELECT 
		COUNT(*) OVER() AS TotalReords
		,(CASE WHEN ISNULL(CFVLinkToSite.Fld_Value,0)=1 AND ISNULL(CI.vcWebSite,'') <> 'http://' THEN CONCAT('<a href="',ISNULL(CI.vcWebSite,''),'" target="_blank">',ISNULL(CI.vcCompanyName,''),'</a>') ELSE ISNULL(CI.vcCompanyName,'') END) vcCompanyName
		,ISNULL(CI.txtComments,'') txtComments
		,ISNULL(CI.vcWebSite,'') vcWebSite
		,(CASE WHEN CFVPoliticalScore.Fld_Value = 87653 THEN CONCAT('<b style="color:green">',ISNULL(LD.vcData,''),'</b>') ELSE CONCAT('<b style="color:red">',ISNULL(LD.vcData,''),'</b>') END) vcPoliticalScore
		,CONCAT((CASE WHEN ISNULL(CFVEvidence1.Fld_Value,'') <> '' THEN CONCAT('<a href="',ISNULL(CFVEvidence1.Fld_Value,''),'" target="_blank">(1)</a>') ELSE '' END)
		,(CASE WHEN ISNULL(CFVEvidence2.Fld_Value,'') <> '' THEN CONCAT(' <a href="',ISNULL(CFVEvidence2.Fld_Value,''),'" target="_blank">(2)</a>') ELSE '' END)
		,(CASE WHEN ISNULL(CFVEvidence3.Fld_Value,'') <> '' THEN CONCAT(' <a href="',ISNULL(CFVEvidence3.Fld_Value,''),'" target="_blank">(3)</a>') ELSE '' END)
		,(CASE WHEN ISNULL(CFVEvidence4.Fld_Value,'') <> '' THEN CONCAT(' <a href="',ISNULL(CFVEvidence4.Fld_Value,''),'" target="_blank">(4)</a>') ELSE '' END)
		,(CASE WHEN ISNULL(CFVEvidence5.Fld_Value,'') <> '' THEN CONCAT(' <a href="',ISNULL(CFVEvidence5.Fld_Value,''),'" target="_blank">(5)</a>') ELSE '' END)) vcEvidence
	FROM 
		CompanyInfo CI
	INNER JOIN
		DivisionMaster DM
	ON
		CI.numCompanyId = DM.numCompanyID
	LEFT JOIN
		CFW_FLD_Values CFVApproved 
	ON
		CFVApproved.Fld_ID=13479
		AND CFVApproved.RecId = DM.numDivisionID
	LEFT JOIN
		CFW_FLD_Values CFVPrimaryBusiness 
	ON
		CFVPrimaryBusiness.Fld_ID=13465
		AND CFVPrimaryBusiness.RecId = DM.numDivisionID
	LEFT JOIN
		CFW_FLD_Values CFVSubCategory
	ON
		CFVSubCategory.Fld_ID=13466
		AND CFVSubCategory.RecId = DM.numDivisionID
	LEFT JOIN
		CFW_FLD_Values CFVBusinessStructure
	ON
		CFVBusinessStructure.Fld_ID=13468
		AND CFVBusinessStructure.RecId = DM.numDivisionID
	LEFT JOIN
		CFW_FLD_Values CFVHeadquarter
	ON
		CFVHeadquarter.Fld_ID=13469
		AND CFVHeadquarter.RecId = DM.numDivisionID
	LEFT JOIN
		CFW_FLD_Values CFVPoliticalScore
	ON
		CFVPoliticalScore.Fld_ID=13467
		AND CFVPoliticalScore.RecId = DM.numDivisionID
	LEFT JOIN
		ListDetails LD
	ON
		CFVPoliticalScore.Fld_Value = LD.numListItemID
	LEFT JOIN
		CFW_FLD_Values CFVEvidence1
	ON
		CFVEvidence1.Fld_ID=13474
		AND CFVEvidence1.RecId = DM.numDivisionID
	LEFT JOIN
		CFW_FLD_Values CFVEvidence2
	ON
		CFVEvidence2.Fld_ID=13475
		AND CFVEvidence2.RecId = DM.numDivisionID
	LEFT JOIN
		CFW_FLD_Values CFVEvidence3
	ON
		CFVEvidence3.Fld_ID=13476
		AND CFVEvidence3.RecId = DM.numDivisionID
	LEFT JOIN
		CFW_FLD_Values CFVEvidence4
	ON
		CFVEvidence4.Fld_ID=13477
		AND CFVEvidence4.RecId = DM.numDivisionID
	LEFT JOIN
		CFW_FLD_Values CFVEvidence5
	ON
		CFVEvidence5.Fld_ID=13478
		AND CFVEvidence5.RecId = DM.numDivisionID
	LEFT JOIN
		CFW_FLD_Values CFVLinkToSite
	ON
		CFVLinkToSite.Fld_ID=13471
		AND CFVLinkToSite.RecId = DM.numDivisionID
	WHERE 
		CI.numDomainID=167
		AND CI.numCompanyType= 47
		AND ISNULL(CFVApproved.Fld_Value,0) = 1
		AND (ISNULL(@vcPrimaryBusinessIds,'') = '' OR CFVPrimaryBusiness.Fld_Value IN (SELECT Id FROM dbo.SplitIDs(@vcPrimaryBusinessIds,',')))
		AND (ISNULL(@vcSubCategoryIds,'') = '' OR CFVSubCategory.Fld_Value IN (SELECT Id FROM dbo.SplitIDs(@vcSubCategoryIds,',')))
		AND (ISNULL(@vcBusinessStructureIds,'') = '' OR CFVBusinessStructure.Fld_Value IN (SELECT Id FROM dbo.SplitIDs(@vcBusinessStructureIds,',')))
		AND (ISNULL(@vcHeadquarterIds,'') = '' OR CFVHeadquarter.Fld_Value IN (SELECT Id FROM dbo.SplitIDs(@vcHeadquarterIds,',')))
		AND (ISNULL(@vcPoliticalScoreIds,'') = '' OR CFVPoliticalScore.Fld_Value IN (SELECT Id FROM dbo.SplitIDs(@vcPoliticalScoreIds,',')))
		AND (ISNULL(@vcSearchText,'') = ''  OR (ISNULL(CI.vcCompanyName,'') LIKE CONCAT('%',@vcSearchText,'%')
												OR ISNULL(CI.txtComments,'') LIKE CONCAT('%',@vcSearchText,'%')
												OR ISNULL(CI.vcWebSite,'') LIKE CONCAT('%',@vcSearchText,'%')
												OR ISNULL(LD.vcData,'') LIKE CONCAT('%',@vcSearchText,'%')
												OR ISNULL(CFVEvidence1.Fld_Value,'') LIKE CONCAT('%',@vcSearchText,'%')
												OR ISNULL(CFVEvidence2.Fld_Value,'') LIKE CONCAT('%',@vcSearchText,'%')
												OR ISNULL(CFVEvidence3.Fld_Value,'') LIKE CONCAT('%',@vcSearchText,'%')
												OR ISNULL(CFVEvidence4.Fld_Value,'') LIKE CONCAT('%',@vcSearchText,'%')
												OR ISNULL(CFVEvidence5.Fld_Value,'') LIKE CONCAT('%',@vcSearchText,'%')))
	ORDER BY 
		CASE WHEN @vcSortDirection = 'asc' THEN
			CASE @vcSortColumn
				WHEN '0' THEN CI.vcCompanyName
				WHEN '2' THEN CI.vcWebSite
				WHEN '3' THEN LD.vcData
			END
		END,
		CASE WHEN @vcSortDirection = 'desc' THEN
			CASE @vcSortColumn
				WHEN '0' THEN CI.vcCompanyName
				WHEN '2' THEN CI.vcWebSite
				WHEN '3' THEN LD.vcData
			END
		END DESC
	OFFSET 
		@numRecordIndex ROWS
	FETCH NEXT
		@numPageSize ROWS ONLY
END
GO