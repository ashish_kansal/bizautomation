        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetParentChildCustomFieldMap')
	DROP PROCEDURE USP_GetParentChildCustomFieldMap
GO

CREATE PROCEDURE [dbo].[USP_GetParentChildCustomFieldMap]
	@numDomainID numeric(18, 0),
	@tintChildModule tinyint
AS
BEGIN
	SELECT 
		numParentChildFieldID
		,PCFM.numDomainID
		,tintParentModule
		,numParentFieldID
		,tintChildModule
		,numChildFieldID
		,PM.Loc_name AS vcParentModule
		,CM.Loc_name AS vcChildModule
		,DFMP.vcFieldName AS vcParentField
		,DFMC.vcFieldName AS vcChildField
	FROM 
		ParentChildCustomFieldMap PCFM 
	JOIN CFW_Loc_Master PM ON PCFM.tintParentModule=PM.Loc_id
	JOIN CFW_Loc_Master CM ON PCFM.tintChildModule=CM.Loc_id
	INNER JOIN
		DycFieldMaster DFMP
	ON
		PCFM.numParentFieldID = DFMP.numFieldId
	INNER JOIN
		DycFieldMaster DFMC
	ON
		PCFM.numChildFieldID = DFMC.numFieldId
	WHERE 
		PCFM.numDomainID=@numDomainID 
		AND ISNULL(PCFM.bitCustomField,0) = 0
		AND (PCFM.tintChildModule=@tintChildModule OR @tintChildModule=0) 
	UNION
	SELECT 
		numParentChildFieldID
		,PCFM.numDomainID
		,tintParentModule
		,numParentFieldID
		,tintChildModule
		,numChildFieldID
		,PM.Loc_name AS vcParentModule
		,CM.Loc_name AS vcChildModule
		,PFM.Fld_label AS vcParentField
		,CFM.Fld_label AS vcChildField
	FROM 
		ParentChildCustomFieldMap PCFM 
	JOIN CFW_Loc_Master PM ON PCFM.tintParentModule=PM.Loc_id
	JOIN CFW_Fld_Master PFM ON PCFM.numParentFieldID=PFM.Fld_id AND PCFM.numDomainID=PFM.numDomainID AND PCFM.tintParentModule=PFM.Grp_id
	JOIN CFW_Loc_Master CM ON PCFM.tintChildModule=CM.Loc_id
	JOIN CFW_Fld_Master CFM ON PCFM.numChildFieldID=CFM.Fld_id AND PCFM.numDomainID=CFM.numDomainID AND PCFM.tintChildModule=CFM.Grp_id
	WHERE 
		PCFM.numDomainID=@numDomainID 
		AND ISNULL(PCFM.bitCustomField,0) = 1
		AND (PCFM.tintChildModule=@tintChildModule OR @tintChildModule=0) 
END
GO