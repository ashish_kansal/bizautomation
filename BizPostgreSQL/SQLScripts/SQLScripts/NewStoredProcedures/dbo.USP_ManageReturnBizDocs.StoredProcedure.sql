SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageReturnBizDocs' ) 
    DROP PROCEDURE USP_ManageReturnBizDocs
GO

CREATE PROCEDURE [dbo].[USP_ManageReturnBizDocs]
    (
      @numReturnHeaderID NUMERIC(9) = 0,
      @tintReceiveType TINYINT,
      @numReturnStatus NUMERIC(9),
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @numAccountID NUMERIC(9),
      @vcCheckNumber VARCHAR(50),
      @IsCreateRefundReceipt BIT,
      @numItemCode	NUMERIC(18,0)
    )
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION
    DECLARE @tintType TINYINT,@tintReturnType TINYINT,@tintOrigReceiveType TINYINT
	
    SELECT @tintReturnType=tintReturnType,@tintOrigReceiveType=tintReceiveType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    

	IF @tintOrigReceiveType = 1
	BEGIN
		RAISERROR('REFUND_RECEIPT_ALREADY_ISSUED', 16, 1)
	END
	ELSE IF @tintOrigReceiveType = 2
	BEGIN
		RAISERROR('CREDIT_MEMO_ALREADY_ISSUED', 16, 1)
	END
	ELSE IF @tintOrigReceiveType = 3
	BEGIN
		RAISERROR('REFUND_ALREADY_ISSUED', 16, 1);
	END
    
	PRINT @tintReturnType
	PRINT @tintReceiveType

    IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		DECLARE @i INT = 1
		DECLARE @COUNT AS INT 
		DECLARE @numTempOppID AS NUMERIC(18,0)
		DECLARE @numTempOppItemID AS NUMERIC(18,0)
		DECLARE @bitTempLotNo AS BIT
		DECLARE @numTempQty AS FLOAT 
		DECLARE @numTempWareHouseItmsDTLID AS INT		

		--SALES RETURN
		IF @tintReturnType = 1
		BEGIN
			UPDATE
				WID
			SET
				WID.numWareHouseItemID = OWSI.numWarehouseItmsID
				,WID.numQty = 1
			FROM
				OppWarehouseSerializedItem OWSI
			INNER JOIN
				WareHouseItmsDTL WID
			ON
				OWSI.numWarehouseItmsDTLID =WID.numWareHouseItmsDTLID
			INNER JOIN
				WareHouseItems
			ON
				OWSI.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numReturnHeaderID = @numReturnHeaderID
				AND ISNULL(Item.bitSerialized,0) = 1

			INSERT INTO WareHouseItmsDTL
			(
				numWareHouseItemID
				,vcSerialNo
				,numQty
			)
			SELECT 
				OWSI.numWarehouseItmsID
				,WID.vcSerialNo
				,OWSI.numQty
			FROM
				OppWarehouseSerializedItem OWSI
			INNER JOIN
				WareHouseItmsDTL WID
			ON
				OWSI.numWarehouseItmsDTLID =WID.numWareHouseItmsDTLID
			INNER JOIN
				WareHouseItems
			ON
				OWSI.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numReturnHeaderID = @numReturnHeaderID
				AND ISNULL(Item.bitLotNo,0) = 1
		END
		ELSE IF @tintReturnType = 2
		BEGIN
			IF (SELECT
						COUNT(*)
				FROM
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
					AND 1 = (CASE 
								WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
								WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
								ELSE 0 
							END)
				) > 0
			BEGIN
				RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
			END
			ELSE
			BEGIN
				-- REMOVE SERIAL/LOT# NUUMBER FROM INVENTORY
				UPDATE WHIDL
					SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
				FROM 
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
			END
		END

        EXEC usp_ManageRMAInventory @numReturnHeaderID,@numDomainId,@numUserCntID,1 
    END   
    
	SET @tintType=(CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 5 
				       When @tintReturnType=1 AND @tintReceiveType=2 THEN 3 
				       When @tintReturnType=2 AND @tintReceiveType=2 THEN 4
				       ELSE 0 
					END) 
		
	PRINT @tintType

	IF 	@tintType>0
	BEGIN
		EXEC dbo.USP_UpdateBizDocNameTemplate @tintType, @numDomainID,@numReturnHeaderID
    END    
    	
    DECLARE @numBizdocTempID AS NUMERIC(18, 0);
	SET @numBizdocTempID=0
        
    IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		IF @tintReceiveType = 2 AND @tintReturnType=1
		BEGIN
		SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
			WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1
					AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
					FROM    dbo.ListDetails WHERE   vcData = 'Sales Credit Memo' AND constFlag = 1 )
		END
		ELSE IF @tintReturnType=1 OR @tintReceiveType=1
        BEGIN 
            SELECT TOP 1 @numBizdocTempID = numBizdocTempID FROM BizDocTemplate
                WHERE   numOppType = 1 AND bitDefault = 1 AND bitEnabled = 1 AND numDomainID = @numDomainID
					AND numBizDocID = ( SELECT TOP 1 numListItemID 
                    FROM    dbo.ListDetails WHERE   vcData = 'Refund Receipt' AND constFlag = 1)
        END
        ELSE IF @tintReceiveType = 2 AND @tintReturnType=2
        BEGIN
                SELECT TOP 1 @numBizdocTempID = numBizDocTempID FROM    BizDocTemplate
                WHERE   numOppType = 2 AND bitDefault = 1 AND bitEnabled = 1
                        AND numDomainID = @numDomainID AND numBizDocID IN (SELECT TOP 1 numListItemID
                        FROM    dbo.ListDetails WHERE   vcData = 'Purchase Credit Memo' AND constFlag = 1 )
        END 
	END
		
	DECLARE @monAmount AS DECIMAL(20,5),@monTotalTax  AS DECIMAL(20,5),@monTotalDiscount AS  DECIMAL(20,5)  
		
	SET @tintType=CASE When @tintReturnType=1 AND @tintReceiveType=1 THEN 9 
				    When @tintReturnType=1 AND @tintReceiveType=2 THEN 7 
				    When @tintReturnType=1 AND @tintReceiveType=3 THEN 9 
				    When @tintReturnType=2 AND @tintReceiveType=2 THEN 8
				    When @tintReturnType=3 THEN 10
				    When @tintReturnType=4 THEN 9 
					END 
				       
	SELECT @monAmount=monAmount,@monTotalTax=monTotalTax,@monTotalDiscount=monTotalDiscount FROM dbo.GetReturnDealAmount(@numReturnHeaderID,@tintType)
		
    UPDATE  dbo.ReturnHeader
    SET     tintReceiveType = @tintReceiveType,
            numReturnStatus = @numReturnStatus,
            numModifiedBy = @numUserCntID,
            dtModifiedDate = GETUTCDATE(),
            numAccountID = @numAccountID,
            vcCheckNumber = @vcCheckNumber,
            IsCreateRefundReceipt = @IsCreateRefundReceipt,
            numBizdocTempID = CASE When @numBizdocTempID>0 THEN @numBizdocTempID ELSE numBizdocTempID END,
            monBizDocAmount= @monAmount + @monTotalTax - @monTotalDiscount,
            numItemCode = @numItemCode
    WHERE   numReturnHeaderID = @numReturnHeaderID
        
	DECLARE @numDepositIDRef AS NUMERIC(18,0)
	SELECT @numDepositIDRef = ISNULL(numDepositIDRef,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID
	PRINT @numDepositIDRef

	IF @numDepositIDRef > 0
	BEGIN
		IF NOT EXISTS(SELECT * FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef) AND @tintReceiveType = 1 AND @tintReturnType = 4
		BEGIN
			UPDATE dbo.DepositMaster SET [monAppliedAmount] = ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] 
																	WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0)
											,[numReturnHeaderID] = @numReturnHeaderID	
											,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([ReturnHeader].[monAmount]) FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numDepositIDRef] = @numDepositIDRef),0) 
			WHERE numDepositId=@numDepositIDRef

		END
		ELSE
		BEGIN
			UPDATE dbo.DepositMaster SET [monAppliedAmount] = @monAmount + ISNULL((SELECT SUM([DD].[monAmountPaid]) FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef),0)
											,[monRefundAmount] = [monDepositAmount] - ISNULL((SELECT SUM([DD].[monAmountPaid]) FROM [dbo].[DepositeDetails] AS DD WHERE [DD].[numDepositID] = @numDepositIDRef),0)
			WHERE numDepositId=@numDepositIDRef						
		END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO