SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemTransitforSO')
DROP PROCEDURE USP_GetItemTransitforSO
GO
CREATE PROCEDURE [dbo].[USP_GetItemTransitforSO] 
( 
@numDomainID as numeric(9)=0,    
@vcItemcode AS varchar(900)=0,
@ClientTimeZoneOffset      AS INT
)
AS 
BEGIN
select opp.numOppId,Opp.vcPOppName,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' else dbo.FormatedDateFromDate(Opp.bintCreatedDate,1) end  bintCreatedDate,
dbo.FormatedDateFromDate(Opp.intpEstimatedCloseDate,@numDomainID) AS DueDate,
dbo.fn_GetContactName(Opp.numContactId) AS vcOrderedBy,
I.vcItemName + CASE WHEN I.vcSKU<>'' THEN '('+I.vcSKU+')' ELSE '' END AS ItemName,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),OppI.numItemCode,OPP.numDomainId,ISNULL(OppI.numUOMId,0)) * (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0))) as numeric(18,0)) as numUnitHour
,ISNULL(u.vcUnitName,'') vcUOMName,OppI.vcManufacturer,ISNULL(OppI.vcItemDesc,'') as vcItemDesc,OppI.vcModelID,
dbo.fn_GetListItemName(Opp.intUsedShippingCompany) vcShipmentMethod,
CASE WHEN ISNULL(VSM.numListValue,0)>0 THEN 
case when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)))= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	 else dbo.FormatedDateFromDate(DATEADD(DAY,VSM.numListValue,DateAdd(minute, -@ClientTimeZoneOffset,Opp.bintCreatedDate)),@numDomainID) end 
	 ELSE '' end bintExpectedDelivery,
	 OPA.vcShipCity As vcDeliverTo,
	 CASE WHEN ISNULL(DM.intDropShip,0)=1 THEN 'Not Available'
	 WHEN ISNULL(DM.intDropShip,0)=2 THEN 'Blank Available'
	 WHEN ISNULL(DM.intDropShip,0)=3 THEN 'Vendor Label'
	 WHEN ISNULL(DM.intDropShip,0)=4 THEN 'Private Label'
	 ELSE '-'
END AS vcDropShip,
 (select
   distinct  
    stuff((
        select ' ' + LD.vcData +'(<span class="text-primary">'+CAST(ISNULL(VM.numListValue,0) AS VARCHAR(100))+' Days</span>)'
        from ListDetails LD LEFT JOIN VendorShipmentMethod VM ON VM.numListItemID=LD.numListItemID 
	AND VM.numVendorid=Opp.numDivisionId AND VM.numAddressID=Opp.numVendorAddressID AND VM.numDomainID=@numDomainID
	WHERE LD.numListID=338 AND (LD.numDomainID=1 or Ld.constFlag=1) AND ISNULL(VM.numListValue,0)>0
        order by  LD.vcData
        for xml path('')
    ),1,1,'') as userlist
from ListDetails LD LEFT JOIN VendorShipmentMethod VM ON VM.numListItemID=LD.numListItemID 
	AND VM.numVendorid=Opp.numDivisionId AND VM.numAddressID=Opp.numVendorAddressID AND VM.numDomainID=@numDomainID
	WHERE LD.numListID=338 AND (LD.numDomainID=1 or Ld.constFlag=1) AND ISNULL(VM.numListValue,0)>0
group by LD.vcData) AS vendorShipmentDays,
CMP.vcCompanyName
      FROM OpportunityMaster Opp INNER JOIN OpportunityItems OppI ON opp.numOppId=OppI.numOppId 
		LEFT join AdditionalContactsInformation ADC        
		 on ADC.numContactID=Opp.numContactID        
		 LEFT join DivisionMaster DM        
		 on DM.numDivisionID=ADC.numDivisionID 
		 LEFT JOIN CompanyInfo CMP
		 on DM.numCompanyID=CMP.numCompanyID      
		   INNER JOIN item I on OppI.numItemCode=i.numItemcode
		   LEFT JOIN OpportunityAddress AS OPA ON OPA.numOppId=OppI.numOppId
		   LEFT JOIN  UOM u ON u.numUOMId = OppI.numUOMId  
		   OUTER APPLY (SELECT TOP 1 * FROM VendorShipmentMethod WHERE numAddressID=Opp.numVendorAddressID AND numVendorID=Opp.numDivisionId ORDER BY ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC) VSM
           WHERE Opp.tintOppType= 2 and Opp.tintOppstatus=1 and Opp.tintShipped=0 
				 and Opp.numDomainId=@numDomainID AND OppI.numItemcode IN ( SELECT CAST(Items AS NUMERIC) FROM Split(@vcItemcode,',') WHERE Items<>'')
				 and (isnull(OppI.numUnitHour,0)-isnull(OppI.numUnitHourReceived,0)>0)
			ORDER BY OppI.vcItemName,Opp.intpEstimatedCloseDate

END