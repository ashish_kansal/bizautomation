SET ANSI_NULLS ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WFDateFieldsDataSync')
DROP PROCEDURE USP_WFDateFieldsDataSync
GO


CREATE PROCEDURE [dbo].[USP_WFDateFieldsDataSync]
	
AS 
BEGIN
	IF CAST((SELECT LastReindexDate FROM ElasticSearchLastReindex WHERE ID = 3) AS DATE) <> CAST(GETDATE() AS DATE) AND DATEPART(hh, GETDATE()) > 1
	BEGIN
	
		delete from [AdditionalContactsInformation_TempDateFields]
		delete from [Cases_TempDateFields]
		delete from [Communication_TempDateFields]
		delete from [DivisionMaster_TempDateFields]
		delete from [OpportunityMaster_TempDateFields]
		delete from [ProjectsMaster_TempDateFields]
		delete from [StagePercentageDetails_TempDateFields]

		INSERT INTO [AdditionalContactsInformation_TempDateFields] (numContactId, numDomainID, bintCreatedDate, bintDOB) 
		SELECT numContactId,numDomainID, bintCreatedDate,bintDOB	
		FROM AdditionalContactsInformation
		WHERE (
				(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(bintDOB >= DATEADD(DAY,-30,GETDATE()) AND bintDOB <= DATEADD(DAY,90,GETDATE()) )
		      )

		---------------------------------------------------------------------------------
		INSERT INTO [Cases_TempDateFields] (numCaseId, numDomainID, bintCreatedDate, intTargetResolveDate) 
		SELECT numCaseId,numDomainID, bintCreatedDate, intTargetResolveDate
	
		FROM Cases
		WHERE (
				(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(intTargetResolveDate >= DATEADD(DAY,-30,GETUTCDATE()) AND intTargetResolveDate <= DATEADD(DAY,90,GETUTCDATE()) )
			  )

		------------------------------------------------------------------------------------

		INSERT INTO [Communication_TempDateFields] (numCommId, numDomainID, dtStartTime) 
		SELECT numCommId,numDomainID, dtStartTime
		FROM Communication
		WHERE (
				(dtStartTime >= DATEADD(DAY,-30,GETUTCDATE()) AND dtStartTime <= DATEADD(DAY,90,GETUTCDATE()) )
			  )

		----------------------------------------------------------------------------------------------

		INSERT INTO [DivisionMaster_TempDateFields] (numDivisionID, numDomainID, bintCreatedDate) 
		SELECT numDivisionID,numDomainID, bintCreatedDate
		FROM DivisionMaster
		WHERE (
				(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
			  )
		------------------------------------------------------------------------------------------------
		INSERT INTO [OpportunityMaster_TempDateFields] (numOppId, numDomainID, bintClosedDate, bintCreatedDate,intpEstimatedCloseDate,dtReleaseDate) 
		SELECT numOppId,numDomainID, bintClosedDate, bintCreatedDate,intpEstimatedCloseDate,dtReleaseDate
		FROM OpportunityMaster
		WHERE (
				(bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				 OR
				(dtReleaseDate >= DATEADD(DAY,-30,GETDATE()) AND dtReleaseDate <= DATEADD(DAY,90,GETDATE()) )
			  )			 

		--------------------------------------------------------------------------------------------------------

		INSERT INTO [ProjectsMaster_TempDateFields] (numProId, numDomainID, bintCreatedDate, bintProClosingDate,intDueDate) 
		SELECT numProId,numDomainID, bintCreatedDate, bintProClosingDate, intDueDate
		FROM ProjectsMaster
		WHERE (
				(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR	
				(bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
		      )

		--------------------------------------------------------------------------------------------------------
		INSERT INTO [StagePercentageDetails_TempDateFields] (numStageDetailsId, numDomainID, dtEndDate, dtStartDate) 
		SELECT numStageDetailsId,numDomainID, dtEndDate, dtStartDate
		FROM StagePercentageDetails
		WHERE (
				(dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR	
				(dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
			  )

		-------------------------------------------------------------------------------------------------------

		UPDATE ElasticSearchLastReindex SET LastReindexDate = GETDATE() WHERE ID = 3
	END
END


GO


