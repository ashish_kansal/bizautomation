SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AllowFulfillOrder')
DROP PROCEDURE USP_AllowFulfillOrder
GO
CREATE PROCEDURE [dbo].[USP_AllowFulfillOrder]
    (
	  @numDomainID NUMERIC(9),
      @numOppID NUMERIC(9),
      @bitAllowFulfillOrder BIT OUTPUT 
    )
AS 
    BEGIN
   
		SET @bitAllowFulfillOrder=0
   
		DECLARE @numPackingSlipBizDocId NUMERIC(9),@numAuthInvoice NUMERIC(9)
		
		SELECT @numPackingSlipBizDocId=numListItemID FROM ListDetails WHERE vcData='Packing Slip' AND constFlag=1 AND numListID=27
	    
		Select @numAuthInvoice=isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=@numDomainId 
	    
	    
		IF NOT EXISTS (SELECT numOppBizDocsId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppID=OBD.numOppID
				WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID  AND (OBD.numBizDocId IN (@numPackingSlipBizDocId,@numAuthInvoice,296)))
			BEGIN
				SET @bitAllowFulfillOrder=1
			END
	    
    END
