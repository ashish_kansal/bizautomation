GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteOnlineBillPayeeDetail')
	DROP PROCEDURE USP_DeleteOnlineBillPayeeDetail
GO

/****** Added By : Joseph ******/
/****** Deletes the Online Bill Payee Detail from Table OnlineBillPayeeDetails ******/


CREATE PROCEDURE [dbo].[USP_DeleteOnlineBillPayeeDetail]
	@numBankDetailId numeric(18, 0),
	@PayeeDetailId NUMERIC(18,0)
AS

SET NOCOUNT ON

DELETE FROM dbo.OnlineBillPayeeDetails
WHERE 
	numPayeeDetailId = @PayeeDetailId
	AND numBankDetailId = @numBankDetailId

--UPDATE dbo.OnlineBillPayeeDetails 
--SET bitIsActive = 0 
--WHERE 
--	vcPayeeFIListID = @vcPayeeFIListID 
--	AND numBankDetailId = @numBankDetailId


GO
