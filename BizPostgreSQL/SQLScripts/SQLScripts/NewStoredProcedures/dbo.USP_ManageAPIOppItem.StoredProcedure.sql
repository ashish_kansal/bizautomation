GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageAPIOppItem' ) 
    DROP PROCEDURE USP_ManageAPIOppItem
GO
--created by Joseph
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
CREATE PROCEDURE [dbo].[USP_ManageAPIOppItem]
@WebApiOrderItemId numeric(18, 0),
@numDomainId	numeric(18, 0),
@numWebApiId	int,
@numOppId	numeric(18, 0),
@numOppItemId numeric(18, 0),
@vcAPIOppId	varchar(50),
@vcApiOppItemId varchar(50),
@tintStatus tinyint = 0,
@numCreatedby	numeric(18, 0)
AS 
BEGIN
  DECLARE @Check as integer
  IF @WebApiOrderItemId =0
	  BEGIN
		SELECT @Check = COUNT(*) FROM   [WebApiOppItemDetails] WHERE  [numDomainId] = @numDomainID AND [numWebApiId] = @numWebApiId 
		AND vcAPIOppId = @vcAPIOppId AND vcApiOppItemId = @vcApiOppItemId
  		IF @Check =0    
		 BEGIN
			INSERT INTO [WebApiOppItemDetails]
				   (
				   [numDomainId]
				   ,[numWebApiId]
				   ,[numOppId]
				   ,[numOppItemId]
				   ,[vcAPIOppId]
				   ,[vcApiOppItemId]
				   ,[tintStatus]
				   ,[numCreatedby]
				   ,[dtCreated])
			 VALUES
				   (
					@numDomainId,
				   @numWebApiId,
				   @numOppId,
				   @numOppItemId,
				   @vcAPIOppId,
				   @vcApiOppItemId,
				   @tintStatus,
				   @numCreatedby,
				   GETUTCDATE())
		END
END
	ELSE 
		IF @WebApiOrderItemId <> 0
			BEGIN
				SELECT @Check = COUNT(*) FROM   [WebApiOppItemDetails] WHERE  [numDomainId] = @numDomainID AND [numWebApiId] = @numWebApiId 
				AND vcAPIOppId = @vcAPIOppId AND vcApiOppItemId = @vcApiOppItemId
				IF @Check =0    
					BEGIN
						INSERT INTO [WebApiOppItemDetails]
							   (
							   [numDomainId]
							   ,[numWebApiId]
							   ,[numOppId]
							   ,[numOppItemId]
							   ,[vcAPIOppId]
							   ,[vcApiOppItemId]
							   ,[tintStatus]
							   ,[numCreatedby]
							   ,[dtCreated],
							   [dtModified])
						 VALUES
							   (
								@numDomainId,
							   @numWebApiId,
							   @numOppId,
							   @numOppItemId,
							   @vcAPIOppId,
							   @vcApiOppItemId,
							   @tintStatus,
							   @numCreatedby,
							   GETUTCDATE(),
							   GETUTCDATE())
					END
				ELSE
					BEGIN
						UPDATE [WebApiOppItemDetails]
						SET    [tintStatus] = @tintStatus,[dtModified] = GETUTCDATE()
						WHERE  [numDomainId] = @numDomainID AND [numWebApiId] = @numWebApiId
						AND [vcAPIOppId] = @vcAPIOppId AND [vcApiOppItemId] = @vcApiOppItemId
					END		
		END
	END
GO
