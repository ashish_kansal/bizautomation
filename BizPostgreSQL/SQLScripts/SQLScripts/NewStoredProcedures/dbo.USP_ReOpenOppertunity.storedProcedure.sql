/****** Object:  StoredProcedure [dbo].[USP_ReOpenOppertunity]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReOpenOppertunity')
DROP PROCEDURE USP_ReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_ReOpenOppertunity]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9),
@numDomainID AS NUMERIC(9)
          
as          
BEGIN TRY
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already opened or not. If already opened then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomainID) = 0
	BEGIN
		RAISERROR('ORDER_OPENED', 16, 1)
	END

   BEGIN TRANSACTION       
			declare @status as varchar(2)            
			declare @OppType as varchar(2)  
			DECLARE @fltExchangeRate AS FLOAT 
			          
			select @status=tintOppStatus,@OppType=tintOppType,@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster            
			where numOppId=@OppID AND [numDomainId] = @numDomainID

			update OpportunityMaster set tintshipped=0,bintAccountClosingDate=NULL,bintClosedDate=NULL where  numOppId=@OppID  AND [numDomainId] = @numDomainID
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = NULL WHERE [numOppId]=@OppID AND numBizDocID <> 296
        			
			-- WE ARE REVERTING RECEIVED (PURCHASED) QTY ONLY BECAUSE USER HAS TO DELETE FULFILLMENT ORDER TO REVERSE SHIPPED (SALED) QTY
			IF @OppType = 2
			BEGIN
				--SET Received qty to 0	
				UPDATE OpportunityItems SET numUnitHourReceived=0,numQtyShipped=0,numQtyReceived=0 WHERE [numOppId]=@OppID
				UPDATE OpportunityKitItems SET numQtyShipped=0 WHERE [numOppId]=@OppID
			END
			
			-- UnArchive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems 
															  WHERE OpportunityItems.numItemCode = I.numItemCode
															  AND OpportunityItems.numoppitemtCode = OI.numoppitemtCode) 
												  THEN 0
												  ELSE 1 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainId] = @numDomainID
				AND ISNULL(I.bitArchiveItem,0) = 1 
			END     
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won              
			declare @numoppitemtCode as numeric            
			declare @numUnits as FLOAT              
			declare @numWarehouseItemID as numeric       
			declare @itemcode as numeric        
			declare @QtyShipped as FLOAT
			declare @QtyReceived as FLOAT
			Declare @monPrice as DECIMAL(20,5)  
			declare @Kit as bit  
			declare @bitWorkOrder AS BIT
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = bitWorkOrder
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainId] = @numDomainID
			order by OI.numoppitemtCode      
            
			 while @numoppitemtCode>0                
			 begin   
 
				EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,3,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			  @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end) 
			  from OpportunityItems OI                                                
			  join Item I                                                
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))
					and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
					AND I.[numDomainId] = @numDomainID
					order by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
				

			IF @OppType=2
			BEGIN
				IF (
					SELECT 
						COUNT(*)
					FROM 
						OppWarehouseSerializedItem OWSI 
					INNER JOIN 
						OpportunityMaster OM 
					ON 
						OWSI.numOppID=OM.numOppId 
						AND tintOppType=1 
					WHERE 
						OWSI.numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = @OppID)
					) > 0
				BEGIN
					RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
				END
				ELSE
				BEGIN
					-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
					UPDATE WHIDL
						SET WHIDL.numQty = ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0)
					FROM 
						WareHouseItmsDTL WHIDL
					INNER JOIN
						OppWarehouseSerializedItem OWSI
					ON
						WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @OppID
						AND ISNULL(Item.bitLotNo,0) = 1

					DELETE 
						WHIDL
					FROM 
						WareHouseItmsDTL WHIDL
					INNER JOIN
						OppWarehouseSerializedItem OWSI
					ON
						WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @OppID
						AND ISNULL(Item.bitSerialized,0) = 1
				END
				
				DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@OppID
			END
		
			
			IF @OppType=2
			BEGIn
			DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0)
			
			DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0
			END
COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH


GO
