GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='GetCommissionItemReport')
DROP PROCEDURE GetCommissionItemReport
GO
CREATE PROCEDURE GetCommissionItemReport
    (     
      @numDomainId numeric(9) ,
      @numUserCntID numeric(9) ,
      @numItemCode numeric(9),
      @tintAssignTo TINYINT,
      @bitCommContact AS BIT 
    )

AS BEGIN


DECLARE @tintComAppliesTo TINYINT
SELECT  @tintComAppliesTo = ISNULL(tintComAppliesTo,0) FROM domain WHERE numDomainID = @numDomainID

DECLARE @numComRuleID NUMERIC,@tintComBasedOn TINYINT,@tintComType TINYINT,@tinComDuration TINYINT 
DECLARE @dFrom DATETIME,@dTo Datetime    
  
      
--DECLARE @i AS INT;SET @i=1
					
					--Loop for AssignTo i=1 and RecordOwner i=2 (set tintAssignTo=i)
					--WHILE @i<3
					--BEGIN
					
--Individual Items tintComAppliesTo=1 and tintType=1
					IF EXISTS(SELECT CR.numComRuleID FROM CommissionRules CR JOIN CommissionRuleItems CRI ON CR.numComRuleID=CRI.numComRuleID
								JOIN CommissionRuleContacts CRC ON CR.numComRuleId=CRC.numComRuleId 
								WHERE CR.numDomainID=@numDomainID AND CR.tintComAppliesTo=1 AND CR.tintAssignTo=@tintAssignTo AND CRI.numValue=@numItemCode 
								AND CRI.tintType=1 AND CRC.numValue=@numUserCntID AND CRC.bitCommContact=@bitCommContact) AND @tintComAppliesTo=1
						BEGIN
							SELECT TOP 1 @numComRuleID=CR.numComRuleID,@tintComBasedOn=CR.tintComBasedOn,@tintComType=CR.tintComType,@tinComDuration=CR.tinComDuration
							FROM CommissionRules CR JOIN CommissionRuleItems CRI ON CR.numComRuleID=CRI.numComRuleID
								JOIN CommissionRuleContacts CRC ON CR.numComRuleId=CRC.numComRuleId 
								WHERE CR.numDomainID=@numDomainID AND CR.tintComAppliesTo=1 AND CR.tintAssignTo=@tintAssignTo AND CRI.numValue=@numItemCode 
								AND CRI.tintType=1 AND CRC.numValue=@numUserCntID AND CRC.bitCommContact=@bitCommContact
						END
						
					--Items Classification	tintComAppliesTo=2 and tintType=2
					ELSE IF EXISTS (SELECT CR.numComRuleID FROM CommissionRules CR JOIN CommissionRuleItems CRI ON CR.numComRuleID=CRI.numComRuleID	JOIN CommissionRuleContacts CRC ON CR.numComRuleId=CRC.numComRuleId 
									WHERE CR.numDomainID=@numDomainID AND CR.tintComAppliesTo=2 AND CR.tintAssignTo=@tintAssignTo AND CRI.numValue=@numItemCode 
									AND CRI.tintType=2 AND CRC.numValue=@numUserCntID AND CRC.bitCommContact=@bitCommContact) AND @tintComAppliesTo=2
						BEGIN
						SELECT TOP 1 @numComRuleID=CR.numComRuleID,@tintComBasedOn=CR.tintComBasedOn,@tintComType=CR.tintComType,@tinComDuration=CR.tinComDuration 
							FROM CommissionRules CR JOIN CommissionRuleItems CRI ON CR.numComRuleID=CRI.numComRuleID	JOIN CommissionRuleContacts CRC ON CR.numComRuleId=CRC.numComRuleId 
									WHERE CR.numDomainID=@numDomainID AND CR.tintComAppliesTo=2 AND CR.tintAssignTo=@tintAssignTo AND CRI.numValue=@numItemCode 
									AND CRI.tintType=2 AND CRC.numValue=@numUserCntID AND CRC.bitCommContact=@bitCommContact
						END
						
					--All Items	 tintComAppliesTo=3
					ELSE IF EXISTS (SELECT CR.numComRuleID FROM CommissionRules CR JOIN CommissionRuleContacts CRC ON CR.numComRuleId=CRC.numComRuleId  WHERE CR.tintComAppliesTo=3 AND CR.numDomainID=@numDomainID AND CR.tintAssignTo=@tintAssignTo AND CRC.numValue=@numUserCntID AND CRC.bitCommContact=@bitCommContact) AND @tintComAppliesTo=3
						BEGIN
							SELECT TOP 1 @numComRuleID=CR.numComRuleID,@tintComBasedOn=CR.tintComBasedOn,@tintComType=CR.tintComType,@tinComDuration=CR.tinComDuration
							FROM CommissionRules CR JOIN CommissionRuleContacts CRC ON CR.numComRuleId=CRC.numComRuleId  WHERE CR.tintComAppliesTo=3 AND CR.numDomainID=@numDomainID AND CR.tintAssignTo=@tintAssignTo AND CRC.numValue=@numUserCntID AND CRC.bitCommContact=@bitCommContact
						END
	
	
	--Get From and To date for Commission Calculation
						IF @tinComDuration=1 --Month
							SELECT @dFrom=DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0),
									@dTo=DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE()) + 1, 0)) 
	
						ELSE IF @tinComDuration=2 --Quarter
							SELECT @dFrom=DATEADD(q, DATEDIFF(q, 0, GETDATE()), 0),
									@dTo=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, GETDATE()) + 1, 0)) 
									
						ELSE IF @tinComDuration=3 --Year
							SELECT @dFrom=DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0),
									@dTo=DATEADD(d, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, 0)) 

									
Select SUM(numComissionAmount) as CommissionAmt,CASE @tintComBasedOn WHEN 1 THEN SUM(BDI.monTotAmount) WHEN 2 THEN SUM(BDI.numUnitHour) end AS numBase,BC.numComRuleID,BC.decCommission,@tintComType AS tintComType,MAX(BC.numComissionID) AS numComissionID
INTO #temp
--Select CAST(SUM(numComissionAmount) AS VARCHAR(30)) + ' - ' + CAST(BC.decCommission AS VARCHAR(30)) + CASE @tintComType WHEN 1 THEN '%' WHEN 2 THEN ' Flat' END AS vcVertical,
--CASE @tintComBasedOn WHEN 1 THEN SUM(BDI.monTotAmount) WHEN 2 THEN SUM(BDI.numUnitHour) END AS numBase
From OpportunityMaster Opp left join OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
Where oppBiz.bitAuthoritativeBizDocs = 1 AND oppbiz.monAmountPaid > 0 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And 
--(Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID)

1= (CASE @tintAssignTo WHEN 1 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN Opp.numAssignedTo=@numUserCntID THEN 1 ELSE 0 END 
										                WHEN 1 THEN CASE WHEN Opp.numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END  END
				       WHEN 2 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN Opp.numRecOwner=@numUserCntID THEN 1 ELSE 0 END 
														WHEN 1 THEN CASE WHEN Opp.numRecOwner IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END END END)	 				 	 

AND 1=(CASE @bitCommContact WHEN 0 THEN CASE WHEN BC.numUserCntId=@numUserCntID THEN 1 else 0 END 
							WHEN 1 THEN CASE WHEN BC.numUserCntId IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END  END)

And Opp.numDomainId=@numDomainId AND BC.numComRuleID=@numComRuleID 
AND 1 = (CASE @tintComAppliesTo WHEN 1 THEN CASE WHEN BDI.numItemCode=@numItemCode THEN 1 ELSE 0 END
								WHEN 2 THEN CASE WHEN BDI.numItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification=@numItemCode AND numDomainID=@numDomainID) THEN 1 ELSE 0 END
								WHEN 3 THEN 1 ELSE 0 END)	
AND oppBiz.numOppBizDocsId IN (SELECT OBD.numOppBizDocsId FROM OpportunityBizDocsDetails BDD
INNER JOIN dbo.OpportunityBizDocs OBD ON BDD.numBizDocsId = OBD.numOppBizDocsId
INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId
WHERE tintopptype = 1 AND tintoppstatus = 1 AND OM.numdomainid = @numDomainId 
AND OBD.bitAuthoritativeBizDocs = 1 AND BDD.dtCreationDate BETWEEN @dFrom AND @dTo AND 
--(OM.numassignedto=@numUserCntID OR OM.numrecowner=@numUserCntID)

1= (CASE @tintAssignTo WHEN 1 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN OM.numAssignedTo=@numUserCntID THEN 1 ELSE 0 END 
										                WHEN 1 THEN CASE WHEN OM.numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END  END
				       WHEN 2 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN OM.numRecOwner=@numUserCntID THEN 1 ELSE 0 END 
														WHEN 1 THEN CASE WHEN OM.numRecOwner IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END END END)	 				 	 
GROUP BY OBD.numOppBizDocsId HAVING SUM(BDD.monAmount)>=MAX(OBD.monDealAmount))
GROUP BY BC.numComRuleID,BC.decCommission ORDER BY numComissionID
    
--SET @i = @i + 1
--END
DECLARE @vcNextTier AS VARCHAR(100)

SELECT TOP 1 @vcNextTier=ISNULL(vcNextTier,'') FROM BizDocComission where numComissionID = (select max(numComissionID) from #temp)
--SELECT TOP 1 @vcNextTier=ISNULL(vcNextTier,'') FROM BizDocComission WHERE numComRuleID=@numComRuleID 
--AND 1=(CASE @bitCommContact WHEN 0 THEN CASE WHEN numUserCntId=@numUserCntID THEN 1 else 0 END 
--							WHEN 1 THEN CASE WHEN numUserCntId IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numUserCntID) THEN 1 ELSE 0 END  END)
--ORDER BY numComissionID desc 	  

--SELECT @vcNextTier

DECLARE @sInputText VARCHAR(100),@sItem VARCHAR(100),@sDelimiter VARCHAR(100),@vcNextTierTo varchar(30),@vcNextTierCommission VARCHAR(30)
DECLARE @i AS INT
SET @i=0
SET @sDelimiter=','

IF LEN(@vcNextTier)>0
BEGIN
SET @sInputText=@vcNextTier
WHILE CHARINDEX(@sDelimiter,@sInputText,0) <> 0
 BEGIN
 SELECT
  @sItem=RTRIM(LTRIM(SUBSTRING(@sInputText,1,CHARINDEX(@sDelimiter,@sInputText,0)-1))),
  @sInputText=RTRIM(LTRIM(SUBSTRING(@sInputText,CHARINDEX(@sDelimiter,@sInputText,0)+LEN(@sDelimiter),LEN(@sInputText))))

SET @i=@i + 1
	
IF @i=4
BEGIN
 SET @vcNextTierTo=@sItem
 SET @vcNextTierCommission=@sInputText		
END
END
END

DECLARE @sql AS varchar(500)

SET @sql='SELECT  CAST(decCommission AS VARCHAR(30)) + CASE ' + CAST(@tintComType AS VARCHAR(10)) + ' WHEN ''1'' THEN ''%'' WHEN ''2'' THEN '' Flat'' END AS vcVertical,
 numBase,''Commission Made: '' + CAST(CommissionAmt AS VARCHAR(30)) AS vcText FROM #temp'

IF LEN(@vcNextTier)>0
BEGIN
SET @sql = @sql + ' Union all SELECT ''' + CAST(@vcNextTierCommission AS VARCHAR(10)) + ''' + CASE ' + CAST(@tintComType AS VARCHAR(10)) + ' WHEN ''1'' THEN ''%'' WHEN ''2'' THEN '' Flat'' END,CAST(' + CAST(@vcNextTierTo AS VARCHAR(10)) +' AS int),''Next Tier'''
END

PRINT(@sql)
EXEC (@sql)

SELECT CASE @tinComDuration WHEN 1 THEN 'This Month' WHEN 2 THEN 'This Quarter' WHEN 3 THEN 'This Year' END vcCommissionDuration,(SELECT sum(CommissionAmt) FROM #temp) AS TotalCommissionAmt,CASE @tintComBasedOn WHEN 1 THEN 'Amount Sold' WHEN 2 THEN 'Units Sold' END AS vcComBasedOn ,@numItemCode AS numItemCode,@numComRuleID AS numComRuleID

DROP table #temp

END

GO


