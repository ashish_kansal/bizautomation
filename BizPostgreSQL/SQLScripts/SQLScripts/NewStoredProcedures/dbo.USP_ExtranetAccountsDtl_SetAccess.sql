GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ExtranetAccountsDtl_SetAccess')
DROP PROCEDURE USP_ExtranetAccountsDtl_SetAccess
GO
CREATE PROCEDURE [dbo].[USP_ExtranetAccountsDtl_SetAccess]
(       
	@numDomainID NUMERIC(18,0)
	,@vcRecords VARCHAR(MAX)
)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @intNoofBusinessPortalUsers INT

	SELECT 
		@intNoofBusinessPortalUsers=ISNULL(intNoofBusinessPortalUsers,0) 
	FROM 
		Subscribers 
	WHERE 
		numTargetDomainID = @numDomainID

	DECLARE @TEMP TABLE
	(
		 numExtranetDtlID NUMERIC(18,0)
         ,tintAccessAllowed BIT
         ,bitPartnerAccess BIT
	)

	DECLARE @hDocItem INT
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcRecords

	INSERT INTO @TEMP
	(
		numExtranetDtlID
         ,tintAccessAllowed
         ,bitPartnerAccess
	)
	SELECT
		numExtranetDtlID
         ,tintAccessAllowed
         ,bitPartnerAccess
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		numExtranetDtlID NUMERIC(18,0)
         ,tintAccessAllowed BIT
         ,bitPartnerAccess BIT
	)

	EXEC sp_xml_removedocument @hDocItem 


	DECLARE @numBusinessPortalUsers INT
	SET @numBusinessPortalUsers = ISNULL((SELECT COUNT(numExtranetDtlID) FROM ExtranetAccountsDtl WHERE numDOmainID=@numDOmainID AND ISNULL(bitPartnerAccess,0)=1 AND numExtranetDtlID NOT IN (SELECT numExtranetDtlID FROM @TEMP)),0)
	SET @numBusinessPortalUsers = ISNULL(@numBusinessPortalUsers,0) + ISNULL((SELECT COUNT(*) FROM @Temp WHERE ISNULL(bitPartnerAccess,0)=1),0)

	IF @numBusinessPortalUsers > @intNoofBusinessPortalUsers
	BEGIN
		RAISERROR('BUSINESS_PORTAL_USERS_EXCEED',16,1)
	END
	ELSE
	BEGIN
		UPDATE
			EAD
		SET
			EAD.tintAccessAllowed=T.tintAccessAllowed
			,EAD.bitPartnerAccess=T.bitPartnerAccess
		FROM
			ExtranetAccountsDtl EAD
		INNER JOIN
			@TEMP T
		ON
			EAD.numExtranetDtlID=T.numExtranetDtlID
		WHERE
			EAD.numDOmainID=@numDOmainID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
GO