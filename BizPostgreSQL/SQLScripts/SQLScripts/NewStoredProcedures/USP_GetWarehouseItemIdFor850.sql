
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
     
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitemidfor850')
DROP PROCEDURE usp_getwarehouseitemidfor850
GO
CREATE PROCEDURE [dbo].[USP_GetWarehouseItemIdFor850]        
	@numDomainID NUMERIC(18,0),        
	@numItemCode NUMERIC(18,0),
	@numQty FLOAT,
	@numOrderSource NUMERIC(18,0),
	@tintSourceType TINYINT,
	@numShipToCountry NUMERIC(18,0),
	@numShipToState NUMERIC(18,0)
AS
BEGIN         
	DECLARE @TEMP TABLE
	(	
		ID INT IDENTITY(1,1)
		,numWareHouseItemID NUMERIC(18,0)
		,bitWarehouseMapped BIT
	)

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID AND ISNULL(bitAutoWarehouseSelection,0)= 1)
	BEGIN
		DECLARE @bitKitParent BIT
		SELECT @bitKitParent=ISNULL(bitKitParent,0) FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		DECLARE @TEMPWarehouse TABLE
		(
			numWarehouseItemID NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,numOnHand FLOAT
		)

		INSERT INTO @TEMPWarehouse
		(
			numWarehouseItemID
			,numWarehouseID
			,numOnHand
		)
		SELECT
			WareHouseItems.numWarehouseItemID
			,WareHouseItems.numWareHouseID
			,ISNULL((SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WareHouseItems.numItemID AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID),0)
		FROM
			WareHouseItems
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		WHERE
			WareHouseItems.numItemID = @numItemCode
			AND WarehouseLocation.numWLocationID IS NULL

		INSERT INTO @TEMP
		(
			numWareHouseItemID
			,bitWarehouseMapped
		)
		SELECT
			numWareHouseItemID
			,1
		FROM
		(
			SELECT 
				ROW_NUMBER() OVER(PARTITION BY titOnHandOrder ORDER BY titOnHandOrder ASC) numRowIndex	
				,*
			FROM
			(
				SELECT
					TW.numWareHouseItemID
					,(CASE 
						WHEN @bitKitParent = 1 
						THEN dbo.fn_IsKitCanBeBuild(@numDomainID,@numItemCode,@numQty,TW.numWarehouseID,'')
						ELSE (CASE WHEN TW.numOnHand >= @numQty THEN 1 ELSE 0 END) 
					END) titOnHandOrder
					,MassSalesFulfillmentWP.intOrder
				FROM
					MassSalesFulfillmentWM
				INNER JOIN
					MassSalesFulfillmentWMState
				ON
					MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
				INNER JOIN
					MassSalesFulfillmentWP
				ON
					MassSalesFulfillmentWM.ID = MassSalesFulfillmentWP.numMSFWMID
				INNER JOIN
					@TEMPWarehouse TW
				ON
					MassSalesFulfillmentWP.numWarehouseID	= TW.numWarehouseID
				WHERE
					MassSalesFulfillmentWM.numDomainID = @numDomainID
					AND MassSalesFulfillmentWM.numOrderSource = @numOrderSource
					AND MassSalesFulfillmentWM.tintSourceType = @tintSourceType
					AND MassSalesFulfillmentWM.numCountryID = @numShipToCountry
					AND MassSalesFulfillmentWMState.numStateID = @numShipToState
					
			) TEMP
		) T1
		ORDER BY
			titOnHandOrder DESC, intOrder ASC	
	END
	
	IF EXISTS (SELECT ID FROM @TEMP)
	BEGIN
		SELECT TOP 1 numWareHouseItemID FROM @TEMP ORDER BY ID
	END
	ELSE
	BEGIN
		IF @numDomainID=209 AND EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=1279 AND ISNULL(numWLocationID,0) = 0)
		BEGIN
			SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=1279 AND ISNULL(numWLocationID,0) = 0
		END
		ELSE
		BEGIN
			SELECT TOP 1 numWarehouseItemID FROM WarehouseItems WHERE numDomainID = @numDomainID AND WarehouseItems.numItemID= @numItemCode
		END
	END
END
GO
