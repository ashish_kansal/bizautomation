-- USP_GetDocListForAttachment 110,''
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDocListForAttachment')
DROP PROCEDURE USP_GetDocListForAttachment
GO
CREATE PROCEDURE USP_GetDocListForAttachment
    @numDomainID NUMERIC(9),
    @vcInput VARCHAR(20)
AS 
    BEGIN

        SELECT  CONVERT(VARCHAR, [numGenericDocID]) + '~G' DocID,
                vcDocName + ' (' + ISNULL([vcData], '') + ')' vcDocName,
                VcFileName
        FROM    GenericDocuments
                INNER JOIN [ListDetails] ON [numListItemID] = [numDocCategory]
        WHERE   [GenericDocuments].[numDomainID] = @numDomainID
                AND LEN([VcFileName]) > 1
                AND [numListItemID] <> 369
                AND [vcDocName] LIKE '%' + @vcInput + '%'
        UNION ALL
        SELECT  CONVERT(VARCHAR, [numGenericDocID]) + '~S' DocID,
                ISNULL([vcDocName], 'NA') + ' (' + ISNULL(OM.[vcPOppName], '')
                + ')' vcDocName,
                [VcFileName]
        FROM    [GenericDocuments] SD
                LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = SD.[numRecID]
        WHERE   SD.[numDomainID] = @numDomainID
                AND SD.[vcDocumentSection] = 'O'
                AND [vcDocName] LIKE '%' + @vcInput + '%'
        UNION ALL
        SELECT  CONVERT(VARCHAR, [numGenericDocID]) + '~S' DocID,
                ISNULL([vcDocName], 'NA') + ' (' + ISNULL(PM.[vcProjectName],
                                                          '') + ')' vcDocName,
                [VcFileName]
        FROM    [GenericDocuments] SD
                LEFT OUTER JOIN [ProjectsMaster] PM ON PM.[numProId] = SD.[numRecID]
        WHERE   SD.[numDomainID] = @numDomainID
                AND SD.[vcDocumentSection] = 'P'
                AND [vcDocName] LIKE '%' + @vcInput + '%'
        UNION ALL
        SELECT  CONVERT(VARCHAR, [numGenericDocID]) + '~S' DocID,
                ISNULL([vcDocName], 'NA') + ' (' + ISNULL(C.[vcFirstName], '')
                + ' ' + ISNULL([vcLastName], '') + ')' vcDocName,
                [VcFileName]
        FROM    [GenericDocuments] SD
                LEFT OUTER JOIN [AdditionalContactsInformation] C ON C.[numContactId] = SD.[numRecID]
        WHERE   SD.[numDomainID] = @numDomainID
                AND SD.[vcDocumentSection] = 'C'
                AND [vcDocName] LIKE '%' + @vcInput + '%'
        UNION ALL
        SELECT  CONVERT(VARCHAR, [numGenericDocID]) + '~S' DocID,
                ISNULL([vcDocName], 'NA') + ' (' + ISNULL(C.[vcCompanyName], '') + ')' vcDocName,
                [VcFileName]
        FROM    [GenericDocuments] SD
                LEFT OUTER JOIN [DivisionMaster] D ON D.[numDivisionID] = SD.[numRecID]
                left JOIN [CompanyInfo] C ON C.[numCompanyId] = D.[numCompanyID]
        WHERE   SD.[numDomainID] = @numDomainID
                AND SD.[vcDocumentSection] = 'A'--Prospect
                AND [vcDocName] LIKE '%' + @vcInput + '%'
		UNION ALL
        SELECT  CONVERT(VARCHAR, [numGenericDocID]) + '~S' DocID,
                ISNULL([vcDocName], 'NA') + ' (' + ISNULL(C.[vcCompanyName], '') + ')' vcDocName,
                [VcFileName]
        FROM    [GenericDocuments] SD
                LEFT OUTER JOIN [DivisionMaster] D ON D.[numDivisionID] = SD.[numRecID]
                left JOIN [CompanyInfo] C ON C.[numCompanyId] = D.[numCompanyID]
        WHERE   SD.[numDomainID] = @numDomainID
                AND SD.[vcDocumentSection] = 'S'--Prospect
                AND [vcDocName] LIKE '%' + @vcInput + '%'
                
                
                
                
    END

--SELECT * FROM [AdditionalContactsInformation] WHERE [numContactId]=85839
--SELECT dbo.[fn_GetComapnyName](55882)* FROM [DivisionMaster] WHERE numdivisionID =55881
--SELECT * FROM [CompanyInfo] WHERE numcompanyid=57111