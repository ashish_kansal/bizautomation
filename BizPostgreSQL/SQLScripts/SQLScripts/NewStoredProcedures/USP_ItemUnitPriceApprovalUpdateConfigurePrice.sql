
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemUnitPriceApprovalUpdateConfigurePrice')
DROP PROCEDURE USP_ItemUnitPriceApprovalUpdateConfigurePrice
GO
CREATE PROCEDURE USP_ItemUnitPriceApprovalUpdateConfigurePrice (
@numOppId NUMERIC(18,0),
@numDomainID NUMERIC(18,0)
)
AS
BEGIN
DECLARE @returnFormattedItems VARCHAR(MAX)=''
SET @returnFormattedItems=''
DECLARE  @tempTableData TABLE
(
	ID INT IDENTITY(1, 1) primary key ,
	numItemCode NUMERIC(18,0),
	numUnitHour DECIMAL(18,2),
	monPrice DECIMAL(18,2),
	monTotAmount DECIMAL(18,2),
	monCost DECIMAL(18,2),
	numWarehouseItmsID NUMERIC(18,2),
	ItemName VARCHAR(MAX),
	numProductCost NUMERIC(18,0),
	numoppitemtCode NUMERIC(18,0)

)
DECLARE @numDivisionID AS NUMERIC(18,0)
DECLARE @numWarehouseItemID AS NUMERIC(18,0)
DECLARE @ItemName AS VARCHAR(MAX)
DECLARE @fltExchangeRate FLOAT

SELECT TOP 1 @numDivisionID=numDivisionId,@fltExchangeRate=fltExchangeRate FROM OpportunityMaster WHERE numOppId=@numOppId
INSERT INTO @tempTableData
SELECT I.numItemCode,O.numUnitHour,monPrice,monTotAmount,ISNULL(V.monCost,0) AS monCost,O.numWarehouseItmsID,I.vcItemName,I.monListPrice,O.numoppitemtCode FROM OpportunityItems As O LEFT JOIN
Item AS I ON O.numItemCode=I.numItemCode
LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
where numOppId=@numOppId AND O.bitItemPriceApprovalRequired=1

DECLARE @numItemClassification NUMERIC(18,0)
	DECLARE @numAbovePercent FLOAT
	DECLARE @numAboveField FLOAT
	DECLARE @numBelowPercent FLOAT
	DECLARE @numBelowField FLOAT
	DECLARE @numDefaultCost TINYINT
	DECLARE @bitCostApproval BIT
	DECLARE @bitListPriceApproval BIT
	DECLARE @numCostDomain INT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	DECLARE @bitCalAmtBasedonDepItems BIT
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numQuantity NUMERIC(18,0)
	DECLARE @numUnitPrice NUMERIC(18,0)
	DECLARE @numTotalAmount NUMERIC(18,0)
	DECLARE @monCost NUMERIC(18,0)
	DECLARE @numProductCost DECIMAL(18,2)=0
	DECLARE @numoppitemtCode DECIMAL(18,2)=0
	DECLARE @bitDiscountType BIT = 0
	DECLARE @fltDiscount NUMERIC(18,0) = 0
	SELECT @numCostDomain = numCost FROM Domain WHERE numDomainId = @numDomainID


DECLARE @i AS INT=1
DECLARE @RowCount AS INT=0
DECLARE @IsApprovalRequired BIT
	DECLARE @ItemAbovePrice FLOAT
	DECLARE @ItemBelowPrice FLOAT
DECLARE @numProposedPercentage AS DECIMAL(18,2)=0
SET @RowCount = (SELECT COUNT(*) FROM @tempTableData)
DECLARE @TEMPPrice TABLE
			(
				bitSuccess BIT
				,monPrice DECIMAL(20,5)
			)
WHILE (@i<=@RowCount)
BEGIN	
		SET @numProposedPercentage = 0
		SELECT 
			@numItemCode=numItemCode,
			@numQuantity=numUnitHour,
			@numUnitPrice=monPrice * (CASE WHEN ISNULL(@fltExchangeRate,0) = 0 THEN 1 ELSE @fltExchangeRate END),
			@numTotalAmount=monTotAmount,
			@monCost=monCost,
			@numWarehouseItemID = numWarehouseItmsID,
			@ItemName=ItemName,
			@numProductCost=numProductCost,
			@numoppitemtCode =numoppitemtCode
		FROM
			@tempTableData
		WHERE
			ID=@i

		SELECT 
			@numItemClassification=ISNULL(numItemClassification,0)
			,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
			,@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) 
		FROM 
			Item 
		WHERE 
			numItemCode=@numItemCode

		IF ISNULL(@numItemClassification,0) > 0 AND EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID=@numDomainID AND numListItemID=@numItemClassification)
		BEGIN
			SELECT
				@numAbovePercent = ISNULL(numAbovePercent,0)
				,@numBelowPercent = ISNULL(numBelowPercent,0)
				,@numBelowField = ISNULL(numBelowPriceField,0)
				,@numAboveField=ISNULL(@numCostDomain,1)
				,@bitCostApproval=ISNULL(bitCostApproval,0)
				,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
			FROM
				ApprovalProcessItemsClassification
			WHERE
				numDomainId=@numDomainID
				AND ISNULL(numListItemID,0)=@numItemClassification
		END
		ELSE
		BEGIN
			SELECT
				@numAbovePercent = ISNULL(numAbovePercent,0)
				,@numBelowPercent = ISNULL(numBelowPercent,0)
				,@numBelowField = ISNULL(numBelowPriceField,0)
				,@numAboveField=ISNULL(@numCostDomain,1)
				,@bitCostApproval=ISNULL(bitCostApproval,0)
				,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
			FROM
				ApprovalProcessItemsClassification
			WHERE
				numDomainId=@numDomainID
				AND ISNULL(numListItemID,0)=0
		END	


			SET @IsApprovalRequired = 0
			SET @ItemAbovePrice = 0
			SET @ItemBelowPrice = 0

			IF @numQuantity > 0
				SET @numUnitPrice = (@numTotalAmount / @numQuantity)
			ELSE
				SET @numUnitPrice = 0

			
			DELETE FROM @TEMPPrice
			IF @numAboveField > 0 AND ISNULL(@bitCostApproval,0) = 1
				BEGIN
			  
				IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 AND @bitCalAmtBasedonDepItems=1
				BEGIN
					DELETE FROM @TEMPPrice

					INSERT INTO @TEMPPrice
					(
						bitSuccess
						,monPrice
					)
					SELECT
						bitSuccess
						,monPrice
					FROM
						dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,'',0,1)
					
					IF (SELECT bitSuccess FROM @TEMPPrice) = 1
					BEGIN
						SET @ItemAbovePrice = (SELECT monPrice FROM @TEMPPrice)
					END
				END
				ELSE
				BEGIN
					IF @numAboveField = 3 -- Primaty Vendor Cost
						SELECT @ItemAbovePrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)
					ELSE IF @numAboveField = 2 -- Product & Sevices Cost
						SELECT @ItemAbovePrice = ISNULL(@numProductCost,0)
					ELSE IF @numAboveField = 1 -- Average Cost
						SELECT @ItemAbovePrice = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
				END
				IF @ItemAbovePrice > 0
				BEGIN
					IF @numUnitPrice < (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))
						SET @numProposedPercentage = (((@numUnitPrice - @ItemAbovePrice) /@ItemAbovePrice)*100)
						PRINT 'ABOVE CASE'
						--UPDATE 
						--	OpportunityItems 
						--SET 
						--	monPrice=(@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100))) ,
						--	monTotAmtBefDiscount = (@numQuantity * (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))),
						--	monTotAmount = (@numQuantity * (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))),
						--	bitDiscountType = 0,
						--	fltDiscount = 0,
						--	bitItemPriceApprovalRequired =0 
						--WHERE 
						--	numOppId=@numOppId AND 
						--	numoppitemtCode=@numoppitemtCode

						--SET @returnFormattedItems= @returnFormattedItems + '<b>' + @ItemName + '</b>' +'('+CAST(@numQuantity As VARCHAR(500))+') Proposed '+CAST(@numProposedPercentage AS VARCHAR(100))+'%, Minimum Allowed '+CAST(@numAbovePercent As VARCHAR(500))+'%, '
						--SET @IsApprovalRequired = 1
				END
				ELSE IF @ItemAbovePrice = 0
				BEGIN
					SET @IsApprovalRequired = 1
				END
			END

			IF @IsApprovalRequired = 0 AND @numBelowField > 0  AND ISNULL(@bitListPriceApproval,0) = 1
			BEGIN
				IF @numBelowField = 1 -- List Price
				BEGIN
					IF (SELECT ISNULL(charItemType,'') AS charItemType FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P' -- Inventory Item
						IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 AND @bitCalAmtBasedonDepItems=1
						BEGIN
							DELETE FROM @TEMPPrice

							INSERT INTO @TEMPPrice
							(
								bitSuccess
								,monPrice
							)
							SELECT
								bitSuccess
								,monPrice
							FROM
								dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,'',0,1)

							IF (SELECT bitSuccess FROM @TEMPPrice) = 1
							BEGIN
								SET @ItemBelowPrice = (SELECT monPrice FROM @TEMPPrice)
							END
						END
						ELSE
						BEGIN
							SELECT @ItemBelowPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
						END
					ELSE
						SELECT @ItemBelowPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
				END
				ELSE IF @numBelowField = 2 -- Price Rule
				BEGIN
					/* Check if valid price book rules exists for sales in domain */
					IF (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainId = @numDomainID AND tintRuleFor = 1 AND tintStep2 > 0 AND tintStep3 > 0) > 0
					BEGIN
						DECLARE @j INT = 0
						DECLARE @Count int = 0
						DECLARE @tempPriority INT
						DECLARE @tempNumPriceRuleID NUMERIC(18,0)
						DECLARE @numPriceRuleIDApplied NUMERIC(18,0) = 0
						DECLARE @TempTable TABLE (numID INT, numRuleID numeric(18,0), numPriority INT)

						INSERT INTO 
							@TempTable
						SELECT 
							ROW_NUMBER() OVER (ORDER BY PriceBookPriorities.Priority) AS id,
							numPricRuleID,
							PriceBookPriorities.Priority 
						FROM 
							PriceBookRules
						INNER JOIN
							PriceBookPriorities
						ON
							PriceBookRules.tintStep2 = PriceBookPriorities.Step2Value AND
							PriceBookRules.tintStep3 = PriceBookPriorities.Step3Value
						WHERE 
							PriceBookRules.numDomainId = @numDomainID AND 
							PriceBookRules.tintRuleFor = 1 AND 
							PriceBookRules.tintStep2 > 0 AND PriceBookRules.tintStep3 > 0
						ORDER BY
							PriceBookPriorities.Priority

						SELECT @Count = COUNT(*) FROM @TempTable

						/* Loop all price rule with priority */
						WHILE (@j < @count)
						BEGIN
							SELECT @tempNumPriceRuleID = numRuleID, @tempPriority = numPriority FROM @TempTable WHERE numID = (@i + 1)
					
							/* IF proprity is 9 then price rule is applied to all items and all customers. 
							So price rule must be applied to item or not.*/
							IF @tempPriority = 9
							BEGIN
								SET @numPriceRuleIDApplied = @tempNumPriceRuleID
								BREAK
							END
							/* Check if current item exists in rule. if eixts then exit loop with rule id else continie loop. if item not exist in any rule then nothing to check */
							ELSE
							BEGIN
								DECLARE @isRuleApplicable BIT = 0
								EXEC @isRuleApplicable = dbo.CheckIfPriceRuleApplicableToItem @numRuleID = @tempNumPriceRuleID, @numItemID = @numItemCode, @numDivisionID = @numDivisionID 

								IF @isRuleApplicable = 1
								BEGIN
									SET @numPriceRuleIDApplied = @tempNumPriceRuleID
									BREAK
								END
							END

							SET @j = @j + 1
						END

						/* If @numPriceRuleIDApplied > 0 Get final unit price limit after applying below rule */

						IF @numPriceRuleIDApplied > 0
						BEGIN
							SET @ItemBelowPrice = dbo.GetUnitPriceAfterPriceRuleApplication(@numPriceRuleIDApplied,@numDomainID,@numItemCode,@numQuantity,@numWarehouseItemID,@numDivisionID)
						END
					END
				END
				ELSE IF @numBelowField = 3 -- Price Level
				BEGIN
					EXEC @ItemBelowPrice = GetUnitPriceAfterPriceLevelApplication @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @isPriceRule = 0, @numPriceRuleID = 0, @numDivisionID = @numDivisionID
				END
		
				IF @numUnitPrice < (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))
				SET @numProposedPercentage = (((@ItemBelowPrice- @numUnitPrice) /@ItemBelowPrice)*100)
				PRINT 'NEW CASE'
					UPDATE 
							OpportunityItems 
						SET 
							monPrice=(@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100))) ,
							monTotAmtBefDiscount = (@numQuantity * (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))),
							monTotAmount = (@numQuantity * (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))),
							bitDiscountType = 0,
							fltDiscount = 0,
							bitItemPriceApprovalRequired =0 
						WHERE 
							numOppId=@numOppId AND 
							numoppitemtCode=@numoppitemtCode
				--SET @returnFormattedItems=@returnFormattedItems + '<b>' + @ItemName + '</b>' +'('+CAST(@numQuantity As VARCHAR(500))+') Proposed '+CAST(@numProposedPercentage AS VARCHAR(500))+'%, Minimum Allowed '+CAST(@numBelowPercent As VARCHAR(500))+'%, '
				--		SET @IsApprovalRequired = 1
			END



	SET @i=@i+1
END
	UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
    --RETURN ISNULL(@returnFormattedItems,'')
END