SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetPriceHistory')
DROP PROCEDURE USP_Item_GetPriceHistory
GO
CREATE PROCEDURE [dbo].[USP_Item_GetPriceHistory]
	@numDomainID NUMERIC(18,0),
	@tintOppType AS TINYINT,
	@numItemCode NUMERIC(18,0),
	@ClientTimeZoneOffset AS INT,
	@numDivisionID NUMERIC(18,0) = 0
AS
BEGIN
	SELECT
		TOP 30
		OpportunityMaster.numOppId,
		dbo.FormatedDateFromDate(dateadd(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintCreatedDate),@numDomainID) AS vcCreatedDate,
		vcPOppName,
		numUnitHour / dbo.fn_UOMConversion(OpportunityItems.numUOMID,OpportunityItems.numItemCode,@numDomainID,Item.numBaseUnit) AS numUnitHour,
		CAST(monPrice AS FLOAT) monPrice,
		numUOMId,
		dbo.fn_GetUOMName(numUOMId) AS vcUOMName,
		dbo.fn_UOMConversion(numUOMID,OpportunityItems.numItemCode,@numDomainID,Item.numBaseUnit) AS decUOMConversionFactor,
		(CASE WHEN ISNULL(OpportunityItems.bitDiscountType,0) = 1 THEN CONCAT(CAST(ISNULL(OpportunityItems.fltDiscount,0) AS FLOAT),' Flat Off') ELSE CONCAT(CAST(ISNULL(OpportunityItems.fltDiscount,0) AS FLOAT),'%') END) as vcDiscount,
		CAST(ISNULL(OpportunityItems.fltDiscount,0) AS FLOAT) AS fltDiscount,
		OpportunityItems.bitDiscountType
	FROM 
		OpportunityMaster
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
		AND OpportunityItems.numItemCode = @numItemCode
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	WHERE
		OpportunityMaster.numDomainId = @numDomainID
		AND tintOppType = @tintOppType
		AND (numDivisionId = @numDivisionID OR ISNULL(@numDivisionID,0) = 0)
	ORDER BY
		OpportunityMaster.numOppId DESC

END