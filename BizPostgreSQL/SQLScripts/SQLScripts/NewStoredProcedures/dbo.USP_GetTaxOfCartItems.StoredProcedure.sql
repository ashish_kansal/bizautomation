GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaxOfCartItems')
DROP PROCEDURE USP_GetTaxOfCartItems
GO
CREATE PROCEDURE [dbo].[USP_GetTaxOfCartItems]
(
    @numDomainId AS NUMERIC(9) = 0 ,
    @numDivisionId AS NUMERIC(9)=0,
    @BaseTaxOn AS Int ,
    @CookieId AS VARCHAR(100),
    @numCountryID AS NUMERIC(9,0),
    @numStateID AS NUMERIC(9,0),
    @vcCity AS VARCHAR(100),
    @vcZipPostalCode AS VARCHAR(20),
    @fltTotalTaxAmount AS FLOAT OUTPUT ,
    @numUserCntId AS NUMERIC(9,0)
)
AS
BEGIN
	Declare  @sqlQuery AS NVARCHAR(MAX) 
	SET @sqlQuery = ''
 
	CREATE TABLE #CartItems
	(
		numItemCode   NUMERIC(9,0) null,
		numTaxItemID NUMERIC(9,0) NULL ,
		numTaxID NUMERIC(18,0) NULL ,
		numUnitHour NUMERIC(18,0) NULL,
		numUOM NUMERIC(18,0) NULL,
		monTotAmount DECIMAL(20,5) NULL ,
		numCartId NUMERIC(18,0),
		bitApplicable BIT 
	)
  
	CREATE TABLE #TaxDetail  
	(
		numTaxID   numeric(9,0) ,
		numTaxItemID NUMERIC (9,0) ,
		numCountryID NUMERIC(9,0) ,
		numStateID NUMERIC(9,0),
		decTaxPercentage FLOAT,
		tintTaxType TINYINT,
		numDomainId NUMERIC(9,0),
		vcCity VARCHAR(100) ,
		vcZipPostal VARCHAR(20)
	)

	CREATE TABLE #ApplicableTaxesForDivision
	( 
		vcTaxName   VARCHAR(100) null,
		decTaxPercentage FLOAT NULL,
		tintTaxType TINYINT,
		numTaxItemID NUMERIC(9,0) ,
		numTaxID NUMERIC(18,0),
		numCountryID NUMERIC(9,0) ,
		numStateID NUMERIC(9,0) ,
		numDivisionID NUMERIC(9,0),
		bitApplicable BIT
	)
  
	CREATE TABLE #TaxAmount  
	(
		numItemCode   numeric(9,0) ,
		numTaxItemID NUMERIC (9,0) ,
		bitApplicable BIT ,
		decTaxPercentage FLOAT,
		vcTaxName VARCHAR(100),
		monTotAmount DECIMAL(20,5) ,
		bitTaxable Bit ,
		fltTaxAmount FLOAT 
	)

	-- here union  is used to remove dublicate rows 
	INSERT INTO 
		#CartItems 
    SELECT 
		IT.numItemCode , 
		IT.numTaxItemID , 
		ISNULL(IT.numTaxID,0),
		CI.numUnitHour,
		CI.numUOM,
		CI.monTotAmount, 
		CI.numCartId, 
		IT.bitApplicable 
    FROM 
		dbo.ItemTax IT 
	INNER JOIN 
		dbo.CartItems CI 
	ON 
		IT.numItemCode =  CI.numItemCode  
    WHERE  
		CI.vcCookieId = @CookieId 
		AND ((CI.numUserCntId = @numUserCntId) OR (CI.numUserCntId = 0))
        AND CI.numItemCode NOT IN (SELECT numShippingServiceItemID FROM Domain WHERE Domain.numDomainID = CI.numDomainID)
    UNION 
    SELECT 
		CI.numItemCode,
		0,
		0,
		CI.numUnitHour,
		CI.numUOM,
		CI.monTotAmount, 
		CI.numCartId, 
		1 
    FROM 
		dbo.CartItems CI 
	INNER JOIN 
		Item I 
	ON 
		CI.numItemCode = I.numItemCode  
    WHERE  
		CI.vcCookieId = @CookieId 
		AND ((CI.numUserCntId = @numUserCntId) OR (CI.numUserCntId = 0)) 
		AND  I.bitTaxable = 1          
        AND CI.numItemCode NOT IN (SELECT numShippingServiceItemID FROM Domain WHERE Domain.numDomainID = CI.numDomainID)
 
	SELECT * FROM #CartItems
 
	DECLARE @TaxPercentage AS NUMERIC(9,0)
 

	DECLARE @Temp1 TABLE
	(
		row_id int NOT NULL PRIMARY KEY IDENTITY(1,1),
		TaxItemID NUMERIC
	)

	DECLARE @RowNo as INT
	
	INSERT INTO @Temp1 
    (   
        TaxItemID
    )
	SELECT DISTINCT 
		numTaxItemID 
	FROM 
		TaxDetails 
	WHERE 
		numDomainID= @numDomainID

	SELECT * FROM @Temp1

	DECLARE @RowsToProcess int
	DECLARE @CurrentRow int

	SELECT @RowsToProcess = COUNT(*) FROM @Temp1
	SET @CurrentRow = 1
 
	WHILE @CurrentRow <= @RowsToProcess
	BEGIN   
		IF @numCountryID >0 and @numStateID >0
		BEGIN
			IF @numStateID>0        
			BEGIN 
				
			    IF EXISTS(SELECT * FROM TaxDetails TD inner JOIN @Temp1 T ON T.TaxItemID = TD.numTaxItemID WHERE numCountryID=@numCountryID and ((numStateID =@numStateID  AND 
							(1=(Case 
								when @BaseTaxOn=0 then Case When numStateID = @numStateID then 1 else 0 end --State
								when @BaseTaxOn=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
								when @BaseTaxOn=2 then Case When vcZipPostal=@vcZipPostalCode then 1 else 0 end --Zip/Postal
								else 0 end)) ))
								and numDomainID= @numDomainID and T.row_id = @CurrentRow)
				BEGIN
					INSERT INTO 
						#TaxDetail 
					SELECT TOP 1
						numTaxID, 
						numTaxItemID,  
						numCountryID, 
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId,
						vcCity,
						vcZipPostal 
					FROM 
						TaxDetails 
					WHERE 
						numCountryID=@numCountryID 
						AND ((numStateID =@numStateID  AND 
							(1=(Case 
								when @BaseTaxOn=0 then Case When numStateID = @numStateID then 1 else 0 end --State
								when @BaseTaxOn=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
								when @BaseTaxOn=2 then Case When vcZipPostal=@vcZipPostalCode then 1 else 0 end --Zip/Postal
								else 0 end)) ))
								
								and numDomainID= @numDomainID  
								and numTaxItemID  = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
				END
				ELSE IF exists(select * from TaxDetails TD inner JOIN @Temp1 T on T.TaxItemID = TD.numTaxItemID 
							where numCountryID=@numCountryID and (numStateID=@numStateID )
							and numDomainID= @numDomainID  and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and T.row_id = @CurrentRow)            
				BEGIN
					INSERT INTO
						#TaxDetail 
					SELECT  TOP 1
						numTaxID, 
						numTaxItemID,
						numCountryID,
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId, 
						vcCity,
						vcZipPostal 
					FROM 
						TaxDetails TD
					WHERE 
						numCountryID=@numCountryID 
						AND (numStateID = @numStateID)
						AND numDomainID = @numDomainID 
						AND ISNULL(vcCity,'') = '' AND ISNULL(vcZipPostal,'') = '' AND numTaxItemID = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
				END
				ELSE    
				BEGIN         
					INSERT INTO 
						#TaxDetail 
					SELECT TOP 1
						numTaxID, 
						numTaxItemID,
						numCountryID,
						numStateID,
						decTaxPercentage,
						tintTaxType,
						numDomainId,
						vcCity,
						vcZipPostal 
					FROM 
						TaxDetails TD
					WHERE 
						numCountryID=@numCountryID 
						AND numStateID=0 
						AND isnull(vcCity,'')='' 
						AND isnull(vcZipPostal,'')='' 
						AND numDomainID= @numDomainID 
						AND numTaxItemID  = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
				END  
			END                   
		END 
		ELSE IF @numCountryID >0              
		BEGIN 
			INSERT INTO 
				#TaxDetail 
			SELECT TOP 1
				numTaxID, 
				numTaxItemID,
				numCountryID,
				numStateID,
				decTaxPercentage,
				tintTaxType,
				numDomainId,
				vcCity,
				vcZipPostal 
			FROM 
				TaxDetails
			WHERE 
				numCountryID=@numCountryID 
				AND numStateID=0 
				AND ISNULL(vcCity,'')='' 
				AND ISNULL(vcZipPostal,'')='' 
				AND numDomainID= @numDomainID 
				AND numTaxItemID  = (Select T.TaxItemID from @Temp1 T where T.row_id = @CurrentRow)   
		END 

		SET @CurrentRow = @CurrentRow + 1  
	END

	SELECT * FROM #TaxDetail


	IF ISNULL(@numDivisionID,0) = 0
	BEGIN
		INSERT INTO 
			#ApplicableTaxesForDivision
		SELECT 
			CASE WHEN TD.numTaxItemID = 0 THEN 'Sales Tax' ELSE TI.vcTaxName END AS vcTaxName
			,TD.decTaxPercentage
			,TD.tintTaxType
			,TD.numTaxItemID 
			,TD.numTaxID
			,TD.numCountryID
			,TD.numStateID
			,0
			,1
		FROM
			#TaxDetail TD
		LEFT JOIN 
			dbo.TaxItems TI 
		ON 
			TD.numDomainId = TI.numDomainID 
			AND TD.numTaxItemID = TI.numTaxItemID
		WHERE 
			TD.numDomainId= @numDomainID
		UNION
		SELECT 'CRV',TD.decTaxPercentage,TD.tintTaxType,1,numTaxID,TD.numCountryID,TD.numStateID,@numDivisionID,1 FROM #TaxDetail TD WHERE numTaxItemID = 1
	END
	ELSE
	BEGIN
		INSERT INTO 
			#ApplicableTaxesForDivision
		SELECT 
			CASE WHEN DTD.numTaxItemID = 0 THEN 'Sales Tax' ELSE TI.vcTaxName END AS vcTaxName
			,TD.decTaxPercentage
			,TD.tintTaxType
			,DTD.numTaxItemID 
			,TD.numTaxID
			,TD.numCountryID
			,TD.numStateID
			,DTD.numDivisionID
			,DTD.bitApplicable
		FROM
			dbo.DivisionTaxTypes DTD 
		LEFT JOIN 
			#TaxDetail TD 
		ON 
			DTD.numTaxItemID = TD.numTaxItemID
		LEFT JOIN 
			dbo.TaxItems TI 
		ON 
			TD.numDomainId = TI.numDomainID 
			AND TD.numTaxItemID = TI.numTaxItemID
		WHERE 
			ISNULL(DTD.bitApplicable,0)=1 
			AND numDivisionID=@numDivisionID 
			AND TD.numDomainId= @numDomainID
		UNION
		SELECT 'CRV',TD.decTaxPercentage,TD.tintTaxType,1,numTaxID,TD.numCountryID,TD.numStateID,@numDivisionID,1 FROM #TaxDetail TD WHERE numTaxItemID = 1
	END

	SELECT * FROM  #ApplicableTaxesForDivision
  
	INSERT INTO 
		#TaxAmount
    SELECT 
		I.numItemCode, 
		TT.numTaxItemID,
		TT.bitApplicable, 
		TT1.decTaxPercentage,
		TT1.vcTaxName,
		TT.monTotAmount,
		I.bitTaxable, 
		CASE 
			WHEN TT1.tintTaxType = 2 --FLAT AMOUNT
			THEN (CONVERT(FLOAT, TT1.decTaxPercentage) * (CONVERT(FLOAT,numUnitHour) * dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,@numDomainID,ISNULL(TT.numUOM, 0))))
			ELSE --PERCENT
				((CONVERT(FLOAT, TT.monTotAmount)   * CONVERT(FLOAT, TT1.decTaxPercentage))/100) 
		END AS fltTaxAmount  
	FROM 
		#CartItems TT
	INNER JOIN 
		#ApplicableTaxesForDivision TT1 
	ON 
		TT.numTaxItemID = TT1.numTaxItemID 
		AND 1 = (CASE WHEN TT.numTaxItemID = 1 THEN (CASE WHEN TT.numTaxID = TT1.numTaxID THEN 1 ELSE 0 END) ELSE 1 END)
    INNER JOIN 
		Item I 
	ON 
		I.numItemCode = TT.numItemCode  
	WHERE 
		I.bitTaxable = 1 ORDER BY numItemCode
  
	SELECT numItemCode,numTaxItemID ,bitApplicable  ,fltTaxAmount AS fltTaxAmount  FROM  #TaxAmount
	SELECT numItemCode ,SUM(fltTaxAmount) AS fltTaxAmount  FROM  #TaxAmount GROUP BY numItemCode 
  
  
	IF EXISTS(SELECT * FROM #TaxAmount )
	BEGIN
		SELECT @fltTotalTaxAmount =  SUM(fltTaxAmount)  FROM  #TaxAmount  WHERE bitTaxable = 1 
	END

	DROP TABLE #CartItems
	DROP TABLE #ApplicableTaxesForDivision
	DROP TABLE #TaxDetail
	DROP TABLE #TaxAmount
 
 END
GO

--exec USP_GetTaxOfCartItems @numDomainId=1,@numDivisionId=1,@BaseTaxOn=2,@CookieId='bbe5f0f4-2791-461f-a5fa-d7d33cf2f7f0',@numCountryID=600,@numStateID=112,@vcCity='Ahmedabad',@vcZipPostalCode='361305',@fltTotalTaxAmount=0,@numUserCntId=1
