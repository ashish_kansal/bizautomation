GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'USP_GetDealsList1_export') AND type in (N'P', N'PC'))
DROP PROCEDURE USP_GetDealsList1_export
GO 
CREATE  Procedure USP_GetDealsList1_export
 @numDomainID AS NUMERIC(9) = 0,
 @numRecOwner AS NUMERIC(18,0)=0,
 @numAssignTO AS NUMERIC(18,0)=0,
 @vcTimeLine AS VARCHAR(50)=''
as 
BEGIN
	DECLARE @dtStartDate DATETIME 
	DECLARE @dtEndDate AS DATETIME 
	if(@vcTimeline <>'')
	BEGIN 
		Set @dtStartDate = LEFT(@vcTimeLine,CHARINDEX('-',@vcTimeLine)-1)
		set @dtEndDate = RIGHT(@vcTimeLine,CHARINDEX('-',@vcTimeLine)-1)
	END 

	SELECT    
        opp.numrecowner        AS RecordOwnerId, 
		opp.numAssignedTo        AS AssignToId, 
        opp.numoppid, 
        opp.bintcreateddate as CreatedDate,
        opp.vcPOppName as [Name],
        isnull(ADC1.vcFirstname,'')+' '+isnull(ADC1.vcLastName,'') as RecordOwner,
		isnull(ADC2.vcFirstname,'')+' '+isnull(ADC2.vcLastName,'') as AssignTo,
        Isnull( 
        ( 
                SELECT Sum(Isnull(monamountpaid,0)) 
                FROM   opportunitybizdocs BD 
                WHERE  bd.numoppid = opp.numoppid 
                AND    Isnull(bitauthoritativebizdocs,0)=1),0) AmtPaid, 
        Isnull( 
        ( 
                SELECT Sum(Isnull(mondealamount,0)) 
                FROM   opportunitybizdocs BD 
                WHERE  bd.numoppid = opp.numoppid 
                AND    Isnull(bitauthoritativebizdocs,0)=1),0) as InvoiceGrandTotal, 
        opp.mondealamount as SalesOrderDealAmount
          
	FROM       opportunitymaster Opp 
	LEFT JOIN  additionalcontactsinformation ADC1 
	ON         opp.numRecOwner = ADC1.numcontactid 
	LEFT JOIN  additionalcontactsinformation ADC2 
	ON         opp.numAssignedTo = ADC2.numcontactid 

	WHERE      opp.numdomainid=@numDomainID
	AND        opp.tintoppstatus=1 

	AND        opp.tintopptype= 1 
	AND        (opp.numrecowner=@numRecOwner or  @numRecOwner=0)
	AND        (opp.numAssignedTo=@numAssignTO or @numAssignTO = 0) 
	AND        opp.bintcreateddate >= @dtStartDate--'04/01/2019' 
	AND        opp.bintcreateddate <= @dtEndDate -- '04/27/2020' 
	AND        Charindex('All', dbo.Checkorderinvoicingstatus(opp.numoppid,opp.numdomainid)) > 0 
	ORDER BY   opp.numOppId ASC 
END
GO