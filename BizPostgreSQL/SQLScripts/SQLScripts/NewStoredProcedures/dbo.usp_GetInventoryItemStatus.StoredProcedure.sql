
/****** Object:  StoredProcedure [dbo].[usp_GetInventoryItemStatus]    Script Date: 06/01/2009 23:50:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Chintan Prajapati
-- exec [dbo].[usp_GetInventoryItemStatus] 1,null,99
-- exec [dbo].[usp_GetInventoryItemStatus] 1,28,43
-- exec [dbo].[usp_GetInventoryItemStatus] 1,24,43
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetInventoryItemStatus')
DROP PROCEDURE usp_GetInventoryItemStatus
GO
CREATE PROCEDURE [dbo].[usp_GetInventoryItemStatus]
           @numDomainID    AS NUMERIC(9)  = 0,
           @numWareHouseID AS NUMERIC(9)  = null,
           @numItemCode    AS NUMERIC(9)
AS
  DECLARE  @bitShowInStock  AS BIT
  DECLARE  @bitShowQuantity  AS BIT
  --DECLARE  @tintColumns  AS TINYINT
  SET @bitShowInStock = 0
  SET @bitShowQuantity = 0
  --SET @tintColumns = 1
  SELECT @bitShowInStock = Isnull(bitShowInStock,0),
         @bitShowQuantity = Isnull(bitShowQOnHand,0)
         --@tintColumns = Isnull(tintItemColumns,1)
  FROM   eCommerceDTL
  WHERE  numDomainID = @numDomainID
  
  
SELECT (CASE 
          WHEN charItemType <> 'S' THEN (CASE 
                                           WHEN @bitShowInStock = 1 THEN (CASE 
                                                                            WHEN numWareHouseID IS NULL THEN 'Out Of Stock'
                                                                            WHEN bitAllowBackOrder = 1 THEN 'In Stock'
                                                                                                              + (CASE 
                                                                                                                   WHEN @bitShowQuantity = 1 THEN '('
                                                                                                                                                    + CONVERT(VARCHAR(20),X.OnHand)
                                                                                                                                                    + ')'
                                                                                                                   ELSE ''
                                                                                                                 END)
                                                                            ELSE (CASE 
                                                                                    WHEN X.OnHand > 0 THEN 'In Stock'
                                                                                                             + (CASE 
                                                                                                                  WHEN @bitShowQuantity = 1 THEN '('
                                                                                                                                                   + CONVERT(VARCHAR(20),X.OnHand)
                                                                                                                                                   + ')'
                                                                                                                  ELSE ''
                                                                                                                END)
                                                                                    ELSE 'Out Of Stock'
                                                                                  END)
                                                                          END)
                                           ELSE ''
                                         END)
          ELSE ''
        END) AS InStock,
       X.OnHand,
       numWareHouseID
FROM   (SELECT   numItemCode,
                 Isnull(SUM(numOnHand),0) AS OnHand,
                 charItemType,
                 bitAllowBackOrder,
                 numWareHouseID
        FROM     Item
                 LEFT JOIN WareHouseItems W
                   ON W.numItemID = numItemCode
        WHERE    Item.numDomainID = @numDomainID
				 AND W.numDomainID=@numDomainID
                 AND numItemCode = @numItemCode
                 AND (numWareHouseID = @numWareHouseID
                       OR @numWareHouseID IS NULL)
        GROUP BY numItemCode,
                 charItemType,
                 bitAllowBackOrder,
                 numWareHouseID) X
		ORDER BY X.OnHand DESC
		