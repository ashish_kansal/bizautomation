GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageOppItemSerialNo')
DROP PROCEDURE USP_ManageOppItemSerialNo
GO
CREATE PROCEDURE USP_ManageOppItemSerialNo
      @strItems TEXT
AS 
BEGIN
	DECLARE @hDoc AS INT                                            
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems
         
	DECLARE @numTempLotQty NUMERIC(18,0)
	DECLARE @numTempWareHouseItmsDTLID NUMERIC(18,0)
	DECLARE @numTempFromWarehouseItemID NUMERIC(18,0)
	DECLARE @numTempToWarehouseItemID NUMERIC(18,0)
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT

	SELECT 
		X.*
		,ROW_NUMBER() OVER( order by X.numWareHouseItemID) AS ROWNUMBER
	INTO 
		#TempTable
	FROM 
	(
		SELECT 
			vcSerialNo
			,numWareHouseItemID
			,numOppId
			,numOppItemId
			,numQty
		FROM 
			OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
		WITH 
			(vcSerialNo varchar(50), numWareHouseItemID NUMERIC(9), numOppId NUMERIC(9),numOppItemId NUMERIC(9),numQty NUMERIC(9))
    ) X

	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @minROWNUMBER INT
	DECLARE @maxROWNUMBER INT
	DECLARE @bitStockTransfer BIT
	DECLARE @numWareHouseItmsDTLID NUMERIC(9)
	DECLARE @numWareHouseItemID NUMERIC(9)
	DECLARE @numTempItemCode NUMERIC(18,0)
	DECLARE @vcTempSerialNo VARCHAR(100)

	SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTable

	WHILE  @minROWNUMBER <= @maxROWNUMBER
	BEGIN
		SELECT @numWareHouseItemID=numWareHouseItemID,@numOppID=numOppID,@numOppItemID=numOppItemId,@numTempLotQty=numQty FROM #TempTable X WHERE X.ROWNUMBER=@minROWNUMBER
		SELECT @vcTempSerialNo=X.vcSerialNo FROM #TempTable X WHERE X.ROWNUMBER=@minROWNUMBER 
		SELECT @bitSerial=ISNULL(bitSerialized,0),@numTempItemCode=numItemCode,@bitLot=ISNULL(bitLotNo,0) FROM Item WHERE numItemCode IN (SELECT numItemID FROM WareHouseItems WHERE numWareHouseItemID=@numWareHouseItemID)
		SELECT @bitStockTransfer=ISNULL(bitStockTransfer,0) FROM OpportunityMaster WHERE numOppId=@numOppID

		IF @bitStockTransfer = 1
		BEGIN
			IF (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID IN (SELECT ISNULL(numWareHouseItemID,0) FROM WareHouseItems WHERE numItemID=@numTempItemCode) AND LOWER(vcSerialNo)=LOWER(@vcTempSerialNo)) = 0
			BEGIN
				RAISERROR('INVALID_SERIAL_NO',16,1)
				RETURN
			END

			SELECT @numTempFromWarehouseItemID=numWarehouseItmsID,@numTempToWarehouseItemID=numToWarehouseItemID FROM OpportunityItems WHERE numOppId=@numOppID AND numoppitemtCode=@numOppItemID
			SELECT TOP 1 @numTempWareHouseItmsDTLID=numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempFromWarehouseItemID AND vcSerialNo=@vcTempSerialNo
			IF @bitLot=1 
			BEGIN
				IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempFromWarehouseItemID AND numQty >= @numTempLotQty AND vcSerialNo=@vcTempSerialNo)
				BEGIN
					UPDATE 
						WareHouseItmsDTL 
					SET 
						numQty = numQty - @numTempLotQty
					WHERE 
						numWareHouseItemID=@numTempFromWarehouseItemID 
						AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID

					INSERT INTO WareHouseItmsDTL
					(
						numWareHouseItemID,
						vcSerialNo,
						numQty,
						dExpirationDate,
						bitAddedFromPO
					)
					SELECT
						@numTempToWarehouseItemID,
						vcSerialNo,
						@numTempLotQty,
						dExpirationDate,
						bitAddedFromPO
					FROM 
						WareHouseItmsDTL
					WHERE
						numWareHouseItemID=@numTempFromWarehouseItemID AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID
				END
				ELSE
				BEGIN
					RAISERROR('INSUFFICIENT LOT QUANTITY',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempFromWarehouseItemID AND vcSerialNo=@vcTempSerialNo AND numQty = 1)
				BEGIN
					PRINT @numTempFromWarehouseItemID
					PRINT @numTempToWarehouseItemID
					PRINT @numTempWareHouseItmsDTLID
					UPDATE 
						WareHouseItmsDTL 
					SET 
						numWareHouseItemID=@numTempToWarehouseItemID
					WHERE 
						numWareHouseItemID=@numTempFromWarehouseItemID 
						AND numWareHouseItmsDTLID=@numTempWareHouseItmsDTLID
				END
				ELSE
				BEGIN
					RAISERROR('INSUFFICIENT SERIAL QUANTITY',16,1)
					RETURN
				END
			END  
		END
		ELSE
		BEGIN
			IF @bitSerial = 1 AND (SELECT COUNT(*) FROM WareHouseItmsDTL WHERE numWareHouseItemID IN (SELECT ISNULL(numWareHouseItemID,0) FROM WareHouseItems WHERE numItemID=@numTempItemCode) AND LOWER(vcSerialNo)=LOWER(@vcTempSerialNo)) > 0
			BEGIN
				RAISERROR('DUPLICATE_SERIAL_NO',16,1)
				RETURN
			END

   			INSERT INTO WareHouseItmsDTL
			(
				numWareHouseItemID
				,vcSerialNo
				,numQty
				,bitAddedFromPO
			)  
			SELECT 
				X.numWareHouseItemID
				,X.vcSerialNo
				,X.numQty
				,1 
			FROM 
				#TempTable X 
			WHERE 
				X.ROWNUMBER=@minROWNUMBER

		    
			SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()
		
			INSERT INTO OppWarehouseSerializedItem
			(
				numWarehouseItmsDTLID
				,numOppId
				,numOppItemId
				,numWarehouseItmsID
				,numQty
			)  
			SELECT 
				@numWareHouseItmsDTLID
				,X.numOppId
				,X.numOppItemId
				,X.numWareHouseItemID
				,X.numQty 
			FROM 
				#TempTable X 
			WHERE 
				X.ROWNUMBER=@minROWNUMBER
		END

		   	 
		SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTable WHERE  [ROWNUMBER] > @minROWNUMBER
	END	

	DROP TABLE #TempTable
	EXEC sp_xml_removedocument @hDoc
END
                      