
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertEmailMaster]    Script Date: 11/02/2011 12:46:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InsertEmailMaster')
DROP PROCEDURE USP_InsertEmailMaster
Go
CREATE  PROCEDURE [dbo].[USP_InsertEmailMaster]
    @vcEmailId VARCHAR(2000),
    @numDomainId NUMERIC(18)
AS --DECLARE @vcEmailId AS VARCHAR(2000)
    DECLARE @Email AS VARCHAR(200)
    DECLARE @EmailName AS VARCHAR(200)
    DECLARE @EmailAdd AS VARCHAR(200)
    DECLARE @StartPos AS INTEGER
    DECLARE @EndPos AS INTEGER
    DECLARE @numEmailid AS NUMERIC(9)
--DECLARE @numDomainId AS NUMERIC(18, 0)
--DECLARE @numEmailId AS NUMERIC(18,0)
    DECLARE @vcNewEmailId AS VARCHAR(2000)
--SET @numDomainId = NULL
--SET @vcNewEmailId ='|pinkal.patel@bizautomation.com,Pinkal Patel|pinkalpatel2007@rediff.com,Pinkal Patel|pinkalpatel2099@rediff.com,'
--SET @vcEmailId = '|pinkal.patel@bizautomation.com,Pinkal Patel|pinkalpatel2007@rediff.com,Pinkal Patel|pinkalpatel2099@rediff.com,'
    SET @StartPos = 0
    SET @vcEmailId = @vcEmailId+'#^#'
    IF @vcEmailId <> '' 
        BEGIN
            WHILE CHARINDEX('#^#', @vcEmailId) > 0	
                BEGIN
                    SET @Email = LTRIM(RTRIM(SUBSTRING(@vcEmailId, 1,
                                                       CHARINDEX('#^#', @vcEmailId, 0)+2)))
					print @Email
                    SET @numEmailid = 0                                                             
                    IF CHARINDEX('$^$', @Email) > 0 
                        BEGIN
                            SET @EmailName = LTRIM(RTRIM(SUBSTRING(@Email, 0, CHARINDEX('$^$', @Email))))
                            SET @EmailAdd = LTRIM(RTRIM(SUBSTRING(@Email, LEN(@EmailName) + 1, CHARINDEX('$^$', @Email) + LEN(@Email) - 1)))
                            SET @EmailAdd = REPLACE(@Emailadd, '$^$', '')
                            SET @EmailAdd = REPLACE(@Emailadd, '#^#', '')
						---------Start Insert Into Email master-----------------------------------
                            SELECT  @numEmailid = COUNT(numEmailId)
                            FROM    dbo.EmailMaster
                            WHERE   vcEmailID = @EmailAdd
                                    AND numDomainId = @numDomainId
                        --PRINT @numEmailid
                            IF @numEmailid = 0 
                                BEGIN
									Declare @numcontactid numeric(9);
									select top 1 @numcontactid=numcontactid from AdditionalContactsInformation where vcEmail = @EmailAdd
													AND numDomainId = @numDomainId

                                    INSERT  INTO EmailMaster
                                            (
                                              vcEmailID,
                                              vcName,
                                              numDomainId,numcontactid
                                            )
                                    VALUES  (
                                              @EmailAdd,
                                              @EmailName,
                                              @numDomainId,@numcontactid
                                            )
                                END
							Else
								BEGIN
									update EmailMaster set vcName=@EmailName  WHERE vcEmailID = @EmailAdd
                                    AND numDomainId = @numDomainId
								END
                        ---------End Insert Into Email master-----------------------------------
                      --  PRINT 'Email Name :' + @EmailName
                      --- PRINT 'EmailAdd :' + @EmailAdd
                        --Insert Record into Email History Table
                        END
                   
                    SET @vcEmailId = LTRIM(RTRIM(SUBSTRING(@vcEmailId,
                                                           CHARINDEX('#^#', @vcEmailId)
                                                           + 3,
                                                           LEN(@vcEmailId))))
				                                                       
	
                END
    
        END
    
               
                 