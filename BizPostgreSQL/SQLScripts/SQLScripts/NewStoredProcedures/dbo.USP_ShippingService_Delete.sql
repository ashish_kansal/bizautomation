GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Priya Sharma 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingService_Delete')
DROP PROCEDURE USP_ShippingService_Delete
GO
CREATE PROCEDURE  [dbo].[USP_ShippingService_Delete]  
@numShippingServiceID AS NUMERIC(18,0)=0,  
@numDomainID AS NUMERIC(9)=0  
AS  
BEGIN
	DELETE FROM ShippingService WHERE numShippingServiceID=@numShippingServiceID and numDomainID=@numDomainID
END
GO
