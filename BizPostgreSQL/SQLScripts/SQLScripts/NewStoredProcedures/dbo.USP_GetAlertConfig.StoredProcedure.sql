GO
/****** Object:  StoredProcedure [dbo].[USP_GetAlertConfig]    Script Date: 11/02/2011 12:42:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAlertConfig')
DROP PROCEDURE USP_GetAlertConfig
GO
CREATE  PROC [dbo].[USP_GetAlertConfig] 
    
    @numDomainId numeric(18, 0),
    @numContactId numeric(9, 0)
    
AS 

SELECT * FROM [dbo].[AlertConfig] WHERE numDomainId = @numDomainId AND numContactId = @numContactId
 
