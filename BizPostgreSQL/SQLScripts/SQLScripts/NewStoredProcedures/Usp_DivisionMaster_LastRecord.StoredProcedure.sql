GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_DivisionMaster_LastRecord' ) 
    DROP PROCEDURE Usp_DivisionMaster_LastRecord
GO

-- =============================================  
-- Author:  <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,17thJan2014>  
-- Description: <Description,,To Get last Id of Divison Master>  
-- =============================================  
CREATE PROCEDURE Usp_DivisionMaster_LastRecord  
 -- Add the parameters for the stored procedure here  
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
  declare @DivisionId int  
  
  select  @DivisionId= IDENT_CURRENT( 'divisionmaster' )  
  
  select @DivisionId+1 as LastDivision  
  
END           
                    
                    
                      
   


