GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_ReceiveOnlyItems')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_ReceiveOnlyItems
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_ReceiveOnlyItems]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @hDocItem INT

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,OppID NUMERIC(18,0)
		,WOID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
		,numQtyToReceive FLOAT
		,IsSuccess BIT
		,ErrorMessage VARCHAR(300)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		OppID
		,WOID
		,OppItemID
		,numQtyToReceive
		,IsSuccess
	)
	SELECT
		ISNULL(OppID,0)
		,ISNULL(WOID,0)
		,ISNULL(OppItemID,0)
		,QtyToReceive
		,IsSuccess
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		OppID NUMERIC(18,0),
		WOID NUMERIC(18,0),
		OppItemID NUMERIC(18,0),
		QtyToReceive FLOAT,
		IsSuccess BIT
	)

	EXEC sp_xml_removedocument @hDocItem 

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numWOID NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @numQtyToReceive FLOAT
	SELECT @iCount = COUNT(*) FROM @Temp

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numOppID = OppID
			,@numWOID=WOID
			,@numOppItemID = ISNULL(OppItemID,0)
			,@numQtyToReceive = ISNULL(numQtyToReceive,0)
		FROM 
			@Temp 
		WHERE 
			ID = @i

		IF @numWOID	> 0
		BEGIN
			IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOID AND ISNULL(numQtyBuilt,0) >= (ISNULL(numQtyReceived,0) + @numQtyToReceive))
			BEGIN
				UPDATE
					WorkOrder
				SET
					numQtyReceived = ISNULL(numQtyReceived,0) + @numQtyToReceive
				WHERE
					numDomainID=@numDomainID 
					AND numWOId=@numWOID

				UPDATE 
					@Temp
				SET
					IsSuccess = 1
				WHERE
					ID=@i
			END
			ELSE
			BEGIN
				UPDATE 
					@Temp
				SET
					IsSuccess = 0
					,ErrorMessage = 'Received quantity can not be greater then quantity built'
				WHERE
					ID=@i
			END
		END
		ELSE IF @numOppItemID > 0
		BEGIN
			IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND numoppitemtCode=@numOppItemID AND ISNULL(numUnitHour,0) >= (ISNULL(numQtyReceived,0) + @numQtyToReceive))
			BEGIN
				UPDATE
					OpportunityItems
				SET
					numQtyReceived = ISNULL(numQtyReceived,0) + @numQtyToReceive
				WHERE
					numOppId=@numOppID 
					AND numoppitemtCode=@numOppItemID

				UPDATE 
					@Temp
				SET
					IsSuccess = 1
				WHERE
					ID=@i
			END
			ELSE
			BEGIN
				UPDATE 
					@Temp
				SET
					IsSuccess = 0
					,ErrorMessage = 'Received quantity can not be greater than ordered quantity'
				WHERE
					ID=@i
			END
		END
		ELSE
		BEGIN
			UPDATE
				OpportunityItems
			SET
				numQtyReceived = numUnitHour
			WHERE
				numOppId=@numOppID 
				AND ISNULL(OpportunityItems.bitDropShip,0)=0
				AND ISNULL(numUnitHour,0) <> ISNULL(numQtyReceived,0)

			UPDATE 
				@Temp
			SET
				IsSuccess = 1
			WHERE
				ID=@i
		END
		

		SET @i = @i + 1
	END

	UPDATE
		OM
	SET
		bintModifiedDate=GETUTCDATE()
		,numModifiedBy = @numUserCntID
	FROM
		OpportunityMaster OM
	INNER JOIN
		@Temp T1
	ON
		OM.numOppId = T1.OppID
		AND T1.IsSuccess = 1
	WHERE
		OM.numDomainId = @numDomainID

	SELECT * FROM @Temp
COMMIT
END TRY
BEGIN CATCH
DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END
GO