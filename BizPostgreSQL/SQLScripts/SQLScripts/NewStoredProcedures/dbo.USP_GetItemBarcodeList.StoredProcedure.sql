SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemBarcodeList')
DROP PROCEDURE USP_GetItemBarcodeList
GO
CREATE PROCEDURE [dbo].[USP_GetItemBarcodeList]                              
@numDomainID as numeric(9)=0,    
@strItemIdList as text,
@bitAllItems as bit=0,
@tinyBarcodeBasedOn tinyint=0
as                
      
Create table #tempTable (                  
  numItemCode numeric(9)                                      
  )  

insert into #tempTable select Item FROM dbo.DelimitedSplit8K(@strItemIdList,',')

declare @str1 as varchar(8000);SET @str1=''               

declare @str as varchar(8000);SET @str=''' '''               

--Create a Temporary table to hold data                                                            
create table #tempCustTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9),
 fld_label varchar(50),
fld_type varchar(50)                                                         
 )                         
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        

If @tinyBarcodeBasedOn=0 --UPC/Barcode
BEGIN
                        
insert into #tempCustTable(numCusFlDItemID,fld_label,fld_type)                                                            
select distinct numOppAccAttrID,fld_label,fld_type from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID=Fld_id where numDomainID=@numDomainID and tintType=2 
            
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempCustTable                         
                         
 while @ID>0                        
 begin    
	IF @ID=1  SET @str='''' + @fld_label
	Else   SET @str= @str + ' + ''  ' + @fld_label  
               
	set @str=@str+ ':'' + dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,numWareHouseItemID,0,'''+@fld_type+''')'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempCustTable                         
   where ID >@ID                        
   if @@rowcount=0 set @ID=0                        
 end 

set @str1 ='select I.numItemCode,I.vcItemName,I.numBarCodeId,'''' as vcWareHouse,'''' as vcAttribute 
			from Item I 
			join #tempTable T on I.numItemCode = T.numItemCode 
			where I.numDomainID='+convert(varchar(15),@numDomainID) + ' 
			and isnull(numItemGroup,0)=0 
			and I.numBarCodeId is not null 
			and len(I.numBarCodeId)>1 
 
			Union

			select I.numItemCode,I.vcItemName,WI.vcBarCode as numBarCodeId,W.vcWareHouse,' + @str + ' as vcAttribute  
			from Item I 
			join #tempTable T on I.numItemCode=T.numItemCode
			join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and I.numItemCode=WI.numItemID  
			join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
			where I.numDomainID='+convert(varchar(15),@numDomainID) + ' 
			and isnull(numItemGroup,0)>0 
			and WI.vcBarCode is not null 
			and len(WI.vcBarCode)>1'


END

ELSE If @tinyBarcodeBasedOn=1 --SKU
	BEGIN
	set @str1 ='select I.numItemCode,I.vcItemName,I.vcSKU as numBarCodeId,'''' as vcWareHouse,'''' as vcAttribute from Item I join #tempTable T on I.numItemCode=T.numItemCode 
	where I.numDomainID='+convert(varchar(15),@numDomainID) + ' and I.vcSKU is not null and len(I.vcSKU)>1'
	END

ELSE If @tinyBarcodeBasedOn=2 --Vendor Part#
	BEGIN
	set @str1 ='select I.numItemCode,I.vcItemName,Vendor.vcPartNo as numBarCodeId,com.vcCompanyName as vcWareHouse,'''' as vcAttribute
	from Vendor 
	join Item I on I.numItemCode= Vendor.numItemCode
	join #tempTable T on I.numItemCode=T.numItemCode  
	join divisionMaster div on div.numdivisionid=Vendor.numVendorid        
	join companyInfo com on com.numCompanyID=div.numCompanyID        
	WHERE I.numDomainID='+convert(varchar(15),@numDomainID) + ' and Vendor.numDomainID='+convert(varchar(15),@numDomainID) + ' 
	and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>1'
	END

ELSE If @tinyBarcodeBasedOn=3 --Serial #/LOT #
	BEGIN
	                        
	insert into #tempCustTable(numCusFlDItemID,fld_label,fld_type)                                                            
	select distinct numOppAccAttrID,fld_label,fld_type from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID=Fld_id where numDomainID=@numDomainID and tintType=2 
	            
	 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempCustTable                         
	                         
	 while @ID>0                        
	 begin    
		IF @ID=1  SET @str='''' + @fld_label
		Else   SET @str= @str + ' + ''  ' + @fld_label  
	               
		set @str=@str+ ':'' + dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,numWareHouseItmsDTLID,0,'''+@fld_type+''')'                                        
	                          
	   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempCustTable                         
	   where ID >@ID                        
	   if @@rowcount=0 set @ID=0                        
	 end 

	set @str1 ='select I.numItemCode,I.vcItemName,WDTL.vcSerialNo as numBarCodeId,W.vcWareHouse,' + @str + ' as vcAttribute 
	from Item I join #tempTable T on I.numItemCode=T.numItemCode
	join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and I.numItemCode=WI.numItemID  
	join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
	join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=WI.numWareHouseItemID    
	where I.numDomainID='+convert(varchar(15),@numDomainID) + ' and (I.bitSerialized=1 or I.bitLotNo=1) and WDTL.vcSerialNo is not null and len(WDTL.vcSerialNo)>1
	and ISNULL(WDTL.numQty,0) > 0 '

	END

ELSE If @tinyBarcodeBasedOn = 4 --Item Code
	BEGIN
	set @str1 ='SELECT I.numItemCode,I.vcItemName,I.numItemCode AS numBarCodeId,'''' AS vcWareHouse,'''' AS vcAttribute 
				FROM Item I JOIN #tempTable T ON I.numItemCode = T.numItemCode 
				WHERE I.numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID)
	END


print(@str1)           
exec (@str1)  

drop table #tempCustTable
drop table  #tempTable
GO
