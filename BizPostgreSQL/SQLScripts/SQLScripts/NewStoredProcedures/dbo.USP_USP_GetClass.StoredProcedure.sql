GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetClass' ) 
    DROP PROCEDURE USP_GetClass
GO
CREATE PROCEDURE USP_GetClass
    @numDomainID NUMERIC,
    @tintMode AS TINYINT,
    @numClassID AS NUMERIC=NULL
AS
BEGIN
    IF @tintMode = 0
        BEGIN
				IF NOT EXISTS(SELECT * FROM dbo.ClassDetails WHERE numDomainID=@numDomainID)
				BEGIN
					SELECT  CAST(0 AS NUMERIC) numParentClassID,
							numListItemID numChildClassID,
							ISNULL(vcData, '') ClassName
					FROM    dbo.ListDetails
					WHERE   numDomainID = @numDomainID
							AND numListID = 381
							UNION 
							SELECT null,0,'Root'
--					ORDER BY sintOrder
					
				END
				ELSE
				BEGIN
					SELECT numID,
						   numParentClassID,
						   numChildClassID,
						   dbo.fn_GetListItemName(numChildClassID) AS ClassName
					FROM
						dbo.ClassDetails CD 
						WHERE CD.numDomainID=@numDomainID AND CD.numChildClassID IN (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=381 AND numDomainID=@numDomainID)
						AND ((CD.numParentClassID=0) OR (CD.numParentClassID IN (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=381 AND numDomainID=@numDomainID) ) )
						UNION 
							SELECT 0,null,0,'Root'
						UNION 
					SELECT  0,CAST(0 AS NUMERIC) numParentClassID,
							numListItemID numChildClassID,
							ISNULL(vcData, '') ClassName
					FROM    dbo.ListDetails
					WHERE   numDomainID = @numDomainID
							AND numListID = 381 AND numListItemID NOT IN (SELECT numChildClassID FROM dbo.ClassDetails WHERE numDomainID=@numDomainID)
				END
        END
        
     IF @tintMode =1 
     BEGIN
     
			IF NOT EXISTS(SELECT * FROM dbo.ClassDetails WHERE numDomainID=@numDomainID)
			BEGIN
				SELECT  CAST(0 AS NUMERIC) numParentClassID,
						numListItemID numChildClassID,
						ISNULL(vcData, '') ClassName
				FROM    dbo.ListDetails
				WHERE   numDomainID = @numDomainID
						AND numListID = 381
				ORDER BY sintOrder
				
			END
			ELSE 
			BEGIN
							CREATE TABLE #Temp
 					(
 					numID NUMERIC,
 					numParentClassID NUMERIC,
 					numChildClassID NUMERIC,
 					ClassName VARCHAR(100),
 					numDomainID NUMERIC
 					)
		 			
		 			INSERT INTO #Temp 
		 			(numID,numParentClassID,numChildClassID,ClassName,numDOmainID) 
		 			SELECT X.numID,X.numParentClassID,X.numChildClassID,X.ClassName,X.numDomainID  
		 			FROM (SELECT numID,
							numParentClassID,
							numChildClassID,
							dbo.fn_GetListItemName(numChildClassID) ClassName,
							numDomainID FROM dbo.ClassDetails WHERE numDomainID=@numDomainID
							UNION 
						SELECT 0,NULL,0,'Root',1)X;

		--			select * FROM #Temp;
					
					WITH DirectReports(numParentClassID, numChildClassID, ClassName, ClassLevel,Sort) AS 
					(
						SELECT 
							numParentClassID,
							numChildClassID,
							CAST(REPLICATE('--',0) + ClassName AS VARCHAR(100)) ClassName,
							0 AS ClassLevel,
							CONVERT(varchar(255), ClassName)
						FROM dbo.#temp
						WHERE numDomainID=1 AND numParentClassID IS NULL 
						UNION ALL
						SELECT 
							t.numParentClassID,
							t.numChildClassID,
							CAST(REPLICATE('--',ClassLevel +1 ) + t.ClassName AS VARCHAR(100)) ClassName,
							ClassLevel +1 ,
							CONVERT (varchar(255), RTRIM(Sort) + '|    ' + t.ClassName)
						FROM dbo.#temp t
						INNER JOIN DirectReports AS d
						ON d.numChildClassID = t.numParentClassID
					)
					SELECT  numChildClassID, ClassName /*, numParentClassID,ClassLevel,Sort*/
					FROM DirectReports
					WHERE numChildClassID <> 0
					ORDER BY Sort;
					DROP TABLE #Temp
			 END
				END
     
 			
--	IF @tintMode =2 
--     BEGIN
--     
--     		
--			
--		
--			
--     END
	 
END