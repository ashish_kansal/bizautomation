GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetContactTypeMapping')
DROP PROCEDURE USP_GetContactTypeMapping
GO
CREATE PROCEDURE [dbo].[USP_GetContactTypeMapping]
	@numMappingID NUMERIC(9)=0,
	@numContactTypeID NUMERIC(9)=0,
	@numDomainID NUMERIC(9)
AS

SELECT [numMappingID], 
	[numContactTypeID], 
	[numAccountID], 
	[numDomainID]
FROM ContactTypeMapping
WHERE 
([numMappingID] = @numMappingID OR @numMappingID = 0 )
AND (numContactTypeID =@numContactTypeID OR @numContactTypeID = 0)
AND [numDomainID] = @numDomainID







