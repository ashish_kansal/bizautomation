
GO
/****** Object:  StoredProcedure [dbo].[USP_SiteMenuDisplayOrder]    Script Date: 08/08/2009 16:24:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SiteMenuDisplayOrder')
DROP PROCEDURE USP_SiteMenuDisplayOrder
GO
CREATE PROCEDURE [dbo].[USP_SiteMenuDisplayOrder]
	@strItems TEXT=null
AS
BEGIN

	
	DECLARE  @hDocItem INT
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,@strItems
            
          UPDATE [SiteMenu] SET SiteMenu.[intDisplayOrder] = X.intDisplayOrder
          FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                            WITH (numMenuID NUMERIC(9),intDisplayOrder int) X
		  WHERE SiteMenu.[numMenuID] = X.numMenuID
          EXEC sp_xml_removedocument
            @hDocItem
        END
END