SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_Disassembled')
DROP PROCEDURE USP_WorkOrder_Disassembled
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_Disassembled]  
	@numDomainID NUMERIC(18,0),
	@numWOID NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	UPDATE WorkOrder SET numParentWOID=0 WHERE numParentWOID=@numWOID
	DELETE FROM WorkOrderDetails WHERE numWOId=@numWOID
	DELETE FROM WorkOrder WHERE numWOId=@numWOID 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END  