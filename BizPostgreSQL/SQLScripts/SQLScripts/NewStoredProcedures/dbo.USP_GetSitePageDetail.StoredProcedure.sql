
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSitePageDetail]    Script Date: 08/08/2009 16:16:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM [PageElementMaster]
-- exec USP_GetSitePageDetail 'home.aspx',3
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitePageDetail')
DROP PROCEDURE USP_GetSitePageDetail
GO
CREATE PROCEDURE [dbo].[USP_GetSitePageDetail]
          @vcPageURL varchar(1000),
          @numSiteID NUMERIC(9)
AS
  DECLARE  @numPageID NUMERIC(9)
  DECLARE  @numTemplateID NUMERIC(9)
  DECLARE  @numDomainID NUMERIC(9)
  
  SELECT @numPageID = numPageID,@numTemplateID=numTemplateID,@numDomainID=numDomainID
  FROM   [SitePages]
  WHERE  LOWER([vcPageURL]) = LOWER(@vcPageURL)
         AND [numSiteID] = @numSiteID
  IF @numPageID > 0
    BEGIN
      SELECT numPageID,
             ISNULL(SP.vcPageName,'') vcPageName,
             ISNULL(SP.vcPageURL,'') vcPageURL,
             tintPageType,
             ISNULL(SP.vcPageTitle,'') vcPageTitle,
--             [numTemplateID],
--             (SELECT vcTemplateName
--              FROM   [SiteTemplates] st
--              WHERE  st.[numTemplateID] = SP.numTemplateID) vcTemplateName,
             ISNULL(MT.vcMetaTag,'') vcMetaTag,
             ISNULL(MT.vcMetaKeywords,'') vcMetaKeywords,
             ISNULL(MT.vcMetaDescription,'') vcMetaDescription,
             ISNULL(MT.numMetaID,0) numMetaID,
             [numDomainID]
      FROM   SitePages SP
             LEFT OUTER JOIN MetaTags MT
               ON MT.numReferenceID = SP.numPageID
      WHERE  (numPageID = @numPageID
               OR @numPageID = 0)
             AND numSiteID = @numSiteID
      --Select StyleSheets for Page
      SELECT S.[numCssID],
             S.[StyleFileName]
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numPageID] = @numPageID
             AND [tintType] = 0
      --Select Javascripts for Page
      SELECT S.[numCssID],
             S.[StyleFileName]
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numPageID] = @numPageID
             AND [tintType] = 1
             ORDER BY intDisplayOrder 
	 -- Select Templates
	 EXECUTE USP_GetSiteTemplates @numTemplateID,@numSiteID,@numDomainID;
	 --select all templates from site to match for nested template
--	 
    END
    

