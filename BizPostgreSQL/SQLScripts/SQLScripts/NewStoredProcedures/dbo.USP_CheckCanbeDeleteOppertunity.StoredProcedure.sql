/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeDeleteOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeDeleteOppertunity')
DROP PROCEDURE USP_CheckCanbeDeleteOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeDeleteOppertunity]              
	@numOppId AS NUMERIC(18,0),
	@tintError AS TINYINT=0 output
AS
BEGIN
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @OppType AS VARCHAR(2)
	DECLARE @OppStatus TINYINT
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numWLocationID AS NUMERIC(18,0)                           
	DECLARE @numUnits AS FLOAT                                            
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @onHand AS FLOAT
	DECLARE @onOrder AS FLOAT                                                                                                                           
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @LotSerial AS BIT                   
	DECLARE @numQtyReceived AS FLOAT
	DECLARE @tintShipped AS TINYINT 

	SET @tintError=0          
	SELECT 
		@OppType=tintOppType,
		@OppStatus=tintOppStatus,
		@tintShipped=tintShipped,
		@numDomainID=numDomainId 
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId=@numOppId
 
	IF @OppType=2 AND @OppStatus=1
	BEGIN
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=ISNULL(numWarehouseItmsID,0),
			@numWLocationID=ISNULL(numWLocationID,0),
			@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
			@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
		FROM 
			OpportunityItems OI
		LEFT JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID=WI.numWareHouseItemID                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode 
			AND numOppId=@numOppId 
			AND (bitDropShip=0 OR bitDropShip is NULL)                                        
		WHERE 
			charitemtype='P' 
		ORDER BY 
			OI.numoppitemtCode

		WHILE @numoppitemtCode>0                               
		BEGIN  
			IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomainID AND numOppID=@numoppitemtCode AND numOppItemID=@numoppitemtCode)
			BEGIN
				IF (SELECT 
						COUNT(*)
					FROM
						OpportunityItemsReceievedLocation OIRL
					INNER JOIN
						WareHouseItems WI
					ON
						OIRL.numWarehouseItemID=WI.numWareHouseItemID
					WHERE
						OIRL.numDomainID=@numDomainID
						AND numOppID=@numOppId
						AND numOppItemID=@numoppitemtCode
					GROUP BY
						OIRL.numWarehouseItemID,
						WI.numOnHand
					HAVING 
						WI.numOnHand < SUM(OIRL.numUnitReceieved)) > 0
				BEGIN
					IF @tintError=0 SET @tintError=1
				END
				ELSE
				BEGIN
					SET @numUnits = @numUnits - (SELECT SUM(numUnitReceieved) FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomainID AND numOppID=@numoppitemtCode AND numOppItemID=@numoppitemtCode)
				END
			END

			IF @tintError = 0
			BEGIN
				SELECT 
					@onHand = ISNULL(numOnHand, 0),
					@onOrder=ISNULL(numonOrder,0) 
				FROM 
					WareHouseItems 
				WHERE 
					numWareHouseItemID=@numWareHouseItemID 
  
				IF @onHand < @numQtyReceived
				BEGIN  
					IF @tintError=0 SET @tintError=1
				END  
				 
				IF @onOrder < (@numUnits-@numQtyReceived)
				BEGIN  
					if @tintError=0 set @tintError=1
				END
			END
	
			SELECT TOP 1 
				@numoppitemtCode=numoppitemtCode,
				@itemcode=OI.numItemCode,
				@numUnits=ISNULL(numUnitHour,0),
				@numQtyShipped=ISNULL(numQtyShipped,0),
				@numQtyReceived=ISNULL(numUnitHourReceived,0),
				@numWareHouseItemID=numWarehouseItmsID,
				@numWLocationID=ISNULL(numWLocationID,0),
				@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
				@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
			FROM 
				OpportunityItems OI                                            
			LEFT JOIN
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN 
				Item I                                            
			ON 
				OI.numItemCode=I.numItemCode 
				AND numOppId=@numOppId                                          
			WHERE 
				charitemtype='P' 
				AND OI.numoppitemtCode>@numoppitemtCode 
				AND (bitDropShip=0 OR bitDropShip IS NULL) 
			ORDER BY 
				OI.numoppitemtCode                                            
    
			IF @@rowcount=0 
				SET @numoppitemtCode=0    
		END  
	END
END
GO
