-- exec Usp_GetItemAttributesForAPI @numItemCode=822625,@numDomainID=170,@WebApiId = 5
/****** Subject:  StoredProcedure [dbo].[Usp_GetItemAttributesForAPI]    Script Date: 12th Jan,2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--created by Manish Anjara
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_GetItemAttributesForAPI' )
    DROP PROCEDURE Usp_GetItemAttributesForAPI
GO
CREATE PROCEDURE [dbo].[Usp_GetItemAttributesForAPI]
    @numItemCode AS NUMERIC(9) = 0 ,
    @numDomainID AS NUMERIC(9) ,
    @WebApiId AS INT
    --@byteMode AS TINYINT = 0
AS
    DECLARE @numDefaultWarehouseID AS NUMERIC(18)
    SELECT  @numDefaultWarehouseID = ISNULL(numwarehouseID, 0)
    FROM    [dbo].[WebAPIDetail] AS WAD
    WHERE   [WAD].[numDomainId] = @numDomainID
            AND [WAD].[WebApiId] = @WebApiId
    IF ISNULL(@numDefaultWarehouseID, 0) = 0
        BEGIN
            SELECT TOP 1
                    *
            FROM    [dbo].[WareHouseItems] AS WHI
            WHERE   [WHI].[numItemID] = @numItemCode
                    AND [WHI].[numDomainID] = @numDomainID
        END


    SELECT  DISTINCT
            FMST.fld_id ,
            Fld_label ,
            ISNULL(FMST.numlistid, 0) AS numlistid ,
            [LD].[numListItemID] ,
            [LD].[vcData]
    INTO    #tmpSpec
    FROM    CFW_Fld_Master FMST
            LEFT JOIN CFw_Grp_Master Tmst ON Tmst.Grp_id = FMST.subgrp
            JOIN CFW_Loc_Master Lmst ON Lmst.Loc_id = FMST.Grp_id
            JOIN [dbo].[ListMaster] AS LM ON [FMST].[numlistid] = LM.[numListID]
            JOIN [dbo].[ListDetails] AS LD ON LD.[numListID] = LM.[numListID]
                                              AND [FMST].[numlistid] = LD.[numListID]
            JOIN [dbo].[ItemGroupsDTL] AS IGD ON IGD.[numOppAccAttrID] = [FMST].[Fld_id]
            JOIN [dbo].[CFW_Fld_Values_Serialized_Items] AS CFVSI ON [CFVSI].[Fld_ID] = [FMST].[Fld_id]
                                                              AND CONVERT(NUMERIC, [CFVSI].[Fld_Value]) = [LD].[numListItemID]
                                                              AND [CFVSI].[RecId] IN (
                                                              SELECT
                                                              numWarehouseItemID
                                                              FROM
                                                              [dbo].[WareHouseItems]
                                                              WHERE
                                                              numwarehouseID = @numDefaultWarehouseID
                                                              AND numDomainID = @numDomainID
                                                              AND numItemID = @numItemCode )
    WHERE   FMST.numDomainID = @numDomainID
            AND Lmst.Loc_id = 9
    ORDER BY [fmst].[Fld_id]

	--SELECT * FROM [#tmpSpec] AS TS
    SELECT DISTINCT
            TS.fld_label [Name] ,
            STUFF(( SELECT  ',' + CAST([vcData] AS VARCHAR(200))
                    FROM    #tmpSpec
                    WHERE   [ts].[Fld_id] = #tmpSpec.[Fld_id]
                  FOR
                    XML PATH('')
                  ), 1, 1, '') [Value]
    FROM    [#tmpSpec] AS TS
    

    DECLARE @bitSerialize AS BIT                      
    DECLARE @str AS NVARCHAR(MAX)                 
    DECLARE @str1 AS NVARCHAR(MAX)               
    DECLARE @ColName AS VARCHAR(25)                      
    SET @str = ''                       
                        
    DECLARE @bitKitParent BIT
    DECLARE @numItemGroupID AS NUMERIC(9)                        
                        
    SELECT  @numItemGroupID = numItemGroup ,
            @bitSerialize = CASE WHEN bitSerialized = 0 THEN bitLotNo
                                 ELSE bitSerialized
                            END ,
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent, 0) = 1
                                        AND ISNULL(bitAssembly, 0) = 1 THEN 0
                                   WHEN ISNULL(bitKitParent, 0) = 1 THEN 1
                                   ELSE 0
                              END )
    FROM    Item
    WHERE   numItemCode = @numItemCode                        
    SET @ColName = 'numWareHouseItemID,0'
                      
              
--Create a Temporary table to hold data                                                            
    CREATE TABLE #tempTable
        (
          ID INT IDENTITY
                 PRIMARY KEY ,
          numCusFlDItemID NUMERIC(9)
        )                         
                        
    INSERT  INTO #tempTable
            ( numCusFlDItemID
            )
            SELECT DISTINCT
                    ( numOppAccAttrID )
            FROM    ItemGroupsDTL
            WHERE   numItemGroupID = @numItemGroupID
                    AND tintType = 2                       
                          
                 
    DECLARE @ID AS NUMERIC(9)                        
    DECLARE @numCusFlDItemID AS VARCHAR(20)                        
    DECLARE @fld_label AS VARCHAR(100) ,
        @fld_type AS VARCHAR(100)                        
    SET @ID = 0                        
    SELECT TOP 1
            @ID = ID ,
            @numCusFlDItemID = numCusFlDItemID ,
            @fld_label = fld_label ,
            @fld_type = fld_type
    FROM    #tempTable
            JOIN CFW_Fld_Master ON numCusFlDItemID = Fld_ID                        
                         
    WHILE @ID > 0
        BEGIN                        
                          
           -- SET @str = @str + ',  dbo.GetCustFldItems(' + @numCusFlDItemID + ',9,' + @ColName + ') as [' + @fld_label + ']'
	
            --IF @byteMode = 1
            SET @str = @str + ', dbo.GetCustFldItemsValue(' + @numCusFlDItemID + ',9,' + @ColName + ',''' + @fld_type + ''') as [' + @fld_label + '_c]'                                        
                          
            SELECT TOP 1
                    @ID = ID ,
                    @numCusFlDItemID = numCusFlDItemID ,
                    @fld_label = fld_label
            FROM    #tempTable
                    JOIN CFW_Fld_Master ON numCusFlDItemID = Fld_ID
                                           AND ID > @ID                        
            IF @@rowcount = 0
                SET @ID = 0                        
                          
        END                        
                       
--      

    DECLARE @KitOnHand AS FLOAT;
    SET @KitOnHand = 0

    SET @str1 = 'select '

    SET @str1 = @str1 + 'I.numItemCode,'
	
    SET @str1 = @str1
        + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '
        + CASE WHEN @bitSerialize = 1 THEN ' '
               ELSE @str
          END
        + '                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent]
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID = WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID=' + CONVERT(VARCHAR(15), @numItemCode)
        + ' AND WareHouseItems.numWarehouseItemID IN (
                                                              SELECT
                                                              numWarehouseItemID
                                                              FROM
                                                              [dbo].[WareHouseItems]
                                                              WHERE
                                                              numwarehouseID = '
        + CONVERT(VARCHAR(15), @numDefaultWarehouseID)
        + '
                                                              AND numDomainID = '
        + CONVERT(VARCHAR(15), @numDomainID)
        + '
                                                              AND numItemID = '
        + CONVERT(VARCHAR(15), @numItemCode) + ' ) '
   
    PRINT ( @str1 )           

    EXECUTE sp_executeSQL @str1, N'@bitKitParent bit', @bitKitParent
                       

GO
