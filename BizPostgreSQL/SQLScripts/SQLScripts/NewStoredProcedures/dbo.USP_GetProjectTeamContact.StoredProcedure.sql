/****** Object:  StoredProcedure [dbo].[USP_GetProjectTeamContact]    Script Date: 09/17/2010 17:35:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectTeamContact')
DROP PROCEDURE USP_GetProjectTeamContact
GO
CREATE PROCEDURE [dbo].[USP_GetProjectTeamContact]
    @numProjectTeamId NUMERIC(9) = 0,
    @numDomainId AS NUMERIC(9) = 0       
--      
AS 
BEGIN    
    
    SELECT  PT.numProjectTeamId,
            PT.numContactId
    FROM    ProjectTeam PT
            JOIN additionalContactsinformation CO ON PT.numContactId = CO.numContactId
    WHERE   PT.numProjectTeamId = @numProjectTeamId
            AND CO.numDomainID = @numDomainId      

END
