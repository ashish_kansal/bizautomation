GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddHelp')
DROP PROCEDURE USP_AddHelp
GO
CREATE PROCEDURE USP_AddHelp
    @numHelpId NUMERIC(9) OUTPUT,
    @helpcategory NVARCHAR(100),
    @helpheader NVARCHAR(400),
    @helpMetatag NVARCHAR(400),
    @helpDescription NVARCHAR(MAX),
    @helpPageUrl NVARCHAR(200),
    @helpshortdesc NVARCHAR(400),
    @helpLinkingPageURL VARCHAR(1000)
AS 
    BEGIN      
      
      DECLARE @intDisplayOrder INT 
      SELECT @intDisplayOrder =ISNULL(MAX(intDisplayOrder),0) +1 FROM HelpMaster WHERE helpCategory=@helpCategory
        INSERT  INTO [HelpMaster]
                (
                  [helpCategory],
                  [helpHeader],
                  [helpMetatag],
                  [helpDescription],
                  helpPageUrl,
                  helpshortdesc,helpLinkingPageURL,
                  intDisplayOrder
                )
        VALUES  (
                  @helpcategory,
                  @helpheader,
                  @helpMetatag,
                  @helpDescription,
                  @helpPageUrl,
                  @helpshortdesc,@helpLinkingPageURL,
                  @intDisplayOrder
                )      
      
        SET @numHelpId = SCOPE_IDENTITY()      
    END 