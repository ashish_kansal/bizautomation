GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBroadcastDataByID' ) 
    DROP PROCEDURE USP_GetBroadcastDataByID
GO
CREATE PROCEDURE USP_GetBroadcastDataByID
  
@numDomainId AS NUMERIC(18),
@numBroadCastId AS NUMERIC(18)=0,
@numSearchID AS Numeric(18) = 0

AS 
BEGIN
  IF @numBroadCastID > 0
    BEGIN        
          select B.*,ISNULL(T.numCategoryId,0) AS numCategoryId,ISNULL(T.bitLastFollowupUpdate,0) AS bitLastFollowupUpdate 
		  from Broadcast AS B LEFT JOIN GenericDocuments AS T ON B.numEmailTemplateID=T.numGenericDocID where numBroadCastId = @numBroadCastId and B.numDomainId = @numDomainId
    END
  ELSE
    BEGIN
         select B.*,ISNULL(T.numCategoryId,0) AS numCategoryId,ISNULL(T.bitLastFollowupUpdate,0) AS bitLastFollowupUpdate 
		  from Broadcast AS B LEFT JOIN GenericDocuments AS T ON B.numEmailTemplateID=T.numGenericDocID where numSearchID = @numSearchID and B.numDomainId = @numDomainId
    END  
END



