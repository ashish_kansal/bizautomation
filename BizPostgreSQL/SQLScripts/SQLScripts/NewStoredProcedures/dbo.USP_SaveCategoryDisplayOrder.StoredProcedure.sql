
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveCategoryDisplayOrder')
DROP PROCEDURE USP_SaveCategoryDisplayOrder
GO
CREATE PROCEDURE [dbo].[USP_SaveCategoryDisplayOrder]
	@strItems TEXT=null
AS
BEGIN

	
	DECLARE  @hDocItem INT
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,@strItems
            
          UPDATE [Category] SET Category.[intDisplayOrder] = X.intDisplayOrder
          FROM   OPENXML (@hDocItem, '/NewDataSet/CategoryOrder', 2)
                            WITH (numCategoryID NUMERIC(9),intDisplayOrder int) X
		  WHERE Category.numCategoryID = X.numCategoryID
          EXEC sp_xml_removedocument
            @hDocItem
        END
END