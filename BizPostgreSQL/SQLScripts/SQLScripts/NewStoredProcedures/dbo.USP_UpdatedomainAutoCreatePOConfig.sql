SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatedomainAutoCreatePOConfig')
DROP PROCEDURE dbo.USP_UpdatedomainAutoCreatePOConfig
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainAutoCreatePOConfig]                                      
@numDomainID as numeric(9)=0,                                      
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0
as                                      
BEGIN                                   
	update Domain                                       
   set                  
bitReOrderPoint=@bitReOrderPoint,
numReOrderPointOrderStatus=@numReOrderPointOrderStatus
 where numDomainId=@numDomainID
END