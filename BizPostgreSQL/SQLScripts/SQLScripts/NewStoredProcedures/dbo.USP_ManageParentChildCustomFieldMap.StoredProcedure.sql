        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageParentChildCustomFieldMap')
	DROP PROCEDURE USP_ManageParentChildCustomFieldMap
GO

CREATE PROCEDURE [dbo].[USP_ManageParentChildCustomFieldMap]
    @numParentChildFieldID numeric(18, 0),
	@numDomainID numeric(18, 0),
	@tintParentModule tinyint,
	@numParentFieldID numeric(18, 0),
	@tintChildModule tinyint,
	@numChildFieldID numeric(18, 0),
	@tintMode AS TINYINT,
	@bitCustomField AS BIT = 0
AS

IF @tintMode = 1
BEGIN
	DELETE FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentChildFieldID=@numParentChildFieldID
END
ELSE
BEGIN
	IF EXISTS(SELECT 1 FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND tintChildModule=@tintChildModule AND numChildFieldID=@numChildFieldID AND bitCustomField=@bitCustomField)
	BEGIN
		RAISERROR ('AlreadyExists',16,1);
	END
	ELSE
	BEGIN
		DECLARE @ParentFld_type AS VARCHAR(20),@ChildFld_type AS VARCHAR(20)

		IF ISNULL(@bitCustomField,0) = 0
		BEGIN
			SELECT @ParentFld_type=vcAssociatedControlType FROM DycFieldMaster WHERE numFieldId=@numParentFieldID
			SELECT @ChildFld_type=vcAssociatedControlType FROM DycFieldMaster WHERE numFieldId=@numChildFieldID

			IF @ParentFld_type != @ChildFld_type
			BEGIN
				RAISERROR ('FieldTypeMisMatch',16,1);
			END	
		
			ELSE
			BEGIN
				INSERT INTO dbo.ParentChildCustomFieldMap 
				(
					numDomainID,tintParentModule,numParentFieldID,tintChildModule,numChildFieldID,bitCustomField
				) 
				SELECT @numDomainID,@tintParentModule,@numParentFieldID,@tintChildModule,@numChildFieldID,0
			END	
		END
		ELSE
		BEGIN
			SELECT @ParentFld_type=Fld_type FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=@tintParentModule AND Fld_id=@numParentFieldID
			SELECT @ChildFld_type=Fld_type FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=@tintChildModule AND Fld_id=@numChildFieldID
			
			IF @ParentFld_type != @ChildFld_type
			BEGIN
				RAISERROR ('FieldTypeMisMatch',16,1);
			END	
		
			ELSE
			BEGIN
				INSERT INTO dbo.ParentChildCustomFieldMap (
					numDomainID,tintParentModule,numParentFieldID,tintChildModule,numChildFieldID,bitCustomField
				) SELECT @numDomainID,@tintParentModule,@numParentFieldID,@tintChildModule,@numChildFieldID,1
			END	
		END
	END		
END
