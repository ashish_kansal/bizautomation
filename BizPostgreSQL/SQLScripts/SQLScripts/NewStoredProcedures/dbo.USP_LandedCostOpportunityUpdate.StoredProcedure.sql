GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostOpportunityUpdate')
DROP PROCEDURE USP_LandedCostOpportunityUpdate
GO
CREATE PROC [dbo].[USP_LandedCostOpportunityUpdate] 
    @numDomainId NUMERIC(18, 0),
    @numOppId NUMERIC(18, 0),
	@vcLanedCost varchar(50)
AS 
	
		DECLARE @monLandedCostTotal DECIMAL(20,5)
		SELECT @monLandedCostTotal = SUM([BH].[monAmountDue]) FROM [dbo].[BillHeader] AS BH 
		WHERE [BH].[numDomainID]=@numDomainId AND [BH].[numOppId]=@numOppId AND [BH].[bitLandedCost] = 1

		--Update Total Landed Cost on Opp
		UPDATE [dbo].[OpportunityMaster] SET [monLandedCostTotal]=@monLandedCostTotal,
		[vcLanedCost]=@vcLanedCost
		WHERE [OpportunityMaster].[numDomainId]=@numDomainId AND [OpportunityMaster].[numOppId]=@numOppId

		--Calculate landed cost for each item and Update Landed Cost on Item
		SELECT [OI].[numoppitemtCode],
		CASE @vcLanedCost WHEN 'Amount' THEN [OI].[monTotAmount]
			WHEN 'Cubic Size' THEN ISNULL(I.[fltWidth],0) * ISNULL(I.[fltHeight],0) * ISNULL(I.[fltLength] ,0)
			WHEN 'Weight' THEN ISNULL(I.[fltWeight],0) END AS Total
		INTO #temp
		FROM [dbo].[OpportunityItems] AS OI JOIN [dbo].[Item] AS I ON OI.[numItemCode] = I.[numItemCode] 
		WHERE [OI].[numOppId] = @numOppId

		DECLARE @Total AS DECIMAL(18,2)
		SELECT @Total = SUM(Total) FROM [#temp] AS T

		UPDATE OI SET [monLandedCost] = CAST(ISNULL((@monLandedCostTotal * T.[Total]) / NULLIF(@Total,0),0) AS DECIMAL(18,2)) FROM [dbo].[OpportunityItems] AS OI
		JOIN #temp T ON OI.[numoppitemtCode] = T.[numoppitemtCode] WHERE OI.[numOppId] = @numOppId


		

		--Update Item Average Cost
		UPDATE 
			I 
		SET 
			[monAverageCost] = CASE 
								WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 
								ELSE 
									CASE WHEN ISNULL(TotalOnHand.Qty,0) <= 0
									THEN 
										I.monAverageCost
									ELSE								
										((ISNULL(I.[monAverageCost],0) * Isnull(TotalOnHand.Qty,0)) + ISNULL(OI.[monLandedCost],0) - ISNULL(OI.monAvgLandedCost,0))/NULLIF(Isnull(TotalOnHand.Qty,0),0)
									END
								END
		FROM 
			[dbo].[OpportunityItems] AS OI 
		JOIN 
			[dbo].[Item] AS I 
		OUTER APPLY
		(
			SELECT
				ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) AS Qty
			FROM
				WarehouseItems
			WHERE 
				numItemID = I.numItemCode
		) AS TotalOnHand
		ON 
			OI.[numItemCode] = I.[numItemCode] WHERE OI.[numOppId] = @numOppId

		--Update monAvgLandedCost for tracking Purpose
		UPDATE OI SET monAvgLandedCost = OI.[monLandedCost] FROM [dbo].[OpportunityItems] AS OI WHERE OI.[numOppId] = @numOppId

		DROP TABLE #temp

