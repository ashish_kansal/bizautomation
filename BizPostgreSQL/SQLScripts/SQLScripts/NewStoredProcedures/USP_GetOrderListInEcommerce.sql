/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by prasanta Pradhan                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderListInEcommerce')
DROP PROCEDURE USP_GetOrderListInEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetOrderListInEcommerce]                            
 @numDivisionID as numeric(9)=0 ,
 @tintOppType AS TINYINT=1,
 @numDomainID AS NUMERIC(9),
 @CurrentPage INT,
 @PageSize INT,
 @tintShipped INT=0,
 @tintOppStatus INT=0,
 @UserId NUMERIC(18,0)
as                        
 
 DECLARE @firstRec INT=0
 DECLARE @lastRec INT=0
DECLARE @ClientTimeZoneOffset Int
SET @ClientTimeZoneOffset=-330
IF (@tintOppType = 1 OR  @tintOppType =2 )
BEGIN
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
	SELECT  * FROM (
	SELECT ROW_NUMBER() OVER(ORDER BY numOppID) AS Rownumber,* FROM (
	SELECT distinct OM.numOppID,OM.vcPOppName,(SELECT SUBSTRING((SELECT '$^$' + CAST(numOppBizDocsId AS VARCHAR(18)) +'#^#'+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=OM.numOppID AND numBizDOcId=287 FOR XML PATH('')),4,200000)) AS vcBizDoc,

		(SELECT SUBSTRING((SELECT '$^$' + CAST(vcTrackingNo AS VARCHAR(18)) +'#^#'+ vcTrackingUrl
										FROM OpportunityBizDocs WHERE numOppId=OM.numOppID FOR XML PATH('')),4,200000)) AS vcShipping,
		(isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) * OM.fltExchangeRate) as monDealAmount,
		(isnull(dbo.GetPaidAmount(OM.numOppID),0) * OM.fltExchangeRate) AS monAmountPaid,
		(isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) * OM.fltExchangeRate)
		 - (isnull(dbo.GetPaidAmount(OM.numOppID),0) * OM.fltExchangeRate) AS BalanceDue,
		(select  TOP 1 vcData from ListDetails where numListItemID=(SELECT TOP 1 numShipVia FROM OpportunityBizDocs WHERE numOppId=OM.numOppID AND numShipVia>0)) AS vcShippingMethod,
		(select  TOP 1 vcData from ListDetails where numListItemID=(SELECT TOP 1 intUsedShippingCompany FROM OpportunityMaster where numOppId=OM.numOppID)) AS vcMasterShippingMethod,
		(SELECT TOP 1 intUsedShippingCompany FROM OpportunityMaster where numOppId=OM.numOppID) AS numOppMasterShipVia,
		(SELECT TOP 1 numShipVia FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS numShipVia,
		(SELECT TOP 1 vcTrackingNo FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS vcTrackingNo,
		(SELECT TOP 1 numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = (SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0)) AS numShippingReportId,
		(SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS numShippingBizDocId,
		[dbo].[FormatedDateFromDate]((SELECT TOP 1 dtDeliveryDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0),OM.numDomainId) AS DeliveryDate,
		[dbo].[FormatedDateFromDate]((SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc),OM.numDomainId) AS BillingDate,

		CASE WHEN (SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE numOppId=OM.numOppID)=1 THEN
		(CASE ISNULL(OM.bitBillingTerms,0)  
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),(SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc)),OM.numDomainId)
                 WHEN 0 THEN [dbo].[FormatedDateFromDate]((SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc),OM.numDomainId)
               END) 
		ELSE 'Multiple' END AS DueDate,
		CASE WHEN (SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE numOppId=OM.numOppID)=1 THEN
		(CASE ISNULL(OM.bitBillingTerms,0)  
                 WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),(SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc))
                 WHEN 0 THEN (SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc)
               END) ELSE NULL END AS DateDueDate,
		[dbo].[FormatedDateFromDate](OM.dtReleaseDate,OM.numDomainId) AS ReleaseDate,
		[dbo].[FormatedDateFromDate](OM.bintCreatedDate,OM.numDomainId) AS CreatedDate,
		(select TOP 1 vcData from ListDetails WHERE numListItemID=OM.numStatus) AS vcOrderStatus,
        dbo.GetCreditTerms(OM.numOppId) as Credit,
		dbo.fn_GetContactName(OM.numCreatedBy)  as CreatedName,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  dtCreatedDate,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  numCreatedDate,
		CASE WHEN 
		(select TOP 1 ISNULL(numPaymentMethod,0) from General_Journal_Details WHERE numJournalId IN (select numJournal_Id from General_Journal_Header where numOppBizDocsId IN(select numBizDocId from OpportunityBizDocs where numOppId=OM.numOppID)))=0
		THEN
		(select TOP 1 vcData from ListDetails WHERE numListItemID IN (
		select ISNULL(numDefaultPaymentMethod,0) from DivisionMaster where numDivisionId=@numDivisionID)) 
		ELSE  (select TOP 1 vcData from ListDetails WHERE numListItemID IN (
			  select numPaymentMethod from General_Journal_Details WHERE numJournalId IN (select numJournal_Id from General_Journal_Header where numOppBizDocsId IN(select numBizDocId from OpportunityBizDocs where numOppId=OM.numOppID))))
		END
		AS PaymentMethod,
		isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) as TotalAmt,
		OM.numContactId,
		ISNULL(OM.tintOppStatus,0) As [tintOppStatus],
		CASE 
WHEN convert(varchar(11),OM.bintCreatedDate)=convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,getutcdate())) 
then CONCAT('<b><font color="#FF0000" style="font-size:14px">Today','</font></b>') 
WHEN convert(varchar(11),OM.bintCreatedDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute,-@ClientTimeZoneOffset,getutcdate()))) 
THEN CONCAT('<b><font color=#ED8F11 style="font-size:14px">Tommorow','</font></b>') 
WHEN OM.bintCreatedDate<DateAdd(day,7,DateAdd(minute, -@ClientTimeZoneOffset,getutcdate()))
AND OM.bintCreatedDate>DateAdd(day,1,DateAdd(minute, -@ClientTimeZoneOffset,getutcdate()))
then CONCAT('<b><font color=#8FAADC style="font-size:14px">',datename(dw,DateAdd(minute, -@ClientTimeZoneOffset,OM.bintCreatedDate)),'<span style="color:#000;font-weight:500"> - ',CAST(DAY(OM.bintCreatedDate) AS VARCHAR(10)),CASE
	WHEN DAY(OM.bintCreatedDate) % 100 IN (11, 12, 13) THEN 'th'
	WHEN DAY(OM.bintCreatedDate) % 10 = 1 THEN 'st'
	WHEN DAY(OM.bintCreatedDate) % 10 = 2 THEN 'nd'
	WHEN DAY(OM.bintCreatedDate) % 10 = 3 THEN 'rd'
ELSE 'th' END,'</span>','</font></b>','<b>','<br/>'
,'<Span style="font-size:14px;font-weight:normal;font-style: italic;">'
,'</span>') ELSE 

CONCAT('<b><font color=#8FAADC style="font-size:14px">',datename(dw,DateAdd(minute, -@ClientTimeZoneOffset,OM.bintCreatedDate)),'<span style="color:#000;font-weight:500"> - ',CAST(DAY(OM.bintCreatedDate) AS VARCHAR(10)),CASE
	WHEN DAY(OM.bintCreatedDate) % 100 IN (11, 12, 13) THEN 'th'
	WHEN DAY(OM.bintCreatedDate) % 10 = 1 THEN 'st'
	WHEN DAY(OM.bintCreatedDate) % 10 = 2 THEN 'nd'
	WHEN DAY(OM.bintCreatedDate) % 10 = 3 THEN 'rd'
ELSE 'th' END,'</span>','</font></b>','<b>','<br/>'
,'<Span style="font-size:14px;font-weight:normal;font-style: italic;">'
,'</span>')
END AS FormattedCreatedDate,
vcCustomerPO#




		FROM dbo.OpportunityMaster OM 
		LEFT JOIN dbo.OpportunityBizDocs OBD ON OM.numoppID=OBD.numoppID
		LEFT JOIN dbo.DivisionMaster D ON D.numDivisionID = OM.numDivisionId
		LEFT JOIN dbo.CompanyInfo C ON C.numCompanyId = D.numCompanyID
		WHERE OM.numDomainId=@numDomainID AND OM.tintOppType= @tintOppType
		AND D.numDivisionID= @numDivisionID 
		--AND ISNULL(bitAuthoritativeBizDocs,0)=1
		AND OM.tintOppStatus=@tintOppStatus  AND OM.numContactId=@UserId
		AND 1=(CASE WHEN @tintOppStatus=1 AND OM.tintShipped=@tintShipped THEN 1 WHEN @tintOppStatus=0 THEN 1 ELSE 0 END)
		--AND ISNULL(OBD.tintDeferred,0) <> 1 AND  isnull(OBD.monDealAmount * OM.fltExchangeRate,0) > 0 
		--AND isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) > 0
		) AS K) AS T
		WHERE T.Rownumber>@firstRec AND T.Rownumber<@lastRec

		SELECT DISTINCT COUNT(OM.numOppId) FROM dbo.OpportunityMaster OM 
		LEFT JOIN dbo.DivisionMaster D ON D.numDivisionID = OM.numDivisionId
		LEFT JOIN dbo.CompanyInfo C ON C.numCompanyId = D.numCompanyID
		WHERE OM.numDomainId=@numDomainID AND OM.tintOppType= @tintOppType
		AND D.numDivisionID= @numDivisionID 
		--AND ISNULL(bitAuthoritativeBizDocs,0)=1
		AND OM.tintOppStatus=@tintOppStatus  AND OM.numContactId=@UserId
		AND 1=(CASE WHEN @tintOppStatus=1 AND OM.tintShipped=@tintShipped THEN 1 WHEN @tintOppStatus=0 THEN 1 ELSE 0 END)
		--AND ISNULL(OBD.tintDeferred,0) <> 1 AND  isnull(OBD.monDealAmount * OM.fltExchangeRate,0) > 0 
		--AND isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) > 0


		----Create a Temporary table to hold data                        
--Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                         
--      numOppID varchar(15),vcPOppName varchar(100),                  
--      vcBizDocID varchar(100),                
--   BizDocName varchar(100),              
--   CreatedName varchar(100),                       
--      Credit VARCHAR(100),                        
--      numCreatedDate datetime,                        
-- monAmountPaid DECIMAL(20,5),    
-- TotalAmt DECIMAL(20,5),
-- numContactId NUMERIC(9),
-- tintOppStatus TINYINT)                        
--                  
--                  
--declare @strSql as varchar(8000)                        
--set @strSql=
--'SELECT 
--O.numOppID,
--O.vcPOppName,               
--CASE WHEN O.tintOppType = 1 AND O.tintOppStatus = 0 THEN ''Sales Opportunity''
--	 WHEN O.tintOppType = 1 AND O.tintOppStatus = 1 THEN ''Sales Order''
--END as BizDocName,              
--dbo.fn_GetContactName(O.numCreatedBy)  as CreatedName,     
--dbo.GetCreditTerms(O.numOppId) as Credit,                  
--CONVERT(DATETIME, O.bintCreatedDate) AS  dtCreatedDate,
--isnull(O.numPClosingPercent,0)as monAmountPaid,
--isnull(dbo.GetDealAmount(O.numOppID,getdate(),0),0) as TotalAmt,
--O.numContactId,ISNULL(O.tintOppStatus,0) As [tintOppStatus]
--FROM dbo.OpportunityMaster O 
--     JOIN dbo.DivisionMaster D 
--        ON D.numDivisionID = O.numDivisionId
--     JOIN dbo.CompanyInfo C 
--        ON C.numCompanyId = D.numCompanyID
--WHERE O.numDomainId = D.numDomainID '
--      
--set @strSql=@strSql + ' and O.tintOppType= ' + convert(varchar(3),@tintOppType)
--
--if @numDivisionID<>0 set @strSql=@strSql + ' and D.numDivisionID= ' + convert(varchar(15),@numDivisionID)                        
--if @SortChar<>'0' set @strSql=@strSql + ' And vcBizDocID like '''+@SortChar+'%'''   
--IF @bitflag =0
--BEGIN
--	SET @strSql = @strSql + ' And O.tintShipped  =1'                                            
--END 
--ELSE IF @bitflag=1
--BEGIN
--SET @strSql = @strSql + ' And (O.tintOppStatus = 1 OR O.tintOppStatus = 0)'
--	
--END
--
--set @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                     
--print @strSql                      
--insert into #tempTable(                        
--      numOppID,                  
-- vcBizDocID,                
-- BizDocName,              
--  CreatedName,                        
--      Credit,                        
--      numCreatedDate,                        
-- monAmountPaid,
-- TotalAmt,
-- numContactId,tintOppStatus)                        
--exec (@strSql)                         
--                        
--  declare @firstRec as integer                         
--  declare @lastRec as integer                        
-- set @firstRec= (@CurrentPage-1) * @PageSize                        
--     set @lastRec= (@CurrentPage*@PageSize+1)                        
--select *,(TotalAmt-monAmountPaid) AS BalanceDue,
--CASE WHEN @tintOppType = 1 AND tintOppStatus = 0 THEN 'Sales Opportunity' 
--	 WHEN @tintOppType = 1 AND tintOppStatus = 1 THEN 'Sales Order' 
--     WHEN @tintOppType = 2 THEN 'Purchase Order' END AS [Type]
-- from #tempTable where ID > @firstRec and ID < @lastRec                        
--set @TotRecs=(select count(*) from #tempTable)                        
--drop table #tempTable
END 
--For bills
IF @tintOppType =  3 
BEGIN
-- SELECT 'Bill' AS [Type],OBD.vcMemo,OBD.vcReference,OBD.monAmount,dbo.FormatedDateFromDate(PD.dtDueDate,OBD.numDomainId) DueDate
-- FROM   dbo.OpportunityBizDocsDetails OBD
--        INNER JOIN dbo.OpportunityBizDocsPaymentDetails PD ON PD.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId
--  WHERE OBD.numDivisionID = @numDivisionID AND PD.bitIntegrated=0

SELECT 0 AS numOppId,'Bill' vcPOppName,0 AS numOppBizDocsId,'Bill' + CASE WHEN len(BH.vcReference)=0 THEN '' ELSE '-' + BH.vcReference END AS [vcBizDocID],
						   BH.monAmountDue as monDealAmount,
						   ISNULL(BH.monAmtPaid, 0) as monAmountPaid,
					       ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) as BalanceDue,
					       [dbo].[FormatedDateFromDate](BH.dtBillDate,BH.numDomainID) AS BillingDate,
						   [dbo].[FormatedDateFromDate](BH.dtDueDate,BH.numDomainID) AS DueDate
						   FROM    
							BillHeader BH 
							WHERE BH.numDomainId=@numDomainID AND BH.numDivisionId = @numDivisionID
							AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
							
END



