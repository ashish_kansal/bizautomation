GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_getNextPrevMailID' ) 
    DROP PROCEDURE USP_getNextPrevMailID
GO
Create PROCEDURE [dbo].[USP_getNextPrevMailID]   
@numDomainId as numeric(9),  
@numUserCntId as numeric(9),    
@numEmailHstrID as numeric(9), 
--@srch as varchar(100) = '',                                                                                           
--@srchBody as varchar(100) = '',                            
--@vcSubjectFromTo as varchar(100) ='',
@numNodeId as numeric(9),
@EmailStatus AS NUMERIC(9)=0
--@ToEmail as varchar (100)
as                                                      
                                                      
/*Note: Bug fix 2027: selective Searching into subject and Bodytext*/      
	DECLARE @strSql AS VARCHAR(8000)

	SET @strSql = '
	select top 1 #Variable# = numEmailHstrID from EmailHistory where 
	numDomainID =' + CONVERT(VARCHAR(10),@numDomainId) + ' And [numUserCntId] = ' + CONVERT(VARCHAR(10),@numUserCntId) + ' AND [numNodeID] = ' + CONVERT(VARCHAR(10),@numNodeId) + '
	and chrSource IN( ''B'',''I'') 
	#EmailHstrID#
	'
	--IF LEN(@srch)>0
	--BEGIN
	--	SET @strSql = 	@strSql + 'AND (vcBodyText LIKE ''% ' + @srch + '%'' OR vcSubject LIKE ''%' + @srch + '%'' )'
	--END
	IF @EmailStatus>0
	BEGIN
		SET @strSql = 	@strSql + 'AND numListItemId = @EmailStatus '
	END

	SET @strSql = 	@strSql + 'and isnull(numemailid,0)>0
	ORDER BY bintCreatedOn '

	 DECLARE @query AS VARCHAR(8000)

	 SET @query = ' DECLARE @numPrevEmailHstrID AS INTEGER, @numNextEmailHstrID AS INTEGER '
	 
	 
	 SET @query = @query + REPLACE( REPLACE(@strSql,'#EmailHstrID#',' and numEmailHstrID<'+ CONVERT(VARCHAR(10),@numEmailHstrID) + ' ') ,'#Variable#',' @numPrevEmailHstrID ')  + ' DESC; '
	 SET @query = @query + REPLACE( REPLACE(@strSql,'#EmailHstrID#',' and numEmailHstrID>'+ CONVERT(VARCHAR(10),@numEmailHstrID) + ' ') ,'#Variable#',' @numNextEmailHstrID ') + ' ASC; '
	 SET @query = @query + ' select isnull(@numPrevEmailHstrID,0) as numPrevEmailHstrID,isnull(@numNextEmailHstrID,0) as numNextEmailHstrID '

	 PRINT @query
	 EXEC(@query)
 
