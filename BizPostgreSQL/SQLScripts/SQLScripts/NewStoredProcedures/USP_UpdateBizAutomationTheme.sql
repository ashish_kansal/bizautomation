/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateBizAutomationTheme')
DROP PROCEDURE USP_UpdateBizAutomationTheme
GO
CREATE PROCEDURE [dbo].[USP_UpdateBizAutomationTheme]                                      
@numDomainID as numeric(9)=0,  
@vcLogoForBizTheme AS VARCHAR(500)=NULL,
@bitLogoAppliedToBizTheme AS BIT=NULL,
@vcThemeClass AS VARCHAR(500)=NULL,  
@vcLogoForLoginBizTheme AS VARCHAR(500)=NULL,
@bitLogoAppliedToLoginBizTheme AS BIT=NULL,
@vcLoginURL AS VARCHAR(500)=NULL
as
BEGIN
	IF((Select LEN(@vcLogoForBizTheme)) > 0)
	Begin
	update Domain Set vcLogoForBizTheme = @vcLogoForBizTheme
						Where numDomainID=@numDomainID
	End
	IF((Select LEN(@vcLogoForLoginBizTheme)) > 0)
	Begin
	update Domain Set vcLogoForLoginBizTheme = @vcLogoForLoginBizTheme
						Where numDomainID=@numDomainID
	End
	IF((LEN(@vcLoginURL)) > 0)
	Begin
		IF((SELECT  COUNT(*) FROM  Domain  WHERE  vcLoginURL=@vcLoginURL AND numDomainID<>@numDomainID)=0)
		BEGIN
		update Domain Set vcLoginURL = @vcLoginURL
							Where numDomainID=@numDomainID
		END
	End
	Update Domain Set	bitLogoAppliedToBizTheme = @bitLogoAppliedToBizTheme,
					    bitLogoAppliedToLoginBizTheme = @bitLogoAppliedToLoginBizTheme,
						  vcThemeClass=@vcThemeClass
						  Where numDomainID=@numDomainID
END