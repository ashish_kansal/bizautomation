GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO   
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryPickList')
DROP PROCEDURE USP_ManageInventoryPickList
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryPickList]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
	,@numUserCntID AS NUMERIC(18,0)
AS
BEGIN
	IF ISNULL(@numUserCntID,0) = 0
	BEGIN
		RAISERROR('INVALID_USERID',16,1)
		RETURN
	END

	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @bitWorkorder AS BIT
	DECLARE @numUnitHour FLOAT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @onHand AS FLOAT                                    
    DECLARE @onOrder AS FLOAT 
    DECLARE @onBackOrder AS FLOAT
    DECLARE @onAllocation AS FLOAT
	DECLARE @onReOrder AS FLOAT
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
	DECLARE @dtItemReceivedDate AS DATETIME=NULL

	DECLARE @j INT = 0
	DECLARE @ChildCount AS INT
	DECLARE @vcChildDescription AS VARCHAR(300)
	DECLARE @numChildAllocation AS FLOAT
	DECLARE @numChildBackOrder AS FLOAT
	DECLARE @numChildOnHand AS FLOAT
	DECLARE @numOppChildItemID AS INT
	DECLARE @numChildItemCode AS INT
	DECLARE @bitChildIsKit AS BIT
	DECLARE @numChildWarehouseItemID AS INT
	DECLARE @numChildQty AS FLOAT
	DECLARE @numChildQtyShipped AS FLOAT

	DECLARE @TempKitSubItems TABLE
	(
		ID INT,
		numOppChildItemID NUMERIC(18,0),
		numChildItemCode NUMERIC(18,0),
		bitChildIsKit BIT,
		numChildWarehouseItemID NUMERIC(18,0),
		numChildQty FLOAT,
		numChildQtyShipped FLOAT
	)

	DECLARE @k AS INT = 1
	DECLARE @CountKitChildItems AS INT
	DECLARE @vcKitChildDescription AS VARCHAR(300)
	DECLARE @numKitChildAllocation AS FLOAT
	DECLARE @numKitChildBackOrder AS FLOAT
	DECLARE @numKitChildOnHand AS FLOAT
	DECLARE @numOppKitChildItemID AS NUMERIC(18,0)
	DECLARE @numKitChildItemCode AS NUMERIC(18,0)
	DECLARE @numKitChildWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numKitChildQty AS FLOAT
	DECLARE @numKitChildQtyShipped AS FLOAT

	DECLARE @TempKitChildKitSubItems TABLE
	(
		ID INT,
		numOppKitChildItemID NUMERIC(18,0),
		numKitChildItemCode NUMERIC(18,0),
		numKitChildWarehouseItemID NUMERIC(18,0),
		numKitChildQty FLOAT,
		numKitChildQtyShipped FLOAT
	)

	DECLARE @TempPickListItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,numWarehouseItemID NUMERIC(18,0)
		,bitWorkorder BIT
		,bitAsset BIT
		,bitKitParent BIT
	)

	INSERT INTO @TempPickListItems
	(
		numOppItemID
		,numItemCode
		,numUnitHour
		,numWarehouseItemID
		,bitWorkorder
		,bitAsset
		,bitKitParent
	)
	SELECT
		OI.numoppitemtCode
		,I.numItemCode
		,OBDI.numUnitHour
		,OI.numWarehouseItmsID
		,ISNULL(OI.bitWorkorder,0)
		,ISNULL(I.bitAsset,0)
		,ISNULL(I.bitKitParent,0)
	FROM
		OpportunityBizDocItems OBDI
	INNER JOIN
		OpportunityItems OI
	ON
		OBDI.numOppItemID = OI.numoppitemtCode
	INNER JOIN 
		WareHouseItems WI 
	ON 
		OI.numWarehouseItmsID=WI.numWareHouseItemID
    INNER JOIN 
		Item I 
	ON 
		OI.numItemCode = I.numItemCode
    WHERE  
		charitemtype='P'
		AND OBDI.numOppBizDocID=@numOppBizDocID
        AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OBDI.numUnitHour,0) > 0 
    ORDER BY 
		OI.numoppitemtCode

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT

	SELECT @iCount=COUNT(*) FROM @TempPickListItems

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numOppItemID=numOppItemID
			,@numItemCode = numItemCode
			,@numUnitHour = numUnitHour
			,@numWareHouseItemID=numWarehouseItemID
			,@bitKitParent=bitKitParent
			,@bitWorkOrder=bitWorkorder
			,@bitAsset=bitAsset
		FROM
			@TempPickListItems
		WHERE
			ID=@i

		SELECT  
			@onHand = ISNULL(numOnHand, 0),
			@onAllocation = ISNULL(numAllocation, 0),
			@onOrder = ISNULL(numOnOrder, 0),
			@onBackOrder = ISNULL(numBackOrder, 0),
			@onReOrder = ISNULL(numReorder,0)
		FROM 
			WareHouseItems
		WHERE
			 numWareHouseItemID = @numWareHouseItemID

		/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
		SET @description=CONCAT('SO Pick List insert/edit (Qty:',@numUnitHour,' Shipped:0)')
				
		IF @bitKitParent = 1 
		BEGIN
			-- CLEAR DATA OF PREVIOUS ITERATION
			DELETE FROM @TempKitSubItems

			-- GET KIT SUB ITEMS DETAIL
			INSERT INTO @TempKitSubItems
			(
				ID,
				numOppChildItemID,
				numChildItemCode,
				bitChildIsKit,
				numChildWarehouseItemID,
				numChildQty,
				numChildQtyShipped
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppChildItemID),
				ISNULL(numOppChildItemID,0),
				ISNULL(I.numItemCode,0),
				ISNULL(I.bitKitParent,0),
				ISNULL(numWareHouseItemId,0),
				((ISNULL(numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,@numDomainID,I.numBaseUnit),1)) * @numUnitHour),
				ISNULL(numQtyShipped,0)
			FROM 
				OpportunityKitItems OKI 
			JOIN 
				Item I 
			ON 
				OKI.numChildItemID=I.numItemCode    
			WHERE 
				charitemtype='P' 
				AND ISNULL(numWareHouseItemId,0) > 0 
				AND OKI.numOppID=@numOppID 
				AND OKI.numOppItemID=@numOppItemID 

			SET @j = 1
			SELECT @ChildCount=COUNT(*) FROM @TempKitSubItems

			--LOOP OVER ALL CHILD ITEMS AND RELESE ALLOCATION
			WHILE @j <= @ChildCount
			BEGIN
				SELECT
					@numOppChildItemID=numOppChildItemID,
					@numChildItemCode=numChildItemCode,
					@bitChildIsKit=bitChildIsKit,
					@numChildWarehouseItemID=numChildWarehouseItemID,
					@numChildQty=numChildQty,
					@numChildQtyShipped=numChildQtyShipped
				FROM
					@TempKitSubItems
				WHERE 
					ID = @j

				SELECT @numChildOnHand=ISNULL(numOnHand,0) FROM WareHouseItems WHERE numWareHouseItemID=@numChildWarehouseItemID

				-- IF CHILD ITEM IS KIT THEN LOOK OUT FOR ITS CHILD ITEMS WHICH ARE STORED IN OpportunityKitChildItems TABLE
				IF @bitChildIsKit = 1
				BEGIN
					-- IF KIT IS ADDED WITHIN KIT THEN THIS SUB KIT HAS ITS OWN ITEM CONFIGURATION WHICH ID STORED IN OpportunityKitChildItems
					IF EXISTS (SELECT numOppKitChildItemID FROM OpportunityKitChildItems WHERE numOppID=@numOppId AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID)
					BEGIN
						-- CLEAR DATA OF PREVIOUS ITERATION
						DELETE FROM @TempKitChildKitSubItems

						-- GET SUB KIT SUB ITEMS DETAIL
						INSERT INTO @TempKitChildKitSubItems
						(
							ID,
							numOppKitChildItemID,
							numKitChildItemCode,
							numKitChildWarehouseItemID,
							numKitChildQty,
							numKitChildQtyShipped
						)
						SELECT 
							ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
							ISNULL(numOppKitChildItemID,0),
							ISNULL(OKCI.numItemID,0),
							ISNULL(OKCI.numWareHouseItemId,0),
							((ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,@numDomainID,I.numBaseUnit),1)) * @numChildQty),
							ISNULL(numQtyShipped,0)
						FROM 
							OpportunityKitChildItems OKCI 
						JOIN 
							Item I 
						ON 
							OKCI.numItemID=I.numItemCode    
						WHERE 
							charitemtype='P' 
							AND ISNULL(numWareHouseItemId,0) > 0 
							AND OKCI.numOppID=@numOppID 
							AND OKCI.numOppItemID=@numOppItemID 
							AND OKCI.numOppChildItemID = @numOppChildItemID

						SET @k = 1
						SELECT @CountKitChildItems=COUNT(*) FROM @TempKitChildKitSubItems

						WHILE @k <= @CountKitChildItems
						BEGIN
							SELECT
								@numOppKitChildItemID=numOppKitChildItemID,
								@numKitChildItemCode=numKitChildItemCode,
								@numKitChildWarehouseItemID=numKitChildWarehouseItemID,
								@numKitChildQty=numKitChildQty,
								@numKitChildQtyShipped=numKitChildQtyShipped
							FROM
								@TempKitChildKitSubItems
							WHERE 
								ID = @k

							UPDATE
								WarehouseItems
							SET 
								numOnHand = (CASE WHEN ISNULL(numOnHand,0) >= @numKitChildQty THEN ISNULL(numOnHand,0) - @numKitChildQty ELSE 0 END)
								,numAllocation = (CASE WHEN ISNULL(numOnHand,0) >= @numKitChildQty THEN ISNULL(numAllocation,0) + @numKitChildQty ELSE ISNULL(numAllocation,0) + ISNULL(numOnHand,0) END)
								,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) >= @numKitChildQty THEN ISNULL(numBackOrder,0) ELSE ISNULL(numBackOrder,0) + (@numKitChildQty - ISNULL(numOnHand,0)) END)
								,dtModified = GETDATE()
							WHERE
								numWareHouseItemID=@numKitChildWarehouseItemID

							SET @vcKitChildDescription = CONCAT('SO Pick List Child Kit insert/edit (Qty:',@numKitChildQty,' Shipped:0)')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numKitChildWarehouseItemID,
								@numReferenceID = @numOppId,
								@tintRefType = 3,
								@vcDescription = @vcKitChildDescription,
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomainID	 

							SET @k = @k + 1
						END
					END

					SET @description=CONCAT('SO Pick List Kit insert/edit (Qty:',@numChildQty,' Shipped:0)')

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numChildWarehouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppID, --  numeric(9, 0)
						@tintRefType = 3, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@dtRecordDate =  @dtItemReceivedDate,
						@numDomainID = @numDomainID
				END
				ELSE
				BEGIN
					UPDATE
						WarehouseItems
					SET 
						numOnHand = (CASE WHEN ISNULL(numOnHand,0) >= @numChildQty THEN ISNULL(numOnHand,0) - @numChildQty ELSE 0 END)
						,numAllocation = (CASE WHEN ISNULL(numOnHand,0) >= @numChildQty THEN ISNULL(numAllocation,0) + @numChildQty ELSE ISNULL(numAllocation,0) + ISNULL(numOnHand,0) END)
						,numBackOrder = (CASE WHEN ISNULL(numOnHand,0) >= @numChildQty THEN ISNULL(numBackOrder,0) ELSE ISNULL(numBackOrder,0) + (@numChildQty - ISNULL(numOnHand,0)) END)
						,dtModified = GETDATE()
					WHERE
						numWareHouseItemID=@numChildWarehouseItemID

					SET @vcChildDescription = CONCAT('SO Pick List Kit insert/edit (Qty:',@numChildQty,' Shipped:0)')

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numChildWarehouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 3,
						@vcDescription = @vcChildDescription,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID
				END

				SET @j = @j + 1
			END
		END 
		ELSE
		BEGIN
			IF @onHand >= @numUnitHour 
			BEGIN                                    
				SET @onHand = @onHand - @numUnitHour                            
				SET @onAllocation = @onAllocation + @numUnitHour                                    
			END                                    
			ELSE IF @onHand < @numUnitHour 
			BEGIN                                    
				SET @onAllocation = @onAllocation + @onHand                                    
				SET @onBackOrder = @onBackOrder + @numUnitHour - @onHand                                    
				SET @onHand = 0                                    
			END    
			                                 
			IF @bitAsset=0--Not Asset
			BEGIN 
				UPDATE
					WareHouseItems
				SET
					numOnHand = @onHand
					,numAllocation = @onAllocation
					,numBackOrder = @onBackOrder
					,dtModified = GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID          
			END
		END  
            
		IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
		BEGIN
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@dtRecordDate =  @dtItemReceivedDate,
				@numDomainID = @numDomainID
		END

		SET @i = @i + 1
	END
END
GO