GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageImportOrganizationContacts' ) 
    DROP PROCEDURE USP_ManageImportOrganizationContacts
GO
CREATE PROCEDURE USP_ManageImportOrganizationContacts
    @numDomainID NUMERIC,
    @numCompanyID NUMERIC OUTPUT,
    @numDivisionID NUMERIC OUTPUT,
    @numNonBizCompanyID VARCHAR(50),
    @tintMode AS TINYINT = 0
AS 
    BEGIN
        IF @tintMode = 0 
            BEGIN
                INSERT  INTO dbo.ImportOrganizationContactReference
                        (
                          numDomainID,
                          numNonBizCompanyID,
                          numCompanyID,
                          numDivisionID
                        )
                VALUES  (
                          @numDomainID,
                          @numNonBizCompanyID,
                          @numCompanyID,
                          @numDivisionID
                        ) 
            END
        IF @tintMode = 1 
            BEGIN
                SELECT TOP 1
                        @numCompanyID = numCompanyID,
                        @numDivisionID = numDivisionID
                FROM    dbo.ImportOrganizationContactReference
                WHERE   numNonBizCompanyID = @numNonBizCompanyID
                        AND numDomainID = @numDomainID
            END
    END
