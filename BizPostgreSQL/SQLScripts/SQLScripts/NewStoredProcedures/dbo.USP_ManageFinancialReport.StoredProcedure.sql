GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageFinancialReport' ) 
    DROP PROCEDURE USP_ManageFinancialReport
GO
CREATE PROC [dbo].[USP_ManageFinancialReport]
    @numFRID NUMERIC(18, 0)=0 OUTPUT,
    @vcReportName VARCHAR(50),
    @numFinancialViewID NUMERIC(18, 0),
    @numFinancialDimensionID NUMERIC(18, 0),
    @intDateRange NUMERIC(18, 0),
    @numDomainID NUMERIC(18, 0),
    @numUserCntID numeric
AS 
IF @numFRID = 0 
    BEGIN
	

        INSERT  INTO [dbo].[FinancialReport] ( [vcReportName],
                                               [numFinancialViewID],
                                               [numFinancialDimensionID],
                                               [intDateRange],
                                               [numDomainID],
                                               [numCreatedBy],
                                               [dtCreateDate],
                                               [numModifiedBy],
                                               [dtModifiedDate]
                                                )
                SELECT  @vcReportName,
                        @numFinancialViewID,
                        @numFinancialDimensionID,
                        @intDateRange,
                        @numDomainID,
                        @numUserCntID,
                        GETUTCDATE(),
                        @numUserCntID,
                        GETUTCDATE()
	
        SET @numFRID = SCOPE_IDENTITY()
    END
ELSE 
    BEGIN
    DECLARE @numOldDimension NUMERIC
    
		SELECT @numOldDimension = numFinancialDimensionID FROM dbo.FinancialReport WHERE numFRID=@numFRID
		IF @numOldDimension <> @numFinancialDimensionID
		BEGIN
			DELETE FROM dbo.FinancialReportDetail WHERE numFRID=@numFRID
		END
    
    
        UPDATE  [dbo].[FinancialReport]
        SET     [vcReportName] = @vcReportName,
                [numFinancialViewID] = @numFinancialViewID,
                [numFinancialDimensionID] = @numFinancialDimensionID,
                [intDateRange] = @intDateRange,
                [numDomainID] = @numDomainID,
                [numModifiedBy] = @numUserCntID,
                [dtModifiedDate] = GETUTCDATE()
        WHERE   [numFRID] = @numFRID

    END
    
GO