GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetActivityInfo')
DROP PROCEDURE usp_GetActivityInfo
GO

CREATE PROCEDURE [dbo].[usp_GetActivityInfo]                                              
	@numActivityid NUMERIC(18,0)=0,                            
	@ClientTimeZoneOffset Int                
AS 
BEGIN
                   
	SELECT TOP 1 
		CI.vcCompanyName
		,isnull(AD.vcFirstName,'-')+' '+isnull(AD.vcLastName,'') AS ContactName
		,AD.vcEmail
		,AD.numPhone As numPhone
		,AD.numDivisionId,AD.numContactId
		,DateAdd(minute, -@ClientTimeZoneOffset,AC.StartDateTimeUtc ) as dtStartTime
		,DateAdd(minute, -@ClientTimeZoneOffset,DATEADD(second,AC.Duration,StartDateTimeUtc)) as dtEndTime
		,AC.ActivityID As numActivityId
		,(Select numListItemID from ListDetails where vcData='Calendar' and numListID=73 and constFlag=1) As bitTask
		,Ac.Priority
		,Ac.Activity
		,Ac.FollowUpStatus
		,Ac.Comments
	FROM 
		Activity AC
	INNER JOIN 
		ActivityAttendees AT 
	ON 
		Ac.ActivityID=AT.ActivityID
	INNER JOIN 
		CompanyInfo CI 
	ON 
		AT.CompanyNameinBiz=CI.numCompanyId
	INNER JOIN 
		AdditionalContactsInformation AD 
	ON 
		AT.DivisionID=AD.numDivisionId ---and AT.ContactID=AD.numContactId
	WHERE 
		AC.ActivityID=@numActivityid
	ORDER BY 
		AttendeeID DESC
END
GO



