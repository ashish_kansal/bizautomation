GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetAddressDetails' ) 
    DROP PROCEDURE USP_GetAddressDetails
GO
CREATE PROCEDURE USP_GetAddressDetails
    @tintMode TINYINT,
    @numDomainID NUMERIC,
    @numRecordID NUMERIC,
    @numAddressID NUMERIC,
    @tintAddressType TINYINT,
    @tintAddressOf TINYINT
AS 
BEGIN

    IF @tintMode = 1 
    BEGIN
        SELECT  numAddressID,
                vcAddressName,
                vcStreet,
                vcCity,
                vcPostalCode,
                numState,
                numCountry,
                bitIsPrimary,
				bitResidential,
                tintAddressOf,
                tintAddressType,
                '<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                        + isnull(dbo.fn_GetState(numState),'') + ' '
                        + isnull(vcPostalCode,'') + ' <br>'
                        + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
				,ISNULL((SELECT TOP 1 numWarehouseID FROM Warehouses WHERE numDomainID=@numDomainID AND numAddressID=AddressDetails.numAddressID),0) AS numWarehouseID
				,ISNULL(numContact,0) numContact
				,ISNULL(bitAltContact,0) bitAltContact
				,ISNULL(vcAltContact,'') vcAltContact
				,(CASE 
					WHEN tintAddressOf = 2 
					THEN ISNULL((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE DM.numDomainID=@numDomainID AND DM.numDivisionID=AddressDetails.numRecordID),'')
					ELSE ISNULL((SELECT CI.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId = DM.numDivisionID INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE ACI.numDomainID=@numDomainID AND DM.numDivisionID=AddressDetails.numRecordID),'')
				END) vcCompanyName
        FROM    dbo.AddressDetails
        WHERE   numDomainID = @numDomainID
                AND numAddressID = @numAddressID
            
    END
    IF @tintMode = 2
    BEGIN
        SELECT  numAddressID,
                vcAddressName,
				vcStreet,
                vcCity,
                isnull(vcPostalCode,'') vcPostalCode,
                numState,
                numCountry,
				'<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                        + isnull(dbo.fn_GetState(numState),'') + ' '
                        + isnull(vcPostalCode,'') + ' <br>'
                        + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
				,ISNULL(numContact,0) numContact
				,ISNULL(bitAltContact,0) bitAltContact
				,ISNULL(vcAltContact,'') vcAltContact
				,(CASE WHEN @tintAddressOf=2 THEN ISNULL((SELECT TOP 1 numContactID FROM AdditionalCOntactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AddressDetails.numRecordID),0) ELSE 0 END) numPrimaryContact
					
				,isnull(vcStreet + ', ','')  + isnull(vcCity + ', ','') 
                        + isnull(dbo.fn_GetState(numState) + ', ','') 
                        + isnull(vcPostalCode + ', ','') 
                        + isnull(dbo.fn_GetListItemName(numCountry),'') FullAddress
				,(CASE 
					WHEN tintAddressOf = 2 
					THEN ISNULL((SELECT CI.vcCompanyName FROM DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE DM.numDomainID=@numDomainID AND DM.numDivisionID=AddressDetails.numRecordID),'')
					ELSE ISNULL((SELECT CI.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId = DM.numDivisionID INNER JOIN CompanyInfo CI ON DM.numCompanyID=CI.numCompanyId WHERE ACI.numDomainID=@numDomainID AND DM.numDivisionID=AddressDetails.numRecordID),'')
				END) vcCompanyName
        FROM    dbo.AddressDetails
        WHERE   numDomainID = @numDomainID
                AND numRecordID = @numRecordID	
                AND tintAddressOf = @tintAddressOf
                AND tintAddressType = @tintAddressType
        ORDER BY bitIsPrimary desc
    END
    IF @tintMode = 3
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
                    vcStreet,
                    vcCity,
                    vcPostalCode,
                    numState,
                    numCountry,
                    bitIsPrimary,
					bitResidential,
                    tintAddressOf,
                    tintAddressType,
                    '<pre>' + isnull(vcStreet,'') + '</pre> ' + isnull(vcCity,'') + ', '
                            + isnull(dbo.fn_GetState(numState),'') + ' '
                            + isnull(vcPostalCode,'') + ' <br>'
                            + isnull(dbo.fn_GetListItemName(numCountry),'') vcFullAddress
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numRecordID = @numRecordID	
                    AND tintAddressOf = @tintAddressOf
                    AND tintAddressType = @tintAddressType
                    AND bitIsPrimary = 1
            ORDER BY bitIsPrimary desc
        END

		IF @tintMode = 4
        BEGIN
            SELECT  numAddressID,
                    vcAddressName,
					isnull(vcPostalCode,'') vcPostalCode
					,ISNULL(numContact,0) numContact
					,ISNULL(bitAltContact,0) bitAltContact
					,ISNULL(vcAltContact,'') vcAltContact
					,isnull(vcStreet + ', ','') + isnull(vcCity + ', ','')
                            + isnull(dbo.fn_GetState(numState) + ', ','')
                            + isnull(vcPostalCode + ', ','') 
                            + isnull(dbo.fn_GetListItemName(numCountry),'') FullAddress
					, ISNULL((SELECT TOP 1 vcStateCode FROM ShippingStateMaster where vcStateName= isnull(dbo.fn_GetState(numState),'')),'') AS StateAbbr
					, ISNULL((SELECT TOP 1 vcCountryCode from ShippingCountryMaster where vcCountryName= isnull(dbo.fn_GetListItemName(numCountry),'')),'') AS CountryAbbr
		
            FROM    dbo.AddressDetails
            WHERE   numDomainID = @numDomainID
                    AND numRecordID = @numRecordID	
                    --AND tintAddressOf = @tintAddressOf
                    --AND tintAddressType = @tintAddressType
					AND numAddressID = @numAddressID
            ORDER BY bitIsPrimary desc
        END
    
        

            
END