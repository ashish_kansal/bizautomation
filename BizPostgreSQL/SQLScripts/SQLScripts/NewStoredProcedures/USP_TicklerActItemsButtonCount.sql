GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsButtonCount')
DROP PROCEDURE USP_TicklerActItemsButtonCount
GO
CREATE Proc [dbo].[USP_TicklerActItemsButtonCount]     
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,
@numViewID AS INT,
@numTotalRecords AS NUMERIC(18,0)=0 OUTPUT,
@TypeId AS INT = 0
AS
BEGIN
IF(@TypeId=0)
BEGIN
	EXEC USP_TicklerActItemsV2TopCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =@numViewID,@numTotalRecords=@numTotalRecords OUTPUT
END
IF(@TypeId=1)
BEGIN
EXEC USP_TicklerActItemsV2TopPurchaseRecordCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =@numViewID,@numTotalRecords=@numTotalRecords OUTPUT
END
--   DECLARE @ItemsToPick AS NUMERIC(18,0)=0
--   DECLARE @ItemsToPackShip AS NUMERIC(18,0)=0
--   DECLARE @ItemsToInvoice AS NUMERIC(18,0)=0
--   DECLARE @SalesOrderToClose AS NUMERIC(18,0)=0
--   DECLARE @ItemsToPutAway AS NUMERIC(18,0)=0
--   DECLARE @ItemsToBill AS NUMERIC(18,0)=0
--   DECLARE @ItemsPOClose AS NUMERIC(18,0)=0
--   DECLARE @ItemsBOMPick AS NUMERIC(18,0)=0

--   DECLARE @bitItemsToPickPackShip AS BIT
--   DECLARE @bitItemsToInvoice AS BIT
--   DECLARE @bitSalesOrderToClose AS BIT
--   DECLARE @bitItemsToPutAway AS BIT
--   DECLARE @bitItemsToBill AS BIT
--   DECLARE @bitPosToClose AS BIT
--   DECLARE @bitBOMSToPick AS BIT

--   DECLARE @vchItemsToPickPackShip AS VARCHAR(500)
--   DECLARE @vchItemsToInvoice AS VARCHAR(500)
--   DECLARE @vchSalesOrderToClose AS VARCHAR(500)
--   DECLARE @vchItemsToPutAway AS VARCHAR(500)
--   DECLARE @vchItemsToBill AS VARCHAR(500)
--   DECLARE @vchPosToClose AS VARCHAR(500)
--   DECLARE @vchBOMSToPick AS VARCHAR(500)

--   SELECT 
--		@bitItemsToPickPackShip=bitItemsToPickPackShip ,
--		@bitItemsToInvoice=bitItemsToInvoice, 
--		@bitSalesOrderToClose=bitSalesOrderToClose ,
--		@bitItemsToPutAway=bitItemsToPutAway ,
--		@bitItemsToBill=bitItemsToBill ,
--		@bitPosToClose=bitPosToClose ,
--		@bitBOMSToPick = bitBOMSToPick,
--		@vchItemsToPickPackShip = vchItemsToPickPackShip,
--		@vchItemsToInvoice = vchItemsToInvoice,
--		@vchSalesOrderToClose = vchSalesOrderToClose,
--		@vchItemsToPutAway = vchItemsToPutAway,
--		@vchItemsToBill = vchItemsToBill,
--		@vchPosToClose = vchPosToClose,
--		@vchBOMSToPick = vchBOMSToPick
--   FROM 
--		Domain
--   WHERE
--		numDomainId=@numDomainID

--   IF(@bitItemsToPickPackShip=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchItemsToPickPackShip,',') WHERE Items<>''))
--   BEGIN
--	EXEC USP_TicklerActItemsV2TopCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =1,@numTotalRecords=@ItemsToPick OUTPUT
--   END
--   IF(@bitItemsToPickPackShip=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchItemsToPickPackShip,',') WHERE Items<>''))
--   BEGIN
--	EXEC USP_TicklerActItemsV2TopCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =2,@numTotalRecords=@ItemsToPackShip OUTPUT
--   END
--   IF(@bitItemsToInvoice=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchItemsToInvoice,',') WHERE Items<>''))
--   BEGIN
--		EXEC USP_TicklerActItemsV2TopCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =3,@numTotalRecords=@ItemsToInvoice OUTPUT
--   END
--   IF(@bitSalesOrderToClose=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchSalesOrderToClose,',') WHERE Items<>''))
--   BEGIN
--		EXEC USP_TicklerActItemsV2TopCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =5,@numTotalRecords=@SalesOrderToClose OUTPUT
--   END
--   IF(@bitItemsToPutAway=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchItemsToPutAway,',') WHERE Items<>''))
--   BEGIN
--	EXEC USP_TicklerActItemsV2TopPurchaseRecordCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =2,@numTotalRecords=@ItemsToPutAway OUTPUT
--   END
--    IF(@bitItemsToBill=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchItemsToBill,',') WHERE Items<>''))
--	BEGIN
--		EXEC USP_TicklerActItemsV2TopPurchaseRecordCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =3,@numTotalRecords=@ItemsToBill OUTPUT
--	END
-- IF(@bitPosToClose=1 AND @numUserCntID IN(SELECT Items FROM Split(@vchPosToClose,',') WHERE Items<>''))
--	BEGIN
--   EXEC USP_TicklerActItemsV2TopPurchaseRecordCount @numUserCntID = @numUserCntID, @numDomainID = @numDomainID,@startDate = @startDate, @endDate = @endDate,@ClientTimeZoneOffset = @ClientTimeZoneOffset,@numViewID =4,@numTotalRecords=@ItemsPOClose OUTPUT
--END
 --  SELECT 
	--@ItemsToPick AS ItemsToPick,
	--@ItemsToPackShip AS ItemsToPackShip,
	--@ItemsToInvoice AS ItemsToInvoice,
	--@SalesOrderToClose AS SalesOrderToClose,
	--@ItemsToPutAway AS ItemsToPutAway,
	--@ItemsToBill AS ItemsToBill,
	--@ItemsPOClose AS ItemsPOClose,
	--@ItemsBOMPick AS ItemsBOMPick
END