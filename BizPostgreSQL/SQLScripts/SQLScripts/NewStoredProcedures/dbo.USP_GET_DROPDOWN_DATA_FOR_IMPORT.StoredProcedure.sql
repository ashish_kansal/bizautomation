GO
IF EXISTS(SELECT * FROM sysobjects SYSOBJ WHERE SYSOBJ.xtype='p'AND SYSOBJ.NAME ='USP_GET_DROPDOWN_DATA_FOR_IMPORT')
DROP PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_DROPDOWN_DATA_FOR_IMPORT 75,1
CREATE PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
(
	@intImportFileID BIGINT,
	@numDomainID BIGINT,
	@tintMode TINYINT,
	@numFormID NUMERIC(18,0),
	@vcFields VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @TempFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,intMapColumnNo INT
		,bitCustom BIT
	)

	IF @tintMode = 2
	BEGIN
		DECLARE @hDocItem int
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFields

		INSERT INTO @TempFields
		(
			numFieldID
			,intMapColumnNo
			,bitCustom
		)
		SELECT
			FormFieldID
			,ImportFieldID
			,IsCustomField
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Table1',2)
		WITH
		(
			FormFieldID NUMERIC(18,0)
			,ImportFieldID NUMERIC(18,0)
			,IsCustomField BIT
		)
	
		EXEC sp_xml_removedocument @hDocItem
	END
	ELSE
	BEGIN
		SELECT @numFormID = numMasterID  FROM dbo.Import_File_Master  WHERE intImportFileID=@intImportFileID
	
		INSERT INTO @TempFields
		(
			numFieldID
			,intMapColumnNo
			,bitCustom
		)
		SELECT
			numFormFieldID
			,intImportFileID
			,ISNULL(bitCustomField,0)
		FROM
			Import_File_Field_Mapping
		WHERE
			intImportFileID=@intImportFileID
	END

	SELECT 
		ROW_NUMBER() OVER (ORDER BY intMapColumnNo) AS [ID]
		,* 
	INTO 
		#tempDropDownData 
	FROM
	(
		SELECT  
			DFFM.numFieldId [numFormFieldId],
			DFFM.vcAssociatedControlType,
			DYNAMIC_FIELD.vcDbColumnName,
			DFFM.vcFieldName [vcFormFieldName],
			FIELD_MAP.intMapColumnNo,
			DYNAMIC_FIELD.numListID,
			0 AS [bitCustomField]					
		FROM 
			@TempFields FIELD_MAP
		INNER JOIN dbo.DycFormField_Mapping DFFM ON FIELD_MAP.numFieldID = DFFM.numFieldId
		INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD.numFieldId = DFFM.numFieldId
		WHERE 
			ISNULL(FIELD_MAP.bitCustom,0) = 0
			AND DFFM.vcAssociatedControlType ='SelectBox'  
			AND DFFM.numFormID = @numFormID	
		UNION ALL
		SELECT 
			CFm.Fld_id AS [numFormFieldId],
			CASE WHEN CFM.Fld_type = 'Drop Down List Box'
					THEN 'SelectBox'
					ELSE CFM.Fld_type 
			END	 AS [vcAssociatedControlType],
			cfm.Fld_label AS [vcDbColumnName],
			cfm.Fld_label AS [vcFormFieldName],
			FIELD_MAP.intMapColumnNo  AS [intMapColumnNo],
			CFM.numlistid AS [numListID],
			1 AS [bitCustomField]
		FROM 
			dbo.CFW_Fld_Master CFM
		INNER JOIN @TempFields FIELD_MAP ON CFM.Fld_id = FIELD_MAP.numFieldID AND ISNULL(FIELD_MAP.bitCustom,0) = 1	
		WHERE 
			numDomainID = @numDomainID
	) TABLE1 
	WHERE  
		vcAssociatedControlType = 'SelectBox' 
              
	SELECT * FROM #tempDropDownData   
	
	DECLARE @intRow AS INT
	SELECT @intRow = COUNT(*) FROM #tempDropDownData
	PRINT @intRow
	
	DECLARE @intCnt AS INT
	DECLARE @numListIDValue AS BIGINT
	DECLARE @numFormFieldIDValue AS BIGINT
	DECLARE @bitCustomField AS BIT
		
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @vcTableNames AS VARCHAR(MAX) 
	DECLARE @strDBColumnName AS NVARCHAR(1000)
	
	SET @intCnt = 0
	SET @strSQL = '' 
	SET @vcTableNames = ''

	CREATE TABLE #tempGroups
	(
		[Key] NUMERIC(9),
		[Value] VARCHAR(100)
	) 	
	INSERT INTO #tempGroups([Key],[Value]) 
	SELECT 0 AS [Key],'Groups' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospects' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospect' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Pros' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Accounts' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Account' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Acc' AS [Value]

	--SELECT * FROM #tempGroups
	
	WHILE(@intCnt < @intRow   )
		BEGIN
			SET @intCnt = @intCnt + 1
				
			SELECT  
					@numListIDValue = numListID,
					@strDBColumnName = vcDbColumnName, 
					@numFormFieldIDValue = numFormFieldID,
					@bitCustomField = bitCustomField 	 
			FROM #tempDropDownData 
			WHERE ID = @intCnt
			
			PRINT 'BIT'
			PRINT @bitCustomField
			PRINT @strDBColumnName
			SELECT @strSQL = CASE  
								WHEN @strDBColumnName = 'numItemGroup'
								THEN @strSQL + 'SELECT ITEM_GROUP.numItemGroupID AS [Key],ITEM_GROUP.vcItemGroup AS [Value] FROM dbo.ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'tintCRMType'
								THEN @strSQL + 'SELECT DISTINCT [Key],[Value] FROM #tempGroups '
								
								WHEN @strDBColumnName = 'vcLocation'
								THEN @strSQL + 'SELECT numWLocationID AS [Key],vcLocation AS [Value] FROM dbo.WarehouseLocation WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'vcExportToAPI'
								THEN @strSQL + 'SELECT DISTINCT WebApiId AS [Key],vcProviderName AS [Value] FROM dbo.WebAPI '
								
								WHEN @strDBColumnName = 'numCategoryID'
								THEN @strSQL + 'SELECT numCategoryID AS [Key], vcCategoryName AS [Value] FROM dbo.Category WHERE numDomainID =' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'numGrpID'
								THEN @strSQL + 'SELECT GRP.numGrpID AS [Key],GRP.vcGrpName AS [Value] FROM dbo.Groups GRP ' 
											    			    
								WHEN @strDBColumnName = 'numWareHouseID' --OR @strDBColumnName = 'numWareHouseItemID'															  
								THEN @strSQL + 'SELECT WAREHOUSE.numWareHouseID AS [Key],WAREHOUSE.vcWareHouse AS [Value] FROM dbo.Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainID) 
								
								WHEN @strDBColumnName = 'numWareHouseItemID'
								THEN @strSQL + 'SELECT WI.numWareHouseItemID AS [Key],W.vcWareHouse AS [Value] FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numWareHouseID = W.numWareHouseID WHERE W.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
												
								WHEN @strDBColumnName = 'numChildItemID' OR @strDBColumnName = 'numItemKitID'
								THEN @strSQL + ' SELECT ITEM.numItemCode AS [Key],ITEM.vcItemName AS [Value] FROM dbo.Item ITEM WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
								
								WHEN (@strDBColumnName = 'numUOMId' OR @strDBColumnName = 'numPurchaseUnit' OR @strDBColumnName = 'numSaleUnit' OR @strDBColumnName = 'numBaseUnit') 
								THEN @strSQL + '  SELECT numUOMId AS [Key], vcUnitName AS [Value] FROM dbo.UOM
												  JOIN Domain d ON dbo.UOM.numDomainID = d.numDomainID 
												  WHERE dbo.UOM.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) +' 
												  AND bitEnabled=1 
												  AND dbo.UOM.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,''E'')=''E'' THEN 1 WHEN d.charUnitSystem=''M'' THEN 2 END) '	
								
								WHEN (@strDBColumnName = 'numIncomeChartAcntId' OR @strDBColumnName = 'numAssetChartAcntId' OR @strDBColumnName = 'numCOGsChartAcntId')
								THEN @strSQL + '  SELECT numAccountId AS [Key], LTRIM(RTRIM(REPLACE(vcAccountName,ISNULL(vcNumber,''''),''''))) AS [Value] FROM dbo.Chart_Of_Accounts WHERE numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID)

								WHEN @strDBColumnName = 'numCampaignID'
								THEN @strSQL + '  SELECT numCampaignID AS [Key], vcCampaignName AS [Value] FROM  CampaignMaster WHERE CampaignMaster.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numVendorID'
								THEN @strSQL + '  SELECT DM.numDivisionID AS [Key], CI.vcCompanyName AS [Value] FROM dbo.CompanyInfo CI INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId 
												  WHERE DM.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numAssignedTo' OR @strDBColumnName = 'numRecOwner' 
								THEN @strSQL + '  SELECT A.numContactID AS [Key], A.vcFirstName+'' ''+A.vcLastName AS [Value] from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID  where UM.numDomainID = A.numDomainID AND UM.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)	
								
------								 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName from UserMaster UM         
------								 join AdditionalContactsInformation A        
------								 on UM.numUserDetailId=A.numContactID          
------								 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID   
							
								WHEN @strDBColumnName = 'numTermsID'
								THEN @strSQL + 'SELECT DISTINCT numTermsID AS [Key],vcTerms AS [Value] FROM BillingTerms 
												WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID) 
												
								WHEN @strDBColumnName = 'numShipState' OR @strDBColumnName = 'numBillState' OR @strDBColumnName = 'numState' OR @strDBColumnName = 'State'
								THEN @strSQL + '  SELECT numStateID AS [Key], vcState AS [Value], (CASE WHEN LEN(ISNULL(vcAbbreviations,'''')) > 0 THEN vcAbbreviations ELSE ''NOTAVAILABLE'' END) vcAbbreviations FROM State where numDomainID=' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'charItemType'
								THEN @strSQL + ' SELECT ''A'' AS [Key], ''asset'' AS [Value]
											     UNION 				
											     SELECT ''P'' AS [Key], ''inventory item'' AS [Value]
											     UNION 				
											     SELECT ''S'' AS [Key], ''service'' AS [Value]
											     UNION 				
											     SELECT ''N'' AS [Key], ''non-inventory item'' AS [Value]'	
								
								WHEN @strDBColumnName = 'tintRuleType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Deduct from List price'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Add to Primary Vendor Cost'' AS [Value]
											     UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'	
											     
								WHEN @strDBColumnName = 'tintDiscountType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Percentage'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Flat Amount'' AS [Value]
												 UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'
												 											     
								WHEN @strDBColumnName = 'numShippingService'
								THEN @strSQL + 'SELECT 
													ShippingService.numShippingServiceID AS [Key]
													,ShippingService.vcShipmentService AS [Value]
													,ISNULL(ShippingServiceAbbreviations.vcAbbreviation,'''') vcAbbreviations
												FROM 
													ShippingService 
												LEFT JOIN
													ShippingServiceAbbreviations
												ON
													ShippingServiceAbbreviations.numShippingServiceID = ShippingService.numShippingServiceID
												WHERE 
													ShippingService.numDomainID=' + CONVERT(VARCHAR,@numDomainID) + ' OR ISNULL(ShippingService.numDomainID,0) = 0'					     																		    
								WHEN @strDBColumnName = 'numChartAcntId'
								THEN @strSQL + 'SELECT 
													numAccountId AS [Key]
													,vcAccountName AS [Value] 
												FROM 
													Chart_Of_Accounts 
												WHERE 
													numDomainId=' + CONVERT(VARCHAR,@numDomainID)
								ELSE 
									 @strSQL + ' SELECT LIST.numListItemID AS [Key],vcData AS [Value] FROM ListDetails LIST LEFT JOIN listorder LIST_ORDER ON LIST.numListItemID = LIST_ORDER.numListItemID AND LIST_ORDER.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) + ' WHERE ( LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR LIST.constFlag = 1) AND LIST.numListID = ' + CONVERT(VARCHAR,@numListIDValue) + ' AND  (LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR constFlag = 1 )  ORDER BY intSortOrder '
						     END						
						     
			SET @vcTableNames = @vcTableNames + CONVERT(VARCHAR,@numFormFieldIDValue) + '_' + @strDBColumnName + ',' 
		END
	
	PRINT @vcTableNames
	PRINT @strSQL	
	
	SELECT @vcTableNames AS TableNames
	EXEC SP_EXECUTESQL @strSQL		
	
	DROP TABLE #tempDropDownData
	DROP TABLE #tempGroups
END
