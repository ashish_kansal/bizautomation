GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatedomainSMTP')
DROP PROCEDURE USP_UpdatedomainSMTP
GO
CREATE PROCEDURE USP_UpdatedomainSMTP
@numDomainID as numeric(9)=0,
@SMTPAuth as bit   ,      
@SMTPSSL as bit   ,      
@vcSMTPPassword as varchar(100),       
@SMTPServer as varchar(200),       
@SMTPPort  as numeric(9),      
@bitSMTPServer as bit ,    
@vcSMTPUserName as varchar (100),
@vcPSMTPDisplayName VARCHAR(50)

as                                      

update Domain                                       
set  
[bitPSMTPAuth]=@SMTPAuth      
 ,[vcPSmtpPassword]=@vcSMTPPassword      
 ,[vcPSMTPServer]=@SMTPServer      
 ,[numPSMTPPort]=@SMTPPort      
 ,[bitPSMTPSSL]  =  @SMTPSSL     
 ,bitPSMTPServer=@bitSMTPServer  ,    
 vcPSMTPUserName=@vcSMTPUserName,
 vcPSMTPDisplayName=@vcPSMTPDisplayName
where numDomainId=@numDomainID