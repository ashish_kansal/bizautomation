SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CompanyInfo_UpdateRating')
DROP PROCEDURE dbo.USP_CompanyInfo_UpdateRating
GO
CREATE PROCEDURE [dbo].[USP_CompanyInfo_UpdateRating]
(
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@tintPerformance TINYINT
)
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION

		UPDATE
			CI
		SET
			CI.numCompanyRating = ISNULL((SELECT numOrganizationRatingID FROM OrganizationRatingRule ORR WHERE ORR.numDomainID = @numDomainID AND Temp.Amount  BETWEEN ORR.numFromAmount AND ORR.numToAmount),0)
		FROM
			CompanyInfo CI
		INNER JOIN
			DivisionMaster D
		ON
			CI.numCompanyId=D.numCompanyID
		OUTER APPLY
		(
			SELECT
				SUM(OM.monDealAmount) Amount
			FROM
				OpportunityMaster OM
			WHERE
				OM.numDivisionId = D.numDivisionID
				AND OM.tintOppType = 1
				AND tintOppStatus = 1
				AND 1 = (
							CASE @tintPerformance
							WHEN 1 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(MONTH,-3,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
							WHEN 3 THEN (CASE WHEN OM.bintCreatedDate BETWEEN DATEADD(YEAR,-1,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
							END
						)
		) AS Temp
		WHERE
			CI.numDomainID = @numDomainID
			AND D.numDomainID = @numDomainID
			AND (D.numDivisionID = @numDivisionID OR @numDivisionID=0)
	
COMMIT
END TRY
BEGIN CATCH

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

END CATCH
END