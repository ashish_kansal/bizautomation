SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatingInventoryForEmbeddedKits')
DROP PROCEDURE USP_UpdatingInventoryForEmbeddedKits
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryForEmbeddedKits]    
	@numOppItemID AS NUMERIC(18,0),
	@Units AS FLOAT,
	@Mode AS TINYINT,
	@numOppID AS NUMERIC(9),
	@tintMode AS TINYINT=0, -- 0:Add/Edit,1:Delete,2: DEMOTED TO OPPORTUNITY
	@numUserCntID AS NUMERIC(9)
AS                            
BEGIN

	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @tintOppType AS TINYINT = 0

	SELECT 
		@numDomain = numDomainID, 
		@tintOppType=tintOppType 
	FROM 
		OpportunityMaster
	WHERE 
		numOppId = @numOppID

	SELECT 
		OKI.numOppChildItemID,
		I.numItemCode,
		I.bitAssembly,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped,
		ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
	INTO 
		#tempKits
	FROM 
		OpportunityKitItems OKI 
	JOIN 
		Item I 
	ON 
		OKI.numChildItemID=I.numItemCode    
	WHERE 
		charitemtype='P' 
		AND numWareHouseItemId > 0 
		AND numWareHouseItemId IS NOT NULL 
		AND OKI.numOppID=@numOppID 
		AND OKI.numOppItemID=@numOppItemID 
  

	DECLARE @minRowNumber INT
	DECLARE @maxRowNumber INT

	DECLARE @numOppChildItemID AS NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @bitAssembly BIT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	
	DECLARE @onHand FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onBackOrder FLOAT
	DECLARE @onReOrder FLOAT
	DECLARE @numUnits FLOAT
	DECLARE @QtyShipped FLOAT
	

	SELECT  
		@minRowNumber = MIN(RowNumber),
		@maxRowNumber = MAX(RowNumber) 
	FROM 
		#tempKits


	DECLARE @description AS VARCHAR(300)

	WHILE  @minRowNumber <= @maxRowNumber
    BEGIN
		SELECT 
			@numOppChildItemID = numOppChildItemID,
			@numItemCode=numItemCode,
			@bitAssembly=ISNULL(bitAssembly,0),
			@numWareHouseItemID=numWareHouseItemID,
			@numUnits=numQtyItemsReq,
			@QtyShipped=numQtyShipped 
		FROM 
			#tempKits 
		WHERE 
			RowNumber=@minRowNumber
    
		IF @Mode=0
				SET @description=CONCAT('SO KIT insert/edit (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
		ELSE IF @Mode=1
			IF @tintMode=2
			BEGIN
				SET @description=CONCAT('SO DEMOTED TO OPPORTUNITY KIT (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
			END
			ELSE
			BEGIN
				SET @description=CONCAT('SO KIT Deleted (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
			END
		ELSE IF @Mode=2
				SET @description=CONCAT('SO KIT Close (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
		ELSE IF @Mode=3
				SET @description=CONCAT('SO KIT Re-Open (Qty:',@numUnits,' Shipped:',@QtyShipped,')')

		-- IF KIT ITEM CONTAINS ANOTHER KIT AS CHILD THEN WE HAVE TO UPDATE INVENTORY OF ITEMS OF KIT CHILD ITEMS
		IF EXISTS (SELECT [numOppKitChildItemID] FROM OpportunityKitChildItems WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID = @numOppChildItemID)
		BEGIN
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain


			EXEC USP_WareHouseItems_ManageInventoryKitWithinKit
				@numOppID = @numOppID,
				@numOppItemID = @numOppItemID,
				@numOppChildItemID = @numOppChildItemID,
				@Mode = @Mode,
				@tintMode = @tintMode,
				@numDomainID = @numDomain,
				@numUserCntID = @numUserCntID,
				@tintOppType = @tintOppType
		END
		ELSE
		BEGIN
			SELECT 
				@onHand=ISNULL(numOnHand,0),
				@onAllocation=ISNULL(numAllocation,0),                                    
				@onOrder=ISNULL(numOnOrder,0),
				@onBackOrder=ISNULL(numBackOrder,0),
				@onReOrder = ISNULL(numReorder,0)                                     
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID

    		IF @Mode=0 --insert/edit
    		BEGIN
    			SET @numUnits = @numUnits - @QtyShipped
    	
				IF @onHand>=@numUnits                                    
				BEGIN                                    
					SET @onHand=@onHand-@numUnits                            
					SET @onAllocation=@onAllocation+@numUnits                                    
				END                                    
				ELSE IF @onHand<@numUnits                                    
				BEGIN                                    
					SET @onAllocation=@onAllocation+@onHand                                    
					SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
					SET @onHand=0                                    
				END                        
    		END
			ELSE IF @Mode=1 --Revert
			BEGIN
				IF @QtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @QtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @QtyShipped 
				END 
								                    
				IF @numUnits >= @onBackOrder 
				BEGIN
					SET @numUnits = @numUnits - @onBackOrder
					SET @onBackOrder = 0
                            
					IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits
					SET @onHand = @onHand + @numUnits                                            
				END                                            
				ELSE IF @numUnits < @onBackOrder 
				BEGIN                  
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
				END 
			END
			ELSE IF @Mode=2 --Close
			BEGIN
				SET @numUnits = @numUnits - @QtyShipped  
				IF @onAllocation >= @numUnits
				BEGIN
					SET @onAllocation = @onAllocation - @numUnits            
				END
				ELSE
				BEGIN
					RAISERROR ('NOTSUFFICIENTQTY_ALLOCATION',16,1);
				END
			END
			ELSE IF @Mode=3 --Re-Open
			BEGIN
				SET @numUnits = @numUnits - @QtyShipped
				SET @onAllocation = @onAllocation + @numUnits            
			END

			UPDATE 
				WareHouseItems 
			SET 
				numOnHand=@onHand,
				numAllocation=@onAllocation,                                    
				numBackOrder=@onBackOrder,
				dtModified = GETDATE()  
			WHERE 
				numWareHouseItemID=@numWareHouseItemID 
	
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppID, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
		END
		
		SET @minRowNumber=@minRowNumber + 1
    END	
  
	DROP TABLE #tempKits
END
GO
