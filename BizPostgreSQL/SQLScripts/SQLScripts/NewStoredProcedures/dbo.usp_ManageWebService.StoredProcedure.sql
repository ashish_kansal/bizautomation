
GO
/****** Object:  StoredProcedure [dbo].[usp_ManageWebService]    Script Date: 06/01/2009 23:45:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ManageWebService')
DROP PROCEDURE usp_ManageWebService
GO
CREATE PROCEDURE [dbo].[usp_ManageWebService]
	@numWebServiceId NUMERIC(9,0) = 0 ,
	@vcToken varchar(500),
	@numDomainID NUMERIC(9,0),
	@numUserCntId NUMERIC(9,0)
AS
BEGIN
	IF (SELECT COUNT(*) FROM WebService WHERE numDomainID= @numDomainID) = 0 
	BEGIN
		INSERT INTO WebService (
			[vcToken],
			[numDomainID],
			[dtCreatedDate],
			[numCreatedby],
			[dtModifiedDate],
			[numModifiedBy]
		)
		VALUES (
			@vcToken,
			@numDomainID,
			GETUTCDATE(),
			@numUserCntId,
			GETUTCDATE(),
			@numUserCntId
		)
		
	END
	ELSE BEGIN
		UPDATE WebService SET 
			[vcToken] = @vcToken,
			[numDomainID] = @numDomainID,
			[dtModifiedDate] = GETUTCDATE(),
			[numModifiedBy] = @numUserCntId
		WHERE numDomainID = @numDomainID

	END
	
END