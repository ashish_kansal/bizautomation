SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetUsedPromotion')
DROP PROCEDURE USP_OpportunityMaster_GetUsedPromotion
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetUsedPromotion]
	@numDomainID NUMERIC(18,0),
	@numOppID NUMERIC(18,0)
AS
BEGIN
	DECLARE @numDivisionID AS NUMERIC(18,0)
	DECLARE @tintShipToAddressType AS TINYINT
	DECLARE @numShipToCountry AS NUMERIC(18,0)

	SELECT  
		@tintShipToAddressType = tintShipToType,
		@numDivisionID = numDivisionId
    FROM    
		OpportunityMaster
    WHERE   
		numOppId = @numOppID
               
    IF (@tintShipToAddressType IS NULL OR @tintShipToAddressType = 1) 
        SELECT  
			@numShipToCountry = AD.numCountry
        FROM    
			dbo.AddressDetails AD
        WHERE   
			AD.numRecordID = @numDivisionID
            AND AD.tintAddressOf = 2
            AND AD.tintAddressType = 2
            AND AD.bitIsPrimary = 1
    ELSE IF @tintShipToAddressType = 0 
        SELECT 
			@numShipToCountry = AD1.numCountry
        FROM    
			companyinfo Com1
        JOIN 
			divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
        JOIN 
			Domain D1 ON D1.numDivisionID = div1.numDivisionID
        JOIN 
			dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
            AND AD1.numRecordID = div1.numDivisionID
            AND tintAddressOf = 2
            AND tintAddressType = 2
            AND bitIsPrimary = 1
        WHERE 
			D1.numDomainID = @numDomainID
    ELSE IF @tintShipToAddressType = 2 
        SELECT 
			@numShipToCountry = numShipCountry
        FROM    
			OpportunityAddress
        WHERE   
			numOppID = @numOppID
    ELSE IF @tintShipToAddressType = 3 
        SELECT 
			@numShipToCountry = numShipCountry
        FROM    
			OpportunityAddress
        WHERE   
			numOppID = @numOppID
				
	SELECT 
		numProId
		,vcProName
		,bitNeverExpires
		,bitRequireCouponCode
		,txtCouponCode
		,tintUsageLimit
		,intCouponCodeUsed
		,bitFreeShiping
		,monFreeShippingOrderAmount
		,numFreeShippingCountry
		,bitFixShipping1
		,monFixShipping1OrderAmount
		,monFixShipping1Charge
		,bitFixShipping2
		,monFixShipping2OrderAmount
		,monFixShipping2Charge
		,tintOfferTriggerValueType
		,fltOfferTriggerValue
		,tintOfferBasedOn
		,fltDiscountValue
		,tintDiscountType
		,tintDiscoutBaseOn
		,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
		,(CASE tintOfferBasedOn
			WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
			WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
		END) As vcPromotionItems
		,(CASE tintDiscoutBaseOn
			WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
			WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																													WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																													WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																													ELSE 0
																												END) FOR XML PATH('')), 1, 1, ''))
		END) As vcItems
		,CONCAT('Buy '
				,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
				,CASE tintOfferBasedOn
						WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
						WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
					END
				,' & get '
				, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
				, CASE 
					WHEN tintDiscoutBaseOn = 1 THEN 
						CONCAT
						(
							CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
							,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
							,'.'
						)
					WHEN tintDiscoutBaseOn = 2 THEN 
						CONCAT
						(
							CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
							,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
							,'.'
						)
					WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
					ELSE '' 
				END
			) AS vcPromotionDescription
			,CONCAT
			(
				(CASE WHEN ISNULL(bitFixShipping1,0)=1 THEN CONCAT('$',monFixShipping1Charge,' shipping on order of $',monFixShipping1OrderAmount,' or more. ') ELSE '' END)
				,(CASE WHEN ISNULL(bitFixShipping2,0)=1  THEN CONCAT('$',monFixShipping2Charge,' shipping on order of $',monFixShipping2OrderAmount,' or more. ') ELSE '' END)
				,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShipToCountry) THEN CONCAT('Free shipping on order of $',monFreeShippingOrderAmount,'. ') ELSE '' END) 
			) AS vcShippingDescription,
			CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END AS tintPriority
	FROM
		PromotionOffer PO
		LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
	WHERE
		PO.numDomainId=@numDomainID 
		AND numProId IN (SELECT Distinct numPromotionID FROM OpportunityItems WHERE numOppId=@numOppID)     

END