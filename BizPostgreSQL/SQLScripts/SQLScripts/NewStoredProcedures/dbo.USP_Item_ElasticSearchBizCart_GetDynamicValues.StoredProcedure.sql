GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_Item_ElasticSearchBizCart_GetDynamicValues')
DROP PROCEDURE USP_Item_ElasticSearchBizCart_GetDynamicValues
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_Item_ElasticSearchBizCart_GetDynamicValues]
(
	@numSiteId AS NUMERIC(18,0)
	,@numDivisionID AS NUMERIC(18,0) = 0
	,@IsAvaiablityTokenCheck AS BIT = 0
	,@numWareHouseID AS NUMERIC(9) = 0
	--,@vcItemCodes AS NVARCHAR(MAX) = ''
	,@UTV_Item_UOMConversion UTV_Item_UOMConversion READONLY
)
AS
BEGIN
	DECLARE @numDomainID AS NUMERIC(18,0) = 0
	DECLARE @numDefaultRelationship AS NUMERIC(18,0) = 0
	DECLARE @numDefaultProfile AS NUMERIC(18,0) = 0
	DECLARE @bitShowInStock AS BIT = 0
	Declare @isPromotionExist as bit = 0 
	DECLARE @bitAutoSelectWarehouse AS BIT

	SELECT  
		@numDomainID = numDomainID
    FROM 
		[Sites]
    WHERE 
		numSiteID = @numSiteID

	IF ISNULL(@numDivisionID,0) > 0
		BEGIN
		SELECT 
			@numDefaultRelationship=ISNULL(numCompanyType,0)
			,@numDefaultProfile=ISNULL(vcProfile,0)
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID 
			AND DivisionMaster.numDomainID=@numDomainID
	END

	SET @isPromotionExist = IIF(EXISTS(select 1 from PromotionOffer where numDomainId=@numDomainID),1,0)


	--KEEP THIS BELOW ABOVE IF CONDITION
	SELECT 
		@numDefaultRelationship=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultRelationship ELSE numRelationshipId END)
		,@numDefaultProfile=(CASE WHEN ISNULL(@numDivisionID,0) > 0 THEN @numDefaultProfile ELSE numProfileId END)
		,@bitShowInStock = bitShowInStock
		,@bitAutoSelectWarehouse = ISNULL(bitAutoSelectWarehouse,0)
	FROM 
		dbo.eCommerceDTL 
	WHERE 
		numSiteID = @numSiteId

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
		IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
		BEGIN
			IF @bitAutoSelectWarehouse = 1
			BEGIN
				SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
			END
			ELSE
			BEGIN
				SELECT 
					@numWareHouseID = ISNULL(numDefaultWareHouseID,0)
				FROM 
					[eCommerceDTL]
				WHERE 
					[numDomainID] = @numDomainID	

				SET @vcWarehouseIDs = @numWareHouseID
			END
		END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END


	SELECT 
		numItemCode
		,vcItemName
		,numTotalPromotions
		,vcPromoDesc
		,bitRequireCouponCode
		,numItemClassification
		,UOM
		,UOMPurchase
		,monListPrice
		,numVendorID
		,charItemType
		,(CASE  
			WHEN @IsAvaiablityTokenCheck = 0 THEN 0 
				ELSE (CASE WHEN charItemType <> 'S' AND charItemType <> 'N' AND ISNULL(bitKitParent,0)=0
						THEN (CASE 
								WHEN bitAllowBackOrder = 1 THEN 1
								WHEN (ISNULL(numOnHand,0)<=0) THEN 0
								ELSE 1
							END)
						ELSE 1
				END)
		END) AS bitInStock
		,(CASE  
			WHEN @IsAvaiablityTokenCheck = 0 THEN '' 
				ELSE (CASE WHEN charItemType <> 'S' AND charItemType <> 'N'
					THEN (CASE WHEN @bitShowInStock = 1
								THEN (CASE 
										WHEN bitAllowBackOrder = 1 AND numDomainID NOT IN (172) AND ISNULL(numOnHand,0)<=0 AND ISNULL(numVendorID,0) > 0 AND dbo.GetVendorPreferredMethodLeadTime(numVendorID,0) > 0 THEN CONCAT('<font class="vendorLeadTime" style="color:green">Usually ships in ',dbo.GetVendorPreferredMethodLeadTime(numVendorID,0),' days</font>')
										WHEN bitAllowBackOrder = 1 AND ISNULL(numOnHand,0)<=0 THEN (CASE WHEN numDomainID  IN (172) THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)
										WHEN (ISNULL(numOnHand,0)<=0) THEN (CASE WHEN numDomainID  IN (172) THEN '<font style="color:red">ON HOLD</font>' ELSE '<font style="color:red">Out Of Stock</font>' END)
										ELSE (CASE WHEN numDomainID IN (172) THEN '<font style="color:green">AVAILABLE</font>' ELSE '<font style="color:green">In Stock</font>' END)  
									END)
								ELSE ''
							END)
					ELSE ''
				END)
			END) AS InStock ,
			CONVERT(FLOAT,0) AS monFirstPriceLevelPrice,
			CONVERT(FLOAT,0) AS fltFirstPriceLevelDiscount,
			W1.[monWListPrice],
			ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID]
			
			INTO #TempTbl
	FROM 
		(SELECT 
			I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,I.numItemClassification
			,CONVERT(INT,0) numTotalPromotions
			,CONVERT(NVARCHAR(100),'') vcPromoDesc
			,CONVERT(BIT,0) bitRequireCouponCode
			,SUM(ISNULL(WHI.numOnHand,0)) AS numOnHand
			,TEMPItem.UOM
			,TEMPItem.UOMPurchase
			,I.monListPrice
		FROM Item I 
		--INNER JOIN
		--(
		--	SELECT [OutParam] FROM dbo.[SplitString](@vcItemCodes,',')
		--) TEMPItem
		--ON
		--	I.numItemCode = TEMPItem.OutParam
		INNER JOIN @UTV_Item_UOMConversion AS TEMPItem ON (I.numItemCode = TEMPItem.numItemCode)
		LEFT JOIN WareHouseItems WHI ON (WHI.numItemID = I.numItemCode)
		--OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(I.numDomainID,@numDivisionID,numItemCode,numItemClassification,@numDefaultRelationship,@numDefaultProfile,1)) TablePromotion
		WHERE WHI.numdomainID = @numDomainID 
		GROUP BY 
			I.numItemCode
			,I.vcItemName
			,I.charItemType
			,I.bitKitParent
			,I.bitAllowBackOrder
			,I.numDomainID
			,I.numVendorID
			,I.numItemClassification
			--,ISNULL(TablePromotion.numTotalPromotions,0)
			--,ISNULL(TablePromotion.vcPromoDesc,'''') 
			--,ISNULL(TablePromotion.bitRequireCouponCode,0) 
			,TEMPItem.UOM
			,TEMPItem.UOMPurchase
			,monListPrice
		)T
		OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
						WHERE  W.numItemID = T.numItemCode 
						AND W.numDomainID = @numDomainID
						AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](@vcWarehouseIDs,','))) AS W1
		


		IF(@isPromotionExist =1)
		BEGIN 
			UPDATE t1 
			SET t1.numTotalPromotions = ISNULL(TablePromotion.numTotalPromotions,0),
			t1.vcPromoDesc = TablePromotion.vcPromoDesc,
			t1.bitRequireCouponCode = ISNULL(TablePromotion.bitRequireCouponCode,0)
			FROM #TempTbl t1
			OUTER APPLY (SELECT * FROM dbo.fn_GetItemPromotions(@numDomainID,@numDivisionID,numItemCode,t1.numItemClassification,@numDefaultRelationship,@numDefaultProfile,1)) TablePromotion
		END

		IF(@numDomainID = 172)
		BEGIN 
			
			UPDATE t1 
			SET t1.monFirstPriceLevelPrice = ISNULL(CASE 
						WHEN tintRuleType = 1 AND tintDiscountType = 1
						THEN ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - (ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) * ( decDiscount / 100 ) )
						WHEN tintRuleType = 1 AND tintDiscountType = 2
						THEN ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount
						WHEN tintRuleType = 2 AND tintDiscountType = 1
						THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * ( decDiscount / 100 ) )
						WHEN tintRuleType = 2 AND tintDiscountType = 2
						THEN ISNULL(Vendor.monCost,0) + decDiscount
						WHEN tintRuleType = 3
						THEN decDiscount
					END, 0),
			t1.fltFirstPriceLevelDiscount = ISNULL(CASE 
					WHEN tintRuleType = 1 AND tintDiscountType = 1 
					THEN decDiscount 
					WHEN tintRuleType = 1 AND tintDiscountType = 2
					THEN (decDiscount * 100) / (ISNULL((CASE WHEN t1.[charItemType] = 'P' THEN ( UOM * ISNULL(t1.[monWListPrice],0)) ELSE (UOM * monListPrice) END),0) - decDiscount)
					WHEN tintRuleType = 2 AND tintDiscountType = 1
					THEN decDiscount
					WHEN tintRuleType = 2 AND tintDiscountType = 2
					THEN (decDiscount * 100) / (ISNULL(Vendor.monCost,0) + decDiscount)
					WHEN tintRuleType = 3
					THEN 0
				END,'')
			FROM #TempTbl t1
			--INNER JOIN Item I ON (I.numItemCode = t1.numItemCode)
			OUTER APPLY (SELECT TOP 1 * FROM PricingTable WHERE numItemCode=t1.numItemCode AND ISNULL(numCurrencyID,0) = 0) AS PT 
			LEFT JOIN Vendor ON Vendor.numVendorID =t1.numVendorID AND Vendor.numItemCode=t1.numItemCode
			 			  
			--CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),t1.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
			--CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase
		END



		SELECT * From #TempTbl

		
	IF OBJECT_ID('tempdb..#TempTbl') IS NOT NULL
	DROP TABLE #TempTbl
END


