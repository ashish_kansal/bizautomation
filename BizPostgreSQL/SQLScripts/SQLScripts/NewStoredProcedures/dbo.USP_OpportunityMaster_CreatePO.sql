SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CreatePO')
DROP PROCEDURE dbo.USP_OpportunityMaster_CreatePO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CreatePO]
(
	@numOppID NUMERIC(18,0) OUTPUT,
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numUnitHour FLOAT,
	@numWarehouseItemID NUMERIC(18,0),
	@bitFromWorkOrder BIT,
	@tintOppStatus TINYINT,
	@numOrderStatus AS NUMERIC(18,0) = 0
)
AS 
BEGIN
    DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    
	BEGIN TRY
	BEGIN TRAN
		DECLARE @tintMinOrderQty AS INTEGER
		DECLARE @numUnitPrice DECIMAL(20,5)
		DECLARE @numContactID NUMERIC(18,0)
	    DECLARE @numDivisionID NUMERIC(18,0)
		DECLARE @numVendorID NUMERIC(18,0)
		DECLARE @fltExchangeRate float    
		DECLARE @numCurrencyID NUMERIC(18,0)
		DECLARE @vcPOppName VARCHAR(1000) = ''                         
		DECLARE @hDocItem int                                                                                                                            
		DECLARE @TotalAmount as DECIMAL(20,5)     
		DECLARE @numAssignedTo NUMERIC(18,0)       
		DECLARE @dtEstimatedCloseDate DATETIME = GETUTCDATE()        
		DECLARE @bitBillingTerms BIT
		DECLARE @intBillingDays INTEGER
		DECLARE @bitInterestType BIT
		DECLARE @fltInterest FLOAT
		DECLARE @intShippingCompany NUMERIC(18,0)
		DECLARE @numDefaultShippingServiceID AS NUMERIC(18,0)                                                                 

		SELECT @numDivisionID = numVendorID FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT 
				@bitBillingTerms=(CASE WHEN ISNULL(tintBillingTerms,0) = 1 THEN 1 ELSE 0 END) 
				,@intBillingDays = ISNULL(numBillingDays,0)
				,@bitInterestType= (CASE WHEN ISNULL(tintInterestType,0) = 1 THEN 1 ELSE 0 END)
				,@fltInterest=ISNULL(fltInterest ,0)
				,@numAssignedTo=ISNULL(numAssignedTo,0)
				,@intShippingCompany=ISNULL(intShippingCompany,0)
				,@numDefaultShippingServiceID=ISNULL(numDefaultShippingServiceID,0)
			FROM 
				DivisionMaster
			WHERE 
				numDomainID=@numDomainID AND numDivisionID=@numDivisionID

			SELECT @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID AND bitPrimaryContact = 1

			IF @numContactID IS NULL
			BEGIN
				SELECT TOP 1 @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID
			END

			SELECT @tintMinOrderQty=ISNULL(intMinQty,0) FROm Vendor WHERE numVendorID = @numDivisionID AND numItemCode = @numItemCode AND numDomainID=@numDomainID
			SET @numUnitHour = @numUnitHour + @tintMinOrderQty
			SET @numUnitHour = IIF(@numUnitHour = 0, 1, @numUnitHour)

			SELECT @numUnitPrice = dbo.fn_FindVendorCost(@numItemCOde,@numDivisionID,@numDomainID,@numUnitHour)

			DECLARE @strItems VARCHAR(MAX) = '<?xml version="1.0" encoding="iso-8859-1" ?><NewDataSet>'

			SELECT 
				@strItems = CONCAT(@strItems
									,'<Item><Op_Flag>1</Op_Flag>'
									,'<numoppitemtCode>',1,'</numoppitemtCode>'
									,'<numItemCode>',numItemCode,'</numItemCode>'
									,'<numUnitHour>',@numUnitHour,'</numUnitHour>'
									,'<monPrice>',@numUnitPrice,'</monPrice>'
									,'<monTotAmount>',(@numUnitHour * @numUnitPrice),'</monTotAmount>'
									,'<vcItemDesc>',txtItemDesc,'</vcItemDesc>'
									,'<numWarehouseItmsID>',@numWarehouseItemID,'</numWarehouseItmsID>'
									,'<ItemType>',charItemType,'</ItemType>'
									,'<DropShip>',0,'</DropShip>'
									,'<bitDiscountType>',1,'</bitDiscountType>'
									,'<fltDiscount>',0,'</fltDiscount>'
									,'<monTotAmtBefDiscount>',(@numUnitHour * @numUnitPrice),'</monTotAmtBefDiscount>'
									,'<vcItemName>',vcItemName,'</vcItemName>'
									,'<numUOM>',ISNULL(numBaseUnit,0),'</numUOM>'
									,'<bitWorkOrder>0</bitWorkOrder>'
									,'<numVendorWareHouse>0</numVendorWareHouse>'
									,'<numShipmentMethod>0</numShipmentMethod>'
									,'<numSOVendorId>0</numSOVendorId>'
									,'<numProjectID>0</numProjectID>'
									,'<numProjectStageID>0</numProjectStageID>'
									,'<numToWarehouseItemID>0</numToWarehouseItemID>'
									,'<Attributes></Attributes>'
									,'<AttributeIDs></AttributeIDs>'
									,'<vcSKU>',vcSKU,'</vcSKU>'
									,'<bitItemPriceApprovalRequired>0</bitItemPriceApprovalRequired>'
									,'<numPromotionID>0</numPromotionID>'
									,'<bitPromotionTriggered>0</bitPromotionTriggered>'
									,'<vcPromotionDetail></vcPromotionDetail>'
									,'<numSortOrder>',1,'</numSortOrder>')
									
			FROM 
				Item 
			WHERE 
				numItemCode=@numItemCode
					
			SET @strItems = CONCAT(@strItems,'</Item>')

			SET @strItems = CONCAT(@strItems,'</NewDataSet>')

			DECLARE @numNewOppID NUMERIC(18,0) = 0

			EXEC USP_OppManage 
				@numNewOppID OUTPUT
				,@numContactID
				,@numDivisionID
				,0
				,''
				,''
				,0
				,@numUserCntID
				,0
				,@numAssignedTo
				,@numDomainID
				,@strItems
				,''
				,@dtEstimatedCloseDate
				,0
				,0
				,2
				,0
				,0
				,0
				,@numCurrencyID
				,@tintOppStatus
				,@tintOppStatus
				,NULL
				,0
				,0
				,0
				,0
				,1
				,0
				,0
				,@bitBillingTerms
				,@intBillingDays
				,@bitInterestType
				,@fltInterest
				,0
				,0
				,NULL
				,''
				,NULL
				,NULL
				,NULL
				,NULL
				,0
				,0
				,0
				,@intShippingCompany
				,0
				,NULL
				,0
				,0
				,0
				,0
				,0
				,0
				,0
				,@numDefaultShippingServiceID
				,0
				,''

			EXEC USP_OpportunityMaster_CT @numDomainID,@numUserCntID,@numNewOppID
		END
	COMMIT TRAN 
	END TRY
	BEGIN CATCH
		IF ( @@TRANCOUNT > 0 ) 
		BEGIN
			ROLLBACK TRAN
		END

		SET @numNewOppID = 0

		-- DO NOT RAISE ERROR
	END CATCH;
END
