GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageReportListMaster')
DROP PROCEDURE USP_ManageReportListMaster
GO
CREATE PROCEDURE [dbo].[USP_ManageReportListMaster]                                        
	@numReportID numeric(18, 0) OUTPUT,
    @vcReportName varchar(200),
    @vcReportDescription varchar(500),
    @numDomainID numeric(18, 0),
    @numReportModuleID numeric(18, 0),
    @numReportModuleGroupID numeric(18, 0),
    @tintReportType tinyint,
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @textQuery text,
    @numSortColumnmReportFieldGroupID NUMERIC(18,0),
    @numSortColumnFieldID numeric(18, 0),
    @vcSortColumnDirection varchar(10),
    @bitSortColumnCustom BIT,
    @vcFilterLogic varchar(200),
    @numDateReportFieldGroupID NUMERIC(18,0),
    @numDateFieldID NUMERIC(18),
    @bitDateFieldColumnCustom BIT,
    @vcDateFieldValue VARCHAR(50),
    @dtFromDate DATETIME,
    @dtToDate DATETIME,
    @tintRecordFilter TINYINT,
    @strText TEXT = '',
    @intNoRows INT,
    @vcKPIMeasureFieldID VARCHAR(50),
    @bitDuplicate BIT=0,
    @bitHideSummaryDetail BIT=0
as                 

IF @bitDuplicate=1
BEGIN
	INSERT INTO [dbo].[ReportListMaster] ([vcReportName], [vcReportDescription], [numDomainID], [numReportModuleID], [numReportModuleGroupID], [tintReportType], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [textQuery], [numSortColumnmReportFieldGroupID], [numSortColumnFieldID], [vcSortColumnDirection], [bitSortColumnCustom], [vcFilterLogic], [numDateReportFieldGroupID], [numDateFieldID], [bitDateFieldColumnCustom], [vcDateFieldValue], [dtFromDate], [dtToDate], [tintRecordFilter], [vcKPIMeasureFieldID], [intNoRows],bitHideSummaryDetail)
	SELECT 'Duplicate - ' + vcReportName, vcReportDescription, numDomainID, numReportModuleID, numReportModuleGroupID, tintReportType, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), bitActive, textQuery, numSortColumnmReportFieldGroupID, numSortColumnFieldID, vcSortColumnDirection, bitSortColumnCustom, vcFilterLogic, numDateReportFieldGroupID, numDateFieldID, bitDateFieldColumnCustom, vcDateFieldValue, dtFromDate, dtToDate, tintRecordFilter, vcKPIMeasureFieldID, intNoRows,bitHideSummaryDetail
	FROM ReportListMaster WHERE numReportID=@numReportID AND numDomainID=@numDomainID
	
	DECLARE @numOLDReportID AS NUMERIC(18,0);SET @numOLDReportID=@numReportID
	
	SET @numReportID=@@IDENTITY
	
		INSERT INTO [dbo].[ReportFieldsList] ([numReportID], [numFieldID], [bitCustom], [numReportFieldGroupID])
			SELECT @numReportID, numFieldID, bitCustom, numReportFieldGroupID
				FROM ReportFieldsList WHERE numReportID=@numOLDReportID
	
	
		INSERT INTO dbo.ReportFilterList (numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID) 
			SELECT @numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID
			  FROM ReportFilterList WHERE numReportID=@numOLDReportID


		INSERT INTO dbo.ReportSummaryGroupList (numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID) 
			SELECT @numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID
			  FROM ReportSummaryGroupList WHERE numReportID=@numOLDReportID


		INSERT INTO dbo.ReportMatrixAggregateList (numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID) 	
				SELECT @numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID
			  FROM ReportMatrixAggregateList WHERE numReportID=@numOLDReportID


		INSERT INTO dbo.ReportKPIThresold (numReportID,vcThresoldType,decValue1,decValue2,decGoalValue) 	
				SELECT @numReportID,vcThresoldType,decValue1,decValue2,decGoalValue
			  FROM ReportKPIThresold WHERE numReportID=@numOLDReportID

END

ELSE
BEGIN
 IF @numSortColumnmReportFieldGroupID = 0 SET @numSortColumnmReportFieldGroupID = NULL  
 IF @numSortColumnFieldID = 0 SET @numSortColumnFieldID = NULL  
 IF @numDateReportFieldGroupID = 0 SET @numDateReportFieldGroupID = NULL  
 IF @numDateFieldID = 0 SET @numDateFieldID = NULL  
 IF @vcKPIMeasureFieldID = '' SET @vcKPIMeasureFieldID = NULL  
 
IF @numReportID=0
BEGIN
	INSERT INTO [dbo].[ReportListMaster] ([vcReportName], [vcReportDescription], [numDomainID], [numReportModuleID], 
		[numReportModuleGroupID], [tintReportType], [numCreatedBy], [dtCreatedDate],
		[bitActive], [textQuery],numSortColumnmReportFieldGroupID, [numSortColumnFieldID], [vcSortColumnDirection], [bitSortColumnCustom], [vcFilterLogic],
		numDateReportFieldGroupID,numDateFieldID,bitDateFieldColumnCustom,vcDateFieldValue,dtFromDate,dtToDate,tintRecordFilter,intNoRows,vcKPIMeasureFieldID,bitHideSummaryDetail)
	SELECT @vcReportName, @vcReportDescription, @numDomainID, @numReportModuleID, 
	    @numReportModuleGroupID, @tintReportType, @numUserCntID, GETUTCDATE(),
	    @bitActive, @textQuery,@numSortColumnmReportFieldGroupID, @numSortColumnFieldID, @vcSortColumnDirection, @bitSortColumnCustom, @vcFilterLogic,
	    @numDateReportFieldGroupID,@numDateFieldID,@bitDateFieldColumnCustom,@vcDateFieldValue,@dtFromDate,@dtToDate,@tintRecordFilter,@intNoRows,@vcKPIMeasureFieldID,@bitHideSummaryDetail
	    
	SET @numReportID=@@IDENTITY
END
ELSE
BEGIN
	UPDATE [dbo].[ReportListMaster]
	SET  vcReportName=@vcReportName,vcReportDescription=@vcReportDescription,[tintReportType] = @tintReportType, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), 
		 [textQuery] = @textQuery,numSortColumnmReportFieldGroupID=@numSortColumnmReportFieldGroupID, [numSortColumnFieldID] = @numSortColumnFieldID, 
		 [vcSortColumnDirection] = @vcSortColumnDirection, [bitSortColumnCustom] = @bitSortColumnCustom, [vcFilterLogic] = @vcFilterLogic,
		 numDateReportFieldGroupID=@numDateReportFieldGroupID,numDateFieldID=@numDateFieldID,bitDateFieldColumnCustom=@bitDateFieldColumnCustom,vcDateFieldValue=@vcDateFieldValue,
		 dtFromDate=@dtFromDate,dtToDate=@dtToDate,tintRecordFilter=@tintRecordFilter,intNoRows=@intNoRows,vcKPIMeasureFieldID=@vcKPIMeasureFieldID,bitHideSummaryDetail=@bitHideSummaryDetail
	WHERE  [numDomainID] = @numDomainID AND [numReportID] = @numReportID
	
	IF DATALENGTH(@strText)>2
	BEGIN
		DECLARE @hDocItem INT                                                                                                                                                                
	
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
        --Delete records into ReportFieldsList and Insert
        DELETE FROM ReportFieldsList WHERE  [numReportID] = @numReportID

		INSERT INTO dbo.ReportFieldsList (numReportID,numFieldID,bitCustom,numReportFieldGroupID) 
			SELECT @numReportID,numFieldID,bitCustom,numReportFieldGroupID
			  FROM OPENXML (@hDocItem, '/NewDataSet/ReportFieldsList',2) WITH ( numFieldID NUMERIC, bitCustom BIT ,numReportFieldGroupID NUMERIC )
	
	
		--Delete records into ReportFilterList and Insert
		DELETE FROM ReportFilterList WHERE  [numReportID] = @numReportID

		INSERT INTO dbo.ReportFilterList (numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID) 
			SELECT @numReportID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterText,numReportFieldGroupID
			  FROM OPENXML (@hDocItem, '/NewDataSet/ReportFilterList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(50),vcFilterText VARCHAR(2000) ,numReportFieldGroupID NUMERIC )


		--Delete records into ReportSummaryGroupList and Insert
		DELETE FROM ReportSummaryGroupList WHERE  [numReportID] = @numReportID

		INSERT INTO dbo.ReportSummaryGroupList (numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID) 
			SELECT @numReportID,numFieldID,bitCustom,tintBreakType,numReportFieldGroupID
			  FROM OPENXML (@hDocItem, '/NewDataSet/ReportSummaryGroupList',2) WITH ( numFieldID NUMERIC, bitCustom BIT ,tintBreakType TINYINT,numReportFieldGroupID NUMERIC )


		--Delete records into ReportMatrixAggregateList and Insert
		DELETE FROM ReportMatrixAggregateList WHERE  [numReportID] = @numReportID
		
		INSERT INTO dbo.ReportMatrixAggregateList (numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID) 	
				SELECT @numReportID,numFieldID,bitCustom,vcAggType,numReportFieldGroupID
			  FROM OPENXML (@hDocItem, '/NewDataSet/ReportMatrixAggregateList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcAggType VARCHAR(50) ,numReportFieldGroupID NUMERIC )


		--Delete records into ReportKPIThresold and Insert
		DELETE FROM ReportKPIThresold WHERE  [numReportID] = @numReportID
		
		INSERT INTO dbo.ReportKPIThresold (numReportID,vcThresoldType,decValue1,decValue2,decGoalValue) 	
				SELECT @numReportID,vcThresoldType,decValue1,decValue2,decGoalValue
			  FROM OPENXML (@hDocItem, '/NewDataSet/ReportKPIThresold',2) WITH ( vcThresoldType VARCHAR(50), decValue1 DECIMAL(18,2), decValue2 DECIMAL(18,2), decGoalValue DECIMAL(18,2))

		EXEC sp_xml_removedocument @hDocItem
	END	
END

DECLARE @numGroupId NUMERIC(18,0)
SELECT @numGroupId=ISNULL(numGroupID,0) FROM UserMaster WHERE numUserDetailId=@numUserCntID

IF NOT EXISTS (SELECT numReportID  FROM ReportDashboardAllowedReports WHERE numDomainID=@numDomainID AND numReportID=@numReportID AND numGrpID=@numGroupId)
BEGIN
	INSERT INTO ReportDashboardAllowedReports
	(
		numDomainId
		,numGrpId
		,numReportId
		,tintReportCategory
	)
	VALUES
	(
		@numDomainId
		,@numGroupId
		,@numReportID
		,0
	)
END

END
	


	
