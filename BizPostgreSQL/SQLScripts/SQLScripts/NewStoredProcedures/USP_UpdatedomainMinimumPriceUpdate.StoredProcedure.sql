USE [Production.2014]
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainMinimumPriceUpdate]    Script Date: 15-01-2018 04:30:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME ='USP_UpdatedomainMinimumPriceUpdate')
DROP PROCEDURE USP_UpdatedomainMinimumPriceUpdate
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainMinimumPriceUpdate]
	@numApprovalRuleID AS NUMERIC(18) = 0, 
	@numListItemID AS NUMERIC(18) = 0,                                    
	@numDomainID AS NUMERIC(18) = 0,
	@numAbovePercent AS NUMERIC(18,2),
	@numBelowPercent AS NUMERIC(18,2),
	@numBelowPriceField AS NUMERIC(18,0),
	@bitCostApproval AS BIT,
	@bitListPriceApproval AS BIT,
	@bitMarginPriceViolated AS BIT,
	@vcUnitPriceApprover AS VARCHAR,
	@byteMode INT 
AS                                      
BEGIN TRY
	BEGIN TRANSACTION   
	                                 
	IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID=@numDomainID AND ISNULL(numListItemID,0)=ISNULL(@numListItemID,0))
	BEGIN
		UPDATE 
			ApprovalProcessItemsClassification
		SET 
			numAbovePercent = @numAbovePercent,
			numBelowPercent = @numBelowPercent,
			numBelowPriceField = @numBelowPriceField,
			bitCostApproval = @bitCostApproval,
			bitListPriceApproval = @bitListPriceApproval,
			bitMarginPriceViolated = @bitMarginPriceViolated
		WHERE 
			numDomainId = @numDomainID
			AND ISNULL(numListItemID,0)=ISNULL(@numListItemID,0)
	END
	ELSE
	BEGIN
		INSERT INTO ApprovalProcessItemsClassification
		(
			numListItemID
			,numDomainID
			,numAbovePercent
			,numBelowPercent
			,numBelowPriceField
			,bitCostApproval
			,bitListPriceApproval
			,bitMarginPriceViolated
		)
		VALUES
		(
			@numListItemID
			,@numDomainID
			,@numAbovePercent
			,@numBelowPercent
			,@numBelowPriceField
			,@bitCostApproval
			,@bitListPriceApproval
			,@bitMarginPriceViolated
		)
	END

	COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH