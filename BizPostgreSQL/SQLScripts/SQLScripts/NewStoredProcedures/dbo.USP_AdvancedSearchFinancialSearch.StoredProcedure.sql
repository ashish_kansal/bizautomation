SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearchFinancialSearch')
DROP PROCEDURE USP_AdvancedSearchFinancialSearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearchFinancialSearch] 
@WhereCondition as varchar(4000)='',                              
@ViewID as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                              
@numGroupID as numeric(9)=0,                              
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnName as varchar(20)='', 
@SortCharacter as char(1),                        
@SortColumnName as varchar(20)='',  
@LookTable as varchar(20)='' ,        
@GetAll as bit                            
as                               
DECLARE  @tintOrder  AS TINYINT
DECLARE  @vcFormFieldName  AS VARCHAR(50)
DECLARE  @vcListItemType  AS VARCHAR(3)
DECLARE  @vcListItemType1  AS VARCHAR(3)
DECLARE  @vcAssociatedControlType VARCHAR(10)
DECLARE  @numListID  AS NUMERIC(9)
DECLARE  @vcDbColumnName VARCHAR(20)
declare @ColumnSearch as varchar(10)
DECLARE  @vcLookBackTableName VARCHAR(50)
DECLARE  @WCondition VARCHAR(1000)

if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter   

CREATE TABLE #temptable (
  id           INT   IDENTITY   PRIMARY KEY,
  numJournalId NUMERIC(18),numTransactionId NUMERIC(18))

declare @strSql as varchar(8000)                                                              
 set  @strSql='Select GJD.numJournalId,GJD.numTransactionId                    
  FROM General_Journal_Header GJH
  JOIN General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId
   LEFT OUTER JOIN (DivisionMaster DM                                               
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId) ON GJD.numCustomerId = DM.numDivisionID    
  LEFT OUTER JOIN CheckHeader CH ON (GJH.numCheckHeaderID = CH.numCheckHeaderID or (GJH.numBillPaymentID=CH.numReferenceID and CH.tintReferenceType=8))' 

  if (@ColumnName<>'' and  @ColumnSearch<>'')                        
  begin                          
	select @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
    if @vcListItemType='LI'                               
    begin                      
		set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
    end                              
  end       
           
                      
  if (@SortColumnName<>'')                        
  begin                          
	select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
    if @vcListItemType1='LI'                   
    begin     
		set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
	END
  end    
                              
  set @strSql=@strSql+' where GJH.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    else  
		set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
END
                          
   if (@SortColumnName<>'')                        
  begin                           
    if @vcListItemType1='LI'                               
		begin                                
		  set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
    else  
       BEGIN     
			set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END  
  END
  ELSE 
	  set @strSql= @strSql +' order by  GJH.datCreatedDate asc '
	  
	  
    PRINT @strSql

	INSERT INTO #temptable (numJournalId,numTransactionId)
	EXEC( @strSql)
	
	SET @strSql = ''


set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select numDomainId,
                numAccountId,
                TransactionType,
                CompanyName,
                dbo.[FormatedDateFromDate](datEntry_Date,numDomainID) Date,
                varDescription,
                ISNULL(BizPayment,'''') + '' '' + ISNULL(CheqNo,'''') AS Narration,
                BizDocID,
                TranRef,
                TranDesc,
                CONVERT(VARCHAR(20),ISNULL(numDebitAmt,0)) numDebitAmt,
                CONVERT(VARCHAR(20),ISNULL(numCreditAmt,0)) numCreditAmt,
                vcAccountName,
                '''' balance,
                VG.numJournal_Id AS JournalId,
				numCheckHeaderID AS CheckId,
				numCashCreditCardId AS CashCreditCardId,
				VG.numTransactionId,
				numOppId,
				numOppBizDocsId,
				numDepositId,
				numCategoryHDRID,
				tintTEType,
				numCategory,
				dtFromDate,
				numUserCntID,bitReconcile,bitCleared,numBillID,numBillPaymentID,dbo.fn_GetListItemName(numClassID) as vcClass,
				VG.numLandedCostOppId '                              
                                                                           
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
	set @strSql=@strSql+'FROM VIEW_GENERALLEDGER VG                       
	   '+@WhereCondition+' inner join #tempTable T on T.numJournalId=VG.numJournal_Id and T.numTransactionId=VG.numTransactionId                                                               
	  WHERE VG.numDomainID  = '+convert(varchar(15),@numDomainID) + ' AND ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) + ' order by T.ID' 
end                           
 else                              
     set @strSql=@strSql+'FROM VIEW_GENERALLEDGER VG                      
  '+@WhereCondition+' join #tempTable T on T.numJournalId=VG.numJournal_Id and T.numTransactionId=VG.numTransactionId 
  WHERE VG.numDomainID  = '+convert(varchar(15),@numDomainID)            
      
print @strSql                                                      
exec(@strSql) 
drop table #tempTable

