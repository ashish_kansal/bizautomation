GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_General_Journal_Details_CheckIfSalesClearingEntryExists')
DROP PROCEDURE dbo.USP_General_Journal_Details_CheckIfSalesClearingEntryExists
GO
CREATE PROCEDURE [dbo].[USP_General_Journal_Details_CheckIfSalesClearingEntryExists]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
AS 
BEGIN
	IF EXISTS 
	(
		SELECT 
			numTransactionId
		FROM
			General_Journal_Header GJH
		INNER JOIN
			General_Journal_Details GJD
		ON
			GJH.numJournal_Id=GJD.numJournalId
		INNER JOIN
			AccountingCharges AC
		ON
			GJD.numChartAcntId = AC.numAccountID
			AND AC.numDomainID=@numDomainID
			AND AC.numChargeTypeId=25
		WHERE
			GJH.numOppId=@numOppID
			AND GJH.numDomainId=@numDomainID
	)
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END