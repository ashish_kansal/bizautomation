GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkFlow_TimeBasdAction')
DROP PROCEDURE USP_WorkFlow_TimeBasdAction
GO
-- =============================================    
-- Author:  <Author,,Sachin Sadhu>    
-- Create date: <Create Date,,27thMarch2014>    
-- Description: <Description,,To Implement Time based action> 
--EXEC USP_WorkFlow_TimeBasdAction 95587,'OpportunityMaster','BINTCREATEDDATE',2,2,1,@result OUTPUT       
-- =============================================       
Create PROCEDURE [dbo].[USP_WorkFlow_TimeBasdAction]     
 -- Add the parameters for the stored procedure here         
 @numRecordID numeric(18, 0)=0,      
 @TableName VARCHAR(50),    
 @columnname   VARCHAR(50),    
 @intDays INT,    
 @intActionOn INT,    
 @numDomainID numeric(18, 0)=0    ,
 @intCustom int = 0,
 @numFormID numeric(18,0)=0,
 @Fld_ID numeric(18,0)=0
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 --SET NOCOUNT ON;    
    -- Insert statements for procedure here    
 DECLARE @MatchingDate DATETIME    
 DECLARE @CurrentDate DATETIME    
 SET @CurrentDate=DATEADD(DD,0, DATEDIFF(DD,0, GETDATE()))    
    
 DECLARE @SQL NVARCHAR(4000)    


 IF (@intCustom = 0)
 BEGIN
		 IF	(@TableName='OpportunityMaster')
			 BEGIN
				 SET @SQL='SELECT @MatchingDate=['+replace(@columnname,' ','')+'] FROM '+@TableName+' WHERE numOppID='+ CAST(@numRecordID as varchar(50))
			 END
		 ELSE IF(@TableName='StagePercentageDetails')    
			 BEGIN
				SET @SQL='SELECT @MatchingDate=['+replace(@columnname,' ','')+'] FROM '+@TableName+' WHERE numStageDetailsId='+ CAST(@numRecordID as varchar(50))
			 END
		 ELSE IF(@TableName='DivisionMaster')    
			 BEGIN
				SET @SQL='SELECT @MatchingDate=['+replace(@columnname,' ','')+'] FROM '+@TableName+' WHERE numDivisionID='+ CAST(@numRecordID as varchar(50))
			 END
		 ELSE IF(@TableName='ProjectsMaster')    
			 BEGIN
				SET @SQL='SELECT @MatchingDate=['+replace(@columnname,' ','')+'] FROM '+@TableName+' WHERE numProId='+ CAST(@numRecordID as varchar(50))
			 END
		 ELSE IF(@TableName='AdditionalContactsInformation')    
			 BEGIN
				SET @SQL='SELECT @MatchingDate=['+replace(@columnname,' ','')+'] FROM '+@TableName+' WHERE numContactId='+ CAST(@numRecordID as varchar(50))
			 END
	     ELSE IF(@TableName='Cases')    
			 BEGIN
				SET @SQL='SELECT @MatchingDate=['+replace(@columnname,' ','')+'] FROM '+@TableName+' WHERE numCaseId='+ CAST(@numRecordID as varchar(50))
			 END
		 ELSE IF(@TableName='Communication')    
			 BEGIN
				SET @SQL='SELECT @MatchingDate=['+replace(@columnname,' ','')+'] FROM '+@TableName+' WHERE numCommId='+ CAST(@numRecordID as varchar(50))
			 END

		 ELSE
			BEGIN
			--Nothing
				Print 'Nothing'
			END
 END

 ELSE
 BEGIN
		--Print 'Must be a custom record'

		IF	(@numFormID= 68) --organization
			 BEGIN				
				 SET @SQL='SELECT @MatchingDate=dbo.FormatedDateFromDate(Fld_Value,'+CAST(@numDomainID as varchar(50)) +') FROM CFW_FLD_Values  WHERE  Fld_ID='+ CAST(@Fld_ID as varchar(50)) +' and RecId='+ CAST(@numRecordID as varchar(50))
			 END
	   ELSE IF(@numFormID= 69)    --Contacts
			 BEGIN			
				SET @SQL='SELECT @MatchingDate=dbo.FormatedDateFromDate(Fld_Value,'+CAST(@numDomainID as varchar(50)) +') FROM CFW_FLD_Values_Cont  WHERE  Fld_ID='+ CAST(@Fld_ID as varchar(50)) +' and RecId='+ CAST(@numRecordID as varchar(50))
			
			 END
		 ELSE IF(@numFormID= 70)    --Opportunities and Orders
			 BEGIN			
				SET @SQL='SELECT @MatchingDate=dbo.FormatedDateFromDate(Fld_Value,'+CAST(@numDomainID as varchar(50)) +') FROM CFW_Fld_Values_Opp  WHERE  Fld_ID='+ CAST(@Fld_ID as varchar(50)) +' and RecId='+ CAST(@numRecordID as varchar(50))
			
			 END
		 ELSE IF(@numFormID= 73) --Projects    
			 BEGIN
				 SET @SQL='SELECT @MatchingDate=dbo.FormatedDateFromDate(Fld_Value,'+CAST(@numDomainID as varchar(50)) +') FROM CFW_FLD_Values_Pro  WHERE  Fld_ID='+ CAST(@Fld_ID as varchar(50)) +' and RecId='+ CAST(@numRecordID as varchar(50))
			 END
		 ELSE IF(@numFormID= 72) --Cases    
			 BEGIN
				 SET @SQL='SELECT @MatchingDate=dbo.FormatedDateFromDate(Fld_Value,'+CAST(@numDomainID as varchar(50)) +') FROM CFW_FLD_Values_Case  WHERE  Fld_ID='+ CAST(@Fld_ID as varchar(50)) +' and RecId='+ CAST(@numRecordID as varchar(50))
			 END
		 ELSE --General custom Fields table :CFW_FLD_Values
			BEGIN
				--Nothing
				SET @SQL='SELECT @MatchingDate=dbo.FormatedDateFromDate(Fld_Value,'+CAST(@numDomainID as varchar(50)) +') FROM CFW_FLD_Values  WHERE  Fld_ID='+ CAST(@Fld_ID as varchar(50)) +' and RecId='+ CAST(@numRecordID as varchar(50))
			END
 END

 


 PRINT @SQL 
    
EXEC SP_EXECUTESQL @SQL, N'@MatchingDate DATETIME OUTPUT',@MatchingDate OUTPUT    
DECLARE @result VARCHAR(50)    
SET @result=0    
IF (@intActionOn=1)    
   BEGIN    
	   IF (@CurrentDate=(DATEADD(DD,0, DATEDIFF(DD,@intDays, @MatchingDate))))    
		   BEGIN    
				SET @result=1    
		   END    
	   ELSE    
		   BEGIN    
				SET @result=0    
		   END           
   END    
ELSE    
   BEGIN    
	   IF (@CurrentDate=(DATEADD(DD,@intDays, DATEDIFF(DD,0, @MatchingDate))))    
		   BEGIN    
				SET @result=1    
		   END    
	  ELSE    
		   BEGIN    
				SET @result=0    
		   END    
   END    
       
 SELECT @result AS TimeMatchStatus      
    
    
    
END 
