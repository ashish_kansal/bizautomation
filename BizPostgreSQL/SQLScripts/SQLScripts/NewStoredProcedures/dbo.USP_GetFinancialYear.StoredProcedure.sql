GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetFinancialYear')
DROP PROCEDURE USP_GetFinancialYear
GO
CREATE PROCEDURE [dbo].[USP_GetFinancialYear]
    @numFinYearId NUMERIC(9),
    @numDomainID NUMERIC(9),
    @tintMode TINYINT = 0
AS 
    BEGIN

        IF @tintMode = 0 
            BEGIN
                SELECT  [numFinYearId],
                        [numDomainId],
                        dbo.[FormatedDateFromDate]([dtPeriodFrom], numDomainID) AS dtPeriodFrom,
                        dbo.[FormatedDateFromDate]([dtPeriodTo], numDomainID) AS dtPeriodTo,
                        [vcFinYearDesc],
                        [bitCloseStatus],
                        [bitAuditStatus],
                        
                        CONVERT(CHAR(1), [bitCurrentYear]) bitCurrentYear,
                        CASE bitCurrentYear
                          WHEN 1 THEN 'Yes'
                          ELSE 'No'
                        END AS CurrentYear,
                        CASE [bitCloseStatus] WHEN 1 THEN 'Yes' ELSE 'No' END  vcCloseStatus,
                        CASE [bitAuditStatus] WHEN 1 THEN 'Yes' ELSE 'No' END  vcAuditStatus
                FROM    FinancialYear
                WHERE   ( [numFinYearId] = @numFinYearId
                          OR @numFinYearId = 0
                        )
                        AND numDomainID = @numDomainID

            END
        ELSE IF @tintMode = 1 
                BEGIN
                    SELECT  [numFinYearId],
                            [vcFinYearDesc]
                    FROM    FinancialYear
                    WHERE   [bitCurrentYear] = 0
                            AND numDomainID = @numDomainID
                            AND dtPeriodFrom > (SELECT dtPeriodTo FROM dbo.FinancialYear WHERE numDomainId=@numDomainID AND bitCurrentYear=1)
                END
        ELSE IF @tintMode = 2 
                BEGIN
                    SELECT TOP 1
                            [numFinYearId],
                            [vcFinYearDesc],
                            dbo.[FormatedDateFromDate]([dtPeriodFrom],
                                                       numDomainID) AS dtPeriodFrom,
                            dbo.[FormatedDateFromDate]([dtPeriodTo],
                                                       numDomainID) AS dtPeriodTo
                    FROM    FinancialYear
                    WHERE   [bitCurrentYear] = 1
                            AND numDomainID = @numDomainID
                END
          ELSE IF @tintMode = 3 -- Get next FY start date
                BEGIN
                DECLARE @NextFinYearFromDate AS VARCHAR(20)
                
                    SELECT @NextFinYearFromDate = dbo.[FormatedDateFromDate]( MAX(dtPeriodTo)+1, @numDomainID)
                    FROM    FinancialYear
                    WHERE   numDomainID = @numDomainID
                    
                    SELECT @NextFinYearFromDate AS NextFinYearFromDate
                END
			ELSE IF @tintMode = 4 -- Get to date of last closed FY
                BEGIN
					DECLARE @MaxFinYearToDate AS VARCHAR(20)
                
                    SELECT @MaxFinYearToDate = ISNULL(dbo.[FormatedDateFromDate]( MAX(dtPeriodTo), @numDomainID),'')
                    FROM    FinancialYear
                    WHERE   
                    (bitAuditStatus=1 OR bitCloseStatus=1) AND
                     numDomainID = @numDomainID
                    
                    SELECT  @MaxFinYearToDate AS  FYClosingDate
                END
    END 
    