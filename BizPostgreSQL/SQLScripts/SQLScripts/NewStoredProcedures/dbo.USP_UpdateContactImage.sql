GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateContactImage' ) 
    DROP PROCEDURE USP_UpdateContactImage
GO

CREATE PROCEDURE USP_UpdateContactImage  
(  
 @numDomainID NUMERIC,  
 @numContactID NUMERIC,  
 @vcImageName VARCHAR(200)   
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  
  
	UPDATE 
		AdditionalContactsInformation 
	SET 
		vcImageName=@vcImageName 
	WHERE 
		numDomainID=@numDomainID 
		AND numContactId=@numContactID   
   
END  


