-- USP_GetDataToExport 72
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDataToExport')
DROP PROCEDURE USP_GetDataToExport
GO
CREATE PROCEDURE USP_GetDataToExport
@numDomainID NUMERIC(9)
--@tintMode TINYINT=1
AS 
BEGIN
	
--	IF @tintMode=1
--	BEGIN
		DECLARE @vcTables VARCHAR(100)
		DECLARE @dtLastDate VARCHAR(100)
		SELECT  @vcTables= [vcExportTables],@dtLastDate=dbo.[FormatedDateTimeFromDate]([dtLastExportedOn],[numDomainID]) FROM [ExportDataSettings] WHERE [numDomainID]=@numDomainID
		SELECT *,@dtLastDate AS dtLastExportedOn FROM [dbo].[SplitIDs](@vcTables,',')
--	END
--	IF @tintMode=2
--	BEGIN
--		DECLARE @vcExportTables VARCHAR(100)
--		SELECT @vcExportTables=[vcExportTables] FROM [ExportDataSettings] WHERE [numDomainID]=@numDomainID
--	
--	--Organization Which includes all contacts
--		IF EXISTS( SELECT * FROM dbo.[SplitIDs](@vcExportTables,',') WHERE Id = 1)
--		BEGIN
--			EXEC USP_ExportDataBackUp @numDomainID,1
--		END
--		
--		--Opportunity and orders
--		IF EXISTS( SELECT * FROM dbo.[SplitIDs](@vcExportTables,',') WHERE Id = 2)
--		BEGIN
--			EXEC USP_ExportDataBackUp @numDomainID,2
--		END
--		
--/*For demo server uncomment following and comment out above*/		
----		SELECT TOP 10 [vcDivisionName],[vcBillCity],[vcBillCountry],[vcBillPostCode],[vcBilState] FROM [DivisionMaster] WHERE [numDomainID]=72 
--
--	END 
END