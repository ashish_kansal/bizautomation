GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingBox')
DROP PROCEDURE USP_GetShippingBox
GO
CREATE PROCEDURE USP_GetShippingBox
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
        SELECT DISTINCT
                [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                CASE WHEN [fltTotalWeight] = 0 THEN SUM([fltWeight]) ELSE [fltTotalWeight] END AS fltTotalWeight,
                [fltHeight],
                [fltWidth],
                [fltLength],
				dbo.FormatedDateFromDate(dtDeliveryDate,numDomainID) dtDeliveryDate,
				ISNULL(monShippingRate,0) [monShippingRate],
				vcShippingLabelImage,
				ISNULL(vcTrackingNumber,'') AS vcTrackingNumber,
				tintServiceType,--Your Packaging
				tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,
				numPackageTypeID,
				ISNULL(vcPackageName,'') AS [vcPackageName],
				numServiceTypeID,
				CAST(ROUND(SUM(fltTotalRegularWeight), 2) AS DECIMAL(9,2)) AS [fltTotalWeightForShipping],
				fltDimensionalWeight AS [fltTotalDimensionalWeight],
				ISNULL(fltDimensionalWeight,0) AS [fltDimensionalWeight],
				ISNULL(numShipCompany,0) AS [numShipCompany],
				numOppID
--				,intBoxQty
				--ShippingReportItemId
        FROM    View_ShippingBox
        WHERE   [numShippingReportId] = @ShippingReportId
        GROUP BY [numBoxID],
                [vcBoxName],
                [numShippingReportId],
                [fltHeight],
                [fltWidth],
                [fltLength],
                fltTotalWeight,dtDeliveryDate,numOppID,View_ShippingBox.numOppBizDocID,
				monShippingRate,vcShippingLabelImage,
				vcTrackingNumber,numDomainID,
				tintServiceType,tintPayorType,
				vcPayorAccountNo,
				vcPayorZip,
				numPayorCountry,numPackageTypeID,vcPackageName,numServiceTypeID,fltDimensionalWeight,numShipCompany--,intBoxQty
				                
        SELECT  [numBoxID],
                [vcBoxName],
                [ShippingReportItemId],
                [numShippingReportId],
                fltTotalWeightItem [fltTotalWeight],
                fltHeightItem [fltHeight],
                fltWidthItem [fltWidth],
                fltLengthItem [fltLength],
                [numOppBizDocItemID],
                [numItemCode],
                [numoppitemtCode],
                ISNULL([vcItemName],'') vcItemName,
                [vcModelID],
                CASE WHEN LEN (ISNULL(vcItemDesc,''))> 100 THEN  CONVERT(VARCHAR(100),[vcItemDesc]) + '..'
                ELSE ISNULL(vcItemDesc,'') END vcItemDesc,
                intBoxQty,
                numServiceTypeID,
                vcUnitName,monUnitPrice,
				numOppID
        FROM    [View_ShippingBox]
        WHERE   [numShippingReportId] = @ShippingReportId       
                
                
    END
