SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EDIQueueLog_GetByOrder')
DROP PROCEDURE dbo.USP_EDIQueueLog_GetByOrder
GO
CREATE PROCEDURE [dbo].[USP_EDIQueueLog_GetByOrder]
(
	@numDomainID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
	@ClientTimeZoneOffset INT
)
AS 
BEGIN
	SELECT
		vcLog
		,bitSuccess
		,dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtDate),numDomainID) AS vcDate
	FROM
		EDIQueueLog
	WHERE
		numDomainID=@numDomainID
		AND numOppID=@numOppID
	ORDER BY
		dtDate DESC
END
GO