GO
/****** Object:  StoredProcedure [dbo].[Get_GetBroadcastingLink]    Script Date: 30/07/2012 17:47:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO


IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBroadcastingLink' ) 
    DROP PROCEDURE USP_GetBroadcastingLink
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GetBroadcastingLink 
  @numBroadcastId AS numeric(18),
  @numLinkId as numeric(18) = 0
AS 

IF EXISTS(select * from BroadcastingLink where numLinkId = @numLinkId)
   BEGIN
          select * from BroadcastingLink where numLinkId = @numLinkId
   END
ELSE
   BEGIN
          select * from BroadcastingLink where numBroadcastId = @numBroadcastId
   END
