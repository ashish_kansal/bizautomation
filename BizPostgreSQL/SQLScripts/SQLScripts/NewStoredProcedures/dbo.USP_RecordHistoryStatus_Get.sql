SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecordHistoryStatus_Get')
DROP PROCEDURE dbo.USP_RecordHistoryStatus_Get
GO
CREATE PROCEDURE [dbo].[USP_RecordHistoryStatus_Get]

AS
BEGIN
	SELECT * FROM RecordHistoryStatus WHERE Id=1
END