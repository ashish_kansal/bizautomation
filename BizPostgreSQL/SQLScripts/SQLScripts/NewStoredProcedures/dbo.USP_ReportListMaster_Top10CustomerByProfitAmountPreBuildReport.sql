SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@tintControlField INT
	,@numRecordCount INT
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP (@numRecordCount)
		numDivisionID
		,(CASE tintCRMType WHEN 1 THEN CONCAT('~/prospects/frmProspects.aspx?DivID=',numDivisionID) WHEN 2 THEN CONCAT('~/Account/frmAccounts.aspx?DivID=',numDivisionID) ELSE CONCAT('~/Leads/frmLeads.aspx?DivID=',numDivisionID) END) AS URL
		,vcCompanyName
		,SUM(Profit) Profit
	FROM
	(
		SELECT
			DM.numDivisionID
			,tintCRMType
			,CI.vcCompanyName
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
			,ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		numDivisionID
		,tintCRMType
		,vcCompanyName

	HAVING
		(CASE
		   WHEN @tintControlField=2 THEN (SUM(Profit)/SUM(monTotAmount)) * 100
		   WHEN @tintControlField=1 THEN SUM(Profit) 
		END ) > 0
		--(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		CASE
		   WHEN @tintControlField=2 THEN (SUM(Profit)/ SUM(monTotAmount)) * 100 
		   WHEN @tintControlField=1 THEN SUM(Profit) 
		END DESC
		--(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
	--HAVING
	--	SUM(Profit) > 0
	--ORDER BY
	--	SUM(Profit) DESC
END
GO