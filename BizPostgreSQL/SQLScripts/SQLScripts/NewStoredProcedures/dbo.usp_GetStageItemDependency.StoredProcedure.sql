
GO
/****** Object:  StoredProcedure [dbo].[usp_GetStageItemDependency]    Script Date: 09/21/2010 11:36:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetStageItemDependency')
DROP PROCEDURE usp_GetStageItemDependency
GO
CREATE PROCEDURE [dbo].[usp_GetStageItemDependency]
    @numDomainid NUMERIC = 0,
    @numProjectID NUMERIC = 0 ,
    @numStageDetailID NUMERIC = 0 ,
    @tintMode TINYINT=0,
    @tintModeType TINYINT=-1    
AS 

IF @tintMode = 0 
BEGIN   
select SP.vcStageName,SD.numStageDetailId,SD.numStageDependancyID,SD.numDependantOnID from
StageDependency SD join StagePercentageDetails SP on SD.numDependantOnID=SP.numStageDetailsId
where SD.numStageDetailID=@numStageDetailID and SP.numdomainid = @numDomainid and
@numProjectID=(case @tintModeType when 0 then SP.numOppID 
				when 1 then SP.numProjectID end)

END
ELSE IF @tintMode = 1
BEGIN   
SELECT [numStageDetailsId],[vcStageName]
		FROM [StagePercentageDetails] where @numProjectID=(case @tintModeType when 0 then numOppID 
				when 1 then numProjectID end) and numDomainid=@numDomainid and [numStageDetailsId]
		not in (select numDependantOnID from StageDependency where numStageDetailID=@numStageDetailID)
		and numStagePercentageId=(select numStagePercentageId from StagePercentageDetails where numStageDetailsId=@numStageDetailID)
		and numStageDetailsId!=@numStageDetailID
END
ELSE IF @tintMode = 2
BEGIN   
select ST.numStageDetailID,ST.numDependantOnID,
case when ST.numStageDetailID=@numStageDetailID then SP2.vcStageName else SP1.vcStageName end vcStageName,
case when ST.numStageDetailID=@numStageDetailID then 'UP' else 'DOWN' end
 DependType  from StageDependency ST left join StagePercentageDetails SP1 on 
ST.numStageDetailID=SP1.numStageDetailsId  left join StagePercentageDetails SP2 on 
ST.numDependantOnID=SP2.numStageDetailsId
where ST.numStageDetailID=@numStageDetailID or ST.numDependantOnID=@numStageDetailID
END
ELSE IF @tintMode = 3
BEGIN   
SELECT [numStageDetailsId],[vcStageName]
		FROM [StagePercentageDetails] where slp_id=@numProjectID and ISNULL(numProjectid,0)=0 and ISNULL(numOppid,0)=0 and numDomainid=@numDomainid	and
		[numStageDetailsId] not in (select numDependantOnID from StageDependency where numStageDetailID=@numStageDetailID)
		and numStagePercentageId=(select numStagePercentageId from StagePercentageDetails where numStageDetailsId=@numStageDetailID)
		and numStageDetailsId!=@numStageDetailID
END
ELSE IF @tintMode = 4 
BEGIN   
select SP.vcStageName,SD.numStageDetailId,SD.numStageDependancyID,SD.numDependantOnID from
StageDependency SD join StagePercentageDetails SP on SD.numDependantOnID=SP.numStageDetailsId
where SD.numStageDetailID=@numStageDetailID and SP.numdomainid = @numDomainid and
slp_id=@numProjectID
END
--delete from StageDependency where numStageDetailID=611 and numDependantOnID=611
