
GO
/****** Object:  StoredProcedure [dbo].[Usp_DeleteSalesReturn]    Script Date: 01/22/2009 01:36:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_DeleteSalesReturn' ) 
    DROP PROCEDURE Usp_DeleteSalesReturn
GO
CREATE PROCEDURE [dbo].[Usp_DeleteSalesReturn]
    @numReturnHeaderID NUMERIC(9),
    @numDomainId NUMERIC(9)
AS 
    BEGIN
	
        IF EXISTS ( SELECT  numReturnHeaderID
                    FROM    ReturnHeader
                    WHERE   numReturnHeaderID = @numReturnHeaderID
                            AND ISNULL(tintReceiveType, 0) <> 0 ) 
            BEGIN
                RAISERROR ( 'CreditMemo_Refund', 16, 1 ) ;
                RETURN ;
            END

		IF EXISTS (SELECT numDepositID FROM dbo.DepositMaster WHERE ISNULL(numReturnHeaderID,0) = @numReturnHeaderID
						 AND numDomainId=@numDomainId
						 And ISNULL(monRefundAmount,0)>0) 
            BEGIN
                RAISERROR ( 'Deposit_Refund_Payment', 16, 1 ) ;
                RETURN ;
            END
            
        IF EXISTS (SELECT numBillPaymentID FROM dbo.BillPaymentHeader WHERE ISNULL(numReturnHeaderID,0) = @numReturnHeaderID
					AND numDomainId=@numDomainId
					And ISNULL(monRefundAmount,0)>0) 
            BEGIN
                RAISERROR ( 'BillPayment_Refund_Payment', 16, 1 ) ;
                RETURN ;
            END

		DELETE FROM OppWarehouseSerializedItem WHERE numReturnHeaderID=@numReturnHeaderID
		
        DELETE  FROM ReturnItems
        WHERE   numReturnHeaderID = @numReturnHeaderID
        
        /*Start : If Refund against Deposit or Bill Payment*/
        DECLARE @numBillPaymentIDRef AS NUMERIC(18);SET @numBillPaymentIDRef=0
        DECLARE @numDepositIDRef AS NUMERIC(18);SET @numDepositIDRef=0
        DECLARE @monAmount AS DECIMAL(20,5);SET @monAmount=0
        
        SELECT @numBillPaymentIDRef=ISNULL(numBillPaymentIDRef,0),
			   @numDepositIDRef=ISNULL(numDepositIDRef,0),
			   @monAmount=ISNULL(monAmount,0)
			 FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID
        
        IF @numBillPaymentIDRef > 0
        BEGIN
			UPDATE dbo.BillPaymentHeader SET monRefundAmount = ISNULL(monRefundAmount,0) - @monAmount
				WHERE numDomainID=@numDomainId AND numBillPaymentID=@numBillPaymentIDRef
        END

		IF @numDepositIDRef > 0
        BEGIN
			UPDATE dbo.DepositMaster SET monRefundAmount = ISNULL(monRefundAmount,0) - @monAmount
				WHERE numDomainID=@numDomainId AND numDepositId=@numDepositIDRef
        END

        DELETE  FROM [dbo].[ReturnItems]
        WHERE   [ReturnItems].[numReturnHeaderID] = @numReturnHeaderID 
        DELETE  FROM ReturnHeader
        WHERE   numReturnHeaderID = @numReturnHeaderID
                AND [numDomainID] = @numDomainId 

		DELETE FROM OpportunityMasterTaxItems WHERE numReturnHeaderID = @numReturnHeaderID
		DELETE FROM OpportunityItemsTaxItems WHERE numReturnHeaderID = @numReturnHeaderID
  
    END

