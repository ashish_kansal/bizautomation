
GO
/****** Object:  StoredProcedure [dbo].[USP_GetProjectStageHierarchy]    Script Date: 09/21/2010 11:36:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectChildStage')
DROP PROCEDURE USP_GetProjectChildStage
GO
CREATE PROCEDURE [dbo].[USP_GetProjectChildStage]         
@numDomainID as numeric(9)=0,
@numStageDetailsId as numeric(9)=0,
@strType as varchar(10)
as       

Declare @numProjectID numeric,@numOppID numeric,@numParentStageID numeric,@numStagePercentageId numeric

select @numProjectID=numProjectID,@numOppID=numOppID,@numParentStageID=numParentStageID,@numStagePercentageId=numStagePercentageId from StagePercentageDetails
where numStageDetailsId=@numStageDetailsId and numDomainId=@numDomainId

If  @strType='child'
BEGIN
	select numStageDetailsId,numStagePercentageId,vcStageName,tintPercentage from StagePercentageDetails
	where numParentStageID=@numStageDetailsId and numDomainId=@numDomainId and numProjectID=@numProjectID and numOppID=@numOppID
END
Else If  @strType='Parent'
BEGIN
	select numStageDetailsId,numStagePercentageId,vcStageName,tintPercentage from StagePercentageDetails
	where numParentStageID=@numParentStageID and numStagePercentageId=@numStagePercentageId and numDomainId=@numDomainId and numProjectID=@numProjectID and numOppID=@numOppID
END

