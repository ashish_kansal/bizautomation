SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Communication_DismissReminder')
DROP PROCEDURE USP_Communication_DismissReminder
GO
CREATE PROCEDURE [dbo].[USP_Communication_DismissReminder]  
	@numCommID NUMERIC(18,0)
AS  
BEGIN  
	UPDATE Communication SET tintRemStatus=0,intRemainderMins=0 WHERE numCommId=@numCommID
END
GO
