SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Get_ActivityFormConfiguration')
DROP PROCEDURE USP_Get_ActivityFormConfiguration
GO
CREATE PROCEDURE [dbo].[USP_Get_ActivityFormConfiguration]  
	@numDomainID AS NUMERIC(18,0),
	@numGroupId AS NUMERIC(18,0),
	@numUserContId AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	SELECT 
		numActivityAuthenticationId, 
		numDomainId, 
		numGroupId, 
		bitFollowupStatus, 
		bitPriority, 
		bitActivity, 
		bitCustomField, 
		bitFollowupAnytime,
		bitAttendee
	FROM
		ActivityFormAuthentication
	WHERE
		numDomainID=@numDomainID AND
		numGroupId=@numGroupId

	SELECT 
		numDisplayId,
		numDomainId,
		numUserCntId,
		bitTitle,
		bitLocation,
		bitDescription
	FROM
		ActivityDisplayConfiguration
	WHERE
		numDomainID=@numDomainID AND
		numUserCntId=@numUserContId


COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END