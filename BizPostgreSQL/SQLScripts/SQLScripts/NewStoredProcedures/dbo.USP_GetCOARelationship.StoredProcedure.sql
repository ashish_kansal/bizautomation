/****** Object:  StoredProcedure [dbo].[USP_GetCOARelationship]    Script Date: 09/25/2009 16:13:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_GetCOARelationship @numCOARelationshipID=0,@numRelationshipID=46,@numDomainID=72,@numDivisionID=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCOARelationship')
DROP PROCEDURE USP_GetCOARelationship
GO
CREATE PROCEDURE [dbo].[USP_GetCOARelationship]
    @numCOARelationshipID NUMERIC(9) = 0,
    @numRelationshipID NUMERIC(9) = 0,
    @numDomainID NUMERIC(9),
    @numDivisionID NUMERIC(9) = 0
AS 
    SET NOCOUNT ON

    IF @numDivisionID > 0 
        BEGIN
            SELECT  @numRelationshipID = C.numCompanyType
            FROM    [CompanyInfo] C
                    INNER JOIN [DivisionMaster] DM ON DM.[numCompanyID] = C.[numCompanyId]
                    INNER JOIN [ListDetails] LD ON C.[numCompanyType] = LD.[numListItemID]
            WHERE   [numDivisionID] = @numDivisionID
        END

    SELECT  R.[numCOARelationshipID],
			[numRelationshipID],
            [numRelationshipID],
            ISNULL([numARParentAcntTypeID], 0) numARParentAcntTypeID,
            ISNULL([numAPParentAcntTypeID], 0) numAPParentAcntTypeID,
--            [numARAccountId],
--            [numAPAccountId],
			ISNULL(A.[numAccountId],0) numARAccountId,
			ISNULL(B.[numAccountId],0) numAPAccountId,
            R.[numDomainID],
            [dtCreateDate],
            [dtModifiedDate],
            [numCreatedBy],
            [numModifiedBy],
            ISNULL(A.[vcAccountName], '') AS ARAccountName,
            ISNULL(B.[vcAccountName], '') AS APAccountName
    FROM    COARelationships R
            LEFT OUTER JOIN [Chart_Of_Accounts] A ON A.[numAccountId] = R.[numARAccountId]
            LEFT OUTER JOIN [Chart_Of_Accounts] B ON B.[numAccountId] = R.[numAPAccountId]
    WHERE   ( [numCOARelationshipID] = @numCOARelationshipID
              OR @numCOARelationshipID = 0
            )
            AND ( numRelationshipID = @numRelationshipID
                  OR @numRelationshipID = 0
                )
            AND R.numDomainID = @numDomainID