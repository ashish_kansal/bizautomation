
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOfferSites_GetByPromotionID')
DROP PROCEDURE USP_PromotionOfferSites_GetByPromotionID
GO
CREATE PROCEDURE [dbo].[USP_PromotionOfferSites_GetByPromotionID]    
	@numProId AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0)
AS
BEGIN
	SELECT 
		Sites.numSiteID,
		vcSiteName,
		(CASE WHEN ISNULL(PromotionOfferSites.numSiteID,0) > 0 THEN 1 ELSE 0 END) AS bitSelcted
	FROM 
		Sites 
	LEFT JOIN
		PromotionOfferSites
	ON
		Sites.numSiteID = PromotionOfferSites.numSiteID
		AND numPromotionID = @numProId
	WHERE 
		numDOmainID=@numDomainID
END 
GO
