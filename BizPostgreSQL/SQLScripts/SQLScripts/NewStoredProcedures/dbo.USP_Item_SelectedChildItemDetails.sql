GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_SelectedChildItemDetails')
DROP PROCEDURE USP_Item_SelectedChildItemDetails
GO
CREATE PROCEDURE [dbo].[USP_Item_SelectedChildItemDetails]        
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numChildItemCode NUMERIC(18,0),
	@vcKitChilds VARCHAR(MAX),
	@bitEdit BIT 
AS
BEGIN
	DECLARE @bitCalAmtBasedonDepItems BIT
	,@tintKitAssemblyPriceBasedOn TINYINT
	,@monListPrice DECIMAL(20,5)
	,@bitKit BIT
	,@bitHasChildKits BIT

	SELECT
		@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0)
		,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
		,@monListPrice=ISNULL(monListPrice,0)
		,@bitKit=ISNULL(bitKitParent,0)
		,@bitHasChildKits = (CASE 
								WHEN ISNULL(bitKitParent,0) = 1 
								THEN 
									(CASE 
										WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
										THEN 1
										ELSE 0
									END)
								ELSE 0 
							END)
	FROM
		Item
	WHERE
		numItemCode=@numItemCode

	DECLARE @TEMP TABLE
	(
		numItemCode NUMERIC(18,0)
		,vcItemName VARCHAR(300)
		,vcSKU VARCHAR(100)
		,vcType VARCHAR(20)
		,numQty FLOAT
		,monListPrice DECIMAL(20,5)
		,monVendorCost DECIMAL(20,5)
		,monAverageCost DECIMAL(20,5)
		,monPrice DECIMAL(20,5)
		,numSequence INT
	)

	IF ISNULL(@bitKit,0) = 1
	BEGIN
		IF LEN(ISNULL(@vcKitChilds,'')) = 0
		BEGIN
			SELECT 
				@vcKitChilds = COALESCE(@vcKitChilds + ', ', '') + CONCAT('0-',numChildItemID,'-',numQtyItemsReq * dbo.fn_UOMConversion(numUOMID,I.numItemCode,I.numDomainID,I.numPurchaseUnit),'-',ISNULL(sintOrder,1)) 
			FROM 
				ItemDetails
			INNER JOIN
				Item I
			ON
				numChildItemID=numItemCode
			WHERE
				numItemKitID=@numItemCode
		END

		IF LEN(ISNULL(@vcKitChilds,'')) > 0
		BEGIN
			DECLARE @TempExistingItems TABLE
			(
				vcItem VARCHAR(100)
			)

			INSERT INTO @TempExistingItems
			(
				vcItem
			)
			SELECT 
				OutParam 
			FROM 
				SplitString(@vcKitChilds,',')

			INSERT INTO @TEMP
			(
				numItemCode
				,vcItemName
				,vcSKU
				,vcType
				,numQty
				,monListPrice
				,monVendorCost
				,monAverageCost
				,monPrice
				,numSequence
			)
			SELECT
				I.numItemCode
				,ISNULL(I.vcItemName,'') vcItemName
				,ISNULL(I.vcSKU,'') vcSKU
				,(CASE 
					WHEN charItemType='P' 
					THEN CONCAT('Inventory',CASE 
												WHEN ISNULL(I.bitKitParent,0)=1 THEN ' (Kit)' 
												WHEN ISNULL(I.bitAssembly,0)=1 THEN ' (Assembly)' 
												WHEN ISNULL(I.numItemGroup,0) > 0 AND (ISNULL(I.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=I.numItemGroup AND tintType=2)) THEN ' (Matrix)' 
												ELSE '' 
											END) 
					WHEN charItemType='S' THEN 'Service' 
					WHEN charItemType='A' THEN 'Accessory' 
					WHEN charItemType='N' THEN 'Non-Inventory' 
				END) AS vcType
				,T1.numQty AS numQty
				,ISNULL(monListPrice,0) monListPrice
				,ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) monVendorCost
				,ISNULL(monAverageCost,0) monAverageCost
				,(CASE 
					WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=4 
					THEN ISNULL(T1.numQty,0) * ISNULL(monListPrice,0)
					WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=3 
					THEN ISNULL(T1.numQty,0) * ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
					WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=2 
					THEN ISNULL(T1.numQty,0) * ISNULL(monAverageCost,0)
					ELSE ISNULL(T1.numQty,0) * ISNULL(monListPrice,0)
				END)
				,T1.numSequence
			FROM
			(
				SELECT 
					Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 1)) As numKitItemID
					,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2)) As numChildItemID
					,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3)) As numQty
					,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 4)) As numSequence
				FROM  
				(
					SELECT 
						vcItem 
					FROM 
						@TempExistingItems
				) As [x] 
			) T1
			INNER JOIN
				Item I
			ON
				T1.numChildItemID = I.numItemCode
			LEFT JOIN
				Vendor V
			ON
				I.numVendorID = V.numVendorID
				AND I.numItemCode=V.numItemCode
		END

		IF (SELECT COUNT(*) FROM @TEMP WHERE numItemCode=@numChildItemCode) = 0
		BEGIN
			INSERT INTO @TEMP
			(
				numItemCode
				,vcItemName
				,vcSKU
				,vcType
				,numQty
				,monListPrice
				,monVendorCost
				,monAverageCost
				,monPrice
				,numSequence
			)
			SELECT
				I.numItemCode
				,ISNULL(I.vcItemName,'') vcItemName
				,ISNULL(I.vcSKU,'') vcSKU
				,(CASE 
					WHEN charItemType='P' 
					THEN CONCAT('Inventory',CASE 
												WHEN ISNULL(I.bitKitParent,0)=1 THEN ' (Kit)' 
												WHEN ISNULL(I.bitAssembly,0)=1 THEN ' (Assembly)' 
												WHEN ISNULL(I.numItemGroup,0) > 0 AND (ISNULL(I.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=I.numItemGroup AND tintType=2)) THEN ' (Matrix)' 
												ELSE '' 
											END) 
					WHEN charItemType='S' THEN 'Service' 
					WHEN charItemType='A' THEN 'Accessory' 
					WHEN charItemType='N' THEN 'Non-Inventory' 
				END) AS vcType
				,1 AS numQty
				,ISNULL(monListPrice,0) monListPrice
				,ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) monVendorCost
				,ISNULL(monAverageCost,0) monAverageCost
				,(CASE 
						WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=4 
						THEN 1 * ISNULL(monListPrice,0)
						WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=3 
						THEN 1 * ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
						WHEN @bitCalAmtBasedonDepItems=1 AND @tintKitAssemblyPriceBasedOn=2 
						THEN 1 * ISNULL(monAverageCost,0)
						WHEN @bitCalAmtBasedonDepItems=1
						THEN 1 * ISNULL(monListPrice,0)
						ELSE 0
				END)
				,ISNULL((SELECT MAX(numSequence) FROM @TEMP),0) + 1
			FROM
				Item I
			LEFT JOIN
				Vendor V
			ON
				I.numVendorID = V.numVendorID
				AND I.numItemCode=V.numItemCode 
			WHERE
				I.numItemCode=@numChildItemCode
		END		
	END
	
	

	SELECT
		 @bitCalAmtBasedonDepItems AS bitCalAmtBasedonDepItems
		 ,@tintKitAssemblyPriceBasedOn AS tintKitAssemblyPriceBasedOn
		 ,@monListPrice AS monListPrice
		 ,@bitKit AS bitKit
		 ,@bitHasChildKits AS bitHasChildKits
		 ,(CASE WHEN @tintKitAssemblyPriceBasedOn = 4 AND @bitCalAmtBasedonDepItems=1 THEN @monListPrice ELSE 0 END) + ISNULL((SELECT SUM(monPrice) FROM @TEMP),0) AS monPrice

	SELECT * FROM @TEMP
END
GO