SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Category_GetByName')
DROP PROCEDURE USP_Category_GetByName
GO
CREATE PROCEDURE [dbo].[USP_Category_GetByName]
(
	@numDomainID NUMERIC(18,0),
	@vcCategotyName VARCHAR(500)
)
AS 
BEGIN
	SELECT TOP 1 numCategoryID, vcCategoryName FROM Category WHERE numDomainID=@numDomainID AND (LOWER(ISNULL(vcCategoryName,'')) = LOWER(@vcCategotyName) OR LOWER(ISNULL(vcCategoryNameURL,'')) = LOWER(@vcCategotyName))
END