

--Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageItemTaxTypes')
DROP PROCEDURE USP_ManageItemTaxTypes
GO
CREATE PROCEDURE USP_ManageItemTaxTypes
@numItemCode as numeric(9),
@strItemDetails as varchar(1000)
as
BEGIN
	DECLARE @hDoc INT    
	DELETE FROM ItemTax WHERE numItemCode=@numItemCode    
	
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItemDetails    

	INSERT INTO ItemTax 
	(
		numItemCode,
		numTaxItemID,
		numTaxID,
		bitApplicable
	)     
	SELECT 
		@numItemCode,
		X.numTaxItemID,
		X.numTaxID,
		X.bitApplicable  
	FROM
	(
		SELECT 
			*
		FROM 
			OPENXML (@hDoc,'/NewDataSet/Table',2)    
		WITH 
		( 
			numTaxItemID NUMERIC(18,0),    
			numTaxID NUMERIC(18,0),
			bitApplicable bit    
		)
	)X    
    
	EXEC sp_xml_removedocument @hDoc
END
