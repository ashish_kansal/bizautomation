
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMatchingDeposits')
	DROP PROCEDURE USP_GetMatchingDeposits
GO
/****** Added By : Joseph ******/
/****** Gets Matching Deposit Transactions Details From table Deposit Master and Deposit Details 
for the Given Transaction Amount and Period of DateTime******/


CREATE PROCEDURE [dbo].[USP_GetMatchingDeposits]
@monAmount DECIMAL(20,5),
@numDomainId numeric(18, 0),
@dtFromDate DATETIME,
@dtToDate DATETIME


AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN  


SELECT DM.numDepositId,DD.numDepositeDetailID, DM.numChartAcntId, DM.dtDepositdate,DD.vcMemo,ROUND(ABS(DD.monAmountPaid),2) AS monAmountPaid, 
DD.numClassID,DD.numProjectID,DD.numReceivedFrom,DD.numAccountID,C.vcCompanyname FROM dbo.DepositMaster DM 
INNER JOIN dbo.DepositeDetails DD ON DM.numDepositId = DD.numDepositID
INNER JOIN dbo.DivisionMaster D on DD.numReceivedFrom = D.numDivisionID
INNER JOIN dbo.companyinfo C on D.numCompanyID=C.numCompanyID

WHERE DM.numDomainID = @numDomainId AND DD.monAmountPaid = ROUND(ABS(@monAmount),2) OR  DD.monAmountPaid < ROUND(ABS(@monAmount),2)  
AND DM.dtDepositdate BETWEEN @dtFromDate AND @dtToDate



 
END