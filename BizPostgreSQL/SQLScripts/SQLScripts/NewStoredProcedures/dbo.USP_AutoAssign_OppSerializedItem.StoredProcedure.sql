SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AutoAssign_OppSerializedItem')
DROP PROCEDURE USP_AutoAssign_OppSerializedItem
GO
CREATE PROCEDURE [dbo].[USP_AutoAssign_OppSerializedItem]  
    @numDomainID as numeric(9)=0,  
    @numOppID as numeric(9)=0,  
    @numOppBizDocsId as numeric(9)=0
AS  
BEGIN
	IF (SELECT ISNULL(bitAutoSerialNoAssign,0) FROM DOMAIN WHERE @numDomainID=numDomainID) = 1
	BEGIN
		CREATE TABLE #tempSerializedItem 
		(
			ID numeric(18) IDENTITY(1,1) NOT NULL,
			numItemCode numeric(9),
			numOppItemtCode numeric(9),
			numWarehouseItmsID numeric(9),
			bitLotNo bit,
			bitSerialized bit,
			numUnitHour FLOAT
		)

		INSERT INTO 
			#tempSerializedItem 
		SELECT 
			I.numItemCode,
			numOppItemtCode,
			numWarehouseItmsID,
			I.bitLotNo,
			I.bitSerialized,
			oppItems.numUnitHour 
		FROM 
			OpportunityItems oppItems 
		JOIN 
			Item I 
		ON 
			oppItems.numItemCode=I.numItemCode 
		WHERE 
			I.numDomainId=@numDomainID 
			AND oppItems.numOppId=@numOppId 
			AND (ISNULL(I.bitLotNo,0)=1 OR ISNULL(I.bitSerialized,0)=1)

		IF (SELECT COUNT(*) FROM #tempSerializedItem)>0
		BEGIN
			DECLARE  @maxID NUMERIC(9),@minID NUMERIC(9)
			DECLARE @numItemCode NUMERIC(9),@numOppItemtCode NUMERIC(9),@numWarehouseItmsID NUMERIC(9),@bitLotNo BIT,@bitSerialized BIT,@numUnitHour FLOAT

			SELECT @maxID = max(ID),@minID = min(ID) FROM  #tempSerializedItem 

			CREATE TABLE #tempWareHouseItmsDTL 
			(                                                                    
				ID numeric(18) IDENTITY(1,1) NOT NULL,
				vcSerialNo VARCHAR(100),
				numQty FLOAT,
				numWareHouseItmsDTLID [numeric](18, 0)                                             
			)   

			WHILE @minID <= @maxID
			BEGIN
				SELECT 
					@numItemCode=numItemCode,
					@numOppItemtCode=numOppItemtCode,
					@numWarehouseItmsID=numWarehouseItmsID,
					@bitLotNo=bitLotNo,
					@bitSerialized=bitSerialized,
					@numUnitHour=numUnitHour 
				FROM 
					#tempSerializedItem 
				WHERE 
					ID = @minID  

				INSERT INTO #tempWareHouseItmsDTL 
				(
					vcSerialNo,numWarehouseItmsDTLID,numQty
				)  
				SELECT 
					vcSerialNo,numWareHouseItmsDTLID,ISNULL(numQty,0) AS TotalQty
				FROM   
					WareHouseItmsDTL   
				WHERE 
					ISNULL(numQty,0) > 0 
					AND numWareHouseItemID=@numWarehouseItmsID  
					and numWareHouseItmsDTLID NOT IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numWarehouseItmsID=@numWarehouseItmsID AND numOppBizDocsId=@numOppBizDocsId) 
				ORDER BY 
					TotalQty desc

				IF ((SELECT SUM(numQty) FROM #tempWareHouseItmsDTL)>=@numUnitHour)
				BEGIN
					DECLARE  @maxWareID NUMERIC(9),@minWareID NUMERIC(9)
					DECLARE  @numWarehouseItmsDTLID NUMERIC(9),@numQty FLOAT,@numUseQty FLOAT
	
					SELECT @maxWareID = max(ID),@minWareID = min(ID) FROM  #tempWareHouseItmsDTL 

					WHILE (@numUnitHour>0 AND @minWareID <= @maxWareID)
					BEGIN
						SELECT 
							@numWarehouseItmsDTLID = numWarehouseItmsDTLID,
							@numQty = numQty 
						FROM  
							#tempWareHouseItmsDTL  
						WHERE 
							ID=@minWareID

						IF @numUnitHour >= @numQty
						BEGIN
							SET @numUseQty=@numQty
	
							SET @numUnitHour=@numUnitHour-@numQty
						END
						else IF @numUnitHour < @numQty
						BEGIN
							SET @numUseQty=@numUnitHour

							SET @numUnitHour=0
						END
		
						INSERT INTO OppWarehouseSerializedItem
						(
							numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty,numOppBizDocsId
						)                
						SELECT 
							@numWarehouseItmsDTLID,@numOppID,@numOppItemTcode,@numWarehouseItmsID,@numUseQty,@numOppBizDocsId

						SET @minWareID=@minWareID + 1
					END
				END

				TRUNCATE TABLE #tempWareHouseItmsDTL

				SET @minID=@minID + 1
			END

			DROP TABLE #tempWareHouseItmsDTL
		END

		DROP TABLE #tempSerializedItem
	END
END
GO
