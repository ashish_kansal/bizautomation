
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageUOMConversion')
DROP PROCEDURE USP_ManageUOMConversion
GO
CREATE PROCEDURE [dbo].[USP_ManageUOMConversion]
	@numDomainID NUMERIC(9),
	@vcUnitConversion NText,
	@bitEnableItemLevelUOM BIT 
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @iDoc int   

	UPDATE Domain SET bitEnableItemLevelUOM = @bitEnableItemLevelUOM WHERE numDomainId = @numDomainID
 
	DELETE FROM UOMConversion WHERE numDomainID=@numDomainID

	EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @vcUnitConversion

	INSERT INTO UOMConversion 
	(
		[numUOM1],[decConv1],[numUOM2],[decConv2],[numDomainID]
	)             
	SELECT 
		numUOM1,decConv1,numUOM2,decConv2,@numDomainID
    FROM 
		OPENXML(@iDoc, '/NewDataSet/UnitConversion', 2)                        
	WITH                        
	(                
		numUOM1 NUMERIC(9),                  
		decConv1 DECIMAL(18,5),                  
		numUOM2 NUMERIC(9),                
		decConv2 DECIMAL(18,5)               
	)

	EXECUTE sp_xml_removedocument @iDoc 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END 
	



