GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItems_ValidateKitChildsOrderImport')
DROP PROCEDURE dbo.USP_OpportunityItems_ValidateKitChildsOrderImport
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItems_ValidateKitChildsOrderImport]
(
	@numDomainID NUMERIC(18,0),
    @numItemCode NUMERIC(18,0),
    @vcKitChilds VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @vcKitChildItemCodes VARCHAR(MAX) = ''

	DECLARE @TempSelectedItems TABLE
	(
		ID INT IDENTITY(1,1)
		,vcKitItemName VARCHAR(300)
		,numKitItemCode NUMERIC(18,0)
		,vcKitChildItemName VARCHAR(300)
		,numKitChildItemCode NUMERIC(18,0)
	)

	DECLARE @bitKitParent BIT
	SELECT @bitKitParent=ISNULL(bitKitParent,0) FROM Item WHERE numItemCode=@numItemCode

	IF ISNULL(@bitKitParent,0) = 1 AND (SELECT 
											COUNT(*) 
										FROM 
											ItemDetails 
										INNER JOIN 
											Item 
										ON 
											ItemDetails.numChildItemID=Item.numItemCode 
										WHERE 
											ItemDetails.numItemKitID=@numItemCode 
											AND ISNULL(Item.bitKitParent,0)=1) > 0
	BEGIN
		IF LEN(ISNULL(@vcKitChilds,'')) > 0
		BEGIN
			DECLARE @TempKits TABLE
			(
				ID INT IDENTITY(1,1)
				,vcKit VARCHAR(MAX)
			)
			DECLARE @posComma int, @strKeyVal varchar(MAX)

			SET @vcKitChilds=RTRIM(@vcKitChilds)
			SET @posComma=PATINDEX('%#,#%', @vcKitChilds)

			IF @posComma > 0
			BEGIN
				WHILE @posComma > 0
				BEGIN
					SET @strKeyVal=LTRIM(RTRIM(SUBSTRING(@vcKitChilds, 1, @posComma-1)))
	
					INSERT INTO @TempKits (vcKit) VALUES (@strKeyVal)

					SET @vcKitChilds=SUBSTRING(@vcKitChilds, @posComma +3, LEN(@vcKitChilds)-@posComma)

					SET @posComma=PATINDEX('%#,#%',@vcKitChilds)	
				END

				INSERT INTO @TempKits (vcKit) VALUES (@vcKitChilds)
			END
			ELSE
			BEGIN
				INSERT INTO @TempKits (vcKit) VALUES (@vcKitChilds)
			END
			
			DECLARE @i INT = 1
			DECLARE @iCount INT
			DECLARE @vcTempKits VARCHAR(MAX)
			DECLARE @vcTempKitChilds VARCHAR(MAX)
			DECLARE @vcTempKitItemName VARCHAR(MAX)
			DECLARE @vcTempKitChildItemName VARCHAR(MAX)
			DECLARE @iPOSComma INT

			SELECT @iCount=COUNT(*) FROM @TempKits

			WHILE @i <= @iCount
			BEGIN
				SELECT @vcTempKits=ISNULL(vcKit,0) FROM @TempKits WHERE ID=@i

				SET @posComma=PATINDEX('%#:#%', @vcTempKits)

				IF @posComma > 0
				BEGIN
					SET @vcTempKitItemName = ISNULL(LTRIM(RTRIM(SUBSTRING(@vcTempKits, 1, @posComma-1))),'')

					SET @vcTempKitChilds = ISNULL(SUBSTRING(@vcTempKits, @posComma + 3, LEN(@vcTempKits)-@posComma),'')

					SET @iPOSComma=PATINDEX('%#~#%', @vcTempKitChilds)

					IF @iPOSComma > 0
					BEGIN
						WHILE @iPOSComma > 0
						BEGIN
							SET @strKeyVal=LTRIM(RTRIM(SUBSTRING(@vcTempKitChilds, 1, @iPOSComma-1)))
	
							INSERT INTO @TempSelectedItems (vcKitItemName,vcKitChildItemName) VALUES (@vcTempKitItemName,@strKeyVal)

							SET @vcTempKitChilds=SUBSTRING(@vcTempKitChilds, @iPOSComma +3, LEN(@vcTempKitChilds)-@iPOSComma)

							SET @iPOSComma=PATINDEX('%#~#%',@vcTempKitChilds)	
						END

						INSERT INTO @TempSelectedItems (vcKitItemName,vcKitChildItemName) VALUES (@vcTempKitItemName,@vcTempKitChilds)
					END
					ELSE
					BEGIN
						INSERT INTO @TempSelectedItems (vcKitItemName,vcKitChildItemName) VALUES (@vcTempKitItemName,@vcTempKitChilds)
					END

					SET @vcTempKits = SUBSTRING(@vcTempKits, @posComma + 3, LEN(@vcTempKits)-@posComma)
				END
				ELSE
				BEGIN
					RAISERROR('INVALID_INCLISION_DETAIL',16,1)
					RETURN
				END

				SET @i = @i + 1
			END
		END
		ELSE
		BEGIN
			IF (SELECT 
					COUNT(*) 
				FROM 
					ItemDetails 
				INNER JOIN 
					Item 
				ON 
					ItemDetails.numChildItemID=Item.numItemCode 
				WHERE 
					ItemDetails.numItemKitID=@numItemCode 
					AND ISNULL(Item.bitKitParent,0)=1 
					AND ISNULL(Item.bitKitSingleSelect,0) = 1) >0
			BEGIN
				RAISERROR('INVALID_INCLISION_DETAIL',16,1)
				RETURN
			END
		END

		DECLARE @numKitItemCode NUMERIC(18,0)
		DECLARE @numKitChildItemCode NUMERIC(18,0)
		DECLARE @vcKitItemName VARCHAR(MAX)
		DECLARE @vcKitChildItemName VARCHAR(MAX)
		SET @i = 1
		SELECT @iCount=COUNT(*) FROM @TempSelectedItems

		WHILE @i <= @iCount
		BEGIN
			SELECT @vcKitItemName=ISNULL(vcKitItemName,''),@vcKitChildItemName=ISNULL(vcKitChildItemName,'') FROM @TempSelectedItems WHERE ID=@i

			IF (SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitItemName) = 0
			BEGIN
				RAISERROR('KIT_ITEM_NOT_FOUND',16,1)
				RETURN
			END
			ELSE IF (SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitItemName) > 1
			BEGIN
				RAISERROR('MULTIPLE_KIT_ITEM_FOUND',16,1)
				RETURN
			END
			ELSE IF (SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitChildItemName) = 0
			BEGIN
				RAISERROR('KIT_CHILD_ITEM_NOT_FOUND',16,1)
				RETURN
			END
			ELSE IF (SELECT COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitChildItemName) > 1
			BEGIN
				RAISERROR('MULTIPLE_KIT_CHILD_ITEM_FOUND',16,1)
				RETURN
			END
			ELSE
			BEGIN
				SET @numKitItemCode = (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitItemName)
				SET @numKitChildItemCode = (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND vcItemName=@vcKitChildItemName)

				IF NOT EXISTS (SELECT 
									numItemDetailID 
								FROM 
									ItemDetails 
								INNER JOIN 
									Item 
								ON 
									ItemDetails.numChildItemID=Item.numItemCode 
								WHERE 
									numItemKitID=@numItemCode 
									AND numChildItemID=@numKitItemCode 
									AND ISNULL(bitKitParent,0)=1)
				BEGIN
					RAISERROR('INVALID_KIT_ITEM',16,1)
					RETURN
				END

				IF NOT EXISTS (SELECT 
									numItemDetailID 
								FROM 
									ItemDetails 
								WHERE 
									numItemKitID=@numKitItemCode 
									AND numChildItemID=@numKitChildItemCode)
				BEGIN
					RAISERROR('INVALID_KIT_CHILD_ITEM',16,1)
					RETURN
				END

				UPDATE 
					@TempSelectedItems 
				SET
					numKitItemCode = @numKitItemCode
					,numKitChildItemCode = @numKitChildItemCode
				WHERE
					ID=@i
			END

			SET @i = @i + 1
		END

		IF EXISTS (
			SELECT 
				numItemDetailID
			FROM 
				ItemDetails 
			INNER JOIN 
				Item 
			ON 
				ItemDetails.numChildItemID=Item.numItemCode 
			WHERE 
				ItemDetails.numItemKitID=@numItemCode 
				AND ISNULL(Item.bitKitParent,0)=1 
				AND ISNULL(Item.bitKitSingleSelect,0) = 1
				AND numChildItemID NOT IN (SELECT numKitItemCode FROM @TempSelectedItems)
		)
		BEGIN
			RAISERROR('CHILD_KIT_VALUES_NOT_PROVIDED',16,1)
			RETURN
		END

		SET @vcKitChildItemCodes = STUFF((SELECT ',' + CONCAT(numKitItemCode,'-',numKitChildItemCode) FROM @TempSelectedItems T1 FOR XML PATH('')), 1,1,SPACE(0))
	END
	ELSE
	BEGIN
		SET @vcKitChildItemCodes = ''
	END

	SELECT @vcKitChildItemCodes
END
GO