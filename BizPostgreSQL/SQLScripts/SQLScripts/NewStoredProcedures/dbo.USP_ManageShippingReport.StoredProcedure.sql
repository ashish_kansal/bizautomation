/****** Object:  StoredProcedure [dbo].[USP_ManageShippingReport]    Script Date: 05/07/2009 17:31:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM [ShippingReport]
--SELECT * FROM [ShippingReportItems]
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingReport')
DROP PROCEDURE USP_ManageShippingReport
GO
CREATE PROCEDURE [dbo].[USP_ManageShippingReport]
  @numDomainId NUMERIC(9),
    @numShippingReportId NUMERIC(9) = 0 OUTPUT,
    @numOppBizDocId NUMERIC(9),
    @numShippingCompany NUMERIC(9),
    @Value1 VARCHAR(100) = NULL,
    @Value2 VARCHAR(100) = NULL,
    @Value3 VARCHAR(100) = NULL,
    @Value4 VARCHAR(100) = NULL,
    @vcFromState VARCHAR(50) = '',
    @vcFromZip VARCHAR(50) = '',
    @vcFromCountry VARCHAR(50) = '',
    @vcToState VARCHAR(50) = '',
    @vcToZip VARCHAR(50) = '',
    @vcToCountry VARCHAR(50) = '',
    @numUserCntId NUMERIC(9),
    @strItems TEXT = NULL,
    @tintMode TINYINT=0,
	@tintPayorType TINYINT=0,
	@vcPayorAccountNo VARCHAR(20)='',
	@numPayorCountry NUMERIC(9),
	@vcPayorZip VARCHAR(50)='',
	@vcFromCity VARCHAR(50)='',
    @vcFromAddressLine1 VARCHAR(50)='',
    @vcFromAddressLine2 VARCHAR(50)='',
    @vcToCity VARCHAR(50)='',
    @vcToAddressLine1 VARCHAR(50)='',
    @vcToAddressLine2 VARCHAR(50)='',
    @vcFromName			VARCHAR(1000)='',
    @vcFromCompany		VARCHAR(1000)='',
    @vcFromPhone		VARCHAR(100)='',
    @bitFromResidential	BIT=0,
    @vcToName			VARCHAR(1000)='',
    @vcToCompany		VARCHAR(1000)='',
    @vcToPhone			VARCHAR(100)='',
    @bitToResidential	BIT=0,
    @IsCOD					BIT = 0,
	@IsDryIce				BIT = 0,
	@IsHoldSaturday			BIT = 0,
	@IsHomeDelivery			BIT = 0,
	@IsInsideDelevery		BIT = 0,
	@IsInsidePickup			BIT = 0,
	@IsReturnShipment		BIT = 0,
	@IsSaturdayDelivery		BIT = 0,
	@IsSaturdayPickup		BIT = 0,
	@numCODAmount			NUMERIC(18,0) = 0,
	@vcCODType				VARCHAR(50) = 'Any',
	@numTotalInsuredValue	NUMERIC(18,2) = 0,
	@IsAdditionalHandling	BIT,
	@IsLargePackage			BIT,
	@vcDeliveryConfirmation	VARCHAR(1000),
	@vcDescription			VARCHAR(MAX),
	@numOppId				NUMERIC(18,0),
	@numTotalCustomsValue	NUMERIC(18,2),
	@tintSignatureType NUMERIC(18,0)
AS 
BEGIN
    IF @numShippingReportId = 0 
    BEGIN
		SELECT
			@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelevery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@vcCODType = vcCODType
			,@vcDeliveryConfirmation = vcDeliveryConfirmation
			,@vcDescription = vcDescription
			,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
		FROM
			OpportunityMaster
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			DivisionMasterShippingConfiguration
		ON
			DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
		WHERE
			OpportunityMaster.numDomainId = @numDomainId
			AND DivisionMaster.numDomainID=@numDomainId
			AND DivisionMasterShippingConfiguration.numDomainID=@numDomainId
			AND OpportunityMaster.numOppId = @numOppID

            INSERT  INTO [ShippingReport]
            (
               [numOppBizDocId],
               [numShippingCompany],
               vcValue1,
               vcValue2,
               vcValue3,
               vcValue4,
               vcFromState,
               vcFromZip,
               vcFromCountry,
               vcToState,
               vcToZip,
               vcToCountry,
               [dtCreateDate],
               [numCreatedBy],
               numDomainId,
				tintPayorType,
				vcPayorAccountNo,
				numPayorCountry,
				vcPayorZip,
				vcFromCity ,
				vcFromAddressLine1 ,
				vcFromAddressLine2 ,
				vcToCity ,
				vcToAddressLine1 ,
				vcToAddressLine2,
				vcFromName,
				vcFromCompany,
				vcFromPhone,
				bitFromResidential,
				vcToName,
				vcToCompany,
				vcToPhone,
				bitToResidential,
				IsCOD,					
				IsDryIce,				
				IsHoldSaturday,			
				IsHomeDelivery,			
				IsInsideDelevery,		
				IsInsidePickup,			
				IsReturnShipment,		
				IsSaturdayDelivery,		
				IsSaturdayPickup,		
				numCODAmount,			
				vcCODType,				
				numTotalInsuredValue,
				IsAdditionalHandling,
				IsLargePackage,
				vcDeliveryConfirmation,
				vcDescription,
				numOppId,numTotalCustomsValue,tintSignatureType
            )
            VALUES  
			(
               @numOppBizDocId,
               @numShippingCompany,
               @value1,
               @value2,
               @value3,
               @value4,
               @vcFromState,
               @vcFromZip,
               @vcFromCountry,
               @vcToState,
               @vcToZip,
               @vcToCountry,
               GETUTCDATE(),
               @numUserCntId,
               @numDomainId,
				@tintPayorType,
				@vcPayorAccountNo,
				@numPayorCountry,
				@vcPayorZip,
				@vcFromCity ,
				@vcFromAddressLine1 ,
				@vcFromAddressLine2 ,
				@vcToCity ,
				@vcToAddressLine1 ,
				@vcToAddressLine2,
				@vcFromName,
				@vcFromCompany,
				@vcFromPhone,
				@bitFromResidential,
				@vcToName,
				@vcToCompany,
				@vcToPhone,
				@bitToResidential,
				@IsCOD,					
				@IsDryIce,				
				@IsHoldSaturday,			
				@IsHomeDelivery,			
				@IsInsideDelevery,		
				@IsInsidePickup,			
				@IsReturnShipment,		
				@IsSaturdayDelivery,		
				@IsSaturdayPickup,		
				@numCODAmount,			
				@vcCODType,				
				@numTotalInsuredValue,
				@IsAdditionalHandling,
				@IsLargePackage,
				@vcDeliveryConfirmation,
				@vcDescription,
				@numOppId,@numTotalCustomsValue,@tintSignatureType						  
            )
                        
			SET @numShippingReportId = SCOPE_IDENTITY()
				
            DECLARE @numBoxID NUMERIC 
			INSERT INTO [ShippingBox] (
				[vcBoxName],
				[numShippingReportId],
				[dtCreateDate],
				[numCreatedBy]
			) VALUES ( 
				/* vcBoxName - varchar(20) */ 'Box1',
				/* numShippingReportId - numeric(18, 0) */ @numShippingReportId,
				/* dtCreateDate - datetime */ GETUTCDATE(),
				/* numCreatedBy - numeric(18, 0) */ @numUserCntId ) 
		                
			SET @numBoxID = SCOPE_IDENTITY()
--				END
                
                
            IF (@strItems IS NOT NULL)
			BEGIN
				DECLARE @hDocItem INT
				EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
	  
				INSERT  INTO [ShippingReportItems]
						(
							[numShippingReportId],
							[numItemCode],
							[tintServiceType],
							[dtDeliveryDate],
							[monShippingRate],
							[fltTotalWeight],
							[intNoOfBox],
							[fltHeight],
							[fltWidth],
							[fltLength],
							[dtCreateDate],
							[numCreatedBy],
							numBoxID,
							[numOppBizDocItemID]
						)
						SELECT  @numShippingReportId,--X.[numShippingReportId],
								X.[numItemCode],
								X.[tintServiceType],
								X.[dtDeliveryDate],
								X.[monShippingRate],
								X.[fltTotalWeight],
								X.[intNoOfBox],
								X.[fltHeight],
								X.[fltWidth],
								X.[fltLength],
								GETUTCDATE(),--X.[dtCreateDate],
								@numUserCntId,
								@numBoxID,
								CASE @tintMode WHEN 0 THEN X.numOppBizDocItemID ELSE (SELECT TOP 1 numOppBizDocItemID FROM OpportunityBizDocItems OBI WHERE OBI.numItemCode = X.numItemCode) END AS numOppBizDocItemID
						FROM    ( SELECT    *
									FROM      OPENXML (@hDocItem, '/NewDataSet/Items', 2)
											WITH ( --numShippingReportId  NUMERIC(18,0),
											numItemCode NUMERIC(18, 0), tintServiceType TINYINT, dtDeliveryDate DATETIME, monShippingRate DECIMAL(20,5), 
											fltTotalWeight FLOAT, intNoOfBox INT, fltHeight FLOAT, fltWidth FLOAT, fltLength FLOAT,numOppBizDocItemID NUMERIC)
								) X
					
				EXEC sp_xml_removedocument @hDocItem	
			END                                
                
			IF ISNULL(@numOppId,0) > 0
			BEGIN
				DECLARE @bitUseShippersAccountNo BIT
				DECLARE @numTempDivisionID NUMERIC(18,0)
				DECLARE @vcPayerAccountNo VARCHAR(MAX)
				DECLARE @vcPayerStreet VARCHAR(MAX)
				DECLARE @vcPayerCity VARCHAR(MAX)
				DECLARE @numPayerState NUMERIC(18,0)

				SELECT 
					@bitUseShippersAccountNo=ISNULL(bitUseShippersAccountNo,0) 
					,@numTempDivisionID=DivisionMaster.numDivisionID
				FROM 
					OpportunityMaster 
				INNER JOIN
					DivisionMaster
				ON
					OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID
				WHERE 
					OpportunityMaster.numDomainId=@numDomainId 
					AND OpportunityMaster.numOppId=@numOppId

				IF ISNULL(@bitUseShippersAccountNo,0) = 1 AND ISNULL(@tintPayorType,0) <> 2 AND EXISTS (SELECT ID FROM DivisionMasterShippingAccount WHERE numDivisionID=@numTempDivisionID AND numShipViaID=@numShippingCompany)
				BEGIN
					SELECT
						@vcPayerAccountNo=ISNULL(vcAccountNumber,'')
						,@vcPayerStreet=ISNULL(vcStreet,'')
						,@vcPayerCity=ISNULL(vcCity,'')
						,@numPayorCountry=numCountry
						,@numPayerState=numState
						,@vcPayorZip=vcZipCode
					FROM
						DivisionMasterShippingAccount
					WHERE
						numDivisionID=@numTempDivisionID 
						AND numShipViaID=@numShippingCompany

					UPDATE
						ShippingReport
					SET
						tintPayorType=2
						,vcPayorAccountNo=@vcPayerAccountNo
						,vcPayerStreet=@vcPayerStreet
						,vcPayerCity=@vcPayerCity
						,numPayorCountry=@numPayorCountry
						,numPayerState=@numPayerState
						,vcPayorZip=@vcPayorZip
					WHERE
						numShippingReportId=@numShippingReportId
				END
			END

			

        END
        ELSE IF @numShippingReportId > 0 
        BEGIN
            UPDATE 
				[ShippingReport]
            SET
				[numShippingCompany] = @numShippingCompany,
                vcValue1 = @Value1,
                vcValue2 = @Value2,
                vcValue3 = @Value3,
                vcValue4 = @Value4,
                vcFromState = @vcFromState,
                vcFromZip = @vcFromZip,
                vcFromCountry = @vcFromCountry,
                vcToState = @vcToState,
                vcToZip = @vcToZip,
                vcToCountry = @vcToCountry,
				tintPayorType = @tintPayorType,
				vcPayorAccountNo=@vcPayorAccountNo,
				numPayorCountry=@numPayorCountry,
				vcPayorZip=@vcPayorZip,
				vcFromCity=@vcFromCity,
				vcFromAddressLine1=@vcFromAddressLine1,
				vcFromAddressLine2=@vcFromAddressLine2,
				vcToCity=@vcToCity,
				vcToAddressLine1=@vcToAddressLine1,
				vcToAddressLine2=@vcToAddressLine2,
				vcFromName = @vcFromName,
				vcFromCompany = @vcFromCompany,
				vcFromPhone = @vcFromPhone,
				bitFromResidential = @bitFromResidential,
				vcToName = @vcToName,
				vcToCompany = @vcToCompany,
				vcToPhone = @vcToPhone,
				bitToResidential = @bitToResidential,
				IsCOD = @IsCOD ,					
				IsDryIce = @IsDryIce,				
				IsHoldSaturday  = @IsHoldSaturday,			
				IsHomeDelivery = @IsHomeDelivery,			
				IsInsideDelevery = @IsInsideDelevery,		
				IsInsidePickup = @IsInsidePickup,			
				IsReturnShipment = @IsReturnShipment,		
				IsSaturdayDelivery = @IsSaturdayDelivery,		
				IsSaturdayPickup = @IsSaturdayPickup,		
				numCODAmount = @numCODAmount,			
				vcCODType = @vcCODType,				
				numTotalInsuredValue = @numTotalInsuredValue,
				IsAdditionalHandling = @IsAdditionalHandling,
				IsLargePackage = @IsLargePackage,
				vcDeliveryConfirmation = @vcDeliveryConfirmation,
				vcDescription = @vcDescription,							
				numOppId = @numOppId,
				numTotalCustomsValue = @numTotalCustomsValue,
				tintSignatureType=@tintSignatureType
            WHERE   
				[numShippingReportId] = @numShippingReportId
        END
    END
GO
