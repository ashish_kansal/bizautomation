GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportDashboard_UpdateLastCacheDate')
DROP PROCEDURE dbo.USP_ReportDashboard_UpdateLastCacheDate
GO
CREATE PROCEDURE [dbo].[USP_ReportDashboard_UpdateLastCacheDate]
(
	@numDomainID NUMERIC(18,0)
	,@numDashboardID NUMERIC(18,0)
)
AS 
BEGIN
	UPDATE 
		ReportDashboard 
	SET 
		dtLastCacheDate = GETUTCDATE()
	WHERE 
		numDomainID=@numDomainID 
		AND numDashBoardID = @numDashboardID
END
GO