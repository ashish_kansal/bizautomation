 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='UpdateInboxDetails')
DROP PROCEDURE UpdateInboxDetails
GO
--this procedure is developed to update existing mails which doesn't have body part
-- UpdateInboxDetails 1,''
--*********** obsolete procedure *******************
CREATE PROCEDURE UpdateInboxDetails
    @tintMode TINYINT = 1,
    @strXML AS TEXT = '',
    @numUserCntID NUMERIC
AS 
    BEGIN
        IF @tintMode = 1 
            BEGIN
		
                SELECT TOP 10
                        [numEmailHstrID],
                        [numUid],
                        [numDomainID],
                        [numUserCntId]
                FROM    [EmailHistory]
                WHERE   [numUid] > 0
                        AND DATALENGTH(ISNULL([vcBody], '')) = 0
                        AND [numUserCntId] = @numUserCntID
                ORDER BY [numDomainID],
                        [numUserCntId],
                        numUId

            END
        IF @tintMode = 2 
            BEGIN
                IF CONVERT(VARCHAR(10), @strXML) <> '' 
                    BEGIN                                        
                        DECLARE @hDoc4 INT                                                
                        EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strXML                                                
                                                
                        UPDATE  [EmailHistory]
                        SET     EmailHistory.[vcBody] = Y.[Body],
                                [vcBodyText] = Y.[BodyText],
                                vcFrom = dbo.GetEmaillName(Y.numEmailHstrID, 4),
                                [vcTo] = dbo.GetEmaillName(Y.numEmailHstrID, 1),
                                [vcCC] = dbo.GetEmaillName(Y.numEmailHstrID, 3)
                        FROM    ( SELECT    X.UId,
                                            ISNULL(X.Body, '<br>') Body,
                                            X.BodyText,
                                            X.numEmailHstrID
                                  FROM      ( SELECT    *
                                              FROM      OPENXML (@hDoc4, '/NewDataSet/Table',2)
                                                        WITH ( numEmailHstrID NUMERIC(9), UId NUMERIC(9), Body TEXT, BodyText TEXT )
                                            ) X
                                ) Y
                        WHERE   [EmailHistory].[numUid] = Y.UID
                                AND [EmailHistory].[numUserCntId] = @numUserCntId
               
                        CREATE TABLE #temp
                            (
                              numEmailHstrID NUMERIC,
                              UId NUMERIC(9),
                              HasAttachments BIT,
                              AttachmentName VARCHAR(500),
                              AttachmentType VARCHAR(500),
                              NewAttachmentName VARCHAR(500)
                            )
               
                        INSERT  INTO #temp
                                SELECT  0 AS numEmailHstrID,
                                        X.UId,
                                        X.HasAttachments,
                                        X.AttachmentName,
                                        X.AttachmentType,
                                        X.NewAttachmentName
                                FROM    ( SELECT    *
                                          FROM      OPENXML (@hDoc4, '/NewDataSet/Table',2)
                                                    WITH ( UId NUMERIC(9), HasAttachments BIT, AttachmentName VARCHAR(500), AttachmentType VARCHAR(500), NewAttachmentName VARCHAR(500) )
                                        ) X
                                WHERE   X.HasAttachments = 1
                                            

                        
                  
          
                        UPDATE  #temp
                        SET     numEmailHstrID = X.numEmailHstrID
                        FROM    ( SELECT    #temp.[UId],
                                            EH.[numEmailHstrID]
                                  FROM      #temp
                                            INNER JOIN [EmailHistory] EH ON [#temp].UId = EH.[numUid]
                                ) X
                        WHERE   [#temp].UId = X.[UId]
          
                        DECLARE @numEmailHstrID NUMERIC 
                        DECLARE @AttachmentName VARCHAR(500)            
                        DECLARE @AttachmentType VARCHAR(500)    
                        DECLARE @NewAttachmentName VARCHAR(500)  
                        DECLARE @minID NUMERIC 
                        SELECT  @minID = MIN(numEmailHstrID)
                        FROM    #temp
          
                        SELECT TOP 1
                                @numEmailHstrID = numEmailHstrID,
                                @AttachmentName = AttachmentName,
                                @AttachmentType = AttachmentType,
                                @NewAttachmentName = NewAttachmentName
                        FROM    #temp
                        ORDER BY numEmailHstrID DESC
                        WHILE @numEmailHstrID > @minID
                            BEGIN
                                DELETE  FROM [EmailHstrAttchDtls]
                                WHERE   [numEmailHstrID] = @numEmailHstrID ;
                                EXEC USP_InsertAttachment @AttachmentName, '',
                                    @AttachmentType, @numEmailHstrID,
                                    @NewAttachmentName  
		  	
                                SELECT TOP 1
                                        @numEmailHstrID = numEmailHstrID,
                                        @AttachmentName = AttachmentName,
                                        @AttachmentType = AttachmentType,
                                        @NewAttachmentName = NewAttachmentName
                                FROM    #temp
                                WHERE   numEmailHstrID < @numEmailHstrID
                                ORDER BY numEmailHstrID DESC
                            END
          
          
                        DROP TABLE [#temp]
                    END  
            END
        IF @tintMode = 3 
            BEGIN
                UPDATE [EmailHistory] SET [vcBody] ='<br/>' WHERE [numUid] IN (SELECT ID FROM dbo.splitIDs(@strXML,','))
                AND [numUserCntId] = @numUserCntID
            END 
	
    END