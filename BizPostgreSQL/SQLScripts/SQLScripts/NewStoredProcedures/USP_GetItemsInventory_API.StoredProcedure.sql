GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemsInventory_API')
DROP PROCEDURE USP_GetItemsInventory_API
GO
CREATE PROCEDURE [dbo].[USP_GetItemsInventory_API]
          @numDomainID AS NUMERIC(9),
          @WebAPIId    AS int,
          @ModifiedDate AS VARCHAR(50)='' 
AS

		-- GET DEFAULT WAREHOUSE SET FOR MARKET PLACE     
		DECLARE @numDefaultWarehouse AS NUMERIC(18,0)
		SELECT @numDefaultWarehouse=numWareHouseID FROM WebAPIDetail WHERE numDomainID=@numDomainID AND WebApiId=@WebAPIId

		DECLARE @TempItem TABLE
		(
			numDomainID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			vcItemName VARCHAR(300),
			vcModelID VARCHAR(200),
			numItemGroup NUMERIC(18,0),
			fltWeight float,
			fltLength float,
			fltHeight float,
			fltWidth float,
			vcManufacturer VARCHAR(250),
			IsArchieve BIT,
			txtItemDesc VARCHAR(1000),
			vcSKU VARCHAR(50),
			numBarCodeId VARCHAR(50),
			bintCreatedDate DATETIME,
			bintModifiedDate DATETIME
		)

		INSERT INTO 
			@TempItem
		SELECT
			numDomainID,
			numItemCode,
			ISNULL(vcItemName,''),
			ISNULL(vcModelID,''),
			ISNULL(numItemGroup,0) numItemGroup,
			ISNULL(fltWeight,0) fltWeight,
			ISNULL(fltLength,0) fltLength,
			ISNULL(fltHeight,0) fltHeight,
			ISNULL(fltWidth,0) fltWidth,
			ISNULL(vcManufacturer,'') vcManufacturer,
			ISNULL(IsArchieve,0) IsArchieve,
			ISNULL(txtItemDesc,'') txtItemDesc,
			ISNULL(vcSKU,''),
			ISNULL(numBarCodeId,''),
			bintCreatedDate,
			bintModifiedDate
		FROM
			Item
		WHERE
			Item.numDomainID = @numDomainID
			AND  LEN(Item.vcItemName) > 0
			AND Item.charItemType ='P'
			AND Item.vcExportToAPI IS NOT NULL
			AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))

		SELECT
			X.*
		FROM
		(
			SELECT
				Item.numDomainID,
				ISNULL(ItemAPI.vcAPIItemID,0) AS vcAPIItemID,
				Item.numItemCode,
				Item.vcItemName,
				Item.vcModelID,
				Item.numItemGroup,
				Item.fltWeight AS intWeight,
				Item.fltWeight,
				Item.fltLength,
				Item.fltHeight,
				Item.fltWidth,
				Item.vcManufacturer,
				Item.IsArchieve,
				Item.txtItemDesc,
				Item.vcSKU,
				Item.numBarCodeId,
				TempWarehouseItem.numOnHand AS QtyOnHand,
				TempWarehouseItem.monWListPrice AS monListPrice,
				(
					SELECT 
						numItemImageId, 
						vcPathForImage, 
						vcPathForTImage, 
						bitDefault,
						CASE 
							WHEN bitdefault = 1 THEN -1 
							ELSE isnull(intDisplayOrder,0) 
						END AS intDisplayOrder 
					FROM 
						ItemImages  
					WHERE 
						numItemCode=Item.numItemCode 
					ORDER BY 
						CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END  asc 
					FOR XML AUTO,ROOT('Images')
				) AS xmlItemImages,
				CAST(ISNULL(ItemExtendedDetails.txtDesc,'') AS VARCHAR(MAX)) AS vcExtendedDescToAPI
			FROM
				@TempItem Item
			LEFT JOIN
				ItemExtendedDetails
			ON
				ItemExtendedDetails.numItemCode = Item.numItemCode
			LEFT JOIN 
				ItemAPI 
			ON 
				ItemAPI.WebApiId = @WebAPIId 
				AND Item.numItemCode = ItemAPI.numItemID 
				AND Item.vcSKU = ItemAPI.vcSKU
			CROSS APPLY
			(
				SELECT
					ISNULL(SUM(numOnHand),0) AS numOnHand,
					ISNULL(MAX(monWListPrice),0) AS monWListPrice,
					MAX(dtModified) AS dtModified
				FROM
					WareHouseItems
				WHERE
					WareHouseItems.numItemID = Item.numItemCode
					AND WareHouseItems.numWareHouseID = @numDefaultWarehouse
				GROUP BY
					numWareHouseItemID
			) TempWarehouseItem
			WHERE
				numItemGroup = 0 --INVENTORY ITEM
				AND 1 = (CASE 
							WHEN @ModifiedDate = '' THEN 1
							ELSE (CASE 
									WHEN TempWarehouseItem.dtModified > @ModifiedDate THEN 1
									ELSE 0
								  END)
						END) 
			UNION
			SELECT
				Item.numDomainID,
				ISNULL(ItemAPI.vcAPIItemID,0) AS vcAPIItemID,
				Item.numItemCode,
				Item.vcItemName,
				Item.vcModelID,
				Item.numItemGroup,
				Item.fltWeight AS intWeight,
				Item.fltWeight,
				Item.fltLength,
				Item.fltHeight,
				Item.fltWidth,
				Item.vcManufacturer,
				Item.IsArchieve,
				Item.txtItemDesc,
				ISNULL(WareHouseItems.vcWHSKU,'') vcSKU,
				ISNULL(WareHouseItems.vcBarCode,'') numBarCodeId,
				WareHouseItems.numOnHand AS QtyOnHand,
				WareHouseItems.monWListPrice AS monListPrice,
				(
					SELECT 
						numItemImageId, 
						vcPathForImage, 
						vcPathForTImage, 
						bitDefault,
						CASE 
							WHEN bitdefault = 1 THEN -1 
							ELSE isnull(intDisplayOrder,0) 
						END AS intDisplayOrder 
					FROM 
						ItemImages  
					WHERE 
						numItemCode=Item.numItemCode 
					ORDER BY 
						CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END ASC 
					FOR XML AUTO,ROOT('Images')
				) AS xmlItemImages,
				CAST(ISNULL(ItemExtendedDetails.txtDesc,'') AS VARCHAR(MAX)) AS vcExtendedDescToAPI
			FROM
				@TempItem Item
			INNER JOIN
				WareHouseItems
			ON
				WareHouseItems.numItemID = Item.numItemCode
				AND WareHouseItems.numWareHouseID = @numDefaultWarehouse
			LEFT JOIN
				ItemExtendedDetails
			ON
				ItemExtendedDetails.numItemCode = Item.numItemCode
			LEFT JOIN 
				ItemAPI 
			ON 
				ItemAPI.WebApiId = @WebAPIId 
				AND Item.numItemCode = ItemAPI.numItemID 
				AND Item.vcSKU = ItemAPI.vcSKU
			WHERE
				numItemGroup > 0 --MATRIX ITEM
				AND 1 = (CASE 
							WHEN @ModifiedDate = '' THEN 1
							ELSE (CASE 
									WHEN WareHouseItems.dtModified > @ModifiedDate THEN 1
									ELSE 0
								  END)
						END) 
		) AS X
		ORDER BY
			X.numItemCode ASC
