GO
/****** Object:  StoredProcedure [dbo].[USP_MANAGE_CUSTOM_BOX]    Script Date: 30/07/2012 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_MANAGE_CUSTOM_BOX' ) 
    DROP PROCEDURE USP_MANAGE_CUSTOM_BOX
GO

CREATE PROCEDURE USP_MANAGE_CUSTOM_BOX
(
	@numCustomPackageID		NUMERIC(18) OUT,
	@vcPackageName			VARCHAR(1000),
	@fltWidth				NUMERIC(18,2),
	@fltHeight				NUMERIC(18,2),
	@fltLength				NUMERIC(18,2),
	@fltTotalWeight			NUMERIC(18,2)
)	
AS 
BEGIN

IF EXISTS(SELECT * FROM dbo.CustomPackages WHERE vcPackageName = @vcPackageName AND [numCustomPackageID] <> @numCustomPackageID )
	BEGIN
	    RAISERROR ('ERROR: Custom Box Name already exists. Please provide another Box Name.', -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
        RETURN
	END

IF EXISTS(SELECT [numCustomPackageID] FROM [dbo].[CustomPackages] WHERE [numCustomPackageID] = @numCustomPackageID)
	BEGIN
		UPDATE dbo.CustomPackages SET vcPackageName = @vcPackageName,
									  fltHeight = @fltHeight,
									  fltLength = @fltLength,
									  fltTotalWeight = @fltTotalWeight,
									  fltWidth = @fltWidth
								  WHERE numCustomPackageID = @numCustomPackageID										    
	END
ELSE
	BEGIN
		DECLARE @numPackageTypeID AS NUMERIC(18,2)
		SET @numPackageTypeID = 100
		SET @numPackageTypeID =  @numPackageTypeID + (SELECT MAX(numPackageTypeID) FROM dbo.CustomPackages )
		
		INSERT INTO dbo.CustomPackages (numPackageTypeID,vcPackageName,fltWidth,fltHeight,fltLength,fltTotalWeight) 
								VALUES (@numPackageTypeID,@vcPackageName,@fltWidth,@fltHeight,@fltLength,@fltTotalWeight) 
		
		SET @numCustomPackageID = SCOPE_IDENTITY()
	END

END

