GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpenOrdersAddBill')
DROP PROCEDURE USP_GetOpenOrdersAddBill
GO
CREATE PROCEDURE [dbo].[USP_GetOpenOrdersAddBill]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	IF ISNULL(@numOppID,0) > 0
	BEGIN
		SELECT
			numOppId
			,vcPOppName
		FROM
			OpportunityMaster
		WHERE
			numDomainId = @numDomainID
			AND numOppId = @numOppID
	END
	ELSE
	BEGIN
		SELECT
			numOppId
			,vcPOppName
		FROM
			OpportunityMaster
		WHERE
			numDomainId = @numDomainID
			AND numDivisionId = @numDivisionID
			AND tintOppType = 1
			AND tintOppStatus = 1
			AND ISNULL(tintshipped,0) = 0
			AND EXISTS (SELECT OI.numoppitemtCode FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode=I.numItemCode WHERE OI.numOppId = OpportunityMaster.numOppId AND ISNULL(I.bitExpenseItem,0) = 1)
		ORDER BY
			numOppId
	END
END
GO