/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddUpdateWareHouseForItems')
DROP PROCEDURE USP_AddUpdateWareHouseForItems
GO
CREATE PROCEDURE [dbo].[USP_AddUpdateWareHouseForItems]  
@numItemCode as numeric(9)=0,  
@numWareHouseID as numeric(9)=0,
@numWareHouseItemID as numeric(9)=0 OUTPUT,
@vcLocation as varchar(250)='',
@monWListPrice as DECIMAL(20,5) =0,
@numOnHand as FLOAT=0,
@numReorder as FLOAT=0,
@vcWHSKU as varchar(100)='',
@vcBarCode as varchar(50)='',
@numDomainID AS NUMERIC(9),
@strFieldList as TEXT='',
@vcSerialNo as varchar(100)='',
@vcComments as varchar(1000)='',
@numQty as numeric(18)=0,
@byteMode as tinyint=0,
@numWareHouseItmsDTLID as numeric(18)=0,
@numUserCntID AS NUMERIC(9)=0,
@dtAdjustmentDate AS DATETIME=NULL,
@numWLocationID NUMERIC(9)=0
AS  
BEGIN
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @numGlobalWarehouseItemID NUMERIC(18,0)
	SET @numDomain = @numDomainID

	DECLARE @bitLotNo AS BIT = 0	
	DECLARE @bitSerialized AS BIT  = 0
	DECLARE @bitMatrix AS BIT = 0
	DECLARE @numItemGroup AS NUMERIC(18,0) =0
	DECLARE @vcSKU AS VARCHAR(200) = ''
	DECLARE @vcUPC AS VARCHAR(200) = ''
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @bitRemoveGlobalWarehouse BIT

	SELECT @bitRemoveGlobalWarehouse=ISNULL(bitRemoveGlobalLocation,0) FROM Domain WHERE numDomainId=@numDomainID

	SELECT 
		@bitLotNo=ISNULL(Item.bitLotNo,0),
		@bitSerialized=ISNULL(Item.bitSerialized,0),
		@bitMatrix = ISNULL(bitMatrix,0),
		@numItemGroup = ISNULL(@numItemGroup,0),
		@vcSKU = ISNULL(vcSKU,0),
		@vcUPC = ISNULL(numBarCodeId,0),
		@monListPrice = ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode 
		AND numDomainID=@numDomainID

	DECLARE @vcDescription AS VARCHAR(100)

	IF @byteMode=0 or @byteMode=1 or @byteMode=2 or @byteMode=5
	BEGIN
		--Insert/Update WareHouseItems
		IF @byteMode=0 or @byteMode=1
		BEGIN
			IF @numWareHouseItemID>0
			BEGIN
				IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numWareHouseItemID)
				BEGIN
					RAISERROR('WAREHOUSE_DETAIL_NOT_FOUND',16,1)
				END
				
				UPDATE 
					WareHouseItems
				SET 
					numWareHouseID=@numWareHouseID,				
					numReorder=@numReorder,
					monWListPrice=@monWListPrice,
					vcLocation=@vcLocation,
					numWLocationID = @numWLocationID,
					vcWHSKU=@vcWHSKU,
					vcBarCode=@vcBarCode,
					dtModified=GETDATE()   
				WHERE 
					numItemID=@numItemCode 
					AND numDomainID=@numDomainID 
					AND numWareHouseItemID=@numWareHouseItemID
		
				IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode) AND NOT EXISTS(SELECT numChildItemID FROM dbo.OpportunityKitItems WHERE numChildItemID = @numItemCode) AND NOT EXISTS(SELECT numItemID FROM dbo.OpportunityKitChildItems WHERE numItemID = @numItemCode)
				BEGIN
					IF ((SELECT ISNULL(numOnHand,0) FROM WareHouseItems where numItemID = @numItemCode AND numDomainId = @numDomainID and numWareHouseItemID = @numWareHouseItemID) = 0)
					BEGIN
						UPDATE 
							WareHouseItems 
						SET 
							numOnHand = @numOnHand 
						WHERE 
							numItemID = @numItemCode 
							AND numDomainID = @numDomainID 
							AND numWareHouseItemID = @numWareHouseItemID		
					END
				END
		
				SET @vcDescription='UPDATE WareHouse'
			END
			ELSE
			BEGIN
				IF NOT EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode)
				BEGIN
					RAISERROR('INVALID_ITEM_CODE',16,1)
				END

				INSERT INTO WareHouseItems 
				(
					numItemID, 
					numWareHouseID,
					numOnHand,
					numReorder,
					monWListPrice,
					vcLocation,
					vcWHSKU,
					vcBarCode,
					numDomainID,
					dtModified,
					numWLocationID
				)  
				VALUES
				(
					@numItemCode,
					@numWareHouseID,
					(Case When @bitLotNo=1 or @bitSerialized=1 then 0 else @numOnHand end),
					@numReorder,
					@monWListPrice,
					@vcLocation,
					@vcWHSKU,
					@vcBarCode,
					@numDomainID,
					GETDATE(),
					@numWLocationID
				)  

				SET @numWareHouseItemID = SCOPE_IDENTITY()


				IF (SELECT COUNT(*) FROM WareHouseItems WHERE numWareHouseID=@numWareHouseID AND numItemID=@numItemCode AND numWLocationID=-1) = 0 AND @bitRemoveGlobalWarehouse = 0
				BEGIN
					INSERT INTO WareHouseItems 
					(
						numItemID, 
						numWareHouseID,
						numWLocationID,
						numOnHand,
						numReorder,
						monWListPrice,
						vcLocation,
						vcWHSKU,
						vcBarCode,
						numDomainID,
						dtModified
					)  
					VALUES
					(
						@numItemCode,
						@numWareHouseID,
						-1,
						0,
						0,
						@monWListPrice,
						'',
						@vcWHSKU,
						@vcBarCode,
						@numDomainID,
						GETDATE()
					)  

					SELECT @numGlobalWarehouseItemID = SCOPE_IDENTITY()
				END



				SET @vcDescription='INSERT WareHouse'
			END
		END

		--Insert/Update WareHouseItmsDTL

		DECLARE @OldQty FLOAT = 0

		IF @byteMode=0 or @byteMode=2 or @byteMode=5
		BEGIN
			IF @bitLotNo=1 or @bitSerialized=1
			BEGIN
				IF @bitSerialized=1
					SET @numQty=1
	
				IF @byteMode=0
				BEGIN
					SELECT TOP 1 
						@numWareHouseItmsDTLID=numWareHouseItmsDTLID,
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItemID=@numWareHouseItemID 
						AND vcSerialNo=@vcSerialNo and numQty > 0
				END
				ELSE IF @numWareHouseItmsDTLID>0
				BEGIN
					SELECT TOP 1 
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID
				END

				IF @numWareHouseItmsDTLID>0
				BEGIN
					UPDATE 
						WareHouseItmsDTL 
					SET 
						vcComments = @vcComments,
						numQty=@numQty,
						vcSerialNo=@vcSerialNo
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
						AND numWareHouseItemID=@numWareHouseItemID
                
					SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Update Lot/Serial#) : ' ELSE 'UPDATE Lot/Serial# : ' END + @vcSerialNo 
				END
				ELSE
				BEGIN
				   INSERT INTO WareHouseItmsDTL
				   (numWareHouseItemID,vcSerialNo,vcComments,numQty,bitAddedFromPO)  
				   VALUES 
				   (@numWareHouseItemID,@vcSerialNo,@vcComments,@numQty,0)
				
				   SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()

				   SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Insert Lot/Serial#) : ' ELSE 'INSERT Lot/Serial# : ' END + @vcSerialNo 
				END

 				UPDATE 
					WareHouseItems 
				SET 
					numOnHand=numOnHand + (@numQty - @OldQty),
					dtModified=GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 
					AND [numDomainID] = @numDomainID
					AND (numOnHand + (@numQty - @OldQty))>=0
			END 
		END

		DECLARE @bitOppOrderExists AS BIT = 0

		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		IF @bitMatrix = 1 AND ISNULL(@bitOppOrderExists,0) <> 1 -- ITEM LEVEL ATTRIBUTES
		BEGIN
			IF ISNULL(@numWareHouseItemID,0) > 0
			BEGIN
				DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId = @numWareHouseItemID
			
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
					@numWareHouseItemID,
					0 
				FROM 
					ItemAttributes
				WHERE
					numDomainID = @numDomainID
					AND numItemCode = @numItemCode
			END

			IF ISNULL(@numGlobalWarehouseItemID,0) > 0
			BEGIN
				DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId = @numGlobalWarehouseItemID
				
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
					@numGlobalWarehouseItemID,
					0 
				FROM 
					ItemAttributes
				WHERE
					numDomainID = @numDomainID
					AND numItemCode = @numItemCode
			END   
		END
		ELSE IF DATALENGTH(@strFieldList) > 2 AND ISNULL(@bitOppOrderExists,0) <> 1
		BEGIN
			--Insert Custom Fields base on WareHouseItems/WareHouseItmsDTL 
			declare @hDoc as int     
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

				declare  @rows as integer                                        
	                                         
			SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
							WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                        
	                                                                           

			if @rows>0                                        
			begin  
				Create table #tempTable (ID INT IDENTITY PRIMARY KEY,Fld_ID numeric(9),Fld_Value varchar(100))   
	                                      
				insert into #tempTable (Fld_ID,Fld_Value)                                        
				SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
				WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                           
	                                         
				delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C inner join #tempTable T on C.Fld_ID=T.Fld_ID where C.RecId=@numWareHouseItemID                              
	      
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,Fld_Value,RecId,bitSerialized
				) 

				select Fld_ID,isnull(Fld_Value,'') as Fld_Value,@numWareHouseItemID,0 from #tempTable 

				drop table #tempTable                                        
			 end  

			 EXEC sp_xml_removedocument @hDoc 
		END	  
 
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numItemCode, --  numeric(9, 0)
		@tintRefType = 1, --  tinyint
		@vcDescription = @vcDescription, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtAdjustmentDate,
		@numDomainID = @numDomain 

		IF ISNULL(@numGlobalWarehouseItemID,0) > 0
		BEGIN        
			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numGlobalWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @vcDescription,
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = @dtAdjustmentDate,
			@numDomainID = @numDomainID 
		END

		-- KEEP IT LAST IN SECTION
		IF ISNULL(@numItemGroup,0) > 0 OR ISNULL(@bitMatrix,0) = 1 -- IF IT's MATRIX ITEM THEN 
		BEGIN
			UPDATE WareHouseItems SET monWListPrice = (CASE WHEN @monListPrice > 0 THEN @monListPrice ELSE monWListPrice END), vcWHSKU=@vcSKU, vcBarCode=@vcUPC WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
		END
	END
	ELSE IF @byteMode=3
	BEGIN
		IF EXISTS (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID]=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF EXISTS (SELECT * FROM OpportunityItemsReceievedLocation WHERE [numDomainId]=@numDomainID AND numWarehouseItemID=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF (SELECT COUNT(*) FROM OpportunityKitItems WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR('OpportunityKitItems_Depend',16,1);
			RETURN
		END

		IF (SELECT COUNT(*) FROM OpportunityKitChildItems WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR('OpportunityKitChildItems_Depend',16,1);
			RETURN
		END

		IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainId=@numDomainID AND numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR ('WorkOrder_Depend',16,1);
			RETURN
		END
	    
		IF @bitLotNo=1 OR @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId in (select numWareHouseItmsDTLID from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID)
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END
		ELSE
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItemID
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		DELETE from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID 
		DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
		DELETE from WareHouseItems where numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
	END
	ELSE IF @byteMode=4
	BEGIN
		IF @bitLotNo=1 or @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItmsDTLID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		update WHI SET numOnHand=WHI.numOnHand - WHID.numQty,dtModified=GETDATE()
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID
		AND (WHI.numOnHand - WHID.numQty)>=0

		SELECT @numWareHouseItemID=WHI.numWareHouseItemID,@numItemCode=numItemID 
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID


		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numItemCode, --  numeric(9, 0)
			@tintRefType = 1, --  tinyint
			@vcDescription = 'DELETE Lot/Serial#', --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = NULL,
			@numDomainID = @numDomain

		DELETE from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
	END
END