GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageUserSMTPDetails')
DROP PROCEDURE USP_ManageUserSMTPDetails
GO
CREATE PROCEDURE  USP_ManageUserSMTPDetails
    @numUserId numeric(18, 0),
	@numDomainId numeric(18, 0),
    @vcSMTPUserName varchar(50),
    @vcSMTPPassword varchar(100),
    @vcSMTPServer varchar(100),
    @numSMTPPort numeric(18, 0),
    @bitSMTPSSL bit,
    @bitSMTPServer bit,
    @bitSMTPAuth bit
AS                                    
BEGIN

	IF NOT EXISTS(SELECT 1 FROM [dbo].[SMTPUserDetails] WHERE [numDomainId] = @numDomainId AND [numUserId] = @numUserId)
	BEGIN
		INSERT INTO [dbo].[SMTPUserDetails] ([numDomainId], [numUserId], [vcSMTPUserName], [vcSMTPPassword], [vcSMTPServer], [numSMTPPort], [bitSMTPSSL], [bitSMTPServer], [bitSMTPAuth])
		SELECT @numDomainId, @numUserId, @vcSMTPUserName, @vcSMTPPassword, @vcSMTPServer, @numSMTPPort, @bitSMTPSSL, @bitSMTPServer, @bitSMTPAuth
	END
	ELSE
	BEGIN
		UPDATE [dbo].[SMTPUserDetails]
			SET    [numDomainId] = @numDomainId, [numUserId] = @numUserId, [vcSMTPUserName] = @vcSMTPUserName, [vcSMTPPassword] = @vcSMTPPassword, [vcSMTPServer] = @vcSMTPServer, [numSMTPPort] = @numSMTPPort, [bitSMTPSSL] = @bitSMTPSSL, [bitSMTPServer] = @bitSMTPServer, [bitSMTPAuth] = @bitSMTPAuth
			WHERE  [numDomainId] = @numDomainId
				   AND [numUserId] = @numUserId
	END
END