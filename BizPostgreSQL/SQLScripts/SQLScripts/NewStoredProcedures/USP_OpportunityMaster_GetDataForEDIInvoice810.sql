SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDataForEDIInvoice810')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDataForEDIInvoice810
GO


CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDataForEDIInvoice810]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		'' AS 'AccountingID'
		,OBD.vcBizDocID AS Invoice
		,CONVERT(VARCHAR(10),OBD.dtCreatedDate, 101)  AS InvoiceDate
		,MST.vcPOppName AS 'PO'
		,CONVERT(VARCHAR(10), Mst.bintCreatedDate, 101) AS 'PODate'
		,'' AS DepartmentNumber
		,'' AS BillofLading
		,'' AS CarrierPro
		,'' AS SCAC
		,CASE 
				WHEN ISNULL(OBD.numShipVia,0) = 0 THEN (CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) END)
				WHEN OBD.numShipVia = -1 THEN 'Will-call'
				ELSE dbo.fn_GetListItemName(OBD.numShipVia)
		 END AS ShipVia	
		,dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipToName
		,ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,2)),'') AS  'ShipToAddress-LineOne'
		,'' AS 'ShipToAddress-LineTwo'
		,ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,2)),'') AS ShipToCity
		,ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,2)),'') AS ShipToState
		,ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,2)),'') AS ShipToZipcode
		,ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,2)),'') AS ShiptoCountry
		,'' AS Store	
		,dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BilltoName
		,ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,1)),'') AS 'BilltoAddress-LineOne'
		,'' AS 'BilltoAddress-LineTwo'
		,ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,1)),'') AS BilltoCity
		,ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,1)),'') AS BilltoState
		,ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,1)),'') AS BilltoZipcode
		,ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(@numOppId,@numDomainID,1)),'') AS BilltoCountry
		,'' AS BilltoCode
		,OBD.dtShippedDate AS ShipDate
		,'' AS TermsDescription
		,ISNULL(BTR.numNetDueInDays,0) AS NetDaysDue
		,'' AS DiscountDaysDue
		,'' AS DiscountPercent
		,'' AS Note
		,'' AS 'Weight'
		,'' AS TotalCasesShipped
		,'' AS TaxAmount
		,'' AS ChargeAmount1
		,'' AS ChargeAmount2
		,'' AS AllowancePercent1
		,'' AS AllowanceAmount1
		,'' AS AllowancePercent2
		,'' AS AllowanceAmount2
FROM OpportunityBizDocs OBD
	JOIN OpportunityMaster Mst ON Mst.numOppId = OBD.numOppId
	LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
 WHERE OBD.numBizDocId = 287 AND Mst.numOppId = @numOppID


	SELECT
	    '' AS RowType
		,numSortOrder AS Line
		,V.vcPartNo AS VendorPart
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE ISNULL(Item.vcSKU,'') END)
			ELSE
				(CASE WHEN LEN(ISNULL(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE ISNULL(Item.vcSKU,'') END)
		END BuyerPart
		,numBarCodeId AS UPC
		,txtItemDesc AS [Description]
		--,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit,0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* ISNULL(ShippingReportItems.intBoxQty,0)) AS NUMERIC(18, 2)) AS numShippedUnits
		,'' AS QuantityShipped
		,ISNULL(UOM.vcUnitName,'Units') UOM
		,OpportunityItems.monPrice AS UnitPrice
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit,0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* OpportunityItems.numUnitHour) AS NUMERIC(18, 2)) AS QuantityOrdered
		--,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* OpportunityItems.numUnitHour) AS NUMERIC(18, 2)) AS numUnits
		,'' AS PackSize
		,'' AS ofInnerPacks
		,'' AS ItemAllowancePercent
		,'' AS ItemAllowanceAmount
		,(CASE 
			WHEN charItemType='P' THEN 'Inventory Item'
			WHEN charItemType='S' THEN 'Service' 
			WHEN charItemType='A' THEN 'Accessory' 
			WHEN charItemType='N' THEN 'Non-Inventory Item' 
		END) AS vcItemType
		,ISNULL(OpportunityItems.vcNotes,'') vcNotes
		,OpportunityItems.numoppitemtCode AS 'BizOppItemID'
	FROM
		OpportunityItems
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	LEFT JOIN 
		UOM 
	ON
		OpportunityItems.numUOMId = UOM.numUOMId
	INNER JOIN Vendor V ON Item.numItemCode = V.numItemCode
	WHERE
		numOppId = @numOppID
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
END

GO


