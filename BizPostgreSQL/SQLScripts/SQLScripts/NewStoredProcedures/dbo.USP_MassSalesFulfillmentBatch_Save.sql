GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentBatch_Save')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentBatch_Save
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentBatch_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@tintMode TINYINT
	,@numBatchID NUMERIC(18,0)
	,@vcBatchName VARCHAR(MAX)
	,@vcBatchRecords VARCHAR(MAX)
)
AS 
BEGIN
	IF @tintMode = 1 -- ADD TO EXISTING BATCH
	BEGIN
		IF NOT EXISTS (SELECT ID FROM MassSalesFulfillmentBatch WHERE numDomainID=@numDomainID AND ID=@numBatchID)
		BEGIN
			RAISERROR('INVALID_BATCH',16,1)
			RETURN
		END
	END
	ELSE IF @tintMode = 2 -- CREATE NEW BATCH AND ADD
	BEGIN
		IF (SELECT COUNT(*) FROM MassSalesFulfillmentBatch WHERE numDomainID=@numDomainID AND ISNULL(bitEnabled,0) = 1) > 20
		BEGIN
			RAISERROR('MAX_ACTIVE_BATCH_LIMIT_EXCEED',16,1)
			RETURN
		END
		ELSE IF (SELECT COUNT(*) FROM MassSalesFulfillmentBatch WHERE numDomainID=@numDomainID AND vcName=@vcBatchName) > 0
		BEGIN
			RAISERROR('BATCH_WITH_SAME_NAME_EXISTS',16,1)
			RETURN
		END
		ELSE
		BEGIN
			INSERT INTO MassSalesFulfillmentBatch
			(
				numDomainID
				,numCreatedBy
				,dtCreatedDate
				,vcName
			)
			VALUES
			(
				@numDomainID
				,@numUserCntID
				,GETUTCDATE()
				,@vcBatchName
			)
		END

		SET @numBatchID = SCOPE_IDENTITY()
	END

	IF @tintMode = 4
	BEGIN
		UPDATE MassSalesFulfillmentBatch SET bitEnabled = 0 WHERE ID=@numBatchID
	END
	IF @tintMode = 3
	BEGIN
		DELETE
			MSFBO
		FROM
			MassSalesFulfillmentBatchOrders MSFBO
		INNER JOIN
		(
			SELECT
				CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0)) numOppID
				,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC(18,0)) numOppItemID
			FROM
				dbo.SplitString(@vcBatchRecords,',')
		) TEMP
		ON
			MSFBO.numOppID = TEMP.numOppID
			AND (ISNULL(MSFBO.numOppItemID,0) = ISNULL(TEMP.numOppItemID,0) OR ISNULL(TEMP.numOppItemID,0) = 0)
		WHERE
			MSFBO.numBatchID = @numBatchID
	END
	ELSE 
	BEGIN
		DECLARE @TEMP TABLE
		(
			numOppID NUMERIC(18,0)
			,numOppItemID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0)) numOppID
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC(18,0)) numOppItemID
		FROM
			dbo.SplitString(@vcBatchRecords,',')

		DELETE 
			MSFBO 
		FROM 
			MassSalesFulfillmentBatchOrders MSFBO 
		INNER JOIN 
			@TEMP T1
		ON
			MSFBO.numOppID = T1.numOppID
			AND ((ISNULL(MSFBO.numOppItemID,0) = 0 AND ISNULL(T1.numOppItemID,0) > 0) OR (ISNULL(MSFBO.numOppItemID,0) > 0 AND ISNULL(T1.numOppItemID,0) = 0))
		WHERE 
			MSFBO.numBatchID =@numBatchID 

		INSERT INTO MassSalesFulfillmentBatchOrders
		(
			numBatchID
			,numOppID
			,numOppItemID
		)
		SELECT
			@numBatchID	
			,numOppID
			,numOppItemID
		FROM
			@TEMP
	END
	
END
GO