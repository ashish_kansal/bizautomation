SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRuleMaster')
DROP PROCEDURE USP_GetRuleMaster
GO
CREATE PROCEDURE [dbo].[USP_GetRuleMaster]
    @numRuleID AS NUMERIC(9) = 0,
    @byteMode AS TINYINT,
    @numDomainID AS NUMERIC(9),
	@tintModuleType as TINYINT
AS 
    IF @byteMode = 0 
        BEGIN    
            SELECT  numRuleID,tintModuleType,vcRuleName,vcRuleDescription,
                    tintBasedOn,numFormFieldIdChange,tintStatus
		    FROM    RuleMaster RM
            WHERE   numRuleID = @numRuleID and numDomainID=@numDomainID 

	select  numRuleConditionID,vcCondition,vcJSON,tintConditionOrder FROM RuleCondition where numRuleID=@numRuleID and numDomainID=@numDomainID  
	ORDER BY tintConditionOrder 
  
        END   
    ELSE IF @byteMode = 1
            BEGIN    
                SELECT  RM.numRuleID,RM.tintModuleType,DFM.vcFormName as vcModuleType,RM.vcRuleName,RM.vcRuleDescription,
                    RM.tintBasedOn,RM.numFormFieldIdChange,RM.tintStatus,
					Case tintStatus When 0 then 'Active' else 'InActive' end as vcStatus
            FROM    RuleMaster RM join DynamicFormMaster as DFM on RM.tintModuleType=DFM.numFormId
            WHERE    numDomainID=@numDomainID  
                END
	ELSE IF @byteMode=2
		BEGIN
			select tintOrder+1 as tintOrder,vcDbColumnName,vcFieldName as vcFormFieldName,                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,numFieldId as numFormFieldId,vcFieldDataType  
FROM View_DynamicDefaultColumns
 where numFormId=@tintModuleType and isnull(bitWorkFlowField,0)=1 and numDomainId=@numDomainId
order by tintOrder asc   
		END
	 ELSE IF @byteMode = 3
            BEGIN    
                SELECT  RM.numRuleID,RM.tintModuleType,DFM.vcFormName as vcModuleType,RM.vcRuleName,RM.vcRuleDescription,
                    RM.tintBasedOn,RM.numFormFieldIdChange
            FROM    RuleMaster RM join DynamicFormMaster as DFM on RM.tintModuleType=DFM.numFormId
            WHERE    numDomainID=@numDomainID and  tintModuleType=@tintModuleType and tintStatus=0
                END
	ELSE IF @byteMode = 4
            BEGIN    
             select  numRuleConditionID,vcCondition,vcJSON,tintConditionOrder FROM RuleCondition where numRuleID=@numRuleID and numDomainID=@numDomainID  
			ORDER BY tintConditionOrder 
                END
GO
