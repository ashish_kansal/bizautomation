

-- =============================================
-- Author:		<Priya>
-- Create date: <28/02/2018>
-- Description:	<Fetching Custom Merge Fields For Emails >
-- =============================================

--exec [usp_GetCustomMergeFieldsForEmailTemplate] 2,72,'383143,',113567

GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetCustomMergeFieldsForEmailTemplate')
DROP PROCEDURE usp_GetCustomMergeFieldsForEmailTemplate
GO
CREATE PROCEDURE [dbo].[usp_GetCustomMergeFieldsForEmailTemplate]
	  @numModuleID NUMERIC,
	  @numDomainID NUMERIC,
	  @vcRecordIDs VARCHAR(8000) = '',
	  @numOppId NUMERIC

AS

BEGIN
					
	IF @numModuleID = 45 OR @numModuleID = 1
	BEGIN
	
		DECLARE @numDivisionId VARCHAR(250)
		SET @numDivisionId = ''

		SELECT 
			@numDivisionId = @numDivisionId + cast(DivisionMaster.numDivisionID AS VARCHAR(10)) + ', ' 
		FROM 
			DivisionMaster
		INNER JOIN 
			AdditionalContactsInformation 
		ON 
			DivisionMaster.numDivisionID = AdditionalContactsInformation.numDivisionId
		WHERE 
			AdditionalContactsInformation.numContactId  IN (SELECT Id FROM dbo.SplitIDs(@vcRecordIDs, ',') ) 	
	
		SELECT 
			CFW_Fld_Values.RecId
			,CFW_Fld_Master.Fld_ID,CFW_Fld_Master.Fld_label
			,'#' + REPLACE(CFW_Fld_Master.Fld_label,' ','' ) +'#'  AS CustomField_Name
			,(CASE 
			WHEN Grp_id = 1 THEN dbo.fn_GetCustFldStringValue(CFW_FLD_Values.Fld_ID, CFW_FLD_Values.RecId, CFW_FLD_Values.Fld_Value)
						
			WHEN Grp_id = 4 THEN dbo.fn_GetCustFldStringValue(CFW_FLD_Values_Cont.Fld_ID, CFW_FLD_Values_Cont.RecId, CFW_FLD_Values_Cont.Fld_Value)
			ELSE '' 
			END)  
			AS CustomField_Value 
		FROM 
			CFW_Fld_Master
		LEFT JOIN 
			CFW_Fld_Values 
		ON 
			CFW_Fld_Values.Fld_ID = CFW_Fld_Master.Fld_id 
		AND 
			CFW_Fld_Values.RecId IN (SELECT Id FROM dbo.SplitIDs(@numDivisionId, ','))	
		LEFT JOIN 
			CFW_FLD_Values_Cont
		ON 
			CFW_FLD_Values_Cont.Fld_ID = CFW_Fld_Master.Fld_id
		AND 
			CFW_FLD_Values_Cont.RecId IN (SELECT Id FROM dbo.SplitIDs(@vcRecordIDs, ',')) 	
		WHERE 
			(grp_id = 1 or grp_id =4) AND CFW_Fld_Master.numDomainID = @numDomainID
	END
	ELSE IF (@numModuleID = 2 or @numModuleID = 8)
	BEGIN
	
		SELECT  CFW_Fld_Values_Opp.RecId,
				CFW_Fld_Master.Fld_ID,CFW_Fld_Master.Fld_label,
				'#' + REPLACE(CFW_Fld_Master.Fld_label,' ','' ) +'#'  AS CustomField_Name,
				(CASE 
				WHEN (Grp_id = 2 or Grp_id = 6) THEN dbo.fn_GetCustFldStringValue(CFW_Fld_Values_Opp.Fld_ID, CFW_Fld_Values_Opp.RecId, CFW_Fld_Values_Opp.Fld_Value)
						
				--WHEN Grp_id = 4 THEN dbo.fn_GetCustFldStringValue(CFW_FLD_Values_Cont.Fld_ID, CFW_FLD_Values_Cont.RecId, CFW_FLD_Values_Cont.Fld_Value)
				ELSE '' 
				END)  
				AS CustomField_Value 
				FROM CFW_Fld_Master
				LEFT JOIN CFW_Fld_Values_Opp
				ON CFW_Fld_Values_Opp.Fld_ID = CFW_Fld_Master.Fld_id
				AND CFW_Fld_Values_Opp.RecId = @numOppId
				WHERE 
					(grp_id = 2 or grp_id = 6) AND CFW_Fld_Master.numDomainID = 72
	END


END

GO


