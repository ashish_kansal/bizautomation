SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EDIQueue_GetPendingRecords')
DROP PROCEDURE dbo.USP_EDIQueue_GetPendingRecords
GO
CREATE PROCEDURE [dbo].[USP_EDIQueue_GetPendingRecords]

AS 
BEGIN
	SELECT TOP 50
		ID
		,EDIQueue.numDomainID
		,EDIQueue.numOppID
		,EDIType
	FROM 
		EDIQueue
	INNER JOIN
		OpportunityMaster
	ON
		EDIQueue.numOppID = OpportunityMaster.numOppId
	WHERE
		ISNULL(bitExecuted,0) = 0
		AND 1 = (CASE 
					WHEN EDIType=940 
					THEN (CASE WHEN ISNULL(tintOppType,0)=1 AND ISNULL(tintOppStatus,0)=1 THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
END
GO