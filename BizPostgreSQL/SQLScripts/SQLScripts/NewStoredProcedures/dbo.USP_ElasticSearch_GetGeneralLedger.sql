GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetGeneralLedger')
DROP PROCEDURE dbo.USP_ElasticSearch_GetGeneralLedger
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetGeneralLedger]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @dtStartDate DATETIME
	DECLARE @dtEndDate DATETIME

	SELECT @dtStartDate=dtPeriodFrom,@dtEndDate=dtPeriodTo FROM FinancialYear WHERE numDomainID=@numDomainID AND ISNULL(bitCurrentYear,0) = 1

	IF Not @dtStartDate IS NULL AND  Not @dtEndDate IS NULL
	BEGIN
		SELECT 
			*,
			CONCAT('<b style="color:#7f7f7f">G/L Transaction (',Search_TransactionType,'):</b>',dbo.FormatedDateFromDate(SearchDate_EntryDate,@numDomainID),', ',Search_CompanyName,', ',Search_Memo,', ',Search_CompanyName,', ',SearchNumber_Amount) AS displaytext
		FROM
		(
			SELECT
				GJD.numJournalId as id,
				'generalledger' as module,
				CONCAT('/Accounting/frmNewJournalEntry.aspx?frm=GeneralLedger&JournalId=',GJD.numJournalId) AS url,
				GJD.numTransactionId AS [text],
				GJH.datEntry_Date AS SearchDate_EntryDate,
				ISNULL(CI.vcCompanyName,'') AS Search_CompanyName,
				(CASE WHEN ISNULL(GJD.numCreditAmt,0)=0 THEN GJD.numDebitAmt ELSE GJD.numCreditAmt END) as SearchNumber_Amount,        
				ISNULL(GJD.varDescription,'') AS Search_Memo,
				(CASE WHEN RIGHT(GJH.varDescription,1) = ':' THEN SUBSTRING(GJH.varDescription,0,LEN(GJH.varDescription)) ELSE ISNULL(GJH.varDescription,'') END)  AS Search_Description,
				CASE 
					WHEN isnull(GJH.numCheckHeaderID, 0) <> 0 THEN + 'Check' 
					WHEN isnull(GJH.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
					WHEN isnull(GJH.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Received Payment' 
					WHEN isnull(GJH.numOppId, 0) <> 0 AND isnull(GJH.numOppBizDocsId, 0) <> 0 AND isnull(GJH.numBizDocsPaymentDetId, 0) = 0 AND dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
					WHEN isnull(GJH.numOppId, 0) <> 0 AND isnull(GJH.numOppBizDocsId, 0) <> 0 AND isnull(GJH.numBizDocsPaymentDetId, 0) = 0 AND dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
					WHEN isnull(GJH.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
					WHEN ISNULL(GJH.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
					WHEN isnull(GJH.numBillID, 0) <> 0 THEN 'Bill'
					WHEN isnull(GJH.numBillPaymentID, 0) <> 0 THEN 'Bill Payment'
					WHEN isnull(GJH.numReturnID, 0) <> 0 THEN 
						CASE 
							WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
							WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
							WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
							WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
							WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
						END 
					WHEN isnull(GJH.numOppId, 0) <> 0 AND isnull(GJH.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
					WHEN GJH.numJournal_Id <> 0 THEN 'Journal' 
				END AS Search_TransactionType,
				ISNULL(CAST(CH.numCheckNo AS VARCHAR),'') Search_CheckNo
			FROM   
				General_Journal_Header GJH 
			INNER JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN Chart_Of_Accounts ON GJD.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId 
			LEFT OUTER JOIN TimeAndExpense ON GJH.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
			LEFT OUTER JOIN DivisionMaster ON GJD.numCustomerId = dbo.DivisionMaster.numDivisionID and GJD.numDomainID = dbo.DivisionMaster.numDomainID
			LEFT OUTER JOIN CompanyInfo CI ON dbo.DivisionMaster.numCompanyID = CI.numCompanyId 
			LEFT OUTER JOIN OpportunityMaster ON GJH.numOppId = dbo.OpportunityMaster.numOppId 
			LEFT OUTER JOIN OpportunityBizDocs ON GJH.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId 
			LEFT OUTER JOIN VIEW_BIZPAYMENT ON GJH.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId 
			LEFT OUTER JOIN dbo.DepositMaster DM ON DM.numDepositId = GJH.numDepositId 
			LEFT OUTER JOIN dbo.ReturnHeader RH ON RH.numReturnHeaderID = GJH.numReturnID 
			LEFT OUTER JOIN dbo.CheckHeader CH ON GJH.numCheckHeaderID = CH.numCheckHeaderID
			LEFT OUTER JOIN BillHeader BH  ON ISNULL(GJH.numBillId,0) = BH.numBillId
			WHERE  
				   GJH.[datEntry_Date] >= @dtStartDate
				   AND GJH.[datEntry_Date] <= @dtEndDate
				   AND GJD.numDomainId = @numDomainId
				   AND GJD.numChartAcntId IN (SELECT COA.numAccountId FROM Chart_Of_Accounts COA WHERE numDomainID=@numDomainID AND numParntAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID  AND vcAccountCode='01010101'))
		) TMEP
		ORDER BY 
			SearchDate_EntryDate
	END
END



			