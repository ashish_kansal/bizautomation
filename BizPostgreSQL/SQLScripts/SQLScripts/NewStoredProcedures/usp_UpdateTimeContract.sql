/****** Object:  StoredProcedure [dbo].[USP_CaseDetails]    Script Date: 07/26/2008 16:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON                     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_UpdateTimeContract')
DROP PROCEDURE usp_UpdateTimeContract
GO
CREATE PROCEDURE [dbo].[usp_UpdateTimeContract]  
@numDomainID as numeric(9),
@numDivisonId as numeric(9) ,    
@numSecounds as numeric(9) ,  
@intType AS INT =0 ,
@numTaskID AS numeric(9),
@outPut AS INT  = 0 OUTPUT,
@balanceContractTime AS BIGINT=0  OUTPUT,
@isProjectActivityEmail AS INT
AS
BEGIN
	IF EXISTS(SELECT * FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1)
	BEGIN
		
		--@intType = 1
		IF(@intType=1)
		BEGIN
			SET @outPut=3
				IF(@isProjectActivityEmail=1)
				BEGIN
					UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract=0 WHERE numTaskId=@numTaskID
				END
				IF(@isProjectActivityEmail=2)
				BEGIN
					UPDATE Communication SET bitTimeAddedToContract=0 WHERE numCommId=@numTaskID
				END
				
				IF(@isProjectActivityEmail=3)
				BEGIN
					UPDATE Activity SET bitTimeAddedToContract=0 WHERE ActivityID=@numTaskID
				END

				IF @isProjectActivityEmail <> 1 --PROJECTS TAsKS TIME IS MANAGED THROUGH TRIGGER ON StagePercentageDetailsTaskTimeLog TABLE
				BEGIN
					UPDATE Contracts SET timeUsed=timeUsed-@numSecounds,dtmModifiedOn=GETUTCDATE() WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID
					DELETE FROM ContractsLog WHERE numDivisionID=@numDivisonId  AND numReferenceId=@numTaskID
					AND numContractId=(SELECT TOP 1 numContractId FRoM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID)
				END
		END
		--@intType = 2
		IF(@intType=2)
		BEGIN			
			IF @isProjectActivityEmail <> 1 AND ((SELECT ISNULL(timeLeft,0) FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1)<@numSecounds)
			BEGIN
				SET @outPut=1 --Time is not available
			END
			ELSE IF @isProjectActivityEmail = 1 AND ((SELECT ISNULL(timeLeft,0) FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1) <= 0)
			BEGIN
				SET @outPut=1 --Time is not available
			END
			ELSE
			BEGIN
				SET @outPut=2 --Time is available
				IF(@isProjectActivityEmail=1)
				BEGIN
					UPDATE StagePercentageDetailsTask SET bitTimeAddedToContract=1 WHERE numTaskId=@numTaskID
				END
				IF(@isProjectActivityEmail=2)
				BEGIN
					UPDATE Communication SET bitTimeAddedToContract=1 WHERE numCommId=@numTaskID
				END
				IF(@isProjectActivityEmail=3)
				BEGIN
					UPDATE Activity SET bitTimeAddedToContract=1 WHERE ActivityID=@numTaskID
				END

				IF @isProjectActivityEmail <> 1 --PROJECTS TAsKS TIME IS MANAGED THROUGH TRIGGER ON StagePercentageDetailsTaskTimeLog TABLE
				BEGIN
					UPDATE Contracts SET timeUsed=timeUsed+@numSecounds,dtmModifiedOn=GETUTCDATE() WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID
					SET @balanceContractTime = (SELECT ISNULL((ISNULL(timeLeft,0)-ISNULL(timeUsed,0)),0) FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1)
					INSERT INTO ContractsLog (
						intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId,tintRecordType
					)
					SELECT TOP 1
						1,@numDivisonId,@numTaskID,GETUTCDATE(),0,@isProjectActivityEmail,@numSecounds,@balanceContractTime,numContractId,@isProjectActivityEmail
					FROM Contracts WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1 ORDER BY numContractId DESC
				END
			END
		END
	END
	ELSE
	BEGIN
		SET @outPut=0
	END

	SET @balanceContractTime = (SELECT TOP 1 ((ISNULL(C.numHours,0) * 60 * 60) + (ISNULL(C.numMinutes,0) * 60)) - ISNULL((SELECT SUM(CLInner.numUsedTime) FROM ContractsLog CLInner WHERE CLInner.numContractId=C.numContractId),0) FROM Contracts C WHERE numDivisonId=@numDivisonId AND numDomainId=@numDomainID AND intType=1 ORDER BY numContractId DESC)
END