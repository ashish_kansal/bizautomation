SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetCommissionRuleContacts')
DROP PROCEDURE usp_GetCommissionRuleContacts
GO
CREATE PROCEDURE [dbo].[usp_GetCommissionRuleContacts]
@numComRuleID AS NUMERIC
AS
BEGIN
	DECLARE @tintAssignTo AS TINYINT

	SELECT @tintAssignTo=tintAssignTo FROM CommissionRules WHERE numComRuleID=@numComRuleID

	IF @tintAssignTo = 3
	BEGIN
		SELECT 
			CONCAT(CC.numValue,'~',CC.bitCommContact,'~1') AS numContactID,
			CONCAT(D.vcPartnerCode,'-',C.vcCompanyName) as vcUserName
		FROM 
			CommissionRuleContacts CC 
		INNER JOIN
			DivisionMaster D
		ON
			CC.numValue = D.numDivisionID
		INNER JOIN
			CompanyInfo C
		ON
			D.numCompanyID =C.numCompanyId
		WHERE 
			CC.numComRuleID=@numComRuleID 
	END
	ELSE
	BEGIN
		SELECT 
			CONCAT(CC.numValue,'~',CC.bitCommContact,'~0') AS numContactID,
			CONCAT(dbo.fn_GetContactName(numValue),CASE CC.bitCommContact WHEN 1 THEN '(' + ISNULL((SELECT CompanyInfo.vcCompanyName FROM AdditionalContactsInformation ACI INNER JOIN DivisionMaster DM ON ACI.numDivisionId=DM.numDivisionID INNER JOIN CompanyInfo ON DM.numCompanyID = CompanyInfo.numCompanyId WHERE ACI.numContactId=CC.numValue),'') + ')' ELSE '' END) as vcUserName
		FROM 
			CommissionRuleContacts CC 
		WHERE 
			CC.numComRuleID=@numComRuleID 
	END
END	
GO
