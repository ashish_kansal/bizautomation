SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetVendorShipmentMethod')
DROP PROCEDURE USP_GetVendorShipmentMethod
GO
CREATE PROCEDURE [dbo].[USP_GetVendorShipmentMethod]         
@numDomainID as numeric(9)=0,    
@numVendorid AS NUMERIC(9)=0,
@numAddressID AS NUMERIC(9)=0,
@numWarehouseID AS NUMERIC(18,0) = 0,
@tintMode AS TINYINT        
AS    
BEGIN
	IF @tintMode=1
	BEGIN 
		IF EXISTS (SELECT numShipmentMethod FROM VendorShipmentMethod VM WHERE VM.numDomainID=@numDomainID AND VM.numVendorid=@numVendorid AND ISNULL(numWarehouseID,0) = @numWarehouseID)
		BEGIN
			SELECT 
				LD.numListItemID
				,LD.vcData AS ShipmentMethod
				,ISNULL(VM.numListValue,0) AS numListValue
				,VM.bitPrimary
				,ISNULL(VM.bitPreferredMethod,0) bitPreferredMethod
			FROM 
				ListDetails LD 
			LEFT JOIN 
				VendorShipmentMethod VM 
			ON 
				VM.numDomainID=@numDomainID
				AND VM.numVendorid=@numVendorid
				AND VM.numAddressID=@numAddressID 
				AND VM.numListItemID=LD.numListItemID
				AND VM.numWarehouseID=@numWarehouseID
			WHERE 
				LD.numListID=338 
				AND (LD.numDomainID=@numDomainID OR Ld.constFlag=1)
		END
		ELSE
		BEGIN
			SELECT 
				LD.numListItemID
				,LD.vcData AS ShipmentMethod
				,ISNULL(VM.numListValue,0) AS numListValue
				,VM.bitPrimary
				,ISNULL(VM.bitPreferredMethod,0) bitPreferredMethod
			FROM 
				ListDetails LD 
			LEFT JOIN 
				VendorShipmentMethod VM 
			ON 
				VM.numDomainID=@numDomainID
				AND VM.numVendorid=@numVendorid
				AND VM.numAddressID=@numAddressID 
				AND VM.numListItemID=LD.numListItemID
				AND ISNULL(VM.numWarehouseID,0)=0
			WHERE 
				LD.numListID=338 
				AND (LD.numDomainID=@numDomainID OR Ld.constFlag=1)
		END
	END
	ELSE IF @tintMode=2
	BEGIN 
		IF ISNULL(@numAddressID,0) = -1
		BEGIN
			SET @numAddressID = ISNULL((SELECT TOP 1 
											AD.numAddressID 
										FROM 
											AddressDetails AD 
										WHERE 
											AD.numDomainID=@numDomainID 
											AND AD.numRecordID=@numVendorid 
											AND AD.tintAddressOf=2 
											AND AD.tintAddressType=2 
										ORDER BY ISNULL(AD.bitIsPrimary,0) DESC),0)
		END

		SELECT LD.numListItemID,LD.vcData AS ShipmentMethod,ISNULL(VM.numListValue,0) AS numListValue,VM.bitPrimary,
		cast(LD.numListItemID AS VARCHAR(10)) + '~' + cast(ISNULL(VM.numListValue,0) AS VARCHAR(10)) AS vcListValue
			from ListDetails LD JOIN VendorShipmentMethod VM ON VM.numListItemID=LD.numListItemID 
		AND VM.numVendorid=@numVendorid AND VM.numAddressID=@numAddressID AND VM.numDomainID=@numDomainID
		WHERE LD.numListID=338 AND (LD.numDomainID=@numDomainID or Ld.constFlag=1) ORDER BY ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC
	END
END
GO
