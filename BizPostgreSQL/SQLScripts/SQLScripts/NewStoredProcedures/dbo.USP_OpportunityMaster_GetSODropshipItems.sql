GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetSODropshipItems')
DROP PROCEDURE USP_OpportunityMaster_GetSODropshipItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetSODropshipItems]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID)
	BEGIN
		RAISERROR('INVALID_OPPID',16,1)
		RETURN
	END 

	DECLARE @numDivisionID AS NUMERIC(18,0)
	DECLARE @tintDefaultCost AS NUMERIC(18,0)

	SELECT @tintDefaultCost = ISNULL(numCost,0) FROM Domain WHERE numDomainId = @numDomainID
	SELECT @numDivisionID=numDivisionId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID

	SELECT
		OI.numItemCode
		,CONCAT(I.vcItemName, CASE WHEN dbo.fn_GetAttributes(OI.numWarehouseItmsID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(OI.numWarehouseItmsID,0),')') ELSE '' END) AS vcItemName
		,I.vcModelID
		,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))  ELSE ISNULL(I.vcSKU,'') END) vcSKU
		,OI.vcNotes
		,I.charItemType
		,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=I.numItemCode AND bitDefault =1), '') AS vcPathForTImage
		,ISNULL(OI.numWarehouseItmsID,0) AS numWarehouseItemID
		,OI.numUnitHour
		,ISNULL(OI.numUOMId,0) AS numUOMID
		,ISNULL(U.vcUnitName, '-') vcUOMName
		,dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId,I.numBaseUnit) AS fltUOMConversionFactor 
		,ISNULL(I.numVendorID, 0) AS numVendorID
		,CASE WHEN @tintDefaultCost = 3 THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) ELSE ISNULL(I.monAverageCost,0) END AS monCost
		,ISNULL(dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
		,ISNULL(V.intMinQty, 0) AS intMinQty
		,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
	FROM    
		dbo.OpportunityItems OI
		INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
		INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
		LEFT JOIN WarehouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
		LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
		LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
	WHERE   
		OI.numOppId = @numOppID
		AND OM.numDomainId = @numDomainId
		AND OM.tintOppType = 1
		AND OM.tintOppStatus=1
		AND ISNULL(OI.bitDropShip, 0) = 1
		AND 1 = (CASE WHEN (SELECT COUNT(*) FROM OpportunityLinking OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numChildOppID=OIInner.numOppId WHERE OMInner.numParentOppID = @numOppID AND OIInner.numItemCode=OI.numItemCode AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)) > 0 THEN 0 ELSE 1 END)


	EXEC usp_getoppaddress @numOppID,1,0

	SELECT dbo.fn_getOPPAddress(@numOppID,@numDomainId,2) AS ShippingAdderss
END
