GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteContracts')
DROP PROCEDURE USP_DeleteContracts
GO
CREATE PROCEDURE [dbo].[USP_DeleteContracts]    
@numDomainID NUMERIC(18,0),
@vcContractId VARCHAR(200)=''
AS
BEGIN
	DELETE FROM Contracts WHERE numContractId IN(SELECT Items FROM Split(@vcContractId,',') WHERE Items<>'') AND numDomainId=@numDomainID
END