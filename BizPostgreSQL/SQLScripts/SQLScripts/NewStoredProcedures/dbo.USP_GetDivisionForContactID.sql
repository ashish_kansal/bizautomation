GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDivisionForContactID')
DROP PROCEDURE USP_GetDivisionForContactID
GO
CREATE PROCEDURE [dbo].[USP_GetDivisionForContactID]      
@numContactId as numeric(9)      
as
BEGIN
	SELECT 
		numDivisionId
	FROM 
		AdditionalContactsInformation 
	WHERE 
		numContactId=@numContactId
END
GO
