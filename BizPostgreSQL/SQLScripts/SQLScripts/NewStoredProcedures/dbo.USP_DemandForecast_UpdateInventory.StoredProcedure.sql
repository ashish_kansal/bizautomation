SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_UpdateInventory')
DROP PROCEDURE dbo.USP_DemandForecast_UpdateInventory
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_UpdateInventory]
	@numOppID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0)
AS 
BEGIN

	-- Update Inventory when order is created from demand forecast
	exec USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
	
END
