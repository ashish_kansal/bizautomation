
/****** Object:  StoredProcedure [dbo].[USP_InsertCartItem]    Script Date: 11/08/2011 17:53:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InsertCartItem' ) 
                    DROP PROCEDURE USP_InsertCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_InsertCartItem]
(
    @numUserCntId NUMERIC(18, 0),
    @numDomainId NUMERIC(18, 0),
    @vcCookieId VARCHAR(100),
    @numOppItemCode NUMERIC(18, 0),
    @numItemCode NUMERIC(18, 0),
    @numUnitHour NUMERIC(18, 0),
    @monPrice DECIMAL(20,5),
    @numSourceId NUMERIC(18, 0),
    @vcItemDesc VARCHAR(2000),
    @numWarehouseId NUMERIC(18, 0),
    @vcItemName VARCHAR(200),
    @vcWarehouse VARCHAR(200),
    @numWarehouseItmsID NUMERIC(18, 0),
    @vcItemType VARCHAR(200),
    @vcAttributes VARCHAR(100),
    @vcAttrValues VARCHAR(100),
    @bitFreeShipping BIT,
    @numWeight NUMERIC(18, 2),
    @tintOpFlag TINYINT,
    @bitDiscountType BIT,
    @fltDiscount DECIMAL(18,2),
    @monTotAmtBefDiscount DECIMAL(20,5),
    @ItemURL VARCHAR(200),
    @numUOM NUMERIC(18, 0),
    @vcUOMName VARCHAR(200),
    @decUOMConversionFactor DECIMAL(18, 5),
    @numHeight NUMERIC(18, 0),
    @numLength NUMERIC(18, 0),
    @numWidth NUMERIC(18, 0),
    @vcShippingMethod VARCHAR(200),
    @numServiceTypeId NUMERIC(18, 0),
    @decShippingCharge DECIMAL(18, 2),
    @numShippingCompany NUMERIC(18, 0),
    @tintServicetype TINYINT,
    @dtDeliveryDate DATETIME,
    @monTotAmount DECIMAL(20,5),
	@numSiteID NUMERIC(18,0) = 0,
	@numParentItemCode NUMERIC(18,0) = 0,
	@vcChildKitItemSelection VARCHAR(MAX) = '',
	@vcPreferredPromotions VARCHAR(MAX)=''
)
AS 
BEGIN TRY
BEGIN TRANSACTION;
	DECLARE @numPreferredPromotionID NUMERIC(18,0)
	DECLARE @hDocItem INT
	DECLARE @TablePreferredPromotion TABLE
	(
		numItemCode NUMERIC(18,0)
		,numPromotionID NUMERIC(18,0)
	)

	IF ISNULL(@vcPreferredPromotions,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcPreferredPromotions

		INSERT INTO @TablePreferredPromotion
		(
			numItemCode
			,numPromotionID
		)
		SELECT
			numItemCode
			,numPromotionID
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numItemCode NUMERIC(18,0)
			,numPromotionID NUMERIC(18,0)
		)

		EXEC sp_xml_removedocument @hDocItem

		SET @numPreferredPromotionID = ISNULL((SELECT TOP 1 numPromotionID  FROM @TablePreferredPromotion WHERE numItemCode=@numItemCode),0)
	END
	
	INSERT INTO [dbo].[CartItems]
    (
        numUserCntId,
        numDomainId,
        vcCookieId,
        numOppItemCode,
        numItemCode,
        numUnitHour,
        monPrice,
        numSourceId,
        vcItemDesc,
        numWarehouseId,
        vcItemName,
        vcWarehouse,
        numWarehouseItmsID,
        vcItemType,
        vcAttributes,
        vcAttrValues,
        bitFreeShipping,
        numWeight,
        tintOpFlag,
        bitDiscountType,
        fltDiscount,
        monTotAmtBefDiscount,
        ItemURL,
        numUOM,
        vcUOMName,
        decUOMConversionFactor,
        numHeight,
        numLength,
        numWidth,
        vcShippingMethod,
        numServiceTypeId,
        decShippingCharge,
        numShippingCompany,
        tintServicetype,
        monTotAmount,
		numParentItem
		,vcChildKitItemSelection
		,monInsertPrice
		,fltInsertDiscount
		,bitInsertDiscountType
		,numPreferredPromotionID
    )
	VALUES  
	(
        @numUserCntId,
        @numDomainId,
        @vcCookieId,
        @numOppItemCode,
        @numItemCode,
        @numUnitHour,
        @monPrice,
        @numSourceId,
        @vcItemDesc,
        @numWarehouseId,
        @vcItemName,
        @vcWarehouse,
        @numWarehouseItmsID,
        @vcItemType,
        @vcAttributes,
        @vcAttrValues,
        @bitFreeShipping,
        @numWeight,
        @tintOpFlag,
        @bitDiscountType,
        @fltDiscount,
        @monTotAmtBefDiscount,
        @ItemURL,
        @numUOM,
        @vcUOMName,
        @decUOMConversionFactor,
        @numHeight,
        @numLength,
        @numWidth,
        @vcShippingMethod,
        @numServiceTypeId,
        @decShippingCharge,
        @numShippingCompany,
        @tintServicetype,
        @monTotAmount,
		@numParentItemCode,
		@vcChildKitItemSelection,
		@monPrice,
		@fltDiscount,
		@bitDiscountType,
		@numPreferredPromotionID
    )

	DECLARE @numCartId NUMERIC(18,0)
	SET @numCartId = SCOPE_IDENTITY()

	IF (SELECT COUNT(*) FROM PromotionOffer WHERE numDomainId=@numDomainId AND ISNULL(bitEnabled,0)=1) > 0
	BEGIN
		EXEC USP_PromotionOffer_ApplyItemPromotionToECommerce @numDomainID,@numUserCntId,@numSiteID,@vcCookieId,0,''
	END

	SELECT @numCartId AS numCartID

 COMMIT TRANSACTION;
  END TRY
  BEGIN CATCH
    IF @@TRANCOUNT > 0
    ROLLBACK TRANSACTION;

    DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
  END CATCH
            