GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_saveDycFieldMaster')
DROP PROCEDURE USP_saveDycFieldMaster
GO
CREATE PROCEDURE [dbo].[USP_saveDycFieldMaster]    
@numDomainID as NUMERIC(9)=0,    
@numModuleId as NUMERIC(9)=0,  
@numFieldId as NUMERIC(9)=0,  
@vcFieldName as NVARCHAR(50)=0,
@vcToolTip AS VARCHAR(1000),
@numCultureID as numeric(9)=0
 
as    
	
   	if exists(select * from DycField_Globalization where numDomainID=@numDomainID and numModuleId=@numModuleId and numFieldId=@numFieldId and numCultureID=@numCultureID)
	   begin
          UPDATE DycField_Globalization SET vcNewFieldName=@vcFieldName,vcToolTip=@vcToolTip 
				where numDomainID=@numDomainID and numModuleId=@numModuleId and numFieldId=@numFieldId and numCultureID=@numCultureID
        END    
    ELSE
    begin
        insert into DycField_Globalization (numModuleId,numFieldId,numDomainId,vcNewFieldName,vcToolTip,numCultureID)
			 	values(@numModuleId,@numFieldId,@numDomainId,@vcFieldName,@vcToolTip,@numCultureID)
    END
    
GO
