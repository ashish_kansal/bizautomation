SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetVendorPriceHistory')
DROP PROCEDURE USP_Item_GetVendorPriceHistory
GO
CREATE PROCEDURE [dbo].[USP_Item_GetVendorPriceHistory]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numVendorID NUMERIC(18,0)
	,@ClientTimeZoneOffset AS INT
)
AS
BEGIN
	SELECT TOP 100
		dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate),@numDomainID) vcCreatedDate
		,ISNULL(OM.vcPOppName,0) vcPOppName
		,dbo.fn_UOMConversion(OI.numUOMId,@numItemCode,@numItemCode,I.numBaseUnit) numUnitHour
		,dbo.fn_GetUOMName(numUOMId) AS vcUOMName
		,CAST(ISNULL(OI.monPrice,0) AS DECIMAL(20,5)) AS monPrice
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.numDivisionId = @numVendorID
		AND OI.numItemCode = @numItemCode
		AND tintOppType = 2
		AND tintOppStatus = 1
		AND ISNULL(bitStockTransfer,0) = 0
	ORDER BY
		OM.numOppId DESC
END