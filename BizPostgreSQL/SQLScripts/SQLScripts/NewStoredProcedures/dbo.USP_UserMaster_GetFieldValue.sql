GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_GetFieldValue')
DROP PROCEDURE USP_UserMaster_GetFieldValue
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_GetFieldValue]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcFieldName VARCHAR(200)
)
AS
BEGIN
	IF @vcFieldName = 'tintPayrollType'
	BEGIN
		SELECT ISNULL(tintPayrollType,1) FROM UserMaster	WHERE numDomainID = @numDomainID AND numUserDetailId = @numUserCntID
	END
END
GO