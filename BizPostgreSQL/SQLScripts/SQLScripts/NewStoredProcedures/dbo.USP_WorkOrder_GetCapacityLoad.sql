GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetCapacityLoad')
DROP PROCEDURE USP_WorkOrder_GetCapacityLoad
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetCapacityLoad]                             
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT                  
AS                            
BEGIN
	SELECT 
		dbo.GetCapacityLoad(@numDomainID,0,0,WO.numWOID,0,4,ISNULL(WO.dtmStartDate,WO.bintCreatedDate),@ClientTimeZoneOffset)
	FROM
		WorkOrder WO
	WHERE
		WO.numDomainID=@numDomainID
		AND WO.numWOId = @numWOID
END
GO