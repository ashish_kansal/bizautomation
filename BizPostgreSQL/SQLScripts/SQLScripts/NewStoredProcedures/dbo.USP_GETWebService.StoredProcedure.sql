
GO
/****** Object:  StoredProcedure [dbo].[USP_GETWebService]    Script Date: 06/01/2009 23:45:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GETWebService')
DROP PROCEDURE USP_GETWebService
GO
CREATE PROCEDURE [dbo].[USP_GETWebService]
@vcToken varchar(500)
AS

SELECT [numWebServiceId], 
	[vcToken], 
	[numDomainID]
FROM WebService
WHERE 
 [vcToken] = @vcToken
