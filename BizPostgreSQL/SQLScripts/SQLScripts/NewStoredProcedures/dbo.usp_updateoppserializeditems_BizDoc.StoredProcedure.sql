/****** Object:  StoredProcedure [dbo].[usp_updateoppserializeditems_BizDoc]    Script Date: 07/26/2008 16:21:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppserializeditems_BizDoc')
DROP PROCEDURE usp_updateoppserializeditems_BizDoc
GO
CREATE PROCEDURE [dbo].[usp_updateoppserializeditems_BizDoc]  
@numOppItemTcode as numeric(9)=0,  
@numOppID as numeric(9)=0,  
@strItems as text='',
@OppType as tinyint,
@numBizDocID as numeric(9)=0
 
as  
-- STORE PROCEDURE LOGIC IS COMMENTED BECAUSE IT IS NOT USED NOW AND CODE IS NOT RELEVANT TO BIZ NOW.
--declare @hDoc as int                
--EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    


--update WareHouseItmsDTL set tintStatus=0 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode AND numOppBizDocsId=@numBizDocID)

--delete from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode AND numOppBizDocsId=@numBizDocID

-- Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty,numOppBizDocsId)                
--  (SELECT numWareHouseItmsDTLID,@numOppID,@numOppItemTcode,numWareHouseItemID,UsedQty,@numBizDocID                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),UsedQty NUMERIC(9)))
      
--update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in 
--(SELECT numWareHouseItmsDTLID                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),UsedQty NUMERIC(9),TotalQty NUMERIC(9)) WHERE (TotalQty-UsedQty)=0)


  
--EXEC sp_xml_removedocument @hDoc
GO
