GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSurveyTemplate')
DROP PROCEDURE USP_GetSurveyTemplate
GO
CREATE PROCEDURE [dbo].[USP_GetSurveyTemplate]
	@numDomainID NUMERIC,
	@numSurId NUMERIC,
	@numPageType TINYINT
AS
SET NOCOUNT ON


SELECT [numDomainID], 
	[numSurId], 
	[txtTemplate], 
	[numPageType]
FROM SurveyTemplate
WHERE [numDomainID] = @numDomainID
AND [numSurId] = @numSurId
AND [numPageType] = @numPageType


 SELECT S.[numCssID],s.StyleFileName
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numSurTemplateId] = (SELECT numSurTemplateId FROM SurveyTemplate
 WHERE [numDomainID] = @numDomainID AND [numSurId] = @numSurId AND [numPageType] = @numPageType)
         AND [tintType] = 3
         
 SELECT S.[numCssID],s.StyleFileName
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numSurTemplateId] = (SELECT numSurTemplateId FROM SurveyTemplate
 WHERE [numDomainID] = @numDomainID AND [numSurId] = @numSurId AND [numPageType] = @numPageType)
         AND [tintType] = 4