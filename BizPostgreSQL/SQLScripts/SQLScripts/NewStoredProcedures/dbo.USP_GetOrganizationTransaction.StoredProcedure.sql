GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrganizationTransaction')
DROP PROCEDURE USP_GetOrganizationTransaction
GO
CREATE PROCEDURE [dbo].[USP_GetOrganizationTransaction]
    (
      @numDivisionID NUMERIC(9)=0,
      @numDomainID NUMERIC,
      @CurrentPage INT=0,
	  @PageSize INT=0,
      @TotRecs INT=0  OUTPUT,
      @vcTransactionType AS VARCHAR(500),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @ClientTimeZoneOffset INT,
	  @SortCreatedDate TINYINT
    )
AS 
    BEGIN
		
		DECLARE @TEMP TABLE
		(
			Items INT
		)

		INSERT INTO @TEMP SELECT Items FROM dbo.Split(@vcTransactionType,',')

		DECLARE @TempTransaction TABLE(intTransactionType INT,vcTransactionType VARCHAR(30),dtCreatedDate DATETIME,numRecordID NUMERIC,
		vcRecordName VARCHAR(100),monTotalAmount DECIMAL(20,5),monPaidAmount DECIMAL(20,5),dtDueDate DATETIME,vcMemo VARCHAR(1000),vcDescription VARCHAR(500),
		vcStatus VARCHAR(100),numRecordID2 NUMERIC,numStatus NUMERIC, vcBizDocId VARCHAR(1000))
		 
		 --Sales Order
		 IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=1)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 1,'Sales Order',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,
		 	OM.monDealAmount,
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Order Invoices(BizDocs)
		 IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=2)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 2,'SO Invoice',OBD.dtCreatedDate,OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,OBD.monDealAmount,
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
        END
		 
		 --Sales Opportunity
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=3)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 3,'Sales Opportunity',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,OM.monDealAmount,
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=4)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 4,'Purchase Order',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,
		 	OM.monDealAmount,
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order Bill(BizDocs)
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=5)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 5,'PO Bill',OBD.dtCreatedDate,OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,OBD.monDealAmount,
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Opportunity
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=6)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 6,'Purchase Opportunity',OM.bintCreatedDate,OM.numOppId,
		 	OM.vcPOppName,OM.monDealAmount,
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Return
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=7)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 7,'Sales Return',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	CASE ISNULL(RH.tintReceiveType,0) WHEN 1 THEN ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0)
		 		 	WHEN 2 THEN ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0)
		 		 	ELSE 0 END,
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Return
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=8)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 8,'Purchase Return',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM BillPaymentHeader BPH WHERE BPH.numDomainID=RH.numDomainID AND BPH.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 2
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Credit Memo
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=9)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 9,'Credit Memo',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 3
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Refund Receipt
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=10)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 10,'Refund Receipt',RH.dtCreatedDate,RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 4
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Bills
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=11)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 11,'Bills',BH.dtCreatedDate,BH.numBillID,
		 	'Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 0

		 END
		 
		  --Bills Landed Cost
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=16)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 16,'Landed Cost',BH.dtCreatedDate,BH.[numOppId],
		 	'Landed Cost Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 1
		 END

		 --UnApplied Bill Payments
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=12)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 12,'Unapplied Bill Payments',BPH.dtCreateDate,BPH.numBillPaymentID,
		 	CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Return Credit #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Bill Payment' END
			,ISNULL(BPH.monPaymentAmount,0),
		 	ISNULL(BPH.monAppliedAmount,0),
		 	NULL,'','','',ISNULL(numReturnHeaderID,0),0,''
		 	 FROM BillPaymentHeader BPH where BPH.numDomainID = @numDomainID AND BPH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BPH.dtCreateDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		 END
		 
		 --Checks
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=13)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 13,'Checks',CH.dtCreatedDate,CH.numCheckHeaderID,
		 	CASE CH.tintReferenceType WHEN 1 THEN 'Checks' 
				  WHEN 8 THEN 'Bill Payment'
				  WHEN 10 THEN 'RMA'
				  WHEN 11 THEN 'Payroll'
				  ELSE 'Check' END + CASE WHEN ISNULL(CH.numCheckNo,0) > 0 THEN ' - #' + CAST(CH.numCheckNo AS VARCHAR(18)) ELSE '' END 
			,ISNULL(CH.monAmount,0),
		 	CASE WHEN ISNULL(bitIsPrint,0)=1 THEN ISNULL(CH.monAmount,0) ELSE 0 END,
		 	NULL,CH.vcMemo,'','',0,0,''
		 	 FROM dbo.CheckHeader CH where CH.numDomainID = @numDomainID AND CH.numDivisionId=@numDivisionId
		 				--AND ch.tintReferenceType=1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,CH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Deposits
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=14)
		 BEGIN
		 	INSERT INTO @TempTransaction 
		 	SELECT 14,'Deposits',DM.dtCreationDate,DM.numDepositId,
		 	'Deposit',
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',0,tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND tintDepositePage IN (1,2)
		 END
		 
		 --Unapplied Payments
		  IF EXISTS (SELECT 1 FROM @TEMP WHERE Items=15)
		 BEGIN
			INSERT INTO @TempTransaction 
		 	SELECT 15,'Unapplied Payments',DM.dtCreationDate,DM.numDepositId,
		 	CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Payment' END,
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',ISNULL(numReturnHeaderID,0),tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND tintDepositePage IN(2,3)
		 END
		 
		 DECLARE  @firstRec  AS INTEGER
		 DECLARE  @lastRec  AS INTEGER

         SET @firstRec = (@CurrentPage - 1) * @PageSize
		 SET @lastRec = (@CurrentPage * @PageSize + 1)
		 SET @TotRecs = (SELECT COUNT(*) FROM   @TempTransaction)
         
		 IF @SortCreatedDate = 1 -- Ascending 
		 BEGIN
			SELECT * FROM 
         (SELECT intTransactionType ,vcTransactionType, dtCreatedDate AS dtActualCreatedDate,dbo.FormatedDateFromDate(dtCreatedDate,@numDomainID) AS [dtCreatedDate],numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate ASC) AS RowNumber FROM @TempTransaction) a
          WHERE RowNumber > @firstRec and RowNumber < @lastRec order BY  intTransactionType, CAST(dtActualCreatedDate AS DATE) ASC
		 END
		 ELSE --By Default Descending
		 BEGIN
			 SELECT * FROM 
         (SELECT intTransactionType ,vcTransactionType, dtCreatedDate AS dtActualCreatedDate, dbo.FormatedDateFromDate(dtCreatedDate,@numDomainID) AS [dtCreatedDate],numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate DESC) AS RowNumber FROM @TempTransaction) a
          WHERE RowNumber > @firstRec and RowNumber < @lastRec order BY  intTransactionType, CAST(dtActualCreatedDate AS DATE) DESC 
         END
    END
