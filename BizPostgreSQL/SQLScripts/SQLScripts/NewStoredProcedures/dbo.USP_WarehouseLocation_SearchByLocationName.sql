GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseLocation_SearchByLocationName')
DROP PROCEDURE USP_WarehouseLocation_SearchByLocationName
GO

CREATE PROCEDURE [dbo].[USP_WarehouseLocation_SearchByLocationName] 
(
	@numDomainID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@vcSearchText VARCHAR(100)
	,@intOffset INT
	,@intPageSize INT
)
AS
BEGIN
	SELECT
		numWLocationID
		,vcLocation
		,numWLocationID AS [id]
		,ISNULL(vcLocation,'') AS [text]
		,COUNT(*) OVER() AS TotalRecords
	FROM
		WarehouseLocation
	WHERE
		numDomainID=@numDomainID
		AND numWarehouseID=@numWarehouseID
		AND (vcLocation LIKE '%' + @vcSearchText + '%' OR LEN(ISNULL(@vcSearchText,'')) = 0)
	ORDER BY
		vcLocation
	OFFSET
		@intOffset ROWS
	FETCH NEXT 
		@intPageSize ROWS ONLY
END