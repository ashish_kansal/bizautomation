
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSites]    Script Date: 08/08/2009 14:59:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSites')
DROP PROCEDURE USP_ManageSites
GO
CREATE PROCEDURE [dbo].[USP_ManageSites]
	@numSiteID NUMERIC(9) = 0 output,
	@vcSiteName varchar(50) = NULL,
	@vcDescription varchar(1000) = NULL,
	@numUserCntID NUMERIC(9)= NULL,
	@numDomainID NUMERIC(9) = NULL,
	@vcHostName VARCHAR(500)= NULL,
	@bitIsActive BIT,
	@vcLiveURL VARCHAR(100)= NULL,
	@numCurrencyID NUMERIC(9) = 0,
	@bitOnePageCheckout BIT =0 ,
	@tintRateType TINYINT = 0 ,
	@vcCategories VARCHAR(2000),
	@Mode INT = 0,
	@bitHtml5 BIT,
	@vcMetaTags TEXT,
	@bitSSLRedirect BIT
AS

IF LEN(@vcLiveURL)>1
BEGIN
	UPDATE Sites SET 
		vcLiveURL=@vcLiveURL
	WHERE [numSiteID] = @numSiteID
	
	RETURN
END


IF @numSiteID = 0 AND (NOT EXISTS(SELECT [vcSiteName] from Sites Where ([vcSiteName] = @vcSiteName OR vcHostName=@vcHostName) )) 
AND (NOT EXISTS(SELECT [vcPortalName] from dbo.Domain Where [vcPortalName] = @vcHostName))
BEGIN
	INSERT INTO Sites 
	(
		[vcSiteName],
		[vcDescription],
		vcHostName,
		[numCreatedBy],
		[dtCreateDate],
		[numModifiedBy],
		[dtModifiedDate],
		[numDomainID],
		[bitIsActive],numCurrencyID,bitOnePageCheckout,
		[tintRateType],
		[bitHtml5],
		[vcMetaTags],
		[bitSSLRedirect]
	)
	VALUES 
	(
		@vcSiteName,
		@vcDescription,
		@vcHostName,
		@numUserCntID,
		GETUTCDATE(),
		@numUserCntID,
		GETUTCDATE(),
		@numDomainID,
		@bitIsActive,@numCurrencyID,@bitOnePageCheckout,
		@tintRateType,
		@bitHtml5,
		@vcMetaTags,
		@bitSSLRedirect
	)
	
	SELECT @numSiteID = SCOPE_IDENTITY();
END
ELSE IF @numSiteID >0 AND (NOT EXISTS(SELECT [vcSiteName] from Sites Where ([vcSiteName] = @vcSiteName OR vcHostName=@vcHostName )AND numSiteID<> @numSiteID))
AND (NOT EXISTS(SELECT [vcPortalName] from dbo.Domain Where [vcPortalName] = @vcHostName))
BEGIN
  IF @Mode = 0
	 BEGIN
			UPDATE Sites SET 
			[vcSiteName] = @vcSiteName,
			[vcDescription] = @vcDescription,
			vcHostName		= @vcHostName,
			[numModifiedBy] = @numUserCntID,
			[dtModifiedDate] = GETUTCDATE(),
			[bitIsActive] = @bitIsActive,numCurrencyID=@numCurrencyID,
			bitOnePageCheckout=@bitOnePageCheckout,
			tintRateType = @tintRateType,
			bitHtml5 = @bitHtml5,
			vcMetaTags = @vcMetaTags,
			bitSSLRedirect=@bitSSLRedirect
			WHERE [numSiteID] = @numSiteID 	
	 END
ELSE IF @Mode = 1
IF	@numSiteID > 0
	BEGIN
          IF EXISTS(SELECT * FROM dbo.SiteCategories WHERE numSiteID = @numSiteID)
			 BEGIN
					DELETE FROM dbo.SiteCategories WHERE numSiteID = @numSiteID 			 	
			 END 

		INSERT INTO dbo.SiteCategories 
		(
			numSiteID,
			numCategoryID
		)  
		SELECT  @numSiteID AS numSiteID,  ID  from dbo.SplitIDs(@vcCategories,',')
	END
END
ELSE
	SET @numSiteID = 0;

