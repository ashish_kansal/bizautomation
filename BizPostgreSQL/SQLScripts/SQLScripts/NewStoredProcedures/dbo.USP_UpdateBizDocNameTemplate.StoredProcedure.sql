GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateBizDocNameTemplate')
DROP PROCEDURE USP_UpdateBizDocNameTemplate
GO
--SELECT * FROM dbo.ReturnHeader
--EXEC USP_UpdateBizDocNameTemplate  6,1,18
CREATE PROCEDURE USP_UpdateBizDocNameTemplate
@tintType TINYINT = 0,
@numDomainID NUMERIC,
@RecordID NUMERIC-- pass Orderid ,BizDocID,ProjectID, or ReturnHeaderID
AS 
BEGIN

	DECLARE @Tempate VARCHAR(200)
	DECLARE @OrgName AS VARCHAR(100)
	DECLARE @numBizDocId AS NUMERIC,@OrderID AS NUMERIC 
	DECLARE @numSequenceId NUMERIC(9),@numMinLength NUMERIC(9)
	
	IF ISNULL(@tintType,0) > 2
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			SET @numSequenceId=0
			
			IF @tintType =3 --Sales Credit Memo
				SET @numBizDocId = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND (vcData='Sales Credit Memo'))
			ELSE IF @tintType =4 --Purchase Credit Memo
				SET @numBizDocId = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND (vcData='Purchase Credit Memo'))
			ELSE IF @tintType =5 --Refund Receipt
				SET @numBizDocId = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND (vcData='Refund Receipt'))
			ELSE IF @tintType =6 --Credit Memo
				SET @numBizDocId = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND (vcData='Credit Memo'))
			ELSE IF @tintType =7 --Sales RMA (Mirror BizDoc)
				SET @numBizDocId = (SELECT numListItemID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND (vcData='RMA'))
				
			IF ISNULL(@numBizDocId,0) =0 
			BEGIN
				RAISERROR('BizDoc Type Not found!!',16,1);
				RETURN ;
			END
				
			IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
			BEGIN
				INSERT INTO NameTemplate(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
					SELECT @numDomainId,2,@numBizDocId,CASE WHEN @numBizDocId=290 THEN 'PROI' WHEN @tintType=3 THEN 'SCM' WHEN @tintType=4 THEN 'PCM' WHEN @tintType=5 THEN 'RFND' WHEN @tintType=6 THEN 'CM' WHEN @tintType=7 THEN 'RMA' ELSE UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) END + '-',1,4
			END
				

			SELECT 
				@Tempate = ISNULL(vcNameTemplate,'')
				,@numSequenceId=ISNULL(numSequenceId,1)
				,@numMinLength=ISNULL(numMinLength,4) 
			FROM 
				dbo.NameTemplate 
			WHERE 
				numDomainID=@numDomainID 
				AND numRecordID=@numBizDocId 
				AND tintModuleID=2			
			
			--Replace Other Values
			SET @Tempate = REPLACE(@Tempate,'$YEAR$',DATENAME(year ,GETUTCDATE()) )
			SET @Tempate = REPLACE(@Tempate,'$MONTH$',DATENAME(month ,GETUTCDATE()) )
			SET @Tempate = REPLACE(@Tempate,'$DAY$',DATENAME(day ,GETUTCDATE()) )
			SET @Tempate = REPLACE(@Tempate,'$QUARTER$',DATENAME(quarter ,GETUTCDATE()) )
				
			IF CHARINDEX(lower(@Tempate),'$organizationname$')>=0
			BEGIN
				SELECT @OrgName= ISNULL(C.vcCompanyName,'') FROM dbo.ReturnHeader RH INNER JOIN dbo.DivisionMaster DM ON RH.numDivisionId=DM.numDivisionID
				INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID
				WHERE RH.numReturnHeaderID=@RecordID;
				
				SET @Tempate = REPLACE(@Tempate,'$OrganizationName$',@OrgName)
			END
			
			SET @Tempate = @Tempate + ISNULL(REPLICATE('0', @numMinLength - LEN(@numSequenceId)),'') + CAST(@numSequenceId AS VARCHAR(18))
			
			PRINT @Tempate
			
			IF EXISTS (SELECT numReturnHeaderID FROM ReturnHeader WHERE numDomainId=@numDomainID AND numReturnHeaderID <> @RecordID AND vcRMA = @Tempate)
			BEGIN
				RAISERROR('Duplicate return name!!',16,1);
				RETURN ;
			END
			
			IF @tintType =3 OR @tintType =4  
				UPDATE dbo.ReturnHeader SET vcBizDocName = @Tempate WHERE numReturnHeaderID = @RecordID
			ELSE IF @tintType =5 OR @tintType =6	
				UPDATE dbo.ReturnHeader SET vcBizDocName = @Tempate,vcRMA=(CASE WHEN tintReturnType=1 THEN vcRMA ELSE @Tempate END) WHERE numReturnHeaderID = @RecordID
			ELSE IF  @tintType =7 --RMA
				UPDATE dbo.ReturnHeader SET vcRMA= @Tempate WHERE numReturnHeaderID = @RecordID
			
			UPDATE NameTemplate SET numSequenceId=numSequenceId + 1  WHERE numDomainID=@numDomainID 
				AND numRecordID=@numBizDocId AND tintModuleID=2
				
		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END
	ELSE
	BEGIN
	
		SELECT @numBizDocId = ISNULL(numBizDocId,0),@OrderID = ISNULL(numOppId,0) FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId =@RecordID
		SET @numSequenceId=0
		
		IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
		BEGIN
			INSERT INTO NameTemplate(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
				SELECT @numDomainId,2,@numBizDocId,CASE WHEN @numBizDocId=290 THEN 'PROI' ELSE UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) END + '-',1,4
		END
		
		--Get Custom Template If enabled
		SELECT @Tempate = ISNULL(vcNameTemplate,'')/*,@numSequenceId=ISNULL(numSequenceId,1),@numMinLength=ISNULL(numMinLength,4)*/ FROM dbo.NameTemplate WHERE numDomainID=@numDomainID 
		AND numRecordID=@numBizDocId AND tintModuleID=2
		
		--Replace Other Values
		SET @Tempate = REPLACE(@Tempate,'$YEAR$',DATENAME(year ,GETUTCDATE()) )
		SET @Tempate = REPLACE(@Tempate,'$MONTH$',DATENAME(month ,GETUTCDATE()) )
		SET @Tempate = REPLACE(@Tempate,'$DAY$',DATENAME(day ,GETUTCDATE()) )
		SET @Tempate = REPLACE(@Tempate,'$QUARTER$',DATENAME(quarter ,GETUTCDATE()) )
			
			IF CHARINDEX(lower(@Tempate),'$organizationname$')>=0
			BEGIN
				SELECT @OrgName= ISNULL(C.vcCompanyName,'') FROM dbo.OpportunityMaster OM INNER JOIN dbo.DivisionMaster DM ON OM.numDivisionId=DM.numDivisionID
				INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID
				WHERE numOppId=@OrderID;
				
				SET @Tempate = REPLACE(@Tempate,'$OrganizationName$',@OrgName)
			END
			
			SET @Tempate = REPLACE(@Tempate,'$OrderID$', @OrderID)
			SET @Tempate = REPLACE(@Tempate,'$BizDocID$',@RecordID )
			
			SET @Tempate = @Tempate --+ ISNULL(REPLICATE('0', @numMinLength - LEN(@numSequenceId)),'') + CAST(@numSequenceId AS VARCHAR(18))
			
			PRINT @Tempate
			
			SELECT @numSequenceId=ISNULL(MAX(CAST(numSequenceId AS BIGINT)),1) FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId
			 WHERE OM.numDomainId=@numDomainID AND OBD.numBizDocId=@numBizDocId
			
			UPDATE dbo.OpportunityBizDocs SET vcBizDocName = @Tempate/*,numSequenceId=ISNULL(@numSequenceId,0) + 1*/  WHERE numOppBizDocsId=@RecordID
			
			IF @numBizDocId <> 297 AND @numBizDocId <> 299
			BEGIN
				UPDATE 
					NameTemplate 
				SET 
					numSequenceId=ISNULL(@numSequenceId,0) + 1  
				WHERE 
					numDomainID=@numDomainID 
					AND numRecordID=@numBizDocId AND tintModuleID=2
			END
	END
END