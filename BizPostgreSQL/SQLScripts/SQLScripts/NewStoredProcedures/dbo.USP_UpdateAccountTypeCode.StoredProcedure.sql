-- SELECT * FROM [AccountTypeDetail]
--This function will update account type code for all children recursively
-- EXEC USP_UpdateAccountTypeCode 1,2
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateAccountTypeCode')
DROP PROCEDURE USP_UpdateAccountTypeCode
GO
CREATE PROCEDURE USP_UpdateAccountTypeCode
@numDomainID NUMERIC(9),
@numAccountTypeID NUMERIC(9), --- AccountId for which the parent id is changing
@numNewParentID NUMERIC(9) --- new parentid

AS 
BEGIN

DECLARE @vcNewAccountCode varchar(50)
DECLARE @vcOldAccountCode varchar(50)

set  @vcNewAccountCode=dbo.GetAccountTypeCode(@numDomainID,@numNewParentID,0)  

SELECT @vcOldAccountCode = vcAccountCode FROM AccountTypeDetail
WHERE numDomainID=@numDomainID AND numAccountTypeID =@numAccountTypeID


UPDATE AccountTypeDetail SET vcAccountCode = @vcNewAccountCode + SUBSTRING(vcAccountCode, LEN(@vcOldAccountCode)+1,LEN(vcAccountCode)) 
WHERE numDomainID=@numDomainID and vcAccountCode LIKE @vcOldAccountCode + '%' 

UPDATE AccountTypeDetail set numParentId=@numNewParentID 
where numDomainID=@numDomainID AND numAccountTypeID =@numAccountTypeID 



END
