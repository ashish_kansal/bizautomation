GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CreateTreeNavigationForDomain' ) 
    DROP PROCEDURE USP_CreateTreeNavigationForDomain
GO
-- exec USP_CreateTreeNavigationForDomain 1
CREATE PROCEDURE [dbo].[USP_CreateTreeNavigationForDomain] 
@numDomainID NUMERIC,
@numPageNavID numeric=0 -- Suppy this parameter when you want to give tree node permissoin to all domain and all user groups by default.
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED

    BEGIN
    
    IF ISNULL(@numPageNavID,0) = 0 
    BEGIN
    	
   
    
		DELETE FROM TreeNavigationAuthorization WHERE numDomainID = @numDomainID
        SELECT  *
        INTO    #tempData
        FROM    ( SELECT    T.numTabID,
                            CASE WHEN bitFixed = 1
                                 THEN CASE WHEN EXISTS ( SELECT numTabName
                                                         FROM   TabDefault
                                                         WHERE  numTabId = T.numTabId
                                                                AND tintTabType = T.tintTabType AND numDomainID = @numDomainID)
                                           THEN ( SELECT TOP 1
                                                            numTabName
                                                  FROM      TabDefault
                                                  WHERE     numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType AND numDomainID = @numDomainID
                                                )
                                           ELSE T.numTabName
                                      END
                                 ELSE T.numTabName
                            END numTabname,
                            vcURL,
                            bitFixed,
                            1 AS tintType,
                            T.vcImage,
                            D.numDomainID,
                            A.numGroupID
                  FROM      TabMaster T
                            CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
                            LEFT JOIN dbo.AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                  WHERE     bitFixed = 1
                            AND D.numDomainId <> -255
                            AND t.tintTabType = 1
                            AND D.numDomainID = @numDomainID
                  UNION ALL
                  SELECT    -1 AS [numTabID],
                            'Administration' numTabname,
                            '' vcURL,
                            1 bitFixed,
                            1 AS tintType,
                            '' AS vcImage,
                            D.numDomainID,
                            A.numGroupID
                  FROM      Domain D
                            JOIN dbo.AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                            WHERE D.numDomainID = @numDomainID
				  
				  UNION ALL
				  SELECT    -3 AS [numTabID],
							'Advanced Search' numTabname,
							'' vcURL,
							1 bitFixed,
							1 AS tintType,
							'' AS vcImage,
							D.numDomainID,
							A.numGroupID
				  FROM      Domain D
							JOIN dbo.AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId	                             
                ) X 

        INSERT  INTO dbo.TreeNavigationAuthorization
                (
                  numGroupID,
                  numTabID,
                  numPageNavID,
                  bitVisible,
                  numDomainID,
                  tintType
                )
                SELECT  numGroupID,
                        PND.numTabID,
                        numPageNavID,
                        bitVisible,
                        numDomainID,
                        tintType
                FROM    dbo.PageNavigationDTL PND
                        JOIN #tempData TD ON TD.numTabID = PND.numTabID 
                        WHERE TD.numDomainID = @numDomainID
				
				UNION ALL 
				
				SELECT  numGroupID AS [numGroupID],
						1 AS [numTabID],
						ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
						1 AS [bitVisible],
						@numDomainID AS [numDomainID],
						1 AS [tintType]
				FROM    dbo.ListDetails LD
				CROSS JOIN dbo.AuthenticationGroupMaster AG
				WHERE   numListID = 27
						AND ISNULL(constFlag, 0) = 1
						AND AG.numDomainID = @numDomainID
						AND numListItemID NOT IN (293,294,295,297,298,299)
--SELECT * FROM #tempData WHERE numGroupID = 1 ORDER BY numDomainID
        DROP TABLE #tempData        	
        
	END
	
	------------------------------------------
	IF ISNULL(@numPageNavID,0) > 0 
	BEGIN

		
		--Delete all existing tree permission from all domains for selected page
			DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID
		
		
			 SELECT    T.numTabID,
                            CASE WHEN bitFixed = 1
                                 THEN CASE WHEN EXISTS ( SELECT numTabName
                                                         FROM   TabDefault
                                                         WHERE  numTabId = T.numTabId
                                                                AND tintTabType = T.tintTabType AND numDomainID =D.numDomainID)
                                           THEN ( SELECT TOP 1
                                                            numTabName
                                                  FROM      TabDefault
                                                  WHERE     numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType AND numDomainID = D.numDomainID
                                                )
                                           ELSE T.numTabName
                                      END
                                 ELSE T.numTabName
                            END numTabname,
                            vcURL,
                            bitFixed,
                            1 AS tintType,
                            T.vcImage,
                            D.numDomainID,
                            A.numGroupID
				  INTO    #tempData1
                  FROM      TabMaster T
                            CROSS JOIN Domain D --ON D.numDomainID = T.numDomainID
                            LEFT JOIN dbo.AuthenticationGroupMaster A ON A.numDomainID = D.numDomainId
                  WHERE     bitFixed = 1
                            AND D.numDomainId > 0
                            AND t.tintTabType = 1
                            
                            

		
--			SELECT * FROM #tempData
		
                            
			INSERT  INTO dbo.TreeNavigationAuthorization
                (
                  numGroupID,
                  numTabID,
                  numPageNavID,
                  bitVisible,
                  numDomainID,
                  tintType
                )
                SELECT  numGroupID,
                        PND.numTabID,
                        numPageNavID,
                        bitVisible,
                        numDomainID,
                        tintType
                FROM    dbo.PageNavigationDTL PND
                        JOIN #tempData1 TD ON TD.numTabID = PND.numTabID 
                        WHERE PND.numPageNavID= @numPageNavID
			
              DROP TABLE #tempData1
                          
		SELECT * FROM dbo.TreeNavigationAuthorization WHERE numPageNavID=@numPageNavID
	END
	
        
    END    
   

 