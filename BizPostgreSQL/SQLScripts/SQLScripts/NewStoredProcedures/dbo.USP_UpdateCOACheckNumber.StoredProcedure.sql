GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateCOACheckNumber' ) 
    DROP PROCEDURE USP_UpdateCOACheckNumber
GO
CREATE PROCEDURE USP_UpdateCOACheckNumber
    @numDomainId NUMERIC,
    @numAccountID NUMERIC,
    @vcStartingCheckNumber VARCHAR(50)
AS 
BEGIN
    UPDATE  Chart_Of_Accounts   SET     vcStartingCheckNumber = @vcStartingCheckNumber
    WHERE   numDomainID = @numDomainId
            AND numAccountID = @numAccountID
END


