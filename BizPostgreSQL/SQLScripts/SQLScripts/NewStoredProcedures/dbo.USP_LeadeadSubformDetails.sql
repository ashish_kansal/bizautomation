/****** Object:  StoredProcedure [dbo].[usp_GetContactInfo]    Script Date: 07/26/2008 16:16:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LeadeadSubformDetails')
DROP PROCEDURE USP_LeadeadSubformDetails
GO
CREATE PROCEDURE [dbo].[USP_LeadeadSubformDetails]        
 @numDomainId numeric(18)=0,
 @numSubFormId numeric(18)=0
AS        
BEGIN  
	SELECT 
		ISNULL(bitByPassRoutingRules,0) AS bitByPassRoutingRules, 
		ISNULL(numAssignTo,0) AS numAssignTo, 
		ISNULL(bitDripCampaign,0) AS bitDripCampaign, 
		ISNULL(numDripCampaign,0) AS numDripCampaign, 
		vcFormName
	FROM 
		LeadsubForms
	WHERE 
		numDomainId=@numDomainId
		AND numSubFormId=@numSubFormId
END
GO