GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER  ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWFRecordData')
DROP PROCEDURE USP_GetWFRecordData
GO
Create PROCEDURE [dbo].[USP_GetWFRecordData]     
    @numDomainID numeric(18, 0),
    @numRecordID numeric(18, 0),
    @numFormID numeric(18, 0)
AS                 
BEGIN
	DECLARE @numContactId AS NUMERIC(18),
			@numRecOwner AS NUMERIC(18),
			@numAssignedTo AS NUMERIC(18),
			@numDivisionId AS NUMERIC(18),
			@txtSignature VARCHAR(8000),
			@vcDateFormat VARCHAR(30),
			@numInternalPM numeric(18,0),
			@numExternalPM numeric(18,0),
			@numNextAssignedTo numeric(18,0),
			@numPhone varchar(15),
			@numOppIDPC numeric(18,0),
			@numStageIDPC numeric(18,0),
			@numOppBizDOCIDPC numeric(18,0),
			@numProjectID NUMERIC(18,0),
			@numCaseId NUMERIC(18,0),
			@numCommId NUMERIC(18,0)

	IF @numFormID=70 --Opportunities & Orders
	BEGIN
		SELECT 
			@numContactId=ISNULL(numContactId,0)
			,@numRecOwner=ISNULL(numRecOwner,0)
			,@numAssignedTo=ISNULL(numAssignedTo,0)
			,@numDivisionId=ISNULL(numDivisionId,0)
		FROM 
			dbo.OpportunityMaster 
		WHERE 
			numDomainId=@numDomainID AND numOppId=@numRecordID

		SELECT 
			@numStageIDPC=ISNULL(s.numStageDetailsId,0)
		FROM 
			OpportunityMaster o 
		LEFT JOIN 
			StagePercentageDetails s 
		on 
			s.numOppID=o.numOppId
		WHERE 
			o.numDomainId=@numDomainID 
			AND o.numOppId=@numRecordID

		SELECT 
			@numOppBizDOCIDPC=ISNULL(s.numOppBizDocsId,0)
			,@numOppIDPC=ISNULL(o.numOppId,0)
		FROM 
			OpportunityMaster o 
		LEFT JOIN 
			OpportunityBizDocs s 
		ON 
			s.numOppID=o.numOppId
		WHERE 
			o.numDomainId=@numDomainID 
			AND o.numOppId=@numRecordID
	END

	IF @numFormID=49 --BizDocs
	BEGIN
		SELECT 
			@numOppBizDOCIDPC=ISNULL(obd.numOppBizDocsId,0)
			,@numContactId=ISNULL(om.numContactId,0)
			,@numRecOwner=ISNULL(om.numRecOwner,0)
			,@numOppIDPC=ISNULL(om.numOppId,0)
			,@numAssignedTo=ISNULL(om.numAssignedTo,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@vcDateFormat=D.vcDateFormat
		FROM 
			dbo.OpportunityBizDocs AS obd 
		INNER JOIN 
			dbo.OpportunityMaster AS OM 
		ON 
			obd.numOppId=om.numOppId 
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=om.numDomainId
		WHERE 
			om.numDomainId=@numDomainID 
			AND obd.numOppBizDocsId=@numRecordID
	END

	IF @numFormID=94 --Sales/Purchase/Project
	BEGIN
		DECLARE @numOppId NUMERIC(18,0)
		SELECT 
			@numProjectID=ISNULL(numProjectID,0)
			,@numOppId=Isnull(numOppId,0) 
		FROM 
			StagePercentageDetails 
		WHERE 
			numStageDetailsId=@numRecordID

		--@numInternalPM numeric(18,0),@numExternalPM numeric(18,0),@numNextAssignedTo numeric(18,0)
		IF @numProjectID=0--Order
		BEGIN
			SELECT 
				@numContactId=ISNULL(om.numContactId,0)
				,@numRecOwner=ISNULL(om.numRecOwner,0)
				,@numAssignedTo=ISNULL(obd.numAssignTo,0)
				,@numDivisionId=ISNULL(om.numDivisionId,0)
				,@vcDateFormat=D.vcDateFormat
			FROM 
				dbo.StagePercentageDetails AS obd 
			INNER JOIN 
				dbo.OpportunityMaster AS OM 
			on 
				obd.numOppId=om.numOppId 
			INNER JOIN 
				dbo.Domain AS d 
			ON 
				d.numDomainId=om.numDomainId
			WHERE 
				om.numDomainId=@numDomainID 
				AND obd.numStageDetailsId=@numRecordID
		END
		ELSE
		BEGIN
			SELECT 
				@numNextAssignedTo=numAssignTo 
			FROM 
				StagePercentageDetails 
			WHERE 
				numStageDetailsId=@numRecordID+1

			SELECT 
				@numContactId=ISNULL(om.numIntPrjMgr,0)
				,@numRecOwner=ISNULL(om.numRecOwner,0)
				,@numAssignedTo=ISNULL(obd.numAssignTo,0)
				,@numDivisionId=ISNULL(om.numDivisionId,0)
				,@numInternalPM=ISNULL(om.numIntPrjMgr,0)
				,@numExternalPM=ISNULL(om.numCustPrjMgr,0)
			FROM 
				dbo.StagePercentageDetails AS  obd 
			INNER JOIN 
				dbo.ProjectsMaster AS OM 
			on 
				obd.numProjectID=om.numProId 	
			WHERE 
				om.numDomainId=@numDomainID 
				AND obd.numStageDetailsId=@numRecordID
		END
	END
	
	IF (@numFormID=68 OR @numFormID=138) --Organization , AR Aging
	BEGIN
		SELECT 
			@numContactId=ISNULL(adc.numContactId,0)
			,@numRecOwner=ISNULL(obd.numRecOwner,0)
			,@numAssignedTo=ISNULL(obd.numAssignedTo,0)
			,@numDivisionId=ISNULL(obd.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.DivisionMaster AS  obd 	
		INNER join 
			dbo.AdditionalContactsInformation as adc 
		ON 
			adc.numDivisionId=obd.numDivisionID
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=obd.numDomainId
		WHERE 
			obd.numDomainId=@numDomainID 
			AND obd.numDivisionID=@numRecordID
	END

	IF @numFormID=69 --Contacts
	BEGIN
		SELECT 
			@numContactId=ISNULL(adc.numContactId,0)
			,@numRecOwner=ISNULL(obd.numRecOwner,0)
			,@numAssignedTo=ISNULL(obd.numAssignedTo,0)
			,@numDivisionId=ISNULL(obd.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.DivisionMaster AS  obd 	
		INNER JOIN 
			dbo.AdditionalContactsInformation as adc 
		ON 
			adc.numDivisionId=obd.numDivisionID
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=obd.numDomainId
		WHERE 
			obd.numDomainId=@numDomainID 
			AND adc.numContactId=@numRecordID
	END

	IF @numFormID=73 --Projects
	BEGIN
		SELECT 
			@numProjectID=ISNULL(OM.numProId,0)
			,@numContactId=ISNULL(om.numIntPrjMgr,0)
			,@numRecOwner=ISNULL(om.numRecOwner,0)
			,@numAssignedTo=ISNULL(om.numAssignedTo,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@numInternalPM=ISNULL(om.numIntPrjMgr,0)
			,@numExternalPM=ISNULL(om.numCustPrjMgr,0)
		FROM 
			dbo.ProjectsMaster AS OM	
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numProId=@numRecordID

		SELECT 
			@numStageIDPC=ISNULL(s.numStageDetailsId,0)
		FROM 
			ProjectsMaster o 
		left join 
			StagePercentageDetails s 
		on 
			s.numProjectID=o.numProId
		WHERE 
			o.numDomainId=@numDomainID 
			AND o.numProId=@numRecordID
	END

	IF @numFormID=72 --Cases
	BEGIN
		SELECT 
			@numCaseId=ISNULL(OM.numCaseId,0)
			,@numContactId=ISNULL(om.numContactId,0)
			,@numRecOwner=ISNULL(om.numRecOwner,0)
			,@numAssignedTo=ISNULL(om.numAssignedTo,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.Cases AS OM	
		INNER JOIN 
			dbo.Domain AS d 
		ON
			d.numDomainId=om.numDomainId
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numCaseId=@numRecordID
	
		SELECT 
			@numOppIDPC=ISNULL(o.numOppId,0)
		FROM 
			Cases om 
		left JOIN 
			CaseOpportunities co 
		on 
			om.numCaseId=co.numCaseId  
		left join 
			OpportunityMaster o 
		on
			o.numOppId=co.numOppId
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numCaseId=@numRecordID
	END

	IF @numFormID=124 --Action items(Ticklers)
	BEGIN
		SELECT 
			@numCommId=ISNULL(OM.numCommId,0)
			,@numContactId=ISNULL(om.numContactId,0)
			,@numRecOwner=ISNULL(om.numAssignedBy,0)
			,@numAssignedTo=ISNULL(om.numAssign,0)
			,@numDivisionId=ISNULL(om.numDivisionId,0)
			,@vcDateFormat=d.vcDateFormat
		FROM 
			dbo.Communication AS OM	
		INNER JOIN 
			dbo.Domain AS d 
		ON 
			d.numDomainId=om.numDomainId
		WHERE 
			om.numDomainId=@numDomainID 
			AND om.numCommId=@numRecordID
	END

	DECLARE @vcEmailID AS VARCHAR(100),
			@ContactName AS VARCHAR(150),
			@bitSMTPServer AS bit,
			@vcSMTPServer AS VARCHAR(100),
			@numSMTPPort AS NUMERIC,
			@bitSMTPAuth AS BIT,
			@vcSmtpPassword AS VARCHAR(100),
			@bitSMTPSSL BIT 

	IF @numRecOwner>0
	BEGIN
		SELECT 
			@vcEmailID=vcEmailID
			,@ContactName=isnull(vcFirstname,'')+' '+isnull(vcLastName,'')
			,@bitSMTPServer=isnull(bitSMTPServer,0)
			,@vcSMTPServer=case when bitSMTPServer= 1 then vcSMTPServer else '' end
			,@numSMTPPort=case when bitSMTPServer= 1 then  isnull(numSMTPPort,0) else 0 end
			,@bitSMTPAuth=isnull(bitSMTPAuth,0)
			,@vcSmtpPassword=isnull([vcSmtpPassword],'')
			,@bitSMTPSSL=isnull(bitSMTPSSL,0)
			,@txtSignature=u.txtSignature
			,@numPhone=numPhone
		FROM 
			UserMaster U 
		JOIN 
			AdditionalContactsInformation A 
		ON 
			A.numContactID=U.numUserDetailId 
		WHERE 
			U.numDomainID=@numDomainID 
			AND numUserDetailId=@numRecOwner
	END

	IF @numAssignedTo=0 --As per Auto Routing Rule
	BEGIN
		DECLARE @numRuleID NUMERIC	 
				
		SELECT @numRuleID=numRoutID FROM RoutingLeads  WHERE numDomainID=@numDomainID and bitDefault=1  

		SELECT 
			@numAssignedTo=rd.numEmpId           
		FROM 
			RoutingLeads r 
		INNER JOIN 
			Routingleaddetails rd 
		ON 
			r.numRoutID=rd.numRoutID
		WHERE 
			r.numRoutID=@numRuleID
	END

	SELECT 
		@numContactId AS numContactId
		,@numRecOwner AS numRecOwner
		,@numAssignedTo AS numAssignedTo
		,@numDivisionId AS numDivisionId
		,@vcEmailID AS vcEmailID
		,ISNULL((SELECT vcPSMTPDisplayName FROM Domain WHERE numDomainId=@numDomainID),@ContactName) AS ContactName
		,@bitSMTPServer AS bitSMTPServer
		,@vcSMTPServer AS vcSMTPServer
		,@numSMTPPort AS numSMTPPort
		,@bitSMTPAuth AS bitSMTPAuth
		,@vcSmtpPassword AS vcSmtpPassword
		,@bitSMTPSSL AS bitSMTPSSL
		,@txtSignature AS Signature
		,@numInternalPM as numInternalPM
		,@numExternalPM as numExternalPM
		,@numNextAssignedTo as numNextAssignedTo
		,@numPhone as numPhone
		,@numOppIDPC as numOppIDPC
		,@numStageIDPC as numStageIDPC
		,@numOppBizDOCIDPC as numOppBizDOCIDPC
		,@numProjectID AS numProjectID
		,@numCaseId AS numCaseId
		,@numCommId AS numCommId
END
GO