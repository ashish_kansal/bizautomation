SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityKitItemsForAuthorizativeAccounting')
DROP PROCEDURE USP_OpportunityKitItemsForAuthorizativeAccounting
GO
CREATE PROCEDURE  [dbo].[USP_OpportunityKitItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
 @numOppId as numeric(9)=null,                                                                                                                                                
 @numOppBizDocsId as numeric(9)=null                                                              
)                                                                                                                                                
                                                                                                                                            
as                           
BEGIN
     --OpportunityKitItems
    SELECT 
		I.[vcItemName] as Item,        
		I.charitemType as type,        
		'' as [desc],        
		(ISNULL(OBI.numUnitHour,0) * (OKI.numQtyItemsReq_Orig * ISNULL(dbo.fn_UOMConversion(OKI.numUOMId,OKI.numChildItemID,i.numDomainID,i.numBaseUnit),1))) as Unit,        
		0 as Price,        
		0 as Amount,0 AS ItemTotalAmount,       
		0 as listPrice,
		CONVERT(VARCHAR,i.numItemCode) as ItemCode,
		numOppChildItemID AS numoppitemtCode,        
		L.vcdata as vcItemClassification,
		CASE WHEN I.bitTaxable=0 THEN 'No' ELSE 'Yes' END AS Taxable,
		0 AS monListPrice,
		ISNULL(i.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(i.numAssetChartAcntId,0) as itemInventoryAsset,
		'NI' as ItemType,
		ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs,
		(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN '0' ELSE ISNULL(OKI.monAvgCost,'0') END) AS AverageCost,
		ISNULL(OBI.bitDropShip,0) AS bitDropShip,  
		0 as DiscAmt,
		NULLIF(Opp.numProjectID,0) numProjectID,
		NULLIF(Opp.numClassID,0) numClassID 
	FROM  
		OpportunityItems opp 
	JOIN 
		OpportunityBizDocItems OBI 
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       
	JOIN 
		OpportunityKitItems OKI 
	ON 
		OKI.numOppItemID=opp.numoppitemtCode 
		AND Opp.numOppId=OKI.numOppId
	LEFT JOIN 
		Item iMain
	ON 
		opp.numItemCode=iMain.numItemCode
	LEFT JOIN 
		Item i 
	ON 
		OKI.numChildItemID=i.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		i.numItemClassification=L.numListItemID        
	WHERE 
		Opp.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
		AND ISNULL(I.bitKitParent,0) = 0
		AND ISNULL(iMain.bitAssembly,0) = 0
	UNION ALL
	SELECT 
		I.[vcItemName] as Item,        
		charitemType as type,        
		'' as [desc],        
		(ISNULL(OBI.numUnitHour,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * (ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(dbo.fn_UOMConversion(OKCI.numUOMId,OKCI.numItemID,i.numDomainID,i.numBaseUnit),1))) as Unit,        
		0 as Price,        
		0 as Amount,0 AS ItemTotalAmount,       
		0 as listPrice,
		CONVERT(VARCHAR,i.numItemCode) as ItemCode,
		OKI.numOppChildItemID AS numoppitemtCode,        
		L.vcdata as vcItemClassification,
		CASE WHEN bitTaxable=0 THEN 'No' ELSE 'Yes' END AS Taxable,
		0 AS monListPrice,
		ISNULL(i.numIncomeChartAcntId,0) AS itemIncomeAccount,
		ISNULL(i.numAssetChartAcntId,0) as itemInventoryAsset,
		'NI' as ItemType,
		ISNULL(i.numCOGsChartAcntId,0) AS itemCoGs,
		(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN '0' ELSE ISNULL(OKCI.monAvgCost,'0') END) AS AverageCost,
		ISNULL(OBI.bitDropShip,0) AS bitDropShip,  
		0 as DiscAmt,
		NULLIF(Opp.numProjectID,0) numProjectID,
		NULLIF(Opp.numClassID,0) numClassID 
	FROM  
		OpportunityKitChildItems OKCI 
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKCI.numOppChildItemID = OKI.numOppChildItemID
	INNER JOIN
		OpportunityItems opp 
	ON
		OKCI.numOppItemID = opp.numoppitemtCode
		AND OKCI.numOppID = @numOppId 
	INNER JOIN 
		OpportunityBizDocItems OBI 
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       	
	LEFT JOIN 
		Item i 
	ON 
		OKCI.numItemID=i.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		i.numItemClassification=L.numListItemID        
	WHERE 
		OKCI.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
END