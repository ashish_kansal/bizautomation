SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_ARPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_ARPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_ARPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @TotalAR DECIMAL(20,5) = (SELECT numOpeningBal FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId IN (SELECT numAccountID FROM AccountingCharges WHERE numDomainID=@numDomainID AND numChargeTypeId=4))
	DECLARE @ARWithin30Days DECIMAL(20,5) = 0
	DECLARE @ARWithin31to60Days DECIMAL(20,5) = 0
	DECLARE @ARWithin61to90Days DECIMAL(20,5) = 0
	DECLARE @ARPastDue DECIMAL(20,5) = 0
	DECLARE @dtFromDate DATETIME = '1990-01-01 00:00:00.000'
	DECLARE @dtToDate DATETIME = GETUTCDATE()

	SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	DECLARE @AuthoritativeSalesBizDocId INTEGER
    SELECT 
		@AuthoritativeSalesBizDocId = ISNULL(numAuthoritativeSales,0)
    FROM 
		AuthoritativeBizDocs
    WHERE 
		numDomainId = @numDomainId;
  
	DECLARE @TEMPARRecord TABLE
	(
		numOppId NUMERIC(18,0),
		numOppBizDocsId NUMERIC(18,0),
		DealAmount DECIMAL(20,5),
		numBizDocId NUMERIC(18,0),
		numCurrencyID NUMERIC(18,0),
		dtDueDate DATETIME,
		AmountPaid DECIMAL(20,5),
		monUnAppliedAmount DECIMAL(20,5)
	)

	BEGIN TRY
		INSERT INTO @TEMPARRecord
		(
			numOppId
			,numOppBizDocsId
			,DealAmount
			,numBizDocId
			,numCurrencyID
			,dtDueDate
			,AmountPaid
		)
		SELECT  
			OB.numOppId,
			OB.numOppBizDocsId,
			ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),0) DealAmount,
			OB.numBizDocId,
			ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
			DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0)) ELSE 0 END, dtFromDate) AS dtDueDate,
			ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),0) as AmountPaid
		FROM    
			OpportunityMaster Opp
		INNER JOIN 
			[dbo].[DivisionMaster] AS DIV 
		ON 
			DIV.[numDivisionID] = Opp.[numDivisionId]
		INNER JOIN 
			OpportunityBizDocs OB 
		ON 
			OB.numOppId = Opp.numOppID
		LEFT OUTER JOIN 
			[Currency] C 
		ON 
			Opp.[numCurrencyID] = C.[numCurrencyID]
		WHERE 
			tintOpptype = 1
			AND tintoppStatus = 1        
			AND opp.numDomainID = @numDomainId
			AND numBizDocId = @AuthoritativeSalesBizDocId
			AND OB.bitAuthoritativeBizDocs=1
			AND ISNULL(OB.tintDeferred,0) <> 1
			AND CONVERT(DATE,OB.[dtCreatedDate]) BETWEEN @dtFromDate AND @dtToDate
		GROUP BY 
			OB.numOppId,
			OB.numOppBizDocsId,
			Opp.fltExchangeRate,
			OB.numBizDocId,
			OB.dtCreatedDate,
			Opp.bitBillingTerms,
			Opp.intBillingDays,
			OB.monDealAmount,
			Opp.numCurrencyID,
			OB.dtFromDate,
			OB.monAmountPaid
		HAVING
			(ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0) - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),0) ) != 0
		--Show Impact of AR journal entries in report as well 
		UNION 
		SELECT 
			0,
			0,
			CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
			0,
			GJD.[numCurrencyID],
			GJH.datEntry_Date,
			0 AS AmountPaid
		FROM  
			dbo.General_Journal_Header GJH
		INNER JOIN 
			dbo.General_Journal_Details GJD 
		ON 
			GJH.numJournal_Id = GJD.numJournalId
			AND GJH.numDomainId = GJD.numDomainId
		INNER JOIN
			dbo.Chart_Of_Accounts COA 
		ON 
			COA.numAccountId = GJD.numChartAcntId
		INNER JOIN 
			[dbo].[DivisionMaster] AS DIV 
		ON 
			DIV.[numDivisionID] = [GJD].[numCustomerId]
		WHERE 
			GJH.numDomainId = @numDomainId
			AND COA.vcAccountCode LIKE '0101010501'
			AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0
			AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
			AND CONVERT(DATE,GJH.[datEntry_Date]) BETWEEN @dtFromDate AND @dtToDate
		UNION ALL
		SELECT
			0,
			0,
			(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1,
			0,
			DM.numCurrencyID,
			DM.dtDepositDate,
			0 AS AmountPaid
		FROM 
			DepositMaster DM 
		INNER JOIN 
			[dbo].[DivisionMaster] AS DIV 
		ON 
			[DM].[numDivisionID] = DIV.[numDivisionID]
		WHERE 
			DM.numDomainId=@numDomainId 
			AND tintDepositePage IN(2,3)  
			AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
			AND CONVERT(DATE,DM.[dtDepositDate]) BETWEEN @dtFromDate AND @dtToDate
		UNION ALL --Standalone Refund against Account Receivable
		SELECT 
			0,
			0,
			CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
			0,
			GJD.[numCurrencyID],
			GJH.datEntry_Date,
			0 AS AmountPaid
		FROM 
			dbo.ReturnHeader RH 
		INNER JOIN 
			dbo.General_Journal_Header GJH 
		ON 
			RH.numReturnHeaderID = GJH.numReturnID
		JOIN 
			[dbo].[DivisionMaster] AS DIV 
		ON 
			DIV.[numDivisionID] = RH.[numDivisionId]
		JOIN 
			General_Journal_Details GJD 
		ON 
			GJH.numJournal_Id = GJD.numJournalId 
		JOIN 
			dbo.Chart_Of_Accounts COA 
		ON 
			COA.numAccountId = GJD.numChartAcntId 
			AND COA.vcAccountCode LIKE '0101010501'
		WHERE 
			RH.tintReturnType=4 
			AND RH.numDomainId=@numDomainId  
			AND ISNULL(RH.numParentID,0) = 0
			AND ISNULL(RH.IsUnappliedPayment,0) = 0
			AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0 --AND ISNULL(RH.numDepositIDRef,0) > 0
			AND CONVERT(DATE,GJH.[datEntry_Date])  BETWEEN @dtFromDate AND @dtToDate
		

		INSERT INTO @TEMPARRecord
		(
			numOppId
			,numOppBizDocsId
			,DealAmount
			,numBizDocId
			,numCurrencyID
			,dtDueDate
			,AmountPaid
			,monUnAppliedAmount
		)
		SELECT 
			0
			,0
			,0
			,0
			,0
			,NULL
			,0
			,SUM(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0))       
		FROM 
			DepositMaster DM 
		INNER JOIN 
			[dbo].[DivisionMaster] AS DIV 
		ON 
			DIV.[numDivisionID] = DM.[numDivisionID]
 		WHERE 
			DM.numDomainId=@numDomainId 
			AND tintDepositePage IN (2,3)  
			AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
			AND CONVERT(DATE,DM.[dtDepositDate]) BETWEEN @dtFromDate AND @dtToDate
		GROUP BY 
			DM.numDivisionId

	

		SET @ARWithin30Days = (SELECT SUM(DealAmount) FROM @TEMPARRecord WHERE DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30)
		SET @ARWithin31to60Days = (SELECT SUM(DealAmount) FROM @TEMPARRecord WHERE DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60)
		SET @ARWithin61to90Days = (SELECT SUM(DealAmount) FROM @TEMPARRecord WHERE DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90)
		SET @ARPastDue = (SELECT SUM(DealAmount) FROM @TEMPARRecord WHERE dbo.GetUTCDateWithoutTime() > [dtDueDate])
	END TRY
	BEGIN CATCH
		-- DO NOT RAISE ERROR
	END CATCH

	SELECT ISNULL(@TotalAR,0) AS TotalAR, ISNULL(@ARWithin30Days,0) AS ARWithin30Days,ISNULL(@ARWithin31to60Days,0) AS ARWithin31to60Days,ISNULL(@ARWithin61to90Days,0) AS ARWithin61to90Days,ISNULL(@ARPastDue,0) AS ARPastDue
END
GO