/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocsForPOFulfillmentSettings')
DROP PROCEDURE USP_GetBizDocsForPOFulfillmentSettings
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocsForPOFulfillmentSettings]
(                        
	@numDomainID AS NUMERIC(9) = NULL,
	@numListID AS NUMERIC(9) = NULL    
)                        
AS 
BEGIN   
	/*********************************************Master Biz Doc List Starts******************************************************************************/
	CREATE TABLE #tempMasterBizDocList 
	(
		ID INT IDENTITY (1, 1) NOT NULL,
		numListItemID NUMERIC(18,0),
		vcData  VARCHAR(100),
	)

	DECLARE @BizDocType INT
	SET @BizDocType = 2

	INSERT INTO #tempMasterBizDocList 
		EXEC USP_GetBizDocType @BizDocType,@numDomainID
		--EXEC USP_GetMasterListItemsForBizDocs @ListID, @numDomainID, @numOppId

	SELECT * FROM #tempMasterBizDocList 
	/*********************************************Master Biz Doc List Ends******************************************************************************/

	/*********************************************Opportunity Biz Doc List Starts******************************************************************************/
	CREATE TABLE #tempOppBizDocList 
	(
		ID INT IDENTITY (1, 1) NOT NULL,
		numBizDocID NUMERIC(18,0),
		vcTemplate VARCHAR(100),
	)

	DECLARE @numOppId AS NUMERIC(9)

	--Purchase Order
	SELECT TOP 1 @numOppId = Opp.numOppId
	FROM OpportunityMaster Opp
		JOIN OpportunityItems OI ON OI.numOppId=Opp.numOppId
		JOIN Item I ON I.numItemCode=OI.numItemCode
	WHERE tintOppType=2 AND tintOppstatus=1 
		AND ISNULL(bitStockTransfer,0)=0 AND tintShipped=0 AND numUnitHour-ISNULL(numUnitHourReceived,0)>0  
		AND I.[charItemType] IN ('P','N','S') AND (OI.bitDropShip=0 OR OI.bitDropShip IS NULL) 
		AND Opp.numDomainID=72 
	ORDER BY  opp.bintCreatedDate DESC

	INSERT INTO #tempOppBizDocList 
		SELECT OPP.numBizDocId,
				ISNULL((SELECT TOP 1 vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS vcTemplateName
		FROM                         
			OpportunityBizDocs OPP   
		WHERE OPP.numOppId = @numOppId

	SELECT * FROM #tempOppBizDocList 
	/*********************************************Opportunity Biz Doc List Ends******************************************************************************/

	SELECT * FROM #tempMasterBizDocList M WHERE NOT EXISTS (SELECT * FROM #tempOppBizDocList B WHERE M.numListItemID = B.numBizDocID )

	DROP TABLE #tempMasterBizDocList
	DROP TABLE #tempOppBizDocList
END

