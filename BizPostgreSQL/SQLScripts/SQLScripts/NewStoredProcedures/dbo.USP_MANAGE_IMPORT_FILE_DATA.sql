GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_IMPORT_FILE_DATA')
DROP PROCEDURE USP_MANAGE_IMPORT_FILE_DATA
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MANAGE_IMPORT_FILE_DATA]
(
   @intImportFileID		BIGINT	OUTPUT,	
   @vcImportFileName	VARCHAR(1000),
   @numMasterID			NUMERIC,
   @numRecordAdded		NUMERIC,
   @numRecordUpdated	NUMERIC,
   @numErrors			NUMERIC,
   @numDuplicates		NUMERIC,
   @numHistoryID		NUMERIC,
   @dtCreateDate		DATETIME,
   @dtImportDate		DATETIME,
   @numDomainID			NUMERIC,
   @numUserContactID	NUMERIC,
   @tintStatus			TINYINT,
   @btHasSendEmail		BIT,
   @intMode				INT
)
AS 
BEGIN
	IF @intMode = 0 -- FOR INSERT
		BEGIN
			INSERT INTO Import_File_Master
				   (vcImportFileName
				   ,numMasterID
				   ,numRecordAdded
				   ,numRecordUpdated
				   ,numErrors
				   ,numDuplicates
				   ,numHistoryID
				   ,dtCreateDate
				   ,dtImportDate
				   ,numDomainID
				   ,numUserContactID
				   ,tintStatus
				   ,btHasSendEmail)
			 VALUES
				   (	
					@vcImportFileName	
					,@numMasterID			
					,@numRecordAdded		
					,@numRecordUpdated	
					,@numErrors			
					,@numDuplicates		
					,@numHistoryID		
					,@dtCreateDate		
					,@dtImportDate		
					,@numDomainID			
					,@numUserContactID	
					,@tintStatus			
					,@btHasSendEmail
				   )
			SET @intImportFileID = @@IDENTITY	   
		END  
	IF @intMode = 1 -- FOR UPDATE	         
		BEGIN
			UPDATE Import_File_Master SET [vcImportFileName] = @vcImportFileName
										  ,[numMasterID] = @numMasterID
										  ,[numRecordAdded] = @numRecordAdded
										  ,[numRecordUpdated] = @numRecordUpdated
										  ,[numErrors] = @numErrors
										  ,[numDuplicates] = @numDuplicates
										  ,[numHistoryID] = @numHistoryID
										  ,[dtCreateDate] = @dtCreateDate
										  ,[dtImportDate] = @dtImportDate
										  ,[numDomainID] = @numDomainID
										  ,[numUserContactID] = @numUserContactID
										  ,[tintStatus] = @tintStatus
										  ,[btHasSendEmail] = @btHasSendEmail
			WHERE intImportFileID = @intImportFileID
		END		
END
