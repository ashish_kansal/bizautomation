        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOpportunityMasterTaxItems' ) 
    DROP PROCEDURE USP_GetOpportunityMasterTaxItems
GO

CREATE PROCEDURE [dbo].[USP_GetOpportunityMasterTaxItems]
    @numOppId NUMERIC(18, 0) =0,
    @numOppItemID NUMERIC(18, 0) =0,
    @tintMode AS TINYINT 
AS 
BEGIN			
	IF @tintMode=1
	BEGIN
		SELECT * FROM OpportunityMasterTaxItems WHERE ISNULL(numOppId,0)=@numOppId 
	END
	ELSE IF @tintMode=2
	BEGIN
		SELECT 
			OITI.* 
			,OMTI.fltPercentage
			,OMTI.tintTaxType
		FROM 
			OpportunityItemsTaxItems OITI
		INNER JOIN
			OpportunityMasterTaxItems OMTI
		ON
			OMTI.numOppID=@numOppId
			AND OITI.numTaxItemID=OMTI.numTaxItemID
			AND ISNULL(OITI.numTaxID,0)=ISNULL(OMTI.numTaxID,0)
		WHERE 
			ISNULL(OITI.numOppId,0)=@numOppId 
			AND ISNULL(numOppItemID,0)=@numOppItemID
	END
END
   