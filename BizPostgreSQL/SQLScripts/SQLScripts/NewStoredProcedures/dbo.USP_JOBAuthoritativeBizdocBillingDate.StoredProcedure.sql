
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_JOBAuthoritativeBizdocBillingDate')
DROP PROCEDURE USP_JOBAuthoritativeBizdocBillingDate
GO
CREATE PROCEDURE [dbo].[USP_JOBAuthoritativeBizdocBillingDate]
AS
BEGIN

IF EXISTS (
SELECT CONVERT(VARCHAR(10),obd.dtCreatedDate,111) dtCreatedDate,CONVERT(VARCHAR(10),obd.dtFromDate,111) dtFromDate,CONVERT(VARCHAR(10),GJH.datEntry_Date,111) datEntry_Date
,GJH.* 
FROM dbo.OpportunityBizDocs OBD JOIN dbo.General_Journal_Header GJH ON OBD.numOppId=GJH.numOppId
AND obd.numOppBizDocsId=GJH.numOppBizDocsId WHERE 
CONVERT(VARCHAR(10),obd.dtFromDate,111)!=CONVERT(VARCHAR(10),GJH.datEntry_Date,111)
AND isnull(Gjh.numBillID,0)=0 AND isnull(gjh.numBillPaymentID,0)=0 AND isnull(GJH.numPayrollDetailID,0)=0
AND isnull(Gjh.numCheckHeaderID,0)=0 AND isnull(gjh.numReturnID,0)=0 
AND isnull(GJH.numDepositId,0)=0 and monDealAMount=numAmount 
AND OBD.bitAuthoritativeBizDocs=1 )
BEGIN
	RAISERROR ('Find_BillingDate_Issue',16,1);
END

END