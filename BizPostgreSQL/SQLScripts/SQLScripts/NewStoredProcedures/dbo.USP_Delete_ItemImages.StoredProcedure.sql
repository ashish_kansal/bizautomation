GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteItemImages' ) 
                    DROP PROCEDURE USP_DeleteItemImages
Go

Create PROCEDURE [dbo].[USP_DeleteItemImages]

@numItemImageIds VARCHAR(100) = '',
@numDomainId NUMERIC(9)=0,
@numItemCode NUMERIC(9)=0

AS
BEGIN
   IF @numItemImageIds != ''
	     BEGIN	 --main        
			 SELECT id INTO #tempItem FROM  dbo.SplitIDs(@numItemImageIds,',')

	         IF EXISTS(SELECT * FROM ItemImages II JOIN #tempItem TI ON II.numItemImageId=TI.id
	                   WHERE numDomainId=@numDomainId AND numItemCode = @numItemCode and II.bitDefault = 1)
	          BEGIN
	              DELETE FROM ItemImages WHERE  numDomainId=@numDomainId AND numItemImageId IN (SELECT id FROM #tempItem)	
	              
                  IF EXISTS (SELECT * FROM ItemImages WHERE numDomainId=@numDomainId AND numItemCode = @numItemCode)
                      BEGIN
                         UPDATE ItemImages  SET bitDefault = 1 WHERE numItemImageId = (SELECT TOP 1 cd.numItemImageId FROM ItemImages cd WHERE cd.numDomainId = @numDomainId AND cd.numItemCode = @numItemCode)
                     END
              END	
            
             ELSE 
              BEGIN
                 DELETE FROM ItemImages WHERE  numDomainId=@numDomainId AND numItemImageId IN (SELECT id FROM #tempItem)	
              END   
              
              DROP TABLE #tempItem	 
		  END --main if 
   ELSE IF @numItemCode != 0
	   
	   BEGIN
	    	DELETE FROM ItemImages WHERE  numDomainId=@numDomainId AND numItemCode = @numItemCode 	
	   END   			
END --root
--EXEC dbo.USP_DeleteItemImages @numItemCode =197605, @numDomainId= 1 ,@numItemImageIds = '81'
