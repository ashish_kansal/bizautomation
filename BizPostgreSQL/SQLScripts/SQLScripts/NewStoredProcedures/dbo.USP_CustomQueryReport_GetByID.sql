SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_GetByID')
DROP PROCEDURE dbo.USP_CustomQueryReport_GetByID
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_GetByID]
	@numReportID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		numReportID,
		numDomainID,
		vcReportName,
		vcReportDescription,
		vcEmailTo,
		tintEmailFrequency,
		vcQuery,
		vcCSS
	FROM 
		CustomQueryReport
	WHERE
		numReportID=@numReportID
END