GO
/****** Object:  StoredProcedure [dbo].[USP_GetProfileEGroupContacts]    Script Date: 06/04/2009 15:16:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProfileEGroupContacts')
DROP PROCEDURE USP_GetProfileEGroupContacts
GO
CREATE PROCEDURE [dbo].[USP_GetProfileEGroupContacts] 
           @numEmailGroupID AS NUMERIC
AS
  BEGIN
    SELECT EG.numContactID,
           ACI.numRecOwner
    FROM   ProfileEGroupDTL EG
           LEFT OUTER JOIN AdditionalContactsInformation ACI
             ON ACI.numContactId = EG.numContactID
    WHERE  numEmailGroupID = @numEmailGroupID
  END