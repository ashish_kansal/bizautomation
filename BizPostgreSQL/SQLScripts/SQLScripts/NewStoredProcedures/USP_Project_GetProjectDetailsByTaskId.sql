
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Project_GetProjectDetailsByTaskId')
DROP PROCEDURE USP_Project_GetProjectDetailsByTaskId
GO
CREATE PROCEDURE [dbo].[USP_Project_GetProjectDetailsByTaskId]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskId NUMERIC(18,0)
)
AS 
BEGIN
	SELECT TOP 1 P.*,C.vcCompanyName FROM StagePercentageDetailsTask AS ST LEFT JOIN ProjectsMaster AS P ON ST.numProjectId=P.numProId 
	LEFT JOIN DivisionMaster AS D ON D.numDivisionID=P.numDivisionId
	LEFT JOIN CompanyInfo AS C ON C.numCompanyId=D.numCompanyID
	WHERE ST.numDomainID=@numDomainID AND ST.numTaskId=@numTaskId AND ST.numProjectId>0
END