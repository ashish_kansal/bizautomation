GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DivisionMasterShippingAccount_Save')
DROP PROCEDURE USP_DivisionMasterShippingAccount_Save
GO
CREATE PROCEDURE [dbo].[USP_DivisionMasterShippingAccount_Save]    
	@numUserCntID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numShipViaID NUMERIC(18,0)
	,@vcAccountNumber VARCHAR(100)
	,@vcStreet VARCHAR(300)
	,@vcCity VARCHAR(100)
	,@numCountry NUMERIC(18,0)
	,@numState NUMERIC(18,0)
	,@vcZipCode VARCHAR(50)
AS
BEGIN
	IF EXISTS (SELECT ID FROM DivisionMasterShippingAccount WHERE numDivisionID=@numDivisionID AND numShipViaID=@numShipViaID)
	BEGIN
		UPDATE
			DivisionMasterShippingAccount 
		SET
			vcAccountNumber=@vcAccountNumber
			,vcStreet=@vcStreet
			,vcCity=@vcCity
			,numCountry=@numCountry
			,numState=@numState
			,vcZipCode=@vcZipCode
			,numModifiedBy=@numUserCntID
			,dtModifiedDate=GETUTCDATE()
		WHERE 
			numDivisionID=@numDivisionID 
			AND numShipViaID=@numShipViaID
	END
	ELSE
	BEGIN
		INSERT INTO DivisionMasterShippingAccount
		(
			numDivisionID
			,numShipViaID
			,vcAccountNumber
			,vcStreet
			,vcCity
			,numCountry
			,numState
			,vcZipCode
			,numCreatedBy
			,dtCreatedDate
		)
		VALUES
		(
			@numDivisionID
			,@numShipViaID
			,@vcAccountNumber
			,@vcStreet
			,@vcCity
			,@numCountry
			,@numState
			,@vcZipCode
			,@numUserCntID
			,GETUTCDATE()
		)
	END
END
GO