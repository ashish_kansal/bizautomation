SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMirrorBizDocDetails' ) 
    DROP PROCEDURE USP_GetMirrorBizDocDetails
GO

-- EXEC USP_GetMirrorBizDocDetails 53,8,1,1,0
CREATE PROCEDURE [dbo].[USP_GetMirrorBizDocDetails]
    (
      @numReferenceID NUMERIC(9) = 0,
      @numReferenceType TINYINT,
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @ClientTimeZoneOffset INT
    )
AS 
    BEGIN
--        DECLARE @numBizDocID AS NUMERIC
        DECLARE @numBizDocType AS NUMERIC
        
        IF @numReferenceType = 1
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Order' AND constFlag=1
        ELSE IF @numReferenceType = 2
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Order' AND constFlag=1
        ELSE IF @numReferenceType = 3
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Sales Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 4
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE numListID=27 AND vcData='Purchase Opportunity' AND constFlag=1
        ELSE IF @numReferenceType = 5 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1
		ELSE IF @numReferenceType = 6 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='RMA' AND constFlag=1 
		ELSE IF @numReferenceType = 7 
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Sales Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 8
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Purchase Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 9
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Refund Receipt' AND constFlag=1 
		ELSE IF @numReferenceType = 10
			SELECT @numBizDocType =  numListItemID FROM dbo.ListDetails WHERE vcData='Credit Memo' AND constFlag=1 
		ELSE IF @numReferenceType = 11
			SELECT  @numBizDocType = numListItemID FROM dbo.ListDetails WHERE vcData='Invoice' AND constFlag=1
	 
        IF @numReferenceType = 1 OR @numReferenceType = 2 OR @numReferenceType = 3 OR @numReferenceType = 4 
		BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,Mst.vcPOppName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 CASE 
							WHEN Mst.intUsedShippingCompany IS NULL 
							THEN '-' 
							WHEN Mst.intUsedShippingCompany = -1 
							THEN 'Will-call' 
							ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) 
						 END AS ShipVia,
						 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
						'' AS vcPackingSlip,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate
						 ,dbo.FormatedDateFromDate(Mst.intpEstimatedCloseDate,@numDomainID) AS vcRequiredDate
						 ,(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
						 ISNULL(vcCustomerPO#,'') vcCustomerPO#,
						 ISNULL(Mst.txtComments,'') vcSOComments,
						 ISNULL(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) AS OrderCreatedDate,
						 '' AS vcVendorInvoice
						 ,ISNULL(dbo.CheckOrderInventoryStatus(Mst.numOppID,Mst.numDomainID,2),'') vcInventoryStatus
						 ,CASE WHEN ISNULL(DM.intDropShip,0)=1 THEN 'Not Available'
							 WHEN ISNULL(DM.intDropShip,0)=2 THEN 'Blank Available'
							 WHEN ISNULL(DM.intDropShip,0)=3 THEN 'Vendor Label'
							 WHEN ISNULL(DM.intDropShip,0)=4 THEN 'Private Label'
							 ELSE '-'
						END AS vcDropShip
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  Mst.numOppID = @numReferenceID AND Mst.numDomainID = @numDomainID
			             
						 EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId,0
             
            END
    
    
        ELSE IF @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 7 OR @numReferenceType = 8 
					OR @numReferenceType = 9 OR @numReferenceType = 10
         BEGIN
         
								DECLARE @numBizDocTempID AS NUMERIC(9)
								DECLARE @numDivisionID AS NUMERIC(9)
         
                                SELECT @numDivisionID=numDivisionID,@numBizDocTempID=(CASE WHEN @numReferenceType = 5 OR @numReferenceType = 6 OR @numReferenceType = 9 OR @numReferenceType = 10 THEN numRMATempID ELSE numBizDocTempID END)				
                                FROM    dbo.ReturnHeader RH
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
    			---------------------
               
                                SELECT  RH.vcBizDocName AS [vcBizDocID],
                                  RH.vcRMA AS OppName,RH.vcBizDocName AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(RH.numCreatedBy,0) AS Owner,
								   CMP.VcCompanyName AS CompName,RH.numContactID,RH.numCreatedBy AS BizDocOwner,
                                        RH.monTotalDiscount AS decDiscount,
                                        ISNULL(RDA.monAmount + RDA.monTotalTax - RDA.monTotalDiscount, 0) AS monAmountPaid,
                                        ISNULL(RH.vcComments, '') AS vcComments,
                                        CONVERT(VARCHAR(20), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) dtCreatedDate,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS numCreatedby,
                                        dbo.fn_GetContactName(RH.numModifiedBy) AS numModifiedBy,
                                        RH.dtModifiedDate,
                                        '' AS numViewedBy,
                                        '' AS dtViewedDate,
                                        0 AS tintBillingTerms,
                                        0 AS numBillingDays,
                                        0 AS [numBillingDaysName],
                                        '' AS vcBillingTermsName,
                                        0 AS tintInterestType,
                                        0 AS fltInterest,
                                        tintReturnType AS tintOPPType,
                                        @numBizDocType AS numBizDocId,
                                        dbo.fn_GetContactName(RH.numCreatedby) AS ApprovedBy,
                                        dtCreatedDate AS dtApprovedDate,
                                        0 AS bintAccountClosingDate,
                                        0 AS tintShipToType,
                                        0 AS tintBillToType,
                                        0 AS tintshipped,
                                        0 AS bintShippedDate,
                                        0 AS monShipCost,
                                        ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
                                        ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath,
                                        RH.numDivisionID,
                                         ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
                                        ISNULL(RH.monTotalDiscount, 0) AS fltDiscount,
                                        0 AS bitDiscountType,
                                        0 AS numShipVia,
                                        '' AS vcTrackingURL,
                                        0 AS numBizDocStatus,
                                        '-' AS BizDocStatus,
                                        '' AS ShipVia,
										'' AS vcShippingService,
										'' AS vcPackingSlip,
                                        ISNULL(C.varCurrSymbol, '') varCurrSymbol,
                                        0 AS ShippingReportCount,
                                        0 bitPartialFulfilment,
                                        vcBizDocName,
                                        dbo.[fn_GetContactName](RH.[numModifiedBy])
                                        + ', '
                                        + CONVERT(VARCHAR(20), DATEADD(MINUTE, -@ClientTimeZoneOffset, RH.dtModifiedDate)) AS ModifiedBy,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OrderRecOwner,
                                        dbo.[fn_GetContactName](DM.[numRecOwner])
                                        + ', '
                                        + dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
                                        RH.dtcreatedDate AS dtFromDate,
                                        0 AS tintTaxOperator,
                                        1 AS monTotalEmbeddedCost,
                                        2 AS monTotalAccountedCost,
                                        ISNULL(BT.bitEnabled, 0) bitEnabled,
                                        ISNULL(BT.txtBizDocTemplate, '') txtBizDocTemplate,
                                        ISNULL(BT.txtCSS, '') txtCSS,
                                        '' AS AssigneeName,
                                        '' AS AssigneeEmail,
                                        '' AS AssigneePhone,
                                        dbo.fn_GetComapnyName(RH.numDivisionId) OrganizationName,
                                        dbo.fn_GetContactName(RH.numContactID) OrgContactName,
                                        dbo.getCompanyAddress(RH.numDivisionId, 1,RH.numDomainId) CompanyBillingAddress,
                                        CASE WHEN ACI.numPhone <> ''
                                             THEN ACI.numPhone
                                                  + CASE WHEN ACI.numPhoneExtension <> ''
                                                         THEN ' - ' + ACI.numPhoneExtension
                                                         ELSE ''
                                                    END
                                             ELSE ''
                                        END OrgContactPhone,
                                        DM.vcComPhone AS OrganizationPhone,
                                        ISNULL(ACI.vcEmail, '') AS OrgContactEmail,
                                        dbo.[fn_GetContactName](RH.[numCreatedBy]) AS OnlyOrderRecOwner,
                                        1 bitAuthoritativeBizDocs,
                                        0 tintDeferred,
                                        0 AS monCreditAmount,
                                        0 AS bitRentalBizDoc,
                                        ISNULL(BT.numBizDocTempID, 0) AS numBizDocTempID,
                                        ISNULL(BT.vcTemplateName, '') AS vcTemplateName,
                                        0 AS monPAmount,
                                        ISNULL(CMP.txtComments, '') AS vcOrganizationComments,
                                        '' AS vcTrackingNo,
                                        '' AS vcShippingMethod,
                                        '' AS dtDeliveryDate,
                                        '' AS vcOppRefOrderNo,
                                        0 AS numDiscountAcntType ,
										0 AS numOppBizDocTempID
                                        ,com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
										,'' AS vcReleaseDate
										,'' AS vcRequiredDate
										,'' AS vcPartner
										,'' vcCustomerPO#
										,'' vcSOComments
										,'' AS vcShippersAccountNo
										,'' AS OrderCreatedDate
										,'' AS vcVendorInvoice
										,'' vcInventoryStatus
										,CASE WHEN ISNULL(DM.intDropShip,0)=1 THEN 'Not Available'
											 WHEN ISNULL(DM.intDropShip,0)=2 THEN 'Blank Available'
											 WHEN ISNULL(DM.intDropShip,0)=3 THEN 'Vendor Label'
											 WHEN ISNULL(DM.intDropShip,0)=4 THEN 'Private Label'
											 ELSE '-'
										END AS vcDropShip
                                FROM    dbo.ReturnHeader RH
                                        JOIN Domain D ON D.numDomainID = RH.numDomainID
                                        JOIN [DivisionMaster] DM ON DM.numDivisionID = RH.numDivisionID
                                        LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = DM.numCurrencyID
                                        LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId
                                                                     AND CMP.numDomainID = @numDomainID
                                        LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = RH.numContactID
                                        LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID
                                                                         AND BT.numBizDocID = @numBizDocType	
                                                                         AND ((BT.numBizDocTempID=ISNULL(@numBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
                                         JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
										 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID,
                                                                         dbo.GetReturnDealAmount(@numReferenceID,@numReferenceType) RDA
                                WHERE   RH.numReturnHeaderID = @numReferenceID
                                
                                	--************Customer/Vendor Billing Address************
                                		SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
											,(CASE 
													WHEN ISNULL(bitAltContact,0) = 1 
													THEN ISNULL(vcAltContact,'') 
													ELSE 
														(CASE 
															WHEN ISNULL(numContact,0) > 0 
															THEN 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
															ELSE 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
															END
														)  
											END) AS vcContact
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1

                                        
                                	--************Customer/Vendor Shipping Address************
                                	SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName,
											(CASE 
													WHEN ISNULL(bitAltContact,0) = 1 
													THEN ISNULL(vcAltContact,'') 
													ELSE 
														(CASE 
															WHEN ISNULL(numContact,0) > 0 
															THEN 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
															ELSE 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
															END
														)  
											END) AS vcContact
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1

                                --************Employer Shipping Address************ 
									SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
									 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
										  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
										  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
									  WHERE  D1.numDomainID = @numDomainID
										AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


								--************Employer Shipping Address************ 
								SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
										isnull(AD.vcStreet,'') AS vcStreet,
										isnull(AD.vcCity,'') AS vcCity,
										isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
										isnull(AD.vcPostalCode,'') AS vcPostalCode,
										isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
										ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
								 FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
									  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
									  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
								 WHERE AD.numDomainID=@numDomainID 
								 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1

								 --************Customer/Vendor Billing Address************
                                		SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName
											,(CASE 
													WHEN ISNULL(bitAltContact,0) = 1 
													THEN ISNULL(vcAltContact,'') 
													ELSE 
														(CASE 
															WHEN ISNULL(numContact,0) > 0 
															THEN 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
															ELSE 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
															END
														)  
											END) AS vcContact
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1

                                        
                                	--************Customer/Vendor Shipping Address************
                                	SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ' ,' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
											isnull(AD.vcStreet,'') AS vcStreet,
											isnull(AD.vcCity,'') AS vcCity,
											isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
											isnull(AD.vcPostalCode,'') AS vcPostalCode,
											isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
											ISNULL(AD.vcAddressName,'') AS vcAddressName,
            								CMP.vcCompanyName,
											(CASE 
													WHEN ISNULL(bitAltContact,0) = 1 
													THEN ISNULL(vcAltContact,'') 
													ELSE 
														(CASE 
															WHEN ISNULL(numContact,0) > 0 
															THEN 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
															ELSE 
																ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
															END
														)  
											END) AS vcContact
										 FROM AddressDetails AD 
										 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
										 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
										 --LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = @numContactID AND ACI.numDivisionId=DM.numDivisionID
										 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
										 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
           END
		
        ELSE IF @numReferenceType = 11
		BEGIN
		      
			---------------------------------------------------------------------------------------
      
				  SELECT Mst.vcPOppName AS vcBizDocID,
						 Mst.vcPOppName AS OppName,OBD.vcBizDocID AS BizDcocName,@numBizDocType AS BizDoc,ISNULL(Mst.numRecOwner,0) AS Owner,
						 CMP.VcCompanyName AS CompName,Mst.numContactID,Mst.numCreatedBy AS BizDocOwner,
						 Mst.fltDiscount AS decDiscount,
						 (SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs WHERE numOppId=MSt.numOppId) AS monAmountPaid,
						 isnull(Mst.txtComments,'') AS vcComments,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) dtCreatedDate,
						 dbo.fn_GetContactName(Mst.numCreatedby) AS numCreatedby,
						 dbo.fn_GetContactName(Mst.numModifiedBy) AS numModifiedBy,
						 Mst.bintModifiedDate AS dtModifiedDate,
						 '' AS numViewedBy,
						 NULL AS dtViewedDate,
						 isnull(Mst.bitBillingTerms,0) AS tintBillingTerms,
						 isnull(Mst.intBillingDays,0) AS numBillingDays,
						 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
						 BTR.vcTerms AS vcBillingTermsName,
						 isnull(Mst.bitInterestType,0) AS tintInterestType,
						 isnull(Mst.fltInterest,0) AS fltInterest,
						 tintOPPType,
						 @numBizDocType AS numBizDocId,
						 Mst.bintAccountClosingDate,
						 tintShipToType,
						 tintBillToType,
						 tintshipped,
						 NULL bintShippedDate,
						 ISNULL(Mst.monShipCost ,0) AS monShipCost,
						 ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
						 ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
						 Mst.numDivisionID,
						  ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter,
						 isnull(Mst.fltDiscount,0) AS fltDiscount,
						 isnull(Mst.bitDiscountType,0) AS bitDiscountType,
						 0 AS numShipVia,
						 '' AS vcTrackingURL,
						 0 AS numBizDocStatus,
						 '' AS BizDocStatus,
						 CASE 
						   WHEN ISNULL(OBD.numShipVia,0) = 0 THEN (CASE WHEN Mst.intUsedShippingCompany IS NULL THEN '-' WHEN Mst.intUsedShippingCompany = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.intUsedShippingCompany) END)
						   WHEN OBD.numShipVia = -1 THEN 'Will-call'
						   ELSE dbo.fn_GetListItemName(OBD.numShipVia)
						 END AS ShipVia,
						 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = Mst.numShippingService),'') AS vcShippingService,
						'' AS vcPackingSlip,
						 ISNULL(C.varCurrSymbol,'') varCurrSymbol,
						 0 AS ShippingReportCount,
						 0 bitPartialFulfilment,
						 vcPOppName as vcBizDocName,
						 dbo.[fn_GetContactName](Mst.[numModifiedBy])  + ', '
							+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Mst.bintModifiedDate)) AS ModifiedBy,
						 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
						 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
						 Mst.bintCreatedDate AS dtFromDate,
						 Mst.tintTaxOperator,
						 0 AS monTotalEmbeddedCost,
						 0 AS monTotalAccountedCost,
						 ISNULL(BT.bitEnabled,0) bitEnabled,
						 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
						 ISNULL(BT.txtCSS,'') txtCSS,
						 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
						 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
						 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
						 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
						 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,
						 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
						 DM.vcComPhone as OrganizationPhone,
						 ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 						 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 						 0 bitAuthoritativeBizDocs,
						0 tintDeferred,0 AS monCreditAmount,
						 0 as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
						 [dbo].[GetDealAmount](Mst.numOppId ,getutcdate(),0 ) as monPAmount,
						 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
						 isnull(CMP.txtComments,'') as vcOrganizationComments,
						 ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Mst.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,'' AS  vcShippingMethod,
						 NULL AS dtDeliveryDate,ISNULL(Mst.vcOppRefOrderNo,'') AS vcOppRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType ,
						 isnull(numOppBizDocTempID,0) as numOppBizDocTempID,
						 com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone
						 ,dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate
						 ,dbo.FormatedDateFromDate(Mst.intpEstimatedCloseDate,@numDomainID) AS vcRequiredDate
						 ,(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner,
						 ISNULL(Mst.vcCustomerPO#,'') vcCustomerPO#,
						 ISNULL(Mst.txtComments,'') vcSOComments,
						 ISNULL(DM.vcShippersAccountNo,'') AS vcShippersAccountNo,
						 CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,Mst.bintCreatedDate)) AS OrderCreatedDate,
						 (CASE WHEN ISNULL(OBD.numSourceBizDocId,0) > 0 THEN ISNULL((SELECT vcVendorInvoice FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=OBD.numSourceBizDocId),'') ELSE ISNULL(vcVendorInvoice,'') END) vcVendorInvoice,
						 (CASE 
							WHEN ISNULL(OBD.numSourceBizDocId,0) > 0
							THEN ISNULL((SELECT vcBizDocID FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppBizDocsId=OBD.numSourceBizDocId),'')
							ELSE ''
						END) vcPackingSlip
						,ISNULL(dbo.CheckOrderInventoryStatus(Mst.numOppID,Mst.numDomainID,2),'') vcInventoryStatus,
			CASE WHEN ISNULL(DM.intDropShip,0)=1 THEN 'Not Available'
				 WHEN ISNULL(DM.intDropShip,0)=2 THEN 'Blank Available'
				 WHEN ISNULL(DM.intDropShip,0)=3 THEN 'Vendor Label'
				 WHEN ISNULL(DM.intDropShip,0)=4 THEN 'Private Label'
				 ELSE '-'
			END AS vcDropShip
				  FROM    OpportunityMaster Mst JOIN Domain D ON D.numDomainID = Mst.numDomainID
					LEFT JOIN OpportunityBizDocs AS OBD ON Mst.numOppid=OBD.numOppId
						  LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
						  LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
						  LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
						  LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
						  LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = @numBizDocType 
							 AND ((BT.numBizDocTempID=ISNULL(Mst.numOppBizDocTempID, 0)) OR (ISNULL(BT.bitDefault,0)=1 AND ISNULL(BT.bitEnabled,0)=1))  
						  LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
						  JOIN divisionmaster div1 ON D.numDivisionID = div1.numDivisionID
						  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
				  WHERE  OBD.numOppBizDocsId = @numReferenceID AND  Mst.numDomainID = @numDomainID
				  SET @numReferenceType=1
			      SET @numReferenceID=(SELECT TOP 1 numOppId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numReferenceID)
					EXEC USP_OPPGetOppAddressDetails @numReferenceID,@numDomainId,0	
             
            END
    END

GO
--exec USP_GetMirrorBizDocItems @numReferenceID=37056,@numReferenceType=1,@numDomainID=1