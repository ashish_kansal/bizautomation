GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostExpenseAccountGet')
DROP PROCEDURE USP_LandedCostExpenseAccountGet
GO
CREATE PROC [dbo].[USP_LandedCostExpenseAccountGet] 
    @numDomainId NUMERIC(18, 0)
AS 
	SELECT [numLCExpenseId],LCEA. [numDomainId], LCEA.[vcDescription], LCEA.[numAccountId], COA.[vcAccountName]
	FROM   [dbo].[LandedCostExpenseAccount] LCEA JOIN [dbo].[Chart_Of_Accounts] AS COA
	ON [LCEA].[numAccountId] = [COA].[numAccountId] AND LCEA.[numDomainId] = COA.[numDomainId]
	WHERE  LCEA.numDomainId = @numDomainId


