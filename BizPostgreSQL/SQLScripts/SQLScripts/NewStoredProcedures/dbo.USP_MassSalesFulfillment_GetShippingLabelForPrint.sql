GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetShippingLabelForPrint')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetShippingLabelForPrint
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetShippingLabelForPrint]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
)
AS
BEGIN
	SELECT 
		vcShippingLabelImage
	FROM
		ShippingReport
	INNER JOIN
		ShippingBox
	ON
		ShippingReport.numShippingReportId = ShippingBox.numShippingReportId
	WHERE
		ShippingReport.numDomainId = @numDomainID
		AND ShippingReport.numOppID=@numOppID
		AND ShippingReport.numOppBizDocId=@numOppBizDocID
		AND LEN(ISNULL(vcShippingLabelImage,'')) > 0
END
GO