GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateDueDate')
DROP PROCEDURE USP_UpdateDueDate
GO
CREATE PROCEDURE USP_UpdateDueDate
    @dtDueDate DATETIME,
    @numOppBizDocsId AS NUMERIC(9),
    @numOppId AS NUMERIC(9)
AS 
    BEGIN
        DECLARE @bitBillingTerms BIT ;
        DECLARE @intBillingDays INT ;
        
        SELECT  @bitBillingTerms = om.[bitBillingTerms],
                --@intBillingDays = ISNULL(dbo.fn_GetListItemName(isnull(om.intBillingDays,0)),0)
                @intBillingDays = ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)
        FROM    [OpportunityBizDocs] BD JOIN dbo.OpportunityMaster om ON BD.numOppID=om.numOppID
        WHERE   [numOppBizDocsId] = @numOppBizDocsId AND om.numOppID = @numOppId
        
        
        UPDATE  [OpportunityBizDocs]
        SET     dtFromDate = DATEADD(DAY, -@intBillingDays, @dtDueDate)
        WHERE   [numOppBizDocsId] = @numOppBizDocsId AND numOppID = @numOppId
        
        
     UPDATE GJH SET datEntry_Date=CONVERT(VARCHAR(10),OBD.dtFromDate,111)
		FROM dbo.OpportunityBizDocs OBD JOIN dbo.General_Journal_Header GJH ON OBD.numOppId=GJH.numOppId
			AND obd.numOppBizDocsId=GJH.numOppBizDocsId WHERE 
			OBD.numOppID = @numOppId AND OBD.numOppBizDocsId = @numOppBizDocsId
			AND ISNULL(Gjh.numBillID,0)=0 AND ISNULL(gjh.numBillPaymentID,0)=0 AND ISNULL(GJH.numPayrollDetailID,0)=0 
			AND ISNULL(Gjh.numCheckHeaderID,0)=0 AND ISNULL(gjh.numReturnID,0)=0 
			AND ISNULL(GJH.numDepositId,0)=0 
        
    END