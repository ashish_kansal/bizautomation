-- exec usp_RemoveStagePercentageDetails @numDomainid=1,@numProjectID=200,@tintModeType=1

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_RemoveStagePercentageDetails')
DROP PROCEDURE usp_RemoveStagePercentageDetails
GO
CREATE PROCEDURE [dbo].[usp_RemoveStagePercentageDetails]
    @numDomainid NUMERIC = 0,
    @numProjectID NUMERIC = 0,
	@tintModeType TINYINT=-1 
AS 


IF EXISTS ( SELECT  *
            FROM    dbo.ProjectsOpportunities
            WHERE   numDomainId = @numDomainid
                    AND @numProjectID = ( CASE @tintModeType
                                            WHEN 0 THEN numOppId
                                            WHEN 1 THEN numProId
                                          END ) ) 
    BEGIN
        RAISERROR ( 'DEPENDANT', 16, 1 );
        RETURN;
    END			


BEGIN TRY 
	BEGIN TRAN

	  DELETE    FROM dbo.StageAccessDetail
	  WHERE     @numProjectID = ( CASE @tintModeType
									WHEN 0 THEN numOppID
									WHEN 1 THEN numProjectID
								  END ) 
								  
					  
			
								  
		DELETE FROM dbo.ProjectsOpportunities
		WHERE numDomainId=@numDomainid AND @numProjectID = ( CASE @tintModeType
									WHEN 0 THEN numOppId
									WHEN 1 THEN numProId
								  END ) 
		
		DELETE FROM dbo.ProjectTeamRights
		WHERE @numProjectID = ( CASE @tintModeType
									WHEN 0 THEN numOppId
									WHEN 1 THEN numProId
								  END ) 
			
		DELETE FROM dbo.DocumentWorkflow WHERE numDocID IN 
			(SELECT numGenericDocID FROM dbo.GenericDocuments WHERE vcDocumentSection='PS' 
			AND numRecID IN (SELECT numStageDetailsId FROM stagepercentagedetails 
			WHERE @numProjectID=(case @tintModeType when 0 then numOppID 
						when 1 then numProjectID end) AND numDomainID = @numDomainID) AND numDomainID=@numDomainid) 
		
		DELETE FROM dbo.GenericDocuments WHERE vcDocumentSection='PS' AND 
			numRecID IN (SELECT numStageDetailsId FROM stagepercentagedetails 
			WHERE @numProjectID=(case @tintModeType when 0 then numOppID 
						when 1 then numProjectID end) AND numDomainID = @numDomainID) AND numDomainID=@numDomainid
		
		delete from StageDependency where numStageDetailID in (select numStageDetailsID from stagepercentagedetails where  
			@numProjectID=(case @tintModeType when 0 then numOppID 
						when 1 then numProjectID end) and numDomainId=@numDomainId) or numDependantOnID in (select numStageDetailsID from stagepercentagedetails where  
			@numProjectID=(case @tintModeType when 0 then numOppID 
						when 1 then numProjectID end) and numDomainId=@numDomainId)

		delete from Comments where  
			numStageId in (select numStageDetailsID from stagepercentagedetails where  
			@numProjectID=(case when @tintModeType=0 then numOppID 
						when @tintModeType=1 then numProjectID end) and numDomainId=@numDomainId)

		delete from stagepercentagedetails where  
			@numProjectID=(case when @tintModeType=0 then numOppID 
						when @tintModeType=1 then numProjectID end) and numDomainId=@numDomainId

		delete from ProjectProgress WHERE  @numProjectID=(case @tintModeType when 0 then numOppID 
			when 1 then numProID end)  AND numDomainId=@numDomainID			
	
		IF @tintModeType=0
			UPDATE dbo.OpportunityMaster SET numBusinessProcessID=NULL WHERE numOppID=@numProjectID and numDomainId=@numDomainId
	
	
	COMMIT TRAN
END TRY 
BEGIN CATCH		
PRINT @@ERROR
PRINT ERROR_MESSAGE() 
    IF (@@TRANCOUNT > 0) 
    BEGIN
        PRINT 'Unexpected error occurred!'
        ROLLBACK TRAN
        RETURN 1
    END
END CATCH	

