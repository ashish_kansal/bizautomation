
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageAlertConfig]    Script Date: 11/02/2011 12:44:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageAlertConfig')
DROP PROCEDURE USP_ManageAlertConfig
GO
CREATE PROC [dbo].[USP_ManageAlertConfig] 
    @numAlertConfigId numeric(18, 0) OUTPUT ,
    @numDomainId numeric(18, 0),
    @numContactId numeric(9, 0),
    @bitOpenActionItem bit,
    @vcOpenActionMsg varchar(15),
    @bitOpenCases bit,
    @vcOpenCasesMsg varchar(15),
    @bitOpenProject bit,
    @vcOpenProjectMsg varchar(15),
    @bitOpenSalesOpp bit,
    @vcOpenSalesOppMsg varchar(15),
    @bitOpenPurchaseOpp bit,
    @vcOpenPurchaseOppMsg varchar(15),
    @bitBalancedue bit,
    @monBalancedue DECIMAL(20,5),
    @vcBalancedueMsg varchar(15),
    @bitUnreadEmail bit,
    @intUnreadEmail int,
    @vcUnreadEmailMsg varchar(15),
    @bitPurchasedPast bit,
    @monPurchasedPast DECIMAL(20,5),
    @vcPurchasedPastMsg varchar(15),
    @bitSoldPast bit,
    @monSoldPast DECIMAL(20,5),
    @vcSoldPastMsg varchar(15),
    @bitCustomField bit,
    @numCustomFieldId numeric(9, 0),
    @vcCustomFieldValue varchar(50),
    @vcCustomFieldMsg varchar(15),
	@bitCampaign BIT
AS 
DECLARE @numCount AS NUMERIC
SET @numCount = 0
SELECT @numCount=COUNT(*) FROM dbo.AlertConfig WHERE numDomainId =@numDomainId AND numContactId=@numContactId
PRINT @numCount 
IF @numCount > 0 
    BEGIN
 UPDATE [dbo].[AlertConfig]
 SET    [numDomainId] = @numDomainId, [numContactId] = @numContactId, [bitOpenActionItem] = @bitOpenActionItem, [vcOpenActionMsg] = @vcOpenActionMsg, 
 [bitOpenCases] = @bitOpenCases, [vcOpenCasesMsg] = @vcOpenCasesMsg, [bitOpenProject] = @bitOpenProject, [vcOpenProjectMsg] = @vcOpenProjectMsg, 
 [bitOpenSalesOpp] = @bitOpenSalesOpp, [vcOpenSalesOppMsg] = @vcOpenSalesOppMsg, [bitOpenPurchaseOpp] = @bitOpenPurchaseOpp,
 [vcOpenPurchaseOppMsg] = @vcOpenPurchaseOppMsg, [bitBalancedue] = @bitBalancedue, [monBalancedue] = @monBalancedue, 
 [vcBalancedueMsg] = @vcBalancedueMsg,[bitUnreadEmail] = @bitUnreadEmail, [intUnreadEmail] = @intUnreadEmail, [vcUnreadEmailMsg] = @vcUnreadEmailMsg, [bitPurchasedPast] = @bitPurchasedPast, [monPurchasedPast] = @monPurchasedPast, [vcPurchasedPastMsg] = @vcPurchasedPastMsg, [bitSoldPast] = @bitSoldPast, [monSoldPast] = @monSoldPast, [vcSoldPastMsg] = @vcSoldPastMsg, [bitCustomField] = @bitCustomField, [numCustomFieldId] = @numCustomFieldId, [vcCustomFieldValue] = @vcCustomFieldValue, [vcCustomFieldMsg] = @vcCustomFieldMsg,
 bitCampaign = @bitCampaign
 WHERE numDomainId =@numDomainId AND numContactId=@numContactId  --[numAlertConfigId] = @numAlertConfigId
     END 
else 
 BEGIN
 INSERT INTO [dbo].[AlertConfig] ([numDomainId], [numContactId], [bitOpenActionItem], [vcOpenActionMsg], [bitOpenCases], [vcOpenCasesMsg], [bitOpenProject], [vcOpenProjectMsg], [bitOpenSalesOpp], [vcOpenSalesOppMsg], [bitOpenPurchaseOpp], [vcOpenPurchaseOppMsg], [bitBalancedue], [monBalancedue], [vcBalancedueMsg], [bitUnreadEmail], [intUnreadEmail], [vcUnreadEmailMsg], [bitPurchasedPast], [monPurchasedPast], [vcPurchasedPastMsg], [bitSoldPast], [monSoldPast], [vcSoldPastMsg], [bitCustomField], [numCustomFieldId], [vcCustomFieldValue], [vcCustomFieldMsg], [bitCampaign])
  SELECT @numDomainId, @numContactId, @bitOpenActionItem, @vcOpenActionMsg, @bitOpenCases, @vcOpenCasesMsg, @bitOpenProject, @vcOpenProjectMsg, @bitOpenSalesOpp, @vcOpenSalesOppMsg, @bitOpenPurchaseOpp, @vcOpenPurchaseOppMsg, @bitBalancedue, @monBalancedue, @vcBalancedueMsg,@bitUnreadEmail, @intUnreadEmail, @vcUnreadEmailMsg, @bitPurchasedPast, @monPurchasedPast, @vcPurchasedPastMsg, @bitSoldPast, @monSoldPast, @vcSoldPastMsg, @bitCustomField, @numCustomFieldId, @vcCustomFieldValue, @vcCustomFieldMsg, @bitCampaign
  
  SET @numAlertConfigId= SCOPE_IDENTITY()
 END 
 
