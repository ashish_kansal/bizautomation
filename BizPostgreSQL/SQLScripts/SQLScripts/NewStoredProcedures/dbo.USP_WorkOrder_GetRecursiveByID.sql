SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetRecursiveByID')
DROP PROCEDURE USP_WorkOrder_GetRecursiveByID
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetRecursiveByID]  
	@numDomainID NUMERIC(18,0),
	@numWOID NUMERIC(18,0)
AS
BEGIN
	WITH CTE(numWOID,numLevel) AS
	(
		SELECT
			numWOId,
			1
		FROM
			WorkOrder
		WHERE
			numDomainID=@numDomainID
			AND numWOId=@numWOID
		UNION ALL
		SELECT
			WorkOrder.numWOId,
			C.numLevel + 1
		FROM
			WorkOrder
		INNER JOIN
			CTE C
		ON
			WorkOrder.numParentWOID=C.numWOID
	)

	SELECT * FROM CTE ORDER BY numLevel DESC
END