GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetItems')
DROP PROCEDURE dbo.USP_ElasticSearch_GetItems
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetItems]
	@numDomainID NUMERIC(18,0)
	,@vcItemCodes VARCHAR(MAX) = NULL
AS 
BEGIN
	DECLARE @CustomerSearchFields AS VARCHAR(MAX) = ''
	DECLARE @CustomerSearchCustomFields AS VARCHAR(MAX) = ''

	DECLARE @CustomerDisplayFields AS VARCHAR(MAX) = ''

	DECLARE @query AS NVARCHAR(MAX);

	----------------------------   Search Fields ---------------------------

	DECLARE @TEMPSearchFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,vcDbColumnName VARCHAR(200)
		,tintOrder TINYINT
		,bitCustom BIT
	)

	INSERT INTO @TEMPSearchFields
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT    
		numFieldId,
		vcDbColumnName,
        tintRow AS tintOrder,
        0 AS Custom
    FROM 
		View_DynamicColumns
    WHERE     
		numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 0
        AND ISNULL(bitSettingField, 0) = 1
        AND numRelCntType = 1
    UNION
    SELECT 
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		1 AS Custom
    FROM 
		View_DynamicCustomColumns
    WHERE 
		Grp_id = 5
        AND numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 1
        AND numRelCntType = 1

	IF NOT EXISTS ( SELECT * FROM @TEMPSearchFields )
    BEGIN
        INSERT INTO @TEMPSearchFields
		(
			numFieldID
			,vcDbColumnName
			,tintOrder
			,bitCustom
		)
        SELECT  
			numFieldId ,
			vcDbColumnName,
			tintorder,
			0
        FROM 
			View_DynamicDefaultColumns
        WHERE 
			numFormId = 22
            AND bitDefault = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND numDomainID = @numDomainID 
    END

	IF EXISTS (SELECT * FROM @TEMPSearchFields)
	BEGIN
		SELECT 
			@CustomerSearchFields = COALESCE (@CustomerSearchFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ' AS Search_' + CAST(vcDbColumnName AS VARCHAR(100)) + ', '	
		FROM 
			@TEMPSearchFields 
		WHERE
			ISNULL(bitCustom,0) = 0
		ORDER BY
			tintOrder ASC
		
		-- Removes last , from string
		IF DATALENGTH(@CustomerSearchFields) > 0
			SET @CustomerSearchFields = LEFT(@CustomerSearchFields, LEN(@CustomerSearchFields) - 1)

		IF EXISTS(SELECT * FROM @TEMPSearchFields WHERE ISNULL(bitCustom,0) = 1)
		BEGIN
			SELECT 
				@CustomerSearchCustomFields = COALESCE (@CustomerSearchCustomFields,'') + 'ISNULL(CFW' + CAST(numFieldId AS VARCHAR(100)) + ','''') AS ' + 'Search_CFW' + CAST(numFieldId AS VARCHAR(100)) + ','
			FROM 
				@TEMPSearchFields 
			WHERE
				ISNULL(bitCustom,0) = 1
			ORDER BY
				tintOrder ASC
		END

		-- Removes last , from string
		IF DATALENGTH(@CustomerSearchCustomFields) > 0
			SET @CustomerSearchCustomFields = LEFT(@CustomerSearchCustomFields, LEN(@CustomerSearchCustomFields) - 1)
	END

	IF  LEN(@CustomerSearchFields) = 0 AND LEN(@CustomerSearchCustomFields) = 0
	BEGIN
		SET @CustomerSearchFields = 'vcItemName AS Search_vcItemName'
	END


	----------------------------   Display Fields ---------------------------
	DECLARE @TEMPDisplayFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,vcDbColumnName VARCHAR(200)
		,tintOrder TINYINT
		,bitCustom BIT
	)


	INSERT INTO @TEMPDisplayFields
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT    
		numFieldId,
		vcDbColumnName,
        tintRow AS tintOrder,
        0 AS Custom
    FROM 
		View_DynamicColumns
    WHERE     
		numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 0
        AND ISNULL(bitSettingField, 0) = 1
        AND numRelCntType = 0
		AND vcDbColumnName <> 'CustomerPartNo'
    UNION
    SELECT 
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		1 AS Custom
    FROM 
		View_DynamicCustomColumns
    WHERE 
		Grp_id = 5
        AND numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 1
        AND numRelCntType = 0


	IF EXISTS (SELECT * FROM @TEMPDisplayFields)
	BEGIN
		SELECT 
			@CustomerDisplayFields = COALESCE (@CustomerDisplayFields,'') + CONCAT('(CASE WHEN LEN(',vcDbColumnName,') > 0 THEN CONCAT(',(CASE WHEN LEN(@CustomerDisplayFields) > 0 THEN ''', '',' ELSE ''''',' END),vcDbColumnName, ') ELSE '''' END)') + ', '	
		FROM 
			@TEMPDisplayFields 
		WHERE
			ISNULL(bitCustom,0) = 0
		ORDER BY
			tintOrder ASC

		IF EXISTS(SELECT * FROM @TEMPDisplayFields WHERE ISNULL(bitCustom,0) = 1)
		BEGIN
			SELECT 
				@CustomerDisplayFields = COALESCE (@CustomerDisplayFields,'') + CONCAT('(CASE WHEN LEN(',CONCAT('CFW',numFieldId),') > 0 THEN CONCAT(',(CASE WHEN LEN(@CustomerDisplayFields) > 0 THEN ''', '',' ELSE ''''',' END),CONCAT('CFW',numFieldId), ') ELSE '''' END)') + ', '	
			FROM 
				@TEMPDisplayFields 
			WHERE
				ISNULL(bitCustom,0) = 1
			ORDER BY
				tintOrder ASC
		END

		-- Removes last , from string
		IF DATALENGTH(@CustomerDisplayFields) > 0
			SET @CustomerDisplayFields = LEFT(@CustomerDisplayFields, LEN(@CustomerDisplayFields) - 1)
	END

	IF  LEN(@CustomerDisplayFields) = 0
	BEGIN
		SET @CustomerDisplayFields = 'vcItemName'
	END

	DECLARE @CustomFields AS VARCHAR(1000)

	SELECT @CustomFields = COALESCE (@CustomFields,'') + 'CFW' + CAST(FLD_ID AS VARCHAR) + ', ' FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
	-- Removes last , from string
	IF DATALENGTH(@CustomFields) > 0
		SET @CustomFields = LEFT(@CustomFields, LEN(@CustomFields) - 1)

	SELECT @query = CONCAT('SELECT numItemCode as id,''item'' As module, CONCAT(''/Items/frmKitDetails.aspx?ItemCode='',numItemCode) AS url,CONCAT(''<b style="color:#ff9900">Item ('',(CASE charItemType WHEN ''P'' THEN ''Inventory''  WHEN ''S'' THEN ''Service'' WHEN ''A'' THEN ''Asset'' ELSE ''Non Inventory'' END),'':)</b> '',',@CustomerDisplayFields,') AS displaytext, CONCAT(''Item ('',(CASE charItemType WHEN ''P'' THEN ''Inventory''  WHEN ''S'' THEN ''Service'' WHEN ''A'' THEN ''Asset'' ELSE ''Non Inventory'' END),''): '',',@CustomerDisplayFields,') AS text',
	CASE @CustomerSearchFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSearchFields
	END
	,
	CASE @CustomerSearchCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSearchCustomFields
	END
	 ,' FROM (SELECT 
				Item.numItemCode
				,charItemType
				,ISNULL(vcItemName,'''') AS vcItemName
				,ISNULL(txtItemDesc,'''') AS txtItemDesc
				,ISNULL(vcModelID,'''') AS vcModelID
				,ISNULL(vcSKU,'''') AS vcSKU
				,ISNULL(numBarCodeId,'''') AS numBarCodeId
				,ISNULL(CompanyInfo.vcCompanyName,'''') vcCompanyName
				,(CASE WHEN charItemType=''p'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numItemID=Item.numItemCode AND ISNULL(monWListPrice,0) > 0),ISNULL(monListPrice,0)) ELSE ISNULL(monListPrice,0) END) AS monListPrice
				,ISNULL(Vendor.vcPartNo,'''') vcPartNo
				,STUFF((SELECT '','' + CustomerPartNo FROM CustomerPartNumber WHERE numItemCode=Item.numItemCode FOR xml path ('''')), 1, 1, '''') CustomerPartNo
				,(CASE WHEN numItemGroup > 0 AND ISNULL(bitMatrix,0) = 1 THEN dbo.fn_GetItemAttributes(Item.numDomainID,Item.numItemCode,0) ELSE '''' END) AS vcAttributes
				,(CASE WHEN charItemType=''p'' AND numItemGroup > 0 AND ISNULL(bitMatrix,0) = 0 THEN STUFF((SELECT '', '' + vcWHSKU FROM WareHouseItems WHERE numItemID = Item.numItemCode FOR XML PATH('''')),1,2,'''') ELSE '''' END) AS vcWHSKU
				,(CASE WHEN charItemType=''p'' AND numItemGroup > 0 AND ISNULL(bitMatrix,0) = 0 THEN STUFF((SELECT '', '' + vcBarCode FROM WareHouseItems WHERE numItemID = Item.numItemCode FOR XML PATH('''')),1,2,'''') ELSE '''' END) AS vcBarCode
				' + (CASE WHEN DATALENGTH(@CustomFields) > 0 THEN ',CustomFields.*' ELSE '' END) + '
			FROM 
				Item
			LEFT JOIN 
				Vendor
			ON
				Item.numVendorID=Vendor.numVendorID
				AND Item.numItemCode=Vendor.numItemCode
			LEFT JOIN 
				DivisionMaster
			ON
				Vendor.numVendorID = DivisionMaster.numDivisionID
			LEFT JOIN 
				CompanyInfo
			ON
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
			' + (CASE WHEN DATALENGTH(@CustomFields) > 0 THEN CONCAT('OUTER APPLY
			(
				SELECT 
				* 
				FROM 
				(
					SELECT  
						CONCAT(''CFW'',CFW_Fld_Values_Item.Fld_ID) Fld_ID,
						ISNULL(dbo.fn_GetCustFldStringValue(CFW_Fld_Values_Item.Fld_ID, CFW_Fld_Values_Item.RecId, CFW_Fld_Values_Item.Fld_Value),'''') Fld_Value 
					FROM 
						CFW_Fld_Master
					LEFT JOIN 
						CFW_Fld_Values_Item
					ON
						CFW_Fld_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
					WHERE 
						RecId = Item.numItemCode
				) p PIVOT (MAX([Fld_Value]) FOR Fld_ID  IN (',@CustomFields,') ) AS pvt
			) CustomFields') ELSE '' END) + '
			WHERE
				Item.numDomainID =',@numDomainID,' AND (Item.numItemCode IN (',ISNULL(@vcItemCodes,'0'),') OR LEN(''',@vcItemCodes,''') = 0)) TEMP1')

	PRINT @query

	EXEC SP_EXECUTESQL @query
END