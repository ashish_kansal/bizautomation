Go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateBizDocsShippingCost' ) 
    DROP PROCEDURE USP_UpdateBizDocsShippingCost
GO
CREATE PROCEDURE [dbo].[USP_UpdateBizDocsShippingCost]
    @numOppBizDocsId NUMERIC(18),
    @numShippingReportId NUMERIC(18)
AS 
    BEGIN

--Update Shipping report line items where each box has only single item
--   UPDATE dbo.ShippingReportItems
--   SET monShippingRate = ISNULL(X.monShippingRate,0),dtDeliveryDate = X.dtDeliveryDate
--   from (SELECT SB.monShippingRate,SB.numBoxID,SRI.ShippingReportItemId,SRI.dtDeliveryDate FROM  dbo.ShippingBox SB
--   INNER JOIN dbo.ShippingReportItems SRI
--   ON SRI.numBoxID=SB.numBoxID
--    WHERE SB.numShippingReportId =@numShippingReportId
--	GROUP BY SB.numBoxID,SB.monShippingRate,SRI.ShippingReportItemId,SRI.dtDeliveryDate
--	HAVING (COUNT(SRI.ShippingReportItemId)=1) ) X 
--	WHERE X.ShippingReportItemId = ShippingReportItems.ShippingReportItemId
--
--   
--
--
--    UPDATE  OBI
--    SET     OBI.vcShippingMethod = ( SELECT vcServiceName
--                                     FROM   dbo.ShippingServiceTypes
--                                     WHERE  intNsoftEnum = SRI.tintServiceType AND numDomainID=0
--                                   ),
--            OBI.monShipCost = isnull(SRI.monShippingRate,0),
--            OBI.dtDeliveryDate = SRI.dtDeliveryDate
--    FROM    OpportunityBizDocItems OBI
--            JOIN dbo.ShippingReportItems SRI ON SRI.numOppBizDocItemID = OBI.numOppBizDocItemID
--    WHERE   OBI.numOppBizDocID = @numOppBizDocsId AND SRI.numShippingReportId = @numShippingReportId

--
--        DECLARE @numOppBizDocItemID NUMERIC(18, 0)
--        DECLARE @vcBoxIDs AS VARCHAR(MAX)  
--        DECLARE @vcTrackingNo AS VARCHAR(MAX)   
--    
--        --SET @numOppBizDocsId = 44177
--        --SET @numShippingReportId = 928
--        CREATE TABLE #tmpBizItemID
--            (
--              ID NUMERIC(18, 0),
--              numOppBizDocItemID NUMERIC(18, 0)
--            )
--    
--        INSERT  INTO #tmpBizItemID
--                (
--                  ID,
--                  numOppBizDocItemID 
--                )
--                SELECT  ROW_NUMBER() OVER ( ORDER BY numOppBizDocItemID ),
--                        numOppBizDocItemID
--                FROM    OpportunityBizDocItems
--                WHERE   numOppBizDocID = @numOppBizDocsId 
----        SELECT  *
----        FROM    #tmpBizItemID 
--    
--        DECLARE @itemCount AS INT
--        DECLARE @intCnt AS INT
--        DECLARE @intrTrimCnt AS INT
--    
--        SET @intCnt = 0
--        SET @intrTrimCnt = 0
--    
--        SELECT  @itemCount = COUNT(*)
--        FROM    #tmpBizItemID 
--        
--        SELECT  @vcTrackingNo = ISNULL(vcTrackingNumber, '')
--        FROM    dbo.ShippingBox
--        WHERE   numShippingReportId = @numShippingReportId
--                AND ISNULL(bitIsMasterTrackingNo, 0) = 1               
--        PRINT 'TRACKING NO :' + CAST(@vcTrackingNo AS VARCHAR(MAX))
--                            
--        WHILE( @itemCount > @intCnt )
--            BEGIN
--                SET @vcBoxIDs = ''
--                SET @intCnt = @intCnt + 1
--			
--                SELECT  @numOppBizDocItemID = numOppBizDocItemID
--                FROM    #tmpBizItemID
--                WHERE   ID = @intCnt
--			
--                SELECT  *
--                FROM    dbo.ShippingReportItems
--                WHERE   numOppBizDocItemID = @numOppBizDocItemID
--                        AND numShippingReportId = @numShippingReportId
--			
--                SELECT  @vcBoxIDs = COALESCE(@vcBoxIDs, '')
--                        + CONVERT(VARCHAR(MAX), ISNULL(numBoxID, 0)) + ','
--                FROM    dbo.ShippingReportItems
--                WHERE   numShippingReportId = @numShippingReportId
--                        AND numOppBizDocItemID = @numOppBizDocItemID
--                
--                IF EXISTS ( SELECT  ID
--                            FROM    dbo.SplitIDs(@vcBoxIDs, ',') ) 
--                    BEGIN				
--                        IF EXISTS ( SELECT  DATA
--                                    FROM    dbo.SplitString(@vcTrackingNo, ',') ) 
--                            BEGIN
--                                PRINT 'LENGTH OF TN:' + CAST((LEN(@vcTrackingNo) ) AS VARCHAR(10))
--                                PRINT 'TN : ' + @vcTrackingNo	
--                                    
--                                UPDATE  OpportunityBizDocItems
--                                SET     vcTrackingNo = @vcTrackingNo
--                                WHERE   numOppBizDocItemID = @numOppBizDocItemID
--                                        AND numOppBizDocID = @numOppBizDocsId	
--                            END
--                        ELSE 
--                            BEGIN
--                                CONTINUE				
--                            END	
--                    END
--                ELSE 
--                    BEGIN
--                        CONTINUE	
--                    END
--                
--			--SELECT * FROM OpportunityBizDocItems WHERE numOppBizDocItemID = @numOppBizDocItemID  
--            END
--        DROP TABLE #tmpBizItemID 
    
--    
--        DECLARE @monShipCost DECIMAL(20,5)
--    --SELECT @monShipCost = SUM(monShippingRate) FROM dbo.ShippingReportItems WHERE numShippingReportId=@numShippingReportId;
--        SELECT  @monShipCost = SUM(ISNULL(monShippingRate, 0))
--        FROM    dbo.ShippingBox
--        WHERE   numShippingReportId = @numShippingReportId

		DECLARE @listStr VARCHAR(500)
		SELECT @listStr = COALESCE(@listStr+',' ,'') + vcTrackingNumber
		FROM dbo.ShippingBox
		WHERE numShippingReportId = @numShippingReportId
		PRINT @listStr

		DECLARE @numShipVia AS NUMERIC
		SELECT @numShipVia = numShippingCompany FROM dbo.ShippingReport WHERE numShippingReportId = @numShippingReportId


        UPDATE  [OpportunityBizDocs]
        SET     vcTrackingNo = @listStr,numShipVia = @numShipVia
        WHERE   [numOppBizDocsId] = @numOppBizDocsId 

    END 
