SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
go

-- exec USP_GetTimeAndExpenseDetail @numProId=200,@numDomainID=1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTimeAndExpenseDetail')
DROP PROCEDURE USP_GetTimeAndExpenseDetail
GO
CREATE PROCEDURE [dbo].[USP_GetTimeAndExpenseDetail]
    @tintMode TINYINT = 0,
    @numProId NUMERIC(9),
    @numStageID NUMERIC(9),
    @numDivisionID NUMERIC(9),
    @numUserCntID NUMERIC(9),
    @numDomainID NUMERIC(9)
AS 
BEGIN
    IF @tintMode = 0 
        BEGIN
        
            SELECT  TE.[numCategoryHDRID],
                    'Billable Time' AS TimeType,
                    I.[vcItemName],
                    OM.[vcPOppName],
                    CASE numType
                      WHEN 1 THEN ISNULL(OPP.numUnitHour, 0)
                      ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),
                                  0)
                    END AS [numUnitHour],
                    ISNULL(OPP.[monPrice], 0) monPrice,
                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
                    dbo.fn_GetContactName(TE.[numUserCntID]) AS ContactName,TE.numOppId
            FROM    [TimeAndExpense] TE
                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
            WHERE   [numProId] = @numProId
                    AND TE.numStageId = @numStageID
                    AND TE.numDivisionID = @numDivisionID
--                    AND TE.numUserCntID = @numUserCntID
                    AND TE.[numDomainID] = @numDomainID
                    AND numType =1 --Billable
			UNION 
			
			SELECT  TE.[numCategoryHDRID],
                    'Non-Billable Time' AS TimeType,
                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
                    '' [vcBizDocID],
                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
                    AS [numUnitHour],
                    0 monPrice,
                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
                    dbo.fn_GetContactName(TE.[numUserCntID]) AS ContactName,0 as numOppId
            FROM    [TimeAndExpense] TE
            WHERE   [numProId] = @numProId
                    AND TE.numStageId = @numStageID
                    AND TE.numDivisionID = @numDivisionID
--                    AND TE.numUserCntID = @numUserCntID
                    AND TE.[numDomainID] = @numDomainID
                    AND numType =2 --Non Billable
			
        END    
        
        
END	

