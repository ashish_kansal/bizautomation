        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_SHIPPING_LABEL_RULE_CHILD')
	DROP PROCEDURE USP_MANAGE_SHIPPING_LABEL_RULE_CHILD
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_MANAGE_SHIPPING_LABEL_RULE_CHILD]
	@numChildShipID numeric(18, 0),
	@numShippingRuleID numeric(18, 0),
	@numFromValue numeric(18, 2),
	@numToValue numeric(18, 2),
	@numDomesticShipID numeric(18, 0),
	@numInternationalShipID numeric(18, 0),
	@numPackageTypeID numeric(18, 0)
AS

IF EXISTS(SELECT [numChildShipID] FROM [dbo].[ShippingLabelRuleChild] WHERE [numChildShipID] = @numChildShipID)
BEGIN
	UPDATE [dbo].[ShippingLabelRuleChild] SET
		[numShippingRuleID] = @numShippingRuleID,
		[numFromValue] = @numFromValue,
		[numToValue] = @numToValue,
		[numDomesticShipID] = @numDomesticShipID,
		[numInternationalShipID] = @numInternationalShipID,
		[numPackageTypeID] = @numPackageTypeID
	WHERE
		[numChildShipID] = @numChildShipID
END
ELSE
BEGIN
	INSERT INTO [dbo].[ShippingLabelRuleChild] (
		[numShippingRuleID],
		[numFromValue],
		[numToValue],
		[numDomesticShipID],
		[numInternationalShipID],
		[numPackageTypeID]
	) VALUES (
		@numShippingRuleID,
		@numFromValue,
		@numToValue,
		@numDomesticShipID,
		@numInternationalShipID,
		@numPackageTypeID
	)
END

