GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageEmbeddedCost')
DROP PROCEDURE USP_ManageEmbeddedCost
GO
CREATE PROCEDURE [dbo].[USP_ManageEmbeddedCost]
    @numOppBizDocID NUMERIC,
    @numCostCatID NUMERIC,
    @numDomainID NUMERIC,
    @numUserCntID NUMERIC,
    @strItems TEXT,
    @tintMode TINYINT = 0
AS 
    BEGIN
        DECLARE @TotalEmbeddedCost DECIMAL(20,5) 
        DECLARE @ItemCount INT
        DECLARE @EmbeddedCostForEachItem DECIMAL(20,5)
        DECLARE @OldEmbeddedCostForEachItem DECIMAL(20,5)
        DECLARE @bitAppliedEmbeddedCost bit

		IF @tintMode = 0 
		BEGIN
			IF @numCostCatID > 0
            AND @numOppBizDocID > 0 
            BEGIN
--						IF NOT exists (SELECT * FROM [EmbeddedCostConfig] WHERE [numDomainID]=@numDomainID)
--						BEGIN
--							raiserror('NOCLASS',16,1);
--							RETURN ;
--						END
--						IF NOT exists (SELECT * FROM [OpportunityBizDocItems] BDI INNER JOIN Item I ON I.[numItemCode] = BDI.[numItemCode]
--			                       WHERE BDI.[numOppBizDocID]=@numOppBizDocID AND I.[numDomainID]=@numDomainID AND I.[numItemClassification] IN (SELECT [numItemClassification] FROM [EmbeddedCostConfig] WHERE [numDomainID]=@numDomainID) )
--						BEGIN
--							raiserror('NOITEM',16,1);
--							RETURN ;
--						END

                


                DECLARE @hDocItem INT
                IF CONVERT(VARCHAR(10), @strItems) <> '' 
                    BEGIN
                        EXEC sp_xml_preparedocument @hDocItem OUTPUT,
                            @strItems
                        INSERT  INTO [EmbeddedCost]
                                (
                                  [numOppBizDocID],
                                  [numCostCatID],
                                  numCostCenterID,
                                  [numAccountID],
                                  [numDivisionID],
                                  [numPaymentMethod],
                                  [monCost],
                                  [monActualCost],
                                  [vcMemo],
                                  [dtDueDate],
                                  [numDomainID],
                                  [numCreatedBy],
                                  [dtCreatedDate],
                                  [numModifiedBy],
                                  [dtModifiedDate]
					   
                                )
                                SELECT  @numOppBizDocID,
                                        @numCostCatID,
                                        X.numCostCenterID,
                                        X.numAccountID,
                                        X.numDivisionID,
                                        X.numPaymentMethod,
                                        0,--X.monCost,
                                        X.monActualCost,
                                        X.vcMemo,
                                        X.dtDueDate,
                                        @numDomainID,
                                        @numUserCntID,
                                        GETUTCDATE(),
                                        @numUserCntID,
                                        GETUTCDATE()
                                FROM    ( SELECT    *
                                          FROM      OPENXML (@hDocItem, '/NewDataSet/Table1[numEmbeddedCostID=0]', 2)
                                                    WITH (numEmbeddedCostID NUMERIC, numCostCenterID NUMERIC,numAccountID NUMERIC, numDivisionID NUMERIC, numPaymentMethod NUMERIC, monCost DECIMAL(20,5), monActualCost DECIMAL(20,5),vcMemo VARCHAR(100), dtDueDate DATETIME )
                                        ) X
                                        
                            UPDATE [EmbeddedCost]	 
                            SET	numCostCenterID = X.numCostCenterID,
                              [numAccountID]=X.numAccountID,
                              [numDivisionID]=X.numDivisionID,
                              [numPaymentMethod]=X.numPaymentMethod,
--                              [monCost]=X.monCost,
                              [monActualCost]=X.monActualCost,
                              [vcMemo]=X.vcMemo,
                              [dtDueDate]=X.dtDueDate
                             FROM    (SELECT Y.*
                                          FROM  OPENXML (@hDocItem, '/NewDataSet/Table1[numEmbeddedCostID>0]', 2)
                                                    WITH (numEmbeddedCostID NUMERIC, numCostCenterID NUMERIC,numAccountID NUMERIC, numDivisionID NUMERIC, numPaymentMethod NUMERIC,/* monCost DECIMAL(20,5),*/ monActualCost DECIMAL(20,5),vcMemo VARCHAR(100), dtDueDate DATETIME ) Y
                                          INNER JOIN EmbeddedCost EC ON EC.numEmbeddedCostID = Y.numEmbeddedCostID LEFT OUTER JOIN 
                                        [OpportunityBizDocsPaymentDetails] PD ON PD.[numBizDocsPaymentDetId] = EC.numBizDocsPaymentDetId
                                        where ISNULL(PD.[bitIntegrated],0) = 0 ) X
							 WHERE EmbeddedCost.numEmbeddedCostID = X.numEmbeddedCostID
                             
                             CREATE TABLE #temp(numEmbeddedCostID NUMERIC,monActualCost DECIMAL(20,5),numDivisionID NUMERIC)
                             INSERT INTO #Temp
                             SELECT    numEmbeddedCostID,monActualCost,numDivisionID
                             FROM      OPENXML (@hDocItem, '/NewDataSet/Table1[numEmbeddedCostID>0]', 2)
                             WITH (numEmbeddedCostID NUMERIC, monActualCost DECIMAL(20,5),numDivisionID NUMERIC) 

							--Remove Bills record which are already paid to account, they can't be updated
                             DELETE FROM [#temp] WHERE [numEmbeddedCostID] IN (SELECT t.[numEmbeddedCostID] FROM [#temp] t INNER JOIN [EmbeddedCost] EC ON EC.numEmbeddedCostID= t.numEmbeddedCostID INNER JOIN [OpportunityBizDocsPaymentDetails] PD ON PD.[numBizDocsPaymentDetId] = EC.numBizDocsPaymentDetId WHERE PD.[bitIntegrated] = 1)
                                        
							 --Update Bill value
							 UPDATE [OpportunityBizDocsDetails] SET [monAmount] = X.monActualCost,[numDivisionID] = X.numDivisionID
							 FROM (SELECT EC.numBizDocsPaymentDetId,t.monActualCost,t.numDivisionID FROM #temp t INNER JOIN [EmbeddedCost] EC ON EC.numEmbeddedCostID= t.numEmbeddedCostID)X
							 WHERE OpportunityBizDocsDetails.[numBizDocsPaymentDetId]= X.numBizDocsPaymentDetId
							 --Update Bill value
							 UPDATE [OpportunityBizDocsPaymentDetails] SET [monAmount] = X.monActualCost
							 FROM (SELECT EC.numBizDocsPaymentDetId,t.monActualCost FROM #temp t INNER JOIN [EmbeddedCost] EC ON EC.numEmbeddedCostID= t.numEmbeddedCostID)X
							 WHERE OpportunityBizDocsPaymentDetails.[numBizDocsPaymentDetId]= X.numBizDocsPaymentDetId
							 
                             DROP TABLE #temp 
                                        
                        EXEC sp_xml_removedocument @hDocItem
                        
--                       SELECT I.[numItemClassification] FROM [OpportunityBizDocItems] BDI INNER JOIN Item I ON I.[numItemCode] = BDI.[numItemCode]
--                       WHERE BDI.[numOppBizDocID]=@numOppBizDocID AND I.[numDomainID]=@numDomainID
--                       
                        
						
						
                    END
            END
		END
		
		--Distibute Total cost among bizdoc items
		IF @tintMode = 1
		BEGIN
		         IF EXISTS ( SELECT  * FROM [EmbeddedCost] WHERE   [numOppBizDocID] = @numOppBizDocID AND [numDomainID] = @numDomainID AND ISNULL(MonCost,0) >0 ) 
                    BEGIN
                    -- remove old cost per item
						UPDATE [OpportunityBizDocItems] 
							SET 
								[monPrice] = ISNULL([monPrice], 0)  - (monEmbeddedCost/[numUnitHour]),
								[monTotAmount] =  (ISNULL([monPrice], 0) * [numUnitHour]) - [monEmbeddedCost],
								[monTotAmtBefDiscount] = (ISNULL([monPrice], 0) * [numUnitHour]) - [monEmbeddedCost],
								bitEmbeddedCost = 0,
								monEmbeddedCost = 0
							WHERE   numOppBizDocID = @numOppBizDocID
								AND [bitEmbeddedCost] =1		
                        
					--Embed new cost in each item
						UPDATE [OpportunityBizDocItems] 
						SET 
							bitEmbeddedCost =1,
							monEmbeddedCost = X.monAmount,
							[monPrice] = ISNULL([monPrice], 0) + (X.monAmount/OpportunityBizDocItems.numUnitHour),
							[monTotAmount] =  (ISNULL([monPrice], 0) * [numUnitHour]) + X.monAmount,
							[monTotAmtBefDiscount] = (ISNULL([monPrice], 0) * [numUnitHour]) + X.monAmount
						FROM (
							SELECT  
									[numOppBizDocItemID],
									SUM(EmbeddedCostItems.monAmount) AS monAmount
							FROM    EmbeddedCost
									INNER JOIN EmbeddedCostItems ON EmbeddedCost.numEmbeddedCostID = EmbeddedCostItems.numEmbeddedCostID
							WHERE   numOppBizDocID = @numOppBizDocID 
							GROUP BY [numOppBizDocItemID]
						) X
						WHERE X.numOppBizDocItemID=[OpportunityBizDocItems].[numOppBizDocItemID] AND OpportunityBizDocItems.[numUnitHour] >0 
						AND [OpportunityBizDocItems].[bitEmbeddedCost]=0
                        
									
					--Integrate bizdoc with Embedded  cost
						   UPDATE   [OpportunityBizDocs]
						   SET      [bitAppliedEmbeddedCost] = 1,
									[monEmbeddedCostPerItem] = (SELECT ISNULL(SUM([monCost]),0) FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocID)
						   WHERE    [numOppBizDocsId] = @numOppBizDocID
				END
				ELSE -- when no embedded cost found and bizdoc has been applied emb cost
				BEGIN
							PRINT 'hi'
							UPDATE [OpportunityBizDocItems] 
							SET 
								[monPrice] = ISNULL([monPrice], 0)  - (monEmbeddedCost/[numUnitHour]),
								[monTotAmount] =  (ISNULL([monPrice], 0) * [numUnitHour]) - [monEmbeddedCost],
								[monTotAmtBefDiscount] = (ISNULL([monPrice], 0) * [numUnitHour]) - [monEmbeddedCost],
								bitEmbeddedCost =0,
								monEmbeddedCost = 0
							WHERE   numOppBizDocID = @numOppBizDocID
								AND [bitEmbeddedCost] =1
			
							
							UPDATE   [OpportunityBizDocs]
							SET [bitAppliedEmbeddedCost] = 0,
								[monEmbeddedCostPerItem] = 0
							WHERE    [numOppBizDocsId] = @numOppBizDocID
						END
				END
				
				 
		END
 
              
      
