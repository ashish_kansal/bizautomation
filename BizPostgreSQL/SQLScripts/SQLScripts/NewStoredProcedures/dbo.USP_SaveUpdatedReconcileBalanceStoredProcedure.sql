
GO
/****** Object:  StoredProcedure [dbo].[USP_SaveUpdatedReconcileBalance]    Script Date: 05/07/2009 22:02:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveUpdatedReconcileBalance')
DROP PROCEDURE USP_SaveUpdatedReconcileBalance
GO
CREATE PROCEDURE [dbo].[USP_SaveUpdatedReconcileBalance]
               @strText AS TEXT  = '',
               @numReconcileID AS NUMERIC(9)
AS
  BEGIN
  
  UPDATE General_Journal_Details SET bitReconcile=0,bitCleared=0,numReconcileID=0 WHERE numReconcileID=@numReconcileID
  
    DECLARE  @hDocItem INT
    IF CONVERT(VARCHAR(10),@strText) <> ''
      BEGIN
        EXEC sp_xml_preparedocument
          @hDocItem OUTPUT ,
          @strText
        UPDATE [General_Journal_Details]
        SET    bitReconcile = X.bitReconcile,
							bitCleared=X.bitCleared,
							numReconcileID=(CASE WHEN ISNULL(X.bitReconcile,0)=1 OR ISNULL(X.bitCleared,0)=1 THEN @numReconcileID ELSE 0 END)
        FROM   OPENXML (@hDocItem, '/NewDataSet/recon', 2)
                  WITH (numTransactionId NUMERIC(9),
                        bitReconcile     BIT,bitCleared BIT) X
        WHERE  General_Journal_Details.numTransactionId = X.numTransactionId
        EXEC sp_xml_removedocument
          @hDocItem
      END
  END