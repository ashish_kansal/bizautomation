GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_IsKitWithChildKits')
DROP PROCEDURE USP_Item_IsKitWithChildKits
GO
CREATE PROCEDURE [dbo].[USP_Item_IsKitWithChildKits]  
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
AS
BEGIN
	SELECT
		(CASE 
			WHEN ISNULL(Item.bitKitParent,0) = 1 
			THEN 
				(
					CASE
						WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=Item.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
						THEN 1
						ELSE 0
					END
				)
			ELSE 0 
		END) bitHasKitAsChild
	FROM
		Item
	WHERE
		numDomainID = @numDomainID
		AND numItemCode = @numItemCode

END