/****** Object:  StoredProcedure [dbo].[USP_ConEmpListBasedonTeam]    Script Date: 07/26/2008 16:15:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj          
-- exec USP_ConEmpListBasedonTeam @numDomainID=72,@numUserCntID=17
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConEmpListByGroupId')
DROP PROCEDURE USP_ConEmpListByGroupId
GO
CREATE PROCEDURE [dbo].[USP_ConEmpListByGroupId]
    @numDomainID AS NUMERIC(9) = 0,
    @numGroupId AS NUMERIC(9) = 0
AS 
BEGIN
	 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName          
 from UserMaster UM         
 join AdditionalContactsInformation A        
 on UM.numUserDetailId=A.numContactID          
 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID and UM.intAssociate=1  AND UM.numGroupID=@numGroupId

END