GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageOpportunityAutomationRules' ) 
    DROP PROCEDURE USP_ManageOpportunityAutomationRules
GO
CREATE PROCEDURE USP_ManageOpportunityAutomationRules
    @numDomainID AS NUMERIC,
    @strItems TEXT,
    @tintMode AS TINYINT,
    @tintOppType AS TINYINT 
AS 
BEGIN
    IF @tintMode = 0
        BEGIN
			SELECT * FROM OpportunityAutomationRules WHERE numDomainID=@numDomainID AND (tintOppType=@tintOppType OR @tintOppType=0)
        END
	ELSE IF @tintMode = 1 
    BEGIN
		DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;
		
		BEGIN TRANSACTION;

		BEGIN TRY
            DECLARE @hDocItem INT
            IF CONVERT(VARCHAR(10), @strItems) <> '' 
            BEGIN
                    EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

					DECLARE @TEMPTABLE TABLE
					(
						numRuleID NUMERIC(18,0),
						numBizDocStatus1 NUMERIC(18,0),
						numBizDocStatus2 NUMERIC(18,0),
						numOrderStatus NUMERIC(18,0)
					)

					INSERT INTO @TEMPTABLE
					SELECT  
						X.numRuleID,
						X.numBizDocStatus1,
						X.numBizDocStatus2,
						X.numOrderStatus
                    FROM    
						( 
							SELECT    
								*
                            FROM      
								OPENXML (@hDocItem, '/NewDataSet/Table', 2)
                            WITH ( numRuleID NUMERIC(18),numBizDocStatus1 NUMERIC(18),numBizDocStatus2 NUMERIC(18),numOrderStatus NUMERIC(18))
                        ) X
                    EXEC sp_xml_removedocument @hDocItem


					--TODO CHECK IF SALES FULLFILLMENT RULES CONTAINS CREATE INVOICE RULE

					IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE numBizDocStatus1 IN (SELECT ISNULL(numListItemID,0) FROM ListDetails WHERE numListID = 11 AND numListType = 287 AND numDomainID = @numDomainID)) > 0
					BEGIN
						IF (SELECT COUNT(WorkFlowActionList.numWFActionID) FROM WorkFlowActionList INNER JOIN WorkFlowMaster ON WorkFlowActionList.numWFID = WorkFlowMaster.numWFID WHERE tintActionType=6 AND numBizDocTypeID=287 AND WorkFlowMaster.numDomainID = @numDomainID) > 0
						BEGIN
							RAISERROR ('WORKFLOW AUTOMATION RULE EXISTS',16,1);
							RETURN -1
						END
					END

					--TODO CHECK IF SALES FULLFILLMENT RULES CONTAINS CREATE PACKING SLIP RULE
					IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE numBizDocStatus1 IN (SELECT ISNULL(numListItemID,0) FROM ListDetails WHERE numListID = 11 AND numListType = 29397 AND numDomainID = @numDomainID)) > 0
					BEGIN
						IF (SELECT COUNT(WorkFlowActionList.numWFActionID) FROM WorkFlowActionList INNER JOIN WorkFlowMaster ON WorkFlowActionList.numWFID = WorkFlowMaster.numWFID WHERE tintActionType=6 AND numBizDocTypeID=29397 AND WorkFlowMaster.numDomainID = @numDomainID) > 0
						BEGIN
							RAISERROR ('WORKFLOW AUTOMATION RULE EXISTS',16,1);
							RETURN -1
						END
					END

					DELETE FROM OpportunityAutomationRules WHERE numDomainID=@numDomainID AND tintOppType=@tintOppType 
            
                    INSERT  INTO dbo.OpportunityAutomationRules 
					(
						numDomainID,tintOppType,numRuleID,numBizDocStatus1,numBizDocStatus2,numOrderStatus
					)
                    SELECT  
						@numDomainID,@tintOppType,numRuleID,numBizDocStatus1,numBizDocStatus2,numOrderStatus
                    FROM  
						@TEMPTABLE
            END
            SELECT  ''

		END TRY
		BEGIN CATCH

			SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

			 IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			-- Use RAISERROR inside the CATCH block to return error
			-- information about the original error that caused
			-- execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
		END CATCH;

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;
	END	
END



