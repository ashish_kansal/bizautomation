SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Save')
DROP PROCEDURE dbo.USP_DemandForecast_Save
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Save]
	@numDFID NUMERIC(18,0),
	@numDomainID NUMERIC (18,0),
	@UserContactID NUMERIC (18,0),
	@bitIncludeOpenReleaseDates BIT,
	@vcItemClassification VARCHAR(500),
	@vcItemGroup VARCHAR(500),
	@vcWarehouse VARCHAR(500),
	@bitLastYear BIT,
	@numAnalysisPattern NUMERIC (18,0),
	@numForecastDays NUMERIC (18,0)
AS 
BEGIN
	DECLARE @numDemandForecastID NUMERIC(18,0) = 0


	IF (SELECT COUNT(*) FROM DemandForecast WHERE numDFID = @numDFID) = 0
	BEGIN
		-- INSERT
		INSERT INTO DemandForecast
		(
			numDomainID,
			numDFAPID,
			bitLastYear,
			numDFDaysID,
			bitIncludeOpenReleaseDates,
			dtCreatedDate,
			numCreatedBy
		)
		VALUES
		(
			@numDomainID,
			@numAnalysisPattern,
			@bitLastYear,
			@numForecastDays,
			@bitIncludeOpenReleaseDates,
			GETDATE(),
			@UserContactID
		)

		SET @numDemandForecastID = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		-- UPDATE
		UPDATE
			DemandForecast
		SET
			numDFAPID=@numAnalysisPattern,
			bitLastYear=@bitLastYear,
			numDFDaysID=@numForecastDays,
			bitIncludeOpenReleaseDates=@bitIncludeOpenReleaseDates,
			dtModifiedDate = GETDATE(),
			numModifiedBy = @UserContactID
		WHERE
			numDFID = @numDFID

		SET @numDemandForecastID = @numDFID

		--Delete Existing Data
		DELETE FROM DemandForecastItemClassification WHERE numDFID = @numDemandForecastID
		DELETE FROM DemandForecastItemGroup WHERE numDFID = @numDemandForecastID
		DELETE FROM DemandForecastWarehouse WHERE numDFID = @numDemandForecastID
	END		
	

	IF LEN(ISNULL(@vcItemClassification,'')) > 0
	BEGIN
		INSERT INTO DemandForecastItemClassification
		(
			numDFID,
			numItemClassificationID
		)
		SELECT
			@numDemandForecastID,
			Split.a.value('.', 'VARCHAR(100)') AS String
		FROM  
		(
		SELECT 
				CAST ('<M>' + REPLACE(@vcItemClassification, ',', '</M><M>') + '</M>' AS XML) AS String  
		) AS A 
		CROSS APPLY 
			String.nodes ('/M') AS Split(a); 
	END

	IF LEN(ISNULL(@vcItemGroup,'')) > 0
	BEGIN
		INSERT INTO DemandForecastItemGroup
		(
			numDFID,
			numItemGroupID
		)
		SELECT
			@numDemandForecastID,
			Split.a.value('.', 'VARCHAR(100)') AS String
		FROM  
		(
		SELECT 
				CAST ('<M>' + REPLACE(@vcItemGroup, ',', '</M><M>') + '</M>' AS XML) AS String  
		) AS A 
		CROSS APPLY 
			String.nodes ('/M') AS Split(a); 
	END

	IF LEN(ISNULL(@vcWarehouse,'')) > 0
	BEGIN
		INSERT INTO DemandForecastWarehouse
		(
			numDFID,
			numWarehouseID
		)
		SELECT
			@numDemandForecastID,
			Split.a.value('.', 'VARCHAR(100)') AS String
		FROM  
		(
		SELECT 
				CAST ('<M>' + REPLACE(@vcWarehouse, ',', '</M><M>') + '</M>' AS XML) AS String  
		) AS A 
		CROSS APPLY 
			String.nodes ('/M') AS Split(a); 
	END

END
