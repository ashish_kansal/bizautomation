SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ValidateCheckNo')
DROP PROCEDURE USP_ValidateCheckNo
GO
CREATE PROCEDURE [dbo].[USP_ValidateCheckNo]
    (
      @numDomainId NUMERIC(18),
      @numChartAcntId NUMERIC(18),
      @vcCheckNos VARCHAR(1000) ,
      @numCheckHeaderID NUMERIC(18)
    )
AS 
   
   SELECT DISTINCT numCheckNo FROM CheckHeader WHERE numDomainId=@numDomainId AND numChartAcntId=@numChartAcntId AND 
			numCheckNo IN (SELECT Items FROM dbo.Split(@vcCheckNos,',')) AND (numCheckHeaderID != @numCheckHeaderID OR @numCheckHeaderID=0)
			ORDER BY numCheckNo
    