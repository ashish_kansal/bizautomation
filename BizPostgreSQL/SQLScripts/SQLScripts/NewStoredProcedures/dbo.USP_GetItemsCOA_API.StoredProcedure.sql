
GO
/****** Object:  StoredProcedure [dbo].[USP_GetItemsCOA_API]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by Joseph
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemsCOA_API')
DROP PROCEDURE USP_GetItemsCOA_API
GO
CREATE PROCEDURE [dbo].[USP_GetItemsCOA_API]
          @numDomainID AS NUMERIC(9),
          @WebAPIId    AS INT
AS
 

SELECT   numItemCode,vcItemName,ISNULL(I.charItemType,'') AS charItemType,
		ISNULL(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		ISNULL(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		ISNULL(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
FROM    Item I 
		LEFT JOIN dbo.Chart_Of_Accounts COA ON I.numAssetChartAcntId = COA.numAccountId
		LEFT JOIN dbo.Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
		LEFT JOIN dbo.Chart_Of_Accounts COA2 ON I.numCOGsChartAcntId = COA2.numAccountId
WHERE   I.numDomainID = @numDomainID  
		AND vcExportToAPI IS NOT NULL
		AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
		AND I.charItemType='P' 
		AND (ISNULL(I.numAssetChartAcntId,0) =0 
		OR ISNULL(I.numCOGsChartAcntId,0)=0 
		OR ISNULL(I.numIncomeChartAcntId,0) = 0)

union


SELECT   numItemCode,vcItemName,ISNULL(I.charItemType,'') AS charItemType,
		ISNULL(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		ISNULL(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		ISNULL(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
FROM    Item I 
		LEFT JOIN dbo.Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
WHERE   I.numDomainID = @numDomainID  
		AND vcExportToAPI IS NOT NULL
		AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
		AND I.charItemType='N' 
		AND (ISNULL(I.numIncomeChartAcntId,0) = 0)

union

SELECT   numItemCode,vcItemName,ISNULL(I.charItemType,'') AS charItemType,
		ISNULL(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		ISNULL(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		ISNULL(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
FROM    Item I 
		LEFT JOIN dbo.Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
WHERE   I.numDomainID = @numDomainID  
		AND vcExportToAPI IS NOT NULL
		AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
		AND I.charItemType='S' 
		AND (ISNULL(I.numIncomeChartAcntId,0) = 0)

union

SELECT   numItemCode,vcItemName,ISNULL(I.charItemType,'') AS charItemType,
		ISNULL(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		ISNULL(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		ISNULL(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
FROM    Item I 
		LEFT JOIN dbo.Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
WHERE   I.numDomainID = @numDomainID  
		AND I.numItemCode in (SELECT ISNULL(WD.numDiscountItemMapping ,0) AS numDiscountItemMapping FROM webApiDetail WD 
								INNER JOIN Domain D ON D.numDomainID = WD.numDomainID 
								WHERE WD.numDomainID = @numDomainID and webAPiID = @WebAPIId )
		AND (ISNULL(I.numIncomeChartAcntId,0) = 0)
		
union

SELECT   numItemCode,vcItemName,ISNULL(I.charItemType,'') AS charItemType,
		ISNULL(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		ISNULL(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		ISNULL(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
FROM    Item I 
		LEFT JOIN dbo.Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
WHERE   I.numDomainID = @numDomainID  
		AND I.numItemCode in (SELECT ISNULL(WD.numSalesTaxItemMapping ,0) AS numSalesTaxItemMapping	FROM webApiDetail WD 
								INNER JOIN Domain D ON D.numDomainID = WD.numDomainID 
								WHERE WD.numDomainID = @numDomainID and webAPiID = @WebAPIId )
		AND (ISNULL(I.numIncomeChartAcntId,0) = 0)
		
union

SELECT   numItemCode,vcItemName,ISNULL(I.charItemType,'') AS charItemType,
		ISNULL(I.numCOGsChartAcntId,0) AS numCOGsChartAcntId,
		ISNULL(I.numAssetChartAcntId,0) AS numAssetChartAcntId,
		ISNULL(I.numIncomeChartAcntId,0) AS numIncomeChartAcntId
FROM    Item I 
		LEFT JOIN dbo.Chart_Of_Accounts COA1 ON I.numIncomeChartAcntId = COA1.numAccountId
WHERE   I.numDomainID = @numDomainID  
		AND I.numItemCode in (SELECT ISNULL(D.numShippingServiceItemID ,0) AS numShippingServiceItemID FROM webApiDetail WD 
								INNER JOIN Domain D ON D.numDomainID = WD.numDomainID 
								WHERE WD.numDomainID = @numDomainID and WD.webApiID = @WebAPIId)
		AND (ISNULL(I.numIncomeChartAcntId,0) = 0)
	

