GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_GetTrackingDetails' ) 
    DROP PROCEDURE usp_GetTrackingDetails
GO
--created by Joseph
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetTrackingDetails]
 @numDomainId AS NUMERIC(9), 
 @numWebApiId AS NUMERIC(9)
AS 
    BEGIN
	
		SELECT AOID.WebApiOrderItemId ,AOID.numDomainId,AOID.numWebApiId,AOID.numOppId,OBI.numItemCode,OBI.numOppBizDocItemID,
		AOID.numOppItemId,vcapiOppId,vcApiOppItemId,OB.numOppBizDocsId,OB.vcTrackingNo ,OB.monShipCost,OB.vcShippingMethod,LD.vcData AS ShippingCarrier,
		(SELECT TOP 1 vcAPIItemID FROM dbo.ItemAPI 
		WHERE numItemID = OBI.numItemCode 
		AND WebApiId = AOID.numWebApiId
		AND 1 = (CASE ISNULL(vcSKU,'') WHEN (SELECT vcSKU FROM Item WHERE numItemCode = numItemID AND numDomainID = @numDomainId ) THEN 1  
									   WHEN (SELECT vcWHSKU FROM dbo.WareHouseItems WI WHERE OBI.numWarehouseItmsID  = WI.numWareHouseItemID ) THEN 1
									   ELSE 0
				 END)  
		) AS vcAPIItemId,
		(SELECT COUNT(*) FROM dbo.OpportunityBizDocItems WHERE numOppBizDocID = OB.numOppBizDocsId AND tintTrackingStatus = 1) AS numTrackingNoUpdated,OBI.numOppBizDocID
		FROM WebApiOppItemDetails AOID 
		INNER JOIN dbo.OpportunityBizDocs OB ON AOID.numOppId  = OB.numOppId AND OB.bitAuthoritativeBizDocs = 1
		INNER JOIN dbo.OpportunityBizDocItems OBI  ON OBI.numOppBizDocID = OB.numOppBizDocsId
		INNER JOIN dbo.ListDetails LD ON LD.numlistItemID = OB.numShipVia

		WHERE  AOID.numWebApiId = @numWebApiId AND AOID.numDomainID = @numDomainId 
		AND len(AOID.vcAPIOppId)>2 AND len(OB.vcTrackingNo)>1
		AND (OBI.tintTrackingStatus = 0 or OBI.tintTrackingStatus IS NULL) 
		
    END
