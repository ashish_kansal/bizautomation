
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_ECampaign]    Script Date: 06/04/2009 15:13:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_ECampaign')
DROP PROCEDURE USP_GET_ECampaign
GO
CREATE PROCEDURE [dbo].[USP_GET_ECampaign]
@numDomainID NUMERIC(9)
AS 
BEGIN

SELECT [numECampaignID], [vcECampName] FROM [ECampaign] 
WHERE [numDomainID] =@numDomainID 
	
END
