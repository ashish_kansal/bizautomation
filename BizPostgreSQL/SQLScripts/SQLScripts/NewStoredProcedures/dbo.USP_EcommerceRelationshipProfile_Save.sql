GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EcommerceRelationshipProfile_Save')
DROP PROCEDURE dbo.USP_EcommerceRelationshipProfile_Save
GO
CREATE PROCEDURE [dbo].[USP_EcommerceRelationshipProfile_Save]
(
	@numSiteID NUMERIC(18,0)
	,@numRelationship NUMERIC(18,0)
	,@numProfile NUMERIC(18,0)
	,@vcItemClassificationIds VARCHAR(500)
)
AS 
BEGIN
	IF ISNULL(@numSiteID,0) = 0 OR NOT EXISTS (SELECT * FROM Sites WHERE numSiteID = @numSiteID)
	BEGIN
		RAISERROR('SITE_DOES_NOT_EXISTS',16,1)
		RETURN
	END
	ELSE IF ISNULL(@numRelationship,0) = 0
	BEGIN
		RAISERROR('RELATIONSHIP_REQUIRED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@numProfile,0) = 0
	BEGIN
		RAISERROR('PROFILE_REQUIRED',16,1)
		RETURN
	END

	DECLARE @TEMP TABLE
	(
		numItemClassification NUMERIC(18,0)
	)

	INSERT INTO @TEMP SELECT Id FROM dbo.SplitIDs(@vcItemClassificationIds,',')

	IF (SELECT COUNT(*) FROM @TEMP) = 0
	BEGIN
		RAISERROR('ITEMCLASSIFICATION_REQUIRED',16,1)
		RETURN
	END

	IF EXISTS (SELECT 
				* 
				FROM 
					EcommerceRelationshipProfile 
				INNER JOIN
					ECommerceItemClassification
				ON
					EcommerceRelationshipProfile.ID=ECommerceItemClassification.[numECommRelatiionshipProfileID]
				WHERE 
					numSiteID=@numSiteID 
					AND numRelationship=@numRelationship 
					AND numProfile=@numProfile 
					AND numItemClassification IN (SELECT numItemClassification FROM @TEMP)
				)
	BEGIN
		RAISERROR('DUPLICATE_SELECTION',16,1)
		RETURN
	END

	INSERT INTO EcommerceRelationshipProfile
	(
		[numSiteID]
		,[numRelationship]
		,[numProfile]
	)
	VALUES
	(
		@numSiteID
		,@numRelationship
		,@numProfile
	)

	DECLARE @ID INT = SCOPE_IDENTITY()

	INSERT INTO ECommerceItemClassification
	(
		[numECommRelatiionshipProfileID]
		,[numItemClassification]
	)
	SELECT
		@ID
		,numItemClassification
	FROM
		@TEMP
END