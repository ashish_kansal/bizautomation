GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DepositMaster_UpdateTransactionHistoryID')
DROP PROCEDURE dbo.USP_DepositMaster_UpdateTransactionHistoryID
GO
CREATE PROCEDURE [dbo].[USP_DepositMaster_UpdateTransactionHistoryID]
(
	@numDepositId NUMERIC(18,0),
    @numTransHistoryID NUMERIC(18,0)
)
AS
BEGIN
	UPDATE DepositMaster SET numTransHistoryID=@numTransHistoryID WHERE numDepositId=@numDepositId
END
GO