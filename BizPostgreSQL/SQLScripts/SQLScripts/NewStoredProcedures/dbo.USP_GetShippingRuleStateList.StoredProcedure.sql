GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingRuleStateList' ) 
    DROP PROCEDURE USP_GetShippingRuleStateList
GO
CREATE PROCEDURE USP_GetShippingRuleStateList
    @numDomainID NUMERIC,
    @numRuleID NUMERIC
AS 
BEGIN
 	
    SELECT DISTINCT  numStateID,
            numCountryID,
            
            dbo.fn_GetListItemName(numCountryID) vcCountry,
            dbo.fn_GetState(numStateID) vcState,
            numRuleID,
            numDomainID,
           
			STUFF((SELECT  ', ' + SS.vcZipPostal
					FROM ShippingRuleStateList SS
					WHERE SS.numStateID = SL.numStateID	AND numRuleID = 20079					
			FOR XML PATH ('')),1,2,'') AS vcZipPostalRange
    FROM    dbo.ShippingRuleStateList SL
    WHERE   numRuleID = @numRuleID
            AND numDomainID = @numDomainID
			GROUP BY numStateID, numRuleStateID,numCountryID,numRuleID,numDomainID,vcZipPostal
 	
END