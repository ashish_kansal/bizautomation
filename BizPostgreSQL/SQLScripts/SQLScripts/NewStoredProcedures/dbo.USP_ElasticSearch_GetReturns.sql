GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetReturns')
DROP PROCEDURE dbo.USP_ElasticSearch_GetReturns
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetReturns]
	@numDomainID NUMERIC(18,0)
	,@vcReturnHeaderIds VARCHAR(MAX)
AS 
BEGIN
	
	SELECT
		numReturnHeaderID AS id
		,'return' AS module
		,CONCAT(CASE WHEN tintReturnType=1 THEN '<b style="color:#ff0000">RMA (Sales):</b> ' ELSE '<b style="color:#00aa50">RMA (Purchase):</b> ' END,vcRMA,', ', vcCompanyName) AS displaytext
		,vcRMA AS [text]
		,CONCAT('/opportunity/frmReturnDetail.aspx?ReturnID=',numReturnHeaderID) AS url
		,ISNULL(vcRMA,'') AS Search_vcRMA
	FROM
		ReturnHeader
	INNER JOIN
		DivisionMaster
	ON
		ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		ReturnHeader.numDomainId=@numDomainID
		AND ReturnHeader.tintReturnType IN (1,2) -- 1:Sales, 2:Purchase
		AND (@vcReturnHeaderIds IS NULL OR numReturnHeaderID IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcReturnHeaderIds,'0'),',')))
END