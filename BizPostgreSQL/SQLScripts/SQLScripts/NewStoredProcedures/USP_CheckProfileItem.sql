/****** Object:  StoredProcedure [dbo].[uso_cfwfldDetails]    Script Date: 07/26/2008 16:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckProfileItem')
DROP PROCEDURE USP_CheckProfileItem
GO
CREATE PROCEDURE [dbo].[USP_CheckProfileItem]  
@numDomainId as numeric(18,0)=null  ,
@numItemCode as numeric(18,0)=null  
as  
DECLARE @numItemGroupID AS NUMERIC(18)
SET @numItemGroupID=(SELECT TOP 1 numItemGroupID FROM ItemGroups WHERE numProfileItem=@numItemCode AND numDomainID=@numDomainId order by numItemGroupID desc)
IF(@numItemGroupID>0)
BEGIN
	SELECT 
		 I.vcItemName+'~'+CAST(DTL.numOppAccAttrID AS varchar) AS numOppAccAttrID,LT.vcData
	FROM
		ItemGroupsDTL AS DTL
	LEFT JOIN
		Item AS I
	ON
		DTL.numOppAccAttrID=I.numItemCode
	LEFT JOIN
		ListDetails AS LT
	ON 
		DTL.numListid=LT.numListItemID
	WHERE
		DTL.numItemGroupID=@numItemGroupID
END
GO