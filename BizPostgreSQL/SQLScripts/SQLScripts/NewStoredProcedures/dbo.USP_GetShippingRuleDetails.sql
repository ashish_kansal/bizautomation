SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingruledetails')
DROP PROCEDURE usp_getshippingruledetails
GO
CREATE PROCEDURE [dbo].[USP_GetShippingRuleDetails]
	 @numDomainID NUMERIC(18,0)
	,@numWareHouseId VARCHAR(20)
	,@ZipCode VARCHAR(20)
	,@StateId NUMERIC(18,0)	
	,@numDivisionID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numCountryId NUMERIC(18,0)	
AS
BEGIN	
	DECLARE @numRuleID AS NUMERIC

	IF (SELECT ISNULL(bitEnableStaticShippingRule,0) FROM Domain WHERE numDomainID = @numDomainID) = 1
	BEGIN

		IF ISNULL(@numSiteID,0) > 0 AND ISNULL(@numDivisionID,0) = 0
																																													BEGIN
			DECLARE @numRelationship NUMERIC(18,0)
			DECLARE @numProfile NUMERIC(18,0)

			SELECT 
				@numRelationship=ISNULL(numRelationshipId,0)
				,@numProfile=ISNULL(numProfileId,0)
			FROM 
				eCommerceDTL 
			WHERE 
				numSiteId=@numSiteID

			SET @numRuleID = (SELECT TOP 1 
								SR.numRuleID		 
							FROM 
								ShippingRules SR						
							INNER JOIN 
								ShippingRuleStateList SRS
							ON 
								SR.numRuleID = SRS.numRuleID
							WHERE 
								(SR.numProfile = @numProfile OR ISNULL(SR.numProfile,0) = 0)
								AND (SR.numRelationship = @numRelationship OR ISNULL(SR.numRelationship,0) = 0)
								AND (CHARINDEX(@numWareHouseId, SR.numWareHouseID) > 0)
								AND SR.numDomainId = @numDomainID
								AND ISNULL(SRS.numCountryID,0)=ISNULL(@numCountryId,0)
								AND (SRS.numStateID = @StateId OR ISNULL(SRS.numStateID,0) = 0)
								AND (ISNULL(SRS.vcZipPostal,'') = '' OR (@ZipCode LIKE (SRS.vcZipPostal + '%')))
							ORDER BY
								(CASE 
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 1
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 2
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 3
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 4
									ELSE 5
								END),
								(CASE
									WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) > 0 THEN 1
									WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) = 0 THEN 2
									WHEN ISNULL(SR.numRelationship,0) = 0 AND ISNULL(SR.numProfile,0) > 0 THEN 3
									ELSE 4
								END),
								LEN(ISNULL(SRS.vcZipPostal,''))
						)
		END
			ELSE
																																												BEGIN
			SET @numRuleID = (SELECT TOP 1 
								SR.numRuleID		 
							FROM 
								ShippingRules SR	
							INNER JOIN 
								CompanyInfo C
							ON 
								(SR.numProfile = C.vcProfile OR ISNULL(SR.numProfile,0) = 0) 
								AND (SR.numRelationship = C.numCompanyType OR ISNULL(SR.numRelationship,0) = 0)					
							INNER JOIN 
								ShippingRuleStateList SRS
							ON 
								SR.numRuleID = SRS.numRuleID
							INNER JOIN 
								DivisionMaster DM
							ON 
								C.numCompanyId = DM.numCompanyID
							WHERE 
								DM.numDivisionID = @numDivisionID
								AND (CHARINDEX(@numWareHouseId, SR.numWareHouseID) > 0)
								AND SR.numDomainId = @numDomainID 
								AND ISNULL(SRS.numCountryID,0)=ISNULL(@numCountryId,0)
								AND (SRS.numStateID = @StateId OR ISNULL(SRS.numStateID,0) = 0)
								AND (ISNULL(SRS.vcZipPostal,'') = '' OR (@ZipCode LIKE (SRS.vcZipPostal + '%')))
							ORDER BY
								(CASE 
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 1
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) > 0 THEN 2
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) > 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 3
									WHEN ISNULL(SRS.numCountryID,0) > 0 AND ISNULL(SRS.numStateID,0) = 0 AND LEN(ISNULL(SRS.vcZipPostal,'')) = 0 THEN 4
									ELSE 5
								END),
								(CASE
									WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) > 0 THEN 1
									WHEN ISNULL(SR.numRelationship,0) > 0 AND ISNULL(SR.numProfile,0) = 0 THEN 2
									WHEN ISNULL(SR.numRelationship,0) = 0 AND ISNULL(SR.numProfile,0) > 0 THEN 3
									ELSE 4
								END),
								LEN(ISNULL(SRS.vcZipPostal,''))
						)
		END

		SELECT * FROM ShippingRules WHERE numRuleID = @numRuleID 

		SELECT ROW_NUMBER() OVER(ORDER BY numServiceTypeID ASC) AS RowNum,
				CAST(monRate AS DECIMAL(20,2)) AS monRate,	
		 * FROM ShippingServiceTypes WHERE numRuleID = @numRuleID 

	 END
END 
GO