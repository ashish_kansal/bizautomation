SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DelPromotionOffer')
DROP PROCEDURE USP_DelPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_DelPromotionOffer]
	@numDomainID AS NUMERIC(18,0),
	@numProId AS NUMERIC(18,0)
AS
BEGIN TRY
	IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainID AND numOrderPromotionID=@numProId)
	BEGIN
		RAISERROR('ITEM_PROMO_USING_ORDER_PROMO',16,1)
		RETURN
	END

	BEGIN TRANSACTION
		DELETE FROM PromotionOfferOrganizations WHERE numProId = @numProId 
		DELETE FROM PromotionOfferItems WHERE numProId = @numProId
		DELETE FROM PromotionOfferSites WHERE numPromotionID=@numProId		
		DELETE FROM PromotionOfferOrder WHERE numPromotionID=@numProId	
		DELETE FROM PromotionOffer WHERE numDomainId=@numDomainID AND numProId=@numProId
	COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
