GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MapBankStatementTransactions')
	DROP PROCEDURE USP_MapBankStatementTransactions
GO

/****** Added By : Joseph ******/
/****** Updates Table BankStatementTransactions with Reference ID and Reference Type of Transaction ******/

CREATE PROCEDURE [dbo].[USP_MapBankStatementTransactions]
	@numTransMappingId numeric(18, 0),
	@numTransactionID numeric(18, 0),
	@tintReferenceType tinyint,
	@numReferenceID numeric(18, 0),
	@bitIsActive bit
	
AS

SET NOCOUNT ON

IF EXISTS(SELECT [numTransMappingId] FROM [dbo].[BankTransactionMapping] WHERE [numTransMappingId] = @numTransMappingId)
BEGIN
	UPDATE [dbo].[BankTransactionMapping] SET
		[numTransactionID] = @numTransactionID,
		[tintReferenceType] = @tintReferenceType,
		[numReferenceID] = @numReferenceID,
		[bitIsActive] = @bitIsActive,
		[dtModifiedDate] = GETDATE()
	WHERE
		[numTransMappingId] = @numTransMappingId
END
ELSE
BEGIN
	INSERT INTO [dbo].[BankTransactionMapping] ( 
		[numTransactionID],
		[tintReferenceType],
		[numReferenceID],
		[bitIsActive],
		[dtCreatedDate] 
	) VALUES ( 
		@numTransactionID,
		@tintReferenceType,
		@numReferenceID,
		@bitIsActive,
		GETDATE()
	)
END


GO