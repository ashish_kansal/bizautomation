GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkFlowMaster')
DROP PROCEDURE USP_ManageWorkFlowMaster
GO
Create PROCEDURE [dbo].[USP_ManageWorkFlowMaster]                                        
	@numWFID numeric(18, 0) OUTPUT,
    @vcWFName varchar(200),
    @vcWFDescription varchar(500),   
    @numDomainID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @numFormID NUMERIC(18,0),
    @tintWFTriggerOn NUMERIC(18),
    @vcWFAction varchar(Max),
    @vcDateField VARCHAR(MAX),
    @intDays INT,
    @intActionOn INT,
	@bitCustom bit,
	@numFieldID numeric(18),
    @strText TEXT = ''
as                 
DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;
		
BEGIN TRANSACTION;

BEGIN TRY
	DECLARE @hDocItems INT                                                                                                                                                                
	EXEC sp_xml_preparedocument @hDocItems OUTPUT, @strText


	DECLARE @TEMP TABLE
	(
		tintActionType TINYINT,
		numBizDocTypeID NUMERIC(18,0)
	)
	INSERT INTO 
		@TEMP
	SELECT 
		tintActionType,
		numBizDocTypeID 
	FROM 
		OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) 
	WITH 
		(
			tintActionType TINYINT, 
			numBizDocTypeID numeric(18,0)
		)

	IF (SELECT COUNT(*) FROM @TEMP WHERE tintActionType = 6 AND (numBizDocTypeID=287 OR numBizDocTypeID=29397)) > 0
		BEGIN
			--CHECK IF CREATE INVOICE OR CREATE PACKING SLIP RULE EXISTS IN MASS SALES FULLFILLMENT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					OpportunityAutomationRules 
				WHERE 
					numDomainID = @numDomainID AND 
					(numRuleID = 1 OR numRuleID = 2)
				) > 0
			BEGIN
				RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
				RETURN -1
			END
		END


	DECLARE @bitDiplicate BIT = 0

	-- FIRST CHECK IF WORKFLOW WITH SAME MODULE AND TRIGGER TYPE EXISTS
	IF EXISTS (SELECT numWFID FROM WorkFlowMaster WHERE numDomainID=@numDomainID AND numWFID <> @numWFID AND numFormID=@numFormID AND tintWFTriggerOn=@tintWFTriggerOn AND (SELECT COUNT(*) FROM WorkFlowConditionList WHERE WorkFlowConditionList.numWFID = WorkFlowMaster.numWFID) = (SELECT COUNT(*) FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH (numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))))
	BEGIN
		-- NOT CHECK IF CONDITIONS ARE ALSO SAME
		DECLARE @TEMPWF TABLE
		(
			ID INT IDENTITY(1,1)
			,numWFID NUMERIC(18,0)
		)
		INSERT INTO 
			@TEMPWF 
		SELECT 
			numWFID 
		FROM 
			WorkFlowMaster 
		WHERE 
			numDomainID=@numDomainID
			AND numWFID <> @numWFID
			AND numFormID=@numFormID 
			AND tintWFTriggerOn=@tintWFTriggerOn

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		DECLARE @numTempWFID NUMERIC(18,0)

		SELECT @iCount=COUNT(*) FROM @TEMPWF

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempWFID=numWFID FROM @TEMPWF WHERE ID=@i

			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1
				EXCEPT
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2
			)
			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0


			IF NOT EXISTS (
				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData
						FROM 
							OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) 
						WITH 
							(numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10), vcFilterData VARCHAR(MAX))
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T1	
				
				EXCEPT

				SELECT 
					numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterData 
				FROM 
				(
					SELECT 
						numFieldID
						,bitCustom
						,vcFilterValue
						,vcFilterOperator
						,Split.a.value('.', 'VARCHAR(100)') AS vcFilterData  
					FROM  
					(
						SELECT 
							numFieldID
							,bitCustom
							,vcFilterValue
							,vcFilterOperator
							,CAST ('<M>' + REPLACE(vcFilterData, ',', '</M><M>') + '</M>' AS XML) AS vcFilterData  
						FROM
							WorkFlowConditionList
						WHERE
							numWFID=@numTempWFID
					) AS A CROSS APPLY vcFilterData.nodes ('/M') AS Split(a)
				) T2				
			)

			SELECT @bitDiplicate = 1 ELSE SELECT @bitDiplicate = 0

			IF @bitDiplicate = 1
			BEGIN
				BREAK
			END

			SET @i = @i + 1
		END
	END


	IF @bitDiplicate = 1
	BEGIN
		RAISERROR ('DUPLICATE_WORKFLOW',16,1);
		RETURN -1
	END

	IF @numWFID=0
	BEGIN
		INSERT INTO [dbo].[WorkFlowMaster] ([numDomainID], [vcWFName], [vcWFDescription], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [numFormID], [tintWFTriggerOn],[vcWFAction],[vcDateField],[intDays],[intActionOn],[bitCustom],[numFieldID])
		SELECT @numDomainID, @vcWFName, @vcWFDescription, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), @bitActive, @numFormID, @tintWFTriggerOn,@vcWFAction,@vcDateField,@intDays,@intActionOn,@bitCustom,@numFieldID

		SET @numWFID=@@IDENTITY
		--start of my
		IF DATALENGTH(@strText)>2
		BEGIN                                                                                                         
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData VARCHAR(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500) ) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItems
		END	
		--end of my
	END
	ELSE
	BEGIN
	
		UPDATE [dbo].[WorkFlowMaster]
		SET    [vcWFName] = @vcWFName, [vcWFDescription] = @vcWFDescription, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), [bitActive] = @bitActive, [numFormID] = @numFormID, [tintWFTriggerOn] = @tintWFTriggerOn,[vcWFAction]=@vcWFAction,[vcDateField]=@vcDateField,[intDays]=@intDays,[intActionOn]=@intActionOn,[bitCustom]=@bitCustom,[numFieldID]=@numFieldID
		WHERE  [numDomainID] = @numDomainID AND [numWFID] = @numWFID
	
		IF DATALENGTH(@strText)>2
		BEGIN
			DECLARE @hDocItem INT                                                                                                                                                                
	
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData varchar(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject ) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText ,vcEmailSendTo,vcMailBody,vcMailSubject
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT ,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500)) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItem
		END	
	END

	COMMIT
END TRY
BEGIN CATCH

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION;

	-- Use RAISERROR inside the CATCH block to return error
	-- information about the original error that caused
	-- execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH;
