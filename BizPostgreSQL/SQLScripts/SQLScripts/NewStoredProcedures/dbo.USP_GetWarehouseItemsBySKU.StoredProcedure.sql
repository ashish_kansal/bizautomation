GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWarehouseItemsBySKU')
DROP PROCEDURE USP_GetWarehouseItemsBySKU
GO
/****** Object:  StoredProcedure [dbo].[USP_GetWarehouseItemsBySKU]    Script Date: 11/03/2014 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetWarehouseItemsBySKU]
(
	@numItemCode NUMERIC(18,0),
	@numWarehouseID NUMERIC(18,0) = 0,
	@vcSKU VARCHAR(50),
	@numDomainID NUMERIC(18,0),
	@numWareHouseItemID NUMERIC(18,0) = 0
)
AS
BEGIN
	
	SELECT * FROM dbo.WareHouseItems WI 
	WHERE numWareHouseID = @numWarehouseID
	AND numItemID = @numItemCode
	AND numDomainID = @numDomainID
	AND ISNULL(vcWHSKU,'') = @vcSKU
	AND (numWareHouseItemID = @numWareHouseItemID OR @numWareHouseItemID = 0)
	
END
GO