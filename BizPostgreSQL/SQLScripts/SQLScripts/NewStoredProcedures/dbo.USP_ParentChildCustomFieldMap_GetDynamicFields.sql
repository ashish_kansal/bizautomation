        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ParentChildCustomFieldMap_GetDynamicFields')
	DROP PROCEDURE USP_ParentChildCustomFieldMap_GetDynamicFields
GO

CREATE PROCEDURE [dbo].[USP_ParentChildCustomFieldMap_GetDynamicFields]
	@tintModule TINYINT
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		fld_id NUMERIC(18,0)
		,Fld_label VARCHAR(300)
	)

	If @tintModule = 2 OR @tintModule = 6
	BEGIN
		INSERT INTO @TEMP
		(
			fld_id
			,Fld_label
		)
		VALUES
		(100,'Assigned To')
		,(122,'Comments')
		,(771,'Ship Via')
	END

	SELECT * FROM @TEMP
END
GO