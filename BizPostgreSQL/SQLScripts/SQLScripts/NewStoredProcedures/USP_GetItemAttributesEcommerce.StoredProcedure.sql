SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='[USP_GetItemAttributesEcommerce]')
DROP PROCEDURE dbo.[USP_GetItemAttributesEcommerce]
GO
CREATE PROCEDURE [dbo].[USP_GetItemAttributesEcommerce]
(
	@numDomainID NUMERIC(9),
	@numWareHouseID AS NUMERIC(9) = 0,
    @FilterQuery AS VARCHAR(2000) = '',
    @numCategoryID AS NUMERIC(18) = 0
)
AS 
BEGIN
	SELECT DISTINCT
		CFW.Fld_id 
		,CFW.Fld_type 
		,CFW.Fld_label 
		,CFW.numlistid
		,CFW.bitAutoComplete
	FROM 
		CFW_Fld_Master CFW  
	JOIN 
		ItemGroupsDTL    
	ON 
		numOppAccAttrID=Fld_id    
	WHERE
		numDomainID=@numDomainID
		AND Grp_id=9
		AND tintType=2 
		AND numItemGroupID IN (SELECT DISTINCT numItemGroup FROM ItemCategory INNER JOIN Item ON ItemCategory.numItemID=Item.numItemCode  WHERE numCategoryID=@numCategoryID)
	ORDER BY 
		bitAutoComplete
END
