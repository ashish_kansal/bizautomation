SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemsByReturnedVsSold')
DROP PROCEDURE USP_ReportListMaster_Top10ItemsByReturnedVsSold
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemsByReturnedVsSold]
	@numDomainID NUMERIC(18,0),
	@numRecordCount INT
AS
BEGIN 
	SELECT TOP (@numRecordCount)
		I.vcItemName
		,CONCAT('~/Items/frmKitDetails.aspx?ItemCode=',I.numItemCode,'&frm=All Items') AS URL
		,SUM(RI.numUnitHour)
		,SUM(OI.numUnitHour)
		,(SUM(RI.numUnitHour) * 100)/ (CASE WHEN SUM(OI.numUnitHour) = 0 THEN 1.0 ELSE SUM(OI.numUnitHour) END) ReturnPercent
	FROM 
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		ReturnItems RI
	ON
		(RI.numOppItemID = OI.numoppitemtCode
		OR RI.numReturnItemID = I.numItemCode)
	LEFT JOIN
		ReturnHeader RH
	ON
		RI.numReturnHeaderID = RH.numReturnHeaderID
	WHERE
		OM.numDomainId=@numDomainID
		AND RH.numDomainId=@numDomainID
		AND RH.tintReturnType IN (1,3)
		AND ISNULL(tintOppType,0) = 1
		AND ISNULL(tintOppStatus,0) = 1
	GROUP BY
		I.numItemCode
		,I.vcItemName
	ORDER BY
		(SUM(RI.numUnitHour) * 100)/ (CASE WHEN SUM(OI.numUnitHour) = 0 THEN 1.0 ELSE SUM(OI.numUnitHour) END) DESC,I.numItemCode ASC
END
GO

