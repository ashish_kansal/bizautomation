GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BizRecurrence_Execute')
DROP PROCEDURE USP_BizRecurrence_Execute
GO
  
-- =============================================  
-- Author:  Sandeep
-- Create date: 1 Oct 2014
-- Description: Recurring order or bizdoc
-- =============================================  
Create PROCEDURE [dbo].[USP_BizRecurrence_Execute]   
	
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 
DECLARE @Date AS DATE = GETDATE()

DECLARE @TEMPTABLE TABLE
(
	RowNo INT,
	numRecConfigID NUMERIC(18,0),
	--numDomainID NUMERIC(18,0),
	--numUserCntID NUMERIC(18,0),
	numOppID NUMERIC(18,0),
	numOppBizDocID NUMERIC(18,0)
	--dtEndDate DATE,
	--numType SMALLINT,
	--numFrequency SMALLINT
)

INSERT INTO @TEMPTABLE 
SELECT
	ROW_NUMBER() OVER(ORDER BY numRecConfigID ASC) As RowNo,
	numRecConfigID,
	numOppID,
	numOppBizDocID
FROM
	RecurrenceConfiguration
WHERE
	dtNextRecurrenceDate <= @Date 
	AND ISNULL(bitDisabled,0) = 0
	AND ISNULL(bitCompleted,0) = 0
	AND numType = 2 --Invoice

DECLARE @Count INT = 0
SELECT @Count = COUNT(*) FROM @TEMPTABLE 

IF @Count > 0
BEGIN
	DECLARE @i INT = 1

	DECLARE @numRecConfigID NUMERIC(18,0)
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numOppBizDocID NUMERIC(18,0)
	
	WHILE @i <= @Count
	BEGIN
		SELECT 
			@numRecConfigID=numRecConfigID,
			@numOppID=numOppID,
			@numOppBizDocID=numOppBizDocID   
		FROM 
			@TEMPTABLE 
		WHERE 
			RowNo = @i
		

		-- If there are no item left to add to invoice then set bitcompleted to 1 for invoice so it will not be recurred
		IF(SELECT 
				COUNT(*)
		   FROM
				OpportunityBizDocItems OBDI 
		   INNER JOIN
				(
					SELECT 
						*
					FROM
						(SELECT 
							OI.numoppitemtCode,   
							OI.numUnitHour AS QtyOrdered,
							(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFill                                   
						FROM      
							OpportunityItems OI
						LEFT JOIN 
							OpportunityMaster OM 
						ON 
							OI.numOppID = oM.numOppID 
						LEFT JOIN 
							OpportunityBizDocItems OBI 
						ON 
							OBI.numOppItemID = OI.numoppitemtCode AND OBI.numOppBizDocId IN (SELECT numOppBizDocsId FROM OpportunityBizDocs OB WHERE OB.numOppId = OI.numOppId AND ISNULL(OB.bitAuthoritativeBizDocs,0) = 1)
						LEFT JOIN 
							WareHouseItems WI 
						ON 
							WI.numWareHouseItemID = OI.numWarehouseItmsID
						INNER JOIN 
							Item I 
						ON 
							I.[numItemCode] = OI.[numItemCode]
						WHERE     
							OI.numOppId = @numOppId
						GROUP BY  
							OI.numoppitemtCode,
							OI.numUnitHour) X
						WHERE   
							X.QtytoFulFill > 0
				) AS TEMP
			ON
				OBDI.numOppItemID = TEMP.numoppitemtCode
			WHERE
				OBDI.numOppBizDocID=@numOppBizDocID) = 0
		BEGIN
			UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
		END

		SET @i = @i + 1
	END
END




--DECLARE @i INT = 1
--DECLARE @COUNT INT
--DECLARE @numRecConfigID NUMERIC(18,0)
--DECLARE @numDomainID NUMERIC(18,0)
--DECLARE @numUserCntID NUMERIC(18,0)
--DECLARE @numOppID NUMERIC(18,0)
--DECLARE @numOppBizDocID NUMERIC(18,0)
--DECLARE @dtEndDate NUMERIC(18,0)
--DECLARE @numType NUMERIC(18,0)
--DECLARE @numFrequency NUMERIC(18,0)

 --Get all the entries from RecurrenceConfiguration table where dtNextRecurrenceDate = @Date
 --@Date will be today date wich is getting passed from windows service which is executed per day

--INSERT INTO
--	@TEMPTABLE
SELECT
	ROW_NUMBER() OVER(ORDER BY numRecConfigID ASC) As RowNo,
	numRecConfigID,
	RecurrenceConfiguration.numDomainID,
	RecurrenceConfiguration.numCreatedBy AS numUserCntID,
	RecurrenceConfiguration.numOppID,
	RecurrenceConfiguration.dtNextRecurrenceDate,
	numOppBizDocID,
	dtEndDate,
	numType,
	numFrequency,
	Domain.bitAutolinkUnappliedPayment,
	OpportunityMaster.tintOppType,
	OpportunityMaster.numDivisionId
FROM
	RecurrenceConfiguration
INNER JOIN
	Domain 
ON
	RecurrenceConfiguration.numDomainID = Domain.numDomainId
INNER JOIN
	OpportunityMaster
ON
	RecurrenceConfiguration.numOppID = OpportunityMaster.numOppId
WHERE
	dtNextRecurrenceDate <= @Date 
	AND ISNULL(bitDisabled,0) = 0
	AND ISNULL(bitCompleted,0) = 0


--SELECT * FROM @TEMPTABLE

--SELECT @COUNT = COUNT(*) FROM @TEMPTABLE

--WHILE @i <= @COUNT
--BEGIN
--	SELECT 
--		@numRecConfigID = numRecConfigID,
--		@numDomainID = numDomainID,
--		@numOppID = numOppID,
--		@numOppBizDocID = numOppBizDocID,
--		@dtEndDate = dtEndDate,
--		@numType = numType,
--		@numFrequency = numFrequency
--	FROM 
--		@TEMPTABLE 
--	WHERE 
--		RowNo = @i

--	-- @numType = 1 = Sales Order
--	-- @numType = 2 = Invoice
--	IF @numType = 1
--	BEGIN
--		DECLARE @numRecuredOppID NUMERIC(18,0) = 0

--		--RECUR SALES ORDER
--		BEGIN TRY
--			EXEC USP_BizRecurrence_Order @numRecConfigID,@numDomainID,@numUserCntID,@numOppID,@numRecuredOppID


--			IF @numRecuredOppID > 0
--			BEGIN
--				-- Insert newly created opportunity to transaction history
--				INSERT INTO RecurrenceTransaction
--				(
--					[numRecConfigID],
--					[numRecurrOppID],
--					[dtCreatedDate]
--				)
--				VALUES
--				(
--					@numRecConfigID,
--					@numRecuredOppID,
--					GETDATE()
--				)

--				--Get next recurrence date for order 
--				DECLARE @dtNextRecurrenceDate DATE = NULL

--				SET @dtNextRecurrenceDate = (CASE @numFrequency
--											WHEN 1 THEN DATEADD(D,1,@Date) --Daily
--											WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
--											WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
--											WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
--											END)
				
--				-- Set recurrence status as completed if next recurrence date is greater than end date
--				If @dtNextRecurrenceDate > @dtEndDate
--				BEGIN
--					UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
--				END
--				ELSE -- Set next recurrence date
--				BEGIN
--					UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
--				END
--			END
--			ELSE 
--			BEGIN
--				RAISERROR ('Something worng with order recurrence. New order id is 0 and no exception returned from USP_BizRecurrence_Order.',16, 1)
--			END
--		END TRY
--		BEGIN CATCH
--			DECLARE @ErrorMessage NVARCHAR(4000);
--			DECLARE @ErrorSeverity INT;
--			DECLARE @ErrorState INT;

--			SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();


--			INSERT INTO RecurrenceErrorLog 
--			(
--				numDomainID,
--				numRecConfigID,
--				vcSource,
--				vcMessage,
--				CreatedDate
--			)
--			VALUES
--			(
--				@numDomainID,
--				@numRecConfigID,
--				'USP_BizRecurrence_Order',
--				@ErrorMessage,
--				GETUTCDATE()
--			)
--		END CATCH
		


--	END
--	ELSE IF @numType = 2
--		--RECUR INVOICE
--	BEGIN

--	END
--END


END  
