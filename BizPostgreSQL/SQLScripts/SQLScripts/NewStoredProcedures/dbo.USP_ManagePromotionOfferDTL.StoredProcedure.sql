SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePromotionOfferDTL')
DROP PROCEDURE USP_ManagePromotionOfferDTL
GO
CREATE PROCEDURE [dbo].[USP_ManagePromotionOfferDTL]    
@numProID as NUMERIC(9)=0,    
@tintRuleAppType as TINYINT,  
@numValue as NUMERIC(9)=0,  
@numProfile as NUMERIC(9)=0,  
@numPODTLID as NUMERIC(9)=0,  
@byteMode as TINYINT,
@tintRecordType TINYINT --5=Promotion Offer Items, 6 = Promotion Disocunt Items,2=ShippingRule
AS    
	IF @byteMode=0  
	BEGIN  
		IF @tintRuleAppType=1 
		BEGIN
			DELETE FROM PromotionOfferItems WHERE tintType = 2 and numProId=@numProId AND tintRecordType=@tintRecordType
			DELETE FROM PromotionOfferItems WHERE tintType = 4 and numProId=@numProId AND tintRecordType=@tintRecordType 
		END
		ELSE IF @tintRuleAppType=2 
		BEGIN
			DELETE FROM PromotionOfferItems WHERE tintType = 1 and numProId=@numProId AND tintRecordType=@tintRecordType
			DELETE FROM PromotionOfferItems WHERE tintType = 4 and numProId=@numProId AND tintRecordType=@tintRecordType 
		END
		ELSE IF @tintRuleAppType=4
		BEGIN
			DELETE FROM PromotionOfferItems WHERE tintType = 1 and numProId=@numProId AND tintRecordType=@tintRecordType
			DELETE FROM PromotionOfferItems WHERE tintType = 2 and numProId=@numProId AND tintRecordType=@tintRecordType 
		END
        
		INSERT INTO [PromotionOfferItems] (numProId,numValue,tintType,tintRecordType) VALUES (@numProId,@numValue,@tintRuleAppType,@tintRecordType)
	END  
	ELSE IF @byteMode=1  
	BEGIN  
		DELETE FROM PromotionOfferItems WHERE numProItemId = @numPODTLID AND tintRecordType=@tintRecordType
	END
GO
