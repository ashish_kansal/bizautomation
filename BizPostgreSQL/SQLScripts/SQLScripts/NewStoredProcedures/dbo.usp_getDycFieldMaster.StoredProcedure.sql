GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getDycFieldMaster')
DROP PROCEDURE usp_getDycFieldMaster
GO
CREATE PROCEDURE [dbo].[usp_getDycFieldMaster]  
 @numDomainID NUMERIC(9),
 @numModuleID NUMERIC(9),
@numCultureID numeric(9)
AS  
BEGIN  

select DFM.numFieldID,DFM.numModuleID,DFM.vcFieldName,isnull(DFG.vcNewFieldName,'') as vcNewFieldName,
DFM.vcAssociatedControlType,
isnull(DFG.vcToolTip,DFM.vcToolTip) as vcToolTip                                                
 FROM DycFieldMaster DFM 
left join DycField_Globalization DFG on DFG.numDomainID=@numDomainID AND DFG.numCultureID=@numCultureID AND DFM.numFieldID=DFG.numFieldID AND DFM.numModuleID=DFG.numModuleID
where DFM.numModuleID=@numModuleID ORDER BY DFM.vcFieldName

END
GO
