GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetListWebSourceLeads')
DROP PROCEDURE USP_GetListWebSourceLeads
GO
CREATE PROCEDURE [dbo].[USP_GetListWebSourceLeads]  
(
	@numDomainId     NUMERIC(9,0),
	@SourceDomain    VARCHAR(1000),
	@FromDate        DATETIME,
	@LeadsOrAccounts bit	
)                      
AS
BEGIN

IF @LeadsOrAccounts = 1 
BEGIN 


SELECT CI.vcCompanyName,DM.numDivisionID,DM.numCompanyId,DM.numGrpId,DM.tintCRMType,dbo.GetDomainNameFromURL(TV.vcReferrer) AS SourceDomain,
CASE WHEN DM.tintCRMType=0 THEN 'Lead' WHEN DM.tintCRMType = 1 THEN 'Prospects'  WHEN DM.tintCRMType = 2 THEN 'Account' END AS CRMType,
CASE WHEN convert(VARCHAR(11),
DateAdd(minute, 330,DM.bintCreatedDate) )= convert(VARCHAR(11),getdate()) THEN '<b><font color=red>Today</font></b>'
WHEN convert(VARCHAR(11),DateAdd(minute, 330,DM.bintCreatedDate) )= convert(VARCHAR(11),dateadd(day,-1,getdate())) 
THEN '<b><font color=purple>YesterDay</font></b>'
WHEN convert(VARCHAR(11),DateAdd(minute, 330,DM.bintCreatedDate) )= convert(VARCHAR(11),dateadd(day,1,getdate())) 
THEN'<b><font color=orange>Tommorow</font></b>' ELSE dbo.FormatedDateFromDate(DateAdd(minute, 330,DM.bintCreatedDate),1) END AS CompanyCreatedOn,
L4.vcData AS Survey ,
numPhone,
numPhoneExtension,
L7.vcData AS NumberOfUsers,
ADC.numContactId,DM.numTerID,ADC.numRecOwner,
vcFirstName,
vcLastName,
vcEmail
FROM CompanyInfo CI 
INNER JOIN DivisionMaster DM ON DM.numCompanyId = CI.numCompanyID
INNER JOIN TrackingVisitorsHDR TV ON TV.numDivisionID = DM.numDivisionID
LEFT JOIN ListDetails lst ON lst.numListItemID=DM.numFollowUpStatus                                                       
JOIN AdditionalContactsInformation ADC ON ADC.numDivisionID=DM.numDivisionID  
LEFT JOIN ListDetails L4 ON L4.numListItemID=DM.numFollowUpStatus
LEFT JOIN ListDetails L7 ON L7.numListItemID=numNoOfEmployeesId 
 
WHERE CI.numDomainId = @numDomainId AND 
--DM.tintCRMType = (CASE WHEN  @LeadsOrAccounts = 1 THEN 0
--WHEN @LeadsOrAccounts = 2 THEN  2 END)
DM.tintCRMType IN (0,1)
AND  ISNULL(ADC.bitPrimaryContact,0)=1   
AND CI.numDomainID=DM.numDomainID   
AND DM.numDomainID=ADC.numDomainID 
AND DM.bitActiveInActive = 1 
AND dbo.GetDomainNameFromURL(TV.vcReferrer) = @SourceDomain
AND TV.dtcreated > @FromDate

END

IF @LeadsOrAccounts = 2 
BEGIN 


SELECT CI.vcCompanyName,DM.numDivisionID,DM.numCompanyId,DM.numGrpId,DM.tintCRMType,dbo.GetDomainNameFromURL(TV.vcReferrer) AS SourceDomain,
CASE WHEN DM.tintCRMType=0 THEN 'Lead' WHEN DM.tintCRMType = 1 THEN 'Prospects'  WHEN DM.tintCRMType = 2 THEN 'Account' END AS CRMType,
CASE WHEN convert(VARCHAR(11),
DateAdd(minute, 330,DM.bintCreatedDate) )= convert(VARCHAR(11),getdate()) THEN '<b><font color=red>Today</font></b>'
WHEN convert(VARCHAR(11),DateAdd(minute, 330,DM.bintCreatedDate) )= convert(VARCHAR(11),dateadd(day,-1,getdate())) 
THEN '<b><font color=purple>YesterDay</font></b>'
WHEN convert(VARCHAR(11),DateAdd(minute, 330,DM.bintCreatedDate) )= convert(VARCHAR(11),dateadd(day,1,getdate())) 
THEN'<b><font color=orange>Tommorow</font></b>' ELSE dbo.FormatedDateFromDate(DateAdd(minute, 330,DM.bintCreatedDate),1) END AS CompanyCreatedOn,
L4.vcData AS Survey ,
numPhone,
numPhoneExtension,
L7.vcData AS NumberOfUsers,
ADC.numContactId,DM.numTerID,ADC.numRecOwner,
vcFirstName,
vcLastName,
vcEmail
FROM CompanyInfo CI 
INNER JOIN DivisionMaster DM ON DM.numCompanyId = CI.numCompanyID
INNER JOIN TrackingVisitorsHDR TV ON TV.numDivisionID = DM.numDivisionID
LEFT JOIN ListDetails lst ON lst.numListItemID=DM.numFollowUpStatus                                                       
JOIN AdditionalContactsInformation ADC ON ADC.numDivisionID=DM.numDivisionID  
LEFT JOIN ListDetails L4 ON L4.numListItemID=DM.numFollowUpStatus
LEFT JOIN ListDetails L7 ON L7.numListItemID=numNoOfEmployeesId 
 
WHERE CI.numDomainId = @numDomainId AND 
--DM.tintCRMType = (CASE WHEN  @LeadsOrAccounts = 1 THEN 0
--WHEN @LeadsOrAccounts = 2 THEN  2 END)
DM.tintCRMType = 2 
AND  ISNULL(ADC.bitPrimaryContact,0)=1   
AND CI.numDomainID=DM.numDomainID   
AND DM.numDomainID=ADC.numDomainID 
AND DM.bitActiveInActive = 1 
AND dbo.GetDomainNameFromURL(TV.vcReferrer) = @SourceDomain
AND TV.dtcreated > @FromDate

END

END