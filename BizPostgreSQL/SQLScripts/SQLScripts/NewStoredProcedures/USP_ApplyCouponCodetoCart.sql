SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ApplyCouponCodetoCart')
DROP PROCEDURE USP_ApplyCouponCodetoCart
GO
CREATE PROCEDURE [dbo].[USP_ApplyCouponCodetoCart]
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0) OUTPUT,
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@vcSendCoupon VARCHAR(200)=0,
	@numSiteID NUMERIC(18,0)=0
AS
BEGIN
	



	IF (SELECT COUNT(*) FROM PromotionOffer PO LEFT JOIN PromotionOfferSites POS ON PO.numProId=POS.numPromotionID WHERE numDomainId=@numDomainID AND ISNULL(POS.numSiteID,0)=@numSiteID AND txtCouponCode = @vcSendCoupon AND ISNULL(bitEnabled,0)=1)=0
	BEGIN
		RAISERROR('INVALID_COUPON_CODE',16,1)
		RETURN
	END 
	ELSE IF (SELECT 
			COUNT(*) 
		FROM 
			PromotionOffer PO 
		LEFT JOIN 
			PromotionOfferSites POS 
		ON 
			PO.numProId=POS.numPromotionID
		WHERE 
			numDomainId=@numDomainID 
			AND POS.numSiteID=@numSiteID
			AND txtCouponCode = @vcSendCoupon 
			AND bitEnabled = 1 
			AND 1= (CASE WHEN ISNULL(tintUsageLimit,0) > 0 THEN (CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) THEN 1 ELSE 0 END) ELSE 1 END)
			AND 1 = (CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 1 ELSE (CASE WHEN GETDATE() BETWEEN dtValidFrom AND dtValidTo THEN 1 ELSE 0 END) END)) = 0
	BEGIN
		RAISERROR('COUPON_EXPIRED',16,1)
		RETURN
	END

	IF((SELECT COUNT(numCartId) FROM CartItems WHERE numDomainId=@numDomainID AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId AND vcCoupon=@vcSendCoupon)=0)
	BEGIN
		SET @numItemCode=1
		
		DECLARE @fltOfferTriggerValue FLOAT
		DECLARE @tintOfferTriggerValueType TINYINT
		DECLARE @tintOfferBasedOn TINYINT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT
		DECLARE @PromotionID NUMERIC(18,0)
		DECLARE @PromotionDesc VARCHAR(500)
		


		 SElECT TOP 1 
			@PromotionID=PO.numProId,
			@fltOfferTriggerValue=PO.fltOfferTriggerValue,
			@tintOfferTriggerValueType=PO.tintOfferTriggerValueType,
			@tintOfferBasedOn=PO.tintOfferBasedOn,
			@fltDiscountValue=fltDiscountValue,
			@tintDiscountType=tintDiscountType,
			@tintDiscoutBaseOn=tintDiscoutBaseOn,
			@PromotionDesc=(CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				))
		FROM
			PromotionOffer  AS PO
		WHERE
			PO.numDomainId=@numDomainID
			AND PO.txtCouponCode=@vcSendCoupon
			AND bitEnabled = 1 
			AND 1= (CASE WHEN ISNULL(tintUsageLimit,0) > 0 THEN (CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) THEN 1 ELSE 0 END) ELSE 1 END)
			AND 1 = (CASE WHEN ISNULL(bitNeverExpires,0) = 1 THEN 1 ELSE (CASE WHEN GETDATE() BETWEEN dtValidFrom AND dtValidTo THEN 1 ELSE 0 END) END)

		DECLARE @TEMPItems TABLE
		(
			ID INT,
			numCartID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numItemClassification NUMERIC(18,0),
			numUnitHour NUMERIC(18,0),
			monPrice NUMERIC(18,0),
			monTotalAmount NUMERIC(18,0)
		)

		IF @tintOfferBasedOn = 1 --individual items
		BEGIN
			INSERT INTO 
				@TEMPItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				CI.numItemCode,
				I.numItemClassification,
				CI.numUnitHour,
				CI.monPrice,
				(CI.numUnitHour * CI.monPrice)
			FROM
				CartItems CI
			INNER JOIN
				Item I
			ON
				CI.numItemCode = I.numItemCode
			WHERE
				CI.numDomainId=@numDomainId
				AND CI.numUserCntId=@numUserCntId
				AND CI.vcCookieId=@cookieId
				AND ISNULL(CI.PromotionID,0) = 0
				AND CI.numItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@PromotionID AND tintRecordType=5 AND tintType=1)
				AND 1 = (CASE 
						WHEN @tintOfferTriggerValueType=1 -- Quantity
						THEN (CASE WHEN numUnitHour >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
						WHEN @tintOfferTriggerValueType=2 -- Amount
						THEN (CASE WHEN (CI.numUnitHour * CI.monPrice) >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
						ELSE 0
						END)
		END
		ELSE IF @tintOfferBasedOn = 2 --item classifications
		BEGIN
			;WITH CTE (numCartId,numItemCode,numItemClassification,numUnitHour,monPrice,monTotalAmount) AS
			(
				SELECT
					numCartId,
					CI.numItemCode,
					I.numItemClassification,
					CI.numUnitHour,
					CI.monPrice,
					(CI.numUnitHour * CI.monPrice)
				FROM
					CartItems CI
				INNER JOIN
					Item I
				ON
					CI.numItemCode = I.numItemCode
				WHERE
					CI.numDomainId=@numDomainId
					AND CI.numUserCntId=@numUserCntId
					AND CI.vcCookieId=@cookieId
					AND ISNULL(CI.PromotionID,0) = 0
					AND I.numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=@PromotionID AND tintRecordType=5 AND tintType=2)
			)

			INSERT INTO 
				@TEMPItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				numItemCode,
				numItemClassification,
				numUnitHour,
				monPrice,
				monTotalAmount
			FROM
				CTE
			WHERE
				1 = (CASE 
					WHEN @tintOfferTriggerValueType=1 -- Quantity
					THEN (CASE WHEN (SELECT SUM(numUnitHour) FROM CTE) >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
					WHEN @tintOfferTriggerValueType=2 -- Amount
					THEN (CASE WHEN (SELECT SUM(monTotalAmount) FROM CTE) >= @fltOfferTriggerValue THEN 1 ELSE 0 END)
					ELSE 0
					END)
		END

		DECLARE @isPromotionTrigerred AS BIT = 0
		DECLARE @i AS INT = 1
		DECLARE @COUNT AS INT
		DECLARE @numTempCartID NUMERIC(18,0)
		DECLARE @numTempItemCode NUMERIC(18,0)
		DECLARE @numTempItemClassification NUMERIC(18,0)
		DECLARE @numTempUnitHour NUMERIC(18,0)
		DECLARE @monTempPrice NUMERIC(18,0)
		DECLARE @monTempTotalAmount NUMERIC(18,0)
		DECLARE @remainingOfferValue FLOAT = @fltOfferTriggerValue
		DECLARE @remainingDiscountValue FLOAT = @fltDiscountValue

		SELECT @COUNT=COUNT(*) FROM @TEMPItems

		IF @COUNT > 0
		BEGIN
			SET @isPromotionTrigerred = 1

			WHILE @i <= @COUNT
			BEGIN
				SELECT 
					@numTempCartID=numCartID,
					@numTempItemCode=numItemCode,
					@numTempItemClassification=ISNULL(numItemClassification,0),
					@numTempUnitHour=numUnitHour,
					@monTempPrice=monPrice,
					@monTempTotalAmount = (numUnitHour * monPrice)
				FROM 
					@TEMPItems 
				WHERE 
					ID=@i

				IF @tintOfferBasedOn = 1 -- Individual Items
					AND 1 = (CASE 
							WHEN @tintOfferTriggerValueType=1 -- Quantity
							THEN CASE WHEN @numTempUnitHour >= @fltOfferTriggerValue THEN 1 ELSE 0 END
							WHEN @tintOfferTriggerValueType=2 -- Amount
							THEN CASE WHEN @monTempTotalAmount >= @fltOfferTriggerValue THEN 1 ELSE 0 END
							ELSE 0
						END) 
				BEGIN
					UPDATE 
						CartItems 
					SET 
						PromotionID=@PromotionID ,
						PromotionDesc=@PromotionDesc,
						bitParentPromotion=1,
						vcCoupon=@vcSendCoupon
					WHERE 
						numCartId=@numTempCartID

					IF 1 =(CASE @tintDiscoutBaseOn
						WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
						WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
						WHEN 3 THEN (CASE @tintOfferBasedOn
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END) 
						ELSE 0
					END)
					BEGIN
						IF @tintDiscountType=1  AND @fltDiscountValue > 0 --Percentage
						BEGIN
							UPDATE 
								CartItems
							SET
								fltDiscount=@fltDiscountValue,
								bitDiscountType=0,
								monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
							WHERE
								numCartId=@numTempCartID
						END
						ELSE IF @tintDiscountType=2 AND @fltDiscountValue > 0 --Flat Amount
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
							DECLARE @usedDiscountAmount AS FLOAT
				
							SELECT
								@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountAmount < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
								WHERE 
									numCartId=@numTempCartID
							END
						END
						ELSE IF @tintDiscountType=3  AND @fltDiscountValue > 0 --Quantity
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
							DECLARE @usedDiscountQty AS FLOAT

							SELECT
								@usedDiscountQty = SUM(ISNULL(fltDiscount,0)/(CASE WHEN ISNULL(monPrice,1) = 0 THEN 1 ELSE ISNULL(monPrice,1) END))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountQty < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN (@numTempUnitHour*@monTempPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
								WHERE 
									numCartId=@numTempCartID
							END
						END
					END

					
					BREAK
				END
				ELSE IF @tintOfferBasedOn = 2 -- Item Classification
				BEGIN
					UPDATE 
						CartItems 
					SET 
						PromotionID=@PromotionID ,
						PromotionDesc=@PromotionDesc,
						bitParentPromotion=1,
						vcCoupon=@vcSendCoupon
					WHERE 
						numCartId=@numTempCartID

					-- CHECK IF DISCOUNT FROM PROMOTION CAN ALSO BE APPLIED TO ITEM
					IF 1=(CASE @tintDiscoutBaseOn
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemCode AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND POI.numValue=@numTempItemClassification AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN (CASE @tintOfferBasedOn
											WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
											WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=@numTempItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
											ELSE 0
										END) 
							ELSE 0
						END)
					BEGIN
						IF @tintDiscountType=1 --Percentage
						BEGIN
							UPDATE 
								CartItems
							SET
								fltDiscount=@fltDiscountValue,
								bitDiscountType=0,
								monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
							WHERE
								numCartId=@numTempCartID
						END
						ELSE IF @tintDiscountType=2 --Flat Amount
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
							SELECT
								@usedDiscountAmount = SUM(ISNULL(fltDiscount,0))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountAmount < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
								WHERE 
									numCartId=@numTempCartID
							END
						END
						ELSE IF @tintDiscountType=3 --Quantity
						BEGIN
							-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
							SELECT
								@usedDiscountQty = SUM(ISNULL(fltDiscount,0)/(CASE WHEN ISNULL(monPrice,1) = 0 THEN 1 ELSE ISNULL(monPrice,1) END))
							FROM
								CartItems
							WHERE
								PromotionID=@PromotionID

							IF @usedDiscountQty < @fltDiscountValue
							BEGIN
								UPDATE 
									CartItems 
								SET 
									monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
									bitDiscountType=1,
									fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= @numTempUnitHour THEN (@numTempUnitHour*@monTempPrice) ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
								WHERE 
									numCartId=@numTempCartID
							END
						END
					END

					SET @remainingOfferValue = @remainingOfferValue - (CASE WHEN @tintOfferTriggerValueType=1 THEN @numTempUnitHour ELSE @monTempTotalAmount END)

					IF @remainingOfferValue <= 0
					BEGIN
						BREAK
					END
				END

				SET @i = @i + 1
			END
		END

		

		IF ISNULL(@isPromotionTrigerred,0) = 1
		BEGIN
			PRINT CONCAT(@isPromotionTrigerred,@tintDiscoutBaseOn)
			DELETE FROM @TEMPItems
				
			INSERT INTO 
				@TEMPItems
			SELECT
				ROW_NUMBER() OVER(ORDER BY numCartID ASC),
				numCartId,
				CI.numItemCode,
				I.numItemClassification,
				CI.numUnitHour,
				CI.monPrice,
				(CI.numUnitHour * CI.monPrice)
			FROM
				CartItems CI
			INNER JOIN
				Item I
			ON
				CI.numItemCode = I.numItemCode
			WHERE
				CI.numDomainId=@numDomainId
				AND CI.numUserCntId=@numUserCntId
				AND CI.vcCookieId=@CookieId
				AND ISNULL(CI.PromotionID,0) = 0
				AND 1 = (CASE @tintDiscoutBaseOn
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemCode AND numProId=@PromotionID AND tintRecordType=6 AND tintType=1) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numValue=I.numItemClassification AND numProId=@PromotionID AND tintRecordType=6 AND tintType=2) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN (CASE @tintOfferBasedOn 
									WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=1)) > 0 THEN 1 ELSE 0 END)
									WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numItemCode=I.numItemCode AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainId AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems POI WHERE POI.numProId=@PromotionID AND tintRecordType=5 AND tintType=2))) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END) 
							ELSE 0
						END)

			DECLARE @k AS INT = 1
			DECLARE @kCount AS INT 

			SELECT @kCount = COUNT(*) FROM @TEMPItems
			
			IF @kCount > 0
			BEGIN
				WHILE @k <= @kCount
				BEGIN
					SELECT 
						@numTempCartID=numCartID,
						@numTempItemCode=numItemCode,
						@numTempItemClassification=ISNULL(numItemClassification,0),
						@numTempUnitHour=numUnitHour,
						@monTempPrice=monPrice,
						@monTempTotalAmount = (numUnitHour * monPrice)
					FROM 
						@TEMPItems 
					WHERE 
						ID=@k

					IF @tintDiscountType=1 --Percentage
					BEGIN
						UPDATE 
							CartItems
						SET
							PromotionID=@PromotionID,
							PromotionDesc=@PromotionDesc,
							bitParentPromotion=0,
							fltDiscount=@fltDiscountValue,
							bitDiscountType=0,
							monTotAmount=(@monTempTotalAmount-((@monTempTotalAmount*@fltDiscountValue)/100))
						WHERE
							numCartId=@numTempCartID
					END
					ELSE IF @tintDiscountType=2 --Flat Amount
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT AMOUNT IS NOT ALREADY USED BY OTHER ITEMS
						SELECT
							@usedDiscountAmount = SUM(fltDiscount)
						FROM
							CartItems
						WHERE
							PromotionID=@PromotionID

						IF @usedDiscountAmount < @fltDiscountValue
						BEGIN
							UPDATE 
								CartItems 
							SET 
								monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount)  >= @monTempTotalAmount THEN 0 ELSE (@monTempTotalAmount-(@fltDiscountValue - @usedDiscountAmount)) END),
								PromotionID=@PromotionID ,
								PromotionDesc=@PromotionDesc,
								bitDiscountType=1,
								fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountAmount) >= @monTempTotalAmount THEN @monTempTotalAmount ELSE (@fltDiscountValue - @usedDiscountAmount) END)
							WHERE 
								numCartId=@numTempCartID
						END
						ELSE
						BEGIN
							BREAK
						END
					END
					ELSE IF @tintDiscountType=3 --Quantity
					BEGIN
						-- FIRST CHECK WHETHER DISCOUNT QTY IS NOT ALREADY USED BY OTHER ITEMS
						SELECT
							@usedDiscountQty = SUM(fltDiscount/monPrice)
						FROM
							CartItems
						WHERE
							PromotionID=@PromotionID

						IF @usedDiscountQty < @fltDiscountValue
						BEGIN
							UPDATE 
								CartItems 
							SET 
								monTotAmount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty)  >= numUnitHour THEN 0 ELSE (@monTempTotalAmount-((@fltDiscountValue - @usedDiscountQty) * @monTempPrice)) END),
								PromotionID=@PromotionID ,
								PromotionDesc=@PromotionDesc,
								bitDiscountType=1,
								fltDiscount=(CASE WHEN (@fltDiscountValue - @usedDiscountQty) >= numUnitHour THEN @monTempTotalAmount ELSE ((@fltDiscountValue - @usedDiscountQty) * @monTempPrice) END) 
							WHERE 
								numCartId=@numTempCartID
						END
						ELSE
						BEGIN
							BREAK
						END
					END

					SET @k = @k + 1
				END
			END
		END
	END
	ELSE
	BEGIN
		SET @numItemCode=2
	END

	SELECT @numItemCode AS ResOutput
END