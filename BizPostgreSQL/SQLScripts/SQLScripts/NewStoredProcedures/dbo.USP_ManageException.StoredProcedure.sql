--Created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageException')
DROP PROCEDURE USP_ManageException
GO
CREATE PROCEDURE USP_ManageException
@vcMessage as varchar(max), 
@numDomainID numeric(9), 
@numUserContactID numeric(9), 
@vcURL as varchar(1000), 
@vcReferrer  as varchar(1000), 
@vcBrowser  as varchar(100), 
@vcBrowserAgent  as varchar(500)
as

insert into ExceptionLog(vcMessage, numDomainID, numUserContactID, dtCreatedDate, vcURL, vcReferrer, vcBrowser, vcBrowserAgent)
values(@vcMessage, @numDomainID, @numUserContactID, getdate(), @vcURL, @vcReferrer, @vcBrowser, @vcBrowserAgent)