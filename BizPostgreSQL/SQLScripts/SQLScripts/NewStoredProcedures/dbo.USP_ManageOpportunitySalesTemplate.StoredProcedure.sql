/****** Object:  StoredProcedure [dbo].[USP_ManageOpportunitySalesTemplate]    Script Date: 09/01/2009 01:18:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageOpportunitySalesTemplate')
DROP PROCEDURE USP_ManageOpportunitySalesTemplate
GO
CREATE PROCEDURE [dbo].[USP_ManageOpportunitySalesTemplate]
	@numSalesTemplateID numeric(9) OUTPUT,
	@vcTemplateName varchar(50),
	@tintType tinyint,
	@numOppId numeric(9),
	@numDivisionID numeric(9),
	@numDomainID numeric(9),
	@numUserCntID numeric(9),
	@tintAppliesTo tinyint
AS

IF @numDivisionID = 0 
	SELECT @numDivisionID = [numDivisionId] FROM [OpportunityMaster] WHERE [numOppId]=@numOppId
IF @numOppId = 0 
	SET @numOppId = NULL;

IF @numSalesTemplateID = 0 
BEGIN	
	INSERT INTO OpportunitySalesTemplate (
		[vcTemplateName],
		[tintType],
		[numOppId],
		[numDivisionID],
		[numDomainID],
		[numCreatedBy],
		[numModifiedBy],
		[tintAppliesTo]
	)
	VALUES (
		@vcTemplateName,
		@tintType,
		@numOppId,
		@numDivisionID,
		@numDomainID,
		@numUserCntID,
		@numUserCntID,
		@tintAppliesTo
	)

	SET @numSalesTemplateID = SCOPE_IDENTITY()
	SELECT @numSalesTemplateID As InsertedID
END
ELSE BEGIN
	UPDATE OpportunitySalesTemplate SET 
		[vcTemplateName] = @vcTemplateName,
		[tintType] = @tintType,
		[numOppId] = @numOppId,
		[numDivisionID] = @numDivisionID,
		[numDomainID] = @numDomainID,
		[dtModifiedDate] = GETUTCDATE(),
		[numModifiedBy] = @numUserCntID,
		[tintAppliesTo] = @tintAppliesTo
	WHERE [numSalesTemplateID] = @numSalesTemplateID
END