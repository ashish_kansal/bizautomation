SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetDefaultSurveyRating')
DROP PROCEDURE usp_GetDefaultSurveyRating
GO
CREATE PROCEDURE [dbo].[usp_GetDefaultSurveyRating]    
 @numDomainID Numeric,       
 @numSurId NUMERIC
AS      
BEGIN      
  SELECT TOP 1 * FROM SurveyCreateRecord WHERE     
   numSurId = @numSurId AND ISNULL(bitDefault,0)=1
END
GO
