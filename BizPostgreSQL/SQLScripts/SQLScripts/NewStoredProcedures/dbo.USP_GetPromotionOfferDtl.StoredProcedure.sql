SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOfferDtl')
DROP PROCEDURE USP_GetPromotionOfferDtl
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOfferDtl]    
@numProId as numeric(9)=0,    
@numDomainID as numeric(9)=0,   
@tintRuleAppType as TINYINT,
@numSiteID AS NUMERIC(9)=0,
@tintRecordType TINYINT --1=Promotions,2=ShippingRule,3-BatchProcessing,4-PackageRule
AS   
BEGIN
	IF @tintRuleAppType=1    
	BEGIN
		SELECT 
			[numProItemId]
			,[numProId]
			,[numValue]
			,vcItemName
			,[txtItemDesc]
			,[vcModelID] 
		FROM 
			Item   
		JOIN 
			[PromotionOfferItems]  
		ON 
			numValue=numItemCode  
		WHERE 
			numDomainID=@numDomainID 
			AND numProId=@numProId 
			AND tintType=1 
			AND tintRecordType=@tintRecordType
	END    
	ELSE IF @tintRuleAppType=2    
	BEGIN
		SELECT 
			[numProItemId]
			,[numProId]
			,[numValue]
			,vcCategoryName 
		FROM 
			[PromotionOfferItems]  POI 
		INNER JOIN 
			[Category] SC 
		ON 
			SC.[numCategoryID] = POI.[numValue]
		WHERE 
			numProId=@numProId 
			AND tintType=2 
			AND tintRecordType=@tintRecordType
 
		SELECT 
			SC.numCategoryID,
			ISNULL([vcCategoryName],'-') vcCategoryName  
		FROM 
			SiteCategories SC 
		JOIN 
			Category C 
		ON 
			SC.numCategoryID=C.numCategoryID 
		WHERE
			C.numDomainID=@numDomainID 
			AND SC.numSiteID=@numSiteID 
			AND SC.numCategoryID NOT IN (select numValue from PromotionOfferItems where tintType=2 and numProId=@numProId AND tintRecordType=@tintRecordType)
	END    
	ELSE IF ISNULL(@tintRuleAppType,0) = 3	
	BEGIN
		SELECT * FROM dbo.ListDetails WHERE numListID = 461 AND numDomainID = @numDomainID
		
		SELECT 
			numProItemId
			,numProId
			,numValue
			,vcData 
		FROM 
			dbo.PromotionOfferItems POI 
		INNER JOIN 
			dbo.ListDetails LD 
		ON 
			POI.numValue = LD.numListItemID
		WHERE 
			numListID = 461
			AND numProId = @numProId 
			AND tintType = 3 
			AND tintRecordType=@tintRecordType	
	END
	ELSE IF ISNULL(@tintRuleAppType,0) = 4	
	BEGIN
		SELECT *  FROM dbo.ListDetails WHERE numListID = 36 AND numDomainID = @numDomainID
		
		SELECT 
			numProItemId
			,numProId
			,numValue
			,vcData 
		FROM 
			dbo.PromotionOfferItems POI	
		INNER JOIN 
			dbo.ListDetails LD 
		ON 
			POI.numValue = LD.numListItemID
		WHERE 
			numListID = 36
			AND numProId = @numProId 
			AND tintType = 4 
			AND tintRecordType=@tintRecordType	
	END
END
GO
