GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDiscountItem')
DROP PROCEDURE USP_GetDiscountItem
GO
CREATE PROCEDURE [dbo].[USP_GetDiscountItem]
@numDomainID as BIGINT = 0,
@str as varchar(20)=''
as

select numItemCode,vcItemName from item where vcItemName like @str +'%' AND numDomainID =  @numDomainID 

GO
