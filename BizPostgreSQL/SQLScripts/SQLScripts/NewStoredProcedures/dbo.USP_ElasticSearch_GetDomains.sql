SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetDomains')
DROP PROCEDURE dbo.USP_ElasticSearch_GetDomains
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetDomains]
	
AS 
BEGIN
	SELECT
		numDomainId
	FROM
		Domain
	WHERE
		ISNULL(bitElasticSearch,0) = 1
END