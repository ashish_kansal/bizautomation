GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBankStatementTransactions')
DROP PROCEDURE USP_ManageBankStatementTransactions
GO
CREATE PROCEDURE USP_ManageBankStatementTransactions
    @numDomainID numeric(18, 0)
AS
	SELECT BST.* FROM BankDetails BD JOIN dbo.BankStatementHeader BSH ON BD.numBankDetailID=BSH.numBankDetailID
			JOIN dbo.BankStatementTransactions BST ON BSH.numStatementID=BST.numStatementID  
			WHERE BD.numDomainID=@numDomainID