SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGoogleMap')
DROP PROCEDURE USP_GetGoogleMap
GO
CREATE PROCEDURE [dbo].USP_GetGoogleMap
    (
      @numDomainID as numeric(9)=0,
	  @xmlStr as varchar(5000),
	  @miles as int,
      @zipcode as varchar(20)
    )
AS 
   select Items into #tempContactId from dbo.split(@xmlStr,',')
   select * into #tempZipCode from dbo.funZips(@zipcode,@miles)

Select  ADC.numContactId,vcCompanyName,isnull(vcFirstName,'') vcFirstName,isnull(vcLastName,'') vcLastName,ZC.Longitude,ZC.Latitude,
isnull(AD1.vcStreet,'') + isnull(AD1.vcCity,'') + ' ,'
                            + isnull(dbo.fn_GetState(AD1.numState),'') + ' '
                            + isnull(AD1.vcPostalCode,'') + ' <br>'
                            + isnull(dbo.fn_GetListItemName(AD1.numCountry),'') vcFullAddress,tZ.Distance                 
  FROM AdditionalContactsInformation ADC join #tempContactId tC on tC.Items=ADC.numContactId                                                  
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId 
  left join AddressDetails AD1 on AD1.numDomainID=@numDomainID and AD1.numRecordID=ADC.numContactId AND AD1.tintAddressOf=1
  join ZipCodes ZC on (REPLICATE('0', 5 - LEN(ZC.ZipCode)) + cast(ZC.ZipCode as varchar(10)))=AD1.vcPostalCode
  join #tempZipCode tZ on tz.ZipCode=ZC.ZipCode
  where DM.numDomainID  = @numDomainID

Union

Select  ADC.numContactId,vcCompanyName,isnull(vcFirstName,'') vcFirstName,isnull(vcLastName,'') vcLastName,ZC.Longitude,ZC.Latitude,
isnull(AD2.vcStreet,'') + isnull(AD2.vcCity,'') + ' ,'
                            + isnull(dbo.fn_GetState(AD2.numState),'') + ' '
                            + isnull(AD2.vcPostalCode,'') + ' <br>'
                            + isnull(dbo.fn_GetListItemName(AD2.numCountry),'') vcFullAddress,tZ.Distance                      
  FROM AdditionalContactsInformation ADC join #tempContactId tC on tC.Items=ADC.numContactId                                                  
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId 
  left join AddressDetails AD2 on AD2.numDomainID=@numDomainID and AD2.numRecordID=ADC.numDivisionId AND AD2.tintAddressOf=2
  join ZipCodes ZC on (REPLICATE('0', 5 - LEN(ZC.ZipCode)) + cast(ZC.ZipCode as varchar(10)))=AD2.vcPostalCode
  join #tempZipCode tZ on tz.ZipCode=ZC.ZipCode
  where DM.numDomainID  = @numDomainID 
order by Distance

drop table #tempContactId
