SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_PrebuildScoreCard')
DROP PROCEDURE USP_ReportListMaster_PrebuildScoreCard
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_PrebuildScoreCard]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @FinancialYearStartDate AS DATETIME
	DECLARE @dtCYFromDate AS DATETIME                                       
	DECLARE @dtCYToDate AS DATETIME
	DECLARE @dtMonthFromDate AS DATETIME                                       
	DECLARE @dtMonthToDate AS DATETIME

	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainID)
	SELECT @dtCYFromDate = @FinancialYearStartDate,@dtCYToDate = dateadd(ms, -3, dateadd(day, datediff(day, 0, getdate())+1, 0));

	SELECT 
		@dtMonthFromDate = DATEADD(m, DATEDIFF(m, 0, GETUTCDATE()), 0)
		,@dtMonthToDate = GETUTCDATE();
	
	

	DECLARE @Table Table
	(
		ID INT
		,vcReport VARCHAR(100)
		,vcObjectsOfMeasure VARCHAR(100)
		,vcPeriod VARCHAR(100)
		,numChange NUMERIC(18,2)
	)

	INSERT INTO @Table (ID) VALUES (1),(2),(3),(4),(5),(6),(7)

	UPDATE @Table SET numChange=dbo.GetPercentageChangeInLeadToAccountConversion(@numDomainID,@dtCYFromDate,@dtCYToDate,1) WHERE ID=1
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInLeadToAccountConversion(@numDomainID,@dtMonthFromDate,@dtMonthToDate,2) WHERE ID=2
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInGrossProfit(@numDomainID,@dtCYFromDate,@dtCYToDate,1) WHERE ID=3
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInGrossProfit(@numDomainID,@dtMonthFromDate,@dtMonthToDate,2) WHERE ID=4
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInSalesOppToOrderConversion(@numDomainID,@dtCYFromDate,@dtCYToDate,1) WHERE ID=5
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInSalesOppToOrderConversion(@numDomainID,@dtCYFromDate,@dtCYToDate,2) WHERE ID=6
	UPDATE @Table SET numChange=dbo.GetPercentageChangeInProjectAverageGrossProfit(@numDomainID,@dtCYFromDate,@dtCYToDate) WHERE ID=7

	SELECT * FROM @Table
END
GO



