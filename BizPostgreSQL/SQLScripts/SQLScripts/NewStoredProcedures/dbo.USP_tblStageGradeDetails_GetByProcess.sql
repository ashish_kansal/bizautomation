GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_tblStageGradeDetails_GetByProcess')
DROP PROCEDURE dbo.USP_tblStageGradeDetails_GetByProcess
GO
CREATE PROCEDURE [dbo].[USP_tblStageGradeDetails_GetByProcess]
(
	@numDomainID NUMERIC(18,0)
	,@numProcessID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		tblStageGradeDetails.numTaskId
		,tblStageGradeDetails.numAssigneId
		,tblStageGradeDetails.vcGradeId
	FROM
		StagePercentageDetails
	INNER JOIN
		tblStageGradeDetails
	ON
		StagePercentageDetails.numStageDetailsId = tblStageGradeDetails.numStageDetailsId
	WHERE
		StagePercentageDetails.numDomainId = @numDomainID
		AND StagePercentageDetails.slp_id = @numProcessID
END
GO