GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentWM_Save')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentWM_Save
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentWM_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
	,@tintSourceType TINYINT
	,@numCountryID NUMERIC(18,0)
	,@vcStateIDs VARCHAR(MAX)
	,@vcWarehousePriorities VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numStateID NUMERIC(18,0)
	)

	DECLARE @TEMPWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numStateID
	)
	SELECT
		Id
	FROM
		dbo.SplitIDs(@vcStateIDs,',')

	INSERT INTO @TEMPWarehouse
	(
		numWarehouseID
	)
	SELECT
		Id
	FROM
		dbo.SplitIDs(@vcWarehousePriorities,',')


	IF EXISTS (SELECT 
					MassSalesFulfillmentWMState.ID
				FROM 
					MassSalesFulfillmentWM
				INNER JOIN
					MassSalesFulfillmentWMState
				ON
					MassSalesFulfillmentWM.ID=MassSalesFulfillmentWMState.numMSFWMID
				WHERE 
					numDomainID = @numDomainID 
					AND numOrderSource=@numOrderSource
					AND numStateID IN (SELECT numStateID FROM @TEMP))
	BEGIN
		RAISERROR('DUPLICATE_STATE',16,1)
		RETURN
	END
	ELSE
	BEGIN
		INSERT INTO MassSalesFulfillmentWM
		(
			numDomainID
			,numOrderSource
			,numCountryID
			,tintSourceType
		)
		VALUES
		(
			@numDomainID
			,@numOrderSource
			,@numCountryID
			,@tintSourceType
		)
	
		DECLARE @ID NUMERIC(18,0)
		SET @ID = SCOPE_IDENTITY()

		INSERT INTO MassSalesFulfillmentWMState
		(
			numMSFWMID
			,numStateID
		)
		SELECT
			@ID
			,numStateID
		FROM
			@TEMP

		INSERT INTO MassSalesFulfillmentWP
		(
			numMSFWMID
			,numWarehouseID
			,intOrder
		)
		SELECT
			@ID
			,numWarehouseID
			,ID
		FROM
			@TEMPWarehouse
	END
END
GO