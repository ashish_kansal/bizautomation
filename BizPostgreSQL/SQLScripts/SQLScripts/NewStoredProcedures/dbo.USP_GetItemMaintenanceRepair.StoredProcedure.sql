SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemMaintenanceRepair')
DROP PROCEDURE USP_GetItemMaintenanceRepair
GO
CREATE PROCEDURE [dbo].[USP_GetItemMaintenanceRepair]         
@numDomainID as numeric(9)=0,    
@numItemCode as numeric(9)=0,
@ClientTimeZoneOffset Int      
as        

SELECT comm.numCommId,comm.numContactId,comm.numDivisionId, comm.textDetails,
comm.numActivity,dbo.GetListIemName(comm.numActivity) as vcActivity,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	else dbo.FormatedDateFromDate(DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime ),1) end  dtStartTime,
isnull(C.[numCorrespondenceID],0) numCorrespondenceID,
ISNULL(C.[numOpenRecordID],0) numOpenRecordID,
ISNULL(C.[tintCorrType],0) tintCorrType,
com.vcCompanyName as vcVendor,C.monMRItemAmount,
case when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,dtEventClosedDate) )= convert(varchar(11),getdate()) then '<b><font color=red>Today</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,dtEventClosedDate) )= convert(varchar(11),dateadd(day,-1,getdate())) then '<b><font color=purple>YesterDay</font></b>'
	 when convert(varchar(11),DateAdd(minute, -@ClientTimeZoneOffset,dtEventClosedDate) )= convert(varchar(11),dateadd(day,1,getdate())) then'<b><font color=orange>Tommorow</font></b>' 
	else dbo.FormatedDateFromDate(DateAdd(minute, -@ClientTimeZoneOffset,dtEventClosedDate ),1) end  dtEventClosedDate,
dbo.GetListIemName(Comm.bitTask)AS Task
FROM communication Comm JOIN [Correspondence] C ON C.[numCommID]=Comm.[numCommId]
join Item I on I.numItemCode= C.numOpenRecordID
join divisionMaster div  on div.numdivisionid=Comm.numDivisionId        
join companyInfo com on com.numCompanyID=div.numCompanyID   
WHERE  C.tintCorrType=7 and C.numDomainID=@numDomainID 
and Comm.numDomainID=@numDomainID
and I.numDomainID=@numDomainID
and C.numOpenRecordID=@numItemCode
and I.numItemCode= @numItemCode

GO
