GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetTotalProgress')
DROP FUNCTION GetTotalProgress
GO
ALTER FUNCTION [dbo].[GetTotalProgress]
(
	@numDomainID NUMERIC(18,0)
	,@numRecordID NUMERIC(18,0)
	,@tintRecordType TINYINT --1:WorkOrder, 2:Project
	,@tintProgressFor TINYINT -- 1:Record, 2:Milestome, 3:Stage
	,@vcMileStone VARCHAR(200)
	,@numStageDetailsId NUMERIC(18,0)	
)
RETURNS INT
AS   
BEGIN  
	DECLARE @numTotalProgress INT = 0

	DECLARE @TEMPStages TABLE
	(
		vcMileStoneName VARCHAR(200)
		,numMilestonePercentage INT
		,numStageDetailsId NUMERIC(18,0)
		,vcStageName VARCHAR(200)
		,numStagePercentage INT
		,numTotalStageTask INT
		,numCompletedStageTask INT
		,numStageProgress INT
		,numMilestoneProgress INT
	)

	INSERT INTO @TEMPStages
	(
		vcMileStoneName
		,numMilestonePercentage
		,numStageDetailsId
		,vcStageName
		,numStagePercentage
		,numTotalStageTask
		,numCompletedStageTask
	)
	SELECT
		StagePercentageDetails.vcMileStoneName
		,StagePercentageMaster.numStagePercentage AS numMilestonePercentage
		,StagePercentageDetails.numStageDetailsId
		,StagePercentageDetails.vcStageName
		,StagePercentageDetails.tintPercentage
		,ISNULL((SELECT COUNT(*) FROM StagePercentageDetailsTask SPDT WHERE SPDT.numStageDetailsId=StagePercentageDetails.numStageDetailsId),0) numTotalStageTask
		,ISNULL((SELECT COUNT(*) FROM StagePercentageDetailsTask SPDT WHERE SPDT.numStageDetailsId=StagePercentageDetails.numStageDetailsId AND EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=SPDT.numTaskId AND tintAction=4)),0) numCompletedStageTask
	FROM
		StagePercentageDetails
	INNER JOIN
		StagePercentageMaster
	ON
		StagePercentageDetails.numStagePercentageId = StagePercentageMaster.numStagePercentageId
	WHERE
		numDomainId=@numDomainID
		AND 1 = (CASE 
					WHEN @tintRecordType = 1 THEN (CASE WHEN numWorkOrderId=@numRecordID THEN 1 ELSE 0 END)
					WHEN @tintRecordType = 2 THEN (CASE WHEN numProjectID=@numRecordID THEN 1 ELSE 0 END)
					ELSE 0
				END)
	DECLARE @numTotalStageTask NUMERIC(18,0)=0
	DECLARE @numCompletedStageTask NUMERIC(18,0)=0
	SET @numTotalStageTask =(SELECT ISNULL((SELECT numTotalStageTask FROM @TEMPStages WHERE numStageDetailsId=@numStageDetailsId),0))
	SET @numCompletedStageTask =(SELECT ISNULL((SELECT numCompletedStageTask FROM @TEMPStages WHERE numStageDetailsId=@numStageDetailsId),0))
	UPDATE @TEMPStages SET numStageProgress = (CASE WHEN numTotalStageTask > 0 THEN (numCompletedStageTask * 100) / numTotalStageTask ELSE 0 END)

	UPDATE @TEMPStages SET numMilestoneProgress = (numStageProgress * numStagePercentage) / 100

	IF @tintProgressFor = 1
	BEGIN
		SET @numTotalProgress = ISNULL((SELECT AVG(numMilestoneProgress) FROM (SELECT SUM(numMilestoneProgress) numMilestoneProgress FROM @TEMPStages GROUP BY vcMileStoneName) TEMP),0)
	END
	ELSE IF @tintProgressFor = 2
	BEGIN
		SET @numTotalProgress = ISNULL((SELECT SUM(numMilestoneProgress) FROM @TEMPStages WHERE vcMileStoneName=@vcMileStone),0)
	END
	ELSE IF @tintProgressFor = 3
	BEGIN
		SET @numTotalProgress = ISNULL((SELECT numStageProgress FROM @TEMPStages WHERE numStageDetailsId=@numStageDetailsId),0)
	END
  
	RETURN @numTotalProgress
END
