SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ApplyPromotionOffer')
DROP PROCEDURE USP_ApplyPromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_ApplyPromotionOffer]    
 @numDomainID AS NUMERIC(9) = 0,
 @numSiteID AS NUMERIC(9) = 0,
 @vcCouponCode AS VARCHAR(20),
 @numDivisionId AS NUMERIC(9)=0
as    

DECLARE @numProId AS NUMERIC(9);SET @numProId=0
DECLARE @tintAppliesTo AS TINYINT;SET @tintAppliesTo=0
DECLARE @tintContactsType AS TINYINT;SET @tintContactsType=0
DECLARE @tintLimitationBasedOn AS TINYINT;SET @tintLimitationBasedOn=0
DECLARE @UsedCouponCount AS INT;SET @UsedCouponCount=0


SELECT TOP 1 @numProId=numProId,@tintAppliesTo=tintAppliesTo,@tintContactsType=tintContactsType,@tintLimitationBasedOn=tintLimitationBasedOn  
FROM PromotionOffer 
WHERE numDomainID=@numDomainID 
AND (numSiteID = @numSiteID OR ISNULL(@numSiteID,0) = -1) -- Changed By Manish Anjara On : 1st Feb,2013
AND vcCouponCode=@vcCouponCode

SELECT * FROM PromotionOffer WHERE numProId=@numProId

--Promotions Offer Contacts
IF @tintContactsType=1 OR @tintContactsType=2 --Organization,Profile-Relationship
  EXEC dbo.USP_GetPromotionOfferDtl @numProId,@numDomainID,2,@tintContactsType,@numSiteID,1
ELSE IF @tintContactsType=3 --All Customers
 SELECT''

--Promotion Offer Items
IF @tintAppliesTo=1 --Items
 EXEC dbo.USP_GetPromotionOfferDtl @numProId,@numDomainID,1,@tintAppliesTo,@numSiteID,1
 
ELSE IF @tintAppliesTo=2 --Items Category
  SELECT DISTINCT numItemID AS numValue FROM SiteCategories SC JOIN ItemCategory IC 
  ON SC.numCategoryID=IC.numCategoryID WHERE SC.numSiteId=@numSiteID AND SC.numCategoryID IN 
(SELECT numvalue FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=2 AND tintRecordType=1 )

ELSE IF @tintAppliesTo=3 --All Items
 SELECT ''


--Usage Limitation tintLimitationBasedOn==> 1:Customer 2:Site
SELECT @UsedCouponCount=COUNT(OL.numPromotionId) FROM OpportunityLinking OL JOIN OpportunityMaster OM ON OM.numOppID = OL.numChildOppID 
WHERE ISNULL(OL.numPromotionId,0)=@numProId AND 1=(CASE WHEN @tintLimitationBasedOn=1 THEN CASE WHEN @numDivisionId=0 THEN 1 --Without Login
																						  WHEN OM.numDivisionId=@numDivisionId THEN 1 ELSE 0 END --Customer
												  WHEN @tintLimitationBasedOn=2 THEN CASE WHEN ISNULL(OL.numSiteID,0)=@numSiteID THEN 1 ELSE 0 END --Site
												  ELSE 0 END)
												  
SELECT @UsedCouponCount	AS UsedCouponCount									  

GO
