GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCRVTaxDetails')
DROP PROCEDURE USP_GetCRVTaxDetails
GO
CREATE PROCEDURE USP_GetCRVTaxDetails
@numDomainID AS NUMERIC(18,0),
@numItemCode AS NUMERIC(9)
AS
BEGIN
	SELECT 
		1 AS numTaxItemID,
		TaxDetails.numTaxID,
		CONCAT('CRV',CASE WHEN ISNULL(vcTaxUniqueName,'') = '' THEN '' ELSE ' (' + vcTaxUniqueName + ')' END) vcTaxName,
		ISNULL(bitApplicable,0) AS bitApplicable 
	FROM 
		TaxDetails
	LEFT JOIN 
		ItemTax 
	ON 
		ItemTax.numTaxItemID = 1 
		AND ItemTax.numTaxID = TaxDetails.numTaxID
		AND numItemCode=@numItemCode
	WHERE 
		numDomainID=@numDomainID 
		AND TaxDetails.numTaxItemID = 1
END

