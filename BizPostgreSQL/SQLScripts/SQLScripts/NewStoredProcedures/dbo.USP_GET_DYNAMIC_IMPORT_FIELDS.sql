GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_DYNAMIC_IMPORT_FIELDS')
DROP PROCEDURE USP_GET_DYNAMIC_IMPORT_FIELDS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- USP_GET_DYNAMIC_IMPORT_FIELDS 1,2,1,1
CREATE PROCEDURE [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]
(
	@intImportMasterID	BIGINT,
	@intMode	INT,
	@numImportFileID NUMERIC = 0,
	@numDomainID NUMERIC = 0
)
AS 
BEGIN
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 1,2,1,1
	-- SELECT * FROM Import_File_Field_Mapping
	IF @intMode = 1 -- TO BIND CATEGORY DROP DOWN
	BEGIN
		SELECT PARENT.intImportMasterID,
			   PARENT.vcImportName
		FROM dbo.Import_Master PARENT
	END
	
	IF @intMode = 2 -- TO GET DETAILS OF DYNAMIC FORM FIELDS
	BEGIN
		DECLARE @numCustomFieldPageID AS NUMERIC
		SELECT @numCustomFieldPageID = numCustomFieldPageID FROM dbo.Import_Master WHERE intImportMasterID = @intImportMasterID
		
		DECLARE @SectionName AS VARCHAR(MAX)
		SET @SectionName = (SELECT vcSectionNames FROM dbo.Import_Master WHERE intImportMasterID = @intImportMasterID )
		SELECT DISTINCT dbo.SplitString.* FROM dbo.SplitString(@SectionName,',')
		INNER JOIN dbo.Import_Field_Master IMPORT_FIELD ON dbo.SplitString.Id = IMPORT_FIELD.intSectionID  
		UNION ALL
		SELECT 1000,'Custom Field'
		
		SELECT  ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.intImportFieldID ) AS SrNo ,
        X.*
FROM    ( SELECT    IFM.intImportFieldID ,
                    IFM.numFormFieldID ,
                    IM.vcSectionNames ,
                    DYNAMIC_FIELD.vcFormFieldName ,
                    DYNAMIC_FIELD.vcDbColumnName ,
                    IFM.intSectionID ,
                    0 [IsCustomField] ,
                     ISNULL((SELECT intMapColumnNo FROM dbo.Import_File_Field_Mapping FFM 
							 INNER JOIN dbo.Import_File_Master FM ON FM.intImportFileID = FFM.intImportFileID 
							 WHERE FFM.numFormFieldID = IFM.numFormFieldID 
							   AND FM.intImportFileID = @numImportFileID 
							   AND FM.numDomainID = @numDomainID),0) intMapColumnNo,
				    DYNAMIC_FIELD.vcPropertyName,
				    CASE WHEN ISNULL(DYNAMIC_FIELD.bitRequired,0) = 0
						 THEN ''
						 ELSE '*'
					END  AS [vcReqString],
					bitRequired AS [bitRequired]
          FROM      dbo.Import_Field_Master IFM
                    INNER JOIN dbo.Import_Master IM ON IFM.intImportMasterId = IM.intImportMasterID
                    LEFT JOIN dbo.DynamicFormFieldMaster DYNAMIC_FIELD ON IFM.numFormFieldID = DYNAMIC_FIELD.numFormFieldId
          WHERE     IM.intImportMasterID = @intImportMasterID
          UNION ALL
          SELECT    -1 ,
                    CFm.Fld_id ,
                    'CustomField' ,
                    cfm.Fld_label ,
                    cfm.Fld_label ,
                    1000 ,
                    1 ,
                    ISNULL(( SELECT intMapColumnNo
                             FROM   dbo.Import_File_Field_Mapping IIFM
                             WHERE  IIFM.intImportFileID = @numImportFileID
                                    AND IIFM.numFormFieldID = CFM.Fld_id
                                    AND bitCustomField=1
                           ), 0) intMapColumnNo,
					'' AS [vcPropertyName] ,
					'' AS [vcReqString],
					0 AS [bitRequired]                         
          FROM      dbo.CFW_Fld_Master CFM
          WHERE     Grp_id = @numCustomFieldPageID
                    AND numDomainID = @numDomainID
                   
        ) X ORDER BY (X.intSectionID)
		
--				
--		SELECT ROW_NUMBER() OVER (ORDER BY intSectionID,CHILD.intImportFieldID,CHILD.numFormFieldID) AS SrNo,
--			   CHILD.intImportFieldID,
--			   CHILD.numFormFieldID,
--			   PARENT.vcSectionNames,
--			   CASE WHEN (SELECT fld_id FROM dbo.CFW_Fld_Master MASTER
--					  LEFT JOIN dbo.CFw_Grp_Master GROUP_MASTER ON MASTER.subgrp = GROUP_MASTER.Grp_id
--					  WHERE MASTER.Grp_id = @numCustomFieldPageID
--					    AND MASTER.numDomainID = @numDomainID
--						AND ISNULL(GROUP_MASTER.tintType,0) = 0
--						AND Fld_id = CHILD.numFormFieldID) = CHILD.numFormFieldID
--			    THEN (SELECT Fld_label FROM dbo.CFW_Fld_Master MASTER
--					  LEFT JOIN dbo.CFw_Grp_Master GROUP_MASTER ON MASTER.subgrp = GROUP_MASTER.Grp_id
--					  WHERE MASTER.Grp_id = @numCustomFieldPageID
--					    AND MASTER.numDomainID = @numDomainID
--						AND ISNULL(GROUP_MASTER.tintType,0) = 0
--						AND Fld_id = CHILD.numFormFieldID)		
--			    ELSE DYNAMIC_FIELD.vcFormFieldName
--		   END	AS vcFormFieldName,
--			   DYNAMIC_FIELD.vcDbColumnName,
--			   CHILD.intSectionID,
--			   ISNULL((SELECT intMapColumnNo FROM dbo.Import_File_Field_Mapping FFM 
--					   INNER JOIN dbo.Import_File_Master FM ON FM.intImportFileID = FFM.intImportFileID 
--					   WHERE FFM.numFormFieldID = CHILD.numFormFieldID 
--					     AND FM.intImportFileID = @numImportFileID 
--					     AND FM.numDomainID=@numDomainID),0) intMapColumnNo,
--			   CASE WHEN (SELECT fld_id FROM dbo.CFW_Fld_Master MASTER
--						  LEFT JOIN dbo.CFw_Grp_Master GROUP_MASTER ON MASTER.subgrp = GROUP_MASTER.Grp_id
--						  WHERE MASTER.Grp_id = @numCustomFieldPageID
--							AND MASTER.numDomainID = @numDomainID
--							AND ISNULL(GROUP_MASTER.tintType,0) = 0
--							AND (Fld_id = CHILD.numFormFieldID OR Fld_id = DYNAMIC_FIELD.numFormFieldId) ) = CHILD.numFormFieldID
--					THEN 1		
--					ELSE 0
--			    END  AS [IsCustomField]
--		FROM dbo.Import_Master PARENT
--		INNER JOIN dbo.Import_Field_Master CHILD ON PARENT.intImportMasterID = CHILD.intImportMasterId
--		LEFT JOIN dbo.DynamicFormFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD.numFormFieldId = CHILD.numFormFieldID
--		WHERE PARENT.intImportMasterID = @intImportMasterID 
	END	
END

