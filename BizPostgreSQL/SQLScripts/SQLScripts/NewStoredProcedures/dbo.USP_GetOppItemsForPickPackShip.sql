GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppItemsForPickPackShip' ) 
    DROP PROCEDURE USP_GetOppItemsForPickPackShip
GO
CREATE PROCEDURE USP_GetOppItemsForPickPackShip
    @numDomainID AS NUMERIC(18,0),
    @numOppID AS NUMERIC(18,0),
	@numBizDocID AS NUMERIC(18,0)
AS 
BEGIN
	IF @numBizDocID = 29397 -- Packing Slip
	BEGIN
		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour - ISNULL(SUM(OBIPackingSlip.numUnitHour),0) AS QtytoFulFillOriginal,
			(OI.numUnitHour - (CASE WHEN ISNULL(OBDFulfillment.numUnitHour,0) > 0 THEN ISNULL(OBDFulfillment.numUnitHour,0) ELSE ISNULL(SUM(OBIPackingSlip.numUnitHour),0) END)) AS QtytoFulFill,
			CONCAT('<ul class="list-unstyled">',STUFF((SELECT 
														CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numOppId,''',''',OB.numOppBizDocsId,''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>') 
													FROM 
														OpportunityBizDocs OB
													INNER JOIN
														OpportunityBizDocItems OBI
													ON
														OB.numOppBizDocsId=OBI.numOppBizDocID
													WHERE 
														OB.numOppId = @numOppID
														AND OB.numBizDocId = @numBizDocID
														AND OBI.numOppItemID=OI.numoppitemtCode
														AND ISNULL(OBI.numUnitHour,0) <> 0
													ORDER BY
														OB.numOppBizDocsId
													FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''),'</ul>') vcPickLists
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		LEFT JOIN
			OpportunityBizDocs OBDPackingSlip
		ON
			OM.numOppId = OBDPackingSlip.numOppId
			AND OBDPackingSlip.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBIPackingSlip
		ON
			OBDPackingSlip.numOppBizDocsId = OBIPackingSlip.numOppBizDocID
			AND OBIPackingSlip.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 296
		) OBDFulfillment
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OBDFulfillment.numUnitHour
	END
	ELSE IF @numBizDocID = 296 -- Fulfillment
	BEGIN
		DECLARE @tintCommitAllocation AS TINYINT
		DECLARE @bitAllocateInventoryOnPickList AS BIT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppID

		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			ISNULL(CASE 
				WHEN @numBizDocId = 296 
				THEN
					(CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
								THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
							END)
						ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
					END)
				ELSE
					(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
			END,0) AS QtytoFulFillOriginal,
			(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) QtyToFulfillWithoutAllocationCheck,
			ISNULL((CASE 
				WHEN ISNULL(SUM(OBI.numUnitHour),0) >= ISNULL(OBDPackingSlip.numUnitHour,0) 
				THEN (CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList) 
								THEN (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
							END)
						ELSE (OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0))
					END) 
				ELSE (CASE 
						WHEN (I.charItemType = 'p' AND ISNULL(OI.bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)
						THEN 
							(CASE 
								WHEN (ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)) <= dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
								THEN (ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)) 
								ELSE dbo.GetOrderItemAvailableQtyToShip(OI.numoppitemtCode,OI.numWarehouseItmsID,ISNULL(I.bitKitParent,0),@tintCommitAllocation,@bitAllocateInventoryOnPickList)
							END)
						ELSE ISNULL(OBDPackingSlip.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0)
					END) 
			END),0) AS QtytoFulFill,
			CONCAT('<ul class="list-unstyled">',STUFF((SELECT 
														CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numOppId,''',''',OB.numOppBizDocsId,''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>') 
													FROM 
														OpportunityBizDocs OB
													INNER JOIN
														OpportunityBizDocItems OBI
													ON
														OB.numOppBizDocsId=OBI.numOppBizDocID
													WHERE 
														OB.numOppId = @numOppID
														AND OB.numBizDocId = @numBizDocID
														AND OBI.numOppItemID=OI.numoppitemtCode
														AND ISNULL(OBI.numUnitHour,0) <> 0
													ORDER BY
														OB.numOppBizDocsId
													FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''),'</ul>') vcFulfillmentBizDocs
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		LEFT JOIN
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND OBD.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBI
		ON
			OBD.numOppBizDocsId = OBI.numOppBizDocID
			AND OBI.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 29397
		) OBDPackingSlip
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OI.numWarehouseItmsID
			,OI.bitDropShip
			,I.charItemType
			,I.bitAsset
			,I.bitKitParent
			,OBDPackingSlip.numUnitHour
	END
	ELSE IF @numBizDocID = 287 -- Invoice
	BEGIN
		SELECT    
			OI.numoppitemtCode,
			OI.numUnitHour AS QtyOrdered,
			OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0) AS QtytoFulFillOriginal,
			ISNULL((CASE 
				WHEN ISNULL(SUM(OBI.numUnitHour),0) >= ISNULL(OBDFulfillment.numUnitHour,0) 
				THEN OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0) 
				ELSE ISNULL(OBDFulfillment.numUnitHour,0) - ISNULL(SUM(OBI.numUnitHour),0) 
			END),0) AS QtytoFulFill,
			CONCAT('<ul class="list-unstyled">',STUFF((SELECT 
														CONCAT('<li><a onclick="return OpenBizInvoice(''',OB.numOppId,''',''',OB.numOppBizDocsId,''',0)" href="#">',OB.vcBizDocID,' (',OBI.numUnitHour,')</a></li>') 
													FROM 
														OpportunityBizDocs OB
													INNER JOIN
														OpportunityBizDocItems OBI
													ON
														OB.numOppBizDocsId=OBI.numOppBizDocID
													WHERE 
														OB.numOppId = @numOppID
														AND OB.numBizDocId = @numBizDocID
														AND OBI.numOppItemID=OI.numoppitemtCode
														AND ISNULL(OBI.numUnitHour,0) <> 0
													ORDER BY
														OB.numOppBizDocsId
													FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''),'</ul>') vcInvoices
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		LEFT JOIN
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
			AND OBD.numBizDocId = @numBizDocID
		LEFT JOIN
			OpportunityBizDocItems OBI
		ON
			OBD.numOppBizDocsId = OBI.numOppBizDocID
			AND OBI.numOppItemID = OI.numoppitemtCode
		OUTER APPLY
		(
			SELECT
				SUM(numUnitHour) AS numUnitHour
			FROM
				OpportunityBizDocs
			LEFT JOIN
				OpportunityBizDocItems 
			ON
				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
			WHERE
				OpportunityBizDocs.numOppId = OM.numOppId
				AND OpportunityBizDocs.numBizDocId = 296
		) OBDFulfillment
		WHERE     
			OI.numOppId = @numOppID
		GROUP BY
			OI.numoppitemtCode
			,OI.numUnitHour
			,OBDFulfillment.numUnitHour
	END
END
GO