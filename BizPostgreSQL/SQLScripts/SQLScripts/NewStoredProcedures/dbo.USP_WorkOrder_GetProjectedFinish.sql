GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetProjectedFinish')
DROP PROCEDURE USP_WorkOrder_GetProjectedFinish
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetProjectedFinish]                             
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)   
	,@ClientTimeZoneOffset INT                     
AS                            
BEGIN
	SELECT dbo.GetProjectedFinish(@numDomainID,2,@numWOID,0,0,NULL,@ClientTimeZoneOffset,0)
END
GO