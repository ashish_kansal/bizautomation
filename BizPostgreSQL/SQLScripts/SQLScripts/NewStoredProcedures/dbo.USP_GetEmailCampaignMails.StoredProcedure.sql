/****** Object:  StoredProcedure [dbo].[USP_GetEmailCampaignMails]    Script Date: 06/04/2009 15:10:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmailCampaignMails')
DROP PROCEDURE USP_GetEmailCampaignMails
GO
CREATE PROCEDURE [dbo].[USP_GetEmailCampaignMails]
AS 
BEGIN
-- Organization Fields Which Needs To Be Replaced
DECLARE  @vcCompanyName  AS VARCHAR(200)
DECLARE  @OrgNoOfEmployee  AS VARCHAR(12)
DECLARE  @OrgShippingAddress  AS VARCHAR(1000)
DECLARE  @Signature  AS VARCHAR(1000)


-- Contact Fields Which Needs To Be Replaced.
DECLARE @ContactAge AS VARCHAR(3)
DECLARE @ContactAssistantEmail AS VARCHAR(100)
DECLARE @AssistantFirstName AS VARCHAR(100)
DECLARE @AssistantLastName AS VARCHAR(100)
DECLARE @ContactAssistantPhone AS VARCHAR(20)
DECLARE @ContactAssistantPhoneExt AS VARCHAR(10)
DECLARE @ContactCategory AS VARCHAR(100)
DECLARE @ContactCell AS VARCHAR(20)
DECLARE @ContactDepartment AS VARCHAR(100)
DECLARE @ContactDripCampaign AS VARCHAR(500)
DECLARE @ContactEmail AS VARCHAR(100)
DECLARE @ContactFax AS VARCHAR(100)
DECLARE @ContactFirstName AS VARCHAR(100)
DECLARE @ContactGender AS VARCHAR(10)
DECLARE @ContactHomePhone AS VARCHAR(20)
DECLARE @ContactLastName AS VARCHAR(100)
DECLARE @ContactManager AS VARCHAR(100)
DECLARE @ContactPhone AS VARCHAR(20)	
DECLARE @ContactPhoneExt AS VARCHAR(10)
DECLARE @ContactPosition AS VARCHAR(100)
DECLARE @ContactPrimaryAddress AS VARCHAR(1000)
DECLARE @ContactStatus AS VARCHAR(100)
DECLARE @ContactTeam AS VARCHAR(100)
DECLARE @ContactTitle AS VARCHAR(100)
DECLARE @ContactType AS VARCHAR(100)
      
DECLARE  @numConEmailCampID  AS NUMERIC(9)
DECLARE  @intStartDate  AS DATETIME


DECLARE  @numConECampDTLID  AS NUMERIC(9)
declare @numContactID as numeric(9)   
declare @numFromContactID as numeric(9)   
declare @numEmailTemplate as varchar(15)
declare @numDomainId as numeric(9)
DECLARE  @tintFromField  AS TINYINT
DECLARE @numCampaignOwner NUMERIC(18,0)

declare @vcSubject as varchar(1000)
declare @vcBody as varchar(8000)              
declare @vcFormattedBody as varchar(8000)

DECLARE @LastDateTime DATETIME
DECLARE @CurrentDateTime DATETIME

SELECT @LastDateTime = [dtLastDateTimeECampaign] FROM [WindowsServiceHistory]
SET @CurrentDateTime = DATEADD(minute, DATEpart(minute,GETUTCDATE()) ,DATEADD(hour,DATEpart(hour,GETUTCDATE()),dbo.[GetUTCDateWithoutTime]()))
PRINT @LastDateTime
PRINT @CurrentDateTime


Create table #tempTableSendApplicationMail
(  vcTo varchar(2000),              
   vcSubject varchar(2000),              
   vcBody varchar(Max),              
   vcCC varchar(2000),      
   numDomainID numeric(9),
   numConECampDTLID NUMERIC(9) NULL,
   tintFromField TINYINT,
   numContactID NUMERIC,
   numFromContactID NUMERIC,
   numCampaignOwner NUMERIC(18,0)
 )                

-- EC.fltTimeZone-2 is used to fetch all email which are not sent from past 2 housrs of last execution date 
-- in case of amazon ses service is down and not able to send email in first try
SELECT   TOP 1 @numContactID = C.numContactID,
               @numConECampDTLID = numConECampDTLID,
               @numConEmailCampID = numConEmailCampID,
               @intStartDate = intStartDate,
               @numEmailTemplate = numEmailTemplate,
               @numDomainId = EC.numDomainID,
               @tintFromField = EC.[tintFromField],
			   @numFromContactID = numFromContactID,
			   @numCampaignOwner = C.numRecOwner
FROM     ConECampaign C
         JOIN ConECampaignDTL DTL
           ON numConEmailCampID = numConECampID
         JOIN ECampaignDTLs E
           ON DTL.numECampDTLId = E.numECampDTLID
         JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID] 
WHERE    bitSend IS NULL 
         AND bitengaged = 1
         AND numEmailTemplate > 0 
		 AND ISNULL(EC.bitDeleted,0) = 0
		 -- When campaign is first time assigned to contact it may happen that it is assigned after service already executed 
		 -- so first email will be send immediately and after that service will compare execution datetime and current datetime
         AND ((DTL.dtExecutionDate BETWEEN DATEADD(HOUR,-2,@LastDateTime) AND @CurrentDateTime) OR
			(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND ISNULL(bitSend,0) = 1) = 0)
          
ORDER BY numConEmailCampID
PRINT '@numConEmailCampID='
PRINT @numConEmailCampID



WHILE @numConEmailCampID > 0
  BEGIN
	PRINT 'inside @numConEmailCampID='
	PRINT @numConEmailCampID

    SELECT 
		   @ContactFirstName = ISNULL(A.vcFirstName,''),
           @ContactLastName = ISNULL(A.vcLastName,''),
           @vcCompanyName = ISNULL(C.vcCompanyName,''),
		   @OrgNoOfEmployee = ISNULL(dbo.GetListIemName(C.numNoOfEmployeesId),''),
		   @OrgShippingAddress = vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>' + ISNULL(AD2.VcCity,'') + ' ,' 
								+ ISNULL(dbo.fn_GetState(AD2.numState),'') + ' ' + ISNULL(AD2.vcPostalCode,'') + ' <br>' + 
								ISNULL(dbo.fn_GetListItemName(AD2.numCountry),''),								   
		   @Signature = ISNULL((select U.txtSignature from UserMaster U where U.numUserDetailId=A.numRecOwner),''),
           @ContactPhone = ISNULL(A.numPhone,''),
           @ContactEmail = ISNULL(A.vcEmail,''),
		   @ContactAge = ISNULL(dbo.GetAge(A.bintDOB, GETUTCDATE()),''),
		   @ContactAssistantEmail = ISNULL(A.vcAsstEmail,''),
		   @AssistantFirstName = ISNULL(A.VcAsstFirstName,''),
		   @AssistantLastName = ISNULL(A.vcAsstLastName,''),
		   @ContactAssistantPhone = ISNULL(A.numAsstPhone,''),
		   @ContactAssistantPhoneExt= ISNULL(A.numAsstExtn,''),
		   @ContactCategory=ISNULL([dbo].[GetListIemName](A.vcCategory),''),
		   @ContactCell=ISNULL(A.numCell,''),
		   @ContactDepartment=ISNULL([dbo].[GetListIemName](A.vcDepartment),''),
		   @ContactDripCampaign=ISNULL((SELECT vcECampName FROM [ECampaign] WHERE numECampaignID = A.numECampaignID), ''),
		   @ContactFax=ISNULL(A.vcFax,''),
		   @ContactGender=(CASE WHEN A.charSex = 'M' THEN 'Male' WHEN A.charSex = 'F' THEN 'Female' ELSE '-' END),
		   @ContactHomePhone=ISNULL(A.numHomePhone,''),
		   @ContactManager=ISNULL(dbo.fn_GetContactName(A.numManagerId),''),
		   @ContactPhoneExt=ISNULL(A.numPhoneExtension,''),
		   @ContactPosition= ISNULL([dbo].[GetListIemName](A.vcPosition),''),
		   @ContactPrimaryAddress=ISNULL(dbo.getContactAddress(A.numContactID),''),
		   @ContactStatus=ISNULL([dbo].[GetListIemName](A.numEmpStatus),''),
		   @ContactTeam=ISNULL([dbo].[GetListIemName](A.numTeam),''),
		   @ContactTitle=ISNULL(A.vcTitle,''),
		   @ContactType=ISNULL(dbo.GetListIemName(A.numContactType),'')
    FROM   AdditionalContactsInformation A
           JOIN DivisionMaster D
             ON A.numDivisionId = D.numDivisionID
           JOIN CompanyInfo C
             ON D.numCompanyID = C.numCompanyId
		   LEFT JOIN 
				dbo.AddressDetails AD2 
			ON 
				AD2.numDomainID=D.numDomainID 
				AND AD2.numRecordID= D.numDivisionID 
				AND AD2.tintAddressOf=2 
				AND AD2.tintAddressType=2 
				AND AD2.bitIsPrimary=1
    WHERE  A.numContactId = @numContactID
    
    SELECT @vcSubject = vcSubject,
           @vcBody = vcDocDesc
    FROM   genericdocuments
    WHERE  numGenericDocID = @numEmailTemplate
    
	--Replace organization fields from body
    SET @vcFormattedBody = REPLACE(@vcBody,'##OrganizationName##',@vcCompanyName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##OrgNoOfEmployee##',@OrgNoOfEmployee)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##OrgShippingAddress##',@OrgShippingAddress)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##Signature##',@Signature)

	--Replace contact fields from body
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactFirstName##',@ContactFirstName)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactLastName##',@ContactLastName)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPhone##',@ContactPhone)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactEmail##',@ContactEmail)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAge##',@ContactAge)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantEmail##',@ContactAssistantEmail)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##AssistantFirstName##',@AssistantFirstName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##AssistantLastName##',@AssistantLastName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantPhone##',@ContactAssistantPhone)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantPhoneExt##',@ContactAssistantPhoneExt)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactCategory##',@ContactCategory)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactCell##',@ContactCell)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactDepartment##',@ContactDepartment)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactDripCampaign##',@ContactDripCampaign)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactFax##',@ContactFax)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactGender##',@ContactGender)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactHomePhone##',@ContactHomePhone)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactManager##',@ContactManager)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPhoneExt##',@ContactPhoneExt)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPosition##',@ContactPosition)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPrimaryAddress##',@ContactPrimaryAddress)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactStatus##',@ContactStatus)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactTeam##',@ContactTeam)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactTitle##',@ContactTitle)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactType##',@ContactType)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactOpt-OutLink##','')

    
   INSERT INTO #tempTableSendApplicationMail
   VALUES (     @ContactEmail,
                @vcSubject,
                @vcFormattedBody,
                '',
                @numDomainId,
                @numConECampDTLID,
                @tintFromField,
                @numContactID,
                @numFromContactID,@numCampaignOwner)
                

    
    SELECT   TOP 1 @numContactID = C.numContactID,
                   @numConECampDTLID = numConECampDTLID,
                   @numConEmailCampID = numConEmailCampID,
                   @intStartDate = intStartDate,
                   @numEmailTemplate = numEmailTemplate,
                   @numDomainId = EC.numDomainID,
                   @tintFromField = EC.[tintFromField],
                   @numFromContactID = numFromContactID,
				   @numCampaignOwner = C.numRecOwner
    FROM     ConECampaign C
             JOIN ConECampaignDTL DTL
               ON numConEmailCampID = numConECampID
             JOIN ECampaignDTLs E
               ON DTL.numECampDTLId = E.numECampDTLID
             JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID] 
--             JOIN AdditionalContactsInformation A
--               ON A.numContactID = C.numRecOwner
    WHERE    bitSend IS NULL 
             AND bitengaged = 1
             AND numConEmailCampID > @numConEmailCampID
			 AND numEmailTemplate > 0 
			 AND ISNULL(EC.bitDeleted,0) = 0
			-- When campaign is first time assigned to contact it may happen that it is assigned after service already executed 
		 -- so first email will be send immediately and after that service will compare execution datetime and current datetime
         AND ((DTL.dtExecutionDate BETWEEN DATEADD(HOUR,-2,@LastDateTime) AND @CurrentDateTime) OR
			(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND ISNULL(bitSend,0) = 1) = 0)
    ORDER BY numConEmailCampID
    
    IF @@ROWCOUNT = 0
      SET @numConEmailCampID = 0
  END
  
  
  SELECT    X.*,
			ISNULL(EmailBroadcastConfiguration.vcAWSDomain,'') vcAWSDomain,
            ISNULL(EmailBroadcastConfiguration.vcAWSAccessKey,'') vcAWSAccessKey,
            ISNULL(EmailBroadcastConfiguration.vcAWSSecretKey,'') vcAWSSecretKey,
            CASE WHEN LEN(vcCampaignOwnerEmail) > 0 THEN vcCampaignOwnerEmail ELSE ISNULL(EmailBroadcastConfiguration.vcFrom,'') END vcFrom
  FROM      ( SELECT    A.[vcTo],
                        A.[vcSubject],
                        A.[vcBody],
                        A.[vcCC],
                        A.[numDomainID],
                        A.[numConECampDTLID],
                        A.[tintFromField],
                        A.[numContactID],
                        CASE [tintFromField]
                          WHEN 1 THEN --Record Owner of Company
                               DM.[numRecOwner]
                          WHEN 2 THEN --Assignee of Company
                               DM.[numAssignedTo]
                          WHEN 3 THEN numFromContactID
                          ELSE DM.[numRecOwner]
                        END AS numFromContactID,
						ISNULL(UM.vcEmailID,'') AS vcCampaignOwnerEmail
              FROM      #tempTableSendApplicationMail A
                        INNER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = A.numContactID
						LEFT JOIN UserMaster UM ON A.numCampaignOwner = UM.numUserDetailId
                        INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = ACI.[numDivisionId]
            ) X
            LEFT JOIN 
				EmailBroadcastConfiguration
			ON
				X.numDomainID = EmailBroadcastConfiguration.numDomainID
  
  DROP TABLE #tempTableSendApplicationMail

  UPDATE [WindowsServiceHistory] SET [dtLastDateTimeECampaign] = GETUTCDATE()
END