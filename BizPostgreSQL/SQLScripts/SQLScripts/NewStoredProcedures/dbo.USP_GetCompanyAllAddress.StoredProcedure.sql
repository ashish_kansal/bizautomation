GO
/****** Object:  StoredProcedure [dbo].[USP_GetCompanyAllAddress]    Script Date: 02/15/2010 22:32:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetCompanyAllAddress @numDomainID=1,@numDivisionID=1

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCompanyAllAddress')
DROP PROCEDURE USP_GetCompanyAllAddress
GO
CREATE PROCEDURE [dbo].[USP_GetCompanyAllAddress]
@numDomainID numeric(18),
@numDivisionID numeric(18)

as

begin

create table #Temp_Location
(
numLocId numeric identity,
vcLocation varchar(2000))

insert into #Temp_Location
 


select 
	isnull(AD1.vcStreet,'') + ',' +
	isnull(AD1.vcCity,'') + ',' + 
	isnull(dbo.fn_GetState(AD1.numState),'') + ',' +
	isnull(AD1.vcPostalCode,'') + ',' +
	isnull(dbo.fn_GetListName(AD1.numCountry,0),'')  as Location
from AddressDetails AD1 where numDomainId=@numDomainID and numRecordID=@numDivisionID AND tintAddressOf=2
UNION
select 
	isnull(AD1.vcStreet,'') + ',' +
	isnull(AD1.vcCity,'') + ',' + 
	isnull(dbo.fn_GetState(AD1.numState),'') + ',' +
	isnull(AD1.vcPostalCode,'') + ',' +
	isnull(dbo.fn_GetListName(AD1.numCountry,0),'')  as Location
from AddressDetails AD1 where numDomainId=@numDomainID and numRecordID=@numDivisionID AND tintAddressOf=1


select numLocId,replace(vcLocation,',,','') as vcLocation  from #Temp_Location where len(vcLocation)>10;
drop table #Temp_Location;
end