SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10SalesOpportunityByPastDue')
DROP PROCEDURE USP_ReportListMaster_Top10SalesOpportunityByPastDue
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10SalesOpportunityByPastDue]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@tintOppType INT
	,@vcDealAmount VARCHAR(250)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@tintTotalProgress INT
	,@tintMinNumber INT
	,@tintMaxNumber INT
	,@tintQtyToDisplay INT
	,@vcDueDate VARCHAR(250)
	,@dtFromDate DATE
	,@dtToDate DATE
AS
BEGIN 

	DECLARE @tintFiscalStartMonth TINYINT

	SELECT @tintFiscalStartMonth=ISNULL(tintFiscalStartMonth,1) FROM Domain WHERE numDomainId=@numDomainID
	DECLARE @CurrentYearStartDate DATE 
	DECLARE @CurrentYearEndDate DATE

	SET @CurrentYearStartDate = DATEFROMPARTS(YEAR(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())),@tintFiscalStartMonth,1)
	IF @CurrentYearStartDate > DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	BEGIN
		SET @CurrentYearStartDate = DATEADD(YEAR,-1,@CurrentYearStartDate)
	END	
	SET @CurrentYearEndDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@CurrentYearStartDate))

	DECLARE @TABLEQuater TABLE
	(
		ID INT
		,QuaterStartDate DATE
		,QuaterEndDate DATE
	)

	INSERT INTO @TABLEQuater
	(
		ID
		,QuaterStartDate
		,QuaterEndDate
	)
	VALUES
	(
		1,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1),DATEADD(DAY,-1,DATEADD(MONTH,3,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		2,DATEADD(MONTH,3,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,6,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		3,DATEADD(MONTH,6,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,9,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		4,DATEADD(MONTH,9,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,12,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	)

	IF @vcDueDate = 'CurYear'
	BEGIN
		SET @dtFromDate = @CurrentYearStartDate
		SET @dtToDate = @CurrentYearEndDate
	END
	ELSE IF @vcDueDate = 'PreYear'
	BEGIN
		SET @dtFromDate =  DATEADD(YEAR,-1,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@dtFromDate))
	END
	ELSE IF @vcDueDate = 'Pre2Year'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-2,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,2,@dtFromDate))
	END
	ELSE IF @vcDueDate = 'Ago2Year'
	BEGIN
		SET @dtFromDate =  NULL
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,-2,@CurrentYearStartDate))
	END
	ELSE IF @vcDueDate = 'CurPreYear'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-1,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@dtFromDate))
	END
	ELSE IF @vcDueDate = 'CurPre2Year'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-2,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,1, DATEFROMPARTS(YEAR(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())),@tintFiscalStartMonth,1)))
	END
	ELSE IF @vcDueDate = 'CuQur'
	BEGIN
		SELECT
			@dtFromDate = QuaterStartDate
			,@dtToDate = QuaterEndDate
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcDueDate = 'PreQur'
	BEGIN
		SELECT
			@dtFromDate = DATEADD(MONTH,-3,QuaterStartDate)
			,@dtToDate = DATEADD(MONTH,-3,QuaterEndDate)
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcDueDate = 'CurPreQur'
	BEGIN
		SELECT
			@dtFromDate = DATEADD(MONTH,-3,QuaterStartDate)
			,@dtToDate = QuaterEndDate
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcDueDate = 'ThisMonth'
	BEGIN
		SET @dtFromDate = dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcDueDate = 'LastMonth'
	BEGIN
		SET @dtFromDate = DATEADD(MONTH,-1,dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = DATEADD(MONTH,-1,dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
	END
	ELSE IF @vcDueDate = 'CurPreMonth'
	BEGIN
		SET @dtFromDate = DATEADD(MONTH,-1,dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcDueDate = 'LastWeek'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-7,dbo.get_week_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = DATEADD(DAY,-7,dbo.get_week_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
	END
	ELSE IF @vcDueDate = 'ThisWeek'
	BEGIN
		SET @dtFromDate = dbo.get_week_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = dbo.get_week_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcDueDate = 'Yesterday'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcDueDate = 'Today'
	BEGIN
		SET @dtFromDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcDueDate = 'Last7Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-7,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcDueDate = 'Last30Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-30,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcDueDate = 'Last60Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-60,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcDueDate = 'Last90Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-90,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcDueDate = 'Last120Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-120,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END

	DECLARE @TodayDate DATE
	DECLARE @CreatedDate DATE
	SET @TodayDate = DATEADD(MINUTE,@ClientTimeZoneOffset,GETUTCDATE())

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -12, @TodayDate)
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -6, @TodayDate)
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @CreatedDate = DATEADD(MONTH, -3, @TodayDate)
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @CreatedDate = DATEADD(DAY, -30, @TodayDate)
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @CreatedDate = DATEADD(DAY, -7, @TodayDate)
	END
	

	SELECT DISTINCT TOP (@tintQtyToDisplay)
		OM.vcPOppName
		,CONCAT('~/opportunity/frmOpportunities.aspx?opId=',OM.numOppId) AS URL
		,ISNULL(monTotAmount,0) monTotAmount
		,ISNULL(OM.numPercentageComplete,0) numPercentageComplete
	FROM
		OpportunityItems OI
	INNER JOIN
		OpportunityMaster OM
	ON
		OI.numOppId = OM.numOppID
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numDivisionId = DM.numDivisionID
	LEFT JOIN 
		ProjectProgress 
	ON 
		OM.numOppId=ProjectProgress.numOppId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=50 
		AND (ISNULL(LD.constFlag,0)=1 OR LD.numDomainID=@numDomainID)
		AND LD.numListItemID = OM.numPercentageComplete
	WHERE
		OM.numDomainId=@numDomainID
		AND ISNULL(OM.tintOppType,0)=@tintOppType
		AND ISNULL(OM.tintOppStatus,0)=0
		AND (1 = (CASE WHEN (@tintTotalProgress) > 0 THEN (CASE WHEN ISNULL(ProjectProgress.intTotalProgress,0) >= @tintTotalProgress  THEN 1 ELSE 0 END) ELSE 1 END))
		AND	(1= (CASE WHEN OM.monDealAmount BETWEEN @tintMinNumber AND @tintMaxNumber THEN 1 ELSE 0 END))
		AND OM.intPEstimatedCloseDate IS NOT NULL
		--AND OM.intPEstimatedCloseDate < DateAdd(minute, -@ClientTimeZoneOffset,GETUTCDATE())
		AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.intPEstimatedCloseDate) AS DATE) BETWEEN @dtFromDate AND @dtToDate )
		AND OM.bintCreatedDate >= @CreatedDate
		AND 1 = (CASE WHEN LEN(ISNULL(@vcTeritorry,'')) > 0 THEN (CASE WHEN DM.numTerID IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE 
					WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)
					THEN 
						(CASE @vcFilterBy
							WHEN 1  -- Assign To
							THEN (CASE WHEN OM.numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 2  -- Record Owner
							THEN (CASE WHEN OM.numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 3  -- Teams (Based on Assigned To)
							THEN (CASE WHEN OM.numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
						END)
					ELSE 1 
				END)
		AND ( 1 = (CASE WHEN CHARINDEX('1-5K',@vcDealAmount) > 0 THEN (CASE WHEN OM.monDealAmount BETWEEN 1000 AND 5000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('5-10K',@vcDealAmount) > 0 THEN (CASE WHEN OM.monDealAmount BETWEEN 5000 AND 10000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('10-20K',@vcDealAmount) > 0 THEN (CASE WHEN OM.monDealAmount BETWEEN 10000 AND 20000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('20-50K',@vcDealAmount) > 0 THEN (CASE WHEN OM.monDealAmount BETWEEN 20000 AND 50000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('>50K',@vcDealAmount) > 0 THEN (CASE WHEN OM.monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))
			
	ORDER BY
		ISNULL(monTotAmount,0) DESC
END
GO