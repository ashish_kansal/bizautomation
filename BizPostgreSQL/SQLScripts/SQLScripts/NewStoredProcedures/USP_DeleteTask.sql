
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteTask')
DROP PROCEDURE USP_DeleteTask
GO
CREATE PROCEDURE [dbo].[USP_DeleteTask]
@numDomainID as numeric(9)=0,    
@numTaskId as numeric(18)=0 
as    
BEGIN   
	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskId AND tintAction = 4)
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	ELSE IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskId)
	BEGIN
		RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
	END
	ELSE IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND tintAction = 4 AND numTaskID IN (SELECT SPDT.numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numDomainID=@numDomainID AND SPDT.numParentTaskId=@numTaskId))
	BEGIN
		RAISERROR('CHILD_TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	ELSE IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID IN (SELECT SPDT.numTaskId FROM StagePercentageDetailsTask SPDT WHERE SPDT.numDomainID=@numDomainID AND SPDT.numParentTaskId=@numTaskId))
	BEGIN
		RAISERROR('CHILD_TASK_IS_ALREADY_STARTED',16,1)
	END
	ELSE IF ISNULL((SELECT numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId),0) > 0 AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTask WHERE numStageDetailsId=ISNULL((SELECT numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId),0) AND numTaskId <> @numTaskId)
	BEGIN
		RAISERROR('STAGE_MUST_HAVE_AT_LEAST_ONE_TASK',16,1)
		RETURN
	END

	DELETE FROM 
		StagePercentageDetailsTask
	WHERE
		numDomainID=@numDomainID AND numParentTaskId=@numTaskId

	DELETE FROM 
		StagePercentageDetailsTask
	WHERE
		numDomainID=@numDomainID AND numTaskId=@numTaskId
END 
GO