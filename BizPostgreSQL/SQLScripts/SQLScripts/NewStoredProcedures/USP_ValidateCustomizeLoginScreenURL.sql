/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ValidateCustomizeLoginScreenURL')
DROP PROCEDURE USP_ValidateCustomizeLoginScreenURL
GO
CREATE PROCEDURE [dbo].[USP_ValidateCustomizeLoginScreenURL] 
@numDomainID AS NUMERIC=0,
@vcLoginURL AS VARCHAR(500)=''
AS
BEGIN
	IF(LEN(@vcLoginURL)>0)
	BEGIN
		IF(@numDomainID>0)
		BEGIN
			SELECT 
			ISNULL(vcThemeClass,'1473b4') vcThemeClass, 
			ISNULL(vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
			ISNULL(bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
			numDomainId
		FROM 
			Domain 
		WHERE 
			vcLoginURL=@vcLoginURL AND numDomainId<>@numDomainID
		END
		ELSE
		BEGIN
		SELECT 
			ISNULL(vcThemeClass,'1473b4') vcThemeClass, 
			ISNULL(vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
			ISNULL(bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
			numDomainId
		FROM 
			Domain 
		WHERE 
			vcLoginURL=@vcLoginURL
		END
	END
END