GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizFormWizardMasterConfiguration_GetSelectedFields' ) 
    DROP PROCEDURE USP_BizFormWizardMasterConfiguration_GetSelectedFields
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 23 July 2014
-- Description:	Gets list of available fields of form
-- =============================================
CREATE PROCEDURE USP_BizFormWizardMasterConfiguration_GetSelectedFields
	@numDomainID NUMERIC(18,0),
	@numFormID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0),
	@numRelCntType NUMERIC(18,0),
	@tintPageType TINYINT,
	@tintColumn INT,
	@bitGridConfiguration BIT = 0,
	@numFormFieldGroupId NUMERIC(18,0)=0
AS
BEGIN
	
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		(
			SELECT 
				CONVERT(VARCHAR(15),numFieldId) + 'R' AS numFormFieldId,
				ISNULL(vcCultureFieldName, vcFieldName) AS vcNewFormFieldName,
				ISNULL(vcCultureFieldName, vcFieldName) AS vcFormFieldName,
				'R' as vcFieldType,
                vcAssociatedControlType,
                numListID,
                vcDbColumnName,
                vcListItemType,
                intColumnNum intColumnNum,
				intRowNum intRowNum,
                0 as boolRequired,
                ISNULL(numAuthGroupID, 0) numAuthGroupID,
                CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
                0 boolAOIField,
                numFormID,
                vcLookBackTableName,
                CONVERT(VARCHAR(15), numFieldId) + '~' +  CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + '0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(numFormFieldGroupId,0) AS numFormGroupId 
			FROM
				View_DynamicColumnsMasterConfig
			WHERE
				numFormId = @numFormID AND
				numDomainID = @numDomainID AND
				numAuthGroupID = @numGroupID AND
				numRelCntType = @numRelCntType AND
				tintPageType = @tintPageType AND
				intColumnNum = @tintColumn AND
				bitCustom = 0 AND
				bitGridConfiguration = @bitGridConfiguration AND 
				ISNULL(numFormFieldGroupId,0)=ISNULL(@numFormFieldGroupId,0)
			UNION
			SELECT 
				CONVERT(VARCHAR(15),numFieldId) + 'C' AS numFormFieldId,
				vcFieldName AS vcNewFormFieldName,
				vcFieldName AS vcFormFieldName,
				'C' as vcFieldType,
				vcFieldType as vcAssociatedControlType,
				ISNULL(numListID, 0) numListID,
				vcDbColumnName,
				CASE WHEN numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
				intColumnNum intColumnNum,
				intRowNum intRowNum,
				0 as boolRequired,
				0 numAuthGroupID,
				CASE WHEN numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
				0 boolAOIField,
				@numFormId AS numFormID,
				'' AS vcLookBackTableName,
				CONVERT(VARCHAR(15), numFieldId) + '~1' + '~0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(numFormFieldGroupId,0) AS numFormGroupId
			FROM
				View_DynamicCustomColumnsMasterConfig 
			WHERE
				numFormId = @numFormID AND
				numDomainID = @numDomainID AND
				numAuthGroupID = @numGroupID AND
				numRelCntType = @numRelCntType AND
				tintPageType = @tintPageType AND
				intColumnNum = @tintColumn AND
				bitCustom = 1 AND
				bitGridConfiguration = @bitGridConfiguration AND 
				ISNULL(numFormFieldGroupId,0)=ISNULL(@numFormFieldGroupId,0)
				
		) AS TEMPFINAL
	ORDER BY
		TEMPFINAL.intRowNum
END
GO