
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTeamsWithCapacityLoadForProject')
DROP PROCEDURE USP_GetTeamsWithCapacityLoadForProject
GO
CREATE PROCEDURE [dbo].[USP_GetTeamsWithCapacityLoadForProject]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@tintDateRange TINYINT -- 1:Today, 2:Week, 3:Month
	,@dtFromDate DATETIME
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	 SELECT 
		LD.numListItemID numTeamID
		,ISNULL(vcData,'') as vcTeamName
		,[dbo].[GetProjectCapacityLoad](@numDomainID,0,LD.numListItemID,@numProId,0,@tintDateRange,@dtFromDate,@ClientTimeZoneOffset) numCapacityLoad
	FROM 
		ListDetails LD                 
	WHERE 
		LD.numDomainID=@numDomainID 
		AND LD.numListID=35
END
