GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_RecurrenceErrorLog_Insert' ) 
    DROP PROCEDURE USP_RecurrenceErrorLog_Insert
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 10 April 2014
-- Description:	Logs recurrence error
-- =============================================
CREATE PROCEDURE USP_RecurrenceErrorLog_Insert
	@numDomainID NUMERIC(18,0),
	@numRecurConfigID NUMERIC(18,0),
	@Type NVARCHAR(100),
	@Source NVARCHAR(100),
	@Message NVARCHAR(MAX),
    @StackStrace NVARCHAR(MAX),
	@bitUpdateTransaction BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO RecurrenceErrorLog 
	(
		numDomainID,
		numRecConfigID,
		vcType,
		vcSource,
		vcMessage,
		vcStackStrace,
		CreatedDate
	) 
	VALUES 
	(
		@numDomainID,
		@numRecurConfigID,
		@Type,
		@Source,
		@Message,
		@StackStrace,
		GETDATE()
	)

	DECLARE @ErrorID AS INT
	SET @ErrorID = SCOPE_IDENTITY()


	IF @bitUpdateTransaction = 1
	BEGIN
		IF EXISTS (SELECT numRecTranID FROM RecurrenceTransaction WHERE numRecConfigID = @numRecurConfigID AND CAST(dtCreatedDate AS DATE) = CAST(GETDATE() AS DATE) AND numErrorID IS NULL)
		BEGIN
			UPDATE 
				RecurrenceTransaction 
			SET 
				numErrorID = @ErrorID
			WHERE 
				numRecConfigID = @numRecurConfigID AND 
				CAST(dtCreatedDate AS DATE) = CAST(GETDATE() AS DATE) AND 
				numRecTranID = (
								SELECT 
									MAX(numRecTranID) 
								FROM 
									RecurrenceTransaction 
								WHERE 
									numRecConfigID = @numRecurConfigID AND 
									CAST(dtCreatedDate AS DATE) = CAST(GETDATE() AS DATE)
								)
		END
		ELSE
		BEGIN
			INSERT INTO RecurrenceTransaction (numRecConfigID,numErrorID, dtCreatedDate) VALUES (@numRecurConfigID,@ErrorID, GETDATE())
		END
	END

END
GO