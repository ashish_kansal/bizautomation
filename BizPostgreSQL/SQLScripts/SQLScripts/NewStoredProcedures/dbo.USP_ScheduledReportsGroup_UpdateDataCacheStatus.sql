GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_UpdateDataCacheStatus')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_UpdateDataCacheStatus
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_UpdateDataCacheStatus]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@tintDataCacheStatus TINYINT
	,@vcExceptionMessage NVARCHAR(MAX)
)
AS 
BEGIN
	UPDATE 
		ScheduledReportsGroup 
	SET 
		tintDataCacheStatus = @tintDataCacheStatus
		,vcExceptionMessage = @vcExceptionMessage
	WHERE 
		numDomainID=@numDomainID AND ID=@numSRGID
END
GO