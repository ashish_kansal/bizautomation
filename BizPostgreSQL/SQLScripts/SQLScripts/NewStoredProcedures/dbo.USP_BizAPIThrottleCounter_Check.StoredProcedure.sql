GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPIThrottleCounter_Check' ) 
    DROP PROCEDURE USP_BizAPIThrottleCounter_Check
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 16 April 2014
-- Description:	Check whether record exists with given id in BizAPIThrottleCounter
-- =============================================
CREATE PROCEDURE USP_BizAPIThrottleCounter_Check
	@ID VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT COUNT(*) FROM dbo.BizAPIThrottleCounter WHERE vcID = @ID
END
GO