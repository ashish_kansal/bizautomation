SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageReportDashboardSize')
DROP PROCEDURE USP_ManageReportDashboardSize
GO
CREATE PROCEDURE  [dbo].[USP_ManageReportDashboardSize] 
@numDomainID as numeric(9)=0,
@numUserCntID as numeric(9)=0,
@tintColumn as tinyint, 
@tintSize as tinyint
as
update ReportDashBoardSize set
		 tintSize=@tintSize
	WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND tintColumn=@tintColumn 
GO
