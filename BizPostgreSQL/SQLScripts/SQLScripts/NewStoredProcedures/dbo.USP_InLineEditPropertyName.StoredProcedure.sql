SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditPropertyName')
DROP PROCEDURE USP_InLineEditPropertyName
GO
CREATE PROCEDURE [dbo].[USP_InLineEditPropertyName] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0
 )
AS 
BEGIN
	IF @bitCustom =0
	BEGIN
		SELECT 
			vcDbColumnName
			,vcOrigDbColumnName
			,ISNULL(vcPropertyName,'') vcPropertyName
			,numListID
			,CASE 
				WHEN vcAssociatedControlType='SelectBox' 
				THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID and numDomainID=@numDomainId),0) 
				ELSE 0 
			END AS ListRelID
			,vcListItemType
		 FROM 
			DycFieldMaster 
		WHERE 
			numFieldId=@numFormFieldId
	END 
	ELSE
	BEGIN
		SELECT 
			'' AS vcDbColumnName
			,'' AS vcOrigDbColumnName
			,'' vcPropertyName
			,numListID
			,CASE 
				WHEN fld_type='SelectBox' 
				THEN ISNULL((SELECT numPrimaryListID FROM FieldRelationship WHERE numSecondaryListID=numListID AND numDomainID=@numDomainId),0) 
				ELSE 0 
			END AS ListRelID
		 FROM 
			CFW_Fld_Master 
		WHERE 
			Fld_id=@numFormFieldId 
	END
END
GO