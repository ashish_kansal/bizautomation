
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DashboardTemplate_GetAllByDomain')
DROP PROCEDURE USP_DashboardTemplate_GetAllByDomain
GO
CREATE PROCEDURE [dbo].[USP_DashboardTemplate_GetAllByDomain]      
@numDomainID AS NUMERIC(18,0)
,@numUserCntID AS NUMERIC(18,0) = 0
AS
BEGIN 

	IF(@numUserCntID=0)
		BEGIN
			SELECT * FROM DashboardTemplate WHERE numDomainID=@numDomainID
		END
	ELSE
		BEGIN
			DECLARE @vcDashboardTemplateIDs AS VARCHAR(250)
			
			SELECT @vcDashboardTemplateIDs = vcDashboardTemplateIDs from UserMaster where numDomainID=@numDomainID	AND numUserDetailId	=@numUserCntID

			IF LEN(ISNULL(@vcDashboardTemplateIDs,'')) = 0
				BEGIN
					SELECT  @vcDashboardTemplateIDs = numDashboardTemplateID FROM UserMaster WHERE numDomainID=@numDomainID
				END
			
			SELECT * FROM DashboardTemplate WHERE numDomainID=@numDomainID AND 
			1= (CASE WHEN LEN(ISNULL(@vcDashboardTemplateIDs,'')) > 0 THEN (CASE WHEN numTemplateID IN (SELECT Id FROM dbo.SplitIDs((@vcDashboardTemplateIDs),',')) THEN 1 ELSE 0 END) ELSE 1 END)
			
		END
END
GO