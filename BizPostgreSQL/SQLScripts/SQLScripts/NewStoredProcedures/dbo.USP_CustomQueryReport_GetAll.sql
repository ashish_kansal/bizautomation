SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_GetAll')
DROP PROCEDURE dbo.USP_CustomQueryReport_GetAll
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_GetAll]

AS 
BEGIN
	SELECT 
		CustomQueryReport.numReportID,
		CustomQueryReport.numDomainID,
		Domain.vcDomainName,
		CustomQueryReport.vcReportName,
		CustomQueryReport.vcReportDescription,
		CustomQueryReport.vcEmailTo,
		(CASE CustomQueryReport.tintEmailFrequency WHEN 1 THEN 'Daily' WHEN 2 THEN 'Weekly' WHEN 3 THEN 'Monthly' WHEN 4 THEN 'Yearly' END) As vcEmailFrequency
	FROM 
		CustomQueryReport
	INNER JOIN
		Domain
	ON
		CustomQueryReport.numDomainID = Domain.numDomainId
	ORDER BY
		Domain.vcDomainName ASC
END