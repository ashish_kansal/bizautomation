SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemByRevenuePreBuildReport')
DROP PROCEDURE USP_ReportListMaster_Top10ItemByRevenuePreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemByRevenuePreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	SELECT TOP 10
		numItemCode
		,vcItemName
		,SUM(monTotAmount) TotalAmount
	FROM
	(
		SELECT
			I.numItemCode
			,I.vcItemName
			,ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
	) TEMP
	GROUP BY
		numItemCode
		,vcItemName
	HAVING
		SUM(monTotAmount) > 0
	ORDER BY
		SUM(monTotAmount) DESC
END
GO