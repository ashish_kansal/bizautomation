/****** Object:  StoredProcedure [dbo].[USP_DeleteCommissionRule]    Script Date: 07/26/2008 16:15:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_DeleteCommissionRule')
DROP PROCEDURE usp_DeleteCommissionRule
GO
CREATE PROCEDURE [dbo].[USP_DeleteCommissionRule]
@numComRuleID as numeric(9)=0
AS

DELETE FROM CommissionRuleContacts WHERE numComRuleID=@numComRuleID
DELETE FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID
delete from CommissionRuleDtl where numComRuleID=@numComRuleID
delete from CommissionRules where numComRuleID=@numComRuleID


GO
