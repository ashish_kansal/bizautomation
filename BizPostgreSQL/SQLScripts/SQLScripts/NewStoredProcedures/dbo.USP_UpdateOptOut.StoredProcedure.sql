-- procedure will unscubscribe contact from email broadcast
--USP_UpdateOptOut 82930,'chintanprajapati@gmail.com',0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateOptOut')
DROP PROCEDURE USP_UpdateOptOut
GO
CREATE PROCEDURE  USP_UpdateOptOut
    @numContactID NUMERIC,
    @vcEmailID VARCHAR(50),
    @bitOptOut BIT,
    @numBroadCastID NUMERIC
AS 
    BEGIN
        UPDATE  [AdditionalContactsInformation]
        SET     [bitOptOut] = @bitOptOut
        WHERE   [numContactId] = @numContactID
                AND [vcEmail] = @vcEmailID
        IF @bitOptOut = 1
           BEGIN
                 Update BroadCastDtls set bitUnsubscribe = 1 where numBroadcastID = @numBroadcastID and numContactID = @numContactID
           END
END