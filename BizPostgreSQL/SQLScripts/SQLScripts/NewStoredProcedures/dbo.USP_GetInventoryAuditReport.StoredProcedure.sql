SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetInventoryAuditReport' ) 
    DROP PROCEDURE USP_GetInventoryAuditReport
GO
CREATE PROCEDURE [dbo].[USP_GetInventoryAuditReport]
    @numDomainID AS NUMERIC(9)
AS 

-----------------------------SO-------------------------
select Opp.numItemCode,opp.numWarehouseItmsID,SUM(opp.numUnitHour - ISNULL(opp.numQtyShipped,0)) SOQty
INTO #tempSO
 FROM   OpportunityItems Opp
        JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
		JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
        LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
WHERE   om.numDomainId = @numDomainID
        AND I.numDomainId = @numDomainID 
        AND ISNULL(OM.tintshipped, 0)=0 AND OM.tintOppStatus = 1
        AND OM.tintOppType=1 AND opp.numWarehouseItmsID>0 AND ISNULL(Opp.bitDropShip,0)=0
        GROUP BY Opp.numItemCode,opp.numWarehouseItmsID

SELECT i.vcItemName,W.vcWareHouse,WI.numAllocation,Wi.numBackOrder,ISNULL(WI.numAllocation,0) + ISNULL(wi.numBackOrder,0) AS WTotal,temp.SOQty
FROM Item i JOIN #tempSo temp ON i.numItemCode=temp.numItemCode
			JOIN dbo.WareHouseItems WI ON WI.numItemID=i.numItemCode AND WI.numWareHouseItemID=temp.numWarehouseItmsID
			JOIN dbo.Warehouses W ON W.numWareHouseID=WI.numWareHouseID
WHERE (ISNULL(WI.numAllocation,0) + ISNULL(wi.numBackOrder,0))<> temp.SOQty		
			
DROP TABLE 	#tempSO		


----------------------PO--------------
select Opp.numItemCode,opp.numWarehouseItmsID,SUM(opp.numUnitHour - ISNULL(opp.numUnitHourReceived,0)) POQty
INTO #tempPO
 FROM   OpportunityItems Opp
        JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
		JOIN divisionMaster DM ON OM.numDivisionID = DM.numDivisionID
        LEFT JOIN CompanyInfo CI ON CI.numCompanyID = DM.numCompanyID
WHERE   om.numDomainId = @numDomainID
        AND I.numDomainId = @numDomainID 
        AND ISNULL(OM.tintshipped, 0)=0 AND OM.tintOppStatus = 2
        AND OM.tintOppType=1 AND opp.numWarehouseItmsID>0 AND ISNULL(Opp.bitDropShip,0)=0
        GROUP BY Opp.numItemCode,opp.numWarehouseItmsID

SELECT i.vcItemName,W.vcWareHouse,WI.numOnOrder,temp.POQty
FROM Item i JOIN #tempPO temp ON i.numItemCode=temp.numItemCode
			JOIN dbo.WareHouseItems WI ON WI.numItemID=i.numItemCode AND WI.numWareHouseItemID=temp.numWarehouseItmsID
			JOIN dbo.Warehouses W ON W.numWareHouseID=WI.numWareHouseID
WHERE ISNULL(WI.numOnOrder,0)<> temp.POQty		
			
DROP TABLE 	#tempPO